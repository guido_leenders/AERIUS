{import_common 'system/layers.sql'}

--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (11, 11, 'wms', 'calculator:wms_nature_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (12, 12, 'wms', 'calculator:wms_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (13, 13, 'wms', 'calculator:wms_habitat_areas_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (14, 14, 'wms', 'calculator:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (15, 15, 'wms', 'verkeersscheidingsstelsel_nz:begrenzing,verkeersscheidingsstelsel_nz:symbolen,verkeersscheidingsstelsel_nz:separatiezones');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (16, 16, 'wms', 'calculator:wms_shipping_maritime_network_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (17, 10, 'wms', 'calculator:wms_habitats_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (18, 10, 'wms', 'calculator:wms_relevant_habitat_info_for_receptor_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (19, 25, 'wms', 'calculator:wms_calculation_substance_deposition_results_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (20, 27, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (21, 28, 'wms', 'calculator:wms_province_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (22, 29, 'wms', 'calculator:wms_inland_shipping_routes');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (23, 30, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (24, 31, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (25, 32, 'wms', 'calculator:wms_user_source_labels');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (26, 33, 'wms', 'calculator:wms_user_road_speed_types');	
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (27, 34, 'wms', 'calculator:wms_user_road_total_vehicles_per_day');	
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (28, 35, 'wms', 'calculator:wms_user_road_total_vehicles_per_day_light_traffic');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (29, 36, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_label');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (30, 37, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage_label');


-- default layers
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (1, 0, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (21, 1, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (3, 2, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (4, 3, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (11, 4, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (12, 5, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (13, 6, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (14, 7, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (15, 8, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (16, 9, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (22, 10, false);