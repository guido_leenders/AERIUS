BEGIN; SELECT setup.ae_load_table('background_cells', '{data_folder}/public/background_cells_20160817.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('background_cell_depositions', '{data_folder}/public/background_cell_depositions_20190620.txt'); COMMIT;


BEGIN;
	INSERT INTO background_cell_results
		SELECT background_cell_id, year, substance_id, result_type, result FROM setup.build_background_cell_results_view;
COMMIT;

BEGIN;
	INSERT INTO receptors_to_background_cells
		SELECT receptor_id, background_cell_id FROM setup.build_receptors_to_background_cells_view;
COMMIT;