/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.calculationpoint;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.DetermineCalculationPointResult;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.wui.calculator.calculationpoint.DetermineCalculationPointsDialogPanel.DetermineCalculationPointsDialogDriver;
import nl.overheid.aerius.wui.calculator.calculationpoint.DetermineCalculationPointsDialogPanel.DetermineReceptorPointsProperties;
import nl.overheid.aerius.wui.calculator.place.CalculationPointsPlace;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Controller for starting the Export dialog and communicating with the server.
 */
@Singleton
public class DetermineCalculationPointsDialogController {

  // 1000 meters = 1 KM. Who would have guessed that.. not me!
  private static final int METERS_TO_KM = 1000;

  private final SimpleBeanEditorDriver<DetermineReceptorPointsProperties, DetermineCalculationPointsDialogPanel> driver = GWT
      .create(DetermineCalculationPointsDialogDriver.class);
  private final ConfirmCancelDialog<DetermineReceptorPointsProperties, DetermineCalculationPointsDialogPanel> dialog;
  private final DetermineCalculationPointsDialogPanel contentPanel;
  private final CalculatorServiceAsync service;
  private final CalculatorUserContext userContext;
  private final PlaceController placeController;
  private ScenarioBasePlace place;
  private DetermineCalculationPointResult result;
  private final DetermineReceptorPointsProperties determineProperties;

  private final ConfirmHandler<DetermineReceptorPointsProperties> confirmHandler = new ConfirmHandler<DetermineReceptorPointsProperties>() {
    @Override
    public void onConfirm(final ConfirmEvent<DetermineReceptorPointsProperties> event) {
      final CalculationPointList pointList = userContext.getCalculationPoints();
      for (final AeriusPoint newCp : result.getPoints()) {
        final String label = newCp.getLabel();
        if (label != null && label.startsWith(DetermineCalculationPointResult.OVERLAP_CENTROID_START)) {
          newCp.setLabel(M.messages().determineCalcPointOverlapCentroid(
              label.substring(DetermineCalculationPointResult.OVERLAP_CENTROID_START.length())));
        }
        pointList.add(newCp);
      }
      dialog.hide();
      placeController.goTo(new CalculationPointsPlace(place));
    }
  };

  private final ClickHandler calculateHandler = new ClickHandler() {

    @Override
    public void onClick(final ClickEvent event) {
      driver.flush();
      if (driver.hasErrors()) {
        ErrorPopupController.addErrors(driver.getErrors());
        return;
      }

      final ArrayList<EmissionSourceList> sourceLists = new ArrayList<EmissionSourceList>();
      sourceLists.add(userContext.getSources(place.getSid1()));
      if (sourceLists.get(0).isEmpty()) {
        contentPanel.showWarningNoSource();
        return;
      }

      dialog.showProgressImage(true);

      if (place.has2Situations()) {
        sourceLists.add(userContext.getSources(place.getSid2()));
      }

      service.determinePointsAutomatically(determineProperties.getDistanceFromSource(), sourceLists,
          new AppAsyncCallback<DetermineCalculationPointResult>() {

        @Override
        public void onSuccess(final DetermineCalculationPointResult result) {
          contentPanel.showAdditionalInfo(true, result.getPoints().size(), result.getAmountOfAssessmentAreas());
          dialog.showProgressImage(false);
          DetermineCalculationPointsDialogController.this.result = result;
          dialog.enableConfirmButton(!result.getPoints().isEmpty());
        }

        @Override
        public void onFailure(final Throwable caught) {
          dialog.showProgressImage(false);
          dialog.enableConfirmButton(false);
          super.onFailure(caught);
        }
      });
    }
  };

  @Inject
  public DetermineCalculationPointsDialogController(final CalculatorServiceAsync service,
      final CalculatorContext context, final CalculatorUserContext userContext,
      final PlaceController placeController) {
    this.service = service;
    this.userContext = userContext;
    this.placeController = placeController;

    contentPanel = new DetermineCalculationPointsDialogPanel(
        (Integer) context.getSetting(SharedConstantsEnum.CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI) / METERS_TO_KM);

    dialog = new ConfirmCancelDialog<DetermineReceptorPointsProperties, DetermineCalculationPointsDialogPanel>(driver,
        M.messages().determineCalcPointsReallyConfirm(),
        M.messages().cancelButton());
    dialog.addConfirmHandler(confirmHandler);
    dialog.setHTML(M.messages().determineCalcPointsTitle());
    dialog.setWidget(contentPanel);

    contentPanel.addClickHandlerOnNotSoUselessCalculateButton(calculateHandler);

    determineProperties = new DetermineReceptorPointsProperties();
  }

  /**
   * Show the dialog with the given scenario as input to used to export.
   * @param place current place
   */
  public void show(final ScenarioBasePlace place) {
    this.place = place;

    // Reset errors styles on widgets
    ErrorPopupController.clearWidgets();
    // Reset some default panel stuff.
    contentPanel.showAdditionalInfo(false, -1, -1);

    result = null;
    dialog.enableConfirmButton(false);

    driver.initialize(contentPanel);
    driver.edit(determineProperties);
    dialog.center();
  }
}
