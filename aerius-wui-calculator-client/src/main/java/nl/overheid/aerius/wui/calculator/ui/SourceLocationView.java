/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.HasTitle;

/**
 * Interface for the view to select the location of a source.
 */
public interface SourceLocationView extends IsWidget, HasTitle {

  /**
   * Enum of available tools to draw different geometries to specify a
   * emission source location.
   */
  enum Tool {
    POINT, LINE, POLYGON, EDIT;

    /**
     * Returns the Tool given a {@link WKTGeometry#Type} object.
     *
     * @param type type to get tool for
     * @return Tool for given type
     */
    public static Tool wkt2Tool(final WKTGeometry.TYPE type) {
      final Tool tool;
      switch (type) {
      case LINE:
        tool = Tool.LINE;
        break;
      case POLYGON:
        tool = Tool.POLYGON;
        break;
      case POINT:
      default:
        tool = Tool.POINT;
        break;
      }
      return tool;
    }
  }

  /**
   * Presenter for the {@link SourceLocationView}.
   */
  interface Presenter extends Activity {
    /**
     * Call when to proceed to setting source details.
     */
    void nextStep();

    /**
     * Call when cancel input.
     */
    void cancelEdit();

    /**
     * Call when user switches tool.
     *
     * @param tool new tool to use
     * @param track whether or not to track the measurement while measuring
     */
    void onSwitchTool(Tool tool, boolean track);

    /**
     * Updates or places the point based on manual input by the user.
     *
     * @param xCoordinate new x coordinate
     * @param yCoordinate new y coordinate
     */
    void updatePosition(Integer xCoordinate, Integer yCoordinate);

    /**
     * Updates the position of a line or polygon.
     *
     * @param value list of x-y coordinates of line or polygon
     */
    void updatePosition(String value);
  }

  /**
   * Set state of next step button.
   *
   * @param nextStep if true button will be enabled
   */
  void setEnabledNextStep(boolean nextStep);

  /**
   * Set the presenter for the view.
   *
   * @param presenter Presenter to use
   */
  void setPresenter(Presenter presenter);

  /**
   * Set the values of the x and y coordinates on the view.
   *
   * @param x x-coordinate
   * @param y y-coordinate
   */
  void setXY(double x, double y);

  /**
   * Set the coordinates of a line or polygon on the view.
   *
   * @param coordinates coordinates
   */
  void setMultiCoordinates(String coordinates);

  /**
   * Select the tool buttons state, but doesn't fire a change event.
   * Method used to set initial state.
   *
   * @param tool Tool to select
   */
  void selectTool(Tool tool);

  /**
   * Indicates whether or not the emission source location is being edited or created.
   *
   * @param editMode true for yay, false for nay
   */
  void setEditMode(boolean editMode);

  /**
   * Set the enabled state of the edit button. If no geometry or if a point the edit button should be disabled.
   *
   * @param enabled if true button is enabled
   */
  void setEnabledEdit(boolean enabled);

  /**
   * @param fail if true creates fail text color.
   * @param tool if fail this is the tool to activate
   * @param errorMessage erorr message of failure
   */
  void invalidMultiCoordinatesWarning(boolean fail, Tool tool, String errorMessage);

  /**
   * Set the input field invalid if calculation of points fails.
   *
   * @param fail if true creates fail text color.
   */
  void invalidInputMultiCoordinates(boolean fail);

  /**
   * Set the partial measurement, which has not been confirmed yet, can be used for partial updates.
   * 
   * @param measure Depending on selected tool, either 1 (point), distance (line), or surface (polygon).
   */
  void setPartialMeasurement(double measure);
}
