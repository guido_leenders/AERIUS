/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.wui.calculator.ui.ResultOverviewView.Presenter;
import nl.overheid.aerius.wui.calculator.ui.melding.MeldingExportController;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationController;
import nl.overheid.aerius.wui.scenario.base.ui.ResultActivity;
import nl.overheid.aerius.wui.scenario.base.ui.ResultView;

/**
 * Presenter for displaying calculation status.
 */
public class ResultOverviewPresenter extends ResultActivity implements Presenter {
  private final ResultOverviewView view;
  private final MeldingExportController meldingController;

  @Inject
  public ResultOverviewPresenter(final ScenarioBaseAppContext<?, ?> appContext, final PlaceController placeController,
      final CalculationController calculationController, final ResultView resultView, final ResultOverviewView view,
      final ScenarioBaseMapLayoutPanel map, final MeldingExportController meldingController) {
    super(resultView, appContext, placeController, calculationController, ResultPlace.ScenarioBaseResultPlaceState.OVERVIEW, map);
    this.view = view;
    this.meldingController = meldingController;
  }

  @Override
  public void startChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    view.setPresenter(this);
    panel.setWidget(view);
    view.setCalculationSummary(appContext.getCalculationSummary());
  }

  @Override
  public void onCalculationSummaryChanged(final CalculationSummaryChangedEvent event) {
    // TODO Does this get called, or parent? (not sure what eventHandler is doing here)
    view.setCalculationSummary(event.getValue());
  }

  @Override
  public void postMelding() {
    meldingController.showPostMeldingDialog();
  }
}
