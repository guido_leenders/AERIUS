/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcePlace;

/**
 * Place for location source.
 */
public class SourceLocationPlace extends SourcePlace {
  private static final String PREFIX = "sloc";

  /**
   * Tokenizer for {@link CalculatorPlace}.
   */
  @Prefix(PREFIX)
  public static class Tokenizer extends SourcePlace.Tokenizer<SourceLocationPlace> implements PlaceTokenizer<SourceLocationPlace> {

    @Override
    protected SourceLocationPlace createPlace() {
      return new SourceLocationPlace();
    }
  }

  // Empty location should mean no source object in user data available.
  public SourceLocationPlace() {
    super(0, null);
  }

  public SourceLocationPlace(final int sourceId, final ScenarioBasePlace currentPlace) {
    super(sourceId, currentPlace);
  }

  public SourceLocationPlace(final SourceLocationPlace currentPlace) {
    super(currentPlace);
  }

  @Override
  public SourcePlace copy() {
    return copyTo(new SourceLocationPlace(this));
  }
}
