/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SectorChanger;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.place.SourceDetailPlace;
import nl.overheid.aerius.wui.calculator.place.SourceLocationPlace;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.event.InputListEmissionItemEditEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceBasicInfoEditor.ESBasicInfoDriver;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceEditor.EmissionSourceDriver;

/**
 * Presenter class for editing a source.
 */
public class SourceDetailPresenter extends AbstractActivity implements SourceDetailView.Presenter {

  interface SourceDetailPresenterEventBinder extends EventBinder<SourceDetailPresenter> {
  }

  private final SourceDetailPresenterEventBinder eventBinder = GWT.create(SourceDetailPresenterEventBinder.class);

  private final SourceDetailView view;
  private final ScenarioBaseMapLayoutPanel map;
  private final PlaceController placeController;
  private final CalculatorAppContext appContext;
  private final CalculatorUserContext userContext;
  private final SourceDetailPlace place;
  private EmissionSource emissionSource;
  private final SimpleBeanEditorDriver<EmissionSource, Editor<EmissionSource>> basicInfoDriver = GWT
      .create(ESBasicInfoDriver.class);
  private final SimpleBeanEditorDriver<EmissionSource, EmissionSourceEditor> driver = GWT
      .create(EmissionSourceDriver.class);

  private EventBus eventBus;
  private boolean removeNewES;

  @Inject
  public SourceDetailPresenter(final PlaceController placeController, final SourceDetailView view,
      final ScenarioBaseMapLayoutPanel map,
      final CalculatorAppContext appContext, final CalculatorContext context, final CalculatorUserContext userContext,
      @Assisted final SourceDetailPlace place, @Assisted final EmissionSource emissionSource) {
    this.placeController = placeController;
    this.appContext = appContext;
    this.userContext = userContext;
    this.place = place;
    this.view = view;
    this.map = map;
    this.emissionSource = emissionSource;
  }

  @Override
  public void onCancel() {
    super.onCancel();
    cleanUp();
  }

  @Override
  public void onStop() {
    super.onStop();
    cleanUp();
  }

  /**
   * If a new source is edited and the user moves away from this page the source is removed from the list. If the user clicked save or edit location
   * the source is not removed from the list. A new source is a 'new' source if the object matches the exact object in the
   * appContext.getNewEmissionSource().
   */
  private void cleanUp() {
    if (removeNewES && emissionSource == appContext.getNewEmissionSource()
        && userContext.getSources(place.getActiveSituationId()).remove(emissionSource)) {
      eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), emissionSource, CHANGE.REMOVE));
    }
    ErrorPopupController.clearWidgets();
  }

  @Override
  public void cancelEdit() {
    if (userContext.getSourceListCopy() != null && !userContext.getSourceListCopy().isEmpty()) {
      // restore
      final EmissionSourceList current = userContext.getSources(place.getActiveSituationId());
      current.clear();
      current.setAll(userContext.getSourceListCopy());
      placeController.goTo(new SourcesPlace(place));
      userContext.getSourceListCopy().clear();
    } else {
      cleanUp();
      appContext.clearNewEmissionSource();
      placeController.goTo(new SourcesPlace(place));
    }
  }

  @Override
  public void nextStep() {
    removeNewES = false;
    final EmissionSource es = driver.flush();

    if (driver.hasErrors()) {
      ErrorPopupController.addErrors(driver.getErrors());
    } else {
      emissionSource = es;

      /* At this time SRM2EmissionSource implements HasOPSSourceCharacteristics also, while it cannot be edited.
       *  So we ignore it specifically for now as warning an user about it is pretty pointless in such a case. */
      if (emissionSource instanceof HasOPSSourceCharacteristics && !(emissionSource instanceof SRM2EmissionSource)) {
        warnIfOPSSourceCharacteristicsAreDefault();
      }

      final EmissionSourceList lst = userContext.getSources(place.getActiveSituationId());
      lst.replace(emissionSource);
      nextPlace();
    }
  }

  private void nextPlace() {
    // If no id, it's a new object
    if (emissionSource.getId() == 0) {
      // If no label set, set a default label, but only for new sources
      if (emissionSource.getLabel() == null) {
        emissionSource.setLabel(M.messages().source() + " " + emissionSource.getId());
      }
      eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), emissionSource, CHANGE.REMOVE));
      userContext.getSources(place.getActiveSituationId()).add(emissionSource);
      eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), emissionSource, CHANGE.ADD));
    } else {
      eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), emissionSource, CHANGE.UPDATE));
    }
    appContext.clearNewEmissionSource();
    placeController.goTo(new SourcesPlace(place));
  }

  private void warnIfOPSSourceCharacteristicsAreDefault() {
    if (emissionSource.getSourceCharacteristics().equals(emissionSource.getSector().getDefaultCharacteristics())) {
      // temporary disable this warning can not determine exact amout of values changed by the user. AER-1852
      // NotificationUtil.broadcastMessage(eventBus, M.messages().notificationSourceWarnDefaultOPS());
    }
  }

  @Override
  public void onValueChange(final ValueChangeEvent<Sector> event) {
    final Sector sector = event.getValue();
    final boolean validSector = sector != null && sector != Sector.SECTOR_UNDEFINED;
    view.updateDetailsVisiblity(sector);
    if (validSector) {
      final EmissionSource newEmissionSource = SectorChanger.changeSector(emissionSource, sector);
      newEmissionSource.setLabel(view.getEmissionSourceEditor().getLabelEditor().getValue());
      editEmissionSource(newEmissionSource);
      //right, so if you want to change color of the icon on the map the second the user picks a sector, enable next line.
      //however, if you want to avoid invalidating results when a sector is picked on edit/new source, keep it disabled.
      //firing a EmissionSourceChangeEvent will cause this to happen, even if user doesn't clicks save but uses the menu to go back to the list.
      //      eventBus.fireEvent(new EmissionSourceChangeEvent(place.getActiveSituationId(), newEmissionSource, CHANGE.UPDATE));
    }
  }

  @Override
  public void editLocation() {
    removeNewES = false;
    final ScenarioBasePlace place = (ScenarioBasePlace) placeController.getWhere();
    placeController.goTo(new SourceLocationPlace(emissionSource.getId(), place));
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;
    removeNewES = true;
    view.setPresenter(this);
    map.setVisibleSubstanceLayers(false);
    panel.setWidget(view);
    driver.initialize(view.getEmissionSourceEditor());
    basicInfoDriver.initialize(view.getBasicInfoEditor());
    view.setSourceDetailVisibility(emissionSource.getSector());
    editEmissionSource(emissionSource);
    eventBinder.bindEventHandlers(this, eventBus);
  }

  /**
   * @param es
   */
  private void editEmissionSource(final EmissionSource es) {
    view.getEmissionSourceEditor().setEmissionValueKey(userContext.getEmissionValueKey());
    driver.edit(es);
    basicInfoDriver.edit(es);
  }

  @EventHandler
  void onSubstanceChange(final EmissionResultKeyChangeEvent event) {
    view.getEmissionSourceEditor().updateEmissionValueKey(userContext.getEmissionValueKey());
  }

  @EventHandler
  void onYearChange(final YearChangeEvent event) {
    view.getEmissionSourceEditor().updateEmissionValueKey(userContext.getEmissionValueKey());
  }

  @EventHandler
  void onInputListEmissionItemEdit(final InputListEmissionItemEditEvent event) {
    view.setNextButtonVisible(!event.getValue());
  }

}
