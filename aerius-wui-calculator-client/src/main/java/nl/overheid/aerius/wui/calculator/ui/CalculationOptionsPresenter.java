/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.ui.CalculationOptionsPanel.CalculationOptionsPanelDriver;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;

/**
 * Presenter for Calculation Options.
 */
public class CalculationOptionsPresenter implements CalculationOptionsView.Presenter {
  private final CalculationOptionsPanelDriver calculationOptionsDriver = GWT.create(CalculationOptionsPanelDriver.class);

  interface CalculationOptionsEventBinder extends EventBinder<CalculationOptionsPresenter> { }

  private final CalculationOptionsEventBinder eventBinder = GWT.create(CalculationOptionsEventBinder.class);

  private final CalculationOptionsPanel view;
  private final CalculatorAppContext appContext;

  @Inject
  public CalculationOptionsPresenter(final PlaceController placeController, final ScenarioBaseCalculationProcessor processor,
      final EventBus eventBus, final CalculatorAppContext appContext, final CalculationOptionsPanel view) {
    this.appContext = appContext;
    this.view = view;

    view.setPresenter(this);

    // Initialize the calculation options driver
    calculationOptionsDriver.initialize(view);
    calculationOptionsDriver.edit(appContext.getCalculationOptions());
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @Override
  public boolean saveOptions(final boolean triggerErrors) {
    final CalculationSetOptions options = calculationOptionsDriver.flush();

    if (triggerErrors && calculationOptionsDriver.hasErrors()) {
      ErrorPopupController.addErrors(calculationOptionsDriver.getErrors());
      return false;
    }

    if (!view.isEndYearValid()) {
      return false;
    }

    appContext.setCalculationOptions(options);
    return true;
  }

  @Override
  public CalculationOptionsView getCalculationOptions() {
    return view;
  }
}
