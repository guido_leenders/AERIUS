/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.processor;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.service.CalculateServiceAsync;
import nl.overheid.aerius.wui.scenario.base.processor.CalculationHighByDistancePollingAgent;
import nl.overheid.aerius.wui.scenario.base.processor.CalculationInitPollingAgent;
import nl.overheid.aerius.wui.scenario.base.processor.CalculationPollingAgent;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;

/**
 * Cancel() may be called when a calculation needs to be interrupted.
 */
@Singleton
public class CalculatorResultProcessor extends ScenarioBaseCalculationProcessor {
  @Inject
  public CalculatorResultProcessor(final CalculateServiceAsync service, final CalculationPollingAgent resultAgent,
      final CalculationInitPollingAgent initAgent, final CalculationHighByDistancePollingAgent progressAgent,
      final Context context, final EventBus eventBus) {
    super(service, resultAgent, initAgent, progressAgent, context, eventBus);
  }

  @Override
  protected void doStartCalculationServiceCall(final CalculatedScenario scenario, final String key, final AsyncCallback<String> callback) {
    service.startCalculation(scenario, key, callback);
  }

  @Override
  protected boolean isCalculationActual() {
    return lastScenario != null && lastScenario.isInSync();
  }
}
