/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui.melding;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.service.InfoServiceAsync;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.util.development.DevModeUtil;
import nl.overheid.aerius.wui.main.widget.dialog.AeriusDialogBox;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class MeldingExportControllerImpl implements MeldingExportController {
  private final CalculatorUserContext userContext;
  private final CalculatorAppContext appContext;

  private final AeriusDialogBox dialog;
  private final InfoServiceAsync service;
  private final String applicationHostPlusName;

  private final EventBus eventBus;

  private final ScenarioBasePlace place;

  @Inject
  public MeldingExportControllerImpl(final Context context, final EventBus eventBus, final CalculatorUserContext userContext,
      final CalculatorAppContext appContext, final InfoServiceAsync service, final PlaceController placeController) {
    this.eventBus = eventBus;
    this.userContext = userContext;
    this.appContext = appContext;
    this.service = service;

    this.place = placeController.getWhere() instanceof ScenarioBasePlace ? (ScenarioBasePlace) placeController.getWhere() : null;

    dialog = new AeriusDialogBox();
    dialog.setHTML(M.messages().calculationOverviewMeldingTitle());

    applicationHostPlusName = (String) context.getSetting(SharedConstantsEnum.MELDING_URL);
  }

  @Override
  public void showPostMeldingDialog() {
    dialog.center();
    dialog.setWidget(new MeldingPreparationPanel());

    // Create CalculatedScenario with stripped calculation IDs
    final CalculatedScenario scenario = ScenarioUtil.toCalculatedScenario(userContext.getScenario(), place);
    //ensure year and temporary project information are set.
    scenario.setYear(userContext.getEmissionValueKey().getYear());
    scenario.setOptions(appContext.getCalculationOptions().copy());
    //ensure calculation points are cleared.
    scenario.getCalculationPoints().clear();

    service.getMeldingGML(scenario, new AsyncCallback<ScenarioGMLs>() {
      @Override
      public void onSuccess(final ScenarioGMLs result) {
        final MeldingForwardPanel panel = new MeldingForwardPanel(meldingURL(), result);

        panel.addSubmitHandler(new SubmitHandler() {
          @Override
          public void onSubmit(final SubmitEvent event) {
            if (!DevModeUtil.I.isDevMode()) {
              dialog.hide();
            }
          }
        });
        panel.addCloseHandler(new CloseHandler<Boolean>() {
          @Override
          public void onClose(final CloseEvent<Boolean> event) {
            dialog.hide();
          }
        });

        dialog.setWidget(panel);
      }

      /**
       * Creates a melding URL with a unique id parameter.
       * @return
       */
      private String meldingURL() {
        return applicationHostPlusName + "?locale=" + LocaleInfo.getCurrentLocale().getLocaleName()
            + '&' + SharedConstants.MELDING_UUID_PARAM + '=' + Math.abs(Random.nextInt()) + 'A' + Math.abs(Random.nextInt());
      }

      @Override
      public void onFailure(final Throwable caught) {
        notifyError(caught);
      }
    });
  }

  private void notifyError(final Throwable caught) {
    dialog.hide();

    NotificationUtil.broadcastError(eventBus, M.messages().calculationOverviewMeldingPrepError());
  }
}
