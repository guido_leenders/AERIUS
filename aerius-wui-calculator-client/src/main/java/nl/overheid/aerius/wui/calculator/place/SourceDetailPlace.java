/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.place;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcePlace;

/**
 * Place for source detail place.
 */
public class SourceDetailPlace extends SourcePlace {
  private static final String PREFIX = "source";

  /**
   * Tokenizer for {@link SourceDetailPlace}.
   */
  @Prefix(PREFIX)
  public static class Tokenizer extends SourcePlace.Tokenizer<SourceDetailPlace> implements PlaceTokenizer<SourceDetailPlace> {

    @Override
    protected SourceDetailPlace createPlace() {
      return new SourceDetailPlace();
    }
  }

  public SourceDetailPlace(final int sourceId, final ScenarioBasePlace currentPlace) {
    super(sourceId, currentPlace);
  }

  public SourceDetailPlace(final SourceDetailPlace currentPlace) {
    super(currentPlace);
  }

  SourceDetailPlace() {
    super(0, null);
  }

  @Override
  public SourcePlace copy() {
    return copyTo(new SourceDetailPlace(this));
  }
}
