/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator;

import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;
import nl.overheid.aerius.wui.calculator.CalculatorActivityMapper.CalculatorActivityFactory;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.calculator.i18n.HelpToolTipCalculator;
import nl.overheid.aerius.wui.calculator.importer.CalculatorImportController;
import nl.overheid.aerius.wui.calculator.place.CalculatorPlaceHistoryMapper;
import nl.overheid.aerius.wui.calculator.processor.CalculatorResultProcessor;
import nl.overheid.aerius.wui.calculator.search.CalculatorMapSearchAction;
import nl.overheid.aerius.wui.calculator.ui.CalculationOptionsPanel;
import nl.overheid.aerius.wui.calculator.ui.CalculationOptionsView;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointView;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointViewImpl;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointsView;
import nl.overheid.aerius.wui.calculator.ui.CalculationPointsViewImpl;
import nl.overheid.aerius.wui.calculator.ui.CalculatorTaskBarImpl;
import nl.overheid.aerius.wui.calculator.ui.CalculatorViewImpl;
import nl.overheid.aerius.wui.calculator.ui.MeldingView;
import nl.overheid.aerius.wui.calculator.ui.MeldingViewImpl;
import nl.overheid.aerius.wui.calculator.ui.ResultOverviewView;
import nl.overheid.aerius.wui.calculator.ui.ResultOverviewViewImpl;
import nl.overheid.aerius.wui.calculator.ui.SourceDetailView;
import nl.overheid.aerius.wui.calculator.ui.SourceDetailViewImpl;
import nl.overheid.aerius.wui.calculator.ui.SourceLocationView;
import nl.overheid.aerius.wui.calculator.ui.SourceLocationViewImpl;
import nl.overheid.aerius.wui.calculator.ui.StartUpView;
import nl.overheid.aerius.wui.calculator.ui.StartUpViewImpl;
import nl.overheid.aerius.wui.calculator.ui.melding.MeldingExportController;
import nl.overheid.aerius.wui.calculator.ui.melding.MeldingExportControllerImpl;
import nl.overheid.aerius.wui.calculator.util.development.CalculatorDevPanel;
import nl.overheid.aerius.wui.calculator.util.development.SuperNitroTurboCalculatorLogger;
import nl.overheid.aerius.wui.calculator.util.development.SuperNitroTurboCalculatorLogger.SuperNitroTurboCalculatorJuggernaut;
import nl.overheid.aerius.wui.geo.LegendTitleFactory;
import nl.overheid.aerius.wui.geo.search.MapSearchAction;
import nl.overheid.aerius.wui.main.AeriusContextProvider;
import nl.overheid.aerius.wui.main.ApplicationInitializer;
import nl.overheid.aerius.wui.main.RootPanelFactory;
import nl.overheid.aerius.wui.main.RootPanelFactory.RootLayoutPanelFactoryImpl;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.place.DefaultPlace;
import nl.overheid.aerius.wui.main.ui.ApplicationDisplay;
import nl.overheid.aerius.wui.main.ui.ApplicationRootMenuViewImpl;
import nl.overheid.aerius.wui.main.ui.ApplicationRootView;
import nl.overheid.aerius.wui.main.ui.menu.MenuItemFactory;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger.SuperNitroTurboJuggernaut;
import nl.overheid.aerius.wui.main.util.scaling.LogarithmicScalingUtil;
import nl.overheid.aerius.wui.main.util.scaling.ScalingUtil;
import nl.overheid.aerius.wui.main.widget.NotificationButton;
import nl.overheid.aerius.wui.main.widget.NotificationButtonCircular;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseClientModule;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.CalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.geo.CalculatorLegendTitleFactory;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.geo.WmsCalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.menu.ScenarioBaseMenuItemFactory;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationController;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationControllerImpl;
import nl.overheid.aerius.wui.scenario.base.ui.CalculatorButtonGroup;
import nl.overheid.aerius.wui.scenario.base.ui.CalculatorButtonGroupImpl;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesView;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.NetworkDetailView;
import nl.overheid.aerius.wui.scenario.base.ui.NetworkDetailViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultFilterView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultFilterViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultGraphicsViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultTableView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultTableViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ResultView;
import nl.overheid.aerius.wui.scenario.base.ui.ResultViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseTaskBar;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseView;
import nl.overheid.aerius.wui.scenario.base.ui.SituationView;
import nl.overheid.aerius.wui.scenario.base.ui.SituationViewImpl;
import nl.overheid.aerius.wui.scenario.base.ui.overview.EditableSourcesPanel;
import nl.overheid.aerius.wui.scenario.base.ui.overview.SourcesPanel;

/**
 * Gin bindings for the Calculator application.
 */
class CalculatorClientModule extends ScenarioBaseClientModule {
  @Override
  protected void configure() {
    bind(PlaceController.class).to(AeriusPlaceController.class).in(Singleton.class);
    bind(ActivityMapper.class).to(CalculatorActivityMapper.class).in(Singleton.class);
    bind(Place.class).annotatedWith(DefaultPlace.class).to(StartUpPlace.class).in(Singleton.class);
    bind(PlaceHistoryMapper.class).to(CalculatorPlaceHistoryMapper.class).in(Singleton.class);
    bind(new TypeLiteral<AeriusContextProvider<?, ?>>() {}).to(CalculatorContextProvider.class);
    bind(new TypeLiteral<ScenarioBaseAppContext<?, ?>>() {}).to(CalculatorAppContext.class);
    bind(new TypeLiteral<AppContext<?, ?>>() {}).to(CalculatorAppContext.class);
    bind(UserContext.class).to(CalculatorUserContext.class);
    bind(Context.class).to(CalculatorContext.class);
    bind(ScalingUtil.class).to(LogarithmicScalingUtil.class).in(Singleton.class);
    bind(HelpToolTipInfo.class).to(HelpToolTipCalculator.class);
    bind(CalculationController.class).to(CalculationControllerImpl.class);
    bind(MeldingExportController.class).to(MeldingExportControllerImpl.class);
    bind(MenuItemFactory.class).to(ScenarioBaseMenuItemFactory.class);
    bind(LegendTitleFactory.class).to(CalculatorLegendTitleFactory.class).in(Singleton.class);
    bind(ApplicationRootView.class).to(ApplicationRootMenuViewImpl.class).in(Singleton.class);
    bind(RootPanelFactory.class).to(RootLayoutPanelFactoryImpl.class);
    bind(ApplicationInitializer.class).to(CalculatorApplicationInitializer.class).in(Singleton.class);
    bind(ScenarioBaseImportController.class).to(CalculatorImportController.class);
    bind(CalculatorButtonGroup.class).to(CalculatorButtonGroupImpl.class);
    bind(ScenarioBaseCalculationProcessor.class).to(CalculatorResultProcessor.class);
    bind(CalculationResultLayerWrapper.class).to(WmsCalculationResultLayerWrapper.class);

    // Application specific
    bind(NotificationButton.class).to(NotificationButtonCircular.class);
    bind(MapSearchAction.class).to(CalculatorMapSearchAction.class);
    bind(SourcesPanel.class).to(EditableSourcesPanel.class);

    // View instances.
    bind(ScenarioBaseView.class).to(CalculatorViewImpl.class);
    bind(ApplicationDisplay.class).to(CalculatorViewImpl.class);
    bind(MapLayoutPanel.class).to(ScenarioBaseMapLayoutPanel.class);
    bind(ScenarioBaseTaskBar.class).to(CalculatorTaskBarImpl.class);

    bind(SituationView.class).to(SituationViewImpl.class);
    bind(StartUpView.class).to(StartUpViewImpl.class);
    bind(SourceLocationView.class).to(SourceLocationViewImpl.class);
    bind(SourceDetailView.class).to(SourceDetailViewImpl.class);
    bind(NetworkDetailView.class).to(NetworkDetailViewImpl.class);
    bind(CalculationPointsView.class).to(CalculationPointsViewImpl.class);
    bind(EmissionSourcesView.class).to(EmissionSourcesViewImpl.class);
    bind(CalculationPointView.class).to(CalculationPointViewImpl.class);
    bind(ResultView.class).to(ResultViewImpl.class);
    bind(ResultOverviewView.class).to(ResultOverviewViewImpl.class);
    bind(ResultGraphicsView.class).to(ResultGraphicsViewImpl.class);
    bind(ResultTableView.class).to(ResultTableViewImpl.class);
    bind(ResultFilterView.class).to(ResultFilterViewImpl.class);
    bind(CalculationOptionsView.class).to(CalculationOptionsPanel.class);
    bind(MeldingView.class).to(MeldingViewImpl.class);

    bind(SuperNitroTurboLogger.class).to(SuperNitroTurboCalculatorLogger.class);
    bind(SuperNitroTurboJuggernaut.class).to(SuperNitroTurboCalculatorJuggernaut.class);
    bind(EmbeddedDevPanel.class).to(CalculatorDevPanel.class);

    // Place to Activity assisted injection
    install(new GinFactoryModuleBuilder().build(CalculatorActivityFactory.class));
  }

  @Provides
  public CalculatorContext getCalculatorContext(final CalculatorContextProvider provider) {
    return provider.getContext();
  }

  @Provides
  public CalculatorUserContext getCalculatorUserContext(final CalculatorContextProvider provider) {
    return provider.getUserContext();
  }
}
