/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.place;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

import nl.overheid.aerius.wui.scenario.base.place.NetworkDetailPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultFilterPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultGraphicsPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultOverviewPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultTablePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;

/**
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a
 * separate TokenizerFactory.
 */
@WithTokenizers({ SourcesPlace.Tokenizer.class, CalculationPointPlace.Tokenizer.class, CalculationPointsPlace.Tokenizer.class,
  SourceDetailPlace.Tokenizer.class, NetworkDetailPlace.Tokenizer.class, SourceLocationPlace.Tokenizer.class, StartUpPlace.Tokenizer.class,
  ResultOverviewPlace.Tokenizer.class, ResultTablePlace.Tokenizer.class, ResultGraphicsPlace.Tokenizer.class, ResultFilterPlace.Tokenizer.class,
  MeldingPlace.Tokenizer.class })
public interface CalculatorPlaceHistoryMapper extends PlaceHistoryMapper {
  // empty interface, used by GWT to generate code.
}
