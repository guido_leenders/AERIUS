/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

@Singleton
public class CalculationPointViewImpl extends Composite implements CalculationPointView {

  private static CalculationPointViewImplUiBinder uiBinder = GWT.create(CalculationPointViewImplUiBinder.class);

  interface CalculationPointViewImplUiBinder extends UiBinder<Widget, CalculationPointViewImpl> {
  }

  @UiField Button buttonNextStep;
  @UiField Button buttonDetermine;
  @UiField TextBox label;
  @UiField IntValueBox xCoordinate;
  @UiField IntValueBox yCoordinate;
  @UiField FlowPanel determinePart;

  private Presenter presenter;

  @Inject
  public CalculationPointViewImpl(final HelpPopupController hpC) {
    initWidget(uiBinder.createAndBindUi(this));

    StyleUtil.I.setPlaceHolder(xCoordinate, M.messages().xCoordinate());
    StyleUtil.I.setPlaceHolder(yCoordinate, M.messages().yCoordinate());

    hpC.addWidget(label, hpC.tt().ttCalculationPointLabel());
    hpC.addWidget(xCoordinate, hpC.tt().ttCalculationPointXCoordinate());
    hpC.addWidget(yCoordinate, hpC.tt().ttCalculationPointYCoordinate());
    hpC.addWidget(buttonNextStep, hpC.tt().ttCalculationPointButtonNextStep());
    hpC.addWidget(buttonDetermine, hpC.tt().ttCalculationPointDetermine());

    label.ensureDebugId(TestID.INPUT_LABEL);
    xCoordinate.ensureDebugId(TestID.INPUT_X_COORDINATE);
    yCoordinate.ensureDebugId(TestID.INPUT_Y_COORDINATE);
    buttonNextStep.ensureDebugId(TestID.BUTTON_NEXTSTEP);
    buttonDetermine.ensureDebugId(TestID.BUTTON_DETERMINE_CALCPOINTS);
  }

  @Override
  public String getName() {
    return label.getText();
  }

  @Override
  public void setXY(final int x, final int y) {
    xCoordinate.setValue(x == 0 ? null : x);
    yCoordinate.setValue(y == 0 ? null : y);
  }

  @Override
  public void setName(final String name) {
    label.setText(name);
  }

  @UiHandler({ "xCoordinate", "yCoordinate" })
  void onKeyUp(final KeyUpEvent event) {
    if (xCoordinate.getValue() != null && yCoordinate.getValue() != null) {
      presenter.updatePosition(xCoordinate.getValue(), yCoordinate.getValue());
    } else {
      setEnabledNextStep(false);
    }
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setEnabledNextStep(final boolean nextStep) {
    buttonNextStep.setEnabled(nextStep);
  }

  @UiHandler("buttonNextStep")
  void onClickButtonNextStep(final ClickEvent e) {
    presenter.finish();
  }

  @UiHandler("buttonDetermine")
  void onClickButtonDetermine(final ClickEvent e) {
    presenter.showDetermineCalculationPointsDialog();
  }

  @Override
  public void showDetermineFunctionality(final boolean show) {
    determinePart.setVisible(show);
  }

  @Override
  public String getTitleText() {
    return M.messages().createCalculationPoint();
  }
}
