/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;

public class MeldingViewImpl extends Composite implements MeldingView {
  interface MeldingViewImplUiBinder extends UiBinder<Widget, MeldingViewImpl> {}

  private static final MeldingViewImplUiBinder UI_BINDER = GWT.create(MeldingViewImplUiBinder.class);

  @UiField HTML calculationViewText;
  @UiField SwitchPanel switchPanel;

  @UiField Button meldingButton;
  @UiField Button exportButton;

  private Presenter presenter;

  @Inject
  public MeldingViewImpl(final CalculatorContext context) {
    initWidget(UI_BINDER.createAndBindUi(this));
    calculationViewText.setHTML(M.messages().calculationViewText(
        (String) context.getSetting(SharedConstantsEnum.PAS_PERMIT_EXPLANATION_URL),
        (String) context.getSetting(SharedConstantsEnum.PAS_PP_LIST_URL)
    ));

    meldingButton.ensureDebugId(TestID.BUTTON_MELDING);
    exportButton.ensureDebugId(TestID.BUTTON_MELDING_EXPORT);
  }

  @Override
  public void setCalculationComplete(final boolean complete) {
    switchPanel.showWidget(complete ? 1 : 0);
  }

  @UiHandler("meldingButton")
  public void onMeldingButtonClick(final ClickEvent e) {
    presenter.postMelding();
  }

  @UiHandler("exportButton")
  public void onExportButtonClick(final ClickEvent e) {
    presenter.doExport();
  }

  @Override
  public String getTitleText() {
    return M.messages().meldingMenuTitle();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }
}
