/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.wui.main.widget.AttachedPopupContent;

/**
 * View for Calculation Options View.
 */
public interface CalculationOptionsView extends IsWidget, Editor<CalculationSetOptions>, AttachedPopupContent {

  interface Presenter {
    /**
     * Returns the CalculationOptionsPanel.
     * @return
     */
    CalculationOptionsView getCalculationOptions();

    /**
     * Saves the calculation options, and returns true if there were no errors. If triggerErrors is false no check on errors is done.
     * @param triggerErrors if true check on errors, and returns false if errors found.
     * @return false if errors found else true
     */
    boolean saveOptions(boolean triggerErrors);
  }

  void setPresenter(Presenter presenter);

}
