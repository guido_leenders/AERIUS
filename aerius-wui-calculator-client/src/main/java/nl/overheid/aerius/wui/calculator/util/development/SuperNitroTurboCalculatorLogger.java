/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.util.development;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.util.development.SuperNitroTurboGeoLogger;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class SuperNitroTurboCalculatorLogger extends SuperNitroTurboGeoLogger {
  interface SuperTurboCalculatorBinder extends EventBinder<SuperNitroTurboCalculatorJuggernaut> {}

  public static class SuperNitroTurboCalculatorJuggernaut extends SuperNitroTurboGeoJuggernaut {

    @EventHandler
    public void onCalculationStart(final CalculationStartEvent e) {
      logger.log("Calculation started.");
    }

    @EventHandler
    public void onCalculationInit(final CalculationInitEvent e) {
      logger.verbose("Init result: " + e.getInitResult());
      logger.verbose("Options: " + e.getInitResult());
    }

    @Override
    @EventHandler
    public void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent event) {
      logger.log("Emission result key changed: " + event.getValue());
    }

    @EventHandler
    public void onCalculationSummaryChangedEvent(final CalculationSummaryChangedEvent e) {
      logger.log("Calculation summary changed.");
    }

    @EventHandler
    public void onCalculationFinish(final CalculationFinishEvent e) {
      logger.log("Calculation finished.");
    }

    @EventHandler
    public void onCalculationCancel(final CalculationCancelEvent e) {
      //      logger.logImportant("Calculation cancelled. By user: " + e.isUserInitiated());
    }

    @EventHandler
    public void onPlaceChangeChange(final PlaceChangeEvent e) {
      logger.log("Place changed (" + parseSimpleClassName(e.getValue().getClass()) + "): " + new ScenarioBasePlace.Tokenizer<ScenarioBasePlace>() {
        @Override
        protected ScenarioBasePlace createPlace() {
          return null;
        }
      }
      .getToken((ScenarioBasePlace) e.getValue()));
    }
  }


  @Inject
  public SuperNitroTurboCalculatorLogger(final EventBus eventBus, final SuperNitroTurboCalculatorJuggernaut juggernaut) {
    super(eventBus, juggernaut);
    final SuperTurboCalculatorBinder playground = GWT.create(SuperTurboCalculatorBinder.class);
    playground.bindEventHandlers(juggernaut, eventBus);
    log("SuperNitroTurboCalculatorJuggernaut attached.");
  }
}
