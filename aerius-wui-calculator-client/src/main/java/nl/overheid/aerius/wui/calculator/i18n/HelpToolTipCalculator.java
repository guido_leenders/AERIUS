/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.i18n;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.i18n.HelpManualUrl;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;

/**
 * HelpTool tips for Calculator with id's to manual.
 */
public interface HelpToolTipCalculator extends HelpToolTipInfo {
  int BUTTON_INFORMATION = 711;
  int BUTTON_SUBSTANCES = 745;
  int BUTTON_YEARS = 733;
  int BUTTON_ADD_SOURCE = 967;
  int MOBILE_NAME = 1272;
  int MOBILE_CUSTOMSTAGE = 1273;
  int MOBILE_CALCULATOR = 1422;
  int MOBILE_CALCULATOR_POPUP = 1424;

  int BUTTON_OPTIONS = 1573;
  int CALCULATE = 760;
  int CALCULATION_OPTIONS = 743;
  int CALCULATION_OPTIONS_PERMIT_RADIUS_TYPE = 3780;
  int CALCULATION_OPTIONS_USER_DEFINED = 694;
  int CALCULATION_OPTIONS_EDIT_SOURCE = 659;
  int CALCULATION_POINT = 693;
  int CREATE_VARIANT = 742;
  int CREATE_VARIANT_SITUATION_TWO = 683;
  int CREATE_VARIANT_COMPARE = 686;
  int CREATE_VARIANT_COMPARE_DEPOSITION = 687;
  int CONFIGURE_SOURCE = 654;
  int CONFIGURE_SUBSOURCE = 658;
  int CONFIGURE_SOURCE_NEXTSTEP = 748;
  int CONFIGURE_SOURCE_CHARACTERISTICS = 657;
  int CONFIGURE_GENERIC_EMISSIONS = 748;
  int EXPORT = 764;
  int HELP_INFO = 628;
  int INFO_HABITATTYPE = 634;
  int LAYER_PANEL = 636;
  int LINE_SOURCE = 670;
  int POLYGON_SOURCE = 1480;
  int LODGING = 753;
  int NOTIFICATION_CENTER = 740;
  int PLACE_SOURCE = 734;
  int SEARCH_BOX = 713;
  int SEARCH_NATURE_AREA_RESULTS = 668;
  int NAVIGATION_CONTROLE = 638;
  int STARTUP_PAGE = 780;
  int STARTUP_PAGE_MANUAL = 662;
  int STARTUP_PAGE_IMPORT = 677;
  int ROAD = 1264;
  int SHIP = 1262;
  int SHIP_INLAND_MOORING = 1429;
  int SHIP_INLAND_ROUTE = 1261;
  int SHIP_MARITIME_ROUTE = 1263;
  int CALCULATOR_HEAT_CONTENT = 657;
  int BUTTON_NEW_SITUATION = 682;
  int MAIN_MENU = 1291;
  int RESULT_GRAPHICS_TAB = 763;
  int RESULT_TABLE_TAB = 1280;
  int RESULT_FILTER_TAB = 1281;
  int RESULT_TYPE_DROPDOWN = 1280;
  int PLAN = 1267;
  int PLAN_NAME = 1268;
  int PLAN_CATEGORY = 1269;
  int PLAN_EDIT = 1270;
  int PLAN_EDIT_ADD = 1271;
  int CONFIGURE_SUBSOURCE_SHIPPING = 1575;
  int CONFIGURE_SUBSOURCE_SHIPPING_INLAND = 1576;
  int CONFIGURE_SUBSOURCE_SHIPPING_INLAND_DOCKED = 1429;
  int CONFIGURE_SUBSOURCE_SHIPPING_MARITIME_INLAND = 1263;
  int CONFIGURE_SUBSOURCE_SHIPPING_MARITIME_DOCKED = 1262;
  int CONFIGURE_SUBSOURCE_SHIPPING_MARITIME_MARITIME = 1574;
  int SHIPPING_WATERWAY_EDITOR = 3779;

  int CONFIGURE_SUBSOURCE_ROAD_TRANSPORTATION = 658;
  int CONFIGURE_SUBSOURCE_AVIANTION = 658;
  int CONFIGURE_SUBSOURCE_RAIL_TRANSPORTATION = 658;
  int CONFIGURE_SUBSOURCE_MOBILE_EQUIPMENT = 658;
  int CONFIGURE_SUBSOURCE_SECTOR_INDUSTRY = 658;
  int CONFIGURE_SUBSOURCE_LIVE_AND_WORK = 658;
  int CONFIGURE_SUBSOURCE_FARM = 753;
  int LAYER_PANEL_SLIDER = 637;
  int LAYER_PANEL_HABITAT_TYPE = 1275;
  int CALCULATION_POINT_LABEL = 696;
  int CALCULATION_POINT_POSITION = 695;
  int CALCULATION_POINT_GENERATE = 1277;
  int CALCULATION_POINT_EDIT = 697;
  int CALCULATION_POINT_IMPORT = 1279;
  int EXPORT_EMAIL = 700;
  int EXPORT_GENERAL = 699;
  int CALCULATION_POINT_OVERVIEW = 1278;
  int CALCULATION_SETTINGS_OVERVIEW = 660;



  @Override
  @HelpManualUrl(MAIN_MENU)
  HelpInfo ttCalculationMainMenu();

  @Override
  @HelpManualUrl(CALCULATION_POINT_LABEL)
  HelpInfo ttCalculationPointLabel();

  @Override
  @HelpManualUrl(CALCULATION_SETTINGS_OVERVIEW)
  HelpInfo ttOverviewCalculation();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS_PERMIT_RADIUS_TYPE)
  HelpInfo ttOverviewCalculationPermitRadiusType();

  @Override
  @HelpManualUrl(CALCULATION_POINT_EDIT)
  HelpInfo ttOverviewCalculationPointsButtonEdit();

  @Override
  @HelpManualUrl(CALCULATION_POINT_EDIT)
  HelpInfo ttOverviewCalculationPointsButtonDelete();

  @Override
  @HelpManualUrl(CALCULATION_POINT_EDIT)
  HelpInfo ttOverviewCalculationPointsButtonCopy();

  @Override
  @HelpManualUrl(CALCULATION_POINT_IMPORT)
  HelpInfo ttOverviewButtonImportCalculationPoint();

  @Override
  @HelpManualUrl(EXPORT_EMAIL)
  HelpInfo ttExportEmail();

  @Override
  @HelpManualUrl(EXPORT_GENERAL)
  HelpInfo ttExport();

  @Override
  @HelpManualUrl(CALCULATION_POINT_POSITION)
  HelpInfo ttCalculationPointXCoordinate();

  @Override
  @HelpManualUrl(CALCULATION_POINT_OVERVIEW)
  HelpInfo ttCalculationPointOverview();

  @Override
  @HelpManualUrl(CALCULATION_POINT_GENERATE)
  HelpInfo ttCalculationPointDetermine();

  @Override
  @HelpManualUrl(MAIN_MENU)
  HelpInfo ttCalculationPointYCoordinate();

  @Override
  @HelpManualUrl(RESULT_GRAPHICS_TAB)
  HelpInfo ttResultGraphicsTab();

  @Override
  @HelpManualUrl(RESULT_TABLE_TAB)
  HelpInfo ttResultTableTab();

  @Override
  @HelpManualUrl(RESULT_FILTER_TAB)
  HelpInfo ttResultFilterTab();

  @Override
  @HelpManualUrl(RESULT_TYPE_DROPDOWN)
  HelpInfo ttResultTypeDropDown();

  @Override
  @HelpManualUrl(BUTTON_OPTIONS)
  HelpInfo ttOptionsPanel();

  @Override
  @HelpManualUrl(BUTTON_NEW_SITUATION)
  HelpInfo ttSituationNew();

  @Override
  @HelpManualUrl(BUTTON_INFORMATION)
  HelpInfo ttInfoPanel();

  @Override
  @HelpManualUrl(BUTTON_SUBSTANCES)
  HelpInfo ttSubstanceSelection();

  @Override
  @HelpManualUrl(BUTTON_YEARS)
  HelpInfo ttYearSelection();

  @Override
  @HelpManualUrl(BUTTON_ADD_SOURCE)
  HelpInfo ttOverviewButtonAddSource();

  @Override
  @HelpManualUrl(MOBILE_CALCULATOR_POPUP)
  HelpInfo ttMobileEmissionCalculator();

  @Override
  @HelpManualUrl(MOBILE_NAME)
  HelpInfo ttMobileSourceName();

  @Override
  @HelpManualUrl(MOBILE_NAME)
  HelpInfo ttMobileSourceStageCategory();

  @Override
  @HelpManualUrl(MOBILE_CUSTOMSTAGE)
  HelpInfo ttMobileSourceCustomCategory();

  @Override
  @HelpManualUrl(MOBILE_NAME)
  HelpInfo ttMobileSourceCategory();

  @Override
  @HelpManualUrl(MOBILE_NAME)
  HelpInfo ttMobileSourceFuelage();

  @Override
  @HelpManualUrl(MOBILE_CUSTOMSTAGE)
  HelpInfo ttMobileOPSHeight();

  @Override
  @HelpManualUrl(MOBILE_CUSTOMSTAGE)
  HelpInfo ttMobileOPSSpread();

  @Override
  @HelpManualUrl(MOBILE_CUSTOMSTAGE)
  HelpInfo ttMobileOPSHeatContent();

  @Override
  @HelpManualUrl(MOBILE_CUSTOMSTAGE)
  HelpInfo ttMobileGenericEmissionNOX();

  @Override
  @HelpManualUrl(CALCULATOR_HEAT_CONTENT)
  HelpInfo ttOPSHeight();

  @Override
  @HelpManualUrl(CALCULATOR_HEAT_CONTENT)
  HelpInfo ttOPSSpread();

  @Override
  @HelpManualUrl(CALCULATOR_HEAT_CONTENT)
  HelpInfo ttOPSHeatContent();

  @Override
  @HelpManualUrl(CONFIGURE_GENERIC_EMISSIONS)
  HelpInfo ttGenericEmissionNOX();

  @Override
  @HelpManualUrl(MOBILE_NAME)
  HelpInfo ttMobileSourceSave();

  @Override
  @HelpManualUrl(MOBILE_CALCULATOR)
  HelpInfo ttMobileShowCalculator();

  @Override
  @HelpManualUrl(CALCULATE)
  HelpInfo ttOverviewButtonCalculate();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS)
  HelpInfo ttOverviewCalculationMaxRange();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS_USER_DEFINED)
  HelpInfo ttOverviewCalculationMethodUserDefined();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS)
  HelpInfo ttOverviewCalculationThreshold();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS)
  HelpInfo ttOverviewCalculationAssessmentBoundary();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS)
  HelpInfo ttOverviewCalculationMethod();

  @Override
  @HelpManualUrl(CALCULATION_OPTIONS_EDIT_SOURCE)
  HelpInfo ttOverviewEditSource();

  @Override
  @HelpManualUrl(CALCULATION_POINT)
  HelpInfo ttOverviewButtonAddCalculationPoint();

  @Override
  @HelpManualUrl(CREATE_VARIANT)
  HelpInfo ttSituationOne();

  @Override
  @HelpManualUrl(CREATE_VARIANT_SITUATION_TWO)
  HelpInfo ttSituationTwo();

  @Override
  @HelpManualUrl(CREATE_VARIANT_COMPARE)
  HelpInfo ttSituationComparison();

  @Override
  @HelpManualUrl(CREATE_VARIANT_COMPARE_DEPOSITION)
  HelpInfo ttSituationComparisonDeposition();

  @Override
  @HelpManualUrl(CALCULATOR_HEAT_CONTENT)
  HelpInfo ttCalculateHeatContent();

  @Override
  @HelpManualUrl(CONFIGURE_SOURCE_NEXTSTEP)
  HelpInfo ttSourceDetailButtonNextStep();

  @Override
  @HelpManualUrl(CONFIGURE_SOURCE)
  HelpInfo ttSourceDetailButtonEditLocation();

  @Override
  @HelpManualUrl(CONFIGURE_SOURCE)
  HelpInfo ttSourceDetailLabel();

  @Override
  @HelpManualUrl(CONFIGURE_SOURCE)
  HelpInfo ttSourceDetailSectorGroup();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE)
  HelpInfo ttSourceDetailSector();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_FARM)
  HelpInfo ttSourceDetailSectorAgriculture();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_LIVE_AND_WORK)

  HelpInfo ttSourceDetailSectorLiveAndWork();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SECTOR_INDUSTRY)
  HelpInfo ttSourceDetailSectorIndustry();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_MOBILE_EQUIPMENT)
  HelpInfo ttSourceDetailSectorMobileEquipment();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_RAIL_TRANSPORTATION)
  HelpInfo ttSourceDetailSectorRailTransportation();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_AVIANTION)
  HelpInfo ttSourceDetailSectorAviation();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_ROAD_TRANSPORTATION)
  HelpInfo ttSourceDetailSectorRoadTransportation();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SHIPPING)
  HelpInfo ttSourceDetailSectorShipping();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SHIPPING_INLAND)
  HelpInfo ttSourceDetailSectorShippingInland();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SHIPPING_INLAND_DOCKED)
  HelpInfo ttSourceDetailSectorShippingInlandDocked();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SHIPPING_MARITIME_DOCKED)
  HelpInfo ttSourceDetailSectorShippingMaritimeDocked();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SHIPPING_MARITIME_INLAND)
  HelpInfo ttSourceDetailSectorShippingMaritimeInland();

  @Override
  @HelpManualUrl(CONFIGURE_SUBSOURCE_SHIPPING_MARITIME_MARITIME)
  HelpInfo ttSourceDetailSectorShippingMaritimeMaritime();

  @Override
  @HelpManualUrl(SHIPPING_WATERWAY_EDITOR)
  HelpInfo ttWaterwayEditor();

  @Override
  @HelpManualUrl(CONFIGURE_SOURCE_CHARACTERISTICS)
  HelpInfo ttSourceCharacteristics();

  @Override
  @HelpManualUrl(CONFIGURE_GENERIC_EMISSIONS)
  HelpInfo ttGenericEmissions();

  @Override
  @HelpManualUrl(EXPORT)
  HelpInfo ttOverviewButtonExport();

  @Override
  @HelpManualUrl(HELP_INFO)
  HelpInfo ttHelpButton();

  @Override
  @HelpManualUrl(LAYER_PANEL)
  HelpInfo ttLayerPanel();

  @Override
  @HelpManualUrl(LAYER_PANEL_SLIDER)
  HelpInfo ttLayerPanelSlider();

  @Override
  @HelpManualUrl(LAYER_PANEL_HABITAT_TYPE)
  HelpInfo ttLayerPanelHabitatType();

  @Override
  @HelpManualUrl(LINE_SOURCE)
  HelpInfo ttLocationButtonLine();

  @Override
  @HelpManualUrl(POLYGON_SOURCE)
  HelpInfo ttLocationButtonPolygon();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingCustomLodgeDescription();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingCustomLodgeEmissionFactor();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingCategory();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingAdditionalCategory();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingReductiveCategory();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingFodderMeasure();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingSystemDefinition();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingAmount();

  @Override
  @HelpManualUrl(LODGING)
  HelpInfo ttFarmLodgingConfirmRow();

  @Override
  @HelpManualUrl(NOTIFICATION_CENTER)
  HelpInfo ttNotificationCenter();

  @Override
  @HelpManualUrl(PLACE_SOURCE)
  HelpInfo ttLocationButtonPoint();

  @Override
  @HelpManualUrl(PLACE_SOURCE)
  HelpInfo ttLocationButtonEditShape();

  @Override
  @HelpManualUrl(PLACE_SOURCE)
  HelpInfo ttLocationXCoordinate();

  @Override
  @HelpManualUrl(PLACE_SOURCE)
  HelpInfo ttLocationYCoordinate();

  @Override
  @HelpManualUrl(SEARCH_BOX)
  HelpInfo ttSearchBox();

  @Override
  @HelpManualUrl(SEARCH_NATURE_AREA_RESULTS)
  HelpInfo ttSearchNatureArea();

  @Override
  @HelpManualUrl(NAVIGATION_CONTROLE)
  HelpInfo ttNavigateControle();

  @Override
  @HelpManualUrl(INFO_HABITATTYPE)
  HelpInfo ttInfoPanelHabitatType();

  @Override
  @HelpManualUrl(STARTUP_PAGE)
  HelpInfo ttLocationButtonSource();

  @Override
  @HelpManualUrl(STARTUP_PAGE_MANUAL)
  HelpInfo ttLocationButtonManual();

  @Override
  @HelpManualUrl(STARTUP_PAGE_IMPORT)
  HelpInfo ttLocationButtonImport();

  @Override
  @HelpManualUrl(STARTUP_PAGE_IMPORT)
  HelpInfo ttImportFileField();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadEmissionsStagnation();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadEmissionsMaxSpeed();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadVehiclesPerTimeUnit();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadVehicleAdd();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadVehicleEdit();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadVehicleDelete();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadVehicleCopy();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadEmissionsEnforcement();

  @Override
  @HelpManualUrl(ROAD)
  HelpInfo ttRoadType();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShipName();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShipRoute();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShipAmount();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttInlandShipTimeUnitAtoB();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttInlandShipTimeUnitBtoA();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShippingTimeUnit();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShipType();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShipResidenceTime();

  @Override
  @HelpManualUrl(SHIP)
  HelpInfo ttShipSave();

  @Override
  @HelpManualUrl(SHIP_INLAND_MOORING)
  HelpInfo ttInlandMooringShipName();

  @Override
  @HelpManualUrl(SHIP_INLAND_MOORING)
  HelpInfo ttInlandMooringShipType();

  @Override
  @HelpManualUrl(SHIP_INLAND_MOORING)
  HelpInfo ttInlandMooringShipResidenceTime();

  @Override
  @HelpManualUrl(SHIP_INLAND_MOORING)
  HelpInfo ttInlandMooringShipSave();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipName();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipType();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipAmountAtoB();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipAmountBtoA();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipPercentageLadenAtoB();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipPercentageLadenBtoA();

  @Override
  @HelpManualUrl(SHIP_INLAND_ROUTE)
  HelpInfo ttInlandShipSave();


  @Override
  @HelpManualUrl(SHIP_MARITIME_ROUTE)
  HelpInfo ttMaritimeRouteShipName();

  @Override
  @HelpManualUrl(SHIP_MARITIME_ROUTE)
  HelpInfo ttMaritimeRouteShipType();

  @Override
  @HelpManualUrl(SHIP_MARITIME_ROUTE)
  HelpInfo ttMaritimeRouteShipMovements();

  @Override
  @HelpManualUrl(SHIP_MARITIME_ROUTE)
  HelpInfo ttMaritimeRouteShipSave();

  @Override
  @HelpManualUrl(SHIP_MARITIME_ROUTE)
  HelpInfo ttMaritimeRouteShipTimeUnit();

  @Override
  @HelpManualUrl(PLAN_NAME)
  HelpInfo ttPlanName();

  @Override
  @HelpManualUrl(PLAN_CATEGORY)
  HelpInfo ttPlanAmount();

  @Override
  @HelpManualUrl(PLAN)
  HelpInfo ttPlanCategory();

  @Override
  @HelpManualUrl(PLAN_CATEGORY)
  HelpInfo ttPlanCategoryList();

  @Override
  @HelpManualUrl(PLAN)
  HelpInfo ttPlanSubmitButton();

  @Override
  @HelpManualUrl(PLAN_EDIT)
  HelpInfo ttPlanEdit();

  @Override
  @HelpManualUrl(PLAN_EDIT)
  HelpInfo ttPlanCopy();

  @Override
  @HelpManualUrl(PLAN_EDIT)
  HelpInfo ttPlanDelete();

  @Override
  @HelpManualUrl(PLAN_EDIT_ADD)
  HelpInfo ttPlanAdd();

  @Override
  HelpInfo ttEcologicalQuality1a();

  @Override
  HelpInfo ttEcologicalQuality1b();

  @Override
  HelpInfo ttEcologicalQuality2();

  @Override
  HelpInfo ttEcologicalQualityNone();

}
