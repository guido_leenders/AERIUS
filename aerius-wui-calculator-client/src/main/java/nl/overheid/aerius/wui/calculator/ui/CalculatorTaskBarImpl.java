/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.calculator.context.CalculatorAppContext;
import nl.overheid.aerius.wui.geo.LayerPanel;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButton;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseTaskBarImpl;

/**
 * Implementation of the Calculator taskbar.
 */
@Singleton
public class CalculatorTaskBarImpl extends ScenarioBaseTaskBarImpl {

  interface CalculatorTaskBarImplUiBinder extends UiBinder<Widget, CalculatorTaskBarImpl> {}

  private static final CalculatorTaskBarImplUiBinder UI_BINDER = GWT.create(CalculatorTaskBarImplUiBinder.class);

  @UiField AttachedPopupButton optionsPanelButton;
  @UiField(provided = true) CalculationOptionsView optionsPanel;

  @Inject
  public CalculatorTaskBarImpl(final CalculatorAppContext appContext, final EventBus eventBus, final HelpPopupController hpC,
      final InfoPanelPresenter infoPanelPresenter, final LayerPanel layerPanel, final CalculationOptionsPresenter calculationOptionsPresenter) {
    super(appContext, eventBus, hpC, infoPanelPresenter, layerPanel);

    this.optionsPanel = calculationOptionsPresenter.getCalculationOptions();
    initWidget(UI_BINDER.createAndBindUi(this));

    hpC.addWidget(optionsPanelButton, hpC.tt().ttOptionsPanel());

    optionsPanelButton.ensureDebugId(TestID.BUTTON_OPTIONSPANEL);
  }
}
