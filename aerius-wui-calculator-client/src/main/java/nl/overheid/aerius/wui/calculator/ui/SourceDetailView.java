/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.ui;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceEditor;

/**
 * View for displaying and editing emission source details.
 */
public interface SourceDetailView extends IsWidget, Editor<EmissionSource>, HasTitle {

  /**
   * Presenter for the {@link SourceDetailView}.
   */
  interface Presenter extends Activity, ValueChangeHandler<Sector> {

    /**
     * Go to the edit location screen.
     */
    void editLocation();

    /**
     * Cancel the edit and go back.
     */
    void cancelEdit();

    /**
     * Save edits and go to the next step, which is the list of sources.
     */
    void nextStep();
  }

  /**
   * Set the presenter for the view.
   *
   * @param presenter Presenter to use
   */
  void setPresenter(Presenter presenter);

  /**
   * Set the initial ui based on the sector.
   * @param sector sector
   */
  void setSourceDetailVisibility(Sector sector);

  /**
   * Basic Emission Source info editor. Read only.
   * @return Basic Emission Source editor
   */
  Editor<EmissionSource> getBasicInfoEditor();

  /**
   * @return emission source editor
   */
  @Ignore
  EmissionSourceEditor getEmissionSourceEditor();

  /**
   * Indicates whether or not the next button should be visible.
   *
   * @param visible show true for yay, false for nay
   */
  void setNextButtonVisible(boolean visible);

  /**
   * Update the ui for the sector.
   * @param sector sector.
   */
  void updateDetailsVisiblity(Sector sector);
}
