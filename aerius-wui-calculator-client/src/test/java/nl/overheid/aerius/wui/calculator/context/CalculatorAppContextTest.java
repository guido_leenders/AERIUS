/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.calculator.context;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwtmockito.GwtMockitoTestRunner;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;

/**
 * Test class for {@link CalculatorAppContext}.
 */
@RunWith(GwtMockitoTestRunner.class)
public class CalculatorAppContextTest {

  @Test
  public void testInitEmissionSourceList() {
    final CalculatorUserContext userContext = new CalculatorUserContext();
    final CalculatorContext context = new CalculatorContext();
    context.setSetting(SharedConstantsEnum.CALCULATE_EMISSIONS_DEFAULT_RADIUS_DISTANCE_UI, 1);
    //dummy receptorGridSettings to get through the test.
    final ArrayList<HexagonZoomLevel> zoomLevels = new ArrayList<>();
    zoomLevels.add(new HexagonZoomLevel(1, 10000));
    context.setReceptorGridSettings(new ReceptorGridSettings(new BBox(), null, 1, zoomLevels));
    final CalculatorAppContext appContext = new CalculatorAppContext(null, null, context , userContext);
    appContext.initEmissionSourceList(0);
    assertEquals("One list should be added", 1, userContext.getSourceLists().size());
    appContext.initEmissionSourceList(3);
    assertEquals("Another list should be added", 2, userContext.getSourceLists().size());
    assertEquals("Id of list should be 3", 3, userContext.getSourceLists().get(1).getId());
    appContext.initEmissionSourceList(-1);
    assertEquals("Nothing should be added", 2, userContext.getSourceLists().size());
  }
}
