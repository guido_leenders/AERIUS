/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.geo;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.Icon;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.Size;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Image;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.Arrow;
import nl.overheid.aerius.wui.main.widget.Arrow.Orientation;

public class NoticeMarker {

  public enum NoticeMarkerType { NORMAL, HIGHLIGHT, SELECTED }

  private static final int SIZE = 38;
  private static final int BORDER_COMBINED_WIDTH = 8;
  private static final double CORRECTION = 5;

  private final Icon defaultIcon = new Icon("", new Size(0, 0));

  private final Marker marker;

  private final Arrow arrow;

  protected final DivElement content = Document.get().createDivElement();
  private final DivElement root;

  /**
   * Creates a marker at the location.
   *
   * @param p (Point)location to place the marker
   */
  public NoticeMarker(final Point p) {
    marker = new Marker(new LonLat(p.getX(), p.getY()), defaultIcon);
    final Element image = marker.getIcon().getJSObject().getPropertyAsDomElement("imageDiv");

    // Clear content
    image.setInnerText(null);

    root = Document.get().createDivElement();
    image.appendChild(root);

    root.getStyle().setPosition(Position.ABSOLUTE);

    content.setClassName(R.css().markerSite());
    arrow = new Arrow(Orientation.BOTTOM, 1);
    arrow.setColor(ColorUtil.black(), ColorUtil.black());
    arrow.getElement().getStyle().setZIndex(-1);

    root.appendChild(arrow.getElement());
    root.appendChild(content);
  }

  /**
   * Fill the marker with the given MarkerItems.
   *
   * @param items The items to fill the Marker with
   * @param marketType Whether to highlight this marker
   */
  public void setMarkerItems(final ArrayList<MarkerItem<?>> items, final NoticeMarkerType marketType) {
    if (items.isEmpty()) {
      return;
    }

    // Get the first item and use its icon/color
    final Notice markerItem = ((NoticeMarkerItem) items.get(0)).getObject();

    // Create and abuse the sector image
    final Image image = new Image(SectorImageUtil.getSectorImageResource(markerItem.getSectorIcon()));
    image.setStyleName(R.css().markerIcon());

    arrow.setSize(SIZE, MathUtil.round(SIZE * 0.5));
    arrow.getElement().getStyle().setLeft(-SIZE * 0.5, Unit.PX);

    // Insert it
    content.appendChild(image.getElement());

    switch (marketType) {
    case HIGHLIGHT:
      content.getStyle().setBackgroundColor(ColorUtil.orange());
      break;
    case SELECTED:
      content.getStyle().setBackgroundColor(ColorUtil.yellow());
      break;
    default:
      content.getStyle().setBackgroundColor(getMarkerColorOrDefault(markerItem));
    }

    content.getStyle().setWidth(SIZE, Unit.PX);
    content.getStyle().setPosition(Position.ABSOLUTE);
    content.getStyle().setLeft(-(SIZE + BORDER_COMBINED_WIDTH) * 0.5, Unit.PX);
    content.getStyle().setBottom(((-arrow.getHeight() + SIZE) - (image.getHeight() * 0.5)) + CORRECTION, Unit.PX);

    root.getStyle().setTop(-arrow.getHeight(), Unit.PX);
  }

  private String getMarkerColorOrDefault(final Notice markerItem) {
    // TODO;
    return ColorUtil.webColor("CCCCCC");
    //    return markerItem.getColor() == null ? ColorUtil.black() : ColorUtil.webColor(markerItem.getColor());
  }

  public Marker getMarker() {
    return marker;
  }
}
