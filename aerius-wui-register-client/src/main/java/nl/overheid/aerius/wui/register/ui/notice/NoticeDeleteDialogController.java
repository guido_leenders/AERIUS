/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.register.place.NoticePlace;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDeleteDialogPanel.NoticeDeleteDialogPanelDriver;

/**
 * Controller acknowlegde the delete Notice.
 */
@Singleton
public class NoticeDeleteDialogController implements NoticeDeleteDialogPanel.Presenter {

  private final SimpleBeanEditorDriver<NoticeDeleteData, NoticeDeleteDialogPanel> driver = GWT.create(NoticeDeleteDialogPanelDriver.class);
  private final ConfirmCancelDialog<NoticeDeleteData, NoticeDeleteDialogPanel> dialog;

  public interface DeleteHandler {
    void deleteNotice();
  }

  private final ConfirmHandler<NoticeDeleteData> confirmHandler = new ConfirmHandler<NoticeDeleteData>() {
    @Override
    public void onConfirm(final ConfirmEvent<NoticeDeleteData> event) {
      handler.deleteNotice();
      dialog.hide();
    }
  };

  private String reference;
  private final NoticeDeleteDialogPanel contentPanel;
  private DeleteHandler handler;

  @Inject
  public NoticeDeleteDialogController(final HelpPopupController hpC) {
    contentPanel = new NoticeDeleteDialogPanel(hpC);
    contentPanel.setPresenter(this);

    dialog = new ConfirmCancelDialog<NoticeDeleteData,
        NoticeDeleteDialogPanel>(driver, M.messages().registerNoticeDeleteLabelExecute(), M.messages().cancelButton());
    dialog.setText(M.messages().registerNoticeDeleteTitle());

    // TODO Internet Explorer, as expected, won't handle the animation correctly.
    dialog.setAnimationEnabled(false);
    dialog.addConfirmHandler(confirmHandler);
    dialog.setWidget(contentPanel);
    enableConfirmButton(false);
  }

  public void setDeleteHandler(final DeleteHandler handler) {
    this.handler = handler;
  }

  public void setReference(final String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  public void show(final NoticePlace place) {
    final NoticeDeleteData deleteData = new NoticeDeleteData();
    deleteData.setDossierId("");
    driver.initialize(contentPanel);
    driver.edit(deleteData);
    dialog.center();
    enableConfirmButton(false);
  }

  @Override
  public void enableConfirmButton(final boolean enable) {
    dialog.enableConfirmButton(enable);
  }

  @Override
  public void validateInput(final String input) {
    enableConfirmButton(reference.equals(input));
  }

}
