/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectGroupButton.PriorityProjectGroupButtonHandler;

public interface PriorityProjectBaseView extends IsWidget, AcceptsOneWidget, HasTitle {
  enum PriorityProjectViewType {
    DOSSIER, PROJECT, APPLICATION, ACTUALISATION;
  }

  interface Presenter extends AsyncCallback<PriorityProject>, PriorityProjectGroupButtonHandler {

    void switchView(PriorityProjectViewType value);

  }

  void setPresenter(Presenter presenter);

  void setViewType(PriorityProjectViewType viewType);

  void setPriorityProject(PriorityProject priorityProject);

  void showButtonGroup(boolean show);

  /**
   * Lock the interactive user interface elements until the operation finished.
   * @param lock if true lock
   */
  void setLocked(boolean lock);

  /**
   * Whether to enable the delete button.
   * @param enabled enable on true, disable on false
   */
  void enableDelete(boolean enable);

  /**
   * Whether to enable the export button.
   * @param enabled enable on true, disable on false
   */
  void enableExport(boolean enable);

  /**
   * Whether to enable the assign completed button.
   * @param enable enable on true, disable on false
   */
  void enableAssignComplete(boolean enable);
}
