/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.SecurityUtil;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButton;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButtonGroup;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.NotificationPanel;

/**
 *
 */
public class UserProfilePanel extends Composite implements Editor<UserProfile> {
  private static final UserProfilePanelUiBinder UI_BINDER = GWT.create(UserProfilePanelUiBinder.class);

  interface UserProfilePanelUiBinder extends UiBinder<Widget, UserProfilePanel> {}

  public interface UserProfileDriver extends SimpleBeanEditorDriver<UserProfile, UserProfilePanel> {}

  @UiField AttachedPopupButtonGroup buttonGroup;
  @Ignore @UiField InlineHTML name;
  @Path("") final LeafValueEditor<UserProfile> nameEditor = new FakeLeafValueEditor<UserProfile>() {
    @Override
    public void setValue(final UserProfile value) {
      name.setText(M.messages().registerUser(value.getLastName(), value.getFirstName()));
    }
  };

  @UiField AttachedPopupButton profilePanelButton;

  @UiField(provided = true) NotificationPanel notificationButton;
  @UiField Button logoutButton;

  @Path("") @UiField(provided = true) UserProfilePopup profilePopup;

  @Inject
  public UserProfilePanel(final UserProfilePopup profilePopup, final HelpPopupController hpC, final NotificationPanel notificationHandler) {
    this.profilePopup = profilePopup;
    this.notificationButton = notificationHandler;

    initWidget(UI_BINDER.createAndBindUi(this));

    notificationHandler.setAlignmentHandle(buttonGroup);

    hpC.addWidget(logoutButton, hpC.tt().ttRegisterLogoutButton());

    logoutButton.ensureDebugId(TestIDRegister.BUTTON_LOGOUT);
    profilePanelButton.ensureDebugId(TestIDRegister.BUTTON_PROFILE);
    notificationButton.ensureDebugId(TestIDRegister.BUTTON_NOTIFICATION);
  }

  @UiHandler("logoutButton")
  void onLogout(final ClickEvent event) {
    SecurityUtil.logout();
  }
}
