/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.retrievers.PollingAgentImpl;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierDetailPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierBaseView.PriorityProjectDossierDetailViewType;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public class PriorityProjectProjectDossierDetailActivity extends PriorityProjectProjectDossierBaseActivity<PriorityProjectProjectDossierDetailPlace>
implements PriorityProjectProjectDossierDetailView.Presenter {
  private class SubProjectExportPollingAgentImpl extends PollingAgentImpl<String, RequestExportResult> {
    @Override
    protected void callService(final String key, final AsyncCallback<RequestExportResult> resultCallback) {
      appContext.getRequestExportService().isExportReady(key, resultCallback);
    }
  }

  private final AppAsyncCallback<RequestExportResult> exportReadyCallback = new AppAsyncCallback<RequestExportResult>() {
    @Override
    public void onSuccess(final RequestExportResult result) {
      pollingAgent.stop();
      Window.open(result.getDownloadUrl(), "_blank", null);
    }
  };

  private final PriorityProjectProjectDossierDetailView view;

  private final SubProjectExportPollingAgentImpl pollingAgent;

  @Inject
  public PriorityProjectProjectDossierDetailActivity(@Assisted final PriorityProjectProjectDossierDetailPlace place,
      final RegisterAppContext appContext, final PriorityProjectBaseView baseView, final PriorityProjectProjectDossierBaseView subBaseView,
      final PriorityProjectProjectDossierDetailView view, final PriorityProjectExportDialogController exportDialogController) {
    super(place, appContext, baseView, subBaseView, PriorityProjectDossierDetailViewType.DETAIL, exportDialogController);
    this.view = view;

    pollingAgent = new SubProjectExportPollingAgentImpl();
  }

  @Override
  protected void onPriorityProjectLoaded(final PriorityProject priorityProject) {
    onPrioritySubProjectLoaded(determinePrioritySubProject(priorityProject));
    appContext.getPriorityProjectService().fetchPrioritySubProject(new PrioritySubProjectKey(
        currentPlace.getKey(), currentPlace.getSubKey().getReference()), new AppAsyncCallback<PrioritySubProject>() {
      @Override
      public void onSuccess(final PrioritySubProject prioritySubProject) {
        onPrioritySubProjectLoaded(prioritySubProject);
        view.setData(prioritySubProject, priorityProject);
      }

      @Override
      public void onFailure(final Throwable caught) {
        super.onFailure(caught);
      }
    });
  }

  @Override
  protected void startSubChild(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  public void openRequestFile(final PrioritySubProject prioritySubProject) {
    Window.open(prioritySubProject.getUrl(RequestFileType.APPLICATION), "_blank", "");
  }

  @Override
  public void startExport() {
    pollingAgent.stop();
    appContext.getRequestExportService().startSubPriorityExport(currentPlace.getSubKey(), new AppAsyncCallback<String>() {
      @Override
      public void onSuccess(final String result) {
        broadcastNotificationMessage(M.messages().registerPrioritySubProjectExportStart());
        pollingAgent.start(result, exportReadyCallback);
      }
    });
  }

}
