/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.geo;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.WMSMapLayer;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;

@Singleton
public class RegisterProjectContributionLayerWrapper {
  interface RegisterProjectContributionLayerWrapperEventBinder extends EventBinder<RegisterProjectContributionLayerWrapper> { }

  private final RegisterProjectContributionLayerWrapperEventBinder eventBinder = GWT.create(RegisterProjectContributionLayerWrapperEventBinder.class);

  private final MapLayoutPanel map;
  private final LayerWMSProps layerProps;
  private WMSMapLayer filterLayer;

  private final FetchGeometryServiceAsync service;

  @Inject
  public RegisterProjectContributionLayerWrapper(final RegisterCoreMapLayoutPanel map, final FetchGeometryServiceAsync service,
      final RegisterContext context) {
    this.map = map;
    this.service = service;
    this.layerProps = context.getLayer(WMSLayerType.WMS_PERMIT_DEMANDS_VIEW);
    LayerPreparationUtil.prepareLayer(service, layerProps);
    eventBinder.bindEventHandlers(this, map.getEventBus());

  }

  public void updateProjectContributionAndDraw(final int id) {
    layerProps.setParamFilter("request_id", id);
    layerProps.setEnabled(true);

    updateLayer();
  }

  /**
   * Updates the layer
   */
  private void updateLayer() {
    if (filterLayer == null) {
      LayerPreparationUtil.prepareLayer(service, layerProps, new AppAsyncCallback<LayerProps>() {
        @Override
        public void onSuccess(final LayerProps result) {
          detach();
          layerProps.setEnabled(true);
          filterLayer = (WMSMapLayer) map.addLayer(layerProps);
        }
      });
    } else {
      filterLayer.redraw();
    }

  }

  public void detach() {
    if (filterLayer != null) {
      map.removeLayer((MapLayer) filterLayer);
      filterLayer = null;
    }
  }
}
