/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectActualisationPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.PriorityProjectViewType;
import nl.overheid.aerius.wui.register.ui.importer.RegisterActualisationDialogController;
import nl.overheid.aerius.wui.register.ui.importer.RegisterFactsheetDialogController;
import nl.overheid.aerius.wui.register.ui.importer.RegisterImportDialogPanel;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public class PriorityProjectActualisationActivity extends PriorityProjectBaseActivity<PriorityProjectPlace>
    implements PriorityProjectActualisationView.Presenter {

  private final PriorityProjectActualisationView view;

  @Inject
  public PriorityProjectActualisationActivity(@Assisted final PriorityProjectActualisationPlace place, final RegisterAppContext appContext,
      final PriorityProjectBaseView baseView, final PriorityProjectActualisationView view,
      final PriorityProjectExportDialogController exportDialogController) {
    super(place, appContext, baseView, PriorityProjectViewType.ACTUALISATION, exportDialogController);
    this.view = view;
  }

  @Override
  public void openFactsheetPriorityProject() {
    final RegisterImportDialogPanel<PriorityProjectKey> dialog =
        new RegisterImportDialogPanel<>(SupportedUploadFormat.PRIORITY_PROJECT_FACTSHEET);

    final RegisterFactsheetDialogController newFactsheetController =
        new RegisterFactsheetDialogController(appContext.getPlaceController(), RegisterRetrieveImportServiceAsync.Util.getInstance(),
            dialog, priorityProject.getReference()) {
      @Override
      public void handleResult(final PriorityProjectKey key) {
        hide();
        appContext.invalidatePriorityProject();
        refresh();
      }
    };

    newFactsheetController.setTitle(M.messages().registerPriorityProjectProjectFactsheetTitle());
    newFactsheetController.show();
  }

  @Override
  public void openActualisePriorityProject() {
    final RegisterImportDialogPanel<PriorityProjectKey> dialog =
        new RegisterImportDialogPanel<>(SupportedUploadFormat.PRIORITY_PROJECT_ACTUALISATION);

    final RegisterActualisationDialogController newActualisationController =
        new RegisterActualisationDialogController(appContext.getPlaceController(), RegisterRetrieveImportServiceAsync.Util.getInstance(),
            dialog, priorityProject.getReference()) {
      @Override
      public final void handleResult(final PriorityProjectKey key) {
        hide();
        appContext.invalidatePriorityProject();
        refresh();
      }
    };

    newActualisationController.setTitle(M.messages().registerPriorityProjectProjectActualiseTitle());
    newActualisationController.show();
  }

  @Override
  public void removeActualisation() {
    if (!Window.confirm(M.messages().registerapplicationDeleteActualisationTitle())) {
      return;
    }
    setLocked(true);
    appContext.getPriorityProjectService().deletePriorityProjectActualisationFile(
        priorityProject.getKey(), priorityProject.getLastModified(), new AppAsyncCallback<Void>() {

      @Override
      public void onSuccess(final Void result) {
        setLocked(false);
        appContext.invalidatePriorityProject();
        refresh();
      }

      @Override
      public void onFailure(final Throwable caught) {
        PriorityProjectActualisationActivity.this.onFailure(caught);
        super.onFailure(caught);
      }
    });
  }

  @Override
  public void startChild(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  protected void onPriorityProjectLoaded(final PriorityProject priorityProject) {
    view.setData(priorityProject);
  }

  @Override
  public void openRequestFile(final Request request, final RequestFileType fileType) {
    Window.open(request.getUrl(fileType), "_blank", "");
  }

}
