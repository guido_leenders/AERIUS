/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register;

import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.GWT;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.SimpleLegendTitleFactory;
import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.context.AdminUserContext;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;
import nl.overheid.aerius.wui.admin.AdminClientModule;
import nl.overheid.aerius.wui.geo.LegendTitleFactory;
import nl.overheid.aerius.wui.main.AeriusClientModule.AeriusPlaceController;
import nl.overheid.aerius.wui.main.AeriusContextProvider;
import nl.overheid.aerius.wui.main.ApplicationInitializer;
import nl.overheid.aerius.wui.main.RootPanelFactory;
import nl.overheid.aerius.wui.main.RootPanelFactory.RootLayoutPanelFactoryImpl;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.place.DefaultPlace;
import nl.overheid.aerius.wui.main.ui.ApplicationDisplay;
import nl.overheid.aerius.wui.main.ui.ApplicationRootMenuViewImpl;
import nl.overheid.aerius.wui.main.ui.ApplicationRootView;
import nl.overheid.aerius.wui.main.ui.menu.MenuItemFactory;
import nl.overheid.aerius.wui.main.ui.menu.SimpleMenuItemFactory;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger.SuperNitroTurboJuggernaut;
import nl.overheid.aerius.wui.main.widget.NotificationButton;
import nl.overheid.aerius.wui.main.widget.NotificationButtonRectangular;
import nl.overheid.aerius.wui.register.RegisterActivityMapper.ActivityFactory;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.geo.RegisterCoreMapLayoutPanel;
import nl.overheid.aerius.wui.register.i18n.HelpToolTipRegister;
import nl.overheid.aerius.wui.register.place.PriorityProjectOverviewPlace;
import nl.overheid.aerius.wui.register.place.RegisterPlaceHistoryMapper;
import nl.overheid.aerius.wui.register.ui.DashboardView;
import nl.overheid.aerius.wui.register.ui.DashboardViewImpl;
import nl.overheid.aerius.wui.register.ui.NoticeView;
import nl.overheid.aerius.wui.register.ui.NoticeViewImpl;
import nl.overheid.aerius.wui.register.ui.PermitDetailView;
import nl.overheid.aerius.wui.register.ui.PermitDetailViewImpl;
import nl.overheid.aerius.wui.register.ui.PermitOverviewView;
import nl.overheid.aerius.wui.register.ui.PermitOverviewViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectActualisationView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectActualisationViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierEditView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierEditViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectOverviewView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectOverviewViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierBaseView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierBaseViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierDetailView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierDetailViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierReviewView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierReviewViewImpl;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectView;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectViewImpl;
import nl.overheid.aerius.wui.register.ui.RegisterViewImpl;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailDossierView;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailDossierViewImpl;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailMapView;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailMapViewImpl;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailView;
import nl.overheid.aerius.wui.register.ui.notice.NoticeDetailViewImpl;
import nl.overheid.aerius.wui.register.util.development.RegisterDevPanel;
import nl.overheid.aerius.wui.register.util.development.SuperNitroTurboRegisterLogger;
import nl.overheid.aerius.wui.register.util.development.SuperNitroTurboRegisterLogger.SuperNitroTurboRegisterJuggernaut;


class RegisterClientModule extends AdminClientModule {
  @Override
  protected void configure() {
    super.configure();

    bind(PlaceController.class).to(AeriusPlaceController.class).in(Singleton.class);
    bind(ActivityMapper.class).to(RegisterActivityMapper.class).in(Singleton.class);
    bind(Place.class).annotatedWith(DefaultPlace.class).to(PriorityProjectOverviewPlace.class).in(Singleton.class);
    bind(PlaceHistoryMapper.class).to(RegisterPlaceHistoryMapper.class).in(Singleton.class);
    bind(new TypeLiteral<AeriusContextProvider<?, ?>>() { }).to(RegisterContextProvider.class);
    bind(new TypeLiteral<AppContext<?, ?>>() {}).to(RegisterAppContext.class);
    bind(UserContext.class).to(RegisterUserContext.class);
    bind(Context.class).to(RegisterContext.class);
    bind(AdminUserContext.class).to(RegisterUserContext.class);
    bind(AdminContext.class).to(RegisterContext.class);
    bind(HelpToolTipInfo.class).to(HelpToolTipRegister.class);
    bind(NotificationButton.class).to(NotificationButtonRectangular.class);
    bind(MapLayoutPanel.class).to(RegisterCoreMapLayoutPanel.class);
    bind(MenuItemFactory.class).to(SimpleMenuItemFactory.class);
    bind(LegendTitleFactory.class).to(SimpleLegendTitleFactory.class).in(Singleton.class);
    bind(ApplicationRootView.class).to(ApplicationRootMenuViewImpl.class).in(Singleton.class);
    bind(RootPanelFactory.class).to(RootLayoutPanelFactoryImpl.class);
    bind(ApplicationInitializer.class).to(RegisterApplicationInitializer.class).in(Singleton.class);

    // View instances.
    bind(ApplicationDisplay.class).to(RegisterViewImpl.class);
    bind(DashboardView.class).to(DashboardViewImpl.class);
    bind(PermitOverviewView.class).to(PermitOverviewViewImpl.class);
    bind(PermitDetailView.class).to(PermitDetailViewImpl.class);
    bind(NoticeView.class).to(NoticeViewImpl.class);
    bind(NoticeDetailView.class).to(NoticeDetailViewImpl.class);
    bind(NoticeDetailDossierView.class).to(NoticeDetailDossierViewImpl.class);
    bind(NoticeDetailMapView.class).to(NoticeDetailMapViewImpl.class);
    bind(PriorityProjectBaseView.class).to(PriorityProjectBaseViewImpl.class);
    bind(PriorityProjectOverviewView.class).to(PriorityProjectOverviewViewImpl.class);
    bind(PriorityProjectDossierView.class).to(PriorityProjectDossierViewImpl.class);
    bind(PriorityProjectDossierEditView.class).to(PriorityProjectDossierEditViewImpl.class);
    bind(PriorityProjectProjectView.class).to(PriorityProjectProjectViewImpl.class);
    bind(PriorityProjectActualisationView.class).to(PriorityProjectActualisationViewImpl.class);
    bind(PriorityProjectProjectDossierBaseView.class).to(PriorityProjectProjectDossierBaseViewImpl.class);
    bind(PriorityProjectProjectDossierDetailView.class).to(PriorityProjectProjectDossierDetailViewImpl.class);
    bind(PriorityProjectProjectDossierReviewView.class).to(PriorityProjectProjectDossierReviewViewImpl.class);

    if (!GWT.isScript()) {
      bind(SuperNitroTurboLogger.class).to(SuperNitroTurboRegisterLogger.class);
      bind(SuperNitroTurboJuggernaut.class).to(SuperNitroTurboRegisterJuggernaut.class);
      bind(EmbeddedDevPanel.class).to(RegisterDevPanel.class);
    }

    // Place to Activity assisted injection
    install(new GinFactoryModuleBuilder().build(ActivityFactory.class));
  }

  @Provides
  public RegisterContext getRegisterContext(final RegisterContextProvider provider) {
    return provider.getContext();
  }

  @Provides
  public RegisterUserContext getRegisterUserContext(final RegisterContextProvider provider) {
    return provider.getUserContext();
  }
}
