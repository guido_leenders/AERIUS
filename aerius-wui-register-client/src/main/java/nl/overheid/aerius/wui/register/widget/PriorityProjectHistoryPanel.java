/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.wui.main.i18n.M;

public class PriorityProjectHistoryPanel extends RequestHistoryPanel {

  public interface Templates extends SafeHtmlTemplates {
    @SafeHtmlTemplates.Template("<strong>{0}</strong> {1}<br />")
    SafeHtml subproject(String label, String reference);
  }

  private static final Templates TEMPLATE = GWT.create(Templates.class);

  @Override
  protected SafeHtml getAuditTrailText(final AuditTrailItem item) {
    final SafeHtmlBuilder builder = new SafeHtmlBuilder();

    if (item.getSegment() == SegmentType.PRIORITY_SUBPROJECTS) {
      builder.append(TEMPLATE.subproject(M.messages().registerPriorityProjectHistoryPanelReference(), item.getReference()));
    }
    builder.append(super.getAuditTrailText(item));

    return builder.toSafeHtml();
  }


}
