/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.geo.LayerPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButton;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.geo.NoticeMarkerLayerWrapper;
import nl.overheid.aerius.wui.register.geo.RegisterCoreMapLayoutPanel;

/**
 * Show map with other project in direct area.
 */
public class NoticeDetailMapViewImpl extends Composite implements NoticeDetailMapView {

  interface NoticeDetailMapViewImplUiBinder extends UiBinder<Widget, NoticeDetailMapViewImpl> {}

  private static final NoticeDetailMapViewImplUiBinder UI_BINDER = GWT.create(NoticeDetailMapViewImplUiBinder.class);

  @UiField Label explanation;
  @UiField(provided = true) NoticesMapTable noticesMapTable;
  @UiField AttachedPopupButton layerPanelButton;
  @UiField(provided = true) RegisterCoreMapLayoutPanel mapPanel;
  @UiField(provided = true) LayerPanel layerPanel;

  private final NoticeMarkerLayerWrapper noticeLayer;
  private com.google.web.bindery.event.shared.HandlerRegistration mapHandlers;
  private Presenter presenter;


  @Inject
  public NoticeDetailMapViewImpl(final RegisterContext context, final RegisterUserContext userContext, final EventBus eventBus,
      final HelpPopupController hpC, final RegisterCoreMapLayoutPanel mapPanel, final LayerPanel layerPanel) {
    this.mapPanel = mapPanel;
    this.layerPanel = layerPanel;
    noticesMapTable = new NoticesMapTable(eventBus);

    initWidget(UI_BINDER.createAndBindUi(this));
    explanation.setText(M.messages().registerNoticeDossierMapTitle(FormatUtil.formatDistanceToWholeWithUnit(context.getMaxRadiusForRequests())));

    noticeLayer = new NoticeMarkerLayerWrapper(mapPanel);
    noticesMapTable.ensureDebugId(TestIDRegister.NOTICES_CELLTABLE);
    noticesMapTable.asDataTable().addSelectionChangeHandler(new Handler() {

      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        presenter.onNoticeSelection(((SingleSelectionModel<Notice>) noticesMapTable.asDataTable().getSelectionModel()).getSelectedObject());
      }
    });

  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void startupMap() {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        mapHandlers = mapPanel.attachEventBinders();
        mapPanel.nudge();
        noticeLayer.attach();
      }
    });
  }

  @Override
  public void cleanup() {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        if (mapHandlers != null) {
          mapHandlers.removeHandler();
        }
        noticeLayer.detach();
      }
    });
  }

  @Override
  public void updateListData(final ArrayList<Notice> result) {
    noticesMapTable.asDataTable().addRowData(result);
  }

  @Override
  public void setLoading(final boolean loading) {
    noticesMapTable.asDataTable().showLoadingWidget(loading);
  }

  @Override
  public void setData(final Notice notice) {
    mapPanel.getMarkersLayer().clear();
  }

}
