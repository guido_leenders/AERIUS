/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectProgress;
import nl.overheid.aerius.shared.domain.register.PriorityProjectSortableAttribute;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader.SortActionHandler;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeaderBinderUtil;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.ScalableImageColumn;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;
import nl.overheid.aerius.wui.register.util.PriorityProjectProgressUtil;

public class PriorityProjectDataTable extends Composite
implements IsInteractiveDataTable<PriorityProject>, SortActionHandler<PriorityProjectSortableAttribute> {

  interface PriorityProjectDataTableUiBinder extends UiBinder<Widget, PriorityProjectDataTable> {}

  private static final PriorityProjectDataTableUiBinder UI_BINDER = GWT.create(PriorityProjectDataTableUiBinder.class);

  private static final int PROGRESS_COLUMN_IMAGE_WIDTH = 31;
  private static final int PROGRESS_COLUMN_IMAGE_HEIGHT = 31;

  @UiField SimpleInteractiveClickDivTable<PriorityProject> table;

  @UiField ColumnSortHeader<PriorityProjectSortableAttribute> headerSector;
  @UiField ColumnSortHeader<PriorityProjectSortableAttribute> headerProgress;
  @UiField ColumnSortHeader<PriorityProjectSortableAttribute> headerProjectName;
  @UiField ColumnSortHeader<PriorityProjectSortableAttribute> headerProjectId;
  @UiField ColumnSortHeader<PriorityProjectSortableAttribute> headerAuthority;

  @UiField(provided = true) SimpleWidgetFactory<PriorityProject> iconColumn;
  @UiField(provided = true) ScalableImageColumn<PriorityProject> progressColumn;
  @UiField(provided = true) TextColumn<PriorityProject> projectNameColumn;
  @UiField(provided = true) TextColumn<PriorityProject> projectIdColumn;
  @UiField(provided = true) TextColumn<PriorityProject> authorityColumn;

  private SortActionHandler<PriorityProjectSortableAttribute> handler;

  public PriorityProjectDataTable() {
    iconColumn = new SimpleWidgetFactory<PriorityProject>() {
      @Override
      public Widget createWidget(final PriorityProject object) {
        final FlowPanel panel = new FlowPanel();
        panel.getElement().getStyle().setPosition(Position.RELATIVE);

        final Image image = new Image(SectorImageUtil.getSectorImageResource(object.getSectorIcon()));
        panel.add(image);

        if (object.isMultipleSectors()) {
          final HTML plus = new HTML(R.images().plus().getText());
          plus.getElement().getStyle().setPosition(Position.ABSOLUTE);
          plus.getElement().getStyle().setRight(0, Unit.PX);
          panel.add(plus);
        }

        return panel;
      }
    };

    progressColumn = new ScalableImageColumn<PriorityProject>(PROGRESS_COLUMN_IMAGE_WIDTH, PROGRESS_COLUMN_IMAGE_HEIGHT) {
      @Override
      public DataResource getValue(final PriorityProject object) {
        return PriorityProjectProgressUtil.getPriorityProjectProgressIcon(PriorityProjectProgress.determineProgress(object));
      }
    };

    projectNameColumn = new TextColumn<PriorityProject>() {
      @Override
      public String getValue(final PriorityProject value) {
        return value.getScenarioMetaData().getProjectName();
      }
    };

    projectIdColumn = new TextColumn<PriorityProject>() {
      @Override
      public String getValue(final PriorityProject value) {
        return value.getDossierMetaData().getDossierId();
      }
    };

    authorityColumn = new TextColumn<PriorityProject>() {
      @Override
      public String getValue(final PriorityProject value) {
        return value.getAuthority().getDescription();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    table.setSelectionModel(new SingleSelectionModel<PriorityProject>());

    headerSector.setHandler(this);
    headerProgress.setHandler(this);
    headerProjectName.setHandler(this);
    headerProjectId.setHandler(this);
    headerAuthority.setHandler(this);

    ColumnSortHeaderBinderUtil.connect(headerSector, headerProgress, headerProjectName, headerProjectId, headerAuthority);

    table.setSelectionModel(new SingleSelectionModel<PriorityProject>());
    table.setLoadingByDefault(true);
  }

  @Override
  public SimpleInteractiveClickDivTable<PriorityProject> asDataTable() {
    return table;
  }

  @Override
  public void setSorting(final PriorityProjectSortableAttribute attribute, final SortableDirection dir) {
    if (handler != null) {
      handler.setSorting(attribute, dir);
    }
  }

  public void setSortHandler(final SortActionHandler<PriorityProjectSortableAttribute> handler) {
    this.handler = handler;
  }
}
