/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class PriorityProjectProjectDossierDetailViewImpl extends Composite implements PriorityProjectProjectDossierDetailView {
  interface PriorityProjectProjectDossierDetailViewImplUiBinder extends UiBinder<Widget, PriorityProjectProjectDossierDetailViewImpl> {}

  private static final PriorityProjectProjectDossierDetailViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectProjectDossierDetailViewImplUiBinder.class);

  private static final int EMISSION_PRECISION = 2;

  private Presenter presenter;
  private PrioritySubProject prioritySubProject;

  @UiField Label label;
  @UiField Label subProjectName;
  @UiField Label referenceId;
  @UiField Label initiator;
  @UiField Label temporayProject;
  @UiField Label nh3Label;
  @UiField Label nh3Value;
  @UiField Label noxLabel;
  @UiField Label noxValue;
  @UiField Label contractor;
  @UiField Anchor reference;

  @UiField Label description;

  @Inject
  public PriorityProjectProjectDossierDetailViewImpl(final Context context, final HelpPopupController hpC) {
    initWidget(UI_BINDER.createAndBindUi(this));

    nh3Label.setText(Substance.NH3.name());
    noxLabel.setText(Substance.NOX.name());
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setData(final PrioritySubProject prioritySubProject, final PriorityProject priorityProject) {
    this.prioritySubProject = prioritySubProject;

    label.setText(prioritySubProject.getLabel() == null ? "-" : prioritySubProject.getLabel());
    subProjectName.setText(prioritySubProject.getScenarioMetaData().getProjectName());
    referenceId.setText(prioritySubProject.getReference());
    initiator.setText(prioritySubProject.getScenarioMetaData().getCorporation());
    if (prioritySubProject.getTemporaryPeriod() != null) {
      temporayProject.setText(prioritySubProject.getTemporaryPeriod().toString());
    } else {
      temporayProject.setText(M.messages().registerApplicationTabAppDurationNA());
    }
    nh3Value.setText(FormatUtil.formatEmissionWithUnit(
        prioritySubProject.getProposedEmissionValues().getEmission(new EmissionValueKey(Substance.NH3)), EMISSION_PRECISION));
    noxValue.setText(FormatUtil.formatEmissionWithUnit(
        prioritySubProject.getProposedEmissionValues().getEmission(new EmissionValueKey(Substance.NOX)), EMISSION_PRECISION));
    contractor.setText(prioritySubProject.getAuthority().getDescription());
    reference.setText(prioritySubProject.getReference());
    description.setText(prioritySubProject.getScenarioMetaData().getDescription());
  }

  @UiHandler({"pdfDownloadImage", "reference"})
  public void onPdfDownloadImage(final ClickEvent event) {
    presenter.openRequestFile(prioritySubProject);
  }

}
