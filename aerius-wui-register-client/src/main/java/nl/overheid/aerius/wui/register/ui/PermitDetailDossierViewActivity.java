/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.shared.GWT;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitDossierEditPlace;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;
import nl.overheid.aerius.wui.register.ui.PermitDetailDossierViewImpl.PermitDetailDossierViewDriver;

/**
 *
 */
public class PermitDetailDossierViewActivity extends PermitDetailDossierBaseActivity<PermitDossierPlace> {

  private final PermitDetailDossierViewDriver driver = GWT.create(PermitDetailDossierViewDriver.class);
  private final PermitDetailDossierViewImpl contentPanel;

  @Inject
  public PermitDetailDossierViewActivity(final RegisterAppContext appContext, final PermitDetailView view,
      final PermitDetailDossierViewImpl contentPanel, @Assisted final PermitDossierPlace place) {
    super(appContext, view, contentPanel, place);
    this.contentPanel = contentPanel;
    driver.initialize(contentPanel);
  }

  @Override
  protected void onPermitLoaded(final Permit permit) {
    super.onPermitLoaded(permit);

    driver.edit(permit);
  }

  @Override
  public void save() {
    // no-op
  }

  @Override
  public void cancel() {
    // no-op
  }

  @Override
  public void edit() {
    goTo(new PermitDossierEditPlace(permit.getPermitKey()));
  }

  @Override
  protected void updatePermissions(final UserProfile user, final Permit permit) {
    super.updatePermissions(user, permit);
    contentPanel.setUserCanEdit(UserProfileUtil.hasPermissionForAuthority(user, permit.getAuthority(),
        RegisterPermission.UPDATE_PERMIT_DATE,
        RegisterPermission.UPDATE_PERMIT_REMARKS,
        RegisterPermission.UPDATE_PERMIT_HANDLER,
        RegisterPermission.UPDATE_PERMIT_DOSSIER_NUMBER));
  }
}
