/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierEditPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.PriorityProjectViewType;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierEditViewImpl.PriorityProjectDossierEditViewDriver;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectDossierEditor;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public class PriorityProjectDossierEditActivity extends PriorityProjectBaseActivity<PriorityProjectDossierEditPlace>
    implements PriorityProjectDossierEditView.Presenter {

  private final SimpleBeanEditorDriver<PriorityProject, PriorityProjectDossierEditor> dossierEditDriver = GWT.create(PriorityProjectDossierEditViewDriver.class);

  private final PriorityProjectDossierEditView view;

  private PriorityProjectKey initialKey;

  @Inject
  public PriorityProjectDossierEditActivity(@Assisted final PriorityProjectDossierEditPlace place, final RegisterAppContext appContext,
      final PriorityProjectBaseView baseView, final PriorityProjectDossierEditView view,
      final PriorityProjectExportDialogController exportDialogController) {
    super(place, appContext, baseView, PriorityProjectViewType.DOSSIER, exportDialogController);
    this.view = view;

    dossierEditDriver.initialize(view.getDossierEditor());
  }

  @Override
  public void startChild(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  public void save() {
    final PriorityProject flush = dossierEditDriver.flush();

    if (dossierEditDriver.hasErrors()) {
      ErrorPopupController.addErrors(dossierEditDriver.getErrors());
      return;
    } else {
      appContext.invalidatePriorityProject();

      appContext.getPriorityProjectService().updatePriorityProject(
          initialKey, flush, priorityProject.getLastModified(), new AppAsyncCallback<Void>() {
        @Override
        public void onSuccess(final Void result) {
          final PriorityProjectKey key = priorityProject.getKey();
          broadcastNotificationMessage(M.messages().registerNotificationPriorityProjectUpdated(key.getDossierId()));

          goTo(new PriorityProjectDossierPlace(key));
        }
      });
    }
  }

  @Override
  public void cancel() {
    appContext.invalidatePriorityProject();

    goTo(new PriorityProjectDossierPlace(initialKey));
  }

  @Override
  protected void onPriorityProjectLoaded(final PriorityProject priorityProject) {
    dossierEditDriver.edit(priorityProject);
    initialKey = priorityProject.getKey();
  }
}
