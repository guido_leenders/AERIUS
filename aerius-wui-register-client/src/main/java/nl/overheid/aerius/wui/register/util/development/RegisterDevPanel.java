/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.util.development;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitPlace;
import nl.overheid.aerius.wui.register.place.RequestKeyPlace;

public class RegisterDevPanel extends Composite implements EmbeddedDevPanel {
  interface RegisterDevPanelUiBinder extends UiBinder<Widget, RegisterDevPanel> {}

  private static RegisterDevPanelUiBinder uiBinder = GWT.create(RegisterDevPanelUiBinder.class);

  private final EventBus eventBus;
  private final RegisterAppContext appContext;


  @Inject
  public RegisterDevPanel(final EventBus eventBus, final RegisterAppContext appContext) {
    this.eventBus = eventBus;
    this.appContext = appContext;
    initWidget(uiBinder.createAndBindUi(this));
  }

  @UiHandler("calc")
  void checkCalcComplete(final ClickEvent e) {
    if (appContext.getPlaceController().getWhere() instanceof RequestKeyPlace) {
      final PermitPlace pp = (PermitPlace) appContext.getPlaceController().getWhere();
      appContext.getPermitService().isCalculationFinished(pp.getKey(), new AppAsyncCallback<PermitKey>() {
        @Override
        public void onSuccess(final PermitKey key) {
          NotificationUtil.broadcastMessage(eventBus, "Calculation done for " + pp.getKey().getDossierId()
              + " - " + (key == null ? "Nope :(" : "Yep!"));
        }
      });
    } else {
      NotificationUtil.broadcastMessage(eventBus, "No application loaded.");
    }
  }
}
