/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.permit;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.widget.SimpleFocusLabel;
import nl.overheid.aerius.wui.main.widget.SimpleTabPanel;
import nl.overheid.aerius.wui.register.place.PermitDevelopmentSpacePlace;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;
import nl.overheid.aerius.wui.register.place.PermitPlace;
import nl.overheid.aerius.wui.register.place.PermitRequestPlace;
import nl.overheid.aerius.wui.register.place.PermitReviewPlace;
import nl.overheid.aerius.wui.register.place.RequestKeyPlace;

/**
 * A simple UI wrapper for the register tab panel.
 */
@Singleton
public class PermitTabPanel extends Composite {
  interface PermitTabPanelUiBinder extends UiBinder<Widget, PermitTabPanel> {
  }

  private static final PermitTabPanelUiBinder UI_BINDER = GWT.create(PermitTabPanelUiBinder.class);

  @UiField SimpleTabPanel tabPanel;
  @UiField SimpleFocusLabel tabApplicationDossier;
  @UiField SimpleFocusLabel tabApplicationInformation;
  @UiField SimpleFocusLabel tabApplicationPermitRules;
  @UiField SimpleFocusLabel tabApplicationDevelopmentSpace;

  private final PlaceController placeController;

  /**
   * Default constructor for the simple {@link SimpleTabPanel} register UI wrapper.
   */
  @Inject
  public PermitTabPanel(final EventBus eventBus, final PlaceController placeController) {
    this.placeController = placeController;
    initWidget(UI_BINDER.createAndBindUi(this));
    eventBus.addHandler(PlaceChangeEvent.TYPE, new PlaceChangeEvent.Handler() {
      @Override
      public void onPlaceChange(final PlaceChangeEvent event) {
        if (event.getNewPlace() instanceof PermitPlace) {
          setTabFocus((PermitPlace) event.getNewPlace());
        }
      }
    });
  }

  /**
   * Lock the tab panel, causing it to never fire a selection event for lockable
   * tabs and change styling on preset tabs.
   *
   * @param locked True to lock, false to unlock.
   */
  public void setLocked(final boolean locked) {
    tabPanel.setLocked(locked);
  }

  public void setTabFocus(final PermitPlace place) {
    if (place instanceof PermitRequestPlace) {
      tabPanel.setFocus(tabApplicationInformation, false);
    } else if (place instanceof PermitReviewPlace) {
      tabPanel.setFocus(tabApplicationPermitRules, false);
    } else if (place instanceof PermitDevelopmentSpacePlace) {
      tabPanel.setFocus(tabApplicationDevelopmentSpace, false);
    } else {
      tabPanel.setFocus(tabApplicationDossier, false);
    }
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    tabApplicationDossier.ensureDebugId(TestIDRegister.APPLICATION_TAB_DOSSIER);
    tabApplicationInformation.ensureDebugId(TestIDRegister.APPLICATION_TAB_DETAILS);
    tabApplicationPermitRules.ensureDebugId(TestIDRegister.APPLICATION_TAB_PERMIT_RULES);
    tabApplicationDevelopmentSpace.ensureDebugId(TestIDRegister.APPLICATION_TAB_DEVELOPMENT_SPACE);
  }

  @UiHandler("tabPanel")
  void onTabSelection(final SelectionEvent<Integer> e) {
    if (placeController.getWhere() instanceof RequestKeyPlace) {
      final PermitPlace place = (PermitPlace) placeController.getWhere();
      final Place newPlace;
      switch (e.getSelectedItem()) {
      case 1:
        newPlace = new PermitRequestPlace(place.getKey());
        break;
      case 2:
        newPlace = new PermitReviewPlace(place.getKey());
        break;
      case 3:
        newPlace = new PermitDevelopmentSpacePlace(place.getKey());
        break;
      case 0:
      default:
        newPlace = new PermitDossierPlace(place.getKey());
        break;
      }
      placeController.goTo(newPlace);
    }
  }
}
