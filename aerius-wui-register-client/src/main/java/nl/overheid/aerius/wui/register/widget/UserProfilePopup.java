/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.TextUtil;
import nl.overheid.aerius.wui.main.widget.AttachedPopupBase;
import nl.overheid.aerius.wui.main.widget.AttachedPopupContent;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;

public class UserProfilePopup extends Composite implements AttachedPopupContent, RequiresResize, Editor<UserProfile> {
  interface UserProfilePopupUiBinder extends UiBinder<Widget, UserProfilePopup> {}

  private static final UserProfilePopupUiBinder UI_BINDER = GWT.create(UserProfilePopupUiBinder.class);

  @UiField AttachedPopupBase wrapper;
  @UiField FlowPanel container;

  @Ignore @UiField Label name;
  @Path("") final LeafValueEditor<UserProfile> nameEditor = new FakeLeafValueEditor<UserProfile>() {
    @Override
    public void setValue(final UserProfile value) {
      name.setText(M.messages().registerFullUser(value.getFirstName(), value.getInitials(), value.getLastName()));
    }
  };

  @UiField Label emailAddress;
  @Path("authority.description") @UiField Label authority;
  @Ignore @UiField Label roles;
  final LeafValueEditor<HashSet<UserRole>> rolesEditor = new FakeLeafValueEditor<HashSet<UserRole>>() {
    @Override
    public void setValue(final HashSet<UserRole> value) {
      final String[] rolesArr = new String[value.size()];
      final int i = 0;
      for (final UserRole role : value) {
        rolesArr[i] = role.getName();
      }

      roles.setText(TextUtil.joinStrings(rolesArr, ", "));
    }
  };

  private boolean layoutScheduled;
  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  @Inject
  public UserProfilePopup() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setPopup(final AttachedPopupPanel popup) {
    wrapper.setPopup(popup);
  }

  @Override
  public void onResize() {
    scheduledLayout();
  }

  private void scheduledLayout() {
    if (layoutScheduled) {
      return;
    }

    layoutScheduled = true;
    Scheduler.get().scheduleDeferred(layoutCmd);
  }

  private void forceLayout() {
    container.getElement().getStyle().setProperty("maxHeight", wrapper.calculateMaxHeightBasedOnScreen(), Unit.PX);
  }
}
