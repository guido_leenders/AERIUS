/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.PermitReviewInfo;
import nl.overheid.aerius.shared.domain.register.PermitSummary;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.register.ui.permit.PermitReviewTable;

/**
 *
 */
public class PermitDetailReviewViewImpl extends Composite implements PermitDetailSummaryView {
  interface PermitDetailReviewViewUiBinder extends UiBinder<Widget, PermitDetailReviewViewImpl> {}

  private static final PermitDetailReviewViewUiBinder UI_BINDER = GWT.create(PermitDetailReviewViewUiBinder.class);

  @UiField SwitchPanel switchPanel;
  @UiField(provided = true) RadioButtonGroup<PermitDetailReviewViewType> viewSwitcher;
  @UiField(provided = true) PermitReviewTable reviewPanel;
  @UiField Image iconInsufficientOR;
  @UiField Label labelInsufficientOR;
  @UiField Image iconSufficientOR;
  @UiField Label labelSufficientOR;

  private PermitSummary summary;
  private PermitDetailReviewViewType type;

  @Inject
  public PermitDetailReviewViewImpl(final PermitReviewTable reviewPanel) {
    this.reviewPanel = reviewPanel;

    viewSwitcher = new RadioButtonGroup<>(new RadioButtonContentResource<PermitDetailReviewViewType>() {

      @Override
      public String getRadioButtonText(final PermitDetailReviewViewType value) {
        return M.messages().registerPermitDetailReviewViewType(value.name());
      }
    });

    initWidget(UI_BINDER.createAndBindUi(this));

    iconInsufficientOR.setResource(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK));
    iconSufficientOR.setResource(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.EXCEEDING_SPACE_CHECK));
    labelInsufficientOR.setText(M.messages().registerReviewLegendDevelopmentRule(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK));
    labelSufficientOR.setText(M.messages().registerReviewLegendDevelopmentRule(DevelopmentRule.EXCEEDING_SPACE_CHECK));

    viewSwitcher.addButtons(PermitDetailReviewViewType.values());
    viewSwitcher.setValue(PermitDetailReviewViewType.ALL, false);

    viewSwitcher.ensureDebugId(TestIDRegister.APPLICATION_TAB_REVIEW_VIEW_SWITCHER);

    switchPanel.showWidget(1);
  }

  @Override
  public void setSummary(final PermitSummary summary) {
    this.summary = summary;

    updateTable();
  }

  @UiHandler("viewSwitcher")
  public void onViewSwitch(final ValueChangeEvent<PermitDetailReviewViewType> e) {
    type = e.getValue();

    updateTable();
  }

  private void updateTable() {
    if (summary != null) {
      final ArrayList<PermitReviewInfo> reviewInfo = new ArrayList<>();

      // I'd like to use Lambda's/predicates and such, but those aren't available in the current GWT version
      for (final PermitReviewInfo item : summary.getReviewInfo()) {
        final boolean filterOnlyExceeding = PermitDetailReviewViewType.ONLY_EXCEEDING == type;
        if (filterOnlyExceeding == item.isOnlyExceeding()) {
          reviewInfo.add(item);
        }
      }

      reviewPanel.asDataTable().setRowData(reviewInfo);
    }
  }

  @Override
  public void setSummaryAvailable(final boolean available) {
    switchPanel.showWidget(available ? 1 : 0);
  }
}
