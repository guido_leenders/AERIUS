/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitDevelopmentSpacePlace;

public class PermitDetailDevelopmentSpaceActivity extends PermitDetailSummaryBaseActivity<PermitDevelopmentSpacePlace> {

  private final PermitDetailDevelopmentSpaceViewImpl contentPanel;

  @Inject
  public PermitDetailDevelopmentSpaceActivity(final RegisterAppContext appContext, final PermitDetailView view,
      final PermitDetailDevelopmentSpaceViewImpl contentPanel, @Assisted final PermitDevelopmentSpacePlace place) {
    super(appContext, view, contentPanel, place);
    this.contentPanel = contentPanel;
  }

  @Override
  protected void onPermitLoaded(final Permit permit) {
    contentPanel.setPermit(permit);
    super.onPermitLoaded(permit);
  }
}
