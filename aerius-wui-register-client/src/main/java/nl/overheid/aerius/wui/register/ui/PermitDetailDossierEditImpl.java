/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.LeafValueAwareEditor;
import nl.overheid.aerius.wui.main.ui.editor.OptGroupListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.util.FormatUtil;

@Singleton
public class PermitDetailDossierEditImpl extends Composite implements PermitDetailDossierView {
  interface PermitDetailDossierEditDriver extends SimpleBeanEditorDriver<Permit, PermitDetailDossierEditImpl> {}

  private static final PermitDetailDossierEditImplUiBinder UI_BINDER = GWT.create(PermitDetailDossierEditImplUiBinder.class);
  protected static final int TWELVE_HOUR = 12;
  protected static final int ZERO_MINUTES = 0;

  interface PermitDetailDossierEditImplUiBinder extends UiBinder<Widget, PermitDetailDossierEditImpl> {}

  @Path("dossierMetaData.dossierId") @UiField(provided = true) TextValueBox dossierIdEditor;
  @Path("dossierMetaData.handler") @UiField(provided = true) OptGroupListBoxEditor<UserProfile> handler = new OptGroupListBoxEditor<UserProfile>() {
    @Override
    protected String getLabel(final UserProfile user) {
      return M.messages().registerUser(user.getLastName(), user.getInitials());
    }
  };
  @Ignore @UiField DateBox receivedDate;
  @Path("dossierMetaData.receivedDate") LeafValueAwareEditor<Date> receivedDateEditor = new LeafValueAwareEditor<Date>() {
    @Override
    public Date getValue() {
      return receivedDate.getValue();
    }
    @Override
    public void setValue(final Date value) {
      receivedDate.setValue(value);
    }
    @Override
    public void flush() {
      if (receivedDate.getTextBox().getText().trim().isEmpty()) {
        getDelegate().recordError(M.messages().registerApplicationTabErrorNoReceivedDate(), receivedDate.getValue(), receivedDate);
      } else if (receivedDate.getValue() == null) {
        getDelegate().recordError(M.messages().registerApplicationTabErrorNoValidReceivedDate(receivedDate.getTextBox().getText()),
            receivedDate.getValue(), receivedDate);
      }
    }
  };
  @Path("dossierMetaData.remarks") @UiField TextArea remarks;
  @UiField Button saveButton;
  @UiField Button cancelButton;
  private Presenter presenter;

  @Inject
  public PermitDetailDossierEditImpl(final RegisterUserContext userContext) {
    dossierIdEditor = new TextValueBox(M.messages().registerApplicationTabAppDossierNumber()) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().registerApplicationTabErrorNoDossierNumber();
      }
    };
    initWidget(UI_BINDER.createAndBindUi(this));

    fillAuthority(userContext.getHandlers(), userContext.getUserProfile());

    dossierIdEditor.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().registerApplicationTabErrorNoDossierNumber();
      }
    });

    receivedDate.addValueChangeHandler(new ValueChangeHandler<Date>() {

      @Override
      public void onValueChange(final ValueChangeEvent<Date> event) {
        final Date date = event.getValue();

        set12OClock(date);
        receivedDate.setValue(date);

      }

      @SuppressWarnings("deprecation")
      private void set12OClock(final Date date) {
        if (date != null && date.getHours() == 0 && date.getMinutes() == 0) {
          date.setHours(TWELVE_HOUR);
          date.setMinutes(ZERO_MINUTES);
        }
      }
    });

    receivedDate.setFormat(new DateBox.DefaultFormat(FormatUtil.getDefaultDateTimeFormatter()));

    dossierIdEditor.ensureDebugId(TestIDRegister.PERMIT_DOSSIER_ID_EDITOR);
    handler.ensureDebugId(TestIDRegister.PERMIT_HANDLER_EDITOR);
    remarks.ensureDebugId(TestIDRegister.PERMIT_REMARKS_EDITOR);
    saveButton.ensureDebugId(TestIDRegister.BUTTON_SAVE_APPLICATION);
    cancelButton.ensureDebugId(TestIDRegister.BUTTON_CANCEL_EDIT_APPLICATION);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @UiHandler("saveButton")
  void onSaveClick(final ClickEvent e) {
    presenter.save();
  }

  @UiHandler("cancelButton")
  void onCancelClick(final ClickEvent e) {
    presenter.cancel();
  }

  /**
   * @param handlers
   * @param userProfile
   */
  private void fillAuthority(final LinkedHashMap<Authority, ArrayList<UserProfile>> handlers, final UserProfile userProfile) {
    // Fetch the user's authority and stick it in the list first
    final Authority userAuthority = userProfile.getAuthority();
    final ArrayList<UserProfile> userProfiles = handlers.get(userAuthority);
    fillHandlerListBox(userAuthority, userProfiles);

    // Iterate over the remainer and stick em in the list
    for (final Entry<Authority, ArrayList<UserProfile>> entry : handlers.entrySet()) {
      if (entry.getKey().getCode().equals(userAuthority.getCode())) {
        continue;
      }

      fillHandlerListBox(entry.getKey(), entry.getValue());
    }
  }

  private void fillHandlerListBox(final Authority userAuthority, final ArrayList<UserProfile> userProfiles) {
    handler.addGroup(userAuthority.getDescription());
    for (final UserProfile user : userProfiles) {
      handler.addItem(user);
    }
  }

  @Override
  public void enableDossierId(final boolean hasPermission) {
    dossierIdEditor.setEnabled(hasPermission);
  }

  @Override
  public void enableHandler(final boolean hasPermission) {
    handler.setEnabled(hasPermission);
  }

  @Override
  public void enableDate(final boolean hasPermission) {
    receivedDate.setEnabled(hasPermission);
  }

  @Override
  public void enableRemarks(final boolean hasPermission) {
    remarks.setEnabled(hasPermission);
  }
}
