/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.place.shared.Place;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.auth.AdminPermission;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.service.ContextServiceAsync;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.admin.util.AdminMenuHandler;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.ApplicationDisplay;
import nl.overheid.aerius.wui.main.ui.menu.MenuItem;
import nl.overheid.aerius.wui.main.ui.menu.SimpleMenuItem;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.place.DashboardPlace;
import nl.overheid.aerius.wui.register.place.NoticeOverviewPlace;
import nl.overheid.aerius.wui.register.place.NoticePlace;
import nl.overheid.aerius.wui.register.place.PermitOverviewPlace;
import nl.overheid.aerius.wui.register.place.PermitPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectOverviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectPlace;
import nl.overheid.aerius.wui.register.ui.search.RegisterSearchPanel;
import nl.overheid.aerius.wui.register.widget.UserProfilePanel;
import nl.overheid.aerius.wui.register.widget.UserProfilePanel.UserProfileDriver;

@Singleton
public class RegisterViewImpl extends Composite implements ApplicationDisplay {

  interface RegisterViewImplUiBinder extends UiBinder<Widget, RegisterViewImpl> {}

  private static final RegisterViewImplUiBinder UI_BINDER = GWT.create(RegisterViewImplUiBinder.class);

  private static final DashboardPlace DP = new DashboardPlace();
  private static final PermitOverviewPlace AP = new PermitOverviewPlace();
  private static final NoticeOverviewPlace NOP = new NoticeOverviewPlace();
  private static final PriorityProjectOverviewPlace PPP = new PriorityProjectOverviewPlace();

  private final SimpleBeanEditorDriver<UserProfile, UserProfilePanel> userProfileDriver = GWT.create(UserProfileDriver.class);

  @UiField SimplePanel contentPanel;
  @UiField(provided = true) RegisterSearchPanel searchPanel;
  @UiField(provided = true) UserProfilePanel userProfilePanel;

  private final RegisterUserContext userContext;
  private final SpanElement helpText = Document.get().createSpanElement();

  @Inject
  public RegisterViewImpl(final CalculatorContext context, final HelpPopupController hpC, final ContextServiceAsync service,
      final RegisterUserContext userContext, final UserProfilePanel userProfilePanel, final RegisterSearchPanel searchPanel) {
    this.userProfilePanel = userProfilePanel;
    this.userContext = userContext;
    this.searchPanel = searchPanel;

    userProfileDriver.initialize(userProfilePanel);
    userProfileDriver.edit(userContext.getUserProfile());

    initWidget(UI_BINDER.createAndBindUi(this));
    helpText.setInnerHTML(M.messages().helpOff());
  }

  @Override
  public void setWidget(final IsWidget w) {
    contentPanel.setWidget(w);
  }

  @Override
  public List<MenuItem> getMenuItems() {
    final List<MenuItem> menus = new ArrayList<>();
    final UserProfile userProfile = userContext.getUserProfile();

    if (userProfile.hasPermission(RegisterPermission.VIEW_DASHBOARD)) {
    menus.add(new SimpleMenuItem(M.messages().registerMenuDashboard(), TestIDRegister.MENU_DASHBOARD) {
      @Override
      public Place getPlace(final Place place) {
        return DP;
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof DashboardPlace;
      }
    });
    }

    if (userProfile.hasPermission(RegisterPermission.VIEW_PRIORITY_PROJECTS)) {
      menus.add(new SimpleMenuItem(M.messages().registerMenuPrioritaryProjects(), TestIDRegister.MENU_PRIORITARY_PROJECTS) {
        @Override
        public Place getPlace(final Place place) {
          return PPP;
        }

        @Override
        public boolean isActivePlace(final Place place) {
          return place instanceof PriorityProjectPlace || place instanceof PriorityProjectOverviewPlace;
        }
      });
    }

    if (userProfile.hasPermission(RegisterPermission.VIEW_PERMITS)) {
      menus.add(new SimpleMenuItem(M.messages().registerMenuApplications(), TestIDRegister.MENU_APPLICATIONS) {
        @Override
        public Place getPlace(final Place place) {
          return AP;
        }

        @Override
        public boolean isActivePlace(final Place place) {
          return place instanceof PermitPlace || place instanceof PermitOverviewPlace;
        }
      });
    }

    if (userProfile.hasPermission(RegisterPermission.VIEW_PRONOUNCEMENTS)) {
      menus.add(new SimpleMenuItem(M.messages().registerMenuMessages(), TestIDRegister.MENU_MESSAGES) {
        @Override
        public Place getPlace(final Place place) {
          return NOP;
        }

        @Override
        public boolean isActivePlace(final Place place) {
          return place instanceof NoticeOverviewPlace || place instanceof NoticePlace;
        }
      });
    }

    AdminMenuHandler.compose(menus, userProfile, AdminPermission.ADMINISTER_USERS);
    return menus;
  }

  @Override
  public DataResource getLogo() {
    return R.images().aeriusRegisterLogo();
  }

  @Override
  public void onResize() {
    // No-op
  }
}
