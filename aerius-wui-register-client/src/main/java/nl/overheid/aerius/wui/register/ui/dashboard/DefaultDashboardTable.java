/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.dashboard;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class DefaultDashboardTable extends Composite implements IsDataTable<DefaultDashboardRow> {

  interface DefaultDashboardTableUiBinder extends UiBinder<Widget, DefaultDashboardTable> {}

  private static final DefaultDashboardTableUiBinder uiBinder = GWT.create(DefaultDashboardTableUiBinder.class);

  @UiField(provided = true) SimpleDivTable<DefaultDashboardRow> table;
  @UiField(provided = true) SimpleWidgetFactory<DefaultDashboardRow> statusColumn;
  @UiField(provided = true) TextColumn<DefaultDashboardRow> numberColumn;

  public DefaultDashboardTable() {
    table = new SimpleDivTable<>(true);

    numberColumn = new TextColumn<DefaultDashboardRow>() {
      @Override
      public String getValue(final DefaultDashboardRow object) {
        final double pct = (object.getAssignedFactor() + object.getPendingFactor()) * SharedConstants.PERCENTAGE_TO_FRACTION;

        return FormatUtil.toWhole(Math.min(pct, SharedConstants.PERCENTAGE_TO_FRACTION));
      }
    };

    statusColumn = new SimpleWidgetFactory<DefaultDashboardRow>() {
      @Override
      public Widget createWidget(final DefaultDashboardRow object) {
        return new DashboardBarChart(object, M.messages().registerPermitDashboardStatusConfirmed());
      }
    };

    initWidget(uiBinder.createAndBindUi(this));
  }

  @Override
  public SimpleDivTable<DefaultDashboardRow> asDataTable() {
    return table;
  }
}
