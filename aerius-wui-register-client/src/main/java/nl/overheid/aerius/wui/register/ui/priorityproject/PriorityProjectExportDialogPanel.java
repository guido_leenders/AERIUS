/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData.PriorityProjectExportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.CheckBoxSelectionGroup;
import nl.overheid.aerius.wui.main.widget.CheckBoxSelectionItem.Messenger;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.widget.ExportRequests.ExportRequestsHandler;

/**
 * Export Dialog content panel with export options.
 */
class PriorityProjectExportDialogPanel extends Composite implements Editor<PriorityProjectExportData> {

  interface Presenter extends ExportRequestsHandler {
    void enableConfirmButton(boolean enable);
  }

  interface PriorityProjectExportDialogDriver extends SimpleBeanEditorDriver<PriorityProjectExportData, PriorityProjectExportDialogPanel> { }

  private final static PriorityProjectExportDialogPanelUiBinder UI_BINDER = GWT.create(PriorityProjectExportDialogPanelUiBinder.class);

  interface PriorityProjectExportDialogPanelUiBinder extends UiBinder<Widget, PriorityProjectExportDialogPanel> { }

  @UiField(provided = true) CheckBoxSelectionGroup<PriorityProjectExportType> exportTypes;

  private Presenter presenter;

  public PriorityProjectExportDialogPanel(final HelpPopupController hpC) {
    exportTypes = new CheckBoxSelectionGroup<PriorityProjectExportType>(PriorityProjectExportType.values(),
        new Messenger<PriorityProjectExportType>() {
      @Override
      public String getOptionText(final PriorityProjectExportType option) {
        return M.messages().registerPriorityProjectExportTypes(option);
      }
    }, TestID.EXPORT_FILE_TYPES);
    exportTypes.addValueChangeHandler(new ValueChangeHandler<ArrayList<PriorityProjectExportType>>() {

      @Override
      public void onValueChange(final ValueChangeEvent<ArrayList<PriorityProjectExportType>> event) {
        presenter.enableConfirmButton(!event.getValue().isEmpty());
      }
    });

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }
}
