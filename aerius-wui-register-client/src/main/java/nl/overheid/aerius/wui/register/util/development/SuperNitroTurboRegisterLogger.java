/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.util.development;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.util.development.SuperNitroTurboGeoLogger;
import nl.overheid.aerius.wui.register.event.PermitCalculationFinishedEvent;
import nl.overheid.aerius.wui.register.event.PermitRetrievedEvent;

public class SuperNitroTurboRegisterLogger extends SuperNitroTurboGeoLogger {
  interface SuperTurboRegisterBinder extends EventBinder<SuperNitroTurboRegisterJuggernaut> {}

  private final SuperTurboRegisterBinder playground;

  public static class SuperNitroTurboRegisterJuggernaut extends SuperNitroTurboGeoJuggernaut {
    @EventHandler
    void onPermitRetrieved(final PermitRetrievedEvent e) {
      logger.log("Application retrieved: " + e.getValue().getPermitKey());
    }

    @EventHandler
    void onPermitCalculationFinishedEvent(final PermitCalculationFinishedEvent e) {
      logger.log("Calculation finished: " + e.getValue().getDossierId());
    }
  }

  @Inject
  public SuperNitroTurboRegisterLogger(final EventBus eventBus, final SuperNitroTurboRegisterJuggernaut juggernaut) {
    super(eventBus, juggernaut);
    playground = GWT.create(SuperTurboRegisterBinder.class);
    playground.bindEventHandlers(juggernaut, eventBus);
    log("SuperNitroTurboRegisterJuggernaut attached.");
  }
}
