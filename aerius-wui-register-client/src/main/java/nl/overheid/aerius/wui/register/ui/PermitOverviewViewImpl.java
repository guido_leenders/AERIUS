/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.ui.filter.PermitFilterPanel;
import nl.overheid.aerius.wui.register.widget.ExportRequests;
import nl.overheid.aerius.wui.register.widget.PermitDataTable;

public class PermitOverviewViewImpl extends Composite implements PermitOverviewView {
  interface PermitOverviewViewImplUiBinder extends UiBinder<Widget, PermitOverviewViewImpl> {}

  private static final PermitOverviewViewImplUiBinder UI_BINDER = GWT.create(PermitOverviewViewImplUiBinder.class);

  @UiField Button newApplicationButton;
  @UiField Button refreshButton;
  @UiField Button exportButton;

  @UiField PermitDataTable permitDataTable;
  @UiField(provided = true) PermitFilterPanel filterPanel;

  private Presenter presenter;
  private final ExportRequests exportRequests;

  private final ValueChangeHandler<PermitFilter> changeHandler = new ValueChangeHandler<PermitFilter>() {
    @Override
    public void onValueChange(final ValueChangeEvent<PermitFilter> event) {
      presenter.onAdaptFilter();
    }
  };

  @Inject
  public PermitOverviewViewImpl(final RegisterUserContext userContext, final HelpPopupController hpC, final PermitFilterPanel filterPanel) {
    this.filterPanel = filterPanel;
    this.filterPanel.addValueChangeHandler(changeHandler);
    this.filterPanel.setValue(userContext.getFilter(PermitFilter.class));

    exportRequests = new ExportRequests(true);

    initWidget(UI_BINDER.createAndBindUi(this));

    exportButton.setVisible(userContext.getUserProfile().hasPermission(RegisterPermission.EXPORT_REQUESTS));
    newApplicationButton.setVisible(userContext.getUserProfile().hasPermission(RegisterPermission.ADD_NEW_PERMIT));

    hpC.addWidget(refreshButton, hpC.tt().ttRegisterRefresh());
    hpC.addWidget(exportButton, hpC.tt().ttRegisterExportPermits());

    permitDataTable.ensureDebugId(TestIDRegister.CELLTABLE_APPLICATIONS);
    refreshButton.ensureDebugId(TestIDRegister.PERMIT_REFRESH_BUTTON);
    exportButton.ensureDebugId(TestIDRegister.PERMIT_EXPORT_BUTTON);
    newApplicationButton.ensureDebugId(TestIDRegister.BUTTON_NEW_APPLICATION);

    permitDataTable.asDataTable().addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        presenter.onPermitSelection(((SingleSelectionModel<Permit>) permitDataTable.asDataTable().getSelectionModel()).getSelectedObject());
      }
    });
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;

    permitDataTable.setSortHandler(presenter);
    exportRequests.setHandler(presenter);
  }

  @UiHandler("newApplicationButton")
  void onNewApplication(final ClickEvent event) {
    presenter.onNewApplication();
  }

  @UiHandler("refreshButton")
  void onRefresh(final ClickEvent event) {
    presenter.onRefresh();
  }

  @UiHandler("exportButton")
  void onPermitExportButton(final ClickEvent event) {
    exportRequests.onExportButtonClick();
  }

  @Override
  public void setDataVisitor(final GridScrollVisitor gridScrollVisitor) {
    permitDataTable.asDataTable().exposeToVisitor(gridScrollVisitor);
  }

  @Override
  public void updateData(final ArrayList<Permit> result) {
    permitDataTable.asDataTable().addRowData(result);
  }

  @Override
  public void setData(final ArrayList<Permit> result) {
    permitDataTable.asDataTable().setRowData(result, true);
  }

  @Override
  public void setLoading(final boolean loading, final boolean hard) {
    if (hard && loading) {
      permitDataTable.asDataTable().clear();
    }

    permitDataTable.asDataTable().showLoadingWidget(loading);
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuApplicationsTitle();
  }

  @Override
  public void updateExportResult(final RequestExportResult requestExportResult) {
    exportRequests.updateExportResult(requestExportResult);
  }
}
