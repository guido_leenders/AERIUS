/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.importer;

import java.util.HashMap;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.importer.ImportDialogController;
import nl.overheid.aerius.wui.main.ui.importer.ImportProperties;
import nl.overheid.aerius.wui.main.util.SecurityUtil;
import nl.overheid.aerius.wui.main.widget.event.CancelEvent;
import nl.overheid.aerius.wui.register.ui.importer.RegisterImportDialogPanel.RequestHandler;

public abstract class RegisterImportDialogController<K> extends
  ImportDialogController<ImportProperties, RegisterImportDialogPanel<K>, K> implements RequestHandler<K>, AsyncCallback<K> {

  protected final RegisterImportDialogPanel<K> content;
  private final PlaceController placeController;
  private final RequestImportPollingAgent<K> pollingAgent;
  private Place openDuplicatePlace;

  public RegisterImportDialogController(final PlaceController placeController, final RegisterImportDialogPanel<K> content,
      final String uploadAction, final RequestImportPollingAgent<K> pollingAgent) {
    this(placeController, content, uploadAction, pollingAgent, null);
  }

  public RegisterImportDialogController(final PlaceController placeController, final RegisterImportDialogPanel<K> content,
      final String uploadAction, final RequestImportPollingAgent<K> pollingAgent, final HashMap<String, String> additionalFormParams) {
    super(content, uploadAction, additionalFormParams);
    this.placeController = placeController;
    this.content = content;
    this.pollingAgent = pollingAgent;

    content.setRequestHandler(this);
  }

  @Override
  public void getImport(final String uuid) {
    pollingAgent.start(uuid, this);
  }

  @Override
  public void onFailure(final Throwable error) {
    setInProgress(false);
    content.showDuplicate(null, false);

    SecurityUtil.reloadOnAuthenticationExpired(error);

    if (error instanceof AeriusException) {
      handleError((AeriusException) error);
    }
  }

  @Override
  public void onSuccess(final K result) {
    pollingAgent.stop();
    handleResult(result);
  }

  @Override
  public void onCancel(final CancelEvent event) {
    pollingAgent.stop();
    super.onCancel(event);
  }

  @Override
  public void handleDuplicate(final ImportDuplicateEntryException duplicateException) {
    // Show default duplicate message if not specified
    content.showError(M.getErrorMessage(duplicateException));
  }

  public void setDuplicateInfo(final String error, final Place openDuplicatePlace) {
    this.openDuplicatePlace = openDuplicatePlace;
    content.showDuplicate(error, openDuplicatePlace != null);
  }

  @Override
  public void openDuplicate() {
    if (openDuplicatePlace != null) {
      placeController.goTo(openDuplicatePlace);
      hide();
    }
  }

  private void handleError(final AeriusException error) {
    if (error != null && error instanceof ImportDuplicateEntryException) {
      // allow specific implementations to handle the duplicate entry, a default implementation is provided though
      handleDuplicate((ImportDuplicateEntryException) error);
    } else if (error != null && error.getReason() == Reason.IMPORT_REQUIRED_ID_MISSING) {
      content.showIdMissing(error);
    } else {
      content.showError(error == null ? null : M.getErrorMessage(error));
    }
  }

  @Override
  protected final void submitClick() {
    // No-op *
  }

}
