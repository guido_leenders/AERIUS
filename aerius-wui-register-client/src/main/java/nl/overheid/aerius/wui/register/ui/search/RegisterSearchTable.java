/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.search;

import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestionType;
import nl.overheid.aerius.wui.main.ui.search.SearchAction;
import nl.overheid.aerius.wui.main.ui.search.SearchSuggestionTable;
import nl.overheid.aerius.wui.main.ui.search.SearchTable;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class RegisterSearchTable extends SearchTable<RegisterSearchSuggestionType, RegisterSearchSuggestion> {
  @Inject
  public RegisterSearchTable(final HelpPopupController hpC, final RegisterSearchAction searchAction) {
    super(hpC, searchAction);
  }

  @Override
  protected SearchSuggestionTable<RegisterSearchSuggestionType, RegisterSearchSuggestion> createSuggestionTable(final HelpPopupController hpC, final SearchAction<RegisterSearchSuggestionType, RegisterSearchSuggestion> searchAction) {
    return new RegisterSearchSuggestionTable(hpC, searchAction);
  }
}
