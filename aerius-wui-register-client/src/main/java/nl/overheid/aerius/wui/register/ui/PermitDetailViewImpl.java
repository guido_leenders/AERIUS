/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.place.PermitPlace;
import nl.overheid.aerius.wui.register.ui.permit.PermitStatusButton;
import nl.overheid.aerius.wui.register.ui.permit.PermitTabPanel;

/**
 * The main permit panel containing the header panel and its content.
 */
@Singleton
public class PermitDetailViewImpl extends Composite implements PermitDetailView {

  interface PermitDetailViewImplUiBinder extends UiBinder<Widget, PermitDetailViewImpl> {}

  private static final PermitDetailViewImplUiBinder UI_BINDER = GWT.create(PermitDetailViewImplUiBinder.class);

  interface CustomStyle extends CssResource {
    String lock();
  }

  @UiField CustomStyle style;

  // Header panels
  @UiField Label titleLabel;
  @UiField(provided = true) PermitStatusButton statusButton;
  @UiField(provided = true) PermitTabPanel tabPanel;

  @UiField SimplePanel contentHolderPanel;

  private HandlerRegistration disregardInputRegistration;

  private final ClickHandler disregardInputHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      event.stopPropagation();
      event.preventDefault();
    }
  };

  @Inject
  public PermitDetailViewImpl(final PermitStatusButton statusButton, final PermitTabPanel tabPanel) {
    this.statusButton = statusButton;
    this.tabPanel = tabPanel;
    initWidget(UI_BINDER.createAndBindUi(this));

    tabPanel.ensureDebugId(TestIDRegister.APPLICATION_TAB_PANEL);
  }

  @Override
  public void setContentPanel(final IsWidget contentPanel) {
    contentHolderPanel.setWidget(contentPanel);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    statusButton.setPermitStatusHandler(presenter);
  }

  @Override
  public void setPermit(final Permit value) {
    titleLabel.setText(value.getDossierMetaData().getDossierId());
    statusButton.setValue(value.getRequestState());
    statusButton.setMarked(value.isMarked());
  }

  @Override
  public void setPlace(final PermitPlace place) {
    tabPanel.setTabFocus(place);
  }

  @Override
  public void setLocked(final boolean lock) {
    contentHolderPanel.setStyleName(style.lock(), lock);

    if (disregardInputRegistration != null) {
      disregardInputRegistration.removeHandler();
    }

    if (lock) {
      disregardInputRegistration = contentHolderPanel.addDomHandler(disregardInputHandler, ClickEvent.getType());
    }

    tabPanel.setLocked(lock);
  }

  @Override
  public void enableStatus(final boolean enabled) {
    statusButton.enableStatus(enabled);
  }

  @Override
  public void enableDelete(final boolean enabled) {
    statusButton.enableDelete(enabled);
  }

  @Override
  public void enableMark(final boolean enabled) {
    statusButton.enableMark(enabled);
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuApplicationsTitle();
  }

}
