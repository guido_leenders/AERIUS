/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.search;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestionType;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.ui.search.SearchAction;
import nl.overheid.aerius.wui.main.ui.search.SearchSuggestionTable;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class RegisterSearchSuggestionTable extends SearchSuggestionTable<RegisterSearchSuggestionType, RegisterSearchSuggestion> {
  interface RegisterSearchSuggestionTableUiBinder extends UiBinder<Widget, RegisterSearchSuggestionTable> {}

  private static final RegisterSearchSuggestionTableUiBinder UI_BINDER = GWT.create(RegisterSearchSuggestionTableUiBinder.class);

  @UiField(provided = true) ImageColumn<RegisterSearchSuggestion> iconColumn;
  @UiField(provided = true) TextColumn<RegisterSearchSuggestion> dateColumn;

  public RegisterSearchSuggestionTable(final HelpPopupController hpC, final SearchAction<RegisterSearchSuggestionType, RegisterSearchSuggestion> searchAction) {
    super(hpC, searchAction);

    iconColumn = new ImageColumn<RegisterSearchSuggestion>() {
      @Override
      public ImageResource getValue(final RegisterSearchSuggestion request) {
        return SectorImageUtil.getSectorImageResource(request.getRequest().getSectorIcon());
      }
    };
    dateColumn = new TextColumn<RegisterSearchSuggestion>() {
      @Override
      public String getValue(final RegisterSearchSuggestion suggestion) {
        final Request request = suggestion.getRequest();

        Date receivedDate = null;
        if (request instanceof Permit) {
          receivedDate = ((Permit) request).getDossierMetaData().getReceivedDate();
        } else if (request instanceof Notice) {
          receivedDate = ((Notice) request).getReceivedDate();
        } else if (request instanceof PriorityProject) {
          receivedDate = ((PriorityProject) request).getDossierMetaData().getReceivedDate();
        } else if (request instanceof PrioritySubProject) {
          receivedDate = ((PrioritySubProject) request).getReceivedDate();
        }

        return FormatUtil.getDefaultCompactDateFormatter().format(receivedDate);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  protected SearchSuggestionTable<RegisterSearchSuggestionType, RegisterSearchSuggestion> createSuggestionTable(final HelpPopupController hpC, final SearchAction<RegisterSearchSuggestionType, RegisterSearchSuggestion> searchAction) {
    return new RegisterSearchSuggestionTable(hpC, searchAction);
  }

  @Override
  protected String getName(final RegisterSearchSuggestion object) {
    return object.getName();
  }
}
