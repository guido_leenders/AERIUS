/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.event;

import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent;
import nl.overheid.aerius.wui.register.geo.NoticeMarker.NoticeMarkerType;

/**
 * Event when the user selects a Notice.
 */
public class NoticeSelectedEvent extends SimpleGenericEvent<Notice> {
  private NoticeMarkerType noticeMarkerType = NoticeMarkerType.NORMAL;

  public NoticeSelectedEvent(final Notice notice, final NoticeMarkerType noticeMarkerType) {
    super(notice);
    this.noticeMarkerType = noticeMarkerType;
  }

  public NoticeMarkerType getNoticeMarkerType() {
    return noticeMarkerType;
  }
}
