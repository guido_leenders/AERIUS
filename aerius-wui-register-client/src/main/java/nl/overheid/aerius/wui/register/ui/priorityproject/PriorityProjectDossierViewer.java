/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.wui.main.i18n.M;

public class PriorityProjectDossierViewer extends Composite implements Editor<PriorityProject> {
  interface PriorityProjectDossierViewerUiBinder extends UiBinder<Widget, PriorityProjectDossierViewer> {}

  private static final PriorityProjectDossierViewerUiBinder UI_BINDER = GWT.create(PriorityProjectDossierViewerUiBinder.class);

  @Path("scenarioMetaData.projectName") @UiField Label name;
  @Path("dossierMetaData.dossierId") @UiField Label dossierId;
  @Path("authority.description") @UiField Label authority;
  @Ignore @UiField Label remarks;
  @Path("dossierMetaData.remarks") Editor<String> remarksEditor = new FakeLeafValueEditor<String>() {
    @Override
    public void setValue(final String value) {
      super.setValue(value);

      if (value == null) {
        remarks.setText(M.messages().registerApplicationTabNoRemarks());
        return;
      }

      // Replace new lines with html breaks (UI only)
      remarks.getElement().setInnerSafeHtml(new SafeHtmlBuilder().appendEscapedLines(value).toSafeHtml());
    }
  };

  public PriorityProjectDossierViewer() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }
}
