/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Export Dialog content panel with export options.
 */
public class NoticeDeleteDialogPanel extends Composite implements Editor<NoticeDeleteData> {

  interface Presenter {
    void enableConfirmButton(boolean enable);

    void validateInput(String reference);
  }

  interface NoticeDeleteDialogPanelDriver extends SimpleBeanEditorDriver<NoticeDeleteData, NoticeDeleteDialogPanel> { }

  private final static NoticeDeleteDialogPanelUiBinder UI_BINDER = GWT.create(NoticeDeleteDialogPanelUiBinder.class);

  interface NoticeDeleteDialogPanelUiBinder extends UiBinder<Widget, NoticeDeleteDialogPanel> { }

  @UiField(provided = true) TextValueBox dossierIdEditor;

  private Presenter presenter;

  public NoticeDeleteDialogPanel(final HelpPopupController hpC) {
    dossierIdEditor = new TextValueBox(M.messages().registerApplicationTabAppDossierNumber());
    initWidget(UI_BINDER.createAndBindUi(this));

    dossierIdEditor.addKeyUpHandler(new KeyUpHandler() {

      @Override
      public void onKeyUp(final KeyUpEvent event) {
        presenter.validateInput(dossierIdEditor.getText());
      }
    });
  }

  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }
}
