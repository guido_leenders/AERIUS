/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.util;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.wui.main.resources.R;

public class PermitStateUtil {

  public static DataResource getPermitStateIcon(final RequestState state) {
    return getPermitStateIcon(state, false);
  }

  public static DataResource getPermitStateIcon(final RequestState state, final boolean potentiallyRejectable) {
    final DataResource resource;

    switch (state) {
    case INITIAL:
      resource = R.images().applicationstateNoReservationPresent();
      break;
    case QUEUED:
      resource = R.images().applicationstateQueued();
      break;
    case PENDING_WITH_SPACE:
      resource = R.images().applicationstatePendingWithSpace();
      break;
    case PENDING_WITHOUT_SPACE:
      resource =
        potentiallyRejectable
          ? R.images().applicationstatePendingWithoutSpacePotentiallyRejectable()
          : R.images().applicationstatePendingWithoutSpace();
      break;
    case ASSIGNED:
      resource = R.images().applicationstateAssigned();
      break;
    case REJECTED_WITHOUT_SPACE:
      resource = R.images().applicationstateRejectedWithoutSpace();
      break;
    case ASSIGNED_FINAL:
      resource = R.images().applicationstateAssignedFinal();
      break;
    case REJECTED_FINAL:
      resource = R.images().applicationstateRejectedFinal();
      break;
    default:
      resource = null;
      break;
    }

    return resource;
  }

  public static DataResource getPermitStateIconInactive(final RequestState state) {
    final DataResource resource;
    switch (state) {
    case QUEUED:
      resource = R.images().applicationstateQueuedInactive();
      break;
    case PENDING_WITH_SPACE:
      resource = R.images().applicationstatePendingWithSpaceInactive();
      break;
    case PENDING_WITHOUT_SPACE:
      resource = R.images().applicationstatePendingWithoutSpaceInactive();
      break;
    case ASSIGNED:
      resource = R.images().applicationstateAssignedInactive();
      break;
    case REJECTED_WITHOUT_SPACE:
      resource = R.images().applicationstateRejectedWithoutSpaceInactive();
      break;
    case ASSIGNED_FINAL:
      resource = R.images().applicationstateAssignedFinalInactive();
      break;
    case REJECTED_FINAL:
      resource = R.images().applicationstateRejectedFinalInactive();
      break;
    default:
      resource = null;
      break;
    }
    return resource;
  }

}
