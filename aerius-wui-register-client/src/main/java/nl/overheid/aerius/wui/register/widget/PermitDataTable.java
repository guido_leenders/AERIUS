/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitSortableAttribute;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader.SortActionHandler;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeaderBinderUtil;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyProviders;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.ScalableImageColumn;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;
import nl.overheid.aerius.wui.register.util.PermitStateUtil;
import nl.overheid.aerius.wui.register.util.RequestMarkedUtil;

/**
 * Table to display Applications.
 */
public class PermitDataTable extends Composite implements IsInteractiveDataTable<Permit>, SortActionHandler<PermitSortableAttribute> {
  interface PermitDataTableUiBinder extends UiBinder<Widget, PermitDataTable> {}

  private static final PermitDataTableUiBinder UI_BINDER = GWT.create(PermitDataTableUiBinder.class);

  private static final DateTimeFormat DATEFORMAT = DateTimeFormat.getFormat("dd-MM-yyyy");
  private static final int STATUS_COLUMN_IMAGE_WIDTH = 31;
  private static final int MARKED_COLUMN_IMAGE_WIDTH = 31;

  @UiField SimpleInteractiveClickDivTable<Permit> table;

  @UiField ColumnSortHeader<PermitSortableAttribute> headerSector;
  @UiField ColumnSortHeader<PermitSortableAttribute> headerStatus;
  @UiField ColumnSortHeader<PermitSortableAttribute> headerDate;
  @UiField ColumnSortHeader<PermitSortableAttribute> headerMarked;
  @UiField ColumnSortHeader<PermitSortableAttribute> headerApplicant;
  @UiField ColumnSortHeader<PermitSortableAttribute> headerCaseNumber;
  @UiField ColumnSortHeader<PermitSortableAttribute> headerAuthority;

  @UiField(provided = true) ImageColumn<Permit> iconColumn;
  @UiField(provided = true) ScalableImageColumn<Permit> statusColumn;
  @UiField(provided = true) TextColumn<Permit> dateColumn;
  @UiField(provided = true) ScalableImageColumn<Permit> markedColumn;
  @UiField(provided = true) TextColumn<Permit> applicantColumn;
  @UiField(provided = true) TextColumn<Permit> dossierColumn;
  @UiField(provided = true) TextColumn<Permit> authorityColumn;

  private SortActionHandler<PermitSortableAttribute> handler;

  public PermitDataTable() {
    iconColumn = new ImageColumn<Permit>() {
      @Override
      public ImageResource getValue(final Permit object) {
        return SectorImageUtil.getSectorImageResource(object.getSectorIcon());
      }
    };

    statusColumn = new ScalableImageColumn<Permit>(STATUS_COLUMN_IMAGE_WIDTH) {
      @Override
      public DataResource getValue(final Permit object) {
        return PermitStateUtil.getPermitStateIcon(object.getRequestState(), object.isPotentiallyRejectable());
      }
    };

    dateColumn = new TextColumn<Permit>() {
      @Override
      public String getValue(final Permit value) {
        return DATEFORMAT.format(value.getDossierMetaData().getReceivedDate());
      }
    };

    markedColumn = new ScalableImageColumn<Permit>(MARKED_COLUMN_IMAGE_WIDTH) {
      @Override
      public DataResource getValue(final Permit object) {
        markedColumn.ensureDebugId(object.getPermitKey().getDossierId() + "-" + (object.isMarked() ? "" : "un") + "marked");
        return RequestMarkedUtil.getRequestMarkedIcon(object.isMarked());
      }
    };

    applicantColumn = new TextColumn<Permit>() {
      @Override
      public String getValue(final Permit value) {
        // TODO Move to messages
        return value.getScenarioMetaData().getProjectName() + ", " + value.getScenarioMetaData().getCorporation();
      }
    };

    dossierColumn = new TextColumn<Permit>() {
      @Override
      public String getValue(final Permit value) {
        return value.getDossierMetaData().getDossierId();
      }
    };

    authorityColumn = new TextColumn<Permit>() {
      @Override
      public String getValue(final Permit value) {
        return value.getAuthority().getDescription();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    headerSector.setHandler(this);
    headerStatus.setHandler(this);
    headerDate.setHandler(this);
    headerMarked.setHandler(this);
    headerApplicant.setHandler(this);
    headerCaseNumber.setHandler(this);
    headerAuthority.setHandler(this);

    ColumnSortHeaderBinderUtil.connect(headerSector, headerStatus, headerDate, headerMarked, headerApplicant, headerCaseNumber, headerAuthority);

    DivTableKeyProviders.autoConfigure(Permit.class, table);

    table.setSelectionModel(new SingleSelectionModel<Permit>());
    table.setLoadingByDefault(true);
  }

  @Override
  public SimpleInteractiveClickDivTable<Permit> asDataTable() {
    return table;
  }

  @Override
  public void setSorting(final PermitSortableAttribute attribute, final SortableDirection dir) {
    if (handler != null) {
      handler.setSorting(attribute, dir);
    }
  }

  public void setSortHandler(final SortActionHandler<PermitSortableAttribute> handler) {
    this.handler = handler;
  }
}
