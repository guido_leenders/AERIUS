/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectSubPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.PriorityProjectViewType;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierBaseView.Presenter;
import nl.overheid.aerius.wui.register.ui.PriorityProjectProjectDossierBaseView.PriorityProjectDossierDetailViewType;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public abstract class PriorityProjectProjectDossierBaseActivity<P extends PriorityProjectSubPlace> extends PriorityProjectBaseActivity<P> implements
    Presenter {

  private final PriorityProjectProjectDossierBaseView subBaseView;

  private final PriorityProjectDossierDetailViewType subViewType;

  protected PrioritySubProject prioritySubProject;

  private final AsyncCallback<Void> savePriorityProjectProjectDossierStateCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      broadcastNotificationMessage(M.messages().registerNotificationApplicationStateChangeDone(prioritySubProject.getReference()));
      reloadCurrentPriorityProject();
    }

    @Override
    public void onFailure(final Throwable caught) {
      setLocked(false);
      PriorityProjectProjectDossierBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  private final AsyncCallback<Void> deleteSubProjectCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      broadcastNotificationMessage(M.messages().registeraNotificationApplicationPrioritySubProjectDeleted(prioritySubProject.getReference()));
      prioritySubProject = null;
      reloadCurrentPriorityProject();
    }

    @Override
    public void onFailure(final Throwable caught) {
      PriorityProjectProjectDossierBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  @Inject
  public PriorityProjectProjectDossierBaseActivity(final P currentPlace, final RegisterAppContext appContext, final PriorityProjectBaseView baseView,
      final PriorityProjectProjectDossierBaseView subBaseView, final PriorityProjectDossierDetailViewType subViewType,
      final PriorityProjectExportDialogController exportDialogController) {
    super(currentPlace, appContext, baseView, PriorityProjectViewType.APPLICATION, exportDialogController);
    this.subBaseView = subBaseView;
    this.subViewType = subViewType;
  }

  @Override
  public boolean onChangeState(final RequestState newState) {
    boolean changed;
    if (newState == RequestState.REJECTED_FINAL && !Window.confirm(M.messages().registerApplicationDeleteTitle())) {
      changed = false;
    } else {
      changed = true;
      if (newState == RequestState.REJECTED_FINAL) {
        rejectPrioritySubProject();

      } else {
        saveApplicationState(newState);
      }
    }
    return changed;
  }

  private void rejectPrioritySubProject() {
    setLocked(true);
    broadcastNotificationMessage(M.messages().registerNotificationApplicationStateBeingChanged(prioritySubProject.getReference()));
    appContext.getPriorityProjectService().rejectPrioritySubProject(new PrioritySubProjectKey(
        currentPlace.getKey(), prioritySubProject.getReference()), prioritySubProject.getLastModified(),
        deleteSubProjectCallback);
  }

  private void saveApplicationState(final RequestState requestState) {
    setLocked(true);
    broadcastNotificationMessage(M.messages().registerNotificationApplicationStateBeingChanged(prioritySubProject.getReference()));
    appContext.getPriorityProjectService().changeStatePrioritySubProject(new PrioritySubProjectKey(
        currentPlace.getKey(), prioritySubProject.getReference()), requestState, prioritySubProject.getLastModified(),
        savePriorityProjectProjectDossierStateCallback);
  }

  @Override
  protected void reloadCurrentPriorityProject() {
    appContext.invalidatePriorityProject();
    if (prioritySubProject == null) {
      goTo(new PriorityProjectProjectPlace(priorityProject.getKey()));
    } else {
      appContext.getPriorityProjectForceReload(priorityProject.getKey(), this);
      // Back to project overview
      goTo(new PriorityProjectProjectPlace(priorityProject.getKey()));
    }
  }

  @Override
  protected void setLocked(final boolean lock) {
    super.setLocked(lock);
    subBaseView.setLocked(lock);
    updatePermissionStates(lock);
  }

  private void updatePermissionStates(final boolean lock) {
    final boolean permissionDelete = prioritySubProject != null
        && (hasPermissionForAuthority(RegisterPermission.DELETE_PRIORITY_PROJECT) || hasPermissionForAuthority(RegisterPermission.DELETE_PRIORITY_SUBPROJECT));

    subBaseView.enableDelete(!lock && permissionDelete);
    subBaseView.enableStatus(!lock && hasPermissionForAuthority(RegisterPermission.UPDATE_PRIORITY_SUBPROJECT_STATE));
  }

  private boolean hasPermissionForAuthority(final RegisterPermission permission) {
    return UserProfileUtil.hasPermissionForAuthority(appContext.getUserContext().getUserProfile(),
        prioritySubProject == null ? null : prioritySubProject.getAuthority(), permission);
  }

  @Override
  public void onPrioritySubProjectLoaded(final PrioritySubProject prioritySubProject) {
    this.prioritySubProject = prioritySubProject;
    subBaseView.setPrioritySubProject(prioritySubProject);
    setLocked(false);
  }

  @Override
  protected void startChild(final AcceptsOneWidget panel) {
    subBaseView.setPresenter(this);
    panel.setWidget(subBaseView);
    startSubChild(subBaseView);
  }

  @Override
  public void deletePermit() {
    if (!Window.confirm(M.messages().registerapplicationDeletePrioritySubProjectTitle())) {
      return;
    }
    setLocked(true);
    appContext.getPriorityProjectService().deletePrioritySubProject(
        new PrioritySubProjectKey(currentPlace.getKey(), prioritySubProject.getReference()), deleteSubProjectCallback);
  }

  protected abstract void startSubChild(AcceptsOneWidget panel);

  protected PrioritySubProject determinePrioritySubProject(final PriorityProject priorityProject) {
    for (final PrioritySubProject sub : priorityProject.getSubProjects()) {
      if (sub.getReference().equals(currentPlace.getSubKey().getReference())) {
        return sub;
      }
    }
    return null;
  }
}
