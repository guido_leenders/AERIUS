/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.Arrays;

import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitSummary;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitOverviewPlace;
import nl.overheid.aerius.wui.register.place.PermitPlace;

/**
 * Base class for activities using {@link PermitSummary}.
 */
abstract class PermitDetailSummaryBaseActivity<P extends PermitPlace> extends PermitDetailBaseActivity {

  private final PermitDetailSummaryView contentPanel;
  private final P place;

  private final AsyncCallback<PermitSummary> summaryCallback = new AppAsyncCallback<PermitSummary>() {
    @Override
    public void onSuccess(final PermitSummary result) {
      loadPermitSummary(result);
    }

    @Override
    public void onFailure(final Throwable caught) {
      super.onFailure(caught);
      if (caught instanceof AeriusException) {
        final AeriusException ae = (AeriusException) caught;

        if (Reason.PERMIT_UNKNOWN.equals(ae.getReason())) {
          goTo(new PermitOverviewPlace());
        }
      }
    }
  };

  public PermitDetailSummaryBaseActivity(final RegisterAppContext appContext, final PermitDetailView view, final PermitDetailSummaryView contentPanel,
      final P place) {
    super(appContext, view, contentPanel, place);
    this.contentPanel = contentPanel;
    this.place = place;
  }

  @Override
  protected void onPermitLoaded(final Permit permit) {
    if (Arrays.asList(RequestState.PERMIT_SUMMARY_VALUES).contains(permit.getRequestState())) {
      appContext.getPermitSummary(place.getKey(), summaryCallback);
    } else {
      contentPanel.setSummaryAvailable(false);
    }
  }

  private void loadPermitSummary(final PermitSummary result) {
    contentPanel.setSummary(result);
  }
}