/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.charts;

import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.user.client.ui.Label;

public class TextBarChart extends BarChart<BarChart.Bar> {
  private final String label;

  public TextBarChart(final String label) {
    this.label = label;

    panel.getElement().getStyle().setPosition(Position.RELATIVE);
  }

  @Override
  public void draw() {
    super.draw();

    final Label labelField = new Label(label);

    panel.insert(labelField, 0);
  }

  @Override
  public BarChart.Bar createBar(final BarChart.Bar bar) {
    return bar;
  }
}
