/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.register.Permit;

/**
 * Interface for showing view or edit mode of permit dossier information.
 */
public interface PermitDetailDossierView extends Editor<Permit>, IsWidget {

  interface Presenter {
    void save();
    void cancel();
    void edit();
    void downloadDecree(String decreeUrl);
  }

  void setPresenter(Presenter presenter);

  void enableDossierId(final boolean hasPermission);

  void enableHandler(final boolean hasPermission);

  void enableDate(final boolean hasPermission);

  void enableRemarks(final boolean hasPermission);
}
