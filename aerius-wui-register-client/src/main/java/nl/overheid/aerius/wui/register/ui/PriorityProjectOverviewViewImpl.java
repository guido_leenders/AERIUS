/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.ui.filter.PriorityProjectFilterPanel;
import nl.overheid.aerius.wui.register.widget.PriorityProjectDataTable;

public class PriorityProjectOverviewViewImpl extends Composite implements PriorityProjectOverviewView {
  interface PriorityProjectsOverviewViewImplUiBinder extends UiBinder<Widget, PriorityProjectOverviewViewImpl> {}

  private static final PriorityProjectsOverviewViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectsOverviewViewImplUiBinder.class);

  @UiField(provided = true) PriorityProjectFilterPanel filterPanel;
  @UiField PriorityProjectDataTable projectDataTable;
  @UiField Button newPrioritaryProjectButton;
  @UiField Button newActualisationButton;

  private final ValueChangeHandler<PriorityProjectFilter> changeHandler = new ValueChangeHandler<PriorityProjectFilter>() {
    @Override
    public void onValueChange(final ValueChangeEvent<PriorityProjectFilter> event) {
      presenter.onAdaptFilter();
    }
  };

  private Presenter presenter;

  @Inject
  public PriorityProjectOverviewViewImpl(final RegisterUserContext userContext, final PriorityProjectFilterPanel filterPanel) {
    this.filterPanel = filterPanel;

    filterPanel.addValueChangeHandler(changeHandler);
    filterPanel.setValue(userContext.getFilter(PriorityProjectFilter.class));

    initWidget(UI_BINDER.createAndBindUi(this));

    newPrioritaryProjectButton.setVisible(userContext.getUserProfile().hasPermission(RegisterPermission.ADD_NEW_PRIORITY_PROJECT_RESERVATION));
    newPrioritaryProjectButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_NEW_BUTTON);

    newActualisationButton.setVisible(userContext.getUserProfile().hasPermission(RegisterPermission.ADD_NEW_PRIORITY_PROJECT));
    newActualisationButton.ensureDebugId(TestIDRegister.ACTUALISATION_NEW_BUTTON);

    projectDataTable.asDataTable().addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        presenter.onProjectSelection(((SingleSelectionModel<PriorityProject>) projectDataTable.asDataTable().getSelectionModel()).getSelectedObject());
      }
    });
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;

    projectDataTable.setSortHandler(presenter);
  }

  @UiHandler("newPrioritaryProjectButton")
  void onNewPriorityProject(final ClickEvent event) {
    presenter.onNewPriorityProject();
  }

  @UiHandler("newActualisationButton")
  void onNewActualisation(final ClickEvent event) {
    presenter.onNewActualisation();
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuPriorityProjectsTitle();
  }

  @UiHandler("refreshButton")
  void onRefresh(final ClickEvent event) {
    presenter.onRefresh();
  }

  @Override
  public void setDataVisitor(final GridScrollVisitor gridScrollVisitor) {
    projectDataTable.asDataTable().exposeToVisitor(gridScrollVisitor);
  }

  @Override
  public void setData(final ArrayList<PriorityProject> result) {
    projectDataTable.asDataTable().setRowData(result);
  }

  @Override
  public void updateData(final ArrayList<PriorityProject> result) {
    projectDataTable.asDataTable().addRowData(result);
  }
}
