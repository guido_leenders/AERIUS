/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.importer;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.datepicker.client.DateBox;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;

public abstract class RegisterImportPermitDialogController extends RegisterImportDialogController<PermitKey> {

  private static class PermitImportPollingAgentImpl extends RequestImportPollingAgent<PermitKey> {

    PermitImportPollingAgentImpl(final RegisterRetrieveImportServiceAsync service) {
      super(service);
    }

    @Override
    protected void callService(final String key, final AsyncCallback<PermitKey> resultCallback) {
      service.getPermitResult(key, resultCallback);
    }
  }

  private static final String UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_PERMIT_SERVLET;

  private static final int TWELVE_HOUR = 12;
  private static final int ZERO_MINUTES = 0;

  public RegisterImportPermitDialogController(final PlaceController placeController, final RegisterRetrieveImportServiceAsync service,
      final RegisterImportDialogPanel<PermitKey> content) {
    super(placeController, content, UPLOAD_ACTION, new PermitImportPollingAgentImpl(service));

    content.showIdField(true,
        M.messages().registerApplicationTabAppDossierNumber(), M.messages().ivRegisterApplicationTabAppDossierNumberRequired());
    final DateBox receivedDate =
        content.addAdditionalDateField(true, SharedConstants.IMPORT_DATE_FIELD_RECEIVEDDATE, M.messages().registerApplicationTabAppDate());

    receivedDate.addValueChangeHandler(new ValueChangeHandler<Date>() {

      @SuppressWarnings("deprecation")
      @Override
      public void onValueChange(final ValueChangeEvent<Date> event) {
        final Date date = event.getValue();

        if (date != null && date.getHours() == 0 && date.getMinutes() == 0) {
          date.setHours(TWELVE_HOUR);
          date.setMinutes(ZERO_MINUTES);
        }

        receivedDate.setValue(date);
      }
    });
  }

  @Override
  public void handleDuplicate(final ImportDuplicateEntryException duplicateException) {
    final PermitKey duplicateKey = (PermitKey) duplicateException.getKey();

    setDuplicateInfo(M.messages().registerImportOpenDuplicateLabelPermit(duplicateKey.getDossierId()), getPlace(duplicateKey));
  }

  protected PermitDossierPlace getPlace(final PermitKey key) {
    return new PermitDossierPlace(key);
  }

}
