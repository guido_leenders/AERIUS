/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.geo;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.wui.geo.DevelopmentRuleLayer;
import nl.overheid.aerius.wui.main.event.DevelopmentRulesEvent;

@Singleton
public class RegisterDevelopmentRuleLayerWrapper {
  interface DevelopmentRuleLayerWrapperEventBinder extends EventBinder<RegisterDevelopmentRuleLayerWrapper> { }

  private final DevelopmentRuleLayerWrapperEventBinder eventBinder = GWT.create(DevelopmentRuleLayerWrapperEventBinder.class);

  private DevelopmentRuleLayer layer;
  private final MapLayoutPanel map;
  private DevelopmentRuleResult developmentRuleResult;
  private Boolean visible = false;

  @Inject
  public RegisterDevelopmentRuleLayerWrapper(final RegisterCoreMapLayoutPanel mapPanel) {
    this.map = mapPanel;
    eventBinder.bindEventHandlers(this, mapPanel.getEventBus());
  }

  public void setVisible(final boolean visible) {
    if (layer != null) {
      map.setVisible(layer, visible);
    }
  }

  /**
   * Updates the marker layer
   */
  private void updateLayer() {
    if (layer == null) {
      addLayer();
    }

    if (visible) {
      if ((developmentRuleResult != null) && !developmentRuleResult.isEmpty()) {
        layer.drawMarkers(developmentRuleResult);
      }
    } else {
      layer.clearMarkers();
    }
  }

  public void clearMarkers() {
    if (layer != null) {
      layer.clearMarkers();
    }
  }

  /**
   * Add the layer from scratch (no null-check).
   */
  private void addLayer() {
    layer = new DevelopmentRuleLayer(map);
    map.addLayer((MapLayer) layer);
  }

  /**
   * Remove the layer fully, such as when a User Input calculation started.
   */
  public void detach() {
    if (developmentRuleResult != null) {
      developmentRuleResult = null;
    }

    if (layer != null) {
      clearMarkers();
      map.removeLayer((MapLayer) layer);
      layer = null;
    }
  }

  public void updateDevelopmentRulesAndDraw(final DevelopmentRulesEvent e) {
    this.developmentRuleResult = e.getResult();
    this.visible = e.isVisible();

    updateLayer();
  }

}
