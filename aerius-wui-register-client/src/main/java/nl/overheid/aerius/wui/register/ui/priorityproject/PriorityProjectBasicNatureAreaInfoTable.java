/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleCollapsibleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.TableColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class PriorityProjectBasicNatureAreaInfoTable extends Composite implements IsInteractiveDataTable<Entry<AssessmentArea, ArrayList<HabitatType>>> {
  interface PriorityProjectBasicNatureAreaInfoTableUiBinder extends UiBinder<Widget, PriorityProjectBasicNatureAreaInfoTable> {}

  private static final PriorityProjectBasicNatureAreaInfoTableUiBinder UI_BINDER = GWT.create(PriorityProjectBasicNatureAreaInfoTableUiBinder.class);

  public interface CustomStyle extends CssResource {
    String contentStyle();
  }

  @UiField SimpleCollapsibleDivTable<Entry<AssessmentArea, ArrayList<HabitatType>>> table;

  @UiField(provided = true) TextColumn<Entry<AssessmentArea, ArrayList<HabitatType>>> nameColumn;
  @UiField(provided = true) TableColumn<HabitatType, Entry<AssessmentArea, ArrayList<HabitatType>>, SimpleDivTable<HabitatType>> content;

  @UiField CustomStyle style;

  public PriorityProjectBasicNatureAreaInfoTable() {
    nameColumn = new TextColumn<Entry<AssessmentArea, ArrayList<HabitatType>>>() {

      @Override
      public String getValue(final Entry<AssessmentArea, ArrayList<HabitatType>> object) {
        return object.getKey().getName();
      }
    };

    content = new TableColumn<HabitatType, Entry<AssessmentArea, ArrayList<HabitatType>>, SimpleDivTable<HabitatType>>() {

      @Override
      public Collection<HabitatType> getRowData(final Entry<AssessmentArea, ArrayList<HabitatType>> object) {
        return object.getValue();
      }

      @Override
      public void applyRowOptions(final Widget rowContainer, final SimpleDivTable<HabitatType> widget) {
        super.applyRowOptions(rowContainer, widget);

        widget.addStyleName(style.contentStyle());
      }

      @Override
      public SimpleDivTable<HabitatType> createDataTable(final Entry<AssessmentArea, ArrayList<HabitatType>> object) {
        final SimpleDivTable<HabitatType> table = new SimpleDivTable<>();
        table.addColumn(new TextColumn<HabitatType>() {

          @Override
          public String getValue(final HabitatType object) {
            return object.getCode() + " " + object.getName();
          }
        });

        return table;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public SimpleCollapsibleDivTable<Entry<AssessmentArea, ArrayList<HabitatType>>> asDataTable() {
    return table;
  }
}
