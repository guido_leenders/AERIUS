/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.ContentLoadingWidget;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.event.PriorityProjectSubProjectsEvent;
import nl.overheid.aerius.wui.register.place.PriorityProjectActualisationPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectOverviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierDetailPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectSubPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.Presenter;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.PriorityProjectViewType;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectAssignCompleteDialogPanel;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public abstract class PriorityProjectBaseActivity<P extends PriorityProjectPlace> extends AdminAbstractActivity<RegisterAppContext>
    implements Presenter {

  @SuppressWarnings("rawtypes")
  interface PriorityProjectBaseActivityEventBinder extends EventBinder<PriorityProjectBaseActivity> { }
  private final PriorityProjectBaseActivityEventBinder eventBinder = GWT.create(PriorityProjectBaseActivityEventBinder.class);

  private final PriorityProjectBaseView baseView;
  protected final P currentPlace;
  private final PriorityProjectViewType viewType;
  private final PriorityProjectExportDialogController exportDialogController;

  protected PriorityProject priorityProject;

  private AcceptsOneWidget panel;

  private final AsyncCallback<Void> deleteProjectCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      broadcastNotificationMessage(
          M.messages().registeraNotificationApplicationPriorityProjectDeleted(priorityProject.getDossierMetaData().getDossierId()));
      appContext.invalidatePriorityProject();
      goTo(new PriorityProjectOverviewPlace());
    }

    @Override
    public void onFailure(final Throwable caught) {
      PriorityProjectBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  private final AsyncCallback<Void> updateAssignCompletedProjectCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setLocked(false);
      reloadCurrentPriorityProject();
    }

    @Override
    public void onFailure(final Throwable caught) {
      PriorityProjectBaseActivity.this.onFailure(caught);
      super.onFailure(caught);
    }
  };

  @Inject
  public PriorityProjectBaseActivity(final P currentPlace, final RegisterAppContext appContext, final PriorityProjectBaseView baseView,
      final PriorityProjectViewType viewType, final PriorityProjectExportDialogController exportDialogController) {
    super(appContext, RegisterPermission.VIEW_PRIORITY_PROJECTS);

    this.currentPlace = currentPlace;
    this.baseView = baseView;
    this.viewType = viewType;
    this.exportDialogController = exportDialogController;

  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    this.panel = panel;
    eventBinder.bindEventHandlers(this, eventBus);

    baseView.setViewType(viewType);
    baseView.setPresenter(this);
    setLocked(true);

    refresh();
  }

  protected void refresh() {
    panel.setWidget(new ContentLoadingWidget());
    appContext.getPriorityProject(currentPlace.getKey(), this);
  }

  @Override
  public void switchView(final PriorityProjectViewType value) {
    goTo(getPlace(value));
  }

  @Override
  public final void onSuccess(final PriorityProject priorityProject) {
    this.priorityProject = priorityProject;

    panel.setWidget(baseView);
    startChild(baseView);

    baseView.setPriorityProject(priorityProject);
    baseView.showButtonGroup(!(currentPlace instanceof PriorityProjectSubPlace));
    setLocked(false);

    onPriorityProjectLoaded(priorityProject);
  }

  @Override
  public void onFailure(final Throwable caught) {
    reloadCurrentPriorityProject();
  }

  protected void reloadCurrentPriorityProject() {
    appContext.invalidatePriorityProject();

    if (priorityProject == null) {
      goTo(new PriorityProjectOverviewPlace());
    } else {
      appContext.getPriorityProjectForceReload(priorityProject.getKey(), this);
    }
  }

  protected abstract void onPriorityProjectLoaded(final PriorityProject priorityProject);

  protected abstract void startChild(AcceptsOneWidget panel);

  private Place getPlace(final PriorityProjectViewType value) {
    final Place place;

    switch (value) {
    case DOSSIER:
      place = new PriorityProjectDossierPlace(currentPlace.getKey());
      break;
    case PROJECT:
      place = new PriorityProjectProjectPlace(currentPlace.getKey());
      break;
    case ACTUALISATION:
      place = new PriorityProjectActualisationPlace(currentPlace.getKey());
      break;
    case APPLICATION:
      // Jump to only available application if there is one present
      final String reference = priorityProject.getSubProjects().size() == 1 ? priorityProject.getSubProjects().get(0).getReference() : null;
      place = new PriorityProjectProjectDossierDetailPlace(new PrioritySubProjectKey(currentPlace.getKey(), reference));
      break;
    default:
      throw new RuntimeException("Unknown viewtype selected.");
    }

    return place;
  }

  protected void setLocked(final boolean lock) {
    baseView.setLocked(lock);
    updatePermissionStates(lock);
  }

  private void updatePermissionStates(final boolean lock) {
    final boolean baseCheck = !lock && priorityProject != null;

    baseView.enableDelete(baseCheck
        && hasPermissionForAuthority(RegisterPermission.DELETE_PRIORITY_PROJECT));
    baseView.enableExport(baseCheck
        && (priorityProject.hasRequestFile(RequestFileType.PRIORITY_PROJECT_ACTUALISATION) || !priorityProject.getSubProjects().isEmpty()));
    final boolean ppAssignCompletePermissionCheck =
        priorityProject != null && priorityProject.isAssignCompleted()
          ? hasPermission(RegisterPermission.REVERT_PRIORITY_PROJECT_ASSIGN_COMPLETE)
          : hasPermissionForAuthority(RegisterPermission.UPDATE_PRIORITY_PROJECT);
    baseView.enableAssignComplete(baseCheck
        && ppAssignCompletePermissionCheck
        && !priorityProject.hasRequestFile(RequestFileType.PRIORITY_PROJECT_ACTUALISATION)
        && priorityProject.hasNoPendingSubProjects());
  }

  private boolean hasPermissionForAuthority(final RegisterPermission permission) {
    return UserProfileUtil.hasPermissionForAuthority(appContext.getUserContext().getUserProfile(),
        priorityProject == null ? null : priorityProject.getAuthority(), permission);
  }

  private boolean hasPermission(final RegisterPermission permission) {
    return UserProfileUtil.hasPermission(appContext.getUserContext().getUserProfile(), permission);
  }

  @EventHandler
  void onPriorityProjectSubProjectsSelected(final PriorityProjectSubProjectsEvent event) {
    final PriorityProjectProjectDossierDetailPlace place =
        new PriorityProjectProjectDossierDetailPlace(new PrioritySubProjectKey(currentPlace.getKey(), event.getValue()));
    goTo(place);
  }

  @Override
  public void exportPriorityProject() {
    exportDialogController.show(currentPlace);
  }

  @Override
  public void deletePriorityProject() {
    if (!Window.confirm(M.messages().registerapplicationDeletePriorityProjectTitle())) {
      return;
    }
    setLocked(true);
    appContext.getPriorityProjectService().deletePriorityProject(priorityProject.getKey(), deleteProjectCallback);
  }

  @Override
  public void assignCompletePriorityProject() {
    final ConfirmCancelDialog<Object, ?> dialog = new ConfirmCancelDialog<>(M.messages().genericOK(), M.messages().genericCancel());
    dialog.setText(priorityProject.isAssignCompleted()
        ? M.messages().registerProirityProjectAssignInProgressDialogPanelTitle()
            : M.messages().registerProirityProjectAssignCompleteDialogPanelTitle());

    dialog.setWidget(new PriorityProjectAssignCompleteDialogPanel(priorityProject.isAssignCompleted()));

    dialog.addConfirmHandler(new ConfirmHandler<Object>() {
      @Override
      public void onConfirm(final ConfirmEvent<Object> event) {
        setAssignCompleted(!priorityProject.isAssignCompleted());
        dialog.hide();
      }
    });
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        dialog.center();
      }
    });
  }

  private void setAssignCompleted(final boolean assignComplete) {
    setLocked(true);
    appContext.getPriorityProjectService().updatePriorityProjectAssignComplete(
        priorityProject.getKey(), assignComplete, priorityProject.getLastModified(), updateAssignCompletedProjectCallback);
  }
}
