/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.HashMap;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestSituation;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.ui.PermitDetailView.Presenter;

@Singleton
public class PermitDetailRequestViewImpl extends Composite implements Editor<Permit> {

  private static final int EMISSION_PRECISION = 2;
  private static final PermitDetailRequestViewImplUiBinder UI_BINDER = GWT.create(PermitDetailRequestViewImplUiBinder.class);

  public interface PermitDetailRequestViewDriver extends SimpleBeanEditorDriver<Permit, PermitDetailRequestViewImpl> {
  }

  interface PermitDetailRequestViewImplUiBinder extends UiBinder<Widget, PermitDetailRequestViewImpl> {
  }

  @Path("scenarioMetaData.corporation") @UiField Label corporation;
  @Path("scenarioMetaData.projectName") @UiField Label project;

  @Path("scenarioMetaData.description") @UiField Label description;

  @Ignore @UiField Label nh3Label;
  @Ignore @UiField Label nh3Value;
  @Ignore @UiField Label noxLabel;
  @Ignore @UiField Label noxValue;
  @Ignore @UiField Button gotoCalculatorButton;

  @UiField @Ignore Label situationName;
  @UiField @Ignore InlineLabel situationType;
  @Path("situations") Editor<HashMap<SituationType, RequestSituation>> situationNameEditor =
      new FakeLeafValueEditor<HashMap<SituationType, RequestSituation>>() {
    @Override
    public void setValue(final HashMap<SituationType, RequestSituation> value) {
      super.setValue(value);
      situationName.setText(value.get(SituationType.PROPOSED).getName());
      if (value.size() == 1) {
        situationType.setText(M.messages().registerApplicationTabAppSituationNew());
      } else {
        situationType.setText(M.messages().registerApplicationTabAppSituationExpansion());
      }
    }
  };

  @UiField @Ignore Label temporaryPeriod;
  Editor<Integer> temporaryPeriodEditor = new FakeLeafValueEditor<Integer>() {
    @Override
    public void setValue(final Integer value) {
      super.setValue(value);
      if (value == null) {
        temporaryPeriod.setText(M.messages().registerApplicationTabAppDurationNA());
      } else {
        temporaryPeriod.setText(M.messages().registerApplicationTabAppDurationValue(value));
      }
    }
  };

  @UiField Anchor reference;
  Editor<String> referenceEditor = new FakeLeafValueEditor<String>() {
    @Override
    public void setValue(final String value) {
      super.setValue(value);
      reference.setText(value);
    }
  };

  @Path("proposedEmissionValues") LeafValueEditor<EmissionValues> emissionValuesEditor = new FakeLeafValueEditor<EmissionValues>() {
    @Override
    public void setValue(final EmissionValues value) {
      super.setValue(value);
      nh3Value.setText(FormatUtil.formatEmissionWithUnit(value.getEmission(new EmissionValueKey(Substance.NH3)), EMISSION_PRECISION));
      noxValue.setText(FormatUtil.formatEmissionWithUnit(value.getEmission(new EmissionValueKey(Substance.NOX)), EMISSION_PRECISION));
    }
  };

  @Path("") LeafValueEditor<Permit> dummyCollector = new FakeLeafValueEditor<>();

  private Presenter presenter;

  @Inject
  public PermitDetailRequestViewImpl(final HelpPopupController hpC) {
    initWidget(UI_BINDER.createAndBindUi(this));

    nh3Label.setText(Substance.NH3.name());
    noxLabel.setText(Substance.NOX.name());

    corporation.ensureDebugId(TestIDRegister.APPLICATION_CORPORATION_VIEWER);
    project.ensureDebugId(TestIDRegister.APPLICATION_PROJECT_VIEWER);
  }

  @UiHandler({"pdfDownloadImage", "reference"})
  public void onPdfDownloadImage(final ClickEvent event) {
    presenter.openRequestFile(dummyCollector.getValue());
  }

  @UiHandler("gotoCalculatorButton")
  public void onGotoCalculator(final ClickEvent event) {
    presenter.showPostCalculatorDialog();
  }

  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;

  }
}
