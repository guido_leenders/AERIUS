/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.context;

import java.util.HashMap;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeKey;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.PermitSummary;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.service.AdminServiceAsync;
import nl.overheid.aerius.shared.service.NoticeServiceAsync;
import nl.overheid.aerius.shared.service.PermitServiceAsync;
import nl.overheid.aerius.shared.service.PriorityProjectServiceAsync;
import nl.overheid.aerius.shared.service.RequestExportServiceAsync;
import nl.overheid.aerius.wui.admin.context.AdminAppContext;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceHandle;
import nl.overheid.aerius.wui.register.event.PermitCalculationFinishedEvent;
import nl.overheid.aerius.wui.register.processor.PermitCalculationPollingAgent;

/**
 * Application Context object for Register.
 */
@Singleton
public class RegisterAppContext extends AdminAppContext<RegisterContext, RegisterUserContext> {

  private final HashMap<String, ConcurrentAsyncServiceHandle<?>> concurrencyMap = new HashMap<>();

  private final PermitServiceAsync permitService;
  private final PriorityProjectServiceAsync priorityProjectService;
  private final NoticeServiceAsync noticeService;
  private final RequestExportServiceAsync requestExportService;

  private Notice currentNotice;
  private Permit currentPermit;
  private PermitSummary currentPermitSummary;
  private PriorityProject currentPriorityProject;
  private final PermitCalculationPollingAgent pollingAgent;

  /**
   * Callback to monitor if permit calculation is finished. Callback stops pollingAgent when done.
   * Note that if the user would navigate to other permit and then the results would come back the onSuccess/onFailure isn't called
   *  because the polling agent handles this.
   */
  private final AsyncCallback<PermitKey> permitCalculationCallback = new AsyncCallback<PermitKey>() {
    @Override
    public void onSuccess(final PermitKey result) {
      if (result != null && result.equals(currentPermit.getPermitKey())) {
        getPollingAgent().stop();
        getEventBus().fireEvent(new PermitCalculationFinishedEvent(result));
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      // silently fail, just stop the polling agent.
      getPollingAgent().stop();
    }
  };

  @Inject
  public RegisterAppContext(final EventBus eventBus, final PlaceController placeController, final RegisterContext context,
      final RegisterUserContext userContext, final PermitServiceAsync permitService, final PriorityProjectServiceAsync priorityProjectService,
      final NoticeServiceAsync noticeService, final AdminServiceAsync adminService, final RequestExportServiceAsync requestExportService) {
    super(eventBus, placeController, context, userContext, adminService);

    this.permitService = permitService;
    this.priorityProjectService = priorityProjectService;
    this.noticeService = noticeService;
    this.requestExportService = requestExportService;
    pollingAgent = new PermitCalculationPollingAgent(permitService);
  }

  public void getNotice(final NoticeKey noticeKey, final AsyncCallback<Notice> callback) {
    if (currentNotice != null && currentNotice.getReference().equals(noticeKey.getReference())) {
      callback.onSuccess(currentNotice);
    } else {
      currentNotice = null;
      noticeService.fetchNoticeDetails(noticeKey, new AppAsyncCallback<Notice>() {
        @Override
        public void onSuccess(final Notice notice) {
          currentNotice = notice;
          callback.onSuccess(notice);
        }

        @Override
        public void onFailure(final Throwable caught) {
          super.onFailure(caught);
          callback.onFailure(caught);
        }
      });
    }
  }

  public void getPermit(final PermitKey permitKey, final AsyncCallback<Permit> callback) {
    if (currentPermit != null && currentPermit.getPermitKey().equals(permitKey)) {
      callback.onSuccess(currentPermit);
    } else {
      getPermitForceReload(permitKey, callback);
    }
  }

  public void getPriorityProject(final PriorityProjectKey projectKey, final AsyncCallback<PriorityProject> callback) {
    if (currentPriorityProject != null && currentPriorityProject.getKey().equals(projectKey)) {
      callback.onSuccess(currentPriorityProject);
    } else {
      getPriorityProjectForceReload(projectKey, callback);
    }
  }

  public void getPriorityProjectForceReload(final PriorityProjectKey projectKey, final AsyncCallback<PriorityProject> callback) {
    currentPriorityProject = null;
    priorityProjectService.fetchPriorityProject(projectKey, new AppAsyncCallback<PriorityProject>() {
      @Override
      public void onSuccess(final PriorityProject priorityProject) {
        currentPriorityProject = priorityProject;
        callback.onSuccess(priorityProject);
      }

      @Override
      public void onFailure(final Throwable caught) {
        super.onFailure(caught);
        callback.onFailure(caught);
      }
    });
  }

  public void getPermitForceReload(final PermitKey permitKey, final AsyncCallback<Permit> callback) {
    currentPermit = null;
    permitService.fetchPermit(permitKey, new AppAsyncCallback<Permit>() {
      @Override
      public void onSuccess(final Permit permit) {
        currentPermit = permit;
        if (permit.getRequestState() == RequestState.INITIAL) {
          getPollingAgent().start(permit.getPermitKey(), permitCalculationCallback);
        }
        callback.onSuccess(permit);
      }

      @Override
      public void onFailure(final Throwable caught) {
        super.onFailure(caught);
        callback.onFailure(caught);
      }
    });
  }

  public void getPermitSummary(final PermitKey permitKey, final AsyncCallback<PermitSummary> callback) {
    if (currentPermitSummary != null && currentPermitSummary.getPermitKey().equals(permitKey)) {
      callback.onSuccess(currentPermitSummary);
    } else {
      currentPermitSummary = null;
      permitService.getPermitSummary(permitKey, new AppAsyncCallback<PermitSummary>() {
        @Override
        public void onSuccess(final PermitSummary result) {
          currentPermitSummary = result;
          callback.onSuccess(result);
        }

        @Override
        public void onFailure(final Throwable caught) {
          super.onFailure(caught);
          callback.onFailure(caught);
        }
      });
    }
  }

  /**
   * Invalidates the current permit. This forces the permit to be reload from the server when the permit is requested.
   */
  public void invalidatePermit() {
    currentPermit = null;
    currentPermitSummary = null;
  }

  /**
   * Invalidates the current permit. This forces the permit to be reload from the server when the permit is requested.
   */
  public void invalidatePriorityProject() {
    currentPriorityProject = null;
  }

  public NoticeServiceAsync getNoticeService() {
    return noticeService;
  }

  public PermitServiceAsync getPermitService() {
    return permitService;
  }

  public PriorityProjectServiceAsync getPriorityProjectService() {
    return priorityProjectService;
  }

  public RequestExportServiceAsync getRequestExportService() {
    return requestExportService;
  }

  public PermitCalculationPollingAgent getPollingAgent() {
    return pollingAgent;
  }

  public HashMap<String, ConcurrentAsyncServiceHandle<?>> getConcurrencyMap() {
    return concurrencyMap;
  }
}
