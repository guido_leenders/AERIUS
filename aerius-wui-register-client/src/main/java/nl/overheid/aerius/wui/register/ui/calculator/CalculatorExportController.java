/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.calculator;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.util.development.DevModeUtil;
import nl.overheid.aerius.wui.main.widget.dialog.AeriusDialogBox;

public abstract class CalculatorExportController {

  private final AeriusDialogBox dialog;
  private final String applicationHostPlusName;

  private final EventBus eventBus;

  public CalculatorExportController(final Context context, final EventBus eventBus) {
    this.eventBus = eventBus;

    dialog = new AeriusDialogBox();
    dialog.setHTML(M.messages().registerOpenInCalculatorPanelTitle());

    applicationHostPlusName = (String) context.getSetting(SharedConstantsEnum.CALCULATOR_FETCH_URL);
  }

  public void showPostCalculatorDialog() {
    dialog.center();
    dialog.setWidget(new CalculatorPreparationPanel());

    doServiceCall(new AsyncCallback<ScenarioGMLs>() {
      @Override
      public void onSuccess(final ScenarioGMLs result) {
        final CalculatorForwardPanel panel = new CalculatorForwardPanel(calculatorURL(), result);

        panel.addSubmitHandler(new SubmitHandler() {
          @Override
          public void onSubmit(final SubmitEvent event) {
            if (!DevModeUtil.I.isDevMode()) {
              dialog.hide();
            }
          }
        });
        panel.addCloseHandler(new CloseHandler<Boolean>() {
          @Override
          public void onClose(final CloseEvent<Boolean> event) {
            dialog.hide();
          }
        });

        dialog.setWidget(panel);
      }

      private String calculatorURL() {
        return applicationHostPlusName + "?locale=" + LocaleInfo.getCurrentLocale().getLocaleName();
      }

      @Override
      public void onFailure(final Throwable caught) {
        notifyError(caught);
      }
    });
  }

  protected abstract void doServiceCall(final AsyncCallback<ScenarioGMLs> callback);

  private void notifyError(final Throwable caught) {
    dialog.hide();

    NotificationUtil.broadcastError(eventBus, M.messages().registerOpenInCalculatorPrepError());
  }
}
