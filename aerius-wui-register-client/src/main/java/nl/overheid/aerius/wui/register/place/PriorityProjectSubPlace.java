/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.place;

import java.util.Map;

import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;

/**
 * Base class for Permit Detail places.
 */
public abstract class PriorityProjectSubPlace extends PriorityProjectPlace {
  private static final String REFERENCE_ID = "ref";

  public static abstract class Tokenizer<P extends PriorityProjectSubPlace> extends RequestKeyPlace.Tokenizer<PriorityProjectKey, P> {
    @Override
    protected void updatePlace(final Map<String, String> tokens, final P place) {
      super.updatePlace(tokens, place);

      place.reference = tokens.get(REFERENCE_ID);
    }

    @Override
    protected void setTokenMap(final P place, final Map<String, String> tokens) {
      super.setTokenMap(place, tokens);

      put(tokens, REFERENCE_ID, place.reference);
    }
  }

  protected String reference;

  protected PriorityProjectSubPlace() {
    super();
  }

  protected PriorityProjectSubPlace(final PrioritySubProjectKey key) {
    super(key);
    this.reference = key.getReference();
  }

  @Override
  public PriorityProjectKey getKey() {
    return new PriorityProjectKey(dossierId, authorityCode);
  }

  public PrioritySubProjectKey getSubKey() {
    return new PrioritySubProjectKey(getKey(), reference);
  }
}
