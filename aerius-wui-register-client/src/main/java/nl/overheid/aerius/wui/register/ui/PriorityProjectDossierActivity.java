/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierEditPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.PriorityProjectViewType;
import nl.overheid.aerius.wui.register.ui.PriorityProjectDossierViewImpl.PriorityProjectDossierViewDriver;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public class PriorityProjectDossierActivity extends PriorityProjectBaseActivity<PriorityProjectDossierPlace> implements
    PriorityProjectDossierView.Presenter {

  private final SimpleBeanEditorDriver<PriorityProject, PriorityProjectDossierViewImpl> dossierViewDriver =
      GWT.create(PriorityProjectDossierViewDriver.class);

  private final PriorityProjectDossierView view;

  private PriorityProject priorityProject;

  @Inject
  public PriorityProjectDossierActivity(
      @Assisted final PriorityProjectDossierPlace place, final RegisterAppContext appContext, final PriorityProjectBaseView baseView,
      final PriorityProjectDossierView view, final PriorityProjectExportDialogController exportDialogController) {
    super(place, appContext, baseView, PriorityProjectViewType.DOSSIER, exportDialogController);
    this.view = view;

    // This here cast is rather unfortunate - but we need an explicit Editor type here. This is a problem inherent to using Editors in a Presenter.
    dossierViewDriver.initialize((PriorityProjectDossierViewImpl) view);
  }

  @Override
  public void startChild(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  public void edit() {
    if (priorityProject != null) {
      goTo(new PriorityProjectDossierEditPlace(priorityProject.getKey()));
    }
  }

  @Override
  protected void onPriorityProjectLoaded(final PriorityProject priorityProject) {
    this.priorityProject = priorityProject;

    dossierViewDriver.edit(priorityProject);

    final boolean editable = UserProfileUtil.hasPermissionForAuthority(appContext.getUserContext().getUserProfile(),
        priorityProject.getAuthority(), RegisterPermission.UPDATE_PRIORITY_PROJECT);
    view.setEditable(editable);
  }
}
