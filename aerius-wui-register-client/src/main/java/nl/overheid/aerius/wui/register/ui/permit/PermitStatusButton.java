/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.permit;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.register.ui.statechange.BaseStatusButton;
import nl.overheid.aerius.wui.register.ui.statechange.PermitStateChangeFlow;

public class PermitStatusButton extends BaseStatusButton {

  public interface PermitStatusHandler extends BaseStatusHandler {
    void markRequest();
  }

  private final Button markButton;
  private PermitStatusHandler permitStatusHandler;

  @Inject
  public PermitStatusButton(final EventBus eventBus, final RegisterUserContext userContext) {
    super(eventBus, userContext, new PermitStateChangeFlow());

    markButton = addAdditionalButton(R.css().buttonMarked(), TestIDRegister.APPLICATION_MARK_BUTTON, new ClickHandler() {

      @Override
      public void onClick(final ClickEvent event) {
        permitStatusHandler.markRequest();
      }
    });
  }

  public void setPermitStatusHandler(final PermitStatusHandler permitStatusHandler) {
    this.permitStatusHandler = permitStatusHandler;

    super.setBaseStatusHandler(permitStatusHandler);
  }

  public void enableMark(final boolean enabled) {
    markButton.setEnabled(enabled);
  }

  public void setMarked(final boolean marked) {
    markButton.setStyleName(R.css().buttonMarked(), marked);
    markButton.setStyleName(R.css().buttonUnmarked(), !marked);
  }

}
