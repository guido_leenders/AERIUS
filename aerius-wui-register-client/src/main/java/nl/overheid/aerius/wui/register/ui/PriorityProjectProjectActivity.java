/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PriorityProjectPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectPlace;
import nl.overheid.aerius.wui.register.ui.PriorityProjectBaseView.PriorityProjectViewType;
import nl.overheid.aerius.wui.register.ui.importer.PriorityProjectImportSubProjectDialogController;
import nl.overheid.aerius.wui.register.ui.importer.RegisterImportDialogPanel;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectExportDialogController;

public class PriorityProjectProjectActivity extends PriorityProjectBaseActivity<PriorityProjectPlace>
    implements PriorityProjectProjectView.Presenter {

  private final PriorityProjectProjectView view;

  @Inject
  public PriorityProjectProjectActivity(@Assisted final PriorityProjectProjectPlace place, final RegisterAppContext appContext,
      final PriorityProjectBaseView baseView, final PriorityProjectProjectView view,
      final PriorityProjectExportDialogController exportDialogController) {
    super(place, appContext, baseView, PriorityProjectViewType.PROJECT, exportDialogController);
    this.view = view;
  }

  @Override
  public void newSubProject() {
    final RegisterImportDialogPanel<PrioritySubProjectKey> dialog =
        new RegisterImportDialogPanel<>(SupportedUploadFormat.PRIORITY_PROJECT_SUBPROJECT);
    dialog.addAdditionalTextField(true, SharedConstants.IMPORT_LABEL_FIELD_NAME,
        M.messages().registerPriorityProjectProjectDossierInformationSubProjectLabel(), null, null);
    dialog.addAdditionalExplanationText(M.messages().registerPriorityProjectProjectDossierInformationSubProjectLabelExplanation());

    final PriorityProjectImportSubProjectDialogController newSubProjectController =
        new PriorityProjectImportSubProjectDialogController(appContext.getPlaceController(), RegisterRetrieveImportServiceAsync.Util.getInstance(),
            dialog, priorityProject.getReference()) {
      @Override
      public final void handleResult(final PrioritySubProjectKey key) {
        hide();
        appContext.invalidatePriorityProject();
        goTo(getPlace(key));
      }
    };

    newSubProjectController.setTitle(M.messages().registerPriorityProjectProjectSubProjectNew());
    newSubProjectController.show();
  }

  @Override
  public void openRequestFile(final Request request, final RequestFileType fileType) {
    Window.open(request.getUrl(fileType), "_blank", "");
  }

  @Override
  public void startChild(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  protected void onPriorityProjectLoaded(final PriorityProject priorityProject) {
    view.setData(priorityProject);
  }

}
