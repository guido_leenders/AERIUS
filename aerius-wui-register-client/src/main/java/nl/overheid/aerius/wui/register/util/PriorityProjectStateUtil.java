/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.util;

import com.google.gwt.resources.client.DataResource;

import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.wui.main.resources.R;

public class PriorityProjectStateUtil {

  public static DataResource getPriorityProjectStateIcon(final RequestState state) {
    final DataResource resource;

    switch (state) {
    case INITIAL:
      resource = R.images().applicationstateNoReservationPresent();
      break;
    case QUEUED:
      resource = R.images().applicationstatePriorityProjectQueued();
      break;
    case ASSIGNED:
      resource = R.images().applicationstateAssigned();
      break;
    case REJECTED_WITHOUT_SPACE:
      resource = R.images().applicationstateRejectedWithoutSpace();
      break;
    case ASSIGNED_FINAL:
      resource = R.images().applicationstateAssignedFinal();
      break;
    case REJECTED_FINAL:
      resource = R.images().applicationstateRejectedFinal();
      break;
    default:
      resource = null;
      break;
    }

    return resource;
  }

  public static DataResource getPriorityProjectStateIconInactive(final RequestState state) {
    final DataResource resource;
    switch (state) {
    case QUEUED:
      resource = R.images().applicationstatePriorityProjectQueuedInactive();
      break;
    case ASSIGNED:
      resource = R.images().applicationstateAssignedInactive();
      break;
    case REJECTED_WITHOUT_SPACE:
      resource = R.images().applicationstateRejectedWithoutSpaceInactive();
      break;
    case ASSIGNED_FINAL:
      resource = R.images().applicationstateAssignedFinalInactive();
      break;
    case REJECTED_FINAL:
      resource = R.images().applicationstateRejectedFinalInactive();
      break;
    default:
      resource = null;
      break;
    }
    return resource;
  }

}
