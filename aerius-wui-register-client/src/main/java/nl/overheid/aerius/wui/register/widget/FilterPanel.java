/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.user.client.ui.Composite;

import nl.overheid.aerius.shared.domain.Filter;

/**
 *
 */
public abstract class FilterPanel<F extends Filter> extends Composite {

  private FilterPanelPresenter presenter;

  public abstract void setFilter(final F filter);

  protected void onFilterChange(final F filter) {
    presenter.onFilterChange(filter);
  }

  public void setPresenter(final FilterPanelPresenter presenter) {
    this.presenter = presenter;
  }

}
