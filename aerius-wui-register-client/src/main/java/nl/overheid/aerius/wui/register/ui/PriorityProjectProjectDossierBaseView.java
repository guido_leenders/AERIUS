/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.wui.register.ui.statechange.BaseStatusButton.BaseStatusHandler;

public interface PriorityProjectProjectDossierBaseView extends IsWidget, AcceptsOneWidget {
  enum PriorityProjectDossierDetailViewType {
    DETAIL, REVIEW;
  }

  interface Presenter extends BaseStatusHandler {
    void onPrioritySubProjectLoaded(PrioritySubProject prioritySubProject);

  }

  /**
   * Whether to enable the status dropdown.
   * @param enabled enable on true, disable on false
   */
  void enableStatus(boolean enabled);

  /**
   * Whether to enable the delete button.
   * @param enabled enable on true, disable on false
   */
  void enableDelete(boolean enabled);

  /**
   * Lock the interactive user interface elements until the operation finished.
   * @param lock if true lock
   */
  void setLocked(boolean lock);

  void setPresenter(Presenter presenter);

  void setViewType(PriorityProjectDossierDetailViewType viewType);

  void setPrioritySubProject(PrioritySubProject prioritySubProject);

}
