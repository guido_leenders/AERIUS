/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.table.DataProvider;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectDossierViewer;
import nl.overheid.aerius.wui.register.widget.RequestHistoryPanel;

public class PriorityProjectDossierViewImpl extends Composite implements PriorityProjectDossierView, Editor<PriorityProject> {
  interface PriorityProjectDossierViewImplUiBinder extends UiBinder<Widget, PriorityProjectDossierViewImpl> {}

  private static final PriorityProjectDossierViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectDossierViewImplUiBinder.class);

  public interface PriorityProjectDossierViewDriver extends SimpleBeanEditorDriver<PriorityProject, PriorityProjectDossierViewImpl> {}

  private Presenter presenter;

  @Path("") @UiField PriorityProjectDossierViewer dossierViewer;

  @Path("changeHistory") DataProvider<ArrayList<AuditTrailItem>, AuditTrailItem> historyData;
  @UiField RequestHistoryPanel historyPanel;
  @UiField Button editButton;

  @Inject
  public PriorityProjectDossierViewImpl(final RegisterUserContext userContext) {
    initWidget(UI_BINDER.createAndBindUi(this));

    historyData = new DataProvider<>();
    historyData.addDataDisplay(historyPanel);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuPriorityProjectsTitle();
  }

  @UiHandler("editButton")
  public void onEditClick(final ClickEvent e) {
    presenter.edit();
  }

  @Override
  public void setEditable(final boolean editable) {
    editButton.setEnabled(editable);
  }
}
