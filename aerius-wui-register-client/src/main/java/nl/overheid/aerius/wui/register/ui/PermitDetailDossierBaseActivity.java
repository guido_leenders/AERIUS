/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.user.client.Window;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitPlace;

/**
 *
 */
abstract class PermitDetailDossierBaseActivity<P extends PermitPlace> extends PermitDetailBaseActivity implements PermitDetailDossierView.Presenter {

  private final PermitDetailDossierView contentPanel;
  protected Permit permit;

  protected PermitDetailDossierBaseActivity(final RegisterAppContext appContext, final PermitDetailView view,
      final PermitDetailDossierView contentPanel, final P place) {
    super(appContext, view, contentPanel, place);
    this.contentPanel = contentPanel;
  }

  @Override
  protected void onStart() {
    super.onStart();
    contentPanel.setPresenter(this);
  }

  @Override
  protected void onPermitLoaded(final Permit permit) {
    this.permit = permit;
    updatePermissions(appContext.getUserContext().getUserProfile(), permit);
  }


  @Override
  public void downloadDecree(final String url) {
    if (url == null) {
      broadcastNotificationMessage(M.messages().registerNotificationApplicationNoDecree());
      return;
    }
    Window.open(url, "_blank", "");
  }

  protected void updatePermissions(final UserProfile user, final Permit permit) {
    final Authority authority = permit.getAuthority();
    final boolean permissionRemarks = UserProfileUtil.hasPermissionForAuthority(user, authority, RegisterPermission.UPDATE_PERMIT_REMARKS);
    final boolean permissionTreator = UserProfileUtil.hasPermissionForAuthority(user, authority, RegisterPermission.UPDATE_PERMIT_HANDLER);
    final boolean permissionDossierId = UserProfileUtil.hasPermissionForAuthority(user, authority, RegisterPermission.UPDATE_PERMIT_DOSSIER_NUMBER);
    final boolean permissionDate = UserProfileUtil.hasPermissionForAuthority(user, authority, RegisterPermission.UPDATE_PERMIT_DATE)
        && (permit.getRequestState() == RequestState.INITIAL || permit.getRequestState() == RequestState.QUEUED);

    contentPanel.enableRemarks(permissionRemarks);
    contentPanel.enableHandler(permissionTreator);
    contentPanel.enableDossierId(permissionDossierId);
    contentPanel.enableDate(permissionDate);
  }

}
