/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.search;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestionType;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.shared.service.RegisterSearchServiceAsync;
import nl.overheid.aerius.wui.main.ui.search.SearchAction;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceCall;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceHandle;
import nl.overheid.aerius.wui.register.place.NoticeDossierPlace;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectDossierPlace;

/**
 * Search actions. Call the server for finding results and perform the action when a user selects a suggestion.
 */
@Singleton
public class RegisterSearchAction implements SearchAction<RegisterSearchSuggestionType, RegisterSearchSuggestion> {
  // Start searching after at least this amount of characters are entered.
  private static final int SEARCH_BOX_AMOUNT_OF_CHARS_NEEDED = 3;

  private static final String SEARCH_THREAD = "service_search";

  final RegisterSearchServiceAsync searchService;

  private final HashMap<String, ConcurrentAsyncServiceHandle<?>> searchConcurrencyMap = new HashMap<>();

  private final PlaceController placeController;

  @Inject
  public RegisterSearchAction(final RegisterSearchServiceAsync searchService, final FetchGeometryServiceAsync fetchGeometryService, final PlaceController placeController) {
    this.searchService = searchService;
    this.placeController = placeController;
  }

  /**
   * Returns whether the given search string is a meaningful search term, that should trigger some action.
   * @param searchText text to search
   * @return True or false; when false the search box will not show the suggestions popup nor the search animation.
   */
  @Override
  public boolean isSearchable(final String searchText) {
    return searchText != null && !searchText.isEmpty()
        && searchText.length() >= SEARCH_BOX_AMOUNT_OF_CHARS_NEEDED;
  }

  /**
   * Search for the given search string and return results via the callback.
   * @param searchText text to search
   * @param callback what to call on complete.
   */
  @Override
  public void search(final String searchText, final AsyncCallback<ArrayList<RegisterSearchSuggestion>> callback) {
    ConcurrentAsyncServiceUtil.register(SEARCH_THREAD, searchConcurrencyMap, callback,
        new ConcurrentAsyncServiceCall<ArrayList<RegisterSearchSuggestion>>() {
      @Override
      public void execute(final AsyncCallback<ArrayList<RegisterSearchSuggestion>> callback) {
        searchService.getSuggestions(searchText, callback);
      }
    });
  }

  @Override
  public void selectSuggestion(final RegisterSearchSuggestion suggestion) {
    switch (suggestion.getType()) {
    case PERMIT:
      final Permit permit = (Permit) suggestion.getRequest();
      placeController.goTo(new PermitDossierPlace(permit.getPermitKey()));
      break;
    case NOTICE:
      final Notice notice = (Notice) suggestion.getRequest();
      placeController.goTo(new NoticeDossierPlace(notice.getKey()));
      break;
    case PRIORITY_PROJECT:
      final PriorityProject project = (PriorityProject) suggestion.getRequest();
      placeController.goTo(new PriorityProjectDossierPlace(project.getKey()));
      break;
      // TODO; enable if we have a SubProjectKey present - also in service
      //    case PRIORITY_SUBPROJECT:
      //      final PrioritySubProject subProject = (PrioritySubProject) suggestion.getRequest();
      //      placeController.goTo(new PriorityProjectProjectDossierDetailPlace(subProject.getKey()));
      //      break;
    default:
      // None of these are supported here.
      break;
    }
  }

  @Override
  public void getSubSuggestions(final RegisterSearchSuggestion parentSuggestion, final AsyncCallback<ArrayList<RegisterSearchSuggestion>> callback) {
    // Not supported here.
  }
}
