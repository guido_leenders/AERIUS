/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.register.place.NoticePlace;

/**
 * Show the Notice details with history.
 */
@Singleton
public class NoticeDetailViewImpl extends Composite implements NoticeDetailView {

  interface NoticeDetailViewImplUiBinder extends UiBinder<Widget, NoticeDetailViewImpl> {}

  private static final NoticeDetailViewImplUiBinder UI_BINDER = GWT.create(NoticeDetailViewImplUiBinder.class);

  interface CustomStyle extends CssResource {
    String lock();
  }

  @UiField CustomStyle style;

  @UiField Label titleLabel;
  @UiField(provided = true) NoticeTabPanel tabPanel;
  @UiField(provided = true) NoticeGroupButton buttonGroup;
  @UiField SimplePanel contentHolderPanel;

  private Presenter presenter;
  private Notice notice;

  @Inject
  public NoticeDetailViewImpl(final NoticeTabPanel tabPanel, final NoticeGroupButton buttonGroup, final HelpPopupController hpC) {
    this.tabPanel = tabPanel;
    this.buttonGroup = buttonGroup;

    initWidget(UI_BINDER.createAndBindUi(this));

    this.tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
      @Override
      public void onSelection(final SelectionEvent<Integer> event) {
        presenter.switchView(event.getSelectedItem() == 0 ? NoticeViewType.DOSSIER : NoticeViewType.MAP);
      }
    });

  }

  @Override
  public void setPresenter(final NoticeDetailView.Presenter presenter) {
    this.presenter = presenter;
    buttonGroup.setGroupButtonHandler(presenter);
  }

  @Override
  public void setPlace(final NoticePlace place) {
    tabPanel.setTabFocus(place);
  }

  @Override
  public void setNotice(final Notice notice) {
    this.notice = notice;
    titleLabel.setText(notice.getScenarioMetaData().getProjectName());
  }

  @Override
  public void setLocked(final boolean lock) {
    contentHolderPanel.setStyleName(style.lock(), lock);
    tabPanel.setLocked(lock);
  }

  @Override
  public void enableDelete(final boolean b) {
    buttonGroup.enableDelete(b);
  }

  @Override
  public void setContentPanel(final IsWidget contentPanel) {
    contentHolderPanel.setWidget(contentPanel);
  }

}
