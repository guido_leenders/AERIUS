/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.widget.SimpleFocusLabel;
import nl.overheid.aerius.wui.main.widget.SimpleTabPanel;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierDetailPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectProjectDossierReviewPlace;
import nl.overheid.aerius.wui.register.place.PriorityProjectSubPlace;
import nl.overheid.aerius.wui.register.place.RequestKeyPlace;

/**
 * A simple UI wrapper for the register tab panel.
 */
@Singleton
public class PriorityProjectProjectDossierTabPanel extends Composite {
  interface PriorityProjectProjectDossierTabPanelUiBinder extends UiBinder<Widget, PriorityProjectProjectDossierTabPanel> {
  }

  private static final PriorityProjectProjectDossierTabPanelUiBinder UI_BINDER = GWT.create(PriorityProjectProjectDossierTabPanelUiBinder.class);

  @UiField SimpleTabPanel tabPanel;
  @UiField SimpleFocusLabel tabProjectInformation;
  @UiField SimpleFocusLabel tabProjectPermitRules;

  private final PlaceController placeController;

  /**
   * Default constructor for the simple {@link SimpleTabPanel} register UI wrapper.
   */
  @Inject
  public PriorityProjectProjectDossierTabPanel(final EventBus eventBus, final PlaceController placeController) {
    this.placeController = placeController;

    initWidget(UI_BINDER.createAndBindUi(this));
    eventBus.addHandler(PlaceChangeEvent.TYPE, new PlaceChangeEvent.Handler() {
      @Override
      public void onPlaceChange(final PlaceChangeEvent event) {
        if (event.getNewPlace() instanceof RequestKeyPlace<?>) {
          setTabFocus((RequestKeyPlace<?>) event.getNewPlace());
        }
      }
    });
  }

  /**
   * Lock the tab panel, causing it to never fire a selection event for lockable
   * tabs and change styling on preset tabs.
   *
   * @param locked True to lock, false to unlock.
   */
  public void setLocked(final boolean locked) {
    tabPanel.setLocked(locked);
  }

  public void setTabFocus(final RequestKeyPlace<?> place) {
    if (place instanceof PriorityProjectProjectDossierDetailPlace) {
      tabPanel.setFocus(tabProjectInformation, false);
    } else {
      tabPanel.setFocus(tabProjectPermitRules, false);
    }
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    tabProjectInformation.ensureDebugId(TestIDRegister.APPLICATION_TAB_SUB_PROJECT_DOSSIER_DETAIL);
    tabProjectPermitRules.ensureDebugId(TestIDRegister.APPLICATION_TAB_SUB_PROJECT_DOSSIER_REVIEW);
  }

  @UiHandler("tabPanel")
  void onTabSelection(final SelectionEvent<Integer> e) {
    if (placeController.getWhere() instanceof RequestKeyPlace) {
      final PriorityProjectSubPlace place = (PriorityProjectSubPlace) placeController.getWhere();
      final Place newPlace;
      switch (e.getSelectedItem()) {
      case 1:
        newPlace = new PriorityProjectProjectDossierReviewPlace(place.getSubKey());
        break;
      case 0:
      default:
        newPlace = new PriorityProjectProjectDossierDetailPlace(place.getSubKey());
        break;
      }
      placeController.goTo(newPlace);
    }
  }

}
