/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 *
 */
public class ExportConfirmDialogPanel extends Composite implements IsWidget {

  interface ExportConfirmDialogPanelUiBinder extends UiBinder<Widget, ExportConfirmDialogPanel> {}

  private static final ExportConfirmDialogPanelUiBinder UI_BINDER = GWT.create(ExportConfirmDialogPanelUiBinder.class);

  @UiField Label explanation;

  private final boolean usesFilterToExport;

  public ExportConfirmDialogPanel(final boolean usesFilterToExport) {
    this.usesFilterToExport = usesFilterToExport;

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public void setAmountToConfirm(final int amountToConfirm, final long filesize) {
    if (amountToConfirm > 0) {
      final long filesizeInMb = Math.round(Long.valueOf(filesize).doubleValue() / SharedConstants.BYTES_TO_MB);
      if (usesFilterToExport) {
        explanation.setText(M.messages().registerExportConfirmPanelExplanation(
            amountToConfirm, filesizeInMb));
      } else {
        explanation.setText(M.messages().registerExportConfirmPanelNoFilterExplanation(amountToConfirm, filesizeInMb));
      }
    } else {
      explanation.setText(
          usesFilterToExport
          ? M.messages().registerExportConfirmPanelExplanationNoItems()
          : M.messages().registerExportConfirmPanelNoFilterExplanationNoItems());
    }
  }

  public void setLoading() {
    explanation.setText(M.messages().registerExportConfirmPanelLoading());
  }

}
