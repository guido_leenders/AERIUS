/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.dashboard;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class NoticeDashboardTable extends Composite implements IsDataTable<DefaultDashboardRow> {

  interface NoticesDashboardTableUiBinder extends UiBinder<Widget, NoticeDashboardTable> {}

  private static final NoticesDashboardTableUiBinder UI_BINDER = GWT.create(NoticesDashboardTableUiBinder.class);

  public interface CustomStyle extends CssResource {
    String specialLabel();
  }

  @UiField(provided = true) SimpleDivTable<DefaultDashboardRow> table;
  @UiField(provided = true) SimpleWidgetFactory<DefaultDashboardRow> statusColumn;
  @UiField(provided = true) TextColumn<DefaultDashboardRow> numberColumn;

  @UiField CustomStyle style;

  public NoticeDashboardTable(final RegisterContext context) {
    table = new SimpleDivTable<DefaultDashboardRow>(true);

    numberColumn = new TextColumn<DefaultDashboardRow>() {
      @Override
      public String getValue(final DefaultDashboardRow object) {
        final double assignedPct = Math.min(object.getAssignedFactor(), 1.0) * SharedConstants.PERCENTAGE_TO_FRACTION;
        return FormatUtil.toWhole(assignedPct);
      }

      @Override
      public void applyCellOptions(final Widget cell, final DefaultDashboardRow object) {
        cell.setStyleName(style.specialLabel(), object.isThresholdChanged());
        super.applyCellOptions(cell, object);
      }
    };

    statusColumn = new SimpleWidgetFactory<DefaultDashboardRow>() {
      @Override
      public Widget createWidget(final DefaultDashboardRow object) {
        return new DashboardBarChart(object, M.messages().registerNoticeDashboardStatusConfirmed());
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public SimpleDivTable<DefaultDashboardRow> asDataTable() {
    return table;
  }
}
