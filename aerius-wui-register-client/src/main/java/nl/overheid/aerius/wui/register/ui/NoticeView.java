/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeSortableAttribute;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.wui.main.widget.table.ColumnSortHeader.SortActionHandler;
import nl.overheid.aerius.wui.main.widget.table.DataTableFetcher;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.widget.ExportRequests.ExportRequestsHandler;

/**
 *
 */
public interface NoticeView extends IsWidget, HasTitle {

  interface Presenter extends DataTableFetcher, SortActionHandler<NoticeSortableAttribute>, ExportRequestsHandler {

    /**
     * Confirm notices.
     */
    void onConfirmNotices();

    /**
     * Adapt the filter used to display applications.
     */
    void onAdaptFilter();

    void onRefresh();

    void onNoticeSelection(Notice notice);

  }

  void setPresenter(Presenter presenter);

  void setDataVisitor(GridScrollVisitor adapter);

  void setLoading(boolean loading);

  void updateListData(ArrayList<Notice> result);

  void startupMap();

  void clear();

  void cleanup();

  void updateConfirmableNotices(int confirmableNotices);

  void updateExportResult(RequestExportResult requestExportResult);
}
