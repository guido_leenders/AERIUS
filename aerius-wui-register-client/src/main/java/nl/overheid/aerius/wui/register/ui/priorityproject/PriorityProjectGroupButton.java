/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.priorityproject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;

public class PriorityProjectGroupButton extends Composite {

  public interface PriorityProjectGroupButtonHandler {
    void exportPriorityProject();
    void deletePriorityProject();
    void assignCompletePriorityProject();
  }

  interface PriorityProjectGroupButtonUiBinder extends UiBinder<Widget, PriorityProjectGroupButton> {}

  private static final PriorityProjectGroupButtonUiBinder UI_BINDER = GWT.create(PriorityProjectGroupButtonUiBinder.class);

  @UiField Button assignCompleteButton;
  @UiField Button exportButton;
  @UiField Button deleteButton;

  private PriorityProjectGroupButtonHandler groupButtonhandler;

  @Inject
  public PriorityProjectGroupButton() {
    initWidget(UI_BINDER.createAndBindUi(this));

    assignCompleteButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_ASSIGNCOMPLETE_BUTTON);
    exportButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_EXPORT_BUTTON);
    deleteButton.ensureDebugId(TestIDRegister.PRIORITYPROJECT_DELETE_BUTTON);
  }

  public void setAssignCompleted() {
    assignCompleteButton.setText(M.messages().registerPriorityProjectAssignComplete());
  }

  public void setAssignInProgress() {
    assignCompleteButton.setText(M.messages().registerPriorityProjectAssignInProgress());
  }

  /**
   * Enable the export button.
   *
   * @param enabled True to enable, false to disable.
   */
  public void enableExport(final boolean enabled) {
    exportButton.setEnabled(enabled);
  }

  /**
   * Enable the delete button.
   *
   * @param enabled True to enable, false to disable.
   */
  public void enableDelete(final boolean enabled) {
    deleteButton.setEnabled(enabled);
  }

  /**
   * Enable the assign complete button.
   *
   * @param enabled True to enable, false to disable.
   */
  public void enableAssignComplete(final boolean enabled) {
    assignCompleteButton.setEnabled(enabled);
  }

  /**
   * @param groupButtonhandler the groupButtonhandler to set
   */
  public void setGroupButtonHandler(final PriorityProjectGroupButtonHandler groupButtonhandler) {
    this.groupButtonhandler = groupButtonhandler;
  }

  @UiHandler("exportButton")
  void onExportButtonClick(final ClickEvent event) {
    groupButtonhandler.exportPriorityProject();
  }

  @UiHandler("deleteButton")
  void onDeleteButtonClick(final ClickEvent event) {
    groupButtonhandler.deletePriorityProject();
  }

  @UiHandler("assignCompleteButton")
  void onAssignCompleteButtonClick(final ClickEvent event) {
    groupButtonhandler.assignCompletePriorityProject();
  }
}
