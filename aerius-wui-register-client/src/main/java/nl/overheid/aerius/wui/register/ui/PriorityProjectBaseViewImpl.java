/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HeadingWidget;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectGroupButton;

public class PriorityProjectBaseViewImpl extends Composite implements PriorityProjectBaseView {
  interface PriorityProjectBaseViewImplUiBinder extends UiBinder<Widget, PriorityProjectBaseViewImpl> {}

  private static final PriorityProjectBaseViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectBaseViewImplUiBinder.class);

  interface CustomStyle extends CssResource {
    String lock();
    String reservationSwitcher();
  }

  @UiField CustomStyle style;

  @UiField HeadingWidget title;

  @UiField(provided = true) RadioButtonGroup<PriorityProjectViewType> viewSwitcher;

  @UiField SimplePanel child;

  @UiField(provided = true) PriorityProjectGroupButton buttonGroup;

  private Presenter presenter;

  @Inject
  public PriorityProjectBaseViewImpl(final RegisterUserContext userContext) {
    viewSwitcher = new RadioButtonGroup<>(new RadioButtonContentResource<PriorityProjectViewType>() {

      @Override
      public String getRadioButtonText(final PriorityProjectViewType value) {
        return M.messages().registerPriorityProjectViewType(value.name());
      }
    });
    buttonGroup = new PriorityProjectGroupButton();

    initWidget(UI_BINDER.createAndBindUi(this));

    viewSwitcher.addButtons(PriorityProjectViewType.values());
  }

  @UiHandler("viewSwitcher")
  public void onViewSwitch(final ValueChangeEvent<PriorityProjectViewType> e) {
    presenter.switchView(e.getValue());
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuPriorityProjectsTitle();
  }

  @Override
  public void setWidget(final IsWidget w) {
    child.setWidget(w);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    buttonGroup.setGroupButtonHandler(presenter);
  }

  @Override
  public void setViewType(final PriorityProjectViewType viewType) {
    viewSwitcher.setValue(viewType, false);
  }

  @Override
  public void setPriorityProject(final PriorityProject priorityProject) {
    title.setText(priorityProject.getScenarioMetaData().getProjectName());

    if (priorityProject.isAssignCompleted()) {
      buttonGroup.setAssignCompleted();
    } else {
      buttonGroup.setAssignInProgress();
    }

    final boolean isActualisation = RequestState.INITIAL == priorityProject.getRequestState();
    viewSwitcher.setVisible(PriorityProjectViewType.PROJECT, !isActualisation);
    viewSwitcher.setVisible(PriorityProjectViewType.APPLICATION,
        priorityProject.getSubProjects().size() == 1 || PriorityProjectViewType.APPLICATION == viewSwitcher.getValue());

    viewSwitcher.setStyleName(style.reservationSwitcher(), !isActualisation);
  }

  @Override
  public void setLocked(final boolean lock) {
    viewSwitcher.setStyleName(style.lock(), lock);
    child.setStyleName(style.lock(), lock);
  }

  @Override
  public void showButtonGroup(final boolean show) {
    buttonGroup.getElement().getStyle().setVisibility(show ? Visibility.VISIBLE : Visibility.HIDDEN);
  }

  @Override
  public void enableDelete(final boolean enable) {
    buttonGroup.enableDelete(enable);
  }

  @Override
  public void enableExport(final boolean enable) {
    buttonGroup.enableExport(enable);
  }

  @Override
  public void enableAssignComplete(final boolean enable) {
    buttonGroup.enableAssignComplete(enable);
  }
}
