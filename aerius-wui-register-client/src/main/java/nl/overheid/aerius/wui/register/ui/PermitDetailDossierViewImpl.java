/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.testing.FakeLeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DataProvider;
import nl.overheid.aerius.wui.register.widget.RequestHistoryPanel;

public class PermitDetailDossierViewImpl extends Composite implements PermitDetailDossierView {
  interface PermitDetailDossierViewDriver extends SimpleBeanEditorDriver<Permit, PermitDetailDossierViewImpl> {}

  private static final PermitDetailDossierViewImplUiBinder UI_BINDER = GWT.create(PermitDetailDossierViewImplUiBinder.class);

  interface PermitDetailDossierViewImplUiBinder extends UiBinder<Widget, PermitDetailDossierViewImpl> {}

  @Ignore @UiField Label handler;
  @Path("dossierMetaData.handler") Editor<UserProfile> handlerEditor = new FakeLeafValueEditor<UserProfile>() {
    @Override
    public void setValue(final UserProfile value) {
      super.setValue(value);

      handler.setText(M.messages().registerUser(value.getLastName(), value.getInitials()));
    }
  };
  @Path("dossierMetaData.dossierId") @UiField Label dossierId;
  @UiField Button editButton;
  @UiField Button downloadDecreeButton;
  @UiField Button downloadDetailDecreeButton;

  @Ignore @UiField Label receivedDate;
  @Path("dossierMetaData.receivedDate") Editor<Date> receivedDateEditor = new FakeLeafValueEditor<Date>() {
    @Override
    public void setValue(final Date value) {
      super.setValue(value);
      receivedDate.setText(value == null ? M.messages().registerApplicationTabNoDate() : FormatUtil.getDefaultDateTimeFormatter().format(value));
    }
  };

  @Ignore @UiField Label remarks;
  @Path("dossierMetaData.remarks") Editor<String> remarksEditor = new FakeLeafValueEditor<String>() {
    @Override
    public void setValue(final String value) {
      super.setValue(value);

      if (value == null) {
        remarks.setText(M.messages().registerApplicationTabNoRemarks());
        return;
      }

      // Replace new lines with html breaks (UI only)
      remarks.getElement().setInnerSafeHtml(new SafeHtmlBuilder().appendEscapedLines(value).toSafeHtml());
    }
  };
  @Path("changeHistory") DataProvider<ArrayList<AuditTrailItem>, AuditTrailItem> historyData;
  @UiField RequestHistoryPanel historyPanel;

  @Path("") LeafValueEditor<Permit> dummyUrlCollector = new FakeLeafValueEditor<>();

  @Path("") LeafValueEditor<Permit> dummyUpdateUrlCollector = new FakeLeafValueEditor<Permit>() {
    @Override
    public void setValue(final Permit permit) {
      updateDecreeDownloadStatus(permit);
    }
  };

  private final ClickHandler editClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (presenter != null) {
        presenter.edit();
      }
    }
  };

  private HandlerRegistration dossierIdHandlerRegistration;
  private HandlerRegistration handlerHandlerRegistration;
  private HandlerRegistration remarksHandlerRegistration;
  private HandlerRegistration receivedDateHandlerRegistration;

  private Presenter presenter;

  @Inject
  public PermitDetailDossierViewImpl() {
    initWidget(UI_BINDER.createAndBindUi(this));

    historyData = new DataProvider<>();
    historyData.addDataDisplay(historyPanel);

    dossierId.ensureDebugId(TestIDRegister.APPLICATION_DOSSIER_ID_VIEWER);
    handler.ensureDebugId(TestIDRegister.APPLICATION_HANDLER_VIEWER);
    remarks.ensureDebugId(TestIDRegister.APPLICATION_REMARKS_VIEWER);
    editButton.ensureDebugId(TestIDRegister.BUTTON_EDIT_APPLICATION);
    downloadDecreeButton.ensureDebugId(TestIDRegister.BUTTON_DOWNLOAD_ANNEX_APPLICATION);

  }

  private void updateDecreeDownloadStatus(final Permit permit) {
    updateDownloadButton(permit, downloadDecreeButton, RequestFileType.DECREE);
    updateDownloadButton(permit, downloadDetailDecreeButton, RequestFileType.DETAIL_DECREE);
  }

  private void updateDownloadButton(final Permit permit, final Button button, final RequestFileType requestFileType) {
    boolean enableButton = false;
    if (!Arrays.asList(RequestState.DECREE_DOWNLOAD_STATUS).contains(permit.getRequestState())) {
      button.setTitle(M.messages().registerApplicationTabDownloadDecreeInfo());
    } else if (!permit.hasRequestFile(requestFileType)) {
      button.setTitle(M.messages().registerApplicationTabDownloadDecreeInfoNotReady());
    } else {
      enableButton = true;
    }
    button.setEnabled(enableButton);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @UiHandler("editButton")
  void onEditClick(final ClickEvent e) {
    presenter.edit();
  }

  @UiHandler("downloadDecreeButton")
  void onDownloadDecreeClick(final ClickEvent e) {
    presenter.downloadDecree(dummyUrlCollector.getValue().getUrl(RequestFileType.DECREE));
  }

  @UiHandler("downloadDetailDecreeButton")
  void onDownloadDetailDecreeClick(final ClickEvent e) {
    presenter.downloadDecree(dummyUrlCollector.getValue().getUrl(RequestFileType.DETAIL_DECREE));
  }

  public void setUserCanEdit(final boolean hasPermission) {
    editButton.setVisible(hasPermission);
  }

  public void setLocked(final boolean lock) {
    editButton.setEnabled(!lock);
  }

  @Override
  public void enableDossierId(final boolean hasPermission) {
    dossierIdHandlerRegistration = enableInputElement(dossierIdHandlerRegistration, dossierId, hasPermission);
  }

  @Override
  public void enableHandler(final boolean hasPermission) {
    handlerHandlerRegistration = enableInputElement(handlerHandlerRegistration, handler, hasPermission);
  }

  @Override
  public void enableRemarks(final boolean hasPermission) {
    remarksHandlerRegistration = enableInputElement(remarksHandlerRegistration, remarks, hasPermission);
  }

  @Override
  public void enableDate(final boolean hasPermission) {
    receivedDateHandlerRegistration = enableInputElement(receivedDateHandlerRegistration, receivedDate, hasPermission);
  }

  /**
   * @param handlerRegistration
   * @param widget
   * @param hasPermission
   */
  private HandlerRegistration enableInputElement(final HandlerRegistration handlerRegistration, final Label widget, final boolean hasPermission) {
    if (handlerRegistration != null) {
      handlerRegistration.removeHandler();
    }
    final HandlerRegistration newHandlerRegistration;
    if (hasPermission) {
      newHandlerRegistration = widget.addClickHandler(editClickHandler);
      widget.addStyleName(R.css().registerEditableIndicator());
    } else {
      newHandlerRegistration = null;
    }
    return newHandlerRegistration;
  }


}
