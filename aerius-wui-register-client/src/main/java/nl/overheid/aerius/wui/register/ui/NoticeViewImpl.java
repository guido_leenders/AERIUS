/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeConfirmationSetting;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.domain.user.AuthoritySetting;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.geo.LayerPanel;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButton;
import nl.overheid.aerius.wui.main.widget.AttachedPopupButtonGroup;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.geo.NoticeMarkerLayerWrapper;
import nl.overheid.aerius.wui.register.geo.RegisterCoreMapLayoutPanel;
import nl.overheid.aerius.wui.register.ui.filter.NoticeFilterPanel;
import nl.overheid.aerius.wui.register.ui.notice.NoticeConfirmDialogPanel;
import nl.overheid.aerius.wui.register.ui.notice.NoticesMapTable;
import nl.overheid.aerius.wui.register.widget.ExportRequests;

/**
 *
 */
public class NoticeViewImpl extends Composite implements NoticeView {
  private final static NoticeViewImplUiBinder UI_BINDER = GWT.create(NoticeViewImplUiBinder.class);

  interface NoticeViewImplUiBinder extends UiBinder<Widget, NoticeViewImpl> {}

  private final ConfirmHandler<Void> onNoticeConfirmHandler = new ConfirmHandler<Void>() {
    @Override
    public void onConfirm(final ConfirmEvent<Void> event) {
      presenter.onConfirmNotices();
      confirmNoticesDialog.hide();
    }
  };

  private final ValueChangeHandler<NoticeFilter> changeHandler = new ValueChangeHandler<NoticeFilter>() {
    @Override
    public void onValueChange(final ValueChangeEvent<NoticeFilter> event) {
      presenter.onAdaptFilter();
    }
  };

  @UiField Button noticeRefreshButton;
  @UiField Button noticeExportButton;
  @UiField Button noticesConfirmButton;
  @UiField(provided = true) NoticesMapTable noticesMapTable;
  @UiField AttachedPopupButtonGroup buttonGroup;
  @UiField AttachedPopupButton layerPanelButton;
  @UiField(provided = true) RegisterCoreMapLayoutPanel mapPanel;
  @UiField(provided = true) LayerPanel layerPanel;
  @UiField(provided = true) NoticeFilterPanel noticeFilter;

  private final ConfirmCancelDialog<Void, LeafValueEditor<Void>> confirmNoticesDialog =
      new ConfirmCancelDialog<Void, LeafValueEditor<Void>>(M.messages().registerNoticeConfirmPanelButton(), M.messages().cancelButton());
  final NoticeConfirmDialogPanel confirmNoticesDialogContent = new NoticeConfirmDialogPanel();

  private NoticeView.Presenter presenter;
  private final NoticeMarkerLayerWrapper noticeLayer;
  private com.google.web.bindery.event.shared.HandlerRegistration mapHandlers;
  private final ExportRequests exportRequests;
  private final EventBus eventBus;

  @Inject
  public NoticeViewImpl(final RegisterUserContext userContext, final RegisterContext context, final EventBus eventBus, final HelpPopupController hpC,
      final RegisterCoreMapLayoutPanel mapPanel, final LayerPanel layerPanel, final NoticeFilterPanel noticeFilter) {
    this.mapPanel = mapPanel;
    this.layerPanel = layerPanel;
    this.eventBus = eventBus;

    noticesMapTable = new NoticesMapTable(eventBus);
    exportRequests = new ExportRequests(true);

    this.noticeFilter = noticeFilter;
    this.noticeFilter.addValueChangeHandler(changeHandler);
    this.noticeFilter.setValue(userContext.getFilter(NoticeFilter.class));

    initWidget(UI_BINDER.createAndBindUi(this));

    noticeExportButton.setVisible(userContext.getUserProfile().hasPermission(RegisterPermission.EXPORT_REQUESTS));

    final NoticeConfirmationSetting confirmationSetting = NoticeConfirmationSetting.safeValueOf(
        userContext.getUserProfile().getAuthority().getSettings().get(AuthoritySetting.NO_PERMIT_NOTICE_CONFIRMATION));
    noticesConfirmButton.setVisible(userContext.getUserProfile().hasPermission(RegisterPermission.CONFIRM_PRONOUNCEMENT)
        && confirmationSetting == NoticeConfirmationSetting.BATCH_CONFIRM);

    hpC.addWidget(noticeRefreshButton, hpC.tt().ttRegisterRefresh());
    hpC.addWidget(noticeExportButton, hpC.tt().ttRegisterExportNotices());
    noticesMapTable.ensureDebugId(TestIDRegister.NOTICES_CELLTABLE);
    noticeRefreshButton.ensureDebugId(TestIDRegister.NOTICES_REFRESH_BUTTON);
    noticeExportButton.ensureDebugId(TestIDRegister.NOTICES_EXPORT_BUTTON);
    noticesConfirmButton.ensureDebugId(TestIDRegister.NOTICES_CONFIRM_BUTTON);
    layerPanelButton.ensureDebugId(TestID.BUTTON_LAYERPANEL);

    noticesMapTable.asDataTable().addSelectionChangeHandler(new Handler() {

      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        presenter.onNoticeSelection(((SingleSelectionModel<Notice>) noticesMapTable.asDataTable().getSelectionModel()).getSelectedObject());
      }
    });

    confirmNoticesDialog.setHTML(M.messages().registerNoticeConfirmPanelTitle());
    confirmNoticesDialog.setWidget(confirmNoticesDialogContent);
    confirmNoticesDialog.addConfirmHandler(onNoticeConfirmHandler);

    noticeLayer = new NoticeMarkerLayerWrapper(mapPanel);

    // TODO Add filterchange event listener of some sort that calls the presenter with the filter
  }

  @Override
  public void setPresenter(final NoticeView.Presenter presenter) {
    this.presenter = presenter;

    noticesMapTable.setSortHandler(presenter);
    exportRequests.setHandler(presenter);
  }

  @UiHandler("noticeRefreshButton")
  void onNoticeRefreshButton(final ClickEvent event) {
    presenter.onRefresh();
  }

  @UiHandler("noticeExportButton")
  void onNoticeExportButton(final ClickEvent event) {
    exportRequests.onExportButtonClick();
  }

  @UiHandler("noticesConfirmButton")
  void onConfirmNoticesButton(final ClickEvent event) {
    confirmNoticesDialog.center();
  }

  @Override
  public void clear() {
    noticesMapTable.asDataTable().clear();
  }

  @Override
  public void updateListData(final ArrayList<Notice> result) {
    noticesMapTable.asDataTable().addRowData(result);
  }

  @Override
  public void setDataVisitor(final GridScrollVisitor adapter) {
    noticesMapTable.asDataTable().exposeToVisitor(adapter);
  }

  @Override
  public void setLoading(final boolean loading) {
    noticesMapTable.asDataTable().showLoadingWidget(loading);
  }

  @Override
  public void startupMap() {
    mapPanel.getMarkersLayer().clear();
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        mapHandlers = mapPanel.attachEventBinders();
        mapPanel.nudge();
        noticeLayer.attach();

        // Reset to default position, every time
        eventBus.fireEvent(new LocationChangeEvent());
      }
    });
  }

  @Override
  public void cleanup() {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        if (mapHandlers != null) {
          mapHandlers.removeHandler();
        }
        noticeLayer.detach();
      }
    });
  }

  @Override
  public void updateConfirmableNotices(final int confirmableNotices) {
    confirmNoticesDialogContent.setAmountToConfirm(confirmableNotices);
  }

  @Override
  protected void onUnload() {
    super.onUnload();
    buttonGroup.deactivate();
  }

  @Override
  public String getTitleText() {
    return M.messages().registerNoticesTitle();
  }

  @Override
  public void updateExportResult(final RequestExportResult requestExportResult) {
    exportRequests.updateExportResult(requestExportResult);
  }
}
