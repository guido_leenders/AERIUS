/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.importer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;

public abstract class RegisterActualisationDialogController extends RegisterImportDialogController<PriorityProjectKey> {

  private static class ActualisationImportPollingAgent extends RequestImportPollingAgent<PriorityProjectKey> {

    ActualisationImportPollingAgent(final RegisterRetrieveImportServiceAsync service) {
      super(service);
    }

    @Override
    protected void callService(final String key, final AsyncCallback<PriorityProjectKey> resultCallback) {
      service.getPriorityProjectActualisationResult(key, resultCallback);
    }
  }

  private static final String UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_ACTUALISATION_SERVLET;

  public RegisterActualisationDialogController(final PlaceController placeController, final RegisterRetrieveImportServiceAsync service,
      final RegisterImportDialogPanel<PriorityProjectKey> content, final String reference) {
    super(placeController, content, UPLOAD_ACTION, new ActualisationImportPollingAgent(service));

    content.setId(reference);
  }
}
