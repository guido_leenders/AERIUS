/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitDossierEditPlace;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;
import nl.overheid.aerius.wui.register.ui.PermitDetailDossierEditImpl.PermitDetailDossierEditDriver;

/**
 *
 */
public class PermitDetailDossierEditActivity extends PermitDetailDossierBaseActivity<PermitDossierEditPlace> {

  private final PermitDetailDossierEditDriver driver = GWT.create(PermitDetailDossierEditDriver.class);

  private PermitKey updatedPermitKey;
  private boolean newPermitSaved;

  private final AsyncCallback<Void> updateDossierCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      newPermitSaved = true;

      // Use permit because if permit key is updated place key is outdated.
      goTo(new PermitDossierPlace(updatedPermitKey));
    }

    @Override
    public void onFailure(final Throwable caught) {
      setLocked(false);
      // If the application is already updated by someone else, fetch the latest data, losing all changes made
      if (caught instanceof AeriusException) {
        final AeriusException exp = (AeriusException) caught;
        if (exp.getReason().equals(Reason.PERMIT_ALREADY_UPDATED)) {
          reloadCurrentPermit();
        }
      }

      super.onFailure(caught);
    }
  };

  @Inject
  public PermitDetailDossierEditActivity(final RegisterAppContext appContext, final PermitDetailView view,
      final PermitDetailDossierEditImpl contentPanel, @Assisted final PermitDossierEditPlace place) {
    super(appContext, view, contentPanel, place);
    driver.initialize(contentPanel);
  }

  @Override
  public String mayStop() {
    return driver.isDirty() && !newPermitSaved ? M.messages().registerApplicationTabAppExitWhileInEdit() : null;
  }

  @Override
  protected void onPermitLoaded(final Permit permit) {
    super.onPermitLoaded(permit);

    newPermitSaved = false;
    driver.edit(permit);
  }

  @Override
  public void cancel() {
    // Reload application with current server version
    appContext.invalidatePermit();
    History.back();
  }

  @Override
  public void save() {
    // Use the old (current) permit key; the server doesn't know about the possibly updated permit key values yet (dossier id, authority)
    final PermitKey currentPermitKey = permit.getPermitKey();

    final Permit newPermit = driver.flush();

    // Display errors and stop saving if there are any
    if (driver.hasErrors()) {
      ErrorPopupController.addErrors(driver.getErrors());
      return;
    } else {
      ErrorPopupController.clearWidgets();
    }

    updatedPermitKey = newPermit.getPermitKey();

    // Switch a flag indicating we need a place change after the update
    appContext.invalidatePermit();

    // Update to the server
    appContext.getPermitService().update(currentPermitKey, newPermit.getDossierMetaData(), permit.getLastModified(), updateDossierCallback);
  }

  @Override
  public void edit() {
    // no-op
  }
}
