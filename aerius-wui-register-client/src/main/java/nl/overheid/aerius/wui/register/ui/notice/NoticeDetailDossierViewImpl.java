/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.notice;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DataProvider;
import nl.overheid.aerius.wui.register.widget.RequestHistoryPanel;

/**
 * Show Notice details.
 */
public class NoticeDetailDossierViewImpl extends Composite implements NoticeDetailDossierView {

  interface NoticeDetailDossierViewImplUiBinder extends UiBinder<Widget, NoticeDetailDossierViewImpl> {}

  private static final NoticeDetailDossierViewImplUiBinder UI_BINDER = GWT.create(NoticeDetailDossierViewImplUiBinder.class);

  private Presenter presenter;

  DataProvider<ArrayList<AuditTrailItem>, AuditTrailItem> historyData;
  @UiField RequestHistoryPanel historyPanel;

  @UiField Label initiator;
  @UiField Label authority;
  @UiField Label location;
  @UiField Label sourceDescription;
  @UiField Anchor reference;
  @UiField Label description;
  @UiField Button openInCalculatorButton;
  @UiField Label receivedDate;

  private Notice notice;

  @Inject
  public NoticeDetailDossierViewImpl(final Context context) {
    initWidget(UI_BINDER.createAndBindUi(this));

    historyData = new DataProvider<>();
    historyData.addDataDisplay(historyPanel);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setData(final Notice notice) {
    this.notice = notice;
    initiator.setText(notice.getScenarioMetaData().getCorporation());
    authority.setText(notice.getAuthority().getDescription());
    location.setText(notice.getProvince().getName());
    sourceDescription.setText(notice.getSector().getDescription());
    reference.setText(notice.getReference());
    description.setText(notice.getScenarioMetaData().getDescription());
    receivedDate.setText(notice.getReceivedDate() == null ? M.messages().registerApplicationTabNoDate()
        : FormatUtil.getDefaultDateTimeFormatter().format(notice.getReceivedDate()));

    historyData.setValue(notice.getChangeHistory());
  }

  @UiHandler({"pdfDownloadImage", "reference"})
  public void onPdfDownloadImage(final ClickEvent event) {
    presenter.openRequestFile(notice);
  }

  @UiHandler({"openInCalculatorButton"})
  public void onOpenInCalculatorButton(final ClickEvent event) {
    presenter.openInCalculator(notice);
  }

}
