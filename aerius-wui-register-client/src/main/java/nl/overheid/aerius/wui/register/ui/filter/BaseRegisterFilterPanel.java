/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.filter;

import java.util.HashSet;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.RequestFilter;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.filter.BaseFilterPanel;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.MultiSelectListBox;
import nl.overheid.aerius.wui.main.widget.SelectImageOptionFactory;
import nl.overheid.aerius.wui.main.widget.SelectOption;
import nl.overheid.aerius.wui.main.widget.SelectTextOptionFactory;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;

/**
 * Base class for Register Filters.
 * @param <F> actual filter subclass.
 */
public abstract class BaseRegisterFilterPanel<F extends RequestFilter<?>> extends BaseFilterPanel<F> {
  @UiField DateBox fromEditor;
  @UiField Button pickFromDateButton;
  @UiField DateBox tillEditor;
  @UiField Button pickTillDateButton;

  @UiField MultiSelectListBox<Province> locationListBox;
  @UiField(provided = true) WidgetFactory<Province, SelectOption<Province>> locationOptionTemplate =
      new SelectTextOptionFactory<Province>() {
    @Override
    public String getItemText(final Province value) {
      return value.getName();
    }
  };

  @UiField MultiSelectListBox<Sector> sectorListBox;
  @UiField(provided = true) WidgetFactory<Sector, SelectOption<Sector>> sectorOptionTemplate =
      new SelectImageOptionFactory<Sector>() {
    @Override
    public String getItemText(final Sector value) {
      return value.getName();
    }

    @Override
    public ImageResource getImageResource(final Sector sector) {
      return SectorImageUtil.getSectorImageResource(sector.getProperties().getIcon());
    }
  };

  private final DateBox.Format format = new DateBox.DefaultFormat(DateTimeFormat.getFormat(M.messages().dateFormat()));
  private final RegisterContext context;

  protected BaseRegisterFilterPanel(final RegisterContext context) {
    this.context = context;
  }

  @Override
  protected void initWidget(final Widget widget) {
    super.initWidget(widget);

    initDateBox(fromEditor, pickFromDateButton);
    initDateBox(tillEditor, pickTillDateButton);
    locationListBox.setValues(context.getProvinces());
    sectorListBox.setValues(context.getCategories().getSectors());

    fromEditor.ensureDebugId(TestIDRegister.INPUT_FILTER_FROM);
    tillEditor.ensureDebugId(TestIDRegister.INPUT_FILTER_TILL);
    locationListBox.ensureDebugId(TestIDRegister.INPUT_FILTER_LOCATION);
    sectorListBox.ensureDebugId(TestIDRegister.INPUT_FILTER_SECTOR);
  }

  private void initDateBox(final DateBox dateBox, final Button partnerButton) {
    dateBox.setFormat(format);
    dateBox.getDatePicker().setYearArrowsVisible(true);
    dateBox.getPopup().addAutoHidePartner(partnerButton.getElement());
    dateBox.getPopup().getElement().getStyle().setZIndex(R.css().zIndexGeneric());
  }

  @Override
  public void setValue(final F filter) {
    fromEditor.setValue(filter.getFrom());
    tillEditor.setValue(filter.getTill());
    locationListBox.setSelectedValues(filter.getProvinces(), true);
    sectorListBox.setSelectedValues(filter.getSectors(), true);
    filter.setAssessmentAreas(new HashSet<AssessmentArea>());
    super.setValue(filter);
  }

  @Override
  protected String getTitleLabel() {
    final String province = locationListBox.getHeaderText(value.getProvinces());

    final String from = FormatUtil.formatDate(value.getFrom());
    final String till = FormatUtil.formatDate(value.getTill());

    return getTitleLabel(province, from, till, M.messages().nullDateDefault().equals(from), value.getMarked());
  }

  protected abstract String getTitleLabel(String province, String from, String till, boolean fromIsDefault, Boolean marked);

  private boolean validateDate() {
    if (fromEditor.getValue() != null && tillEditor.getValue() != null
        && fromEditor.getValue().after(tillEditor.getValue())) {
      tillEditor.setValue(null);
      return false;
    }

    return true;
  }

  @UiHandler("pickFromDateButton")
  void onPickFromDateButton(final ClickEvent event) {
    toggleDateBoxVisiblity(fromEditor);
  }

  @UiHandler("pickTillDateButton")
  void onPickTillDateButton(final ClickEvent event) {
    toggleDateBoxVisiblity(tillEditor);
  }

  private void toggleDateBoxVisiblity(final DateBox dateBox) {
    if (dateBox.isDatePickerShowing()) {
      dateBox.hideDatePicker();
    } else {
      dateBox.showDatePicker();
    }
  }

  protected void updateProvince() {
    value.setProvinces(locationListBox.getSelectedValues());
  }

  protected void updateDate() {
    value.setFrom(fromEditor.getValue());
    value.setTill(tillEditor.getValue());
  }

  protected void updateSector() {
    value.setSectors(sectorListBox.getSelectedValues());
  }

  protected void updateArea() {
    value.setAssessmentAreas(new HashSet<AssessmentArea>());
  }

  @Override
  protected void submit() {
    if (validateDate()) {
      super.submit();
    }
  }

  @Override
  protected void updateFilter() {
    updatePermitData();
  }

  protected void updatePermitData() {
    updateProvince();
    updateDate();
    updateSector();
    updateArea();
  }
}
