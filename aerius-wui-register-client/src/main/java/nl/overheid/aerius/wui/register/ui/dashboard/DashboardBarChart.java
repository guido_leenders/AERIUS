/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.dashboard;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.charts.BarChart;
import nl.overheid.aerius.wui.main.widget.charts.BarChart.Bar;
import nl.overheid.aerius.wui.main.widget.charts.TextBarChart;

public class DashboardBarChart extends Composite {

  interface DashboardBarChartUiBinder extends UiBinder<Widget, DashboardBarChart> {
  }

  private static final DashboardBarChartUiBinder UI_BINDER = GWT.create(DashboardBarChartUiBinder.class);

  @UiField(provided = true) BarChart<Bar> barChart;

  @UiField SimplePanel line;

  public DashboardBarChart(final DefaultDashboardRow object, final String assignedLabel) {
    barChart = new TextBarChart(object.getAssessmentArea().getName());

    final double assignedPct = Math.min(object.getAssignedFactor(), 1.0) * SharedConstants.PERCENTAGE_TO_FRACTION;
    final double pendingPct = Math.min(object.getPendingFactor(), 1.0) * SharedConstants.PERCENTAGE_TO_FRACTION;
    final double remainingPct = SharedConstants.PERCENTAGE_TO_FRACTION - assignedPct - pendingPct;

    final Bar assignedBar = new Bar("cfc5e5", assignedLabel, assignedPct);
    final Bar pendingBar = new Bar("e7e2f2", M.messages().registerNoticeDashboardStatusPending(), pendingPct);
    final Bar remainingBar = new Bar("transparent", M.messages().registerNoticeDashboardStatusRemaining(), remainingPct);

    barChart.addBar(assignedBar);
    barChart.addBar(pendingBar);
    barChart.addBar(remainingBar);
    barChart.draw();

    initWidget(UI_BINDER.createAndBindUi(this));
  }

}
