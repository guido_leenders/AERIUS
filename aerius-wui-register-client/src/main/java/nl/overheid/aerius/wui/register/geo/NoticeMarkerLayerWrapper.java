/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.geo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.gwtopenmaps.openlayers.client.Marker;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.BaseMarkerLayerWrapper;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.geo.ClusteredReceptorPoint;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeMapData;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.util.ClusterUtil;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.main.event.NoticeMapDataRetrievedEvent;
import nl.overheid.aerius.wui.main.event.NoticeMapMarkersPurgeEvent;
import nl.overheid.aerius.wui.register.event.NoticeSelectedEvent;
import nl.overheid.aerius.wui.register.geo.NoticeMarker.NoticeMarkerType;

/**
 * Layer to show source objects of a Notice(Melding) as markers on a map.
 */
public class NoticeMarkerLayerWrapper extends BaseMarkerLayerWrapper<MarkerItem<?>> {
  interface NoticeMarkerLayerEventBinder extends EventBinder<NoticeMarkerLayerWrapper> {}

  private final NoticeMarkerLayerEventBinder eventBinder = GWT.create(NoticeMarkerLayerEventBinder.class);

  private static final String NOTICE_MARKERS_NAME = "noticeMarkers";

  private final ArrayList<MarkerItem<?>> items = new ArrayList<>();

  private MarkerItem<?> highlightedMarker;

  private MarkerItem<?> selectedMarker;

  public NoticeMarkerLayerWrapper(final MapLayoutPanel map) {
    super(map, NOTICE_MARKERS_NAME);
    eventBinder.bindEventHandlers(this, map.getEventBus());
  }

  @EventHandler
  void onNoticeSelected(final NoticeSelectedEvent event) {
    final Notice selected = event.getValue();

    if (event.getNoticeMarkerType() == NoticeMarkerType.HIGHLIGHT) {
      highlight(addMarker(selected));
    } else if (event.getNoticeMarkerType() == NoticeMarkerType.SELECTED)  {
      selected(addMarker(selected));
    } else {
      resetMarker(selected);
    }
  }

  private void resetMarker(final Notice selected) {
    if (highlightedMarker != null && selected.getId() == highlightedMarker.getId()) {
      highlightedMarker = null;

    } else if (selectedMarker != null && selected.getId() == selectedMarker.getId()) {
      selectedMarker = null;
    }
    redraw(true);
  }

  private void highlight(final MarkerItem<?> item) {
    highlightedMarker = item;

    redraw(true);
  }

  private void selected(final MarkerItem<?> item) {
    selectedMarker = item;

    redraw(true);
  }

  @EventHandler
  void onNoticeMapDataRetrieved(final NoticeMapDataRetrievedEvent event) {
    purgeAllNoticeMarkers(null);
    addMarkers(event.getValue());

    // Hard redraw
    redraw(false);
  }

  @EventHandler
  void purgeAllNoticeMarkers(final NoticeMapMarkersPurgeEvent e) {
    items.clear();

    // Hard redraw
    redraw(false);
  }

  private void addMarkers(final NoticeMapData data) {
    for (final Notice notice : data.getNotices()) {
      addMarker(notice);
    }
  }

  private MarkerItem<?> addMarker(final Request request) {
    final MarkerItem<?> marker = request instanceof Notice ? new NoticeMarkerItem((Notice) request) : new RequestMarkerItem(request);

    items.add(marker);
    return marker;
  }

  @Override
  protected Marker createMarker(final ClusteredReceptorPoint<MarkerItem<?>> p) {
    final NoticeMarker marker = new NoticeMarker(p);

    if (p.getPoints().contains(highlightedMarker)) {
      marker.setMarkerItems(p.getPoints(), NoticeMarkerType.HIGHLIGHT);
    } else if (p.getPoints().contains(selectedMarker)) {
      marker.setMarkerItems(p.getPoints(), NoticeMarkerType.SELECTED);
    } else {
      marker.setMarkerItems(p.getPoints(), NoticeMarkerType.NORMAL);
    }

    return marker.getMarker();
  }

  /**
   * Make sure we don't cluster. I didn't make this up.
   */
  @Override
  protected double getDistanceForScale(final double scale) {
    return Double.MAX_VALUE;
  }

  @Override
  protected Set<ClusteredReceptorPoint<MarkerItem<?>>> createClusters(final double scale) {
    final HashSet<ClusteredReceptorPoint<MarkerItem<?>>> clusters = new HashSet<>();
    for (final MarkerItem<?> item : items) {
      final ArrayList<MarkerItem<?>> list = new ArrayList<>();
      list.add(item);
      clusters.addAll(ClusterUtil.clusterGenericPoints(list, scale));
    }

    return clusters;
  }

}
