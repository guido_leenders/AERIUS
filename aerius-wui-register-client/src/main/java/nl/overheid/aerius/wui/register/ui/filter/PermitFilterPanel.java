/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui.filter;

import java.util.Locale;
import java.util.Set;

import javax.inject.Singleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.MultiSelectListBox;
import nl.overheid.aerius.wui.main.widget.SelectOption;
import nl.overheid.aerius.wui.main.widget.SelectScalableImageOptionFactory;
import nl.overheid.aerius.wui.main.widget.SelectTextOptionFactory;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;
import nl.overheid.aerius.wui.register.util.PermitStateUtil;
import nl.overheid.aerius.wui.register.util.RequestMarkedUtil;

/**
 *  Filter Panel for Permits.
 */
@Singleton
public class PermitFilterPanel extends BaseRegisterFilterPanel<PermitFilter> {
  private static final PermitFilterPanelUiBinder UI_BINDER = GWT.create(PermitFilterPanelUiBinder.class);

  interface PermitFilterPanelUiBinder extends UiBinder<Widget, PermitFilterPanel> {}

  private static final int STATUS_LIST_BOX_IMAGE_WIDTH = 31;
  private static final int MARKED_LIST_BOX_IMAGE_WIDTH = 31;

  @UiField MultiSelectListBox<RequestState> statusListBox;
  @UiField(provided = true) WidgetFactory<RequestState, SelectOption<RequestState>> statusOptionTemplate =
      new SelectScalableImageOptionFactory<RequestState>(STATUS_LIST_BOX_IMAGE_WIDTH) {
    @Override
    public String getItemText(final RequestState value) {
      return M.messages().registerRequestState(value);
    }

    @Override
    public DataResource getDataResource(final RequestState state) {
      return PermitStateUtil.getPermitStateIcon(state);
    }
  };

  @UiField MultiSelectListBox<Authority> authorityListBox;
  @UiField(provided = true) WidgetFactory<Authority, SelectOption<Authority>> authorityOptionTemplate = new SelectTextOptionFactory<Authority>() {
    @Override
    public String getItemText(final Authority value) {
      return value.getDescription();
    }
  };

  @UiField MultiSelectListBox<Boolean> markedListBox;
  @UiField(provided = true) WidgetFactory<Boolean, SelectOption<Boolean>> markedOptionTemplate =
      new SelectScalableImageOptionFactory<Boolean>(MARKED_LIST_BOX_IMAGE_WIDTH) {
    @Override
    public String getItemText(final Boolean value) {
      return M.messages().registerRequestMarked(value);
    }

    @Override
    public DataResource getDataResource(final Boolean marked) {
      return RequestMarkedUtil.getRequestMarkedIcon(marked);
    }
  };

  @Inject
  public PermitFilterPanel(final RegisterContext context, final RegisterUserContext userContext) {
    super(context);
    initWidget(UI_BINDER.createAndBindUi(this));
    statusListBox.setValues(RequestState.FILTER_VALUES);
    authorityListBox.setValues(userContext.getHandlers().keySet());
    markedListBox.addValue(Boolean.TRUE);
    markedListBox.addValue(Boolean.FALSE);

    statusListBox.ensureDebugId(TestIDRegister.INPUT_FILTER_STATUS);
    authorityListBox.ensureDebugId(TestIDRegister.INPUT_FILTER_AUTHORITY);
    markedListBox.ensureDebugId(TestIDRegister.INPUT_FILTER_MARKED);
  }

  @Override
  public void setValue(final PermitFilter filter) {
    statusListBox.setSelectedValues(filter.getStates(), true);
    authorityListBox.setSelectedValues(filter.getAuthorities(), true);
    markedListBox.setSelectedValue(filter.getMarked(), true);

    super.setValue(filter);
  }

  @Override
  protected String getTitleLabel(final String province, final String from, final String till, final boolean fromIsDefault, final Boolean marked) {
    final String status = statusListBox.getHeaderText(statusListBox.getSelectedValues());
    final String authority = authorityListBox.getHeaderText(authorityListBox.getSelectedValues());
    final String sector = sectorListBox.getHeaderText(sectorListBox.getSelectedValues());
    String title;
    if (M.messages().nullDateDefault().equals(from)) {
      title = M.messages().registerPermitFilterCollapseTitleNoDate(status, province, authority, sector);
    } else {
      title = M.messages().registerPermitFilterCollapseTitle(status, province, from, till, authority, sector);
    }
    if (marked != null) {
      title += M.messages().registerPermitFilterCollapseTitleMarkedAddendum(M.messages().registerRequestMarked(marked).toLowerCase(Locale.ENGLISH));
    }
    return title;
  }

  @Override
  protected void updatePermitData() {
    super.updatePermitData();
    updateStatus();
    updateAuthority();
    updateMarked();
  }

  protected void updateStatus() {
    getValue().setStates(statusListBox.getSelectedValues());
  }

  protected void updateAuthority() {
    getValue().setAuthorities(authorityListBox.getSelectedValues());
  }

  protected void updateMarked() {
    final Set<Boolean> selected = markedListBox.getSelectedValues();
    getValue().setMarked(selected.isEmpty() ? null : selected.iterator().next());
  }
}
