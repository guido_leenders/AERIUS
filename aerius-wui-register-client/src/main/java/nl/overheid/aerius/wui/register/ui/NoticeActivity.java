/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.NoticeKey;
import nl.overheid.aerius.shared.domain.register.NoticeMapData;
import nl.overheid.aerius.shared.domain.register.NoticeSortableAttribute;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.service.RequestExportServiceAsync;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.geo.LayerConfigPanel;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.event.NoticeMapDataRetrievedEvent;
import nl.overheid.aerius.wui.main.event.NoticeMapMarkersPurgeEvent;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.NoticeDossierPlace;

/**
 * Activity for overview of 'Meldingen'.
 */
public class NoticeActivity extends AdminAbstractActivity<RegisterAppContext> implements NoticeView.Presenter {

  private final class UpdateAppAsyncCallback extends AppAsyncCallback<ArrayList<Notice>> {
    private final boolean fetchingInitial;

    private UpdateAppAsyncCallback(final boolean fetchingInitial) {
      this.fetchingInitial = fetchingInitial;
    }

    @Override
    public void onSuccess(final ArrayList<Notice> result) {
      if (activeCallback == this) {
        if (result.isEmpty()) {
          view.setLoading(false);
          // If we are busy fetching additional entries - which may be empty - we shouldn't purge the markers on the map.
          if (fetchingInitial) {
            fireEvent(new NoticeMapMarkersPurgeEvent());
          }
        } else {
          view.updateListData(result);
          fireEvent(new NoticeMapDataRetrievedEvent(new NoticeMapData(result)));
        }
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      if (activeCallback == this) {
        onSuccess(new ArrayList<Notice>());
        super.onFailure(caught);
      }
    }
  }

  private static final int MAX_SIZE = 50;


  private final AsyncCallback<ArrayList<Integer>> confirmableNoticesCallBack = new AppAsyncCallback<ArrayList<Integer>>() {

    @Override
    public void onSuccess(final ArrayList<Integer> result) {
      confirmableNoticeIds = result;
      view.updateConfirmableNotices(result.size());
    }
  };

  private final NoticeView view;
  private final LayerConfigPanel layerConfigPanel;
  private ArrayList<Integer> confirmableNoticeIds;
  private NoticeFilter filter;
  private AppAsyncCallback<ArrayList<Notice>> activeCallback;
  private RequestExportResult requestExportResult;

  @Inject
  public NoticeActivity(final RegisterAppContext appContext, final NoticeView view, final RequestExportServiceAsync requestExportService,
      final LayerConfigPanel layerConfigPanel) {
    super(appContext, RegisterPermission.VIEW_PRONOUNCEMENTS);

    this.view = view;
    this.layerConfigPanel = layerConfigPanel;
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    panel.setWidget(view);
    view.setDataVisitor(new GridScrollVisitor(this));

    filter = appContext.getUserContext().getFilter(NoticeFilter.class);

    appContext.getNoticeService().getConfirmableNoticeIds(confirmableNoticesCallBack);
    loadData(0);
    view.startupMap();
  }

  @Override
  public void onStop() {
    CloseEvent.fire(layerConfigPanel, true);
    view.cleanup();
  }

  @Override
  public void loadData(final int offset) {
    activeCallback = new UpdateAppAsyncCallback(offset == 0);
    view.setLoading(true);
    appContext.getNoticeService().getNotices(offset, MAX_SIZE, filter, activeCallback);
    fireEvent(new LocationChangeEvent());
  }

  @Override
  public void onConfirmNotices() {
    if (confirmableNoticeIds != null) {
      appContext.getNoticeService().confirmNotices(confirmableNoticeIds, new AppAsyncCallback<Void>() {

        @Override
        public void onSuccess(final Void result) {
          //retrieve the confirmable notices again, updating texts in the dialog once retrieved.
          appContext.getNoticeService().getConfirmableNoticeIds(confirmableNoticesCallBack);
        }
      });
    }
  }

  @Override
  public void onRefresh() {
    onAdaptFilter();
  }

  @Override
  public void onAdaptFilter() {
    filter = appContext.getUserContext().getFilter(NoticeFilter.class);
    view.clear();
    loadData(0);
  }

  @Override
  public void prepareExport() {
    final NoticeFilter filter = appContext.getUserContext().getFilter(NoticeFilter.class);

    appContext.getRequestExportService().prepare(filter, new AppAsyncCallback<RequestExportResult>() {

      @Override
      public void onSuccess(final RequestExportResult result) {
        NoticeActivity.this.requestExportResult = result;
        view.updateExportResult(result);
      }
    });
  }

  @Override
  public void downloadExport() {
    Window.open(requestExportResult.getDownloadUrl(), "_blank", "");
  }

  @Override
  public void setSorting(final NoticeSortableAttribute attribute, final SortableDirection dir) {
    final NoticeFilter filter = appContext.getUserContext().getFilter(NoticeFilter.class);

    filter.setSortAttribute(attribute);
    filter.setSortDirection(dir);

    onAdaptFilter();
  }

  @Override
  public void onNoticeSelection(final Notice notice) {
    if (notice != null) {
      goTo(new NoticeDossierPlace(new NoticeKey(notice.getReference())));
    }

  }
}
