/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.PermitSortableAttribute;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportServiceAsync;
import nl.overheid.aerius.wui.admin.ui.AdminAbstractActivity;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.table.GridScrollVisitor;
import nl.overheid.aerius.wui.register.context.RegisterAppContext;
import nl.overheid.aerius.wui.register.place.PermitDossierPlace;
import nl.overheid.aerius.wui.register.ui.importer.RegisterImportDialogPanel;
import nl.overheid.aerius.wui.register.ui.importer.RegisterImportPermitDialogController;

/**
 * Activity for filtered Permits.
 */
public class PermitOverviewActivity extends AdminAbstractActivity<RegisterAppContext> implements PermitOverviewView.Presenter {
  private static final int MAX_RESULTS = 30;

  private final class SetDataAppAsyncCallback extends AppAsyncCallback<ArrayList<Permit>> {
    @Override
    public void onSuccess(final ArrayList<Permit> result) {
      if (activeCallback == this) {
        view.setData(result);
      }
    }
  }

  private final class UpdateAppAsyncCallback extends AppAsyncCallback<ArrayList<Permit>> {
    @Override
    public void onSuccess(final ArrayList<Permit> result) {
      if (activeCallback == this) {
        view.updateData(result);
      }
    }
  }

  private final PermitOverviewView view;
  private AsyncCallback<ArrayList<Permit>> activeCallback;
  private RequestExportResult requestExportResult;

  @Inject
  public PermitOverviewActivity(final RegisterAppContext appContext, final PermitOverviewView view) {
    super(appContext, RegisterPermission.VIEW_PERMITS);

    this.view = view;
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    view.setPresenter(this);
    view.setDataVisitor(new GridScrollVisitor(this));
    panel.setWidget(view);
    onRefresh();
  }

  @Override
  public void onNewApplication() {
    final RegisterImportDialogPanel<PermitKey> dialog = new RegisterImportDialogPanel<>(SupportedUploadFormat.PERMIT);

    final RegisterImportPermitDialogController newApplicationController =
        new RegisterImportPermitDialogController(
            appContext.getPlaceController(), RegisterRetrieveImportServiceAsync.Util.getInstance(), dialog) {
      @Override
      public final void handleResult(final PermitKey key) {
        hide();
        goTo(getPlace(key));
      }
    };
    newApplicationController.setTitle(M.messages().registerNewApplicationTitle());
    newApplicationController.show();
  }

  @Override
  public void onPermitSelection(final Permit permit) {
    if (permit != null) {
      goTo(new PermitDossierPlace(permit.getPermitKey()));
    }
  }

  @Override
  public void onRefresh() {
    onAdaptFilter();
  }

  @Override
  public void onAdaptFilter() {
    final PermitFilter filter = appContext.getUserContext().getFilter(PermitFilter.class);

    view.setLoading(true, true);
    activeCallback = new SetDataAppAsyncCallback();
    appContext.getPermitService().getPermits(0, MAX_RESULTS, filter, activeCallback);
  }

  @Override
  public void loadData(final int offset) {
    final PermitFilter filter = appContext.getUserContext().getFilter(PermitFilter.class);

    view.setLoading(true, false);
    activeCallback = new UpdateAppAsyncCallback();
    appContext.getPermitService().getPermits(offset, MAX_RESULTS, filter, activeCallback);
  }

  @Override
  public void prepareExport() {
    final PermitFilter filter = appContext.getUserContext().getFilter(PermitFilter.class);

    appContext.getRequestExportService().prepare(filter, new AppAsyncCallback<RequestExportResult>() {

      @Override
      public void onSuccess(final RequestExportResult result) {
        PermitOverviewActivity.this.requestExportResult = result;
        view.updateExportResult(result);
      }
    });
  }

  @Override
  public void downloadExport() {
    Window.open(requestExportResult.getDownloadUrl(), "_blank", "");
  }

  @Override
  public void setSorting(final PermitSortableAttribute attribute, final SortableDirection dir) {
    final PermitFilter filter = appContext.getUserContext().getFilter(PermitFilter.class);

    filter.setSortAttribute(attribute);
    filter.setSortDirection(dir);

    onAdaptFilter();
  }
}
