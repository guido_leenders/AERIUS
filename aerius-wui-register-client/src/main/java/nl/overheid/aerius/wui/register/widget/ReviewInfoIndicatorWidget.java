/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;

public class ReviewInfoIndicatorWidget extends Composite {

  interface ReviewInfoIndicatorWidgetUiBinder extends UiBinder<Widget, ReviewInfoIndicatorWidget> {
  }

  private static final ReviewInfoIndicatorWidgetUiBinder UI_BINDER = GWT.create(ReviewInfoIndicatorWidgetUiBinder.class);

  @UiField(provided = true) Image icon;
  @UiField(provided = true) Label label;

  public ReviewInfoIndicatorWidget(final DevelopmentRule rule, final String txt) {
    icon = new Image(DevelopmentSpaceUtil.getDevelopmentRuleIcon(rule));
    label = new Label(txt);

    initWidget(UI_BINDER.createAndBindUi(this));
  }
}
