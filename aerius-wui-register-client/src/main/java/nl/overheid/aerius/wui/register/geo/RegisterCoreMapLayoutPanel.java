/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.geo;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.LayerFactory;
import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.NavigationWidget;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Special MapLayoutPanel for register. Only contains base layer by default.
 */
@Singleton
public class RegisterCoreMapLayoutPanel extends MapLayoutPanel {
  private boolean nudged;

  private final RegisterUserContext userContext;
  private final MarkerLayerWrapper markersLayer;

  private final FetchGeometryServiceAsync service;

  @Inject
  public RegisterCoreMapLayoutPanel(final LayerFactory lf, final FetchGeometryServiceAsync service, final RegisterContext context,
      final RegisterUserContext userContext, final EventBus eventBus, final HelpPopupController hpC) {
    super(lf, eventBus, context.getReceptorGridSettings());
    this.service = service;

    this.userContext = userContext;

    markersLayer = new MarkerLayerWrapper(this, "markers");

    setNavigationWidget(new NavigationWidget(this, hpC));
  }

  public void nudge() {
    if (nudged) {
      return;
    }

    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        setDefaultExtent();
        zoomToExtent();
        zoomToExtent();

        LayerPreparationUtil.prepareLayer(service, userContext.getBaseLayer(), new AppAsyncCallback<LayerProps>() {
          @Override
          public void onSuccess(final LayerProps result) {
            addLayer(result);
          }
        });

        nudged = true;

        markersLayer.attach();
      }
    });
  }

  public MarkerLayerWrapper getMarkersLayer() {
    return markersLayer;
  }
}
