/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.register.ui;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.register.IndicativeReceptorInfo;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.LabelledWidget;
import nl.overheid.aerius.wui.main.widget.charts.BarChart;
import nl.overheid.aerius.wui.main.widget.charts.BarChart.Bar;
import nl.overheid.aerius.wui.main.widget.charts.SimpleBarChart;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectBasicNatureAreaInfoTable;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectSubProjectsDataTable;
import nl.overheid.aerius.wui.register.ui.priorityproject.PriorityProjectSubProjectsDataTable.PPSubProjectTableEventHandler;

public class PriorityProjectProjectViewImpl extends Composite implements PriorityProjectProjectView {
  interface PriorityProjectProjectViewImplUiBinder extends UiBinder<Widget, PriorityProjectProjectViewImpl> {
  }

  private static final PriorityProjectProjectViewImplUiBinder UI_BINDER = GWT.create(PriorityProjectProjectViewImplUiBinder.class);

  interface CustomStyle extends CssResource {
    String barChart();
  }

  private final PPSubProjectTableEventHandler subProjectTableEventHandler = new PPSubProjectTableEventHandler() {

    @Override
    public void onDownloadFile(final PrioritySubProject subProject, final RequestFileType type) {
      presenter.openRequestFile(subProject, type);
    }
  };

  private final RegisterUserContext userContext;
  private Presenter presenter;
  private PriorityProject priorityProject;

  @UiField CustomStyle style;
  @UiField LabelledWidget percentageAssignedLabel;
  @UiField Label noResultsSoNoEditText;
  @UiField Label projectName;
  @UiField LabelledWidget reservationBlock;
  @UiField Anchor reservation;
  @UiField Label applicationVersion;
  @UiField Label databaseVersion;
  @UiField(provided = true) BarChart<Bar> barChart;
  @UiField LabelledWidget indicativeHexagonBlock;
  @UiField Label indicativeHexagon;
  @UiField PriorityProjectBasicNatureAreaInfoTable indicativeHexagonTable;
  @UiField(provided = true) PriorityProjectSubProjectsDataTable subProjectsDataTable;
  @UiField Button newSubProjectButton;

  @Inject
  public PriorityProjectProjectViewImpl(final RegisterUserContext userContext, final EventBus eventBus) {
    this.userContext = userContext;
    barChart = new SimpleBarChart();
    barChart.addStyleName(R.css().sourceDetailCol2Var());

    subProjectsDataTable = new PriorityProjectSubProjectsDataTable(eventBus, subProjectTableEventHandler);

    initWidget(UI_BINDER.createAndBindUi(this));
    barChart.addStyleName(style.barChart());
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public String getTitleText() {
    return M.messages().registerMenuPriorityProjectsTitle();
  }

  @Override
  public void setData(final PriorityProject priorityProject) {
    this.priorityProject = priorityProject;

    final boolean noResults = priorityProject.getRequestState() == RequestState.INITIAL;

    noResultsSoNoEditText.setVisible(noResults);
    reservationBlock.setVisible(!noResults);
    indicativeHexagonBlock.setVisible(!noResults);
    percentageAssignedLabel.setVisible(!noResults);

    final double percentageAssigned = priorityProject.getFractionAssigned() * SharedConstants.PERCENTAGE_TO_FRACTION;

    projectName.setText(priorityProject.getScenarioMetaData().getProjectName());
    reservation.setText(priorityProject.getReference());
    applicationVersion.setText(priorityProject.getApplicationVersion());
    databaseVersion.setText(priorityProject.getDatabaseVersion());
    percentageAssignedLabel.setLabel(M.messages().registerPriorityProjectProjectPercentageAssigned(percentageAssigned));

    barChart.clear();
    barChart.addBar(new Bar("552890", M.messages().registerPriorityProjectProjectAssigned(), percentageAssigned));
    barChart.addBar(new Bar("transparent", M.messages().registerPriorityProjectProjectRemaining(), 100 - percentageAssigned));
    barChart.draw();

    final IndicativeReceptorInfo indRecepInfo = priorityProject.getIndicativeReceptorInfo();
    if (indRecepInfo == null) {
      indicativeHexagon.setText(M.messages().registerPriorityProjectProjectIndicativeHexagonEmptyValue());
    } else {
      final float maxPercentageUsed = indRecepInfo.getMaxFractionUsed() * SharedConstants.PERCENTAGE_TO_FRACTION;
      indicativeHexagon.setText(
          M.messages().registerPriorityProjectProjectIndicativeHexagonValue(
              indRecepInfo.getReceptor().getX(),
              indRecepInfo.getReceptor().getY(),
              maxPercentageUsed,
              String.valueOf(indRecepInfo.getReceptor().getId()))
          );

      final Set<Entry<AssessmentArea, ArrayList<HabitatType>>> entrySet = indRecepInfo.getAssessmentAreas().entrySet();
      indicativeHexagonTable.asDataTable().setRowData(entrySet);
      // If there is only one assessment area (which should be most of the time) auto-expand.
      if (indRecepInfo.getAssessmentAreas().size() == 1) {
        indicativeHexagonTable.asDataTable().setSelectedItem(entrySet.iterator().next());
      }
    }

    subProjectsDataTable.asDataTable().setRowData(priorityProject.getSubProjects());

    final boolean allowChangingAnything =
        userContext.getUserProfile().hasPermission(RegisterPermission.ADD_NEW_PRIORITY_SUBPROJECT)
        && !priorityProject.isAssignCompleted()
        && !noResults;

    newSubProjectButton.setVisible(allowChangingAnything);
  }

  @UiHandler({"pdfDownloadImage", "reservation"})
  public void onReservationDownloadImage(final ClickEvent event) {
    presenter.openRequestFile(priorityProject, RequestFileType.PRIORITY_PROJECT_RESERVATION);
  }

  @UiHandler("newSubProjectButton")
  public void onNewSubProject(final ClickEvent event) {
    presenter.newSubProject();
  }

}
