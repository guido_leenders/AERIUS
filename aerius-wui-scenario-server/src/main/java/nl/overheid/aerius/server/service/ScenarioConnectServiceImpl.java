/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import nl.overheid.aerius.connect.domain.CalculateDeltaRequest;
import nl.overheid.aerius.connect.domain.CalculateRequest;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.ConvertRequest;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.DeltaValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.ErrorResponse;
import nl.overheid.aerius.connect.domain.HighestValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.ReportRequest;
import nl.overheid.aerius.connect.domain.TotalValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.ValidateRequest;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.server.util.ConnectCalculationSessionUtil;
import nl.overheid.aerius.server.util.UtilDataFileDownloadUtil;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.export.ExportStatus;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.exception.APIException;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.ScenarioConnectService;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.HttpClientManager;

import io.swagger.util.Json;

/**
 * Service object to send calculation or report job to connect.
 */
class ScenarioConnectServiceImpl implements ScenarioConnectService {
  private static final Logger LOGGER = LoggerFactory.getLogger(ScenarioConnectServiceImpl.class);

  private static final int API_TIMEOUT = 300;
  private static final String FILE_PREFIX = "AERIUS_";

  private final ConnectDataConverter converter = new ConnectDataConverter();
  private final ScenarioSession session;
  private final PMF pmf;

  public ScenarioConnectServiceImpl(final PMF pmf, final ScenarioSession session) {
    this.session = session;
    this.pmf = pmf;
  }

  protected String createSessionKey() {
    return ExportStatus.KEY_PREFIX + UUID.randomUUID().toString();
  }

  protected CloseableHttpClient getHttpClient() {
    return HttpClientManager.getHttpClient(API_TIMEOUT);
  }

  private String submitReport(final ReportRequest reportRequest, final ConnectCalculationSessionUtil util) throws AeriusException {
    for (final DataObject dataObject : reportRequest.getReportDataObjects()) {
      setDataObject(dataObject, util);
    }
    return submitJob("report", reportRequest);
  }

  private String submitCalculateDelta(final CalculateDeltaRequest calculateDeltaRequest, final ConnectCalculationSessionUtil util) throws AeriusException {
    for (final DataObject dataObject : calculateDeltaRequest.getSituationDataObjects()) {
      setDataObject(dataObject, util);
    }
    return submitJob("calculateDelta", calculateDeltaRequest);
  }

  private String submitCalculation(final ConnectCalculationInformation connectCalculationInformation, final ConnectCalculationSessionUtil util)
      throws AeriusException {
    final String result;
    if (converter.isReport(connectCalculationInformation)) {
      result = submitReport(converter.convertToReportRequest(connectCalculationInformation), util);
    } else if (converter.isCalculateDelta(connectCalculationInformation)) {
        result = submitCalculateDelta(converter.convertToCalculateDeltaRequest(connectCalculationInformation), util);
      } else {
        result = submitCalculation(converter.convertToCalculateRequest(connectCalculationInformation), util);
      }
     return result;
  }

  private String submitCalculation(final CalculateRequest calculateRequest, final ConnectCalculationSessionUtil util) throws AeriusException {
    for (final DataObject dataObject : calculateRequest.getCalculateDataObjects()) {
      setDataObject(dataObject, util);
    }
    return submitJob("calculate", calculateRequest);
  }

  @Override
  public String submitCalculation(final ConnectCalculationInformation connectCalculationInformation) throws AeriusException {
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(session.getSession());
    return submitCalculation(connectCalculationInformation, util);
  }

  @Override
  public String submitUtil(final ConnectUtilInformation connectUtilInformation) throws AeriusException {
    final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(session.getSession());
    String result = null;
    switch (connectUtilInformation.getUtilType()) {
    case VALIDATE:
      result = submitUtilValidate(converter.convertToValidateRequest(connectUtilInformation), util);
      break;
    case CONVERT:
      result = submitUtilConvert(converter.convertToConvertRequest(connectUtilInformation), util);
      break;
    case DELTA_VALUE:
      result = submitUtilDeltaValuePerHexagonValue(converter.convertToDeltaValuePerHexagonRequest(connectUtilInformation), util);
      break;
    case HIGHEST_VALUE:
      result = submitUtilHighestValuePerHexagonValue(converter.convertToHighestValuePerHexagonRequest(connectUtilInformation), util);
      break;
    case TOTAL_VALUE:
      result = submitUtilTotalValuePerHexagonValue(converter.convertToTotalValuePerHexagonRequest(connectUtilInformation), util);
      break;
    default:
      // not known
    }

    if (connectUtilInformation.getUtilType() != UtilType.VALIDATE) {
      result = saveReplaceDataObject(result);
    }
    return result;
  }

  private String saveReplaceDataObject(final String result) {
    final String resultSessionKey = createSessionKey();
    final ExportedData exportedData = new ExportedData();
    String updatedResult = "";

    try {
      final ConvertResponse response = Json.mapper().readValue(result, ConvertResponse.class);
      if (response.getDataObject() != null) {
        final StringBuilder builder = new StringBuilder();
        builder.append('.');
        builder.append(response.getDataObject().getDataType());
        exportedData.setFileName(FileUtil.getFileName(FILE_PREFIX, builder.toString(), null, null));
        final ContentType contentType = response.getDataObject().getContentType();
        if (contentType == ContentType.BASE64) {
          exportedData.setFileContent(Base64.decodeBase64(response.getDataObject().getData()));
        } else {
          exportedData.setFileContent(response.getDataObject().getData().getBytes(StandardCharsets.UTF_8.name()));
        }
        setSessionData(resultSessionKey, exportedData);

        //replace data with download url
        response.getDataObject().setData(UtilDataFileDownloadUtil.getDownloadUrl(resultSessionKey));
        updatedResult = Json.mapper().writeValueAsString(response);
      } else {
        updatedResult = result;
      }

    } catch (final IOException e) {
      LOGGER.error("Unable to deserialize final result file {}", result, e);
    }
    return updatedResult;
  }

  private void setSessionData(final String resultSessionKey, final ExportedData exportResult) {
    try {
      session.getSession().setAttribute(resultSessionKey, exportResult);
    } catch (final IllegalStateException e) {
      // Eat error, but warn
      LOGGER.info("Export error eaten.", e);
    }
  }

  public String submitUtilValidate(final ValidateRequest validateRequest, final ConnectCalculationSessionUtil util) throws AeriusException {
    setDataObject(validateRequest.getDataObject(), util);
    return submitJob("validate", validateRequest);
  }

  public String submitUtilConvert(final ConvertRequest convertRequest, final ConnectCalculationSessionUtil util) throws AeriusException {
    setDataObject(convertRequest.getDataObject(), util);
    return submitJob("convert", convertRequest);
  }

  public String submitUtilDeltaValuePerHexagonValue(final DeltaValuePerHexagonRequest convertRequest, final ConnectCalculationSessionUtil util)
      throws AeriusException {
    setDataObject(convertRequest.getCurrent(), util);
    setDataObject(convertRequest.getProposed(), util);
    return submitJob("valuePerHexagon/delta", convertRequest);
  }

  public String submitUtilHighestValuePerHexagonValue(final HighestValuePerHexagonRequest convertRequest, final ConnectCalculationSessionUtil util)
      throws AeriusException {
    for (final DataObject dataObject : convertRequest.getDataObjects()) {
      setDataObject(dataObject, util);
    }
    return submitJob("valuePerHexagon/highest", convertRequest);
  }

  public String submitUtilTotalValuePerHexagonValue(final TotalValuePerHexagonRequest convertRequest, final ConnectCalculationSessionUtil util)
      throws AeriusException {
    for (final DataObject dataObject : convertRequest.getDataObjects()) {
      setDataObject(dataObject, util);
    }
    return submitJob("valuePerHexagon/total", convertRequest);
  }

  private void setDataObject(final DataObject dataObject, final ConnectCalculationSessionUtil util) throws AeriusException {
    final String uid = dataObject.getData();
    final String filePath = util.getFilePath(uid);
    if (StringUtils.isEmpty(filePath)) {
      throw new AeriusException(Reason.IMPORTED_FILE_NOT_FOUND, uid);
    }
    final ImportType importType = ImportType.determineByFilename(filePath);
    if (importType == null) {
      throw new AeriusException(Reason.IMPORT_FILE_TYPE_NOT_ALLOWED, new File(filePath).getName());
    }
    dataObject.setContentType(ContentType.BASE64);

    final FileFormat fileFormat = importType.getFileFormat();
    dataObject.setDataType(DataType.valueOf(fileFormat.getExtension().toUpperCase(Locale.ENGLISH)));
    try {
      dataObject.setData(util.getFileContent(uid));
    } catch (final IOException e) {
      LOGGER.error("Error reading file for file:{}", filePath, e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  private String submitJob(final String cmd, final Object data) throws AeriusException {
    final Gson gson = new Gson();
    final String jsonToSend = gson.toJson(data);
    LOGGER.debug("cmd: {} jsonToSend {}", cmd, jsonToSend);
    final String url = ConstantRepository.getString(pmf, SharedConstantsEnum.CONNECT_API_URL) + cmd;
    try (final CloseableHttpClient httpClient = getHttpClient()) {
      return HttpClientProxy.postJsonRemoteContent(httpClient, url, jsonToSend);
    } catch (final HttpException e) {
      if (HttpStatus.SC_BAD_REQUEST == e.getStatusCode()) {
        try {
          final ErrorResponse error = gson.fromJson(e.getContent(), ErrorResponse.class);
          throw new APIException(Reason.fromErrorCode(error.getCode()), error.getMessage());
        } catch (final JsonSyntaxException jse) {
          // Eat error. An internal error will be thrown below.
        }
      }

      LOGGER.error("Error while trying to communicate with the API", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    } catch (final SocketTimeoutException e) {
      LOGGER.error("Timeout while trying to communicate with the API", e);
      throw new AeriusException(Reason.SCENARIO_API_CONNECTION_ERROR, "timeout");
    } catch (final IOException | ParseException | URISyntaxException e) {
      LOGGER.error("Error while trying to communicate with the API", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  @Override
  public void deleteUploadedFile(final String uuid) throws AeriusException {
    ConnectCalculationSessionUtil.deleteUploadedFile(session.getSession(), uuid);
  }

  @Override
  public ArrayList<Calculation> fetchJobCalculations(final String jobKey) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      return CalculationRepository.getCalculationsFromConnect(connection, jobKey);
    } catch (final SQLException e) {
      LOGGER.error("Could not fetch calculations for job key {}", jobKey, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

}
