/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.server.util.UtilDataFileDownloadUtil;
import nl.overheid.aerius.shared.domain.export.ExportedData;

/**
 * Allows the user to download the file content that was returned by connect api.
 */
@WebServlet("/" + UtilDataFileDownloadUtil.DOWNLOAD_LOCATION)
public class UtilDataFileDownloadServlet extends AbstractDownloadServlet {

  private static final long serialVersionUID = 6239988981799001346L;

  private static final Logger LOG = LoggerFactory.getLogger(UtilDataFileDownloadServlet.class);

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    final String uuid = request.getParameter(UtilDataFileDownloadUtil.PARAMETER_ID);

    // Validate the parameters.
    if (StringUtils.isEmpty(uuid)) {
      missingRequiredParameter(response);
    } else {
      sendFileToUser(request, response, uuid);
    }

  }

  private void sendFileToUser(final HttpServletRequest request, final HttpServletResponse response, final String uuid) {
    try {
      final ExportedData exportedData = getExportedDataFromSession(request, uuid);
      // test contenttype
      if (exportedData.getFileContent() == null) {
        LOG.error("File in session seems empty, this should never be the case.");
        internalError(response);
      } else {
        response.setContentType(MIMETYPE_ZIP);
        response.setHeader("Content-Disposition", "attachment; filename=\""
            + exportedData.getFileName()
            + "\"");

        try {
          response.getOutputStream().write(exportedData.getFileContent());
        } catch (final IOException e) {
          internalError(response);
          LOG.error("Error while sending file", e);
        } finally {
          //clean up session.
          request.getSession().removeAttribute(uuid);
        }
      }
    } catch (final ServletException e) {
      internalError(response);
      LOG.error("Internal error occurred", e);
    }
  }

  private ExportedData getExportedDataFromSession(final HttpServletRequest request, final String uuid) throws ServletException {
    if (request.getSession().getAttribute(uuid) instanceof ExportedData) {
      return (ExportedData) request.getSession().getAttribute(uuid);
    } else {
      throw new ServletException("Did not find exported data in session, found: " + request.getSession().getAttribute(uuid));
    }
  }

  @Override
  protected Logger getLogger() {
    return LOG;
  }

}
