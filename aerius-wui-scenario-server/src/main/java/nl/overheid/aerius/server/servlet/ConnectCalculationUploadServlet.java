/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.server.util.ConnectCalculationSessionUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Servlet to process collect files to calculate via connect as uploaded in Scenario web application.
 * Save files in a temp directory while keeping a reference to them in the session for efficiency reasons.
 */
@MultipartConfig
@SuppressWarnings("serial")
@WebServlet("/aerius/" + SharedConstants.IMPORT_CONNECT_CALCULATION_SERVLET)
public class ConnectCalculationUploadServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(ConnectCalculationUploadServlet.class);

  /**
   * Prefix used to prepend the uuid - to give it a friendly flair.
   */
  private static final String UUID_PREFIX = "import_connect_calculation_";


  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    final Locale locale = Internationalizer.getLocaleFromCookie(request);
    final String uuid = UUID_PREFIX + UUID.randomUUID();
    final ImportService service = new ImportService(ServerPMF.getInstance(), locale);
    response.setStatus(HttpServletResponse.SC_CREATED);
    service.sendResponse(response, storeUpload(request, uuid, service), uuid);
  }

  boolean storeUpload(final HttpServletRequest request, final String uuid, final ImportService service) {
    boolean success;
    try {
      service.processUploadDocument(request);
      final String uploadPath = ConnectCalculationSessionUtil.createUploadPath(service.getFileName());
      final ConnectCalculationSessionUtil util = new ConnectCalculationSessionUtil(request.getSession());
      util.registerFileUpload(uuid, uploadPath);
      service.getPart().write(uploadPath);
      final ImportResult result = processUpload(request, uuid, service, uploadPath);
      LOG.trace("Received file '{}'. The file is written to: '{}'", service.getFileName(), uploadPath);
      success = result.getExceptions().isEmpty();
    } catch (final AeriusException e) {
      success = false;
    } catch (final IOException | RuntimeException e) {
      LOG.error("Error during upload connect file: {}", uuid, e);
      success = false;
    }
    return success;
  }

  private ImportResult processUpload(final HttpServletRequest request, final String uuid, final ImportService service, final String filePath)
      throws AeriusException, IOException {
    final UploadOptions uo = new UploadOptions();
    // We don't want it to return any of this - validating the file should suffice.
    uo.setReturnSources(false);
    uo.setValidate(true);
    try (final InputStream is = new BufferedInputStream(Files.newInputStream(Paths.get(filePath)))) {
      if (service.processUpload(request, uuid, uo, is)) {
        return service.getResult();
      } else {
        throw service.getServerException();
      }
    }
  }
}
