/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.context.ScenarioUserContext;
import nl.overheid.aerius.shared.exception.AeriusException;

public class ScenarioContextServiceImpl extends ScenarioBaseContextServiceImpl<ScenarioSession> {
  public ScenarioContextServiceImpl(final PMF pmf, final ScenarioSession session) {
    super(pmf, session);
  }

  @Override
  public ScenarioContext getContext() throws AeriusException {
    return (ScenarioContext) super.getContext();
  }

  @Override
  public ScenarioUserContext getUserContext() throws AeriusException {
    return (ScenarioUserContext) getUserContext(new ScenarioUserContext());
  }
}
