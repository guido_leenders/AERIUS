/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.connect.domain.CalculateDataObject;
import nl.overheid.aerius.connect.domain.CalculateDeltaRequest;
import nl.overheid.aerius.connect.domain.CalculateRequest;
import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOptions.PermitCalculationRadiusTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOutputOptions;
import nl.overheid.aerius.connect.domain.ConvertRequest;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DeltaValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.HighestValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.ReportRequest;
import nl.overheid.aerius.connect.domain.SituationDataObject;
import nl.overheid.aerius.connect.domain.SituationType;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.domain.TotalValuePerHexagonRequest;
import nl.overheid.aerius.connect.domain.ValidateRequest;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile.Situation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.CalculationType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;

/**
 * Convert Connect UI specific data objects to the Connect API data objects.
 */
class ConnectDataConverter {

  private static final Set<ExportType> REPORT_EXPORT_TYPES = EnumSet.of(ExportType.PDF_CALCULATION
      /*, ExportType.PDF_DEMAND, ExportType.PDF_DEVELOPMENT_SPACES */);

  public boolean isReport(final ConnectCalculationInformation cci) {
    return REPORT_EXPORT_TYPES.contains(cci.getExportType());
  }

  public boolean isCalculateDelta(final ConnectCalculationInformation cci) {
    return ExportType.GML_DELTA == cci.getExportType();
  }

  public CalculateRequest convertToCalculateRequest(final ConnectCalculationInformation cci) {
    final CalculateRequest cr = new CalculateRequest();
    cr.setOptions(getOptions(cci));
    cr.setApiKey(cci.getApiKey());
    cr.setCalculateDataObjects(getCalculationDataObject(cci.getFiles()));
    return cr;
  }

  public CalculateDeltaRequest convertToCalculateDeltaRequest(final ConnectCalculationInformation cci) {
    final CalculateDeltaRequest rr = new CalculateDeltaRequest();
    rr.setOptions(getOptions(cci));
    rr.setApiKey(cci.getApiKey());
    rr.situationDataObjects(getSituationDataObjects(cci.getFiles()));
    return rr;
  }

  public ReportRequest convertToReportRequest(final ConnectCalculationInformation cci) {
    final ReportRequest rr = new ReportRequest();
    rr.setOptions(getOptions(cci));
    rr.setApiKey(cci.getApiKey());
    rr.reportDataObjects(getSituationDataObjects(cci.getFiles()));
    return rr;
  }

  public ValidateRequest convertToValidateRequest(final ConnectUtilInformation cui) {
    final ValidateRequest vr = new ValidateRequest();
    vr.setValidateAsPriorityProject(cui.isValidateAsPriorityProject());
    vr.setDataObject(getValidateDataObject(cui.getFiles().get(0))); // only one file
    return vr;
  }

  public ConvertRequest convertToConvertRequest(final ConnectUtilInformation cui) {
    final ConvertRequest cr = new ConvertRequest();
    cr.setDataObject(getValidateDataObject(cui.getFiles().get(0))); // only one file
    return cr;
  }

  public DeltaValuePerHexagonRequest convertToDeltaValuePerHexagonRequest(final ConnectUtilInformation cui) {
    final DeltaValuePerHexagonRequest dvphr = new DeltaValuePerHexagonRequest();
    final ConnectCalculationFile currentFile = getSituationFile(Situation.CURRENT, cui.getFiles());
    if (currentFile != null) {
      dvphr.setCurrent(getValidateDataObject(currentFile));
    }
    final ConnectCalculationFile proposedFile = getSituationFile(Situation.PROPOSED, cui.getFiles());
    if (proposedFile != null) {
      dvphr.setProposed(getValidateDataObject(proposedFile));
    }
    dvphr.setOnlyIncreasement(cui.isOnlyIncreasement());
    return dvphr;
  }

  private ConnectCalculationFile getSituationFile(final Situation requestedSituation, final ArrayList<ConnectCalculationFile> files) {
    for (final ConnectCalculationFile ccf : files) {
      if (ccf.getSituation() == requestedSituation) {
        return ccf;
      }
    }
    return null;
  }

  public HighestValuePerHexagonRequest convertToHighestValuePerHexagonRequest(final ConnectUtilInformation cui) {
    final HighestValuePerHexagonRequest hvphr = new HighestValuePerHexagonRequest();
    hvphr.setDataObjects(getValuePerHexagonDataObject(cui.getFiles()));
    return hvphr;
  }

  public TotalValuePerHexagonRequest convertToTotalValuePerHexagonRequest(final ConnectUtilInformation cui) {
    final TotalValuePerHexagonRequest tvphr = new TotalValuePerHexagonRequest();
    tvphr.setDataObjects(getValuePerHexagonDataObject(cui.getFiles()));
    return tvphr;
  }

  private DataObject getValidateDataObject(final ConnectCalculationFile file) {
    final DataObject dataObject = new DataObject();
    dataObject.setData(file.getUuid());
    return dataObject;
  }

  private CalculationOptions getOptions(final ConnectCalculationInformation cci) {
    final CalculationOptions co = new CalculationOptions();
    co.setCalculationType(getCalculationType(cci.getCalculationType()));
    co.setYear(cci.getYear());
    co.setName(cci.getName());
    co.addSubstancesItem(Substance.NH3).addSubstancesItem(Substance.NOX);
    co.setOutputOptions(getOutputOptions(cci));
    co.setTempProjectYears(cci.isTemporaryProjectImpact() ? cci.getTemporaryProjectYears() : null);
    if (cci.isPermitCalculationRadiusTypeImpact()) {
      co.setPermitCalculationRadiusType(PermitCalculationRadiusTypeEnum.valueOf(cci.getPermitCalculationRadiusType().getCode()));
    }
// This field has been shortcircuited out in the ReportApiServiceImpl
//    if (isReport(cci)) {
//      co.setPermitReportType(getPermitReportType(cci.getExportType()));
//    }
    return co;
  }

//  private PermitReportTypeEnum getPermitReportType(final ExportType exportType) {
//    final PermitReportTypeEnum result;
//
//    if (exportType == null) {
//      result = null;
//    } else {
//      switch (exportType) {
//      case PDF_DEMAND:
//        result = PermitReportTypeEnum.DEMAND;
//        break;
//      case PDF_DEVELOPMENT_SPACES:
//        result = PermitReportTypeEnum.DEVELOPMENT_SPACES;
//        break;
//      default:
//        throw new IllegalArgumentException("Could not convert ExportType " + exportType.name() + " to a proper PermitReportTypeEnum.");
//      }
//    }
//
//    return result;
//  }

  private CalculationTypeEnum getCalculationType(final CalculationType calculationType) {
    if (calculationType == null) {
      return CalculationTypeEnum.NBWET;
    }

    final CalculationTypeEnum ct;
    switch (calculationType) {
    case NBWET:
    default:
      ct = CalculationTypeEnum.NBWET;
      break;
    }
    return ct;
  }

  private CalculationOutputOptions getOutputOptions(final ConnectCalculationInformation cci) {
    final CalculationOutputOptions coo = new CalculationOutputOptions();
    coo.setSectorOutput(cci.getExportType() == ExportType.GML_PER_SECTOR);
    return coo;
  }

  private List<CalculateDataObject> getCalculationDataObject(final List<ConnectCalculationFile> files) {
    final List<CalculateDataObject> cros = new ArrayList<>();
    for (final ConnectCalculationFile ccf : files) {
      final CalculateDataObject cdo = new CalculateDataObject();
      cdo.setData(ccf.getUuid());
      cros.add(cdo);
    }
    return cros;
  }

  private List<SituationDataObject> getSituationDataObjects(final List<ConnectCalculationFile> files) {
    final List<SituationDataObject> cros = new ArrayList<>();
    for (final ConnectCalculationFile ccf : files) {
      final SituationDataObject rdo = new SituationDataObject();
      rdo.setSituationType(getSituationType(ccf.getSituation()));
      rdo.setData(ccf.getUuid());
      cros.add(rdo);
    }
    return cros;
  }

  private List<DataObject> getValuePerHexagonDataObject(final List<ConnectCalculationFile> files) {
    final List<DataObject> dol = new ArrayList<>();
    for (final ConnectCalculationFile ccf : files) {
      final DataObject dataObject = new DataObject();
      dataObject.setData(ccf.getUuid());
      dol.add(dataObject);
    }
    return dol;
  }

  private SituationType getSituationType(final Situation situation) {
    return situation == Situation.CURRENT ? SituationType.CURRENT : SituationType.PROPOSED;
  }

}
