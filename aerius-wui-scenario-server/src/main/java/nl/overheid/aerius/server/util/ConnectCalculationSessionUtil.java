/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ConnectCalculationSessionUtil {

  /**
   * The temp directory prefix where uploaded files are stored for Scenario calculations.
   */
  private static final String CONNECT_CALCULATION_UPLOAD_DIRECTORY = "connect_calculation_upload";
  /**
   * The session prefix which prepended to a valid UUID  of an uploaded file allows for finding the upload path for Scenario calculations.
   */
  private static final String CONNECT_CALCULATION_UPLOADPATH_PREFIX = "connect_calculation_uploadpath_";

  private static final Logger LOGGER = LoggerFactory.getLogger(ConnectCalculationSessionUtil.class);
  private final HttpSession session;

  public ConnectCalculationSessionUtil(final HttpSession session) {
    this.session = session;
  }

  public static String createUploadPath(final String fileName) throws IOException {
    return new File(Files.createTempDirectory(CONNECT_CALCULATION_UPLOAD_DIRECTORY).toFile(),
        FilenameUtils.getName(fileName)).getAbsolutePath();
  }

  public void registerFileUpload(final String uuid, final String uploadPath) {
    session.setAttribute(getUploadPathAttribute(uuid), uploadPath);
  }

  public static boolean deleteUploadedFile(final HttpSession session, final String uuid) {
    boolean deleted = false;

    final String attributeKey = getUploadPathAttribute(uuid);
    if (session.getAttribute(attributeKey) != null) {
      final String uploadPath = (String) session.getAttribute(attributeKey);
      session.removeAttribute(attributeKey);
      deleted = true;

      try {
        final File file = new File(uploadPath);
        Files.delete(file.toPath());
        final Path parent = Paths.get(file.getParent());
        if (parent.toFile().isDirectory() && parent.toFile().list().length == 0) {
          Files.delete(parent);
        } else {
          LOGGER.warn("Wanted to delete tmp directory '{}', but it's not empty", parent);
        }
      } catch (final IOException e) {
        // Even if deleting the file fails, the user session will not point to it anymore, so no error handling needed.
        LOGGER.warn("Failed to delete uploaded Scenario calculation file '{}'", uploadPath, e);
      }
    }

    return deleted;
  }

  /**
   * Get the file content in Base64.
   * @param session The HTTP session to use.
   * @param uuid The UUID of the file to get.
   * @return File content if present, null if not found.
   * @throws IOException On I/O errors.
   */
  public String getFileContent(final String uuid) throws IOException {
    return Base64.encodeBase64String(Files.readAllBytes(Paths.get(getFilePath(uuid))));
  }

  /**
   * Get the file path.
   * @param session The HTTP session to use.
   * @param uuid The UUID of the file to get path for.
   * @return
   */
  public String getFilePath(final String uuid) {
    return (String) session.getAttribute(getUploadPathAttribute(uuid));
  }

  private static String getUploadPathAttribute(final String uuid) {
    return CONNECT_CALCULATION_UPLOADPATH_PREFIX + uuid;
  }
}
