/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.server.service.ImportService;
import nl.overheid.aerius.server.util.ConnectCalculationSessionUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.util.LocaleUtils;


/**
 * Test for class {@link ConnectCalculationUploadServlet}.
 */
public class ConnectCalculationUploadServletTest extends BaseDBTest {

  private static final String INPUT_FILE = "../uploadtest.gml";
  private static final String CONTENT_DISPOSITION = "filename=" + INPUT_FILE;

  @Test
  public void testStoreUpload() throws IOException, IllegalStateException, ServletException {
    final ConnectCalculationUploadServlet servlet = new ConnectCalculationUploadServlet();
    final HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    final HttpSession session = Mockito.mock(HttpSession.class);
    Mockito.when(request.getSession()).thenReturn(session);
    final Map<String, Object> sessionMap = new HashMap<>();
    Mockito.when(session.getAttribute(Mockito.anyString())).then(new Answer<Object>() {
      @Override
      public Object answer(final InvocationOnMock invocation) throws Throwable {
        return sessionMap.get(invocation.getArgumentAt(0, String.class));
      }
    });
    final Part filePart = Mockito.mock(Part.class);
    Mockito.when(filePart.getHeader("content-disposition")).thenReturn(CONTENT_DISPOSITION);
    final InputStream fileInputstream = getClass().getResourceAsStream(INPUT_FILE);
    Mockito.when(filePart.getInputStream()).thenReturn(fileInputstream);
    Mockito.when(filePart.getSize()).thenReturn(1L);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        Files.copy(fileInputstream, Paths.get(invocation.getArgumentAt(0, String.class)));
        return null;
      }
    }).when(filePart).write(Mockito.anyString());
    Mockito.when(request.getPart(SharedConstants.IMPORT_FILE_FIELD_NAME)).thenReturn(filePart);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        sessionMap.put(invocation.getArgumentAt(0, String.class), invocation.getArgumentAt(1, Object.class));
        return null;
      }
    }).when(session).setAttribute(Mockito.anyString(), Mockito.any());
    final String uuid = UUID.randomUUID().toString();
    try {
      final ImportService service = new ImportService(getCalcPMF(), LocaleUtils.getDefaultLocale());
      final boolean success = servlet.storeUpload(request, uuid, service);
      assertNull("No server exceptions: " + service.getServerException(), service.getServerException());
      assertTrue("No errors reading file: " + service.getResult().getExceptions(), service.getResult().getExceptions().isEmpty());
      assertTrue("Upload should be successfull", success);
    } finally {
      ConnectCalculationSessionUtil.deleteUploadedFile(session, uuid);
    }
  }
}
