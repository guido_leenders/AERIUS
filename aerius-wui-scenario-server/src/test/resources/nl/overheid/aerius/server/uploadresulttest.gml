<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:imaer="http://imaer.aerius.nl/2.0" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/2.0 http://imaer.aerius.nl/2.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2017</imaer:year>
                    <imaer:name>reken met scheepvaart ....</imaer:name>
                    <imaer:corporation>Ministerie Infrastructuur en Milieu</imaer:corporation>
                    <imaer:facilityLocation>
                        <imaer:Address>
                            <imaer:streetAddress>Postbus 20901</imaer:streetAddress>
                            <imaer:postcode>2500EX</imaer:postcode>
                            <imaer:city>Den Haag</imaer:city>
                        </imaer:Address>
                    </imaer:facilityLocation>
                    <imaer:description>Grote Zeesluis Terneuzen verschil</imaer:description>
                    <imaer:submitter>MinIenM</imaer:submitter>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>2020 xxxxxxxxxxxxxxxxxxxxx vind mij terug</imaer:name>
                    <imaer:reference>RP4EaHdWn3f7</imaer:reference>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:type>PERMIT</imaer:type>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>1.0-SNAPSHOT_20170215_6704cd9d8d</imaer:aeriusVersion>
                    <imaer:databaseVersion>1.0-SNAPSHOT_20170215_04814ebbbe</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:EmissionSource sectorId="2100" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Bron 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>0.22</imaer:heatContent>
                    <imaer:emissionHeight>40.0</imaer:emissionHeight>
                    <imaer:diurnalVariation>INDUSTRIAL_ACTIVITY</imaer:diurnalVariation>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.POINT">
                            <gml:pos>103579.9 517401.36</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>2100.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>2100.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:ReceptorPoint receptorPointId="5914714" gml:id="CP.5914714">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.5914714</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.5914714.POINT">
                    <gml:pos>104295.0 504622.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.REPR.5914714">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>104326.0 504676.0 104357.0 504622.0 104326.0 504568.0 104264.0 504568.0 104233.0 504622.0 104264.0 504676.0 104326.0 504676.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.119</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.007048</imaer:value>
                </imaer:Result>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>