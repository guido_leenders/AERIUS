# AERIUS

AERIUS is the calculation tool of the Integrated Approach to Nitrogen (PAS).
AERIUS supports the permit issuing process and spatial planning around the
Natura 2000 areas, as well as monitoring of the PAS.

See [www.aerius.nl](https://www.aerius.nl) for more background details, as well as
fact sheets, and user manuals. For support and other questions about AERIUS contact
the [helpdesk](http://pas.bij12.nl).

## About AERIUS

The Integrated Approach to Nitrogen (PAS) is the Dutch policy for coping with
the issues around nitrogen. The PAS will ensure that the objectives of European
nature policy are being achieved, while creating the necessary room for
economic development. For more information about the PAS see: [pas.natura2000.nl](http://pas.natura2000.nl)

### AERIUS, the calculation tool of the PAS

The AERIUS calculation tool is one of the cornerstones of the Integrated
Approach to Nitrogen (PAS). It calculates the level of nitrogen deposition
in Natura 2000 areas, caused by projects and development plans. AERIUS supports
the issuing of permits for economic activities that involve the emission of
nitrogen, and monitors whether the total nitrogen burden continues to decline.
In addition, AERIUS also facilitates spatial planning in relation to nitrogen.

### For all areas and sectors
AERIUS may be used for calculations for all nitrogen-sensitive Natura 2000
areas and all nitrogen-emitting sectors (agriculture, industry, and traffic
& transport). In this way, AERIUS is in keeping with the area- and
sector-exceeding character of the PAS.

## AERIUS Development

### OSS development paradigm
AERIUS is developed using git. To effectively use git read the
[OSS Development paradigm](doc/git/oss_development_paradigm.md) page on how to
effectively work with git.

### Development, Debugging and Testing
AERIUS is build with open source technologies. How to setup a development
environment, build the project, debug the project and test the project
read the [development page](doc/development_build_test.md) page.

### Production and deploying
To learn more about running AERIUS in a production environment, read the
[deploy](doc/deploy.md) page.

### Manual, Factsheets, Use Cases
The [manuals](https://www.aerius.nl/nl/manuals/calculator) (Dutch)
for all AERIUS products can be found on the AERIUS website.
There is also an extensive list of [factsheets](https://www.aerius.nl/nl/factsheets) (Dutch) describing the domain in detail.

To understand the internal workings of the system several use cases have been
described. These use cases explain what happens internally for the main functionality
of the applications. The use cases are split per application:

[Calculator](doc/use_case/use_case_calculator.md)

[Register](doc/use_case/use_case_register.md)

[AERIUS UK](doc/use_case/use_case_uk.md)

### Architecture, design patterns
To learn more about the architecture, project structure, design patterns,
goals and pointers see:

[Non Functional Requirements](doc/non_functional_requirements.md)

[Architecture](doc/architecture.md)

[Scaling AERIUS](doc/scaling_aerius.md)

[Project structure](doc/project_structure.md)

[Design patterns](doc/design_patterns.md)

[Design patterns UI](doc/design_patterns_ui.md)

### Database development

PostgreSQL conventions ([dutch](doc/database/postgresql_conventies.md))

Common modules descriptions ([dutch](doc/database/AERIUS_common_modules.md))

Database buildscript and datasources guide ([dutch](doc/database/buildsysteem_en_databronnen.md))

Database fundamentals ([dutch](doc/database/AERIUS_db_implementatie_keuzes.md))

[Setting up the database development environment](aerius-database-build/vagrant/vagrant.md)

### IMAER development

Coding guidelines ([dutch](doc/imaer/imaer_coding_guidelines.md))

### Disclaimer

Although the source code published here is of the AERIUS products no rights are granted. The owner of AERIUS assumes no liability. AERIUS is a registered trademark in the Benelux. All rights not expressly granted herein are reserved.

### LICENSE

Copyright [the State of the Netherlands](https://www.government.nl)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.

