/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.register.DashboardRepository;
import nl.overheid.aerius.db.register.FilterRepository;
import nl.overheid.aerius.db.register.NoticeModifyRepository;
import nl.overheid.aerius.db.register.NoticeRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.server.util.NoticeDeleteMailUtil;
import nl.overheid.aerius.server.util.RequestFileDownloadUtil;
import nl.overheid.aerius.server.util.ScenarioUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeConfirmationSetting;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.NoticeKey;
import nl.overheid.aerius.shared.domain.register.NoticeMapData;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.domain.user.AuthoritySetting;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;
import nl.overheid.aerius.shared.service.NoticeService;
import nl.overheid.aerius.shared.util.UserProfileUtil;

/**
 * Service with CRUD methods for notices.
 */
public class NoticeServiceImpl implements NoticeService {

  private static final Logger LOG = LoggerFactory.getLogger(NoticeServiceImpl.class);
  private final PMF pmf;

  /**
   * @param pmf
   */
  public NoticeServiceImpl(final PMF pmf) {
    this.pmf = pmf;
  }

  @Override
  public Notice fetchNoticeDetails(final NoticeKey key) throws AeriusException {
    Notice notice = null;
    try (final Connection con = pmf.getConnection()) {
      final int noticeId = NoticeRepository.getNoticeIdByReference(con, key.getReference());
      notice = NoticeRepository.getNoticeWithAuditTrail(con, noticeId);
      // add download url
      RequestFileDownloadUtil.setDownloadUrls(con, notice);
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving notice " + key);
    }
    validateNotice(notice, key);
    return notice;
  }

  @Override
  public ArrayList<Notice> getNotices(final int offset, final int size, final NoticeFilter filter) throws AeriusException {
    ArrayList<Notice> notices = null;

    try (final Connection con = pmf.getConnection()) {
      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(con);

      // Find the (filtered) notices
      notices = NoticeRepository.getNotices(con, size, offset, filter);

      // Update the filter for the user
      FilterRepository.insertOrUpdateFilter(con, filter, userProfile);

    } catch (final SQLException e) {
      handleSQLException(e, "fetching notices");
    }

    return notices;
  }

  @Override
  public NoticeMapData getNoticesMapData(final int id) throws AeriusException {
    final NoticeMapData data = new NoticeMapData();
    data.setNoticeId(id);

    try (final Connection con = pmf.getConnection()) {
      final int maxRadius = ConstantRepository.getInteger(con, ConstantsEnum.MAX_RADIUS_FOR_REQUESTS);
      data.setNotices(NoticeRepository.getNoticesWithinRadius(con, id, maxRadius));
    } catch (final SQLException e) {
      handleSQLException(e, "fetching noticesMapData");
    }

    return data;
  }

  @Override
  public ArrayList<Integer> getConfirmableNoticeIds() throws AeriusException {
    ArrayList<Integer> noticeIds = new ArrayList<>();
    // Get the user profile to check if the user can actually batch confirm.
    final UserProfile userProfile = UserService.getCurrentUserProfileStrict(pmf);
    final NoticeConfirmationSetting setting = NoticeConfirmationSetting.safeValueOf(
        userProfile.getAuthority().getSettings().get(AuthoritySetting.NO_PERMIT_NOTICE_CONFIRMATION));

    if (setting == NoticeConfirmationSetting.BATCH_CONFIRM) {
      //get the confirmable notice IDs from the database.
      try (final Connection con = pmf.getConnection()) {
        noticeIds = NoticeRepository.getConfirmableNoticesIds(con, userProfile.getAuthority().getId());
      } catch (final SQLException e) {
        handleSQLException(e, "retrieving confirmable notice IDs");
      }
    }

    return noticeIds;
  }

  @Override
  public void confirmNotices(final ArrayList<Integer> noticeIds) throws AeriusException {
    // Get the user profile to check if the user can actually batch confirm.
    final UserProfile userProfile = UserService.getCurrentUserProfileStrict(pmf);
    UserProfileUtil.checkPermission(userProfile, RegisterPermission.CONFIRM_PRONOUNCEMENT);
    final NoticeConfirmationSetting setting = NoticeConfirmationSetting.safeValueOf(
        userProfile.getAuthority().getSettings().get(AuthoritySetting.NO_PERMIT_NOTICE_CONFIRMATION));
    switch (setting) {
    case ONE_BY_ONE_CONFIRM:
      if (noticeIds.size() > 1) {
        //for now throw an exception
        throw new AuthorizationException();
      }
      //fall through to confirm
    case BATCH_CONFIRM:
      try (final Connection con = pmf.getConnection()) {
        NoticeModifyRepository.confirmNotices(con, noticeIds, userProfile);
      } catch (final SQLException e) {
        handleSQLException(e, "confirming notice IDs");
      }
      break;
    case AUTO_CONFIRM:
    default:
      //don't bother setting confirmed = true (in case a authority changes option later on).
      break;
    }
  }

  @Override
  public ArrayList<DefaultDashboardRow> getDashboardNoticeRows(final NoticeDashboardFilter filter) throws AeriusException {
    ArrayList<DefaultDashboardRow> rows = null;

    try (final Connection con = pmf.getConnection()) {
      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(con);
      rows = DashboardRepository.getNoticeRows(con, filter);
      FilterRepository.insertOrUpdateFilter(con, filter, userProfile);
    } catch (final SQLException e) {
      handleSQLException(e, "fetching dashboard notice rows");
    }

    return rows;
  }

  @Override
  public void deleteNotice(final int noticeId) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(con);
      UserProfileUtil.checkPermission(userProfile,
          RegisterPermission.DELETE_PRONOUNCEMENT);
      final Notice notice = NoticeRepository.getNotice(con, noticeId);

      NoticeDeleteMailUtil.sendMailToAuthority(con, TaskClientFactory.getInstance(), notice, userProfile);

      NoticeModifyRepository.deleteMelding(con, noticeId, userProfile);

    } catch (final IOException e) {
      LOG.error("Error sending delete mail for notice ID {}.", noticeId, e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    } catch (final SQLException e) {
      handleSQLException(e, "deleting notice: " + noticeId);
    }
  }

  @Override
  public ScenarioGMLs getGML(final NoticeKey noticeKey) throws AeriusException {
    ScenarioGMLs scenarioGMLs = new ScenarioGMLs();

    try (final Connection connection = pmf.getConnection()) {
      scenarioGMLs = ScenarioUtil.getScenarioGMLs(NoticeRepository.getNoticePDFContent(connection, noticeKey));
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving GML for :" + noticeKey);
    }

    return scenarioGMLs;
  }


  private void validateNotice(final Notice notice, final NoticeKey key) throws AeriusException {
    if (notice == null) {
      throw new AeriusException(Reason.MELDING_UNKNOWN, key == null ? null : key.getReference());
    }
  }

  private void handleSQLException(final SQLException e, final String action) throws AeriusException {
    LOG.error("Error while {}", action, e);
    throw new AeriusException(Reason.SQL_ERROR);
  }
}
