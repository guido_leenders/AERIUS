/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.PriorityProjectModifyRepository;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.server.service.RequestUploadService;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportService;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Handles factsheet file uploads.
 */
@MultipartConfig
@SuppressWarnings("serial")
@WebServlet("/aerius/" + SharedConstants.IMPORT_FACTSHEET_SERVLET)
public class RegisterFactsheetUploadServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterFactsheetUploadServlet.class);

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    final UploadOptions uploadOptions = new UploadOptions();
    uploadOptions.setSupportedFormats(SupportedUploadFormat.PRIORITY_PROJECT_FACTSHEET);

    final RequestUploadService<PriorityProjectKey> requestUploadService = new RequestUploadService<PriorityProjectKey>(
        getPMF(),
        Internationalizer.getLocaleFromCookie(request),
        RegisterRetrieveImportService.UUID_PREFIX_PP_FACTSHEET,
        RequestFileType.PRIORITY_PROJECT_FACTSHEET,
        uploadOptions,
        RegisterPermission.ADD_NEW_PRIORITY_PROJECT) {

      @Override
      public PriorityProjectKey handleProcessedUpload(final ImportOutput output, final String reference, final UserProfile uploadedBy,
          final InsertRequestFile insertRequestFile) throws AeriusException {
        return updateFactsheet(reference, insertRequestFile, uploadedBy);
      }

      @Override
      protected TaskManagerClient getTaskManagerClient() throws IOException {
        return RegisterFactsheetUploadServlet.this.getTaskManagerClient();
      }
    };

    requestUploadService.processRequest(request, response, true);
  }

  private PriorityProjectKey updateFactsheet(final String reference, final InsertRequestFile insertRequestFile, final UserProfile uploadedBy)
      throws AeriusException {

    try (final Connection connection = getPMF().getConnection()) {
      final PriorityProjectKey priorityProjectKey = PriorityProjectRepository.getPriorityProjectKey(connection, reference);
      final PriorityProject priorityProject = PriorityProjectRepository.getSkinnedPriorityProjectByKey(connection, priorityProjectKey);

      // if the assign is complete, we shouldn't be able to upload.
      if (priorityProject.isAssignCompleted()) {
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }

      // Only now can we verify if user is from the correct authority
      UserProfileUtil.checkPermissionAndAuthority(uploadedBy, priorityProject.getAuthority());

      PriorityProjectModifyRepository.updateFactsheet(connection, priorityProjectKey, insertRequestFile, uploadedBy);
      return priorityProjectKey;
    } catch (final SQLException e) {
      LOG.error("SQLException while handling ", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  protected PMF getPMF() {
    return ServerPMF.getInstance();
  }

  protected TaskManagerClient getTaskManagerClient() throws IOException {
    return TaskClientFactory.getInstance();
  }
}
