/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.server.util.ExportedDataFileDownloadUtil;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Allows the user to download the file content that was returned by a worker.
 * Can be used together with an ExportService implementation.
 */
@WebServlet("/" + ExportedDataFileDownloadUtil.DOWNLOAD_LOCATION)
public class ExportedDataFileDownloadServlet extends AbstractDownloadServlet {

  private static final long serialVersionUID = -7752663404643966555L;

  private static final Logger LOG = LoggerFactory.getLogger(ExportedDataFileDownloadServlet.class);

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    final String uuid = request.getParameter(ExportedDataFileDownloadUtil.PARAMETER_ID);

    // Validate the parameters.
    if (StringUtils.isEmpty(uuid)) {
      missingRequiredParameter(response);
    } else {
      sendFileToUser(request, response, uuid);
    }

  }

  private void sendFileToUser(final HttpServletRequest request, final HttpServletResponse response, final String uuid) {
    try {
      final ExportedData exportedData = getExportedDataFromSession(request, uuid);

      if (exportedData.getFileContent() == null) {
        LOG.error("File in session seems empty, this should never be the case.");
        internalError(response);
      } else {
        response.setContentType(MIMETYPE_ZIP);
        response.setHeader("Content-Disposition", "attachment; filename=\""
            + exportedData.getFileName()
            + "\"");

        try {
          response.getOutputStream().write(exportedData.getFileContent());
        } catch (final IOException e) {
          LOG.warn("Error while sending file", e);
        } finally {
          //clean up session.
          request.getSession().removeAttribute(uuid);
        }
      }
    } catch (final AeriusException e) {
      internalError(response);
    }
  }

  private ExportedData getExportedDataFromSession(final HttpServletRequest request, final String uuid) throws AeriusException {
    if (request.getSession().getAttribute(uuid) instanceof ExportedData) {
      return (ExportedData) request.getSession().getAttribute(uuid);
    } else {
      LOG.error("Did not find exported data in session, found {}", request.getSession().getAttribute(uuid));
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  @Override
  protected Logger getLogger() {
    return LOG;
  }

}
