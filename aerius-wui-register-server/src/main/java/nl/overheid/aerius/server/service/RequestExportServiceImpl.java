/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.NoticeRepository;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.server.util.ExportRequestsDownloadUtil;
import nl.overheid.aerius.server.util.ExportedDataFileDownloadUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportStatus.Status;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.export.PrioritySubProjectExportData;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.domain.register.RequestFilter;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.RequestExportService;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;

public class RequestExportServiceImpl extends ExportService<RequestExportResult> implements RequestExportService {

  private static final Logger LOG = LoggerFactory.getLogger(RequestExportServiceImpl.class);
  private final PMF pmf;

  public RequestExportServiceImpl(final PMF pmf, final RegisterSession session) {
    super(session);
    this.pmf = pmf;
  }

  @Override
  public RequestExportResult prepare(final RequestFilter<?> filter) throws AeriusException {
    UserProfileUtil.checkPermission(UserService.getCurrentUserProfileStrict(pmf), RegisterPermission.EXPORT_REQUESTS);

    final RequestExportResult result = new RequestExportResult();

    if (filter != null) {
      addRequestFileInfo(result, filter);

      // Only set downloadURL and put the filter in the session if there are any hits
      if (result.getAmount() > 0) {
        final String uuid = UUID.randomUUID().toString();
        ExportRequestsDownloadUtil.setFilterForUuid(session.getSession(), uuid, filter);
        result.setDownloadUrl(ExportRequestsDownloadUtil.getDownloadUrl(uuid));
      }
    }

    return result;
  }

  private void addRequestFileInfo(final RequestExportResult result, final RequestFilter<?> filter) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final ArrayList<Integer> requestIds = getFilteredRequestIds(con, filter);

      // fetch/set the statistics of these requests
      result.setAmount(requestIds.size());
      result.setTotalSize(RequestRepository.getRequestsFileSize(con, requestIds));
    } catch (final SQLException e) {
      LOG.error("Error while exporting requests in Register: {}", filter, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private ArrayList<Integer> getFilteredRequestIds(final Connection con, final RequestFilter<?> filter) throws SQLException, AeriusException {
    final ArrayList<? extends Request> requests;
    if (filter instanceof PermitFilter) {
      requests = PermitRepository.getPermits(con, Integer.MAX_VALUE, 0, (PermitFilter) filter);
    } else if (filter instanceof NoticeFilter) {
      requests = NoticeRepository.getNotices(con, Integer.MAX_VALUE, 0, (NoticeFilter) filter);
    } else {
      LOG.error("Unimplemented filter type received: {}", filter);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }

    // Could use something like Google Collections to transform this, but that's not allowed at this moment. =(
    final ArrayList<Integer> requestIds = new ArrayList<>();
    for (final Request request : requests) {
      requestIds.add(request.getId());
    }
    return requestIds;
  }

  @Override
  public String startSubPriorityExport(final PrioritySubProjectKey prioritySubProjectKey) throws AeriusException {
    LOG.info("Starting priority sub project export: {}", prioritySubProjectKey);

    final String resultSessionKey = createSessionKey();

    final PrioritySubProjectExportData exportData = new PrioritySubProjectExportData();
    exportData.setPrioritySubProjectKey(prioritySubProjectKey);

    sendToWorkers(exportData, resultSessionKey, prioritySubProjectKey);

    return resultSessionKey;
  }

  @Override
  public String startPriorityExport(final PriorityProjectExportData priorityProjectExportData) throws AeriusException {
    LOG.info("Starting priority project export: {}, {}", priorityProjectExportData.getPriorityProjectKey(),
        priorityProjectExportData.getExportTypes());

    final String resultSessionKey = createSessionKey();
    sendToWorkers(priorityProjectExportData, resultSessionKey, priorityProjectExportData.getPriorityProjectKey());
    return resultSessionKey;
  }

  private void sendToWorkers(final ExportData exportData, final String resultSessionKey, final Object projectKey) throws AeriusException {
    try {
      TaskClientFactory.getInstance().sendTask(exportData, createResultCallback(resultSessionKey), WorkerType.REGISTER, QueueEnum.REGISTER_EXPORT);
    } catch (final IOException e) {
      LOG.error("Error while trying to start register export: {}", projectKey, e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  @Override
  public RequestExportResult isExportReady(final String exportId) throws AeriusException {
    RequestExportResult result = null;
    if (getCurrentStatus(exportId) == Status.FINISHED) {
      result = getExportResult(exportId);
    } else if (getCurrentStatus(exportId) == Status.ERROR) {
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return result;
  }

  @Override
  protected RequestExportResult getExportResult(final String sessionKey, final ExportedData exportedData) {
    final RequestExportResult result = new RequestExportResult();
    result.setAmount(exportedData.getFilesInZip());
    result.setTotalSize(exportedData.getFileContent().length);
    result.setDownloadUrl(ExportedDataFileDownloadUtil.getDownloadUrl(sessionKey));
    return result;
  }

}
