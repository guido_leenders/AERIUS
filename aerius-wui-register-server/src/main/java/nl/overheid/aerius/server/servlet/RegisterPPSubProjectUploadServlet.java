/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.db.register.PrioritySubProjectModifyRepository;
import nl.overheid.aerius.db.register.PrioritySubProjectRepository;
import nl.overheid.aerius.register.RegisterImportUtil;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.server.service.RequestInserter;
import nl.overheid.aerius.server.service.RequestUploadService;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportService;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Handles Priority SubProject uploads.
 */
@MultipartConfig
@SuppressWarnings("serial")
@WebServlet("/aerius/" + SharedConstants.IMPORT_PP_SUBPROJECT_SERVLET)
public class RegisterPPSubProjectUploadServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterPPSubProjectUploadServlet.class);

  /**
   * Processes the uploaded file and stores it in the database.
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    final UploadOptions uploadOptions = new UploadOptions();
    uploadOptions.setSupportedFormats(SupportedUploadFormat.PRIORITY_PROJECT_SUBPROJECT);
    uploadOptions.setPersistResults(true);

    final String label = request.getParameter(SharedConstants.IMPORT_LABEL_FIELD_NAME);
    final RequestUploadService<PrioritySubProjectKey> requestUploadService = new RequestUploadService<PrioritySubProjectKey>(
        getPMF(),
        Internationalizer.getLocaleFromCookie(request),
        RegisterRetrieveImportService.UUID_PREFIX_PP_SUBPROJECT,
        RequestFileType.APPLICATION,
        uploadOptions,
        RegisterPermission.ADD_NEW_PRIORITY_SUBPROJECT) {

      @Override
      public PrioritySubProjectKey handleProcessedUpload(final ImportOutput output, final String reference, final UserProfile uploadedBy,
          final InsertRequestFile insertRequestFile) throws AeriusException {
        return insertSubProject(output, reference, uploadedBy, insertRequestFile, label);
      }

      @Override
      protected TaskManagerClient getTaskManagerClient() throws IOException {
        return RegisterPPSubProjectUploadServlet.this.getTaskManagerClient();
      }
    };

    requestUploadService.processRequest(request, response);
  }

  private PrioritySubProjectKey insertSubProject(final ImportOutput output, final String reference, final UserProfile uploadedBy,
      final InsertRequestFile insertRequestFile, final String label) throws AeriusException {
    try (final Connection connection = getPMF().getConnection()) {
      final PriorityProject priorityProject = PriorityProjectRepository.getSkinnedPriorityProjectByReference(connection, reference);
      // if the assign is complete or status of project is initial (no results), we shouldn't be able to upload.
      if (priorityProject.isAssignCompleted() || priorityProject.getRequestState() == RequestState.INITIAL) {
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }

      final PrioritySubProject subProject = RegisterImportUtil.convertToPrioritySubProject(output);
      subProject.setLabel(StringUtils.isEmpty(label) ? null : label);
      final PrioritySubProjectKey subProjectKey = new PrioritySubProjectKey(priorityProject.getKey(), subProject.getReference());

      final PrioritySubProject inDb = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(connection, subProjectKey);
      if (inDb == null) {
        final RequestInserter<PrioritySubProject> insertHelper = new RequestInserter<PrioritySubProject>() {

          @Override
          public PrioritySubProject insertRequest(final Connection connection, final PrioritySubProject sp, final UserProfile editedBy,
              final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
            return PrioritySubProjectModifyRepository.insert(connection, priorityProject.getKey(), sp, editedBy, insertRequestFile);
          }
        };
        final PrioritySubProject insertedSubProject =
            insertHelper.insertInTransaction(connection, output, uploadedBy, subProject, insertRequestFile);

        return new PrioritySubProjectKey(priorityProject.getKey(), insertedSubProject.getReference());
      } else {
        // We need to get the PP in which the SubProject already exists in, this might be different than the current PP
        final PriorityProject inDbPP = PriorityProjectRepository.getSkinnedPriorityProject(
            connection, PrioritySubProjectRepository.getParentProjectIdForSubProject(connection, inDb.getId()));

        throw new ImportDuplicateEntryException(new PrioritySubProjectKey(inDbPP.getKey(), inDb.getReference()));
      }
    } catch (final SQLException e) {
      LOG.error("SQLException while handling ", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  protected PMF getPMF() {
    return ServerPMF.getInstance();
  }

  protected TaskManagerClient getTaskManagerClient() throws IOException {
    return TaskClientFactory.getInstance();
  }
}
