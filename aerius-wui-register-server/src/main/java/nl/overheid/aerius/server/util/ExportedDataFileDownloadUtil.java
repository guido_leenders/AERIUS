/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;


/**
 * Util for downloading files returned by a worker.
 * The 'missing link' between ExportService and ExportDataFileDownloadServlet.
 */
public final class ExportedDataFileDownloadUtil {

  public static final String DOWNLOAD_LOCATION = "aerius/priority-sub-project-export-download";
  /**
   * The name of the id request parameter.
   */
  public static final String PARAMETER_ID = "id";

  private ExportedDataFileDownloadUtil() {
    //util class
  }

  /**
   * Get the download URL for given export ID.
   * @param uuid The ID to use (uuid).
   * @return The full download URL.
   */
  public static String getDownloadUrl(final String uuid) {
    return DOWNLOAD_LOCATION + "?" + PARAMETER_ID + "=" + uuid;
  }

}
