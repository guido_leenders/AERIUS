/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.profile.pas.PASUtil;
import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.db.register.RequestModifyRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;

public abstract class RequestInserter<R extends Request> {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(RequestInserter.class);

  public R insertInTransaction(final Connection connection, final ImportOutput output, final UserProfile editedBy, final R request,
      final InsertRequestFile insertRequestFile) throws AeriusException {
    return insertInTransaction(connection, output, editedBy, request, insertRequestFile, true);
  }

  public R insertInTransaction(final Connection connection, final ImportOutput output, final UserProfile editedBy, final R request,
      final InsertRequestFile insertRequestFile, final boolean persistResults) throws AeriusException {
    final CalculatedScenario calculatedScenario = output.getCalculatedScenario();
    calculatedScenario.setOptions(PASUtil.getCalculationSetOptions(output.getImportedTemporaryPeriod(), output.getPermitCalculationRadiusType()));
    calculatedScenario.setYear(output.getImportedYear());

    try {
      final Transaction transaction = new Transaction(connection);
      try {
        // insert the request
        final R insertedRequest = insertRequest(connection, request, editedBy, insertRequestFile);

        if (persistResults && calculatedScenario.hasCalculations()) {
          // links the results to the request
          if (calculatedScenario instanceof CalculatedComparison) {
            RequestModifyRepository.insertSituationCalculation(
                connection, SituationType.CURRENT, ((CalculatedComparison) calculatedScenario).getCalculationIdOne(), insertedRequest);
            RequestModifyRepository.insertSituationCalculation(
                connection, SituationType.PROPOSED, ((CalculatedComparison) calculatedScenario).getCalculationIdTwo(), insertedRequest);
            CalculationDevelopmentSpaceRepository.insertCalculationDemands(connection,
                ((CalculatedComparison) calculatedScenario).getCalculationIdOne(),
                ((CalculatedComparison) calculatedScenario).getCalculationIdTwo(), false);

          } else if (calculatedScenario instanceof CalculatedSingle) {
            RequestModifyRepository.insertSituationCalculation(
                connection, SituationType.PROPOSED, ((CalculatedSingle) calculatedScenario).getCalculationId(), insertedRequest);
            CalculationDevelopmentSpaceRepository.insertCalculationDemands(connection, ((CalculatedSingle) calculatedScenario).getCalculationId(), 0,
                false);

          } else {
            throw new IllegalArgumentException("Error linking calculation results to new request - calculatedScenario has an unhandled type: "
                + calculatedScenario.getClass().getName());
          }

          // set request to status QUEUED as it already has results
          RequestModifyRepository.updateRequestToQueued(connection, insertedRequest, editedBy);

          // allow to do post operations inside the same transaction
          doPersistResultsPostOperations(connection, insertedRequest, editedBy);
        }

        return insertedRequest;
      } catch (final SQLException se) {
        transaction.rollback();
        throw DatabaseErrorCode.createAeriusException(se, LOG, "Error inserting request");
      } catch (final AeriusException e) {
        transaction.rollback();
        throw e;
      } finally {
        transaction.commit();
      }
    } catch (final SQLException se) {
      throw DatabaseErrorCode.createAeriusException(se, LOG, "Error with transaction while trying to insert request");
    }
  }

  public abstract R insertRequest(final Connection connection, final R request, final UserProfile editedBy,
      final InsertRequestFile insertRequestFile) throws SQLException, AeriusException;

  public void doPersistResultsPostOperations(final Connection connection, final R request, final UserProfile editedBy) throws AeriusException {
    // defaults to no-op
  }

}
