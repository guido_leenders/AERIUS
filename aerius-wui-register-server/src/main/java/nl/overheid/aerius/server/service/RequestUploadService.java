/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.IOException;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.ImportInput;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.util.UserProfileUtil;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;
import nl.overheid.aerius.taskmanager.client.WorkerType;

public abstract class RequestUploadService<R> {

  private static final Logger LOG = LoggerFactory.getLogger(RequestUploadService.class);

  private static final String PARAM_ID = "id";

  private final PMF pmf;
  private final Locale locale;
  private final String uuidPrefix;
  private final RequestFileType requestFileType;
  private final UploadOptions uploadOptions;
  private final RegisterPermission[] permissions;

  protected RequestUploadService(final PMF pmf, final Locale locale, final String uuidPrefix, final RequestFileType requestFileType,
      final UploadOptions uploadOptions, final RegisterPermission... permissions) {
    this.pmf = pmf;
    this.locale = locale;
    this.uuidPrefix = uuidPrefix;
    this.requestFileType = requestFileType;
    this.uploadOptions = uploadOptions;
    this.permissions = permissions;

  }

  public final void processRequest(final HttpServletRequest request, final HttpServletResponse response) {
    processRequest(request, response, false);
  }

  /**
   * Process the upload request.
   * @param request The request object.
   * @param response The response object.
   * @param skipImport Whether to skip the import phase. If skipped the file will not be processed by the worker
   *  and aside from simple fileformat checks and such, no extensive validations will be done.
   */
  public final void processRequest(final HttpServletRequest request, final HttpServletResponse response, final boolean skipImport) {
    final String uuid = UUID.randomUUID().toString();
    final String fullUuid = uuidPrefix + uuid;
    final HttpSession session = request.getSession(true);

    final BaseUploadService service = new BaseUploadService();

    boolean success = false;
    try {
      final UserProfile userProfile = getAuthorizedUser();

      final String dossierId = request.getParameter(PARAM_ID);
      if (StringUtils.isBlank(dossierId)) {
        throw new AeriusException(Reason.IMPORT_REQUIRED_ID_MISSING);
      }

      success = service.processUploadDocument(request);

      if (success) {
        final ImportInput importInput = getImportInput(service);

        if (skipImport) {
          setSuccess(session, fullUuid, handleProcessedUpload(null, dossierId, userProfile, toInsertRequestFile(importInput)));
        } else {
          startImportTask(session, fullUuid, importInput, dossierId, userProfile);
        }
      } else {
        throw service.getServerException();
      }
    } catch (final IOException e) {
      LOG.error("Error while starting import task", e);
      setFailure(session, fullUuid, new AeriusException(Reason.INTERNAL_ERROR));
      success = false;
    } catch (final AeriusException e) {
      setFailure(session, fullUuid, e);
      success = false;
    }

    response.setStatus(HttpServletResponse.SC_CREATED);
    service.sendResponse(response, success, uuid);
  }

  private void startImportTask(final HttpSession session, final String uuid, final ImportInput importInput, final String dossierId,
      final UserProfile userProfile) throws IOException {
    getTaskManagerClient().sendTask(importInput, new TaskResultCallback() {

      @Override
      public void onSuccess(final Object value, final String correlationId, final String messageId) {
        if (value instanceof ImportOutput) {
          onResult((ImportOutput) value);
        } else {
          LOG.error("Received unexpected object {}", value);
          setFailure(session, uuid, new AeriusException(Reason.INTERNAL_ERROR));
        }
      }

      private void onResult(final ImportOutput output) {
        if (output.isSuccess()) {
          try {
            setSuccess(session, uuid, handleProcessedUpload(output, dossierId, userProfile, toInsertRequestFile(importInput)));
          } catch (final AeriusException e) {
            setFailure(session, uuid, e);
          }
        } else {
          setFailure(session, uuid, output.getExceptions().get(0));
        }
      }

      @Override
      public void onFailure(final Exception exception, final String correlationId, final String messageId) {
        if (exception instanceof AeriusException) {
          setFailure(session, uuid, (AeriusException) exception);
        } else {
          LOG.error("Received exception from worker, converting to internal error", exception);
          setFailure(session, uuid, new AeriusException(Reason.INTERNAL_ERROR));
        }
      }
    }, WorkerType.IMPORT_REGISTER, QueueEnum.IMPORT);
  }

  /**
   * Handle the processed upload.
   * @param service The UploadService containing the processed upload.
   * @param dossierId The dossierId.
   * @param uploadedBy The userProfile the file is uploaded by.
   * @param insertRequestFile The request file to be inserted.
   * @return R result if it's needed by the client or null.
   * @throws AeriusException On validation errors and such.
   */
  public abstract R handleProcessedUpload(ImportOutput output, final String dossierId, final UserProfile uploadedBy,
      final InsertRequestFile insertRequestFile) throws AeriusException;

  private UserProfile getAuthorizedUser() throws AeriusException {
    UserProfile userProfile;
    try {
      userProfile = UserService.getCurrentUserProfileStrict(pmf);
      UserProfileUtil.checkPermission(userProfile, permissions);
    } catch (final AeriusException e) {
      LOG.error("Error while getting authorized user", e);
      userProfile = null;
    }
    return userProfile;
  }

  private ImportInput getImportInput(final BaseUploadService service) throws IOException, AeriusException {
    final byte[] cacheContent = IOUtils.toByteArray(service.getPart().getInputStream());
    final ImportType type = ImportType.determineByFilename(service.getFileName());
    if (!isValidType(type)) {
      throw new AeriusException(Reason.IMPORT_FILE_TYPE_NOT_ALLOWED);
    }
    final ImportInput input = new ImportInput(service.getFileName(), type, cacheContent);
    input.setReturnSources(uploadOptions.isReturnSources());
    input.setPersistResults(uploadOptions.isPersistResults());
    input.setValidate(uploadOptions.isValidate());
    input.setValidateMetadata(uploadOptions.isValidateMetadata());
    input.setCheckLimits(false);
    input.setLocale(locale);
    return input;
  }

  private boolean isValidType(final ImportType importType) {
    return SupportedUploadFormat.isSupported(uploadOptions.getSupportedFormats(), importType);
  }

  private InsertRequestFile toInsertRequestFile(final ImportInput input) {
    final InsertRequestFile insertRequestFile = new InsertRequestFile();
    insertRequestFile.setFileContent(input.getFileContent());
    insertRequestFile.setFileFormat(input.getFileType().getFileFormat());
    insertRequestFile.setFileName(input.getFileName());
    insertRequestFile.setRequestFileType(requestFileType);
    return insertRequestFile;
  }

  private void setSuccess(final HttpSession session, final String uuid, final R result) {
    session.setAttribute(uuid, result);
  }

  private void setFailure(final HttpSession session, final String uuid, final AeriusException e) {
    session.setAttribute(uuid, e);
  }

  /**
   * Returns the TaskManagerClient
   * @return
   * @throws IOException
   */
  protected abstract TaskManagerClient getTaskManagerClient() throws IOException;
}
