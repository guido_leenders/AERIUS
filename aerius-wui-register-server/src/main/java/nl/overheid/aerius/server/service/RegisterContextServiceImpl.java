/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ContextRepository;
import nl.overheid.aerius.db.common.LayerRepository;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.register.FilterRepository;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.auth.AdminPermission;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.RegisterUserContext;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Service with all methods related to contextual data. This is both global context data which is static during a user session as well as user
 * specific context data.
 */
public class RegisterContextServiceImpl extends AbstractContextService<RegisterSession> {

  private static final List<Class<? extends Filter>> FILTER_LIST = Arrays.asList(NoticeDashboardFilter.class, PermitDashboardFilter.class,
      PermitFilter.class, NoticeFilter.class, PriorityProjectFilter.class, UserManagementFilter.class);

  private static final Logger LOG = LoggerFactory.getLogger(RegisterContextServiceImpl.class);

  public RegisterContextServiceImpl(final PMF pmf, final RegisterSession session) {
    super(pmf, session);
  }

  @Override
  public Context getContext() throws AeriusException {
    try (Connection con = pmf.getConnection()) {

      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(con);

      final boolean admin = userProfile.hasPermission(AdminPermission.ADMINISTER_USERS);

      return ContextRepository.getRegisterContext(con, getMessagesKey(), admin);
    } catch (final SQLException e) {
      LOG.error("Could nog get Register Context from database", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public UserContext getUserContext() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final RegisterUserContext u = new RegisterUserContext();
      u.setLayers(LayerRepository.getLayers(con, getMessagesKey()));
      u.setBaseLayer(LayerRepository.getBaseLayer(con, getMessagesKey()));

      u.setHandlers(UserRepository.getAuthorityUserMap(con));
      u.setEmissionValueKey(new EmissionValueKey(SharedConstants.getCurrentYear(), Substance.NH3));

      u.setEmissionResultValueDisplaySettings(ContextRepository.getEmissionResultValueDisplaySettings(con));

      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(con);
      u.setUserProfile(userProfile);
      setFilters(con, u, userProfile, FILTER_LIST);
      return u;
    } catch (final SQLException e) {
      LOG.error("Could not retrieve whole usercontext from DB.", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private static void setFilters(final Connection con, final RegisterUserContext u, final UserProfile userProfile,
      final List<Class<? extends Filter>> clazzes) throws SQLException {
    for (final Class<? extends Filter> clazz : clazzes) {
      u.useFilter(FilterRepository.getFilter(con, userProfile, clazz));
    }
  }

  @Override
  public void closeSession(final String lastCalculationKey) {
    session.close();
  }
}
