/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.NoticeRepository;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.RegisterSearchSuggestionType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.RegisterSearchService;
import nl.overheid.aerius.shared.util.UserProfileUtil;

public class RegisterSearchServiceImpl implements RegisterSearchService {
  private static final Logger LOG = LoggerFactory.getLogger(RegisterSearchServiceImpl.class);
  private final PMF pmf;

  /**
   * @param pmf
   */
  public RegisterSearchServiceImpl(final PMF pmf) {
    this.pmf = pmf;
  }

  @Override
  public ArrayList<RegisterSearchSuggestion> getSuggestions(final String searchString) throws AeriusException {
    final UserProfile userProfile = UserService.getCurrentUserProfileStrict(pmf);

    final ArrayList<RegisterSearchSuggestion> suggestions = new ArrayList<>();

    try (final Connection con = pmf.getConnection()) {
      for (final RegisterSearchSuggestionType searchType : RegisterSearchSuggestionType.values()) {
        if (!UserProfileUtil.hasPermission(userProfile, searchType.getPermission())) {
          continue;
        }

        switch (searchType) {
        case PERMIT:
          for (final Permit permit : PermitRepository.search(con, searchString)) {
            addSearchEntry(suggestions, RegisterSearchSuggestionType.PERMIT, permit,
                permit.getDossierMetaData() != null ? permit.getDossierMetaData().getDossierId() : permit.getReference());
          }
          break;
        case NOTICE:
          for (final Notice notice : NoticeRepository.search(con, searchString)) {
            addSearchEntry(suggestions, RegisterSearchSuggestionType.NOTICE, notice, notice.getScenarioMetaData().getCorporation());
          }
          break;
        case PRIORITY_PROJECT:
          for (final PriorityProject project : PriorityProjectRepository.search(con, searchString)) {
            addSearchEntry(suggestions, RegisterSearchSuggestionType.PRIORITY_PROJECT, project,
                project.getDossierMetaData().getDossierId());
          }
          break;
//        case PRIORITY_SUBPROJECT:
          // TODO; enable if we have a SubProjectKey present - also in RegisterSearchAction
//        for (final PrioritySubProject subProject : PrioritySubProjectRepository.search(con, searchString)) {
//          addSearchEntry(suggestions, RegisterSearchSuggestionType.PRIORITY_SUBPROJECT, subProject,
//              subProject.getScenarioMetaData() != null ? subProject.getScenarioMetaData().getProjectName() : subProject.getReference());
//        }
        default:
          break;
        }
      }
    } catch (final SQLException e) {
      LOG.error("Error while searching in Register.", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }

    return suggestions;
  }

  private void addSearchEntry(final ArrayList<RegisterSearchSuggestion> suggestions, final RegisterSearchSuggestionType type,
      final Request request, final String name) {
    final RegisterSearchSuggestion suggestion = new RegisterSearchSuggestion(request.getId(), name, type);
    suggestion.setRequest(request);
    suggestions.add(suggestion);
  }
}
