/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.FilterRepository;
import nl.overheid.aerius.db.register.PriorityProjectModifyRepository;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.db.register.PrioritySubProjectModifyRepository;
import nl.overheid.aerius.db.register.PrioritySubProjectRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.db.register.ReviewInfoRepository;
import nl.overheid.aerius.server.util.RequestFileDownloadUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;
import nl.overheid.aerius.shared.service.PriorityProjectService;
import nl.overheid.aerius.shared.util.UserProfileUtil;

/**
 * Service with CRUD methods for priority projects.
 */
public class PriorityProjectServiceImpl implements PriorityProjectService {

  private static final Logger LOG = LoggerFactory.getLogger(PriorityProjectServiceImpl.class);

  private final PMF pmf;
  private final RegisterSession session;

  public PriorityProjectServiceImpl(final PMF pmf, final RegisterSession session) {
    this.pmf = pmf;
    this.session = session;
  }

  @Override
  public PriorityProject fetchPriorityProject(final PriorityProjectKey key) throws AeriusException {
    PriorityProject priorityProject = null;
    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);
      priorityProject = PriorityProjectRepository.getPriorityProjectByKey(connection, key, session.getLocale());
      RequestFileDownloadUtil.setDownloadUrls(connection, priorityProject);
      for (final PrioritySubProject subProject : priorityProject.getSubProjects()) {
        RequestFileDownloadUtil.setDownloadUrls(connection, subProject);
      }
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving priority project " + key);
    }
    return priorityProject;
  }

  @Override
  public PrioritySubProject fetchPrioritySubProject(final PrioritySubProjectKey key) throws AeriusException {
    PrioritySubProject prioritySubProject = null;
    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);
      prioritySubProject = PrioritySubProjectRepository.getPrioritySubProjectByKey(connection, key);
      RequestFileDownloadUtil.setDownloadUrls(connection, prioritySubProject);
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving priority sub project " + key);
    }
    return prioritySubProject;
  }

  @Override
  public ArrayList<PriorityProject> getPriorityProjects(final int offset, final int size, final PriorityProjectFilter filter) throws AeriusException {
    ArrayList<PriorityProject> foundPriorityProjects = null;

    try (final Connection connection = pmf.getConnection()) {
      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(connection);
      foundPriorityProjects = PriorityProjectRepository.getPriorityProjects(connection, size, offset, filter);

      FilterRepository.insertOrUpdateFilter(connection, filter, userProfile);
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving priority projects");
    }
    return foundPriorityProjects;
  }

  @Override
  public void updatePriorityProject(final PriorityProjectKey key, final PriorityProject project, final long lastModified) throws AeriusException {
    checkPermissionAndAuthority(project, RegisterPermission.UPDATE_PRIORITY_PROJECT);

    try (final Connection con = pmf.getConnection()) {
      // Update the priority project using the new metadata
      PriorityProjectModifyRepository.update(con, key, project, lastModified);
    } catch (final SQLException e) {
      handleSQLException(e, "updating priority project");
    }
  }

  @Override
  public void updatePriorityProjectAssignComplete(final PriorityProjectKey key, final boolean assignmentComplete, final long lastModified)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByKey(con, key);
      if (assignmentComplete) {
        checkPermissionAndAuthority(priorityProject, RegisterPermission.UPDATE_PRIORITY_PROJECT);
      } else {
        checkPermission(RegisterPermission.REVERT_PRIORITY_PROJECT_ASSIGN_COMPLETE);
      }

      final boolean hasActualisation =
          RequestRepository.getSpecificRequestFile(con, priorityProject.getReference(), RequestFileType.PRIORITY_PROJECT_ACTUALISATION) != null;

      if (!hasActualisation && priorityProject.hasNoPendingSubProjects()) {
        PriorityProjectModifyRepository.updateAssignComplete(
            con, key, assignmentComplete, UserService.getCurrentUserProfileStrict(con), lastModified);
      }
    } catch (final SQLException e) {
      handleSQLException(e, "updating priority project assign complete");
    }
  }

  @Override
  public void deletePriorityProject(final PriorityProjectKey key) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);

      final PriorityProject priorityProject = PriorityProjectRepository.getSkinnedPriorityProjectByKey(connection, key);
      checkPermissionAndAuthority(priorityProject, RegisterPermission.DELETE_PRIORITY_PROJECT);

      PriorityProjectModifyRepository.delete(connection, key, UserService.getCurrentUserProfileStrict(pmf));
    } catch (final SQLException e) {
      handleSQLException(e, "deleting priority project " + key);
    }
  }

  @Override
  public void deletePriorityProjectActualisationFile(final PriorityProjectKey key, final long lastModified) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);

      final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByKey(connection, key);
      checkPermissionAndAuthority(priorityProject, RegisterPermission.ADD_NEW_PRIORITY_PROJECT);

      if (priorityProject.getRequestState() != RequestState.INITIAL) {
        // file checking is not needed as the repository only adds the audit trail if the file was present
        PriorityProjectModifyRepository.deleteActualisation(connection, key, UserService.getCurrentUserProfileStrict(pmf), lastModified);
      }
    } catch (final SQLException e) {
      handleSQLException(e, "deleting actualisation request file of priority project " + key);
    }
  }

  @Override
  public ArrayList<PrioritySubProjectReviewInfo> getPrioritySubProjectReviewInfo(final PrioritySubProjectKey key) throws AeriusException {
    ArrayList<PrioritySubProjectReviewInfo> rows = null;
    try (final Connection con = pmf.getConnection()) {
      rows = ReviewInfoRepository.getPrioritySubProjectReviewInfo(con, key);
    } catch (final SQLException e) {
      handleSQLException(e, "fetching priority subproject review info rows");
    }
    return rows;
  }

  @Override
  public void changeStatePrioritySubProject(final PrioritySubProjectKey key, final RequestState state, final long lastModified)
      throws AeriusException {
    if (state == RequestState.REJECTED_FINAL) {
      // Calling this function with this state is a programmer fault, rejectPrioritySubProject() should be used instead.
      // The check is done here, because the repository function thinks its fine (after all, rejectPrioritySubProject() uses it too).
      LOG.error("Do not call {}.changeStatePrioritySubProject() with REJECTED_FINAL; call rejectPrioritySubProject() instead.",
          PriorityProjectServiceImpl.class.getSimpleName());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }

    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);

      final PrioritySubProject subProject = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(connection, key);
      checkPermissionAndAuthority(subProject, RegisterPermission.UPDATE_PRIORITY_SUBPROJECT_STATE);
      checkChangeStateSpecialPermissions(subProject.getAuthority(), subProject.getRequestState(), state);

      PrioritySubProjectModifyRepository.updateState(connection, key, state, UserService.getCurrentUserProfileStrict(connection), lastModified);
    } catch (final SQLException e) {
      handleSQLException(e, "update priority subproject state: " + key);
    }
  }

  private void checkChangeStateSpecialPermissions(final Authority authority, final RequestState oldState, final RequestState newState)
      throws AeriusException, AuthorizationException {
    if (oldState == RequestState.ASSIGNED_FINAL
        && newState == RequestState.REJECTED_WITHOUT_SPACE
        && !hasPermissionForAuthority(authority, RegisterPermission.UPDATE_PRIORITY_SUBPROJECT_STATE_ASSIGNED_FINAL_TO_REJECTED)) {
      throw new AuthorizationException();
    }
  }

  @Override
  public void deletePrioritySubProject(final PrioritySubProjectKey key) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);

      final PrioritySubProject subProject = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(connection, key);
      checkPermissionAndAuthority(subProject, RegisterPermission.DELETE_PRIORITY_SUBPROJECT);

      PrioritySubProjectModifyRepository.delete(connection, key, UserService.getCurrentUserProfileStrict(pmf));
    } catch (final SQLException e) {
      handleSQLException(e, "deleting priority subproject " + key);
    }
  }

  /**
   * Rejecting a subproject is the combination of changing the state to REJECTED_FINAL and then deleting
   * the permit. While that state is virtual, it will correctly fill the audit trail.
   */
  @Override
  public void rejectPrioritySubProject(final PrioritySubProjectKey key, final long lastModified) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExisting(connection, key);

      final PrioritySubProject subProject = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(connection, key);
      checkPermissionAndAuthority(subProject, RegisterPermission.UPDATE_PRIORITY_SUBPROJECT_STATE_REJECT_FINAL);

      // Update state + audit, then delete
      PrioritySubProjectModifyRepository.updateState(connection, key, RequestState.REJECTED_FINAL,
          UserService.getCurrentUserProfileStrict(pmf), lastModified);
      PrioritySubProjectModifyRepository.delete(connection, key, UserService.getCurrentUserProfileStrict(pmf));
    } catch (final SQLException e) {
      handleSQLException(e, "rejecting priority subproject " + key);
    }
  }

  private void checkExisting(final Connection connection, final DossierAuthorityKey key) throws SQLException, AeriusException {
    if (!PriorityProjectRepository.priorityProjectExists(connection, key)) {
      throw new AeriusException(Reason.PRIORITY_PROJECT_UNKNOWN, key.getDossierId());
    }
  }

  private void checkExisting(final Connection connection, final PrioritySubProjectKey key) throws SQLException, AeriusException {
    if (!PrioritySubProjectRepository.prioritySubProjectExists(connection, key)) {
      throw new AeriusException(Reason.PRIORITY_SUBPROJECT_UNKNOWN, key.getReference());
    }
  }

  private boolean hasPermissionForAuthority(final Authority authority, final RegisterPermission permission) throws AeriusException {
    return UserProfileUtil.hasPermissionForAuthority(UserService.getCurrentUserProfileStrict(pmf), authority, permission);
  }

  private void checkPermissionAndAuthority(final PriorityProject priorityProject, final RegisterPermission... permissions) throws AeriusException {
    UserProfileUtil.checkPermissionAndAuthority(UserService.getCurrentUserProfileStrict(pmf),
        priorityProject.getAuthority(), permissions);
  }

  private void checkPermission(final RegisterPermission... permissions) throws AeriusException {
    UserProfileUtil.checkPermission(UserService.getCurrentUserProfileStrict(pmf), permissions);
  }

  private void checkPermissionAndAuthority(final PrioritySubProject subProject, final RegisterPermission... permissions) throws AeriusException {
    UserProfileUtil.checkPermissionAndAuthority(UserService.getCurrentUserProfileStrict(pmf),
        subProject.getAuthority(), permissions);
  }

  private void handleSQLException(final SQLException e, final String action) throws AeriusException {
    LOG.error("Error while {}", action, e);
    throw new AeriusException(Reason.SQL_ERROR);
  }

}
