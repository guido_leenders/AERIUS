/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import nl.overheid.aerius.paa.PAAImport;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public final class ScenarioUtil {

  private ScenarioUtil() {
    // util class
  }
  public static ScenarioGMLs getScenarioGMLs(final byte[] content) throws AeriusException {
    ScenarioGMLs scenarioGMLs = new ScenarioGMLs();

    // If not null, extract and combine the GML strings
    if (content != null) {
      try (final ByteArrayInputStream bis = new ByteArrayInputStream(content)) {
        scenarioGMLs = PAAImport.importPAAFromStream(bis);
      } catch (final IOException e) {
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }
    return scenarioGMLs;
  }
}
