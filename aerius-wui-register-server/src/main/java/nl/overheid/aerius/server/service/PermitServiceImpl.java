/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.DashboardRepository;
import nl.overheid.aerius.db.register.DevelopmentRuleRepository;
import nl.overheid.aerius.db.register.FilterRepository;
import nl.overheid.aerius.db.register.PermitModifyRepository;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.db.register.RequestModifyRepository;
import nl.overheid.aerius.db.register.ReviewInfoRepository;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.server.util.RequestFileDownloadUtil;
import nl.overheid.aerius.server.util.ScenarioUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.PermitSummary;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;
import nl.overheid.aerius.shared.service.PermitService;
import nl.overheid.aerius.shared.util.UserProfileUtil;

public class PermitServiceImpl implements PermitService {

  private static final Logger LOG = LoggerFactory.getLogger(PermitServiceImpl.class);
  private final RegisterSession session;
  private final PMF pmf;

  public PermitServiceImpl(final PMF pmf, final RegisterSession session) {
    this.session = session;
    this.pmf = pmf;
  }

  @Override
  public void delete(final PermitKey permitKey) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);

      //Get the permit
      final Permit permit = PermitRepository.getSkinnedPermitByKey(connection, permitKey);
      //check if current user has the proper permissions.
      checkPermissionAndAuthority(permit, RegisterPermission.DELETE_PERMIT_ALL, RegisterPermission.DELETE_PERMIT_INACTIVE_INITIAL);

      //extra check when user only has the DELETE_PERMIT_INACTIVE_INITIAL permission.
      if (!hasPermissionForAuthority(permit.getAuthority(), RegisterPermission.DELETE_PERMIT_ALL)
          && permit.getRequestState() != RequestState.INITIAL) {
        throw new AuthorizationException();
      }

      // Delete the permit (user has proper permissions at this point)
      final UserProfile currentUserProfile = UserService.getCurrentUserProfile(connection);
      PermitModifyRepository.deletePermit(connection, permit, currentUserProfile);
    } catch (final SQLException e) {
      handleSQLException(e, "deleting permit " + permitKey);
    }
  }

  @Override
  public Permit fetchPermit(final PermitKey permitKey) throws AeriusException {
    // Get the permit
    Permit permit = null;
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);
      permit = PermitRepository.getPermitByKey(connection, permitKey);
      RequestFileDownloadUtil.setDownloadUrls(connection, permit);
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving permit " + permitKey);
    }
    return permit;
  }

  @Override
  public void changeState(final PermitKey permitKey, final RequestState state, final long lastModified) throws AeriusException {
    if (state == RequestState.REJECTED_FINAL) {
      // Calling this function with this state is a programmer fault, reject() should be used instead.
      // The check is done here, because the repository function thinks its fine (after all, reject() uses it too).
      LOG.error("Do not call {}.changeState() with REJECTED_FINAL; call reject() instead.", PermitServiceImpl.class.getSimpleName());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }

    // Update its state.
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);

      final Permit permit = PermitRepository.getSkinnedPermitByKey(connection, permitKey);

      // Permission checks (not done in the database function)
      checkPermissionAndAuthority(permit, RegisterPermission.UPDATE_PERMIT_STATE);
      checkChangeStateSpecialPermissions(permit.getAuthority(), permit.getRequestState(), state);

      PermitModifyRepository.updateState(connection, permitKey, state, UserService.getCurrentUserProfile(connection), lastModified);
    } catch (final SQLException e) {
      handleSQLException(e, "changing permit state: " + permitKey);
    }
  }

  private void checkChangeStateSpecialPermissions(final Authority authority, final RequestState oldState, final RequestState newState)
      throws AeriusException, AuthorizationException {
    if (oldState == RequestState.QUEUED
        && (newState == RequestState.PENDING_WITH_SPACE || newState == RequestState.PENDING_WITHOUT_SPACE)
        && !hasPermissionForAuthority(authority, RegisterPermission.UPDATE_PERMIT_STATE_DEQUEUE)) {
      throw new AeriusException(Reason.PERMIT_DEQUEUE_NO_AUTHORIZATION);
    }
    if ((oldState == RequestState.PENDING_WITH_SPACE || oldState == RequestState.PENDING_WITHOUT_SPACE)
        && newState == RequestState.QUEUED
        && !hasPermissionForAuthority(authority, RegisterPermission.UPDATE_PERMIT_STATE_DEQUEUE)) {
      throw new AeriusException(Reason.PERMIT_ENQUEUE_NO_AUTHORIZATION);
    }
    if (oldState == RequestState.REJECTED_WITHOUT_SPACE
        && newState == RequestState.QUEUED
        && !hasPermissionForAuthority(authority,
            RegisterPermission.UPDATE_PERMIT_STATE_ENQUEUE_REJECTED_WITHOUT_SPACE)) {
      throw new AuthorizationException();
    }
  }

  /**
   * Rejecting a permit is the combination of changing the state to REJECTED_FINAL and then deleting
   * the permit. While that state is virtual, it will correctly fill the audit trail.
   */
  @Override
  public void reject(final PermitKey permitKey, final long lastModified) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);

      Permit permit = PermitRepository.getSkinnedPermitByKey(connection, permitKey);
      checkPermissionAndAuthority(permit, RegisterPermission.UPDATE_PERMIT_STATE_REJECT_FINAL);

      // Corner case: permissions to dequeue
      if (permit.getRequestState() == RequestState.QUEUED
          && !hasPermissionForAuthority(permit.getAuthority(), RegisterPermission.UPDATE_PERMIT_STATE_DEQUEUE)) {
        throw new AeriusException(Reason.PERMIT_DEQUEUE_NO_AUTHORIZATION, permitKey.getDossierId());
      }

      // Update state + audit
      final UserProfile currentUserProfile = UserService.getCurrentUserProfile(connection);
      PermitModifyRepository.updateState(connection, permitKey, RequestState.REJECTED_FINAL, currentUserProfile, lastModified);

      // Delete the permit
      permit = PermitRepository.getSkinnedPermitByKey(connection, permitKey);
      PermitModifyRepository.deletePermit(connection, permit, currentUserProfile);
    } catch (final SQLException e) {
      handleSQLException(e, "rejecting permit " + permitKey);
    }
  }

  @Override
  public void update(final PermitKey permitKey, final DossierMetaData dossierMetaData, final long lastModified) throws AeriusException {
    // Dossier ID not allowed to be null/empty after edit (UI should check for this as well and give some good feedback)
    if (StringUtils.isEmpty(dossierMetaData.getDossierId())) {
      LOG.error("Permit dossier ID is empty or null");
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }

    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);

      final Permit permit = PermitRepository.getSkinnedPermitByKey(connection, permitKey);
      int amountOfPermissionsMatched = 0;

      // We're going to use the the values from the DB for now if the user does not have permission to change them.
      final DossierMetaData newMetaData = permit.getDossierMetaData();
      final Authority currentAuthority = newMetaData.getHandler().getAuthority();
      if (hasPermissionForAuthority(currentAuthority, RegisterPermission.UPDATE_PERMIT_DOSSIER_NUMBER)) {
        newMetaData.setDossierId(dossierMetaData.getDossierId());
        amountOfPermissionsMatched++;
      }
      if (hasPermissionForAuthority(currentAuthority, RegisterPermission.UPDATE_PERMIT_HANDLER)) {
        newMetaData.setHandler(dossierMetaData.getHandler());
        amountOfPermissionsMatched++;
      }
      if (hasPermissionForAuthority(currentAuthority, RegisterPermission.UPDATE_PERMIT_REMARKS)) {
        newMetaData.setRemarks(dossierMetaData.getRemarks());
        amountOfPermissionsMatched++;
      }
      if (hasPermissionForAuthority(currentAuthority, RegisterPermission.UPDATE_PERMIT_DATE)
          && (permit.getRequestState() == RequestState.INITIAL || permit.getRequestState() == RequestState.QUEUED)) {
        newMetaData.setReceivedDate(dossierMetaData.getReceivedDate());
        amountOfPermissionsMatched++;
      }
      if (amountOfPermissionsMatched == 0) {
        throw new AuthorizationException();
      }

      // Update the permit using the new metadata
      PermitModifyRepository.updatePermit(connection, permitKey, newMetaData, UserService.getCurrentUserProfile(connection), lastModified);
    } catch (final SQLException e) {
      handleSQLException(e, "updating permit: " + permitKey);
    }
  }

  @Override
  public void updateMark(final PermitKey permitKey, final boolean mark) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);

      // Get the permit
      final Permit permit = PermitRepository.getSkinnedPermitByKey(connection, permitKey);
      // check if current user has the proper permissions.
      checkPermissionAndAuthority(permit, RegisterPermission.UPDATE_PERMIT_MARK);

      RequestModifyRepository.updateMarked(connection, permit, mark);
    } catch (final SQLException e) {
      handleSQLException(e, "updating mark " + permitKey);
    }
  }

  private boolean hasPermissionForAuthority(final Authority currentAuthority, final RegisterPermission permission) throws AeriusException {
    return UserProfileUtil.hasPermissionForAuthority(UserService.getCurrentUserProfileStrict(pmf), currentAuthority, permission);
  }

  private void checkPermissionAndAuthority(final Permit permit, final RegisterPermission... permissions) throws AeriusException {
    UserProfileUtil.checkPermissionAndAuthority(UserService.getCurrentUserProfileStrict(pmf), permit.getAuthority(), permissions);
  }

  @Override
  public ScenarioGMLs getGML(final PermitKey permitKey) throws AeriusException {
    ScenarioGMLs scenarioGMLs = new ScenarioGMLs();
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);

      scenarioGMLs = ScenarioUtil.getScenarioGMLs(PermitRepository.getPermitPDFContent(connection, permitKey));
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving GML for :" + permitKey);
    }
    return scenarioGMLs;
  }

  @Override
  public PermitKey isCalculationFinished(final PermitKey permitKey) throws AeriusException {
    PermitKey finishedKey = null;
    try (final Connection connection = pmf.getConnection()) {
      checkExistingPermit(connection, permitKey);
      finishedKey = PermitRepository.isCalculationFinished(connection, permitKey) ? permitKey : null;
    } catch (final SQLException e) {
      handleSQLException(e, "determining permit is finished: " + permitKey);
    }
    return finishedKey;
  }

  @Override
  public PermitSummary getPermitSummary(final PermitKey permitKey) throws AeriusException {
    final PermitSummary permitSummary = new PermitSummary();
    permitSummary.setPermitKey(permitKey);
    final byte[] content;
    try {
      try (final Connection con = pmf.getConnection()) {
        checkExistingPermit(con, permitKey);
        final HashSet<DevelopmentRuleResultList> developmentRuleResultsByArea = DevelopmentRuleRepository.getPermitResultsByArea(con,
            permitKey);

        permitSummary.setDevelopmentRuleResults(getDevelopmentRuleResults(developmentRuleResultsByArea));
        permitSummary.setReviewInfo(ReviewInfoRepository.getPermitReviewInfo(con, permitKey));

        // Get the PDF content from the repo
        content = PermitRepository.getPermitPDFContent(con, permitKey);
      } // no nesting of connection, so close db connection here.
      // If not null, parse GML content
      if (content != null) {
        try (final ByteArrayInputStream inputStream = new ByteArrayInputStream(content)) {
          final Importer importer = new Importer(pmf, session.getLocale());
          final ImportResult importResult = importer.convertInputStream2ImportResult(permitKey.getDossierId() + ".pdf", inputStream);

          // Get the last sources in this scenario. May seem a little convoluted but this is guaranteed to succeed.
          final int proposedSituationId = importResult.getSourceLists().get(importResult.getSourceLists().size() - 1).getId();
          final EmissionSourceList sources = importResult.getSources(proposedSituationId);

          permitSummary.setEmissionSources(sources);
        } catch (final IOException e) {
          LOG.error("[getPermitSummary] Could not parse saved GML for permit: {}", permitKey);
          throw new AeriusException(Reason.GML_PARSE_ERROR);
        }
      }
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving permit summary:" + permitKey);
    }
    return permitSummary;
  }

  /**
   * @param developmentRuleResultsByArea
   * @return
   */
  private List<CalculationAreaSummaryResult> getDevelopmentRuleResults(final HashSet<DevelopmentRuleResultList> dRulesList) {
    final List<CalculationAreaSummaryResult> casrList = new ArrayList<>();
    for (final DevelopmentRuleResultList dRules : dRulesList) {
      final CalculationAreaSummaryResult casr = new CalculationAreaSummaryResult();
      casr.setArea(dRules.getAssessmentArea());
      casr.getDevelopmentRuleResults().addAll(dRules.getDevelopmentRuleResults());
      casrList.add(casr);
    }
    return casrList;
  }

  @Override
  public ArrayList<Permit> getPermits(final int offset, final int size, final PermitFilter filter) throws AeriusException {
    ArrayList<Permit> foundPermits = null;
    try (final Connection connection = pmf.getConnection()) {
      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(connection);
      //first find the permits.
      foundPermits = PermitRepository.getPermits(connection, size, offset, filter);
      //next save the filter (avoiding saving a filter that crashes retrieval).
      FilterRepository.insertOrUpdateFilter(connection, filter, userProfile);
      //and return results.
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving permits");
    }
    return foundPermits;
  }

  @Override
  public ArrayList<DefaultDashboardRow> getPermitDashboardRows(final PermitDashboardFilter filter) throws AeriusException {
    ArrayList<DefaultDashboardRow> rows = null;

    try (final Connection con = pmf.getConnection()) {
      final UserProfile userProfile = UserService.getCurrentUserProfileStrict(con);
      rows = DashboardRepository.getPermitRows(con, filter);
      FilterRepository.insertOrUpdateFilter(con, filter, userProfile);
    } catch (final SQLException e) {
      handleSQLException(e, "fetching dashboard permit rows");
    }

    return rows;
  }

  private void checkExistingPermit(final Connection connection, final PermitKey permitKey) throws SQLException, AeriusException {
    if (!PermitRepository.permitExists(connection, permitKey)) {
      throw new AeriusException(Reason.PERMIT_UNKNOWN, permitKey == null ? null : permitKey.getDossierId());
    }
  }

  private void handleSQLException(final SQLException e, final String action) throws AeriusException {
    LOG.error("Error while {}", action, e);
    throw new AeriusException(Reason.SQL_ERROR);
  }

}
