/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.util.RequestFileDownloadUtil;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.EnumUtil;

/**
 * Allows the user to download the file content as saved by an permit.
 */
@WebServlet("/" + RequestFileDownloadUtil.DOWNLOAD_LOCATION)
public class RequestFileDownloadServlet extends AbstractDownloadServlet {

  private static final long serialVersionUID = -7752663404643966555L;

  private static final Logger LOG = LoggerFactory.getLogger(RequestFileDownloadServlet.class);

  private static final String REFERENCE_STRING = "[referenceString]";

  private static final String APPLICATION_FILENAME = "Berekening " + REFERENCE_STRING;
  private static final String DECREE_FILENAME = "Bijlage bij Besluit " + REFERENCE_STRING;
  private static final String DETAIL_DECREE_FILENAME = "Detailkaarten " + REFERENCE_STRING;
  private static final String UNKNOWN_TYPE_FILENAME = "Onbekend " + REFERENCE_STRING;

  private final ServerPMF pmf = ServerPMF.getInstance();

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException {
    final String hash = request.getParameter(RequestFileDownloadUtil.PARAMETER_HASH);
    final String referenceString = request.getParameter(RequestFileDownloadUtil.PARAMETER_REFERENCE);
    final RequestFileType fileType = EnumUtil.get(RequestFileType.class, request.getParameter(RequestFileDownloadUtil.PARAMETER_DOCUMENT_TYPE));

    // Validate the parameters.
    if (StringUtils.isEmpty(hash) || StringUtils.isEmpty(referenceString)) {
      missingRequiredParameter(response);
      return;
    }

    try (final Connection con = pmf.getConnection()) {
      if (RequestFileDownloadUtil.getHash(con, referenceString, fileType).equals(hash)) {
        handle(con, response, referenceString, fileType);
      } else {
        LOG.error("Hash for requested reference was incorrect.");
        internalError(response);
      }
    } catch (final AeriusException e) {
      LOG.error("AeriusException occured.", e);
      internalError(response);
    } catch (final SQLException e) {
      LOG.error("SQL error occured.", e);
      internalError(response);
    } catch (final NoSuchAlgorithmException e) {
      LOG.error("Could not find digest algorithm.", e);
      internalError(response);
    }
  }

  private void handle(final Connection con, final HttpServletResponse response, final String referenceString, final RequestFileType fileType)
      throws SQLException {
    final RequestFile requestFile = getRequestFile(con, referenceString, fileType);
    if (requestFile != null) {
      final byte[] fileContent = RequestRepository.getRequestFileContent(con, referenceString, fileType);
      response.setContentType(getContentType(requestFile));
      response.setHeader("Content-Disposition", "attachment; filename=\""
          + getFileName(requestFile, referenceString)
          + "\"");

      try {
        final OutputStream out = response.getOutputStream();
        out.write(fileContent);
      } catch (final IOException e) {
        LOG.warn("Error while sending file content", e);
      }
    } else {
      LOG.error("File in the database seems empty, this should never be the case.");
      internalError(response);
    }
  }

  private String getContentType(final RequestFile requestFile) {
    final String mimeType;
    switch (requestFile.getFileFormat()) {
    case GML:
      mimeType = MIMETYPE_GML;
      break;
    case ZIP:
      mimeType = MIMETYPE_ZIP;
      break;
    default:
      mimeType = MIMETYPE_PDF;
      break;
    }
    return mimeType;
  }

  private String getFileName(final RequestFile requestFile, final String referenceString) {
    final String fileName;
    if (!StringUtils.isEmpty(requestFile.getFileName())) {
      fileName = requestFile.getFileName();
    } else {
      final String requestFileTypeName;
      switch (requestFile.getRequestFileType()) {
      case APPLICATION:
        requestFileTypeName = APPLICATION_FILENAME;
        break;
      case DECREE:
        requestFileTypeName = DECREE_FILENAME;
        break;
      case DETAIL_DECREE:
        requestFileTypeName = DETAIL_DECREE_FILENAME;
        break;
      default:
        // hope this never gets triggered, but getting a weird filename beats the download failing fully
        requestFileTypeName = UNKNOWN_TYPE_FILENAME;
      }
      fileName = requestFileTypeName.replace(REFERENCE_STRING, referenceString) + "." + requestFile.getFileFormat().getExtension();
    }
    return fileName;
  }

  //TODO: proper file name based on file type.

  private RequestFile getRequestFile(final Connection connection, final String reference, final RequestFileType fileType) throws SQLException {
    final ArrayList<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(connection, reference);
    RequestFile foundRequestFile = null;
    for (final RequestFile requestFile : requestFiles) {
      if (requestFile.getRequestFileType() == fileType) {
        foundRequestFile = requestFile;
      }
    }
    return foundRequestFile;
  }

  @Override
  protected Logger getLogger() {
    return LOG;
  }

}
