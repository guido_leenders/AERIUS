/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.PermitModifyRepository;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.register.RegisterImportUtil;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.server.service.RequestUploadService;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.exception.ImportDuplicateEntryException;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.RegisterRetrieveImportService;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Handles Permit file uploads.
 */
@MultipartConfig
@SuppressWarnings("serial")
@WebServlet("/aerius/" + SharedConstants.IMPORT_PERMIT_SERVLET)
public class RegisterPermitUploadServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(RegisterPermitUploadServlet.class);
  private static final String RECEIVED_DATE_FORMAT = "dd-MM-yyyy HH:mm";

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    final UploadOptions uploadOptions = new UploadOptions();
    uploadOptions.setSupportedFormats(SupportedUploadFormat.PERMIT);
    final String receivedDateString = request.getParameter(SharedConstants.IMPORT_DATE_FIELD_RECEIVEDDATE);

    final RequestUploadService<PermitKey> requestUploadService = new RequestUploadService<PermitKey>(
        getPMF(),
        Internationalizer.getLocaleFromCookie(request),
        RegisterRetrieveImportService.UUID_PREFIX_PERMIT,
        RequestFileType.APPLICATION,
        uploadOptions,
        RegisterPermission.ADD_NEW_PERMIT) {

      @Override
      public PermitKey handleProcessedUpload(final ImportOutput output, final String dossierId, final UserProfile uploadedBy,
          final InsertRequestFile insertRequestFile) throws AeriusException {
        final Date receivedDate;
        try {
          receivedDate = new SimpleDateFormat(RECEIVED_DATE_FORMAT, LocaleUtils.getDefaultLocale()).parse(receivedDateString);
        } catch (final ParseException e) {
          throw new AeriusException(Reason.IMPORT_NO_VALID_RECEIVED_DATE);
        }
        return insertPermit(uploadedBy, output, dossierId, insertRequestFile, receivedDate);
      }

      @Override
      protected TaskManagerClient getTaskManagerClient() throws IOException {
        return RegisterPermitUploadServlet.this.getTaskManagerClient();
      }
    };

    requestUploadService.processRequest(request, response);
  }

  private PermitKey insertPermit(final UserProfile userProfile, final ImportOutput output, final String dossierId,
      final InsertRequestFile insertRequestFile, final Date receivedDate) throws AeriusException {
    // TODO Check if AERIUS version is the same. What if not?
    //boolean sameVersion = AeriusVersion.getVersionNumber().equals(service.getResult().getVersion());

    checkValidImport(output);
    final String reference = output.getCalculatedScenario().getMetaData().getReference();

    try (final Connection connection = getPMF().getConnection()) {
      // Check if reference already exists in the database and if so return this as an error.
      // If not, store result in the database.
      final PermitKey inDb = PermitRepository.getPermitKey(connection, reference);
      final PermitKey newPermitKey = new PermitKey(dossierId, userProfile.getAuthority().getCode());
      final Permit permitInDB = PermitRepository.getPermitByKey(connection, newPermitKey);
      if (inDb == null && permitInDB == null) {
        // Convert the permit from the imported result to an object we can use
        final Permit permit = RegisterImportUtil.convertToPermit(output);
        permit.getDossierMetaData().setHandler(userProfile);
        // Set the dossier ID to the value given.
        permit.getDossierMetaData().setDossierId(dossierId);
        permit.getDossierMetaData().setReceivedDate(receivedDate);

        // Test if it is a Permit request and not a Notice
        if (permit.getDossierMetaData() == null || StringUtils.isBlank(permit.getScenarioMetaData().getReference())) {
          throw new AeriusException(Reason.IMPORT_FILE_COULD_NOT_BE_READ);
        }

        // insert the new permit.
        PermitModifyRepository.insertNewPermit(connection, permit, userProfile, insertRequestFile);
        return PermitRepository.getPermitKey(connection, reference);
      } else {
        throw new ImportDuplicateEntryException(inDb == null ? newPermitKey : inDb);
      }
    } catch (final SQLException e) {
      LOG.error("SQLException while handling ", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private void checkValidImport(final ImportOutput output) throws AeriusException {
    if (!output.getExceptions().isEmpty()) {
      throw output.getExceptions().get(0);
    }
  }

  protected PMF getPMF() {
    return ServerPMF.getInstance();
  }

  protected TaskManagerClient getTaskManagerClient() throws IOException {
    return TaskClientFactory.getInstance();
  }
}
