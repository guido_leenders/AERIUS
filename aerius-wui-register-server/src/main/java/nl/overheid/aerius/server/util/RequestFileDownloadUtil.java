/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.register.DownloadRequestFile;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.SaltDigestUtil;

/**
 * Util for downloading request files.
 */
public final class RequestFileDownloadUtil {

  public static final String DOWNLOAD_LOCATION = "aerius/request-file-download";
  /**
   * The name of the hash request parameter.
   */
  public static final String PARAMETER_HASH = "hash";

  /**
   * The name of the request reference request parameter.
   */
  public static final String PARAMETER_REFERENCE = "reference";

  /**
   * The name of the request reference request parameter.
   */
  public static final String PARAMETER_DOCUMENT_TYPE = "documentType";

  private static final Logger LOG = LoggerFactory.getLogger(RequestFileDownloadUtil.class);

  private RequestFileDownloadUtil() {
    //util class
  }

  public static void setDownloadUrls(final Connection connection, final Request request) throws AeriusException, SQLException {
    final ArrayList<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(connection, request.getReference());
    for (final RequestFile requestFile : requestFiles) {
      final DownloadRequestFile downloadRequestFile = new DownloadRequestFile(requestFile,
          getDownloadUrl(connection, request.getReference(), requestFile.getRequestFileType()));
      request.getFiles().put(requestFile.getRequestFileType(), downloadRequestFile);
    }
  }

  /**
   * Get the download URL for given request Id.
   * @param reference The request reference to use.
   * @param application
   * @return The full download URL.
   * @throws AeriusException If the algorithm used for encrypting the hash cannot be found.
   * @throws SQLException
   */
  public static String getDownloadUrl(final Connection con, final String reference, final RequestFileType fileType)
      throws AeriusException, SQLException {
    try {
      return DOWNLOAD_LOCATION + "?" + PARAMETER_REFERENCE + "=" + reference + "&"
          + PARAMETER_HASH + "=" + getHash(con, reference, fileType) + "&" + PARAMETER_DOCUMENT_TYPE + "=" + fileType;
    } catch (final NoSuchAlgorithmException e) {
      LOG.error("Error while retrieving download URL for reference {}", reference, e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  /**
   * Returns a hash for the given request reference.
   * @param reference The reference to hash.
   * @param fileType
   * @return Encrypted, encoded and formatted form of the given request reference.
   * @throws NoSuchAlgorithmException
   * @throws SQLException
   * @throws AeriusException If salt is empty. This is not allowed for security reasons.
   */
  public static String getHash(final Connection connection, final String reference, final RequestFileType fileType)
      throws NoSuchAlgorithmException, SQLException, AeriusException {
    final String salt = ConstantRepository.getString(connection, ConstantsEnum.SALT_REQUEST_FILE_DOWNLOAD);
    if (StringUtils.isEmpty(salt)) {
      LOG.error("The salt constant is empty, which is not allowed for security reasons. Please set one and make sure it's decent.");
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return SaltDigestUtil.combineSecret(reference + fileType, salt);
  }
}
