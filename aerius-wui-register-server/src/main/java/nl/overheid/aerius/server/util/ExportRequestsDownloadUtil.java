/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import javax.servlet.http.HttpSession;

import nl.overheid.aerius.shared.domain.register.RequestFilter;

/**
 * Util for downloading export requests.
 */
public final class ExportRequestsDownloadUtil {

  public static final String DOWNLOAD_LOCATION = "aerius/export-requests-download";

  /**
   * The name of the id request parameter.
   */
  public static final String PARAMETER_ID = "id";

  private static final String SESSIONKEY = "REGISTER_REQUEST_EXPORT_";

  private ExportRequestsDownloadUtil() {
    // util class
  }

  /**
   * Get the download URL for given export ID.
   * @param id The ID to use (uuid).
   * @return The full download URL.
   */
  public static String getDownloadUrl(final String id) {
    return DOWNLOAD_LOCATION + "?" + PARAMETER_ID + "=" + id;
  }

  public static void setFilterForUuid(final HttpSession session, final String uuid, final RequestFilter<?> filter) {
    session.setAttribute(SESSIONKEY + uuid, filter);
  }

  public static RequestFilter<?> getFilterForUuid(final HttpSession session, final String uuid) {
    return (RequestFilter<?>) session.getAttribute(SESSIONKEY + uuid);
  }

  public static void removeFilterForUuid(final HttpSession session, final String uuid) {
    session.removeAttribute(SESSIONKEY + uuid);
  }

}
