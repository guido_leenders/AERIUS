/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.Part;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Test class for {@link RegisterPermitUploadServlet}.
 */
public class RegisterPermitUploadServletTest extends UploadTestBase {

  private static final String REGISTER_PERMIT_UPLOAD_SERVLET_PDF = "RegisterPermitUploadServlet.pdf";

  private RegisterPermitUploadServlet servlet;

  @SuppressWarnings("serial")
  @Before
  public void before() throws SQLException, IOException, AeriusException {
    final HashSet<UserRole> roles = new HashSet<>();
    roles.add(new UserRole(3, "", "")); // TODO create test role instead of hard-code specific existing role id.
    super.before(roles);
    servlet = new RegisterPermitUploadServlet() {
      @Override
      protected PMF getPMF() {
        return getRegPMF();
      }
      @Override
      protected TaskManagerClient getTaskManagerClient() throws IOException {
        return taskManagerClient;
      };
    };
  }

  @Test
  public void testPostSubProject() throws IllegalStateException, IOException, ServletException, SQLException, AeriusException {
    when(request.getParameter("id")).thenReturn("RandomZaakNummer");
    final Part part = mock(Part.class);
    when(part.getHeader("content-disposition")).thenReturn("filename=" + REGISTER_PERMIT_UPLOAD_SERVLET_PDF);
    when(part.getSize()).thenReturn(1L);
    doAnswer(new Answer<InputStream>() {
      @Override
      public InputStream answer(final InvocationOnMock invocation) throws Throwable {
        return getFileInputStream(REGISTER_PERMIT_UPLOAD_SERVLET_PDF);
      }
    }).when(part).getInputStream();
    when(request.getPart(SharedConstants.IMPORT_FILE_FIELD_NAME)).thenReturn(part);
    servlet.doPost(request, response);
    assertResults();
  }

}
