/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Util class to create example content to test servlets.
 */
public final class ExampleUtil {

  private static final String MOCK_EMAIL = UUID.randomUUID().toString();

  private ExampleUtil() {
    // util class.
  }

  public static HttpServletRequest mockHttpServletRequest(final HttpSession session) {
    final HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getSession(true)).thenReturn(session);
    return request;
  }

  public static HttpServletResponse mockHttpServletResponse(final StringWriter writer) throws IOException {
    final HttpServletResponse response = mock(HttpServletResponse.class);
    writer.flush();
    final PrintWriter printWriter = new PrintWriter(writer);
    when(response.getWriter()).thenReturn(printWriter);
    return response;
  }

  public static Map<String, Object> mockAttributes(final HttpSession session) {
    final Map<String, Object> attributes = new HashMap<>();
    attributes.clear();
    doAnswer(new Answer<Object>() {
      @Override
      public Object answer(final InvocationOnMock invocation) throws Throwable {
        return attributes.get(invocation.getArgumentAt(0, String.class));
      }
    }).when(session).getAttribute(anyString());
    doAnswer(new Answer<Object>() {
      @Override
      public Object answer(final InvocationOnMock invocation) throws Throwable {
        attributes.put(invocation.getArgumentAt(0, String.class), invocation.getArgumentAt(1, Object.class));
        return null;
      }
    }).when(session).setAttribute(anyString(), any());
    return attributes;
  }

  public static void mockSubject(final Connection con, final HashSet<UserRole> roles) throws SQLException, AeriusException {
    final SecurityManager securityManager = mock(SecurityManager.class);
    SecurityUtils.setSecurityManager(securityManager);
    final Subject subject = mock(Subject.class);
    when(securityManager.createSubject((SubjectContext) any())).thenReturn(subject);
    when(subject.isAuthenticated()).thenReturn(Boolean.TRUE);
    when(subject.getPrincipal()).thenReturn(MOCK_EMAIL);
    final AdminUserProfile user = new AdminUserProfile();
    user.setEmailAddress(MOCK_EMAIL);
    user.setName(MOCK_EMAIL);
    final Authority authority = new Authority();
    authority.setId(1);
    user.setAuthority(authority);
    user.setRoles(roles);
    UserRepository.createUser(con, user, "password");
  }

  public static TaskManagerClient mockTaskManagerClient() throws IOException {
    return mock(TaskManagerClient.class);
  }

  public static PriorityProject getExamplePriorityProject() throws SQLException {
    final PriorityProject priorityProject = new PriorityProject();
    TestDomain.fillExampleRequest(priorityProject);
    final DossierMetaData metaData = new DossierMetaData();
    metaData.setDossierId(UUID.randomUUID().toString());
    priorityProject.setDossierMetaData(metaData);

    return priorityProject;
  }
}
