/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client.configuration;

/**
 * Data object containing RabbitMQ connection information.
 */
public class ConnectionConfigurationBean implements ConnectionConfiguration {

  private String brokerHost;
  private int brokerPort = DEFAULT_BROKER_PORT;
  private int brokerManagementPort = DEFAULT_BROKER_MANAGEMENT_PORT;
  private String brokerUsername;
  private String brokerPassword;
  private String brokerVirtualHost = DEFAULT_BROKER_VIRTUAL_HOST;
  private int managementRefreshRate = DEFAULT_MANAGEMENT_REFRESH_RATE;

  /**
   * Empty constructor.
   */
  public ConnectionConfigurationBean() {
  }

  /**
   * @param host The name of the host.
   * @param brokerPort the post of the broker, if null the default port is used.
   * @param userName The username to use when connection to broker.
   * @param password The password to use when connection to broker.
   */
  public ConnectionConfigurationBean(final String host, final Integer brokerPort, final String userName, final String password) {
    this.brokerHost = host;
    if (brokerPort != null) {
      this.brokerPort = brokerPort;
    }
    this.brokerUsername = userName;
    this.brokerPassword = password;
  }

  /**
   * @return the brokerHost
   */
  @Override
  public String getBrokerHost() {
    return brokerHost;
  }

  /**
   * @param brokerHost the brokerHost to set
   */
  public void setBrokerHost(final String brokerHost) {
    this.brokerHost = brokerHost;
  }

  /**
   * @return the brokerPort
   */
  @Override
  public int getBrokerPort() {
    return brokerPort;
  }

  /**
   * @param brokerPort the brokerPort to set
   */
  public void setBrokerPort(final int brokerPort) {
    this.brokerPort = brokerPort;
  }

  /**
   * @return the brokerUsername
   */
  @Override
  public String getBrokerUsername() {
    return brokerUsername;
  }

  /**
   * @param brokerUsername the brokerUsername to set
   */
  public void setBrokerUsername(final String brokerUsername) {
    this.brokerUsername = brokerUsername;
  }

  /**
   * @return the brokerPassword
   */
  @Override
  public String getBrokerPassword() {
    return brokerPassword;
  }

  /**
   * @param brokerPassword the brokerPassword to set
   */
  public void setBrokerPassword(final String brokerPassword) {
    this.brokerPassword = brokerPassword;
  }

  /**
   * @return the brokerVirtualHost
   */
  @Override
  public String getBrokerVirtualHost() {
    return brokerVirtualHost;
  }

  /**
   * @param brokerVirtualHost the brokerVirtualHost to set
   */
  public void setBrokerVirtualHost(final String brokerVirtualHost) {
    this.brokerVirtualHost = brokerVirtualHost;
  }

  @Override
  public int getBrokerManagementPort() {
    return brokerManagementPort;
  }

  public void setBrokerManagementPort(final int brokerManagementPort) {
    this.brokerManagementPort = brokerManagementPort;
  }

  @Override
  public int getManagementRefreshRate() {
    return managementRefreshRate;
  }

  /**
   * @param managementRefreshRate management data refresh rate in seconds
   */
  public void setManagementRefreshRate(final int managementRefreshRate) {
    this.managementRefreshRate = managementRefreshRate;
  }
}
