/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

/**
 * Contains the names of the static queues used.
 */
public enum QueueEnum {
  CALCULATION_UI("calculation_ui"),
  PAA_EXPORT("paa_export"),
  CONNECT_PAA_EXPORT("connect_paa_export"),
  GML_EXPORT("gml_export"),
  CONNECT_GML_EXPORT("connect_gml_export"),
  OPS_EXPORT("ops_export"),
  REGISTER_EXPORT("register_export"),
  PERMIT("permit"),
  MELDING("melding"),
  IMPORT("import"),
  MESSAGE("message");

  private String queueName;

  private QueueEnum(final String queueName) {
    this.queueName = queueName;
  }

  public String getQueueName() {
    return queueName;
  }
}
