/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.GetResponse;

/**
 * Receiving end of the Worker. Programs reading from the worker queue should use this class to listen to the worker
 * queue.
 */
public class WorkerClient implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerClient.class);

  private final String queueName;
  private final WorkerHandler handler;

  private WorkerConsumer workerConsumer;
  private final BrokerConnectionFactory connectionFactory;

  public WorkerClient(final BrokerConnectionFactory connectionFactory, final WorkerHandler handler, final WorkerType workerType) {
    this(connectionFactory, handler, workerType.getWorkerQueueName());
  }

  public WorkerClient(final BrokerConnectionFactory connectionFactory, final WorkerHandler handler, final String queueName) {
    this.connectionFactory = connectionFactory;
    this.handler = handler;
    this.queueName = queueName;
  }

  @Override
  public void run() {
    try {
      run(queueName, true, true);
      LOG.info("Worker queue started = {}", queueName);
    } catch (final IOException e) {
      LOG.error("Error starting worker {}", queueName, e);
    }
  }

  /**
   * Starts a consuming process that waits for tasks to arrive on the queue.
   *
   * @param queueName name of the queue
   * @param durable if the queue created must be durable
   * @param sendResults sends results back via the reply queue if true
   * @return returns the channel used to listen on
   * @throws IOException
   */
  public Channel run(final String queueName, final boolean durable, final boolean sendResults) throws IOException {
    //create the channel to get messages on and start consuming.
    final Channel channel = connectionFactory.getConnection().createChannel();
    channel.basicQos(1);
    workerConsumer = new WorkerConsumer(channel, handler, sendResults);
    channel.basicConsume(queueName, false, queueName, workerConsumer);
    return channel;
  }

  /**
   * Gets a task from the queue. If no task is available null is returned.
   *
   * @param queueName name of the queue
   * @param ack if true the message from the queue is acknowledged and the message will be removed from the queue.
   * @param sendResults sends results back via the reply queue if true
   * @throws Exception
   */
  public void get(final String queueName, final boolean ack, final boolean sendResults) throws Exception {
    final Channel channel = connectionFactory.getConnection().createChannel();
    final GetResponse res = channel.basicGet(queueName, false);
    workerConsumer = new WorkerConsumer(channel, handler, sendResults);
    if (res == null) {
      throw new IOException("Trying to read message of queue '" + queueName + "', but this queue is empty or has Unacked messages");
    }
    workerConsumer.handleDelivery(res.getProps(), res.getBody(), channel);
    if (ack) {
      channel.basicAck(res.getEnvelope().getDeliveryTag(), false);
    }
  }

  /**
   * Closes down the queue connections.
   * @throws IOException
   */
  public void shutdown() throws IOException {
    if (workerConsumer != null && workerConsumer.getChannel().isOpen()) {
      workerConsumer.getChannel().basicCancel(queueName);
      workerConsumer.getChannel().close();
    }
  }
}
