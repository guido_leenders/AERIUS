/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;


/**
 * TaskResultCallback that does nothing.
 */
public final class NoopCallback implements TaskResultCallback {
  @Override
  public void onSuccess(final Object value, final String correlationId, final String messageId) {
    // no-op
  }

  @Override
  public void onFailure(final Exception e, final String correlationId, final String messageId) {
    // no-op
  }
}
