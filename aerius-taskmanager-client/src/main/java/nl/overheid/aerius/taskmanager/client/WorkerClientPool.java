/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * Client to be used to communicate with the taskmanager (more importantly, receive tasks from the taskmanager).
 * Usage example (to start 1 worker):
 * <pre>
 * WorkerHandler workerHandler = new SomeWorkerHandlerImpl();
 * WorkerClient client = new WorkerClient(connectionConfiguration, handler, workerQueueName);
 * client.startConsuming(1);
 * </pre>
 */
public class WorkerClientPool implements ShutdownListener, Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(WorkerClientPool.class);

  private final WorkerHandler handler;
  private final Queue<WorkerClient> workerConsumers = new LinkedList<>();
  private final WorkerType workerType;
  private final AtomicInteger numberOfWorkers = new AtomicInteger();
  private final Semaphore semaphore = new Semaphore(0);

  private final BrokerConnectionFactory connectionFactory;

  private boolean running;


  /**
   * Creates a worker client pool with a number of workers. Each worker can have at most 1 task to handle.
   * For instance, if there's capacity for 8 workers (because of 8 cores or something), the numberOfWorkers should be 8.
   * Each worker will run on it's own thread that is started by RabbitMQ and will have it's own channel.
   *
   * @param connectionFactory Configuration used for communicating with the MQ broker.
   * @param handler The object that will handle the actual work and should compile a result object to be returned to sender.
   * @param workerType The naming strategy used for queues.
   * @param numberOfWorkers Number of workers to start.
   */
  public WorkerClientPool(final BrokerConnectionFactory connectionFactory, final WorkerHandler handler, final WorkerType workerType,
      final int numberOfWorkers) {
    if (connectionFactory == null || handler == null || workerType == null) {
      throw new IllegalArgumentException("No arguments are allowed to be null.");
    }
    this.connectionFactory = connectionFactory;
    this.handler = handler;
    this.workerType = workerType;
    // if input <= 0, no workers will be started.
    running = this.numberOfWorkers.addAndGet(Math.max(0, numberOfWorkers)) > 0;
  }

  @Override
  public void run() {
    LOG.debug("Workers started for: {}", workerType);
    while (running) {
      connectionFactory.getConnection().addShutdownListener(this);
      int cnt = 0;
      while (numberOfWorkers.get() > workerConsumers.size()) {
        LOG.debug("Start worker #{} of '{}'", ++cnt, workerType);
        final WorkerClient client = new WorkerClient(connectionFactory, handler, workerType);
        client.run();
        workerConsumers.add(client);
      }
      try {
        LOG.info("Started #{} workers for worker type: {}", cnt, workerType);
        semaphore.acquire();
      } catch (final InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }
    LOG.debug("Workers stopped for: {}", workerType);
  }

  /**
   * Stop a number of workers. Stopping a worker causes it to no longer retrieve any messages from the MQ,
   * and the corresponding channel will be closed causing the current message the worker was handling to be requeued.
   * @param numberOfWorkers Number of workers to stop (if more then current, current will be taken).
   * @throws IOException When an error occurred while communicating with the MQ broker.
   * The last worker will still be removed from the list (assuming it was stopped some other way).
   */
  public void stopConsuming(final int numberOfWorkers) throws IOException {
    // if input <= 0, no workers will be stopped.
    // If input > current number of workers all current workers will be stopped..
    final int safeNumberOfWorkersToStop = Math.max(0, Math.min(numberOfWorkers, this.numberOfWorkers.get()));
    this.numberOfWorkers.addAndGet(-safeNumberOfWorkersToStop);
    while (this.numberOfWorkers.get() < workerConsumers.size()) {
      workerConsumers.remove().shutdown();
    }
  }

  /**
   * @return The number of workers this client has at the moment.
   * Starting workers happens on different thread, so this might be different from getNumberOfWorkers.
   */
  public int getCurrentNumberOfWorkers() {
    return workerConsumers.size();
  }

  /**
   * @return The number of workers this client *should* have.
   */
  public int getNumberOfWorkers() {
    return numberOfWorkers.get();
  }

  /**
   * Try to stop all consuming work at the moment.
   * Exception might occur, but this will be saved until all workers are attempted to be closed.
   * @throws IOException When an error occurred while communicating with the MQ broker.
   */
  public void stopAllConsuming() throws IOException {
    shutDown(false, true);
  }

  @Override
  public void shutdownCompleted(final ShutdownSignalException cause) {
    if (!cause.isInitiatedByApplication()) {
      //all workers are now useless, stop em all WITHOUT setting number of workers to 0.
      try {
        shutDown(true, false);
      } catch (final IOException e) {
        LOG.trace("Exception during #shutdownCompleted, but ignored", e);
      }
    }
  }

  private void shutDown(final boolean restart, final boolean throwException) throws IOException {
    IOException occurredException = null;
    try {
      if (!restart) {
        numberOfWorkers.set(0);
      }
      WorkerClient workerConsumer;
      while ((workerConsumer = workerConsumers.poll()) != null) {
        try {
          workerConsumer.shutdown();
        } catch (final IOException e) {
          occurredException = e;
        }
      }
      if (!restart) {
        running = false;
      }
    } finally {
      LOG.debug("Release lock for: {}", workerType);
      semaphore.release();
    }
    // if closing connection caused an exception, that will be the exception thrown,
    // otherwise it'll be the last exception caused by closing down a consumer.
    if (throwException && occurredException != null) {
      throw occurredException;
    }
  }
}
