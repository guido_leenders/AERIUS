/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rabbitmq.client.ShutdownSignalException;

/**
 * Test class for {@link WorkerClientPool}.
 */
public class WorkerClientTest {

  private static final WorkerType workerType = WorkerType.TEST;
  private static ExecutorService executor;
  private WorkerClientPool workerClient;
  private BrokerConnectionFactory factory;

  @BeforeClass
  public static void setupClass() {
    executor = Executors.newFixedThreadPool(4);
  }

  @AfterClass
  public static void afterClass() {
    executor.shutdown();
  }

  @Before
  public void setUp() throws Exception {
    factory = new BrokerConnectionFactory(executor) {
      @Override
      protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
        return new MockConnection();
      };
    };
  }

  @After
  public void tearDown() throws Exception {
    if (workerClient != null) {
      workerClient.stopAllConsuming();
    }
  }

  /**
   * Test method for {@link nl.overheid.aerius.taskmanager.client.WorkerClientPool#startConsuming(int)}.
   */
  @Test(timeout = 3000)
  public void testStartConsuming() throws IOException {
    workerClient = createPool(1);
    assertEquals("Number of workers that should be there", 1, workerClient.getNumberOfWorkers());
    sleepAfterStartConsuming();
    if (workerClient.getCurrentNumberOfWorkers() != 1) {
      try {
        Thread.sleep(100);
      } catch (final InterruptedException e) {
      }
    }
    assertEquals("Number of current workers", 1, workerClient.getCurrentNumberOfWorkers());
    assertEquals("Number of workers that should be there", 1, workerClient.getNumberOfWorkers());
  }

  /**
   * Test method for {@link nl.overheid.aerius.taskmanager.client.WorkerClientPool#stopConsuming(int)}.
   */
  @Test(timeout = 3000)
  public void testStopConsuming() throws IOException {
    workerClient = createPool(1);
    workerClient.stopConsuming(1);
    assertEquals("Number of current workers", 0, workerClient.getCurrentNumberOfWorkers());
    assertEquals("Number of workers that should be there", 0, workerClient.getNumberOfWorkers());
    testStartConsuming();
    workerClient.stopConsuming(100);
    assertEquals("Number of current workers", 0, workerClient.getCurrentNumberOfWorkers());
    assertEquals("Number of workers that should be there", 0, workerClient.getNumberOfWorkers());
  }

  /**
   * Test method for {@link nl.overheid.aerius.taskmanager.client.WorkerClientPool#startConsuming(int)}.
   */
  @Test(timeout = 3000)
  public void testShutdownSignal() throws IOException {
    testStartConsuming();
    workerClient.shutdownCompleted(new ShutdownSignalException(true, false, "Tryout", null));
    assertEquals("Number of current workers right away after shutdown", 0, workerClient.getCurrentNumberOfWorkers());
    assertEquals("Number of workers that should be there", 1, workerClient.getNumberOfWorkers());
    sleepAfterStartConsuming();
    assertEquals("Number of current workers some time after shutdown", 1, workerClient.getCurrentNumberOfWorkers());
  }

  @Test(expected = IllegalArgumentException.class, timeout = 3000)
  public void testWorkerClientWithoutWorkerHandler() throws IOException {
    workerClient = new WorkerClientPool(factory, null, workerType, 1);
  }

  @Test(expected = IllegalArgumentException.class, timeout = 3000)
  public void testWorkerClientWithoutQueueNaming() throws IOException {
    final WorkerHandler workerHandler = new MockWorkerHandler();
    workerClient = new WorkerClientPool(factory, workerHandler, null, 1);
  }

  private void sleepAfterStartConsuming() {
    try {
      //start consuming happens on different thread.
      Thread.sleep(500);
    } catch (final InterruptedException e) {
    }
  }

  private WorkerClientPool createPool(final int workers) {
    final WorkerClientPool client = new WorkerClientPool(factory, new MockWorkerHandler(), workerType, workers);
    
    executor.execute(client);
    return client;
  }
  
}
