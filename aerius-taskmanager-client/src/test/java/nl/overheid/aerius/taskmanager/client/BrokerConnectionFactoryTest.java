/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.client;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfigurationBean;

/**
 * Test for class {@link BrokerConnectionFactory}.
 */
public class BrokerConnectionFactoryTest {

  private static ExecutorService executor;

  @BeforeClass
  public static void setupClass() {
    executor = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void afterClass() {
    executor.shutdown();
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithoutBrokerHost() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerHost(null);
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithEmptyBrokerHost() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerHost("");
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithoutBrokerUsername() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerUsername(null);
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithEmptyBrokerUsername() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerUsername("");
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithoutBrokerPassword() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerPassword(null);
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithEmptyBrokerPassword() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerPassword("");
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithoutBrokerVirtualHost() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerVirtualHost(null);
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testTaskManagerClientWithEmptyBrokerVirtualHost() throws IOException {
    final ConnectionConfigurationBean connectionConfigurationBean = getFullConnectionConfigurationBean();
    connectionConfigurationBean.setBrokerVirtualHost("");
    new BrokerConnectionFactory(executor, connectionConfigurationBean);
  }

  private ConnectionConfigurationBean getFullConnectionConfigurationBean() {
    final ConnectionConfigurationBean bean = new ConnectionConfigurationBean();
    bean.setBrokerHost("localhost");
    bean.setBrokerUsername("username");
    bean.setBrokerPassword("password");
    bean.setBrokerPort(1234);
    bean.setBrokerVirtualHost("/");
    return bean;
  }
}
