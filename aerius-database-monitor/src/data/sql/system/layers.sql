{import_common 'system/layers.sql'}

--update the layer capabilities URL to be monitor specific.
UPDATE system.layer_capabilities SET url = 'https://test.aerius.nl/monitor/aerius-geo-wms?' WHERE layer_capabilities_id = 1;

--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (11, 11, 'wms', 'monitor:wms_nature_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (12, 13, 'wms', 'monitor:wms_habitat_areas_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (13, 14, 'wms', 'monitor:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (14, 17, 'wms', 'monitor:wms_other_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (15, 18, 'wms', 'monitor:wms_assessment_area_receptor_deposition_spaces_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (16, 19, 'wms', 'monitor:wms_assessment_area_receptor_delta_depositions_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (17, 20, 'wms', 'monitor:wms_assessment_area_receptor_delta_policy_depositions_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (18, 21, 'wms', 'monitor:wms_sector_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (19, 12, 'wms', 'monitor:wms_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (20, 22, 'wms', 'monitor:wms_deviations_from_critical_deposition_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (21, 23, 'wms', 'monitor:wms_delta_space_desire_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (22, 24, 'wms', 'monitor:wms_included_receptors_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (23, 10, 'wms', 'monitor:wms_rehabilitation_strategies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (24, 10, 'wms', 'monitor:wms_habitats_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (25, 10, 'wms', 'monitor:wms_receptors_to_habitats_with_relevant_geometry_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (26, 10, 'wms', 'monitor:wms_critical_deposition_area_receptor_depositions_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (27, 10, 'wms', 'monitor:wms_critical_deposition_area_receptor_delta_depositions_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (28, 10, 'wms', 'monitor:wms_critical_deposition_area_receptor_deviations_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (29, 28, 'wms', 'monitor:wms_province_areas_view');


-- default layers
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (1, 0, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (29, 1, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (3, 2, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (4, 3, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (11, 4, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (12, 5, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (13, 6, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (14, 7, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (15, 8, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (16, 9, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (17, 10, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (18, 11, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (19, 12, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (20, 13, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (21, 14, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (22, 15, false);
