{import_common 'system/general.sql'}

{import_common 'system/sectors.sql'}

BEGIN; SELECT setup.ae_load_table('system.assessment_area_report_images', '{data_folder}/system/system.assessment_area_report_images_20150722.txt'); COMMIT;

INSERT INTO system.constants (key, value) VALUES ('INTERNAL_WMS_PROXY_URL', 'https://test.aerius.nl/monitor/aerius-geo-wms?');
INSERT INTO system.constants (key, value) VALUES ('SHARED_WMS_PROXY_URL', 'https://test.aerius.nl/monitor/aerius-geo-wms?');

--URLs where geoserver can access the servlet for SLD
INSERT INTO system.constants (key, value) VALUES ('SHARED_WMS_URL', 'https://test.aerius.nl/geoserver-monitor/wms?');
INSERT INTO system.constants (key, value) VALUES ('GEOSERVER_CAPIBILITIES_ORIGINAL_URL', 'https://test.aerius.nl:80/geoserver-monitor/wms?');
INSERT INTO system.constants (key, value) VALUES ('SHARED_SLD_URL', 'http://localhost:8080/monitor/aerius/');

-- Min and max values for deposition space, this is used to scale deposition space buckets proportionally for each nature
-- area. If needed, this could be gotten from a min/max of actual data instead of from the constants.
INSERT INTO system.constants (key, value) VALUES ('DEPOSITION_SPACE_MIN', '4000');
INSERT INTO system.constants (key, value) VALUES ('DEPOSITION_SPACE_MAX', '50000');

INSERT INTO system.constants (key, value) VALUES ('MONITOR_MAX_EMISSION_SOURCES', '25000');
INSERT INTO system.constants (key, value) VALUES ('NUMBER_OF_TASKCLIENT_THREADS', '40');

-- Min and max row height for deposition space buckets
INSERT INTO system.constants (key, value) VALUES ('DEPOSITION_SPACE_BUCKET_HEIGHT_MIN', '3');
INSERT INTO system.constants (key, value) VALUES ('DEPOSITION_SPACE_BUCKET_HEIGHT_MAX', '7');

INSERT INTO system.constants (key, value) VALUES ('MANUAL_URL', 'http://www.aerius.nl/nl/manuals/monitor');

INSERT INTO system.constants (key, value) VALUES ('MONITOR_START_YEAR', '2014');

INSERT INTO system.constants (key, value) VALUES ('SHIPPING_INLAND_LOCK_HEAT_CONTENT_FACTOR', '0.15');

INSERT INTO system.constants (key, value) VALUES ('MONITOR_EXPORT_README_CONTENT', 'Via de volgende link kunt u de toelichting bij deze export lezen: http://www.aerius.nl/nl/factsheets/toelichting-bij-aerius-monitor-export/');


/* kleurstellingen voor filter */

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 0, 'FFFFD4');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 700 ,'FEE391');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 1000 ,'FEC44F');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 1300 ,'FE9929');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 1600 ,'EC7014');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 1900 ,'CC4C02');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('total_deposition', 2200 ,'8C2D04');

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', -800, '6C8B41');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', -400 ,'89A267');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', -200 ,'A7B98D');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', -100 ,'C4D1B3');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', -50 ,'E2E8D9');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', 0 ,'F2F2F2');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', 50 ,'DCD9E9');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', 100 ,'B9B3D4');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', 200 ,'978CBE');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', 400 ,'7466A9');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('delta_deposition', 800 ,'514093');

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', -2000, '6C8B41');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', 0 ,'F2F2F2');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', 200 ,'DCD9E9');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', 400 ,'B9B3D4');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', 600 ,'978CBE');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', 800 ,'7466A9');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('deviation_from_critical_deposition', 1000 ,'514093');

/* Kleurstellingen voor GS */
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_delta_deposition_jurisdiction_policies_decrease', '-Infinity','F0F9E8');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_delta_deposition_jurisdiction_policies_decrease', 50         ,'BAE4BC');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_delta_deposition_jurisdiction_policies_decrease', 100        ,'7BCCC4');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_delta_deposition_jurisdiction_policies_decrease', 175        ,'43A2CA');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_delta_deposition_jurisdiction_policies_decrease', 250        ,'0868AC');

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_delta_depositions', 0.05,'000080');

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 0   ,'FFFFD4');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 700 ,'FEE391');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 1000,'FEC44F');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 1300,'FE9929');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 1600,'EC7014');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 1900,'CC4C02');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_depositions', 2200,'8C2D04');

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 0  ,'F2F0F7');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 20 ,'DADAEB');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 40 ,'BCBDDC');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 60 ,'9E9AC8');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 80 ,'807DBA');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 100,'6A51A3');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_deposition_spaces', 150,'4A1486');

INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_receptor_max_nitrogen_loads', 1,'41AB5D');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_receptor_max_nitrogen_loads', 2,'F7FFF7');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_receptor_max_nitrogen_loads', 3,'C2A5CF');
INSERT INTO system.color_ranges (color_range_type, lower_value, color) VALUES ('report_receptor_receptor_max_nitrogen_loads', 4,'7B3294');


