/*
 * Dit is ui-data aangeleverd door Jacob Bijsterbosch op 05-06-2014.
 * https://docs.google.com/a/aerius-team.nl/spreadsheets/d/1d-55i4kU5iKYjWyK4LBeMuhxlFPsZuBwH2rKAFVQXUA/edit#gid=1835398995
 */

/*
 * Insert system.sectorgroup_properties.
 */
INSERT INTO system.sectorgroup_properties (sectorgroup, name, color, icon_type) VALUES ('agriculture', 'agriculture','668A49'::system.color, 'agriculture');
INSERT INTO system.sectorgroup_properties (sectorgroup, name, color, icon_type) VALUES ('industry', 'industry','C0027A'::system.color, 'industry');
INSERT INTO system.sectorgroup_properties (sectorgroup, name, color, icon_type) VALUES ('road_transportation', 'transportation','C6311D'::system.color, 'traffic_and_transport');
INSERT INTO system.sectorgroup_properties (sectorgroup, name, color, icon_type) VALUES ('road_freeway', 'road_freeway','C41214'::system.color, 'road_freeway');
INSERT INTO system.sectorgroup_properties (sectorgroup, name, color, icon_type) VALUES ('shipping', 'shipping','1A4789'::system.color, 'shipping');
INSERT INTO system.sectorgroup_properties (sectorgroup, name, color, icon_type) VALUES ('other', 'other','EECE9B'::system.color, 'other');
/*
 * Insert system.other_deposition_type_properties.
 */
INSERT INTO system.other_deposition_type_properties (other_deposition_type, name, color, icon_type) VALUES ('abroad_deposition', 'ABROAD','668A49'::system.color, 'ABROAD');
INSERT INTO system.other_deposition_type_properties (other_deposition_type, name, color, icon_type) VALUES ('measurement_correction', 'MEASUREMENT_CORRECTION','668A49'::system.color, 'MEASUREMENT_CORRECTION');
INSERT INTO system.other_deposition_type_properties (other_deposition_type, name, color, icon_type) VALUES ('remaining_deposition', 'BACKGROUND_DEPOSITIONS','668A49'::system.color, 'BACKGROUND_DEPOSITION');
INSERT INTO system.other_deposition_type_properties (other_deposition_type, name, color, icon_type) VALUES ('returned_deposition_space', 'RETURNED_DEPOSITION_SPACE','668A49'::system.color, 'RETURNED_DEPOSITION_SPACE');
INSERT INTO system.other_deposition_type_properties (other_deposition_type, name, color, icon_type) VALUES ('returned_deposition_space_limburg', 'RETURNED_DEPOSITION_SPACE_LIMBURG','668A49'::system.color, 'RETURNED_DEPOSITION_SPACE_LIMBURG');
INSERT INTO system.other_deposition_type_properties (other_deposition_type, name, color, icon_type) VALUES ('deposition_space_addition', 'DEPOSITION_SPACE_ADDITION','668A49'::system.color, 'DEPOSITION_SPACE_ADDITION');

/*
 * Insert system.sectors_sectorgroup.
 * Different list from the one in common due to the use of less sectorgroups.
 * The sector 4600 doesn't exist in the common list either, it's not available to use there.
 */
INSERT INTO system.sectors_sectorgroup (sector_id, sectorgroup) SELECT sector_id, sectorgroup::text::system.sectorgroup FROM setup.sectors_sectorgroup;
