BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_corrections_no_policies', '{data_folder}/setup/setup.gcn_sector_corrections_no_policies_agriculture_farm_lodgings_20160617.txt'); COMMIT; -- stagnatie correctie
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_corrections_no_policies', '{data_folder}/setup/setup.gcn_sector_corrections_no_policies_post_correction_20161005_2.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_corrections_no_policies', '{data_folder}/setup/setup.gcn_sector_corrections_no_policies_rwc_correction_20170504.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('setup.sector_deposition_space_corrections_jurisdiction_policies', '{data_folder}/setup/setup.sector_deposition_space_corrections_jurisdiction_policies_20160630.txt'); COMMIT;


BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_growth_corrections', '{data_folder}/setup/setup.gcn_sector_economic_growth_corrections_agriculture_farm_lodgings_20160630.txt'); COMMIT; -- stoppers correctie
BEGIN; UPDATE setup.gcn_sector_economic_growth_corrections SET correction = correction * 0.5; COMMIT;

BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_growth_corrections', '{data_folder}/setup/setup.gcn_sector_economic_growth_corrections_hwn_20160927.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_growth_corrections', '{data_folder}/setup/setup.gcn_sector_economic_growth_corrections_own_20170315.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_economic_growth_corrections', '{data_folder}/setup/setup.gcn_sector_economic_growth_corrections_rail_20160523.txt'); COMMIT;


-- Update setup.sector_deposition_space_corrections_jurisdiction_policies
--   De (depositieruimte) correctie mag nooit meer zijn dan de helft van de extra reductie door provinciaal beleid (reductie jp - reductie gp).
BEGIN;
	UPDATE setup.sector_deposition_space_corrections_jurisdiction_policies AS org
		SET correction = new.correction

		FROM
			(SELECT
				receptor_id,
				year,
				sector_id,
				ROUND(((jp.reduction - gp.reduction) / 2)::numeric, 3) AS correction

				FROM setup.sector_deposition_space_corrections_jurisdiction_policies
					INNER JOIN setup.sector_reductions_global_policies_view AS gp USING (receptor_id, year, sector_id)
					INNER JOIN setup.sector_reductions_jurisdiction_policies_view AS jp USING (receptor_id, year, sector_id)

				WHERE sector_id = 4110

				ORDER BY receptor_id, year) AS new
		WHERE
			org.receptor_id = new.receptor_id
			AND org.year = new.year
			AND org.sector_id = new.sector_id
			AND org.correction > new.correction
	;
COMMIT;

-- Optionele correctie voor receptoren met een te korte rekenafstand tot de bronnen. 
-- BEGIN; SELECT setup.ae_load_table('setup.sector_overkill_corrections', '{data_folder}/setup/setup.sector_overkill_corrections_20151008.txt'); COMMIT;

