--
-- other_depositions --
--
BEGIN; SELECT setup.ae_load_table('other_depositions', '{data_folder}/public/other_depositions_abroad_deposition_20161111.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('other_depositions', '{data_folder}/public/other_depositions_dune_area_correction_20160719.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('other_depositions', '{data_folder}/public/other_depositions_measurement_correction_20161130.txt'); COMMIT;

--
-- gcn_sector_depositions --
--

-- agriculture farm lodgings
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_agriculture', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_agriculture_farm_lodgings_20160630.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_global_policies', '{data_folder}/setup/setup.gcn_sector_depositions_global_policies_agriculture_farm_lodgings_20160630.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_jurisdiction_policies', '{data_folder}/setup/setup.gcn_sector_depositions_jurisdiction_policies_agriculture_farm_lodgings_20160630.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_jurisdiction_policies_no_growth', '{data_folder}/setup/setup.gcn_sector_depositions_jurisdiction_policies_no_growth_agriculture_farm_lodgings_20160714.txt'); COMMIT;

-- agriculture fertilizer use
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_agriculture', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_agriculture_fertilizer_use_20160701.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_global_policies', '{data_folder}/setup/setup.gcn_sector_depositions_global_policies_fertilizer_use_20160701.txt'); COMMIT;

-- agriculture other
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_agriculture', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_agriculture_other_20160623.txt'); COMMIT;

-- rijnmond
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_industry', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_industry_rijnmond_20160711.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_shipping', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_shipping_rijnmond_20160711.txt'); COMMIT;

-- no policies (other)
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_industry', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_industry_20160701.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_other', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_other_20161003.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_road_transportation', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_road_transportation_20160630.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_road_transportation', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_road_transportation_aviation_refined_20160622.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_road_transportation', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_road_transportation_aviation_not_refined_20160622.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_road_transportation', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_road_transportation_er_mobile_20160616.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_shipping', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_shipping_20160629.txt'); COMMIT;

-- no policies, no growth
BEGIN; SELECT setup.ae_load_table('setup.gcn_sector_depositions_no_policies_no_growth', '{data_folder}/setup/setup.gcn_sector_depositions_no_policies_no_growth_hwn_20160927.txt'); COMMIT;


--
-- sector_priority_project_demands --
--
BEGIN; SELECT setup.ae_load_table('setup.sector_priority_project_demands_growth', '{data_folder}/setup/setup.sector_priority_project_demands_growth_20170424.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.sector_priority_project_demands_desire', '{data_folder}/setup/setup.sector_priority_project_demands_desire_20170424.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.sector_priority_project_demands_desire_divided', '{data_folder}/setup/setup.sector_priority_project_demands_desire_divided_20170424.txt'); COMMIT;
