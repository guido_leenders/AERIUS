{import_common 'i18n/sectors.sql'}
{import_common 'i18n/calculations.sql'}
{import_common 'i18n/habitats.sql'}
{import_common 'i18n/messages/'}

BEGIN;
	INSERT INTO i18n.farm_lodging_types
	SELECT farm_lodging_type_id, unnest(ARRAY['nl', 'en']::i18n.language_code_type[]), description FROM farm_lodging_types;
END;