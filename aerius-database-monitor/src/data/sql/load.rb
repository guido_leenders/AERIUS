add_build_constants

cluster_tables

run_sql "general.sql"
run_sql "areas.sql"
run_sql "setup.sql"
run_sql "depositions.sql"
run_sql "corrections.sql"
run_sql "sources.sql"
run_sql "build.sql"
run_sql_folder "system"
run_sql_folder "i18n"

synchronize_serials

$do_validate_contents = true if has_build_flag :validate
$do_create_summary = true if has_build_flag :summary
