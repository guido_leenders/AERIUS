--
-- gcn sources data
--
/*
BEGIN; SELECT setup.ae_load_table('origins', '{data_folder}/public/origins_gcn_only_20140429.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sites', '{data_folder}/public/sites_gcn_only_20140429.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sources', '{data_folder}/public/sources_gcn_only_20140430.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sources_gcn_sector', '{data_folder}/public/sources_gcn_sector_gcn_only_20140430.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('source_emissions', '{data_folder}/public/source_emissions_gcn_only_20140429.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('source_source_characteristics', '{data_folder}/public/source_source_characteristics_gcn_only_20140429.txt'); COMMIT;
*/

--
-- farm lodging data
--

-- mics categories
BEGIN; SELECT setup.ae_load_table('farm_animal_categories', '{data_folder}/public/farm_animal_categories_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_emission_ceiling_categories', '{data_folder}/public/farm_emission_ceiling_categories_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_types', '{data_folder}/public/farm_lodging_types_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_lodging_type_emission_factors', '{data_folder}/public/farm_lodging_type_emission_factors_20160530.txt'); COMMIT;


-- sources and sites
BEGIN; SELECT setup.ae_load_table('origins', '{data_folder}/public/origins_farm_only_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sites', '{data_folder}/public/sites_farm_only_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sources', '{data_folder}/public/sources_farm_only_20160530.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('farm_sites', '{data_folder}/public/farm_sites_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_sources', '{data_folder}/public/farm_sources_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_source_lodging_types', '{data_folder}/public/farm_source_lodging_types_20160530.txt'); COMMIT;


-- growths
BEGIN; SELECT setup.ae_load_table('farm_animal_category_economic_growths', '{data_folder}/public/farm_animal_category_economic_growths_20160530.txt'); COMMIT;


-- formal ceilings
BEGIN; SELECT setup.ae_load_table('farm_emission_formal_ceilings_no_policies', '{data_folder}/public/farm_emission_formal_ceilings_no_policies_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_emission_formal_ceilings_global_policies', '{data_folder}/public/farm_emission_formal_ceilings_global_policies_20160530.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('farm_emission_formal_ceilings_jurisdiction_policies', '{data_folder}/public/farm_emission_formal_ceilings_jurisdiction_policies_20160530.txt'); COMMIT;


-- nema data
SELECT setup.ae_load_table('farm_nema_clusters', '{data_folder}/public/farm_nema_clusters_20160530.txt');
SELECT setup.ae_load_table('farm_nema_categories', '{data_folder}/public/farm_nema_categories_20160530.txt');
SELECT setup.ae_load_table('farm_nema_category_emissions', '{data_folder}/public/farm_nema_category_emissions_20160607.txt');


-- mics fractions
BEGIN; SELECT setup.ae_load_table('setup.farm_grazing_fractions', '{data_folder}/temp/setup.temp_farm_grazing_fractions_20140519.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('setup.farm_substitution_fractions', '{data_folder}/public/setup.farm_substitution_fractions_20160530.txt'); COMMIT;


-- build site property data
SELECT ae_raise_notice('Build: site_generated_properties @ ' || timeofday());
BEGIN;
	INSERT INTO site_generated_properties(site_id, sector_id, number_of_sources, geometry)
		SELECT site_id, sector_id, number_of_sources, geometry
		FROM setup.build_site_generated_properties_view;
COMMIT;