/** Areas */

{import_common 'areas_hexagons_and_receptors/load_monitor.sql'}


/** Search widget data */

{import_common 'search_widget/'}


/** Monitor area-related data */

BEGIN; SELECT setup.ae_load_table('receptors', '{data_folder}/temp/temp_receptors_monitor_20170116.txt'); COMMIT; -- De verwijderde BN receptoren (welke nog wel in de M16 data zitten).
BEGIN; SELECT setup.ae_load_table('jurisdictions', '{data_folder}/public/jurisdictions_20190129.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('assessment_area_detailed_extents', '{data_folder}/public/assessment_area_detailed_extents_20160720.txt'); COMMIT;


/** Herstelmaatregelen */

BEGIN; SELECT setup.ae_load_table('rehabilitation_strategies', '{data_folder}/public/rehabilitation_strategies_20171121.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('rehabilitation_strategies_to_receptors', '{data_folder}/public/rehabilitation_strategies_to_receptors_20161121.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('rehabilitation_strategy_management_periods', '{data_folder}/public/rehabilitation_strategy_management_periods_20171214.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('rehabilitation_strategy_habitat_types', '{data_folder}/public/rehabilitation_strategy_habitat_types_20171214.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('rehabilitation_strategy_files', '{data_folder}/public/rehabilitation_strategy_files_20171121.txt'); COMMIT;

BEGIN;
	UPDATE rehabilitation_strategies
		SET geometry = update_view.geometry
		FROM setup.update_rehabilitation_strategies_geometries_view AS update_view
		WHERE rehabilitation_strategies.rehabilitation_strategy_id = update_view.rehabilitation_strategy_id;
COMMIT;

BEGIN; UPDATE rehabilitation_strategies SET geometry = ST_MakeValid(geometry) WHERE NOT ST_IsValid(geometry); COMMIT; -- Fix invalid geometries


/** Override tables **/
BEGIN; SELECT setup.ae_load_table('override_relevant_development_space_receptors', '{data_folder}/public/override_relevant_development_space_receptors_20170731.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('override_development_space_values', '{data_folder}/temp/temp_override_development_space_values_20170828.txt'); COMMIT;
