/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Test;

/**
 * Test class for {@link WorkerConfiguration}.
 */
public class WorkerConfigurationTest {

  private static final String ENV_TEST = "test";

  @Test
  public void testGetProperty() {
    final String key = "var1";
    final TestWorkerConfiguration twc = getTestConfiguration(key, "100");
    assertEquals("Read value as in", 100, twc.getPropertyInt(key).intValue());
  }

  @Test
  public void testValidationDirectory() throws IOException {
    final String key = "directory";
    final List<String> reasons = new ArrayList<>();

    final Path tempDir = Files.createTempDirectory(null);
    try {
      final TestWorkerConfiguration twc = getTestConfiguration(key, tempDir.toFile().getAbsolutePath());
      twc.validateDirectoryProperty(key, reasons, true);
      assertValid(reasons);
    } finally {
      Files.delete(tempDir);
    }
  }

  @Test
  public void testValidationFilepath() throws IOException {
    final String key = "path";
    final List<String> reasons = new ArrayList<>();
    final String extension = ".ext";
    final String badExtension = ".est";

    final Path tempFile = Files.createTempFile(null, extension);
    try {
      final TestWorkerConfiguration twc = getTestConfiguration(key, tempFile.toFile().getAbsolutePath());
      twc.validateFilepathProperty(key, reasons, null);
      assertValid(reasons);

      twc.validateFilepathProperty(key, reasons, extension);
      assertValid(reasons);

      twc.validateFilepathProperty(key, reasons, badExtension);
      assertEquals("Expected one validation error", 1, reasons.size());
    } finally {
      Files.delete(tempFile);
    }
  }

  private TestWorkerConfiguration getTestConfiguration(final String key, final String value) {
    final Properties p = new Properties();
    p.setProperty(ENV_TEST + "." + key, value);
    return new TestWorkerConfiguration(p);
  }

  private void assertValid(final List<String> reasons) {
    assertEquals("No validation errors expected", 0, reasons.size());
  }

  private static class TestWorkerConfiguration extends WorkerConfiguration {

    public TestWorkerConfiguration(final Properties properties) {
      super(properties, ENV_TEST);
    }

    @Override
    protected List<String> getValidationErrors() {
      return null;
    }
  }
}
