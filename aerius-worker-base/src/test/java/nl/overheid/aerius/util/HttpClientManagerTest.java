/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.protocol.HttpContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.util.HttpClientManager.ServiceUnavailableRetryStrategyImpl;

/**
 * Test class for {@link HttpClientManagerTest}.
 */
@RunWith(Parameterized.class)
public class HttpClientManagerTest {

  final ServiceUnavailableRetryStrategyImpl retryStrategy = new ServiceUnavailableRetryStrategyImpl();

  private final int retry;
  private final int statusCode;
  private final boolean expectedToRetry;

  public HttpClientManagerTest(final int statusCode, final int retry, final boolean expectedToRetry) {
    this.statusCode = statusCode;
    this.retry = retry;
    this.expectedToRetry = expectedToRetry;
  }

  @Parameters(name = "statusCode: {0}, retryExecutionCount: {1}, expected to retry: {2}")
  public static List<Object[]> data() {
    final Object[] realData = new Object[] {
        200, HttpClientManager.RETRY_AMOUNT_MAX - 1, false,
        200, HttpClientManager.RETRY_AMOUNT_MAX + 1, false,
        200, HttpClientManager.RETRY_AMOUNT_MAX, false,
        400, HttpClientManager.RETRY_AMOUNT_MAX - 1, true,
        400, HttpClientManager.RETRY_AMOUNT_MAX + 1, false,
        400, HttpClientManager.RETRY_AMOUNT_MAX, true,
        503, HttpClientManager.RETRY_AMOUNT_MAX - 1, true,
        503, HttpClientManager.RETRY_AMOUNT_MAX + 1, false,
        503, HttpClientManager.RETRY_AMOUNT_MAX, true,
        504, HttpClientManager.RETRY_AMOUNT_MAX - 1, true,
        504, HttpClientManager.RETRY_AMOUNT_MAX + 1, false,
        504, HttpClientManager.RETRY_AMOUNT_MAX, true,
    };

    final List<Object[]> data = new ArrayList<>();
    for (int i = 0; i < realData.length - 1; i = i + 3) {
      data.add(new Object[] { realData[i], realData[i + 1], realData[i + 2] });
    }
    return data;
  }

  @Test
  public void testRetryOK() {
    final StatusLine statusLine = mock(StatusLine.class);
    doReturn(statusCode).when(statusLine).getStatusCode();

    final HttpResponse response = mock(HttpResponse.class);
    doReturn(statusLine).when(response).getStatusLine();

    final boolean result = retryStrategy.retryRequest(response, retry, new MockedHttpContext());

    if (expectedToRetry) {
      assertTrue("Retry unexpectedly failed", result);
    } else {
      assertFalse("Retry unexpectedly succeeded", result);
    }
  }

  private class MockedHttpContext implements HttpContext {

    @Override
    public Object getAttribute(final String id) {
      return null;
    }

    @Override
    public void setAttribute(final String id, final Object obj) { }

    @Override
    public Object removeAttribute(final String id) {
      return null;
    }

  }

}
