/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.Calendar;

import org.junit.Test;

/**
 * Test class for {@link FileUtil}.
 */
public class FileUtilTest {

  private static final String TEST_FILE_NAME = "someFile";
  private static final String TEST_FILE_NAME_WITH_SPACES = "some Fil   e   ";
  private static final String TEST_FILE_PREFIX = "naam";
  private static final String TEST_FILE_NAME_EXTENSION = "test";
  private static final String TEST_LONG_FILE_NAME = "abcdefghijklmnopqrstuvwxyz_are_the_letters_in_the_alphabet";

  @Test
  public void testGetSafeFilename() {
    String fileName = FileUtil.getSafeFilename(null);
    assertNull("Supplying null should return null", fileName);
    fileName = FileUtil.getSafeFilename("");
    assertEquals("Supplying empty string should return empty string", "", fileName);
    fileName = FileUtil.getSafeFilename(TEST_FILE_NAME);
    assertEquals("Correct file name should be returned as is", TEST_FILE_NAME, fileName);
    fileName = FileUtil.getSafeFilename(TEST_FILE_NAME_WITH_SPACES);
    assertEquals("Spaces should be removed", TEST_FILE_NAME, fileName);
  }

  @Test
  public void testGetFileName() {
    String fileName = FileUtil.getFileName(TEST_FILE_PREFIX, TEST_FILE_NAME_EXTENSION, null, null);
    assertNotNull("filename returned shouldn't be null", fileName);
    assertTrue("filename should start with prefix. " + fileName, fileName.startsWith(TEST_FILE_PREFIX));
    assertTrue("filename should end with extension. " + fileName, fileName.endsWith("." + TEST_FILE_NAME_EXTENSION));
    fileName = FileUtil.getFileName(TEST_FILE_PREFIX, "." + TEST_FILE_NAME_EXTENSION, TEST_FILE_NAME, null);
    assertNotNull("filename returned shouldn't be null", fileName);
    assertTrue("filename should start with prefix. " + fileName, fileName.startsWith(TEST_FILE_PREFIX));
    assertTrue("filename should end with extension. " + fileName, fileName.endsWith("." + TEST_FILE_NAME_EXTENSION));
    assertFalse("filename shouldn't end with double .. before extension. " + fileName, fileName.endsWith(".." + TEST_FILE_NAME_EXTENSION));
    assertTrue("filename should contain the optional name. " + fileName, fileName.contains(TEST_FILE_NAME));
    final Calendar tryoutDate = Calendar.getInstance();
    tryoutDate.set(2013, 11, 30, 18, 21);
    fileName = FileUtil.getFileName(TEST_FILE_PREFIX, TEST_FILE_NAME_EXTENSION, TEST_FILE_NAME_WITH_SPACES, tryoutDate.getTime());
    assertTrue("filename should contain the right date format. " + fileName , fileName.contains("201312301821"));
    assertTrue("filename should contain the optional name without spaces. " + fileName, fileName.contains(TEST_FILE_NAME));
  }

  @Test
  public void testGetFileNameOptional() {
    String longFileName = TEST_LONG_FILE_NAME;
    while (longFileName.length() < FileUtil.MAX_OPTIONAL_FILENAME_LENGTH) {
      longFileName += TEST_LONG_FILE_NAME;
    }
    final String fileName = FileUtil.getFileName(TEST_FILE_PREFIX, TEST_FILE_NAME_EXTENSION, longFileName, null);
    assertTrue("filename should contain the first X letters of the optional name. " + fileName ,
        fileName.contains(longFileName.substring(0, FileUtil.MAX_OPTIONAL_FILENAME_LENGTH)));
    assertFalse("filename shouldn't contain more than the first X letters of the optional name. " + fileName,
        fileName.contains(longFileName.substring(0, FileUtil.MAX_OPTIONAL_FILENAME_LENGTH + 1)));
  }

  @Test
  public void testGetFileNameEmpty() {
    final String fileName = FileUtil.getFileName(TEST_FILE_PREFIX, null, null);
    assertNotNull("filename returned shouldn't be null", fileName);
    assertTrue("filename should start with prefix. " + fileName, fileName.startsWith(TEST_FILE_PREFIX));
    assertFalse("filename should end with extension. " + fileName, fileName.endsWith("."));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetFileNameWithoutExtendsionNullPrefix() {
    FileUtil.getFileName(null, null, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetFileNameNullPrefix() {
    FileUtil.getFileName(null, TEST_FILE_NAME_EXTENSION, null, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetFileNameNullExtension() {
    FileUtil.getFileName(TEST_FILE_NAME, null, null, null);
  }

  @Test
  public void testGetExtension() {
    final File fileWithoutExtension = new File("NO");
    assertNull("File without extension should return null", FileUtil.getExtension(fileWithoutExtension));
    final File fileWithExtension = new File("YES.tst");
    assertEquals("File with extension", "tst", FileUtil.getExtension(fileWithExtension));
    final File fileWithDoubleExtension = new File("YES.another.tst");
    assertEquals("File with double dots", "tst", FileUtil.getExtension(fileWithDoubleExtension));
  }

  @Test
  public void testGetFiles() throws FileNotFoundException {
    final String file = FileUtilTest.class.getResource("").getFile();
    assertFalse("Check if find files in directory with no filter", FileUtil.getFilesWithExtension(new File(file), null).isEmpty());
    assertFalse("Check if find files in directory with", FileUtil.getFilesWithExtension(new File(file), new FilenameFilter() {

      @Override
      public boolean accept(final File dir, final String name) {
        return name.endsWith("class");
      }
    }).isEmpty());
    assertFalse("Check if find this file", FileUtil.getFilesWithExtension(new File(file, FileUtilTest.class.getSimpleName() + ".class"),
        new FilenameFilter() {
      @Override
      public boolean accept(final File dir, final String name) {
        return name.endsWith("class");
      }
    }).isEmpty());
  }
}
