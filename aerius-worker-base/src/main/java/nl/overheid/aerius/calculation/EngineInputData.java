/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Base class for Calculation Engine input data.
 */
public class EngineInputData<T extends EngineSource, R extends AeriusPoint> implements Serializable {

  private static final long serialVersionUID = 7575745211023233444L;

  private final Profile profile;
  private int year;
  private Collection<T> emissionSources = new ArrayList<>();
  private Collection<R> receptors = new ArrayList<>();
  private List<Substance> substances;
  private EnumSet<EmissionResultKey> emissionResultKeys;

  protected EngineInputData(final Profile profile) {
    this.profile = profile;
  }

  public Profile getProfile() {
    return profile;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public int getSourcesSize() {
    return emissionSources.size();
  }

  public Collection<T> getEmissionSources() {
    return emissionSources;
  }

  public void setEmissionSources(final Collection<T> emissionSources) {
    this.emissionSources = emissionSources;
  }

  public Collection<R> getReceptors() {
    return receptors;
  }

  public void setReceptors(final Collection<R> receptors) {
    this.receptors = receptors;
  }

  public List<Substance> getSubstances() {
    return substances;
  }

  public void setSubstances(final List<Substance> substances) {
    this.substances = substances;
  }

  public EnumSet<EmissionResultKey> getEmissionResultKeys() {
    return emissionResultKeys;
  }

  public void setEmissionResultKeys(final EnumSet<EmissionResultKey> emissionResultKeys) {
    this.emissionResultKeys = emissionResultKeys;
  }

  @Override
  public String toString() {
    return "#receptors=" + receptors + ",#emissionSources=" + emissionSources.size()
    + ", year=" + year + ", substances=" + substances + ", emissionResultKeys=" + emissionResultKeys;
  }

}
