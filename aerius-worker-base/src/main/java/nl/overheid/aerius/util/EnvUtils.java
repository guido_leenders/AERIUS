/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.Locale;

public final class EnvUtils {

  private static final String ENV_PREFIX = "AERIUS.";

  private EnvUtils() {
    // util class
  }

  /**
   * The propertyname will be prepended with the default ENV prefix as configured above (in <code>ENV_PREFIX</code>) before fetching.
   * Dots will be replaces by underscores and the resulting String will be uppercased.
   * e.g.: propertyName <code>mail.host</code> results in <code>AERIUS_MAIL_HOST</code> being fetched.
   * @param propertyName The property name to use.
   * @return Env property if it exists, null otherwise.
   */
  public static String getEnvProperty(final String propertyName) {
    final String envPropertyName = (ENV_PREFIX + propertyName).replace('.', '_').toUpperCase(Locale.ENGLISH);

    return System.getenv(envPropertyName);
  }

  /**
   * Same as {@link #getEnvProperty(String)} but with a default String for in case it is not set.
   * @param propertyName The property name to use.
   * @param defaultString The default to return if the env property isn't found.
   * @return Env property if it exists, <code>defaultString</code> otherwise.
   */
  public static String getEnvProperty(final String propertyName, final String defaultString) {
    final String result = getEnvProperty(propertyName);

    return result == null ? defaultString : result;
  }
}
