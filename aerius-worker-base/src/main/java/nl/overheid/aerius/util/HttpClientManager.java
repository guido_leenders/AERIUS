/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;

/**
 * Manager to retrieve a HttpClient, fully configured and ready to use.
 */
public final class HttpClientManager {

  static final int RETRY_AMOUNT_MAX = 5;
  static final int TIME_WAIT_BETWEEN_RETRIES = 10; // in seconds

  private HttpClientManager() {
    // Not to be constructed or extended.
  }

  private static HttpClientBuilder getBaseBuilder(final int timeout) {
    final int millisecondsTimeout = (int) TimeUnit.SECONDS.toMillis(timeout);
    final RequestConfig requestConfig = RequestConfig.custom()
        .setConnectTimeout(millisecondsTimeout)
        .setConnectionRequestTimeout(millisecondsTimeout)
        .setSocketTimeout(millisecondsTimeout).build();
    return HttpClients.custom()
        .useSystemProperties()
        .setDefaultRequestConfig(requestConfig);
  }

  /**
   * Returns a new HTTP client with the default settings already set. The client can and should be re-used if possible.
   * Client will NOT retry if a request returns an HTTP error.
   * @param timeout Connect timeout in seconds. -1 is to not set this and use the OS/Java defaults and such.
   * @return reusable HTTP client
   */
  public static CloseableHttpClient getHttpClient(final int timeout) {
    return getBaseBuilder(timeout)
        .build();
  }


  /**
   * Returns a new HTTP client with the default settings already set. The client can and should be re-used if possible.
   * Client will retry GET requests on errors. Errors with HTTP status code 400 and above are retried.
   * @param timeout Connect timeout in seconds. -1 is to not set this and use the OS/Java defaults and such.
   * @return reusable HTTP client
   */
  public static CloseableHttpClient getRetryingHttpClient(final int timeout) {
    return getBaseBuilder(timeout)
        .setServiceUnavailableRetryStrategy(new ServiceUnavailableRetryStrategyImpl())
        .build();
  }

  static class ServiceUnavailableRetryStrategyImpl implements ServiceUnavailableRetryStrategy {

    @Override
    public boolean retryRequest(final HttpResponse response, final int executionCount, final HttpContext context) {
      boolean retry = false;
      final boolean isHttpError = response.getStatusLine().getStatusCode() >= HttpStatus.SC_BAD_REQUEST; // 400 and up are considered errors

      if (isHttpError && executionCount <= RETRY_AMOUNT_MAX) {
        final HttpClientContext clientContext = HttpClientContext.adapt(context);
        final HttpRequest request = clientContext.getRequest();

        // Retry if the request is considered idempotent.
        retry = !(request instanceof HttpEntityEnclosingRequest);
      }

      return retry;
    }

    @Override
    public long getRetryInterval() {
      return TimeUnit.SECONDS.toMillis(TIME_WAIT_BETWEEN_RETRIES);
    }

  }

}
