/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

/**
 * Util class for OS related functionality.
 */
public final class OSUtils {

  /**
   * Windows newline characters.
   */
  public static final String WNL = "\r\n";
  /**
   * Linux newline character.
   */
  public static final String LNL = "\n";
  /**
   * New line based on current OS running.
   */
  public static final String NL = OSUtils.isWindows() ? OSUtils.WNL : OSUtils.LNL;

  private OSUtils() {
  }

  /**
   * Returns true if the current OS is windows.
   *
   * @return true if os is Windows
   */
  public static boolean isWindows() {
    return System.getProperty("os.name").startsWith("Windows");
  }
}
