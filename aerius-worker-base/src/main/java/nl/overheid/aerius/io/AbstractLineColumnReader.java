/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.io;


/**
 * Line file reader that splits the line into columns. For index instead of using the position in the string use the position in
 * the column.
 * @param <K> data type for the information read from each line
 */
public abstract class AbstractLineColumnReader<K> extends AbstractLineReader<K> {

  private static final String SPLIT_PATTERN = "[\t ]+";
  private String[] splitCurrentLine;
  private final String splitPattern;

  protected AbstractLineColumnReader() {
    splitPattern = SPLIT_PATTERN;
  }

  protected AbstractLineColumnReader(final String splitPattern) {
    this.splitPattern = splitPattern;
  }

  @Override
  protected void preprocessLine(final String line) {
    splitCurrentLine = line.trim().split(splitPattern);
  }

  @Override
  protected String processColumn(final int index) {
    return splitCurrentLine[index];
  }

  @Override
  protected String processColumn(final int index, final int size) {
    return processColumn(index);
  }
}
