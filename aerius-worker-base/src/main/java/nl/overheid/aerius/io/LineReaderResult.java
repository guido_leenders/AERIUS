/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.io;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Contains the results of a AbstractLineReader. The objects successfully read and the possible exceptions encountered.
 * @param <K> The object type that is being read.
 */
public class LineReaderResult<K> {

  private final List<K> objects = new ArrayList<>();
  private final List<AeriusException> exceptions = new ArrayList<>();
  private final List<AeriusException> warnings = new ArrayList<>();

  /**
   * Add an object to the list of objects.
   * @param object The object to add.
   */
  public void addObject(final K object) {
    objects.add(object);
  }

  /**
   * Add an exception to the list of exceptions.
   * @param exception The exception to add.
   */
  public void addException(final AeriusException exception) {
    exceptions.add(exception);
  }

  /**
   * It's an ArrayList, so cast away if needed for the client.
   * @return ArrayList (YA RLY)
   */
  public List<K> getObjects() {
    return objects;
  }

  public List<AeriusException> getExceptions() {
    return exceptions;
  }

  public List<AeriusException> getWarnings() {
    return warnings;
  }
}
