/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.OSUtils;

/**
 * Abstract class to read an input per line and provide extra functionality to
 * read column based data per line. When an error would occur, for example if
 * an integer could not be parsed via the getCurrent... methods it can be
 * retrieved what triggered the error.
 * @param <K> Objects of these type will be read.
 */
public abstract class AbstractLineReader<K> {

  private String currentLine;
  private int currentLineNumber;
  private String currentColumnName;
  private String currentColumnContent;
  private int currentIndex;

  public abstract LineReaderResult<K> readObjects(InputStream inputStream) throws IOException;

  protected LineReaderResult<K> readObjects(final InputStreamReader inputStream, final int headerRowCount) throws IOException {
    return readObjects(inputStream, headerRowCount, Integer.MAX_VALUE, false);
  }

  protected LineReaderResult<K> readObjects(final InputStreamReader inputStream, final int headerRowCount, final int maxRowNumber,
      final boolean failOnFirstError) throws IOException {
    try (final BufferedReader reader = new BufferedReader(inputStream)) {
      return readObjects(reader, headerRowCount, maxRowNumber, failOnFirstError);
    }
  }

  /**
   * Read the objects from the buffered reader.
   *
   * @param reader buffered reader that can be reset
   * @param headerRowCount amount of starting rows containing headers, so they can be skipped.
   * @param maxRowNumber amount of rows to read, including header rows.
   * @param failOnFirstError whether to fail on the first line with an error.
   * @return LineReaderResult Containing all objects (/lines) read successfully.
   *  The lines that are not correct will be added to the list of exceptions.  If failOnFirstError is true the {@link LineReaderResult} will contain
   *  one exception and the objects read successfully so far. I'd advice to simply ignore the results in this case and fail,
   *  but hey.. it's an advice. By using this method your hereby accept the responsibility [INSERT rest of EULA here].
   * @throws IOException On IO error.
   */
  protected LineReaderResult<K> readObjects(final BufferedReader reader, final int headerRowCount, final int maxRowNumber,
      final boolean failOnFirstError) throws IOException {
    final LineReaderResult<K> result = createLineReaderResult();
    //ensure line numbers are correct in case of re-using the reader.
    currentLineNumber = 0;
    try {
      skipHeaderRow(reader, headerRowCount);
      boolean run = true;

      while (run) {
        run = readLineForObject(reader, result, maxRowNumber, failOnFirstError);
      }
    } catch (final AeriusException e1) {
      result.addException(e1);
    }

    return result;
  }

  protected LineReaderResult<K> createLineReaderResult() {
    return new LineReaderResult<>();
  }

  private boolean readLineForObject(final BufferedReader reader, final LineReaderResult<K> result, final int maxRowNumber,
      final boolean failOnFirstError) throws IOException, AeriusException {
    boolean nextRow = true;
    try {
      nextRow = readObject(reader, result) && currentLineNumber < maxRowNumber;
    } catch (final NumberFormatException | IndexOutOfBoundsException | AeriusException e) {
      result.addException(processException(e));
      if (failOnFirstError) {
        nextRow = false;
      }
    }
    return nextRow;
  }

  protected AeriusException processException(final Exception e) {
    final AeriusException ae;
    if (e instanceof NumberFormatException) {
      ae = new AeriusException(Reason.IO_EXCEPTION_NUMBER_FORMAT, String.valueOf(getCurrentLineNumber()),
          String.valueOf(getCurrentColumnName()), String.valueOf(getCurrentColumnContent()));
    } else if (e instanceof IndexOutOfBoundsException) {
      ae = new AeriusException(Reason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, String.valueOf(getCurrentLineNumber()));
    } else {
      ae = e instanceof AeriusException ? (AeriusException) e
          : new AeriusException(Reason.IO_EXCEPTION_UNKNOWN, String.valueOf(getCurrentLineNumber()));
    }
    return ae;
  }

  private boolean readObject(final BufferedReader reader, final LineReaderResult<K> result) throws IOException, AeriusException {
    final String line = reader.readLine();

    incrementCurrentLineNumber();
    if (line != null && !line.trim().isEmpty()) {
      preprocessLine(line);
      final K object = parseLine(line, result.getWarnings());
      if (object != null) {
        result.addObject(object);
      }
      return true;
    }
    return false;
  }

  /**
   * Parses the header (The first n lines of the file containing header text) by number of rows and returns the header as Strign.
   *
   * @param reader Buffer to read lines from
   * @param headerRowCount Number of rows the header is
   * @return header text
   * @throws IOException
   * @throws AeriusException
   */
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    final StringBuilder headerBuffer = new StringBuilder();

    for (int i = 0; i < headerRowCount; ++i) {
      incrementCurrentLineNumber();
      headerBuffer.append(reader.readLine());
      headerBuffer.append(OSUtils.NL);
    }
    return headerBuffer.toString();
  }

  protected void incrementCurrentLineNumber() {
    currentLineNumber++;
  }

  public int getCurrentLineNumber() {
    return currentLineNumber;
  }

  public String getCurrentColumnContent() {
    return currentColumnContent;
  }

  public String getCurrentColumnName() {
    return currentColumnName;
  }

  public int getCurrentIndex() {
    return currentIndex;
  }

  protected int getInt(final String columnName, final int index) {
    return getSubString(columnName, index).isEmpty() ? 0 : (int) Double.parseDouble(getSubString(columnName, index));
  }

  protected int getInt(final String columnName, final int index, final int size) {
    return (int) Double.parseDouble(getSubString(columnName, index, size));
  }

  protected float getFloat(final String columnName, final int index) {
    return getSubString(columnName, index).isEmpty() ? 0.0F : Float.parseFloat(getSubString(columnName, index));
  }

  protected float getFloat(final String columnName, final int index, final int size) {
    return Float.parseFloat(getSubString(columnName, index, size));
  }

  protected double getDouble(final String columnName, final int index) {
    return getSubString(columnName, index).isEmpty() ? 0.0 : Double.parseDouble(getSubString(columnName, index));
  }

  protected double getDouble(final String columnName, final int index, final int size) {
    return Double.parseDouble(getSubString(columnName, index, size));
  }

  protected String getString(final String columnName, final int index) {
    return getSubString(columnName, index);
  }

  protected String getString(final String columnName, final int index, final int size) {
    return getSubString(columnName, index, size);
  }

  protected void preprocessLine(final String line) {
    currentLine = line;
  }

  /**
   * Implementing subclasses should parse the line and store results into the object for this line reader.
   * The actual line parsing itself should not be done. But data columns should be access via the get[Type] methods.
   * @param line line currently being parsed, use only for reference
   * @param warnings Add warnings to this list
   * @return data object containing information from line converted to data object
   */
  protected abstract K parseLine(String line, List<AeriusException> warnings) throws AeriusException;

  /**
   * @param index
   * @param size
   * @return
   */
  protected String processColumn(final int index) {
    return currentLine.substring(index);
  }

  protected String processColumn(final int index, final int size) {
    return currentLine.substring(index, index + size);
  }

  /**
   * @param columnName
   * @param index
   * @return
   */
  private String getSubString(final String columnName, final int index) {
    currentColumnName = columnName;
    this.currentIndex = index;
    currentColumnContent = "";
    currentColumnContent = processColumn(index).trim();
    return currentColumnContent;
  }

  private String getSubString(final String columnName, final int index, final int size) {
    currentColumnName = columnName;
    this.currentIndex = index;
    currentColumnContent = "";
    currentColumnContent = processColumn(index, size).trim();
    return currentColumnContent;
  }
}
