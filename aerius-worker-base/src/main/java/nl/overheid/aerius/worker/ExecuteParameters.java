/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;


/**
 * Contains the various calculation methods that we can use in the worker.
 */
public class ExecuteParameters {

  /**
   * Windows Executable extension.
   */
  private static final String WINDOWS_EXE_EXT = ".exe";

  // Executable file name, without the executable extension if present.
  private final String executeableFilename;
  // Optional args that are placed before the final arg, which is the settingsfile/ID of project and such to calculate.
  private final String[] args;

  public ExecuteParameters(final String executeableFilename, final String ... args) {
    this.executeableFilename = executeableFilename;
    this.args = args;
  }

  public String getExecuteableFilename() {
    return executeableFilename;
  }

  /**
   * Get the executable filename.
   * @param windowsBinary Whether the binary is the Windows one, if so append the Windows executable extension to the filename.
   * @return executable filename
   */
  public String getExecuteableFilename(final boolean windowsBinary) {
    return executeableFilename + (windowsBinary ? WINDOWS_EXE_EXT : "");
  }

  public String[] getArgs() {
    return args;
  }
}
