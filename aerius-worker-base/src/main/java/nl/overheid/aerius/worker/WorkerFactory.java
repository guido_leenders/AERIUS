/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Factory to create worker configuration and the worker. Workers should implement this interface and add the usage to the main
 * worker class.
 * @param <C> specific worker
 */
public interface WorkerFactory<C extends WorkerConfiguration> {

  /**
   * Returns a worker specific configuration class given a properties object. The specific configuration class can have package visibility
   * to reduce the visibility of the specific configuration class as it should only used with the worker package itself.
   * @param properties properties object with worker configuration
   * @return worker specific configuration class.
   */
  C createConfiguration(Properties properties);

  /**
   * Returns a the actual worker that performs the operation. The class returned should preferable be a subclass of {@link WorkerHandlerImpl}.
   * @param configuration worker specific configuration returned by {@link #createConfiguration(Properties)}.
   * @param factory broker connection factory
   * @return worker
   * @throws IOException any io error
   * @throws AeriusException exception
   * @throws SQLException
   */
  WorkerHandler createWorkerHandler(C configuration, BrokerConnectionFactory factory) throws IOException, AeriusException, SQLException;

  /**
   * Returns the worker type queue group this worker runs on.
   * @return worker type
   */
  WorkerType getWorkerType();
}
