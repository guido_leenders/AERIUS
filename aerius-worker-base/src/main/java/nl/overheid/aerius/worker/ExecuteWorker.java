/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.util.OSUtils;

/**
 * Performs the actual execution of the CalculationMethod executable and wraps the output to be directed correctly.
 */
public final class ExecuteWorker {

  private static final Logger LOG = LoggerFactory.getLogger(ExecuteWorker.class);

  private static final String ERROR_LOGGING_PREFIX = "ERROR";

  private final ExecuteParameters executeParameters;
  private final String binDir;
  private final boolean runAsWindows;

  /**
   * Initializes.
   *
   * @param calculationMethod the calculation method to use for this execution.
   * @param binDir directory to the calculation method executable
   */
  public ExecuteWorker(final ExecuteParameters calculationMethod, final String binDir) {
    this.executeParameters = calculationMethod;
    this.binDir = binDir;
    this.runAsWindows = OSUtils.isWindows();
  }

  /**
   * Run with default stream gobblers.
   * @param runId the ID that is only used when logging, so the process running can be identified
   * @param runArgument the argument that is needed for the calculation method to run, like the id of the project or path to a file.
   * @param currentWorkingDirectory The current working directory to use (optional). If null, the current working directory of
   *  the process executing it will be used.
   * @throws IOException on I/O errors
   * @throws InterruptedException on interrupted streams
   */
  public void run(final String runId, final File currentWorkingDirectory) throws IOException, InterruptedException {
    run(currentWorkingDirectory,
        new StreamGobbler(ERROR_LOGGING_PREFIX, Level.ERROR, executeParameters.getExecuteableFilename() + "-" + runId),
        new StreamGobbler(ERROR_LOGGING_PREFIX, Level.ERROR, "") {
          @Override
          public void run() {
            // We ignore the stdout as it doesn't contain useful information
            try (final BufferedInputStream br = new BufferedInputStream(getInputStream())) {
              while (br.read() != -1) {
                // read and ignore till the end of the stream has been reached
              }
            } catch (final IOException ioe) {
              LOG.error("Error while reading stdout while running external.", ioe);
            }
          }
        });
  }

  /**
   *
   * @param runArgument the argument that is needed for the calculation method to run, like the id of the project or path to a file.
   * @param currentWorkingDirectory The current working directory to use (optional). If null, the current working directory of
   *  the process executing it will be used.
   * @param errorGobbler error stream handler
   * @param outputGobbler error stream handler
   * @throws IOException on I/O errors
   * @throws InterruptedException on interrupted streams
   */
  public void run(final File currentWorkingDirectory,
      final StreamGobbler errorGobbler, final StreamGobbler outputGobbler) throws IOException, InterruptedException {
    try {
      final String executeString = Paths.get(binDir, executeParameters.getExecuteableFilename(runAsWindows)).toString();

      final String[] executeArray = new String[executeParameters.getArgs().length + 1];
      executeArray[0] = executeString;
      System.arraycopy(executeParameters.getArgs(), 0, executeArray, 1, executeParameters.getArgs().length);

      if (LOG.isDebugEnabled()) {
        LOG.debug("Executing: {}", StringUtils.join(executeArray, ' '));
      }
      final Process process = Runtime.getRuntime().exec(executeArray, null, currentWorkingDirectory);

      // redirect streams to the gobbler to be able to let process.waitFor
      // function correctly
      errorGobbler.setInputStream(process.getErrorStream());
      outputGobbler.setInputStream(process.getInputStream());
      errorGobbler.start();
      outputGobbler.start();
      process.waitFor();
    } finally {
      try {
        if (errorGobbler != null && errorGobbler.isAlive()) {
          errorGobbler.interrupt();
        }
      } catch (final Exception e) {
        LOG.error("Error reading error gobbler.", e);
      }
      try {
        if (outputGobbler != null && outputGobbler.isAlive()) {
          outputGobbler.interrupt();
        }
      } catch (final Exception e) {
        LOG.error("Error reading output gobbler.", e);
      }
    }
  }
}
