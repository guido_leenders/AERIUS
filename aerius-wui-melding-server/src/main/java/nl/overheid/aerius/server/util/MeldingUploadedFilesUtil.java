/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Util class to handle uploaded files.
 */
public final class MeldingUploadedFilesUtil {
  private static final Logger LOG = LoggerFactory.getLogger(MeldingUploadedFilesUtil.class);

  private MeldingUploadedFilesUtil() {
    // Util class
  }

  public static void deleteFile(final Map<String, File> files, final String fileName, final String uuid) {
    LOG.info("[{}] deletefile {} exist in list {}", uuid, fileName, files.get(fileName) == null ? "no" : "yes");
    final File file = files.get(fileName);
    if (file == null) {
      LOG.warn("[{}] Tried to delete non-existing file {}", uuid, fileName);
    } else {
      LOG.info("Delete file {}", fileName);
      files.remove(fileName);
      if (file.isFile()) {
        file.delete();
      }
    }
  }

  public static byte[] createZipFromFiles(final Map<String, File> files, final List<String> fileNames, final String uuid) throws IOException {
    final byte[] content;
    int count = 0;
    if (fileNames.isEmpty()) {
      content = null;
    } else {
      try (final ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
        try (final ZipFileMaker fileMaker = new ZipFileMaker(bos)) {
          for (final String fileName : fileNames) {
            if (files.containsKey(fileName)) {
              LOG.info("[{}] Attach file: {}", uuid, fileName);
              final File file = files.get(fileName);
              fileMaker.add(fileName, file);
              if (!file.delete()) {
                LOG.warn("[{}] Cleaning up generated file failed: {}", uuid, file.getAbsolutePath());
              }
              count++;
            } else {
              throw new IOException("file " + fileName + " not found. Uploading failed?");
            }
          }
        }
        content = bos.toByteArray();
      }
    }
    if (LOG.isInfoEnabled()) {
      LOG.info("[{}] Zip files added count: {}, size: {}", uuid, count, content == null ? null : content.length);
    }
    return content;
  }

  public static Boolean existFile(final Map<String, File> files, final String fileName, final String uuid) {
    LOG.info("[{}] does file {} exist in list {}", uuid, fileName, files.get(fileName) == null ? "no" : "yes");
    return files.get(fileName) == null ? false : true;
  }
}
