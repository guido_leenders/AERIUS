/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.server.service.BaseUploadService;
import nl.overheid.aerius.server.util.MeldingSessionUtil;
import nl.overheid.aerius.shared.SharedConstants;

/**
 * Standard servlet to process file upload via HTTP POST (the only way to upload
 * files to the server).
 * Save files on temp.
 */
@MultipartConfig
@SuppressWarnings("serial")
@WebServlet("/aerius/" + SharedConstants.IMPORT_SAVE_SERVLET)
public class MeldingUploadServlet extends HttpServlet {

  private static final Logger LOG = LoggerFactory.getLogger(MeldingUploadServlet.class);

  /**
   * Processes the uploaded file and stores it in the database.
   * @throws IOException
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      final String uuid = request.getParameter(SharedConstants.MELDING_UUID_PARAM);
      final BaseUploadService service = new BaseUploadService();
      final boolean success = service.processUploadDocument(request);
      if (success) {
        final File file = MeldingSessionUtil.addUploadFile(request.getSession(),
            "undefined".equalsIgnoreCase(uuid) ? null : uuid, service.getFileName());
        service.getPart().write(file.getAbsolutePath());
        LOG.info("Received file {} -> {}", service.getFileName(), file);
      } else {
        LOG.warn("Could not process file: {}", service.getFileName());
      }
      response.setStatus(HttpServletResponse.SC_CREATED);
      service.sendResponse(response, success, uuid);
    } catch (final IOException e) {
      LOG.error("MeldingUploadServlet", e);
    }
  }
}
