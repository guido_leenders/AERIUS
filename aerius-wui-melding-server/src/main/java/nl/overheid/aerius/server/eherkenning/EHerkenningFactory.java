/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.eherkenning;

import org.apache.shiro.util.AbstractFactory;
import org.federatenow.er4b.sp.client.factories.DependencyFactory;
import org.federatenow.er4b.sp.client.providers.ContextParameterProvider;
import org.federatenow.er4b.sp.client.providers.IContextParameterProvider;
import org.federatenow.er4b.sp.client.providers.IKeyConfigProvider;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;

/**
 * Factory to get configuration for library. Reads configuration from the database.
 */
public class EHerkenningFactory extends AbstractFactory<DependencyFactory> {

  private static final String CONFIG_DIR = "org.federatenow.er4b.sp.client.configDir";

  @Override
  protected DependencyFactory createInstance() {
    return new DependencyFactory() {
      @Override
      public IKeyConfigProvider createKeyConfigProvider() {
        return new IKeyConfigProvider() {

          @Override
          public String getKeyStorePassword() {
            return ConstantRepository.getString(ServerPMF.getInstance(), SharedConstantsEnum.EHERKENNING_JKSPASS);
          }

          @Override
          public String getKeyStoreLocation() {
            return ConstantRepository.getString(ServerPMF.getInstance(), SharedConstantsEnum.EHERKENNING_JKSFILE);
          }
        };
      }

      @Override
      public IContextParameterProvider createContextParameterProvider() {
        return new ContextParameterProvider() {
          @Override
          public String getContextParameter(final String arg0) {
            return CONFIG_DIR.equals(arg0)
                ? ConstantRepository.getString(ServerPMF.getInstance(), SharedConstantsEnum.EHERKENNING_CONFIGDIR) : super.getContextParameter(arg0);
          }
        };
      }
    };
  }
}
