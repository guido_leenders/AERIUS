/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.federatenow.er4b.sp.client.entities.AcsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ContextRepository;
import nl.overheid.aerius.server.servlet.MeldingInitServlet;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;

/**
 * Service with all methods related to contextual data. This is both global
 * context data which is static during a user session as well as user specific context data.
 */
public class MeldingContextServiceImpl extends AbstractContextService<MeldingSession> {
  private static final Logger LOG = LoggerFactory.getLogger(MeldingInitServlet.class);

  public MeldingContextServiceImpl(final PMF pmf, final MeldingSession session) {
    super(pmf, session);
  }

  @Override
  public Context getContext() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ContextRepository.getMeldingContext(con);
    } catch (final SQLException e) {
      LOG.error("Error getting context for product {}", pmf.getProductType(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public UserContext getUserContext() throws AeriusException {
    final MeldingUserContext u = new MeldingUserContext();
    setProvidedData(u, session.getLocale());
    return u;
  }

  @Override
  public void closeSession(final String lastCalculationKey) {
    final Subject subject = SecurityUtils.getSubject();
    subject.logout();
  }

  private void setProvidedData(final MeldingUserContext u, final Locale locale) throws AeriusException {
    final Subject subject = SecurityUtils.getSubject();
    if (!subject.isAuthenticated()) {
      // The subject is not authenticated; throw auth error.
      throw new AuthorizationException();
    }
    for (final Object p : subject.getPrincipals()) {
      if (p instanceof AcsResponse) {
        final AcsResponse acs = (AcsResponse) p;
        final MeldingInformation meldingInformation = new MeldingInformation();
        meldingInformation.setUUID(((AcsResponse) p).getRelayState());
        meldingInformation.setOrganizationOin(acs.getOrganizationOin());
        meldingInformation.setLocale(locale.getLanguage());
        u.setMeldingInformation(meldingInformation);
        LOG.info("User successfully logged in via eHerkenning: {}", meldingInformation.getUUID());
      }
    }
  }
}
