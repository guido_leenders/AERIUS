/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.service.MeldingServiceRegistry;
import nl.overheid.aerius.server.service.MeldingSession;
import nl.overheid.aerius.shared.ServiceURLConstants;

/**
 * Dispatcher servlet for Melding. Uses a MeldingServiceRegistry instance for service lookups and provides it with a MeldingSession.
 */
@WebServlet(ServiceURLConstants.DISPATCHER_SERVLET_MAPPING)
public class MeldingDispatcherServlet extends DispatcherServlet {
  private static final long serialVersionUID = -6455703892737631309L;

  @Override
  public void init(final ServletConfig config) throws ServletException {
    super.init(config);

    final MeldingSession meldingSession = new AbstractMeldingSession() {
      @Override
      public HttpServletRequest getRequest() {
        return getThreadLocalRequest();
      }
    };
    registry = new MeldingServiceRegistry(ServerPMF.getInstance(), meldingSession);
  }
}
