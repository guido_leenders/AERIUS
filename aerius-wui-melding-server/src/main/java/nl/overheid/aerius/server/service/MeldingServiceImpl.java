/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import nl.overheid.aerius.register.MeldingPayloadData;
import nl.overheid.aerius.server.task.MeldingTaskClient;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.server.util.MeldingSessionUtil;
import nl.overheid.aerius.server.util.MeldingUploadedFilesUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;
import nl.overheid.aerius.shared.service.MeldingService;
import nl.overheid.aerius.taskmanager.client.NoopCallback;
import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Implementation of Melding service. Submits melding to register.
 */
public class MeldingServiceImpl implements MeldingService {
  private static final Logger LOG = LoggerFactory.getLogger(MeldingServiceImpl.class);

  private final MeldingSession session;

  public MeldingServiceImpl(final MeldingSession session) {
    this.session = session;
  }

  @Override
  public void submitMelding(final MeldingInformation meldingInformation) throws AeriusException {
    if (SecurityUtils.getSubject() == null || !SecurityUtils.getSubject().isAuthenticated()) {
      throw new AuthorizationException();
    }
    if (meldingInformation == null) {
      LOG.error("No meldingInformation received in submitMelding");
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    LOG.info("[{}] Received {}",  meldingInformation.getUUID(), meldingInformation);
    final HttpSession httpSession = session.getSession();
    if (httpSession == null) {
      LOG.error("[{}] Session seems to be gone.", meldingInformation.getUUID());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    try {
      final byte[] payload = createPayload(httpSession, meldingInformation);
      MeldingTaskClient.startMeldingTask(TaskClientFactory.getInstance(), payload, new NoopCallback());
    } catch (final IOException e) {
      LOG.error("[{}] Error sending task to workers",  meldingInformation.getUUID(), e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  /**
   * create payload zip file with all melding content
   *
   * @param jsonInformation
   * @param proposedGML
   * @param currentGML
   * @param zipContent
   * @return
   * @throws AeriusException
   */
  private byte[] createPayload(final HttpSession httpSession, final MeldingInformation meldingInformation)
      throws AeriusException {
    final byte[] fileContent;
    try (final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final ZipFileMaker fileMaker = new ZipFileMaker(bos)) {
      LOG.info("Creating melding payload for session {} with UUID {}", httpSession.getId(), meldingInformation.getUUID());
      addJson(fileMaker, meldingInformation);
      addGML(fileMaker, MeldingSessionUtil.getGMLFromSession(httpSession, meldingInformation.getUUID(), SharedConstants.MELDING_GML_PROPOSED),
          MeldingPayloadData.PROPOSED_GML);
      addGML(fileMaker, MeldingSessionUtil.getGMLFromSession(httpSession, meldingInformation.getUUID(), SharedConstants.MELDING_GML_CURRENT),
          MeldingPayloadData.CURRENT_GML);
      addFiles(fileMaker, httpSession, meldingInformation);
      fileContent = bos.toByteArray();
    } catch (final IOException e) {
      LOG.error("[{}] Error creating zip payload", meldingInformation.getUUID(), e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return fileContent;
  }

  /**
   * Add json to zip.
   * @param fileMaker
   * @param meldingInformation
   * @throws IOException
   */
  private void addJson(final ZipFileMaker fileMaker, final MeldingInformation meldingInformation) throws IOException {
    final Gson gson = new Gson();
    fileMaker.add(MeldingPayloadData.MELDING_JSON, new ByteArrayInputStream(gson.toJson(meldingInformation).getBytes(StandardCharsets.UTF_8.name())));
  }

  /**
   * Add gml to zip.
   * @param fileMaker
   * @param httpSession
   * @throws IOException
   * @throws UnsupportedEncodingException
   */
  private void addGML(final ZipFileMaker fileMaker, final String gml, final String payloadKey) throws UnsupportedEncodingException, IOException {
    if (gml != null && !gml.isEmpty()) {
      fileMaker.add(payloadKey, new ByteArrayInputStream(gml.getBytes(StandardCharsets.UTF_8.name())));
    }
    if (LOG.isInfoEnabled()) {
      LOG.info("GML '{}', size: {}", payloadKey, gml == null ? null : gml.length());
    }
  }

  private void addFiles(final ZipFileMaker fileMaker, final HttpSession httpSession, final MeldingInformation meldingInformation) throws IOException {
    final List<String> filenames = new ArrayList<>();
    filenames.addAll(meldingInformation.getAuthorizationFiles());
    filenames.addAll(meldingInformation.getSubstantiationFiles());
    final Map<String, File> files = MeldingSessionUtil.getUploadFiles(httpSession, meldingInformation.getUUID());
    final byte[] zipContent = MeldingUploadedFilesUtil.createZipFromFiles(files, filenames, meldingInformation.getUUID());
    if (zipContent != null && zipContent.length > 0) {
      fileMaker.add(MeldingPayloadData.ATTACHMENTS, new ByteArrayInputStream(zipContent));
    }
  }


  @Override
  public void deleteUploadedFile(final String fileName, final String uuid) throws AeriusException {
    final HttpSession httpSession = session.getSession();
    MeldingUploadedFilesUtil.deleteFile(MeldingSessionUtil.getUploadFiles(httpSession, uuid), fileName, uuid);
  }

  @Override
  public Boolean validateUploadedFile(final String fileName, final String uuid) throws AeriusException {
    final HttpSession httpSession = session.getSession();
    return MeldingUploadedFilesUtil.existFile(MeldingSessionUtil.getUploadFiles(httpSession, uuid), fileName, uuid);

  }
}
