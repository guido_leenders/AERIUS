/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.eherkenning;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.federatenow.er4b.sp.client.entities.AcsResponse;
import org.federatenow.er4b.sp.client.entities.AuthnStatusCode;
import org.federatenow.er4b.sp.client.factories.DependencyFactory;
import org.federatenow.er4b.sp.client.providers.IConfigurationProvider;
import org.federatenow.er4b.sp.client.services.ISpClientHttpService;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
  * This filter validates the eHerkenning service to authenticate the user.
  * For example, in {@code shiro.ini}:
  * <pre>
  * [main]
  * eHerkenningClient = nl.overheid.aerius.server.eherkenning.EHerkenningFactory
  * ...
  * eHerkenningFilter = nl.overheid.aerius.server.eherkenning.EHerkenningFilter
  * eHerkenningFilter.dependencyFactory = $eHerkenningClient
  *
  * [urls]
  * /** = eHerkenningFilter
  * ...
  * </pre>
  */
public class EHerkenningFilter extends AuthenticatingFilter {

  private static final int SERVICE_INDEX = 2;
  private ISpClientHttpService spClientHttpService;
  private IConfigurationProvider configurationProvider;
  private DependencyFactory factory;

  @Override
  protected AuthenticationToken createToken(final ServletRequest request, final ServletResponse response)
      throws Exception {
    AcsResponse acsResponse;
    try {
      final String spEntityId = getConfigurationProvider().getDefaultEntityID();
      acsResponse = getSpClientHttpService().handleACS(spEntityId, WebUtils.toHttp(request), WebUtils.toHttp(response));
    } catch (final NullPointerException e) {
      // The only known way to determine if this is the redirect back from eHerkenning is call the library.
      // The library crashes with a null pointer if it isn't a redirect. Then we assume no token available and return null.
      acsResponse = null;
    }
    return new EHerkenningToken(acsResponse);
  }

  @Override
  protected boolean onAccessDenied(final ServletRequest request, final ServletResponse response) throws Exception {
    final boolean loggedIn;
    final boolean hasUUID = request.getParameter(SharedConstants.MELDING_UUID_PARAM) != null;
    final Subject subject = getSubject(request, response);
    // If not authenticated, check if has uuid
    if (subject.getPrincipal() == null || !subject.isAuthenticated()) {
      if (hasUUID) {
        // Redirected from the init page, with replayState argument, redirect to eHerkenning login page.
        loggedIn = false;
        saveRequestAndRedirectToLogin(request, response);
      } else {
        // No UUID, check if it's or direct access or the return from eHerkenning:
        final EHerkenningToken token = (EHerkenningToken) createToken(request, response);
        loggedIn = onLoginSuccess(token, subject, request, response);
        // If it was from eHerkenning we should have been logged in, else it's a direct access, and the user should get an error.
        if (!loggedIn) {
          throw new AeriusException(Reason.MELDING_NOT_VIA_CALCULATOR);
        }
      }
    } else { // Authenticated so we can continue using the site.
      loggedIn = true;
    }
    return loggedIn;
  }

  @Override
  protected void redirectToLogin(final ServletRequest request, final ServletResponse response) throws IOException {
    final String uuid = request.getParameter(SharedConstants.MELDING_UUID_PARAM);
    getSpClientHttpService().handleStartSSO(SERVICE_INDEX, uuid, (HttpServletRequest) request, (HttpServletResponse) response);
  }

  @Override
  protected boolean onLoginSuccess(final AuthenticationToken token, final Subject subject, final ServletRequest request,
      final ServletResponse response) throws Exception {
    final boolean succes;
    if (token instanceof EHerkenningToken && ((EHerkenningToken) token).getAcsResponse() != null
        && ((EHerkenningToken) token).getAcsResponse().getStatusCode() == AuthnStatusCode.Success) {
        subject.login(token);
        succes = true;
    } else {
      succes = false;
    }
    return succes;
  }

  @Override
  protected boolean onLoginFailure(final AuthenticationToken token, final AuthenticationException e,
      final ServletRequest request,
      final ServletResponse response) {
    return false;
  }

  public DependencyFactory getDependencyFactory() {
    return factory;
  }

  public void setDependencyFactory(final DependencyFactory factory) {
    this.factory = factory;
  }

  protected IConfigurationProvider getConfigurationProvider() {
    if (configurationProvider == null) {
        configurationProvider = getDependencyFactory().createConfigurationProvider();
    }
    return configurationProvider;
  }

  private ISpClientHttpService getSpClientHttpService() {
    if (spClientHttpService == null) {
      spClientHttpService = getDependencyFactory().createSpClientHttpService();
    }
    return spClientHttpService;
  }
}
