/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.util.MeldingSessionUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Servlet that stores the data passed from the calculator in the session.
 */
@WebServlet("/melden")
public class MeldingInitServlet extends HttpServlet {
  private static final long serialVersionUID = 4188401671164437475L;

  private static final Logger LOG = LoggerFactory.getLogger(MeldingInitServlet.class);

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * @param request
   * @param response
   * @throws IOException
   * @throws ServletException
   * @throws AeriusException
   */
  private void processRequest(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    // Always perform a logout before starting a new session. Not this doesn't logout from eHerkenning, only invalidates the current session.
    logout();
    Internationalizer.initLanguage(request, response);
    final String uuid = MeldingSessionUtil.setGMLInSession(request);
    if (uuid == null) {
      LOG.warn("User called /melden place, but no uuid or gml could be found in post.");
      throw new ServletException(new AeriusException(Reason.FAULTY_REQUEST));
    } else {
      response.sendRedirect(request.getContextPath() + "/?" + SharedConstants.MELDING_UUID_PARAM + '=' + uuid);
    }
  }

  private void logout() {
    final Subject subject = SecurityUtils.getSubject();
    if (subject != null) {
      subject.logout();
    }
  }
}