/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.Date;

/**
 * Class to store a notice.
 */
public class Notice extends Request {

  private static final long serialVersionUID = 7559641416824598191L;

  private Date receivedDate;

  public Notice() {
    super(SegmentType.PERMIT_THRESHOLD);
  }

  public NoticeKey getKey() {
    return new NoticeKey(getReference());
  }

  public Date getReceivedDate() {
    return receivedDate;
  }

  public void setReceivedDate(final Date receivedDate) {
    this.receivedDate = receivedDate;
  }

}
