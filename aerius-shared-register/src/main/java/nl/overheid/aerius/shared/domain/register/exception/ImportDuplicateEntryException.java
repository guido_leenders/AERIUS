/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register.exception;

import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Thrown when the clients tries to upload files that are already present (ID is already present).
 */
public class ImportDuplicateEntryException extends AeriusException {

  private static final long serialVersionUID = 369661835429100701L;

  private DossierAuthorityKey key;

  protected ImportDuplicateEntryException() {
  }

  /**
   * Default constructor - create {@link AeriusException} with the appropriate reason error code.
   * @param key The key which leads to the entry. Can be used to allow the user to open the duplicate entry in the UI.
   * i.e. PermitKey/PriorityProjectKey etc.
   */
  public ImportDuplicateEntryException(final DossierAuthorityKey key) {
    super(Reason.IMPORT_DUPLICATE_ENTRY);
    this.key = key;
  }

  public DossierAuthorityKey getKey() {
    return key;
  }

  public void setKey(final DossierAuthorityKey key) {
    this.key = key;
  }

}
