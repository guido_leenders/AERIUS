/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
@RemoteServiceRelativePath(ServiceURLConstants.MELDING_SERVICE_PATH)
public interface MeldingService extends RemoteService {

  /**
   * Submit the current melding (with the scenario in the session) to register.
   * @param meldingInformation The information to submit along with the actual scenario.
   * @throws AeriusException In case of errors.
   */
  void submitMelding(MeldingInformation meldingInformation) throws AeriusException;

  /**
   * Delete the file from the server.
   * @param filename file to delete
   * @throws AeriusException In case of errors.
   */
  void deleteUploadedFile(String filename, String uuid) throws AeriusException;

  /**
   * Validates if a file that has been uploades exists.
   * @param filename file to delete
   * @throws AeriusException In case of errors.
   */
  Boolean validateUploadedFile(String filename, String uuid) throws AeriusException;

}
