/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service with CRUD methods for priority projects.
 */
@RemoteServiceRelativePath(ServiceURLConstants.REGISTER_PRIORITY_PROJECT_SERVICE_PATH)
public interface PriorityProjectService extends RemoteService {

  /**
   * Fetch the priority project for given key.
   * @param permitKey The prioriy project key to search for
   * @return the proper priority project
   * @throws AeriusException on error
   */
  PriorityProject fetchPriorityProject(PriorityProjectKey key) throws AeriusException;

  /**
   * Fetch the priority sub project for given key.
   * @param permitKey The prioriy sub project key to search for
   * @return the proper priority sub project
   * @throws AeriusException on error
   */
  PrioritySubProject fetchPrioritySubProject(PrioritySubProjectKey key) throws AeriusException;

  /**
   * Returns a list of priority projects.
   * @param offset the offset to fetch from (allows for pagination)
   * @param size the size to fetch (allows for pagination)
   * @param filter to limit results
   * @return list of priority projects
   * @throws AeriusException on error
   */
  ArrayList<PriorityProject> getPriorityProjects(int offset, int size, PriorityProjectFilter filter) throws AeriusException;

  /**
   * Update the PriorityProject with the given key.
   *
   * @param key Key of priority project to update
   * @param project new values (dossierMetaData.remarks and dossierMetaData.dossierId - probably)
   * @param lastModified Time this was last modified
   * @throws AeriusException on error.
   */
  void updatePriorityProject(PriorityProjectKey key, PriorityProject project, long lastModified) throws AeriusException;

  /**
   * Update whether the assignment is complete for the PriorityProject with the given key.
   *
   * @param key Key of the priority project to update
   * @param assignComplete Whether the assign is completed
   * @param lastModified Time this was last modified
   * @throws AeriusException on error.
   */
  void updatePriorityProjectAssignComplete(PriorityProjectKey key, boolean assignComplete, long lastModified) throws AeriusException;

  /**
   * Delete the PriorityProject with the given key.
   *
   * @param key Key of priority project to delete
   */
  void deletePriorityProject(PriorityProjectKey key) throws AeriusException;

  /**
   * Delete the actualisation request file for the PriorityProject with the given key.
   *
   * @param key Key of the priority project to delete actualisation file of
   * @param lastModified Time the priority project was last modified.
   * @throws AeriusException
   */
  void deletePriorityProjectActualisationFile(PriorityProjectKey key, long lastModified) throws AeriusException;


  /**
   * Fetches the review info for PrioritySubProject with the given key. It's basically a list of table
   * rows for each relevant assessment area.
   */
  ArrayList<PrioritySubProjectReviewInfo> getPrioritySubProjectReviewInfo(PrioritySubProjectKey key) throws AeriusException;

  /**
   * Change the PrioritySubProject state with the given key.
   *
   * @param key Key of priority subproject to update the state
   * @param state RequestState new state to update to
   * @param lastModified Time this was last modified
   * @throws AeriusException on error.
   */
  void changeStatePrioritySubProject(PrioritySubProjectKey key, RequestState state, long lastModified) throws AeriusException;

  /**
   * Delete the PrioritySubProject and remove assigned space
   * @param key Key of priority subproject to delete
   */
  void deletePrioritySubProject(PrioritySubProjectKey key) throws AeriusException;

  /**
   * Reject the PrioritySubProject (change state to rejected then delete).
   * @param key Key of priority subproject to reject
   * @param lastModified the timestamp of the priority subproject's last modification.
   */
  void rejectPrioritySubProject(PrioritySubProjectKey key, long lastModified) throws AeriusException;

}
