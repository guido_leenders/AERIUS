/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Output class for an ImportWorker.
 */
public class ImportOutput implements Serializable {

  private static final long serialVersionUID = -496668921854124796L;

  private CalculatedScenario calculatedScenario;
  private int importedYear;
  private Integer importedTemporaryPeriod;
  private PermitCalculationRadiusType permitCalculationRadiusType;
  private String version;
  private String databaseVersion;
  private ImportType type;
  private int srid;
  private ArrayList<AeriusException> exceptions = new ArrayList<>();
  private ArrayList<AeriusException> warnings = new ArrayList<>();

  public CalculatedScenario getCalculatedScenario() {
    return calculatedScenario;
  }

  public void setCalculatedScenario(final CalculatedScenario calculatedScenario) {
    this.calculatedScenario = calculatedScenario;
  }

  public int getImportedYear() {
    return importedYear;
  }

  public void setImportedYear(final int importedYear) {
    this.importedYear = importedYear;
  }

  public Integer getImportedTemporaryPeriod() {
    return importedTemporaryPeriod;
  }

  public void setImportedTemporaryPeriod(final Integer importedTemporaryPeriod) {
    this.importedTemporaryPeriod = importedTemporaryPeriod;
  }

  public PermitCalculationRadiusType getPermitCalculationRadiusType() {
    return permitCalculationRadiusType;
  }

  public void setPermitCalculationRadiusType(final PermitCalculationRadiusType permitCalculationRadiusType) {
    this.permitCalculationRadiusType = permitCalculationRadiusType;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getDatabaseVersion() {
    return databaseVersion;
  }

  public void setDatabaseVersion(final String databaseVersion) {
    this.databaseVersion = databaseVersion;
  }

  public ArrayList<AeriusException> getExceptions() {
    return exceptions;
  }

  public ArrayList<AeriusException> getWarnings() {
    return warnings;
  }

  public boolean isSuccess() {
    return exceptions.isEmpty();
  }

  public int getSrid() {
    return srid;
  }

  public void setSrid(final int srid) {
    this.srid = srid;
  }

  public ImportType getType() {
    return type;
  }

  public void setType(final ImportType type) {
    this.type = type;
  }
}
