/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.NoticeKey;
import nl.overheid.aerius.shared.domain.register.NoticeMapData;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service with CRUD methods for notices.
 */
@RemoteServiceRelativePath(ServiceURLConstants.REGISTER_NOTICE_SERVICE_PATH)
public interface NoticeService extends RemoteService {

  /**
   * Returns a notice with all information and audit trail.
   * @param key Key of the notice to fetch
   * @return A notice object
   * @throws AeriusException on error
   */
  Notice fetchNoticeDetails(NoticeKey key) throws AeriusException;

  /**
   * Returns a list of notices.
   * @param offset the offset to fetch from (allows for pagination)
   * @param size the size to fetch (allows for pagination)
   * @param filter to limit results
   * @return list of notices
   * @throws AeriusException on error
   */
  ArrayList<Notice> getNotices(int offset, int size, NoticeFilter filter) throws AeriusException;

  /**
   * Return map data (containing notices / permits) in a radius of given notice id.
   * @param id The id to fetch data in radius for.
   * @return map data
   * @throws AeriusException on error
   */
  NoticeMapData getNoticesMapData(int id) throws AeriusException;

  /**
   * Get a list of notice IDs that can be confirmed in one batch.
   * @return noticeIds The ids to confirm.
   * @throws AeriusException on error
   */
  ArrayList<Integer> getConfirmableNoticeIds() throws AeriusException;

  /**
   * Confirm a list of notices.
   * @param noticeIds The ids to confirm.
   * @throws AeriusException on error
   */
  void confirmNotices(ArrayList<Integer> noticeIds) throws AeriusException;

  /**
   * Get a list of rows usable for the NoticeDashboard.
   * @param filter The filter to use when retrieving (no permit) Notice Dashboard rows.
   * @return The list of NoticeDashboardRows which match the filter.
   * @throws AeriusException on error.
   */
  ArrayList<DefaultDashboardRow> getDashboardNoticeRows(final NoticeDashboardFilter filter) throws AeriusException;

  /**
   * Delete a notices.
   * @param noticeId The ids of the notice to delete.
   * @throws AeriusException on error
   */
  void deleteNotice(int noticeId) throws AeriusException;

  /**
   * Returns the ScenarioGMLs for the supplied permit.
   * @param noticeKey Key of the notice to get the gml(s) for.
   * @return ScenarioGMLs for permit.
   * @throws AeriusException on error
   */
  ScenarioGMLs getGML(NoticeKey noticeKey) throws AeriusException;

}
