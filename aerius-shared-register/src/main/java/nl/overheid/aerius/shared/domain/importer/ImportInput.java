/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import java.io.Serializable;
import java.util.Locale;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;

/**
 * Input class for an ImportWorker.
 */
public class ImportInput implements Serializable {

  private static final long serialVersionUID = -5759233524961025889L;

  private final String fileName;
  private final ImportType fileType;
  private final byte[] fileContent;

  private Locale locale;
  private Substance substance;
  private CalculationSetOptions options;

  private boolean validateMetadata;
  private boolean returnSources = true;
  private boolean persistResults;
  private boolean validate = true;
  private boolean validateAgainstSchema = true;
  private boolean createGeolayers;

  private boolean checkLimits;

  public ImportInput(final String fileName, final ImportType fileType, final byte[] fileContent) {
    this.fileName = fileName;
    this.fileType = fileType;
    this.fileContent = fileContent;
  }

  public String getFileName() {
    return fileName;
  }

  public ImportType getFileType() {
    return fileType;
  }

  public byte[] getFileContent() {
    return fileContent;
  }

  public Substance getSubstance() {
    return substance;
  }

  public void setSubstance(final Substance substance) {
    this.substance = substance;
  }

  public CalculationSetOptions getOptions() {
    return options;
  }

  public void setOptions(final CalculationSetOptions options) {
    this.options = options;
  }

  public boolean isValidateMetadata() {
    return validateMetadata;
  }

  public void setValidateMetadata(final boolean validateMetadata) {
    this.validateMetadata = validateMetadata;
  }

  public boolean isReturnSources() {
    return returnSources;
  }

  public void setReturnSources(final boolean returnSources) {
    this.returnSources = returnSources;
  }

  public boolean isValidate() {
    return validate;
  }

  public void setValidate(final boolean validate) {
    this.validate = validate;
  }

  public boolean isCheckLimits() {
    return checkLimits;
  }

  public void setCheckLimits(final boolean checkLimits) {
    this.checkLimits = checkLimits;
  }

  public boolean isValidateAgainstSchema() {
    return validateAgainstSchema;
  }

  public void setValidateAgainstSchema(final boolean validateAgainstSchema) {
    this.validateAgainstSchema = validateAgainstSchema;
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(final Locale locale) {
    this.locale = locale;
  }

  public boolean isCreateGeolayers() {
    return createGeolayers;
  }

  public void setCreateGeolayers(final boolean createGeolayers) {
    this.createGeolayers = createGeolayers;
  }

  public boolean isPersistResults() {
    return persistResults;
  }

  public void setPersistResults(final boolean persistResults) {
    this.persistResults = persistResults;
  }

  @Override
  public String toString() {
    return "ImportInputData [fileName=" + fileName + ", fileType=" + fileType + "]";
  }

}
