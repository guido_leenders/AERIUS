/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.Locale;

/**
 *
 */
public enum RequestFileType {

  /**
   * The application file: The inital file used to describe situations for a request.
   */
  APPLICATION,

  /**
   * The decree file: The file used to explain on which grounds a request has been assigned or rejected.
   */
  DECREE,

  /**
   * The detail decree file: The file containing the detail pages for the decree (appendix for the appendix).
   */
  DETAIL_DECREE,

  /**
   * The reservation file: Explains the basis of the reserved space for a priority project.
   */
  PRIORITY_PROJECT_RESERVATION,

  /**
   * The actualisation file: Supposed to replace the reservation-file of a priority project when the new
   * monitoring period begins.
   */
  PRIORITY_PROJECT_ACTUALISATION,

  /**
   * The factsheet file: Explains why the request is and should be a priority project.
   */
  PRIORITY_PROJECT_FACTSHEET;

  /**
   * Safely returns an RequestFileType. It is case independent and returns null in
   * case the input was null or the RequestFileType could not be found.
   *
   * @param value value to convert
   * @return RequestFileType or null if no valid input
   */
  public static RequestFileType safeValueOf(final String value) {
    try {
      return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }

}
