/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register.dashboard;


/**
 * A row in the table of information about a certain type of deposition space on the dashboard.
 * Each row has information for an assessment area. In the default case the percentage
 * of the space that has been used (assigned) in that area.
 * Currently all dashboards use this.
 */
public class DefaultDashboardRow extends AbstractDashboardRow {

  private static final long serialVersionUID = 4599884455572815709L;

  private double assignedFactor;
  private double pendingFactor;
  private boolean thresholdChanged;

  public double getAssignedFactor() {
    return assignedFactor;
  }

  public void setAssignedFactor(final double assignedFactor) {
    this.assignedFactor = assignedFactor;
  }

  public boolean isThresholdChanged() {
    return thresholdChanged;
  }

  public void setThresholdChanged(final boolean thresholdChanged) {
    this.thresholdChanged = thresholdChanged;
  }

  public double getPendingFactor() {
    return pendingFactor;
  }

  public void setPendingFactor(final double pendingFactor) {
    this.pendingFactor = pendingFactor;
  }

}
