/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;


/**
 * Used to identify notice (from the database), using a user-friendly string (instead of an id).
 */
public class NoticeKey extends ReferenceKey {

  private static final long serialVersionUID = 5500449038847810262L;

  @SuppressWarnings("unused")
  private NoticeKey() { /* GWT-needed constructor */
  }

  public NoticeKey(final String reference) {
    super(reference);
  }

}
