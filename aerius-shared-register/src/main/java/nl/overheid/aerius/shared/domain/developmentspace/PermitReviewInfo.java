/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.developmentspace;

/**
 * Holds some numbers that support the review info for permits.
 */
public class PermitReviewInfo extends BaseReviewInfo {

  private static final long serialVersionUID = -2136930698667275030L;

  private boolean onlyExceeding;
  private double requiredSpaceForPermit;
  private double availableExclusive;

  public boolean isOnlyExceeding() {
    return onlyExceeding;
  }

  public void setOnlyExceeding(final boolean onlyExceeding) {
    this.onlyExceeding = onlyExceeding;
  }

  public double getRequiredSpaceForPermit() {
    return requiredSpaceForPermit;
  }

  public void setRequiredSpaceForPermit(final double requiredSpaceForPermit) {
    this.requiredSpaceForPermit = requiredSpaceForPermit;
  }

  public double getAvailableExclusive() {
    return availableExclusive;
  }

  public void setAvailableExclusive(final double availableExclusive) {
    this.availableExclusive = availableExclusive;
  }

}
