/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.io.Serializable;

/**
 * Used to identify (database) objects using a combination of dossier id string and authority code (instead of an id).
 */
public class DossierAuthorityKey implements Serializable {

  private static final long serialVersionUID = -403614814243911818L;

  private String dossierId;
  private String authorityCode;

  protected DossierAuthorityKey() { /* GWT-needed constructor */
  }

  public DossierAuthorityKey(final String dossierId, final String authorityCode) {
    this.dossierId = dossierId;
    this.authorityCode = authorityCode;
  }

  public String getDossierId() {
    return dossierId;
  }

  public void setDossierId(final String dossierId) {
    this.dossierId = dossierId;
  }

  public String getAuthorityCode() {
    return authorityCode;
  }

  public void setAuthorityCode(final String authorityCode) {
    this.authorityCode = authorityCode;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((authorityCode == null) ? 0 : authorityCode.hashCode());
    result = prime * result + ((dossierId == null) ? 0 : dossierId.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    boolean equals = false;
    if (obj != null && this.getClass() == obj.getClass()) {
      final DossierAuthorityKey other = (DossierAuthorityKey) obj;
      equals = ((this.authorityCode != null && this.authorityCode.equals(other.authorityCode))
          || (this.authorityCode == null && other.authorityCode == null))
          && ((this.dossierId != null && this.dossierId.equals(other.dossierId))
          || (this.dossierId == null && other.dossierId == null));
    }
    return equals;
  }

  @Override
  public String toString() {
    return "DossierAuthorityKey [dossierId=" + dossierId + ", authorityCode=" + authorityCode + "]";
  }

}
