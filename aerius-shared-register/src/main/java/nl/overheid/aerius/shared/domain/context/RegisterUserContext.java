/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;

/**
 *
 */
public class RegisterUserContext extends AdminUserContext {

  private static final long serialVersionUID = 6790901584162291969L;

  private LayerProps baseLayer;

  private ArrayList<LayerProps> layers;

  private EmissionValueKey emissionValueKey;

  private LinkedHashMap<Authority, ArrayList<UserProfile>> handlers;

  //Can't use a HashMap<Class, Filter> here because GWT can't serialize Class (private constructor)
  private HashSet<Filter> filters = new HashSet<>();

  private EmissionResultValueDisplaySettings emissionResultValueDisplaySettings;

  @Override
  public EmissionValueKey getEmissionValueKey() {
    return emissionValueKey;
  }

  @Override
  public LayerProps getBaseLayer() {
    return baseLayer;
  }

  public void setBaseLayer(final LayerProps baseLayer) {
    this.baseLayer = baseLayer;
  }

  @Override
  public ArrayList<LayerProps> getLayers() {
    return layers;
  }

  public void setLayers(final ArrayList<LayerProps> layers) {
    this.layers = layers;
  }

  public void setEmissionValueKey(final EmissionValueKey emissionValueKey) {
    this.emissionValueKey = emissionValueKey;
  }

  public void setHandlers(final LinkedHashMap<Authority, ArrayList<UserProfile>> handlers) {
    this.handlers = handlers;
  }

  public LinkedHashMap<Authority, ArrayList<UserProfile>> getHandlers() {
    return handlers;
  }

  /**
   * @param filter The filter to add to the user context. Will replace any old one.
   */
  public void useFilter(final Filter filter) {
    // Remove the old one, the filters are not required to overwrite equals method.
    filters.remove(getFilter(filter.getClass()));
    filters.add(filter);
  }

  /**
   * @param filterClass The filter class to get from the usercontext.
   * @param <T> The type of the filter.
   * @return The correct filter or null if not found.
   */
  @Override
  @SuppressWarnings("unchecked")
  public <T extends Filter> T getFilter(final Class<T> filterClass) {
    T rightFilter = null;
    for (final Filter filter : filters) {
      if (filter.getClass().equals(filterClass)) {
        rightFilter = (T) filter;
      }
    }
    return rightFilter;
  }

  /**
   * Private setter, which is ridiculous, but here to make absolutely sure the filters field will not be made final.
   *
   * @param filters Filters you can't set.
   */
  @SuppressWarnings("unused")
  private void setFilters(final HashSet<Filter> filters) {
    this.filters = filters;
  }

  @Override
  public EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings() {
    return emissionResultValueDisplaySettings;
  }

  public void setEmissionResultValueDisplaySettings(final EmissionResultValueDisplaySettings emissionResultValueDisplaySettings) {
    this.emissionResultValueDisplaySettings = emissionResultValueDisplaySettings;
  }
}
