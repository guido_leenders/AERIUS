/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.search;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.register.Request;

/**
 * POJO containing a Search suggestion to be used in the search widget.
 */
public class RegisterSearchSuggestion extends SearchSuggestion<RegisterSearchSuggestionType, RegisterSearchSuggestion> implements Serializable {
  private static final long serialVersionUID = 7159504301022125284L;

  private Request request;

  /**
   * GWT Constructor.
   */
  public RegisterSearchSuggestion() {
    this(0, null, null);
  }

  public RegisterSearchSuggestion(final int id, final String name, final RegisterSearchSuggestionType type) {
    super(id, name, type);
  }

  public Request getRequest() {
    return request;
  }

  public void setRequest(final Request request) {
    this.request = request;
  }
}
