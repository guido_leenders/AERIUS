/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

/**
 * Contains various values corresponding with the type of change done.
 */
public enum AuditTrailType {
  /** State field has changed. */
  STATE,
  /** Dossier ID field has changed. */
  DOSSIER_ID,
  /** AERIUS ID field has been set. */
  AERIUS_ID,
  /** The handler has been changed. */
  HANDLER,
  /** The date received has been changed. */
  DATE_RECEIVED,
  /** The request is deleted. */
  DELETE,
  /** The melding is replaced (new value = reference of replacement) - while this option is never to be used it's kept for older audit trails.
   *  !! DO NOT REMOVE !! */
  REPLACE,
  /** The melding is confirmed */
  CONFIRM,
  /** The priority project is done with assigning space */
  ASSIGN_COMPLETED,
  /** The factsheet of the priority project has changed. */
  FACTSHEET,
  /** The actualisation of the priority project has changed */
  ACTUALISATION,
  /** The actualisation of the priority project is deleted. */
  ACTUALISATION_DELETE,
  /** The segment has been set. */
  SEGMENT;

}
