/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.importer;

import nl.overheid.aerius.shared.domain.FileFormat;

/**
 * Keep tracks of the formats supported by the various upload services.
 */
public enum SupportedUploadFormat {
  PERMIT(ImportType.PAA),
  PRIORITY_PROJECT(ImportType.GML, ImportType.ZIP),
  PRIORITY_PROJECT_FACTSHEET(ImportType.PAA), // technically not a PAA, should we change this?
  PRIORITY_PROJECT_ACTUALISATION(ImportType.GML, ImportType.ZIP),
  PRIORITY_PROJECT_SUBPROJECT(ImportType.PAA, ImportType.GML, ImportType.ZIP);

  private ImportType[] supported;

  SupportedUploadFormat(final ImportType ... supported) {
    this.supported = supported;
  }

  public ImportType[] getSupported() {
    return supported;
  }

  public static boolean isSupported(final SupportedUploadFormat supportedEntry, final ImportType check) {
    return isSupported(supportedEntry, check == null ? null : check.getFileFormat());
  }

  public static boolean isSupported(final SupportedUploadFormat supportedEntry, final FileFormat check) {
    boolean isSupported = supportedEntry.getSupported().length == 0;

    for (final ImportType supportedType : supportedEntry.getSupported()) {
      if (supportedType.getFileFormat() == check) {
        isSupported = true;
        break;
      }
    }

    return isSupported;
  }
}
