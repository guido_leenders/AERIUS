/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.melding;

import java.io.Serializable;
import java.util.ArrayList;

public class MeldingInformation implements Serializable {

  private static final long serialVersionUID = -7161539914442296286L;

  private String uuid;
  private String organizationOin;
  private BeneficiaryInformation beneficiaryInformation = new BeneficiaryInformation();

  private boolean benefactorCustom;
  private OrganisationInformation benefactorInformation = new OrganisationInformation();
  private boolean benefactorEmailConfirmation;
  private ArrayList<String> authorizationFiles;

  private boolean executorCustom;
  private OrganisationInformation executorInformation = new OrganisationInformation();
  private boolean executorEmailConfirmation;

  private boolean existingPermitPresent;
  private String existingPermitReference;
  private boolean existingSourcePermitPresent;
  private ArrayList<String> substantiationFiles;

  private boolean prevMeldingPresent;
  private String prevMeldingReference;

  /**
   * Onderbouwing referentiesituatie
   */
  private String substantiation;

  private String locale;

  /**
   * @return the uuid
   */
  public String getUUID() {
    return uuid;
  }

  public void setUUID(final String uuid) {
    this.uuid = uuid;
  }

  public BeneficiaryInformation getBeneficiaryInformation() {
    return beneficiaryInformation;
  }

  public void setBeneficiaryInformation(final BeneficiaryInformation beneficiaryInformation) {
    this.beneficiaryInformation = beneficiaryInformation;
  }

  public boolean isBenefactorCustom() {
    return benefactorCustom;
  }

  public void setBenefactorCustom(final boolean benefactorCustom) {
    this.benefactorCustom = benefactorCustom;
  }

  public OrganisationInformation getBenefactorInformation() {
    return benefactorInformation;
  }

  public void setBenefactorInformation(final OrganisationInformation benefactorInformation) {
    this.benefactorInformation = benefactorInformation;
  }

  public ArrayList<String> getAuthorizationFiles() {
    return authorizationFiles;
  }

  public void setAuthorizationFiles(final ArrayList<String> authorizationFiles) {
    this.authorizationFiles = authorizationFiles;
  }

  public OrganisationInformation getExecutorInformation() {
    return executorInformation;
  }

  public boolean isExecutorCustom() {
    return executorCustom;
  }

  public void setExecutorInformation(final OrganisationInformation executorInformation) {
    this.executorInformation = executorInformation;
  }


  public void setExecutorCustom(final boolean executorCustom) {
    this.executorCustom = executorCustom;
  }

  public boolean isBenefactorEmailConfirmation() {
    return benefactorEmailConfirmation;
  }

  public void setBenefactorEmailConfirmation(final boolean benefactorEmailConfirmation) {
    this.benefactorEmailConfirmation = benefactorEmailConfirmation;
  }

  public boolean isExecutorEmailConfirmation() {
    return executorEmailConfirmation;
  }

  public void setExecutorEmailConfirmation(final boolean executorEmailConfirmation) {
    this.executorEmailConfirmation = executorEmailConfirmation;
  }

  public boolean isExistingPermitPresent() {
    return existingPermitPresent;
  }

  public void setExistingPermitPresent(final boolean existingPermitPresent) {
    this.existingPermitPresent = existingPermitPresent;
  }

  public boolean isExistingSourcePermitPresent() {
    return existingSourcePermitPresent;
  }

  public void setExistingSourcePermitPresent(final boolean existingSourcePermitPresent) {
    this.existingSourcePermitPresent = existingSourcePermitPresent;
  }

  public String getExistingPermitReference() {
    return existingPermitReference;
  }

  public void setExistingPermitReference(final String existingPermitReference) {
    this.existingPermitReference = existingPermitReference;
  }

  public ArrayList<String> getSubstantiationFiles() {
    return substantiationFiles;
  }

  public void setSubstantiationFiles(final ArrayList<String> substantiationFiles) {
    this.substantiationFiles = substantiationFiles;
  }

  public boolean isPreviousMeldingPresent() {
    return prevMeldingPresent;
  }

  public void setPreviousMeldingPresent(final boolean prevMeldingPresent) {
    this.prevMeldingPresent = prevMeldingPresent;
  }

  public String getPreviousMeldingReference() {
    return prevMeldingReference;
  }

  public void setPreviousMeldingReference(final String prevMeldingReference) {
    this.prevMeldingReference = prevMeldingReference;
  }

  public String getSubstantiation() {
    return substantiation;
  }

  public void setSubstantiation(final String substantiation) {
    this.substantiation = substantiation;
  }

  public String getOrganizationOin() {
    return organizationOin;
  }

  public void setOrganizationOin(final String organizationOin) {
    this.organizationOin = organizationOin;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(final String locale) {
    this.locale = locale;
  }

  @Override
  public String toString() {
    return "MeldingInformation [benefactorInformation=" + benefactorInformation + ", beneficiaryInformation="
        + beneficiaryInformation + ", executorInformation=" + executorInformation + ", benefactorCustom="
        + benefactorCustom + ", executorCustom=" + executorCustom + ", beneficiaryEmailConfirmation="
        + benefactorEmailConfirmation + ", executorEmailConfirmation=" + executorEmailConfirmation
        + ", existingPermitPresent=" + existingPermitPresent + ", existingSourcePermitPresent=" + existingSourcePermitPresent
        + ", existingPermitReference=" + existingPermitReference + ", prevMeldingPresent=" + prevMeldingPresent
        + ", prevMeldingReference=" + prevMeldingReference + ", substantiation=" + substantiation
        + ", organizationOin=" + organizationOin + "]";
  }

}
