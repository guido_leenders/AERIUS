/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.SortableAttribute;
import nl.overheid.aerius.shared.domain.SortableDirection;

/**
 * Base interface for Register specific filters.
 *
 * @param <S> The type of sortable attributes.
 */
public abstract class RegisterFilter<S extends SortableAttribute> implements Filter {
  private static final long serialVersionUID = -6272068354801259541L;

  private S sortAttribute;
  private SortableDirection sortDirection;

  public S getSortAttribute() {
    return sortAttribute;
  }

  public void setSortAttribute(final S sortAttribute) {
    this.sortAttribute = sortAttribute;
  }

  public SortableDirection getSortDirection() {
    return sortDirection;
  }

  public void setSortDirection(final SortableDirection sortDirection) {
    this.sortDirection = sortDirection;
  }

}
