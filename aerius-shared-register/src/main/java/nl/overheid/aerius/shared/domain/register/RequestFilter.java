/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.Date;
import java.util.HashSet;

import nl.overheid.aerius.shared.domain.SortableAttribute;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 * Base interface for Register specific filters.
 *
 * @param <S> The type of sortable attributes.
 */
public abstract class RequestFilter<S extends SortableAttribute> extends RegisterFilter<S> {

  private static final long serialVersionUID = 2787643621079801419L;

  private Date from;
  private Date till;
  private Boolean marked;

  private HashSet<Province> provinces = new HashSet<>();
  private HashSet<Authority> authorities = new HashSet<>();
  private HashSet<Sector> sectors = new HashSet<>();
  private HashSet<AssessmentArea> assessmentAreas = new HashSet<>();

  public Date getFrom() {
    return from;
  }

  public void setFrom(final Date from) {
    this.from = from;
  }

  public Date getTill() {
    return till;
  }

  public void setTill(final Date till) {
    this.till = till;
  }

  public HashSet<Province> getProvinces() {
    return provinces;
  }

  public void setProvinces(final HashSet<Province> provinces) {
    this.provinces = provinces;
  }

  public HashSet<Authority> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(final HashSet<Authority> authorities) {
    this.authorities = authorities;
  }

  public HashSet<Sector> getSectors() {
    return sectors;
  }

  public void setSectors(final HashSet<Sector> sectors) {
    this.sectors = sectors;
  }

  public HashSet<AssessmentArea> getAssessmentAreas() {
    return assessmentAreas;
  }

  public void setAssessmentAreas(final HashSet<AssessmentArea> assessmentAreas) {
    this.assessmentAreas = assessmentAreas;
  }

  public Boolean getMarked() {
    return marked;
  }

  public void setMarked(final Boolean marked) {
    this.marked = marked;
  }

  @Override
  public void fillDefault() {
    from = null;
    till = null;
    provinces.clear();
    authorities.clear();
    sectors.clear();
    assessmentAreas.clear();
  }

  @Override
  public String toString() {
    return "RequestFilter [from=" + from + ", till=" + till + ", marked=" + marked + ", provinces=" + provinces + ", authorities="
        + authorities + ", sectors=" + sectors + ", assessmentAreas=" + assessmentAreas + "]";
  }

}
