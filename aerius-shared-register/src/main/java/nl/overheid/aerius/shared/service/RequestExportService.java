/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestExportResult;
import nl.overheid.aerius.shared.domain.register.RequestFilter;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service with CRUD methods for Request Exports.
 */
@RemoteServiceRelativePath(ServiceURLConstants.REGISTER_REQUEST_EXPORT_SERVICE_PATH)
public interface RequestExportService extends RemoteService {

  /**
   * Prepared the export for download. Will save the filter and return an object which will allow
   *  the user to download the export matching the filter. The result will also contain the amount of files
   *  and the total uncompressed file size to be exported if the download is triggered.
   * Note though that the amount of files/uncompressed file size can change if between preparing and
   *  actually downloading
   * @param filter to limit results
   * @return
   * @throws AeriusException on error
   */
  RequestExportResult prepare(RequestFilter<?> filter) throws AeriusException;

  /**
   * Start an export for the 'toetsing' part of a priority sub project.
   * @param prioritySubProjectKey The key of the subproject to export.
   * @return The string to use to poll if the export is ready and to retrieve the result.
   * @throws AeriusException In case of exceptions.
   */
  String startSubPriorityExport(PrioritySubProjectKey prioritySubProjectKey) throws AeriusException;

  /**
   * Start an export for a priority project.
   * @param priorityProjectExportData The data required for the priority project export.
   * @return The string to use to poll if the export is ready and to retrieve the result.
   * @throws AeriusException In case of exceptions.
   */
  String startPriorityExport(PriorityProjectExportData priorityProjectExportData) throws AeriusException;

  /**
   * Method to use to check if an export is ready or not.
   * @param exportId The ID returned by {@link #startSubPriorityExport(PrioritySubProjectKey)}.
   * @return RequestExportResult if the export is ready to be received, null if not.
   * @throws AeriusException In case of errors.
   */
  RequestExportResult isExportReady(String exportId) throws AeriusException;

}
