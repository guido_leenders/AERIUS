/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Contains all the states a request could have had, past and present.
 * For extra documentation containing the present states see {@link RequestState}.
 */
public enum AuditTrailRequestState {
  INITIAL,
  QUEUED,
  PENDING_WITH_SPACE,
  PENDING_WITHOUT_SPACE,
  ASSIGNED,
  REJECTED_WITH_SPACE,
  REJECTED_WITHOUT_SPACE,
  ASSIGNED_FINAL,
  REJECTED_FINAL;

  public static final HashSet<AuditTrailRequestState> REMOVED_STATES = new HashSet<>(Arrays.asList(REJECTED_WITH_SPACE));

  /**
   * Safely returns a requestState. It is case independent and returns null in
   * case the input was null or the RequestState could not be found.
   *
   * @param value value to convert
   * @return requestState or null if no valid input
   */
  public static AuditTrailRequestState safeValueOf(final String value) {
    if (value == null) {
      return null;
    }
    try {
      return valueOf(value.toUpperCase());
    } catch (final Exception e) {
      return null;
    }
  }

  public String getDbValue() {
    return name().toLowerCase();
  }

}
