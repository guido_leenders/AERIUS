/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

import nl.overheid.aerius.shared.domain.Option;
import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Province;

/**
 *
 */
public class RegisterContext extends AdminContext implements Serializable {
  private static final long serialVersionUID = 5017526479113398722L;

  private ArrayList<AssessmentArea> natura2kAreas;

  private HashSet<Province> provinces;

  private ArrayList<Option<Double>> minimalDepositionSpaceOptions;

  private Integer maxRadiusForRequests;

  public ArrayList<AssessmentArea> getNatura2kAreas() {
    return natura2kAreas;
  }

  public void setNatura2kAreas(final ArrayList<AssessmentArea> natura2kAreas) {
    this.natura2kAreas = natura2kAreas;
  }

  public HashSet<Province> getProvinces() {
    return provinces;
  }

  public void setProvinces(final HashSet<Province> provinces) {
    this.provinces = provinces;
  }

  public ArrayList<Option<Double>> getMinimalDepositionSpaceOptions() {
    return minimalDepositionSpaceOptions;
  }

  public void setMinimalDepositionSpaceOptions(final ArrayList<Option<Double>> minimalDepositionSpaceOptions) {
    this.minimalDepositionSpaceOptions = minimalDepositionSpaceOptions;
  }

  @Override
  public Permission[] getPermissions() {
    return RegisterPermission.values();
  }

  public Integer getMaxRadiusForRequests() {
    return maxRadiusForRequests;
  }

  public void setMaxRadiusForRequests(final Integer maxRadiusForRequests) {
    this.maxRadiusForRequests = maxRadiusForRequests;
  }


}
