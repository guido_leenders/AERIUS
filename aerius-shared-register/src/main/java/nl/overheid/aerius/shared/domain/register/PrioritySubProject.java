/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.Date;

/**
 * Class to store a priority subproject and related data.
 */
public class PrioritySubProject extends Request {

  private static final long serialVersionUID = 6221511207784689947L;

  private Date receivedDate;
  private PriorityProjectKey parentKey;
  private String label;

  public PrioritySubProject() {
    super(SegmentType.PRIORITY_SUBPROJECTS);
  }

  public PrioritySubProjectKey getKey() {
    return new PrioritySubProjectKey(parentKey, getReference());
  }

  public Date getReceivedDate() {
    return receivedDate;
  }

  public void setReceivedDate(final Date receivedDate) {
    this.receivedDate = receivedDate;
  }

  public PriorityProjectKey getParentKey() {
    return parentKey;
  }

  public void setParentKey(final PriorityProjectKey parentKey) {
    this.parentKey = parentKey;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return "PrioritySubProject [parentKey=" + parentKey + ", label=" + label + "] " + super.toString();
  }

}
