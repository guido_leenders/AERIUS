/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import java.util.HashMap;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;

/**
 * Class to store a permit and related data.
 */
public class Permit extends Request {

  private static final long serialVersionUID = -9099602368869202168L;

  private DossierMetaData dossierMetaData;

  private HashMap<AssessmentArea, DevelopmentRuleResultList> developmentRules = new HashMap<>();

  private boolean potentiallyRejectable;

  public Permit() {
    super(SegmentType.PROJECTS);
  }

  /**
   * @return The key for this permit.
   */
  public PermitKey getPermitKey() {
    return dossierMetaData.getHandler() == null ? null
        : new PermitKey(dossierMetaData.getDossierId(), dossierMetaData.getHandler().getAuthority().getCode());
  }

  public DossierMetaData getDossierMetaData() {
    return dossierMetaData;
  }

  public void setDossierMetaData(final DossierMetaData dossierMetaData) {
    this.dossierMetaData = dossierMetaData;
  }

  public HashMap<AssessmentArea, DevelopmentRuleResultList> getDevelopmentRules() {
    return developmentRules;
  }

  public void setDevelopmentRules(final HashMap<AssessmentArea, DevelopmentRuleResultList> developmentRules) {
    this.developmentRules = developmentRules;
  }

  public boolean isPotentiallyRejectable() {
    return potentiallyRejectable;
  }

  public void setPotentiallyRejectable(final boolean potentiallyRejectable) {
    this.potentiallyRejectable = potentiallyRejectable;
  }

  @Override
  public String toString() {
    return "Permit [dossierMetaData=" + dossierMetaData + ", developmentRules=" + developmentRules + ", potentiallyRejectable="
        + potentiallyRejectable + "] " + super.toString();
  }

}
