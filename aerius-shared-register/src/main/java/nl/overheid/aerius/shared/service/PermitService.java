/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitFilter;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.PermitSummary;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service with CRUD methods for permits.
 */
@RemoteServiceRelativePath(ServiceURLConstants.REGISTER_PERMIT_SERVICE_PATH)
public interface PermitService extends RemoteService {

  /**
   * Delete the permit.
   * @param permitKey key of permit to delete
   * @throws AeriusException on error
   */
  void delete(PermitKey permitKey) throws AeriusException;

  /**
   * Fetch the permit for given key.
   * @param permitKey The permit key to search for
   * @return permit
   * @throws AeriusException on error
   */
  Permit fetchPermit(PermitKey permitKey) throws AeriusException;

  /**
   * Update the given dossier with the given metaData.
   * @param permitKey The key of the permit to update
   * @param metaData The metaData to update
   * @param lastModified the timestamp of the permit's last modification.
   * @throws AeriusException on error
   */
  void update(PermitKey permitKey, DossierMetaData metaData, long lastModified) throws AeriusException;

  /**
   * Change the state of the given permit.
   * @param permitKey key of permit to change state of
   * @param state the new state
   * @param lastModified the timestamp of the permit's last modification.
   * @throws AeriusException on error
   */
  void changeState(PermitKey permitKey, RequestState state, long lastModified) throws AeriusException;

  /**
   * Reject the permit (change state to rejected then delete).
   * @param permitKey key of permit to reject
   * @param lastModified the timestamp of the permit's last modification.
   * @throws AeriusException on error
   */
  void reject(PermitKey permitKey, long lastModified) throws AeriusException;

  /**
   * (Un)mark permit.
   * @param permitKey key of permit to (un)mark
   * @param mark mark permit of true, unmark otherwise
   * @throws AeriusException on error
   */
  void updateMark(PermitKey permitKey, boolean mark) throws AeriusException;

  /**
   * Returns a list of permits.
   * @param offset start offset (to allow pagination)
   * @param size amount to fetch (to allow pagination)
   * @param filter to limit results
   * @return list of permits
   * @throws AeriusException on error
   */
  ArrayList<Permit> getPermits(int offset, int size, PermitFilter filter) throws AeriusException;

  /**
   * Returns the ScenarioGMLs for the supplied permit.
   * @param permitKey Key of the permit to get the gml(s) for.
   * @return ScenarioGMLs for permit.
   * @throws AeriusException on error
   */
  ScenarioGMLs getGML(PermitKey permitKey) throws AeriusException;

  /**
   * Check to see if the calculation(s) for an permit are finished. If the given permit is finished it's key is returned.
   * @param permitKey key of permit to determine the calculation state of
   * @return key if finished, null if not
   * @throws AeriusException on error
   */
  PermitKey isCalculationFinished(PermitKey permitKey) throws AeriusException;

  /**
   * Get a list of rows usable for the PermitDashboard.
   * @param filter The filter to use when retrieving Permit Dashboard rows.
   * @return The list of PermitDashboardRows which match the filter.
   * @throws AeriusException on error.
   */
  ArrayList<DefaultDashboardRow> getPermitDashboardRows(PermitDashboardFilter filter) throws AeriusException;

  /**
   * Get the summary for a permit, containing stuff like development rule results for assessment areas.
   * @param permitKey The key of the permit to get the summary for.
   * @return The PermitSummary for the permit.
   * @throws AeriusException on error
   */
  PermitSummary getPermitSummary(final PermitKey permitKey) throws AeriusException;
}
