/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register.dashboard;

/**
 * Filter for the dashboard: the no permit notices part.
 */
public class NoticeDashboardFilter extends AbstractDashboardFilter {

  private static final long serialVersionUID = 893385619900442129L;

  private static final double DEFAULT_MIN_FRACTION_USED = 0.70;

  private double minFractionUsed;

  public double getMinFractionUsed() {
    return minFractionUsed;
  }

  public void setMinFractionUsed(final double minFractionUsed) {
    this.minFractionUsed = minFractionUsed;
  }

  @Override
  public void fillDefault() {
    setProvinceId(null);
    minFractionUsed = DEFAULT_MIN_FRACTION_USED;
  }
}
