/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoticeConfirmationSettingTest {

  @Test
  public void testDefaultSetting() {
    assertEquals("default notice is auto confirm", NoticeConfirmationSetting.AUTO_CONFIRM, NoticeConfirmationSetting.safeValueOf(""));
    assertEquals("null input returns default", NoticeConfirmationSetting.AUTO_CONFIRM, NoticeConfirmationSetting.safeValueOf(null));
    assertEquals("upper case returns right one", NoticeConfirmationSetting.BATCH_CONFIRM, NoticeConfirmationSetting.safeValueOf("BATCH_CONFIRM"));
    assertEquals("lower case returns right one", NoticeConfirmationSetting.BATCH_CONFIRM, NoticeConfirmationSetting.safeValueOf("batch_confirm"));
    assertEquals("mixed case returns right one", NoticeConfirmationSetting.BATCH_CONFIRM, NoticeConfirmationSetting.safeValueOf("BaTcH_conFirm"));
  }
}
