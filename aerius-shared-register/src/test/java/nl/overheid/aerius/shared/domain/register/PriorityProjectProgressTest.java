/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.register;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for {@link PriorityProjectProgress}.
 */
public class PriorityProjectProgressTest {

  @Test
  public void testLimits() {
    assertTrue("FULL hasLower limit", PriorityProjectProgress.FULL.hasLowerLimit());
    assertFalse("FULL has no Higher limit", PriorityProjectProgress.FULL.hasHigherLimit());
    assertTrue("UNDER_HALF has Higher limit", PriorityProjectProgress.UNDER_HALF.hasHigherLimit());
    assertFalse("NO_RESERVATION_PRESENT has no Lower limit", PriorityProjectProgress.NO_RESERVATION_PRESENT.hasLowerLimit());
    assertFalse("NO_RESERVATION_PRESENT has no Higher limit", PriorityProjectProgress.NO_RESERVATION_PRESENT.hasHigherLimit());
  }

  @Test
  public void testDetermineProgress() {
    assertSame("null object", null, PriorityProjectProgress.determineProgress(null));
    assertSame("Actualisation pp", PriorityProjectProgress.NO_RESERVATION_PRESENT, createPPP(RequestState.INITIAL, true, 2.0));
    assertSame("Completed pp", PriorityProjectProgress.DONE, createPPP(RequestState.QUEUED, true, 2.0));
    assertSame("Full", PriorityProjectProgress.FULL, createPPP(RequestState.QUEUED, false, 2.0));
    assertSame("Full", PriorityProjectProgress.FULL, createPPP(RequestState.QUEUED, false, 1.0));
    assertSame("Over half", PriorityProjectProgress.OVER_HALF, createPPP(RequestState.QUEUED, false, 0.6));
    assertSame("Under half", PriorityProjectProgress.UNDER_HALF, createPPP(RequestState.QUEUED, false, 0.5));
    assertSame("Under half", PriorityProjectProgress.UNDER_HALF, createPPP(RequestState.QUEUED, false, 0.1));
    assertSame("None", PriorityProjectProgress.NONE, createPPP(RequestState.QUEUED, false, 0.0));
  }

  private PriorityProjectProgress createPPP(final RequestState rs, final boolean assignCompleted, final double fractionAssigned) {
    final PriorityProject pp = new PriorityProject();
    pp.setRequestState(rs);
    pp.setAssignCompleted(assignCompleted);
    pp.setFractionAssigned(fractionAssigned);
    return PriorityProjectProgress.determineProgress(pp);
  }
}
