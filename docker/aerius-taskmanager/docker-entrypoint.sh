#!/bin/sh

set -e

# replace placeholders in configuration and write it to proper place
envsubst < "${PROPERTIES_TEMPLATE_PATH}" > "${PROPERTIES_PATH}"

# execute arguments (default the docker CMD)
exec "$@"
