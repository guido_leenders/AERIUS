### Base for web application images using Jetty

Adds dependencies needed for (pooling) DB connections and handles the configuration of the database for the webapp in a clean way.
Webapps will be exposed on port 8080. Webapp will be hosted under `http://<container_ip>:8080/<appname>`.

Only for use in other images.

Dependencies needed to build this image - not present in source control:
- jetty-deps/commons-collections-3.2.2.jar
- jetty-deps/commons-dbcp-1.4.jar
- jetty-deps/commons-pool-1.5.4.jar

##### ENV variables for running images based on this
- `DATASOURCENAME`: Defaults to `AeriusDB`. The datasource as required by the webapp.
- `APPNAME`: [REQUIRED] Set in extending Dockerfile. Webapp will be deployed under this name. The webapp war should be named `<appname>.war`.
- `DBNAME`: Defaults to `aerius`. Override in extending Dockerfile advised.
- `DBHOSTNAME`: Defaults to `localhost`. Override advised on container run.
- `DBUSERNAME`: Defaults to `aerius`. Override advised on container run.
- `DBPASSWORD`: Defaults to `aerius`. Override advised on container run.

##### Example build
```shell
docker build -t aerius-wui-server:latest .
```
