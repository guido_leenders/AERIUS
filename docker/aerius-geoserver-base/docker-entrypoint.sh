#!/bin/sh

set -e

# This is required - without a proper passwd everything will crash
echo "${GEOSERVERPASSWD_ENCRYPTED}" > /etc/tomcat/geoserverpasswd

# if GEOSERVER_CONSOLE_DISABLED is set to true, disable the web interface - this also speeds up startup
if [ "${GEOSERVER_CONSOLE_DISABLED}" = "true" ]; then
  export JAVA_OPTIONS="-DGEOSERVER_CONSOLE_DISABLED=true"
fi

# execute aerius-wui-server's default entrypoint
/docker-aerius-wui-server-entrypoint.sh "${@}"
