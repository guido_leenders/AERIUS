### Base for GeoServer images

Handles enable/disabling the GeoServer UI Console + the handling of `geoserverpasswd`.
Adds generic fonts to be available for all GeoServer images.

Only for use in other images.

##### ENV variables for running images based on this
- `GEOSERVERPASSWD_ENCRYPTED`: [REQUIRED] Contains the encrypted GeoServer password.
- `GEOSERVER_CONSOLE_DISABLED`: If set to true will disable the console. By default the console is enabled.

##### Example build
```shell
docker build -t aerius-geoserver-base:latest .
```
