### Register GeoServer image

The webapp war should be placed in this directory as `geoserver-register.war` while building the image.

##### Example build
```shell
docker build -t aerius-geoserver-register:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -e GEOSERVERPASSWD_ENCRYPTED="my_encrypted_password" \
  -e DBPASSWORD="password" \
  -p 8080:8080 \
  aerius-geoserver-register:latest
```

##### Example run without GeoServer Console (starts way faster)
```shell
docker run --rm --network host \
  -e GEOSERVERPASSWD_ENCRYPTED="my_encrypted_password" \
  -e DBPASSWORD="password" \
  -e GEOSERVER_CONSOLE_DISABLED=true \
  -p 8080:8080 \
  aerius-geoserver-register:latest
```