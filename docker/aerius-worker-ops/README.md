### OPS Worker image

`ops` directory should be placed in this directory while building the image.

##### Example build
```shell
docker build -t aerius-worker-ops:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  aerius-worker-ops:latest
```

##### Example run with 4 OPS processes (defaults to 1)
```shell
docker run --rm --network host \
  -e AERIUS_BROKER_USERNAME=user \
  -e AERIUS_BROKER_PASSWORD=password \
  -e AERIUS_OPS_PROCESSES=4 \
  aerius-worker-ops:latest
```