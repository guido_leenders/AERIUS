<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <NamedLayer>
    <Name>wms_user_geo_layer_road_total_vehicles_per_day</Name>
    <UserStyle>
      <Title>Total vehicles per day</Title>

        <FeatureTypeStyle>
          <Rule>
            <Name>&lt; 10.000</Name>
            <ogc:Filter>
              <ogc:PropertyIsLessThan>
                <ogc:PropertyName>value</ogc:PropertyName>
                <ogc:Literal>10000</ogc:Literal>
              </ogc:PropertyIsLessThan>
            </ogc:Filter>
            <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke">#FFB767</CssParameter>
                <CssParameter name="stroke-width">6</CssParameter>
              </Stroke>
            </LineSymbolizer>
          </Rule>
        </FeatureTypeStyle>

        <FeatureTypeStyle>
          <Rule>
            <Name>10.000 - 20.000</Name>
            <ogc:Filter>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>value</ogc:PropertyName>
                  <ogc:Literal>10000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsLessThan>
                  <ogc:PropertyName>value</ogc:PropertyName>
                  <ogc:Literal>20000</ogc:Literal>
                </ogc:PropertyIsLessThan>
              </ogc:And>
            </ogc:Filter>
            <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke">#EB1C23</CssParameter>
                <CssParameter name="stroke-width">6</CssParameter>
              </Stroke>
            </LineSymbolizer>
          </Rule>
        </FeatureTypeStyle>

        <FeatureTypeStyle>
          <Rule>
            <Name>20.000 - 40.000</Name>
            <ogc:Filter>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>value</ogc:PropertyName>
                  <ogc:Literal>20000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsLessThan>
                  <ogc:PropertyName>value</ogc:PropertyName>
                  <ogc:Literal>40000</ogc:Literal>
                </ogc:PropertyIsLessThan>
              </ogc:And>
            </ogc:Filter>
            <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke">#0073B7</CssParameter>
                <CssParameter name="stroke-width">6</CssParameter>
              </Stroke>
            </LineSymbolizer>
          </Rule>
        </FeatureTypeStyle>

        <FeatureTypeStyle>
          <Rule>
            <Name>40.000 - 80.000</Name>
            <ogc:Filter>
              <ogc:And>
                <ogc:PropertyIsGreaterThanOrEqualTo>
                  <ogc:PropertyName>value</ogc:PropertyName>
                  <ogc:Literal>40000</ogc:Literal>
                </ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyIsLessThan>
                  <ogc:PropertyName>value</ogc:PropertyName>
                  <ogc:Literal>80000</ogc:Literal>
                </ogc:PropertyIsLessThan>
              </ogc:And>
            </ogc:Filter>
            <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke">#000000</CssParameter>
                <CssParameter name="stroke-width">6</CssParameter>
              </Stroke>
            </LineSymbolizer>
          </Rule>
        </FeatureTypeStyle>

        <FeatureTypeStyle>
          <Rule>
            <Name>&gt; 80.000</Name>
            <ogc:Filter>
              <ogc:PropertyIsGreaterThanOrEqualTo>
                <ogc:PropertyName>value</ogc:PropertyName>
                <ogc:Literal>80000</ogc:Literal>
              </ogc:PropertyIsGreaterThanOrEqualTo>
            </ogc:Filter>
            <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke">#00AA6D</CssParameter>
                <CssParameter name="stroke-width">6</CssParameter>
              </Stroke>
            </LineSymbolizer>
          </Rule>
        </FeatureTypeStyle>

    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>