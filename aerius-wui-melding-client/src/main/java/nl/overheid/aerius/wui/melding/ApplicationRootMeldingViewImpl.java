/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.ApplicationRootView;
import nl.overheid.aerius.wui.main.util.development.DevModeUtil;
import nl.overheid.aerius.wui.main.util.development.DevPanel;

/**
 * Main Application Display implementation. Shows a menu on the left side and
 * a content panel on the right side. The content panel is the display panel
 * of the ActivityManager.
 */
@Singleton
public final class ApplicationRootMeldingViewImpl extends Composite implements ApplicationRootView {
  private static final ApplicationRootViewUiBinder UI_BINDER = GWT.create(ApplicationRootViewUiBinder.class);

  interface ApplicationRootViewUiBinder extends UiBinder<Widget, ApplicationRootMeldingViewImpl> {}

  @UiField FlowPanel dock;
  @UiField SimplePanel content;
  @UiField Image logoImage;

  @Inject
  public ApplicationRootMeldingViewImpl(final DevPanel devPanel) {
    logoImage = new Image(R.images().aeriusCalculatorLogo().getSafeUri());

    initWidget(UI_BINDER.createAndBindUi(this));

    if (devPanel != null && DevModeUtil.I.isDevMode()) {
      dock.insert(devPanel, 0);
    }
  }

  @Override
  public void setWidget(final IsWidget w) {
    content.setWidget(w);
  }
}
