/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.context;

import com.google.inject.Singleton;

/**
 * Application Context object for Melding.
 */
@Singleton
public class MeldingAppContext {
  public static enum MeldingSubmitState {
    TO_BE_SEND,
    IN_PROGRESS,
    SUCCESS,
    FAILED,
  }

  MeldingSubmitState submitState = MeldingSubmitState.TO_BE_SEND;

  public boolean isSend() {
    return getSubmitState() != MeldingSubmitState.TO_BE_SEND;
  }

  public MeldingSubmitState getSubmitState() {
    return submitState;
  }

  public void setSubmitState(final MeldingSubmitState submitState) {
    this.submitState = submitState;
  }
}
