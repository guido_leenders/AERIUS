/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.inject.Inject;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.melding.context.MeldingAppContext;
import nl.overheid.aerius.wui.melding.place.MeldingCompletePlace;
import nl.overheid.aerius.wui.melding.place.MeldingExtraInformationPlace;
import nl.overheid.aerius.wui.melding.place.MeldingInitiatorPlace;
import nl.overheid.aerius.wui.melding.ui.MeldingCompleteActivity;
import nl.overheid.aerius.wui.melding.ui.MeldingExtraInformationActivity;
import nl.overheid.aerius.wui.melding.ui.MeldingInitiatorActivity;

/**
 * Activity Mapper for the Melding product.
 */
class MeldingActivityMapper implements ActivityMapper {
  private static boolean startedUp;
  private final ActivityFactory factory;
  private final MeldingAppContext appContext;
  @Inject
  public MeldingActivityMapper(final ActivityFactory factory, final MeldingAppContext appContext) {
    this.factory = factory;
    this.appContext = appContext;
  }

  @Override
  public Activity getActivity(final Place place) {
    Activity presenter = null;
    startUp();
    if (place instanceof MeldingCompletePlace) {
      presenter = factory.createNoticeCompleteActivity((MeldingCompletePlace) place);
    } else if (appContext.isSend()) {
      // If completed always show same page, but fake the place object.
      presenter = factory.createNoticeCompleteActivity(new MeldingCompletePlace());
    } else if (place instanceof MeldingInitiatorPlace) {
      presenter = factory.createNoticeInitiatorActivity((MeldingInitiatorPlace) place);
    } else if (place instanceof MeldingExtraInformationPlace) {
      presenter = factory.createNoticeExtraInformationActivity((MeldingExtraInformationPlace) place);
    }

    return presenter;
  }

  private void startUp() {
    if (!startedUp) {
      Window.addWindowClosingHandler(new ClosingHandler() {
        @Override
        public void onWindowClosing(final ClosingEvent event) {
          if (appContext != null && !appContext.isSend()) {
            event.setMessage(M.messages().meldingLeaveUnfinished());
          }
        }
      });
    }
  }

  /**
   * Methods capable of creating presenters given the place that is passed in.
   */
  public interface ActivityFactory {
    MeldingCompleteActivity createNoticeCompleteActivity(MeldingCompletePlace place);

    MeldingExtraInformationActivity createNoticeExtraInformationActivity(MeldingExtraInformationPlace place);

    MeldingInitiatorActivity createNoticeInitiatorActivity(MeldingInitiatorPlace place);
  }
}
