/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.InputElement;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.ui.validation.ValidatorCollection;
import nl.overheid.aerius.wui.main.widget.validation.IsWidgetErrorStyle;

public class FileUploadPanel extends Composite implements ValueAwareEditor<ArrayList<String>>, LeafValueEditor<ArrayList<String>>,
IsWidgetErrorStyle {
  private static final String DEFAULT_UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_SAVE_SERVLET;

  public interface FileUploadActions {
    void deleteFile(String filename, AsyncCallback<Void> callback);

    void fileExist(String filename,  AsyncCallback<Boolean> callback);

    void startSubmit(String uploadFileName);

    void onSubmitComplete();

    boolean isFileNameInList(String file);

    /**
     * @return
     */
    String getUUID();
  }

  private static FileUploadPanelUiBinder uiBinder = GWT.create(FileUploadPanelUiBinder.class);

  interface FileUploadPanelUiBinder extends UiBinder<Widget, FileUploadPanel> {
  }

  private final AsyncCallback<Void> deleteCallback = new AsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      setDeleteButtonStatus();
    }

    @Override
    public void onFailure(final Throwable caught) {
      setDeleteButtonStatus();
    }
  };

  private final AsyncCallback<Boolean> existsCallback = new AsyncCallback<Boolean>() {
    @Override
    public void onSuccess(final Boolean result) {
      uploadFileCheckDone(result);
    }

    @Override
    public void onFailure(final Throwable caught) {
      setDeleteButtonStatus();
    }

  };

  @UiField SimplePanel loaderPanel;
  @UiField Label titleTextLabel;
  @UiField FormPanel fileForm;
  @UiField FileUpload fileField;
  @UiField Button deleteFileButton;
  @UiField ListBoxEditor<String> uploadList;
  @UiField @Ignore Label warningUniqueFile;
  @UiField @Ignore Label warningUploadFailure;
  private FileUploadActions actions;
  private EditorDelegate<ArrayList<String>> delegate;
  private final ValidatorCollection<ArrayList<String>> validators = new ValidatorCollection<>();

  protected String uploadFile;

  public FileUploadPanel(final String titleText, final String filename, final int rows) {
    initWidget(uiBinder.createAndBindUi(this));
    titleTextLabel.setText(titleText);
    uploadList.setVisibleItemCount(rows);
    fileField.setName(filename);
    fileForm.setEncoding(FormPanel.ENCODING_MULTIPART);
    fileForm.setMethod(FormPanel.METHOD_POST);
    fileForm.setAction(DEFAULT_UPLOAD_ACTION);
    final InputElement uuidElement = Document.get().createHiddenInputElement();
    uuidElement.setName(SharedConstants.MELDING_UUID_PARAM);
    fileForm.getElement().appendChild(uuidElement);

    fileField.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        final String filename = extractFilename(fileField.getFilename());
        if (!validateFileName(filename)) {
          if (!filename.isEmpty()) {
            uploadFile = filename;
            uuidElement.setValue(actions.getUUID());
            fileForm.submit();
          }
        }
      }
    });
    fileForm.addSubmitHandler(new SubmitHandler() {
      @Override
      public void onSubmit(final SubmitEvent event) {
        uploading(true);
        actions.startSubmit(uploadFile);
      }
    });
    fileForm.addSubmitCompleteHandler(new SubmitCompleteHandler() {
      @Override
      public void onSubmitComplete(final SubmitCompleteEvent event) {
        actions.fileExist(uploadFile, existsCallback);
      }
    });
    deleteFileButton.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        final int selectedIndex = uploadList.getSelectedIndex();
        if (selectedIndex >= 0) {
          if (actions != null) {
            actions.deleteFile(uploadList.getSelectedValue(), deleteCallback);
          }
          uploadList.removeItem(selectedIndex);
        }
      }
    });
    fileField.ensureDebugId(TestID.INPUT_IMPORTFILE);
    uploadList.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        setDeleteButtonStatus();
        showWarningUniqueFile(false);
        showWarningFileNotUploaded(false);
      }
    });

    // reset upload panel
    uploading(false);
  }

  private void uploading(final boolean status) {
    loaderPanel.setVisible(status);
    fileField.setVisible(!status);
  }

  private void uploadFileCheckDone(final boolean result) {
    uploading(false);
    if (result) {
      uploadList.addItem(uploadFile, uploadFile);
    } else {
      showWarningFileNotUploaded(true);
    }
    fileField.getElement().setPropertyString("value", "");
    actions.onSubmitComplete();
  }

  private boolean validateFileName(final String newFile) {
    final Boolean exists = getValue().contains(newFile) || actions.isFileNameInList(newFile);
    showWarningUniqueFile(exists);
    return exists;
  }

  private void showWarningUniqueFile(final boolean visible) {
    warningUniqueFile.setVisible(visible);
  }

  private void showWarningFileNotUploaded(final boolean visible) {
    warningUploadFailure.setVisible(visible);
  }

  public void addValidator(final Validator<ArrayList<String>> validator) {
    validators.addValidator(validator);
  }

  public void setFileUploadActions(final FileUploadActions actions) {
    this.actions = actions;
  }

  @Override
  public void setValue(final ArrayList<String> fileNames) {
    showWarningUniqueFile(false);
    showWarningFileNotUploaded(false);
    uploadList.clear();
    uploadList.addItems(fileNames);
  }

  @Override
  public ArrayList<String> getValue() {
    final ArrayList<String> files = new ArrayList<>();
    for (int i = 0; i < uploadList.getItemCount(); i++) {
      files.add(uploadList.getValue(i));
    }
    return files;
  }

  private void setDeleteButtonStatus() {
    deleteFileButton.setVisible(uploadList.getSelectedIndex() > -1);
  }

  private String extractFilename(final String filename) {
    final String seperator = filename.contains("/") ? "/" : "\\";
    final int pos = fileField.getFilename().lastIndexOf(seperator);
    return pos >= 0 ? filename.substring(pos + 1) : filename;
  }

  @Override
  public void setDelegate(final EditorDelegate<ArrayList<String>> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    if (delegate != null) {
      final ArrayList<String> value = getValue();
      final String errorMessage = validators.validate(value);
      if (errorMessage != null) {
        delegate.recordError(errorMessage, value.size(), FileUploadPanel.this);
      }
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  public void setEditorEnabled(final boolean editorEnabled) {
    if (!editorEnabled) {
      clear();
    }
  }

  private void clear() {
    if (actions != null) {
      for (int i = 0; i < uploadList.getItemCount(); i++) {
        actions.deleteFile(uploadList.getValue(i), deleteCallback);
      }
      uploadList.clear();
    }
  }

  @Override
  public Widget getFocusWidget() {
    return fileField;
  }
}
