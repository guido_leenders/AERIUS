/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.melding.OrganisationInformation;

public class OrganisationInformationEditor extends AbstractOrganisationInformationEditor<OrganisationInformation> {
  interface OrganisationInformationEditorUiBinder extends UiBinder<Widget, OrganisationInformationEditor> {}

  private static final OrganisationInformationEditorUiBinder UI_BINDER = GWT.create(OrganisationInformationEditorUiBinder.class);

  public OrganisationInformationEditor() {
    initWidget(UI_BINDER.createAndBindUi(this));

    addValidators();

    setPlaceHolders();
  }

}
