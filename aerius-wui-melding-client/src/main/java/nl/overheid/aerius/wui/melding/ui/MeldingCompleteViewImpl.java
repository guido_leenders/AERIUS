/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;

import nl.overheid.aerius.wui.melding.context.MeldingAppContext.MeldingSubmitState;

@Singleton
public class MeldingCompleteViewImpl extends Composite implements MeldingCompleteView {
  interface NoticeCompleteViewImplUiBinder extends UiBinder<Widget, MeldingCompleteViewImpl> {}

  private static final NoticeCompleteViewImplUiBinder UI_BINDER = GWT.create(NoticeCompleteViewImplUiBinder.class);

  @UiField DeckPanel switchPanel;

  public MeldingCompleteViewImpl() {
    initWidget(UI_BINDER.createAndBindUi(this));

    switchPanel.showWidget(0);
  }

  @Override
  public void setState(final MeldingSubmitState submitState) {
    if (submitState == MeldingSubmitState.SUCCESS) {
      switchPanel.showWidget(1);
    } else if (submitState == MeldingSubmitState.FAILED) {
      switchPanel.showWidget(2);
    } else {
      switchPanel.showWidget(0);
    }
  }
}
