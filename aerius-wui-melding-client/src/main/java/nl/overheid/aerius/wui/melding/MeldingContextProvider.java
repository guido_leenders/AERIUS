/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.shared.domain.context.MeldingContext;
import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.wui.main.AeriusContextProvider;
import nl.overheid.aerius.wui.melding.context.MeldingAppContext;

/**
 * ContextProvider for Calculator specific data.
 */
@Singleton
class MeldingContextProvider extends AeriusContextProvider<MeldingContext, MeldingUserContext> {
  interface ContextProviderEventBinder extends EventBinder<MeldingContextProvider> {}

  private final ContextProviderEventBinder eventBinder = GWT.create(ContextProviderEventBinder.class);

  private final MeldingAppContext appContext = new MeldingAppContext();

  @Inject
  public MeldingContextProvider(final EventBus eventBus) {
    // Not needed (yet) - left it in here because we will.
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @Provides
  @Singleton
  public MeldingAppContext getAppContext() {
    return appContext;
  }

  @Provides
  @Singleton
  public MeldingContext getMeldingContext() {
    return (MeldingContext) getContext();
  }

  @Provides
  @Singleton
  public MeldingUserContext getMeldingUserContext() {
    return (MeldingUserContext) getUserContext();
  }
}
