/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.service.MeldingServiceAsync;
import nl.overheid.aerius.wui.melding.place.MeldingExtraInformationPlace;
import nl.overheid.aerius.wui.melding.place.MeldingInitiatorPlace;
import nl.overheid.aerius.wui.melding.ui.MeldingInitiatorViewImpl.MeldingInformationDriver;

public class MeldingInitiatorActivity extends BaseMeldingActivity<MeldingInitiatorView> implements MeldingInitiatorView.Presenter {
  private final MeldingInitiatorView view;
  private final MeldingServiceAsync service;
  private final BaseMeldingView baseView;

  @SuppressWarnings("unchecked")
  @Inject
  public MeldingInitiatorActivity(final BaseMeldingView baseView, @Assisted final MeldingInitiatorPlace place, final MeldingInitiatorView view,
      final PlaceController placeController, final MeldingUserContext userContext, final MeldingServiceAsync service) {
    super(baseView, place, placeController, userContext,
        (SimpleBeanEditorDriver<MeldingInformation, MeldingInitiatorView>) GWT.create(MeldingInformationDriver.class));
    view.setPresenter(this);
    this.baseView = baseView;
    this.view = view;
    this.service = service;
  }

  @Override
  protected MeldingInitiatorView getContentView() {
    view.setPresenter(this);
    return view;
  }

  @Override
  protected Place getPreviousPlace() {
    return place; // there is no previous place. this is the first place
  }

  @Override
  protected Place getNextPlace() {
    return new MeldingExtraInformationPlace();
  }

  @Override
  public String getUUID() {
    return userContext.getMeldingInformation().getUUID();
  }

  @Override
  public void deleteFile(final String filename, final AsyncCallback<Void> callback) {
    service.deleteUploadedFile(filename, userContext.getMeldingInformation().getUUID(), callback);
  }

  @Override
  public void fileExist(final String filename, final AsyncCallback<Boolean> callback) {
    service.validateUploadedFile(filename, userContext.getMeldingInformation().getUUID(), callback);
  }

  @Override
  public void startSubmit(final String uploadFileName) {
    baseView.setFileUploadfile(uploadFileName);
    baseView.setNextEnabled(false);
  }

  @Override
  public void onSubmitComplete() {
    baseView.setFileUploadfile("");
    baseView.setNextEnabled(true);
  }

  @Override
  public boolean isFileNameInList(final String file) {
    return  userContext.getMeldingInformation().getSubstantiationFiles() == null ? false
        : userContext.getMeldingInformation().getSubstantiationFiles().contains(file);
  }

}
