/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding;

import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.MeldingContext;
import nl.overheid.aerius.shared.domain.context.MeldingUserContext;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;
import nl.overheid.aerius.wui.main.AeriusClientModule.AeriusPlaceController;
import nl.overheid.aerius.wui.main.AeriusContextProvider;
import nl.overheid.aerius.wui.main.ApplicationInitializer;
import nl.overheid.aerius.wui.main.RootPanelFactory;
import nl.overheid.aerius.wui.main.RootPanelFactory.RootPanelFactoryImpl;
import nl.overheid.aerius.wui.main.place.DefaultPlace;
import nl.overheid.aerius.wui.main.ui.ApplicationRootView;
import nl.overheid.aerius.wui.main.util.development.EmbeddedDevPanel;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;
import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger.SuperNitroTurboJuggernaut;
import nl.overheid.aerius.wui.melding.MeldingActivityMapper.ActivityFactory;
import nl.overheid.aerius.wui.melding.context.MeldingAppContext;
import nl.overheid.aerius.wui.melding.i18n.HelpToolTipInfoMelding;
import nl.overheid.aerius.wui.melding.place.MeldingInitiatorPlace;
import nl.overheid.aerius.wui.melding.place.MeldingPlaceHistoryMapper;
import nl.overheid.aerius.wui.melding.ui.BaseMeldingView;
import nl.overheid.aerius.wui.melding.ui.BaseMeldingViewImpl;
import nl.overheid.aerius.wui.melding.ui.MeldingCompleteView;
import nl.overheid.aerius.wui.melding.ui.MeldingCompleteViewImpl;
import nl.overheid.aerius.wui.melding.ui.MeldingExtraInformationView;
import nl.overheid.aerius.wui.melding.ui.MeldingExtraInformationViewImpl;
import nl.overheid.aerius.wui.melding.ui.MeldingInitiatorView;
import nl.overheid.aerius.wui.melding.ui.MeldingInitiatorViewImpl;
import nl.overheid.aerius.wui.melding.util.development.MeldingDevPanel;
import nl.overheid.aerius.wui.melding.util.development.SuperNitroTurboMeldingLogger;
import nl.overheid.aerius.wui.melding.util.development.SuperNitroTurboMeldingLogger.SuperNitroTurboMeldingJuggernaut;

class MeldingClientModule extends AbstractGinModule {
  @Override
  protected void configure() {
    bind(PlaceController.class).to(AeriusPlaceController.class).in(Singleton.class);
    bind(ActivityMapper.class).to(MeldingActivityMapper.class).in(Singleton.class);
    bind(Place.class).annotatedWith(DefaultPlace.class).to(MeldingInitiatorPlace.class).in(Singleton.class);
    bind(PlaceHistoryMapper.class).to(MeldingPlaceHistoryMapper.class).in(Singleton.class);
    bind(new TypeLiteral<AeriusContextProvider<?, ?>>() {}).to(MeldingContextProvider.class);
    bind(HelpToolTipInfo.class).to(HelpToolTipInfoMelding.class);
    bind(ApplicationRootView.class).to(ApplicationRootMeldingViewImpl.class).in(Singleton.class);
    bind(RootPanelFactory.class).to(RootPanelFactoryImpl.class);
    bind(ApplicationInitializer.class).to(MeldingApplicationInitializer.class).in(Singleton.class);

    // UI
    bind(BaseMeldingView.class).to(BaseMeldingViewImpl.class).in(Singleton.class);
    bind(MeldingInitiatorView.class).to(MeldingInitiatorViewImpl.class).in(Singleton.class);
    bind(MeldingExtraInformationView.class).to(MeldingExtraInformationViewImpl.class).in(Singleton.class);
    bind(MeldingCompleteView.class).to(MeldingCompleteViewImpl.class).in(Singleton.class);

    // See CCM
    bind(SuperNitroTurboLogger.class).to(SuperNitroTurboMeldingLogger.class);
    bind(SuperNitroTurboJuggernaut.class).to(SuperNitroTurboMeldingJuggernaut.class);
    bind(EmbeddedDevPanel.class).to(MeldingDevPanel.class);

    // Place to Activity assisted injection
    install(new GinFactoryModuleBuilder().build(ActivityFactory.class));
  }

  @Provides
  public MeldingAppContext getAppContainer(final MeldingContextProvider provider) {
    return provider.getAppContext();
  }

  @Provides
  public MeldingContext getMeldingContext(final MeldingContextProvider provider) {
    return provider.getMeldingContext();
  }

  @Provides
  public Context getContext(final MeldingContextProvider provider) {
    return provider.getContext();
  }

  @Provides
  public MeldingUserContext getMeldingUserContext(final MeldingContextProvider provider) {
    return provider.getMeldingUserContext();
  }

  @Provides
  public UserContext getUserContext(final MeldingContextProvider provider) {
    return provider.getUserContext();
  }
}
