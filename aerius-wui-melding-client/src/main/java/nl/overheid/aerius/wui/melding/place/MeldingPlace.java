/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import nl.overheid.aerius.shared.domain.melding.NoticeStep;

/**
 * Base Melding Place.
 */
public abstract class MeldingPlace extends Place {
  private NoticeStep progressStep;

  public abstract static class Tokenizer<P extends MeldingPlace> implements PlaceTokenizer<P> {
    @Override
    public String getToken(final P place) {
      return "";
    }

    @Override
    public P getPlace(final String token) {
      return createPlace();
    }

    protected abstract P createPlace();
  }

  public MeldingPlace() { }

  public MeldingPlace(final NoticeStep progressStep) {
    this.progressStep = progressStep;
  }

  public NoticeStep getProgressStep() {
    return progressStep;
  }

  public void setProgressStep(final NoticeStep progressStep) {
    this.progressStep = progressStep;
  }

  public boolean hasPreviousPlace() {
    return false;
  }

  public boolean hasNextPlace() {
    return false;
  }

  public boolean isFinalPlace() {
    return false;
  }
}
