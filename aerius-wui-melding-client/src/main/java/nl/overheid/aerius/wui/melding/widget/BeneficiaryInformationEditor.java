/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.melding.BeneficiaryInformation;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

public class BeneficiaryInformationEditor extends AbstractOrganisationInformationEditor<BeneficiaryInformation> {
  interface BeneficiaryInformationEditorUiBinder extends UiBinder<Widget, BeneficiaryInformationEditor> {
  }

  private static final BeneficiaryInformationEditorUiBinder UI_BINDER =
      GWT.create(BeneficiaryInformationEditorUiBinder.class);

  @UiField FlowPanel correspondenceAddressContainer;
  @UiField TextValueBox correspondenceAddress;
  @UiField TextValueBox correspondencePostcode;
  @UiField TextValueBox correspondenceCity;
  @Ignore @UiField RadioButton correspondenceSame;
  @UiField RadioButton correspondenceCustom;

  public BeneficiaryInformationEditor() {
    initWidget(UI_BINDER.createAndBindUi(this));

    addValidators();

    setPlaceHolders();
  }

  @Override
  protected void addValidators() {
    super.addValidators();

    correspondenceAddress.addValidator(new NotEmptyValidator() {
      @Override
      public String validate(final String value) {
        return correspondenceCustom.getValue() ? super.validate(value) : null;
      }

      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationAddress();
      }
    });
    correspondencePostcode.addValidator(new NotEmptyValidator() {
      @Override
      public String validate(final String value) {
        return correspondenceCustom.getValue() ? super.validate(value) : null;
      }

      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationPostcode();
      }
    });
    correspondenceCity.addValidator(new NotEmptyValidator() {
      @Override
      public String validate(final String value) {
        return correspondenceCustom.getValue() ? super.validate(value) : null;
      }

      @Override
      protected String getMessage(final String value) {
        return M.messages().validatorOrganisationCity();
      }
    });
  }

  @Override
  protected void setPlaceHolders() {
    super.setPlaceHolders();

    StyleUtil.I.setPlaceHolder(correspondenceAddress, M.messages().placeHolderOrganisationAddress());
    StyleUtil.I.setPlaceHolder(correspondencePostcode, M.messages().placeHolderOrganisationPostcode());
    StyleUtil.I.setPlaceHolder(correspondenceCity, M.messages().placeHolderOrganisationCity());
  }

  @UiHandler({"correspondenceSame", "correspondenceCustom"})
  public void onBeneficiaryInformationSelectionChange(final ValueChangeEvent<Boolean> e) {
    final boolean correspondenceEnabled = correspondenceCustom.getValue();

    if (!correspondenceEnabled) {
      ErrorPopupController.clearWidgets();
    }

    correspondenceAddressContainer.setVisible(correspondenceEnabled);
  }

  @Override
  public void clear() {
    super.clear();
    correspondenceAddress.setText(null);
    correspondencePostcode.setText(null);
    correspondenceCity.setText(null);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    correspondenceAddress.ensureDebugId(baseID + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.ORG_ADDR);
    correspondencePostcode.ensureDebugId(baseID + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.ORG_POST);
    correspondenceCity.ensureDebugId(baseID + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.ORG_CITY);
    correspondenceSame.ensureDebugId(baseID + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.SAME);
    correspondenceCustom.ensureDebugId(baseID + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.CUSTOM);
  }
}
