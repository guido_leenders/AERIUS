/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.place;

import com.google.gwt.place.shared.Prefix;

import nl.overheid.aerius.shared.domain.melding.NoticeStep;

public class MeldingExtraInformationPlace extends MeldingPlace {
  private static final String PREFIX = "s3";

  @Prefix(PREFIX)
  public static class Tokenizer extends MeldingPlace.Tokenizer<MeldingExtraInformationPlace> {
    @Override
    public MeldingExtraInformationPlace createPlace() {
      return new MeldingExtraInformationPlace();
    }
  }

  public MeldingExtraInformationPlace() {
    super(NoticeStep.EXTRA_INFORMATION);
  }

  @Override
  public boolean isFinalPlace() {
    return true;
  }

  @Override
  public boolean hasNextPlace() {
    return true;
  }

  @Override
  public boolean hasPreviousPlace() {
    return true;
  }
}
