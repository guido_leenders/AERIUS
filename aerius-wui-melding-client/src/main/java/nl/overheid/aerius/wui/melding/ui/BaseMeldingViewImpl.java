/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.melding.NoticeStep;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.SimpleProgressIndicator;

@Singleton
public class BaseMeldingViewImpl extends Composite implements BaseMeldingView {
  interface BaseMeldingViewImplUiBinder extends UiBinder<Widget, BaseMeldingViewImpl> { }

  private static final BaseMeldingViewImplUiBinder UI_BINDER = GWT.create(BaseMeldingViewImplUiBinder.class);

  @UiField(provided = true) SimpleProgressIndicator<NoticeStep> progress;

  @UiField SimplePanel content;

  @UiField Button previousButton;
  @UiField Button nextButton;

  private Presenter presenter;

  public BaseMeldingViewImpl() {
    progress = new SimpleProgressIndicator<NoticeStep>() {
      @Override
      protected String getStepText(final NoticeStep object) {
        return M.messages().meldingProgressStep(object);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    progress.setSteps(NoticeStep.values());

    previousButton.ensureDebugId(TestIDMelding.BUTTON_PREVIOUS_PAGE);
    nextButton.ensureDebugId(TestIDMelding.BUTTON_NEXT_PAGE);
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setWidget(final IsWidget w) {
    content.setWidget(w);
  }

  @Override
  public void setProgress(final NoticeStep progressStep) {
    progress.setStepState(progressStep);
  }

  @Override
  public void setNextEnabled(final boolean enabled) {
    nextButton.setEnabled(enabled);
  }

  @Override
  public void setPreviousEnabled(final boolean enabled) {
    previousButton.setEnabled(enabled);
  }

  @Override
  public void setNextButtonText(final NoticeStep progressStep) {
    nextButton.setText(M.messages().meldingNextStep(progressStep));
  }

  @Override
  public void setShowButtons(final boolean showButtons) {
    nextButton.setVisible(showButtons);
    previousButton.setVisible(showButtons);
  }

  @UiHandler("previousButton")
  public void onPreviousButtonClick(final ClickEvent e) {
    presenter.goToPrevious();
  }

  @UiHandler("nextButton")
  public void onNextButtonClick(final ClickEvent e) {
    presenter.goToNext();
  }

  @Override
  public void setFileUploadfile(final String uploadFileName) {
    nextButton.setTitle((uploadFileName.isEmpty() ? "" : M.messages().meldingBusyUploading(uploadFileName)));
    previousButton.setTitle((uploadFileName.isEmpty() ? "" : M.messages().meldingBusyUploading(uploadFileName)));
  }

}
