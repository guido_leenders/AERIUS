/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.melding.util.development;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.wui.main.util.development.SuperNitroTurboLogger;

public class SuperNitroTurboMeldingLogger extends SuperNitroTurboLogger {
  interface SuperTurboMeldingBinder extends EventBinder<SuperNitroTurboMeldingJuggernaut> {}

  private final SuperTurboMeldingBinder playground;

  public static class SuperNitroTurboMeldingJuggernaut extends SuperNitroTurboJuggernaut {
  }

  @Inject
  public SuperNitroTurboMeldingLogger(final EventBus eventBus, final SuperNitroTurboMeldingJuggernaut juggernaut) {
    super(eventBus, juggernaut);
    playground = GWT.create(SuperTurboMeldingBinder.class);
    playground.bindEventHandlers(juggernaut, eventBus);
    verbose("SuperNitroTurboMeldingJuggernaut attached.");
  }
}
