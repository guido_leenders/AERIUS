<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?= isset($_GET["code"]) && is_numeric($_GET["code"]) ? $_GET["code"] : "503" ?> | AERIUS</title>
<link href="/resources/css/splash.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#splash .splash {
  background: rgb(20, 102, 155);
  background: linear-gradient(rgb(24, 144, 195), rgb(16, 68, 116));
  background: -o-linear-gradient(bottom, rgb(24, 144, 195) 0%, rgb(16, 68, 116) 100%);
  background: -moz-linear-gradient(bottom, rgb(24, 144, 195) 0%, rgb(16, 68, 116) 100%);
  background: -webkit-linear-gradient(bottom, rgb(24, 144, 195) 0%, rgb(16, 68, 116) 100%);
  background: -ms-linear-gradient(top, rgb(24, 144, 195) 0%, rgb(16, 68, 116) 100%);
}

#splash .logo {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAAAfCAYAAAA1MiXTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABUBJREFUeNrsWzFo3EgUHS2GgzSOGjdpcmsIpAvIhJRn0NZJo+sOnEZLqsTVbpe7zls5qcJukxzpssWd6xWcyyMg4lSBwAo3adzIaQKpdH+4P8l4MquZr9Va40UPPl6tNMNo9eb990djrygK1qKFBhHEAUQXYgrRh8g77e+ylrgO8Q7/VsEYCbMD4UEkEDMIvyXMeuIQ4g7EkwptY04MiF8h5hAFHnOVib02Ja0dfoH4Rzr+GeKU0J6TpAeRIVn45zeoNrOWMOuHd6guAscQu4T2BaYh8ZljCDHix21KWi/sKWQRivOA0AdPPQPpmJMlxEhahWmhwkeDy4mTQ0wkE9xrFaaFihz9CkOlKbC05l4mbRWmBQneohM/vf7A2XWgOZV+/e32jqnjG0dzChN7n+5vJ/IXD/89KwyzIMX1genLe1vZogtffPx8oZ9HtzYv3PPbsy8Xzt/duuaZBqu0SaBNr9RFFoWPkh7gbA2l05kUfWI19Bg9y01CuxOMP9EQk1CWksIF3wdAJt+BPBsioedArjGE7+KMBLLEWKqOcY1D/V0FgWLLLvli3F9YOj8gkoUhwfaw/ctaCIOECEvaRY49lxjXClwjywESxYbMuWW3h8Sqx1RVHVIabFiqS4py+k1lKgyuTLZTU2NIO56UrsSMlIkbwvcxXDdxhCyhUp6KkjVV7tdHlbFNQ3s1D5WvBj9nlot7toQZYqklKwwl3zLVoywDIAXvKwGCzJSxhlgGugBVhfue5000xKL0eX+F6ze/L+NhIsXkJopk+pC2AgceytTSdzVNmFRHlgq4s6Kxblb2MEgEX/NQEgcfTqYxw66g6bH8sQoidiz9i85nuGZ8dQRqErIiB1gtXRZeYYo5rrvjjin3YjrSKYwL5XVANc8NpssxkIbHqifaOcS+qA/q7nxDU04HupsG4qRwPlekNrI1mSULeQkY4h514Ljuos7axDHCxJryP0ajO8XxTgkltW0qOsfPp3j8dFUKExlmrBM+BsvqmVKOpq6U1BxgchNDJRnhGs1cU34vg+uYjkRsrkxhNARINMdRE4SxeFXQZ46BV0agJhkSIiwxx2LvbB338HSV91RGmJynIQOBeHkdSj6nDD0Lc1gFXFWGoC45cxCoNIn0PilcUDDEqOgT5jA2JP8SLiinZQOcwXWZkgpCG+9Q58KdSmJXyaIQR+wt4arTRYIMNGnqahBGI5kxkMOmFLyUtCReDUj+5Vv1Ad9dCdJI5OGTbgjEydnFHQGh62Pv1LCuwsvr7mUNGF8LTDQeoI71Er51oXQFW3M+XYI4I3bF0MF01GX2L8AaUxkJI+VBx6g8VZAS70VV3cqLhZiaaiHfkjilKoyqLiPwK96iaLq8xg1T6uysuidGvZcDUJF4gboMFMLkOq9nSRYfy2pW5hsVnBjOH7P/N8WpcWxo957qYagym7Af3xKX4sbRvOyaDEwxdaZO8OGJWdpFEzkk9jPVpLQxkka8dBUVjqoIo7tb13INGQIcS4rt5XsTah4rRUZuYXiPWLV/TjPhbyphIsOsM81Km/J6VnJuqFEMk8rkoCi8nbxxagDf8S2b1tIODzwDcvQ1sz1g5ft++NbMUcnaSkTwhTkuO+QWCvKK1bsn5hkpJcGD/oEs8OBLB47rM3nDPoaTZqoh75jaDzx4PrP7hDWhkWkfLzEl7hD8yz6Spi6y7FNNb0hUl0X5tqm312oKCkBlBhVJs439TZU0kuHvws9tw7WmtCeuTzQklPvaYd//LdUW/D0Rf6m4i6nkhHirQqV2qWTh+E+AAQDVlddce6hodQAAAABJRU5ErkJggg==);
  background-repeat:no-repeat;
}

#splash .dev {
  color: #bbd5e6;
}
</style>
</head>

<body>
<?php
if (isset($_GET["message"])) {
  $error_message = $_GET["message"];
} else {
  // check if there is a custom message set for current host, if so use it
  $filecheckpath = "./resources/" . $_SERVER["SERVER_NAME"] . ".txt";
  $realpath = realpath($filecheckpath);

  if ($realpath === false || strpos($realpath, realpath("./resources/")) !== 0) {
   $error_message = "AERIUS is momenteel onbereikbaar. Probeer later opnieuw.\n\nNeem contact op met de Helpdesk als het te lang aan houdt.";
  } else {
   $error_message = file_get_contents($realpath);
  }
}
?>
<div id="splash">
  <div class="container">
    <div class="splash">
      <div class="logo" title="AERIUS&reg;"></div>
      <div class="textsmall">
        <p class="version">&nbsp;</p>
      </div>
      <p class="dev">AERIUS wordt ontwikkeld in opdracht van de Rijksoverheid en de gezamenlijke provincies</p>
    </div>
    <div class="min withform">
      <img src="/resources/images/min-lnv.png" alt="Ministerie van Landbouw, Natuur en Voedselkwaliteit" width="590" height="51" />
    </div>
    <div class="message errorMessage">
      <h2 id="message"><?= nl2br(htmlentities($error_message)) ?></h2>
    </div>
  </div>
</div>
</body>
</html>

