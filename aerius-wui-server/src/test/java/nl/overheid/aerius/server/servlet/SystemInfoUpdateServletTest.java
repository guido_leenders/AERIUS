/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.SystemInfoMessageRepository;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;

@RunWith(MockitoJUnitRunner.class)
public class SystemInfoUpdateServletTest extends BaseDBTest {

  private static final String MESSAGE_BODY = "messageBody";
  private static final String MESSAGE_TITLE = "messageTitle";
  private static final String UUID = "uuid";
  private static final String LOCALE = "locale";
  private static final String MESSAGE = "message";

  private static final String FAULT_PASSKEY = "non-existing-UUID";
  private static final String PASSKEY = "58cc5716-132e-4062-ab13-4bc63033aaf0";

  @Mock
  private HttpServletRequest request;
  @Mock
  private HttpServletResponse response;
  @Mock
  private RequestDispatcher dispatcher;

  private String locale;

  private SystemInfoUpdateServlet servlet;

  @Override
  @Before
  public void setUp() throws SQLException {
    servlet = new SystemInfoUpdateServlet() {
      @Override
      protected PMF getPmf() {
        return getCalcPMF();
      }
    };
    // add key
    ConstantRepository.setConstantByKey(getCalcPMF().getConnection(), SharedConstantsEnum.SYSTEM_INFO_PASSKEY.name(), PASSKEY);
    // clear old message
    SystemInfoMessageRepository.clearAllMessages(getCalcPMF().getConnection());
    locale = Locale.getDefault().getLanguage();
  }

  @Override
  @After
  public void tearDown() {
  }

  @Test
  public void testNoValidUUID() throws SQLException, ServletException, IOException {
    final String messageTitle = "Fout bij verwerken";
    final String messageBody = "Bericht is niet bijgewerkt, problemen met UUID.";
    assertUpdateSystemMessage("Systeem gaat voor onderhoud uit de lucht", FAULT_PASSKEY, messageTitle, messageBody, false);
  }

  @Test
  public void testUpdateSystemMessage() throws SQLException, ServletException, IOException {
    final String messageTitle = "Bericht verwerkt.";
    final String messageBody = "Nieuw bericht is bijgewerkt.";
    assertUpdateSystemMessage("Systeem gaat voor onderhoud uit de lucht", PASSKEY, messageTitle, messageBody, true);
  }

  @Test
  public void testClearSystemMessage() throws SQLException, ServletException, IOException {
    final String messageTitle = "Bericht verwerkt.";
    final String messageBody = "Nieuw bericht is bijgewerkt.";
    assertUpdateSystemMessage("", PASSKEY, messageTitle, messageBody, true);
  }

  private void assertUpdateSystemMessage(final String message, final String passkey, final String messageTitle, final String messageBody,
      final boolean shouldSet) throws SQLException, ServletException, IOException {
    prepairRequest(message, passkey);
    prepairResponse(HttpStatus.SC_OK);
    prepairRequestMessage(messageTitle, messageBody);
    servlet.doPost(request, response);
    final String currentMessage = SystemInfoMessageRepository.getMessage(getCalcPMF().getConnection(), locale);

    assertEquals("Expect status 200", HttpStatus.SC_OK, response.getStatus());
    assertEquals("Expect message title", messageTitle, request.getParameter(MESSAGE_TITLE));
    assertEquals("Expect message body", messageBody, request.getParameter(MESSAGE_BODY));

    if (shouldSet) {
      assertEquals("New message is not set while we expect it to be set", message, currentMessage);
    } else {
      assertNotEquals("Message must not be changed but is :(", message, currentMessage);
    }

  }

  private void prepairRequest(final String newmessage, final String passKey) {
    Mockito.when(request.getParameter(MESSAGE)).thenReturn(newmessage);
    Mockito.when(request.getParameter(LOCALE)).thenReturn(locale);
    Mockito.when(request.getParameter(UUID)).thenReturn(passKey);
    Mockito.when(request.getRequestDispatcher("/WEB-INF/jsp/misc/message.jsp")).thenReturn(dispatcher);
  }

  private void prepairRequestMessage(final String messageTitle, final String messageBody) {
    Mockito.when(request.getParameter(MESSAGE_TITLE)).thenReturn(messageTitle);
    Mockito.when(request.getParameter(MESSAGE_BODY)).thenReturn(messageBody);
  }

  private void prepairResponse(final int scOk) {
    Mockito.when(response.getStatus()).thenReturn(HttpStatus.SC_OK);
  }

}
