/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.startup;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.logging.server.RemoteLoggingServiceImpl;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.metrics.MetricFactory;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.server.util.MetricPropertiesUtil;
import nl.overheid.aerius.shared.domain.ProductType;

/**
 * Listener that is run during startup of application and can be used to initialize static variables.
 */
@WebListener
public class StartupContextListener implements ServletContextListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(StartupContextListener.class);

  private static final String APPLICATION_TITLE_KEY = "applicationTitle";

  private ExecutorService executor;

  @Override
  public void contextDestroyed(final ServletContextEvent event) {
    TaskClientFactory.shutdown();
    if (executor != null && !executor.isShutdown()) {
      executor.shutdown();
    }
  }

  @Override
  public void contextInitialized(final ServletContextEvent event) {
    // Get product name, also used for logging
    final String productName = event.getServletContext().getInitParameter(APPLICATION_TITLE_KEY);
    final ProductType productType = ProductType.determineProductType(productName);
    ServerPMF.getInstance().setProductType(productType);

    // Initialize the localized DB messages
    DBMessages.init(ServerPMF.getInstance());
    LOGGER.info("{}: Translations initialized.", productName);

    // Use a fixed thread pool because number of users is unspecified, meaning large number of users could cause problems when using a
    // cached thread pool.
    try (Connection con = ServerPMF.getInstance().getConnection()) {
      executor = Executors.newFixedThreadPool(ConstantRepository.getInteger(con, ConstantsEnum.NUMBER_OF_TASKCLIENT_THREADS));
      TaskClientFactory.init(con, executor);

      // Initialize the graphite metrics
      MetricFactory.init(MetricPropertiesUtil.fetchMetricProperties(con), productType.name());
    } catch (final SQLException exception) {
      LOGGER.error("Exception when trying to create rabbitmq connection", exception);
    }

    // Initialize the client side stacktrace deobfuscator symbolmap location.
    new RemoteLoggingServiceImpl().setSymbolMapsDirectory(event.getServletContext().getRealPath("/WEB-INF/deploy/aerius/symbolMaps/"));
    LOGGER.info("{}: SymbolMapDirectory for GWT deobfuscator set.", productName);
  }
}
