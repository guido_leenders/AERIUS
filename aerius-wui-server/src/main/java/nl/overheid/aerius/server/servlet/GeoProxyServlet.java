/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.http.HttpClientProxy;
import nl.overheid.aerius.http.HttpException;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.filter.GeoProxyFilter;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.util.HttpClientManager;

/**
 * Proxy to communicate with GeoServer.
 */
@WebServlet("/aerius-geo-wms")
public class GeoProxyServlet extends HttpServlet {

  private static final long serialVersionUID = -1843787034566344324L;

  private static final Logger LOG = LoggerFactory.getLogger(GeoProxyServlet.class);

  private static final String PARAM_SLD = "SLD";

  private static final String PARAM_REQUEST = "request";
  private static final String PARAM_REQUEST_GETCAPABILITIES = "GetCapabilities";

  @Override
  public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    final String wmsUrl;
    final String getCapabilitiesOriginalUrl;
    final String wmsProxyUrl;

    try (final Connection con = ServerPMF.getInstance().getConnection()) {
      wmsUrl = ConstantRepository.getString(con, SharedConstantsEnum.SHARED_WMS_URL);
      wmsProxyUrl = ConstantRepository.getString(con, SharedConstantsEnum.SHARED_WMS_PROXY_URL);
      getCapabilitiesOriginalUrl = ConstantRepository.getString(con, SharedConstantsEnum.GEOSERVER_CAPIBILITIES_ORIGINAL_URL);
    } catch (final SQLException e) {
      LOG.error("GeoProxyServlet fetching constant failed:", e);
      response.flushBuffer();
      return;
    }

    final List<NameValuePair> params = new ArrayList<NameValuePair>();
    /* Add SLD param first, so the caller can overwrite it, if specified in the original request. */
    if (request.getAttribute(GeoProxyFilter.ATTRIBUTE_SLD) != null) {
      params.add(new BasicNameValuePair(PARAM_SLD, (String) request.getAttribute(GeoProxyFilter.ATTRIBUTE_SLD)));
    }
    for (final String paramName : Collections.list(request.getParameterNames())) {
      params.add(new BasicNameValuePair(paramName, request.getParameter(paramName)));
    }

    final String paramsString = URLEncodedUtils.format(params, StandardCharsets.UTF_8);

    /* Check if it's a GetCapabilities request, if so we need to update the content with the current URL's (to the proxy). */
    boolean isGetCapabilitiesRequest = false;
    if (request.getParameter(PARAM_REQUEST) != null && PARAM_REQUEST_GETCAPABILITIES.equalsIgnoreCase(request.getParameter(PARAM_REQUEST))) {
      isGetCapabilitiesRequest = true;
    }

    try (CloseableHttpClient client = HttpClientManager.getHttpClient(-1)) {
      final String url = wmsUrl + paramsString;
      LOG.debug("Proxying WMS: {}", url);

      byte[] remoteContent = HttpClientProxy.getRemoteContent(client, url);

      if (isGetCapabilitiesRequest) {
        final String strContent = new String(remoteContent, StandardCharsets.UTF_8);
        remoteContent = strContent.replace(getCapabilitiesOriginalUrl, wmsProxyUrl).getBytes(StandardCharsets.UTF_8);
      }

      response.getOutputStream().write(remoteContent);
    } catch (final ParseException | HttpException | URISyntaxException e) {
      LOG.error("GeoProxyServlet proxy failed:", e);
      response.flushBuffer();
    }
  }
}
