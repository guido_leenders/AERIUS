/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.util;

import java.util.Locale;

import nl.overheid.aerius.db.i18n.AeriusMessages;
import nl.overheid.aerius.shared.domain.importer.ImportResult;

public abstract class AfterPASHandler {

  public static final String AFTERPAS_OPTION_TEMPORARY_PERIOD = "afterPASOptionTemporaryPeriod";
  public static final String AFTERPAS_OPTION_TEMPPROJECT_YEARS = "afterPASOptionTempProjectYears";
  public static final String AFTERPAS_OPTION_DISTANCE_LIMIT = "afterPASOptionDistanceLimit";
  public static final String AFTERPAS_CALCULATION_WILLNOTRUN_REMOVEOPTION = "afterPASCalculationWillNotRunRemoveOption";
  public static final String AFTERPAS_REPORTWILLNOTRUN_REMOVEOPTION = "afterPASReportWillNotRunRemoveOption";
  public static final String AFTERPAS_INVALID_INPUT_REMOVEWITHCONVERT = "afterPASNonValidInputRemoveWithConvert";
  public static final String AFTERPAS_CONVERT_INVALID_INPUT_HASBEENREMOVED = "afterPASConvertNonValidInputHasBeenRemoved";
  public static final String AFTERPAS_2B_DETERMINED = "afterPAS2BDetermined";
  public static final String AFTERPAS_READ_INSTRUCTIONS = "afterPASReadInstructions";

  protected String afterPASSpecificMessageId;
  protected AeriusMessages aeriusMessages;

  protected AfterPASHandler(final Locale locale, final String productSpecificMessageId) {
    this.afterPASSpecificMessageId = productSpecificMessageId;
    this.aeriusMessages = new AeriusMessages(locale);
  }

  public abstract void validateAll(ImportResult result);
}
