/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.servlet.annotation.WebServlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.search.SearchLocationServer;
import nl.overheid.aerius.search.SearchRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.MapSearchService;

/**
 * Implementation of the SearchService.
 */
@WebServlet("/aerius/map-aerius-search")
public class MapSearchServiceImpl extends RemoteServiceServlet implements MapSearchService {

  private static final long serialVersionUID = 544170669276332964L;

  private static final Logger LOG = LoggerFactory.getLogger(MapSearchServiceImpl.class);

  private static final int TIMEOUT_QUERY_SUGGESTIONS = 2000;
  private static final int TIMEOUT_SEARCHRESULTS = 500 + Math.max(SearchLocationServer.TIMEOUT_LOCATIESERVER,
      TIMEOUT_QUERY_SUGGESTIONS);

  // Do a full search after at least this amount of characters are entered. Otherwise search on N2000-id only.
  private static final int FULL_SEARCH_TERM_MIN_LENGTH = 3;
  // Do N2000-id searches only upto this search term length.
  private static final int N2K_ID_SEARCH_TERM_MAX_LENGTH = 3;

  private final SearchLocationServer gcSearch = new SearchLocationServer();
  private final ExecutorService executor = Executors.newCachedThreadPool();
  private final String epsgCode;

  public MapSearchServiceImpl() throws SQLException {
    final PMF pmf = ServerPMF.getInstance();
    epsgCode = EPSGProxy.getEPSG(ReceptorGridSettingsRepository.getSrid(pmf)).getEpsgCode();
  }

  /**
   * Polls the database and a geocoder for search results.
   * @param searchString The string to search for (trimmed, not-empty)
   */
  @Override
  public ArrayList<MapSearchSuggestion> getSuggestions(final String searchString) throws AeriusException {
    final ArrayList<MapSearchSuggestion> output = new ArrayList<>();
    final List<Callable<ArrayList<MapSearchSuggestion>>> tasks = new ArrayList<>();

    final PMF pmf = ServerPMF.getInstance();
    // When a small number is entered...
    searchOnSmallNumbers(pmf, searchString, tasks);

    // When enough characters entered...
    searchOnCharacters(pmf, searchString, tasks);

    performSearches(output, tasks);

    // Sort so that results are grouped in our preferred order
    sortResults(output);

    return output;
  }


  private void searchOnSmallNumbers(final PMF pmf, final String searchString, final List<Callable<ArrayList<MapSearchSuggestion>>> tasks) {
    if ((searchString.length() <= N2K_ID_SEARCH_TERM_MAX_LENGTH) && NumberUtils.isDigits(searchString)) {
      try {
        final int n2kId = Integer.parseInt(searchString);
        // ...Get the search result from that N2000-id from the database
        tasks.add(new Callable<ArrayList<MapSearchSuggestion>>() {
          @Override
          public ArrayList<MapSearchSuggestion> call() throws Exception {
            final MapSearchSuggestion n2k = SearchRepository.getN2000IdSuggestion(pmf, n2kId);
            return n2k == null ? new ArrayList<>() : new ArrayList<>(Arrays.asList(n2k));
          }
        });
      } catch (final NumberFormatException e) {
        // Do nothing. User entered a number that could not be parsed to integer (maybe too long?)
      }
    }
  }

  private void searchOnCharacters(final PMF pmf, final String searchString, final List<Callable<ArrayList<MapSearchSuggestion>>> tasks) {
    if (searchString.length() >= FULL_SEARCH_TERM_MIN_LENGTH) {
      final String geocoderUrl = ConstantRepository.getString(pmf, SharedConstantsEnum.PDOK_LOCATIESERVER_URL, false);
      // ...Do a geocoder call only if the appropriate constant is set
      if (!StringUtils.isEmpty(geocoderUrl)) {
        tasks.add(new Callable<ArrayList<MapSearchSuggestion>>() {
          @Override
          public ArrayList<MapSearchSuggestion> call() {
            return gcSearch.callLocatieServer(epsgCode, geocoderUrl, searchString);
          }
        });
      }

      // ...And do a full database call
      tasks.add(new Callable<ArrayList<MapSearchSuggestion>>() {
        @Override
        public ArrayList<MapSearchSuggestion> call() throws Exception {
          return SearchRepository.getSuggestions(pmf, searchString);
        }
      });
    }
  }

  private void performSearches(final ArrayList<MapSearchSuggestion> output, final List<Callable<ArrayList<MapSearchSuggestion>>> tasks) {
    if (!tasks.isEmpty()) {
      try {
        final List<Future<ArrayList<MapSearchSuggestion>>> taskResults = executor.invokeAll(tasks, TIMEOUT_SEARCHRESULTS,
            TimeUnit.MILLISECONDS);
        for (final Future<ArrayList<MapSearchSuggestion>> taskResult : taskResults) {
          if (!taskResult.isCancelled()) {
            output.addAll(taskResult.get());
          }
        }
      } catch (final Exception e) {
        LOG.error("getSuggestions failed.", e);
      }
    }
  }

  private void sortResults(final ArrayList<MapSearchSuggestion> output) {
    if (!output.isEmpty()) {
      Collections.sort(output, new Comparator<MapSearchSuggestion>() {
        @Override
        public int compare(final MapSearchSuggestion o1, final MapSearchSuggestion o2) {
          return Integer.compare(o1.getType().ordinal(), o2.getType().ordinal());
        }
      });
    }
  }

  @Override
  public ArrayList<MapSearchSuggestion> getSubSuggestions(final MapSearchSuggestion parentSuggestion) throws AeriusException {
    final PMF pmf = ServerPMF.getInstance();
    return SearchRepository.getSubSuggestions(pmf, parentSuggestion);
  }

  @Override
  public void destroy() {
    super.destroy();
    executor.shutdownNow();
  }
}
