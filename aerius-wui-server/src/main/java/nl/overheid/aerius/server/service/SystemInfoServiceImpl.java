/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.SystemInfoMessageRepository;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.SystemInfoService;

/**
 * Lookup the systeminfomessage string.
 */
public class SystemInfoServiceImpl implements SystemInfoService {
  private static final Logger LOG = LoggerFactory.getLogger(SystemInfoServiceImpl.class);

  private final PMF pmf;
  private final AeriusSession session;

  /**
   * Constructor for SystemInfoMessage.
   * 
   * @param pmf the persistence object.
   * @param session the session.
   */
  public SystemInfoServiceImpl(final PMF pmf, final AeriusSession session) {
    this.pmf = pmf;
    this.session = session;
  }

  @Override
  public String getSystemInfoMessage() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return SystemInfoMessageRepository.getMessage(con, session.getLocale());
    } catch (final SQLException e) {
      LOG.error("Error getSystemInfoMessage", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }
}
