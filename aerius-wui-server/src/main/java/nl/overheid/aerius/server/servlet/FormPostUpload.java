/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.sql.SQLException;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Processes uploads when the calculator/scenario is opened with a gml embedded in the page. This is done from register.
 */
public final class FormPostUpload {

  private static final Logger LOG = LoggerFactory.getLogger(FormPostUpload.class);

  private static final String UUID_PREFIX = "form_import_";

  private FormPostUpload() {
    // util class
  }

  /**
   * Static method to process a form based gml upload. If specific parameters are
   * present in the request these are processed and stored in the user session with
   * the uuid returned by this method. If no parameters present null is returned.
   *
   * @param request request.
   * @param returnSources if true return sources
   * @param persistResults if true return results
   * @param persistResults if true persist results in DB
   * @return uuid if data was processed or null if nothing was done
   */
  public static String processFormPost(final HttpServletRequest request, final boolean returnSources, final boolean persistResults) {
    final String[] payload = request.getParameterValues(SharedConstants.FORM_POST_PAYLOAD);
    String type = request.getParameter(SharedConstants.FORM_POST_TYPE);
    if (type == null) {
      type = SharedConstants.FORM_POST_TYPE_DEFAULT;
    }

    if (payload != null && payload.length > 0) {
      // We'll ignore extra GML uploads at this time
      final String payloadGMLCurrent = StringUtils.isEmpty(payload[0]) ? null : payload[0];
      final String payloadGMLProposed = payload.length < 2 || StringUtils.isEmpty(payload[1]) ? null : payload[1];

      final String uuid = UUID_PREFIX + UUID.randomUUID().toString();
      final Locale locale = Internationalizer.getLocaleFromCookie(request);
      final ImportService service = new ImportService(ServerPMF.getInstance(), locale);
      // Process the import, storing a result object in the session
      boolean success;
      try {
        success =
            service.processGMLStrings(SectorRepository.getSectorCategories(ServerPMF.getInstance(), locale), payloadGMLCurrent, payloadGMLProposed,
                request.getSession(true), uuid, returnSources, persistResults);
      } catch (final SQLException e) {
        LOG.error("Error getting calculator context", e);
        request.getSession(true).setAttribute(SharedConstants.IMPORT_SCENARIO_PREFIX + uuid, new AeriusException(Reason.SQL_ERROR));
        return null;
      }

      request.getSession(true).setAttribute(SharedConstants.IMPORT_SCENARIO_PREFIX + uuid, success ? service.getResult() : service.getServerException());
      return uuid;
    }
    return null;
  }
}
