/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service.sld;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class SLDService {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(SLDService.class);

  // The SQL queries used in this service.
  private static final String GET_LAYER_QUERY =
      "SELECT sld_wms_layer_id, wms_zoom_level_id, sld_id, target_layer_name FROM system.sld_wms_layers WHERE name = ? ";
  private static final String GET_ZOOMLEVELS_QUERY =
      "SELECT min_scale, max_scale, zoom_level "
          + " FROM system.wms_zoom_level_properties WHERE wms_zoom_level_id = ? ORDER BY min_scale ";
  private static final String GET_RULES_QUERY =
      "SELECT sld_rule_id, condition, \"fill_color\", \"stroke_color\", \"image_url\", \"custom_draw_sld\", \"custom_condition_sld\" "
          + " FROM system.sld_rules WHERE sld_id = ? ORDER BY sld_rule_id ";


  private SLDService() {
    //Not allowed to instantiate.
  }

  /**
   * Fetch WMSLayer information with the rules and zoomlevels.
   * @param con the database connection to use.
   * @param layerKey The key for the layer to obtain information for.
   * @return The WMSLayer filled with information from the database.
   * @throws SQLException On error.
   */
  public static WMSLayer getWMSLayer(final Connection con, final String layerKey) throws SQLException {
    LOG.debug("Fetching WMSlayer: {}", layerKey);
    WMSLayer wmsLayer = null;
    int zoomLevelId = 0;
    try {
      try (final PreparedStatement getLayerPS = con.prepareStatement(GET_LAYER_QUERY)) {
        getLayerPS.setString(1, layerKey);
        final ResultSet rstLayer = getLayerPS.executeQuery();

        if (rstLayer.next()) {
          final int wmsLayerId = rstLayer.getInt("sld_wms_layer_id");
          final String targetLayer = rstLayer.getString("target_layer_name");
          wmsLayer = new WMSLayer(wmsLayerId, rstLayer.getInt("sld_id"), targetLayer == null ? layerKey : targetLayer);
          zoomLevelId = rstLayer.getInt("wms_zoom_level_id");
        }
      }
      if (wmsLayer != null) {
        addZoomLevels(con, layerKey, wmsLayer, zoomLevelId);
        addRules(con, wmsLayer);
      }

      return wmsLayer;
    } catch (final SQLException se) {
      LOG.error("An SQL exception occured in getWMSLayer", se);
      throw se;
    }
  }

  public static boolean hasWMSLayer(final Connection con, final String layerKey) throws SQLException {
    try {
      try (final PreparedStatement getLayerPS = con.prepareStatement(GET_LAYER_QUERY)) {
        getLayerPS.setString(1, layerKey);
        final ResultSet rstLayer = getLayerPS.executeQuery();
        return rstLayer.next();
      }
    } catch (final SQLException se) {
      LOG.error("An SQL exception occured in hasWMSLayer", se);
      throw se;
    }
  }

  private static void addZoomLevels(final Connection con, final String layerKey, final WMSLayer wmsLayer, final int zoomLevelId) throws SQLException {
    if (zoomLevelId > 0) {
      LOG.debug("Fetching zoomlevels for ID: {} - layerKey: {}", zoomLevelId, layerKey);
      try (final PreparedStatement getZoomLevelsPS = con.prepareStatement(GET_ZOOMLEVELS_QUERY)) {
        getZoomLevelsPS.setInt(1, zoomLevelId);
        final ResultSet rstZoom = getZoomLevelsPS.executeQuery();

        while (rstZoom.next()) {
          wmsLayer.addZoomLevel(
              new WMSZoomLevel(
                  zoomLevelId,
                  rstZoom.getInt("min_scale"),
                  rstZoom.getInt("max_scale"),
                  rstZoom.getInt("zoom_level")));
        }
      }
    }
  }

  private static void addRules(final Connection con, final WMSLayer wmsLayer) throws SQLException {
    try (final PreparedStatement getRulesPS = con.prepareStatement(GET_RULES_QUERY)) {
      getRulesPS.setInt(1, wmsLayer.getSldId());
      final ResultSet rstRules = getRulesPS.executeQuery();

      while (rstRules.next()) {
        wmsLayer.addRule(
            new SLDRule(
                rstRules.getInt("sld_rule_id"),
                rstRules.getString("condition"),
                rstRules.getString("fill_color"), rstRules.getString("stroke_color"),
                rstRules.getString("image_url"),
                rstRules.getString("custom_draw_sld"), rstRules.getString("custom_condition_sld")));
      }
    }
  }
}
