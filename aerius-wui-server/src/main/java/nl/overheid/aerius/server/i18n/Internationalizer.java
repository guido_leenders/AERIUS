/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.i18n;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.i18n.AeriusMessages;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Sets the locale passed from the client in a cookie and reads it from the cookie.
 */
public final class Internationalizer {
  private static final String LANGUAGE_PARAMETER = "locale";
  /**
   * This sets the 'messages' field in the session, which is used by the jsp pages to get localized texts.
   */
  private static final String AERIUS_MESSAGES_KEY = "messages";
  /**
   * This sets the 'const' field in the session, which is used by the jsp pages to get localized texts.
   */
  private static final String AERIUS_CONST_KEY = "consts";
  private static final Logger LOG = LoggerFactory.getLogger(Internationalizer.class);
  private static final String AERIUS_LOCALE_KEY = "AERIUS_Locale";

  private Internationalizer() {
    // Util method
  }

  /**
   * Initialises the locale language in a cookie. This method should be called from the main jsp, in order to have effect.
   *
   * @param request request object
   * @param response response object
   * @return language of the locale
   */
  public static String initLanguage(final HttpServletRequest request, final HttpServletResponse response) {
    final Locale locale = getLocale(request, response);

    initMessageBundle(request, locale);

    initSessionCookie(response, locale);

    initCustomConstants(request, locale);

    return locale.getLanguage();
  }

  private static void initCustomConstants(final HttpServletRequest request, final Locale locale) {
    final HashMap<String, String> aeriusConsts = new HashMap<>();

    try (Connection con = ServerPMF.getInstance().getConnection()) {
      aeriusConsts.put("splashAttributionInfo", MessageRepository.getString(con, MessagesEnum.SPLASH_ATTRIBUTION_TEXT, locale));
      aeriusConsts.put("splashAttributionImage", MessageRepository.getString(con, MessagesEnum.SPLASH_ATTRIBUTION_IMAGE, locale));
    } catch (final SQLException e) {
      LOG.error("Could not reach database while initialising custom constants.", e);
    }

    // Set the messages in the session
    request.getSession().setAttribute(AERIUS_CONST_KEY, aeriusConsts);
  }

  private static void initMessageBundle(final HttpServletRequest request, final Locale locale) {
    // Create the AeriusMessages bundle
    final AeriusMessages aeriusMessages = new AeriusMessages(locale);

    // Set the messages in the session
    request.getSession().setAttribute(AERIUS_MESSAGES_KEY, aeriusMessages);
  }

  private static void initSessionCookie(final HttpServletResponse response, final Locale locale) {
    final Cookie cookie = new Cookie(AERIUS_LOCALE_KEY, locale.getLanguage());
    cookie.setHttpOnly(true);
    cookie.setMaxAge(Integer.MAX_VALUE);

    response.addCookie(cookie);
  }

  private static Locale getLocale(final HttpServletRequest request, final HttpServletResponse response) {
    final String langParam = request.getParameter(LANGUAGE_PARAMETER);

    Locale locale;
    if (langParam == null) {
      // Get the locale from cookie if not present in the request
      locale = getLocaleFromCookie(request, null);

      // On failure, get a default locale based on request - or take default
      if (locale == null) {
        locale = getRequestLocaleOrDefault(request, response);
      }
    } else {
      // Take the requested language or default if it does not exist
      locale = LocaleUtils.getSupportedLocaleOrDefault(langParam);
    }

    return locale;
  }

  /**
   * Sets the locale in a cookie. The locale is taken from the browser or if not no locale send by the browser the default locale will be taken.
   *
   * @param request request object
   * @param response response object
   * @return the locale set in the cookie
   */
  private static Locale getRequestLocaleOrDefault(final HttpServletRequest request, final HttpServletResponse response) {
    return request.getLocale() == null ? LocaleUtils.getDefaultLocale() : request.getLocale();
  }

  /**
   * Returns the locale from the cookie or the default if no locale set in cookie.
   *
   * @param request request
   * @return locale object
   */
  public static Locale getLocaleFromCookie(final HttpServletRequest request) {
    return getLocaleFromCookie(request, LocaleUtils.getDefaultLocale());
  }

  private static Locale getLocaleFromCookie(final HttpServletRequest request, final Locale defaultLocale) {
    Locale locale = defaultLocale;
    final Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (final Cookie cookie : cookies) {
        if (AERIUS_LOCALE_KEY.equals(cookie.getName()) && (cookie.getValue() != null)) {
          locale = LocaleUtils.getLocale(cookie.getValue());
          break;
        }
      }
    }
    return locale;
  }
}
