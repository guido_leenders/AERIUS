/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.auth;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * This filter validates the NetIQ authentication service to authenticate the user.
 * To configure place the following in {@code shiro.ini}:
 * <pre>
 * [main]
 * netIQFilter = nl.overheid.aerius.server.auth.NetIQFilter
 * netIQRealm = nl.overheid.aerius.server.auth.NetIQRealm
 * </pre>
 */
public class NetIQFilter extends AuthenticatingFilter {
  private static final Logger LOG = LoggerFactory.getLogger(NetIQFilter.class);

  private static final String EMAIL = "X-Mail";

  @Override
  protected AuthenticationToken createToken(final ServletRequest request, final ServletResponse response) throws Exception {
    final AuthenticationToken token;
    if (request instanceof HttpServletRequest) {
      final HttpServletRequest hsr = (HttpServletRequest) request;
      token = new NetIQToken(hsr.getHeader(EMAIL));
    } else {
      token = null;
    }
    return token;
  }

  @Override
  protected boolean onAccessDenied(final ServletRequest request, final ServletResponse response) throws Exception {
    final boolean loggedIn;
    LOG.trace("onAccessDenied");
    final Subject subject = getSubject(request, response);
    if (LOG.isTraceEnabled()) {
      LOG.trace("subject:{}", subject);
    }
    if (subject.getPrincipal() == null || !subject.isAuthenticated()) {
      loggedIn = onLoginSuccess(createToken(request, response), subject, request, response);
    } else {
      loggedIn = true;
    }
    return loggedIn;
  }

  @Override
  protected boolean onLoginSuccess(final AuthenticationToken token, final Subject subject, final ServletRequest request,
      final ServletResponse response) throws Exception {
    if (LOG.isTraceEnabled()) {
      LOG.trace("token:{}", token);
    }
    if (token instanceof NetIQToken) {
      checkUser(((NetIQToken) token).getEmail());
      LOG.trace("User passed check!");
      subject.login(token);
      return true;
    }
    LOG.error("Unknown token, this should not happen! Token: {}", token);
    throw new AeriusException(Reason.AUTHORIZATION_ERROR);
  }

  /**
   * Checks if the user exists in the database and is enabled. If something is wrong an exception is thrown.
   * @param email email passed.
   * @throws AeriusException exception thrown when something is wrong.
   */
  private void checkUser(final String email) throws AeriusException {
    if (email == null || email.isEmpty()) {
      throw new AeriusException(Reason.AUTHORIZATION_ERROR);
    } else {
      final AdminUserProfile up;
      try (Connection con = getDBConnection()) {
        up = UserRepository.getAdminUserProfileByName(con, email);
        if (LOG.isTraceEnabled()) {
          LOG.trace("userprofile:{}" + up);
        }
      } catch (final SQLException e) {
        LOG.error("SQL exception when checking user", e);
        throw new AeriusException(Reason.AUTHORIZATION_ERROR);
      }
      if (up == null || !up.isEnabled()) {
        throw new AeriusException(Reason.AUTHORIZATION_ERROR);
      }
    }
  }

  protected Connection getDBConnection() throws SQLException {
    return ServerPMF.getInstance().getConnection();
  }
}
