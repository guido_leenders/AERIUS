/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import nl.overheid.aerius.server.i18n.Internationalizer;

/**
 * Basic implementation of RegisterSession. Subclasses only need to provide a method for retrieving the HttpServletRequest.
 */
public abstract class AbstractSession implements AeriusSession {
  @Override
  public final HttpSession getSession() {
    return getRequest().getSession();
  }

  @Override
  public final Locale getLocale() {
    return Internationalizer.getLocaleFromCookie(getRequest());
  }

  @Override
  public final void close() {
    final HttpSession session = getRequest().getSession(false);

    if (session != null) {
      session.invalidate();
    }
  }

  /**
   * @return The current http request.
   */
  public abstract HttpServletRequest getRequest();
}
