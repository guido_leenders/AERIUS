/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class BaseUploadService {

  private static final Logger LOG = LoggerFactory.getLogger(BaseUploadService.class);

  private AeriusException serverException;
  private Part part;
  private String fileName;

  public boolean processUploadDocument(final HttpServletRequest request) {
    boolean processed = false;
    try {
      this.part = getFileContentPart(request);
      this.fileName = getFilename(part);
      processed = true;
    } catch (final ImportException e) {
      // logging already done. errors accessible via #getServerException().
    }
    return processed;
  }

  public String getFileName() {
    return fileName;
  }

  public Part getPart() {
    return part;
  }

  public AeriusException getServerException() {
    return serverException;
  }

  private Part getFileContentPart(final HttpServletRequest request) throws ImportException {
    // Retrieve the file Part from request.
    Part part = null;
    try {
      part = request.getPart(SharedConstants.IMPORT_FILE_FIELD_NAME);
      // If there is no part or its size is 0, no import file was supplied
      if (part == null || part.getSize() == 0) {
        saveImportError(Reason.IMPORT_FILE_NOT_SUPPLIED, null);
      }
    } catch (final IOException e) {
      saveImportError(Reason.IMPORT_FILE_COULD_NOT_BE_READ, e);
    } catch (final IllegalStateException e) {
      saveImportError(Reason.IMPORT_FILE_COULD_NOT_BE_READ, e);
    } catch (final ServletException e) {
      saveImportError(Reason.IMPORT_FILE_COULD_NOT_BE_READ, e);
    }
    return part;
  }

  public void saveImportError(final Reason errorCode, final Exception e) throws ImportException {
    saveImportError(new AeriusException(errorCode), e);
  }

  public void saveImportError(final AeriusException serverException, final Exception e) throws ImportException {
    this.serverException = serverException;
    final ImportException importException = new ImportException();
    if (e != null) {
      try {
        LOG.error("Error importing file: {}", fileName, e);
      } catch (final IllegalStateException e1) {
        LOG.error("Error during logging import error", e);
      }
    }
    throw importException;
  }

  /**
   * Sends a response back to the client. The result will be of the following format:
   * <ul>
   * <li>On success: SUCCESS-_-[UUID]
   * <li>On Failure: ERROR-_-[UUID]
   * </ul>
   *
   * @param response HttpServletResponse
   * @param success true if processing of upload succeeded, if false processing failed
   * @param uuid uuid with which the client can retrieve the associated content back from the server
   */
  public void sendResponse(final HttpServletResponse response, final boolean success, final String uuid) {
    // Return the key to the user(setting content-type to text/html)
    // Otherwise the browser will wrap the returned data in a <pre> tag or comparable

    response.setContentType("text/html;charset=" + StandardCharsets.UTF_8);
    try {
      response.getWriter().write((success ? SharedConstants.IMPORT_SUCCESS_PREFIX
          : SharedConstants.IMPORT_ERROR_PREFIX) + SharedConstants.IMPORT_ARGUMENT_SEPERATOR + uuid);
      response.flushBuffer();
    } catch (final IOException e) {
      LOG.error("[sendError] Exception while writing an error response.", e);
    }
  }

  /**
   * @param part The part to determine the filename for.
   * @return The filename (or null if not able to determine).
   */
  private static String getFilename(final Part part) {
    return getFilename(getContentDisposition(part));
  }

  /**
   * It appears parsing the file name out of the Part headers is the only way...
   */
  private static String getFilename(final String contentDisposition) {
    for (final String cd : contentDisposition.split(";")) {
      if (cd.trim().startsWith("filename")) {
        final String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
        return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
      }
    }
    return null;
  }

  private static String getContentDisposition(final Part part) {
    return part.getHeader("content-disposition");
  }

  /**
   * Error thrown when an import fails. This exception is only intended to break
   * the current program flow, as the actual exception is catched and stored in
   * this class.
   */
  protected static class ImportException extends Exception {

    private static final long serialVersionUID = 6040524589880681871L;

    ImportException() {
    }
  }

}
