/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import javax.servlet.http.HttpServletRequest;

/**
 * A service registry is responsible for managing services and mapping incoming http requests to services.
 */
public interface ServiceRegistry {

  /**
   * Finds the service to handle the request.
   * 
   * @param request
   *          The http request to find a service for.
   * @return The service to handle the request, or null if none was found
   */
  Object findService(HttpServletRequest request);
}
