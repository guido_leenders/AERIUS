/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server;

import javax.servlet.annotation.WebServlet;

import com.google.gwt.logging.server.RemoteLoggingServiceImpl;

/**
 * Wrapper class for GWT remote logging that adds an annotation so we don't have to add the servlet configuration to the web.xml.
 */
@WebServlet("/aerius/remote_logging")
public class AeriusRemoteLoggingServiceImpl extends RemoteLoggingServiceImpl {

  private static final long serialVersionUID = 4826206602229801819L;

  // No specific implementation
}
