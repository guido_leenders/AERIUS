/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.filter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.servlet.InstrumentedFilter;

@WebFilter(urlPatterns = { "/*" }, initParams = @WebInitParam(name = "name-prefix", value = "RequestFilter"))
public class InstrumentedMetricsFilter extends InstrumentedFilter {
  private static final Logger LOG = LoggerFactory.getLogger(InstrumentedMetricsFilter.class);

  public InstrumentedMetricsFilter() {
    LOG.info("InstrumentedMetricsFilter initialised.");
  }
}
