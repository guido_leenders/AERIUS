/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ContextRepository;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.server.util.AfterPASErrorsHandler;
import nl.overheid.aerius.server.util.AfterPASHandler;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Service class to process import files from a Form post. To use create an instance
 * of this class and retrieve the error or result from the member field.
 */
public class ImportService extends BaseUploadService {

  private final PMF pmf;
  private final Locale locale;
  private ImportResult result;

  public ImportService(final PMF pmf, final Locale locale) {
    this.pmf = pmf;
    this.locale = locale;
  }

  public ImportResult getResult() {
    return result;
  }

  /**
   * Process an import request and handles the error reporting. The process stores
   * the results or error in field members and returns a boolean with the state
   * if it was successful or if it failed.
   * @param request HttpServletRequest with the form data
   * @param categories sector categories
   * @param uo Upload options
   * @return Returns the boolean indicating if the upload succeeded or not
   * @throws ImportException
   * @throws IOException
   */
  public boolean processUpload(final HttpServletRequest request, final String uuid, final UploadOptions uo) {
    boolean result = false;
    try {
      result = processUploadDocument(request) && processUpload(request, uuid, uo, getPart().getInputStream());
    } catch (final IOException e) {
      try {
        saveImportError(Reason.INTERNAL_ERROR, e);
      } catch (final ImportException ie) {
        result = false;
      }
    }
    return result;
  }

  public boolean processUpload(final HttpServletRequest request, final String uuid, final UploadOptions uo,
      final InputStream inputStream) {
    return processUpload(request.getSession(true), uuid, getSubstanceParameter(request), uo, inputStream);
  }

  private boolean processUpload(final HttpSession session, final String uuid, final String sub, final UploadOptions uo,
      final InputStream inputStream) {
    boolean result = false;
    // Parse the file
    try {
      try (InputStream bis = new BufferedInputStream(inputStream)) {
        result = parseFile(session, uuid, getFileName(), bis, getSubstance(sub), uo);
      } catch (final IOException e) {
        saveImportError(Reason.INTERNAL_ERROR, e);
      }
    } catch (final ImportException e) {
      result = false;
    }
    return result;
  }

  /**
   * Process GML(s) passed and stores the results in this class. If parsing
   * was successful true is returned else false. In case of success the result is
   * in {@link #result}. In case of a failure the error is in {@link #serverException}.
   * @param categories sector categories
   * @param currentGML The current GML content (if present).
   * @param proposedGML The proposed GML content (always present).
   * @param returnSources return sources
   * @param persistResults persist results
   * @return true if content successful parsed, false otherwise.
   */
  public boolean processGMLStrings(final SectorCategories categories, final String currentGML, final String proposedGML,
      final HttpSession session, final String uuid, final boolean returnSources, final boolean persistResults) {
    boolean result = false;

    try {
      // Use a dummy filename, append it with the file type specified (which is ZIP, as we are faking a ZIP).
      // Fake it till you make it!
      // We are using a ZIP as otherwise we cannot import a current and proposed GML at this point. Item #1691 is introduced
      //  to fix this later on. We could also only use a ZIP if there is also a current GML present, but we won't as Hilbrand
      //  doesn't like premature optimizations. Most GML's will be small anyway, though a GML with results can be imported
      //  theoretically. Those can be huge.
      final String dummyFileName = UUID.randomUUID().toString() + FileFormat.ZIP.getDottedExtension();
      final ByteArrayOutputStream zipByteArray = new ByteArrayOutputStream();

      try (final ZipFileMaker zipMaker = new ZipFileMaker(zipByteArray)) {
        if (!StringUtils.isEmpty(currentGML)) {
          zipMaker.add(UUID.randomUUID().toString() + FileFormat.GML.getDottedExtension(), currentGML);
        }
        if (!StringUtils.isEmpty(proposedGML)) {
          zipMaker.add(UUID.randomUUID().toString() + FileFormat.GML.getDottedExtension(), proposedGML);
        }
      }

      final UploadOptions uo = new UploadOptions();
      uo.setReturnSources(returnSources);
      uo.setPersistResults(persistResults);
      try (InputStream is = new ByteArrayInputStream(zipByteArray.toByteArray())) {
        // FIXME: might need to disable checks here, but for now doesn't really seem to be used
        result = parseFile(session, uuid, dummyFileName, is, null, uo);
      } catch (final IOException e) {
        saveImportError(Reason.INTERNAL_ERROR, e);
      }
    } catch (final ImportException | IOException e) {
      result = false;
    }
    return result;
  }

  private Substance getSubstance(final String substanceParameter) throws ImportException {
    // Validate and retrieve form values
    Substance sub = null;
    if (substanceParameter != null) {
      try {
        sub = Substance.substanceFromId(Integer.parseInt(substanceParameter));
        if (sub == null) {
          saveImportError(Reason.FAULTY_REQUEST, new NullPointerException());
        }
      } catch (final NumberFormatException e) {
        saveImportError(Reason.FAULTY_REQUEST, e);
      }
    }
    return sub;
  }

  private String getSubstanceParameter(final HttpServletRequest request) {
    return request.getParameter(SharedConstants.IMPORT_SUBSTANCE_FIELD_NAME);
  }

  /**
   * Parse an input stream containing the data. The type of data is based on the
   * extension of the filename. For OPS source files the parameter sub (from substance)
   * is only relevant, for other format this can be null.
   *
   * @param session
   * @param uuid
   * @param filename
   * @param inputStream
   * @param sub
   * @param uo upload options
   * @return returns true if file successful parsed
   * @throws ImportException
   */
  private boolean parseFile(final HttpSession session, final String uuid, final String filename, final InputStream inputStream, final Substance sub,
      final UploadOptions uo)
      throws ImportException {
    boolean result = false;
    try {
      // determine what type of import it is.
      final ImportType type = ImportType.determineByFilename(filename);

      if (type == null) {
        saveImportError(Reason.IMPORT_FILE_UNSUPPORTED, null);
      } else if ((uo.getSupportedFormats() != null) && !SupportedUploadFormat.isSupported(uo.getSupportedFormats(), type)) {
        saveImportError(Reason.IMPORT_FILE_TYPE_NOT_ALLOWED, null);
      }

      final CalculatorLimits limits;
      try (Connection con = pmf.getConnection()) {
        limits = ContextRepository.getCalculatorLimits(con);
      }
      final Importer importer = new Importer(pmf, locale);
      importer.setUseValidMetaData(uo.isValidateMetadata());
      importer.setIncludeResults(uo.isReturnSources());
      importer.setValidateAgainstSchema(uo.isValidate());
      // based on the type we're importing, set the right attribute value.
      this.result = importer.convertInputStream2ImportResult(filename, inputStream, sub, limits);
      // Calculator import warning.
      validateAfterPAS(uuid);

      // return a success line (for client to act upon)
      result = true;
    } catch (final ImportException e) {
      throw e;
    } catch (final AeriusException e) {
      saveImportError(e, e);
    } catch (final Exception e) {
      saveImportError(Reason.INTERNAL_ERROR, e);
    }
    return result;
  }

  /**
   * @param uuid
   */
  private void validateAfterPAS(final String uuid) {
    final String messageKey;

    if (uuid.contains("_connect_")) {
      messageKey = AfterPASHandler.AFTERPAS_CALCULATION_WILLNOTRUN_REMOVEOPTION;
    } else {
      messageKey = AfterPASHandler.AFTERPAS_READ_INSTRUCTIONS;
    }
    final AfterPASHandler afterPASHandler = new AfterPASErrorsHandler(locale, messageKey);
    afterPASHandler.validateAll(this.result);
  }

  /**
   * Options to consider when importing a file.
   */
  public static final class UploadOptions {
    private SupportedUploadFormat supportedFormats;
    private boolean validateMetadata;
    private boolean returnSources = true;
    private boolean persistResults;
    private boolean validate = true;
    private boolean createGeoLayers;

    public boolean isValidateMetadata() {
      return validateMetadata;
    }

    public void setValidateMetadata(final boolean validateMetadata) {
      this.validateMetadata = validateMetadata;
    }

    public boolean isReturnSources() {
      return returnSources;
    }

    public void setReturnSources(final boolean returnSources) {
      this.returnSources = returnSources;
    }

    public boolean isValidate() {
      return validate;
    }

    public void setValidate(final boolean validate) {
      this.validate = validate;
    }

    public SupportedUploadFormat getSupportedFormats() {
      return supportedFormats;
    }

    public void setSupportedFormats(final SupportedUploadFormat supportedFormats) {
      this.supportedFormats = supportedFormats;
    }

    public boolean isCreateGeoLayers() {
      return createGeoLayers;
    }

    public void setCreateGeoLayers(final boolean createGeoLayers) {
      this.createGeoLayers = createGeoLayers;
    }

    public boolean isPersistResults() {
      return persistResults;
    }

    public void setPersistResults(final boolean persistResults) {
      this.persistResults = persistResults;
    }
  }
}
