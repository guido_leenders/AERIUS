/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.i18n.AeriusMessages;
import nl.overheid.aerius.db.i18n.SystemInfoMessageRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Servlet to update database fields to show or hide system messages.
 * use url post command to update the database value
 */
@WebServlet("/aerius-system-info-update")
public class SystemInfoUpdateServlet extends HttpServlet {


  private static final long serialVersionUID = 7935830861005084874L;

  private static final Logger LOG = LoggerFactory.getLogger(SystemInfoUpdateServlet.class);

  private static final String CHECKBOX_SELECTED_VALUE = "on";
  private static final String UUID = "uuid";
  private static final String LOCALE = "locale";
  private static final String MESSAGE = "message";
  private static final String CLEAR_MESSAGE = "clearmessage";

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
    try {
      request.getRequestDispatcher("/WEB-INF/jsp/system/systeminfo.jsp").forward(request, response);
    } catch (final ServletException | IOException unkownHost) {
      LOG.error("SystemMessageUpdateServlet redirect to systeminfo.jsp failed:", unkownHost);
    }
  }

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    final PMF pmf = getPmf();

    try (final Connection con = pmf.getConnection()) {
      final String uuid = ConstantRepository.getString(con, SharedConstantsEnum.SYSTEM_INFO_PASSKEY);
      final Locale locale = LocaleUtils.getSupportedLocaleOrDefault(request.getParameter(LOCALE));
      if (uuid == null || uuid.length() == 0) {
        LOG.info("SystemMessageUpdateServlet update not allowd no UUID in database update message message({}):'{}'",
            request.getParameter(LOCALE), request.getParameter(MESSAGE));
        setError(request);
      } else if (request.getParameter(UUID) != null && request.getParameter(UUID).equals(uuid)) {
        LOG.info("SystemMessageUpdateServlet update message message({}):'{}'", request.getParameter(LOCALE), request.getParameter(MESSAGE));
        if (getClearMessageValue(request.getParameter(CLEAR_MESSAGE))) {
          SystemInfoMessageRepository.clearAllMessages(con);
        } else {
          SystemInfoMessageRepository.updateMessage(con, request.getParameter(MESSAGE), locale);
        }
        setSuccess(request);
      } else {
        setError(request);
      }
    } catch (final SQLException e) {
      LOG.error("SystemMessageUpdateServlet update system info message failed:", e);
      setError(request);
    }
    try {
      request.getRequestDispatcher("/WEB-INF/jsp/misc/message.jsp").forward(request, response);
    } catch (final ServletException | IOException unkownHost) {
      LOG.error("SystemMessageUpdateServlet redirect to message.jsp failed:", unkownHost);
    }
  }

  private boolean getClearMessageValue(final String checkBoxValue) {
    boolean selectedValue = false;
    if (checkBoxValue != null && CHECKBOX_SELECTED_VALUE.contentEquals(checkBoxValue.toLowerCase())) {
      selectedValue = true;
    }
    return selectedValue;
  }

  private void setSuccess(final HttpServletRequest request) {
    request.setAttribute("messageTitle", getMessages(request).getString("systemInfoEditorSentTitle"));
    request.setAttribute("messageBody", getMessages(request).getString("systemInfoEditorSentText"));
  }

  private void setError(final HttpServletRequest request) {
    setError(request, "systemInfoEditorErrorTitle", "systemInfoEditorErrorText");
  }

  private void setError(final HttpServletRequest request, final String titleKey, final String bodyKey) {
    request.setAttribute("messageTitle", getMessages(request).getString(titleKey));
    request.setAttribute("messageBody", getMessages(request).getString(bodyKey));
    request.setAttribute("isError", true);
  }

  private AeriusMessages getMessages(final HttpServletRequest request) {
    return new AeriusMessages(Internationalizer.getLocaleFromCookie(request));
  }

  protected PMF getPmf() {
    return ServerPMF.getInstance();
  }
}
