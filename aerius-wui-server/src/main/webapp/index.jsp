<%@page import="nl.overheid.aerius.server.i18n.Internationalizer"
%><% final String lang = Internationalizer.initLanguage(request, response);%><!doctype html>
<html style="height:100%;" lang="<%=lang%>">
<head><title>AERIUS - ${initParam.applicationTitle}</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=0.6, max-scale=1, user-scalable=yes" />
<meta name="gwt:property" content="locale=<%=lang%>">
<link rel="shortcut icon" href="favicon.ico" />
<link type="text/css" rel="stylesheet" href="fonts/rijksoverheidfonts.css">
<%if(!"Melding".equals(config.getInitParameter("applicationTitle"))){%><script src="aerius/js/gwt-openlayers/util.js"></script>
<script src="aerius/OpenLayers.js"></script>
<script src="aerius/proj4js-compressed.js"></script><% } %>
<script src="aerius/aerius.nocache.js"></script><%@include file="WEB-INF/jsp/module_head.jsp" %><%@include file="WEB-INF/jsp/analytics.jsp"%><%@include file="WEB-INF/jsp/splash/splash_inline_css.jsp" %>
<style type="text/css" id="moduleStyle"><%@include file="WEB-INF/jsp/splash/module_style.css" %>
</style>
</head>
<body style="margin:0px;height:100%;background: #9ca8a8 url(images/${messages['backgroundImage']}) no-repeat center;" >
<%@include file="WEB-INF/jsp/splash/splash.jsp" %>
</body>
</html>
