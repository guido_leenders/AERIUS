<%@ page import="nl.overheid.aerius.AeriusVersion"%>

<div id="splash">
  <div class="container">
    <div class="splash">
      <div class="logo" title="AERIUS�"></div>
      <div id="loadingAnimation" class="animation"></div>
      <div class="texts">
        <p class="hold" id="statusText">${messages['splashLoadingText']}</p>
        <p class="version">
          ${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>
      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="min" title="${messages['splashOwner']}">
      ${messages['splashOwnerImage']}
    </div> 
    <div class="message errorMessage">
      <h2 id="message"></h2>
      <p style="margin-top: 15px">${messages['splashSorryError']}</p>
    </div>
    <noscript>
      <div class="message errorMessage" style="display: block; margin-top: -20px">
        <h2>${messages['splashNoJavaScript']}</h2>
      </div>
    </noscript>
    <div class="browsers message long">
      <h2>${messages['splashBrowserIncompatible']}</h2>
      <p>${messages['splashBrowserDownloadText']}</p>
      <p>
        <a href="http://www.google.com/chrome" class="btn">Chrome</a> <a href="http://support.apple.com/downloads/#safari" class="btn">Safari</a> <a href="http://www.mozilla.org/firefox/" class="btn">Firefox</a> <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" class="btn">Internet Explorer</a>
      </p>
    </div>
    <div class="attribution">
      <p>${consts['splashAttributionInfo']}</p>
    </div>
    <div class="message provinces">
      ${consts['splashAttributionImage']}
    </div>
  </div>
</div>
