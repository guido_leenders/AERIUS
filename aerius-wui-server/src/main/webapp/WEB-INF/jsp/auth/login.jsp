<%@page import="nl.overheid.aerius.server.i18n.Internationalizer"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%Internationalizer.initLanguage(request, response);%><!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${messages['loginPageTitle']}</title>
<link href="${pageContext.request.contextPath}/css/splash.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<%@ include file="/WEB-INF/jsp/auth/module_style.css" %>
</style>
</head>
<body style="margin:0px;height:100%;background: #9ca8a8 url(images/${messages['backgroundImage']}) no-repeat center;" >
  <div class="container">
    <div class="splash">
      <div title="AERIUS Calculator" class="logo"></div>
      <div class="textsmall">
        <p class="version">${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>
      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="min" title="${messages['splashOwner']}">
      ${messages['splashOwnerImage']}
    </div> 
    <div class="message loginform">
      <div class="text-left">
        <h2>${messages['loginTitle']}</h2>
        <p>${messages['loginTextIfNotRegistered']}</p>
        <p class="expand">
          <a href="forgotten" id="gwt-debug-forgotten">${messages['loginPasswordForgotten']}</a>
        </p>
      </div>
      <div class="text-right">
        <form method="post" action="">
          <input type="text" id="gwt-debug-userName" name="username" placeholder="${messages['loginUsername']}" /><input type="password" id="gwt-debug-password" name="password" placeholder="${messages['loginPassword']}" /><input type="hidden" name="rememberMe" value="true" /><input type="submit" id="gwt-debug-Logon" value="${messages['loginSubmit']}" />

          <c:if test="${requestScope['shiroLoginFailure'] != null}">
            <p class="error">${messages['loginError']}</p>
          </c:if>
        </form>
      </div>
    </div>
    <div class="login attribution">
      <p>${consts['splashAttributionInfo']}</p>
    </div>
    <div class="message provinces">
      ${consts['splashAttributionImage']}
    </div>
  </div>
</body>
</html>
