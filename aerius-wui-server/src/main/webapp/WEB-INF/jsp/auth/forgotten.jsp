<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${messages['signupPageTitle']}</title>
<link href="${pageContext.request.contextPath}/css/splash.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<%@ include file="/WEB-INF/jsp/auth/module_style.css" %>
</style>
</head>
<body>
  <div class="container login">
    <div class="splash">
      <div title="AERIUS" class="logo"></div>
      <div class="textsmall">
        <p class="version">${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>

      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="min withform">
      <img src="${pageContext.request.contextPath}/images/min-lnv.png" alt="${messages['splashOwner']}" width="590" height="51" />
    </div>
    <div class="message">
      <div class="text-left">
        <h2>${messages['passwordForgottenTitle']}</h2>
        <p>${messages['passwordForgottenText']}</p>
        <p class="expand">${messages['passwordForgottenRemembered']} <a href="login" id="gwt-debug-forgotten-back-to-login">${messages['signupLinkLoginHref']}</a><p>
      </div>
      <div class="text-right">
        <form method="post" action="">
          <input type="text" name="email" id="gwt-debug-forgotten-email" placeholder="${messages['passwordForgottenEmail']}" /> <input type="submit" id="gwt-debug-forgotten-submit" value="${messages['passwordForgottenSubmit']}" />
          
          <!-- Temporary -->
          <c:if test="${requestScope['error'] != null}">
            <p class="error">${messages['passwordForgottenError']}</p>
          </c:if>
          <c:if test="${requestScope['success'] == true}">
            <p class="success">${messages['passwordForgottenSuccess']}</p>
          </c:if>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
