<%@page import="nl.overheid.aerius.server.i18n.Internationalizer"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%Internationalizer.initLanguage(request, response);%><!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${messages['systemInfoEditorPageTitle']}</title>
<link href="${pageContext.request.contextPath}/css/splash.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<%@ include file="/WEB-INF/jsp/system/module_style.css" %>
</style>
</head>
<body style="margin:0px;height:100%;background: #9ca8a8 url(images/${messages['backgroundImage']}) no-repeat center;" >
  <div class="container">
    <div class="splash">
      <div title="AERIUS" class="logo"></div>
      <div class="textsmall">
        <p class="version">${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>
      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="min" title="${messages['splashOwner']}">
      ${messages['splashOwnerImage']}
    </div>
    <div class="message loginform">
      <div class="text-left">
        <h2>${messages['systemInfoEditorTitle']}</h2>
        <p>${messages['systemInfoEditorBody']}</p>
      </div>
      <div class="text-right">
        <form method="post" action="">
          <input type="text" id="gwt-debug-message" name="message" placeholder="${messages['systemInfoEditorMessage']}" />
          <input type="text" maxlength="2" id="gwt-debug-locale" name="locale" placeholder="${messages['systemInfoEditorLocale']}" />
          <p><input type="checkbox" id="gwt-debug-delete-all" name="clearmessage" placeholder="info" /> ${messages['systemInfoEditorClearMessage']}</p>
          <input type="text" maxlength="36" id="gwt-debug-uuid" name="uuid" placeholder="${messages['systemInfoEditorPasskey']}" />
          <input type="submit" id="gwt-debug-submit" value="${messages['systemInfoEditorSubmit']}" />
        </form>
      </div>
    </div>
    <div class="login attribution">
      <p>${consts['splashAttributionInfo']}</p>
    </div>
    <div class="message provinces">
      ${consts['splashAttributionImage']}
    </div>
  </div>
</body>
</html>
