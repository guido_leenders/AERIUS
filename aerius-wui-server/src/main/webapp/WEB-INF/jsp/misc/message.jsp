<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${messages['signupPageTitle']}</title>
<link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/splash.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<%@ include file="/WEB-INF/jsp/auth/module_style.css" %>
.errorMessage {
    padding: 0px;
}
</style>
</head>
<body>
  <div class="container login">
    <div class="splash">
      <div title="AERIUS" class="logo"></div>
      <div class="textsmall">
        <p class="version">
          ${messages['splashVersionPrefix']}
          <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>
      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="min" title="${messages['splashOwner']}">
      ${messages['splashOwnerImage']}
    </div>
    <c:choose>
      <c:when test="${!isError}">
        <div class="message">
      </c:when>
      <c:otherwise>
        <div class="message errorMessage">
      </c:otherwise>
    </c:choose>
    <h2 id="message">${messageTitle}</h2>
    <p>${messageBody}</p>
  </div>
  </div>
</body>
</html>
