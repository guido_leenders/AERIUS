<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>403 | AERIUS - ${initParam.applicationTitle}</title>
<link href="${pageContext.request.contextPath}/css/splash.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<%@ include file="/WEB-INF/jsp/splash/module_style.css" %>
</style>
</head>

<body>
<div id="splash">
  <div class="container">
    <div class="splash">
      <div class="logo" title="AERIUS�"></div>
      <div class="textsmall">
        <p class="version">${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>
      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="message errorMessage">
      <h2 id="message">${messages['errorForbidden']}</h2>
    </div>
  </div>
</div>
</body>
</html>
