<%@page isErrorPage="true" import="nl.overheid.aerius.server.i18n.Internationalizer,nl.overheid.aerius.shared.exception.AeriusException,nl.overheid.aerius.shared.i18n.AeriusExceptionMessages"
%><%Internationalizer.initLanguage(request, response);%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="height:100%;" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>AERIUS - ${initParam.applicationTitle}</title>
<%@include file="/WEB-INF/jsp/splash/splash_inline_css.jsp" %>
<style type="text/css">
<%@ include file="/WEB-INF/jsp/splash/module_style.css" %>
</style>
</head>

<body style="margin:0px;height:100%;background: #9ca8a8 url(images/${messages['backgroundImage']}) no-repeat center;" >
<div id="splash" class="error">
  <div class="container">
    <div class="splash">
      <div class="logo" title="AERIUS�"></div>
      <div class="textsmall">
        <p class="version">${messages['splashVersionPrefix']} <%=nl.overheid.aerius.AeriusVersion.getFriendlyVersionNumber()%></p>
      </div>
      <p class="dev">${messages['splashDevelopmentInfo']}</p>
    </div>
    <div class="min" title="${messages['splashOwner']}">
      ${messages['splashOwnerImage']}
    </div> 
    <div class="message errorMessage">
      <h2 id="message"><%=new AeriusExceptionMessages(Internationalizer.getLocaleFromCookie(request)).getString(exception) %></h2>
    </div>
  </div>
</div>
</body>
</html>
