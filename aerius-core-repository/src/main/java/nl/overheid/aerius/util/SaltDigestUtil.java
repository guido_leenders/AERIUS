/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.bitcoin.core.Base58;

/**
 * <p>Handles digesting of a plain-text {@link String} securely using a salt.</p>
 *
 * <p>Takes a salt and hashes a given {@link String} with it. Formats the output
 * and encodes it in {@link Base58}</p>
 *
 * <p>The resulting String given by this util may be stored as-is. It may then be used to
 * verify if any given plain text String is equal (with a non-coincidentally high probability) to
 * its previously hashed form.</p>
 */
public final class SaltDigestUtil {

  private static final Logger LOG = LoggerFactory.getLogger("SaltDigestUtil");

  /**
   * <p>Seperator String used to seperate encoded salt and encoded plain text string.</p>
   *
   * <p>This seperator must contain characters that are not used in Base58.</p>
   *
   * <p>See <a href="http://en.wikipedia.org/wiki/Base58">this</a> for the index table.</p>
   */
  private static final String SEPERATOR = ":";

  private SaltDigestUtil() {
  }

  /**
   * Extract the salt out of a Base58 encoded hashed secret and return it.
   *
   * @param combinedSecret Base58 encoded hash of a secret and a salt.
   *
   * @return Plain-text salt, contained in the combinedSecret.
   */
  public static String extractSalt(final String combinedSecret) {
    final String[] decodedParts = new String(Base58.decode(combinedSecret)).split(SEPERATOR, 2);

    return new String(Base58.decode(new String(decodedParts[1].getBytes())));
  }

  /**
   * Verifies if the given hashed and salted string is equal to an unhashed form.
   * @throws DecoderException
   */
  public static boolean isEqual(final String digested, final String plain) throws NoSuchAlgorithmException, DecoderException {
    return combineSecret(plain, extractSalt(digested)).equals(digested);
  }

  /**
   * Hashes the given plain text {@link String} using the given salt bytes.
   *
   * @param secret A {@link String}
   * @param salt Salt bytes
   *
   * @return Hashed, encoded and formatted form of the given 2 inputs.
   *
   * @throws NoSuchAlgorithmException Occurs when an digesting algorithm is missing.
   */
  public static String combineSecret(final String secret, final String salt) throws NoSuchAlgorithmException {
    if (secret == null) {
      throw new AssertionError("Secret must not be null");
    }
    if (salt == null) {
      throw new AssertionError("Salt must not be null");
    }
    if (salt.isEmpty()) {
      throw new AssertionError("Salt bytes must not be empty.");
    }

    // Concatenate plain-text bytes with salt bytes
    final byte[] passwordBytes = ArrayUtils.addAll(secret.getBytes(), salt.getBytes());

    // Construct password parts, encoding in Hex
    final String hashedPassword = new String(Base58.encode(digestBytes(passwordBytes)));
    final String encodedSalt = new String(Base58.encode(salt.getBytes()));
    final String encodedPasswordSalt = hashedPassword + SEPERATOR + encodedSalt;

    // Finally, return it
    return new String(Base58.encode(encodedPasswordSalt.getBytes()));
  }

  public static byte[] digestBytes(final byte[] bytes) throws NoSuchAlgorithmException {
    try {
      // The hashing algorithm to use.
      // See http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest
      // Using a String literal to paranoidly avoid reflection abuse.
      // By current knowledge, SHA-512 (and all of the SHA-2 family, for that matter) is secure. (2013-07-18)
      final MessageDigest md = MessageDigest.getInstance("SHA-512");

      return md.digest(bytes);
    } catch (final NoSuchAlgorithmException e) {
      LOG.error("Can not digest passwords, digest algorithm is missing", e);
      throw e;
    }
  }
}
