/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.algorithm.ConvexHull;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.linearref.LengthIndexedLine;
import com.vividsolutions.jts.operation.IsSimpleOp;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Util class for Geometric functions.
 */
public final class GeometryUtil {

  private static final Logger LOG = LoggerFactory.getLogger(GeometryUtil.class);

  private GeometryUtil() {
    //util class
  }

  /**
   * @param wktGeometry The geometry to determine the middle for.
   * @return The middle of the geometry.
   * @throws AeriusException In case the WKTgeometry did not define a right geometry.
   */
  public static Point middleOfGeometry(final WKTGeometry wktGeometry) throws AeriusException {
    final Geometry geometry = getGeometry(wktGeometry.getWKT());

    final Point midPoint;
    if (geometry instanceof com.vividsolutions.jts.geom.Point) {
      final com.vividsolutions.jts.geom.Point geomPoint = (com.vividsolutions.jts.geom.Point) geometry;
      midPoint = new Point(geomPoint.getX(), geomPoint.getY());
    } else if (geometry instanceof LineString) {
      midPoint = GeometryUtil.middleOfLineString((LineString) geometry);
    } else {
      midPoint = new Point(geometry.getCentroid().getX(), geometry.getCentroid().getY());
    }

    return midPoint;
  }

  /**
   * @param lineString The linestring to determine the middle for.
   * @return The middle point of the linestring (on the line).
   */
  private static Point middleOfLineString(final LineString lineString) {
    final int middle = (int) Math.round(lineString.getLength() / 2);
    Coordinate prevCoord = lineString.getCoordinateN(0);
    int curLength = 0;
    for (final Coordinate nextCoord : lineString.getCoordinates()) {
      final double d = prevCoord.distance(nextCoord);

      if (curLength + d >= middle) {
        final double posOnLine = (middle - curLength) / d;

        return new Point(prevCoord.x + (nextCoord.x - prevCoord.x) * posOnLine,
            prevCoord.y + (nextCoord.y - prevCoord.y) * posOnLine);
      }
      curLength += d;
      prevCoord = nextCoord;
    }
    return new Point(prevCoord.x, prevCoord.y);
  }

  /**
   * Determine the minimum distance.
   * @param point The point to determine the minimum distance to.
   * @param geometries The geometries that have to be used to determine minimum distance.
   * @return The minimum distance between the point and the list of geometries.
   * @throws AeriusException When a WKT geometry is invalid.
   */
  public static double minDistance(final Point point, final List<WKTGeometry> geometries) throws AeriusException {
    Double distance = null;
    final Geometry pointGeometry = getGeometry(point.toWKT());
    for (final WKTGeometry geometry : geometries) {
      final double thisDistance = getGeometry(geometry.getWKT()).distance(pointGeometry);
      if (distance == null || thisDistance < distance) {
        distance = thisDistance;
      }
    }
    return distance == null ? 0 : distance.doubleValue();
  }

  /**
   * @param wkt The WKT to validate.
   * @return The (JTS) geometry that is contained in the WKT.
   */
  public static boolean validWKT(final String wkt) {
    //consider a null,empty or whitespace string as invalid.
    boolean valid = StringUtils.isNotBlank(wkt);
    if (valid) {
      try {
        getGeometry(wkt);
      } catch (final AeriusException e) {
        valid = false;
      }
    }
    return valid;
  }

  /**
   * @param wkt The WKT to convert to a Point object.
   * @return The point that is contained in the WKT.
   * @throws AeriusException In case the WKT couldn't be converted to a valid point object.
   */
  public static Point getPoint(final String wkt) throws AeriusException {
    final Geometry geom = getGeometry(wkt);
    if (!(geom instanceof com.vividsolutions.jts.geom.Point)) {
      throw new AeriusException(Reason.GEOMETRY_INVALID, wkt);
    }
    final com.vividsolutions.jts.geom.Point point = (com.vividsolutions.jts.geom.Point) geom;
    return new Point(point.getX(), point.getY());
  }

  /**
   * Returns the The (JTS) geometry that is contained in the AERIUS geometry.
   * @param aeriusGeometry AERIUS geometry object.
   * @return The (JTS) geometry that is contained in the WKT.
   * @throws AeriusException exception in case of geometry problems
   */
  public static Geometry getGeometry(final nl.overheid.aerius.geo.shared.Geometry aeriusGeometry) throws AeriusException {
    final Geometry geometry = getGeometry(aeriusGeometry instanceof WKTGeometry ? ((WKTGeometry) aeriusGeometry).getWKT()
        : aeriusGeometry instanceof Point ? ((Point) aeriusGeometry).toWKT() : null);
    if (geometry == null) {
      throw new AeriusException(Reason.GEOMETRY_INVALID, String.valueOf(aeriusGeometry));
    }
    return geometry;
  }

  /**
   * @param wkt The WKT to convert to a JTS Geometry object.
   * @return The (JTS) geometry that is contained in the WKT.
   * @throws AeriusException In case the WKT couldn't be converted to a valid JTS Geometry object.
   */
  public static Geometry getGeometry(final String wkt) throws AeriusException {
    Geometry geom = null;
    final WKTReader reader = new WKTReader();
    try {
      geom = reader.read(wkt);
    } catch (final ParseException e) {
      LOG.trace("WKT parse expection, rethrown as AeriusException", e);
      throw new AeriusException(Reason.GEOMETRY_INVALID, wkt);
    }
    return geom;
  }

  /**
   * @param wkt The WKT to convert to a JTS Geometry object.
   * @return The (JTS) geometry that is contained in the WKT.
   * @throws IllegalArgumentException in case of invalid WKT string.
   */
  public static Geometry getGeometryUnsafe(final String wkt) {
    try {
      return getGeometry(wkt);
    } catch (final AeriusException e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * Check if a wkt has intersections.
   * @param wkt The WKT to check for intersections.
   * @return true if invalid, false if not.
   * @throws AeriusException In case the WKT was incorrect.
   */
  public static boolean hasIntersections(final String wkt) throws AeriusException {
    final Geometry geometry = getGeometry(wkt);
    final IsSimpleOp isSimpleOp = new IsSimpleOp(geometry);
    //isSimple implementation for polygons always returns true because if
    //isValid returns true it's by definition simple. Therefore both test are
    //done here, as isSimple is needed for checking lines.
    return (geometry instanceof Polygon || geometry instanceof MultiPolygon)
        && (!isSimpleOp.isSimple() || !geometry.isValid());
  }

  /**
   * Determine the length of a geometry as rounded int.
   * @param wkt The (WKT) geometry to determine the length for (must be a linestring).
   * @return The length of the linestring (in rounded m).
   */
  public static int determineLengthAsInt(final String wkt) {
    return (int) Math.round(determineLength(wkt));
  }

  /**
   * Determine the length of a geometry.
   * @param wkt The (WKT) geometry to determine the length for (must be a linestring).
   * @return The length of the linestring (in m).
   */
  public static double determineLength(final String wkt) {
    if (wkt == null) {
      throw new IllegalArgumentException("WKT can't be null");
    }
    Geometry jtsGeometry = null;
    try {
      jtsGeometry = getGeometry(wkt);
    } catch (final AeriusException e) {
      throw new IllegalArgumentException("Could not parse WKT (for linestring): " + wkt);
    }
    if (!(jtsGeometry instanceof LineString)) { // no null check needed, as instanceof will return false if object is null
      throw new IllegalArgumentException("WKT did not describe a linestring: " + wkt);
    }
    return ((LineString) jtsGeometry).getLength();
  }

  /**
   * Determine the surface of a geometry as rounded int.
   * @param wkt The (WKT) geometry to determine the surface for (must be a polygon).
   * @return The surface of the polygon (in rounded m^2).
   */
  public static int determineAreaAsInt(final String wkt) {
    return (int) (determineArea(wkt) + 0.5); // don't round as round can't handle double larger max int
  }

  /**
   * Determine the surface of a geometry.
   * @param wkt The (WKT) geometry to determine the surface for (must be a polygon).
   * @return The surface of the polygon (in m^2).
   */
  public static double determineArea(final String wkt) {
    if (wkt == null) {
      throw new IllegalArgumentException("WKT can't be null");
    }
    Geometry jtsGeometry = null;
    try {
      jtsGeometry = getGeometry(wkt);
    } catch (final AeriusException e) {
      throw new IllegalArgumentException("Could not parse WKT (for polygon): " + wkt);
    }
    if (!(jtsGeometry instanceof Polygon)) { // no null check needed, as instanceof will return false if object is null
      throw new IllegalArgumentException("WKT did not describe a polygon: " + wkt);
    }
    final Polygon polygon = (Polygon) jtsGeometry;
    return polygon.getArea();
  }

  /**
   * @param wkt The WKT string containing LINESTRING
   * @return the point representing the last point of the linestring.
   * @throws AeriusException When no valid LINESTRING could be made from the WKT.
   */
  public static Point lastPointFromWKT(final String wkt) throws AeriusException {
    final Point point = new Point();
    final Geometry lineString = getGeometry(wkt);
    final Coordinate lastCoordinate = lineString.getCoordinates()[lineString.getCoordinates().length - 1];
    point.setX(lastCoordinate.getOrdinate(Coordinate.X));
    point.setY(lastCoordinate.getOrdinate(Coordinate.Y));
    return point;
  }

  /**
   * @param <T> The class of the points to search through.
   * @param toPoint The point to measure distance to.
   * @param points The list of points that is a candidate for closest point.
   * @return The closest point (same object as in the list).
   */
  public static <T extends Point> T getClosestPoint(final Point toPoint, final ArrayList<T> points) {
    T rightPoint = null;
    double currentDistance = Double.MAX_VALUE;
    for (final T fromPoint : points) {
      if (fromPoint.distance(toPoint) < currentDistance) {
        currentDistance = fromPoint.distance(toPoint);
        rightPoint = fromPoint;
      }
    }
    return rightPoint;
  }

  /**
   * Convert a Line (or LineString) into a list of points that represent the line.
   * Each point represents an equal part of the line.
   * @param line The line to convert.
   * @param maxSegmentSize The maximum size of the segments that should be returned.
   * @return The converted points that represent the line.
   * @throws AeriusException When the geometry wasn't right.
   */
  public static List<Point> convertToPoints(final WKTGeometry line, final double maxSegmentSize) throws AeriusException {
    final List<Point> points = new ArrayList<>();
    final Geometry geometry = getGeometry(line.getWKT());
    if (geometry instanceof LineString) {
      final LineString lineString = (LineString) geometry;
      final double lineLength = lineString.getLength();
      final double numSegments = BigDecimal.valueOf(lineLength).divide(BigDecimal.valueOf(maxSegmentSize), 0, RoundingMode.CEILING).doubleValue();
      final double segmentSize = lineLength / numSegments;
      for (int i = 0; i < numSegments; i++) {
        final LengthIndexedLine lengthIndexedLine = new LengthIndexedLine(lineString);
        final Coordinate coord = lengthIndexedLine.extractPoint(i * segmentSize + segmentSize / 2);
        points.add(new Point(coord.x, coord.y));
      }
    } else {
      LOG.error("Can't convert {}. It is not a valid linestring geometry.", line);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }

    return points;
  }

  /**
   * @param points The points to determine the bounding box for.
   * @param <P> The type of the points.
   * @return the bounding box.
   */
  public static <P extends Point> BBox determineBBox(final List<P> points) {
    final BBox boundingBox = new BBox();
    if (!points.isEmpty()) {
      boundingBox.setMaxX(points.get(0).getX());
      boundingBox.setMinX(points.get(0).getX());
      boundingBox.setMaxY(points.get(0).getY());
      boundingBox.setMinY(points.get(0).getY());
      for (final Point point : points) {
        if (boundingBox.getMaxX() < point.getX()) {
          boundingBox.setMaxX(point.getX());
        }
        if (boundingBox.getMinX() > point.getX()) {
          boundingBox.setMinX(point.getX());
        }
        if (boundingBox.getMaxY() < point.getY()) {
          boundingBox.setMaxY(point.getY());
        }
        if (boundingBox.getMinY() > point.getY()) {
          boundingBox.setMinY(point.getY());
        }
      }
    }
    return boundingBox;
  }

  /**
   * @param geometries The geometries to determine the centroid for.
   * @return the centroid for the supplied geometries.
   * @throws AeriusException In case the WKT Strings could not be converted to a geometry.
   */
  public static Point determineCentroid(final int srid, final List<WKTGeometry> geometries) throws AeriusException {
    final Point centroid;
    if (geometries.isEmpty()) {
      centroid = new Point();
    } else {
      final List<Geometry> jtsGeometries = new ArrayList<>();
      for (final WKTGeometry geometry : geometries) {
        jtsGeometries.add(getGeometry(geometry.getWKT()));
      }

      final GeometryCollection collection = new GeometryCollection(jtsGeometries.toArray(new Geometry[jtsGeometries.size()]),
          new GeometryFactory(new PrecisionModel(), srid));
      final ConvexHull convexHull = new ConvexHull(collection);
      final com.vividsolutions.jts.geom.Point point = convexHull.getConvexHull().getCentroid();
      centroid = new Point(point.getX(), point.getY());
    }
    return centroid;
  }

  /**
   * Get the minimum BBox needed where the given geometry will fit in.
   * @param geometry The geometry
   * @return BBox or null if a type of {@link org.postgis.Geometry} is encountered which is not yet implemented.
   * @throws AeriusException in case converting the geometry failed.
   */
  public static BBox determineBBoxForGeometry(final WKTGeometry geometry) {
    final Geometry jtsGeometry;
    try {
      jtsGeometry = getGeometry(geometry.getWKT());
    } catch (final AeriusException e) {
      throw new IllegalArgumentException(e);
    }
    return new BBox(jtsGeometry.getEnvelopeInternal().getMinX(), jtsGeometry.getEnvelopeInternal().getMinY(),
        jtsGeometry.getEnvelopeInternal().getMaxX(), jtsGeometry.getEnvelopeInternal().getMaxY());
  }


  /**
   * Calculate the minimum BBox needed to fit all the geometries.
   * @param geometries The geometries to determine the bbox for.
   * @return BBox.
   */
  public static BBox determineBBoxForGeometries(final List<WKTGeometry> geometries) {
    final List<Geometry> jtsGeometries = new ArrayList<>();
    for (final WKTGeometry geometry : geometries) {
      try {
        jtsGeometries.add(getGeometry(geometry.getWKT()));
      } catch (final AeriusException e) {
        throw new IllegalArgumentException(e);
      }
    }
    final Envelope env = new Envelope();
    for (final Geometry g : jtsGeometries) {
      env.expandToInclude(g.getEnvelopeInternal());
    }
    return new BBox(env.getMinX(), env.getMinY(), env.getMaxX(), env.getMaxY());
  }

  /**
   * Calculate the scale of the image. A default DPI of 90 is used.
   * @param size The size of the map. This can be the width or the height.
   *  Be sure the same side of the image is supplied below for this to make sense.
   *  If using a spatial referencing system, make sure it's one without funny transformations.
   * @param imageSize The image size (in pixels) the map will be.
   * @return scale
   */
  public static double getScaleOfImage(final double size, final double imageSize) {
    final int dpi = 90;
    final double pixelScale = BigDecimal.valueOf(SharedConstants.INCH_TO_CM / dpi).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();

    return size / imageSize / pixelScale * SharedConstants.CM_TO_M;
  }

  /**
   * Adds the given padding factor to the sides of the BBox.
   * <p>Example: If the width of the bbox is 100, the height 200 and a padding factor is supplied of 0.1.
   * The left and right parts will be padded with 10 (100 * 0.1) and the top and bottom parts with 20 (200 * 0.1).
   * @param bbox The BBox
   * @param paddingFactor The factor to use
   * @return A new BBox with the padding
   */
  public static BBox addPadding(final BBox bbox, final double paddingFactor) {
    final BBox newBbox = bbox.copy();

    final double addWidthToBBox = newBbox.getWidth() * paddingFactor;
    final double addHeightToBBox = newBbox.getHeight() * paddingFactor;
    newBbox.setMinX(newBbox.getMinX() - addWidthToBBox);
    newBbox.setMaxX(newBbox.getMaxX() + addWidthToBBox);
    newBbox.setMinY(newBbox.getMinY() - addHeightToBBox);
    newBbox.setMaxY(newBbox.getMaxY() + addHeightToBBox);

    return newBbox;
  }

  /**
   * Adds the given padding to the sides of the BBox.
   * <p>Example: If the width of the bbox is 100, the height 200 and a padding is supplied of 50.
   * All sides will be padded with 50.
   * @param bbox The BBox
   * @param padding The padding to use
   * @return A new BBox with the padding
   */
  public static BBox addPaddingStatic(final BBox bbox, final double padding) {
    final BBox newBbox = bbox.copy();

    newBbox.setMinX(newBbox.getMinX() - padding);
    newBbox.setMaxX(newBbox.getMaxX() + padding);
    newBbox.setMinY(newBbox.getMinY() - padding);
    newBbox.setMaxY(newBbox.getMaxY() + padding);

    return newBbox;
  }

  /**
   * Ensure a bbox has the minimum width/height.
   * <p>Example: If the width of current bbox is 50 and the height 200, and the minimum width/height is 100,100.
   * The returned bbox will be expanded to get a bbox of 100 by 200 (center of the bbox will remain the same).
   * @param bbox The bbox to get a minimum width/height version of (this instance will not be changed).
   * @param minimumWidth Minimum width the returned bbox must have.
   * @param minimumHeight Minimum height the returned bbox must have.
   * @return The new bbox which has at least the minimum width/height.
   */
  public static BBox ensureMinimumWidthHeight(final BBox bbox, final double minimumWidth, final double minimumHeight) {
    final BBox newBbox = bbox.copy();

    if (minimumWidth > newBbox.getWidth()) {
      final double centerX = newBbox.getMinX() + newBbox.getWidth() / 2;
      newBbox.setMinX(centerX - minimumWidth / 2);
      newBbox.setMaxX(centerX + minimumWidth / 2);
    }
    if (minimumHeight > newBbox.getHeight()) {
      final double centerY = newBbox.getMinY() + newBbox.getHeight() / 2;
      newBbox.setMinY(centerY - minimumHeight / 2);
      newBbox.setMaxY(centerY + minimumHeight / 2);
    }

    return newBbox;
  }

  /**
   * Merge 2 bounding boxes.
   * @param bbox1 First bounding box to merge.
   * @param bbox2 Second bounding box to merge.
   * @return The merged bounding box:
   * If both are null, it returns null.
   * If either is null, it returns the one not null.
   * If neither are null, it returns a bbox with the maximum of the max-values and the minimum of the min-values.
   */
  public static BBox merge(final BBox bbox1, final BBox bbox2) {
    BBox merged = new BBox();
    if (bbox1 == null) {
      merged = bbox2.copy();
    } else if (bbox2 == null) {
      merged = bbox1.copy();
    } else {
      merged.setMaxX(Math.max(bbox1.getMaxX(), bbox2.getMaxX()));
      merged.setMaxY(Math.max(bbox1.getMaxY(), bbox2.getMaxY()));
      merged.setMinX(Math.min(bbox1.getMinX(), bbox2.getMinX()));
      merged.setMinY(Math.min(bbox1.getMinY(), bbox2.getMinY()));
    }
    return merged;
  }

  /**
   * Use this if you have a BBox which you want to fit fully in a viewport or in an image.
   * The fixed BBox should contain at least the given BBox. The width and height are used to calculate
   *  the width/height ratio which will be used in turn to calculate the width/height ratio to use in the fixed BBox.
   * The unit which it should represent according to your assumption doesn't matter.
   *
   * The Bbox will be expanded at the sides, if needed, so your given BBox will always be in the center.
   * @param bbox The bounding box
   * @param width The width
   * @param height The height
   * @return Fixed BBox
   */
  public static BBox getFixedBBox(final BBox bbox, final int width, final int height) {
    if (bbox == null) {
      throw new IllegalArgumentException("BBox not allowed to be null");
    } else if (width == 0 || height == 0) {
      throw new IllegalArgumentException("Width and/or height not allowed to be 0. Width " + width + ", height " + height);
    }
    final BBox result = bbox.copy();

    final double currentWidthHeightRatio = result.getWidth() / result.getHeight();
    final double wantedWidthHeightRatio = ((Integer) width).doubleValue() / height;

    if (currentWidthHeightRatio > wantedWidthHeightRatio) {
      // We need to add to the height.
      final double newFullHeight = result.getHeight() * (currentWidthHeightRatio / wantedWidthHeightRatio);

      // Add half of difference to both sides, so the given BBox will be at the center.
      final double halfOfDifference = (newFullHeight - result.getHeight()) / 2;
      result.setMaxY(result.getMaxY() + halfOfDifference);
      result.setMinY(result.getMinY() - halfOfDifference);
    } else if (currentWidthHeightRatio < wantedWidthHeightRatio) {
      // We need to add to the width.
      final double newFullWidth = result.getWidth() * (wantedWidthHeightRatio / currentWidthHeightRatio);

      // Add half of difference to both sides, so the given BBox will be at the center.
      final double halfOfDifference = (newFullWidth - result.getWidth()) / 2;
      result.setMaxX(result.getMaxX() + halfOfDifference);
      result.setMinX(result.getMinX() - halfOfDifference);
    }
    // No else needed, according to my calculations the ratio's should be the same. Return the copy without changes. /knowitallmode

    return result;
  }

  /**
   * Determine the bounding box containing all bounding boxes in a collection (like merge).
   * @param collection The collection of bounding boxes to merge.
   * @return The minimum bounding box for the supplied collection.
   */
  public static BBox merge(final Collection<BBox> collection) {
    final BBox bbox = new BBox();
    boolean firstEntry = true;
    for (final BBox entry : collection) {
      if (firstEntry || entry.getMinX() < bbox.getMinX()) {
        bbox.setMinX(entry.getMinX());
      }
      if (firstEntry || entry.getMaxX() > bbox.getMaxX()) {
        bbox.setMaxX(entry.getMaxX());
      }
      if (firstEntry || entry.getMinY() < bbox.getMinY()) {
        bbox.setMinY(entry.getMinY());
      }
      if (firstEntry || entry.getMaxY() > bbox.getMaxY()) {
        bbox.setMaxY(entry.getMaxY());
      }

      firstEntry = false;
    }
    return bbox;
  }
}
