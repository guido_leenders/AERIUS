/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.SharedConstants;

/**
 * Utility class to add in everything Locale-related.
 */
public final class LocaleUtils {
  // For some insane reason, Locale does not maintain the all-important dutch language among its defaults.
  private static final Locale DUTCH = new Locale(SharedConstants.LOCALE_NL);

  // Known locales that should be loaded on init and database messages validation/tests.
  public static final Locale[] KNOWN_LOCALES = new Locale[] { DUTCH, Locale.ENGLISH };

  // Not to be constructed.
  private LocaleUtils() {}

  /**
   * Get locale for given locale string.
   * @param locale The locale string to parse
   * @return The locale
   */
  public static Locale getLocale(final String locale) {
    final Locale rightLocale;
    if (StringUtils.isEmpty(locale)) {
      rightLocale = getDefaultLocale();
    } else {
      final String[] split = locale.toLowerCase().split("_");
      rightLocale = new Locale(split[0], split.length > 1 ? split[1] : "");
    }
    return isValidLocale(rightLocale) ? rightLocale : getDefaultLocale();
  }

  public static Locale getSupportedLocaleOrDefault(final String localeString) {
    final Locale locale = LocaleUtils.getLocale(localeString);

    for (final Locale known : KNOWN_LOCALES) {
      // Only check for language because that's all we're interested in.
      if (known.getLanguage().equals(locale.getLanguage())) {
        // Return known rather than user-given because we know exactly what's in known.
        return known;
      }
    }

    return getDefaultLocale();
  }

  public static boolean isValidLocale(final Locale test) {
    for (final Locale locale : KNOWN_LOCALES) {
      if (locale.equals(test)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Get the default locale.
   * @return The locale
   */
  public static Locale getDefaultLocale() {
    return getLocale(SharedConstants.DEFAULT_LOCALE);
  }

  /**
   * Get the default locale from the DB.
   * @param pmf The pmf to use.
   * @return The default locale.
   */
  public static Locale getDefaultLocale(final PMF pmf) {
    return getLocale(ConstantRepository.getString(pmf, ConstantsEnum.DEFAULT_LOCALE));
  }

}
