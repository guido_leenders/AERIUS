/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import java.util.ArrayList;
import java.util.stream.IntStream;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Utility class for validating PriorityProjects on not allowed sectors.
 */
public final class PriorityProjectValidationUtil {

  private static final int[] NOT_ALLOWED_SECTORS = new int[] {9000, 9999};

  // Not to be constructed.
  private PriorityProjectValidationUtil() {
  }

  public static void checkNonAllowedSectors(final CalculatedScenario scenario) throws AeriusException {
    for (final Calculation calculation : scenario.getCalculations()) {
      checkNonAllowedSectors(scenario.getSources(calculation.getCalculationId()));
    }
  }

  public static void checkNonAllowedSectors(final ArrayList<EmissionSourceList> sourceLists) throws AeriusException {
    for (final EmissionSourceList source : sourceLists) {
      checkNonAllowedSectors(source);
    }
  }

  private static void checkNonAllowedSectors(final EmissionSourceList sources) throws AeriusException {
    for (final EmissionSource source : sources) {
      validateNotAllowedSectors(source.getSector());
    }
  }

  private static void validateNotAllowedSectors(final Sector sector) throws AeriusException {
    if (IntStream.of(NOT_ALLOWED_SECTORS).anyMatch(id -> id == sector.getSectorId())) {
      throw new AeriusException(Reason.PRIORITY_PROJECT_NON_ALLOWED_SECTOR, Integer.toString(sector.getSectorId()), sector.getName());
    }
  }

}
