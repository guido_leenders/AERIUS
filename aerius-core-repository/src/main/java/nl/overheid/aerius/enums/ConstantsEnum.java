/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.ProductType;

/**
 * Global constants stored in the database and that can only be used on the server (i.e. everywhere
 * except the client). Constants that also need to be accessed in the client should
 * be placed in {@link nl.overheid.aerius.shared.constants.SharedConstantsEnum}.
 *
 * <p>These can be accessed live from the database via the methods in
 * {@link nl.overheid.aerius.db.common.ConstantRepository}.</p>
 *
 * <p>The values of the entries are stored in the database tables <code>public.constants</code>
 * and <code>system.constants</code>.</p>
 */
public enum ConstantsEnum {

  /**
   * Url to the proxy of pdok.
   */
  PDOX_PROXY_URL(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  /**
   * layer id of the background layer to be used by the pdf document.
   */
  PAA_BACKGROUND_MAP_ID(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Email address used as the from e-mail of e-mails send.
   */
  NOREPLY_EMAIL(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  /**
   * Base location where the PAA file can be downloaded from.
   */
  PAA_MAIL_DOWNLOAD_LINK(ProductType.CALCULATOR, ProductType.MELDING),
  /**
   * Default base location where a file can be downloaded from.
   */
  DEFAULT_FILE_MAIL_DOWNLOAD_LINK(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  CURRENT_DATABASE_NAME(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  CURRENT_DATABASE_VERSION(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  /**
   * The type of release for the currently running application (valid values: PRODUCTION, CONCEPT, DEPRECATED).
   */
  RELEASE(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  /**
   * Melding mail: The central authority email address (used for BCC on error).
   */
  MELDING_AUTHORITIES_CENTRAL_MAIL_ADDRESS(ProductType.REGISTER),
  /**
   * Maximum number of sources allowed in the calculator.
   */
  CALCULATOR_LIMITS_MAX_SOURCES(ProductType.CALCULATOR),
  /**
   * Maximum length of a line allowed in the calculator in meters.
   */
  CALCULATOR_LIMITS_MAX_LINE_LENGTH(ProductType.CALCULATOR),
  /**
   * Maximum surface allowed in the calculator in hectares.
   */
  CALCULATOR_LIMITS_MAX_POLYGON_SURFACE(ProductType.CALCULATOR),
  /**
   * Length of a segment when dividing a line in points in meters.
   */
  CONVERT_LINE_TO_POINTS_SEGMENT_SIZE(ProductType.CALCULATOR, ProductType.REGISTER),
  /**
   * Size used when converting polygon to grid based points.
   */
  CONVERT_POLYGON_TO_POINTS_GRID_SIZE(ProductType.CALCULATOR, ProductType.REGISTER),

  /**
   * Salt for download link for requests.
   */
  SALT_REQUEST_FILE_DOWNLOAD(ProductType.REGISTER),

  /**
   * An 'OPS Calculation Time Unit' is determined by factoring the number of sources with the number of receptors.
   * The resulting number linearly scales with the time it theoretically takes OPS to finish a calculation.
   *
   * This constant represents the ideal value - expressed in 'OPS Calculation Time Units' - for any one chunk.
   */
  MAX_OPS_UNITS_UI(ProductType.CALCULATOR, ProductType.MELDING),

  /**
   * The minimum number of receptors to calculate in a single chunk for the UI. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  MIN_RECEPTORS_UI(ProductType.CALCULATOR, ProductType.MELDING),

  /**
   * The maximum number of receptors to calculate in a single chunk for the UI. This limit is imposed because
   * the responsiveness of the UI is related to the number of receptors it needs to process.
   */
  MAX_RECEPTORS_UI(ProductType.CALCULATOR, ProductType.MELDING),

  /**
   * The minimum number of receptors to calculate in a single chunk for a worker. This limit is imposed because
   * the overhead introduced by a low number of receptors becomes substantial.
   */
  MIN_RECEPTORS_WORKER(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * The maximum number of receptors to calculate in a single chunk for a worker. Limit is imposed for consistency.
   */
  MAX_RECEPTORS_WORKER(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Maximum number of chunks per calculator job to run concurrently for the worker.
   */
  MAX_CONCURRENT_CHUNKS_WORKER(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * The maximum 'Calculation Engine Time Unit' to use by the worker.
   */
  MAX_CALCULATION_ENGINE_UNITS_WORKER(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Maximum number of chunks per calculator job to run concurrently.
   */
  MAX_CHUNKS_CONCURRENT_WEBSERVER(ProductType.CALCULATOR, ProductType.MELDING),
  /**
   * Maximum number of engine sources in a single calculation in the calculator ui.
   */
  MAX_ENGINE_SOURCES_UI(ProductType.CALCULATOR, ProductType.MELDING),
  /**
   * Maximum number of task client threads (in the webserver).
   */
  NUMBER_OF_TASKCLIENT_THREADS(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),
  /**
   * The end year of the current PAS period.
   */
  CURRENT_PAS_PERIOD_END_YEAR(ProductType.REGISTER),

  /**
   * Melding threshold value (drempelwaarde). If a receptor has more deposition than this, at least a
   * "melding" must be made.
   * (Note: if we do not want to use the database term "pronouncement" in Java (for "melding"), then we
   * need to add an "alias" functionality to this Enum to handle the different constant names.)
   */
  PRONOUNCEMENT_THRESHOLD_VALUE(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Default permit threshold value (grenswaarde). If a receptor has more deposition than this, a permit
   * is required. This threshold can be changed per assessment area; this is the default.
   */
  DEFAULT_PERMIT_THRESHOLD_VALUE(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * The maximum radius (in m) within which requests (both permits and notices) will be retrieved for UI when needed.
   */
  MAX_RADIUS_FOR_REQUESTS(ProductType.REGISTER),

  /**
   * Time at which the development spaces were synced from Register to Calculator.
   */
  DEVELOPMENT_SPACE_SYNC_TIMESTAMP(ProductType.CALCULATOR, ProductType.MELDING),
  /**
   * Determines if it is allowed to generate a new API key for Connect.
   */
  CONNECT_GENERATING_APIKEY_ENABLED(ProductType.CALCULATOR),
  /**
   * Specifies the amount of max concurrent jobs for a new user.
   */
  CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER(ProductType.CALCULATOR),
  /**
   * Specifies the URL the Connect index.jsp (root of the webapp) will redirect to.
   */
  CONNECT_ROOT_REDIRECT_URL(ProductType.CALCULATOR),
  /**
   * Specifies the validation threshold in KiB.
   * If the supplied file is larger, no XML validation will occur on import within the Connect Util section.
   */
  CONNECT_UTIL_VALIDATION_THRESHOLD_KB(ProductType.CALCULATOR),

  /**
   * The conversion factor for emission results, to be used in concert with unit display settings.
   */
  EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR(ProductType.CALCULATOR),

  /**
   * The display unit for emission results, to be used in concert with conversion factor.
   */
  EMISSION_RESULT_DISPLAY_UNIT(ProductType.CALCULATOR),

  /**
   * The number of decimal points to display emission results for.
   */
  EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH(ProductType.CALCULATOR),

  /**
   * The number of precise decimal points to display emission results for.
   */
  EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH(ProductType.CALCULATOR),

  /**
   * Surface of zoom level 1.
   */
  SURFACE_ZOOM_LEVEL_1(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Number of zoom levels.
   */
  MAX_ZOOM_LEVEL(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Default locale.
   */
  DEFAULT_LOCALE(ProductType.CALCULATOR, ProductType.MELDING, ProductType.REGISTER),

  /**
   * Hostname for the graphite metrics collection instance.
   *
   * If this property has no value, metrics will not be reported (although they will be collected).
   */
  METRICS_GRAPHITE_HOST,

  /**
   * Port number for the grahpite metrics collection instance.
   *
   * Optional property: The default value for this property is 2004 in MetricFactory.
   */
  METRICS_GRAPHITE_PORT,

  /**
   * Report interval for metric reporting.
   *
   * Optional property: The default value for this property is 5 in MetricFactory.
   */
  METRICS_GRAHPITE_INTERVAL,

  /**
   * Report prefix for metric reporting.
   *
   * Optional property: The default value for this property is empty.
   */
  METRICS_GRAPHITE_PREFIX;

  private final ArrayList<ProductType> productTypes;

  ConstantsEnum(final ProductType... products) {
    productTypes = new ArrayList<>();
    for (final ProductType product : products) {
      productTypes.add(product);
    }
  }

  public ArrayList<ProductType> getProductTypes() {
    return productTypes;
  }
}
