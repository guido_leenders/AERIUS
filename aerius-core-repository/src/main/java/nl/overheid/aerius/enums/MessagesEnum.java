/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

/**
 * Messages constants that can be used everywhere,except the client and of which
 * the value is stored in the database. These messages are only messages that
 * are expected to be changed during the application life cycle, such as email
 * body text. In general messages text should be stored in the i18n modules.
 */
public enum MessagesEnum {

  /**
   * The content of the mail to use when sending a PAA-file (location).
   */
  PAA_MAIL_CONTENT,
  /**
   * The subject to use when sending a PAA-file (location) - Development Spaces.
   */
  PAA_DEVELOPMENT_SPACES_MAIL_SUBJECT,
  /**
   * The subject to use when sending a PAA-file (location) - Demand.
   */
  PAA_DEMAND_MAIL_SUBJECT,
  /**
   * The subject to use when sending a PAA-file (location).
   */
  PAA_OWN_USE_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a PAA-file (location).
   */
  PAA_OWN_USE_MAIL_CONTENT,
  /**
   * The subject of the mail to use when sending a GML file (download location) and job.
   */
  GML_MAIL_SUBJECT_JOB,
  /**
   * The content of the mail to use when sending a GML file (download location).
   */
  GML_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a GML file (download location).
   */
  GML_MAIL_CONTENT,
  /**
   * The subject of the mail to use when sending a CSV file (download location) and job.
   */
  CSV_MAIL_SUBJECT_JOB,
  /**
   * The subject of the mail to use when sending a CSV file (download location).
   */
  CSV_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a CSV file (download location).
   */
  CSV_MAIL_CONTENT,
  /**
   * The subject to use when sending an email informing the user about an error.
   */
  ERROR_MAIL_SUBJECT,
  /**
   * The content of the mail to use when informing the user about an error.
   */
  ERROR_MAIL_CONTENT,
  /**
   * The default subject to use when sending a file (location) to a user.
   */
  DEFAULT_FILE_MAIL_SUBJECT,
  /**
   * The default content to use when sending a file (location) to a user.
   */
  DEFAULT_FILE_MAIL_CONTENT,

  /**
   * The actual template of body of a mail (containing disclaimer and such).
   * Should contain the replacement token MAIL_CONTENT and the MAIL_SIGNATURE.
   */
  MAIL_CONTENT_TEMPLATE,
  /**
   * The actual template of subject of a mail.
   * Should contain the replacement token MAIL_SUBJECT.
   */
  MAIL_SUBJECT_TEMPLATE,
  /**
   * Default signature for the email.
   */
  MAIL_SIGNATURE_DEFAULT,
  /**
   * Subject text for a password reset request mail.
   */
  PASSWORD_RESET_REQUEST_SUBJECT,

  /**
   * Body text for a password reset request mail.
   */
  PASSWORD_RESET_REQUEST_BODY,

  /**
   * Subject text for a password reset confirm mail.
   */
  PASSWORD_RESET_CONFIRM_SUBJECT,

  /**
   * Body text for a password reset confirm mail.
   */
  PASSWORD_RESET_CONFIRM_BODY,

  /**
   * Subject to use when sending a new generated apikey.
   */
  CONNECT_APIKEY_CONFIRM_SUBJECT,

  /**
   * Body text to use when sending a new generated apikey.
   */
  CONNECT_APIKEY_CONFIRM_BODY,

  /**
   * The subject to use when sending a Melding registered mail to the applicant(s).
   */
  MELDING_REGISTERED_USER_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a Melding registered mail to the applicant(s).
   */
  MELDING_REGISTERED_USER_MAIL_CONTENT,
  /**
   * The content of the mail to use when sending a Melding registered mail to the applicant(s) with a list of send files.
   */
  MELDING_REGISTERED_USER_MAIL_WITH_ATTACHMENTS_CONTENT,
  /**
   * The subject to use when sending a Melding not registered mail to the applicant(s).
   */
  MELDING_NOT_REGISTERED_USER_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a Melding not registered mail to the applicant(s) ith a list of send files.
   */
  MELDING_NOT_REGISTERED_USER_MAIL_ATTACHMENTSLIST_CONTENT,
  /**
   * The content of the mail to use when sending a Melding not registered mail to the applicant(s).
   */
  MELDING_NOT_REGISTERED_USER_MAIL_CONTENT,
  /**
   * The subject to use when sending a Melding not registered mail due to technical issues to the applicant(s).
   */
  MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT,
  /**
   * The row in the mail to use when sending a Melding not registered mail due to technical issues to the applicant(s).
   */
  MELDING_NOT_REGISTERED_TECHNICAL_REASON,
  /**
   * The subject to use when sending a Melding not registered mail to the authority.
   */
  MELDING_NOT_REGISTERED_AUTHORITY_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a Melding not registered mail to the authority.
   */
  MELDING_NOT_REGISTERED_AUTHORITY_MAIL_CONTENT,
  /**
   * The subject to use when sending a Melding accepted mail to the authority.
   */
  MELDING_AUTHORITY_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a Melding accepted mail to the authority.
   */
  MELDING_AUTHORITY_MAIL_CONTENT,
  /**
   * The subject to use when sending a Melding delete mail to the authority.
   */
  MELDING_DELETE_AUTHORITY_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a Melding delete mail to the authority.
   */
  MELDING_DELETE_AUTHORITY_MAIL_CONTENT,
  /**
   * The subject to use when sending a melding attached documents to the authority.
   */
  MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBJECT,
  /**
   * The content of the mail to use when sending a melding attached documents to the authority.
   */
  MELDING_DOCUMENTS_AUTHORITY_MAIL_CONTENT,
  /**
   * Subject for the mail with attachments send result.
   */
  MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_SUBJECT,
  /**
   * The content for the mail with attachments send result.
   */
  MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_CONTENT,

  //following values are a bit different: they represent a small part of the content
  /**
   * The content of optional text if user add remarks for reference situation.
   */
  MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBSTANTIATION,

  /**
   * Send status ok.
   */
  MELDING_ATTACHMENTS_SEND_STATUS_OK,
  /**
   * Send status failed.
   */
  MELDING_ATTACHMENTS_SEND_STATUS_FAIL,

  /**
   * Melding decline does not fit permit required.
   */
  MELDING_DOES_NOT_FIT,
  /**
   * Melding decline above permit threshold.
   */
  MELDING_ABOVE_PERMIT_THRESHOLD,
  /**
   * Melding decline technical issues.
   */
  MELDING_TECHNICAL_ISSUES,
  /**
   * Melding decline other.
   */
  MELDING_REJECT_REASON_OTHER,

  /**
   * Landing page or handbook link.
   */
  LANDING_PAGE_LINK,

  /**
   * The text to display on the splash screen showing product attribution.
   */
  SPLASH_ATTRIBUTION_TEXT,

  /**
   * The image to display on the splash screen showing product attribution.
   */
  SPLASH_ATTRIBUTION_IMAGE;

}
