/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.ShippingLaden;

/**
 *
 */
public class InlandCategoryEmissionFactorKey extends InlandCategoryKey {

  private final EmissionValueKey emissionValueKey;

  /**
   * @param emissionValueKey The emission value key for the emission factor.
  * @param direction The direction the ship is going.
   * @param laden The laden state.
   * @param waterwayCategoryId The ID of the waterway category.
   */
  public InlandCategoryEmissionFactorKey(final EmissionValueKey emissionValueKey, final WaterwayDirection direction, final ShippingLaden laden,
      final int waterwayCategoryId) {
    super(direction, laden, waterwayCategoryId);
    this.emissionValueKey = emissionValueKey;
  }

  public EmissionValueKey getEmissionValueKey() {
    return emissionValueKey;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + super.hashCode();
    result = prime * result + ((emissionValueKey == null) ? 0 : emissionValueKey.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj != null && this.getClass() == obj.getClass()) {
      final InlandCategoryEmissionFactorKey other = (InlandCategoryEmissionFactorKey) obj;
      return super.equals(obj) && (emissionValueKey != null && emissionValueKey.equals(other.getEmissionValueKey())
          || emissionValueKey == null && other.getEmissionValueKey() == null);
    }
    return false;
  }

  @Override
  public String toString() {
    return "InlandCategoryEmissionFactorKey [emissionValueKey=" + emissionValueKey + super.toString() + "]";
  }

}
