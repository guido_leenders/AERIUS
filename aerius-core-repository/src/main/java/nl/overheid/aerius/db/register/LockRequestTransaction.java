/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * This can be used to prevent two users from modifying a request's property at the same time.
 * E.g. two users modifying the description.
 * A request knows when it was last modified. When modifying a property, this field is checked to
 * ensure no-one else modified anything since. Otherwise an error appears.
 *
 * Note that this only locks a single request, and has nothing to do with locking the development
 * space table. So when changing the permit status, the development space is locked by the appropriate
 * database function.
 */
public final class LockRequestTransaction extends Transaction {

  public LockRequestTransaction(final Connection con, final int permitId, final long lastModification, final JoinClause... joinClauses)
      throws SQLException, AeriusException {
    super(con);

    boolean locked = false;
    final Query query = getQuery(joinClauses);
    try (final PreparedStatement stmt = connection.prepareStatement(query.get() + " FOR UPDATE")) {
      stmt.setInt(1, permitId); //NOSONAR squid:S2695 can't see above query does contain parameter.
      final ResultSet rst = stmt.executeQuery();

      if (rst.next()) {
        final long dbLastModification = rst.getTimestamp(RegisterAttribute.LAST_MODIFIED.attribute()).getTime();

        if (dbLastModification == lastModification) {
          locked = true;
        }
      }
    } catch (final SQLException se) {
      rollback();
      throw se;
    }

    if (!locked) {
      rollback();
      throw new AeriusException(Reason.PERMIT_ALREADY_UPDATED);
    }
  }

  private Query getQuery(final JoinClause... joinClauses) {
    return QueryBuilder.from("requests")
        .select(RegisterAttribute.REQUEST_ID, RegisterAttribute.LAST_MODIFIED)
        .join(joinClauses)
        .where(RegisterAttribute.REQUEST_ID).getQuery();
  }

}
