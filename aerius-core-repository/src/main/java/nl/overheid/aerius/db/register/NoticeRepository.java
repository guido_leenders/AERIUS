/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.common.AuthorityRepository;
import nl.overheid.aerius.db.common.GeneralRepository;
import nl.overheid.aerius.db.util.ILikeWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SearchWhereClause;
import nl.overheid.aerius.db.util.SpecialAttribute;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.NoticeKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 * Functionality for fetching melding information from the database.
 * All returned Notice objects have all information fields filled in, but not the audit trail. To
 * include that, use {@link #getNoticeWithAuditTrail}.
 */
public final class NoticeRepository {

  private static final int UNKNOWN_NOTICE_ID = -1;

  private static final WhereClause ASSIGNED_ONLY =
      new StaticWhereClause(RegisterAttribute.STATUS.attribute() + " = '" + RequestState.ASSIGNED.getDbValue() + "'");

  private static final Query GET_NOTICE = getNoticesFullInfoQuery()
      .where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query GET_NOTICE_ID_BY_REFERENCE = getNoticesFullInfoQuery()
      .where(RegisterAttribute.REFERENCE).getQuery();

  private static final Query CONFIRMABLE_NOTICE_IDS = getNoticesBaseQuery()
      .where(QueryAttribute.AUTHORITY_ID)
      .where(new StaticWhereClause(RegisterAttribute.CHECKED + " = FALSE")).getQuery();

  private static final Query GET_NOTICES_WITHIN_RADIUS = getNoticesFullInfoQuery()
      .join(new JoinClause("ae_requests_within_distance(?, ?)", RegisterAttribute.REQUEST_ID, RegisterAttribute.REQUEST_ID,
          QueryAttribute.MAX_DISTANCE))
      .where(ASSIGNED_ONLY)
      .orderBy(QueryAttribute.DISTANCE).limit().getQuery();

  private static final Query SEARCH_NOTICE = getNoticesFullInfoQuery()
      .where(new SearchWhereClause(RegisterAttribute.REFERENCE, RegisterAttribute.CORPORATION))
      .getQuery();

  private static final Query DETERMINE_AUTHORITY_ID = QueryBuilder
      .from("ae_determine_pronouncement_authority_id(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid())) AS authority_id",
          QueryAttribute.X_COORD, QueryAttribute.Y_COORD)
      .select(QueryAttribute.AUTHORITY_ID).getQuery();

  private NoticeRepository() {
  }

  /**
   * Get a Notice with all information (no audit trail, see {@link #getNoticeWithAuditTrail}).
   * @param con Connection to use.
   * @param noticeId Notice id to retrieve.
   * @return A notice
   * @throws SQLException In case of a database error.
   */
  public static Notice getNotice(final Connection con, final int noticeId) throws SQLException {
    Notice notice = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_NOTICE.get())) {
      GET_NOTICE.setParameter(selectPS, RegisterAttribute.REQUEST_ID, noticeId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          notice = getNoticeFromResultSet(con, rs);

          notice.setProvince(GeneralRepository.getProvince(con, QueryAttribute.PROVINCE_AREA_ID.getInt(rs)));
        }
      }
    }
    return notice;
  }

  /**
   * Get a Notice with all information and also the audit trail.
   * @param con Connection to use.
   * @param noticeId Notice id to retrieve.
   * @return A notice
   * @throws SQLException In case of a database error.
   */
  public static Notice getNoticeWithAuditTrail(final Connection con, final int noticeId) throws SQLException {
    final Notice notice = getNotice(con, noticeId);
    if (notice != null) {
      notice.setChangeHistory(RequestAuditRepository.getAuditTrail(con, noticeId));
    }
    return notice;
  }

  /**
   * Get a notice id using a reference for lookup.
   * @param con Connection to use.
   * @param reference Reference of the notice id to retrieve.
   * @return A notice id
   * @throws SQLException In case of a database error.
   */
  public static int getNoticeIdByReference(final Connection con, final String reference) throws SQLException {
    int noticeId = UNKNOWN_NOTICE_ID;
    if (reference != null) {
      try (final PreparedStatement selectPS = con.prepareStatement(GET_NOTICE_ID_BY_REFERENCE.get())) {
        GET_NOTICE_ID_BY_REFERENCE.setParameter(selectPS, RegisterAttribute.REFERENCE, reference);
        try (final ResultSet rs = selectPS.executeQuery()) {
          if (rs.next()) {
            noticeId = QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
          }
        }
      }
    }
    return noticeId;
  }

  /**
   * @param con The connection to use.
   * @param maxResults The maximum results to return.
   * @param offset The offset of the results (to be used for paging).
   * @param filter The filter to use when retrieving results.
   * @return A list of notices.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<Notice> getNotices(final Connection con, final int maxResults, final int offset, final NoticeFilter filter)
      throws SQLException {
    final EntityCollector<Notice> collector = new EntityCollector<Notice>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
      }

      @Override
      public Notice fillEntity(final ResultSet rs) throws SQLException {
        return getNoticeFromResultSet(con, rs);
      }
    };
    final Query query = getNoticesFilterQuery(filter);
    try (final PreparedStatement selectPS = con.prepareStatement(query.get())) {
      RequestFilterUtil.setRequestFilterParams(con, query, selectPS, filter);

      query.setParameter(selectPS, SpecialAttribute.LIMIT, maxResults);
      query.setParameter(selectPS, SpecialAttribute.OFFSET, offset);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        collector.collectEntity(rs);
      }
    }

    return collector.getEntities();
  }

  /**
   * @param con The connection to use.
   * @param searchString The string to search for.
   * @return Any notices that fit the string.
   * @throws SQLException In case of exception
   */
  public static ArrayList<Notice> search(final Connection con, final String searchString) throws SQLException {
    final ArrayList<Notice> notices = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(SEARCH_NOTICE.get())) {
      SEARCH_NOTICE.setParameter(selectPS, RegisterAttribute.REFERENCE, ILikeWhereClause.getSearchString(searchString));
      SEARCH_NOTICE.setParameter(selectPS, RegisterAttribute.CORPORATION, ILikeWhereClause.getSearchString(searchString));

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        notices.add(getNoticeFromResultSet(con, rs));
      }
    }

    return notices;
  }

  /**
   * @param con The connection to use.
   * @param requestId The request ID to get the notices within a certain radius for (excluding request itself).
   * @param maxRadius The maximum radius (in m) to look for notices.
   * @return A list of notices, which should be located within the max radius.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<Notice> getNoticesWithinRadius(final Connection con, final int requestId, final int maxRadius)
      throws SQLException {
    final ArrayList<Notice> notices = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_NOTICES_WITHIN_RADIUS.get())) {
      GET_NOTICES_WITHIN_RADIUS.setParameter(selectPS, RegisterAttribute.REQUEST_ID, requestId);
      GET_NOTICES_WITHIN_RADIUS.setParameter(selectPS, QueryAttribute.MAX_DISTANCE, maxRadius);
      GET_NOTICES_WITHIN_RADIUS.setParameter(selectPS, SpecialAttribute.LIMIT, RequestRepository.MAX_REQUESTS_RETRIEVED);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        notices.add(getNoticeFromResultSet(con, rs));
      }
    }

    return notices;
  }

  /**
   * @param con The connection to use.
   * @param authorityId The authority to get all the confirmable notice IDs for.
   * @return A list of IDs for notices that can be confirmed for the authority.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<Integer> getConfirmableNoticesIds(final Connection con, final int authorityId)
      throws SQLException {
    final ArrayList<Integer> noticeIds = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(CONFIRMABLE_NOTICE_IDS.get())) {
      CONFIRMABLE_NOTICE_IDS.setParameter(selectPS, QueryAttribute.AUTHORITY_ID, authorityId);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        noticeIds.add(QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID));
      }
    }
    return noticeIds;
  }

  /**
   * @param con The connection to use.
   * @param centroid The centroid (of the proposed sources) to use to determine the authority for a notice.
   * @return The authority for the notice.
   * @throws SQLException In case of a database error.
   */
  public static Authority getAuthorityForNotice(final Connection con, final Point centroid) throws SQLException {
    int authorityId = 0;
    try (PreparedStatement stmt = con.prepareStatement(DETERMINE_AUTHORITY_ID.get())) {
      DETERMINE_AUTHORITY_ID.setParameter(stmt, QueryAttribute.X_COORD, centroid.getX());
      DETERMINE_AUTHORITY_ID.setParameter(stmt, QueryAttribute.Y_COORD, centroid.getY());

      try (final ResultSet rs = stmt.executeQuery()) {
        if (rs.next()) {
          authorityId = QueryAttribute.AUTHORITY_ID.getInt(rs);
        }
      }
    }
    return AuthorityRepository.getSkinnedAuthority(con, authorityId);
  }

  /**
   * @param con The connection to use.
   * @param key The key of the notice to retrieve.
   * @return The proper notice PDF content (or null if not found).
   * @throws SQLException In case of a database error.
   */
  public static byte[] getNoticePDFContent(final Connection con, final NoticeKey key) throws SQLException {
    return RequestRepository.getRequestFileContent(con, key.getReference(), RequestFileType.APPLICATION);
  }

  private static Notice getNoticeFromResultSet(final Connection con, final ResultSet rs)
      throws SQLException {
    final Notice notice = new Notice();
    notice.setId(QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID));
    RequestRepository.fillRequestFromResultSet(con, rs, notice);
    notice.setReceivedDate(rs.getTimestamp(RegisterAttribute.INSERT_DATE.attribute()));
    return notice;
  }

  private static Query getNoticesFilterQuery(final NoticeFilter filter) {
    final QueryBuilder builder = getNoticesFullInfoQuery();
    builder.where(ASSIGNED_ONLY);
    RequestFilterUtil.addRequestFilter(builder, filter, RegisterAttribute.INSERT_DATE);

    RegisterOrderByUtil.setOrderByParams(builder, filter.getSortAttribute(), filter.getSortDirection());

    builder.orderBy(new OrderByClause(RegisterAttribute.INSERT_DATE, OrderType.DESC))
        .orderBy(RegisterAttribute.CORPORATION)
        .limit().offset();
    return builder.getQuery();
  }

  private static QueryBuilder getNoticesBaseQuery() {
    return QueryBuilder.from("pronouncements_view")
        .select(RegisterAttribute.REQUEST_ID);
  }

  private static QueryBuilder getNoticesFullInfoQuery() {
    final QueryBuilder builder = getNoticesBaseQuery();
    RequestRepository.addRequiredAttributes(builder);
    return builder.select(QueryAttribute.PROVINCE_AREA_ID, RegisterAttribute.INSERT_DATE, RegisterAttribute.CHECKED);
  }

}
