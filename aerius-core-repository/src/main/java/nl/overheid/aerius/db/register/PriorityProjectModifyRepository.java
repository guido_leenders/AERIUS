/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.importer.SupportedUploadFormat;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Functionality for modifying priority projects in the database, e.g. updating.
 */
public final class PriorityProjectModifyRepository {

  private static final Logger LOG = LoggerFactory.getLogger(PriorityProjectModifyRepository.class);

  private static final JoinClause LOCK_JOIN_CLAUSE = new JoinClause("priority_projects", RegisterAttribute.REQUEST_ID);

  private static final Query INSERT_PRIORITY_PROJECT = InsertBuilder.into("priority_projects").insert(RegisterAttribute.REQUEST_ID,
      RegisterAttribute.DOSSIER_ID, RegisterAttribute.ASSIGN_COMPLETED).getQuery();

  private static final String UPDATE_PRIORITY_PROJECT = "UPDATE priority_projects SET remarks = ?, dossier_id = ? WHERE request_id = ? ";

  private static final String UPDATE_PRIORITY_PROJECT_ASSIGN_COMPLETE = "UPDATE priority_projects SET assign_completed = ? WHERE request_id = ? ";

  private static final String DELETE_PRIORITY_PROJECT = "SELECT ae_delete_priority_project(?) ";

  // v_request_id , v_only_within_total_reservation
  private static final String INSERT_PRIORITY_PROJECT_DEVELOPMENT_SPACES = "SELECT ae_insert_priority_project_development_spaces(?, ?)";

  private static final String UPDATE_FROM_INITIAL_TO_ASSIGNED = "UPDATE requests SET status = 'assigned'::request_status_type"
      + " WHERE request_id = ? AND status = 'initial'::request_status_type";

  private static final String ACTION_UPDATE = "updating";
  private static final String ACTION_DELETE = "deleting";
  private static final String ACTION_REQUESTFILE = "updating {0}";
  private static final String ACTION_ACTUALISATION_DELETE = "deleting actualisation";

  private PriorityProjectModifyRepository() {
    // Not allowed to instantiate.
  }

  /**
   * Insert a priority project into the database. Includes request file, situations, audit trail, and checking that it does not exist yet.
   *
   * @param con The connection to use.
   * @param priorityProject The priority project to insert.
   * @param insertedBy Which user wants to insert this project.
   * @param insertRequestFile The PDF or GML file.
   * @return The new {@link PriorityProject} object from the database (id will be set now)
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the priority project already exists (by reference or dossier ID).
   */
  public static PriorityProject insert(final Connection con, final PriorityProject priorityProject, final UserProfile insertedBy,
      final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
    ensureNew(con, priorityProject);

    final int requestId = insertRequestRecords(con, priorityProject, insertRequestFile);
    insertPriorityProjectRecord(con, priorityProject, requestId);

    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.DOSSIER_ID, null, priorityProject.getDossierMetaData().getDossierId());
    RequestAuditRepository.addNewRequestChanges(changes, priorityProject.getReference(), SegmentType.PRIORITY_PROJECTS);
    RequestAuditRepository.saveAuditTrailItem(con, priorityProject, changes, insertedBy);

    return PriorityProjectRepository.getPriorityProjectByKey(con, priorityProject.getKey());
  }

  /**
   * Insert a record in the requests* tables.
   */
  private static int insertRequestRecords(final Connection con, final PriorityProject priorityProject, final InsertRequestFile insertRequestFile)
      throws SQLException, AeriusException {
    final int requestId = RequestModifyRepository.insertNewRequest(con, SegmentType.PRIORITY_PROJECTS, priorityProject.getAuthority(),
        priorityProject, insertRequestFile);
    priorityProject.setId(requestId);
    priorityProject.setRequestState(RequestState.INITIAL);
    RequestModifyRepository.insertSituations(con, priorityProject.getSituations(), requestId);
    return requestId;
  }

  /**
   * Insert a record in the priority_projects table.
   */
  private static void insertPriorityProjectRecord(final Connection con, final PriorityProject priorityProject, final int requestId)
      throws SQLException, AeriusException {
    try (final PreparedStatement ps = con.prepareStatement(INSERT_PRIORITY_PROJECT.get())) {
      INSERT_PRIORITY_PROJECT.setParameter(ps, RegisterAttribute.REQUEST_ID, requestId);
      INSERT_PRIORITY_PROJECT.setParameter(ps, RegisterAttribute.DOSSIER_ID, priorityProject.getDossierMetaData().getDossierId());
      INSERT_PRIORITY_PROJECT.setParameter(ps, RegisterAttribute.ASSIGN_COMPLETED, Boolean.FALSE);
      ps.executeUpdate();
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        LOG.error("Priority Project already exists: {}", priorityProject.getReference(), e);
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, priorityProject.getReference());
      } else {
        throw e;
      }
    }
  }

  /**
   * @param con The connection to use.
   * @param key The key of the priority project to insert development spaces for.
   * @param editedBy Which user is performing the action.
   * @param lastModified When the entry is last modified according to the client (so we can detect if another change is already made)
   * @throws SQLException In case of a database error.
   * @throws AeriusException In case of other errors.
   */
  public static void insertDevelopmentSpaces(final Connection con, final PriorityProjectKey key, final UserProfile editedBy, final long lastModified)
      throws AeriusException {
    final PriorityProject databasePP = find(con, key, "inserting development spaces");

    try {
      final ArrayList<AuditTrailChange> changes = new ArrayList<>();
      insertDevelopmentSpaces(con, databasePP, editedBy);

      if (updateToAssigned(con, databasePP)) {
        RequestAuditRepository.addChange(changes, AuditTrailType.STATE, RequestState.INITIAL.getDbValue(), RequestState.ASSIGNED.getDbValue());
      }

      RequestModifyRepository.updateLastModified(con, databasePP);
      RequestAuditRepository.saveAuditTrailItem(con, databasePP, changes, editedBy);
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error inserting development spaces priority project: " + key, key.getDossierId());
    }
  }

  private static void insertDevelopmentSpaces(final Connection con, final PriorityProject priorityProject, final UserProfile editedBy)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT_DEVELOPMENT_SPACES)) {
      int parameterIndex = 1;
      stmt.setInt(parameterIndex++, priorityProject.getId());
      stmt.setBoolean(parameterIndex++, !editedBy.hasPermission(RegisterPermission.OVERRIDE_PRIORITY_PROJECTS_RESERVATION));
      stmt.executeQuery();
    }
  }

  private static boolean updateToAssigned(final Connection con, final PriorityProject priorityProject) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_FROM_INITIAL_TO_ASSIGNED)) {
      stmt.setInt(1, priorityProject.getId());
      stmt.executeUpdate();
      return stmt.getUpdateCount() > 0;
    }
  }

  public static void updateAssignComplete(final Connection con, final PriorityProjectKey key, final boolean assignComplete,
      final UserProfile editedBy, final long lastModified) throws AeriusException {
    final PriorityProject databasePP = find(con, key, ACTION_UPDATE);

    try {
      final LockRequestTransaction lock = new LockRequestTransaction(con, databasePP.getId(), lastModified, LOCK_JOIN_CLAUSE);
      try {
        final ArrayList<AuditTrailChange> changes = new ArrayList<>();
        RequestAuditRepository.addChange(changes, AuditTrailType.ASSIGN_COMPLETED, String.valueOf(databasePP.isAssignCompleted()),
            String.valueOf(assignComplete));
        try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_PRIORITY_PROJECT_ASSIGN_COMPLETE)) {
          int parameterIndex = 1;
          updatePS.setBoolean(parameterIndex++, assignComplete);
          updatePS.setInt(parameterIndex++, databasePP.getId());
          updatePS.executeUpdate();
        }

        RequestModifyRepository.updateLastModified(con, databasePP);
        RequestAuditRepository.saveAuditTrailItem(con, databasePP, changes, editedBy);
      } catch (final SQLException se) {
        lock.rollback();
        throw se;
      } finally {
        lock.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error updating priority project assign complete: " + key, key.getDossierId());
    }
  }

  /**
   * @param con The connection to use.
   * @param key The key of the priority project to update.
   * @param priorityProject The PriorityProject to update.
   * @param lastModified When the entry is last modified according to the client (so we can detect if another change is already made)
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the priority project does not exist (by id).
   */
  public static void update(final Connection con, final PriorityProjectKey key, final PriorityProject priorityProject, final long lastModified)
      throws AeriusException {
    final PriorityProject databasePP = find(con, key, ACTION_UPDATE);

    try {
      final LockRequestTransaction lock = new LockRequestTransaction(con, databasePP.getId(), lastModified, LOCK_JOIN_CLAUSE);
      try {
        try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_PRIORITY_PROJECT)) {
          int parameterIndex = 1;
          updatePS.setString(parameterIndex++, priorityProject.getDossierMetaData().getRemarks());
          updatePS.setString(parameterIndex++, priorityProject.getDossierMetaData().getDossierId());
          updatePS.setInt(parameterIndex++, priorityProject.getId());
          updatePS.executeUpdate();
        }

        RequestModifyRepository.updateLastModified(con, databasePP);
      } catch (final SQLException se) {
        lock.rollback();
        throw se;
      } finally {
        lock.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error updating priority project: " + key, key.getDossierId());
    }
  }

  /**
   * Delete a priority project.
   */
  public static void delete(final Connection con, final PriorityProjectKey key, final UserProfile deletedBy) throws AeriusException {
    final PriorityProject priorityProject = find(con, key, ACTION_DELETE);
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.DELETE, priorityProject.getRequestState(), null);
    try {
      try (final PreparedStatement ps = con.prepareStatement(DELETE_PRIORITY_PROJECT)) {
        QueryUtil.setValues(ps, priorityProject.getId());
        ps.executeQuery();
      }
      RequestAuditRepository.saveAuditTrailItem(con, priorityProject, changes, deletedBy);
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error deleting priority project: " + key, key.getDossierId());
    }
  }

  public static void deleteActualisation(final Connection con, final PriorityProjectKey key, final UserProfile deletedBy, final long lastModified)
      throws SQLException, AeriusException {
    final PriorityProject priorityProject = find(con, key, ACTION_ACTUALISATION_DELETE);

    final LockRequestTransaction lock = new LockRequestTransaction(con, priorityProject.getId(), lastModified, LOCK_JOIN_CLAUSE);
    try {
      if (RequestModifyRepository.deleteRequestFile(con, priorityProject.getId(), RequestFileType.PRIORITY_PROJECT_ACTUALISATION)) {
        final ArrayList<AuditTrailChange> changes = new ArrayList<>();
        RequestAuditRepository.addChange(changes, AuditTrailType.ACTUALISATION_DELETE);
        RequestModifyRepository.updateLastModified(con, priorityProject);
        RequestAuditRepository.saveAuditTrailItem(con, priorityProject, changes, deletedBy);
      }
    } catch (final SQLException se) {
      lock.rollback();
      throw se;
    } finally {
      lock.commit();
    }
  }

  public static void updateFactsheet(final Connection con, final PriorityProjectKey key, final InsertRequestFile insertRequestFile,
      final UserProfile uploadedBy) throws AeriusException {
    updateRequestFile(con, key, insertRequestFile, uploadedBy, RequestFileType.PRIORITY_PROJECT_FACTSHEET, AuditTrailType.FACTSHEET,
        SupportedUploadFormat.PRIORITY_PROJECT_FACTSHEET);
  }

  public static void updateActualisation(final Connection con, final PriorityProjectKey key, final InsertRequestFile insertRequestFile,
      final UserProfile uploadedBy) throws AeriusException {
    updateRequestFile(con, key, insertRequestFile, uploadedBy, RequestFileType.PRIORITY_PROJECT_ACTUALISATION, AuditTrailType.ACTUALISATION,
        SupportedUploadFormat.PRIORITY_PROJECT_ACTUALISATION);
  }

  private static void updateRequestFile(final Connection con, final PriorityProjectKey key, final InsertRequestFile insertRequestFile,
      final UserProfile uploadedBy, final RequestFileType requestFileType, final AuditTrailType auditTrailType,
      final SupportedUploadFormat supportedFormats) throws AeriusException {
    checkRequestFile(insertRequestFile, requestFileType, supportedFormats);
    final PriorityProject databasePP = find(con, key, MessageFormat.format(ACTION_REQUESTFILE, requestFileType));
    try {
      final LockRequestTransaction lock = new LockRequestTransaction(con, databasePP.getId(), databasePP.getLastModified(), LOCK_JOIN_CLAUSE);
      try {
        updateRequestFile(con, databasePP, insertRequestFile, uploadedBy, requestFileType, auditTrailType);
      } catch (final SQLException se) {
        lock.rollback();
        throw se;
      } finally {
        lock.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error updating requestFile " + requestFileType + " for priority project: " + key,
          key.getDossierId());
    }
  }

  private static void updateRequestFile(final Connection con, final PriorityProject databasePP, final InsertRequestFile insertRequestFile,
      final UserProfile uploadedBy, final RequestFileType requestFileType, final AuditTrailType auditTrailType) throws SQLException, AeriusException {
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    // retrieve existing file information for audit trail.
    final RequestFile existing = RequestRepository.getSpecificRequestFile(con, databasePP.getReference(), requestFileType);
    RequestAuditRepository.addChange(changes, auditTrailType, existing == null ? null : existing.getFileName(), insertRequestFile.getFileName());
    // delete existing before inserting the new (only 1 allowed).
    RequestModifyRepository.deleteRequestFile(con, databasePP.getId(), requestFileType);
    RequestModifyRepository.insertFileContent(con, databasePP.getId(), insertRequestFile);
    RequestModifyRepository.updateLastModified(con, databasePP);
    RequestAuditRepository.saveAuditTrailItem(con, databasePP, changes, uploadedBy);
  }

  private static void checkRequestFile(final InsertRequestFile insertRequestFile, final RequestFileType requestFileType,
      final SupportedUploadFormat supportedFormats) throws AeriusException {
    if (insertRequestFile.getRequestFileType() != requestFileType) {
      LOG.error("RequestFileType wasn't {} but {}", requestFileType, insertRequestFile.getRequestFileType());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    if (!SupportedUploadFormat.isSupported(supportedFormats, insertRequestFile.getFileFormat())) {
      LOG.error("FileFormat {} of file '{}' isn't correct for {}.", insertRequestFile.getFileFormat(), insertRequestFile.getFileName(),
          requestFileType);
      throw new AeriusException(Reason.IMPORT_FILE_UNSUPPORTED);
    }
  }

  /**
   * Fetches a priority project via its key, and throws/logs errors when it does not exist.
   */
  static PriorityProject find(final Connection con, final DossierAuthorityKey key, final String action) throws AeriusException {
    return find(con, key, action, null);
  }

  /**
   * Fetches a priority project via its key, and throws/logs errors when it does not exist.
   */
  static PriorityProject find(final Connection con, final DossierAuthorityKey authKey, final String action, final DBMessagesKey key)
      throws AeriusException {
    try {
      final PriorityProject priorityProject = PriorityProjectRepository.getSkinnedPriorityProjectByKey(con, authKey, key);
      if (priorityProject == null) {
        LOG.error("Unknown priority project key while {}: {}", action, key);
        throw new AeriusException(Reason.PRIORITY_PROJECT_UNKNOWN, key == null ? "" : authKey.getDossierId());
      }
      return priorityProject;
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error getting existing priority project: " + authKey, authKey.getDossierId());
    }
  }

  private static void ensureNew(final Connection con, final PriorityProject priorityProject) throws AeriusException, SQLException {
    ensureNew(con, priorityProject, null);
  }

  private static void ensureNew(final Connection con, final PriorityProject priorityProject, final DBMessagesKey key)
      throws AeriusException, SQLException {
    final PriorityProject existingPermitByKey = PriorityProjectRepository.getSkinnedPriorityProjectByKey(con, priorityProject.getKey(), key);
    final PriorityProjectKey existingKeyByReference = PriorityProjectRepository.getPriorityProjectKey(con, priorityProject.getReference());
    if (existingPermitByKey != null || existingKeyByReference != null) {
      throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, priorityProject.getKey().getDossierId(), priorityProject.getKey().getAuthorityCode());
    }
  }

}
