/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.GeneralRepository;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestSituation;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

/**
 * Functionality for modifying generic requests in the database, e.g. inserting.
 */
public final class RequestModifyRepository {

  //The logger.
  private static final Logger LOG = LoggerFactory.getLogger(RequestModifyRepository.class);

  private static final String INSERT_REQUEST = "INSERT INTO requests "
      + " (segment, status, authority_id, province_area_id, sector_id, has_multiple_sectors, reference, start_year, "
      + " temporary_period, corporation, project_name, description, application_version, database_version, geometry) "
      + " VALUES (?::segment_type, ?::request_status_type, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid())) ";

  private static final String INSERT_REQUEST_SITUATION_CALCULATIONS = "INSERT INTO request_situation_calculations "
      + " (request_id, situation, calculation_id) "
      + " VALUES (?, ?::situation_type, ?) ";

  private static final String INSERT_REQUEST_SITUATION_PROPERTIES =
      "INSERT INTO request_situation_properties "
          + " (request_id, situation, name) "
          + " VALUES (?, ?::situation_type, ?)";

  private static final String INSERT_REQUEST_SITUATION_EMISSIONS =
      "INSERT INTO request_situation_emissions "
          + " (request_id, situation, substance_id, total_emission) "
          + " VALUES (?, ?::situation_type, ?, ?)";

  private static final String DELETE_REQUEST_SITUATION_EMISSIONS =
      "DELETE FROM request_situation_calculations WHERE calculation_id = ?";

  private static final String INSERT_REQUEST_FILE_CONTENT = "INSERT INTO request_files "
      + " (request_id, file_type, file_format_type, file_name, content) "
      + " VALUES (?, ?::request_file_type, ?::request_file_format_type, ?, ?) ";

  private static final String DELETE_REQUEST_FILE = "DELETE FROM request_files "
      + " WHERE request_id = ? AND file_type = ?::request_file_type";

  private static final String UPDATE_REQUEST_AUTHORITY = "UPDATE requests "
      + " SET authority_id = ? "
      + " WHERE request_id = ? ";

  private static final String UPDATE_MODIFIED =
      "UPDATE requests SET "
          + " last_modified = ? "
          + " WHERE request_id = ? ";

  private static final String UPDATE_MARKED =
      "UPDATE requests SET "
          + " marked = ? "
          + " WHERE request_id = ? ";

  private static final String UPDATE_FROM_INITIAL_TO_QUEUED =
      "UPDATE requests SET "
          + " status = 'queued'::request_status_type"
          + " WHERE request_id = ? AND status = 'initial'::request_status_type";

  private RequestModifyRepository() {
    //Not allowed to instantiate.
  }

  static int insertNewRequest(final Connection con, final SegmentType segment, final Authority authority, final Request request,
      final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
    int requestId;
    validateReference(request.getScenarioMetaData().getReference());

    final Province province = GeneralRepository.determineProvince(con, request.getPoint());
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST, Statement.RETURN_GENERATED_KEYS)) {
      // (segment, status, authority_id, sector_id, has_multiple_sectors, reference, start_year, temporary_period,
      // corporation, project_name, description, geometry)
      int parameterIdx = 1;
      stmt.setString(parameterIdx++, segment.getDbValue());
      stmt.setString(parameterIdx++, RequestState.INITIAL.getDbValue());
      stmt.setInt(parameterIdx++, authority.getId());
      stmt.setInt(parameterIdx++, province.getId());
      stmt.setInt(parameterIdx++, request.getSector().getSectorId());
      stmt.setBoolean(parameterIdx++, request.isMultipleSectors());
      stmt.setString(parameterIdx++, request.getScenarioMetaData().getReference());
      stmt.setInt(parameterIdx++, request.getStartYear());
      stmt.setObject(parameterIdx++, request.getTemporaryPeriod(), Types.INTEGER);
      stmt.setString(parameterIdx++, getValidValue(request.getScenarioMetaData().getCorporation()));
      stmt.setString(parameterIdx++, getValidValue(request.getScenarioMetaData().getProjectName()));
      stmt.setString(parameterIdx++, getValidValue(request.getScenarioMetaData().getDescription()));
      stmt.setString(parameterIdx++, request.getApplicationVersion());
      stmt.setString(parameterIdx++, request.getDatabaseVersion());
      stmt.setDouble(parameterIdx++, request.getPoint().getX());
      stmt.setDouble(parameterIdx++, request.getPoint().getY());

      stmt.executeUpdate();
      final ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        requestId = rs.getInt(1);
      } else {
        throw new SQLException("No generated key obtained while saving new request: {}",
            request.getReference());
      }
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, request.getReference());
      } else {
        throw e;
      }
    }

    insertFileContent(con, requestId, insertRequestFile);
    return requestId;
  }

  private static String getValidValue(final String value) {
    return value == null ? "" : value;
  }

  public static void insertSituationCalculation(final Connection con, final SituationType situation, final int calculationId, final Request request)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST_SITUATION_CALCULATIONS)) {
      // (appendix_id, situation, calculation_id)
      stmt.setInt(1, request.getId());
      stmt.setString(2, situation.getDBName());
      stmt.setInt(3, calculationId);
      stmt.executeUpdate();
    }
  }

  public static void insertSituations(final Connection con, final HashMap<SituationType, RequestSituation> situationMap, final int requestId)
      throws SQLException {
    for (final Entry<SituationType, RequestSituation> entry : situationMap.entrySet()) {
      try (final PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST_SITUATION_PROPERTIES)) {
        // (appendix_id, situation, name)
        stmt.setInt(1, requestId);
        stmt.setString(2, entry.getKey().getDBName());
        stmt.setString(3, entry.getValue().getName());
        stmt.executeUpdate();
        insertTotalEmissions(con, entry.getValue().getTotalEmissionValues(), requestId, entry.getKey());
      }
    }
  }

  private static void insertTotalEmissions(final Connection con, final EmissionValues emissionValues, final int requestId,
      final SituationType situation)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST_SITUATION_EMISSIONS)) {
      // (request_id, situation, substance_id, total_emission)
      for (final EmissionValueKey key : emissionValues.getKeys()) {
        if (key.getSubstance() != Substance.NOXNH3) {
          int parameterIdx = 1;
          stmt.setInt(parameterIdx++, requestId);
          stmt.setString(parameterIdx++, situation.getDBName());
          stmt.setInt(parameterIdx++, key.getSubstance().getId());
          stmt.setDouble(parameterIdx++, emissionValues.getEmission(key));
          stmt.executeUpdate();
        }
      }
    }
  }

  /**
   * @param con The connection to use.
   * @param requestId The ID of the request for which to insert the file.
   * @param insertRequestFile The imported file for the request.
   * @throws SQLException In case of database exceptions.
   * @throws AeriusException In case of exceptions inserting the content.
   */
  public static void insertFileContent(final Connection con, final int requestId, final InsertRequestFile insertRequestFile)
      throws SQLException, AeriusException {
    try (final InputStream inputStream = new ByteArrayInputStream(insertRequestFile.getFileContent());
        PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST_FILE_CONTENT)) {
      // (request_id, file_type, file_format_type, file_name, content)
      int parameterIdx = 1;
      stmt.setInt(parameterIdx++, requestId);
      stmt.setString(parameterIdx++, insertRequestFile.getRequestFileType().name().toLowerCase());
      stmt.setString(parameterIdx++, insertRequestFile.getFileFormat().name().toLowerCase());
      stmt.setString(parameterIdx++, insertRequestFile.getFileName());
      stmt.setBinaryStream(parameterIdx++, inputStream, insertRequestFile.getFileContent().length);
      stmt.executeUpdate();
    } catch (final IOException e) {
      LOG.error("Exception when inserting PDF content", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  /**
   * @param con The connection to use.
   * @param requestId The request to delete a file for.
   * @param requestFileType The type of the file to delete.
   * @return true if the request file was found and delete, false otherwise.
   * @throws SQLException In case of errors deleting.
   */
  static boolean deleteRequestFile(final Connection con, final int requestId, final RequestFileType requestFileType)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(DELETE_REQUEST_FILE)) {
      int parameterIdx = 1;
      stmt.setInt(parameterIdx++, requestId);
      stmt.setString(parameterIdx++, requestFileType.name().toLowerCase());
      return stmt.executeUpdate() > 0;
    }
  }

  /**
   * For a request object deletes the reference to the calculations.
   * @param con The connection to use
   * @param calculationId calculation id
   * @throws SQLException In case of a database error.
   */
  public static void removeCalculationReference(final Connection con, final int calculationId) throws SQLException {
    try (final PreparedStatement deletePS = con.prepareStatement(DELETE_REQUEST_SITUATION_EMISSIONS)) {
      deletePS.setInt(1, calculationId);
      deletePS.executeUpdate();
    }
  }

  static void updateRequestAuthority(final Connection con, final Request request, final Authority authority) throws SQLException, AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_REQUEST_AUTHORITY)) {
      int parameterIdx = 1;
      stmt.setInt(parameterIdx++, authority.getId());
      stmt.setInt(parameterIdx++, request.getId());

      stmt.executeUpdate();
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, request.getReference());
      } else {
        throw e;
      }
    }
  }

  static void updateLastModified(final Connection con, final Request request) throws SQLException {
    try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_MODIFIED)) {
      int parameterIndex = 1;
      updatePS.setTimestamp(parameterIndex++, new Timestamp(new Date().getTime()));
      updatePS.setInt(parameterIndex++, request.getId());
      updatePS.executeUpdate();
    }
  }

  public static void updateMarked(final Connection con, final Request request, final boolean marked) throws SQLException {
    try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_MARKED)) {
      int parameterIndex = 1;
      updatePS.setBoolean(parameterIndex++, marked);
      updatePS.setInt(parameterIndex++, request.getId());
      updatePS.executeUpdate();
    }
  }

  /**
   * @param con The connection to use.
   * @param request The request to update the status to QUEUED for. Only works when status is initial, else ignored.
   * @param editedBy The userprofile doing the update. Will be used for audit trail.
   * @throws SQLException In case of database exceptions.
   */
  public static void updateRequestToQueued(final Connection con, final Request request, final UserProfile editedBy) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_FROM_INITIAL_TO_QUEUED)) {
      final ArrayList<AuditTrailChange> changes = new ArrayList<>();
      RequestAuditRepository.addChange(changes, AuditTrailType.STATE, RequestState.INITIAL.getDbValue(), RequestState.QUEUED.getDbValue());
      stmt.setInt(1, request.getId());

      stmt.executeUpdate();
      if (stmt.getUpdateCount() > 0) {
        RequestAuditRepository.saveAuditTrailItem(con, request, changes, editedBy);
      }
    }
  }

  private static void validateReference(final String reference) throws AeriusException {
    if (!ReferenceUtil.validateAnyReference(reference)) {
      throw new AeriusException(Reason.REQUEST_INVALID_REFERENCE, reference);
    }
  }

}
