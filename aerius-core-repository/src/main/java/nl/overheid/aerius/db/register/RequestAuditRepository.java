/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.InsertClause;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.user.UserProfile;

/**
 *
 */
final class RequestAuditRepository {

  private enum RepositoryAttribute implements Attribute {

    REQUEST_AUDIT_TRAIL_ITEM_ID,
    AUDIT_TRAIL_DATE_TIME,
    AUDIT_TRAIL_TYPE,
    OLD_VALUE,
    NEW_VALUE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Query INSERT_REQUEST_AUDIT_TRAIL_ITEM = InsertBuilder.into("request_audit_trail_items")
  .insert(new InsertClause(RegisterAttribute.SEGMENT.attribute(), "?::segment_type", RegisterAttribute.SEGMENT))
  .insert(RegisterAttribute.REQUEST_ID, RegisterAttribute.REFERENCE, RegisterAttribute.USER_ID, RepositoryAttribute.AUDIT_TRAIL_DATE_TIME)
  .getQuery();

  private static final String INSERT_REQUEST_AUDIT_TRAIL_CHANGE =
      "INSERT INTO request_audit_trail_item_changes "
          + " (request_audit_trail_item_id, audit_trail_type, old_value, new_value) "
          + " VALUES (?, ?, ?, ?) ";

  private static final String UPDATE_REQUEST_IDS = "UPDATE request_audit_trail_items "
      + " SET request_id = ? WHERE request_id = ?";

  private static final Query GET_REQUEST_AUDIT_TRAIL = getAuditTrailQuery("requests_audit_trail_view");

  private RequestAuditRepository() {
    //Not allowed to instantiate.
  }

  static void saveAuditTrailItem(final Connection con, final Request request, final ArrayList<AuditTrailChange> changes,
      final UserProfile editedBy) throws SQLException {
    final AuditTrailItem createdAuditTrailItem = new AuditTrailItem(new Date(), editedBy, new ArrayList<AuditTrailChange>());
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST_AUDIT_TRAIL_ITEM.get(), Statement.RETURN_GENERATED_KEYS)) {
      INSERT_REQUEST_AUDIT_TRAIL_ITEM.setParameter(stmt, RegisterAttribute.REQUEST_ID, request.getId());
      INSERT_REQUEST_AUDIT_TRAIL_ITEM.setParameter(stmt, RegisterAttribute.REFERENCE, request.getReference());
      INSERT_REQUEST_AUDIT_TRAIL_ITEM.setParameter(stmt, RegisterAttribute.SEGMENT, request.getSegment().getDbValue());
      INSERT_REQUEST_AUDIT_TRAIL_ITEM.setParameter(stmt, RegisterAttribute.USER_ID, createdAuditTrailItem.getEditedBy().getId());
      INSERT_REQUEST_AUDIT_TRAIL_ITEM.setParameter(stmt, RepositoryAttribute.AUDIT_TRAIL_DATE_TIME, createdAuditTrailItem.getDate(), Types.TIMESTAMP);

      stmt.executeUpdate();
      final ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        createdAuditTrailItem.setId(rs.getLong(1));
      } else {
        throw new SQLException("No generated key obtained while saving new audit trail item.");
      }
    }
    for (final AuditTrailChange change : changes) {
      try (final PreparedStatement stmt = con.prepareStatement(INSERT_REQUEST_AUDIT_TRAIL_CHANGE)) {
        // (request_audit_trail_item_id, audit_trail_type, old_value, new_value)
        int parameterIndex = 1;
        stmt.setLong(parameterIndex++, createdAuditTrailItem.getId());
        stmt.setString(parameterIndex++, change.getType().name());
        stmt.setString(parameterIndex++, change.getOldValue());
        stmt.setString(parameterIndex++, change.getNewValue());

        stmt.executeUpdate();
        createdAuditTrailItem.getItems().add(change);
      }
    }
  }

  static void updateRequestIds(final Connection con, final int prevRequestId, final int newRequestId) throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(UPDATE_REQUEST_IDS)) {
      QueryUtil.setValues(ps, newRequestId, prevRequestId);
      ps.executeUpdate();
    }
  }

  static ArrayList<AuditTrailItem> getAuditTrail(final Connection con, final int requestId) throws SQLException {
    return getAuditTrail(con, requestId, GET_REQUEST_AUDIT_TRAIL);
  }

  private static ArrayList<AuditTrailItem> getAuditTrail(final Connection con, final int requestId, final Query query) throws SQLException {
    final Map<Long, AuditTrailItem> auditTrailMap = new HashMap<>();
    final ArrayList<AuditTrailItem> auditTrail = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(query.get())) {
      query.setParameter(selectPS, RegisterAttribute.REQUEST_ID, requestId);

      final ResultSet rs = selectPS.executeQuery();
      // segment, request_audit_trail_item_id, user_id, audit_trail_date_time, audit_trail_type, old_value, new_value
      while (rs.next()) {
        final long itemId = QueryUtil.getInt(rs, RepositoryAttribute.REQUEST_AUDIT_TRAIL_ITEM_ID);
        AuditTrailItem item = auditTrailMap.get(itemId);
        if (item == null) {
          item = new AuditTrailItem();
          item.setId(itemId);
          item.setDate(QueryUtil.getDate(rs, RepositoryAttribute.AUDIT_TRAIL_DATE_TIME));
          item.setEditedBy(UserRepository.getUserProfile(con, QueryAttribute.USER_ID.getInt(rs)));
          item.setItems(new ArrayList<AuditTrailChange>());
          item.setSegment(SegmentType.safeValueOf(QueryAttribute.SEGMENT.getString(rs)));
          item.setReference(QueryAttribute.REFERENCE.getString(rs));
          auditTrailMap.put(itemId, item);
          auditTrail.add(item);
        }
        final AuditTrailChange change = new AuditTrailChange();
        change.setType(QueryUtil.getEnum(rs, RepositoryAttribute.AUDIT_TRAIL_TYPE, AuditTrailType.class));
        change.setOldValue(QueryUtil.getString(rs, RepositoryAttribute.OLD_VALUE));
        change.setNewValue(QueryUtil.getString(rs, RepositoryAttribute.NEW_VALUE));
        item.getItems().add(change);
      }
    }
    return auditTrail;
  }

  /**
   * Add a change to the audit trail. Old and new value are enum values. The change is only added if the old and new
   * values are different, and if at least one of them is not null.
   */
  static <T extends Enum<?>> void addChange(final ArrayList<AuditTrailChange> auditTrail, final AuditTrailType type, final T oldValue,
      final T newValue) {
    addChange(auditTrail, type, oldValue != null ? oldValue.name() : null, newValue != null ? newValue.name() : null);
  }

  /**
   * Add a change to the audit trail. Old and new value is a string.
   */
  static void addChange(final ArrayList<AuditTrailChange> auditTrail, final AuditTrailType type, final String oldValue, final String newValue) {
    addChangeItem(auditTrail, type, oldValue, newValue);
  }

  /**
   * Add a change to the audit trail that is not associated with an old and new value. Usually this is an action.
   */
  static void addChange(final ArrayList<AuditTrailChange> auditTrail, final AuditTrailType type) {
    addChangeItem(auditTrail, type, null, null);
  }

  static void addNewRequestChanges(final ArrayList<AuditTrailChange> auditTrail, final String reference, final SegmentType segmentType) {
    addChange(auditTrail, AuditTrailType.AERIUS_ID, null, reference);
    addChange(auditTrail, AuditTrailType.SEGMENT, null, segmentType.getDbValue());
    addChange(auditTrail, AuditTrailType.STATE, null, RequestState.INITIAL.getDbValue());
  }

  /**
   * Add a change item without checking whether old and new values are filled.
   */
  private static void addChangeItem(final ArrayList<AuditTrailChange> auditTrail, final AuditTrailType type,
      final String oldValue, final String newValue) {
    final AuditTrailChange change = new AuditTrailChange();
    change.setNewValue(newValue);
    change.setOldValue(oldValue);
    change.setType(type);
    auditTrail.add(0, change);
  }

  private static Query getAuditTrailQuery(final String view) {
    return QueryBuilder.from(view)
        .select(QueryAttribute.SEGMENT, QueryAttribute.REFERENCE, RepositoryAttribute.REQUEST_AUDIT_TRAIL_ITEM_ID, QueryAttribute.USER_ID,
            RepositoryAttribute.AUDIT_TRAIL_DATE_TIME, RepositoryAttribute.AUDIT_TRAIL_TYPE, RepositoryAttribute.OLD_VALUE,
            RepositoryAttribute.NEW_VALUE)
        .where(RegisterAttribute.REQUEST_ID)
        .orderBy(new OrderByClause(RepositoryAttribute.AUDIT_TRAIL_DATE_TIME, OrderType.DESC),
            new OrderByClause(RepositoryAttribute.REQUEST_AUDIT_TRAIL_ITEM_ID, OrderType.DESC),
            new OrderByClause(RepositoryAttribute.AUDIT_TRAIL_TYPE, OrderType.DESC)).getQuery();
  }

}
