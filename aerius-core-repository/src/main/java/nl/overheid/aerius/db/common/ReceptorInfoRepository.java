/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.postgis.PGbox2d;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.common.HabitatRepository.HabitatInfoCollector;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.EcologyQualityType;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Service class for info related database data.
 */
public final class ReceptorInfoRepository {

  private enum RepositoryAttribute implements Attribute {

    NATURA2000_AREA_ID,
    ECOLOGY_QUALITY_TYPE,
    LANDSCAPE_TYPE,
    DIRECTIVE,
    DESIGN_STATUS,

    ESTABLISHED_DATE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Attributes BACKGROUND_RESULTS_ATTRIBUTES = new Attributes(
      QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT_TYPE, QueryAttribute.RESULT);

  private static final Query SELECT_HABITAT_AREA_IN_RECEPTOR_INFO = QueryBuilder.from("relevant_habitat_info_for_receptor_view")
      .select(QueryAttribute.HABITAT_TYPE_ID, QueryAttribute.CRITICAL_LEVEL, QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT_TYPE,
          QueryAttribute.SURFACE)
      .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_NATURA2000_AREA_INFO = QueryBuilder.from("natura2000_area_info_view")
      .join(new JoinClause("receptors_to_assessment_areas", QueryAttribute.ASSESSMENT_AREA_ID))
      .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_BACKGROUND_EMISSION_RESULTS = QueryBuilder.from("background_cell_results_view")
      .select(BACKGROUND_RESULTS_ATTRIBUTES)
      .where(QueryAttribute.YEAR).where(new StaticWhereClause("ST_Within(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), geometry)",
          QueryAttribute.X_COORD, QueryAttribute.Y_COORD))
      .getQuery();

  private static final Query SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS = QueryBuilder.from("background_results_view")
      .select(BACKGROUND_RESULTS_ATTRIBUTES)
      .where(QueryAttribute.YEAR, QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query SELECT_HABITAT_NAME_INFO = QueryBuilder.from("relevant_habitat_name_info_for_receptor_view")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.ASSESSMENT_AREA_NAME,
          QueryAttribute.HABITAT_TYPE_ID)
      .where(QueryAttribute.RECEPTOR_ID).getQuery();

  // Not allowed to instantiate.
  private ReceptorInfoRepository() {
  }

  /**
   * Get the information about natura 2000 areas that cover at least part of a specific receptor (hexagon).
   *
   * @param con The connection to use for the DB-query.
   * @param aeriusPoint The receptor to get the information for. Information retrieved based on ID.
   * @return The list of natura2000 areas that insersect with the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static ArrayList<Natura2000Info> getBasicNaturaAreaInfo(final Connection con, final AeriusPoint aeriusPoint) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SELECT_NATURA2000_AREA_INFO.get())) {
      SELECT_NATURA2000_AREA_INFO.setParameter(statement, QueryAttribute.RECEPTOR_ID, aeriusPoint.getId());
      final ResultSet rs = statement.executeQuery();
      final ArrayList<Natura2000Info> lst = new ArrayList<>();
      while (rs.next()) {
        lst.add(getNatura2000Info(rs));
      }
      return lst;
    }
  }

  /**
   * Get the information about natura 2000 areas that cover at least part of a specific receptor (hexagon).
   * Includes habitat info in the assessment area.
   *
   * @param con The connection to use for the DB-query.
   * @param receptor The receptor to get the information for.
   * @return The list of natura2000 areas that insersect with the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static ArrayList<Natura2000Info> getNaturaAreaInfo(final Connection con, final AeriusPoint receptor, final DBMessagesKey key)
      throws SQLException {
    final ArrayList<Natura2000Info> lst = getBasicNaturaAreaInfo(con, receptor);
    for (final Natura2000Info info : lst) {
      info.setHabitats(HabitatRepository.getRelevantHabitatInfo(con, info.getId(), key));
    }

    return lst;
  }

  /**
   * Get information about all habitat types that are inside a specific receptor (hexagon).
   *
   * @param con The connection to use for the DB-query.
   * @param receptor The receptor to get the information for.
   * @return The list of all habitat types that are inside the hexagon that goes with the receptor.
   * @throws SQLException When an error occurs executing the query.
   */
  public static ArrayList<HabitatInfo> getReceptorHabitatAreaInfo(final Connection con, final AeriusPoint receptor, final DBMessagesKey key)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(SELECT_HABITAT_AREA_IN_RECEPTOR_INFO.get())) {
      SELECT_HABITAT_AREA_IN_RECEPTOR_INFO.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptor.getId());
      final ResultSet rs = statement.executeQuery();

      final HabitatInfoCollector habitatInfoCollector = new HabitatInfoCollector(key);
      habitatInfoCollector.collectEntities(rs);
      return habitatInfoCollector.getEntities();
    }
  }

  /**
   * Get the background concentration and deposition information for a specific receptor.
   *
   * I can see you wondering; why in the name of the great oozelords would this code execute 2 queries to achieve the (seemingly) same thing?
   *
   * Well, the answer is simple.
   * The former is a general query for all background concentrations and depositions at a given point.
   * The latter is a specific query for (improved) background depositions inside a Natura 2000 area.
   * If the second query finds such a deposition, it will overwrite the deposition from the first query.
   *
   * @param con The connection to use for the DB-query.
   * @param rec The receptor to get the info for.
   * @param year The year to get the info for.
   * @return The deposition info for the specified receptor/version/year combination.
   * @throws SQLException When an error occurs executing the query.
   */
  public static EmissionResults getBackgroundEmissionResult(final Connection con, final AeriusPoint rec, final int year)
      throws SQLException {
    final EmissionResults emissionResults = new EmissionResults();

    try (final PreparedStatement statement = con.prepareStatement(SELECT_BACKGROUND_EMISSION_RESULTS.get())) {
      SELECT_BACKGROUND_EMISSION_RESULTS.setParameter(statement, QueryAttribute.YEAR, year);
      SELECT_BACKGROUND_EMISSION_RESULTS.setParameter(statement, QueryAttribute.X_COORD, rec.getX());
      SELECT_BACKGROUND_EMISSION_RESULTS.setParameter(statement, QueryAttribute.Y_COORD, rec.getY());
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        addBackgroundEmissionResult(rs, emissionResults);
      }
    }
    // Override (improved) background depositions inside Natura 2000 areas.
    try (final PreparedStatement statement = con.prepareStatement(SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS.get())) {
      SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS.setParameter(statement, QueryAttribute.YEAR, year);
      SELECT_RECEPTOR_BACKGROUND_EMISSION_RESULTS.setParameter(statement, QueryAttribute.RECEPTOR_ID, rec.getId());
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        addBackgroundEmissionResult(rs, emissionResults);
      }
    }

    return emissionResults;
  }

  private static void addBackgroundEmissionResult(final ResultSet rs, final EmissionResults emissionResults) throws SQLException {
    final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
    final EmissionResultType resultType = QueryAttribute.RESULT_TYPE.getEnum(rs, EmissionResultType.class);
    final double result = QueryAttribute.RESULT.getDouble(rs);
    emissionResults.put(EmissionResultKey.valueOf(substance, resultType), result);
  }

  /**
   * @param rs The result set containing the natura2000 area information.
   * @return The filled N2K info based on the result set.
   * @throws SQLException In case of errors retrieving properties from the resultset.
   */
  public static Natura2000Info getNatura2000Info(final ResultSet rs) throws SQLException {
    final Natura2000Info info = new Natura2000Info();
    info.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
    info.setNatura2000AreaId(QueryUtil.getInt(rs, RepositoryAttribute.NATURA2000_AREA_ID));
    info.setName(QueryAttribute.NAME.getString(rs));
    info.setContractor(QueryAttribute.AUTHORITY.getString(rs));
    info.setEcologyQualityType(EcologyQualityType.getByCode(QueryUtil.getString(rs, RepositoryAttribute.ECOLOGY_QUALITY_TYPE)));
    info.setEnvironment(QueryUtil.getString(rs, RepositoryAttribute.LANDSCAPE_TYPE));
    info.setSurface(Math.round(QueryUtil.getLong(rs, QueryAttribute.SURFACE) / SharedConstants.M2_TO_HA)); // Surface is in m^2, convert to ha
    info.setProtection(QueryUtil.getString(rs, RepositoryAttribute.DIRECTIVE));
    info.setStatus(QueryUtil.getString(rs, RepositoryAttribute.DESIGN_STATUS));
    info.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
    return info;
  }

  /**
   * Get basic assessment area and habitat type information.
   * @param con Connection to use.
   * @param receptorId The ID of the receptor to get the information for.
   * @return The map containing all assessment areas that touch the hexagon of the receptor and all (relevant) habitats contained within.
   * @throws SQLException In case of database errors.
   */
  public static HashMap<AssessmentArea, ArrayList<HabitatType>> getPasAreaAndHabitatInfo(final Connection con, final int receptorId,
      final DBMessagesKey key) throws SQLException {
    final HashMap<AssessmentArea, ArrayList<HabitatType>> info = new HashMap<>();
    final EntityCollector<AssessmentArea> areaCollector = new EntityCollector<AssessmentArea>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
      }

      @Override
      public AssessmentArea fillEntity(final ResultSet rs) throws SQLException {
        final AssessmentArea area = new AssessmentArea();
        area.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
        area.setName(QueryAttribute.ASSESSMENT_AREA_NAME.getString(rs));
        return area;
      }
    };
    try (final PreparedStatement statement = con.prepareStatement(SELECT_HABITAT_NAME_INFO.get())) {
      statement.setInt(1, receptorId);
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final AssessmentArea area = areaCollector.collectEntity(rs);
        if (!info.containsKey(area)) {
          info.put(area, new ArrayList<HabitatType>());
        }
        final HabitatType habitatType = new HabitatType();
        habitatType.setId(QueryAttribute.HABITAT_TYPE_ID.getInt(rs));
        if (key != null) {
          DBMessages.setHabitatTypeMessages(habitatType, key);
        }
        info.get(area).add(habitatType);
      }
    }
    return info;
  }
}
