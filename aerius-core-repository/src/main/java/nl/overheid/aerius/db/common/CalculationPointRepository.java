/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgis.Geometry;
import org.postgis.GeometryCollection;
import org.postgis.PGgeometry;

import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.DetermineCalculationPointResult;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Service class for Calculation Point specific stuff.
 */
public final class CalculationPointRepository {

  // If the distance is higher than this value in KM's, only return assessment area CP's.
  static final int SWITCH_TO_ASSESSMENT_ONLY_LIMIT = 10;

  private static final String QUERY_CALCPOINTS_ASSESSMENT_AREA =
      "SELECT assessment_area_id, NULL AS habitat_name, assessment_area_name, distance, ST_X(geometry) AS xpos, ST_Y(geometry) as ypos "
          + "FROM ae_assessment_area_points_of_interest_by_source_collection(?, ?) ";
  private static final String QUERY_CALCPOINTS_ASSESSMENT_AREA_PLUS_HABITAT =
      "SELECT assessment_area_id, habitat_name, assessment_area_name, distance, ST_X(geometry) AS xpos, ST_Y(geometry) as ypos "
          + "FROM ae_relevant_habitat_points_of_interest_by_source_collection(?, ?) "
          + "UNION "
          + QUERY_CALCPOINTS_ASSESSMENT_AREA;

  private CalculationPointRepository() {
  }

  /**
   * Determine some calculation points based on nature-area's and/or habitats.
   *
   * @param con Database connection
   * @param distanceFromSource the distance from the sources
   * @param sourceLists The source list(s) to determine calculation points for
   * @param srid the geometric srid of the points.
   * @return result
   * @throws SQLException database related exception
   */
  public static DetermineCalculationPointResult determinePointsAutomatically(final Connection con,
      final int distanceFromSource, final ArrayList<EmissionSourceList> sourceLists) throws SQLException {
    final DetermineCalculationPointResult result = new DetermineCalculationPointResult();
    final int srid = ReceptorGridSettingsRepository.getSrid(con);
    final GeometryCollection coll = extractGeometries(sourceLists, srid);

    if (coll != null) {
      final boolean assessmentOnly = distanceFromSource > SWITCH_TO_ASSESSMENT_ONLY_LIMIT;

      try (final PreparedStatement stmt = con.prepareStatement(
          assessmentOnly ? QUERY_CALCPOINTS_ASSESSMENT_AREA : QUERY_CALCPOINTS_ASSESSMENT_AREA_PLUS_HABITAT)) {
        stmt.setObject(1, new PGgeometry(coll));
        stmt.setInt(2, distanceFromSource * SharedConstants.M_TO_KM);
        if (!assessmentOnly) {
          stmt.setObject(3, new PGgeometry(coll));
          stmt.setInt(4, distanceFromSource * SharedConstants.M_TO_KM);
        }
        final ResultSet rst = stmt.executeQuery();

        while (rst.next()) {
          final double xPos = MathUtil.round(rst.getDouble("xpos"));
          final double yPos = MathUtil.round(rst.getDouble("ypos"));
          final double distance = rst.getDouble("distance");
          final AeriusPoint existingPoint = result.getCalculationPoint(xPos, yPos);

          final StringBuilder label = new StringBuilder(rst.getString("assessment_area_name"));
          if (rst.getString("habitat_name") != null) {
            label.append(' ').append(rst.getString("habitat_name"));
          }

          if (existingPoint != null) {
            existingPoint.setLabel(existingPoint.getLabel() + " & " + label);
            continue;
          }

          if (distance == 0) {
            label.insert(0, DetermineCalculationPointResult.OVERLAP_CENTROID_START);
          } else if (distance >= SharedConstants.M_TO_KM / 2) {
            label.append(" (").append(Math.round(distance / SharedConstants.M_TO_KM)).append(" km)");
          }

          final AeriusPoint cp = new AeriusPoint(0, AeriusPointType.POINT, xPos, yPos);

          cp.setLabel(label.toString());
          result.add(rst.getInt("assessment_area_id"), cp);
        }
      }
    }

    return result;
  }

  private static GeometryCollection extractGeometries(final ArrayList<EmissionSourceList> sourceLists, final int srid) {
    final List<Geometry> geoms = new ArrayList<>();

    for (final EmissionSourceList list : sourceLists) {
      for (final EmissionSource source : list) {
        geoms.add(PGisUtils.getGeometry(source.getGeometry().getWKT()));
      }
    }
    if (geoms.isEmpty()) {
      return null;
    } else {
      final GeometryCollection coll = new GeometryCollection(geoms.toArray(new Geometry[geoms.size()]));
      coll.setSrid(srid);
      return coll;
    }
  }

}
