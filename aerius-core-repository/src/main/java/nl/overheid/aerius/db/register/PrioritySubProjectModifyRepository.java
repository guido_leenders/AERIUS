/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.InsertBuilder;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.auth.RegisterPermission;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Functionality for modifying priority subprojects in the database, e.g. inserting, deleting, status change.
 */
public final class PrioritySubProjectModifyRepository {

  private static final Logger LOG = LoggerFactory.getLogger(PrioritySubProjectModifyRepository.class);

  private static final JoinClause LOCK_JOIN_CLAUSE = new JoinClause("priority_subprojects", RegisterAttribute.REQUEST_ID);

  private static final Query INSERT_PRIORITY_SUBPROJECT = InsertBuilder.into("priority_subprojects")
      .insert(RegisterAttribute.REQUEST_ID, RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID, QueryAttribute.LABEL).getQuery();

  private static final String UPDATE_PRIORITY_SUBPROJECT_STATUS =
      "SELECT ae_change_priority_subproject_state(?, ?::request_status_type, ?) ";

  private static final String DELETE_PRIORITY_SUBPROJECT =
      "SELECT ae_delete_priority_subproject(?) ";

  private static final String ACTION_UPDATESTATE = "updating priority subproject state";
  private static final String ACTION_INSERT = "inserting new priority subproject";
  private static final String ACTION_DELETE = "deleting priority subproject";

  private PrioritySubProjectModifyRepository() {
    // Not allowed to instantiate.
  }

  /**
   * Insert a priority subproject into the database, using the given {@link PrioritySubProject} object.
   * Includes request file, situations, audit trail, and checking that it does not exist yet.
   *
   * @param con The connection to use.
   * @param priorityProjectKey The parent priority project to insert the subproject into.
   * @param subProject The priority subproject to insert.
   * @param insertedBy Which user wants to insert this subproject.
   * @param insertRequestFile The PDF or GML file.
   * @return The new {@link PrioritySubProject} object from the database (id will be set now)
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the priority subproject already exists (by reference).
   */
  public static PrioritySubProject insert(final Connection con, final PriorityProjectKey priorityProjectKey, final PrioritySubProject subProject,
      final UserProfile insertedBy, final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
    ensureNew(con, priorityProjectKey, subProject);

    final int requestId = insertRequestRecords(con, subProject, insertedBy, insertRequestFile);
    insertPrioritySubProjectRecord(con, priorityProjectKey, subProject, requestId);

    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addNewRequestChanges(changes, subProject.getReference(), SegmentType.PRIORITY_SUBPROJECTS);
    RequestAuditRepository.saveAuditTrailItem(con, subProject, changes, insertedBy);

    return PrioritySubProjectRepository.getPrioritySubProjectByKey(con, new PrioritySubProjectKey(priorityProjectKey, subProject.getReference()));
  }

  /**
   * Insert a record in the requests* tables (and ensure the file is an application)
   */
  private static int insertRequestRecords(final Connection con, final PrioritySubProject subProject, final UserProfile insertedBy,
      final InsertRequestFile insertRequestFile) throws SQLException, AeriusException {
    insertRequestFile.setRequestFileType(RequestFileType.APPLICATION);
    final int requestId = RequestModifyRepository.insertNewRequest(con, SegmentType.PRIORITY_SUBPROJECTS, insertedBy.getAuthority(), subProject,
        insertRequestFile);
    subProject.setId(requestId);
    subProject.setRequestState(RequestState.INITIAL);
    RequestModifyRepository.insertSituations(con, subProject.getSituations(), requestId);
    return requestId;
  }

  /**
   * Insert a record in the priority_subprojects table.
   */
  private static void insertPrioritySubProjectRecord(final Connection con, final PriorityProjectKey priorityProjectKey,
      final PrioritySubProject subProject, final int requestId)
      throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectModifyRepository.find(con, priorityProjectKey, ACTION_INSERT);

    try (final PreparedStatement ps = con.prepareStatement(INSERT_PRIORITY_SUBPROJECT.get())) {
      INSERT_PRIORITY_SUBPROJECT.setParameter(ps, RegisterAttribute.REQUEST_ID, requestId);
      INSERT_PRIORITY_SUBPROJECT.setParameter(ps, RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID, priorityProject.getId());
      INSERT_PRIORITY_SUBPROJECT.setParameter(ps, QueryAttribute.LABEL, subProject.getLabel());
      ps.executeUpdate();
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        LOG.error("PRIORITY_SUBPROJECT_ALREADY_EXISTS: {}", subProject.getReference(), e);
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, subProject.getReference());
      } else {
        throw e;
      }
    }
  }

  /**
   * Update status of a priority subproject.
   *
   * @param con The connection to use.
   * @param key The key of the priority subproject to update.
   * @param state The state to update.
   * @param editedBy Which user is performing the update.
   * @param lastModified When the entry is last modified according to the client (so we can detect if another change is already made)
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the priority subproject does not exist (by id).
   */
  public static void updateState(final Connection con, final PrioritySubProjectKey key, final RequestState state,
      final UserProfile editedBy, final long lastModified) throws AeriusException {
    final PrioritySubProject subProject = find(con, key, ACTION_UPDATESTATE);
    try {
      final LockRequestTransaction lock = new LockRequestTransaction(con, subProject.getId(), lastModified, LOCK_JOIN_CLAUSE);
      try {
        final ArrayList<AuditTrailChange> changes = new ArrayList<>();
        RequestAuditRepository.addChange(changes, AuditTrailType.STATE, subProject.getRequestState().getDbValue(), state.getDbValue());

        // REJECTED_FINAL will be skipped because it is a nonexisting state in the database. However,
        // all the audit logging should commence as normal.
        if (state != RequestState.REJECTED_FINAL) {
          try (final PreparedStatement updatePS = con.prepareStatement(UPDATE_PRIORITY_SUBPROJECT_STATUS)) {
            QueryUtil.setValues(updatePS,
                subProject.getId(),
                state.getDbValue(),
                !editedBy.hasPermission(RegisterPermission.OVERRIDE_PRIORITY_PROJECTS_RESERVATION));
            updatePS.execute();
          }
        }

        RequestAuditRepository.saveAuditTrailItem(con, subProject, changes, editedBy);
      } catch (final SQLException se) {
        lock.rollback();
        throw DatabaseErrorCode.createAeriusException(se, LOG, "Error updating priority subproject state: " + key, key.getReference());
      } finally {
        lock.commit();
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Transaction error during updating priority project state: " + key, key.getReference());
    }
  }

  /**
   * Delete a priority subproject.
   */
  public static void delete(final Connection con, final PrioritySubProjectKey key, final UserProfile deletedBy)
      throws AeriusException {
    final PrioritySubProject subProject = find(con, key, ACTION_DELETE);
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.DELETE, subProject.getRequestState(), null);
    try {
      try (final PreparedStatement ps = con.prepareStatement(DELETE_PRIORITY_SUBPROJECT)) {
        QueryUtil.setValues(ps, subProject.getId());
        ps.executeQuery();
      }
      RequestAuditRepository.saveAuditTrailItem(con, subProject, changes, deletedBy);
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error deleting priority subproject: " + key, key.getReference());
    }
  }

  /**
   * Fetches a priority subproject via its key, and throws/logs errors when it does not exist.
   */
  static PrioritySubProject find(final Connection con, final PrioritySubProjectKey key, final String action)
      throws AeriusException {
    try {
      final PrioritySubProject subProject = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(con, key);
      if (subProject == null) {
        LOG.error("Unknown priority subproject key while {}: {}", action, key);
        throw new AeriusException(Reason.PRIORITY_SUBPROJECT_UNKNOWN, key.getReference());
      }
      return subProject;
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error finding existing priority subproject: " + key, key.getReference());
    }
  }

  /**
   * Ensures that a priority subproject does not exist yet via its reference, and throws an exception if it does.
   */
  static void ensureNew(final Connection con, final PriorityProjectKey priorityProjectKey, final PrioritySubProject subProject) throws AeriusException {
    final PrioritySubProjectKey key = new PrioritySubProjectKey(priorityProjectKey, subProject.getReference());
    try {
      if (PrioritySubProjectRepository.prioritySubProjectExists(con, key)) {
        LOG.error("Priority subproject already exists: {}", key);
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, key.getReference());
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error checking existance of priority subproject: " + key, key.getReference());
    }
  }

}
