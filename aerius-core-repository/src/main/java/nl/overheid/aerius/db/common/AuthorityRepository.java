/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.BooleanWhereClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.AuthoritySetting;

/**
 *
 */
public final class AuthorityRepository {

  private enum RepositoryAttribute implements Attribute {

    EMAIL_ADDRESS,
    CODE,
    REGISTER_AUTHORITY,

    SETTING_KEY,
    SETTING_VALUE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Attributes AUTHORITY_ATTRIBUTES = new Attributes(
      QueryAttribute.AUTHORITY_ID, RepositoryAttribute.CODE, QueryAttribute.NAME, RepositoryAttribute.EMAIL_ADDRESS);

  private static final String AUTHORITIES = "register_authorities_view";

  private static final Query GET_AUTHORITIES = QueryBuilder.from(AUTHORITIES)
      .where(new BooleanWhereClause(RepositoryAttribute.REGISTER_AUTHORITY, true))
      .select(AUTHORITY_ATTRIBUTES).getQuery();

  private static final Query GET_AUTHORITY = QueryBuilder.from(AUTHORITIES)
      .select(AUTHORITY_ATTRIBUTES)
      .where(QueryAttribute.AUTHORITY_ID).getQuery();

  private static final Query GET_AUTHORITY_SETTINGS = QueryBuilder.from("authority_settings")
      .select(RepositoryAttribute.SETTING_KEY, RepositoryAttribute.SETTING_VALUE)
      .where(QueryAttribute.AUTHORITY_ID).getQuery();

  private AuthorityRepository() {
  }

  /**
   * @param con The connection to use.
   * @return The list of authorities in the database.
   * @throws SQLException In case of database exceptions.
   */
  public static ArrayList<Authority> getAuthorities(final Connection con) throws SQLException {
    final AuthorityCollector collector = new AuthorityCollector();
    try (final PreparedStatement stmt = con.prepareStatement(GET_AUTHORITIES.get())) {

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        collector.collectEntity(rs);
      }
    }
    return collector.getEntities();
  }

  /**
   * @param con The connection to use.
   * @param authorityId The ID of the authority to retrieve.
   * @return The authority, including settings.
   * @throws SQLException In case of database exceptions.
   */
  public static Authority getAuthority(final Connection con, final int authorityId) throws SQLException {
    return getAuthority(con, authorityId, true);
  }

  /**
   * @param con The connection to use.
   * @param authorityId The ID of the authority to retrieve.
   * @return The authority, excluding settings.
   * @throws SQLException In case of database exceptions.
   */
  public static Authority getSkinnedAuthority(final Connection con, final int authorityId) throws SQLException {
    return getAuthority(con, authorityId, false);
  }

  private static Authority getAuthority(final Connection con, final int authorityId, final boolean includeSettings) throws SQLException {
    Authority authority = null;
    try (final PreparedStatement stmt = con.prepareStatement(GET_AUTHORITY.get())) {
      GET_AUTHORITY.setParameter(stmt, QueryAttribute.AUTHORITY_ID, authorityId);

      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        authority = getAuthority(rs);
        if (includeSettings) {
          setAuthoritySettings(con, authority);
        }
      }
    }
    return authority;
  }

  /**
   * @param con The connection to use.
   * @param authority The authority to set the settings for.
   * @throws SQLException In case of a database exception.
   */
  private static void setAuthoritySettings(final Connection con, final Authority authority) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(GET_AUTHORITY_SETTINGS.get())) {
      GET_AUTHORITY.setParameter(stmt, QueryAttribute.AUTHORITY_ID, authority.getId());

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final AuthoritySetting setting = QueryUtil.getEnum(rs, RepositoryAttribute.SETTING_KEY, AuthoritySetting.class);
        authority.getSettings().put(setting, QueryUtil.getString(rs, RepositoryAttribute.SETTING_VALUE));
      }
    }
  }

  private static Authority getAuthority(final ResultSet rs) throws SQLException {
    final Authority authority = new Authority();
    authority.setId(QueryAttribute.AUTHORITY_ID.getInt(rs));
    authority.setCode(QueryUtil.getString(rs, RepositoryAttribute.CODE));
    authority.setDescription(QueryAttribute.NAME.getString(rs));
    authority.setEmailAddress(QueryUtil.getString(rs, RepositoryAttribute.EMAIL_ADDRESS));
    return authority;
  }

  private static class AuthorityCollector extends EntityCollector<Authority> {

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.AUTHORITY_ID.getInt(rs);
    }

    @Override
    public Authority fillEntity(final ResultSet rs) throws SQLException {
      return getAuthority(rs);
    }

  }

}
