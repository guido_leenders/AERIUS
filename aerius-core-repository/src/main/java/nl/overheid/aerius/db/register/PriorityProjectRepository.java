/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.common.EmissionResultRepositoryUtil;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.ReceptorInfoRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.BooleanWhereClause;
import nl.overheid.aerius.db.util.ILikeWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.OrderByUtil;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SearchWhereClause;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.db.util.SpecialAttribute;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.IndicativeReceptorInfo;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectFilter;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PriorityProjectProgress;
import nl.overheid.aerius.shared.domain.register.PriorityProjectSortableAttribute;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Functionality for fetching priority project information from the database.
 */
public final class PriorityProjectRepository {

  private enum RepositoryAttribute implements Attribute {
    ASSIGNED_FRACTION_FROM, ASSIGNED_FRACTION_TILL,

    FRACTION_SPACE_ASSIGNED,

    ASSIGNED
    ;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final int UNKNOWN_PRIORITY_PROJECT_ID = -1;

  private static final Query GET_PRIORITY_PROJECT_KEY_BY_REFERENCE = getPriorityProjectBase()
      .select(RegisterAttribute.DOSSIER_ID, QueryAttribute.CODE).join(new JoinClause("authorities", QueryAttribute.AUTHORITY_ID))
      .where(QueryAttribute.REFERENCE).getQuery();

  private static final Query GET_PRIORITY_PROJECT_ID_BY_KEY = getPriorityProjectBase()
      .join(new JoinClause("authorities", QueryAttribute.AUTHORITY_ID)).where(RegisterAttribute.DOSSIER_ID, QueryAttribute.CODE).getQuery();

  private static final Query GET_PRIORITY_PROJECT = getPriorityProjectFullInfo().where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query GET_PRIORITY_SUBPROJECTS = QueryBuilder.from("priority_subprojects_view")
      .select(RegisterAttribute.REQUEST_ID, RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID).where(RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID)
      .getQuery();

  private static final Query GET_INDICATIVE_RECEPTOR = QueryBuilder.from("priority_project_max_space_assigned_receptor_view")
      .select(QueryAttribute.RECEPTOR_ID, RepositoryAttribute.FRACTION_SPACE_ASSIGNED).where(RegisterAttribute.REQUEST_ID).getQuery();

  private static final Query SEARCH_PRIORITY_PROJECTS = getPriorityProjectFullInfo().where(
      new SearchWhereClause(RegisterAttribute.REFERENCE, RegisterAttribute.DOSSIER_ID, RegisterAttribute.PROJECT_NAME, RegisterAttribute.CORPORATION))
      .getQuery();

  private static final QueryBuilder GET_DEMAND_RESULTS_FOR_EXPORT = QueryBuilder.from("priority_project_demands_gml_export_view")
      .select(QueryAttribute.RECEPTOR_ID).select(QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT_TYPE)
      .where(RegisterAttribute.REQUEST_ID);

  // Not allowed to instantiate.
  private PriorityProjectRepository() {}

  /**
   * Get the PriorityProjectKey for a specific priority project's reference.
   *
   * @param con The connection to use.
   * @param reference The reference to check
   * @return PriorityProjectKey for the priority project if one exists with the reference. Null if it doesn't exist.
   * @throws SQLException In case of a database error.
   */
  public static PriorityProjectKey getPriorityProjectKey(final Connection con, final String reference) throws SQLException {
    PriorityProjectKey key = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_PROJECT_KEY_BY_REFERENCE.get())) {
      GET_PRIORITY_PROJECT_KEY_BY_REFERENCE.setParameter(selectPS, QueryAttribute.REFERENCE, reference);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          key = new PriorityProjectKey(QueryUtil.getString(rs, RegisterAttribute.DOSSIER_ID), QueryAttribute.CODE.getString(rs));
        }
      }
    }
    return key;
  }

  /**
   * @param con The connection to use.
   * @param key The key of the priority project to check
   * @return True if the priority project exists, false if not.
   * @throws SQLException In case of a database error.
   */
  public static boolean priorityProjectExists(final Connection con, final DossierAuthorityKey key) throws SQLException {
    return getPriorityProjectIdByKey(con, key) != UNKNOWN_PRIORITY_PROJECT_ID;
  }

  /**
   * Get a skinned priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param key Key of the priority project to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProjectByKey(final Connection con, final DossierAuthorityKey key) throws SQLException {
    return getSkinnedPriorityProjectByKey(con, key, (DBMessagesKey) null);
  }

  /**
   * Get a skinned priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param key Key of the priority project to retrieve.
   * @param Locale Locale to fetch this project in
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProjectByKey(final Connection con, final DossierAuthorityKey key, final Locale locale)
      throws SQLException {
    return getSkinnedPriorityProjectByKey(con, key, new DBMessagesKey(ProductType.REGISTER, locale));
  }

  /**
   * Get a skinned priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param key Key of the priority project to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProjectByKey(final Connection con, final DossierAuthorityKey authKey, final DBMessagesKey key)
      throws SQLException {
    return getSkinnedPriorityProject(con, getPriorityProjectIdByKey(con, authKey), key);
  }

  /**
   * Get a skinned priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param reference Reference of the priority project to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProjectByReference(final Connection con, final String reference) throws SQLException {
    return getSkinnedPriorityProjectByReference(con, reference, null);
  }

  /**
   * Get a skinned priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param reference Reference of the priority project to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProjectByReference(final Connection con, final String reference, final DBMessagesKey key)
      throws SQLException {
    return getSkinnedPriorityProjectByKey(con, getPriorityProjectKey(con, reference), key);
  }

  /**
   * Get a priority project by its key.
   *
   * @param con Connection to use.
   * @param authKey Key of the priority project to retrieve.
   * @return A priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getPriorityProjectByKey(final Connection con, final PriorityProjectKey key) throws SQLException {
    return getPriorityProjectByKey(con, key, (DBMessagesKey) null);
  }

  /**
   * Get a priority project by its key.
   *
   * @param con Connection to use.
   * @param authKey Key of the priority project to retrieve.
   * @return A priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getPriorityProjectByKey(final Connection con, final DossierAuthorityKey authKey, final Locale locale)
      throws SQLException {
    return getPriorityProjectByKey(con, authKey, new DBMessagesKey(ProductType.REGISTER, locale));
  }

  /**
   * Get a priority project by its key.
   *
   * @param con Connection to use.
   * @param authKey Key of the priority project to retrieve.
   * @return A priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getPriorityProjectByKey(final Connection con, final DossierAuthorityKey authKey, final DBMessagesKey key)
      throws SQLException {
    return getPriorityProject(con, getPriorityProjectIdByKey(con, authKey), key);
  }

  /**
   * Get a priority project by its reference.
   *
   * @param con Connection to use.
   * @param reference reference to fetch
   * @return A priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getPriorityProjectByReference(final Connection con, final String reference, final DBMessagesKey key)
      throws SQLException {
    return getPriorityProjectByKey(con, getPriorityProjectKey(con, reference), key);
  }

  /**
   * Get an priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param priorityProjectId Priority project id to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProject(final Connection con, final int priorityProjectId) throws SQLException {
    return getPriorityProject(con, priorityProjectId, null, false);
  }

  /**
   * Get an priority project without detailed information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param priorityProjectId Priority project id to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  public static PriorityProject getSkinnedPriorityProject(final Connection con, final int priorityProjectId, final DBMessagesKey key)
      throws SQLException {
    return getPriorityProject(con, priorityProjectId, key, false);
  }

  /**
   * Get a priority project with all information such as situations, audit trail or validation rules.
   *
   * @param con Connection to use.
   * @param priorityProjectId Priority project id to retrieve.
   * @return A skinned priority project
   * @throws SQLException In case of a database error.
   */
  protected static PriorityProject getPriorityProject(final Connection con, final int priorityProjectId, final DBMessagesKey key)
      throws SQLException {
    return getPriorityProject(con, priorityProjectId, key, true);
  }

  /**
   * @param con The connection to use.
   * @param priorityProjectId DB Id of the priority project to retrieve.
   * @param fetchAdditionalInfo Whether to also fetch emission values, development rules, result information and change history.
   * @return The proper priority project (or null if not found).
   * @throws SQLException In case of a database error.
   */
  private static PriorityProject getPriorityProject(final Connection con, final int priorityProjectId, final DBMessagesKey key,
      final boolean fetchAdditionalInfo) throws SQLException {
    PriorityProject priorityProject = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_PROJECT.get())) {
      GET_PRIORITY_PROJECT.setParameter(selectPS, RegisterAttribute.REQUEST_ID, priorityProjectId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          priorityProject = getPriorityProjectFromResultSet(con, rs);
        }
      }
    }

    if (priorityProject != null && fetchAdditionalInfo) {
      priorityProject.setSituations(RequestRepository.getSituations(con, priorityProjectId));
      priorityProject.setChangeHistory(RequestAuditRepository.getAuditTrail(con, priorityProjectId));
      priorityProject.setSubProjects(getSubProjects(con, priorityProject));
      priorityProject.setIndicativeReceptorInfo(getIndicativeReceptorInfo(con, priorityProjectId, key));
    }

    return priorityProject;
  }

  /**
   * @param con The connection to use.
   * @param key The key of the priority project to retrieve the id for.
   * @return The proper priority project id or -1 if not found
   * @throws SQLException In case of a database error.
   */
  private static int getPriorityProjectIdByKey(final Connection con, final DossierAuthorityKey key) throws SQLException {
    final int priorityProjectId;

    if (key == null) {
      priorityProjectId = UNKNOWN_PRIORITY_PROJECT_ID;
    } else {
      try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_PROJECT_ID_BY_KEY.get())) {
        GET_PRIORITY_PROJECT_ID_BY_KEY.setParameter(selectPS, RegisterAttribute.DOSSIER_ID, key.getDossierId());
        GET_PRIORITY_PROJECT_ID_BY_KEY.setParameter(selectPS, QueryAttribute.CODE, key.getAuthorityCode());

        try (final ResultSet rs = selectPS.executeQuery()) {
          if (rs.next()) {
            priorityProjectId = QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
          } else {
            priorityProjectId = UNKNOWN_PRIORITY_PROJECT_ID;
          }
        }
      }
    }

    return priorityProjectId;
  }

  /**
   * @param con The connection to use.
   * @param maxResults The maximum results to return.
   * @param offset The offset of the results (to be used for paging).
   * @param filter The filter to use when retrieving results.
   * @return A list of priority projects.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<PriorityProject> getPriorityProjects(final Connection con, final int maxResults, final int offset,
      final PriorityProjectFilter filter) throws SQLException {
    final EntityCollector<PriorityProject> collector = new EntityCollector<PriorityProject>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
      }

      @Override
      public PriorityProject fillEntity(final ResultSet rs) throws SQLException {
        return getPriorityProjectFromResultSet(con, rs);
      }
    };
    final Query query = getPriorityProjectsQuery(filter);
    try (final PreparedStatement selectPS = con.prepareStatement(query.get())) {
      if (filter.getProgressState() != null && filter.getProgressState().hasLowerLimit()) {
        query.setParameter(selectPS, RepositoryAttribute.ASSIGNED_FRACTION_FROM, filter.getProgressState().getLowerLimit());
      }
      if (filter.getProgressState() != null && filter.getProgressState().hasHigherLimit()) {
        query.setParameter(selectPS, RepositoryAttribute.ASSIGNED_FRACTION_TILL, filter.getProgressState().getHigherLimit());
      }
      RequestFilterUtil.setRequestFilterParams(con, query, selectPS, filter);

      query.setParameter(selectPS, SpecialAttribute.LIMIT, maxResults);
      query.setParameter(selectPS, SpecialAttribute.OFFSET, offset);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          collector.collectEntity(rs);
        }
      }
    }

    return collector.getEntities();
  }

  /**
   * @param con The connection to use.
   * @param searchString The string to search for.
   * @return Any priority projects that match the string.
   * @throws SQLException In case of exception
   */
  public static ArrayList<PriorityProject> search(final Connection con, final String searchString) throws SQLException {
    final ArrayList<PriorityProject> priorityProjects = new ArrayList<>();
    final String sqlSearchString = ILikeWhereClause.getSearchString(searchString);
    try (final PreparedStatement selectPS = con.prepareStatement(SEARCH_PRIORITY_PROJECTS.get())) {
      SEARCH_PRIORITY_PROJECTS.setParameter(selectPS, RegisterAttribute.REFERENCE, sqlSearchString);
      SEARCH_PRIORITY_PROJECTS.setParameter(selectPS, RegisterAttribute.DOSSIER_ID, sqlSearchString);
      SEARCH_PRIORITY_PROJECTS.setParameter(selectPS, RegisterAttribute.PROJECT_NAME, sqlSearchString);
      SEARCH_PRIORITY_PROJECTS.setParameter(selectPS, RegisterAttribute.CORPORATION, sqlSearchString);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        priorityProjects.add(getPriorityProjectFromResultSet(con, rs));
      }
    }

    return priorityProjects;
  }

  private static PriorityProject getPriorityProjectFromResultSet(final Connection con, final ResultSet rs) throws SQLException {
    final PriorityProject priorityProject = new PriorityProject();
    priorityProject.setId(QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID));
    priorityProject.setRequestState(QueryUtil.getEnum(rs, RegisterAttribute.STATUS, RequestState.class));
    priorityProject.setFractionAssigned(QueryUtil.getDouble(rs, RegisterAttribute.FRACTION_ASSIGNED));
    priorityProject.setAssignCompleted(QueryUtil.getBoolean(rs, RegisterAttribute.ASSIGN_COMPLETED));

    RequestRepository.fillRequestFromResultSet(con, rs, priorityProject);

    final DossierMetaData dossierMetaData = new DossierMetaData();
    RequestRepository.fillDossierMetaDataFromResultSet(con, rs, dossierMetaData);
    priorityProject.setDossierMetaData(dossierMetaData);

    return priorityProject;
  }

  private static ArrayList<PrioritySubProject> getSubProjects(final Connection con, final PriorityProject priorityProject) throws SQLException {
    final ArrayList<PrioritySubProject> prioritySubProjects = new ArrayList<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_PRIORITY_SUBPROJECTS.get())) {
      GET_PRIORITY_SUBPROJECTS.setParameter(selectPS, RegisterAttribute.PRIORITY_PROJECT_REQUEST_ID, priorityProject.getId());
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final int prioritySubProjectId = QueryUtil.getInt(rs, RegisterAttribute.REQUEST_ID);
          final PrioritySubProject prioritySubProject = PrioritySubProjectRepository.getSkinnedPrioritySubProject(con, prioritySubProjectId);
          prioritySubProjects.add(prioritySubProject);
        }
      }
    }
    return prioritySubProjects;
  }

  private static IndicativeReceptorInfo getIndicativeReceptorInfo(final Connection con, final int priorityProjectId, final DBMessagesKey key)
      throws SQLException {
    IndicativeReceptorInfo receptorInfo = null;
    final ReceptorUtil ru = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    try (final PreparedStatement selectPS = con.prepareStatement(GET_INDICATIVE_RECEPTOR.get())) {
      GET_INDICATIVE_RECEPTOR.setParameter(selectPS, RegisterAttribute.REQUEST_ID, priorityProjectId);
      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          receptorInfo = new IndicativeReceptorInfo();
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          receptorInfo.setReceptor(ru.setAeriusPointFromId(new AeriusPoint(receptorId)));
          receptorInfo.setMaxFractionUsed(QueryUtil.getFloat(rs, RepositoryAttribute.FRACTION_SPACE_ASSIGNED));
          receptorInfo.setAssessmentAreas(ReceptorInfoRepository.getPasAreaAndHabitatInfo(con, receptorId, key));
        }
      }
    }
    return receptorInfo;
  }

  private static Query getPriorityProjectsQuery(final PriorityProjectFilter filter) {
    final QueryBuilder builder = getPriorityProjectFullInfo();
    if (filter.getProgressState() != null) {
      if (filter.getProgressState().hasLowerLimit()) {
        builder.where(new StaticWhereClause(RegisterAttribute.FRACTION_ASSIGNED.attribute() + " > ?", RepositoryAttribute.ASSIGNED_FRACTION_FROM));
      }
      if (filter.getProgressState().hasHigherLimit()) {
        builder.where(new StaticWhereClause(RegisterAttribute.FRACTION_ASSIGNED.attribute() + " <= ?", RepositoryAttribute.ASSIGNED_FRACTION_TILL));
      }
      if (filter.getProgressState() == PriorityProjectProgress.NO_RESERVATION_PRESENT) {
        builder.where(
            new StaticWhereClause(RegisterAttribute.STATUS.attribute() + " = '" + RequestState.INITIAL.getDbValue() + "'"));
      } else {
        builder.where(
            new StaticWhereClause(RegisterAttribute.STATUS.attribute() + " != '" + RequestState.INITIAL.getDbValue() + "'"));
      }
      builder.where(new BooleanWhereClause(RegisterAttribute.ASSIGN_COMPLETED, filter.getProgressState() == PriorityProjectProgress.DONE));
    }
    RequestFilterUtil.addRequestFilter(builder, filter, RegisterAttribute.RECEIVED_DATE);
    RegisterOrderByUtil.setOrderByParams(builder, filter.getSortAttribute(), filter.getSortDirection());

    if (filter.getSortAttribute() == PriorityProjectSortableAttribute.PROGRESS) {
      builder.orderBy(new OrderByClause(RegisterAttribute.ASSIGN_COMPLETED, OrderByUtil.getDirection(filter.getSortDirection())));
    }

    builder
        .orderBy(new OrderByClause(RegisterAttribute.RECEIVED_DATE, OrderType.ASC), new OrderByClause(RegisterAttribute.REQUEST_ID, OrderType.DESC))
        .limit().offset();
    return builder.getQuery();
  }

  private static QueryBuilder getPriorityProjectBase() {
    return QueryBuilder.from("priority_projects_view").select(RegisterAttribute.REQUEST_ID);
  }

  private static QueryBuilder getPriorityProjectFullInfo() {
    final QueryBuilder builder = getPriorityProjectBase().join(new JoinClause("authorities", QueryAttribute.AUTHORITY_ID));
    RequestRepository.addRequiredAttributes(builder);
    return builder.select(RegisterAttribute.DOSSIER_ID, QueryAttribute.CODE, RegisterAttribute.RECEIVED_DATE, RegisterAttribute.STATUS,
        RegisterAttribute.SEGMENT, RegisterAttribute.REMARKS, RegisterAttribute.FRACTION_ASSIGNED, RegisterAttribute.ASSIGN_COMPLETED);
  }

  public static List<AeriusResultPoint> getDemandAssignedResultsForExport(final Connection con, final int requestId) throws SQLException {
    final QueryBuilder query = GET_DEMAND_RESULTS_FOR_EXPORT;
    query.select(new SelectClause(RepositoryAttribute.ASSIGNED, "result"));
    return getResultsForExport(con, requestId, query.getQuery());
  }

  private static void addResultsToReceptorMap(final ResultSet rs, final ReceptorUtil ru, final HashMap<Integer, AeriusResultPoint> receptorMap)
      throws SQLException {
    final AeriusResultPoint point = new AeriusResultPoint(QueryAttribute.RECEPTOR_ID.getInt(rs));
    ru.setAeriusPointFromId(point);
    EmissionResultRepositoryUtil.addResultsToPoint(rs, receptorMap, point);
  }

  /**
   * @param con The connection to use.
   * @param requestId The ID of the request, will be set based on the query.
   * @param query The query to get results for export. Should contain all required attributes and a where clause on request_id.
   * @param assigned
   * @return The proper results.
   * @throws SQLException In case of database errors.
   */
  static List<AeriusResultPoint> getResultsForExport(final Connection con, final int requestId, final Query query)
      throws SQLException {
    final ArrayList<AeriusResultPoint> receptors = new ArrayList<>();
    final ReceptorUtil ru = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    try (final PreparedStatement ps = con.prepareStatement(query.get())) {
      query.setParameter(ps, RegisterAttribute.REQUEST_ID, requestId);

      final HashMap<Integer, AeriusResultPoint> receptorMap = new HashMap<>();
      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          addResultsToReceptorMap(rs, ru, receptorMap);
        }
      }
      receptors.addAll(receptorMap.values());
    }

    return receptors;
  }

}
