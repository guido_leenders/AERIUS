/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.util.QueryUtil;

/**
 * DB Class to obtain localized messages from the database.
 */
public final class SystemInfoMessageRepository {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(SystemInfoMessageRepository.class);

  // The SQL queries used in this service.
  private static final String GET_MESSAGE_QUERY =
      "SELECT message, language_code FROM i18n.system_info_messages";

  private static final String DELETE_MESSAGE_QUERY =
      "DELETE FROM i18n.system_info_messages WHERE language_code = ?::i18n.language_code_type";

  private static final String DELETE_ALL_MESSAGE_QUERY =
      "DELETE FROM i18n.system_info_messages";

  private static final String INSERT_MESSAGE_QUERY =
      "INSERT INTO i18n.system_info_messages (message,language_code) VALUES (?,?::i18n.language_code_type)";

  // Not allowed to instantiate.
  private SystemInfoMessageRepository() {
  }

  /**
   * Delete a constant value from the database. The connection is NOT closed after usage.
   * @param connection The connection to used (won't be closed).
   * @param language The language string to delete the value for.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static void deleteMessage(final Connection connection, final String language) throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(DELETE_MESSAGE_QUERY)) {
      QueryUtil.setValues(stmt, language);
      stmt.executeUpdate();
    } catch (final SQLException e) {
      LOG.error("Error deleting system info message {})", language, e);
      throw e;
    }
  }

  /**
   * Delete all constant values from the database. The connection is NOT closed after usage.
   * @param connection The connection to used (won't be closed).
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static void deleteAllMessage(final Connection connection) throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(DELETE_ALL_MESSAGE_QUERY)) {
      stmt.executeUpdate();
    } catch (final SQLException e) {
      LOG.error("Error deleting all system info messages ", e);
      throw e;
    }
  }

  /**
   * Insert a constant value from the database. The connection is NOT closed after usage.
   * @param connection The connection to used (won't be closed).
   * @param message The string to set.
   * @param language The language string to delete the value for.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static void insertMessage(final Connection connection, final String message,
      final String language) throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(INSERT_MESSAGE_QUERY)) {
      QueryUtil.setValues(stmt, message, language);
      stmt.executeUpdate();

    } catch (final SQLException e) {
      LOG.error("Error inserting system info message {})", language, e);
      throw e;
    }
  }

  /**
   * Update a constant value from the database. The connection is NOT closed after usage.
   * if the clearMessage string is set we clear all the messages in the database/
   * @param connection The connection to used (won't be closed).
   * @param message The string to set.
   * @param clearMessage The string that determines if we have to clear all messages
   * @param language The language string to delete the value for.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static void updateMessage(final Connection connection, final String message, final Locale locale)
      throws SQLException {
      deleteMessage(connection, locale.getLanguage());
      insertMessage(connection, message, locale.getLanguage());
  }

  /**
   * Clear all message in the database. The connection is NOT closed after usage.
   * if the clearMessage string is set we clear all the messages in the database/
   * @param connection The connection to used (won't be closed).
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static void clearAllMessages(final Connection connection) throws SQLException {
      deleteAllMessage(connection);
  }

  /**
   * Get a constant value from the database. The connection is NOT closed after usage.
   * If we have only one result this will be returned as default message.
   * @param connection The connection to used (won't be closed).
   * @param language The language string to delete the value for.
   * @return The value of the constant empty when no record is found.
   * @throws SQLException When an error occurred communicating with the database.
   */
  public static String getMessage(final Connection connection, final String language) throws SQLException {
    String messageValue = "";
    String defaultMessage = null;
    try (final PreparedStatement stmt = connection.prepareStatement(GET_MESSAGE_QUERY)) {
      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        if (defaultMessage == null) {
          defaultMessage = rs.getString(1);
        }
        if (rs.getString(2).contains(language)) {
          messageValue = rs.getString(1);
        }
      }
      if (defaultMessage != null && "".equals(messageValue)) {
        messageValue = defaultMessage;
      }
    } catch (final SQLException e) {
      LOG.error("Error fetching system info message ({})", language, e);
    }
    return messageValue;
  }

  public static String getMessage(final Connection connection, final Locale locale) throws SQLException {
    return getMessage(connection, locale.getLanguage());
  }

}
