/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import nl.overheid.aerius.db.util.Attribute;

/**
 *
 */
enum RegisterAttribute implements Attribute {

  REQUEST_ID,

  HAS_MULTIPLE_SECTORS,
  MARKED,
  REFERENCE,
  DOSSIER_ID,
  RECEIVED_DATE,
  STATUS,
  CORPORATION,
  PROJECT_NAME,
  SEGMENT,
  START_YEAR,
  TEMPORARY_PERIOD,
  HANDLER_ID,
  REMARKS,
  LAST_MODIFIED,
  INSERT_DATE,
  APPLICATION_VERSION,
  DATABASE_VERSION,
  USER_ID,

  SITUATION,

  TOTAL_EMISSION,

  CONTENT,

  FINISHED,

  CHECKED,

  FILE_TYPE,

  FRACTION_ASSIGNED,
  ASSIGN_COMPLETED,

  PRIORITY_PROJECT_REQUEST_ID,
  PRIORITY_PROJECT_DOSSIER_ID,
  PRIORITY_PROJECT_AUTHORITY;

  @Override
  public String attribute() {
    return name().toLowerCase();
  }

}
