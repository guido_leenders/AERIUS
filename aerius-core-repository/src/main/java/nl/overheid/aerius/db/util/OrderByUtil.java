/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import java.util.HashMap;

import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.shared.domain.SortableAttribute;
import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.register.NoticeSortableAttribute;
import nl.overheid.aerius.shared.domain.register.PermitSortableAttribute;
import nl.overheid.aerius.shared.domain.register.PriorityProjectSortableAttribute;

/**
 * Static util sort of thing. It's extremely ugly but serves its rather unfortunate purpose quite well.
 *
 * Extending classes need to mirror setOrderByParams statically and call the one in here in order to make
 * the extending class initialize (if ATTRIBUTE_MAPPING needs additions).
 *
 * It's the poor guy's inheritance, really.
 */
public class OrderByUtil {
  protected static final HashMap<SortableAttribute, Attribute> ATTRIBUTE_MAPPING = new HashMap<>();

  static {
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.SECTOR, QueryAttribute.SECTOR_ID);
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.AUTHORITY, QueryAttribute.AUTHORITY_ID);

    ATTRIBUTE_MAPPING.put(NoticeSortableAttribute.SECTOR, QueryAttribute.SECTOR_ID);

    ATTRIBUTE_MAPPING.put(PriorityProjectSortableAttribute.SECTOR, QueryAttribute.SECTOR_ID);
  }

  public static void setOrderByParams(final QueryBuilder builder, final SortableAttribute sortAttribute, final SortableDirection sortDirection) {
    final Attribute attribute = ATTRIBUTE_MAPPING.get(sortAttribute);

    if (attribute != null) {
      if (sortDirection == null) {
        builder.orderBy(attribute);
      } else {
        builder.orderBy(new OrderByClause(attribute, getDirection(sortDirection)));
      }
    }
  }

  public static OrderType getDirection(final SortableDirection dir) {
    switch (dir) {
    case DESC:
      return OrderType.DESC;
    default:
    case ASC:
      return OrderType.ASC;
    }
  }
}
