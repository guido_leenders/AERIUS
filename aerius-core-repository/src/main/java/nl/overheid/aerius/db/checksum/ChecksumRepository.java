/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.checksum;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ChecksumRepository {

  private static final String CHECK_SUM = "checksum";

  /**
   * Get all non-catalog non-temporary tables in the database, filtered by an array.
   */
  private static final String QUERY_GET_ALL_OUR_TABLES =
      "SELECT schemaname || '.' || tablename AS cs_table "
          + "  FROM pg_tables "
          + "    INNER JOIN pg_namespace ON (nspname = schemaname) "
          + "  WHERE "
          + "    pg_namespace.oid <> pg_my_temp_schema() AND "
          + "    schemaname NOT IN ('information_schema', 'pg_catalog') AND "
          + "    NOT (schemaname || '.' || tablename)::regclass = ANY(?::regclass[]) ";

  /**
   * Generate a checksum for a table based on contents.
   */
  private static final String QUERY_TABLE_CONTENT_CHECKSUM = "SELECT SUM(hashtext((tbl.*)::text)) AS checksum FROM %s AS tbl";

  /**
   * Generate a checksum for a database based on its structure: tables, views, (aggregate) functions, triggers,
   * constraints and indexes.
   * ATTENTION: It's not ideal that this huge query is in here. But we can't put it into the database, because
   * a hacker with database access could then modify that checksum function.
   */
  private static final String QUERY_DATABASE_STRUCTURE_CHECKSUM =
      "SELECT SUM(hashtext(definition)) AS checksum FROM ( "
          + " SELECT relnamespace AS nspoid, nspname, (attname, typname, attlen, attnum, attnotnull, atthasdef)::text AS definition "
          + "   FROM pg_attribute "
          + "     INNER JOIN pg_type ON (atttypid = pg_type.oid) "
          + "     INNER JOIN pg_class ON (attrelid = pg_class.oid) "
          + "     INNER JOIN pg_namespace ON (relnamespace = pg_namespace.oid) "
          + "   WHERE relkind = 'r' AND relpersistence <> 't' AND attnum > 0 "
          + " UNION ALL "
          + " SELECT pg_namespace.oid AS nspoid, nspname, definition "
          + "   FROM pg_views "
          + "     INNER JOIN pg_namespace ON (schemaname = pg_namespace.nspname) "
          + " UNION ALL "
          + " SELECT pronamespace AS nspoid, nspname, pg_get_functiondef(pg_proc.oid) AS definition "
          + "   FROM pg_proc "
          + "       INNER JOIN pg_namespace ON (pronamespace = pg_namespace.oid) "
          + "   WHERE NOT proisagg "
          + " UNION ALL "
          + " SELECT pronamespace AS nspoid, nspname, (aggtransfn, aggfinalfn)::text AS definition "
          + "   FROM pg_aggregate "
          + "     INNER JOIN pg_proc ON (aggfnoid = pg_proc.oid) "
          + "     INNER JOIN pg_namespace ON (pronamespace = pg_namespace.oid) "
          + " UNION ALL "
          + " SELECT relnamespace AS nspoid, nspname, pg_get_triggerdef(pg_trigger.oid) AS definition "
          + "   FROM pg_trigger "
          + "     INNER JOIN pg_class ON (tgrelid = pg_class.oid) "
          + "     INNER JOIN pg_namespace ON (relnamespace = pg_namespace.oid) "
          + "   WHERE NOT tgisinternal "
          + " UNION ALL "
          + " SELECT connamespace AS nspoid, nspname, pg_get_constraintdef(pg_constraint.oid) AS definition "
          + "   FROM pg_constraint "
          + "     INNER JOIN pg_namespace ON (connamespace = pg_namespace.oid) "
          + " UNION ALL "
          + " SELECT pg_namespace.oid AS nspoid, nspname, indexdef AS definition "
          + "   FROM pg_indexes "
          + "     INNER JOIN pg_namespace ON (schemaname = pg_namespace.nspname) "
          + ") AS definition "
          + " WHERE nspoid <> pg_my_temp_schema() AND nspname NOT IN ('information_schema', 'pg_catalog') ";

  private ChecksumRepository() {
    //not allowed to instantiate.
  }

  /**
   * Generate checksum for the contents of all our tables in the database, minus the given excluded tables.
   * The checksum query is executed for each table, and the result is collected in a Map and returned.
   * @param con Connection to execute query on
   * @param excludeTableNames A list of tables to exclude; these are nonstatic tables
   * @return The checksum for each applicable table; the String key is as "schemaname.tablename"
   * @throws SQLException When an error occurs executing the query
   */
  public static Map<String, Long> getTablesContentChecksum(final Connection con, final List<String> excludeTableNames) throws SQLException {
    final List<String> tableNames = getApplicableTables(con, excludeTableNames);

    final Map<String, Long> checksums = new HashMap<>();
    for (final String tableName : tableNames) {
      checksums.put(tableName, getTableContentChecksum(con, tableName));
    }
    return checksums;
  }

  /**
   * Get all non-catalog non-temporary tables in the database, filtered by an array.
   * @param con Connection to execute query on
   * @param excludeTableNames A list of tables to exclude; these are nonstatic tables
   * @return List of tables without the filtered ones
   * @throws SQLException When an error occurs executing the query
   */
  static List<String> getApplicableTables(final Connection con, final List<String> excludeTableNames) throws SQLException {
    final List<String> tableNames = new ArrayList<>();
    try (final PreparedStatement statement = con.prepareStatement(QUERY_GET_ALL_OUR_TABLES)) {
      statement.setArray(1, con.createArrayOf("text", excludeTableNames.toArray()));
      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        tableNames.add(rs.getString("cs_table"));
      }
    }
    return tableNames;
  }

  /**
   * Generate checksum for the contents of the given table. Each record is stringified and hashed.
   * All record hashes for the table are then summed.
   * @param con Connection to execute query on
   * @param tableName Table to generate checksum for
   * @return Checksum value
   * @throws SQLException When an error occurs executing the query
   */
  static long getTableContentChecksum(final Connection con, final String tableName) throws SQLException {
    long checksum = 0;
    final String query = String.format(QUERY_TABLE_CONTENT_CHECKSUM, tableName);
    try (final PreparedStatement statement = con.prepareStatement(query)) {
      final ResultSet rs = statement.executeQuery();
      if (rs.next()) {
        checksum = rs.getLong(CHECK_SUM);
      }
    }
    return checksum;
  }

  /**
   * Generate checksum for the entire database structure. The query hashes the definitions of tables, views,
   * functions, aggregate functions, triggers, constraints and indexes.
   * @param con Connection to execute query on
   * @return Checksum value
   * @throws SQLException When an error occurs executing the query
   */
  public static long getDatabaseStructureChecksum(final Connection con) throws SQLException {
    long checksum = 0;
    try (final PreparedStatement statement = con.prepareStatement(QUERY_DATABASE_STRUCTURE_CHECKSUM)) {
      final ResultSet rs = statement.executeQuery();
      if (rs.next()) {
        checksum = rs.getLong(CHECK_SUM);
      }
    }
    return checksum;
  }

}
