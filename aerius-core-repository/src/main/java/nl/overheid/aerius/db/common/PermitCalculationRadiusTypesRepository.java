/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;

/**
 * Repository class to read {@link PermitCalculationRadiusType} from the database.
 */
public final class PermitCalculationRadiusTypesRepository {

  enum RepositoryAttribute implements Attribute {
    /**
     * Permit calculation radius type ID.
     */
    PERMIT_CALCULATION_RADIUS_TYPE_ID,
    /**
     *
     */
    RADIUS;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final String PERMIT_CALCULATION_RADIUS_TYPES = "permit_calculation_radius_types";
  private static final Attributes ATTRIBUTES =
      new Attributes(RepositoryAttribute.PERMIT_CALCULATION_RADIUS_TYPE_ID, QueryAttribute.CODE, RepositoryAttribute.RADIUS);
  private static final Query PERMIT_CALCULATION_RADIUS_TYPES_LIST = QueryBuilder.from(PERMIT_CALCULATION_RADIUS_TYPES).select(ATTRIBUTES).getQuery();

  private PermitCalculationRadiusTypesRepository() {
    // Repository class
  }

  public static ArrayList<PermitCalculationRadiusType> getTypes(final PMF pmf, final Locale locale) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      return getTypes(con, new DBMessagesKey(pmf.getProductType(), locale));
    }
  }

  /**
   * Returns {@link PermitCalculationRadiusType}s from the database.
   * @param con Database connection
   * @param messagesKey Locale and product type to get descriptions for sectors for
   * @return
   * @throws SQLException
   */
  public static ArrayList<PermitCalculationRadiusType> getTypes(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(PERMIT_CALCULATION_RADIUS_TYPES_LIST.get())) {
      final ResultSet rs = statement.executeQuery();
      final ArrayList<PermitCalculationRadiusType> lst = new ArrayList<>();

      while (rs.next()) {
        final PermitCalculationRadiusType permitCalculationRadiusType = new PermitCalculationRadiusType();
        permitCalculationRadiusType.setId(QueryUtil.getInt(rs, RepositoryAttribute.PERMIT_CALCULATION_RADIUS_TYPE_ID));
        permitCalculationRadiusType.setCode(QueryAttribute.CODE.getString(rs));
        permitCalculationRadiusType.setRadius(QueryUtil.getInt(rs, RepositoryAttribute.RADIUS));
        DBMessages.setPermitCalculationRadiusTypeMessages(permitCalculationRadiusType, messagesKey);
        lst.add(permitCalculationRadiusType);
      }
      return lst;
    }
  }
}
