/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;

/**
 * Service class for Source Characteristics database queries.
 */
public final class SourceCharacteristicsRepository {
  private static final String DIURNAL_VARIATIONS = "emission_diurnal_variations";
  private static final Attributes DIURNAL_VARIATION_ATTRIBUTES = new Attributes(QueryAttribute.EMISSION_DIURNAL_VARIATION_ID, QueryAttribute.CODE);
  private static final Query DIURNAL_VARIATION_LIST = QueryBuilder.from(DIURNAL_VARIATIONS).select(DIURNAL_VARIATION_ATTRIBUTES).getQuery();

  private SourceCharacteristicsRepository() {}

  public static ArrayList<DiurnalVariationSpecification> getDiurnalVariations(final Connection con, final DBMessagesKey messagesKey)
      throws SQLException {
    try (final PreparedStatement statement = con.prepareStatement(DIURNAL_VARIATION_LIST.get())) {
      final ResultSet rs = statement.executeQuery();

      final ArrayList<DiurnalVariationSpecification> lst = new ArrayList<>();

      while (rs.next()) {
        final DiurnalVariationSpecification diurnalVariationSpecification = new DiurnalVariationSpecification();
        diurnalVariationSpecification.setId(QueryAttribute.EMISSION_DIURNAL_VARIATION_ID.getInt(rs));
        diurnalVariationSpecification.setCode(QueryAttribute.CODE.getString(rs));
        DBMessages.setDiurnalVariationMessages(diurnalVariationSpecification, messagesKey);
        lst.add(diurnalVariationSpecification);
      }
      return lst;
    }
  }
}
