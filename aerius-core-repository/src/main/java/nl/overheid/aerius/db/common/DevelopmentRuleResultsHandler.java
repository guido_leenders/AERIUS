/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryHandler;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;

/**
 * Abstract class to get Development Rule Results from the database.
 * NOT Threadsafe.
 */
public abstract class DevelopmentRuleResultsHandler extends QueryHandler {

  private enum RepositoryAttribute implements Attribute {

    RULE,
    PASSED;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private final Comparator<DevelopmentRuleResult> developmentRuleCompator = new Comparator<DevelopmentRuleResult>() {

    @Override
    public int compare(final DevelopmentRuleResult dr1, final DevelopmentRuleResult dr2) {
      return Integer.compare(dr2.getRule().ordinal(), dr1.getRule().ordinal());
    }
  };

  private final TreeMap<AssessmentArea, HashMap<DevelopmentRule, DevelopmentRuleResult>> intermediateResult;
  private final AssessmentAreaCollector collector;

  public DevelopmentRuleResultsHandler(final Connection connection) {
    intermediateResult = new TreeMap<>();
    collector = new AssessmentAreaCollector(intermediateResult, connection);
  }

  public static QueryBuilder getQueryBuilder(final String fromFunction, final Attribute... functionAttributes) {
    return QueryBuilder
        .from(fromFunction, functionAttributes)
        .join(new JoinClause("receptors_to_assessment_areas", QueryAttribute.RECEPTOR_ID))
        .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.RECEPTOR_ID, RepositoryAttribute.RULE, RepositoryAttribute.PASSED);
  }

  public HashSet<DevelopmentRuleResultList> getResults() {
    final HashSet<DevelopmentRuleResultList> results = new LinkedHashSet<>();

    for (final Entry<AssessmentArea, HashMap<DevelopmentRule, DevelopmentRuleResult>> entry : intermediateResult.entrySet()) {
      final DevelopmentRuleResultList ruleResult = new DevelopmentRuleResultList();
      ruleResult.setAssessmentArea(entry.getKey());
      ruleResult.setDevelopmentRuleResults(new ArrayList<>(entry.getValue().values()));
      Collections.sort(ruleResult.getDevelopmentRuleResults(), developmentRuleCompator);

      results.add(ruleResult);
    }

    return results;
  }

  @Override
  protected void handleResult(final ResultSet rs) throws SQLException {
    final AssessmentArea area = collector.collectEntity(rs);
    final HashMap<DevelopmentRule, DevelopmentRuleResult> ruleResults = intermediateResult.get(area);
    final DevelopmentRule rule = QueryUtil.getEnum(rs, RepositoryAttribute.RULE, DevelopmentRule.class);
    final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
    final boolean passed = QueryUtil.getBoolean(rs, RepositoryAttribute.PASSED);

    if (!passed) {
      ruleResults.get(rule).add(receptorId);
    }

    if (rule == DevelopmentRule.EXCEEDING_SPACE_CHECK && passed) {
      // The NOT_EXCEEDING_SPACE_CHECK needs to be filled as opposite of EXCEEDING_SPACE_CHECK
      ruleResults.get(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK).add(receptorId);
    }
  }

  private static class AssessmentAreaCollector extends EntityCollector<AssessmentArea> {

    private final TreeMap<AssessmentArea, HashMap<DevelopmentRule, DevelopmentRuleResult>> result;
    private final Connection con;

    AssessmentAreaCollector(final TreeMap<AssessmentArea, HashMap<DevelopmentRule, DevelopmentRuleResult>> result, final Connection con) {
      this.result = result;
      this.con = con;
    }

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
    }

    @Override
    public AssessmentArea fillEntity(final ResultSet rs) throws SQLException {
      final AssessmentArea assessmentArea = GeneralRepository.getAssessmentArea(con, QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
      final HashMap<DevelopmentRule, DevelopmentRuleResult> results = new HashMap<DevelopmentRule, DevelopmentRuleResult>();
      for (final DevelopmentRule rule : DevelopmentRule.values()) {
        results.put(rule, new DevelopmentRuleResult(rule));
      }
      result.put(assessmentArea, results);
      return assessmentArea;
    }
  }

}
