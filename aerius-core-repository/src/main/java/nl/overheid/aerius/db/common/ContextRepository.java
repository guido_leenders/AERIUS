/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.OptionType;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.context.AERIUS2Context;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.context.MeldingContext;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Repository class to create and fill a context object. Collects all needed database-related from other repositories using a database connection.
 * (Could be seen as a service class as well, but that would make service an overloaded term).
 */
public final class ContextRepository {

  private ContextRepository() {
  }

  private static void addSetting(final Connection con, final Context c, final SharedConstantsEnum constant) throws SQLException {
    c.setSetting(constant, ConstantRepository.getString(con, constant));
  }

  private static void addOptionalSetting(final Connection con, final Context c, final SharedConstantsEnum constant) throws SQLException {
    c.setSetting(constant, ConstantRepository.getString(con, constant, false));
  }

  private static void addIntSetting(final Connection con, final Context c, final SharedConstantsEnum constant) throws SQLException {
    c.setSetting(constant, ConstantRepository.getNumber(con, constant, Integer.class));
  }

  private static void addDoubleSetting(final Connection con, final Context c, final SharedConstantsEnum constant) throws SQLException {
    c.setSetting(constant, ConstantRepository.getNumber(con, constant, Double.class));
  }

  private static void addBooleanSetting(final Connection con, final Context c, final SharedConstantsEnum constant) throws SQLException {
    c.setSetting(constant, ConstantRepository.getBoolean(con, constant));
  }

  /**
   * @param con the database connection
   * @param messagesKey the db message key
   * @param admin Whether to include admin specific information.
   * @return A filled register context object.
   * @throws SQLException When a database error occurs.
   * @throws AeriusException When another error occurs.
   */
  public static RegisterContext getRegisterContext(final Connection con, final DBMessagesKey messagesKey, final boolean admin)
      throws SQLException, AeriusException {
    final RegisterContext c = new RegisterContext();

    c.setReceptorGridSettings(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    addSetting(con, c, SharedConstantsEnum.CALCULATOR_FETCH_URL);
    addSetting(con, c, SharedConstantsEnum.WEBAPPNAME_CALCULATOR);
    addSetting(con, c, SharedConstantsEnum.HELP_URL_TEMPLATE);
    addSetting(con, c, SharedConstantsEnum.AERIUS_HOST);
    addSetting(con, c, SharedConstantsEnum.MANUAL_URL);
    addOptionalSetting(con, c, SharedConstantsEnum.LOGOUT_URL);
    addDoubleSetting(con, c, SharedConstantsEnum.PRONOUNCEMENT_SPACE_ALMOST_FULL_TRIGGER);

    fillContextWithBaseData(con, c, messagesKey);

    c.setNatura2kAreas(GeneralRepository.getNatura2kAreas(con));
    c.setProvinces(GeneralRepository.getProvinces(con));
    c.setAuthorities(AuthorityRepository.getAuthorities(con));
    c.setMaxRadiusForRequests(ConstantRepository.getInteger(con, ConstantsEnum.MAX_RADIUS_FOR_REQUESTS));

    if (admin) {
      c.setRoles(UserRepository.getRolesAndPermissions(con));
    }

    c.setMinimalDepositionSpaceOptions(OptionsRepository.getOptions(con, OptionType.FILTER_MIN_FRACTION_USED, Double.class));

    addLayer(con, c, WMSLayerType.WMS_PERMIT_DEMANDS_VIEW, ProductType.REGISTER, messagesKey);

    return c;
  }

  /**
   * The context for the Melding application.
   *
   * @param con the database connection
   * @return A filled context object.
   * @throws SQLException When a database error occurs.
   */
  public static MeldingContext getMeldingContext(final Connection con) throws SQLException {
    final MeldingContext meldingContext = new MeldingContext();

    addIntSetting(con, meldingContext, SharedConstantsEnum.SYSTEM_INFO_POLLING_TIME);
    return meldingContext;
  }

  /**
   * @param con the database connection
   * @param messagesKey the db message key
   * @return A filled context object.
   * @throws SQLException When a database error occurs.
   * @throws AeriusException When another error occurs.
   */
  public static CalculatorContext getCalculatorContext(final Connection con, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    final CalculatorContext c = new CalculatorContext();

    fillScenarioBaseContext(con, c, messagesKey);

    addBooleanSetting(con, c, SharedConstantsEnum.MELDING_ENABLED);
    addSetting(con, c, SharedConstantsEnum.MELDING_URL);
    addSetting(con, c, SharedConstantsEnum.PAS_PERMIT_EXPLANATION_URL);
    addSetting(con, c, SharedConstantsEnum.PAS_PP_LIST_URL);

    addDoubleSetting(con, c, SharedConstantsEnum.MARITIME_MOORING_INLAND_ROUTE_SHIPPING_NODE_SEARCH_DISTANCE);

    fillContextWithBaseData(con, c, messagesKey);

    return c;
  }

  public static ScenarioContext getScenarioContext(final Connection con, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    final ScenarioContext c = new ScenarioContext();

    fillScenarioBaseContext(con, c, messagesKey);
    fillContextWithBaseData(con, c, messagesKey);

    //looks a bit weird to have product type calculator, but scenario uses the calculator workspace in geoserver.
    addLayer(con, c, WMSLayerType.WMS_HABITATS_VIEW, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_RELEVANT_HABITAT_INFO_FOR_RECEPTOR_VIEW, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_PERCENTAGE, ProductType.CALCULATOR,
        messagesKey);
    addLayer(con, c, WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_LABEL, ProductType.CALCULATOR,
        messagesKey);
    addLayer(con, c, WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_PERCENTAGE_LABEL, ProductType.CALCULATOR,
        messagesKey);
    addLayer(con, c, WMSLayerType.WMS_USER_ROAD_TOTAL_VEHICLES_PER_DAY, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_USER_SOURCE_LABELS, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_USER_ROAD_SPEED_TYPES, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_USER_ROAD_TOTAL_VEHICLES_PER_DAY_LIGHT_TRAFFIC, ProductType.CALCULATOR, messagesKey);
    return c;
  }

  /**
   * @param con the database connection
   * @param messagesKey the db message key
   * @return A filled context object.
   * @throws SQLException When an error occurs.
   */
  public static AERIUS2Context getConnectContext(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final AERIUS2Context c = new AERIUS2Context();
    setGlobalDefaults(con, c);
    return c;
  }

  /**
   * @param con the database connection
   * @return A filled context object.
   * @throws SQLException When a database error occurs.
   * @throws AeriusException When another error occurs.
   */
  public static <C extends ScenarioBaseContext> C fillScenarioBaseContext(final Connection con, final C c, final DBMessagesKey messagesKey)
      throws SQLException, AeriusException {
    addIntSetting(con, c, SharedConstantsEnum.CALCULATOR_POLLING_TIME);

    setGlobalDefaults(con, c);

    addIntSetting(con, c, SharedConstantsEnum.CALCULATE_EMISSIONS_DEFAULT_RADIUS_DISTANCE_UI);

    addSetting(con, c, SharedConstantsEnum.OPS_FACTSHEET_URL);

    addIntSetting(con, c, SharedConstantsEnum.CALCULATOR_LIMITS_RECEPTORS_RECEPTOR_TILE);
    addSetting(con, c, SharedConstantsEnum.CALCULATOR_BOUNDARY);

    addSetting(con, c, SharedConstantsEnum.LAYER_SHIP_NETWORK);
    addSetting(con, c, SharedConstantsEnum.LAYER_MARITIME_SHIP_NETWORK);

    addSetting(con, c, SharedConstantsEnum.CONNECT_API_URL);

    addIntSetting(con, c, SharedConstantsEnum.CALCULATION_DELAY_NOTIFICATION_TIME);

    // May seem a bit weird to have product type calculator set here, but scenario(base) uses the calculator workspace in geoserver.
    addLayer(con, c, WMSLayerType.WMS_HABITATS_VIEW, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_RELEVANT_HABITAT_INFO_FOR_RECEPTOR_VIEW, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_CALCULATION_SUBSTANCE_DEPOSITION_RESULTS_VIEW, ProductType.CALCULATOR, messagesKey);
    addLayer(con, c, WMSLayerType.WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW, ProductType.CALCULATOR, messagesKey);

    return c;
  }

  private static void setGlobalDefaults(final Connection con, final AERIUS2Context c) throws SQLException {
    addIntSetting(con, c, SharedConstantsEnum.MIN_YEAR);
    addIntSetting(con, c, SharedConstantsEnum.MAX_YEAR);
    addIntSetting(con, c, SharedConstantsEnum.CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI);
    c.setReceptorGridSettings(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
  }

  /**
   * @param con The connection to use
   * @param c The context
   * @param messagesKey the localization key
   * @throws SQLException on DB error
   */
  private static void fillContextWithBaseData(final Connection con, final AERIUS2Context c, final DBMessagesKey messagesKey) throws SQLException {
    final ArrayList<Substance> substances = new ArrayList<>();
    substances.add(Substance.NOXNH3);
    substances.add(Substance.NH3);
    substances.add(Substance.NOX);
    substances.add(Substance.PM25);
    substances.add(Substance.PM10);
    substances.add(Substance.NO2);
    c.setSubstances(substances);
    c.setCategories(SectorRepository.getSectorCategories(con, messagesKey));
    addSetting(con, c, SharedConstantsEnum.HELP_URL_TEMPLATE);
    addSetting(con, c, SharedConstantsEnum.AERIUS_HOST);
    addSetting(con, c, SharedConstantsEnum.MANUAL_URL);
    addIntSetting(con, c, SharedConstantsEnum.SYSTEM_INFO_POLLING_TIME);
  }

  /**
   * Retrieves the calculator limits from the database.
   *
   * @param con The connection to use.
   * @return calculator limits
   * @throws SQLException In case of DB errors.
   */
  public static CalculatorLimits getCalculatorLimits(final Connection con) throws SQLException {
    final CalculatorLimits cl = new CalculatorLimits();
    cl.setMaxSources(ConstantRepository.getNumber(con, ConstantsEnum.CALCULATOR_LIMITS_MAX_SOURCES, Integer.class));
    cl.setMaxLineLength(ConstantRepository.getNumber(con, ConstantsEnum.CALCULATOR_LIMITS_MAX_LINE_LENGTH, Integer.class));
    cl.setMaxPolygonSurface(ConstantRepository.getNumber(con, ConstantsEnum.CALCULATOR_LIMITS_MAX_POLYGON_SURFACE, Integer.class));
    return cl;
  }

  private static void addLayer(final Connection con, final AERIUS2Context context, final WMSLayerType layerType, final ProductType productType,
      final DBMessagesKey key) throws SQLException, AeriusException {
    final LayerProps layerProps = LayerRepository.getLayer(con, layerType.getLayerName(productType), key);
    if (layerProps != null) {
      context.setLayer(layerType, layerProps);
    }
  }

  @SuppressWarnings("unchecked")
  public static <C extends Context> C getContext(final Connection con, final ProductType productType, final DBMessagesKey messagesKey)
      throws SQLException, AeriusException {
    switch (productType) {
    case CALCULATOR:
      return (C) getCalculatorContext(con, messagesKey);
    case SCENARIO:
      return (C) getScenarioContext(con, messagesKey);
    default:
      return null;
    }
  }

  public static EmissionResultValueDisplaySettings getEmissionResultValueDisplaySettings(final Connection con) throws SQLException {
    final double conversionFactor = ConstantRepository.getDouble(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_CONVERSION_FACTOR);
    final DepositionValueDisplayType conversionUnit = DepositionValueDisplayType
        .valueOf(ConstantRepository.getString(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_UNIT));
    final int depositionValueRoundingLength = ConstantRepository.getInteger(con, ConstantsEnum.EMISSION_RESULT_DISPLAY_ROUNDING_LENGTH);
    final int depositionValuePreciseRoundingLength = ConstantRepository.getInteger(con,
        ConstantsEnum.EMISSION_RESULT_DISPLAY_PRECISE_ROUNDING_LENGTH);

    return new EmissionResultValueDisplaySettings(conversionUnit, conversionFactor, depositionValueRoundingLength,
        depositionValuePreciseRoundingLength);
  }
}
