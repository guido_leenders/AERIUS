/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;

/**
 * Service class for FarmLodgingCategory table.
 */
public final class FarmLodgingCategoryRepository {

  private static final String DESCRIPTION_FORMAT = "%s (%s)";

  // Queries

  private static final Query FARM_ANIMAL_CATEGORIES_QUERY =
      QueryBuilder
          .from("farm_animal_categories")
          .select(QueryAttribute.FARM_ANIMAL_CATEGORY_ID, QueryAttribute.CODE, QueryAttribute.NAME)
          .getQuery();

  private static final Query FARM_LODGING_SYSTEM_DEFINITIONS_QUERY =
      QueryBuilder
          .from("farm_lodging_system_definitions")
          .select(QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID, QueryAttribute.CODE, QueryAttribute.NAME)
          .getQuery();

  private static final Query FARM_LODGING_TYPES_QUERY =
      QueryBuilder
          .from("farm_lodging_type_emission_factors_view")
          .select(QueryAttribute.FARM_LODGING_TYPE_ID, QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID, QueryAttribute.EMISSION_FACTOR,
              QueryAttribute.FARM_ANIMAL_CATEGORY_ID, QueryAttribute.FARM_OTHER_LODGING_TYPE_ID, QueryAttribute.SCRUBBER)
          .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
          .getQuery();

  private static final Query FARM_ADDITIONAL_LODGING_SYSTEMS_QUERY =
      QueryBuilder
          .from("farm_additional_lodging_system_emission_factors_view")
          .select(QueryAttribute.FARM_ADDITIONAL_LODGING_SYSTEM_ID, QueryAttribute.EMISSION_FACTOR, QueryAttribute.FARM_ANIMAL_CATEGORY_ID,
              QueryAttribute.SCRUBBER, QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID)
          .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
          .getQuery();

  private static final Query FARM_REDUCTIVE_LODGING_SYSTEMS_QUERY =
      QueryBuilder
          .from("farm_reductive_lodging_system_reduction_factors_view")
          .select(QueryAttribute.FARM_REDUCTIVE_LODGING_SYSTEM_ID, QueryAttribute.REDUCTION_FACTOR, QueryAttribute.FARM_ANIMAL_CATEGORY_ID,
              QueryAttribute.SCRUBBER, QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID)
          .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
          .getQuery();

  private static final Query FARM_LODGING_FODDER_MEASURES_QUERY =
      QueryBuilder
          .from("farm_lodging_fodder_measure_reduction_factors_view")
          .select(QueryAttribute.FARM_LODGING_FODDER_MEASURE_ID, QueryAttribute.FARM_ANIMAL_CATEGORY_ID,
              QueryAttribute.REDUCTION_FACTOR_FLOOR, QueryAttribute.REDUCTION_FACTOR_CELLAR, QueryAttribute.REDUCTION_FACTOR_TOTAL,
              QueryAttribute.PROPORTION_FLOOR, QueryAttribute.PROPORTION_CELLAR)
          .select(AbstractCategoryCollector.BASE_CATEGORY_ATTRIBUTES)
          .getQuery();

  private static final Query LINK_FARM_ADDITIONAL_LODGING_SYSTEMS_QUERY =
      QueryBuilder
          .from("farm_lodging_types_to_additional_lodging_systems")
          .select(QueryAttribute.FARM_LODGING_TYPE_ID, QueryAttribute.FARM_ADDITIONAL_LODGING_SYSTEM_ID)
          .getQuery();

  private static final Query LINK_FARM_REDUCTIVE_LODGING_SYSTEMS_QUERY =
      QueryBuilder
          .from("farm_lodging_types_to_reductive_lodging_systems")
          .select(QueryAttribute.FARM_LODGING_TYPE_ID, QueryAttribute.FARM_REDUCTIVE_LODGING_SYSTEM_ID)
          .getQuery();

  // Private constructor

  private FarmLodgingCategoryRepository() {
  }

  // Data getters

  /**
   * Returns all farm lodging types, additional lodging systems, reductive lodging systems, fodder
   * measures, and the way these are linked to each other, from the database.
   *
   * @param con Connection to use
   * @param messagesKey DBMessagesKey to get descriptions from
   * @return FarmLodgingCategories object containing the lists.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static FarmLodgingCategories getFarmLodgingCategories(final Connection connection, final DBMessagesKey messagesKey) throws SQLException {
    final FarmAnimalCategoryCollector farmAnimalCategoryCollector = new FarmAnimalCategoryCollector(messagesKey);
    try (PreparedStatement ps = connection.prepareStatement(FARM_ANIMAL_CATEGORIES_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        farmAnimalCategoryCollector.collectEntity(rs);
      }
    }
    final Map<Integer, FarmAnimalCategory> farmAnimalCategories = farmAnimalCategoryCollector.getEntityMap();

    final FarmLodgingSystemDefinitionCollector farmLodgingSystemDefinitionCollector = new FarmLodgingSystemDefinitionCollector(messagesKey);
    try (PreparedStatement ps = connection.prepareStatement(FARM_LODGING_SYSTEM_DEFINITIONS_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        farmLodgingSystemDefinitionCollector.collectEntity(rs);
      }
    }
    final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions = farmLodgingSystemDefinitionCollector.getEntityMap();

    final FarmLodgingTypeCollector farmLodgingTypeCollector = new FarmLodgingTypeCollector(messagesKey, farmAnimalCategories,
        farmLodgingSystemDefinitions);
    try (PreparedStatement ps = connection.prepareStatement(FARM_LODGING_TYPES_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        farmLodgingTypeCollector.collectEntity(rs);
      }
    }

    final FarmAdditionalLodgingSystemCollector farmAdditionalLodgingSystemCollector = new FarmAdditionalLodgingSystemCollector(messagesKey,
        farmAnimalCategories, farmLodgingSystemDefinitions);
    try (PreparedStatement ps = connection.prepareStatement(FARM_ADDITIONAL_LODGING_SYSTEMS_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        farmAdditionalLodgingSystemCollector.collectEntity(rs);
      }
    }

    final FarmReductiveLodgingSystemCollector farmReductiveLodgingSystemCollector = new FarmReductiveLodgingSystemCollector(messagesKey,
        farmAnimalCategories, farmLodgingSystemDefinitions);
    try (PreparedStatement ps = connection.prepareStatement(FARM_REDUCTIVE_LODGING_SYSTEMS_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        farmReductiveLodgingSystemCollector.collectEntity(rs);
      }
    }

    final FarmLodgingFodderMeasureCollector farmLodgingFodderMeasureCollector = new FarmLodgingFodderMeasureCollector(messagesKey,
        farmAnimalCategories);
    try (PreparedStatement ps = connection.prepareStatement(FARM_LODGING_FODDER_MEASURES_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        farmLodgingFodderMeasureCollector.collectEntity(rs);
      }
    }

    final Map<Integer, FarmLodgingCategory> farmLodgingTypeMap = farmLodgingTypeCollector.getEntityMap();
    final Map<Integer, FarmAdditionalLodgingSystemCategory> farmAdditionalLodgingSystemMap = farmAdditionalLodgingSystemCollector.getEntityMap();
    final Map<Integer, FarmReductiveLodgingSystemCategory> farmReductiveLodgingSystemMap = farmReductiveLodgingSystemCollector.getEntityMap();

    // Link traditional types to farm lodging types.
    for (final Entry<FarmLodgingCategory, Integer> otherEntry : farmLodgingTypeCollector.getOtherFarmLodgingCategories().entrySet()) {
      otherEntry.getKey().setTraditionalFarmLodgingCategory(farmLodgingTypeMap.get(otherEntry.getValue()));
    }

    // Link additional lodging systems to the farm lodging types.
    try (PreparedStatement ps = connection.prepareStatement(LINK_FARM_ADDITIONAL_LODGING_SYSTEMS_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        // No presence checking, should fail if not present.
        final FarmLodgingCategory farmLodgingType = farmLodgingTypeMap.get(QueryAttribute.FARM_LODGING_TYPE_ID.getInt(rs));
        final FarmAdditionalLodgingSystemCategory farmAdditionalLodgingSystem = farmAdditionalLodgingSystemMap
            .get(QueryAttribute.FARM_ADDITIONAL_LODGING_SYSTEM_ID.getInt(rs));
        farmLodgingType.getFarmAdditionalLodgingSystemCategories().add(farmAdditionalLodgingSystem);
      }
    }

    // Link reductive lodging systems to the farm lodging types.
    try (PreparedStatement ps = connection.prepareStatement(LINK_FARM_REDUCTIVE_LODGING_SYSTEMS_QUERY.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        // No presence checking, should fail if not present.
        final FarmLodgingCategory farmLodgingType = farmLodgingTypeMap.get(QueryAttribute.FARM_LODGING_TYPE_ID.getInt(rs));
        final FarmReductiveLodgingSystemCategory farmReductiveLodgingSystem = farmReductiveLodgingSystemMap
            .get(QueryAttribute.FARM_REDUCTIVE_LODGING_SYSTEM_ID.getInt(rs));
        farmLodgingType.getFarmReductiveLodgingSystemCategories().add(farmReductiveLodgingSystem);
      }
    }

    final FarmLodgingCategories categories = new FarmLodgingCategories();
    categories.setFarmLodgingSystemCategories(farmLodgingTypeCollector.getEntities());
    categories.setFarmAdditionalLodgingSystemCategories(farmAdditionalLodgingSystemCollector.getEntities());
    categories.setFarmReductiveLodgingSystemCategories(farmReductiveLodgingSystemCollector.getEntities());
    categories.setFarmLodgingFodderMeasureCategories(farmLodgingFodderMeasureCollector.getEntities());
    return categories;
  }

  // Collectors

  private static class FarmAnimalCategoryCollector extends AbstractCategoryCollector<FarmAnimalCategory> {

    protected FarmAnimalCategoryCollector(final DBMessagesKey messagesKey) {
      super(QueryAttribute.FARM_ANIMAL_CATEGORY_ID, messagesKey);
    }

    @Override
    FarmAnimalCategory getNewCategory() throws SQLException {
      return new FarmAnimalCategory();
    }

    @Override
    void setDescription(final FarmAnimalCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmAnimalCategoryMessages(category, messagesKey);
    }
  }

  private static class FarmLodgingSystemDefinitionCollector extends AbstractCategoryCollector<FarmLodgingSystemDefinition> {

    protected FarmLodgingSystemDefinitionCollector(final DBMessagesKey messagesKey) {
      super(QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID, messagesKey);
    }

    @Override
    FarmLodgingSystemDefinition getNewCategory() throws SQLException {
      return new FarmLodgingSystemDefinition();
    }

    @Override
    void setDescription(final FarmLodgingSystemDefinition category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmLodgingSystemDefinitionMessages(category, messagesKey);
    }
  }

  private static class FarmLodgingTypeCollector extends AbstractCategoryCollector<FarmLodgingCategory> {

    private final Map<Integer, FarmAnimalCategory> farmAnimalCategories;
    private final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions;
    private final Map<FarmLodgingCategory, Integer> otherFarmLodgingCategories = new HashMap<>();

    protected FarmLodgingTypeCollector(final DBMessagesKey messagesKey,
        final Map<Integer, FarmAnimalCategory> farmAnimalCategories,
        final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions) {
      super(QueryAttribute.FARM_LODGING_TYPE_ID, messagesKey);
      this.farmAnimalCategories = farmAnimalCategories;
      this.farmLodgingSystemDefinitions = farmLodgingSystemDefinitions;
    }

    @Override
    FarmLodgingCategory getNewCategory() throws SQLException {
      return new FarmLodgingCategory();
    }

    @Override
    void setDescription(final FarmLodgingCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmLodgingTypeMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmLodgingCategory category, final ResultSet rs) throws SQLException {
      // Category only supports one substance at the moment, so next bit is fine.
      // as soon as other emission factors (like for PM10) are introduced, this will fail.
      category.setEmissionFactor(QueryAttribute.EMISSION_FACTOR.getDouble(rs));

      if (!QueryAttribute.FARM_OTHER_LODGING_TYPE_ID.isNull(rs)) {
        getOtherFarmLodgingCategories().put(category, QueryAttribute.FARM_OTHER_LODGING_TYPE_ID.getInt(rs));
      }
      category.setScrubber(QueryAttribute.SCRUBBER.getBoolean(rs));

      category.setAnimalCategory(farmAnimalCategories.get(QueryAttribute.FARM_ANIMAL_CATEGORY_ID.getInt(rs)));
      category.setDescription(String.format(DESCRIPTION_FORMAT, category.getDescription(), category.getAnimalCategory().getDescription()));
    }

    @Override
    public void appendEntity(final FarmLodgingCategory farmLodgingType, final ResultSet rs) throws SQLException {
      // Add BWL-code(s).
      final int id = QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID.getInt(rs);
      if (farmLodgingSystemDefinitions.containsKey(id)) {
        farmLodgingType.getFarmLodgingSystemDefinitions().add(farmLodgingSystemDefinitions.get(id));
      }
    }

    public Map<FarmLodgingCategory, Integer> getOtherFarmLodgingCategories() {
      return otherFarmLodgingCategories;
    }
  }

  private static class FarmAdditionalLodgingSystemCollector extends AbstractCategoryCollector<FarmAdditionalLodgingSystemCategory> {

    private final Map<Integer, FarmAnimalCategory> farmAnimalCategories;
    private final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions;

    protected FarmAdditionalLodgingSystemCollector(final DBMessagesKey messagesKey,
        final Map<Integer, FarmAnimalCategory> farmAnimalCategories, final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions) {
      super(QueryAttribute.FARM_ADDITIONAL_LODGING_SYSTEM_ID, messagesKey);
      this.farmAnimalCategories = farmAnimalCategories;
      this.farmLodgingSystemDefinitions = farmLodgingSystemDefinitions;
    }

    @Override
    FarmAdditionalLodgingSystemCategory getNewCategory() throws SQLException {
      return new FarmAdditionalLodgingSystemCategory();
    }

    @Override
    void setDescription(final FarmAdditionalLodgingSystemCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setAdditionalFarmLodgingSystemMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmAdditionalLodgingSystemCategory category, final ResultSet rs) throws SQLException {
      category.setEmissionFactor(QueryAttribute.EMISSION_FACTOR.getDouble(rs));
      category.setScrubber(QueryAttribute.SCRUBBER.getBoolean(rs));

      final FarmAnimalCategory animalCategory = farmAnimalCategories.get(QueryAttribute.FARM_ANIMAL_CATEGORY_ID.getInt(rs));
      category.setDescription(String.format(DESCRIPTION_FORMAT, category.getDescription(), animalCategory.getDescription()));
    }

    @Override
    public void appendEntity(final FarmAdditionalLodgingSystemCategory category, final ResultSet rs) throws SQLException {
      // Add BWL-code(s).
      final int id = QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID.getInt(rs);
      if (farmLodgingSystemDefinitions.containsKey(id)) {
        category.getFarmLodgingSystemDefinitions().add(farmLodgingSystemDefinitions.get(id));
      }
    }
  }

  private static class FarmReductiveLodgingSystemCollector extends AbstractCategoryCollector<FarmReductiveLodgingSystemCategory> {

    private final Map<Integer, FarmAnimalCategory> farmAnimalCategories;
    private final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions;

    protected FarmReductiveLodgingSystemCollector(final DBMessagesKey messagesKey,
        final Map<Integer, FarmAnimalCategory> farmAnimalCategories, final Map<Integer, FarmLodgingSystemDefinition> farmLodgingSystemDefinitions) {
      super(QueryAttribute.FARM_REDUCTIVE_LODGING_SYSTEM_ID, messagesKey);
      this.farmAnimalCategories = farmAnimalCategories;
      this.farmLodgingSystemDefinitions = farmLodgingSystemDefinitions;
    }

    @Override
    FarmReductiveLodgingSystemCategory getNewCategory() throws SQLException {
      return new FarmReductiveLodgingSystemCategory();
    }

    @Override
    void setDescription(final FarmReductiveLodgingSystemCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setReductiveFarmLodgingSystemMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmReductiveLodgingSystemCategory category, final ResultSet rs) throws SQLException {
      category.setReductionFactor(QueryAttribute.REDUCTION_FACTOR.getDouble(rs));
      category.setScrubber(QueryAttribute.SCRUBBER.getBoolean(rs));

      final FarmAnimalCategory animalCategory = farmAnimalCategories.get(QueryAttribute.FARM_ANIMAL_CATEGORY_ID.getInt(rs));
      category.setDescription(String.format(DESCRIPTION_FORMAT, category.getDescription(), animalCategory.getDescription()));
    }

    @Override
    public void appendEntity(final FarmReductiveLodgingSystemCategory category, final ResultSet rs) throws SQLException {
      // Add BWL-code(s).
      final int id = QueryAttribute.FARM_LODGING_SYSTEM_DEFINITION_ID.getInt(rs);
      if (farmLodgingSystemDefinitions.containsKey(id)) {
        category.getFarmLodgingSystemDefinitions().add(farmLodgingSystemDefinitions.get(id));
      }
    }
  }

  private static class FarmLodgingFodderMeasureCollector extends AbstractCategoryCollector<FarmLodgingFodderMeasureCategory> {

    private final Map<Integer, FarmAnimalCategory> farmAnimalCategories;

    protected FarmLodgingFodderMeasureCollector(final DBMessagesKey messagesKey, final Map<Integer, FarmAnimalCategory> farmAnimalCategories) {
      super(QueryAttribute.FARM_LODGING_FODDER_MEASURE_ID, messagesKey);
      this.farmAnimalCategories = farmAnimalCategories;
    }

    @Override
    FarmLodgingFodderMeasureCategory getNewCategory() throws SQLException {
      return new FarmLodgingFodderMeasureCategory();
    }

    @Override
    void setDescription(final FarmLodgingFodderMeasureCategory category, final ResultSet rs) throws SQLException {
      DBMessages.setFarmLodgingFodderMeasureMessages(category, messagesKey);
    }

    @Override
    void setRemainingInfo(final FarmLodgingFodderMeasureCategory category, final ResultSet rs) throws SQLException {
      category.setReductionFactorFloor(QueryAttribute.REDUCTION_FACTOR_FLOOR.getDouble(rs));
      category.setReductionFactorCellar(QueryAttribute.REDUCTION_FACTOR_CELLAR.getDouble(rs));
      category.setReductionFactorTotal(QueryAttribute.REDUCTION_FACTOR_TOTAL.getDouble(rs));
    }

    @Override
    public void appendEntity(final FarmLodgingFodderMeasureCategory category, final ResultSet rs) throws SQLException {
      // Add proportions per animal category
      category.addAmmoniaProportion(farmAnimalCategories.get(QueryAttribute.FARM_ANIMAL_CATEGORY_ID.getInt(rs)),
          QueryAttribute.PROPORTION_FLOOR.getDouble(rs), QueryAttribute.PROPORTION_CELLAR.getDouble(rs));
    }
  }
}
