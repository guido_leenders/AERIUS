/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.util.ArrayWhereClause;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.BooleanWhereClause;
import nl.overheid.aerius.db.util.ILikeWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.JoinClause.JoinType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.QueryUtil.ValueFactory;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.db.util.SpecialAttribute;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Most of this is temporary, to quickly create users.
 */
public final class UserRepository {

  private enum RepositoryAttribute implements Attribute {

    ENABLED, USERNAME, EMAIL_ADDRESS, INITIALS, FIRSTNAME, LASTNAME,

    USERROLE_ID, ROLENAME, PERMISSION,

    SETTING_KEY, SETTING_VALUE,

    PASSWORD;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final Logger LOG = LoggerFactory.getLogger(UserRepository.class);

  private static final String INSERT_USER =
      // ae_add_user(v_username text, v_password text, v_email_address text, v_initials text, v_firstname text, v_lastname text,
      // v_authority_id integer)
      "SELECT ae_add_user(?, ?, ?, ?, ?, ?, ?) AS created_user_id ";

  private static final String UPDATE_USER_IN_USERS = "UPDATE users SET email_address = ?, enabled = ? WHERE user_id = ?";
  private static final String UPDATE_USER_DETAILS = "UPDATE user_details SET authority_id = ?, initials"
      + " = ?, firstname = ?, lastname = ? WHERE user_id = ?";
  private static final String UPDATE_ROLE = "UPDATE userroles SET name = ?, color = ? WHERE userrole_id = ?";
  private static final String DELETE_CURRENT_ROLES_FOR_USER = "DELETE FROM users_userroles WHERE user_id = ?";
  private static final String INSERT_ROLE_FOR_USER = "INSERT INTO users_userroles (user_id, userrole_id) VALUES (?, ?)";
  private static final String DELETE_USER = "SELECT ae_delete_user(?)";

  private static final Query GET_USER = getBaseUserProfiles("users_view").where(QueryAttribute.USER_ID).getQuery();
  private static final Query GET_USER_ID_BY_NAME = QueryBuilder.from("users").select(QueryAttribute.USER_ID).where(RepositoryAttribute.USERNAME)
      .getQuery();
  private static final Query GET_ROLES_AND_PERMISSIONS_FOR_USER = QueryBuilder.from("users_roles_permissions_view")
      .select(RepositoryAttribute.USERROLE_ID, RepositoryAttribute.ROLENAME, QueryAttribute.COLOR, RepositoryAttribute.PERMISSION)
      .where(QueryAttribute.USER_ID).getQuery();
  private static final Query SELECT_USER_PASSWORD = QueryBuilder.from("users").select(RepositoryAttribute.PASSWORD)
      .where(RepositoryAttribute.EMAIL_ADDRESS).getQuery();
  private static final Query GET_AUTHORITY_USERS_MAP = getBaseUserProfiles("users_with_edit_rights_view").getQuery();
  private static final Query GET_SPECIFIC_ROLE = getBaseUserRoles().where(QueryAttribute.NAME).getQuery();
  private static final Query GET_ROLES_AND_PERMISSIONS = getBaseUserRoles()
      .join(new JoinClause("userroles_to_permissions", QueryAttribute.USERROLE_ID))
      .join(new JoinClause("userpermissions", QueryAttribute.PERMISSION_ID))
      .select(new SelectClause("userpermissions.name", RepositoryAttribute.PERMISSION.attribute())).getQuery();

  private static final String UPDATE_USER_PASSWORD = "UPDATE users SET password = ? WHERE email_address = ?";

  private UserRepository() {
    // Not allowed to instantiate.
  }

  /**
   * @param con The connection to use.
   * @param userProfile The userProfile to create.
   * @param password The password to use for the new user.
   * @return The ID of the created user.
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the user already exists.
   */
  public static int createUser(final Connection con, final AdminUserProfile userProfile, final String password) throws SQLException, AeriusException {
    try {
      final UserProfile existingProfile = getUserProfileByName(con, userProfile.getName());
      if (existingProfile != null) {
        throw new AeriusException(Reason.USER_ALREADY_EXISTS, userProfile.getName());
      }
    } catch (final AeriusException e) {
      if (e.getReason() != Reason.USER_DOES_NOT_EXIST) {
        throw e;
      }
    }
    final int createdUserId;
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_USER)) {
      // ae_add_user(v_username text, v_password text, v_email_address text, v_initials text, v_firstname text,
      // v_lastname text, v_authority_id integer)
      int parameterIndex = 1;
      stmt.setString(parameterIndex++, userProfile.getName());
      stmt.setString(parameterIndex++, password);
      stmt.setString(parameterIndex++, userProfile.getEmailAddress());
      stmt.setString(parameterIndex++, userProfile.getInitials() == null ? "" : userProfile.getInitials());
      stmt.setString(parameterIndex++, userProfile.getFirstName() == null ? "" : userProfile.getFirstName());
      stmt.setString(parameterIndex++, userProfile.getLastName() == null ? "" : userProfile.getLastName());
      stmt.setInt(parameterIndex++, userProfile.getAuthority().getId());
      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        createdUserId = rs.getInt("created_user_id");
      } else {
        throw new AeriusException(Reason.INTERNAL_ERROR, userProfile.getName());
      }
      userProfile.setId(createdUserId);
      setUsersUserRoles(con, userProfile);
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        LOG.error("Error creating userprofile for {}", userProfile.getName(), e);
        throw new AeriusException(Reason.USER_ALREADY_EXISTS, userProfile.getName());
      } else {
        throw e;
      }
    }
    return createdUserId;
  }

  /**
   * @param con The connection to use.
   * @param userProfile The userProfile to update.
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the user does not exist.
   */
  public static void updateUser(final Connection con, final AdminUserProfile userProfile) throws SQLException, AeriusException {
    validateExistingUser(con, userProfile);
    final Transaction lock = new Transaction(con);
    try {
      updateUsersTable(con, userProfile);

      updateUserDetails(con, userProfile);

      setUsersUserRoles(con, userProfile);
    } catch (final SQLException e) {
      LOG.error("Error setting user roles for user with ID {}", userProfile.getId(), e);
      lock.rollback();
      throw e;
    } finally {
      lock.commit();
    }
  }

  public static void updateRole(final Connection con, final AdminUserRole role) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_ROLE)) {
      // UPDATE userroles SET name = ?, color = ? WHERE userrole_id = ?
      int parameterIndex = 1;
      stmt.setString(parameterIndex++, role.getName());
      stmt.setString(parameterIndex++, role.getColor());
      stmt.setInt(parameterIndex++, role.getId());
      stmt.execute();

      // TODO Update permissions (not supported in client)
    } catch (final SQLException e) {
      LOG.error("Error updating role: {}", role, e);
      throw e;
    }
  }

  private static void setUsersUserRoles(final Connection con, final AdminUserProfile userProfile) throws AeriusException, SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(DELETE_CURRENT_ROLES_FOR_USER)) {
      // DELETE FROM users_userroles WHERE user_id = ?
      stmt.setInt(1, userProfile.getId());
      stmt.execute();
    }
    if (userProfile.getRoles() != null) {
      for (final UserRole userRole : userProfile.getRoles()) {
        try (final PreparedStatement stmt = con.prepareStatement(INSERT_ROLE_FOR_USER)) {
          // INSERT INTO users_userroles (user_id, userrole_id) VALUES (?, ?)
          stmt.setInt(1, userProfile.getId());
          stmt.setInt(2, userRole.getId());
          stmt.execute();
        }
      }
    }
  }

  private static void updateUsersTable(final Connection con, final AdminUserProfile userProfile) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_USER_IN_USERS)) {
      // UPDATE users SET email_address = ?, enabled = ? WHERE user_id = ?
      int parameterIndex = 1;
      stmt.setString(parameterIndex++, userProfile.getEmailAddress());
      stmt.setBoolean(parameterIndex++, userProfile.isEnabled());
      stmt.setInt(parameterIndex++, userProfile.getId());
      stmt.execute();
    }
  }

  private static void updateUserDetails(final Connection con, final UserProfile userProfile) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_USER_DETAILS)) {
      // UPDATE user_details SET authority_id = ?, initials = ?, firstname = ?, lastname = ? WHERE user_id = ?
      int parameterIndex = 1;
      stmt.setInt(parameterIndex++, userProfile.getAuthority().getId());
      stmt.setString(parameterIndex++, userProfile.getInitials() == null ? "" : userProfile.getInitials());
      stmt.setString(parameterIndex++, userProfile.getFirstName() == null ? "" : userProfile.getFirstName());
      stmt.setString(parameterIndex++, userProfile.getLastName() == null ? "" : userProfile.getLastName());
      stmt.setInt(parameterIndex++, userProfile.getId());
      stmt.execute();
    }
  }

  /**
   * @param con The connection to use.
   * @param userProfile The userProfile to delete.
   * @throws SQLException In case of a database error.
   * @throws AeriusException When the user already exists.
   */
  public static void deleteUser(final Connection con, final UserProfile userProfile) throws SQLException, AeriusException {
    validateExistingUser(con, userProfile);
    try (final PreparedStatement stmt = con.prepareStatement(DELETE_USER)) {
      stmt.setInt(1, userProfile.getId());

      if (!stmt.execute()) {
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Deleting user " + userProfile.getName(), userProfile.getName());
    }
  }

  /**
   * @param con The connection to use.
   * @param username The username of the userprofile to retrieve.
   * @return The userprofile that fits the username.
   * @throws SQLException In case of a database error.
   * @throws AeriusException
   */
  public static UserProfile getUserProfileByName(final Connection con, final String username) throws SQLException, AeriusException {
    return getUserProfileByNameFrom(new UserProfile(), con, username);
  }

  /**
   * @param con The connection to use.
   * @param username The username of the userprofile to retrieve.
   * @return The userprofile that fits the username.
   * @throws SQLException In case of a database error.
   * @throws AeriusException
   */
  public static AdminUserProfile getAdminUserProfileByName(final Connection con, final String username) throws SQLException, AeriusException {
    return getUserProfileByNameFrom(new AdminUserProfile(), con, username);
  }

  /**
   * @param con The connection to use.
   * @param username The username of the userprofile to retrieve.
   * @return The userprofile that fits the username.
   * @throws SQLException In case of a database error.
   * @throws AeriusException
   */
  public static <U extends UserProfile> U getUserProfileByNameFrom(final U profile, final Connection con, final String username)
      throws SQLException, AeriusException {
    int userProfileId = 0;
    try (final PreparedStatement stmt = con.prepareStatement(GET_USER_ID_BY_NAME.get())) {
      GET_USER_ID_BY_NAME.setParameter(stmt, RepositoryAttribute.USERNAME, username);

      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        userProfileId = QueryAttribute.USER_ID.getInt(rs);
      } else {
        throw new AeriusException(Reason.USER_DOES_NOT_EXIST, username);
      }
    }
    return getUserProfileFrom(profile, con, userProfileId);
  }

  public static UserProfile getUserProfile(final Connection con, final int userId) throws SQLException {
    return getUserProfileFrom(new UserProfile(), con, userId);
  }

  public static AdminUserProfile getAdminUserProfile(final Connection con, final int userId) throws SQLException {
    return getUserProfileFrom(new AdminUserProfile(), con, userId);
  }

  /**
   * Get the user profile associated with the given userId.
   *
   * @param con The connection to use.
   * @param userId The ID of the userprofile to retrieve.
   * @return The userprofile that matches the user ID.
   * @throws SQLException In case of a database error.
   */
  public static <U extends UserProfile> U getUserProfileFrom(final U profile, final Connection con, final int userId) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(GET_USER.get())) {
      GET_USER.setParameter(stmt, QueryAttribute.USER_ID, userId);

      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        getBasicUserProfileFrom(rs, profile);
        profile.setAuthority(AuthorityRepository.getAuthority(con, QueryUtil.getInt(rs, QueryAttribute.AUTHORITY_ID)));

        profile.setEmailAddress(QueryUtil.getString(rs, RepositoryAttribute.EMAIL_ADDRESS));

        setRolesAndPermissions(con, profile);
      } else {
        return null;
      }
    }
    return profile;
  }

  private static void setRolesAndPermissions(final Connection con, final UserProfile userProfile) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(GET_ROLES_AND_PERMISSIONS_FOR_USER.get())) {
      GET_ROLES_AND_PERMISSIONS_FOR_USER.setParameter(stmt, QueryAttribute.USER_ID, userProfile.getId());

      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        if (userProfile instanceof AdminUserProfile) {
          ((AdminUserProfile) userProfile).getRoles().add(new UserRole(QueryUtil.getInt(rs, RepositoryAttribute.USERROLE_ID),
              QueryUtil.getString(rs, RepositoryAttribute.ROLENAME), QueryAttribute.COLOR.getString(rs)));
        }
        userProfile.getPermissions().add(QueryUtil.getString(rs, RepositoryAttribute.PERMISSION));
      }
    }
  }

  /**
   * @param con The connection to use.
   * @param email The email of the user.
   * @return The hashed password of the user.
   * @throws SQLException In case of database error.
   * @throws AeriusException When user was not found.
   */
  public static String getUserPasswordHashed(final Connection con, final String email) throws SQLException, AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(SELECT_USER_PASSWORD.get())) {
      SELECT_USER_PASSWORD.setParameter(stmt, RepositoryAttribute.EMAIL_ADDRESS, email.toLowerCase());

      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        return QueryUtil.getString(rs, RepositoryAttribute.PASSWORD);
      } else {
        throw new AeriusException(Reason.USER_DOES_NOT_EXIST, email);
      }
    }
  }

  /**
   * @param con Connection to use.
   * @param email The email of the user who wants to update their password.
   * @param password The (already encrypted) password.
   * @throws SQLException In case of DB errors.
   */
  public static void updateUserPassword(final Connection con, final String email, final String password) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(UPDATE_USER_PASSWORD)) {
      stmt.setString(2, email.toLowerCase());
      stmt.setString(1, password);
      stmt.executeUpdate();
    }
  }

  /**
   * @param con The connection to use.
   * @return A map containing the authority as key and a list of users that are linked to this authority.
   * @throws SQLException In case of a database error.
   */
  public static LinkedHashMap<Authority, ArrayList<UserProfile>> getAuthorityUserMap(final Connection con) throws SQLException {
    final LinkedHashMap<Authority, ArrayList<UserProfile>> authorityUserMap = new LinkedHashMap<>();
    final EntityCollector<Authority> authorityCollector = new EntityCollector<Authority>() {

      @Override
      public int getKey(final ResultSet rs) throws SQLException {
        return QueryAttribute.AUTHORITY_ID.getInt(rs);
      }

      @Override
      public Authority fillEntity(final ResultSet rs) throws SQLException {
        return AuthorityRepository.getSkinnedAuthority(con, QueryAttribute.AUTHORITY_ID.getInt(rs));
      }
    };
    try (final PreparedStatement stmt = con.prepareStatement(GET_AUTHORITY_USERS_MAP.get())) {
      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        final UserProfile profile = getBasicUserProfileFrom(rs, new UserProfile());

        final Authority authority = authorityCollector.collectEntity(rs);

        if (!authorityUserMap.containsKey(authority)) {
          authorityUserMap.put(authority, new ArrayList<UserProfile>());
        }
        profile.setAuthority(authority);

        final List<UserProfile> usersForAuthority = authorityUserMap.get(authority);

        usersForAuthority.add(profile);
      }
    }
    return authorityUserMap;
  }

  /**
   * @param con The connection to use.
   * @return A list of all roles in the database.
   * @throws SQLException In case of a database error.
   */
  public static UserRole getRole(final Connection con, final String roleName) throws SQLException {
    UserRole role = null;
    try (final PreparedStatement selectPS = con.prepareStatement(GET_SPECIFIC_ROLE.get())) {
      GET_SPECIFIC_ROLE.setParameter(selectPS, QueryAttribute.NAME, roleName);
      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          role = new UserRole(QueryUtil.getInt(rs, RepositoryAttribute.USERROLE_ID), QueryUtil.getString(rs, QueryAttribute.NAME),
              QueryAttribute.COLOR.getString(rs));
        }
      }
    }
    return role;
  }

  /**
   * @param con The connection to use.
   * @return A list of all roles in the database.
   * @throws SQLException In case of a database error.
   */
  public static HashSet<AdminUserRole> getRolesAndPermissions(final Connection con) throws SQLException {
    final HashMap<Integer, AdminUserRole> roles = new HashMap<>();
    try (final PreparedStatement selectPS = con.prepareStatement(GET_ROLES_AND_PERMISSIONS.get())) {
      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final int id = QueryUtil.getInt(rs, RepositoryAttribute.USERROLE_ID);
          if (!roles.containsKey(id)) {
            roles.put(id, new AdminUserRole(id, QueryUtil.getString(rs, QueryAttribute.NAME), QueryAttribute.COLOR.getString(rs)));
          }

          roles.get(id).getPermissions().add(QueryUtil.getString(rs, RepositoryAttribute.PERMISSION));
        }
      }
    }

    return new HashSet<AdminUserRole>(roles.values());
  }

  /**
   * @param con The connection to use.
   * @param maxResults The maximum results to return, if 0 no limit is set.
   * @param offset The offset of the results (to be used for paging).
   * @param filter The filter to use when retrieving results.
   * @return A list of userprofiles.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<AdminUserProfile> getUserProfiles(final Connection con, final int maxResults, final int offset,
      final UserManagementFilter filter) throws SQLException {
    final ArrayList<AdminUserProfile> results = new ArrayList<>();
    final boolean doLimit = maxResults > 0;
    final boolean doOffset = offset > 0;
    final Query query = getUsersQuery(filter, doLimit, doOffset);
    try (final PreparedStatement selectPS = con.prepareStatement(query.get())) {
      setFilterParams(con, query, selectPS, filter);

      if (doLimit) {
        query.setParameter(selectPS, SpecialAttribute.LIMIT, maxResults);
      }
      if (doOffset) {
        query.setParameter(selectPS, SpecialAttribute.OFFSET, offset);
      }

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final AdminUserProfile userProfile = getBasicUserProfileFrom(rs, new AdminUserProfile());
          userProfile.setEmailAddress(QueryUtil.getString(rs, RepositoryAttribute.EMAIL_ADDRESS));

          userProfile.setAuthority(AuthorityRepository.getSkinnedAuthority(con, QueryUtil.getInt(rs, QueryAttribute.AUTHORITY_ID)));

          setRolesAndPermissions(con, userProfile);
          results.add(userProfile);
        }
      }
    }
    return results;
  }

  private static void setFilterParams(final Connection con, final Query query, final PreparedStatement selectPS, final UserManagementFilter filter)
      throws SQLException {
    if (!StringUtils.isEmpty(filter.getUsername())) {
      query.setParameter(selectPS, RepositoryAttribute.USERNAME, ILikeWhereClause.getSearchString(filter.getUsername()));
      query.setParameter(selectPS, RepositoryAttribute.LASTNAME, ILikeWhereClause.getSearchString(filter.getUsername()));
    }

    if (filter.getEnabled() != null) {
      query.setParameter(selectPS, RepositoryAttribute.ENABLED, filter.getEnabled());
    }

    if (!filter.getOrganisations().isEmpty()) {
      query.setParameter(selectPS, QueryAttribute.AUTHORITY_ID,
          QueryUtil.toNumericSQLArray(con, filter.getOrganisations(), new ValueFactory<Authority, Integer>() {
            @Override
            public Integer getValue(final Authority obj) {
              return obj.getId();
            }
          }));
    }
    if (!filter.getRoles().isEmpty()) {
      query.setParameter(selectPS, RepositoryAttribute.USERROLE_ID,
          QueryUtil.toNumericSQLArray(con, filter.getRoles(), new ValueFactory<AdminUserRole, Integer>() {
            @Override
            public Integer getValue(final AdminUserRole obj) {
              return obj.getId();
            }
          }));
    }
  }

  private static void validateExistingUser(final Connection con, final UserProfile userProfile) throws AeriusException, SQLException {
    final UserProfile existingProfile = getUserProfileFrom(new UserProfile(), con, userProfile.getId());
    if (existingProfile == null) {
      throw new AeriusException(Reason.USER_DOES_NOT_EXIST, userProfile.getName());
    }
  }

  private static Query getUsersQuery(final UserManagementFilter filter, final boolean doLimit, final boolean doOffset) {
    final QueryBuilder builder = getBaseUserProfiles("users_view")
        .join(new JoinClause("users_roles_permissions_view", QueryAttribute.USER_ID, JoinType.LEFT))
        .distinct();

    if (doLimit) {
      builder.limit();
    }
    if (doOffset) {
      builder.offset();
    }

    if (filter.getEnabled() != null) {
      builder.where(new BooleanWhereClause(RepositoryAttribute.ENABLED, filter.getEnabled()));
    }

    if (!StringUtils.isEmpty(filter.getUsername())) {
      builder.where(new StaticWhereClause("(username ILIKE ? OR lastname ILIKE ?)", RepositoryAttribute.USERNAME, RepositoryAttribute.LASTNAME));
    }
    if (!filter.getOrganisations().isEmpty()) {
      builder.where(new ArrayWhereClause(QueryAttribute.AUTHORITY_ID));
    }
    if (!filter.getRoles().isEmpty()) {
      builder.where(new ArrayWhereClause(RepositoryAttribute.USERROLE_ID));
    }

    return builder.getQuery();
  }

  private static <U extends UserProfile> U getBasicUserProfileFrom(final ResultSet rs, final U profile) throws SQLException {
    profile.setId(QueryAttribute.USER_ID.getInt(rs));

    if (profile instanceof AdminUserProfile) {
      ((AdminUserProfile) profile).setEnabled(QueryUtil.getBoolean(rs, RepositoryAttribute.ENABLED));
    }

    profile.setName(QueryUtil.getString(rs, RepositoryAttribute.USERNAME));
    profile.setInitials(QueryUtil.getString(rs, RepositoryAttribute.INITIALS));
    profile.setFirstName(QueryUtil.getString(rs, RepositoryAttribute.FIRSTNAME));
    profile.setLastName(QueryUtil.getString(rs, RepositoryAttribute.LASTNAME));
    profile.setEmailAddress(QueryUtil.getString(rs, RepositoryAttribute.EMAIL_ADDRESS));

    return profile;
  }

  private static QueryBuilder getBaseUserProfiles(final String from) {
    return QueryBuilder.from(from).select(QueryAttribute.USER_ID, RepositoryAttribute.ENABLED, QueryAttribute.AUTHORITY_ID,
        RepositoryAttribute.USERNAME, RepositoryAttribute.INITIALS, RepositoryAttribute.FIRSTNAME, RepositoryAttribute.LASTNAME,
        RepositoryAttribute.EMAIL_ADDRESS);
  }

  private static QueryBuilder getBaseUserRoles() {
    return QueryBuilder.from("userroles").select(RepositoryAttribute.USERROLE_ID, QueryAttribute.COLOR)
        .select(new SelectClause("userroles.name", null));
  }
}
