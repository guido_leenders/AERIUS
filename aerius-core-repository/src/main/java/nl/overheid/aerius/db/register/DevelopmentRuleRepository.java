/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;

import nl.overheid.aerius.db.common.DevelopmentRuleResultsHandler;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.SegmentType;

/**
 * Functionality for fetching development rules from the database.
 */
public final class DevelopmentRuleRepository {

  private static final Query GET_PERMIT_DEVELOPMENT_RULE_RESULTS = DevelopmentRuleResultsHandler
      .getQueryBuilder("ae_development_rule_request_checks(?, ?::segment_type)", RegisterAttribute.REQUEST_ID, QueryAttribute.SEGMENT).getQuery();

  private DevelopmentRuleRepository() {
    // Not allowed to instantiate.
  }

  /**
   * @param connection The connection to use.
   * @param key The key of the permit to get the development rule results for.
   * @return The map with the results per AssessmentArea.
   * @throws SQLException In case of errors.
   */
  public static HashSet<DevelopmentRuleResultList> getPermitResultsByArea(final Connection connection,
      final DossierAuthorityKey key) throws SQLException {
    final int permitId = PermitRepository.getPermitIdByKey(connection, key);
    final DevelopmentRuleResultsHandler handler = new DevelopmentRuleResultsHandler(connection) {
      @Override
      protected void setParameters(final PreparedStatement selectPS, final Query query) throws SQLException {
        query.setParameter(selectPS, RegisterAttribute.REQUEST_ID, permitId);
        query.setParameter(selectPS, QueryAttribute.SEGMENT, SegmentType.PROJECTS.getDbValue());
      }
    };
    handler.execute(connection, GET_PERMIT_DEVELOPMENT_RULE_RESULTS);
    return handler.getResults();
  }
}
