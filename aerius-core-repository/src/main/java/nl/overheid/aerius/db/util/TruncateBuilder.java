/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * TRUNCATE query construction helper.
 * <br><br>
 * This a very simple utility class because it only takes the table name.
 */
public final class TruncateBuilder {

  private static final String TRUNCATE_STATEMENT = "TRUNCATE TABLE ";

  private final List<String> tables = new ArrayList<>();

  private TruncateBuilder(final String table) {
    tables.add(table);
  }

  private TruncateBuilder(final String... tables) {
    this.tables.addAll(Arrays.asList(tables));
  }

  /**
   * Method to start building a truncate query.
   * @param table The table to truncate.
   * @return The TruncateBuilder to continue working with.
   */
  public static TruncateBuilder truncate(final String table) {
    return new TruncateBuilder(table);
  }

  /**
   * Method to start building a truncate query that truncates multiple tables simultaneously.
   * @param tables The tables to truncate.
   * @return The TruncateBuilder to continue working with.
   */
  public static TruncateBuilder truncate(final String... tables) {
    return new TruncateBuilder(tables);
  }

  /**
   * Method to add another table to truncate. Chainable.
   * @param table The table to truncate.
   * @return The TruncateBuilder to continue working with.
   */
  public TruncateBuilder alsoTruncate(final String table) {
    tables.add(table);

    return this;
  }

  /**
   * @return The query that fits the previously called methods.
   */
  public Query getQuery() {
    final StringBuilder builder = new StringBuilder();
    builder.append(TRUNCATE_STATEMENT);
    builder.append(StringUtils.join(tables, ", "));
    return new Query(builder.toString(), null);
  }

}
