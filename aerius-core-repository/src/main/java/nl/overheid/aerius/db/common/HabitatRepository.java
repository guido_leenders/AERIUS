/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.BooleanWhereClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.info.EcologyQualityType;
import nl.overheid.aerius.shared.domain.info.HabitatGoal;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.HabitatInfo.HabitatSurfaceType;
import nl.overheid.aerius.shared.domain.info.HabitatType;

/**
 * Service class for Habitat database queries.
 */
public class HabitatRepository {
  // Queries

  private static final Attribute[] EXTENDED_ATTRIBUTES = new Attribute[] { QueryAttribute.SURFACE, QueryAttribute.HABITAT_QUALITY_TYPE,
      QueryAttribute.QUALITY_GOAL, QueryAttribute.EXTENT_GOAL, QueryAttribute.DESIGNATED, QueryAttribute.COVERAGE, QueryAttribute.RELEVANT_COVERAGE,
      QueryAttribute.CARTOGRAPHIC_SURFACE, QueryAttribute.RELEVANT_SURFACE, QueryAttribute.RELEVANT_CARTOGRAPHIC_SURFACE, };

  private static final Attribute[] CRITICAL_LEVEL_ATTRIBUTES = new Attribute[] { QueryAttribute.CRITICAL_LEVEL, QueryAttribute.SUBSTANCE_ID,
      QueryAttribute.RESULT_TYPE, };

  private static final Query HABITAT_TYPES = getHabitatTypeQueryBuilder("habitat_types").orderBy(QueryAttribute.NAME).getQuery();

  private static final Query HABITAT_INFO_BY_ASSESSMENT_AREA = getExtendedHabitatInfoQueryBuilder("habitat_info_for_assessment_area_view").getQuery();

  private static final Query RELEVANT_HABITAT_INFO_BY_ASSESSMENT_AREA = getExtendedHabitatInfoQueryBuilder("habitat_info_for_assessment_area_view")
      .where(new BooleanWhereClause(QueryAttribute.RELEVANT, true)).getQuery();

  protected HabitatRepository() {}

  // Query builders

  protected static QueryBuilder getGoalHabitatTypeQueryBuilder(final String from) {
    return QueryBuilder.from(from).select(QueryAttribute.GOAL_HABITAT_TYPE_ID);
  }

  protected static QueryBuilder getHabitatTypeQueryBuilder(final String from) {
    return QueryBuilder.from(from).select(QueryAttribute.HABITAT_TYPE_ID);
  }

  protected static QueryBuilder getBaseGoalHabitatQueryBuilder(final String from) {
    return getGoalHabitatTypeQueryBuilder(from).where(QueryAttribute.ASSESSMENT_AREA_ID);
  }

  protected static QueryBuilder getBaseHabitatQueryBuilder(final String from) {
    return getHabitatTypeQueryBuilder(from).where(QueryAttribute.ASSESSMENT_AREA_ID);
  }

  protected static QueryBuilder getExtendedGoalHabitatInfoQueryBuilder(final String from) {
    return getBaseGoalHabitatQueryBuilder(from).select(EXTENDED_ATTRIBUTES).select(CRITICAL_LEVEL_ATTRIBUTES);
  }

  protected static QueryBuilder getExtendedHabitatInfoQueryBuilder(final String from) {
    return getBaseHabitatQueryBuilder(from).select(EXTENDED_ATTRIBUTES).select(CRITICAL_LEVEL_ATTRIBUTES);
  }

  // Info getters

  /**
   * Returns a list of all habitat types from the database.
   *
   * @param con The connection to use
   * @return List of all habitat types.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<HabitatType> getHabitatTypes(final Connection con, final DBMessagesKey key) throws SQLException {
    return collectAll(con, HABITAT_TYPES, new HabitatTypeCollector(key));
  }

  /**
   * @param con The connection to use
   * @param assessmentAreaId The id of the assessment area to get the habitat type info for.
   * @return The list of habitats on the assessment area.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<HabitatInfo> getHabitatInfo(final Connection con, final int assessmentAreaId, final DBMessagesKey key) throws SQLException {
    return collectByAssessmentArea(con, HABITAT_INFO_BY_ASSESSMENT_AREA, assessmentAreaId, new HabitatCriticalLevelExtendedInfoCollector(key));
  }

  /**
   * @param con The connection to use
   * @param assessmentAreaId The id of the assessment area to get the habitat type info for.
   * @return The list of relevant habitats on the assessment area.
   * @throws SQLException In case of a database error.
   */
  public static ArrayList<HabitatInfo> getRelevantHabitatInfo(final Connection con, final int assessmentAreaId, final DBMessagesKey key)
      throws SQLException {
    return collectByAssessmentArea(con, RELEVANT_HABITAT_INFO_BY_ASSESSMENT_AREA, assessmentAreaId,
        new HabitatCriticalLevelExtendedInfoCollector(key));
  }

  // Info getters - query/resultset/collector helpers

  protected static <T extends HasId> ArrayList<T> collectAll(final Connection con, final Query query, final EntityCollector<T> collector)
      throws SQLException {
    try (PreparedStatement ps = con.prepareStatement(query.get())) {
      final ResultSet rs = ps.executeQuery();
      collector.collectEntities(rs);
    }
    return collector.getEntities();
  }

  protected static <T extends HasId> ArrayList<T> collectByAssessmentArea(final Connection con, final Query query, final int assessmentAreaId,
      final EntityCollector<T> collector) throws SQLException {
    try (PreparedStatement ps = con.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);

      final ResultSet rs = ps.executeQuery();
      collector.collectEntities(rs);
    }
    return collector.getEntities();
  }

  // Collectors

  public abstract static class GoalHabitatCollector<T extends HabitatType> extends EntityCollector<T> {
    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.GOAL_HABITAT_TYPE_ID.getInt(rs);
    }
  }

  public abstract static class HabitatCollector<T extends HabitatType> extends EntityCollector<T> {
    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.HABITAT_TYPE_ID.getInt(rs);
    }
  }

  public static class HabitatTypeCollector extends HabitatCollector<HabitatType> {
    private final DBMessagesKey key;

    public HabitatTypeCollector(final DBMessagesKey key) {
      this.key = key;
    }

    @Override
    public HabitatType fillEntity(final ResultSet rs) throws SQLException {
      return setHabitatTypeFromResultSet(rs, new HabitatType(), key);
    }
  }

  public static class HabitatInfoCollector extends HabitatCollector<HabitatInfo> {
    private final DBMessagesKey key;

    public HabitatInfoCollector(final DBMessagesKey key) {
      this.key = key;
    }

    @Override
    public HabitatInfo fillEntity(final ResultSet rs) throws SQLException {
      return setHabitatInfoFromResultSet(rs, new HabitatInfo(), key);
    }

    @Override
    public void appendEntity(final HabitatInfo entity, final ResultSet rs) throws SQLException {
      EmissionResultRepositoryUtil.addCriticalLevel(rs, entity.getCriticalLevels());
    }
  }

  public abstract static class HabitatExtendedInfoCollector extends HabitatCollector<HabitatInfo> {
    private final DBMessagesKey key;

    public HabitatExtendedInfoCollector(final DBMessagesKey key) {
      this.key = key;
    }

    @Override
    public HabitatInfo fillEntity(final ResultSet rs) throws SQLException {
      return setExtendedHabitatInfoFromResultSet(rs, new HabitatInfo(), key);
    }
  }

  public static class HabitatCriticalLevelExtendedInfoCollector extends HabitatExtendedInfoCollector {
    public HabitatCriticalLevelExtendedInfoCollector(final DBMessagesKey key) {
      super(key);
    }

    @Override
    public void appendEntity(final HabitatInfo entity, final ResultSet rs) throws SQLException {
      EmissionResultRepositoryUtil.addCriticalLevel(rs, entity.getCriticalLevels());
    }
  }

  // Collectors - helpers

  protected static <T extends HabitatType> T setHabitatTypeFromResultSet(final ResultSet rs, final T habitatInfo, final DBMessagesKey key)
      throws SQLException {
    return setAgnosticHabitatTypeFromResultSet(QueryAttribute.HABITAT_TYPE_ID, rs, habitatInfo, key);
  }

  protected static <T extends HabitatType> T setAgnosticHabitatTypeFromResultSet(final QueryAttribute habitatTypeAttribute, final ResultSet rs,
      final T habitatType, final DBMessagesKey key) throws SQLException {
    habitatType.setId(habitatTypeAttribute.getInt(rs));
    DBMessages.setHabitatTypeMessages(habitatType, key);
    return habitatType;
  }

  protected static <T extends HabitatInfo> T setHabitatInfoFromResultSet(final ResultSet rs, final T habitatInfo, final DBMessagesKey key)
      throws SQLException {
    return setAgnosticHabitatInfoFromResultSet(QueryAttribute.HABITAT_TYPE_ID, rs, habitatInfo, key);
  }

  protected static <T extends HabitatInfo> T setAgnosticHabitatInfoFromResultSet(final QueryAttribute habitatTypeAttribute, final ResultSet rs,
      final T habitatInfo, final DBMessagesKey key) throws SQLException {
    setAgnosticHabitatTypeFromResultSet(habitatTypeAttribute, rs, habitatInfo, key);
    habitatInfo.setSurface(QueryAttribute.SURFACE.getDouble(rs));

    return habitatInfo;
  }

  protected static <T extends HabitatInfo> T setBasicHabitatInfoFromResultSet(final QueryAttribute habitatTypeAttribute, final ResultSet rs,
      final T habitatInfo, final DBMessagesKey key) throws SQLException {
    setAgnosticHabitatInfoFromResultSet(habitatTypeAttribute, rs, habitatInfo, key);
    habitatInfo.setEcologyQuality(EcologyQualityType.getByCode(QueryUtil.getString(rs, QueryAttribute.HABITAT_QUALITY_TYPE)));
    habitatInfo.setCoverage(QueryAttribute.COVERAGE.getDouble(rs));
    habitatInfo.setDesignated(QueryAttribute.DESIGNATED.getBoolean(rs));
    habitatInfo.setQualityGoal(QueryUtil.getEnum(rs, QueryAttribute.QUALITY_GOAL, HabitatGoal.class));
    habitatInfo.setExtentGoal(QueryUtil.getEnum(rs, QueryAttribute.EXTENT_GOAL, HabitatGoal.class));

    return habitatInfo;
  }

  protected static <T extends HabitatInfo> T setExtendedGoalHabitatInfoFromResultSet(final ResultSet rs, final T habitatInfo, final DBMessagesKey key)
      throws SQLException {
    return setExtendedAgnosticHabitatInfoFromResultSet(QueryAttribute.GOAL_HABITAT_TYPE_ID, rs, habitatInfo, key);
  }

  protected static <T extends HabitatInfo> T setExtendedHabitatInfoFromResultSet(final ResultSet rs, final T habitatInfo, final DBMessagesKey key)
      throws SQLException {
    return setExtendedAgnosticHabitatInfoFromResultSet(QueryAttribute.HABITAT_TYPE_ID, rs, habitatInfo, key);
  }

  protected static <T extends HabitatInfo> T setExtendedAgnosticHabitatInfoFromResultSet(final QueryAttribute habitatTypeAttribute,
      final ResultSet rs, final T habitatInfo, final DBMessagesKey key) throws SQLException {
    setBasicHabitatInfoFromResultSet(habitatTypeAttribute, rs, habitatInfo, key);
    habitatInfo.setAdditionalSurface(HabitatSurfaceType.TOTAL_MAPPED, QueryAttribute.SURFACE.getDouble(rs));
    habitatInfo.setAdditionalSurface(HabitatSurfaceType.TOTAL_CARTOGRAPHIC, QueryUtil.getDouble(rs, QueryAttribute.CARTOGRAPHIC_SURFACE));
    habitatInfo.setAdditionalSurface(HabitatSurfaceType.RELEVANT_MAPPED, QueryUtil.getDouble(rs, QueryAttribute.RELEVANT_SURFACE));
    habitatInfo.setAdditionalSurface(HabitatSurfaceType.RELEVANT_CARTOGRAPHIC, QueryUtil.getDouble(rs, QueryAttribute.RELEVANT_CARTOGRAPHIC_SURFACE));
    habitatInfo.setCoverageRelevant(QueryUtil.getDouble(rs, QueryAttribute.RELEVANT_COVERAGE));
    return habitatInfo;
  }

}
