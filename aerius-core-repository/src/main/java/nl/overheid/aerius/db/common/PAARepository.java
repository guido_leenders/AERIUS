/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.postgis.PGbox2d;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.deposition.PAACalculationPoint;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.info.Country;
import nl.overheid.aerius.shared.domain.info.NatureAreaDirective;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportDepositionInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportHabitatInfo;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * DB Class to get PAA specific objects from the database.
 * @TODO rename to PDFExportRepository.
 */
public final class PAARepository {

  private enum RepositoryAttribute implements Attribute {

    PAS_AREA,
    FOREIGN_AUTHORITY,
    DIRECTIVE,

    RECEPTOR_ID_FOR_MAX_DELTA,
    CURRENT_DEMAND_FOR_MAX_DELTA,
    PROPOSED_DEMAND_FOR_MAX_DELTA,
    MAX_DELTA_DEMAND,
    MAX_BACKGROUND_DEPOSITION,
    MAX_DELTA_DEMAND_ONLY_EXCEEDING,

    COUNTRY_ID,
    COUNTRY_CODE,
    COUNTRY_NAME,

    RULE,
    PASSED,
    WITH_DEMAND,

    PAGE_WIDTH,
    PAGE_HEIGHT;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final String CALCULATION_POINT_RESULTS_WITH_BACKGROUND_DEPOSITIONS_VIEW =
      "SELECT calculation_id, calculation_point_id, label, substance_id, result_type, result, background_deposition,"
          + " ST_X(geometry) as x, St_Y(geometry) as y "
          + "FROM calculation_point_results_with_background_depositions_view "
          + "WHERE calculation_id = ? AND result_type = ?::emission_result_type "
          + "ORDER BY calculation_point_id ";

  private static final Attributes BASIC_DEPOSITION_INFO_ATTRIBUTES = new Attributes(
      RepositoryAttribute.CURRENT_DEMAND_FOR_MAX_DELTA, RepositoryAttribute.PROPOSED_DEMAND_FOR_MAX_DELTA, RepositoryAttribute.MAX_DELTA_DEMAND,
      RepositoryAttribute.MAX_DELTA_DEMAND_ONLY_EXCEEDING);

  private static final Query DETERMINE_ASSESSMENT_AREA_INFO_PDF_EXPORT = QueryBuilder
      .from("full_assessment_areas_pdf_export_view")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.NAME, RepositoryAttribute.PAS_AREA, RepositoryAttribute.COUNTRY_ID,
          RepositoryAttribute.COUNTRY_CODE, RepositoryAttribute.COUNTRY_NAME, RepositoryAttribute.FOREIGN_AUTHORITY, RepositoryAttribute.DIRECTIVE,
          RepositoryAttribute.RECEPTOR_ID_FOR_MAX_DELTA, RepositoryAttribute.MAX_BACKGROUND_DEPOSITION)
      .select(BASIC_DEPOSITION_INFO_ATTRIBUTES).where(QueryAttribute.PROPOSED_CALCULATION_ID).where(QueryAttribute.CURRENT_CALCULATION_ID)
      .select(new SelectClause("Box2D(geometry)", QueryAttribute.BOUNDINGBOX.name()))
      .orderBy(new OrderByClause(RepositoryAttribute.MAX_DELTA_DEMAND, OrderType.DESC),
          new OrderByClause(RepositoryAttribute.PROPOSED_DEMAND_FOR_MAX_DELTA, OrderType.DESC))
      .getQuery();

  private static final Query DETERMINE_HABITATS_INFO_PDF_EXPORT = QueryBuilder.from("full_habitats_pdf_export_view")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.HABITAT_TYPE_ID, QueryAttribute.NAME, QueryAttribute.DESCRIPTION)
      .select(BASIC_DEPOSITION_INFO_ATTRIBUTES)
      .where(QueryAttribute.PROPOSED_CALCULATION_ID).where(QueryAttribute.CURRENT_CALCULATION_ID)
      .orderBy(new OrderByClause(RepositoryAttribute.MAX_DELTA_DEMAND, OrderType.DESC),
          new OrderByClause(RepositoryAttribute.PROPOSED_DEMAND_FOR_MAX_DELTA, OrderType.DESC))
      .getQuery();

  private static final Query DETERMINE_RULE_RESULTS_ASSESSMENT_AREA = QueryBuilder
      .from("ae_development_rule_checks_assessment_areas(?, ?, ?::segment_type, ?)",
          QueryAttribute.PROPOSED_CALCULATION_ID, QueryAttribute.CURRENT_CALCULATION_ID, QueryAttribute.SEGMENT, RepositoryAttribute.WITH_DEMAND)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.RULE, RepositoryAttribute.PASSED)
      .getQuery();

  private static final Query DETERMINE_RULE_RESULTS_HABITAT = QueryBuilder.from("ae_development_rule_checks_habitats(?, ?, ?::segment_type, ?)",
      QueryAttribute.PROPOSED_CALCULATION_ID, QueryAttribute.CURRENT_CALCULATION_ID, QueryAttribute.SEGMENT, RepositoryAttribute.WITH_DEMAND)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.HABITAT_TYPE_ID, RepositoryAttribute.RULE, RepositoryAttribute.PASSED)
      .getQuery();

  private static final Query CALCULATION_DETAIL_PAGES_EXTENTS = QueryBuilder.from("ae_calculation_detail_pages_pdf_export(?, ?, ?, ?)",
      QueryAttribute.PROPOSED_CALCULATION_ID, QueryAttribute.CURRENT_CALCULATION_ID, RepositoryAttribute.PAGE_WIDTH, RepositoryAttribute.PAGE_HEIGHT)
      .select(new SelectClause("Box2D(geometry)", QueryAttribute.BOUNDINGBOX.name())).getQuery();

  // Not allowed to instantiate.
  private PAARepository() {
  }

  /**
   * Get the calculationpoint depositions for the given calculation id.
   * @param connection the DB connection
   * @param calculationId the id of the calculation to get point depositions for
   * @param emissionSourceList the emission source list, this is used to calculate the distance to the closest source.
   * @throws SQLException If an error occurred communicating with the DB.
   * @throws AeriusException If an error occurred while trying to determine the distance to closest source.
   *  Should happen if a bad (WKT) geometry is present inside a source.
   * @return list of {@link PAACalculationPoint}'s
   *
   * @deprecated new PDF export should not use this.
   */
  @Deprecated
  public static ArrayList<PAACalculationPoint> getCalculationPointDepositions(final Connection connection,
      final long calculationId, final EmissionSourceList emissionSourceList) throws SQLException, AeriusException {
    final ArrayList<PAACalculationPoint> result = new ArrayList<>();

    final List<WKTGeometry> emissionSourceGeometries = new ArrayList<>();
    for (final EmissionSource source : emissionSourceList) {
      emissionSourceGeometries.add(source.getGeometry());
    }

    try (final PreparedStatement stmt = connection.prepareStatement(CALCULATION_POINT_RESULTS_WITH_BACKGROUND_DEPOSITIONS_VIEW)) {
      stmt.setInt(1, (int) calculationId);
      stmt.setString(2, EmissionResultType.DEPOSITION.toDatabaseString());
      final ResultSet rst = stmt.executeQuery();
      // calculation_id, calculation_point_id, name, substance_id, deposition,
      // background_deposition, x, y
      PAACalculationPoint latest = null;
      while (rst.next()) {
        final int pointId = rst.getInt("calculation_point_id");
        if (latest == null || latest.getId() != pointId) {
          latest = new PAACalculationPoint();
          latest.setId(pointId);
          latest.setX(rst.getDouble("x"));
          latest.setY(rst.getDouble("y"));
          latest.setLabel(rst.getString("label"));
          latest.setBackgroundDeposition(rst.getDouble("background_deposition"));
          latest.setDistanceToClosestSource(GeometryUtil.minDistance(latest, emissionSourceGeometries));

          result.add(latest);
        }

        final EmissionResultKey key = EmissionResultKey.valueOf(Substance.substanceFromId(rst.getInt("substance_id")), EmissionResultType.DEPOSITION);
        latest.setEmissionResult(key, rst.getDouble("result"));
      }
    }

    return result;
  }

  /**
  * @param con The connection to use.
  * @param proposedCalculationId The proposed calculation ID.
  * @param currentCalculationId The current calculation ID, optional.
  * @param pageWidth Width of each page
  * @param pageHeight Height of each page.
  * @return A list of all boundingboxes to use for detail pages. Can be either portrait or landscape.
  * @throws SQLException In case of exceptions querying the DB.
  */
  public static ArrayList<BBox> getDetailPagesExtents(final Connection con, final int proposedCalculationId,
      final Integer currentCalculationId, final float pageWidth, final float pageHeight) throws SQLException {
    final ArrayList<BBox> result = new ArrayList<>();
    try (PreparedStatement stmt = con.prepareStatement(CALCULATION_DETAIL_PAGES_EXTENTS.get())) {
      CALCULATION_DETAIL_PAGES_EXTENTS.setParameter(stmt, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      CALCULATION_DETAIL_PAGES_EXTENTS.setParameter(stmt, QueryAttribute.CURRENT_CALCULATION_ID, currentCalculationId);
      CALCULATION_DETAIL_PAGES_EXTENTS.setParameter(stmt, RepositoryAttribute.PAGE_WIDTH, pageWidth);
      CALCULATION_DETAIL_PAGES_EXTENTS.setParameter(stmt, RepositoryAttribute.PAGE_HEIGHT, pageHeight);

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final BBox extents = PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs));

        result.add(extents);
      }
    }

    return result;
  }

  public static ArrayList<PDFExportAssessmentAreaInfo> determineExportInfo(final Connection con, final int proposedCalculationId,
      final Integer currentCalculationId, final SegmentType segment, final boolean withDemand) throws SQLException {
    final ReceptorUtil ru = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
    final PDFExportAssessmentAreaCollector collector = new PDFExportAssessmentAreaCollector(ru);

    collectAssessmentAreaInfo(con, proposedCalculationId, currentCalculationId, collector);
    addFailedRulesArea(con, proposedCalculationId, currentCalculationId, segment, withDemand, collector);
    addHabitatInfo(con, proposedCalculationId, currentCalculationId, collector);
    addFailedRulesHabitat(con, proposedCalculationId, currentCalculationId, segment, withDemand, collector);
    return collector.getEntities();
  }

  private static void collectAssessmentAreaInfo(final Connection con, final int proposedCalculationId, final Integer currentCalculationId,
      final PDFExportAssessmentAreaCollector collector) throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(DETERMINE_ASSESSMENT_AREA_INFO_PDF_EXPORT.get())) {
      DETERMINE_ASSESSMENT_AREA_INFO_PDF_EXPORT.setParameter(selectPS, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      DETERMINE_ASSESSMENT_AREA_INFO_PDF_EXPORT.setParameter(selectPS, QueryAttribute.CURRENT_CALCULATION_ID,
          ObjectUtils.defaultIfNull(currentCalculationId, 0));
      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        collector.collectEntity(rs);
      }
    }
  }

  private static void addHabitatInfo(final Connection con, final int proposedCalculationId,
      final Integer currentCalculationId, final PDFExportAssessmentAreaCollector collector) throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(DETERMINE_HABITATS_INFO_PDF_EXPORT.get())) {
      DETERMINE_HABITATS_INFO_PDF_EXPORT.setParameter(selectPS, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      DETERMINE_HABITATS_INFO_PDF_EXPORT.setParameter(selectPS, QueryAttribute.CURRENT_CALCULATION_ID,
          ObjectUtils.defaultIfNull(currentCalculationId, 0));
      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        if (collector.hasEntity(assessmentAreaId)) {
          final PDFExportAssessmentAreaInfo areaInfo = collector.getEntity(assessmentAreaId);

          final PDFExportHabitatInfo habitatInfo = new PDFExportHabitatInfo();
          habitatInfo.setId(QueryAttribute.HABITAT_TYPE_ID.getInt(rs));
          habitatInfo.setCode(QueryAttribute.NAME.getString(rs));
          habitatInfo.setName(QueryAttribute.DESCRIPTION.getString(rs));
          habitatInfo.setDepositionInfo(getBasicDepositionInfo(rs));

          areaInfo.getHabitatInfos().add(habitatInfo);
        }
      }
    }
  }

  private static void addFailedRulesArea(final Connection con, final int proposedCalculationId,
      final Integer currentCalculationId, final SegmentType segment, final boolean withDemand, final PDFExportAssessmentAreaCollector collector)
      throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.get())) {
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, QueryAttribute.CURRENT_CALCULATION_ID, currentCalculationId);
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, QueryAttribute.SEGMENT, segment.getDbValue());
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, RepositoryAttribute.WITH_DEMAND, withDemand);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        final boolean passed = QueryUtil.getBoolean(rs, RepositoryAttribute.PASSED);
        final DevelopmentRule rule = QueryUtil.getEnum(rs, RepositoryAttribute.RULE, DevelopmentRule.class);
        if (rule != null && collector.hasEntity(assessmentAreaId)) {
          addHabitatRuleResult(collector.getEntity(assessmentAreaId).getShowRules(), rule, passed);
        }
      }
    }
  }

  private static void addFailedRulesHabitat(final Connection con, final int proposedCalculationId,
      final Integer currentCalculationId, final SegmentType segment, final boolean withDemand, final PDFExportAssessmentAreaCollector collector)
      throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(DETERMINE_RULE_RESULTS_HABITAT.get())) {
      DETERMINE_RULE_RESULTS_HABITAT.setParameter(selectPS, QueryAttribute.PROPOSED_CALCULATION_ID, proposedCalculationId);
      DETERMINE_RULE_RESULTS_HABITAT.setParameter(selectPS, QueryAttribute.CURRENT_CALCULATION_ID, currentCalculationId);
      DETERMINE_RULE_RESULTS_HABITAT.setParameter(selectPS, QueryAttribute.SEGMENT, segment.getDbValue());
      DETERMINE_RULE_RESULTS_HABITAT.setParameter(selectPS, RepositoryAttribute.WITH_DEMAND, withDemand);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        final int habitatTypeId = QueryAttribute.HABITAT_TYPE_ID.getInt(rs);
        final boolean passed = QueryUtil.getBoolean(rs, RepositoryAttribute.PASSED);
        final DevelopmentRule rule = QueryUtil.getEnum(rs, RepositoryAttribute.RULE, DevelopmentRule.class);
        if (rule != null && collector.hasEntity(assessmentAreaId)) {
          PDFExportHabitatInfo habitatInfo = null;
          for (final PDFExportHabitatInfo info : collector.getEntity(assessmentAreaId).getHabitatInfos()) {
            if (info.getId() == habitatTypeId) {
              habitatInfo = info;
            }
          }
          if (habitatInfo != null) {
            addHabitatRuleResult(habitatInfo.getShowRules(), rule, passed);
          }
        }
      }
    }
  }

  private static void addHabitatRuleResult(final HashSet<DevelopmentRule> rules, final DevelopmentRule rule, final boolean passed) {
    if (!passed) {
      rules.add(rule);
    }

    if (rule == DevelopmentRule.EXCEEDING_SPACE_CHECK && passed) {
      // The NOT_EXCEEDING_SPACE_CHECK needs to be used when EXCEEDING_SPACE_CHECK has passed.
      rules.add(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK);
    }
  }

  private static PDFExportDepositionInfo getBasicDepositionInfo(final ResultSet rs) throws SQLException {
    final PDFExportDepositionInfo info = new PDFExportDepositionInfo();
    info.setCurrentDemandMaxDelta(QueryUtil.getDouble(rs, RepositoryAttribute.CURRENT_DEMAND_FOR_MAX_DELTA));
    info.setProposedDemandMaxDelta(QueryUtil.getDouble(rs, RepositoryAttribute.PROPOSED_DEMAND_FOR_MAX_DELTA));
    info.setMaxDeltaDemand(QueryUtil.getDouble(rs, RepositoryAttribute.MAX_DELTA_DEMAND));
    info.setMaxDeltaDemandOnlyExceeding(QueryUtil.getDouble(rs, RepositoryAttribute.MAX_DELTA_DEMAND_ONLY_EXCEEDING));
    return info;
  }

  private static class PDFExportAssessmentAreaCollector extends EntityCollector<PDFExportAssessmentAreaInfo> {

    private final ReceptorUtil receptorUtil;

    public PDFExportAssessmentAreaCollector(final ReceptorUtil receptorUtil) {
      this.receptorUtil = receptorUtil;
    }

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
    }

    @Override
    public PDFExportAssessmentAreaInfo fillEntity(final ResultSet rs) throws SQLException {
      final PDFExportAssessmentAreaInfo areaInfo = new PDFExportAssessmentAreaInfo();
      areaInfo.setName(QueryAttribute.NAME.getString(rs));
      areaInfo.setBounds(PGisUtils.getBox((PGbox2d) QueryAttribute.BOUNDINGBOX.getObject(rs)));
      areaInfo.setPasArea(QueryUtil.getBoolean(rs, RepositoryAttribute.PAS_AREA));
      areaInfo.setForeignAuthority(QueryUtil.getBoolean(rs, RepositoryAttribute.FOREIGN_AUTHORITY));
      areaInfo.setDirective(NatureAreaDirective.getFromDB(QueryUtil.getString(rs, RepositoryAttribute.DIRECTIVE)));

      final AeriusPoint point = new AeriusPoint(QueryUtil.getInt(rs, RepositoryAttribute.RECEPTOR_ID_FOR_MAX_DELTA));
      receptorUtil.setAeriusPointFromId(point);
      areaInfo.setMaxDeltaPoint(point);

      areaInfo.setDepositionInfo(getBasicDepositionInfo(rs));
      areaInfo.getDepositionInfo().setMaxBackgroundDeposition(QueryUtil.getDouble(rs, RepositoryAttribute.MAX_BACKGROUND_DEPOSITION));

      final Country country = new Country();
      country.setId(QueryUtil.getInt(rs, RepositoryAttribute.COUNTRY_ID));
      country.setCode(QueryUtil.getString(rs, RepositoryAttribute.COUNTRY_CODE));
      country.setName(QueryUtil.getString(rs, RepositoryAttribute.COUNTRY_NAME));
      areaInfo.setCountry(country);

      return areaInfo;
    }

  }

}
