/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadSpeedType;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;

/**
 * Service class for Road Emission factors table.
 */
public final class RoadEmissionCategoryRepository {

  private enum RepositoryAttribute implements Attribute {

    ROAD_TYPE,
    VEHICLE_TYPE,
    MAXIMUM_SPEED,
    SPEED_LIMIT_ENFORCEMENT,
    STAGNATED_EMISSION_FACTOR,
    ROAD_SPEED_PROFILE_NAME;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final String STRICT_SPEED_LIMIT_ENFORCEMENT = "strict";
  private static final String IRRELEVANT_SPEED_LIMIT_ENFORCEMENT = "irrelevant";
  private static final String SUBSTANCE_WHERE = "substance_id IN ("
      + Stream.of(Substance.EMISSION_SUBSTANCES).map(Substance::getId).map(String::valueOf).collect(Collectors.joining(","))
      + ")";

  /**
   * This view works on road_category_emission_factors and interpolates all missing years.
   * Only query for PM10, NO2, NOx, and NH3 because those are only supported.
   */
  private static final Query INTERPOLATED_ROAD_EMISSION_FACTORS_QUERY = QueryBuilder.from("road_categories_view")
      .select(QueryAttribute.YEAR, QueryAttribute.SUBSTANCE_ID, RepositoryAttribute.ROAD_TYPE,
          RepositoryAttribute.VEHICLE_TYPE, RepositoryAttribute.MAXIMUM_SPEED, RepositoryAttribute.SPEED_LIMIT_ENFORCEMENT,
          QueryAttribute.EMISSION_FACTOR, RepositoryAttribute.STAGNATED_EMISSION_FACTOR, RepositoryAttribute.ROAD_SPEED_PROFILE_NAME)
      .where(new StaticWhereClause(SUBSTANCE_WHERE)).getQuery();

  private RoadEmissionCategoryRepository() {
  }

  /**
   * Returns all Road Emission Categories and interpolates the years in between.
   *
   * @param con Database connection
   * @return A new RoadEmissionCategories object filled with all categories.
   * @throws SQLException throws SQLException in case an SQL error occurred.
   */
  public static RoadEmissionCategories findAllRoadEmissionCategories(
      final Connection con) throws SQLException {
    final RoadEmissionCategories categories = new RoadEmissionCategories();

    try (final PreparedStatement statement = con.prepareStatement(INTERPOLATED_ROAD_EMISSION_FACTORS_QUERY.get())) {
      final ResultSet rs = statement.executeQuery();

      while (rs.next()) {
        final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
        final EmissionValueKey evk = new EmissionValueKey(QueryAttribute.YEAR.getInt(rs), substance);
        final RoadType roadType = QueryUtil.getEnum(rs, RepositoryAttribute.ROAD_TYPE, RoadType.class);
        final VehicleType vehicleType = QueryUtil.getEnum(rs, RepositoryAttribute.VEHICLE_TYPE, VehicleType.class);
        final String speedLimitEnforcement = QueryUtil.getString(rs, RepositoryAttribute.SPEED_LIMIT_ENFORCEMENT);
        final boolean strictSLE = STRICT_SPEED_LIMIT_ENFORCEMENT.equals(speedLimitEnforcement);
        final boolean irrelevantSLE = IRRELEVANT_SPEED_LIMIT_ENFORCEMENT.equals(speedLimitEnforcement);
        // if speed is irrelevant store it as 0.
        final int maximumSpeed = irrelevantSLE ? 0 : QueryUtil.getInt(rs, RepositoryAttribute.MAXIMUM_SPEED);
        final RoadSpeedType speedType =
            roadSpeedProfileName2SpeedType(QueryUtil.getString(rs, RepositoryAttribute.ROAD_SPEED_PROFILE_NAME), roadType);

        final RoadEmissionCategory newCategory = new RoadEmissionCategory(roadType, vehicleType, strictSLE, maximumSpeed, speedType);
        RoadEmissionCategory category = categories.getRoadEmissionCategory(newCategory);

        if (category == null) {
          category = newCategory;
          categories.add(category);
        }

        category.setEmissionFactor(evk, QueryAttribute.EMISSION_FACTOR.getDouble(rs));
        category.setStagnatedEmissionFactor(evk, QueryUtil.getDouble(rs, RepositoryAttribute.STAGNATED_EMISSION_FACTOR));
      }
    }

    return categories;
  }

  private static RoadSpeedType roadSpeedProfileName2SpeedType(final String roadSpeedProfileName, final RoadType roadType) {
    if (roadType == RoadType.FREEWAY) {
      return null; // This speed type is purely for non freeways. So we can skip right here for freeways.
    }
    final RoadSpeedType speedType;

    switch (roadSpeedProfileName) {
    case "Buitenweg algemeen":
      speedType = RoadSpeedType.B;
      break;
    case "Normaal":
      speedType = RoadSpeedType.C;
      break;
    case "Stagnerend":
      speedType = RoadSpeedType.D;
      break;
    case "Doorstromend":
      speedType = RoadSpeedType.E;
      break;
    default:
      speedType = null;
      break;
    }
    return speedType;
  }
}
