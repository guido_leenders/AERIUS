/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgis.LineString;
import org.postgis.PGgeometry;
import org.postgis.Polygon;

import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.Point;

/**
 * DB Class to convert objects from one to the other using the database.
 */
public final class ConversionRepository {

  private static final String CONVERT_LINE_TO_POINTS =
      "SELECT ae_line_to_point_sources(?, ?)";

  private static final String CONVERT_POLYGON_TO_POINTS =
      "SELECT ST_X(point), "
          + " ST_Y(point), "
          + " weight "
          + " FROM ae_polygon_to_point_sources(?,"
          + " (SELECT value FROM system.constants WHERE key = '"
          + ConstantsEnum.CONVERT_POLYGON_TO_POINTS_GRID_SIZE + "')::double precision)";

  private ConversionRepository() {
  }

  /**
   * Convert a Line (or LineString) into a list of points that represent the line.
   * Each point represents an equal part of the line.
   * @param connection The database connection.
   * @param line The line to convert.
   * @param maxSegmentSize The maximum size of the segments that should be returned.
   * @return The converted points that represent the line.
   * @throws SQLException When a database exception occurs.
   */
  public static List<Point> convertToPoints(final Connection connection, final LineString line, final double maxSegmentSize) throws SQLException {
    final List<Point> points = new ArrayList<>();
    try (final PreparedStatement convertPS = connection.prepareStatement(CONVERT_LINE_TO_POINTS)) {
      convertPS.setObject(1, new PGgeometry(line));
      convertPS.setDouble(2, maxSegmentSize);

      final ResultSet rs = convertPS.executeQuery();
      while (rs.next()) {
        // convert result to point.
        points.add(PGisUtils.getPoint((PGgeometry) rs.getObject(1)));
      }
    }
    return points;
  }

  /**
   * Convert a Polygon into a list of points that represent the polygon plane.
   * Each point represents a point in the polygon.
   * @param connection The database connection.
   * @param polygon The polygon to convert.
   * @return The converted points that represent the polygon.
   * @throws SQLException When a database exception occurs.
   */
  public static List<WeightedPoint> convertToPoints(final Connection connection, final Polygon polygon) throws SQLException {
    final List<WeightedPoint> points = new ArrayList<>();
    try (final PreparedStatement convertPS = connection.prepareStatement(CONVERT_POLYGON_TO_POINTS)) {
      convertPS.setObject(1, new PGgeometry(polygon));

      final ResultSet rs = convertPS.executeQuery();
      while (rs.next()) {
        // convert result to point.
        points.add(new WeightedPoint(rs.getDouble(1), rs.getDouble(2), rs.getDouble(3)));
      }
    }
    return points;
  }

}
