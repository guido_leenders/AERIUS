/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.PAARepository;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportHabitatInfo;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;

/**
 *
 */
public final class PriorityProjectPDFRepository {

  private enum RepositoryAttribute implements Attribute {

    WITH_DEMAND,
    PASSED;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final Query DETERMINE_RULE_RESULTS_ASSESSMENT_AREA = QueryBuilder
      .from("ae_priority_subproject_checks_assessment_areas(?, ?)",
          RegisterAttribute.REQUEST_ID, RepositoryAttribute.WITH_DEMAND)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.PASSED)
      .getQuery();

  private static final Query DETERMINE_RULE_RESULTS_HABITAT = QueryBuilder
      .from("ae_priority_subproject_checks_habitats(?, ?)",
          RegisterAttribute.REQUEST_ID, RepositoryAttribute.WITH_DEMAND)
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.HABITAT_TYPE_ID, RepositoryAttribute.PASSED)
      .getQuery();

  private PriorityProjectPDFRepository() {
    //repository class
  }

  public static ArrayList<PDFExportAssessmentAreaInfo> determineExportInfo(final PMF pmf, final PrioritySubProjectKey key,
      final boolean withDemand) throws SQLException {
    final ArrayList<PDFExportAssessmentAreaInfo> areaInfos;
    try (final Connection con = pmf.getConnection()) {
      final Map<SituationType, Integer> calculationIds = RequestRepository.getCalculationIds(con, key.getReference());
      areaInfos = PAARepository.determineExportInfo(con, calculationIds.get(SituationType.PROPOSED), calculationIds.get(SituationType.CURRENT),
          SegmentType.PRIORITY_PROJECTS, withDemand);
      fillInfoWithSubprojectResults(con, key, areaInfos, withDemand);
    }
    return areaInfos;
  }

  private static void fillInfoWithSubprojectResults(final Connection con, final PrioritySubProjectKey key,
      final ArrayList<PDFExportAssessmentAreaInfo> pdfExportInfo, final boolean withDemand) throws SQLException {
    final int subProjectId = PrioritySubProjectRepository.getPrioritySubProjectIdByKey(con, key);
    final Map<Integer, PDFExportAssessmentAreaInfo> areaInfoMap = new HashMap<>();
    for (final PDFExportAssessmentAreaInfo areaInfo : pdfExportInfo) {
      areaInfoMap.put(areaInfo.getId(), areaInfo);
      areaInfo.getShowRules().clear();
      for (final PDFExportHabitatInfo habitatInfo : areaInfo.getHabitatInfos()) {
        habitatInfo.getShowRules().clear();
      }
    }

    addFailedRulesArea(con, subProjectId, areaInfoMap, withDemand);
    addFailedRulesHabitat(con, subProjectId, areaInfoMap, withDemand);
  }

  private static void addFailedRulesArea(final Connection con, final int subProjectId,
      final Map<Integer, PDFExportAssessmentAreaInfo> pdfExportInfo, final boolean withDemand) throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.get())) {
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, RegisterAttribute.REQUEST_ID, subProjectId);
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, RepositoryAttribute.WITH_DEMAND, withDemand);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        final boolean passed = QueryUtil.getBoolean(rs, RepositoryAttribute.PASSED);
        final PDFExportAssessmentAreaInfo areaInfo = pdfExportInfo.get(assessmentAreaId);
        if (areaInfo != null) {
          addRuleResult(areaInfo.getShowRules(), passed);
        }
      }
    }
  }

  private static void addFailedRulesHabitat(final Connection con, final int subProjectId,
      final Map<Integer, PDFExportAssessmentAreaInfo> pdfExportInfo, final boolean withDemand)
      throws SQLException {
    try (final PreparedStatement selectPS = con.prepareStatement(DETERMINE_RULE_RESULTS_HABITAT.get())) {
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, RegisterAttribute.REQUEST_ID, subProjectId);
      DETERMINE_RULE_RESULTS_ASSESSMENT_AREA.setParameter(selectPS, RepositoryAttribute.WITH_DEMAND, withDemand);

      final ResultSet rs = selectPS.executeQuery();
      while (rs.next()) {
        final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        final int habitatTypeId = QueryAttribute.HABITAT_TYPE_ID.getInt(rs);
        final boolean passed = QueryUtil.getBoolean(rs, RepositoryAttribute.PASSED);
        final PDFExportAssessmentAreaInfo areaInfo = pdfExportInfo.get(assessmentAreaId);
        if (areaInfo != null) {
          PDFExportHabitatInfo habitatInfo = null;
          for (final PDFExportHabitatInfo info : areaInfo.getHabitatInfos()) {
            if (info.getId() == habitatTypeId) {
              habitatInfo = info;
            }
          }
          if (habitatInfo != null) {
            addRuleResult(habitatInfo.getShowRules(), passed);
          }
        }
      }
    }
  }

  private static void addRuleResult(final HashSet<DevelopmentRule> rules, final boolean passed) {
    if (passed) {
      rules.add(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK);
    } else {
      rules.add(DevelopmentRule.EXCEEDING_SPACE_CHECK);
    }
  }

}
