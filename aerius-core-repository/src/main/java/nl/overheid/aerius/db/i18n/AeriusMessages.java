/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * AERIUS Resource bundle for a specific locale.
 */
public class AeriusMessages extends ResourceBundle implements Serializable {
  private static final long serialVersionUID = -8774995500364222828L;
  private static final String AERIUS_MESSAGES = AeriusMessages.class.getName();

  public AeriusMessages(final Locale locale) {
    setLocale(locale);
  }

  @Override
  protected Object handleGetObject(final String key) {
    return parent.getObject(key);
  }

  @Override
  public Enumeration<String> getKeys() {
    return parent.getKeys();
  }

  private void setLocale(final Locale locale) {
    if (parent == null || !parent.getLocale().equals(locale)) {
      setParent(getBundle(AERIUS_MESSAGES, locale));
    }
  }
}
