/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.SelectClause;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.info.CriticalDepositionAreaType;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * DB class to retrieve information about calculations.
 */
public final class CalculationInfoRepository {

  private enum RepositoryAttribute implements Attribute {

    MIN_X,
    MIN_Y,
    MAX_X,
    MAX_Y,

    CALCULATION_DEPOSITION_RECEPTOR_ID,
    CALCULATION_DEPOSITION,
    TOTAL_DEPOSITION_RECEPTOR_ID,
    TOTAL_DEPOSITION,
    DEVELOPMENT_SPACE_PERCENTAGE_RECEPTOR_ID,
    DEVELOPMENT_SPACE_PERCENTAGE,

    SUM_DEPOSITION,
    MAX_DEPOSITION,
    AVG_DEPOSITION,
    PERCENTAGE_CRITICAL_DEPOSITION,
    CALCULATION_SUBSTANCE,

    BASE_CALCULATION_ID,
    VARIANT_CALCULATION_ID,
    TOTAL_DEPOSITION_DIFF,
    MAX_DEPOSITION_DIFF,
    AVG_DEPOSITION_DIFF;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final WhereClause WHERE_CRITICAL_DEPOSITION_AREA_TYPE =
      new StaticWhereClause(QueryAttribute.TYPE.name() + " = ?::critical_deposition_area_type", QueryAttribute.TYPE);

  private static final Query GET_RECEPTOR_EMISSION_RESULTS =
      getQueryResultBase("calculation_results_view")
          .where(QueryAttribute.RECEPTOR_ID).getQuery();

  private static final Query GET_ALL_RECEPTOR_RESULTS =
      getAllReceptorResultsBase("calculation_results_view")
          .getQuery();

  private static final Query GET_ALL_SECTOR_RECEPTOR_RESULTS =
      getAllReceptorResultsBase("calculation_sector_results_view")
          .where(QueryAttribute.SECTOR_ID)
          .getQuery();

  private static final Query GET_ALL_CALCULATION_POINT_RESULTS =
      getQueryCalculationPointResultsBase("calculation_point_results_view")
          .getQuery();

  private static final Query GET_ALL_CALCULATION_SECTOR_POINT_RESULTS =
      getQueryCalculationPointResultsBase("calculation_point_sector_results_view")
          .where(QueryAttribute.SECTOR_ID)
          .getQuery();

  private static final Query DETERMINE_BOUNDING_BOX_RESULTS =
      QueryBuilder
          .from("ae_calculation_results_bounding_box(?,?)", QueryAttribute.CALCULATION_ID, QueryAttribute.ZOOM_LEVEL)
          .select(RepositoryAttribute.MIN_X, RepositoryAttribute.MIN_Y, RepositoryAttribute.MAX_X, RepositoryAttribute.MAX_Y)
          .getQuery();

  private static final Query DETERMINE_DEPOSITION_MARKERS =
      QueryBuilder
          .from("calculation_markers_for_assessment_area_view")
          .select(QueryAttribute.ASSESSMENT_AREA_ID,
              RepositoryAttribute.CALCULATION_DEPOSITION_RECEPTOR_ID, RepositoryAttribute.CALCULATION_DEPOSITION,
              RepositoryAttribute.TOTAL_DEPOSITION_RECEPTOR_ID, RepositoryAttribute.TOTAL_DEPOSITION,
              RepositoryAttribute.DEVELOPMENT_SPACE_PERCENTAGE_RECEPTOR_ID, RepositoryAttribute.DEVELOPMENT_SPACE_PERCENTAGE)
          .where(QueryAttribute.CALCULATION_ID)
          .getQuery();

  private static final Query GET_CALC_ASSESSMENT_ALL_HABITAT_RECEPTORS =
      getQueryCalculationAssessmentHabitatReceptors()
          .getQuery();

  private static final Query GET_CALC_ASSESSMENT_HABITAT_RECEPTORS =
      getQueryCalculationAssessmentHabitatReceptors()
          .where(QueryAttribute.CRITICAL_DEPOSITION_AREA_ID)
          .getQuery();

  private CalculationInfoRepository() {
    //don't instantiate.
  }

  /**
   * Retrieve the "total" results for a certain calculation.
   * More results can still be added after query.
   * To be sure all results are in, compare the size of the result to the expected value.
   * Other way to be sure would be to check the state of a calculation,
   * That does depend on the state actually being set to COMPLETE when all is done however.
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @return The (current) PartialCalculationResult that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   * @TODO move to {@link CalculationRepository}
   */
  public static PartialCalculationResult getCalculationResults(final Connection connection, final int calculationId) throws SQLException {
    final PartialCalculationResult calculationResult = new PartialCalculationResult();
    calculationResult.getResults().addAll(getReceptors(connection, calculationId, null));
    calculationResult.getResults().addAll(getCalculationPoints(connection, calculationId, null));
    return calculationResult;
  }

  /**
   * The same as {@link #getCalculationResults}, but returns the "sector" results for the given
   * sector (instead of the "total" results).
   *
   * @param connection The connection to use
   * @param calculationId The ID of the calculation to retrieve results for
   * @param sectorId The ID of the sector to retrieve results for
   * @return The (current) PartialCalculationResult that match this calculation ID
   * @throws SQLException If an error occurred communicating with the DB
   * @TODO move to {@link CalculationRepository}
   */
  public static PartialCalculationResult getCalculationSectorResults(final Connection connection, final int calculationId, final int sectorId)
      throws SQLException {
    final PartialCalculationResult calculationResult = new PartialCalculationResult();
    calculationResult.getResults().addAll(getReceptors(connection, calculationId, sectorId));
    calculationResult.getResults().addAll(getCalculationPoints(connection, calculationId, sectorId));
    return calculationResult;
  }

  private static ArrayList<AeriusResultPoint> getReceptors(final Connection connection, final int calculationId, final Integer sectorId)
      throws SQLException {
    final ArrayList<AeriusResultPoint> receptors = new ArrayList<AeriusResultPoint>();
    final Query query = sectorId == null ? GET_ALL_RECEPTOR_RESULTS : GET_ALL_SECTOR_RECEPTOR_RESULTS;

    final ReceptorUtil receptorUtil = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(connection));
    try (final PreparedStatement ps = connection.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);
      if (sectorId != null) {
        query.setParameter(ps, QueryAttribute.SECTOR_ID, sectorId);
      }

      final HashMap<Integer, AeriusResultPoint> hashedResults = new HashMap<>();
      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          final int receptorId = QueryAttribute.RECEPTOR_ID.getInt(rs);
          final AeriusResultPoint receptorPoint = new AeriusResultPoint(receptorId);
          receptorUtil.setAeriusPointFromId(receptorPoint);

          EmissionResultRepositoryUtil.addResultsToPoint(rs, hashedResults, receptorPoint);
        }
      }
      receptors.addAll(hashedResults.values());
    }

    return receptors;
  }

  private static ArrayList<AeriusResultPoint> getCalculationPoints(final Connection connection, final int calculationId, final Integer sectorId)
      throws SQLException {
    final ArrayList<AeriusResultPoint> points = new ArrayList<AeriusResultPoint>();
    final Query query = (sectorId == null) ? GET_ALL_CALCULATION_POINT_RESULTS : GET_ALL_CALCULATION_SECTOR_POINT_RESULTS;

    try (final PreparedStatement ps = connection.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);
      if (sectorId != null) {
        query.setParameter(ps, QueryAttribute.SECTOR_ID, sectorId);
      }

      final HashMap<Integer, AeriusResultPoint> hashedResults = new HashMap<>();
      try (final ResultSet rs = ps.executeQuery()) {

        while (rs.next()) {
          final int calculationPointId = QueryAttribute.CALCULATION_POINT_ID.getInt(rs);
          final AeriusResultPoint calculationPoint = new AeriusResultPoint(calculationPointId);
          calculationPoint.setX(QueryUtil.getDouble(rs, QueryAttribute.X_COORD));
          calculationPoint.setY(QueryUtil.getDouble(rs, QueryAttribute.Y_COORD));
          calculationPoint.setLabel(QueryAttribute.LABEL.getString(rs));
          calculationPoint.setPointType(AeriusPointType.POINT);

          EmissionResultRepositoryUtil.addResultsToPoint(rs, hashedResults, calculationPoint);
        }
      }
      points.addAll(hashedResults.values());
    }

    return points;
  }

  /**
   * @param connection The connection to use.
   * @param calculationId The calculation ID to retrieve results for.
   * @param receptorId The receptor to retrieve results for.
   * @return The emission results for the specified receptor.
   * @throws SQLException In case of database errors.
   */
  public static EmissionResults getEmissionResults(final Connection connection, final int calculationId, final int receptorId)
      throws SQLException {
    final EmissionResults emissionResults = new EmissionResults();
    try (final PreparedStatement statement = connection.prepareStatement(GET_RECEPTOR_EMISSION_RESULTS.get())) {
      GET_RECEPTOR_EMISSION_RESULTS.setParameter(statement, QueryAttribute.CALCULATION_ID, calculationId);
      GET_RECEPTOR_EMISSION_RESULTS.setParameter(statement, QueryAttribute.RECEPTOR_ID, receptorId);

      try (final ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          EmissionResultRepositoryUtil.addEmissionResult(rs, emissionResults);
        }
      }
    }
    return emissionResults;
  }

  /**
   * Returns the bounding box of the results for the given calculation.
   *
   * @param connection The connection to use
   * @param calculationId id of the calculation
   * @return the bounding box of the calculation results.
   * @throws SQLException If an error occurred communicating with the DB or when any of the ID's are unknown.
   */
  public static BBox determineBoundingBox(final Connection connection, final int calculationId) throws SQLException {
    BBox result = null;
    try (final PreparedStatement selectPS = connection.prepareStatement(DETERMINE_BOUNDING_BOX_RESULTS.get())) {
      DETERMINE_BOUNDING_BOX_RESULTS.setParameter(selectPS, QueryAttribute.CALCULATION_ID, calculationId);
      //For now use zoom level 1, but might be possible to determine the right level in the view.
      DETERMINE_BOUNDING_BOX_RESULTS.setParameter(selectPS, QueryAttribute.ZOOM_LEVEL, 1);

      try (final ResultSet rs = selectPS.executeQuery()) {
        if (rs.next()) {
          result = new BBox(QueryUtil.getDouble(rs, RepositoryAttribute.MIN_X), QueryUtil.getDouble(rs, RepositoryAttribute.MIN_Y),
              QueryUtil.getDouble(rs, RepositoryAttribute.MAX_X), QueryUtil.getDouble(rs, RepositoryAttribute.MAX_Y));
        }
      }
    }
    return result;
  }

  /**
   * Determine the deposition markers for a calculation/substance combination.
   * These markers are only available for nature areas.
   * @param connection The connection to use.
   * @param calculationId The ID of the calculation to determine the markers for.
   * @return The map with receptor ID's as key and the marker as value.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static ArrayList<DepositionMarker> determineMarkers(final Connection connection, final int calculationId) throws SQLException {
    final ArrayList<DepositionMarker> result = new ArrayList<>();
    final ReceptorUtil receptorUtil = new ReceptorUtil(ReceptorGridSettingsRepository.getReceptorGridSettings(connection));

    try (final PreparedStatement selectPS = connection.prepareStatement(DETERMINE_DEPOSITION_MARKERS.get())) {
      DETERMINE_DEPOSITION_MARKERS.setParameter(selectPS, QueryAttribute.CALCULATION_ID, calculationId);

      try (final ResultSet rs = selectPS.executeQuery()) {
        while (rs.next()) {
          final int assessmentAreaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
          final int receptorIdHighestCalculatedDeposition = QueryUtil.getInt(rs, RepositoryAttribute.CALCULATION_DEPOSITION_RECEPTOR_ID);
          final int receptorIdHighestTotalDeposition = QueryUtil.getInt(rs, RepositoryAttribute.TOTAL_DEPOSITION_RECEPTOR_ID);
          final double highestCalculatedDeposition = QueryUtil.getDouble(rs, RepositoryAttribute.CALCULATION_DEPOSITION);
          final double highestTotalDeposition = QueryUtil.getDouble(rs, RepositoryAttribute.TOTAL_DEPOSITION);
          //there's always a receptor with highest deposition if there is a result.
          final DepositionMarker markerHighestDeposition = getMarker(receptorUtil, result, receptorIdHighestCalculatedDeposition,
              assessmentAreaId);
          markerHighestDeposition
              .combine(DepositionMarkerType.HIGHEST_CALCULATED_DEPOSITION, highestCalculatedDeposition);
          //highest total deposition marker is only shown when the KDW is exceeded.
          //query returns null for total_deposition if the KDW isn't exceeded, which translates to 0.0 with the getDouble.
          if (highestTotalDeposition != 0.0) {
            final DepositionMarker markerHighestTotalDeposition = getMarker(receptorUtil, result, receptorIdHighestTotalDeposition,
                assessmentAreaId);
            markerHighestTotalDeposition.combine(DepositionMarkerType.HIGHEST_TOTAL_DEPOSITION, highestTotalDeposition);
          }
        }
      }
    }
    return result;
  }

  /**
   * @param con The connection to use.
   * @param calculationId The calculation ID to determine the calculated receptors for.
   * @param assessmentAreaId The assessment area ID to determine the calculated receptors for.
   * @param habitatId The habitat ID to determine the calculated receptors for. Can be 0 for all habitats.
   * @return The map of receptor IDs/surface for habitat that were calculated for the supplied combination.
   * @throws SQLException In case of database errors.
   */
  public static HashMap<Integer, Double> getCalculationAssessmentHabitatReceptors(final Connection con,
      final int calculationId, final int assessmentAreaId, final int habitatId) throws SQLException {
    final HashMap<Integer, Double> map = new HashMap<>();

    final Query query = habitatId == 0 ? GET_CALC_ASSESSMENT_ALL_HABITAT_RECEPTORS : GET_CALC_ASSESSMENT_HABITAT_RECEPTORS;

    try (final PreparedStatement ps = con.prepareStatement(query.get())) {
      query.setParameter(ps, QueryAttribute.CALCULATION_ID, calculationId);
      query.setParameter(ps, QueryAttribute.ASSESSMENT_AREA_ID, assessmentAreaId);
      query.setParameter(ps, QueryAttribute.CRITICAL_DEPOSITION_AREA_ID, habitatId);
      query.setParameter(ps, QueryAttribute.TYPE, CriticalDepositionAreaType.RELEVANT_HABITAT.name().toLowerCase());

      try (final ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          map.put(QueryAttribute.RECEPTOR_ID.getInt(rs), QueryAttribute.SURFACE.getDouble(rs));
        }
      }
    }

    return map;
  }

  private static DepositionMarker getMarker(final ReceptorUtil receptorUtil, final ArrayList<DepositionMarker> existingMarkers, final int receptorId,
      final int assessmentAreaId) {
    final AeriusPoint point = new AeriusPoint(receptorId);
    receptorUtil.setAeriusPointFromId(point);
    DepositionMarker marker = null;
    for (final DepositionMarker existingMarker : existingMarkers) {
      if (existingMarker.getReceptorId() == receptorId && existingMarker.getAssessmentAreaId() == assessmentAreaId) {
        marker = existingMarker;
        break;
      }
    }
    if (marker == null) {
      marker = new DepositionMarker();
      marker.setReceptorId(receptorId);
      marker.setAssessmentAreaId(assessmentAreaId);
      marker.setX(point.getX());
      marker.setY(point.getY());
      //ensure the marker is put in the list of existing markers.
      existingMarkers.add(marker);
    }
    return marker;
  }

  private static QueryBuilder getQueryResultBase(final String fromView) {
    return QueryBuilder.from(fromView)
        .select(EmissionResultRepositoryUtil.REQUIRED_ATTRIBUTES)
        .where(QueryAttribute.CALCULATION_ID);
  }

  private static QueryBuilder getAllReceptorResultsBase(final String fromView) {
    return getQueryResultBase(fromView)
        .select(QueryAttribute.RECEPTOR_ID);
  }

  private static QueryBuilder getQueryCalculationPointResultsBase(final String fromView) {
    return getQueryResultBase(fromView)
        .select(QueryAttribute.CALCULATION_POINT_ID, QueryAttribute.LABEL)
        .select(new SelectClause("ST_X(geometry)", QueryAttribute.X_COORD.attribute()),
            new SelectClause("ST_Y(geometry)", QueryAttribute.Y_COORD.attribute()));
  }

  private static QueryBuilder getQueryCalculationAssessmentHabitatReceptors() {
    return QueryBuilder.from("receptors_to_calculation_critical_deposition_areas_view")
        .select(QueryAttribute.RECEPTOR_ID, QueryAttribute.SURFACE)
        .where(QueryAttribute.CALCULATION_ID, QueryAttribute.ASSESSMENT_AREA_ID)
        .where(WHERE_CRITICAL_DEPOSITION_AREA_TYPE);
  }
}
