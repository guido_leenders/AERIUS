/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.db.util.WhereClause;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.CriticalDepositionAreaType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Repository class to collect summary results of a calculation.
 */
public final class CalculationSummaryRepository {
  private enum RepositoryAttribute implements Attribute {
    CALCULATION_SUBSTANCE,
    SUM_DEPOSITION,
    MAX_DEPOSITION,
    AVG_DEPOSITION,
    PERCENTAGE_CRITICAL_DEPOSITION,

    BASE_CALCULATION_ID,
    VARIANT_CALCULATION_ID,
    TOTAL_DEPOSITION_DIFF,
    MAX_DEPOSITION_DIFF,
    AVG_DEPOSITION_DIFF;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private static final WhereClause WHERE_CRITICAL_DEPOSITION_AREA_TYPE =
      new StaticWhereClause(QueryAttribute.TYPE.name() + " = ?::critical_deposition_area_type", QueryAttribute.TYPE);

  private static final Query DETERMINE_HABITAT_DEPOSITIONS =
      getQueryHabitatDepositionBase("critical_deposition_area_calculation_results_view")
      .select(RepositoryAttribute.SUM_DEPOSITION, RepositoryAttribute.MAX_DEPOSITION,
          RepositoryAttribute.AVG_DEPOSITION,
          RepositoryAttribute.PERCENTAGE_CRITICAL_DEPOSITION)
          .where(QueryAttribute.CALCULATION_ID).getQuery();

  private static final Query CALC_DIFFERENCE_PER_HABITAT_ASSESSMENT_AREA_VIEW =
      getQueryHabitatDepositionBase("critical_deposition_area_calculations_difference_view")
      .select(RepositoryAttribute.TOTAL_DEPOSITION_DIFF, RepositoryAttribute.MAX_DEPOSITION_DIFF,
          RepositoryAttribute.AVG_DEPOSITION_DIFF)
          .where(RepositoryAttribute.BASE_CALCULATION_ID, RepositoryAttribute.VARIANT_CALCULATION_ID).getQuery();

  private CalculationSummaryRepository() {
    // repository class is static.
  }

  private static QueryBuilder getQueryHabitatDepositionBase(final String fromView) {
    return QueryBuilder
        .from(fromView)
        .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.CALCULATION_SUBSTANCE,
            QueryAttribute.CRITICAL_DEPOSITION_AREA_ID, QueryAttribute.NAME, QueryAttribute.DESCRIPTION)
            .where(
                WHERE_CRITICAL_DEPOSITION_AREA_TYPE,
                new StaticWhereClause(
                    RepositoryAttribute.CALCULATION_SUBSTANCE.name() + " = ANY (?::calculation_substance_type[])",
                    RepositoryAttribute.CALCULATION_SUBSTANCE))
                    .orderBy(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.CRITICAL_DEPOSITION_AREA_ID);
  }

  /**
   * Determine the results for a calculation based on assessment areas and habitat types.
   * @param connection The connection to use.
   * @param areas list of natura areas.
   * @param result calculation summary result object to store data in.
   * @param calculationId the id of the calculation to determine the depositions for.
   * @param substances the substances to determine the values per variant for.
   * and the deposition values for each habitat type in that assessment area as value.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static void determineHabitatResults(final Connection connection, final Map<Integer, AssessmentArea> areas,
      final CalculationSummary result, final int calculationId, final List<Substance> substances) throws SQLException {
    determineHabitatResults(connection, areas, result, calculationId, DETERMINE_HABITAT_DEPOSITIONS, substances);
  }

  private static void determineHabitatResults(final Connection connection, final Map<Integer, AssessmentArea> areas, final CalculationSummary result,
      final int calculationId, final Query query, final List<Substance> substances) throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(query.get())) {
      query.setParameter(stmt, QueryAttribute.CALCULATION_ID, calculationId);
      query.setParameter(stmt, QueryAttribute.TYPE, CriticalDepositionAreaType.RELEVANT_HABITAT.name().toLowerCase());
      query.setParameter(stmt, RepositoryAttribute.CALCULATION_SUBSTANCE, toSQLArray(connection, substances));

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final CalculationAreaSummaryResult areaResult = getAreaResult(result, areas, rs);
          final HabitatSummaryResults habitatResults =
              getHabitatSummaryResults(rs, areaResult, QueryAttribute.CRITICAL_DEPOSITION_AREA_ID.getInt(rs));
          final HabitatSummaryResult hsr = habitatResults.getHabitatSummaryResults(calculationId);
          final Substance substance = Substance.safeValueOf(QueryUtil.getString(rs, RepositoryAttribute.CALCULATION_SUBSTANCE));
          final EmissionResultKey erk = EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION);
          //AER-1788 disabled retrieving totals.
          //hsr.getTotal().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.SUM_DEPOSITION));
          hsr.getMax().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.MAX_DEPOSITION));
          hsr.getAverage().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.AVG_DEPOSITION));
          hsr.getPercentageKDW().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.PERCENTAGE_CRITICAL_DEPOSITION));
        }
      }
    }
  }

  private static CalculationAreaSummaryResult getAreaResult(final CalculationSummary result, final Map<Integer, AssessmentArea> areas,
      final ResultSet rs) throws SQLException {
    final int aaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
    if (!result.getSummaryResult().containsKey(aaId)) {
      final CalculationAreaSummaryResult aaesult = new CalculationAreaSummaryResult();
      aaesult.setArea(areas.get(aaId));
      result.getSummaryResult().put(aaId, aaesult);
    }
    return result.getSummaryResult().get(aaId);
  }

  /**
   *
   * @param rs
   * @param areaResult
   * @param habitatTypeId
   * @return
   * @throws SQLException
   */
  private static HabitatSummaryResults getHabitatSummaryResults(final ResultSet rs, final CalculationAreaSummaryResult areaResult,
      final int habitatTypeId) throws SQLException {
    if (!areaResult.getHabitats().containsKey(habitatTypeId)) {
      final HabitatSummaryResults itemHabitat = new HabitatSummaryResults();
      itemHabitat.setId(habitatTypeId);
      itemHabitat.setCode(QueryAttribute.NAME.getString(rs));
      itemHabitat.setName(QueryAttribute.DESCRIPTION.getString(rs));
      areaResult.getHabitats().put(habitatTypeId, itemHabitat);
    }
    return areaResult.getHabitats().get(habitatTypeId);
  }

  /**
   * Determine the results for a calculation based on assessment areas and habitat types.
   * @param connection The DB connection.
   * @param result the summary boject to fill
   * @param calculationId1 The (base) calculation.
   * @param calculationId2 The second calculation, only supply this if you want to diff between the two.
   * @param substances The substances to determine the values for.
   * @throws SQLException If an error occurred communicating with the DB.
   */
  public static void determineHabitatResultsComparison(final Connection connection, final CalculationSummary result,
      final int calculationId1, final Integer calculationId2, final List<Substance> substances) throws SQLException {
    determineHabitatResultsComparison(
        connection, result, calculationId1, calculationId2, CALC_DIFFERENCE_PER_HABITAT_ASSESSMENT_AREA_VIEW, substances);
  }

  private static void determineHabitatResultsComparison(final Connection connection, final CalculationSummary result,
      final int calculationId1, final Integer calculationId2, final Query query, final List<Substance> substances) throws SQLException {
    try (final PreparedStatement stmt = connection.prepareStatement(query.get())) {
      query.setParameter(stmt, RepositoryAttribute.BASE_CALCULATION_ID, calculationId1);
      query.setParameter(stmt, RepositoryAttribute.VARIANT_CALCULATION_ID, calculationId2);
      query.setParameter(stmt, QueryAttribute.TYPE, CriticalDepositionAreaType.RELEVANT_HABITAT.name().toLowerCase());
      query.setParameter(stmt, RepositoryAttribute.CALCULATION_SUBSTANCE, toSQLArray(connection, substances));

      try (final ResultSet rs = stmt.executeQuery()) {
        while (rs.next()) {
          final CalculationAreaSummaryResult areaResult = result.getSummaryResult().get(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));

          final HabitatSummaryResults habitatResults =
              getHabitatSummaryResults(rs, areaResult, QueryAttribute.CRITICAL_DEPOSITION_AREA_ID.getInt(rs));
          final HabitatSummaryResult hsr = habitatResults.getComparisonSummary(calculationId1, calculationId2);

          final Substance substance = Substance.safeValueOf(QueryUtil.getString(rs, RepositoryAttribute.CALCULATION_SUBSTANCE));
          final EmissionResultKey erk = EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION);
          hsr.getTotal().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.TOTAL_DEPOSITION_DIFF));
          hsr.getMax().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.MAX_DEPOSITION_DIFF));
          hsr.getAverage().put(erk, QueryUtil.getDouble(rs, RepositoryAttribute.AVG_DEPOSITION_DIFF));
          // The 3 "difference" values above are all calculated at the receptor level, so the database
          // view can do this. The "percentage KDW difference" is however calculated at assessment-area/habitat
          // level and that makes it really difficult to do in the view (also slower in performance).
          // So that's why -- in the case of comparison results -- this value will be "manually" subtracted
          // later in the code (#getComparisonInfo}).
        }
      }
    }
  }

  private static Array toSQLArray(final Connection connection, final List<Substance> substances) throws SQLException {
    final Set<String> substanceNames = new HashSet<>();
    final Set<Substance> substancesSet = new HashSet<>(substances);
    // If nox and nh3 are requested, also request noxnh3 together if it's not yet present.
    if (!substancesSet.contains(Substance.NOXNH3) && substancesSet.contains(Substance.NH3) && substancesSet.contains(Substance.NOX)) {
      substancesSet.add(Substance.NOXNH3);
    }

    for (final Substance substance : substancesSet) {
      substanceNames.add(substance.toDatabaseString());
    }

    return connection.createArrayOf("text", substanceNames.toArray(new String[substanceNames.size()]));
  }
}
