/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.GregorianCalendar;

import nl.overheid.aerius.db.util.ArrayWhereClause;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.BooleanWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.QueryUtil.ValueFactory;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.RequestFilter;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 *
 */
final class RequestFilterUtil {

  private enum ConvenienceFilterAttribute implements Attribute {
    FROM,
    TILL;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final JoinClause JOIN_ASSESSMENT_AREAS = new JoinClause("requests_to_assessment_areas_view", RegisterAttribute.REQUEST_ID);

  private RequestFilterUtil() {
    //util class
  }

  /**
   * Set RequestFilter specific where clauses on a builder.
   * To be used together with {@link RequestFilterUtil#setRequestFilterParams(Connection, Query, PreparedStatement, RequestFilter)}.
   *
   * @param builder The builder to add specific where clauses to.
   * @param filter The filter to use to determine which clauses to add.
   * @param dateAttribute The attribute to use when determining the 'from' and 'till' parts.
   */
  static void addRequestFilter(final QueryBuilder builder, final RequestFilter<?> filter, final Attribute dateAttribute) {
    if (!filter.getProvinces().isEmpty()) {
      builder.where(new ArrayWhereClause(QueryAttribute.PROVINCE_AREA_ID));
    }
    if (!filter.getAuthorities().isEmpty()) {
      builder.where(new ArrayWhereClause(QueryAttribute.AUTHORITY_ID));
    }
    if (!filter.getSectors().isEmpty()) {
      builder.where(new ArrayWhereClause(QueryAttribute.SECTOR_ID));
    }
    if (!filter.getAssessmentAreas().isEmpty()) {
      builder.join(JOIN_ASSESSMENT_AREAS).where(new ArrayWhereClause(QueryAttribute.ASSESSMENT_AREA_ID));
    }
    if (filter.getFrom() != null) {
      builder.where(new StaticWhereClause(dateAttribute.attribute() + " > ?", ConvenienceFilterAttribute.FROM));
    }
    if (filter.getTill() != null) {
      builder.where(new StaticWhereClause(dateAttribute.attribute() + " <= ?", ConvenienceFilterAttribute.TILL));
    }
    final Boolean marked = filter.getMarked();
    if (marked != null) {
      builder.where(new BooleanWhereClause(RegisterAttribute.MARKED, marked));
    }
  }

  /**
   * Set RequestFilter specific parameter values on a prepared statement.
   * To be used together with {@link RequestFilterUtil#addRequestFilter(QueryBuilder, RequestFilter, Attribute)}.
   *
   * @param con The connection to use (used to construct arrays).
   * @param query The query to use when setting parameters.
   * @param selectPS The prepared statement to set the parameters on.
   * @param filter The filter containing the values to use.
   * @throws SQLException In case setting the parameter values failed.
   */
  static void setRequestFilterParams(final Connection con, final Query query, final PreparedStatement selectPS, final RequestFilter filter)
      throws SQLException {
    if (!filter.getProvinces().isEmpty()) {
      query.setParameter(selectPS, QueryAttribute.PROVINCE_AREA_ID,
          QueryUtil.toNumericSQLArray(con, filter.getProvinces(), new ValueFactory<Province, Integer>() {
            @Override
            public Integer getValue(final Province obj) {
              return obj.getProvinceId();
            }
          }));
    }
    if (!filter.getAuthorities().isEmpty()) {
      query.setParameter(selectPS, QueryAttribute.AUTHORITY_ID,
          QueryUtil.toNumericSQLArray(con, filter.getAuthorities(), new ValueFactory<Authority, Integer>() {
            @Override
            public Integer getValue(final Authority obj) {
              return obj.getId();
            }
          }));
    }
    if (!filter.getSectors().isEmpty()) {
      query.setParameter(selectPS, QueryAttribute.SECTOR_ID,
          QueryUtil.toNumericSQLArray(con, filter.getSectors(), new ValueFactory<Sector, Integer>() {
            @Override
            public Integer getValue(final Sector obj) {
              return obj.getSectorId();
            }
          }));
    }
    if (!filter.getAssessmentAreas().isEmpty()) {
      query.setParameter(selectPS, QueryAttribute.ASSESSMENT_AREA_ID,
          QueryUtil.toNumericSQLArray(con, filter.getAssessmentAreas(), new ValueFactory<AssessmentArea, Integer>() {
            @Override
            public Integer getValue(final AssessmentArea obj) {
              return obj.getId();
            }
          }));
    }
    if (filter.getFrom() != null) {
      query.setParameter(selectPS, ConvenienceFilterAttribute.FROM, filter.getFrom(), Types.TIMESTAMP);
    }
    if (filter.getTill() != null) {
      final Calendar calendar = GregorianCalendar.getInstance();
      calendar.setTime(filter.getTill());
      calendar.add(Calendar.DATE, 1);

      query.setParameter(selectPS, ConvenienceFilterAttribute.TILL, calendar.getTime(), Types.TIMESTAMP);
    }
    final Boolean marked = filter.getMarked();
    if (marked != null) {
      query.setParameter(selectPS, RegisterAttribute.MARKED, marked);
    }
  }

}
