/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.DatabaseErrorCode;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Functionality for modifying notices (meldingen) in the database, e.g. inserting.
 */
public final class NoticeModifyRepository {

  enum RepositoryAttribute implements Attribute {

    OLD_VALUE,
    NEW_VALUE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Logger LOG = LoggerFactory.getLogger(NoticeModifyRepository.class);

  private static final String INSERT_MELDING = "INSERT INTO pronouncements (request_id) VALUES (?) ";

  // ae_assign_pronouncement(v_request_id integer)
  private static final Query ASSIGN_MELDING = QueryBuilder.from("ae_assign_pronouncement(?)", RegisterAttribute.REQUEST_ID).getQuery();

  private static final String DELETE_MELDING = "SELECT ae_delete_pronouncement(?)";

  private static final Query UPDATE_PERMIT_THRESHOLD_VALUES =
      QueryBuilder.from("ae_update_permit_threshold_values()").
          select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.OLD_VALUE, RepositoryAttribute.NEW_VALUE).getQuery();

  private static final String UPDATE_PERMIT_THRESHOLD_VALUES_WITH_AUDIT_TRAIL =
      "INSERT INTO permit_threshold_values_audit_trail(" + QueryAttribute.ASSESSMENT_AREA_ID.attribute() + ","
          + RepositoryAttribute.OLD_VALUE.attribute() + "," + RepositoryAttribute.NEW_VALUE.attribute() + ") "
          + UPDATE_PERMIT_THRESHOLD_VALUES.get();

  private static final String RESET_PERMIT_THRESHOLD_VALUE =
      "UPDATE permit_threshold_values SET value = ae_constant('DEFAULT_PERMIT_THRESHOLD_VALUE')::real WHERE assessment_area_id = ?";

  private static final String CONFIRM_NOTICES = "UPDATE pronouncements "
      + " SET checked = TRUE where request_id = ANY (?)";

  // Not allowed to instantiate.
  private NoticeModifyRepository() {
  }

  /**
   * @param con The connection to use.
   * @param notice The notice that portrays the Melding.
   * @param insertedBy The user that is inserting the application (used for audit trail).
   * @param insertRequestFile The file to use as application file.
   * @return The ID of the melding.
   * @throws AeriusException In case of a database error.
   */
  public static int insertMelding(final Connection con, final Notice notice, final UserProfile insertedBy,
      final InsertRequestFile insertRequestFile) throws AeriusException {
    final int requestId;
    try {
      ensureNew(con, notice);

      //insert basic request stuff.
      insertRequestFile.setRequestFileType(RequestFileType.APPLICATION);
      final Authority authority = NoticeRepository.getAuthorityForNotice(con, notice.getPoint());
      requestId = RequestModifyRepository.insertNewRequest(con, SegmentType.PERMIT_THRESHOLD, authority, notice, insertRequestFile);

      insertNewMelding(con, notice, requestId);
      notice.setId(requestId);

      final ArrayList<AuditTrailChange> changes = new ArrayList<>();
      RequestAuditRepository.addNewRequestChanges(changes, notice.getReference(), SegmentType.PERMIT_THRESHOLD);
      RequestAuditRepository.saveAuditTrailItem(con, notice, changes, insertedBy);
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error inserting melding");
    }
    return requestId;
  }

  private static void insertNewMelding(final Connection con, final Notice notice, final int requestId) throws SQLException, AeriusException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_MELDING)) {
      // (request_id, handler_id, dossier_id, received_date, remarks)
      QueryUtil.setValues(stmt,
          requestId);
      stmt.executeUpdate();
    } catch (final PSQLException e) {
      if (PMF.UNIQUE_VIOLATION.equals(e.getSQLState())) {
        LOG.error("NOTICE_ALREADY_EXISTS: {}", notice.getReference(), e);
        throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, notice.getReference());
      } else {
        throw e;
      }
    }
  }

  /**
   * @param con The connection to use.
   * @param scenario The scenario that portrays the Melding.
   * @return The ID of the melding.
   * @throws AeriusException In case of errors assigning the melding. See reason as to why.
   */
  public static void assignMelding(final Connection con, final Notice notice, final UserProfile insertedBy)
      throws AeriusException {
    try {
      try (final PreparedStatement stmt = con.prepareStatement(ASSIGN_MELDING.get())) {
        ASSIGN_MELDING.setParameter(stmt, RegisterAttribute.REQUEST_ID, notice.getId());

        final ResultSet rs = stmt.executeQuery();
        if (!rs.next()) {
          LOG.error("Insert melding failed for {}", notice.getReference());
          throw new AeriusException(Reason.INTERNAL_ERROR);
        }

        // Insert the audit trail.
        final ArrayList<AuditTrailChange> changes = new ArrayList<>();
        RequestAuditRepository.addChange(changes, AuditTrailType.STATE, RequestState.QUEUED.getDbValue(), RequestState.ASSIGNED.getDbValue());
        RequestAuditRepository.saveAuditTrailItem(con, notice, changes, insertedBy);
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "Error inserting melding");
    }
  }

  /**
   * @param con The connection to use.
   * @param meldingId The id of the melding to delete.
   * @param deletedBy Which user is performing the delete.
   * @throws AeriusException In case of a database error.
   */
  public static void deleteMelding(final Connection con, final int meldingId, final UserProfile deletedBy) throws AeriusException {
    try {
      final ArrayList<AuditTrailChange> changes = new ArrayList<>();
      final Notice notice = NoticeRepository.getNotice(con, meldingId);
      RequestAuditRepository.addChange(changes, AuditTrailType.DELETE, null, notice.getReference());

      try (final PreparedStatement ps = con.prepareStatement(DELETE_MELDING)) {
        QueryUtil.setValues(ps, meldingId);
        ps.executeQuery();

        RequestAuditRepository.saveAuditTrailItem(con, notice, changes, deletedBy);
        RequestAuditRepository.updateRequestIds(con, meldingId, 0);
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "delete notice");
    }
  }

  /**
   * Calls the database function that checks and updates the permit threshold values if needed,
   * and immediately stores the result of the function in the permit threshold audit trail table.
   *
   * @param con The connection to use.
   * @throws SQLException In case of a database error.
   */
  public static void updatePermitThresholdValues(final Connection con) throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(UPDATE_PERMIT_THRESHOLD_VALUES_WITH_AUDIT_TRAIL)) {
      ps.executeUpdate();
    }
  }

  /**
   * Reset the threshold value for the given assessment area to the default value.
   * @param con The connection to use.
   * @param assessmentAreaId id of the assessment area.
   * @throws SQLException In case of a database error.
   */
  public static void resetPermitThresholdValue(final Connection con, final int assessmentAreaId) throws SQLException {
    try (final PreparedStatement ps = con.prepareStatement(RESET_PERMIT_THRESHOLD_VALUE)) {
      ps.setInt(1, assessmentAreaId);
      ps.executeUpdate();
    }
  }

  /**
   * @param con The connection to use.
   * @param noticeIds The Notices to confirm.
   * @throws AeriusException In case of a database error.
   */
  public static void confirmNotices(final Connection con, final List<Integer> noticeIds, final UserProfile confirmedBy) throws AeriusException {
    final ArrayList<AuditTrailChange> changes = new ArrayList<>();
    RequestAuditRepository.addChange(changes, AuditTrailType.CONFIRM); // should probably add reference once used

    try (final PreparedStatement selectPS = con.prepareStatement(CONFIRM_NOTICES)) {
      selectPS.setArray(1, QueryUtil.toNumericSQLArray(con, noticeIds));
      selectPS.executeUpdate();

      for (final Integer noticeId : noticeIds) {
        final Notice notice = NoticeRepository.getNotice(con, noticeId);

        RequestAuditRepository.saveAuditTrailItem(con, notice, changes, confirmedBy);
      }
    } catch (final SQLException e) {
      throw DatabaseErrorCode.createAeriusException(e, LOG, "confirm notice");
    }
  }

  private static void ensureNew(final Connection con, final Notice notice) throws AeriusException, SQLException {
    final int existingId = NoticeRepository.getNoticeIdByReference(con, notice.getReference());
    if (existingId > 0) {
      throw new AeriusException(Reason.REQUEST_ALREADY_EXISTS, notice.getReference());
    }
  }
}
