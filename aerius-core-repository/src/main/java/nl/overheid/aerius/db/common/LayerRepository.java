/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.JoinClause.JoinType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.geo.shared.LayerAeriusWMSProps;
import nl.overheid.aerius.geo.shared.LayerCapabilities;
import nl.overheid.aerius.geo.shared.LayerMultiWMSProps;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.LayerProps.TextLegend;
import nl.overheid.aerius.geo.shared.LayerTMSProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.geo.shared.MultiLayerProps;
import nl.overheid.aerius.shared.domain.info.HabitatType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Database methods for getting layer data.
 */
public final class LayerRepository {

  private static final Logger LOG = LoggerFactory.getLogger(LayerRepository.class);

  private enum RepositoryAttribute implements Attribute {

    LAYER_LEGEND_ID,
    LEGEND_TYPE,
    SORT_ORDER,

    LAYER_ID,
    LAYER_TYPE,
    TITLE,
    MIN_SCALE,
    MAX_SCALE,
    OPACITY,
    ENABLED,
    PART_OF_BASE_LAYER,
    RANGE,

    LAYER_CAPABILITIES_ID,
    URL,
    SLD_URL,
    BEGIN_YEAR,
    END_YEAR,
    DYNAMIC_TYPE,

    BASE_URL,
    IMAGE_TYPE,
    SERVICE_VERSION,
    ATTRIBUTION,
    TILE_SIZE;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private enum LegendType {
    HEXAGON(true), CIRCLE(true), TEXT(false);

    private final boolean colorLegend;

    private LegendType(final boolean colorLegend) {
      this.colorLegend = colorLegend;
    }

    public boolean isColorLegend() {
      return colorLegend;
    }

  }

  private enum LayerType {
    TMS, WMS;
  }

  private enum DynamicLayerType {
    YEAR, HABITAT_TYPE, DATA_RANGE;
  }

  private static final Attributes LAYER_ATTRIBUTES = new Attributes(
      RepositoryAttribute.LAYER_ID, RepositoryAttribute.LAYER_TYPE, QueryAttribute.NAME, RepositoryAttribute.TITLE,
      RepositoryAttribute.LAYER_LEGEND_ID,
      RepositoryAttribute.MIN_SCALE, RepositoryAttribute.MAX_SCALE, RepositoryAttribute.OPACITY, RepositoryAttribute.ENABLED,
      RepositoryAttribute.BASE_URL, RepositoryAttribute.IMAGE_TYPE, RepositoryAttribute.SERVICE_VERSION, RepositoryAttribute.ATTRIBUTION,
      RepositoryAttribute.URL, RepositoryAttribute.SLD_URL, RepositoryAttribute.BEGIN_YEAR, RepositoryAttribute.END_YEAR,
      RepositoryAttribute.DYNAMIC_TYPE, RepositoryAttribute.TILE_SIZE);

  private static final Query LAYER_LEGEND = QueryBuilder.from("system.layer_legends")
      .join(new JoinClause("system.layer_legend_color_items", RepositoryAttribute.LAYER_LEGEND_ID, JoinType.LEFT))
      .select(RepositoryAttribute.LEGEND_TYPE, QueryAttribute.DESCRIPTION, QueryAttribute.NAME, QueryAttribute.COLOR)
      .where(RepositoryAttribute.LAYER_LEGEND_ID)
      .orderBy(RepositoryAttribute.SORT_ORDER)
      .getQuery();

  private static final Query LAYER_DATA_RANGE_ITMES = QueryBuilder.from("system.layer_data_range_items")
      .select(RepositoryAttribute.RANGE, QueryAttribute.NAME)
      .where(RepositoryAttribute.LAYER_LEGEND_ID)
      .orderBy(RepositoryAttribute.SORT_ORDER)
      .getQuery();

  private static final Query DEFAULT_LAYERS = getLayerQuery()
      .join(new JoinClause("system.default_layers", RepositoryAttribute.LAYER_ID))
      .orderBy(RepositoryAttribute.SORT_ORDER)
      .where(RepositoryAttribute.PART_OF_BASE_LAYER)
      .getQuery();

  private static final Query SPECIFIC_LAYER = getLayerQuery()
      .where(QueryAttribute.NAME)
      .getQuery();

  private LayerRepository() {
    // Util class.
  }

  private static QueryBuilder getLayerQuery() {
    return QueryBuilder
        .from("system.layers_view")
        .select(LAYER_ATTRIBUTES);
  }

  /**
   * @param con The connection to use to get the layers.
   * @param key DBMessages key.
   * @return The list of layer properties.
   * @throws SQLException In case of database errors.
   * @throws AeriusException In case of other errors.
   */
  public static ArrayList<LayerProps> getLayers(final Connection con, final DBMessagesKey key) throws SQLException, AeriusException {
    final ArrayList<LayerProps> layers = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(DEFAULT_LAYERS.get())) {
      DEFAULT_LAYERS.setParameter(stmt, RepositoryAttribute.PART_OF_BASE_LAYER, false);

      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        layers.add(getLayerProps(con, rs, key));
      }
    }

    return layers;
  }

  /**
   * @param con The connection to use.
   * @param legendId The ID of the legend to retrieve.
   * @return The correct legend.
   * @throws SQLException In case of exceptions.
   */
  public static Legend getLegend(final Connection con, final int legendId) throws SQLException {
    Legend legend = null;
    try (final PreparedStatement stmt = con.prepareStatement(LAYER_LEGEND.get())) {
      LAYER_LEGEND.setParameter(stmt, RepositoryAttribute.LAYER_LEGEND_ID, legendId);

      final ResultSet rs = stmt.executeQuery();

      final List<String> legendNames = new ArrayList<>();
      final List<String> colors = new ArrayList<>();
      LegendType legendType = null;
      String description = null;
      while (rs.next()) {
        legendNames.add(QueryAttribute.NAME.getString(rs));
        colors.add("#" + QueryAttribute.COLOR.getString(rs));
        legendType = QueryUtil.getEnum(rs, RepositoryAttribute.LEGEND_TYPE, LegendType.class);
        description = QueryAttribute.DESCRIPTION.getString(rs);
      }
      if (legendType != null && legendType.isColorLegend() && !legendNames.isEmpty()) {
        legend = new ColorRangesLegend(legendNames.toArray(new String[legendNames.size()]), colors.toArray(new String[colors.size()]),
            legendType == LegendType.HEXAGON);
      } else if (legendType != null && legendType == LegendType.TEXT) {
        legend = new TextLegend(description);
      }
    }

    return legend;
  }

  /**
   * @param con The connection to use.
   * @param name The name to get the layer for.
   * @return The correct layer (or null if not found)
   * @throws SQLException In case of database errors.
   * @throws AeriusException In case of
   */
  public static LayerProps getLayer(final Connection con, final String name, final DBMessagesKey key) throws SQLException, AeriusException {
    LayerProps layer = null;
    try (final PreparedStatement stmt = con.prepareStatement(SPECIFIC_LAYER.get())) {
      SPECIFIC_LAYER.setParameter(stmt, QueryAttribute.NAME, name);

      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        layer = getLayerProps(con, rs, key);
      }
    }

    return layer;
  }

  private static LayerProps getLayerProps(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException, AeriusException {
    LayerProps layer;
    final String name = QueryAttribute.NAME.getString(rs);
    final LayerType type = QueryUtil.getEnum(rs, RepositoryAttribute.LAYER_TYPE, LayerType.class);
    if (type == null) {
      LOG.error("Unexpected layer type for layer with name {}", name);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    switch (type) {
    case WMS:
      layer = getWMSLayerProps(con, rs, key);
      break;
    case TMS:
      layer = getTMSLayerProps(rs);
      break;
    default:
      layer = new LayerProps();
      break;
    }
    layer.setId(QueryUtil.getInt(rs, RepositoryAttribute.LAYER_ID));
    layer.setName(name);
    layer.setTitle(QueryUtil.getString(rs, RepositoryAttribute.TITLE));
    layer.setAttribution(QueryUtil.getString(rs, RepositoryAttribute.ATTRIBUTION));
    layer.setMinScale(QueryUtil.getInt(rs, RepositoryAttribute.MIN_SCALE));
    layer.setMaxScale(QueryUtil.getInt(rs, RepositoryAttribute.MAX_SCALE));
    layer.setOpacity(QueryUtil.getFloat(rs, RepositoryAttribute.OPACITY));
    layer.setEnabled(QueryUtil.getBoolean(rs, RepositoryAttribute.ENABLED));
    layer.setLegend(getLegend(con, QueryUtil.getInt(rs, RepositoryAttribute.LAYER_LEGEND_ID)));
    return layer;
  }

  private static LayerWMSProps getWMSLayerProps(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException {
    final LayerWMSProps layer = getSpecificWMSLayerProperties(con, rs, key);
    final LayerCapabilities capabilities = new LayerCapabilities();
    capabilities.setUrl(QueryUtil.getString(rs, RepositoryAttribute.URL));
    layer.setCapabilities(capabilities);
    layer.setSldURL(QueryUtil.getString(rs, RepositoryAttribute.SLD_URL));
    layer.setTileSize(QueryUtil.getInt(rs, RepositoryAttribute.TILE_SIZE));
    return layer;
  }

  private static LayerWMSProps getSpecificWMSLayerProperties(final Connection con, final ResultSet rs, final DBMessagesKey key) throws SQLException {
    final DynamicLayerType layerType = QueryUtil.getEnum(rs, RepositoryAttribute.DYNAMIC_TYPE, DynamicLayerType.class);
    final int layerLegendId = QueryUtil.getInt(rs, RepositoryAttribute.LAYER_LEGEND_ID);
    final LayerWMSProps layer;
    if (layerType == null) {
      layer = new LayerWMSProps();
    } else {
      switch (layerType) {
      case YEAR:
        layer = new LayerAeriusWMSProps();
        break;
      case DATA_RANGE:
        layer = getDataRangeItems(con, layerLegendId);
        break;
      case HABITAT_TYPE:
        layer = getHabitatLayer(con, key);
        break;

      default:
        layer = new LayerWMSProps();
        break;
      }
    }
    return layer;
  }

  private static LayerWMSProps getHabitatLayer(final Connection con, final DBMessagesKey key) throws SQLException {
    final LayerMultiWMSProps<String> layer = new LayerMultiWMSProps<>();

    final ArrayList<HabitatType> habitatTypes = HabitatRepository.getHabitatTypes(con, key);
    final LinkedHashMap<Integer, String> habitatFilterTypes = new LinkedHashMap<>();
    for (final HabitatType ht : habitatTypes) {
      habitatFilterTypes.put(ht.getId(), ht.getCode() + ": " + ht.getName());
    }

    layer.setTypes(habitatFilterTypes);
    return layer;
  }

  /**
   * @param con The connection to use.
   * @param legendId The ID of the legend to retrieve.
   * @return The correct legend to set a limit for the result of wms layer
   * @throws SQLException In case of exceptions.
   */
  public static LayerWMSProps getDataRangeItems(final Connection con, final int legendId) throws SQLException {
    final LayerMultiWMSProps<String> layer = new LayerMultiWMSProps<>();
    final LinkedHashMap<Number, String> rangeValues = new LinkedHashMap<>();
    layer.setUseRangeFilter(true);

    try (final PreparedStatement stmt = con.prepareStatement(LAYER_DATA_RANGE_ITMES.get())) {
      LAYER_LEGEND.setParameter(stmt, RepositoryAttribute.LAYER_LEGEND_ID, legendId);

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        rangeValues.put(QueryUtil.getDouble(rs, RepositoryAttribute.RANGE), QueryUtil.getString(rs, QueryAttribute.NAME));
      }
    }
    layer.setTypes(rangeValues);
    return layer;
  }

  private static LayerTMSProps getTMSLayerProps(final ResultSet rs) throws SQLException {
    final LayerTMSProps layer = new LayerTMSProps();
    layer.setBaseUrl(QueryUtil.getString(rs, RepositoryAttribute.BASE_URL));
    layer.setType(QueryUtil.getString(rs, RepositoryAttribute.IMAGE_TYPE));
    layer.setServiceVersion(QueryUtil.getString(rs, RepositoryAttribute.SERVICE_VERSION));
    return layer;
  }

  public static MultiLayerProps getBaseLayer(final Connection con, final DBMessagesKey key) throws SQLException, AeriusException {
    final MultiLayerProps baseLayer = new MultiLayerProps();

    try (final PreparedStatement stmt = con.prepareStatement(DEFAULT_LAYERS.get())) {
      DEFAULT_LAYERS.setParameter(stmt, RepositoryAttribute.PART_OF_BASE_LAYER, true);

      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        baseLayer.getLayers().add(getLayerProps(con, rs, key));
      }
    }

    return baseLayer;
  }

}
