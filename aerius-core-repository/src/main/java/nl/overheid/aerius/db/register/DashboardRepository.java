/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import nl.overheid.aerius.db.EntityCollector;
import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.EnumWhereClause;
import nl.overheid.aerius.db.util.JoinClause;
import nl.overheid.aerius.db.util.OrderByClause;
import nl.overheid.aerius.db.util.OrderByClause.OrderType;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.db.util.StaticWhereClause;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.dashboard.AbstractDashboardFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;

/**
 * Queries to fill the Dashboard page in Register.
 * This is information about permit space and pronouncement space per assessment area, and users
 * have some filter options.
 */
public final class DashboardRepository {

  private static final String ASSESSMENT_AREAS_TO_PROVINCE_AREAS_VIEW = "assessment_areas_to_province_areas_view";
  private static final String ASSESSMENT_AREA_DEVELOPMENT_SPACES_VIEW = "assessment_area_development_spaces_view";
  private static final String ASSESSMENT_AREA_DEVELOPMENT_SPACES_PERMIT_THRESHOLD_VIEW = "assessment_area_development_spaces_permit_threshold_view";

  private enum RepositoryAttribute implements Attribute {
    LIMITER_FRACTION,
    ASSIGNED_FRACTION_FOR_LIMITER_FRACTION,
    PERMIT_THRESHOLD_VALUE_CHANGED;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }
  }

  private DashboardRepository() {
    // Not allowed to instantiate.
  }

  public static ArrayList<DefaultDashboardRow> getNoticeRows(final Connection con, final NoticeDashboardFilter filter) throws SQLException {
    final ArrayList<DefaultDashboardRow> rows = new ArrayList<>();
    final AssessmentAreaCollector collector = new AssessmentAreaCollector();
    final Query query = getNoticeFilterQuery(filter);
    try (final PreparedStatement stmt = con.prepareStatement(query.get())) {
      query.setParameter(stmt, RepositoryAttribute.LIMITER_FRACTION, filter.getMinFractionUsed());
      applyDefaultFilter(stmt, query, filter);

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final DefaultDashboardRow row = new DefaultDashboardRow();
        row.setAssessmentArea(collector.collectEntity(rs));
        row.setAssignedFactor(QueryUtil.getDouble(rs, RepositoryAttribute.LIMITER_FRACTION));
        row.setThresholdChanged(QueryUtil.getBoolean(rs, RepositoryAttribute.PERMIT_THRESHOLD_VALUE_CHANGED));
        rows.add(row);
      }
    }
    return rows;
  }

  /**
   * Processes retrieval of default Permit rows
   * @param con The connection to use.
   * @param filter The filter to use when retrieving rows.
   * @return The list of dashboard rows which match the filter.
   * @throws SQLException In case of database errors.
   */
  public static ArrayList<DefaultDashboardRow> getPermitRows(final Connection con, final PermitDashboardFilter filter)
      throws SQLException {
    final Query permitsQuery = getPermitFilterQuery(filter);
    final ArrayList<DefaultDashboardRow> rows = new ArrayList<>();
    final AssessmentAreaCollector collector = new AssessmentAreaCollector();
    try (final PreparedStatement stmt = con.prepareStatement(permitsQuery.get())) {
      applyDefaultFilter(stmt, permitsQuery, filter);

      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        final DefaultDashboardRow row = new DefaultDashboardRow();
        row.setAssessmentArea(collector.collectEntity(rs));
        row.setAssignedFactor(QueryUtil.getDouble(rs, RepositoryAttribute.ASSIGNED_FRACTION_FOR_LIMITER_FRACTION));
        row.setPendingFactor(QueryUtil.getDouble(rs, RepositoryAttribute.LIMITER_FRACTION)
            - QueryUtil.getDouble(rs, RepositoryAttribute.ASSIGNED_FRACTION_FOR_LIMITER_FRACTION));
        rows.add(row);
      }
    }
    return rows;
  }

  private static Query getNoticeFilterQuery(final NoticeDashboardFilter filter) {
    final QueryBuilder builder = getBaseQueryBuilder(filter, SegmentType.PERMIT_THRESHOLD);
    builder.select(RepositoryAttribute.PERMIT_THRESHOLD_VALUE_CHANGED);
    final String condition = "ROUND(" + RepositoryAttribute.LIMITER_FRACTION.attribute() + "::numeric, 2) >= ?";
    builder.where(new StaticWhereClause(condition, RepositoryAttribute.LIMITER_FRACTION));
    return builder.getQuery();
  }

  private static Query getPermitFilterQuery(final PermitDashboardFilter filter) {
    final QueryBuilder builder = getBaseQueryBuilder(filter, SegmentType.PROJECTS);
    builder.select(RepositoryAttribute.ASSIGNED_FRACTION_FOR_LIMITER_FRACTION);
    builder.where(new EnumWhereClause<SegmentType>(QueryAttribute.SEGMENT, SegmentType.PROJECTS));
    return builder.getQuery();
  }

  private static QueryBuilder getBaseQueryBuilder(final AbstractDashboardFilter filter, final SegmentType segment) {
    final QueryBuilder builder = QueryBuilder.from(determineAssessmentAreaDevelopmentSpaceView(segment))
        .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.ASSESSMENT_AREA_NAME, RepositoryAttribute.LIMITER_FRACTION)
        .orderBy(new OrderByClause(RepositoryAttribute.LIMITER_FRACTION, OrderType.DESC))
        .orderBy(QueryAttribute.ASSESSMENT_AREA_NAME);
    buildDefaultFilter(builder, filter);
    return builder;
  }

  private static String determineAssessmentAreaDevelopmentSpaceView(final SegmentType segment) {
    if (segment == SegmentType.PERMIT_THRESHOLD) {
      return ASSESSMENT_AREA_DEVELOPMENT_SPACES_PERMIT_THRESHOLD_VIEW;
    } else {
      return ASSESSMENT_AREA_DEVELOPMENT_SPACES_VIEW;
    }
  }

  private static void buildDefaultFilter(final QueryBuilder builder, final AbstractDashboardFilter filter) {
    if (filter.getProvinceId() != null && filter.getProvinceId() != 0) {
      builder.join(new JoinClause(ASSESSMENT_AREAS_TO_PROVINCE_AREAS_VIEW, QueryAttribute.ASSESSMENT_AREA_ID));
      builder.where(QueryAttribute.PROVINCE_AREA_ID);
    }
  }

  private static void applyDefaultFilter(final PreparedStatement stmt, final Query query, final AbstractDashboardFilter filter)
      throws SQLException {
    if (query.hasParameter(QueryAttribute.PROVINCE_AREA_ID)) {
      query.setParameter(stmt, QueryAttribute.PROVINCE_AREA_ID, filter.getProvinceId());
    }
  }

  /**
   * Collect id and name information for the assessment areas in query results.
   */
  private static class AssessmentAreaCollector extends EntityCollector<AssessmentArea> {

    @Override
    public int getKey(final ResultSet rs) throws SQLException {
      return QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
    }

    @Override
    public AssessmentArea fillEntity(final ResultSet rs) throws SQLException {
      final AssessmentArea assessmentArea = new AssessmentArea();
      assessmentArea.setId(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs));
      assessmentArea.setName(QueryAttribute.ASSESSMENT_AREA_NAME.getString(rs));
      return assessmentArea;
    }
  }

}
