/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.category.FarmLodgingCategoryRepository;
import nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository;
import nl.overheid.aerius.db.common.sector.category.OffRoadMachineryTypesRepository;
import nl.overheid.aerius.db.common.sector.category.PlanEmissionCategoryRepository;
import nl.overheid.aerius.db.common.sector.category.RoadEmissionCategoryRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.db.i18n.DBMessages;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorProperties;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.util.EnumUtil;

/**
 * Service class for Sector database queries.
 */
public final class SectorRepository {
  // Order sectors by group, description to display sectors in alphabetical order in UI.
  private static final String GET_SECTORS = "SELECT * FROM sectors INNER JOIN system.sector_properties_view USING (sector_id) "
      + " ORDER BY sector_properties_view.sectorgroup, sectors.sector_id";
  private static final String GET_ALL_SECTOR_CHARACTERISTICS =
      "SELECT * "
          + " FROM default_source_characteristics_view ";
  private static final String GET_SECTOR_CHARACTERISTICS =
      "SELECT * "
          + " FROM default_source_characteristics_view "
          + " WHERE sector_id = ? ";

  private static final Map<DBMessagesKey, ArrayList<Sector>> SECTOR_CACHE = new HashMap<>();

  private SectorRepository() {
  }

  public static SectorCategories getSectorCategories(final PMF pmf, final Locale locale) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      return SectorRepository.getSectorCategories(con, new DBMessagesKey(pmf.getProductType(), locale));
    }
  }

  /**
   * Returns the {@link SectorCategories} object containing all Sector Category data.
   * @param con Database connection
   * @param messagesKey Locale and product type to get descriptions for sectors for
   * @return SectorCategories object
   * @throws SQLException database related exception
   */
  public static SectorCategories getSectorCategories(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final SectorCategories c = new SectorCategories();
    c.setSectors(SectorRepository.getSectors(con, messagesKey));

    c.setFarmLodgingCategories(FarmLodgingCategoryRepository.getFarmLodgingCategories(con, messagesKey));
    c.setRoadEmissionCategories(RoadEmissionCategoryRepository.findAllRoadEmissionCategories(con));
    c.setOffRoadMobileSourceCategories(MobileSourceCategoryRepository.findOffRoadMobileSourceCategories(con, messagesKey));
    c.setOnRoadMobileSourceCategories(MobileSourceCategoryRepository.findOnRoadMobileSourceCategories(con, messagesKey));
    c.setMaritimeShippingCategories(ShippingCategoryRepository.findAllMaritimeShippingCategories(con, messagesKey));
    c.setInlandShippingCategories(ShippingCategoryRepository.findInlandShippingCategories(con, messagesKey));
    c.setPlanEmissionCategories(PlanEmissionCategoryRepository.findAllPlanEmissionCategories(con, messagesKey));
    c.setShippingSnappableNodes(ShippingRepository.getShippingSnappableNodes(con));
    c.setOffRoadMachineryTypes(OffRoadMachineryTypesRepository.findAllOffRoadMachineryTypes(con, messagesKey));
    c.setDiurnalVariations(SourceCharacteristicsRepository.getDiurnalVariations(con, messagesKey));

    return c;
  }

  /**
   * Returns a list of all Sectors from the database. For all sectors also the
   * Source characteristics are queried.
   *
   * @param con Database connection
   * @param messagesKey Locale and product type to get descriptions for sectors for
   * @return List of sub sectors
   * @throws SQLException database related exception
   */
  public static ArrayList<Sector> getSectors(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    if (!SECTOR_CACHE.containsKey(messagesKey)) {
      SECTOR_CACHE.put(messagesKey, getSectorsFromDB(con, messagesKey));
    }
    return SECTOR_CACHE.get(messagesKey);
  }

  private static ArrayList<Sector> getSectorsFromDB(final Connection con, final DBMessagesKey messagesKey) throws SQLException {
    final ArrayList<Sector> sectors = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(GET_SECTORS)) {
      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        final Sector ss = new Sector();
        ss.setSectorId(rs.getInt("sector_id"));
        ss.setSectorGroup(EnumUtil.get(SectorGroup.class, rs.getString("sectorgroup")));
        DBMessages.setSectorMessages(ss, messagesKey);
        ss.setProperties(new SectorProperties(rs.getString("color"), rs.getString("icon_type"),
            rs.getString("emission_calculation_method"), rs.getString("calculation_engine"), rs.getBoolean("building_possible")));
        sectors.add(ss);
      }
    }
    setSectorCharacteristics(con, sectors, messagesKey);
    return sectors;
  }

  public static Map<Integer, SectorProperties> getSectorProperties(final Connection con) throws SQLException {
    final Map<Integer, SectorProperties> map = new HashMap<>();
    try (final PreparedStatement stmt = con.prepareStatement(GET_SECTORS)) {
      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        map.put(rs.getInt("sector_id"),
            new SectorProperties(rs.getString("color"), rs.getString("icon_type"), rs.getString("emission_calculation_method"),
                rs.getString("calculation_engine"), rs.getBoolean("building_possible")));
      }
    }
    return map;
  }

  /**
   * Sets the sub sector characteristics on the given sector by querying them from the database. It could be no default values are available.
   *
   * @param con Database connection
   * @param sectors Sets characteristics on these sectors
   * @param messagesKey the message key to optionally internationalise data with
   * @throws SQLException database related exception
   */
  private static void setSectorCharacteristics(final Connection con, final ArrayList<Sector> sectors, final DBMessagesKey messagesKey)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(GET_ALL_SECTOR_CHARACTERISTICS)) {
      final ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        final int sectorId = QueryAttribute.SECTOR_ID.getInt(rs);
        final OPSSourceCharacteristics sc = new OPSSourceCharacteristics();
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(sc, rs, rs.getInt("particle_size_distribution"), messagesKey);
        setSectorCharacteristicsForSector(sectors, sectorId, sc);
      }
    }
  }

  private static void setSectorCharacteristicsForSector(final List<Sector> sectors, final int sectorId, final OPSSourceCharacteristics sc) {
    for (final Sector sector : sectors) {
      if (sector.getSectorId() == sectorId) {
        sector.setDefaultCharacteristics(sc);
        break;
      }
    }
  }

  /**
   * Returns the OPS Characteristics for a specific sector.
   * @param con Database connection
   * @param sector sector to query
   * @return the OPS Characteristics
   * @throws SQLException database related exception
   */
  public static OPSSourceCharacteristics getOPSSourceCharacteristics(final Connection con, final Sector sector) throws SQLException {
    OPSSourceCharacteristics sc = null;
    try (final PreparedStatement stmt = con.prepareStatement(GET_SECTOR_CHARACTERISTICS)) {
      stmt.setInt(1, sector.getSectorId());
      final ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        sc = new OPSSourceCharacteristics();
        final int particleSizeDistribution = rs.getInt("particle_size_distribution");
        SourceCharacteristicsUtil.setSourceCharacteristicsFromResultSet(sc, rs, particleSizeDistribution);
      }
    }
    return sc;
  }

}
