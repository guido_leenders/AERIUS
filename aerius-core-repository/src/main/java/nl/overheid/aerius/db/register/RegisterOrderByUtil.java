/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import nl.overheid.aerius.db.util.OrderByUtil;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.shared.domain.SortableAttribute;
import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.register.NoticeSortableAttribute;
import nl.overheid.aerius.shared.domain.register.PermitSortableAttribute;
import nl.overheid.aerius.shared.domain.register.PriorityProjectSortableAttribute;

public class RegisterOrderByUtil extends OrderByUtil {
  static {
    // Permits
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.STATUS, RegisterAttribute.STATUS);
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.DATE, RegisterAttribute.RECEIVED_DATE);
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.APPLICANT, RegisterAttribute.CORPORATION);
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.CASE_NUMBER, RegisterAttribute.DOSSIER_ID);
    ATTRIBUTE_MAPPING.put(PermitSortableAttribute.MARKED, RegisterAttribute.MARKED);

    // Notices
    ATTRIBUTE_MAPPING.put(NoticeSortableAttribute.DATE, RegisterAttribute.INSERT_DATE);
    ATTRIBUTE_MAPPING.put(NoticeSortableAttribute.REFERENCE, RegisterAttribute.REFERENCE);
    ATTRIBUTE_MAPPING.put(NoticeSortableAttribute.CORPORATION, RegisterAttribute.CORPORATION);

    // Priority projects
    ATTRIBUTE_MAPPING.put(PriorityProjectSortableAttribute.PROGRESS, RegisterAttribute.FRACTION_ASSIGNED);
    ATTRIBUTE_MAPPING.put(PriorityProjectSortableAttribute.PROJECT_NAME, RegisterAttribute.PROJECT_NAME);
    ATTRIBUTE_MAPPING.put(PriorityProjectSortableAttribute.PROJECT_ID, RegisterAttribute.DOSSIER_ID);
    ATTRIBUTE_MAPPING.put(PriorityProjectSortableAttribute.AUTHORITY, QueryAttribute.AUTHORITY_ID);
  }

  /**
   * Mirror function required to make this class initialize.
   *
   * Sort of unfortunate.
   */
  public static void setOrderByParams(final QueryBuilder builder, final SortableAttribute sortAttribute, final SortableDirection sortDirection) {
    OrderByUtil.setOrderByParams(builder, sortAttribute, sortDirection);
  }
}
