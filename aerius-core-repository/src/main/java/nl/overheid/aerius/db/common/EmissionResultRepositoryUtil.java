/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import nl.overheid.aerius.db.util.Attributes;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CriticalLevels;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Util class for repositories that have to do with obtaining emission results for points from the database.
 */
public final class EmissionResultRepositoryUtil {

  public static final Attributes REQUIRED_ATTRIBUTES = new Attributes(QueryAttribute.SUBSTANCE_ID, QueryAttribute.RESULT_TYPE, QueryAttribute.RESULT);

  private EmissionResultRepositoryUtil() {
    // util class
  }

  /**
   * @param rs The result set containing emission results.
   * @param emissionResults The emission result set to add results to.
   * @throws SQLException In case of errors.
   */
  public static void addCriticalLevel(final ResultSet rs, final CriticalLevels emissionResults) throws SQLException {
    addResultValue(rs, emissionResults, QueryAttribute.CRITICAL_LEVEL);
  }

  /**
   * @param rs The result set containing emission results.
   * @param emissionResults The emission result set to add results to.
   * @throws SQLException In case of errors.
   */
  public static void addEmissionResult(final ResultSet rs, final EmissionResults emissionResults) throws SQLException {
    addResultValue(rs, emissionResults, QueryAttribute.RESULT);
  }

  public static void addCriticalDeposition(final ResultSet rs, final CriticalLevels criticalLevels) throws SQLException {
    addEmissionResult(criticalLevels, Substance.NOXNH3, EmissionResultType.DEPOSITION, QueryAttribute.CRITICAL_DEPOSITION.getDouble(rs));
  }

  /**
   * @param rs The result set containing emission results.
   * @param emissionResults The emission result set to add results to.
   * @param column the column where the result value resides in (must be double)
   * @throws SQLException In case of errors.
   */
  private static void addResultValue(final ResultSet rs, final EmissionResults emissionResults, final QueryAttribute column) throws SQLException {
    final Substance substance = Substance.substanceFromId(QueryAttribute.SUBSTANCE_ID.getInt(rs));
    final EmissionResultType type = QueryAttribute.RESULT_TYPE.getEnum(rs, EmissionResultType.class);
    final double emissionResult = column.getDouble(rs);

    addEmissionResult(emissionResults, substance, type, emissionResult);
  }

  private static void addEmissionResult(final EmissionResults emissionResults, final Substance substance, final EmissionResultType type,
      final double emissionResult) {
    emissionResults.put(EmissionResultKey.valueOf(substance, type), emissionResult);
  }

  /**
   * @param rs The resultset containing a row of results from a query. Should contain all the required attributes.
   * @param resultPointMap The map containing already obtained results.
   * @param point The point that's in the row (should be filled correctly based on situation).
   * @throws SQLException In case of errors.
   */
  public static void addResultsToPoint(final ResultSet rs, final HashMap<Integer, AeriusResultPoint> resultPointMap, final AeriusResultPoint point)
      throws SQLException {
    addEmissionResult(rs, point.getEmissionResults());

    final AeriusResultPoint arp = resultPointMap.get(point.getId());
    if (arp == null) {
      resultPointMap.put(point.getId(), point);
    } else {
      arp.merge(point);
    }
  }

}
