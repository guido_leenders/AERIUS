/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.shared.domain.ProductType;

@RunWith(value = Parameterized.class)
public class ConstantsEnumTest extends BaseDBTest {

  private final ProductType productType;

  public ConstantsEnumTest(final ProductType productType) {
    this.productType = productType;
  }

  @Parameters(name = "{0}")
  public static List<Object[]> data() throws SQLException, IOException {
    final List<Object[]> tests = new ArrayList<>();
    for (final ProductType productType : ProductType.values()) {
      tests.add(new Object[] { productType });
    }
    return tests;
  }

  @Test
  public void testConstants() throws SQLException {
    final Connection connection = getConnection(productType);
    for (final ConstantsEnum constant : ConstantsEnum.values()) {
      if (constant.getProductTypes().contains(productType)) {
        try {
          final String value = ConstantRepository.getString(connection, constant);
          assertFalse("Empty constant: " + constant + " @ " + productType.getDbRepresentation(), value.isEmpty());
        } catch (final IllegalArgumentException e) {
          fail(constant + " does not exist in database " + productType.getDbRepresentation());
        }
      }
    }
  }

}
