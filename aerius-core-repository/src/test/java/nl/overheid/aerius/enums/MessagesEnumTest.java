/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.enums;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.util.LocaleUtils;

@RunWith(value = Parameterized.class)
public class MessagesEnumTest extends BaseDBTest {

  private final ProductType productType;
  private final Locale locale;

  public MessagesEnumTest(final ProductType productType, final Locale locale) {
    this.productType = productType;
    this.locale = locale;
  }

  @Parameters(name = "{0} - {1}")
  public static List<Object[]> data() throws SQLException, IOException {
    final List<Object[]> tests = new ArrayList<>();
    for (final ProductType productType : ProductType.values()) {
      if (productType != ProductType.MELDING) {
        for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
          tests.add(new Object[] { productType, locale });
        }
      }
    }
    return tests;
  }

  @Test
  public void testMessages() throws SQLException {
    final Connection connection = getConnection(productType);
    for (final MessagesEnum message : MessagesEnum.values()) {
      try {
        final String value = MessageRepository.getString(connection, message, locale);
        assertFalse("Empty message: " + message + " @ " + productType.getDbRepresentation(), value.isEmpty());
      } catch (final IllegalArgumentException e) {
        fail(message + " (" + locale.getLanguage() + ") does not exist in database " + productType.getDbRepresentation());
      }
    }
  }

}
