/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.apache.commons.codec.DecoderException;
import org.junit.Test;

/**
 * Test class for {@link SaltDigestUtil}.
 */
public class SaltDigestUtilTest {
  private static final String TEST = "Test";
  private static final String TEST_DOT = "Test.";
  private static final String RANDOM_MUBO_JUMBO = "!@#!#%&@$#^!#%♪Üj_·974óA496È1";
  private static final String LAZY_DOG_DOT = "The quick brown fox jumps over the lazy dog.";
  private static final String LAZY_DOG = "The quick brown fox jumps over the lazy dog";
  private static final String SOME_UUID = "747d4328-dcea-4f2b-a6db-b1fa5e7cafb1";

  @Test
  public void testValidity() throws NoSuchAlgorithmException, DecoderException {
    assertCombineSecret(TEST);
    assertCombineSecret(RANDOM_MUBO_JUMBO);
    assertCombineSecret(LAZY_DOG);
    assertCombineSecret(LAZY_DOG_DOT);
    assertCombineSecret(SOME_UUID);
  }

  private void assertCombineSecret(final String secretE) throws NoSuchAlgorithmException, DecoderException {
    final String e = SaltDigestUtil.combineSecret(secretE, createSalt());
    assertTrue(secretE, SaltDigestUtil.isEqual(e, secretE));
  }

  @Test
  public void testInvalidity() throws NoSuchAlgorithmException, DecoderException {
    assertCombineSecretInvalid(TEST, TEST_DOT);
    assertCombineSecretInvalid(RANDOM_MUBO_JUMBO, "@@#!#%&@$#^!#%♪Üj_·974óA496È1");
    assertCombineSecretInvalid(LAZY_DOG, LAZY_DOG_DOT);
    assertCombineSecretInvalid(LAZY_DOG_DOT, LAZY_DOG);
    assertCombineSecretInvalid(SOME_UUID, "747d4328-dcea-4f2b-a6db-b1fa5f7cafb1");
  }

  private void assertCombineSecretInvalid(final String secret, final String notTheSameSecret) throws NoSuchAlgorithmException, DecoderException {
    final String a = SaltDigestUtil.combineSecret(secret, createSalt());
    assertFalse(secret + " != " + notTheSameSecret, SaltDigestUtil.isEqual(a, notTheSameSecret));
  }

  @Test
  public void testConsistency() throws NoSuchAlgorithmException, DecoderException {
    final String a = SaltDigestUtil.combineSecret("abcd", "efgh");
    final String b = SaltDigestUtil.combineSecret("abcd", "efgh");

    assertEquals("2 of the same should be the same", a, b);

    final String c = SaltDigestUtil.combineSecret(RANDOM_MUBO_JUMBO, LAZY_DOG);
    final String d = SaltDigestUtil.combineSecret(RANDOM_MUBO_JUMBO, LAZY_DOG);

    assertEquals("2 complex of the same should be the same", c, d);

    final String e = SaltDigestUtil.combineSecret("abcd", "efgh");
    final String f = SaltDigestUtil.combineSecret("abcd", "efghi");

    assertNotEquals("2 different should not be the same", e, f);
  }

  @Test(expected = AssertionError.class)
  public void testNullSalt() throws NoSuchAlgorithmException, DecoderException {
    SaltDigestUtil.combineSecret("abcd", null);
  }

  @Test(expected = AssertionError.class)
  public void testNullSecret() throws NoSuchAlgorithmException, DecoderException {
    SaltDigestUtil.combineSecret(null, "asdf");
  }

  @Test(expected = AssertionError.class)
  public void testEmptySalt() throws NoSuchAlgorithmException, DecoderException {
    SaltDigestUtil.combineSecret("asdf", "");
  }

  private static String createSalt() {
    return UUID.randomUUID().toString();
  }
}