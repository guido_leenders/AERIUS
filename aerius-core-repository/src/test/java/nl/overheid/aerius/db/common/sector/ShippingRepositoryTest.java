/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Needs a proper (filled) database to test against, testing DB operations for the shipping sector.
 */
public class ShippingRepositoryTest extends BaseDBTest {

  private static final WKTGeometry TEST_INLAND_ROUTE = new WKTGeometry("LINESTRING(208000 455000,208000 456000)", 1000);
  private static final String TEST_INLAND_ROUTE_TYPE = "IJssel";
  private static final WKTGeometry TEST_INLAND_LOCK_ROUTE = new WKTGeometry("LINESTRING(126720 427954,125624 426261)", 2000);
  private static final String TEST_INLAND_LOCK_ROUTE_TYPE = "CEMT_IV";
  private static final WKTGeometry POINT = new WKTGeometry("POINT(1000 1000)", 0);
  private static final WKTGeometry POLYGON = new WKTGeometry("POLYGON((1000 1000, 1001 1000, 1001 1001, 1000 1001, 1000 1000))", 1);

  @Test
  public void testGetShippingSnappableNodes() throws SQLException {
    final ArrayList<ShippingNode> nodes = ShippingRepository.getShippingSnappableNodes(getCalcConnection());
    assertNotNull("There should be some nodes in the DB", nodes);
    assertFalse("There should be some nodes in the DB", nodes.isEmpty());
  }

  @Test
  public void testGetShippingRoute() throws SQLException, AeriusException {
    final MooringMaritimeVesselGroup vgev = getExampleVesselGroupEmissionValues();
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeMooringShippingInlandRoute(
        getCalcConnection(), vgev, 25);
    assertNotNull("Route shouldn't be null", routePoints);
    assertFalse("Route should consist of some points", routePoints.isEmpty());
    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      assertTrue("Source X for routePoint:" + routePoint, routePoint.getX() > 10000 && routePoint.getX() < 10100);
      assertTrue("Source Y for routePoint:" + routePoint, routePoint.getX() > 10000 && routePoint.getX() < 10100);
      assertNotEquals("Measure for routePoint > 0:" + routePoint, 0.0, routePoint.getMeasure(), 1E-3);
    }
  }

  private MooringMaritimeVesselGroup getExampleVesselGroupEmissionValues() throws SQLException {
    final ArrayList<ShippingNode> nodes = ShippingRepository.getShippingSnappableNodes(getCalcConnection());
    final ShippingNode node = nodes.get(4);
    final MooringMaritimeVesselGroup vgev = new MooringMaritimeVesselGroup();
    final MaritimeShippingCategory category = new MaritimeShippingCategory();
    category.setId(1);
    vgev.setCategory(category);
    final ShippingRoute route = new ShippingRoute();
    route.setGeometry(new WKTGeometry("LINESTRING(10000 10000,10100 10100)"));
    route.setEndPoint(node);
    vgev.setInlandRoute(route);
    return vgev;
  }

  @Test
  public void testGetMaritimeShippingRoutePoints() throws SQLException, AeriusException {
    final WKTGeometry route = new WKTGeometry("LINESTRING(1000 1000,1000 2000)", 1000);
    final List<MaritimeShippingRoutePoint> routePoints =
        ShippingRepository.getMaritimeShippingRoutePoints(getCalcConnection(), route, 25);
    assertNotNull("Route shouldn't be null", routePoints);
    assertEquals("Route should consist of specified number of points", 40, routePoints.size());
    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      assertEquals("Source X for routePoint:" + routePoint, 1000, routePoint.getX(), 1E-3);
      assertTrue("Source Y for routePoint:" + routePoint, routePoint.getY() > 1000 && routePoint.getY() < 2000);
      assertNotEquals("Measure for routePoint > 0:" + routePoint, 0.0, routePoint.getMeasure(), 1E-3);
    }
  }

  @Test
  public void testGetInlandShippingRoutePoints() throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints =
        ShippingRepository.getInlandShippingRoutePoints(getCalcConnection(), TEST_INLAND_ROUTE);
    assertNotNull("Route shouldn't be null", routePoints);
    assertEquals("Route should consist of specified number of points", 40, routePoints.size());
    for (final InlandShippingRoutePoint routePoint : routePoints) {
      assertEquals("Source X for routePoint:" + routePoint, 208000, routePoint.getX(), 1E-3);
      assertTrue("Source Y for routePoint:" + routePoint, routePoint.getY() > 455000 && routePoint.getY() < 456000);
      assertNotEquals("Measure for routePoint > 0:" + routePoint, 0.0, routePoint.getMeasure(), 1E-3);
      assertEquals("Lockfactor for routePoint:" + routePoint, 1.0, routePoint.getLockFactor(), 1E-3);
      assertNull("Direction for routePoint:" + routePoint, routePoint.getDirection());
      assertNull("Waterway type for routePoint:" + routePoint, routePoint.getWaterwayCategoryCode());
    }
  }

  @Test
  public void testGetInlandShippingRoutePointsWithLocks() throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints =
        ShippingRepository.getInlandShippingRoutePoints(getCalcConnection(), TEST_INLAND_LOCK_ROUTE);
    assertNotNull("Route shouldn't be null", routePoints);
    assertEquals("Route should consist of specified number of points", 84, routePoints.size());
    boolean foundLock = false;
    for (final InlandShippingRoutePoint routePoint : routePoints) {
      if (Math.abs(routePoint.getLockFactor() - 1.0) > 1E-3) {
        foundLock = true;
        break;
      }
    }
    assertTrue("Lock should be found in the route", foundLock);
  }

  @Test
  public void testGetInlandShippingRoutePointsWithWaterways() throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints =
        ShippingRepository.getInlandShippingRoutePointsWithWaterways(getCalcConnection(), TEST_INLAND_ROUTE);
    assertNotNull("Route shouldn't be null", routePoints);
    assertEquals("Route should consist of specified number of points", 40, routePoints.size());
    for (final InlandShippingRoutePoint routePoint : routePoints) {
      assertEquals("Source X for routePoint:" + routePoint, 208000, routePoint.getX(), 1E-3);
      assertTrue("Source Y for routePoint:" + routePoint, routePoint.getY() > 455000 && routePoint.getY() < 456000);
      assertNotEquals("Measure for routePoint > 0:" + routePoint, 0.0, routePoint.getMeasure(), 1E-3);
      assertEquals("Lockfactor for routePoint:" + routePoint, 1.0, routePoint.getLockFactor(), 1E-3);
      assertEquals("Direction for routePoint:" + routePoint, WaterwayDirection.DOWNSTREAM, routePoint.getDirection());
      assertEquals("Waterway type for routePoint:" + routePoint, TEST_INLAND_ROUTE_TYPE, routePoint.getWaterwayCategoryCode());
    }
  }

  @Test
  public void testSuggestInlandShippingWaterway() throws SQLException, AeriusException {
    final List<InlandWaterwayType> iwt1 = ShippingRepository.suggestInlandShippingWaterway(getCalcConnection(), TEST_INLAND_ROUTE);
    assertEquals("Suggested waterway", TEST_INLAND_ROUTE_TYPE, iwt1.get(0).getWaterwayCategory().getCode());
    final List<InlandWaterwayType> iwt2 = ShippingRepository.suggestInlandShippingWaterway(getCalcConnection(), TEST_INLAND_LOCK_ROUTE);
    assertEquals("Suggested waterway", TEST_INLAND_LOCK_ROUTE_TYPE, iwt2.get(0).getWaterwayCategory().getCode());
  }

  @Test
  public void testInvalidGeometries() throws SQLException, AeriusException {
    assertInvalidGeometry(POINT);
    assertInvalidGeometry(POLYGON);
  }

  private void assertInvalidGeometry(final WKTGeometry geometry) throws SQLException, AeriusException {
    try {
      final MooringMaritimeVesselGroup vgev = getExampleVesselGroupEmissionValues();
      vgev.getInlandRoute().setGeometry(geometry);
      ShippingRepository.getMaritimeMooringShippingInlandRoute(getCalcConnection(), vgev, 25);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }

    try {
      ShippingRepository.getMaritimeShippingRoutePoints(getCalcConnection(), geometry, 25);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
    try {
      ShippingRepository.getInlandShippingRoutePointsWithWaterways(getCalcConnection(), geometry);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
    try {
      ShippingRepository.getInlandShippingRoutePoints(getCalcConnection(), geometry);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
    try {
      ShippingRepository.suggestInlandShippingWaterway(getCalcConnection(), geometry);
      failExpectedInvalidGeometryException(geometry);
    } catch (final AeriusException e) {
      assertRouteGeometryNotAllowed(e);
    }
  }

  private void failExpectedInvalidGeometryException(final WKTGeometry geometry) {
    fail("Expected exception due to use of invalid geometry:" + geometry);
  }

  private void assertRouteGeometryNotAllowed(final AeriusException e) {
    assertEquals("Exception should be no valid geometry", Reason.SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED, e.getReason());
  }
}
