/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.register.NoticeModifyRepository.RepositoryAttribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryAttribute;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

/**
 *
 */
public class NoticeModifyRepositoryTest extends RequestTestBase {

  private static final double EPSILON = 1E-5;

  private static final int ASSESSMENT_AREA_ID = 57;

  private static final int RECEPTOR_POINT_ID_1 = 4776053;
  private static final int RECEPTOR_POINT_ID_2 = 4776054;
  private static final int RECEPTOR_POINT_ID_3 = 4776055;

  private static final Query READ_PERMIT_THRESHOLD_VALUES = QueryBuilder.from("permit_threshold_values")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, QueryAttribute.VALUE).getQuery();

  private static final Query READ_PERMIT_THRESHOLD_VALUES_AUDIT_TRAIL = QueryBuilder.from("permit_threshold_values_audit_trail")
      .select(QueryAttribute.ASSESSMENT_AREA_ID, RepositoryAttribute.OLD_VALUE, RepositoryAttribute.NEW_VALUE).getQuery();

  @Override
  @Before
  public void setUp() throws SQLException {
    NoticeModifyRepository.resetPermitThresholdValue(getRegConnection(), ASSESSMENT_AREA_ID);
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_1, 0.10);
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_2, 0.10);
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_3, 0.10);
  }

  @Test
  public void testAssignMeldingUnderThresholds() throws SQLException, AeriusException {
    final double assignedSpaceBefore = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);

    final int meldingId = assignMelding(0.01, ReferenceUtil.generateMeldingReference());
    assertNotEquals("Melding ID shouldn't be 0 (even under threshold a Melding can be done).", 0, meldingId);

    final double assignedSpaceAfter = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);
    assertEquals("Assigned space after melding is done should still be " + assignedSpaceBefore, assignedSpaceBefore, assignedSpaceAfter, 1E-5);
  }

  @Test
  public void testInsertMeldingAboveThresholds() throws SQLException, AeriusException {
    // the above threshold exception should only occur when the threshold is actually the same as the PRONOUNCEMENT_THRESHOLD_VALUE...
    // update one receptor to trigger an update of the threshold value to the right value.
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_1, 0.95);
    NoticeModifyRepository.updatePermitThresholdValues(getRegConnection());

    try {
      assignMelding(1000, ReferenceUtil.generateMeldingReference());
      fail("Expected an exception above threshold");
    } catch (final AeriusException e) {
      assertEquals("Melding should be above permit threshold.", Reason.MELDING_ABOVE_PERMIT_THRESHOLD, e.getReason());
    }
  }

  @Test
  public void testInsertMeldingExceedsSpace() throws SQLException, AeriusException {
    try {
      assignMelding(1000, ReferenceUtil.generateMeldingReference());
      fail("Expected an exception when exceeding space");
    } catch (final AeriusException e) {
      assertEquals("Melding should not fit.", Reason.MELDING_DOES_NOT_FIT, e.getReason());
    }
  }

  @Test
  public void testInsertMeldingGoldylock() throws SQLException, AeriusException {
    final double assignedSpaceBefore = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);

    final int meldingId = assignMelding(1, ReferenceUtil.generateMeldingReference());
    assertNotEquals("Melding ID shouldn't be 0 (it's a melding).", 0, meldingId);
    final double assignedSpaceAfter = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);
    assertNotEquals("Assigned space after melding is done should be different", assignedSpaceBefore, assignedSpaceAfter, 1E-5);
  }

  @Test(expected = AeriusException.class)
  public void testInsertDuplicateMelding() throws SQLException, AeriusException {
    final String reference = ReferenceUtil.generateMeldingReference();
    int meldingId = assignMelding(1, reference);
    assertNotEquals("Melding ID shouldn't be 0 (it's a melding).", 0, meldingId);
    meldingId = assignMelding(0.5, reference);
    fail("Expected a duplicate key violation at this point");
  }

  @Test
  public void testInsertMeldingComparison() throws SQLException, AeriusException {
    final double assignedSpaceBefore = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);
    final Notice notice = new Notice();
    TestDomain.fillExampleRequest(notice);
    final String reference = ReferenceUtil.generateMeldingReference();
    notice.getScenarioMetaData().setReference(reference);
    NoticeModifyRepository.insertMelding(getRegConnection(), notice, getExampleUserProfile(), getFakeInsertRequestFile());
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());
    final Calculation calculationOne = getCalculation(1);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.CURRENT, calculationOne.getCalculationId(), notice);
    final Calculation calculationTwo = getCalculation(1.5);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationTwo.getCalculationId(), notice);
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getRegConnection(), calculationTwo.getCalculationId(),
        calculationOne.getCalculationId(), true);

    NoticeModifyRepository.assignMelding(getRegConnection(), notice, getExampleUserProfile());
    assertNotEquals("Melding ID shouldn't be 0 (comparisons should be possible).", 0, notice.getId());

    final double assignedSpaceAfter = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);
    assertNotEquals("Assigned space after melding is done should be different", assignedSpaceBefore, assignedSpaceAfter, 1E-5);
  }

  @Test
  public void testInsertMeldingComparisonDecrease() throws SQLException, AeriusException {
    final double assignedSpaceBefore = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);
    final Notice notice = new Notice();
    TestDomain.fillExampleRequest(notice);
    final String reference = ReferenceUtil.generateMeldingReference();
    notice.getScenarioMetaData().setReference(reference);
    NoticeModifyRepository.insertMelding(getRegConnection(), notice, getExampleUserProfile(), getFakeInsertRequestFile());
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());
    final Calculation calculationOne = getCalculation(1);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.CURRENT, calculationOne.getCalculationId(), notice);
    final Calculation calculationTwo = getCalculation(0.5);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationTwo.getCalculationId(), notice);
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());

    NoticeModifyRepository.assignMelding(getRegConnection(), notice, getExampleUserProfile());
    assertNotEquals("Melding ID shouldn't be 0 (decrease with comparisons should be possible).", 0, notice.getId());

    final double assignedSpaceAfter = getAssignedDevelopmentSpace(RECEPTOR_POINT_ID_1, SegmentType.PERMIT_THRESHOLD);
    assertEquals("Assigned space after melding is done should still be " + assignedSpaceBefore, assignedSpaceBefore, assignedSpaceAfter, 1E-5);
  }

  @Test
  public void testUpdatePermitThresholdValues() throws SQLException {
    final int auditInitial = amountOfAuditTrailsPresent();

    final double defaultThreshold = ConstantRepository.getDouble(getRegConnection(), ConstantsEnum.DEFAULT_PERMIT_THRESHOLD_VALUE);
    final double lowerThreshold = ConstantRepository.getDouble(getRegConnection(), ConstantsEnum.PRONOUNCEMENT_THRESHOLD_VALUE);

    final Map<Integer, Double> valuesBefore = readPermitThresholdValues();
    assertTrue("Area " + ASSESSMENT_AREA_ID + " should exist in the list", valuesBefore.containsKey(ASSESSMENT_AREA_ID));
    assertEquals("Area " + ASSESSMENT_AREA_ID + " should not have a lower threshold yet", defaultThreshold, valuesBefore.get(ASSESSMENT_AREA_ID), EPSILON);

    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_1, 0.95);
    NoticeModifyRepository.updatePermitThresholdValues(getRegConnection());

    final Map<Integer, Double> valuesAfter = readPermitThresholdValues();
    for (final Entry<Integer, Double> entry : valuesAfter.entrySet()) {
      if (entry.getKey() == ASSESSMENT_AREA_ID) {
        // in case of the locked area the value should be the new lowerThreshold
        assertEquals("Permit threshold value for area " + ASSESSMENT_AREA_ID + " should be " + lowerThreshold, lowerThreshold, entry.getValue(), EPSILON);
      } else {
        assertEquals("Permit threshold value should stay the same ", valuesBefore.get(entry.getKey()), entry.getValue(), EPSILON);
      }
    }

    assertEquals("Permit threshold value list before and after should be same size", valuesBefore.size(), valuesAfter.size());
    assertAuditTrail(auditInitial, ASSESSMENT_AREA_ID, valuesBefore, valuesAfter);
  }

  @Test
  public void testUpdatePermitThresholdValuesNoUnlock() throws SQLException {
    final double lowerThreshold = ConstantRepository.getDouble(getRegConnection(), ConstantsEnum.PRONOUNCEMENT_THRESHOLD_VALUE);

    // 95% utilization is reached, so threshold is lowered
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_1, 0.95);
    NoticeModifyRepository.updatePermitThresholdValues(getRegConnection());

    final Map<Integer, Double> valuesBefore = readPermitThresholdValues();
    assertTrue("Area " + ASSESSMENT_AREA_ID + " should exist in the list", valuesBefore.containsKey(ASSESSMENT_AREA_ID));
    assertEquals("Area " + ASSESSMENT_AREA_ID + " should have a lower threshold", lowerThreshold, valuesBefore.get(ASSESSMENT_AREA_ID), EPSILON);

    // Back to 90% utilization, but threshold should still be lowered
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_POINT_ID_1, 0.90);
    NoticeModifyRepository.updatePermitThresholdValues(getRegConnection());

    final Map<Integer, Double> valuesAfter = readPermitThresholdValues();
    assertTrue("Area " + ASSESSMENT_AREA_ID + " should exist in the list", valuesBefore.containsKey(ASSESSMENT_AREA_ID));
    assertEquals("Area " + ASSESSMENT_AREA_ID + " should still have a lower threshold", lowerThreshold, valuesBefore.get(ASSESSMENT_AREA_ID), EPSILON);

    for (final Entry<Integer, Double> entry : valuesAfter.entrySet()) {
      assertEquals("Permit threshold value should stay the same ", valuesBefore.get(entry.getKey()), entry.getValue(), EPSILON);
    }

    assertEquals("Permit threshold value list before and after should be same size", valuesBefore.size(), valuesAfter.size());
  }

  private int amountOfAuditTrailsPresent() throws SQLException {
    int auditInitial = 0;
    try (final PreparedStatement ps = getRegConnection().prepareStatement(READ_PERMIT_THRESHOLD_VALUES_AUDIT_TRAIL.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        auditInitial++;
      }
    }
    return auditInitial;
  }

  private void assertAuditTrail(final int auditInitial, final int lockedAreaId, final Map<Integer, Double> valuesBefore, final Map<Integer, Double> valuesAfter)
      throws SQLException {
    // Check audit trail
    int auditChanged = 0;
    int auditAmount = 0;
    try (final PreparedStatement ps = getRegConnection().prepareStatement(READ_PERMIT_THRESHOLD_VALUES_AUDIT_TRAIL.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        final int areaId = QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs);
        if (areaId == lockedAreaId) {
          final double oldValue = QueryUtil.getDouble(rs, RepositoryAttribute.OLD_VALUE);
          final double newValue = QueryUtil.getDouble(rs, RepositoryAttribute.NEW_VALUE);
          assertEquals("Old value must match audit trail", valuesBefore.get(areaId), oldValue, EPSILON);
          assertEquals("New value must match audit trail", valuesAfter.get(areaId), newValue, EPSILON);
          auditChanged++;
        }
        auditAmount++;
      }
    }
    assertEquals("Audit trail changes must match with the number of changed values", 1, auditChanged);
    assertEquals("Audit trail size must have increased by the number of changed values", auditInitial + auditChanged, auditAmount);
  }

  @Test
  public void testConfirmNotices() throws SQLException, AeriusException {
    assignMelding(1, ReferenceUtil.generateMeldingReference());
    final ArrayList<Integer> noConfirms = NoticeRepository.getConfirmableNoticesIds(getRegConnection(), 15);
    assertNotNull("Notices list shouldn't be null", noConfirms);
    assertTrue("Notices list for authority 15 should be empty", noConfirms.isEmpty());

    final ArrayList<Integer> beforeConfirm = NoticeRepository.getConfirmableNoticesIds(getRegConnection(), 7);
    assertNotNull("Notices list shouldn't be null", beforeConfirm);
    assertFalse("Notices list for authority 7 shouldn't be empty", beforeConfirm.isEmpty());

    final List<Integer> allNotices = new ArrayList<>();
    for (final Notice notice : NoticeRepository.getNotices(getRegConnection(), 10000, 0, new NoticeFilter())) {
      allNotices.add(notice.getId());
    }
    NoticeModifyRepository.confirmNotices(getRegConnection(), allNotices, getExampleUserProfile());

    final ArrayList<Integer> afterConfirm = NoticeRepository.getConfirmableNoticesIds(getRegConnection(), 3);
    assertTrue("Notices list for authority 3 should now be empty", afterConfirm.isEmpty());
  }

  private Map<Integer, Double> readPermitThresholdValues() throws SQLException {
    final Map<Integer, Double> values = new HashMap<>();
    try (final PreparedStatement ps = getRegConnection().prepareStatement(READ_PERMIT_THRESHOLD_VALUES.get())) {
      final ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        values.put(QueryAttribute.ASSESSMENT_AREA_ID.getInt(rs), QueryAttribute.VALUE.getDouble(rs));
      }
    }
    assertFalse("No values in permit_threshold_values", values.isEmpty());
    return values;
  }

  private Notice insertMelding(final double scale, final String reference) throws SQLException, AeriusException {
    final Notice notice = new Notice();
    TestDomain.fillExampleRequest(notice);
    notice.getScenarioMetaData().setReference(reference);
    NoticeModifyRepository.insertMelding(getRegConnection(), notice, getExampleUserProfile(), getFakeInsertRequestFile());
    final Calculation calculation = getCalculation(scale);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculation.getCalculationId(), notice);
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());
    return notice;
  }

  private int assignMelding(final double scale, final String reference) throws SQLException, AeriusException {
    final Notice notice = insertMelding(scale, reference);
    NoticeModifyRepository.assignMelding(getRegConnection(), notice, getExampleUserProfile());
    return notice.getId();
  }

  private Calculation getCalculation(final double scale) throws SQLException {
    Calculation calculation = new Calculation();
    calculation.setYear(TestDomain.YEAR);
    calculation = CalculationRepository.insertCalculation(getRegConnection(), calculation, null);
    final PartialCalculationResult cr = getExampleOPSOutputData(scale);
    cr.setCalculationId(calculation.getCalculationId());
    CalculationRepository.insertCalculationResults(getRegConnection(), calculation.getCalculationId(), cr.getResults());
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getRegConnection(), calculation.getCalculationId(), 0, true);
    return calculation;
  }

  protected PartialCalculationResult getExampleOPSOutputData(final double scale) {
    final PartialCalculationResult result = new PartialCalculationResult();

    final AeriusResultPoint rp1 = new AeriusResultPoint();
    rp1.setId(RECEPTOR_POINT_ID_1);
    rp1.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 0.1 * scale);
    result.add(rp1);

    final AeriusResultPoint rp2 = new AeriusResultPoint();
    rp2.setId(RECEPTOR_POINT_ID_2);
    rp2.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 0.05 * scale);
    result.add(rp2);

    final AeriusResultPoint rp3 = new AeriusResultPoint();
    rp3.setId(RECEPTOR_POINT_ID_3);
    rp3.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 0.01 * scale);
    result.add(rp3);

    return result;
  }

}
