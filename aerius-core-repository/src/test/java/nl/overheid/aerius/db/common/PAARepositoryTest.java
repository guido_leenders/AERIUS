/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportDepositionInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportHabitatInfo;
import nl.overheid.aerius.shared.domain.register.SegmentType;

/**
 * Test class for {@link PAARepository}.
 */
public class PAARepositoryTest extends CalculationRepositoryTestBase {

  @Test
  public void testDetermineExportInfoSingleCalculation() throws SQLException {
    insertCalculationResults();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), 0, true);
    try (final Connection connection = getCalcConnection()) {
      final ArrayList<PDFExportAssessmentAreaInfo> results =
          PAARepository.determineExportInfo(connection, calculation.getCalculationId(), null, SegmentType.PROJECTS, true);
      assertNotNull("Resulting list", results);
      assertEquals("Resulting list size", 2, results.size());
      for (final PDFExportAssessmentAreaInfo assessmentAreaInfo : results) {
        validateAssessmentAreaInfo(assessmentAreaInfo, true);
      }
    }
  }

  @Test
  public void testDetermineExportInfoComparisonCalculation() throws SQLException {
    //first the current situation
    insertCalculationResults();
    final int currentCalculationId = calculation.getCalculationId();
    //next the proposed (having higher results)
    newCalculation();
    calculationResultScale = 1.2;
    insertCalculationResults();
    final int proposedCalculationId = calculation.getCalculationId();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), proposedCalculationId, currentCalculationId, true);
    try (final Connection connection = getCalcConnection()) {
      final ArrayList<PDFExportAssessmentAreaInfo> results = PAARepository.determineExportInfo(connection, proposedCalculationId,
          currentCalculationId, SegmentType.PROJECTS, true);
      assertNotNull("Resulting list", results);
      assertEquals("Resulting list size", 2, results.size());
      for (final PDFExportAssessmentAreaInfo assessmentAreaInfo : results) {
        validateAssessmentAreaInfo(assessmentAreaInfo, false);
      }
    }
  }

  @Test
  public void testGetDetailedExtents() throws SQLException {
    insertCalculationResults();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), calculation.getCalculationId(), 0, true);
    try (final Connection connection = getCalcConnection()) {
      final ArrayList<BBox> results = PAARepository.getDetailPagesExtents(connection, calculation.getCalculationId(), null, 7000, 9000);
      validateBoundingBoxResults(results);
    }
  }

  @Test
  public void testGetDetailedExtentsComparison() throws SQLException {
    //first the current situation
    insertCalculationResults();
    final int currentCalculationId = calculation.getCalculationId();
    //next the proposed (having higher results)
    newCalculation();
    calculationResultScale = 1.2;
    insertCalculationResults();
    final int proposedCalculationId = calculation.getCalculationId();
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getCalcConnection(), currentCalculationId, proposedCalculationId, true);
    try (final Connection connection = getCalcConnection()) {
      final ArrayList<BBox> results = PAARepository.getDetailPagesExtents(connection, currentCalculationId, proposedCalculationId, 7000, 9000);
      //not changing any of the actual points, so should be same result.
      validateBoundingBoxResults(results);
    }
  }

  private void validateBoundingBoxResults(final ArrayList<BBox> results) {
    assertNotNull("Resulting list", results);
    assertEquals("Resulting list size", 3, results.size());
//    BBox [minX=111162.432636075, maxX=118162.432636075, minY=460978.230943111, maxY=469978.230943111]
//    BBox [minX=111162.432636075, maxX=118162.432636075, minY=550978.230943111, maxY=559978.230943111]
//    BBox [minX=181162.432636075, maxX=188162.432636075, minY=460978.230943111, maxY=469978.230943111]
    assertEquals("min x first result", 111162, results.get(0).getMinX(), 1);
    assertEquals("max x first result", 118162, results.get(0).getMaxX(), 1);
    assertEquals("min y first result", 460978, results.get(0).getMinY(), 1);
    assertEquals("max y first result", 469978, results.get(0).getMaxY(), 1);
    assertEquals("min x second result", 111162, results.get(1).getMinX(), 1);
    assertEquals("max x second result", 118162, results.get(1).getMaxX(), 1);
    assertEquals("min y second result", 550978, results.get(1).getMinY(), 1);
    assertEquals("max y second result", 559978, results.get(1).getMaxY(), 1);
    assertEquals("min x third result", 181162, results.get(2).getMinX(), 1);
    assertEquals("max x third result", 188162, results.get(2).getMaxX(), 1);
    assertEquals("min y third result", 460978, results.get(2).getMinY(), 1);
    assertEquals("max y third result", 469978, results.get(2).getMaxY(), 1);
  }

  private void validateAssessmentAreaInfo(final PDFExportAssessmentAreaInfo assessmentAreaInfo, final boolean singleCalculation) {
    assertNotNull("Name for assessment area", assessmentAreaInfo.getName());
    assertNotNull("Bounds (or bbox) for assessment area", assessmentAreaInfo.getBounds());
    assertNotNull("Country for assessment area", assessmentAreaInfo.getCountry());
    assertNotNull("Country code for assessment area", assessmentAreaInfo.getCountry().getCode());
    assertNotNull("Country name for assessment area", assessmentAreaInfo.getCountry().getName());

    assertNotNull("Directive for assessment area", assessmentAreaInfo.getDirective());
    assertNotNull("Deposition info for assessment area", assessmentAreaInfo.getDepositionInfo());
    validateMainDepositionInfo(assessmentAreaInfo.getDepositionInfo(), "assessment area", singleCalculation);
    assertNotEquals("max background deposition for assessment area", 0.0, assessmentAreaInfo.getDepositionInfo().getMaxBackgroundDeposition(), 1E-5);

    assertNotNull("Maximum delta point for assessment area", assessmentAreaInfo.getMaxDeltaPoint());
    assertFalse("habitat infos for assessment area", assessmentAreaInfo.getHabitatInfos().isEmpty());
    for (final PDFExportHabitatInfo habitatInfo : assessmentAreaInfo.getHabitatInfos()) {
      validateHabitatInfo(habitatInfo, singleCalculation);
    }
    assertFalse("show rules for assessment area", assessmentAreaInfo.getShowRules().isEmpty());
  }

  private void validateHabitatInfo(final PDFExportHabitatInfo habitatInfo, final boolean singleCalculation) {
    assertNotNull("Name for habitat", habitatInfo.getName());
    assertNotNull("Deposition info for habitat", habitatInfo.getDepositionInfo());
    validateMainDepositionInfo(habitatInfo.getDepositionInfo(), "habitat", singleCalculation);
    assertFalse("show rules for habitat", habitatInfo.getShowRules().isEmpty());
  }

  private void validateMainDepositionInfo(final PDFExportDepositionInfo depositionInfo, final String description, final boolean singleCalculation) {
    if (singleCalculation) {
      assertEquals("current demand for max delta for " + description, 0.0, depositionInfo.getCurrentDemandMaxDelta(), 1E-5);
    } else {
      assertNotEquals("current demand for max delta for " + description, 0.0, depositionInfo.getCurrentDemandMaxDelta(), 1E-5);
    }
    assertNotEquals("proposed demand for max delta for " + description, 0.0, depositionInfo.getProposedDemandMaxDelta(), 1E-5);
    assertNotEquals("max delta demand for " + description, 0.0, depositionInfo.getMaxDeltaDemand(), 1E-5);
    // the delta for proposed demand also works for 1 situation
    assertNotEquals("max proposed demand for " + description, 0.0, depositionInfo.getProposedDemandMaxDelta(), 1E-5);
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

}
