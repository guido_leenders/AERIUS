/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.Notice;
import nl.overheid.aerius.shared.domain.register.NoticeFilter;
import nl.overheid.aerius.shared.domain.register.NoticeSortableAttribute;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.sector.ScenarioSectorInformation;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

/**
 * Test class for querying the database for {@link NoticeFilter}.
 */
public class NoticeRepositoryFilterTest extends RequestFilterTestBase<NoticeFilter, Notice, NoticeSortableAttribute> {

  private static final String UPDATE_NOTICE_INSERT_DATE =
      "UPDATE requests SET insert_date = ? WHERE request_id = ?";

  private int olderNoticeId;
  private String newerNoticeReference;

  @Override
  void init() throws SQLException, AeriusException {
    //assure 2 notices in the DB.
    final Sector olderNoticeSector = new Sector();
    olderNoticeSector.setSectorId(3210);
    final Point olderNoticePoint = new Point(TestDomain.XCOORD_2, TestDomain.YCOORD_2);

    olderNoticeId = insertNotice(olderNoticePoint, new ScenarioSectorInformation(olderNoticeSector, true));

    newerNoticeReference = ReferenceUtil.generateMeldingReference();
    final Point newerNoticePoint = new Point(TestDomain.XCOORD_1, TestDomain.YCOORD_1);
    insertNotice(newerNoticeReference, newerNoticePoint, new ScenarioSectorInformation(getTestSector(), false));

    //assure the olderNotice has a startdate before the other one (have to update by hand).
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(UPDATE_NOTICE_INSERT_DATE)) {
      final Calendar calendar = new GregorianCalendar();
      calendar.add(Calendar.YEAR, -1);
      final Date oldNoticeDate = calendar.getTime();
      ps.setTimestamp(1, new Timestamp(oldNoticeDate.getTime()));
      ps.setInt(2, olderNoticeId);
      ps.executeUpdate();
    }
  }

  @Override
  NoticeFilter getNewFilter() {
    return new NoticeFilter();
  }

  @Override
  List<Notice> getList(final NoticeFilter filter) throws SQLException {
    return NoticeRepository.getNotices(getRegConnection(), Integer.MAX_VALUE, 0, filter);
  }

  @Override
  protected Date getTestDate() {
    final Calendar calendar = new GregorianCalendar();
    calendar.add(Calendar.DATE, -2);
    return calendar.getTime();
  }

  @Override
  Date getDateForRequest(final Notice request) {
    return request.getReceivedDate();
  }

  @Override
  Authority getAuthorityForRequest(final Notice request) {
    //bit lame, but assure a fail when the older notice is found by returning an authority with ID 0.
    return request.getId() == olderNoticeId ? new Authority() : getTestAuthority();
  }

  @Override
  void fillForFullFilter(final NoticeFilter filter) {
    //no-op
  }

  private int insertNotice(final Point point, final ScenarioSectorInformation sectorInformation) throws SQLException, AeriusException {
    return insertNotice(ReferenceUtil.generateMeldingReference(), point, sectorInformation);
  }

  private int insertNotice(final String reference, final Point point, final ScenarioSectorInformation sectorInformation) throws SQLException,
      AeriusException {
    final Notice notice = new Notice();
    TestDomain.fillExampleRequest(notice);
    notice.setSector(sectorInformation.getMainSector());
    notice.setMultipleSectors(sectorInformation.isMultipleSectors());
    notice.getScenarioMetaData().setReference(reference);
    notice.setPoint(point);
    final int meldingId = NoticeModifyRepository.insertMelding(getRegConnection(), notice, getExampleUserProfile(), getFakeInsertRequestFile());
    addCalculation(notice);
    RequestModifyRepository.updateRequestToQueued(getRegConnection(), notice, getExampleUserProfile());
    NoticeModifyRepository.assignMelding(getRegConnection(), notice, getExampleUserProfile());
    return meldingId;
  }

  @Override
  Authority getTestAuthority() {
    final Authority newNoticeAuthority = new Authority();
    newNoticeAuthority.setId(11);
    return newNoticeAuthority;
  }

  @Override
  Province getTestProvince() {
    final Province newNoticeProvince = new Province();
    newNoticeProvince.setProvinceId(7);
    return newNoticeProvince;
  }

  @Override
  Sector getTestSector() {
    final Sector newerNoticeSector = new Sector();
    newerNoticeSector.setSectorId(1400);
    return newerNoticeSector;
  }

  @Override
  int getNewerRequestCalculationId() throws SQLException {
    return RequestRepository.getCalculationIds(getRegConnection(), newerNoticeReference).get(SituationType.PROPOSED);
  }

  @Override
  NoticeSortableAttribute[] getPossibleSortAttributes() {
    return NoticeSortableAttribute.values();
  }

}
