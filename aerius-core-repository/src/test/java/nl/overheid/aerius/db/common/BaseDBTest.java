/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.TestPMF;
import nl.overheid.aerius.db.Transaction;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Base class for Database tests. This class initializes the database connection
 * configuration in @Before state and closes the connection in @After.
 */
public class BaseDBTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(BaseDBTest.class);

  protected static ReceptorGridSettings RECEPTOR_GRID_SETTINGS;
  protected static ReceptorUtil RECEPTOR_UTIL;
  protected static ExecutorService EXECUTOR;

  private static final String TEST_DATABASE_PROPERTIES = "testdatabase.properties";
  private static final TestPMF CALCULATOR_PMF = new TestPMF(true);
  private static final TestPMF MELDING_PMF = new TestPMF(true);
  private static final TestPMF REGISTER_PMF = new TestPMF(true);
  private static Properties PROPS;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    EXECUTOR = Executors.newSingleThreadExecutor();
    PROPS = new Properties();
    PROPS.setProperty("database.username", System.getProperty("database.username", ""));
    PROPS.setProperty("database.password", System.getProperty("database.password", ""));
    PROPS.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(TEST_DATABASE_PROPERTIES));
    initPMF(CALCULATOR_PMF, PROPS.getProperty("database.calculator.URL"), PROPS, ProductType.CALCULATOR);
    initPMF(MELDING_PMF, PROPS.getProperty("database.calculator.URL"), PROPS, ProductType.MELDING);
    initPMF(REGISTER_PMF, PROPS.getProperty("database.register.URL"), PROPS, ProductType.REGISTER);
    RECEPTOR_GRID_SETTINGS = ReceptorGridSettingsRepository.getReceptorGridSettings(getCalcPMF());
    RECEPTOR_UTIL = new ReceptorUtil(RECEPTOR_GRID_SETTINGS);
  }

  @AfterClass
  public static void afterClass() {
    EXECUTOR.shutdown();
  }

  @Before
  public void setUp() throws Exception {
    // When someone uses this class from a unit test, we need to ensure transaction management still
    // results in a clean unit test database.
    Transaction.setNested();
  }

  @After
  public void tearDown() throws Exception {
    CALCULATOR_PMF.close();
    MELDING_PMF.close();
    REGISTER_PMF.close();
  }

  private static void initPMF(final TestPMF pmf, final String databaseURL, final Properties props, final ProductType productType) throws SQLException {
    pmf.setJdbcURL(databaseURL);
    pmf.setDbUsername(props.getProperty("database.username"));
    pmf.setDbPassword(props.getProperty("database.password"));
    pmf.setProductType(productType);
  }

  protected static Properties getProperties() {
    return PROPS;
  }

  protected static TestPMF getCalcPMF() {
    return CALCULATOR_PMF;
  }

  protected static TestPMF getMeldPMF() {
    return MELDING_PMF;
  }

  protected static TestPMF getRegPMF() {
    return REGISTER_PMF;
  }

  protected static TestPMF getPMF(final ProductType productType) throws SQLException {
    switch (productType) {
    case CALCULATOR:
    case SCENARIO:
      return getCalcPMF();
    case MELDING:
      return getMeldPMF();
    case REGISTER:
      return getRegPMF();
    default:
      throw new IllegalArgumentException();
    }
  }

  protected Connection getCalcConnection() throws SQLException {
    return getConnection(CALCULATOR_PMF);
  }

  protected Connection getMeldConnection() throws SQLException {
    return getConnection(MELDING_PMF);
  }

  protected Connection getRegConnection() throws SQLException {
    return getConnection(REGISTER_PMF);
  }

  protected Connection getConnection(final ProductType productType) throws SQLException {
    return getConnection(getPMF(productType));
  }

  protected static DBMessagesKey getCalcMessagesKey() {
    return new DBMessagesKey(ProductType.CALCULATOR, LocaleUtils.getDefaultLocale());
  }

  protected static DBMessagesKey getRegMessagesKey() {
    return new DBMessagesKey(ProductType.REGISTER, LocaleUtils.getDefaultLocale());
  }

  private static Connection getConnection(final TestPMF pmf) throws SQLException {
    return pmf.getConnection();
  }

  /**
   * Convenience method to get an input stream on a file. The caller should close
   * the stream.
   *
   * @param fileName file to read, it's relative to the calling class package
   * @return input stream to file
   * @throws FileNotFoundException
   */
  protected InputStream getFileInputStream(final String fileName) throws FileNotFoundException {
    final InputStream is = getClass().getResourceAsStream(fileName);
    if (is == null) {
      throw new FileNotFoundException("Input file not found:" + fileName);
    }
    return new BufferedInputStream(is);
  }

  /**
   * Execute debug query which will print the data (to stderr) as generic as possible.
   * This is needed as debugging the database is hard when using transactions, this allows us to query the database inside the current transaction.
   * @param con The connection to use.
   * @param query The query to use for the preparedStatement.
   * @param objects The objects to set in the preparedStatement.
   * @throws SQLException On DB error.
   */
  protected void executeDebugQuery(final Connection con, final String query, final Object... objects) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(query)) {
      QueryUtil.setValues(stmt, objects);

      final ResultSet rst = stmt.executeQuery();
      final int columnCount = rst.getMetaData().getColumnCount();
      while (rst.next()) {
        final String[] row = new String[columnCount];
        for (int i = 0; i < columnCount; i++) {
          row[i] = rst.getString(i + 1);
        }

        LOGGER.error("[{}]", String.join(" | ", row));
      }
    }
  }
}
