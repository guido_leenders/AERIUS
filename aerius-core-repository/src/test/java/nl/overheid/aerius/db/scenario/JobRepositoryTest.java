/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.scenario;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.CalculationRepositoryTestBase;
import nl.overheid.aerius.db.scenario.JobRepository.RepositoryAttribute;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.scenario.JobProgress;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;

public class JobRepositoryTest extends CalculationRepositoryTestBase {

  private static final String TEST_API_KEY = "wnaR9FavGRGv8RXCmdfXKEqeIt1DTZUS";
  private static final String TEST_EMAIL = "test@example.com";

  @Test
  public void testGetUnexistingJobId() throws SQLException {
    final int jobId = JobRepository.getJobId(getCalcConnection(), UUID.randomUUID().toString());
    assertEquals("No job id should be found", 0, jobId);
  }

  @Test
  public void testGetUnexistingJobProgress() throws SQLException {
    final JobProgress jp = JobRepository.getProgress(getCalcConnection(), UUID.randomUUID().toString());
    assertNull("No job progress should be found", jp);
  }

  @Test
  public void testCreateEmptyJobWithoutCalculations() throws SQLException, AeriusException {
    final ScenarioUser user = setupUser();
    final String correlationIdentifier = UUID.randomUUID().toString();
    final int jobId = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier);
    assertTrue("JobId must be > 0", jobId > 0);

    final JobProgress jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    validateEmptyProgress(jp, jobId);
    assertSame("State may must be initialized", JobState.INITIALIZED, jp.getState());
    assertNull("Creation time may not be set", jp.getCreationDateTime());
    assertNull("Name should not be set", jp.getName());

    final int testJobId = JobRepository.getJobId(getCalcConnection(), correlationIdentifier);
    assertEquals("JobId must match", jobId, testJobId);
  }

  @Test
  public void testCreateEmptyJobWithCalculations() throws SQLException, AeriusException {
    final ScenarioUser user = setupUser();
    final String correlationIdentifier = UUID.randomUUID().toString();
    final int jobId = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier, Arrays.asList(calculation.getCalculationId()));

    final JobProgress jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    validateEmptyProgress(jp, jobId);
    assertEquals("State must be initialized", JobState.RUNNING, jp.getState());
    assertNotNull("Creation time must be set", jp.getCreationDateTime());
    assertFalse("Creation time can't be in the future", jp.getCreationDateTime().getTime() > new Date().getTime());
    assertTrue("Creation time must match that of the calculation", calculation.getCreationDate().getTime() < jp.getCreationDateTime().getTime());
  }

  @Test
  public void testUpdateJobProgress() throws SQLException, AeriusException {
    final ScenarioUser user = setupUser();
    final String correlationIdentifier = UUID.randomUUID().toString();
    final String testName = "My very long and weird job name.. GI%4j5h4g4jgR$_ 43-A";
    final int jobId = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier, testName);
    JobProgress jp;

    JobRepository.increaseHexagonCounter(getCalcConnection(), correlationIdentifier, 1);
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertEquals("Type must be a calculation", JobType.CALCULATION, jp.getType());
    assertEquals("Hexagon counter must be incremented", 1, jp.getHexagonCount());
    assertEquals("Name must be updated", testName, jp.getName());
    assertNotNull("Start time must be set", jp.getStartDateTime());
    assertFalse("Start time can't be in the future", jp.getStartDateTime().getTime() > new Date().getTime());

    JobRepository.increaseHexagonCounter(getCalcConnection(), correlationIdentifier, 12345);
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertEquals("Hexagon counter must be incremented", 1 + 12345, jp.getHexagonCount());

    JobRepository.setEndTimeToNow(getCalcConnection(), correlationIdentifier);
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertNotNull("End time must be set", jp.getEndDateTime());
    assertFalse("End time can't be in the future", jp.getEndDateTime().getTime() > new Date().getTime());

    JobRepository.setResultUrl(getCalcConnection(), correlationIdentifier, "abc");
    jp = JobRepository.getProgress(getCalcConnection(), correlationIdentifier);
    assertNotNull("Result url must be set", jp.getResultUrl());

    // Ensure getProgressForUser gives same result
    final List<JobProgress> jpl = JobRepository.getProgressForUser(getCalcConnection(), user);
    assertEquals("User must have 1 progress result", 1, jpl.size());
    jp = jpl.get(0);
    assertEquals("Hexagon counter must be incremented", 1 + 12345, jp.getHexagonCount());
    assertNotNull("Start time must be set", jp.getStartDateTime());
    assertFalse("Start time can't be in the future", jp.getStartDateTime().getTime() > new Date().getTime());
    assertNotNull("End time must be set", jp.getEndDateTime());
    assertFalse("End time can't be in the future", jp.getEndDateTime().getTime() > new Date().getTime());
    assertNotNull("Result url must be set", jp.getResultUrl());
    assertNotNull("Key should be present", jp.getKey());
  }

  @Test
  public void testGetCalculationsProgressForUser() throws SQLException, AeriusException {
    List<JobProgress> progresses;

    final ScenarioUser user = setupUser();
    assertJobProgressAmount(user, 0);

    final String correlationIdentifier1 = UUID.randomUUID().toString();
    final String correlationIdentifier2 = UUID.randomUUID().toString();
    final int jobId1 = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier1);
    final int jobId2 = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier2);

    assertJobProgressAmount(user, 2);

    progresses = JobRepository.getProgressForUser(getCalcConnection(), user);
    final JobProgress jp1a = progresses.get(0); // order by job_id
    final JobProgress jp2a = progresses.get(1);
    assertEquals("Hexagon counter must be 0 (1st entry)", 0, jp1a.getHexagonCount());
    assertEquals("Hexagon counter must be 0 (2nd entry)", 0, jp2a.getHexagonCount());
    assertNotNull("Start time must be set (1st entry)", jp1a.getStartDateTime());
    assertNotNull("Start time must be set (2nd entry)", jp2a.getStartDateTime());

    JobRepository.increaseHexagonCounter(getCalcConnection(), correlationIdentifier1, 12345);
    progresses = JobRepository.getProgressForUser(getCalcConnection(), user);
    final JobProgress jp1b = progresses.get(0);
    final JobProgress jp2b = progresses.get(1);
    assertEquals("Hexagon counter must be 0 (1st entry)", 12345, jp1b.getHexagonCount());
    assertEquals("Hexagon counter must still be 0 (2nd entry)", 0, jp2b.getHexagonCount());
    assertNotNull("Start time must be set (2nd entry)", jp1b.getStartDateTime());
    assertNotNull("Start time must be set (2nd entry)", jp2b.getStartDateTime());
  }

  @Test
  public void testRemoveJobsWithMinAge() throws SQLException, AeriusException {
    final ScenarioUser user = setupUser();
    final int jobCountOffset = JobRepository.removeJobsWithMinAge(getCalcConnection(), 5);
    final String correlationIdentifier1 = UUID.randomUUID().toString();
    final String correlationIdentifier2 = UUID.randomUUID().toString();
    final String correlationIdentifier3 = UUID.randomUUID().toString();

    JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier1);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier1, Arrays.asList(calculation.getCalculationId()));
    CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getCalcConnection(), correlationIdentifier1, JobState.COMPLETED);
    newCalculation();
    JobRepository.createJob(getCalcConnection(), user, JobType.REPORT, correlationIdentifier2);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier2, Arrays.asList(calculation.getCalculationId()));
    CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getCalcConnection(), correlationIdentifier2, JobState.COMPLETED);
    newCalculation();
    JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, correlationIdentifier3);
    insertCalculationResults();
    JobRepository.attachCalculations(getCalcConnection(), correlationIdentifier3, Arrays.asList(calculation.getCalculationId()));
    CalculationRepository.updateCalculationState(getCalcConnection(), calculation.getCalculationId(), CalculationState.COMPLETED);
    JobRepository.updateJobStatus(getCalcConnection(), correlationIdentifier3, JobState.COMPLETED);

    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -10); // so the timestamps created have the amount of days + 10 minutes of age
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date endTimeJob1 = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date startTimeJob2 = calendar.getTime();
    calendar.add(Calendar.DAY_OF_YEAR, -1);
    final Date endTimeJob3 = calendar.getTime();

    JobRepository.updateField(getCalcConnection(), correlationIdentifier1, RepositoryAttribute.END_TIME, new Timestamp(endTimeJob1.getTime()));
    JobRepository.updateField(getCalcConnection(), correlationIdentifier2, RepositoryAttribute.START_TIME, new Timestamp(startTimeJob2.getTime()));
    JobRepository.updateField(getCalcConnection(), correlationIdentifier2, RepositoryAttribute.END_TIME, null);
    JobRepository.updateField(getCalcConnection(), correlationIdentifier3, RepositoryAttribute.END_TIME, new Timestamp(endTimeJob3.getTime()));

    // start point - I am expecting 3 jobs
    assertJobProgressAmount(user, 3);

    assertEquals("There should be 0 removals", jobCountOffset, JobRepository.removeJobsWithMinAge(getCalcConnection(), 5));
    assertJobProgressAmount(user, 3);

    assertEquals("There should be 1 removal", jobCountOffset + 1, JobRepository.removeJobsWithMinAge(getCalcConnection(), 3));
    assertJobProgressAmount(user, 2);

    assertEquals("There should be 2 removals", jobCountOffset + 2, JobRepository.removeJobsWithMinAge(getCalcConnection(), 0));
    assertJobProgressAmount(user, 0);
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

  private ScenarioUser setupUser() throws SQLException, AeriusException {
    final ScenarioUser user = new ScenarioUser();
    user.setApiKey(TEST_API_KEY);
    user.setEmailAddress(TEST_EMAIL);
    ScenarioUserRepository.createUser(getCalcConnection(), user);
    return user;
  }

  private void assertJobProgressAmount(final ScenarioUser user, final int amount) throws SQLException {
    assertEquals("Couldn't find the expected amount of jobs", amount, JobRepository.getProgressForUser(getCalcConnection(), user).size());
  }

  private void validateEmptyProgress(final JobProgress jp, final int checkJobId) throws SQLException {
    assertNotNull("Job progress should be found", jp);
    assertEquals("Job type must be a calculation", JobType.CALCULATION, jp.getType());
    assertEquals("Hexagon counter must be 0", 0, jp.getHexagonCount());
    assertNotNull("Start time must be set", jp.getStartDateTime());
    assertNull("End time may not be set", jp.getEndDateTime());
    assertNull("Result url may not be set", jp.getResultUrl());
    assertNotNull("jobKey should be present", jp.getKey());
  }

}
