/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.db.TestPMF;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.LayerTMSProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.geo.shared.MultiLayerProps;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 *
 */
@RunWith(Parameterized.class)
public class LayerRepositoryTest extends BaseDBTest {

  private static final int TEST_LEGEND_ID = 1;
  private static final String TEST_WMS_LAYER_NAME = "wms_nature_areas_view";
  private static final String TEST_TMS_LAYER_NAME = "brtachtergrondkaart";

  private final TestPMF pmf;
  private final DBMessagesKey messagesKey;

  /**
   * Initialize test with version and file to test.
   */
  public LayerRepositoryTest(final TestPMF pmf, final String testDescription, final DBMessagesKey messagesKey) {
    this.pmf = pmf;
    this.messagesKey = messagesKey;
  }

  @Parameters(name = "Database {1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> connectionTests = new ArrayList<>();
    final Object[] calculator = new Object[3];
    calculator[0] = getCalcPMF();
    calculator[1] = "Calculator";
    calculator[2] = new DBMessagesKey(ProductType.CALCULATOR, LocaleUtils.getDefaultLocale());
    connectionTests.add(calculator);
    final Object[] register = new Object[3];
    register[0] = getRegPMF();
    register[1] = "Register";
    register[2] = new DBMessagesKey(ProductType.REGISTER, LocaleUtils.getDefaultLocale());
    connectionTests.add(register);
    return connectionTests;
  }

  @Test
  public void testGetLayers() throws SQLException, AeriusException {
    final ArrayList<LayerProps> layers = LayerRepository.getLayers(pmf.getConnection(), messagesKey);
    assertNotNull("Returned list", layers);
    assertFalse("Returned list shouldn't be empty", layers.isEmpty());
  }

  @Test
  public void testGetLegend() throws SQLException {
    final Legend legend = LayerRepository.getLegend(pmf.getConnection(), TEST_LEGEND_ID);
    assertTrue("Should be a legend with ID " + TEST_LEGEND_ID + ", but found " + legend, legend instanceof ColorRangesLegend);
    final ColorRangesLegend colorLegend = (ColorRangesLegend) legend;
    assertFalse("First one isn't a hexagon-kind", colorLegend.isHexagon());
    assertEquals("Nr of legend names", 4, colorLegend.getLegendNames().length);
    assertEquals("Nr of colors", 4, colorLegend.getColors().length);
    assertEquals("First legend name", "Habitatrichtlijn", colorLegend.getLegendNames()[0]);
    assertEquals("First legend color", "#E6E600", colorLegend.getColors()[0]);
    assertEquals("Third legend name", "Vogelrichtlijn, Habitatrichtlijn", colorLegend.getLegendNames()[2]);
    assertEquals("Third legend color", "#D1FF73", colorLegend.getColors()[2]);
  }

  @Test
  public void testGetLayer() throws SQLException, AeriusException {
    LayerProps layerProps;
    if (pmf.getProductType() != ProductType.REGISTER) {
      final String actualWMSLayerName = pmf.getProductType().getLayerName(TEST_WMS_LAYER_NAME);
      layerProps = LayerRepository.getLayer(pmf.getConnection(), actualWMSLayerName, messagesKey);
      assertTrue("Should be a WMS layer with name " + TEST_WMS_LAYER_NAME + ", but found " + layerProps, layerProps instanceof LayerWMSProps);
      final LayerWMSProps wmsProps = (LayerWMSProps) layerProps;
      assertEquals("Name", actualWMSLayerName, wmsProps.getName());
      assertEquals("ID", 11, wmsProps.getId());
      assertEquals("min scale", 1504000, wmsProps.getMinScale(), 1E-6);
      assertEquals("max scale", 0, wmsProps.getMaxScale(), 1E-6);
      assertEquals("max scale", 0.8, wmsProps.getOpacity(), 1E-6);
      assertNotNull("Legend", wmsProps.getLegend());
      assertTrue("Returned WMS should be enabled", wmsProps.isEnabled());
      assertNotNull("Capabilities", wmsProps.getCapabilities());
      assertNotNull("URL of capabilities", wmsProps.getCapabilities().getUrl());
    }

    layerProps = LayerRepository.getLayer(pmf.getConnection(), TEST_TMS_LAYER_NAME, messagesKey);
    assertTrue("Should be a TMS layer with name " + TEST_TMS_LAYER_NAME + ", but found " + layerProps, layerProps instanceof LayerTMSProps);
    final LayerTMSProps tmsProps = (LayerTMSProps) layerProps;
    assertEquals("Name", TEST_TMS_LAYER_NAME, tmsProps.getName());
    assertEquals("ID", 1, tmsProps.getId());
    assertEquals("min scale", 0, tmsProps.getMinScale(), 1E-6);
    assertEquals("max scale", 0, tmsProps.getMaxScale(), 1E-6);
    assertEquals("max scale", 0.8, tmsProps.getOpacity(), 1E-6);
    assertNull("Legend", tmsProps.getLegend());
    assertTrue("Returned TMS should be enabled", tmsProps.isEnabled());
    assertEquals("Type", "png8", tmsProps.getType());
    assertNotNull("base URL", tmsProps.getBaseUrl());
    assertEquals("Service", "1.0.0", tmsProps.getServiceVersion());
    assertEquals("Attribution", "&copy; OSM &amp; Kadaster", tmsProps.getAttribution());
  }

  @Test
  public void testGetBaseLayer() throws SQLException, AeriusException {
    final LayerProps layerProps = LayerRepository.getBaseLayer(pmf.getConnection(), messagesKey);
    assertTrue("Should be a multi layer properties", layerProps instanceof MultiLayerProps);
    final MultiLayerProps multiLayerProps = (MultiLayerProps) layerProps;
    assertEquals("Should be 2 layers in there", 2, multiLayerProps.getLayers().size());
  }

}
