/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.TestPMF;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Province;

/**
 *
 */
@RunWith(Parameterized.class)
public class GeneralRepositoryTest extends BaseDBTest {

  private final TestPMF pmf;
  private final String testDescription;

  /**
   * Initialize test with version and file to test.
   */
  public GeneralRepositoryTest(final TestPMF pmf, final String testDescription) {
    this.pmf = pmf;
    this.testDescription = testDescription;
  }

  @Parameters(name = "Database {1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> connectionTests = new ArrayList<>();
    final Object[] calculator = new Object[2];
    calculator[0] = getCalcPMF();
    calculator[1] = "Calculator";
    connectionTests.add(calculator);
    final Object[] register = new Object[2];
    register[0] = getRegPMF();
    register[1] = "Register";
    connectionTests.add(register);
    return connectionTests;
  }

  @Test
  public void testGetNatura2kAreas() throws SQLException {
    final List<AssessmentArea> n2kAreas = GeneralRepository.getNatura2kAreas(pmf.getConnection());
    assertNotNull(testDescription + ": Areas shouldn't be null", n2kAreas);
    assertFalse(testDescription + ": Areas should be found", n2kAreas.isEmpty());
    for (final AssessmentArea area : n2kAreas) {
      assertNotEquals(testDescription + ": Area ID", 0, area.getId());
      assertNotNull(testDescription + ": Area name", area.getName());
    }
  }

  @Test
  public void testGetProvinces() throws SQLException {
    final Set<Province> provinces = GeneralRepository.getProvinces(pmf.getConnection());
    assertNotNull(testDescription + ": Provinces shouldn't be null", provinces);
    //seems ok to test this for now, but guess it could change...
    assertEquals(testDescription + ": Number of provinces", 12, provinces.size());
    for (final Province province : provinces) {
      assertNotEquals(testDescription + ": Area ID", 0, province.getProvinceId());
      assertNotNull(testDescription + ": Area name", province.getName());
    }
  }

  @Test
  public void testGetProvince() throws SQLException {
    final Province province = GeneralRepository.getProvince(pmf.getConnection(), 1);
    assertNotNull(testDescription + ": Province shouldn't be null", province);
    assertNotNull(testDescription + ": Province name shouldn't be null", province.getName());
  }

  @Test
  public void testGetAssessmentArea() throws SQLException {
    AssessmentArea area = GeneralRepository.getAssessmentArea(pmf.getConnection(), 0);
    assertNull(testDescription + ": AssessmentArea should be null", area);

    area = GeneralRepository.getAssessmentArea(pmf.getConnection(), TestDomain.BINNENVELD_ID);
    assertNotNull(testDescription + ": AssessmentArea shouldn't be null", area);
    assertEquals(testDescription + ": Area ID", TestDomain.BINNENVELD_ID, area.getId());
    assertEquals(testDescription + ": Area AssessmentAreaID", TestDomain.BINNENVELD_ID, area.getId());
    assertEquals(testDescription + ": Area name", "Binnenveld", area.getName());
    assertNotNull(testDescription + ": Area bounding box", area.getBounds());
  }

  @Test
  public void testGetNearestProvinces() throws SQLException {
    Province province = GeneralRepository.determineProvince(pmf.getConnection(), new Point(135587, 455397));
    assertNotNull("Provinces shouldn't be null", province);
    assertEquals("Area ID", 7, province.getProvinceId());

    province = GeneralRepository.determineProvince(pmf.getConnection(), new Point(208429, 474197));
    assertNotNull("Provinces shouldn't be null", province);
    assertEquals("Area ID", 2, province.getProvinceId());

    province = GeneralRepository.determineProvince(pmf.getConnection(), new Point(241346, 622778));
    assertNotNull("Provinces shouldn't be null", province);
    assertEquals("Area ID", 10, province.getProvinceId());

    province = GeneralRepository.determineProvince(pmf.getConnection(), new Point(84958, 316400));
    assertNotNull("Provinces shouldn't be null", province);
    assertEquals("Area ID", 8, province.getProvinceId());
  }
}
