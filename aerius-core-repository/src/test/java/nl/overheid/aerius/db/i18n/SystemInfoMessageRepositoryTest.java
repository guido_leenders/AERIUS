/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.i18n;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.util.LocaleUtils;

@RunWith(value = Parameterized.class)
public class SystemInfoMessageRepositoryTest extends BaseDBTest {

  private final String MESSAGE = "Some message as system info.";
  private final ProductType productType;
  private final Locale locale;

  public SystemInfoMessageRepositoryTest(final ProductType productType, final Locale locale) {
    this.productType = productType;
    this.locale = locale;
  }

  @Parameters(name = "{0} - {1}")
  public static List<Object[]> data() throws SQLException, IOException {
    final List<Object[]> tests = new ArrayList<>();
    for (final ProductType productType : ProductType.values()) {
      if (productType != ProductType.MELDING) {
        for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
          tests.add(new Object[] {productType, locale});
        }
      }
    }
    return tests;
  }

  @Test
  public void testNoExisitingMessages() throws SQLException {
    final Connection connection = getConnection(productType);
    try {
      final String value = SystemInfoMessageRepository.getMessage(connection, locale);
      assertTrue("Message should me empty ", value.isEmpty());
    } catch (final IllegalArgumentException e) {
      fail("Entry for system info message " + " (" + locale.getLanguage() + ") does not exist in database " + productType.getDbRepresentation());
    }
  }

  @Test
  public void testExistingMessages() throws SQLException {
    final Connection connection = getConnection(productType);
    SystemInfoMessageRepository.insertMessage(connection, MESSAGE, locale.getLanguage());
    try {
      final String value = SystemInfoMessageRepository.getMessage(connection, locale);
      assertEquals("Message should be same", MESSAGE, value);
    } catch (final IllegalArgumentException e) {
      fail("Entry for system info message " + " (" + locale.getLanguage() + ") does not exist in database " + productType.getDbRepresentation());
    }
  }

  @Test
  public void testFallbackMessages() throws SQLException {
    final Connection connection = getConnection(productType);
    // only set the default language to test fallback message for other languages
    SystemInfoMessageRepository.insertMessage(connection, MESSAGE, Locale.getDefault().getLanguage());

    try {
      final String value = SystemInfoMessageRepository.getMessage(connection, locale);
      assertEquals("Message should be same", MESSAGE, value);
    } catch (final IllegalArgumentException e) {
      fail("Entry for system info message " + " (" + locale.getLanguage() + ") does not exist in database " + productType.getDbRepresentation());
    }
  }

}
