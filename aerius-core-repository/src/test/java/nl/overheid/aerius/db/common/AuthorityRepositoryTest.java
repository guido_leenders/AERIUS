/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.user.Authority;

public class AuthorityRepositoryTest extends BaseDBTest {

  private static final int AUTHORITY_ID_UNKNOWN = 1;
  private static final String AUTHORITY_CODE_UNKNOWN = "Onbekend";
  private static final String AUTHORITY_CODE_MINLNV = "MinLNV";

  @Test
  public void testGetAuthorities() throws SQLException {
    final ArrayList<Authority> authorities = AuthorityRepository.getAuthorities(getRegConnection());
    assertNotNull("Returned list shouldn't be null", authorities);
    assertFalse("Returned list shouldn't be empty", authorities.isEmpty());

    for (final Authority authority : authorities) {
      if (authority.getId() == AUTHORITY_ID_UNKNOWN) {
        validateAuthorityUnknown(authority, false);
      } else if (authority.getCode().equalsIgnoreCase(AUTHORITY_CODE_MINLNV)) {
        validateAuthorityMinLNV(authority, false);
      } else {
        validateAuthority(authority, false);
      }
    }
  }

  @Test
  public void testGetAuthority() throws SQLException {
    final Authority authority = AuthorityRepository.getAuthority(getRegConnection(), AUTHORITY_ID_UNKNOWN);
    validateAuthorityUnknown(authority, true);
  }

  @Test
  public void testGetSkinnedAuthority() throws SQLException {
    final Authority authority = AuthorityRepository.getSkinnedAuthority(getRegConnection(), AUTHORITY_ID_UNKNOWN);
    validateAuthorityUnknown(authority, false);
  }

  private void validateAuthority(final Authority authority, final boolean hasSettings) {
    assertNotEquals("Authority ID", 0, authority.getId());
    assertNotNull("Authority code", authority.getCode());
    assertNotNull("Authority description", authority.getDescription());
    if (authority.getCode().toLowerCase().equals("overijssel")) {
      assertNotNull("Authority email", authority.getEmailAddress());
    } else if (authority.getDescription().toLowerCase().contains("ministerie")) {
      assertNull("Authority email", authority.getEmailAddress());
    }
    validateAuthoritySettings(authority, hasSettings);
  }

  private void validateAuthoritySettings(final Authority authority, final boolean hasSettings) {
    assertNotNull("Authority settings", authority.getSettings());
    if (hasSettings) {
      assertFalse("Should have at least some settings", authority.getSettings().isEmpty());
    } else {
      assertTrue("Should have no settings", authority.getSettings().isEmpty());
    }
  }

  private void validateAuthorityUnknown(final Authority authority, final boolean hasSettings) {
    validateAuthority(authority, hasSettings);
    assertEquals("Authority ID", AUTHORITY_ID_UNKNOWN, authority.getId());
    assertEquals("Authority code", AUTHORITY_CODE_UNKNOWN, authority.getCode());
    assertEquals("Authority description", AUTHORITY_CODE_UNKNOWN, authority.getDescription());
    assertEquals("Authority email", null, authority.getEmailAddress());
  }

  private void validateAuthorityMinLNV(final Authority authority, final boolean hasSettings) {
    validateAuthority(authority, hasSettings);
    assertEquals("Authority code", AUTHORITY_CODE_MINLNV, authority.getCode());
    assertEquals("Authority description", "Ministerie van Landbouw, Natuur en Voedselkwaliteit", authority.getDescription());
    assertEquals("Authority email", null, authority.getEmailAddress());
  }

}
