/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common.sector.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategories;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.ShippingLaden;

/**
 * Needs a proper database to test against, testing DB operations for categories.
 * Tests for now only check if queries work, not if the returned values are as expected. That'd require a strict test database.
 */
public class CategoryRepositoriesTest extends BaseDBTest {

  private static final int YEAR = 2020;
  private static final int FARM_LODGING_TYPE_ID_WITH_ADDITIONAL = 184;
  private static final int FARM_LODGING_TYPE_ID_WITH_REDUCTIVE = 45;

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.RoadEmissionCategoryRepository#findAllRoadEmissionCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  public void testFindAllRoadEmissionCategories() throws SQLException {
    final RoadEmissionCategories categories = RoadEmissionCategoryRepository.findAllRoadEmissionCategories(getCalcConnection());
    assertNotNull("Retrieved categories should not be null", categories);
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.FarmLodgingCategoryRepository#getFarmLodgingCategories}.
   * @throws SQLException
   */
  @Test
  public void testGetFarmLodgingCategories() throws SQLException {
    final FarmLodgingCategories categories = FarmLodgingCategoryRepository.getFarmLodgingCategories(getCalcConnection(), getCalcMessagesKey());
    testReturnedCategories(categories.getFarmAdditionalLodgingSystemCategories());
    testReturnedCategories(categories.getFarmReductiveLodgingSystemCategories());
    testFarmLodgingCategories(categories.getFarmLodgingSystemCategories());
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository#findOffRoadMobileSourceCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  public void testFindOffRoadMobileSourceCategories() throws SQLException {
    final ArrayList<OffRoadMobileSourceCategory> categories =
        MobileSourceCategoryRepository.findOffRoadMobileSourceCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved categories should not be null", categories);
    testReturnedCategories(categories);
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.MobileSourceCategoryRepository#findOffRoadMobileSourceCategories(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  public void testFindOnRoadMobileSourceCategories() throws SQLException {
    final ArrayList<OnRoadMobileSourceCategory> categories =
        MobileSourceCategoryRepository.findOnRoadMobileSourceCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved categories should not be null", categories);
    testReturnedCategories(categories);
  }

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository#findAllMaritimeShippingCategories(Connection, nl.overheid.aerius.i18n.DBMessages.DBMessagesKey)}
   * @throws SQLException
   */
  @Test
  public void testFindAllMaritimeShippingCategories() throws SQLException {
    final ArrayList<MaritimeShippingCategory> categories = getTestMaritimeShippingCategories();
    assertNotNull("Retrieved categories should not be null", categories);
    testReturnedCategories(categories);
  }

  @Test
  public void testFindAllInlandShipCategories() throws SQLException {
    final ArrayList<InlandShippingCategory> categories = getTestInlandShippingCategories();
    assertNotNull("Retrieved categories should not be null", categories);
    testReturnedCategories(categories);
  }

  @Test
  public void testGetInlandWaterwayCategories() throws SQLException {
    final ArrayList<InlandWaterwayCategory> categories =
        ShippingCategoryRepository.getInlandWaterwayCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved categories should not be null", categories);
  }

  @Test
  public void testGetInlandShippingCategories() throws SQLException {
    final InlandShippingCategories categories =
        ShippingCategoryRepository.findInlandShippingCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved categories should not be null", categories);
    final InlandWaterwayCategory cemtCategory = categories.getWaterwayCategoryByCode("CEMT_I");
    assertNotNull("Existing waterway type: CEMT_I", cemtCategory);
    assertEquals("Number of directions for CEMT_I", 1, cemtCategory.getDirections().size());
    final InlandWaterwayCategory waalCategory = categories.getWaterwayCategoryByCode("Waal");
    assertNotNull("Another existing waterway type: Waal", waalCategory);
    assertEquals("Number of directions for Waal", 2, waalCategory.getDirections().size());
    final InlandShippingCategory shipCategory = categories.getShipCategoryByCode("C3B");
    assertNotNull("Existing ship category", shipCategory);
    assertFalse("Ship not allowed on CEMT_I", categories.isValidCombination(shipCategory, cemtCategory));
    assertTrue("Ship allowed on Waal", categories.isValidCombination(shipCategory, waalCategory));
  }

  @Test
  public void testGetMaritimeCategoryEmissionFactors() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final HashMap<EmissionValueKey, Double> emissionFactors =
        ShippingCategoryRepository.findMaritimeCategoryEmissionFactors(getCalcConnection(), category, ShippingMovementType.INLAND, YEAR);
    assertNotNull("Retrieved emissionFactors should not be null", emissionFactors);
  }

  @Test
  public void testGetMaritimeShippingCharacteristics() throws SQLException {
    final MaritimeShippingCategory category = getTestMaritimeShippingCategories().get(0);
    final OPSSourceCharacteristics characteristics =
        ShippingCategoryRepository.getCharacteristics(getCalcConnection(), category, ShippingMovementType.INLAND, YEAR);
    assertNotNull("Retrieved characteristics should not be null", characteristics);
  }

  @Test
  public void testGetInlandCategoryRouteEmissionFactors() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors =
        ShippingCategoryRepository.findInlandCategoryRouteEmissionFactors(getCalcConnection(), category, YEAR);
    assertNotNull("Retrieved emissionFactors should not be null", emissionFactors);
    boolean foundLaden = false;
    boolean foundUnladen = false;
    final Set<WaterwayDirection> directions = new HashSet<>();
    for (final Entry<InlandCategoryEmissionFactorKey, Double> entry : emissionFactors.entrySet()) {
      assertNotEquals("Retrieved emissionFactor", 0.0, entry.getValue().doubleValue(), 1E-6);
      if (entry.getKey().isLaden()) {
        foundLaden = true;
      } else {
        foundUnladen = true;
      }
      directions.add(entry.getKey().getDirection());
    }
    assertTrue("Expected emissionfactors for laden ships.", foundLaden);
    assertTrue("Expected emissionfactors for unladen ships.", foundUnladen);
    assertEquals("Directions found " + directions, WaterwayDirection.values().length, directions.size());
  }

  @Test
  public void testGetInlandCategoryRouteCharacteristics() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics =
        ShippingCategoryRepository.getInlandCategoryRouteCharacteristics(getCalcConnection(), category);
    assertNotNull("Retrieved characteristics should not be null", characteristics);
    for (final Entry<InlandCategoryKey, OPSSourceCharacteristics> entry : characteristics.entrySet()) {
      assertNotEquals("Retrieved emissionFactor", 0, entry.getValue().getHeatContent());
      assertNotEquals("Retrieved emissionFactor", 0, entry.getValue().getEmissionHeight());
    }
  }

  @Test
  public void testGetInlandCategoryMooringEmissionFactors() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<EmissionValueKey, Double> emissionFactors =
        ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(getCalcConnection(), category, YEAR);
    assertNotNull("Retrieved emissionFactors should not be null", emissionFactors);
    for (final Entry<EmissionValueKey, Double> entry : emissionFactors.entrySet()) {
      assertNotEquals("Retrieved emissionFactor", 0.0, entry.getValue(), 1E-3);
    }
  }

  @Test
  public void testGetInlandCategoryMooringCharacteristics() throws SQLException {
    final InlandShippingCategory category = getTestInlandShippingCategories().get(0);
    final Map<ShippingLaden, OPSSourceCharacteristics> characteristics =
        ShippingCategoryRepository.getInlandCategoryMooringCharacteristics(getCalcConnection(), category);
    assertNotNull("Retrieved characteristics should not be null", characteristics);
    assertEquals("Should have characteristics for both laden and unladen.", 2, characteristics.size());
    for (final Entry<ShippingLaden, OPSSourceCharacteristics> entry : characteristics.entrySet()) {
      assertNotEquals("Retrieved emissionFactor", 0, entry.getValue().getHeatContent());
      assertNotEquals("Retrieved emissionFactor", 0, entry.getValue().getEmissionHeight());
    }
  }

  @Test
  public void testFindAllPlanCategories() throws SQLException {
    final ArrayList<PlanCategory> categories =
        PlanEmissionCategoryRepository.findAllPlanEmissionCategories(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved categories should not be null", categories);
    testReturnedCategories(categories);
    for (final PlanCategory category : categories) {
      assertNotNull("Returned characteristics for category " + category, category.getCharacteristics());
      assertNotEquals("Height for category " + category, 1, category.getCharacteristics().getHeatContent(), 1E-6);
      assertNotNull("Returned sectorIcon for category " + category, category.getSectorIcon());
    }
  }

  @Test
  public void testFindAllOffRoadMachineryTypes() throws SQLException {
    final ArrayList<OffRoadMachineryType> categories =
        OffRoadMachineryTypesRepository.findAllOffRoadMachineryTypes(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved categories should not be null", categories);
    testReturnedCategories(categories);
    for (final OffRoadMachineryType machineryType : categories) {
      assertFalse("OffRoadMachineryType should not be empty:" + machineryType.getName(), machineryType.getFuelTypes().isEmpty());
      for (final FuelTypeProperties ftp : machineryType.getFuelTypes()) {
        assertNotNull("FuelType of properties should not be null:" + machineryType.getName() + ", machinery:" + ftp.getId(), ftp.getFuelType());
      }
    }
  }

  private void testReturnedCategories(final List<? extends AbstractCategory> categories) {
    assertNotNull("Testing categories should not be null", categories);
    final HashSet<Integer> ids = new HashSet<>();
    for (final AbstractCategory category : categories) {
      assertFalse(category.getClass().getSimpleName() + " category list has duplicate IDs. ID: " + category.getId(),
          ids.contains(category.getId()));
      ids.add(category.getId());
      assertNotNull(category.getClass().getSimpleName() + " code was null. ID: " + category.getId(), category.getCode());
      assertNotNull(category.getClass().getSimpleName() + " name was null. ID: " + category.getId(), category.getName());
      //description can be null
    }
  }

  private void testFarmLodgingCategories(final List<FarmLodgingCategory> categories) {
    testReturnedCategories(categories);
    for (final FarmLodgingCategory cat : categories) {
      assertFalse(cat.getClass().getSimpleName() + " has no lodging system definitions. ID: " +
          cat.getId(), cat.getFarmLodgingSystemDefinitions().isEmpty());
      final HashSet<Integer> ids = new HashSet<>();
      for (final FarmLodgingSystemDefinition def : cat.getFarmLodgingSystemDefinitions()) {
        assertFalse(def.getClass().getSimpleName() + " category list has duplicate IDs. ID: " +
            def.getId() + ", lodging ID: " + cat.getId(), ids.contains(def.getId()));
        ids.add(def.getId());
        assertNotNull(def.getClass().getSimpleName() + " code was null. ID: " + def.getId() +
            ", lodging ID: " + cat.getId(), def.getCode());
        assertNotNull(def.getClass().getSimpleName() + " name was null. ID: " + def.getId() +
            ", lodging ID: " + cat.getId(), def.getName());
        //description can be null
      }

      if (cat.getId() == FARM_LODGING_TYPE_ID_WITH_ADDITIONAL) {
        assertFalse(cat.getClass().getSimpleName() + " has no additional lodging systems. ID: " +
            cat.getId(), cat.getFarmAdditionalLodgingSystemCategories().isEmpty());
      }

      if (cat.getId() == FARM_LODGING_TYPE_ID_WITH_REDUCTIVE) {
        assertFalse(cat.getClass().getSimpleName() + " has no reductive lodging systems. ID: " +
            cat.getId(), cat.getFarmReductiveLodgingSystemCategories().isEmpty());
      }
    }
  }

  private ArrayList<MaritimeShippingCategory> getTestMaritimeShippingCategories() throws SQLException {
    return ShippingCategoryRepository.findAllMaritimeShippingCategories(getCalcConnection(), getCalcMessagesKey());
  }

  private ArrayList<InlandShippingCategory> getTestInlandShippingCategories() throws SQLException {
    return ShippingCategoryRepository.findAllInlandShippingCategories(getCalcConnection(), getCalcMessagesKey());
  }

}
