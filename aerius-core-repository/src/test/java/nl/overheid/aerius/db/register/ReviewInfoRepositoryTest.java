/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.shared.domain.developmentspace.BaseReviewInfo;
import nl.overheid.aerius.shared.domain.developmentspace.PermitReviewInfo;
import nl.overheid.aerius.shared.domain.developmentspace.PrioritySubProjectReviewInfo;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ReviewInfoRepository}.
 */
public class ReviewInfoRepositoryTest extends RequestTestBase {

  private static final double EPSILON = 1E-5;

  private static final String TEST_REFERENCE_PRIORITY_SUBPROJECT = "pdp_Overijssel_10_4600";

  @Test
  public void testGetPermitReviewInfo() throws SQLException, AeriusException {
    clearPermitsAndNoticesDevelopmentSpaces();

    final Permit request = getInsertedExamplePermit();

    final int proposedId = addCalculation(request, 1.0, SituationType.PROPOSED);

    final ArrayList<PermitReviewInfo> infoSet1 = ReviewInfoRepository.getPermitReviewInfo(getRegConnection(),
        request.getPermitKey());

    assertEquals("Assessment area count", 4, infoSet1.size());
    for (final PermitReviewInfo info : infoSet1) {
      if (Math.abs(info.getAssessmentArea().getId()) == TestDomain.DWINGELDERVELD_ID) {
        validatePermitReviewInfo("Dwingelderveld 1 situation", info, 1.6, false);
      } else if (Math.abs(info.getAssessmentArea().getId()) == TestDomain.BINNENVELD_ID) {
        validatePermitReviewInfo("Binnenveld 1 situation", info, 1.23, false);
      } else {
        fail("Unexpected assessment area id" + info.getAssessmentArea().getId());
      }
      assertEquals("Receptor count with shortage inclusive", 0, info.getNumReceptorsShortageInclusive());
    }

    final int currentId = addCalculation(request, 0.3, SituationType.CURRENT);
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getRegConnection(), proposedId, currentId, true);
    final ArrayList<PermitReviewInfo> infoSet2 = ReviewInfoRepository.getPermitReviewInfo(getRegConnection(),
        request.getPermitKey());

    assertEquals("Assessment area count", 4, infoSet2.size());
    for (final PermitReviewInfo info : infoSet2) {
      if (Math.abs(info.getAssessmentArea().getId()) == TestDomain.DWINGELDERVELD_ID) {
        validatePermitReviewInfo("Dwingelderveld 2 situations", info, 1.12, false);
      } else if (Math.abs(info.getAssessmentArea().getId()) == TestDomain.BINNENVELD_ID) {
        validatePermitReviewInfo("Binnenveld 2 situations", info, 0.861, false);
      } else {
        fail("Unexpected assessment area id" + info.getAssessmentArea().getId());
      }
      assertEquals("Receptor count with shortage inclusive", 0, info.getNumReceptorsShortageInclusive());
    }
  }

  @Test
  public void testGetPermitReviewInfoBigPermit() throws SQLException, AeriusException {
    clearPermitsAndNoticesDevelopmentSpaces();

    final Permit request = getInsertedExamplePermit();

    addCalculation(request, 10.0, SituationType.PROPOSED);
    final ArrayList<PermitReviewInfo> infoSet = ReviewInfoRepository.getPermitReviewInfo(getRegConnection(),
        request.getPermitKey());

    assertEquals("Assessment area count", 4, infoSet.size());
    for (final PermitReviewInfo info : infoSet) {
      if (Math.abs(info.getAssessmentArea().getId()) == TestDomain.DWINGELDERVELD_ID) {
        validatePermitReviewInfo("Dwingelderveld big permit", info, 16, false);

        assertEquals("Receptor count with shortage inclusive", 0, info.getNumReceptorsShortageInclusive());
      } else if (Math.abs(info.getAssessmentArea().getId()) == TestDomain.BINNENVELD_ID) {
        validatePermitReviewInfo("Binnenveld big permit", info, 12.3, false);

        assertEquals("Receptor count with shortage inclusive", 0, info.getNumReceptorsShortageInclusive());
      } else {
        fail("Unexpected assessment area id" + info.getAssessmentArea().getId());
      }
    }
  }

  @Test
  public void testGetPrioritySubProjectReviewInfo() throws SQLException, AeriusException {
    final ArrayList<PrioritySubProjectReviewInfo> infoSet = ReviewInfoRepository.getPrioritySubProjectReviewInfo(
        getRegConnection(), new PrioritySubProjectKey(new PriorityProjectKey(null, null), TEST_REFERENCE_PRIORITY_SUBPROJECT));

    assertFalse("Query results returned", infoSet.isEmpty());
    for (final PrioritySubProjectReviewInfo info : infoSet) {
      validatePrioritySubProjectReviewInfo(info);
    }
  }

  private void validateBaseReviewInfo(final BaseReviewInfo info, final boolean shortageIsNegative) {
    assertNotNull("Assessment area not null", info.getAssessmentArea());
    assertFalse("Assessment area name not empty", info.getAssessmentArea().getName() == null || info.getAssessmentArea().getName().isEmpty());
    if (info.getNumReceptorsShortageInclusive() > 0) {
      if (shortageIsNegative) {
        assertTrue("Should have shortage < 0", info.getShortageInclusive() < 0);
      } else {
        assertTrue("Should have shortage > 0", info.getShortageInclusive() > 0);
      }
    } else {
      assertEquals("Should have shortage 0", Double.doubleToLongBits(info.getShortageInclusive()), 0);
    }
  }

  private void validatePermitReviewInfo(final String description, final PermitReviewInfo info, final double expectedRequiredSpace,
      final boolean shouldHaveShortage) {
    validateBaseReviewInfo(info, true);

    assertNotEquals("Available space exclusive " + description, 0.0, info.getAvailableExclusive(), EPSILON);
    if (shouldHaveShortage) {
      assertNotEquals("Shortage inclusive " + description, 0.0, info.getShortageInclusive(), EPSILON);
    } else {
      assertEquals("Shortage inclusive " + description, 0.0, info.getShortageInclusive(), EPSILON);
    }

    assertEquals("Required space " + description, expectedRequiredSpace, info.getRequiredSpaceForPermit(), EPSILON);
  }

  private void validatePrioritySubProjectReviewInfo(final PrioritySubProjectReviewInfo info) {
    validateBaseReviewInfo(info, false);
  }

}
