/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test class for {@link CalculationSummaryRepository}.
 */
public class CalculationSummaryRepositoryTest extends CalculationRepositoryTestBase {

  private static final String H4030 = "H4030";
  private static final int EXPECTED_HT_DUINEN_TEXEL = 22;
  private static final Substance TEST_SUBSTANCE_2 = Substance.NH3;

  private static final double RECEPTOR_1_DEPOSITION_CALCULATION_2 = 10.0;

  @Test
  public void testDetermineHabitatDepositions() throws SQLException {
    //first insert the normal results.
    insertCalculationResults();
    try (final Connection connection = getCalcConnection()) {
      //depends a lot on getExampleOPSOutputData staying the same.
      //using NH3 in testInsertCalculationSetOptions to get a result
      final Map<Integer, AssessmentArea> areas = GeneralRepository.getAssessmentAreas(connection);
      final CalculationSummary cs = new CalculationSummary();
      CalculationSummaryRepository.determineHabitatResults(connection, areas, cs, calculation.getCalculationId(), TEST_SUBSTANCE_2.hatch());

      assertHabitatDepositions(cs, calculation.getCalculationId(), -1, true);
    }
  }

  @Test
  public void testDetermineHabitatDepositionsDifferences() throws SQLException {
    //first insert the one results.
    insertCalculationResults();
    final int calculationIdOne = calculation.getCalculationId();
    newCalculation();
    //next insert the two results
    final int calculationIdTwo = calculation.getCalculationId();
    assertNotEquals("Base and variant calc ID", calculationIdOne, calculationIdTwo);
    final PartialCalculationResult outputData = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    for (final AeriusResultPoint point : outputData.getResults()) {
      point.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, RECEPTOR_1_DEPOSITION_CALCULATION_2);
    }
    outputData.setCalculationId(calculation.getCalculationId());
    CalculationRepository.insertCalculationResults(getCalcConnection(), calculation.getCalculationId(), outputData.getResults());
    final CalculationSummary cs = new CalculationSummary();
    final Map<Integer, AssessmentArea> areas = GeneralRepository.getAssessmentAreas(getCalcConnection());
    CalculationSummaryRepository.determineHabitatResults(getCalcConnection(), areas, cs, calculationIdOne, TEST_SUBSTANCE_2.hatch());
    CalculationSummaryRepository.determineHabitatResults(getCalcConnection(), areas, cs, calculationIdTwo, TEST_SUBSTANCE_2.hatch());
    CalculationSummaryRepository.determineHabitatResultsComparison(
        getCalcConnection(), cs, calculationIdOne, calculationIdTwo, TEST_SUBSTANCE_2.hatch());
    CalculationSummaryRepository.determineHabitatResultsComparison(
        getCalcConnection(), cs, calculationIdTwo, calculationIdOne, TEST_SUBSTANCE_2.hatch());

    assertHabitatDepositions(cs, calculationIdOne, -1, true); // To be sure
    assertHabitatDepositions(cs, calculationIdOne, calculationIdTwo, true);
    assertHabitatDepositions(cs, calculationIdTwo, calculationIdOne, false);

    for (final Entry<Integer, CalculationAreaSummaryResult> casr : cs.getSummaryResult().entrySet()) {
      for (final Entry<Integer, HabitatSummaryResults> hsr : casr.getValue().getHabitats().entrySet()) {
        final double valDiffBase =
            hsr.getValue().getComparisonSummary(calculationIdOne, calculationIdTwo).getPercentageKDW().get(EmissionResultKey.NH3_DEPOSITION);
        final double valDiffVariant =
            hsr.getValue().getComparisonSummary(calculationIdTwo, calculationIdOne).getPercentageKDW().get(EmissionResultKey.NH3_DEPOSITION);
        assertEquals("%KDW (difference base) == -(difference variant)", valDiffBase, -valDiffVariant, 0.0001);
      }
    }
  }

  private void assertHabitatDepositions(final CalculationSummary cs, final int calculation1Id, final int calculation2Id, final boolean twoMinOne) {
    final boolean isNoComparison = calculation2Id == -1;
    assertNotNull("Result", cs);
    assertEquals("Result size", 2, cs.getSummaryResult().size());
    for (final Entry<Integer, CalculationAreaSummaryResult> entry : cs.getSummaryResult().entrySet()) {
      final HashMap<Integer, HabitatSummaryResults> habitats = entry.getValue().getHabitats();
      HabitatSummaryResults firstHabitat = null;
      for (final Entry<Integer, HabitatSummaryResults> hEntry : habitats.entrySet()) {
        if (entry.getKey() != TestDomain.DUINEN_TEXEL_ID ||
            (entry.getKey() == TestDomain.DUINEN_TEXEL_ID && hEntry.getKey() == EXPECTED_HT_DUINEN_TEXEL)) {
          firstHabitat = hEntry.getValue();
          break;
        }
      }
      if (firstHabitat == null) {
        fail("habitat not found");
      }
      final HabitatSummaryResult firstResult = isNoComparison
          ? firstHabitat.getHabitatSummaryResults(calculation1Id) : firstHabitat.getComparisonSummary(calculation1Id, calculation2Id);
      if (entry.getKey() == TestDomain.VELUWE_ID) {
        //veluwe, should have higher deposition.
        assertEquals("Area ID 1 name", "Veluwe", entry.getValue().getArea().getName());
        assertEquals("habitat types found", 1, habitats.size());
        assertEquals("habitat code", H4030, firstHabitat.getCode());
        assertEquals("habitat name", "Droge heiden", firstHabitat.getName());
        if (isNoComparison) {
          assertEquals("Should be same deposition as inserted.", RECEPTOR_1_DEPOSITION,
              firstResult.getMax().get(EmissionResultKey.NH3_DEPOSITION), 0.001);
        } else if (twoMinOne) {
          assertEquals("Should be deposition 2 min 1.", RECEPTOR_1_DEPOSITION_CALCULATION_2 - RECEPTOR_1_DEPOSITION,
              firstResult.getMax().get(EmissionResultKey.NH3_DEPOSITION), 0.001);
        } else {
          assertEquals("Should be deposition 1 min 2.", RECEPTOR_1_DEPOSITION - RECEPTOR_1_DEPOSITION_CALCULATION_2,
              firstResult.getMax().get(EmissionResultKey.NH3_DEPOSITION), 0.001);
        }
        //only 1 receptor in the area, so next should hold.
        //while the next should hold, this is no longer the case due to the change in CalculationSummaryRepository
//        assertEquals("Max deposition difference should be same as total deposition difference.",
//            firstResult.getTotal().get(EmissionResultKey.NH3_DEPOSITION),
//            firstResult.getMax().get(EmissionResultKey.NH3_DEPOSITION), 0.0001);
        assertEquals("Avg deposition difference should be same as max deposition difference.",
            firstResult.getMax().get(EmissionResultKey.NH3_DEPOSITION),
            firstResult.getAverage().get(EmissionResultKey.NH3_DEPOSITION), 0.0001);
        if (isNoComparison) {
          assertTrue("Should be exceeding.", firstResult.getPercentageKDW().get(EmissionResultKey.NH3_DEPOSITION) > 0);
        }
      } else if (entry.getKey() == TestDomain.DUINEN_TEXEL_ID) {
        assertEquals("Area ID 2 name", "Duinen en Lage Land Texel", entry.getValue().getArea().getName());
        assertEquals("habitat types found", 8, entry.getValue().getHabitats().size());
        //percentages KDW are not retrieved for comparisons: this is calculated in CalculationFactory
        if (isNoComparison) {
          assertTrue("Percentage KDW should be exceeding.", firstResult.getPercentageKDW().get(EmissionResultKey.NH3_DEPOSITION) > 0);
          //not whole surface of receptor is covered by habitat (and total deposition looks at surface covered).
          //check for comparison is a bit harder, as it'll be the difference between the two.
          assertTrue("Should be at most deposition as inserted.", firstResult.getTotal().get(EmissionResultKey.NH3_DEPOSITION) < RECEPTOR_2_DEPOSITION);
        }
      } else {
        fail("Did not expect another assessment area. Found: " + entry.getKey());
      }
      assertNotNull("Assessment area should contain bounding box", entry.getValue().getArea().getBounds());
    }
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getCalcConnection();
  }

}
