/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.register.dashboard.DefaultDashboardRow;
import nl.overheid.aerius.shared.domain.register.dashboard.NoticeDashboardFilter;
import nl.overheid.aerius.shared.domain.register.dashboard.PermitDashboardFilter;

/**
 *
 */
public class DashboardRepositoryTest extends RequestTestBase {

  private static final int RECEPTOR_ID = 389390;
  private static final int GEULDAL = 157;

  @Test
  public void testGetNoticeRows() throws SQLException {
    // Set one receptor to 80%
    clearPermitsAndNoticesDevelopmentSpaces();
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_ID, 0.8);

    ArrayList<DefaultDashboardRow> rows;
    rows = DashboardRepository.getNoticeRows(getRegConnection(), new NoticeDashboardFilter());
    validateDefaultRows(rows, "Empty filter", false);

    final NoticeDashboardFilter filter = new NoticeDashboardFilter();
    filter.setMinFractionUsed(0.85);
    rows = DashboardRepository.getNoticeRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Fraction outside filter", true);

    filter.setMinFractionUsed(0.8);
    rows = DashboardRepository.getNoticeRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Fraction on filter", false);

    filter.setMinFractionUsed(0.7);
    rows = DashboardRepository.getNoticeRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Fraction inside filter", false);

    filter.setMinFractionUsed(0.7);
    filter.setProvinceId(2);
    rows = DashboardRepository.getNoticeRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Province ID outside filter", true);

    filter.setMinFractionUsed(0.7);
    filter.setProvinceId(6);
    rows = DashboardRepository.getNoticeRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Province ID inside filter", false);
  }

  @Test
  public void testGetPermitRows() throws SQLException {
    // Set one receptor to 80%
    clearPermitsAndNoticesDevelopmentSpaces();
    updateReceptorPermitsAndNoticesDevelopmentSpaces(RECEPTOR_ID, 0.8);

    ArrayList<DefaultDashboardRow> rows;
    rows = DashboardRepository.getPermitRows(getRegConnection(), new PermitDashboardFilter());
    validateDefaultRows(rows, "Empty filter", false);

    final PermitDashboardFilter filter = new PermitDashboardFilter();
    filter.setProvinceId(98);
    rows = DashboardRepository.getPermitRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Bogus province ID in filter", true);

    filter.setProvinceId(2);
    rows = DashboardRepository.getPermitRows(getRegConnection(), filter);
    //rows are returned, as we're not filtering on the percentage so it doesn't matter they all have 0%.
    validateDefaultRows(rows, "Province ID outside filter", false);

    filter.setProvinceId(6);
    rows = DashboardRepository.getPermitRows(getRegConnection(), filter);
    validateDefaultRows(rows, "Province ID inside filter", false);
  }

  private void validateDefaultRows(final ArrayList<DefaultDashboardRow> rows, final String testDescription, final boolean expectedEmpty) {
    assertNotNull(testDescription + ": Rows should never be null", rows);
    if (expectedEmpty) {
      assertTrue(testDescription + ": Row list should be empty", rows.isEmpty());
    } else {
      assertFalse(testDescription + ": Row list shouldn't be empty", rows.isEmpty());
    }
    final Set<AssessmentArea> areas = new HashSet<AssessmentArea>();
    for (final DefaultDashboardRow row : rows) {
      assertNotNull(testDescription + ": Assessment area", row.getAssessmentArea().getId());
      assertNotNull(testDescription + ": Assessment area name", row.getAssessmentArea().getName());
      if (row.getAssessmentArea().getId() == GEULDAL) {
        assertNotEquals(testDescription + ": Assigned fraction", row.getAssignedFactor(), 0.0, 1E-3);
      } else {
        assertEquals(testDescription + ": Assigned fraction", row.getAssignedFactor(), 0.0, 1E-3);
      }
      if (areas.contains(row.getAssessmentArea())) {
        fail(testDescription + ": Found duplicate assessment area: " + row.getAssessmentArea().getName());
      }
      areas.add(row.getAssessmentArea());
      //confirmed and unconfirmed could very well be 0 without test data. Expecting the total not to be...
    }
  }

}
