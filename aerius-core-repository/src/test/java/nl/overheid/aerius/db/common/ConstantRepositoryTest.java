/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.SQLException;

import org.junit.Test;

import nl.overheid.aerius.enums.ConstantsEnum;

/**
 * Test class for {@link ConstantRepository}.
 */
public class ConstantRepositoryTest extends BaseDBTest {

  private enum Dummy {
    DUMMY;
  }

  private static final String TEST_VALUE = "12345";

  @Test
  public void testGetNotExists() throws SQLException {
    final Integer value = ConstantRepository.getNumber(getCalcConnection(), Dummy.DUMMY, Integer.class, false);
    assertNull("value should be null", value);
  }

  @Test(expected = SQLException.class)
  public void testGetNotExistsMustExist() throws SQLException {
    ConstantRepository.getNumber(getCalcConnection(), Dummy.DUMMY, Integer.class, true);
  }

  @Test(expected = SQLException.class)
  public void testSetNotExists() throws SQLException {
    ConstantRepository.setString(getCalcConnection(), Dummy.DUMMY, Dummy.DUMMY.name());
  }

  @Test
  public void testSetAndGet() throws SQLException {
    ConstantRepository.setString(getCalcConnection(), ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP, TEST_VALUE);

    final String res = ConstantRepository.getString(getCalcConnection(), ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP);
    assertEquals(TEST_VALUE, res);

    final Integer integer = ConstantRepository.getInteger(getCalcConnection(), ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP);
    assertEquals(Integer.valueOf(TEST_VALUE), integer);

    final Double doubleprec = ConstantRepository.getDouble(getCalcConnection(), ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP);
    assertEquals(Double.valueOf(TEST_VALUE), doubleprec);

    final Boolean bool = ConstantRepository.getBoolean(getCalcConnection(), ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP);
    assertEquals(false, bool);

    final Long number = ConstantRepository.getNumber(getCalcConnection(), ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP, Long.class);
    assertEquals(Long.valueOf(TEST_VALUE), number);
  }

}
