/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.register.AuditTrailItem;
import nl.overheid.aerius.shared.domain.register.AuditTrailType;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public class PriorityProjectModifyRepositoryTest extends RequestTestBase {

  private static final String TEST_REFERENCE = "pp_Overijssel_10_4600";

  @Test
  public void testInsert() throws AeriusException, SQLException {
    final PriorityProject priorityProject = getExamplePriorityProject();
    PriorityProjectModifyRepository.insert(getRegConnection(), priorityProject, getExampleUserProfile(),
        getFakeInsertRequestFile());
    final PriorityProject inDB = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), priorityProject.getKey(), getRegMessagesKey());
    assertEquals("reference", priorityProject.getReference(), inDB.getReference());
    assertEquals("key", priorityProject.getKey(), inDB.getKey());
    assertEquals("Authority", priorityProject.getAuthority(), inDB.getAuthority());
    assertEquals("Application version", priorityProject.getApplicationVersion(), inDB.getApplicationVersion());
    assertEquals("Database version", priorityProject.getDatabaseVersion(), inDB.getDatabaseVersion());
    assertEquals("Number of changes so far", 1, inDB.getChangeHistory().size());
    assertEquals("Items in change", 4, inDB.getChangeHistory().get(0).getItems().size());
  }

  @Test
  public void testUpdate() throws AeriusException, SQLException {
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_REFERENCE);

    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    //retrieve permit again for the modified date
    final PriorityProjectKey originalKey = priorityProject.getKey();
    final String originalReference = priorityProject.getReference();
    final String newReference = UUID.randomUUID().toString();
    final RequestState originalState = priorityProject.getRequestState();
    priorityProject.getDossierMetaData().setDossierId(newReference);
    priorityProject.setRequestState(RequestState.PENDING_WITH_SPACE);
    priorityProject.getScenarioMetaData().setReference(newReference);
    priorityProject.getScenarioMetaData().setCorporation("Roadhouse");
    priorityProject.getScenarioMetaData().setProjectName("Projectus infernus");
    priorityProject.getDossierMetaData().setRemarks("Make some smart remark");
    PriorityProjectModifyRepository.update(getRegConnection(), originalKey, priorityProject, priorityProject.getLastModified());
    final PriorityProject updatedPriorityProject = PriorityProjectRepository.getPriorityProject(getRegConnection(), priorityProject.getId(),
        getRegMessagesKey());
    assertEquals("Updated remarks", priorityProject.getDossierMetaData().getRemarks(), updatedPriorityProject.getDossierMetaData().getRemarks());
    assertEquals("Updated dossier id", priorityProject.getDossierMetaData().getDossierId(),
        updatedPriorityProject.getDossierMetaData().getDossierId());

    //most things are not updated by the updatePriorityProject calls
    assertNotEquals("Current state", priorityProject.getRequestState(), updatedPriorityProject.getRequestState());
    assertNotEquals("PDF reference", newReference, updatedPriorityProject.getReference());
    assertNotEquals("Corporation", priorityProject.getScenarioMetaData().getCorporation(),
        updatedPriorityProject.getScenarioMetaData().getCorporation());
    assertNotEquals("Project name", priorityProject.getScenarioMetaData().getProjectName(),
        updatedPriorityProject.getScenarioMetaData().getProjectName());
    assertEquals("Current state", originalState, updatedPriorityProject.getRequestState());
    assertEquals("PDF reference", originalReference, updatedPriorityProject.getReference());
  }

  @Test
  public void testUpdateAssignComplete() throws AeriusException, SQLException {
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_REFERENCE);

    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    final PriorityProjectKey originalKey = priorityProject.getKey();
    final boolean originalAssignCompleted = priorityProject.isAssignCompleted();
    priorityProject.setAssignCompleted(!originalAssignCompleted);
    PriorityProjectModifyRepository.updateAssignComplete(getRegConnection(), originalKey, priorityProject.isAssignCompleted(), getExampleUserProfile(), priorityProject.getLastModified());
    final PriorityProject updatedPriorityProject = PriorityProjectRepository.getPriorityProject(getRegConnection(), priorityProject.getId(),
        getRegMessagesKey());
    assertNotEquals("Updated assign completed", originalAssignCompleted, updatedPriorityProject.isAssignCompleted());
    assertEquals("Updated assign completed", priorityProject.isAssignCompleted(), updatedPriorityProject.isAssignCompleted());

    assertEquals("Number of audits", 1, updatedPriorityProject.getChangeHistory().size());
    for (final AuditTrailItem trailItem : updatedPriorityProject.getChangeHistory()) {
      assertEquals("Edited by", getExampleUserProfile(), trailItem.getEditedBy());
      assertEquals("Number of changes in item", 1, trailItem.getItems().size());
      assertEquals("Type of change", AuditTrailType.ASSIGN_COMPLETED, trailItem.getItems().get(0).getType());
    }
  }

  @Test
  public void testDelete() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());
    final PriorityProjectKey key = priorityProject.getKey();
    final List<PrioritySubProjectKey> subKeys = new ArrayList<>();
    for (final PrioritySubProject subProject : priorityProject.getSubProjects()) {
      subKeys.add(new PrioritySubProjectKey(key, subProject.getReference()));
    }

    assertTrue("Priority project must still exist", PriorityProjectRepository.priorityProjectExists(getRegConnection(), key));
    PriorityProjectModifyRepository.delete(getRegConnection(), key, getExampleUserProfile());
    assertFalse("Priority project must no longer exist", PriorityProjectRepository.priorityProjectExists(getRegConnection(), key));
    assertNull("Priority project should not be retrievable",
        PriorityProjectRepository.getSkinnedPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey()));

    for (final PrioritySubProjectKey subKey : subKeys) {
      assertFalse("Subproject should not exist", PrioritySubProjectRepository.prioritySubProjectExists(getRegConnection(), subKey));
    }
  }

  @Test
  public void testUpdateFactsheet() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());

    List<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), priorityProject.getReference());
    assertEquals("Number of files before updating factsheet (non existed before)", 1, requestFiles.size());

    final InsertRequestFile firstFactsheet = getFakeFactsheetFile("first");
    PriorityProjectModifyRepository.updateFactsheet(getRegConnection(), priorityProject.getKey(), firstFactsheet, getExampleUserProfile());
    requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), priorityProject.getReference());
    assertEquals("Number of files after updating factsheet (non existed before)", 2, requestFiles.size());
    validateFoundRequestFile(requestFiles, RequestFileType.PRIORITY_PROJECT_FACTSHEET, firstFactsheet.getFileName());
    PriorityProject refreshedProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());
    assertEquals("Should be a change added", priorityProject.getChangeHistory().size() + 1, refreshedProject.getChangeHistory().size());
    AuditTrailItem lastChange = refreshedProject.getChangeHistory().get(0);
    validateAuditTrail(lastChange, null, firstFactsheet.getFileName(), AuditTrailType.FACTSHEET);

    final InsertRequestFile secondFactsheet = getFakeFactsheetFile("second");
    PriorityProjectModifyRepository.updateFactsheet(getRegConnection(), priorityProject.getKey(), secondFactsheet, getExampleUserProfile());
    requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), priorityProject.getReference());
    assertEquals("Number of files after updating factsheet again (previous should be replaced)", 2, requestFiles.size());
    validateFoundRequestFile(requestFiles, RequestFileType.PRIORITY_PROJECT_FACTSHEET, secondFactsheet.getFileName());
    refreshedProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE, getRegMessagesKey());
    assertEquals("Should be a change added", priorityProject.getChangeHistory().size() + 2, refreshedProject.getChangeHistory().size());
    lastChange = refreshedProject.getChangeHistory().get(0);
    validateAuditTrail(lastChange, firstFactsheet.getFileName(), secondFactsheet.getFileName(), AuditTrailType.FACTSHEET);
  }


  @Test
  public void testUpdateActualisation() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());

    List<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), priorityProject.getReference());
    assertEquals("Number of files before updating actualisation (non existed before)", 1, requestFiles.size());

    final InsertRequestFile firstActualisation = getFakeActualisationFile("first");
    PriorityProjectModifyRepository.updateActualisation(getRegConnection(), priorityProject.getKey(), firstActualisation, getExampleUserProfile());
    requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), priorityProject.getReference());
    assertEquals("Number of files after updating actualisation (non existed before)", 2, requestFiles.size());
    validateFoundRequestFile(requestFiles, RequestFileType.PRIORITY_PROJECT_ACTUALISATION, firstActualisation.getFileName());
    PriorityProject refreshedProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());
    assertEquals("Should be a change added", priorityProject.getChangeHistory().size() + 1, refreshedProject.getChangeHistory().size());
    AuditTrailItem lastChange = refreshedProject.getChangeHistory().get(0);
    validateAuditTrail(lastChange, null, firstActualisation.getFileName(), AuditTrailType.ACTUALISATION);

    final InsertRequestFile secondActualisation = getFakeActualisationFile("second");
    PriorityProjectModifyRepository.updateActualisation(getRegConnection(), priorityProject.getKey(), secondActualisation, getExampleUserProfile());
    requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), priorityProject.getReference());
    assertEquals("Number of files after updating actualisation again (previous should be replaced)", 2, requestFiles.size());
    validateFoundRequestFile(requestFiles, RequestFileType.PRIORITY_PROJECT_ACTUALISATION, secondActualisation.getFileName());
    refreshedProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE, getRegMessagesKey());
    assertEquals("Should be a change added", priorityProject.getChangeHistory().size() + 2, refreshedProject.getChangeHistory().size());
    lastChange = refreshedProject.getChangeHistory().get(0);
    validateAuditTrail(lastChange, firstActualisation.getFileName(), secondActualisation.getFileName(), AuditTrailType.ACTUALISATION);
  }

  private InsertRequestFile getFakeFactsheetFile(final String fileName) {
    final InsertRequestFile fakeFactsheetFile = getFakeInsertRequestFile();
    fakeFactsheetFile.setFileName(fileName);
    fakeFactsheetFile.setFileFormat(FileFormat.PDF);
    fakeFactsheetFile.setRequestFileType(RequestFileType.PRIORITY_PROJECT_FACTSHEET);
    return fakeFactsheetFile;
  }

  private InsertRequestFile getFakeActualisationFile(final String fileName) {
    final InsertRequestFile firstActualisation = getFakeInsertRequestFile();
    firstActualisation.setFileName(fileName);
    firstActualisation.setFileFormat(FileFormat.GML);
    firstActualisation.setRequestFileType(RequestFileType.PRIORITY_PROJECT_ACTUALISATION);
    return firstActualisation;
  }

  private void validateFoundRequestFile(final List<RequestFile> requestFiles, final RequestFileType expectedRequedFileType,
      final String expectedFileName) {
    boolean found = false;
    for (final RequestFile requestFile : requestFiles) {
      if (requestFile.getRequestFileType() == expectedRequedFileType) {
        assertFalse("Should be first found", found);
        assertEquals("Request file name", expectedFileName, requestFile.getFileName());
        found = true;
      }
    }
    assertTrue("Request should be found once", found);
  }

  private void validateAuditTrail(final AuditTrailItem change, final String oldName, final String newName, final AuditTrailType auditTrailType) {
    assertEquals("Should consist of one 1 item changed", 1, change.getItems().size());
    assertEquals("Type of change", auditTrailType, change.getItems().get(0).getType());
    assertEquals("Value before change", oldName, change.getItems().get(0).getOldValue());
    assertEquals("Value after change", newName, change.getItems().get(0).getNewValue());
  }

  @Test
  public void testUpdateFactsheetIncorrectType() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());

    final InsertRequestFile factsheet = getFakeInsertRequestFile();
    try {
      PriorityProjectModifyRepository.updateFactsheet(getRegConnection(), priorityProject.getKey(), factsheet, getExampleUserProfile());
      fail("expected exception due to incorrect file type");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception (only PRIORITY_PROJECT_FACTSHEET allowed)", Reason.INTERNAL_ERROR, e.getReason());
    }
  }

  @Test
  public void testUpdateActualisationIncorrectType() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());

    final InsertRequestFile actualisation = getFakeInsertRequestFile();
    try {
      PriorityProjectModifyRepository.updateActualisation(getRegConnection(), priorityProject.getKey(), actualisation, getExampleUserProfile());
      fail("expected exception due to incorrect file type");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception (only PRIORITY_PROJECT_ACTUALISATION allowed)", Reason.INTERNAL_ERROR, e.getReason());
    }
  }

  @Test
  public void testUpdateFactsheetIncorrectFormat() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());

    final InsertRequestFile factsheet = getFakeInsertRequestFile();
    factsheet.setFileFormat(FileFormat.GML);
    factsheet.setRequestFileType(RequestFileType.PRIORITY_PROJECT_FACTSHEET);
    try {
      PriorityProjectModifyRepository.updateFactsheet(getRegConnection(), priorityProject.getKey(), factsheet, getExampleUserProfile());
      fail("expected exception due to incorrect file format");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception (only PDF allowed)", Reason.IMPORT_FILE_UNSUPPORTED, e.getReason());
    }
  }

  @Test
  public void testUpdateActualisationIncorrectFormat() throws SQLException, AeriusException {
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByReference(getRegConnection(), TEST_REFERENCE,
        getRegMessagesKey());

    final InsertRequestFile actualisation = getFakeInsertRequestFile();
    actualisation.setRequestFileType(RequestFileType.PRIORITY_PROJECT_ACTUALISATION);
    try {
      PriorityProjectModifyRepository.updateActualisation(getRegConnection(), priorityProject.getKey(), actualisation, getExampleUserProfile());
      fail("expected exception due to incorrect file format");
    } catch (final AeriusException e) {
      assertEquals("Reason for exception (only GML/ZIP allowed)", Reason.IMPORT_FILE_UNSUPPORTED, e.getReason());
    }
  }

  @Test
  public void testInsertDevelopmentSpaces() throws AeriusException, SQLException {
    final PriorityProject priorityProject = getExamplePriorityProject();

    PriorityProjectModifyRepository.insert(getRegConnection(), priorityProject, getExampleUserProfile(),
        getFakeInsertRequestFile());

    addCalculation(priorityProject, 0.05, SituationType.PROPOSED);

    PriorityProject inDB = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), priorityProject.getKey(), getRegMessagesKey());
    assertEquals("Request state before inserting development space", RequestState.INITIAL, inDB.getRequestState());

    PriorityProjectModifyRepository.insertDevelopmentSpaces(getRegConnection(), priorityProject.getKey(), getExampleUserProfile(),
        inDB.getLastModified());
    inDB = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), priorityProject.getKey(), getRegMessagesKey());
    assertEquals("Request state after inserting development space", RequestState.ASSIGNED, inDB.getRequestState());
  }

  @Test(expected = AeriusException.class)
  public void testInsertDevelopmentSpacesExceeding() throws AeriusException, SQLException {
    final PriorityProject priorityProject = getExamplePriorityProject();

    PriorityProjectModifyRepository.insert(getRegConnection(), priorityProject, getExampleUserProfile(),
        getFakeInsertRequestFile());

    addCalculation(priorityProject, 1000.0, SituationType.PROPOSED);

    final PriorityProject inDB = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), priorityProject.getKey(), getRegMessagesKey());

    PriorityProjectModifyRepository.insertDevelopmentSpaces(getRegConnection(), priorityProject.getKey(), getLowLevelExampleUserProfile(),
        inDB.getLastModified());
  }

  private PriorityProject getExamplePriorityProject() throws SQLException {
    final PriorityProject priorityProject = new PriorityProject();
    TestDomain.fillExampleRequest(priorityProject);
    final DossierMetaData metaData = new DossierMetaData();
    metaData.setDossierId(UUID.randomUUID().toString());
    priorityProject.setDossierMetaData(metaData);

    return priorityProject;
  }

}
