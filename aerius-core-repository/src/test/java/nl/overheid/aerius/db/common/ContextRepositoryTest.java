/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.context.AERIUS2Context;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.RegisterContext;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ContextRepository}.
 */
public class ContextRepositoryTest extends BaseDBTest {

  @Test
  public void testGetCalculatorLimits() throws SQLException {
    try (final Connection connection = getCalcConnection()) {
      final CalculatorLimits limits = ContextRepository.getCalculatorLimits(connection);
      assertTrue("MaxSources", limits.getMaxSources() > 0);
      assertTrue("MaxLineLength", limits.getMaxLineLength() > 0);
      assertTrue("MaxPolygonSurface", limits.getMaxPolygonSurface() > 0);
    }
  }

  @Test
  public void testGetCalculatorContext() throws SQLException, AeriusException {
    try (Connection con = getCalcConnection()) {
      final CalculatorContext context = ContextRepository.getCalculatorContext(con, getCalcMessagesKey());
      assertNotNull("Context for calculator", context);
      validateBaseContext(context, ProductType.CALCULATOR);
    }
  }

  @Test
  public void testGetRegisterContext() throws SQLException, AeriusException {
    try (Connection con = getRegConnection()) {
      final RegisterContext context = ContextRepository.getRegisterContext(con, getRegMessagesKey(), false);
      assertNotNull("Context for register", context);
      validateBaseContext(context, ProductType.REGISTER);
    }
  }

  private void validateBaseContext(final AERIUS2Context context, final ProductType product) {
    final SectorCategories categories = context.getCategories();
    assertNotNull("substances for " + product, context.getSubstances());
    assertNotNull("sectors for " + product, categories.getSectors());
    assertNotNull("farmLodgingCategories for " + product, categories.getFarmLodgingCategories());
    assertNotNull("roadEmissionCategories for " + product, categories.getRoadEmissionCategories());
    assertNotNull("onRoadMobileSourceCategories for " + product, categories.getOnRoadMobileSourceCategories());
    assertNotNull("offRoadMobileSourceCategories for " + product, categories.getOffRoadMobileSourceCategories());
    assertNotNull("planEmissionCategories for " + product, categories.getPlanEmissionCategories());
    assertNotNull("maritimeShippingCategories for " + product, categories.getMaritimeShippingCategories());
    assertNotNull("inlandShippingCategories for " + product, categories.getInlandShippingCategories());
    assertNotNull("shippingSnappableNodes for " + product, categories.getShippingSnappableNodes());
    assertNotNull("offRoadMachineryTypes for " + product, categories.getOffRoadMachineryTypes());
  }
}
