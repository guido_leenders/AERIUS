/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;

/**
 * Test class for {@link GeometryRepository}.
 */
public class GeometryRepositoryTest extends BaseDBTest {

  private static final Point AREA_POINT = new Point(123466, 473782);
  private static final float TOLERANCE = 100.0F;
  private static final String NAME_GUIDE = "Bo";
  private static final Integer AREA_ID = 83;

  @Test
  public void testAreaPointNameGuide() throws SQLException {
    final String nameGuide = "";
    final WKTGeometryWithBox geometry = GeometryRepository.getGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, AREA_POINT, nameGuide);
    assertResult(geometry);
  }

  @Test
  public void testAreaPoint() throws SQLException {
    final WKTGeometryWithBox geometry = GeometryRepository.getGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, AREA_POINT, null);
    assertResult(geometry);
  }

  @Test
  public void testNameGuide() throws SQLException {
    final WKTGeometryWithBox geometry = GeometryRepository.getGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, null, NAME_GUIDE);
    assertResult(geometry);
  }

  @Test
  public void testOnlyArea() throws SQLException {
    final WKTGeometryWithBox geometry = GeometryRepository.getGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, null, null);
    assertResult(geometry);
  }

  @Test
  public void testToleranceAreaPointNameGuide() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, AREA_POINT, TOLERANCE, NAME_GUIDE);
    assertResult(geometry);
  }

  @Test
  public void testToleranceAreaPoint() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, AREA_POINT, TOLERANCE, null);
    assertResult(geometry);
  }

  @Test
  public void testToleranceNameGuide() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, null, TOLERANCE, NAME_GUIDE);
    assertResult(geometry);
  }

  @Test
  public void testTolerance() throws SQLException {
    final WKTGeometryWithBox geometry =
        GeometryRepository.getSimplifiedGeometry(getCalcPMF(), AreaType.NATURA2000_AREA, AREA_ID, null, TOLERANCE, null);
    assertResult(geometry);
  }

  private void assertResult(final WKTGeometryWithBox geometry) {
    assertNotNull("Geometry not null", geometry == null);
    assertNotNull("Bounding Box not null", geometry.getBoundingBox());
  }

}
