/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.DetermineCalculationPointResult;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;

/**
 *
 */
public class CalculationPointRepositoryTest extends BaseDBTest {

  @Test
  public void testDeterminePointsAutomatically() throws SQLException {
    int distanceFromSource;
    //first < switch limit
    distanceFromSource = CalculationPointRepository.SWITCH_TO_ASSESSMENT_ONLY_LIMIT - 1;
    DetermineCalculationPointResult result = CalculationPointRepository.determinePointsAutomatically(getCalcConnection(), distanceFromSource, getExampleSourceLists());
    assertNotEquals("Should be some results", 0, result.getAmountOfAssessmentAreas());
    assertNotEquals("Should be some results", 0, result.getPoints().size());
    assertAllCoordinatesRounded(result.getPoints());
    //now for > switch limit
    distanceFromSource = CalculationPointRepository.SWITCH_TO_ASSESSMENT_ONLY_LIMIT + 1;
    result = CalculationPointRepository.determinePointsAutomatically(getCalcConnection(), distanceFromSource, getExampleSourceLists());
    assertNotEquals("Should be some results", 0, result.getAmountOfAssessmentAreas());
    assertNotEquals("Should be some results", 0, result.getPoints().size());
    assertAllCoordinatesRounded(result.getPoints());
    //check no results for distance 0.
    distanceFromSource = 0;
    result = CalculationPointRepository.determinePointsAutomatically(getCalcConnection(), distanceFromSource, getExampleSourceLists());
    assertEquals("Should be some results", 0, result.getAmountOfAssessmentAreas());
    assertEquals("Should be some results", 0, result.getPoints().size());
    assertAllCoordinatesRounded(result.getPoints());
    //check no results for negative distance.
    distanceFromSource = -10;
    result = CalculationPointRepository.determinePointsAutomatically(getCalcConnection(), distanceFromSource, getExampleSourceLists());
    assertEquals("Should be some results", 0, result.getAmountOfAssessmentAreas());
    assertEquals("Should be some results", 0, result.getPoints().size());
    assertAllCoordinatesRounded(result.getPoints());
    //check no results for no sources.
    distanceFromSource = CalculationPointRepository.SWITCH_TO_ASSESSMENT_ONLY_LIMIT - 1;
    result = CalculationPointRepository.determinePointsAutomatically(getCalcConnection(), distanceFromSource, new ArrayList<EmissionSourceList>());
    assertEquals("Should be some results", 0, result.getAmountOfAssessmentAreas());
    assertEquals("Should be some results", 0, result.getPoints().size());
    assertAllCoordinatesRounded(result.getPoints());
  }

  private ArrayList<EmissionSourceList> getExampleSourceLists() {
    final ArrayList<EmissionSourceList> sourceLists = new ArrayList<>();
    final EmissionSourceList list = new EmissionSourceList();
    final GenericEmissionSource source1 = new GenericEmissionSource(1, new Point(172230, 447455));
    source1.setGeometry(new WKTGeometry(source1.toWKT(), 1));
    list.add(source1);
    final GenericEmissionSource source2 = new GenericEmissionSource(2, new Point(172240, 447455));
    source2.setGeometry(new WKTGeometry("LINESTRING(172240 447405,172240 447505)", 100));
    list.add(source2);
    final GenericEmissionSource source3 = new GenericEmissionSource(3, new Point(172200, 447400));
    source3.setGeometry(new WKTGeometry("POLYGON((172200 447400,172200 447500,172300 447400,172200 447400))", 100));
    list.add(source3);
    sourceLists.add(list);
    return sourceLists;
  }

  /**
   * Asserts whether both coordinates of all points have correctly been rounded.
   * 
   * 
   * @param calculationPoints
   */
  private void assertAllCoordinatesRounded(List<AeriusPoint> calculationPoints) {
    for (final AeriusPoint point : calculationPoints) {
      final double theX = point.getX();
      assertEquals("The x coordinate should have been rounded before", Math.round(theX), theX, 0d);
      final double theY = point.getY();
      assertEquals("The y coordinate should have been rounded before", Math.round(theY), theY, 0d);
    }
  }
}
