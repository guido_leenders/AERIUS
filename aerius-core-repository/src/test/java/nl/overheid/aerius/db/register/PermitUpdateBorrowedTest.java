/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;

public class PermitUpdateBorrowedTest extends RequestTestBase {

  private static final String RESERVED_DEVELOPMENT_SPACE_BORROWED_RECEPTOR_TO =
      "UPDATE reserved_development_spaces SET borrowed = ? WHERE receptor_id = ? AND segment = 'projects'";

  private static final double SPACE_BY_DEFAULT = 10.0;
  private static final double EPSILON = 0.0001;

  private UserProfile userProfile;
  private Permit permit;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();

    userProfile = getExampleUserProfile();

    // We want to have SPACE_BY_DEFAULT space free by default, so we need to set reserved development space to the assigned space + SPACE_BY_DEFAULT
    for (final AeriusResultPoint point : getExampleResultPoints(1.0)) {
      final int receptorId = point.getId();

      setReservedDevelopmentSpace(receptorId, SegmentType.PROJECTS, getAssignedDevelopmentSpace(receptorId, SegmentType.PROJECTS) + SPACE_BY_DEFAULT);
    }
    // example permit has state PENDING_WITH_SPACE
    permit = prepareExamplePermit();
  }

  @Test
  public void testWithoutBorrowed() throws Exception {
    permit = updateStateTo(permit, RequestState.ASSIGNED, userProfile);

    assertEquals("State should be updated to ASSIGNED", RequestState.ASSIGNED, permit.getRequestState());
  }

  @Test
  public void testWithBorrowed() throws Exception {
    permit = updateStateTo(permit, RequestState.ASSIGNED, userProfile);

    assertEquals("State should be updated to ASSIGNED", RequestState.ASSIGNED, permit.getRequestState());

    final double toBorrow = SPACE_BY_DEFAULT / 2;

    // Keep track of initial available development spaces before removal of permit borrowing space
    final Map<Integer, Double> initialAvailable = new HashMap<>();
    for (final AeriusResultPoint point : getExampleResultPoints(1.0)) {
      final int receptorId = point.getId();

      updateBorrowedForReceptor(receptorId, toBorrow);
      updateInitialAvailableDevelopmentSpacesReceptor(receptorId);

      initialAvailable.put(receptorId, getInitialAvailableDevelopmentSpaceReceptor(receptorId, SegmentType.PROJECTS));
    }

    // Delete permit - borrowed space should not flow back
    PermitModifyRepository.deletePermit(getRegConnection(), permit, userProfile);

    // We expect that all initial available development spaces should have been subtracted by toBorrow
    for (final AeriusResultPoint point : getExampleResultPoints(1.0)) {
      final int receptorId = point.getId();

      assertEquals("Initial available isn't subtracted by toBorrow as expected =(",
          initialAvailable.get(receptorId) - toBorrow,
          getInitialAvailableDevelopmentSpaceReceptor(receptorId, SegmentType.PROJECTS),
          EPSILON);
    }
  }

  private Permit prepareExamplePermit() throws SQLException, AeriusException {
    Permit permit = getInsertedExamplePermit();
    permit = PermitRepository.getPermit(getRegConnection(), permit.getId());

    final Calculation calculation = getExampleCalculation();
    final int calculationId = calculation.getCalculationId();

    // request has INITIAL state by default, insert results/demands and after update state to QUEUED.
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationId, permit);
    addResults(calculationId);
    permit = updateStateTo(permit, RequestState.QUEUED, userProfile);

    // we cannot use the dequeuePermits() solution as it takes several minutes
    permit = updateStateTo(permit, RequestState.PENDING_WITH_SPACE, userProfile);

    return permit;
  }

  /**
   * Adds results which will claim the full space that is left (without taking borrowed into account).
   * @throws SQLException
   */
  private void addResults(final int calculationId) throws SQLException {
    final List<AeriusResultPoint> result = getExampleResultPoints(1);

    for (final AeriusResultPoint point : result) {
      final HashMap<EmissionResultKey, Double> map = point.getEmissionResults().getHashMap();

      // If we have multiple substances we should split SPACE_BY_DEFAULT by the amount so we still want SPACE_BY_DEFAULT in total
      for (final EmissionResultKey erk : point.getEmissionResults().getHashMap().keySet()) {
        point.setEmissionResult(erk, SPACE_BY_DEFAULT / map.size());
      }
    }

    CalculationRepository.insertCalculationResults(getRegConnection(), calculationId, result);
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getRegConnection(), calculationId, 0, true);
  }

  private Permit updateStateTo(final Permit permit, final RequestState requestState, final UserProfile userProfile)
      throws SQLException, AeriusException {
    PermitModifyRepository.updateState(getRegConnection(), permit.getPermitKey(), requestState, userProfile, permit.getLastModified());
    return PermitRepository.getPermit(getRegConnection(), permit.getId());
  }

  protected void updateBorrowedForReceptor(final int receptorId, final double borrowed) throws SQLException {
    try (final PreparedStatement stmt = getRegConnection().prepareStatement(RESERVED_DEVELOPMENT_SPACE_BORROWED_RECEPTOR_TO)) {
      QueryUtil.setValues(stmt, borrowed, receptorId);

      stmt.executeUpdate();
    }
  }

}
