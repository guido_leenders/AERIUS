/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.validation;

import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.util.EnumUtil;

@RunWith(value = Parameterized.class)
public class ValidationResultsTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(ValidationResultsTest.class);

  private enum ResultType {
    SUCCESS, HINT, WARNING, ERROR;
  }

  private static final String GET_LAST_VALIDATION_RUN_VALIDATION_RESULT_COUNT = "SELECT * FROM setup.last_validation_run_results_view";

  private final String productName;
  private final int validationRunId;
  private final String validationFunctionName;
  private final ResultType resultType;

  public ValidationResultsTest(final String productName, final int validationRunId, final String validationFunctionName, final ResultType resultType) {
    this.productName = productName;
    this.validationRunId = validationRunId;
    this.validationFunctionName = validationFunctionName;
    this.resultType = resultType;
  }

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    // Prevent BaseDBTest.setUpBeforeClass from being called twice
  }

  @Parameters(name = "{0}:{2}")
  public static List<Object[]> data() throws SQLException, IOException {
    BaseDBTest.setUpBeforeClass();
    final ArrayList<Object[]> validationResults = new ArrayList<>();
    validationResults.addAll(getValidationResults(getCalcPMF().getConnection(), getCalcPMF().getProductType()));
    validationResults.addAll(getValidationResults(getRegPMF().getConnection(), getRegPMF().getProductType()));
    return validationResults;
  }

  private static ArrayList<Object[]> getValidationResults(final Connection con, final ProductType productType) throws SQLException {
    final ArrayList<Object[]> validationResults = new ArrayList<>();
    try (final PreparedStatement stmt = con.prepareStatement(GET_LAST_VALIDATION_RUN_VALIDATION_RESULT_COUNT)) {
      final ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        validationResults.add(new Object[] {
            productType.toString(),
            rs.getInt("validation_run_id"),
            rs.getString("name"),
            EnumUtil.get(ResultType.class, rs.getString("result")) });
      }
    }
    return validationResults;
  }

  @Test
  public void testValidationFunctions() throws SQLException {
    LOG.info("{} validation function {} (run id {}): Result was {}.",
        productName, validationFunctionName, validationRunId, resultType);
    assertNotEquals("Validation of function '" + validationFunctionName + "' failed (product:" + productName + ")",
        ResultType.ERROR, resultType);
    //warnings and hints are not considered failures.
  }
}
