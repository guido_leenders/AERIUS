/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;

import org.junit.Test;

import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link DevelopmentRuleRepository}.
 */
public class DevelopmentRuleRepositoryTest extends RequestTestBase {

  @Test
  public void testGetPermitResultsByArea() throws SQLException, AeriusException, IOException {
    final Permit request = getInsertedExamplePermit();

    HashSet<DevelopmentRuleResultList> results = DevelopmentRuleRepository
        .getPermitResultsByArea(getRegConnection(), request.getPermitKey());

    assertTrue("Without calculation, results should be empty.", results.isEmpty());

    final int proposedId = addCalculation(request, 1.0, SituationType.PROPOSED);
    results = DevelopmentRuleRepository.getPermitResultsByArea(getRegConnection(), request.getPermitKey());
    assertFalse("With just proposed calculation, results shouldn't be empty.", results.isEmpty());
    assertDevelopmentRuleResults(results);

    final int currentId = addCalculation(request, 0.3, SituationType.CURRENT);
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getRegConnection(), proposedId, currentId, true);
    results = DevelopmentRuleRepository.getPermitResultsByArea(getRegConnection(), request.getPermitKey());
    assertFalse("With both proposed and current calculation, results shouldn't be empty.", results.isEmpty());
    assertDevelopmentRuleResults(results);
  }

  private void assertDevelopmentRuleResults(final HashSet<DevelopmentRuleResultList> results) {
    for (final DevelopmentRuleResultList result : results) {
      assertNotEquals("Assessment area bounding box", 0, result.getAssessmentArea().getId());
      assertNotNull("Assessment area name", result.getAssessmentArea().getName());
      assertNotNull("Assessment area bounding box", result.getAssessmentArea().getBounds());
      assertFalse("Results for the area", result.getDevelopmentRuleResults().isEmpty());
    }
  }

}
