/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeFalse;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
@RunWith(Parameterized.class)
public class PermitUpdateStateTest extends RequestTestBase {

  private static final Logger LOGGER = LoggerFactory.getLogger(PermitUpdateStateTest.class);

  private static final EnumSet<RequestState> ASSIGNED_SPACE_STATES = EnumSet.of(RequestState.ASSIGNED, RequestState.ASSIGNED_FINAL);

  private static final String SQL_SET_REQUEST_STATE = "UPDATE requests SET status = ?::request_status_type WHERE request_id = ?";

  @Parameter(value = 0) public RequestState initialState;
  @Parameter(value = 1) public RequestState targetState;
  @Parameter(value = 2) public boolean isInvalidStateChange;

  private boolean expectSqlException;
  private boolean expectDoesNotFitException;
  private boolean exceedingSpaceTest;

  @Parameters(name = "{0} -> {1} (isInvalidStateChange={2})")
  public static List<Object[]> data() throws FileNotFoundException {
    final Map<RequestState, List<RequestState>> validStateChanges = getSuperUserValidStateChanges();

    final List<Object[]> objects = new ArrayList<>();
    for (final RequestState initial : RequestState.values()) {
      for (final RequestState target : RequestState.values()) {
        if (initial != RequestState.REJECTED_FINAL) {
          objects.add(new Object[] {initial, target, !validStateChanges.get(initial).contains(target)});
        }
      }
    }
    return objects;
  }

  @Test
  public void testUpdateState() throws Exception {
    exceedingSpaceTest = false;
    expectSqlException = isInvalidStateChange;
    expectDoesNotFitException = false;
    actualStateChangeTest();
  }

  @Test
  public void testUpdateStateExceedingSpace() throws Exception {
    assumeFalse(isInvalidStateChange); // skip invalid state changes, the other test already did these

    exceedingSpaceTest = true;
    expectSqlException = false;
    expectDoesNotFitException = couldThrowDoesNotFitException(initialState, targetState);
    actualStateChangeTest();
  }

  private static Map<RequestState, List<RequestState>> getSuperUserValidStateChanges() {
    final Map<RequestState, List<RequestState>> validStateChanges = getNormalUserValidStateChanges();
    //superuser can't do much more then a regular user (to prevent assigning too much OR which would introduce anomalies)
    //he can however switch from initial to queued by hand
    validStateChanges.get(RequestState.INITIAL).add(RequestState.QUEUED);
    //and from queued to the two pending states.
    validStateChanges.get(RequestState.QUEUED).add(RequestState.PENDING_WITH_SPACE);
    validStateChanges.get(RequestState.QUEUED).add(RequestState.PENDING_WITHOUT_SPACE);
    //and other way around
    //these changes are not entirely without risks: if a user changes to queued before there's actually a finished calculation,
    //the permit will pass everything...
    validStateChanges.get(RequestState.PENDING_WITH_SPACE).add(RequestState.QUEUED);
    validStateChanges.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.QUEUED);
    //superusers can reject finally
    validStateChanges.get(RequestState.ASSIGNED).add(RequestState.REJECTED_FINAL);
    validStateChanges.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.REJECTED_FINAL);
    //and enqueue a rejected without space
    validStateChanges.get(RequestState.REJECTED_WITHOUT_SPACE).add(RequestState.QUEUED);
    return validStateChanges;
  }

  private static Map<RequestState, List<RequestState>> getNormalUserValidStateChanges() {
    //normal user can only do the valid flow changes from the pending states onwards.
    final Map<RequestState, List<RequestState>> validStateChanges = getInitialStateChangeMap();
    //normal user can't change from initial or queued: automatic processes.

    //pending_without_space to rejected_without_space is always fine
    validStateChanges.get(RequestState.PENDING_WITHOUT_SPACE).add(RequestState.REJECTED_WITHOUT_SPACE);

    //rejected_without_space to rejected_final is fine as well (rejected_without_space is only there to generate a 'bijlage bij besluit')
    //however, rejected_final is actually a 'delete' action...

    //Assigned to Assigned_final are always fine as well
    //apparantly the request fit the OR at some point, and the OR has already been reserved for this particular request.
    //It shouldn't be counted twice (that is, the demand of the request shouldn't be looked at).
    validStateChanges.get(RequestState.ASSIGNED).add(RequestState.ASSIGNED_FINAL);
    //from assigned it's possible to go to rejected_final as well, but again, that's a 'delete' action.

    //in addition to the exceeding state changes:
    //normal user can change from pending_with_space to their appropriate next states when there is actually space.
    //pending_with_space to assigned
    validStateChanges.get(RequestState.PENDING_WITH_SPACE).add(RequestState.ASSIGNED);

    return validStateChanges;
  }

  private static Map<RequestState, List<RequestState>> getInitialStateChangeMap() {
    final Map<RequestState, List<RequestState>> validStateChanges = new HashMap<>();
    for (final RequestState state : RequestState.values()) {
      // rejected_final is just a UI state to indicate deletion. We don't expect it here.
      if (state != RequestState.REJECTED_FINAL) {
        validStateChanges.put(state, new ArrayList<RequestState>());
        // from all states it's valid to go to rejected_final, because the only thing it does is save an audit trail which never fails
        validStateChanges.get(state).add(RequestState.REJECTED_FINAL);
      }
    }
    return validStateChanges;
  }

  private static boolean couldThrowDoesNotFitException(final RequestState from, final RequestState to) {
    return ASSIGNED_SPACE_STATES.contains(to) && !ASSIGNED_SPACE_STATES.contains(from);
  }

  private void actualStateChangeTest() throws SQLException, AeriusException {
    final Permit permit = prepareExamplePermit();

    if (exceedingSpaceTest && !isInvalidStateChange) {
      // decrease reserved development spaces so next state change won't fit
      setReservedDevelopmentSpacesForResultPoints(getExampleResultPoints(1.0), SegmentType.PROJECTS, 0);
    }

    try {
      performTargetStateChange(permit);
      handleNoExceptionThrown(permit);
    } catch (final AeriusException e) {
      handleExceptionThrown(e);
    }
  }

  private void performTargetStateChange(final Permit permit) throws SQLException, AeriusException {
    LOGGER.debug("Target state change to {}...", targetState.name());
    final UserProfile userProfile = getExampleUserProfile();
    PermitModifyRepository.updateState(getRegConnection(), permit.getPermitKey(), targetState, userProfile, permit.getLastModified());
  }

  private void handleExceptionThrown(final AeriusException e) throws AeriusException {
    if (!expectSqlException && !expectDoesNotFitException) {
      throw e;
    }

    if (expectSqlException) {
      assertEquals("SQL error expected but got something else", Reason.SQL_ERROR, e.getReason());
    }
    if (expectDoesNotFitException) {
      assertEquals("PERMIT_DOES_NOT_FIT expected as reason but got something else", Reason.PERMIT_DOES_NOT_FIT, e.getReason());
    }
  }

  private void handleNoExceptionThrown(final Permit permit) throws SQLException {
    if (expectSqlException) {
      fail("Expected an exception (AeriusException Reason=SQL_ERROR), but did not get one.");
    } else if (expectDoesNotFitException) {
      fail("Expected an exception (AeriusException Reason=PERMIT_DOES_NOT_FIT, but did not get one.");
    } else {
      // refresh the permit from DB.
      final Permit permitFromDB = PermitRepository.getPermit(getRegConnection(), permit.getId());
      if (targetState == RequestState.REJECTED_FINAL) {
        assertEquals("Ending with state", initialState, permitFromDB.getRequestState());
      } else {
        assertEquals("Ending with state", targetState, permitFromDB.getRequestState());
      }
    }
  }

  private Permit prepareExamplePermit() throws SQLException, AeriusException {
    final Permit emptyPermit = getInsertedExamplePermit();
    final Permit examplePermit;
    if (isInvalidStateChange) {
      // No demands needed for the invalid state change test
      examplePermit = emptyPermit;
    } else {
      examplePermit = expandPermitWithExampleCalculation(emptyPermit);
    }
    LOGGER.debug("Testing permit {} {} with dossierId {}...", examplePermit.getId(), examplePermit.getReference(),
        examplePermit.getDossierMetaData().getDossierId());
    LOGGER.debug("Test: {} -> {}", initialState.name(), targetState.name());
    return setupInitialState(examplePermit, initialState);
  }

  private Permit expandPermitWithExampleCalculation(final Permit permit) throws SQLException, AeriusException {
    final Calculation calculation = getExampleCalculation();
    final int calculationId = calculation.getCalculationId();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, calculationId, permit);
    addCalculationResults(1.0, calculationId);
    return PermitRepository.getPermit(getRegConnection(), permit.getId());
  }

  private Permit setupInitialState(final Permit permit, final RequestState state) throws SQLException, AeriusException {
    final Permit updatedPermit;
    if (isInvalidStateChange) {
      // We don't have to move development space because there is never a space check, only a
      // state transition check in the database.
      updatedPermit = updateWithoutSideEffectsTo(initialState, permit);
    } else {
      clearExampleReceptorsDevelopmentSpaces();
      updatedPermit = updateProperlyTo(initialState, permit);
    }

    assertEquals("Starting state", initialState, updatedPermit.getRequestState());
    return updatedPermit;
  }

  private Permit updateWithoutSideEffectsTo(final RequestState requestState, final Permit permit) throws SQLException {
    LOGGER.debug("Hard initial state change to {}...", requestState.name());
    try (final Connection con = getRegConnection(); final PreparedStatement stmt = con.prepareStatement(SQL_SET_REQUEST_STATE)) {
      stmt.setString(1, requestState.getDbValue());
      stmt.setInt(2, permit.getId());
      stmt.executeUpdate();
    }
    return PermitRepository.getPermit(getRegConnection(), permit.getId());
  }

  private Permit updateProperlyTo(final RequestState state, final Permit permit) throws SQLException, AeriusException {
    final UserProfile userProfile = getExampleUserProfile();
    switch (state) {
    case INITIAL:
      return permit;
    case QUEUED:
      return updateToQueued(permit, userProfile);
    case PENDING_WITH_SPACE:
      return updateToPendingWithSpace(permit, userProfile);
    case PENDING_WITHOUT_SPACE:
      return updateToPendingWithoutSpace(permit, userProfile);
    case ASSIGNED:
      return updateToAssigned(permit, userProfile);
    case ASSIGNED_FINAL:
      return updateToAssignedFinal(permit, userProfile);
    case REJECTED_WITHOUT_SPACE:
      return updateToRejectedWithoutSpace(permit, userProfile);
    default:
      throw new IllegalArgumentException("Invalid state");
    }
  }

  private Permit updateToQueued(final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    return updateTo(RequestState.QUEUED, permit, userProfile);
  }

  private Permit updateToPendingWithSpace(final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    return updateTo(RequestState.PENDING_WITH_SPACE, updateToQueued(permit, userProfile), userProfile);
  }

  private Permit updateToPendingWithoutSpace(final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    return updateTo(RequestState.PENDING_WITHOUT_SPACE, updateToQueued(permit, userProfile), userProfile);
  }

  private Permit updateToAssigned(final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    return updateTo(RequestState.ASSIGNED, updateToPendingWithSpace(permit, userProfile), userProfile);
  }

  private Permit updateToAssignedFinal(final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    return updateTo(RequestState.ASSIGNED_FINAL, updateToAssigned(permit, userProfile), userProfile);
  }

  private Permit updateToRejectedWithoutSpace(final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    return updateTo(RequestState.REJECTED_WITHOUT_SPACE, updateToPendingWithoutSpace(permit, userProfile), userProfile);
  }

  private Permit updateTo(final RequestState requestState, final Permit permit, final UserProfile userProfile) throws SQLException, AeriusException {
    LOGGER.debug("Initial state change to {}...", requestState.name());
    PermitModifyRepository.updateState(getRegConnection(), permit.getPermitKey(), requestState, userProfile, permit.getLastModified());
    return PermitRepository.getPermit(getRegConnection(), permit.getId());
  }

}
