/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.postgis.PGbox2d;
import org.postgis.PGgeometry;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Geometry;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;

public class PGisUtilsTest {

  @Test
  public void testGetPoint() {
    final Point nullPoint = PGisUtils.getPoint(null);
    assertNull("input null should return null", nullPoint);

    //only testing if point actually gets converted to a point, not any other geometry.
    final org.postgis.Point pgPoint = getExamplePGPoint();
    final PGgeometry pgGeometry = new PGgeometry();
    pgGeometry.setGeometry(pgPoint);
    final Point point = PGisUtils.getPoint(pgGeometry);
    assertEquals("X coord", pgPoint.getX(), point.getX(), 0.001);
    assertEquals("Y coord", pgPoint.getY(), point.getY(), 0.001);

    //not only points will be converted, any geometry will
    final org.postgis.LineString pgLineString = getExamplePGLineString();
    final PGgeometry pgGeometryWithLine = new PGgeometry();
    pgGeometryWithLine.setGeometry(pgLineString);
    final Point pointFromLineString = PGisUtils.getPoint(pgGeometry);
    assertNotNull("Any geometry should be converted", pointFromLineString);
    //could probably figure out where the point should be in this case, but not bothering.
  }

  @Test
  public void testGetGeometry() {
    final org.postgis.Point pgPoint = getExamplePGPoint();
    final PGgeometry pgGeometryWithPoint = new PGgeometry();
    pgGeometryWithPoint.setGeometry(pgPoint);
    final Geometry point = PGisUtils.getGeometry(pgGeometryWithPoint);
    assertTrue("geometry should be point", point instanceof Point);

  }

  @Test
  public void testGetBox() {
    final org.postgis.Point lowerLeftPoint = new org.postgis.Point(10.0, 10.0);
    final org.postgis.Point upperRightPoint = new org.postgis.Point(20.0, 30.0);
    final PGbox2d pgBBox = new PGbox2d(lowerLeftPoint, upperRightPoint);
    final BBox bbox = PGisUtils.getBox(pgBBox);
    assertEquals("minX", pgBBox.getLLB().getX(), bbox.getMinX(), 0.001);
    assertEquals("minY", 10, bbox.getMinY(), 0.001);
    assertEquals("maxX", 20, bbox.getMaxX(), 0.001);
    assertEquals("maxY", 30, bbox.getMaxY(), 0.001);
    assertEquals("width", 10, bbox.getWidth(), 0.001);
    assertEquals("height", 20, bbox.getHeight(), 0.001);
  }

  @Test
  public void testGetBoxNullArgument() {
    final BBox nullBBox = PGisUtils.getBox(null);
    assertNull("input null should return null", nullBBox);
  }

  @Test
  public void testGetGeometryWKT() {
    //just test if it returns as expected, no need for all cases as that'd be testing the postgis library...
    org.postgis.Geometry geometry = PGisUtils.getGeometry("POINT(1 2)");
    geometry = PGisUtils.getGeometry("POINT (1 2)");
    assertNotNull("Geometry", geometry);
    assertTrue("Geometry is a point", geometry instanceof org.postgis.Point);
    assertEquals("Point x", 1, geometry.getPoint(0).getX(), 0.001);
    assertEquals("Point y", 2, geometry.getPoint(0).getY(), 0.001);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetGeometryWrongWKT() {
    PGisUtils.getGeometry("PINT(1 2)");
  }

  private Point getExamplePoint() {
    final double xCoord = 7.80;
    final double yCoord = 4.55;
    return new Point(xCoord, yCoord);
  }

  private WKTGeometry getExampleLineString() {
    final double xCoord0 = 7.80;
    final double yCoord0 = 4.55;
    final double xCoord1 = 44.77;
    final double yCoord1 = 2.4;
    final String wktLineString = "LINESTRING ("
        + xCoord0 + " " + yCoord0 + ","
        + xCoord1 + " " + yCoord1 + ")";
    final double length = Math.sqrt(
        Math.pow(xCoord0 - xCoord1, 2)
            + Math.pow(yCoord0 - yCoord1, 2)
        );
    return new WKTGeometry(wktLineString, (int) Math.round(length));
  }

  private org.postgis.Point getExamplePGPoint() {
    final double xCoord = 1.10;
    final double yCoord = 3.01;
    return new org.postgis.Point(xCoord, yCoord);
  }

  private org.postgis.LineString getExamplePGLineString() {
    final double xCoord0 = 4.10;
    final double yCoord0 = 4.01;
    final double xCoord1 = 5.05;
    final double yCoord1 = 7.698;
    final org.postgis.Point[] points = new org.postgis.Point[2];
    points[0] = new org.postgis.Point(xCoord0, yCoord0);
    points[1] = new org.postgis.Point(xCoord1, yCoord1);
    return new org.postgis.LineString(points);
  }

}
