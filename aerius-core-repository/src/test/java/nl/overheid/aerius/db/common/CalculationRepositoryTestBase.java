/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Base class for calculation related tests.
 */
public abstract class CalculationRepositoryTestBase extends BaseDBTest {

  //x: 187026 y: 464594, Veluwe
  protected static final int RECEPTOR_POINT_ID_1 = 4776053;
  //x: 112298 y: 556953, Duinen en Lage Land Texel
  protected static final int RECEPTOR_POINT_ID_2 = 7404003;
  //point outside nature area; buried the body here.
  protected static final int RECEPTOR_POINT_ID_3 = 4758851;
  //x: 174184 y: 326512, Foreign area
  protected static final int RECEPTOR_POINT_ID_4 = 846454;
  //x: 270595 y: 474158, Foreign area
  protected static final int RECEPTOR_POINT_ID_5 = 5048664;

  protected static final double RECEPTOR_1_CONCENTRATION = 42.91;
  protected static final double RECEPTOR_1_DEPOSITION = 234.23;
  protected static final double RECEPTOR_1_NUM_EXCESS_DAYS = 60;
  protected static final double RECEPTOR_2_CONCENTRATION = 4.82;
  protected static final double RECEPTOR_2_DEPOSITION = 1.23;
  protected static final double RECEPTOR_2_NUM_EXCESS_DAYS = 10;
  protected static final double RECEPTOR_3_CONCENTRATION = 9.31;
  protected static final double RECEPTOR_3_DEPOSITION = 9.1;
  protected static final double RECEPTOR_3_NUM_EXCESS_DAYS = 9;
  protected static final double RECEPTOR_4_CONCENTRATION = 23.2;
  protected static final double RECEPTOR_4_DEPOSITION = 23.1;
  protected static final double RECEPTOR_4_NUM_EXCESS_DAYS = 15;
  protected static final double RECEPTOR_5_CONCENTRATION = 0.2;
  protected static final double RECEPTOR_5_DEPOSITION = 0.1;
  protected static final double RECEPTOR_5_NUM_EXCESS_DAYS = 0.15;

  protected static final int TEST_CALCULATION_RESULTS_SECTOR_ID = 3220;

  protected Calculation calculation;

  protected double calculationResultScale = 1.0; // Change this to create different results

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    refreshCalculationSet();
  }

  protected void insertCalculationResults() throws SQLException {
    insertCalculationResultsNH3();
    insertCalculationResultPM10();
  }

  protected PartialCalculationResult insertCalculationPoints(final Substance substance) throws SQLException {
    final PartialCalculationResult partialCalculationResult = getExampleCalculationPointOutputData(substance);
    final List<AeriusPoint> calculationPointList = new ArrayList<>();
    for (final AeriusResultPoint point : partialCalculationResult.getResults()) {
      if (point instanceof AeriusPoint) {
        calculationPointList.add(point);
      }
    }
    partialCalculationResult.setCalculationId(calculation.getCalculationId());
    final int calculationPointSetId = CalculationRepository.insertCalculationPointSet(getConnection(), calculationPointList);
    partialCalculationResult.setCalculationId(calculationPointSetId);
    return partialCalculationResult;
  }

  protected PartialCalculationResult insertCalculationResultsNH3() throws SQLException {
    final PartialCalculationResult calculationResult = getExampleOPSOutputData(EmissionResultKey.NH3_CONCENTRATION, EmissionResultKey.NH3_DEPOSITION);
    // 3 receptors * 2 result types = 6 results.
    final int numberOfResults = 6;
    insertResults(calculationResult, numberOfResults);
    return calculationResult;
  }

  protected PartialCalculationResult insertCalculationResultPM10() throws SQLException {
    final PartialCalculationResult calculationResult = getExampleOPSOutputData(EmissionResultKey.PM10_CONCENTRATION,
        EmissionResultKey.PM10_NUM_EXCESS_DAY);
    final int numberOfResults = 3;
    // 3 receptors * 2 result types = 6 results. However, num excess days won't be persisted. Hence 3 results.
    insertResults(calculationResult, numberOfResults);
    return calculationResult;
  }

  protected void insertResults(final PartialCalculationResult calculationResult, final int numberOfResults) throws SQLException {
    calculationResult.setCalculationId(calculation.getCalculationId());

    final int numTotal = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        calculationResult.getResults());
    assertEquals("Number of emission results inserted", numberOfResults, numTotal);

    final int numSector = CalculationRepository.insertCalculationResults(getConnection(), calculation.getCalculationId(),
        CalculationResultSetType.SECTOR, TEST_CALCULATION_RESULTS_SECTOR_ID, calculationResult.getResults());
    assertEquals("Number of emission results inserted", numberOfResults, numSector);
  }

  protected PartialCalculationResult getExampleOPSOutputData(final EmissionResultKey concentrationKey, final EmissionResultKey otherKey) {
    final PartialCalculationResult result = new PartialCalculationResult();

    result.add(createResult(RECEPTOR_POINT_ID_1, concentrationKey, otherKey,
        RECEPTOR_1_CONCENTRATION, RECEPTOR_1_DEPOSITION, RECEPTOR_1_NUM_EXCESS_DAYS));
    result.add(createResult(RECEPTOR_POINT_ID_2, concentrationKey, otherKey,
        RECEPTOR_2_CONCENTRATION, RECEPTOR_2_DEPOSITION, RECEPTOR_2_NUM_EXCESS_DAYS));
    result.add(createResult(RECEPTOR_POINT_ID_3, concentrationKey, otherKey,
        RECEPTOR_3_CONCENTRATION, RECEPTOR_3_DEPOSITION, RECEPTOR_3_NUM_EXCESS_DAYS));

    //point in foreign country, somewhere in belgium... Disabled for now as it breaks quite some unittests
//    final AeriusResultPoint rp4 = new AeriusResultPoint();
//    rp4.setId(RECEPTOR_POINT_ID_4);
//    ReceptorUtil.setAeriusPointFromId(rp4);
//    rp4.setEmissionResult(concentrationKey, RECEPTOR_4_CONCENTRATION * calculationResultScale);
//    rp4.setEmissionResult(otherKey, (otherKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? RECEPTOR_4_DEPOSITION
//        : RECEPTOR_4_NUM_EXCESS_DAYS) * calculationResultScale);
//    result.add(rp4);

    //another point in another foreign country, this time germany
//    final AeriusResultPoint rp5 = new AeriusResultPoint();
//    rp5.setId(RECEPTOR_POINT_ID_5);
//    ReceptorUtil.setAeriusPointFromId(rp5);
//    rp5.setEmissionResult(concentrationKey, RECEPTOR_5_CONCENTRATION * calculationResultScale);
//    rp5.setEmissionResult(otherKey, (otherKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? RECEPTOR_5_DEPOSITION
//        : RECEPTOR_5_NUM_EXCESS_DAYS) * calculationResultScale);
//    result.add(rp5);

    return result;
  }

  protected AeriusResultPoint createResult(final int id, final EmissionResultKey concentrationKey, final EmissionResultKey otherKey, final double... emissions) {
    final AeriusResultPoint rp1 = new AeriusResultPoint();
    rp1.setId(id);
    RECEPTOR_UTIL.setAeriusPointFromId(rp1);
    if (concentrationKey != null) {
      rp1.setEmissionResult(concentrationKey, emissions[0] * calculationResultScale);
    }
    rp1.setEmissionResult(otherKey, (otherKey.getEmissionResultType() == EmissionResultType.DEPOSITION ? emissions[1] : emissions[2])
      * calculationResultScale);
    return rp1;
  }

  protected PartialCalculationResult getExampleCalculationPointOutputData(final Substance substance) {
    final PartialCalculationResult result = new PartialCalculationResult();

    final AeriusResultPoint cp1 = new AeriusResultPoint();
    cp1.setId(RECEPTOR_POINT_ID_1);
    cp1.setPointType(AeriusPointType.POINT);
    RECEPTOR_UTIL.setAeriusPointFromId(cp1);
    cp1.setId(1);
    cp1.setLabel("Test receptor 1");
    cp1.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.CONCENTRATION), RECEPTOR_1_CONCENTRATION * calculationResultScale);
    cp1.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION), RECEPTOR_1_DEPOSITION * calculationResultScale);
    cp1.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DRY_DEPOSITION),
        (RECEPTOR_1_DEPOSITION * calculationResultScale) / 2 + 1);
    cp1.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.WET_DEPOSITION),
        (RECEPTOR_1_DEPOSITION * calculationResultScale) / 2 - 1);
    result.add(cp1);

    final AeriusResultPoint cp2 = new AeriusResultPoint();
    cp2.setId(RECEPTOR_POINT_ID_2);
    cp2.setPointType(AeriusPointType.POINT);
    RECEPTOR_UTIL.setAeriusPointFromId(cp2);
    cp2.setId(2);
    cp2.setLabel("Test receptor 2");
    cp2.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.CONCENTRATION), RECEPTOR_2_CONCENTRATION * calculationResultScale);
    cp2.setEmissionResult(EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION), RECEPTOR_2_DEPOSITION * calculationResultScale);
    result.add(cp2);

    return result;
  }

  private void refreshCalculationSet() throws SQLException {
    removeCalculationResults();
    createCalculation(null);
  }

  protected void createCalculation(final Integer calculationPointSetId) throws SQLException {
    if (calculation == null) {
      calculation = new Calculation();
      calculation.setYear(TestDomain.YEAR);
      calculation = CalculationRepository.insertCalculation(getConnection(), calculation, calculationPointSetId);
    }
  }

  protected void removeCalculationResults() throws SQLException {
    if (calculation != null) {
      CalculationRepository.removeCalculation(getConnection(), calculation.getCalculationId());
      calculation = null;
    }
  }

  protected void newCalculation() throws SQLException {
    calculation = null;
    createCalculation(null);
  }

  protected abstract Connection getConnection() throws SQLException;
}
