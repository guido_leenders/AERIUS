/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.postgis.LineString;
import org.postgis.Polygon;
import org.postgresql.util.PSQLException;

import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Needs a proper database to test against, testing DB operations for conversions.
 */
public class ConversionRepositoryTest extends BaseDBTest {

  @Test
  public void testConvertToPoints() throws SQLException {
    final int numberOfCoordinates = 5;
    final ArrayList<org.postgis.Point> linePoints = new ArrayList<>();
    //create linestring with (0,0), (0,4000), (0,8000), (0,12000), (0,16000)
    for (int i = 0; i < numberOfCoordinates; i++) {
      final org.postgis.Point linePoint = new org.postgis.Point(0 * i, 4000 * i);
      linePoints.add(linePoint);
    }
    final LineString lineString = new LineString(linePoints.toArray(new org.postgis.Point[linePoints.size()]));
    List<Point> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      final Double maxSegementSize = 76.4;
      convertedPoints = ConversionRepository.convertToPoints(connection, lineString, maxSegementSize);
      assertEquals("Number of segments",
          new BigDecimal((numberOfCoordinates - 1) * 4000).divide(new BigDecimal(maxSegementSize), 0,
              RoundingMode.UP).intValue(),
          convertedPoints.size());
    }
    final double segmentSize = new BigDecimal((numberOfCoordinates - 1) * 4000).divide(new BigDecimal(convertedPoints.size()), 4,
        RoundingMode.HALF_UP).doubleValue();
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      assertEquals("X-coord of point " + i, 0, convertedPoint.getX(), 1E-2);
      assertEquals("Y-coord of point " + i, i * segmentSize + segmentSize / 2.0, convertedPoint.getY(), 1E-2);
    }
  }

  @Test
  public void testValidateConvertToPoints() throws SQLException, AeriusException {
    //check if the DB version of line -> points gives us the same results as the java version.
    //Try some random linestring.
    final String line = "LINESTRING(231 12311,1155 382424,122342 2349)";
    final LineString lineString = new LineString(line);
    List<Point> convertedPointsDB = new ArrayList<>();
    final Double maxSegmentSize = 76.4;
    try (final Connection connection = getCalcConnection()) {
      convertedPointsDB = ConversionRepository.convertToPoints(connection, lineString, maxSegmentSize);
    }
    final List<Point> convertedPoints = GeometryUtil.convertToPoints(new WKTGeometry(line), maxSegmentSize);
    assertEquals("Lists should have same size.", convertedPoints.size(), convertedPointsDB.size());
    for (int i = 0; i < convertedPoints.size(); i++) {
      final Point convertedPoint = convertedPoints.get(i);
      final Point convertedPointDB = convertedPointsDB.get(i);
      assertEquals("X-coord of point " + i, convertedPoint.getX(), convertedPointDB.getX(), 1E-5);
      assertEquals("Y-coord of point " + i, convertedPoint.getY(), convertedPointDB.getY(), 1E-5);
    }
  }

  @Test
  public void testConvertPolygonToPoints() throws SQLException {
    //create polygon with points (0,0), (0,4000), (4000,4000), (4000,0), (0,0)
    final int side = 4000;
    final String polygonWKT = "POLYGON((0 0,0 " + side + "," + side + " " + side + "," + side + " 0,0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    double convertPolygonToPointsGridSize;
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
      convertPolygonToPointsGridSize = ConstantRepository.getNumber(connection, ConstantsEnum.CONVERT_POLYGON_TO_POINTS_GRID_SIZE, Double.class);
    }
    //known case for squares where each side is divisable by the max square side size.
    //function starts with creating squares in the middle, radiating outwards.
    //each point inside the surface (in this case the square) will be returned.
    //points on the border will have a weight factor of 0.5 and those exactly on the corners 0.25.
    assertEquals("Number of segments",
        Math.pow(side / convertPolygonToPointsGridSize + 1, 2),
        convertedPoints.size(), 0.5);
    // The coordinates of the point of the b
    final double offset = convertPolygonToPointsGridSize / 4;
    for (final WeightedPoint point : convertedPoints) {
      assertTrue("Point x coord inside square", point.getX() >= 0 && point.getX() <= side);
      assertTrue("Point y coord inside square", point.getY() >= 0 && point.getY() <= side);
      //points on border get weight factor 0.5, others get 1 (are fully inside).
      if ((point.getX() == offset || point.getX() == (side - offset))
          && (point.getY() == offset || point.getY() == (side - offset))) {
        assertEquals("Weight factor points on the corners", 0.25, point.getFactor(), 0.001);
      } else if (point.getX() == offset || point.getX() == (side - offset)
          || point.getY() == offset || point.getY() == (side - offset)) {
        assertEquals("Weight factor points on the border", 0.5, point.getFactor(), 0.001);
      } else {
        assertEquals("Weight factor points within the polygon", 1, point.getFactor(), 0.001);
      }
    }
  }

  @Test
  public void testConvertPolygonToPointsSmall() throws SQLException {
    //create small polygon with points (0,0), (0,20), (20,20), (20,0), (0,0)
    final int side = 20;
    final String polygonWKT = "POLYGON((0 0,0 " + side + "," + side + " " + side + "," + side + " 0,0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
    }
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertEquals("Number of segments", 1, convertedPoints.size());
    final WeightedPoint point = convertedPoints.get(0);
    assertTrue("Weight factor", point.getFactor() > 0.0);
    assertTrue("Point x coord inside square", point.getX() >= 0 && point.getX() <= side);
    assertTrue("Point y coord inside square", point.getY() >= 0 && point.getY() <= side);
    assertEquals("Point x coord center of square", side / 2.0, point.getX(), 0.001);
    assertEquals("Point Y coord center of square", side / 2.0, point.getY(), 0.001);
  }

  @Test
  public void testConvertPolygonToPointsSmallWidth() throws SQLException {
    //create diagonal small width long polygon with points (0,0), (0,20), (4000,4020), (4000,4000), (0,0)
    final int side = 20;
    final String polygonWKT = "POLYGON((0 0,0 " + side + ",4000 " + (4000 + side) + ",4000 4000,0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
    }
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertTrue("Number of segments should be more than 1 (unsure how many exactly)", convertedPoints.size() > 1);
    //can we tell anything about where the points should generally be?
  }

  /**
   * Tests self-intersecting polygons; They are not supported (yet) thus it should throw an exception.
   * @throws SQLException exception on self intersect.
   */
  @Test(expected = PSQLException.class)
  public void testPolygotnToPointsBowTie() throws SQLException {
    //create Bow-Tie polygon with points (0,0), (4000,4000), (4000,0), (0,4000), (0,0)
    final int side = 4000;
    final String polygonWKT = "POLYGON((0 0," + side + " " + side + "," + side + " 0,0 " + side + ",0 0))";
    final Polygon polygon = new Polygon(polygonWKT);
    List<WeightedPoint> convertedPoints = new ArrayList<>();
    try (final Connection connection = getCalcConnection()) {
      polygon.setSrid(ReceptorGridSettingsRepository.getSrid(connection));
      convertedPoints = ConversionRepository.convertToPoints(connection, polygon);
    }
    //if surface for the polygon is below the max surface, it should just return one point with full weight
    //it should be centered on polygon as well.
    assertTrue("Number of segments should be more than 1 (unsure how many exactly)", convertedPoints.size() > 1);
  }
}
