/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;

import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Needs a proper database to test against, testing DB operations for sectors.
 */
public class SectorRepositoryTest extends BaseDBTest {

  /**
   * Test method for {@link nl.overheid.aerius.db.common.sector.SectorRepository#getSectors(java.sql.Connection)}.
   * @throws SQLException
   */
  @Test
  public void testGetSectors() throws SQLException {
    final ArrayList<Sector> sectors = SectorRepository.getSectors(getCalcConnection(), getCalcMessagesKey());
    assertNotNull("Retrieved sectors should not be null", sectors);
    final HashSet<Integer> sectorIds = new HashSet<>();
    for (final Sector sector : sectors) {
      if (sectorIds.contains(sector.getSectorId())) {
        fail("Found duplicate sector id: " + sector.getSectorId());
      } else {
        sectorIds.add(sector.getSectorId());
      }
    }
  }

  @Test
  public void testGetOPSSourceCharacteristics() throws SQLException {
    final OPSSourceCharacteristics characteristics =
        SectorRepository.getOPSSourceCharacteristics(getCalcConnection(),
            SectorRepository.getSectors(getCalcConnection(), getCalcMessagesKey()).get(0));
    assertNotNull("Retrieved characteristics should not be null", characteristics);
  }

  @Test
  public void testGetOPSSourceCharacteristicsSpecificNH3() throws SQLException {
    final Sector sector = new Sector();
    // Sector: 4130
    sector.setSectorId(4130);
    final OPSSourceCharacteristics characteristics = SectorRepository.getOPSSourceCharacteristics(getCalcConnection(), sector);
    assertNotNull("Retrieved characteristics should not be null", characteristics);
    assertEquals("Emission height", 0.5, characteristics.getEmissionHeight(), 1E-3);
    assertEquals("Spread", 0.3, characteristics.getSpread(), 1E-3);
    assertEquals("Heatcontent", 0, characteristics.getHeatContent(), 1E-3);
    assertEquals("Diameter from DB always 0", 0, characteristics.getDiameter());
    assertEquals("Diurnal variation", 5, characteristics.getDiurnalVariationSpecification().getId());
    assertEquals("PSD", 4130, characteristics.getParticleSizeDistribution());
  }

  @Test
  public void testGetOPSSourceCharacteristicsSpecific() throws SQLException {
    final Sector sector = new Sector();
    //Sector: 1100;"Voedings- en genotmiddelen "
    sector.setSectorId(1100);
    final OPSSourceCharacteristics characteristics = SectorRepository.getOPSSourceCharacteristics(getCalcConnection(), sector);
    assertNotNull("Retrieved characteristics should not be null", characteristics);
    assertEquals("Emission height", 15.0, characteristics.getEmissionHeight(), 1E-3);
    assertEquals("Spread", 7.5, characteristics.getSpread(), 1E-3);
    assertEquals("Heatcontent", 0.34, characteristics.getHeatContent(), 1E-3);
    assertEquals("Diameter from DB always 0", 0, characteristics.getDiameter());
    assertEquals("Diurnal variation", 1, characteristics.getDiurnalVariationSpecification().getId());
    assertEquals("PSD", 1100, characteristics.getParticleSizeDistribution());
  }
}
