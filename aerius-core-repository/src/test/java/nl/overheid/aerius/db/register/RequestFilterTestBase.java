/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.SortableAttribute;
import nl.overheid.aerius.shared.domain.SortableDirection;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Province;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFilter;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public abstract class RequestFilterTestBase<T extends RequestFilter<S>, K extends Request, S extends SortableAttribute> extends RequestTestBase {

  private final static int ENGBERT_AREA_ID = 40;
  private final static int ENGBERT_RECEPTOR_ID = 5747266;

  private List<K> allRequests;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    init();
    allRequests = getList(getNewFilter());
  }

  abstract void init() throws SQLException, AeriusException;

  abstract List<K> getList(T filter) throws SQLException;

  abstract T getNewFilter();

  @Test
  public void testDefaultFilter() throws SQLException {
    final T filter = getNewFilter();
    //empty filter should return all requests of this type
    final List<K> requests = getList(filter);
    assertFalse("Empty filter: All requests shouldn't be empty", requests.isEmpty());
  }

  abstract Date getDateForRequest(K request);

  abstract Date getTestDate();

  @Test
  public void testFilterFrom() throws SQLException {
    final T filter = getNewFilter();
    final Date testDate = getTestDate();
    filter.setFrom(getTestDate());
    final List<K> requests = getList(filter);
    defaultValidations("From", requests);
    for (final K request : requests) {
      assertTrue("Every date should be after the test date", getDateForRequest(request).after(testDate));
    }
  }

  @Test
  public void testFilterTill() throws SQLException {
    final T filter = getNewFilter();
    final Date testDate = getTestDate();
    filter.setTill(testDate);
    final List<K> requests = getList(filter);
    defaultValidations("Till", requests);
    for (final K request : requests) {
      assertTrue("Every date should be before the test date", getDateForRequest(request).before(getDateInclusive(testDate)));
    }
  }

  @Test
  public void testFilterFromAndTill() throws SQLException {
    final T filter = getNewFilter();
    final Date testDate = getTestDate();
    filter.setFrom(testDate);
    final Date testTillDate = getTillDate();
    filter.setTill(testTillDate);
    final List<K> requests = getList(filter);
    defaultValidations("From and Till", requests);
    for (final K request : requests) {
      assertTrue("Every date should be after the test date", getDateForRequest(request).after(testDate));
      assertTrue("Every date should be before the other date", getDateForRequest(request).before(getDateInclusive(testTillDate)));
    }
  }

  private Date getTillDate() {
    final Calendar calendar = Calendar.getInstance();
    calendar.setTime(getTestDate());
    calendar.add(Calendar.DATE, 10);
    return calendar.getTime();
  }

  private Date getDateInclusive(final Date dateExclusive) {
    final Calendar calendar = Calendar.getInstance();
    calendar.setTime(dateExclusive);
    calendar.add(Calendar.DATE, 1);
    return calendar.getTime();
  }

  abstract Authority getTestAuthority();

  abstract Authority getAuthorityForRequest(K request);

  @Test
  public void testFilterAuthority() throws SQLException {
    final T filter = getNewFilter();
    //proper authority
    final Authority authority = getTestAuthority();
    filter.getAuthorities().add(authority);
    List<K> requests = getList(filter);
    defaultValidations("Authority", requests);

    for (final K request : requests) {
      assertEquals("Every request should have the same authority",
          authority.getId(),
          getAuthorityForRequest(request).getId());
    }

    //invalid authority
    filter.getAuthorities().clear();
    final Authority bogusAuthority = new Authority();
    bogusAuthority.setId(90909090);
    filter.getAuthorities().add(bogusAuthority);
    requests = getList(filter);
    assertTrue("Invalid authority: requests should now be empty", requests.isEmpty());
  }

  abstract Province getTestProvince();

  @Test
  public void testFilterProvince() throws SQLException {
    final T filter = getNewFilter();
    //proper province
    final Province province = getTestProvince();
    filter.getProvinces().add(province);
    List<K> requests = getList(filter);
    defaultValidations("Province", requests);

    //invalid province
    filter.getProvinces().clear();
    final Province bogusProvince = new Province();
    bogusProvince.setProvinceId(88088088);
    filter.getProvinces().add(bogusProvince);
    requests = getList(filter);
    assertTrue("Invalid province: requests should now be empty", requests.isEmpty());
  }

  abstract Sector getTestSector();

  @Test
  public void testFilterSector() throws SQLException {
    final T filter = getNewFilter();
    //proper sector
    final Sector sector = getTestSector();
    filter.getSectors().add(sector);
    List<K> requests = getList(filter);
    defaultValidations("Sector", requests);
    for (final K request : requests) {
      assertEquals("Every request should have the same sector",
          sector.getSectorId(),
          request.getSector().getSectorId());
    }

    //invalid sector
    filter.getSectors().clear();
    final Sector bogusSector = new Sector();
    bogusSector.setSectorId(7970970);
    filter.getSectors().add(bogusSector);
    requests = getList(filter);
    assertTrue("Invalid sector: requests should now be empty", requests.isEmpty());
  }

  private AssessmentArea getTestAssessmentArea(final int id) {
    final AssessmentArea testArea = new AssessmentArea();
    testArea.setId(id);
    return testArea;
  }

  @Test
  public void testFilterAssessmentArea() throws SQLException {
    assertFilterAssessmentArea(true);
  }

  protected void assertFilterAssessmentArea(final boolean shouldHaveLessAreas) throws SQLException {
    final T filter = getNewFilter();
    ensureResultForTest();
    //proper area
    final AssessmentArea area = getTestAssessmentArea(ENGBERT_AREA_ID);
    filter.getAssessmentAreas().add(area);
    List<K> requests = getList(filter);
    if (shouldHaveLessAreas) {
      defaultValidations("Assessment area", requests);
    } else {
      assertFalse("Assessment areas: requests shouldn't be empty", requests.isEmpty());
    }
    //can't do any other validations really

    //multiple areas
    final AssessmentArea area2 = getTestAssessmentArea(TestDomain.BINNENVELD_ID);
    filter.getAssessmentAreas().add(area2);
    requests = getList(filter);
    assertFalse("Multiple areas: requests shouldn't be empty", requests.isEmpty());
    final Set<Integer> ids = new HashSet<Integer>();
    for (final K request : requests) {
      ids.add(request.getId());
    }
    assertEquals("Multiple areas: nr of Unique requests", ids.size(), requests.size());

    //invalid area
    filter.getAssessmentAreas().clear();
    final AssessmentArea bogusArea = new AssessmentArea();
    bogusArea.setId(646468);
    filter.getAssessmentAreas().add(bogusArea);
    requests = getList(filter);
    assertTrue("Invalid area: requests should now be empty", requests.isEmpty());
  }

  @Test
  public void testFullFilter() throws SQLException {
    final T filter = getNewFilter();
    ensureResultForTest();
    filter.setFrom(getTestDate());
    filter.setTill(getTillDate());
    filter.getProvinces().add(getTestProvince());
    filter.getAuthorities().add(getTestAuthority());
    filter.getSectors().add(getTestSector());
    filter.getAssessmentAreas().add(getTestAssessmentArea(ENGBERT_AREA_ID));
    fillForFullFilter(filter);
    final List<K> requests = getList(filter);
    defaultValidations("Full filter", requests);
  }

  @Test
  public void testSortingFilter() throws SQLException {
    final T filter = getNewFilter();
    for (final S sortAttribute : getPossibleSortAttributes()) {
      filter.setSortAttribute(sortAttribute);
      filter.setSortDirection(SortableDirection.ASC);
      final List<K> requests = getList(filter);
      assertFalse("Sorting by " + sortAttribute + ", direction asc", requests.isEmpty());

      filter.setSortDirection(SortableDirection.DESC);
      final List<K> requestsDesc = getList(filter);
      assertFalse("Sorting by " + sortAttribute + ", direction desc", requestsDesc.isEmpty());
      assertEquals("Size of lists with opposite sorting", requests.size(), requestsDesc.size());
      //bit hard to test (in a generic way at least) the actual sort: if 2 have the same value for the sorted attribute, ordering depends on the type...
      //least we can test is that the order has changed. That way we at least verify that sorting has been done.
      assertNotEquals("First/last", requests.get(0), requestsDesc.get(0));
    }
  }

  abstract S[] getPossibleSortAttributes();

  void defaultValidations(final String description, final List<K> requests) {
    assertFalse(description + ": requests shouldn't be empty", requests.isEmpty());
    assertTrue(description + ": Should be less requests now", requests.size() < allRequests.size());
  }

  private void ensureResultForTest() throws SQLException {
    final int newerRequestCalculation = getNewerRequestCalculationId();
    if (newerRequestCalculation != 0) {
      final List<AeriusResultPoint> extraResults = new ArrayList<>();
      final AeriusResultPoint extraResult = new AeriusResultPoint(ENGBERT_RECEPTOR_ID);
      extraResult.getEmissionResults().put(EmissionResultKey.NH3_DEPOSITION, 0.07);
      extraResults.add(extraResult);
      CalculationRepository.insertCalculationResults(getRegConnection(), newerRequestCalculation, extraResults);
    }
  }

  abstract int getNewerRequestCalculationId() throws SQLException;

  abstract void fillForFullFilter(T filter);

}
