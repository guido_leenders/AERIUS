/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;

/**
 * Test class for {@link ReceptorGridSettingsRepository}.
 */
public class ReceptorGridSettingsRepositoryTest extends BaseDBTest {

  @Test
  public void testSrid() throws SQLException {
    assertEquals("SRID not correctly set", RDNew.SRID, ReceptorGridSettingsRepository.getSrid(getCalcPMF()));
  }

  @Test
  public void testGridBBox() throws SQLException {
    final BBox bbox = ReceptorGridSettingsRepository.getCalculatorGridBBox(getCalcConnection());
    assertTrue("MaxX not correctly set:" + bbox.getMaxX(), bbox.getMaxX() > 0);
    assertTrue("MaxY not correctly set:" + bbox.getMaxY(), bbox.getMaxY() > 0);
  }

  @Test
  public void testZoomLevels() throws SQLException {
    final List<HexagonZoomLevel> zoomLevels = ReceptorGridSettingsRepository.getZoomLevels(getCalcConnection());
    assertEquals("Number of zoomlevels not as expected", 5, zoomLevels.size());
    assertEquals("Surface level1 not as expected", 10000, zoomLevels.get(0).getHexagonSurface(), 0.1E-5);
  }
}
