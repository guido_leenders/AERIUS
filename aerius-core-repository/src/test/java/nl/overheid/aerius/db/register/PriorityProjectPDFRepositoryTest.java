/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportHabitatInfo;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

/**
 *
 */
public class PriorityProjectPDFRepositoryTest extends RequestTestBase {

  private static final PriorityProjectKey DEFAULT_PROJECT_KEY = new PriorityProjectKey(null, null);
  private static final String TEST_REFERENCE = "pdp_Overijssel_10_4600";
  private static final PrioritySubProjectKey TEST_SUBPROJECTKEY = new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_REFERENCE);

  private static final String TEST_PP_REFERENCE = "pp_Gelderland_5_3210";

  private static final String TEST_INSERT_REFERENCE = ReferenceUtil.generatePermitReference();

  private PrioritySubProject getTestPrioritySubProject() throws SQLException {
    return PrioritySubProjectRepository.getPrioritySubProjectByKey(getRegConnection(), TEST_SUBPROJECTKEY);
  }

  private PriorityProject getSkinnedTestPriorityProject() throws SQLException {
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_PP_REFERENCE);
    return PriorityProjectRepository.getSkinnedPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
  }

  @Test
  public void testDetermineExportInfo() throws SQLException, AeriusException {
    final PriorityProjectKey priorityProjectKey = getSkinnedTestPriorityProject().getKey();
    final PrioritySubProject subProject = getTestPrioritySubProject();
    subProject.getScenarioMetaData().setReference(TEST_INSERT_REFERENCE);
    subProject.getChangeHistory().clear();
    final PrioritySubProject newSubProject = PrioritySubProjectModifyRepository.insert(getRegConnection(), priorityProjectKey, subProject,
        getExampleUserProfile(), getFakeInsertRequestFile());
    assertRetrievedPrioritySubProject(newSubProject);
    addCalculation(newSubProject);

    final List<PDFExportAssessmentAreaInfo> exportInfo = PriorityProjectPDFRepository.determineExportInfo(getRegPMF(), new PrioritySubProjectKey(priorityProjectKey, TEST_INSERT_REFERENCE), true);

    assertNotNull("Returned list", exportInfo);
    assertEquals("Returned area size", 2, exportInfo.size());
    for (final PDFExportAssessmentAreaInfo areaInfo : exportInfo) {
      assertNotNull("Returned area name", areaInfo.getName());
      assertFalse("Returned area should have rule results", areaInfo.getShowRules().isEmpty());
      assertFalse("Returned area should have habitats", areaInfo.getHabitatInfos().isEmpty());
      for (final PDFExportHabitatInfo habitatInfo : areaInfo.getHabitatInfos()) {
        assertNotNull("Returned habitat name", habitatInfo.getName());
        assertFalse("Returned habitat should have rule results", habitatInfo.getShowRules().isEmpty());
      }
    }
  }

}
