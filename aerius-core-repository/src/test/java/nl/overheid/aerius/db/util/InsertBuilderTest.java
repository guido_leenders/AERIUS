/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 *
 */
public class InsertBuilderTest {

  private static final String TEST_TABLE = "SomeTable";

  private enum TestAttribute implements Attribute {
    TEST_COLUMN_1, TEST_COLUMN_2, TEST_COLUMN_3;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  @Test
  public void testInto() {
    final InsertBuilder builder = InsertBuilder.into(TEST_TABLE);
    final Query query = builder.getQuery();
    assertEquals("Query without attributes", "INSERT INTO SomeTable() VALUES ()", query.get());
  }

  @Test
  public void testInsertAttribute() {
    final InsertBuilder builder = InsertBuilder.into(TEST_TABLE);
    builder.insert(TestAttribute.TEST_COLUMN_1);
    Query query = builder.getQuery();
    assertEquals("Query with 1 attribute", "INSERT INTO SomeTable(test_column_1) VALUES (?)", query.get());
    assertEquals("Query with 1 attribute index known attribute", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Query with 1 attribute index unknown attribute", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Query with 1 attribute index unknown attribute 2", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));

    builder.insert(TestAttribute.TEST_COLUMN_3, TestAttribute.TEST_COLUMN_2);
    query = builder.getQuery();
    assertEquals("Query with 3 attributes", "INSERT INTO SomeTable(test_column_1, test_column_3, test_column_2) VALUES (?, ?, ?)", query.get());
    assertEquals("Query with 3 attributes index known attribute 1", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Query with 3 attributes index known attribute 2", 3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Query with 3 attributes index known attribute 3", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }

  @Test
  public void testInsertWithClause() {
    final InsertBuilder builder = InsertBuilder.into(TEST_TABLE);
    builder.insert(new InsertClause("someColumn", TestAttribute.TEST_COLUMN_1));
    Query query = builder.getQuery();
    assertEquals("Query with 1 insertclause", "INSERT INTO SomeTable(someColumn) VALUES (?)", query.get());
    assertEquals("Query with 1 insertclause index known attribute", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Query with 1 insertclause index unknown attribute", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Query with 1 insertclause index unknown attribute 2", 0, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));

    builder.insert(new InsertClause("WeirdColumn", "WeirdValue", TestAttribute.TEST_COLUMN_3),
        new InsertClause("AndSomething", TestAttribute.TEST_COLUMN_2));
    query = builder.getQuery();
    assertEquals("Query with 3 insertclauses", "INSERT INTO SomeTable(someColumn, WeirdColumn, AndSomething) VALUES (?, WeirdValue, ?)", query.get());
    assertEquals("Query with 3 insertclauses index known attribute 1", 1, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_1));
    assertEquals("Query with 3 insertclauses index known attribute 2", 3, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_2));
    assertEquals("Query with 3 insertclauses index known attribute 3", 2, query.getParameterIndexOf(TestAttribute.TEST_COLUMN_3));
  }


}
