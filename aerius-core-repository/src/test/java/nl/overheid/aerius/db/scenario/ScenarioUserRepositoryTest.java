/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.scenario;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ScenarioUserRepository}.
 */
public class ScenarioUserRepositoryTest extends BaseDBTest {

  private static final String TEST_API_KEY1 = "wnaR9FavGRGv8RXCmdfXKEqeIt1DTZUS";
  private static final String TEST_API_KEY2 = "SPWQ9l9IelzKFfJQnvyQLpEYlrQDrNH0";
  private static final String TEST_EMAIL1 = "test@example.com";
  private static final String TEST_EMAIL2 = "abc@example.com";

  @Test
  public void testCreateAndGetUser() throws SQLException, AeriusException {
    final ScenarioUser userIn1 = new ScenarioUser();
    userIn1.setApiKey(TEST_API_KEY1);
    userIn1.setEmailAddress(TEST_EMAIL1);
    assertEquals("User id must not be set", 0, userIn1.getId());
    ScenarioUserRepository.createUser(getCalcConnection(), userIn1);
    assertNotEquals("User id must be set", 0, userIn1.getId());

    final ScenarioUser userOut1 = ScenarioUserRepository.getUserByApiKey(getCalcConnection(), TEST_API_KEY1);
    assertNotNull("User must be found by API-key", userOut1);
    assertEquals("User id must match", userIn1.getId(), userOut1.getId());
    assertEquals("API-key must match", TEST_API_KEY1, userOut1.getApiKey());

    final ScenarioUser userOut2 = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL1);
    assertNotNull("User must be found by e-mail", userOut2);
    assertEquals("User id must match", userIn1.getId(), userOut2.getId());
    assertEquals("E-mail must match", TEST_EMAIL1, userOut2.getEmailAddress());

    assertEquals("Users must match", userOut1, userOut2);
    assertEquals("User id's must match", userOut1.getId(), userOut2.getId());
    assertEquals("User Enabled must match", userOut1.isEnabled(), userOut2.isEnabled());
    assertEquals("User MaxConcurrentCalculations must match", userOut1.getMaxConcurrentJobs(), userOut2.getMaxConcurrentJobs());

    // Secondary user with different properties
    final ScenarioUser userIn2 = new ScenarioUser();
    userIn2.setApiKey(TEST_API_KEY2);
    userIn2.setEmailAddress(TEST_EMAIL2);
    userIn2.setEnabled(false);
    final int maxConcurrent = ConstantRepository.getInteger(getCalcConnection(), ConstantsEnum.CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER);
    userIn2.setMaxConcurrentJobs(maxConcurrent + 1);
    ScenarioUserRepository.createUser(getCalcConnection(), userIn2);
    final ScenarioUser userOut3 = ScenarioUserRepository.getUserByApiKey(getCalcConnection(), TEST_API_KEY2);

    assertNotEquals("Users must not match", userOut1, userOut3);
    assertNotEquals("User id's must not match", userOut1.getId(), userOut3.getId());
    assertFalse("User Enabled must be false", userOut3.isEnabled());
    assertEquals("User MaxConcurrentCalculations should be the db value", maxConcurrent, userOut3.getMaxConcurrentJobs());
  }

  @Test
  public void testCreateAndGetUserCaseInsentive() throws SQLException, AeriusException {
    final ScenarioUser userIn1 = new ScenarioUser();
    userIn1.setApiKey(TEST_API_KEY1);
    userIn1.setEmailAddress(TEST_EMAIL1.toUpperCase());
    assertEquals("User id must not be set", 0, userIn1.getId());
    ScenarioUserRepository.createUser(getCalcConnection(), userIn1);
    assertNotEquals("User id must be set", 0, userIn1.getId());

    final ScenarioUser userOut1 = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL1.toLowerCase());
    assertNotNull("User must be found by lowercase email", userOut1);
    assertEquals("User id must match", userIn1.getId(), userOut1.getId());

    final ScenarioUser userIn2 = new ScenarioUser();
    userIn2.setApiKey(TEST_API_KEY2);
    userIn2.setEmailAddress(TEST_EMAIL2.toLowerCase());
    ScenarioUserRepository.createUser(getCalcConnection(), userIn2);

    final ScenarioUser userOut2 = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL2.toUpperCase());
    assertNotNull("User must be found by uppercase email", userOut2);
    assertEquals("User id must match", userIn2.getId(), userOut2.getId());
  }

  @Test(expected=AeriusException.class)
  public void testCreateExistingApiUser() throws SQLException, AeriusException {
    final ScenarioUser userIn = new ScenarioUser();
    userIn.setApiKey(TEST_API_KEY1);
    userIn.setEmailAddress(TEST_EMAIL1);
    ScenarioUserRepository.createUser(getCalcConnection(), userIn);
    userIn.setEmailAddress(TEST_EMAIL2);
    ScenarioUserRepository.createUser(getCalcConnection(), userIn);
  }

  @Test(expected=AeriusException.class)
  public void testCreateExistingEmailUser() throws SQLException, AeriusException {
    final ScenarioUser userIn = new ScenarioUser();
    userIn.setApiKey(TEST_API_KEY1);
    userIn.setEmailAddress(TEST_EMAIL1);
    ScenarioUserRepository.createUser(getCalcConnection(), userIn);
    userIn.setApiKey(TEST_API_KEY2);
    ScenarioUserRepository.createUser(getCalcConnection(), userIn);
  }

}
