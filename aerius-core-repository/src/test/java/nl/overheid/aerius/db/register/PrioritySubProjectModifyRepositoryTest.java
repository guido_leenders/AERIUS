/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;

public class PrioritySubProjectModifyRepositoryTest extends RequestTestBase {

  private static final PriorityProjectKey DEFAULT_PROJECT_KEY = new PriorityProjectKey(null, null);
  private static final String TEST_EXISTING_REFERENCE = "pdp_Overijssel_10_4600";
  private static final PrioritySubProjectKey TEST_EXISTING_KEY = new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_EXISTING_REFERENCE);

  private static final String TEST_PP_REFERENCE = "pp_Gelderland_5_3210";

  private static final String TEST_INSERT_REFERENCE = ReferenceUtil.generatePermitReference();
  private static final PrioritySubProjectKey TEST_INSERTED_KEY = new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_INSERT_REFERENCE);

  private PrioritySubProject getSkinnedTestPrioritySubProject(final PrioritySubProjectKey key) throws SQLException {
    return PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(getRegConnection(), key);
  }

  private PrioritySubProject getTestPrioritySubProject() throws SQLException {
    return PrioritySubProjectRepository.getPrioritySubProjectByKey(getRegConnection(), TEST_EXISTING_KEY);
  }

  private PriorityProject getSkinnedTestPriorityProject() throws SQLException {
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_PP_REFERENCE);
    return PriorityProjectRepository.getSkinnedPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
  }

  private PrioritySubProject getInsertedPrioritySubProject() throws SQLException, AeriusException {
    final PriorityProjectKey priorityProjectKey = getSkinnedTestPriorityProject().getKey();
    final PrioritySubProject subProject = getTestPrioritySubProject();
    subProject.getScenarioMetaData().setReference(TEST_INSERT_REFERENCE);
    final PrioritySubProject newSubProject = PrioritySubProjectModifyRepository.insert(getRegConnection(), priorityProjectKey, subProject,
        getExampleUserProfile(), getFakeInsertRequestFile());
    addCalculation(newSubProject);
    return newSubProject;
  }

  @Test
  public void testInsert() throws SQLException, AeriusException {
    final PrioritySubProject newSubProject = getInsertedPrioritySubProject();
    assertRetrievedPrioritySubProject(newSubProject);
    assertEquals("Reference", TEST_INSERT_REFERENCE, newSubProject.getReference());
    assertEquals("Request state", RequestState.INITIAL, newSubProject.getRequestState());
    assertTrue("ID", newSubProject.getId() > 0);
    assertFalse("Situations", newSubProject.getSituations().isEmpty());
    assertNotNull("Situation proposed", newSubProject.getSituations().get(SituationType.PROPOSED));
    assertNotNull("Situation proposed emission values", newSubProject.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues());
    // Test audit trail bij getting trail of priority project.
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), TEST_PP_REFERENCE);
    final PriorityProject testPriorityProject = PriorityProjectRepository.getPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    assertFalse("Audit ", testPriorityProject.getChangeHistory().isEmpty());
    assertEquals("Number of changes so far", 1, testPriorityProject.getChangeHistory().size());
    assertEquals("Items in change", 3, testPriorityProject.getChangeHistory().get(0).getItems().size());
  }

  @Test(expected = AeriusException.class)
  public void testInsertForNonExistingPriorityProject() throws SQLException, AeriusException {
    final PriorityProjectKey priorityProjectKey = new PriorityProjectKey("bla", "bla");
    final PrioritySubProject subProject = getTestPrioritySubProject();
    subProject.getScenarioMetaData().setReference(TEST_INSERT_REFERENCE);
    try {
      PrioritySubProjectModifyRepository.insert(getRegConnection(), priorityProjectKey, subProject, getExampleUserProfile(),
          getFakeInsertRequestFile());
    } catch (final AeriusException e) {
      assertEquals("Exception reason", Reason.PRIORITY_PROJECT_UNKNOWN, e.getReason());
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testInsertExisting() throws SQLException, AeriusException {
    final PriorityProjectKey priorityProjectKey = getSkinnedTestPriorityProject().getKey();
    final PrioritySubProject subProject = getTestPrioritySubProject();
    try {
      PrioritySubProjectModifyRepository.insert(getRegConnection(), priorityProjectKey, subProject, getExampleUserProfile(),
          getFakeInsertRequestFile());
    } catch (final AeriusException e) {
      assertEquals("Exception reason", Reason.REQUEST_ALREADY_EXISTS, e.getReason());
      throw e;
    }
  }

  @Test
  public void testUpdateState() throws SQLException, AeriusException {
    PrioritySubProject subProject = getInsertedPrioritySubProject();
    assertEquals("Priority subproject state in DB", RequestState.INITIAL, subProject.getRequestState());
    PrioritySubProjectModifyRepository.updateState(getRegConnection(), TEST_INSERTED_KEY, RequestState.QUEUED,
        getExampleUserProfile(), subProject.getLastModified());

    PrioritySubProjectModifyRepository.updateState(getRegConnection(), TEST_INSERTED_KEY, RequestState.REJECTED_WITHOUT_SPACE,
        getExampleUserProfile(), subProject.getLastModified());
    subProject = getSkinnedTestPrioritySubProject(TEST_INSERTED_KEY);
    assertEquals("Priority subproject state in DB", RequestState.REJECTED_WITHOUT_SPACE, subProject.getRequestState());

    PrioritySubProjectModifyRepository.updateState(getRegConnection(), TEST_INSERTED_KEY, RequestState.ASSIGNED, getExampleUserProfile(),
        subProject.getLastModified());
    subProject = getSkinnedTestPrioritySubProject(TEST_INSERTED_KEY);
    assertEquals("Priority subproject state in DB", RequestState.ASSIGNED, subProject.getRequestState());
  }

  @Test
  public void testUpdateStateWithEdits() throws SQLException, AeriusException {
    // TODO! once subprojects can be updated too
  }

  @Test(expected = AeriusException.class)
  public void testUpdateStateFailure() throws SQLException, AeriusException {
    final PrioritySubProject subProject = getSkinnedTestPrioritySubProject(TEST_EXISTING_KEY);
    assertEquals("Priority subproject state in DB", RequestState.ASSIGNED, subProject.getRequestState());

    PrioritySubProjectModifyRepository.updateState(getRegConnection(), TEST_EXISTING_KEY, RequestState.INITIAL, getLowLevelExampleUserProfile(),
        subProject.getLastModified());
  }

  @Test(expected = AeriusException.class)
  public void testUpdateStateUnknownSubProject() throws SQLException, AeriusException, IOException {
    PrioritySubProjectModifyRepository.updateState(getRegConnection(), new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, "NotAReference"),
        RequestState.ASSIGNED, getExampleUserProfile(), new Date().getTime());
  }

  @Test
  public void testDelete() throws SQLException, AeriusException {
    getInsertedPrioritySubProject();
    PrioritySubProjectModifyRepository.delete(getRegConnection(), TEST_INSERTED_KEY, getExampleUserProfile());
    assertFalse("Subproject must no longer exist", PrioritySubProjectRepository.prioritySubProjectExists(getRegConnection(), TEST_INSERTED_KEY));
    assertNull("Subproject should not be retrievable", getSkinnedTestPrioritySubProject(TEST_INSERTED_KEY));
  }

  @Test
  public void testFindSuccess() throws SQLException, AeriusException {
    final PrioritySubProject subProject = PrioritySubProjectModifyRepository.find(getRegConnection(), TEST_EXISTING_KEY, "test find");
    assertRetrievedPrioritySubProject(subProject);
  }

  @Test(expected = AeriusException.class)
  public void testFindFailed() throws SQLException, AeriusException {
    try {
      PrioritySubProjectModifyRepository.find(getRegConnection(), new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, "bla"), "test find");
    } catch (final AeriusException e) {
      assertEquals("Exception reason", Reason.PRIORITY_SUBPROJECT_UNKNOWN, e.getReason());
      throw e;
    }
  }

  @Test
  public void testEnsureNewSuccess() throws SQLException, AeriusException {
    final PrioritySubProject subProject = getSkinnedTestPrioritySubProject(TEST_EXISTING_KEY);
    subProject.getScenarioMetaData().setReference(TEST_INSERT_REFERENCE);
    PrioritySubProjectModifyRepository.ensureNew(getRegConnection(), DEFAULT_PROJECT_KEY, subProject);
  }

  @Test(expected = AeriusException.class)
  public void testEnsureNewFailed() throws SQLException, AeriusException {
    try {
      PrioritySubProjectModifyRepository.ensureNew(getRegConnection(), DEFAULT_PROJECT_KEY, getSkinnedTestPrioritySubProject(TEST_EXISTING_KEY));
    } catch (final AeriusException e) {
      assertEquals("Exception reason", Reason.REQUEST_ALREADY_EXISTS, e.getReason());
      throw e;
    }
  }
}
