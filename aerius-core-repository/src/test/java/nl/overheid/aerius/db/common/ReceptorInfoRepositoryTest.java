/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

public class ReceptorInfoRepositoryTest extends BaseDBTest {

  private static final int RECEPTOR_POINT_ID = 4275970;
  private static final int NATURA2000_ID = 65;

  @Test
  public void testGetNaturaAreaInfo() throws SQLException {
    List<Natura2000Info> infos = ReceptorInfoRepository.getNaturaAreaInfo(getCalcConnection(), getReceptor(0), getCalcMessagesKey());
    assertNotNull("Result shouldn't be null", infos);
    assertEquals("Number of nature areas", 0, infos.size());
    infos = ReceptorInfoRepository.getNaturaAreaInfo(getCalcConnection(), getReceptor(RECEPTOR_POINT_ID), getCalcMessagesKey());
    assertNotNull("Result shouldn't be null", infos);
    assertEquals("Number of nature areas", 1, infos.size());
    final Natura2000Info info = infos.get(0);
    assertEquals("Assessment Area id", NATURA2000_ID, info.getId());
    assertEquals("Natura 2000 Area id", NATURA2000_ID, info.getId());
    assertEquals("Nature area name", "Binnenveld", info.getName());
    assertEquals("Number of habitat types in nature area", 3, info.getHabitats().size());
    assertEquals("Nature area protection", "HR", info.getProtection());
    assertNull("Geometry shouldn't be filled: too much info", info.getGeometry());
  }

  @Test
  public void testGetReceptorHabitatAreaInfo() throws SQLException {
    final List<HabitatInfo> infos = ReceptorInfoRepository.getReceptorHabitatAreaInfo(getCalcConnection(), getReceptor(RECEPTOR_POINT_ID),
        getCalcMessagesKey());
    assertNotNull("Result shouldn't be empty", infos);
    assertEquals("Number of habitat types", 1, infos.size());
    final HabitatInfo info = infos.get(0);
    assertEquals("Code of habitat type", "H7140A", info.getCode());
    assertEquals("Name of habitat type", "Overgangs- en trilvenen (trilvenen)", info.getName());
    assertEquals("KDW of habitat type", 1214, info.getCriticalLevels().get(EmissionResultKey.NOXNH3_DEPOSITION), 0.0);
    assertNotEquals("Hexagon overlap of habitat type", 0.0, info.getSurface(), 1E-3);
  }

  @Test
  public void testGetBackgroundEmissionResultInfo() throws SQLException {
    EmissionResults info = ReceptorInfoRepository.getBackgroundEmissionResult(getCalcConnection(), getReceptor(RECEPTOR_POINT_ID), 13);
    assertTrue("Emission results info object should be empty when retrieving wrong year", info.isEmpty());
    info = ReceptorInfoRepository.getBackgroundEmissionResult(getCalcConnection(), getReceptor(1), TestDomain.YEAR);
    assertTrue("Emission results info object should be empty when retrieving outside NL", info.isEmpty());
    info = ReceptorInfoRepository.getBackgroundEmissionResult(getCalcConnection(), getReceptor(RECEPTOR_POINT_ID), TestDomain.YEAR);
    assertNotNull("Emission results info object", info);
    assertNotEquals("Background NH3+NOx deposition", 0, info.get(EmissionResultKey.NOXNH3_DEPOSITION), 1);
  }

  private AeriusPoint getReceptor(final int id) {
    final AeriusPoint rp = new AeriusPoint(id);
    RECEPTOR_UTIL.setAeriusPointFromId(rp);
    return rp;
  }

}
