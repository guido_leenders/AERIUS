/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class RequestRepositoryTest extends RequestTestBase {

  @Test
  public void testGetExistingRequestFiles() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    ArrayList<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), request.getReference());
    assertNotNull("List of request files", requestFiles);
    assertEquals("Size of list of request files", 1, requestFiles.size());
    assertEquals("File format", FileFormat.PDF, requestFiles.get(0).getFileFormat());
    assertEquals("File type", RequestFileType.APPLICATION, requestFiles.get(0).getRequestFileType());
    assertNull("File name", requestFiles.get(0).getFileName());

    final InsertRequestFile requestFile = getFakeInsertRequestFile();
    requestFile.setRequestFileType(RequestFileType.DECREE);
    requestFile.setFileFormat(FileFormat.GML);
    requestFile.setFileName("SomeTestFile.wat");
    RequestModifyRepository.insertFileContent(getRegConnection(), requestId, requestFile);

    requestFiles = RequestRepository.getExistingRequestFiles(getRegConnection(), request.getReference());
    assertEquals("Size of list of request files", 2, requestFiles.size());
    boolean foundNew = false;
    for (final RequestFile foundRequestFile : requestFiles) {
      if (foundRequestFile.getRequestFileType() == RequestFileType.DECREE) {
        foundNew = true;
        assertEquals("File format decree", FileFormat.GML, foundRequestFile.getFileFormat());
        assertEquals("File name decree", "SomeTestFile.wat", foundRequestFile.getFileName());
      }
    }
    assertTrue("Should have found the new file", foundNew);
  }

  @Test
  public void testGetSpecificRequestFile() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    RequestFile requestFile = RequestRepository.getSpecificRequestFile(getRegConnection(), request.getReference(), RequestFileType.APPLICATION);
    assertNotNull("Returned application request file (inserted for every new request)", requestFile);
    assertEquals("File type", RequestFileType.APPLICATION, requestFile.getRequestFileType());
    assertEquals("File format", FileFormat.PDF, requestFile.getFileFormat());
    assertNull("File name", requestFile.getFileName());

    requestFile = RequestRepository.getSpecificRequestFile(getRegConnection(), request.getReference(), RequestFileType.DECREE);
    assertNull("Returned decree request file (shouldn't be there for a new request)", requestFile);

    final InsertRequestFile fakeFile = getFakeInsertRequestFile();
    fakeFile.setRequestFileType(RequestFileType.DECREE);
    fakeFile.setFileFormat(FileFormat.GML);
    fakeFile.setFileName("SomeTestFile.wat");
    RequestModifyRepository.insertFileContent(getRegConnection(), requestId, fakeFile);

    requestFile = RequestRepository.getSpecificRequestFile(getRegConnection(), request.getReference(), RequestFileType.DECREE);
    assertNotNull("Returned decree request file (after insertion)", requestFile);
    assertEquals("Decree File type", RequestFileType.DECREE, requestFile.getRequestFileType());
    assertEquals("Decree File format", fakeFile.getFileFormat(), requestFile.getFileFormat());
    assertEquals("Decree File name", fakeFile.getFileName(), requestFile.getFileName());
  }

  @Test
  public void testGetRequestFileSize() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final byte[] pdfContent = new byte[] {0, 1, 0, 1};
    final HashSet<Integer> requestIds = new HashSet<>();

    final InsertRequestFile insertRequestFile = getFakeInsertRequestFile();
    insertRequestFile.setFileContent(pdfContent);

    requestIds.add(RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request, insertRequestFile));
    assertEquals("Request file size", pdfContent.length, RequestRepository.getRequestsFileSize(getRegConnection(), requestIds));
  }

  @Test
  public void testGetRequestFileSizesComplicated() throws SQLException, AeriusException {
    final int amountOfRequests = 10;
    final HashSet<Integer> requestIds = new HashSet<>();
    int amountOfBytesExpected = 0;

    for (int i = 0; i < amountOfRequests; i++) {
      final Request request = getExampleRequest();
      final byte[] pdfContent = new byte[(int) (Math.random() * 30) + 1];
      amountOfBytesExpected += pdfContent.length;
      final InsertRequestFile insertRequestFile = getFakeInsertRequestFile();
      insertRequestFile.setFileContent(pdfContent);

      final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
          insertRequestFile);
      requestIds.add(requestId);

      if (i % 2 == 0) {
        final byte[] pdfContentDecree = new byte[(int) (Math.random() * 30) + 1];
        amountOfBytesExpected += pdfContentDecree.length;
        final InsertRequestFile insertRequestFileDecree = getFakeInsertRequestFile();
        insertRequestFileDecree.setFileContent(pdfContentDecree);
        insertRequestFileDecree.setRequestFileType(RequestFileType.DECREE);

        RequestModifyRepository.insertFileContent(getRegConnection(), requestId, insertRequestFileDecree);
      }
    }

    assertEquals("Request file sizes", amountOfBytesExpected, RequestRepository.getRequestsFileSize(getRegConnection(), requestIds));
  }

  @Test
  public void testGetAuthorityForRequestWithReference() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    assertNotEquals("Request ID returned", 0, requestId);
    final Authority authority = RequestRepository.getAuthorityForRequestWithReference(getRegConnection(), request.getReference());
    assertEquals("Authority for project", getAuthority(), authority);
  }

  @Test
  public void testGetRequestFileContent() throws SQLException, AeriusException, IOException {
    final Request request = getExampleRequest();
    final byte[] pdfContent = new byte[] {0, 1, 0};
    final InsertRequestFile insertRequestFile = getFakeInsertRequestFile();
    insertRequestFile.setFileContent(pdfContent);
    RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request, insertRequestFile);

    final byte[] pdfContentFromDb = RequestRepository.getRequestFileContent(getRegConnection(), request.getReference(), RequestFileType.APPLICATION);
    assertNotNull("Returned byte array", pdfContentFromDb);
    assertEquals("PDF content length in DB", pdfContent.length, pdfContentFromDb.length);
    assertArrayEquals("PDF content in DB", pdfContent, pdfContentFromDb);
  }

  @Test
  public void testGetRequestCalculationIds() throws SQLException, AeriusException {
    final Request request = getExampleRequest();
    final int requestId = RequestModifyRepository.insertNewRequest(getRegConnection(), SegmentType.PROJECTS, getAuthority(), request,
        getFakeInsertRequestFile());
    request.setId(requestId);
    Map<SituationType, Integer> calculationIdMap = RequestRepository.getCalculationIds(getRegConnection(), request.getReference());
    assertNotNull("Calculation ID map for request without calculations", calculationIdMap);
    assertTrue("Calculation ID map for request without calculations shouldn't be empty", calculationIdMap.isEmpty());

    final Calculation proposedCalculation = getExampleCalculation();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.PROPOSED, proposedCalculation.getCalculationId(), request);
    calculationIdMap = RequestRepository.getCalculationIds(getRegConnection(), request.getReference());
    assertEquals("Calculation ID map for request with 1 calculation", 1, calculationIdMap.size());
    assertEquals("Proper calculation ID for proposed", proposedCalculation.getCalculationId(), (int) calculationIdMap.get(SituationType.PROPOSED));

    final Calculation currentCalculation = getExampleCalculation();
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), SituationType.CURRENT, currentCalculation.getCalculationId(), request);
    calculationIdMap = RequestRepository.getCalculationIds(getRegConnection(), request.getReference());
    assertEquals("Calculation ID map for request with 1 calculation", 2, calculationIdMap.size());
    assertEquals("Proper calculation ID for proposed", proposedCalculation.getCalculationId(), (int) calculationIdMap.get(SituationType.PROPOSED));
    assertEquals("Proper calculation ID for current", currentCalculation.getCalculationId(), (int) calculationIdMap.get(SituationType.CURRENT));
  }

  @Test
  public void testGetRequestResultsForExport() throws SQLException {
    final PriorityProjectKey key = PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), "pp_Gelderland_5_3210");
    assertNotNull("Should be a priority project key for test", key);
    final Request request = PriorityProjectRepository.getSkinnedPriorityProjectByKey(getRegConnection(), key, getRegMessagesKey());
    assertNotNull("Should be a priority project for test", request);

    final List<AeriusResultPoint> resultPoints = RequestRepository.getResultsForExport(getRegConnection(), request.getId());
    assertNotNull("Result", resultPoints);
    assertFalse("Should be some results in test database", resultPoints.isEmpty());
  }

  @Test
  public void testGetOldestRequestWithoutCalculations() throws SQLException, AeriusException, IOException {
    final Request oldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);

    final Request newPermit = getInsertedExamplePermit();
    Request currentOldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
    if (oldestPermit != null) {
      assertEquals("Oldest permit should still be the same", oldestPermit.getId(), currentOldestPermit.getId());
    }

    addCalculation(oldestPermit == null ? newPermit : oldestPermit);
    currentOldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
    if (currentOldestPermit == null) {
      assertNull("If the current oldest is null, we should have started out without one.", oldestPermit);
    } else {
      assertNotEquals("Should retrieve some other permit", oldestPermit == null ? newPermit.getId() : oldestPermit.getId(),
          currentOldestPermit.getId());
    }
  }

  @Test
  public void testGetOldestPermitWithoutCalculationsOrder() throws SQLException, AeriusException, IOException, InterruptedException {
    //clean up all permits in DB.
    Request oldPermitAtStart = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
    if (oldPermitAtStart != null) {
      while (oldPermitAtStart != null) {
        addCalculation(oldPermitAtStart);
        oldPermitAtStart = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
      }
    }

    final Permit olderPermit = getInsertedExamplePermit();
    final Permit oldPermit = getInsertedExamplePermit();
    final Permit newPermit = getInsertedExamplePermit();

    //oldest permit should come first. If same date, the request_id (which is basically insertion order) is used.
    Request currentOldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
    assertEquals("Oldest permit ID should be the older permit", olderPermit.getId(), currentOldestPermit.getId());

    addCalculation(olderPermit);
    currentOldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
    assertEquals("Oldest permit ID should be the old permit.", oldPermit.getId(), currentOldestPermit.getId());

    addCalculation(oldPermit);
    currentOldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);
    assertEquals("Oldest permit ID should be the new permit.", newPermit.getId(), currentOldestPermit.getId());

    addCalculation(newPermit);
    currentOldestPermit = RequestRepository.getOldestRequestWithoutCalculations(getRegConnection(), SegmentType.PROJECTS);

    assertNull("As before, return value should be null", currentOldestPermit);
  }

  private Request getExampleRequest() throws SQLException {
    final Request request = new Request(SegmentType.PROJECTS);
    TestDomain.fillExampleRequest(request);
    return request;
  }

  private Authority getAuthority() {
    return TestDomain.getDefaultExampleAuthority();
  }

}
