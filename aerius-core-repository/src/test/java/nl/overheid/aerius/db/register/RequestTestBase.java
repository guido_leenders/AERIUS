/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.register.AuditTrailChange;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Base class for testing request related methods.
 */
public class RequestTestBase extends BaseDBTest {

  // used to create emission results
  private static final double EMISSION_FACTOR = 0.3;

  private static final String CLEAR_DEVELOPMENT_SPACES_PERMITS_NOTICES =
      "UPDATE development_spaces SET space = 0 WHERE segment IN ('projects', 'permit_threshold') AND space <> 0";

  private static final String CLEAR_DEVELOPMENT_SPACES_RECEPTOR =
      "UPDATE development_spaces SET space = 0 WHERE receptor_id = ?";

  private static final String UPDATE_DEVELOPMENT_SPACES_RECEPTOR_PERMITS_NOTICES = ""
      + "UPDATE development_spaces AS d "
      + "  SET space = ? * r.space "
      + "FROM reserved_development_spaces AS r "
      + "WHERE r.segment = d.segment "
      + "  AND r.receptor_id = d.receptor_id "
      + "  AND status = 'assigned' "
      + "  AND d.segment IN ('projects', 'permit_threshold') "
      + "  AND d.receptor_id = ? ";

  private static final String UPDATE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES_SEGMENT =
      "SELECT ae_update_initial_available_development_spaces_for_segment(?::segment_type)";

  private static final String UPDATE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES_RECEPTOR =
      "SELECT ae_update_initial_available_development_spaces_for_receptor(?)";

  private static final String RETRIEVE_ASSIGNED_DEVELOPMENT_SPACE =
      "SELECT space FROM development_spaces WHERE status = 'assigned' AND receptor_id = ? AND segment = ?::segment_type";

  private static final String RESERVED_DEVELOPMENT_SPACE_RECEPTOR_TO =
      "UPDATE reserved_development_spaces SET space = ? WHERE receptor_id = ? AND segment = ?::segment_type";
  private static final String RETRIEVE_RESERVED_DEVELOPMENT_SPACE_RECEPTOR =
      "SELECT space FROM reserved_development_spaces WHERE receptor_id = ? AND segment = ?::segment_type";

  private static final String INITIAL_AVAILABLE_DEVELOPMENT_SPACES_RECEPTOR =
      "SELECT space FROM initial_available_development_spaces WHERE receptor_id = ? AND segment = ?::segment_type";

  protected void assertPermitAfterInsert(final Permit permit) {
    assertNotEquals("Permit ID after inserting", 0, permit.getId());
    assertRetrievedPermit(permit);
    assertEquals("Permit state before inserting", RequestState.INITIAL, permit.getRequestState());
    assertNotNull("Permit sector", permit.getSector());
    assertNotNull("Permit location", permit.getPoint());
    assertNotNull("Permit application version", permit.getApplicationVersion());
    assertNotNull("Permit database version", permit.getDatabaseVersion());
    assertNotNull("Permit situations", permit.getSituations());
    assertNotNull("Permit situation for Proposed", permit.getSituations().get(SituationType.PROPOSED));
    assertNotNull("Permit emission values", permit.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues());
    assertEquals("Permit proposed emission values", permit.getProposedEmissionValues(),
        permit.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues());
    assertNotNull("Permit audit trail", permit.getChangeHistory());
    assertEquals("Permit audit trail size", 1, permit.getChangeHistory().size());
    assertNotNull("Permit audit trail item date", permit.getChangeHistory().get(0).getDate());
    assertEquals("Permit audit trail item changes size", 4, permit.getChangeHistory().get(0).getItems().size());
    for (final AuditTrailChange change : permit.getChangeHistory().get(0).getItems()) {
      switch (change.getType()) {
      case STATE:
        assertNull("State change old value", change.getOldValue());
        assertEquals("State change new value", RequestState.INITIAL.getDbValue(), change.getNewValue());
        break;
      case DOSSIER_ID:
        assertNull("Dossier ID change old value", change.getOldValue());
        assertEquals("Dossier ID change new value", permit.getDossierMetaData().getDossierId(), change.getNewValue());
        break;
      case AERIUS_ID:
        assertNull("AERIUS ID change old value", change.getOldValue());
        break;
      case SEGMENT:
        assertNull("Segment change old value", change.getOldValue());
        assertEquals("Segment change new value", SegmentType.PROJECTS.getDbValue(), change.getNewValue());
        break;
      default:
        fail("Found change type that wasn't expected on insert: " + change.getType());
        break;
      }
    }
  }

  protected void assertRetrievedPermit(final Permit permit) {
    //tests if the retrieved permit has the bare minimum of information.
    assertRetrievedRequest(permit);
    assertRetrievedDossierMetaData(permit.getDossierMetaData());
    assertNotNull("Dossier metadata handler", permit.getDossierMetaData().getHandler());
  }

  protected void assertRetrievedPriorityProject(final PriorityProject priorityProject) {
    //tests if the retrieved permit has the bare minimum of information.
    assertRetrievedRequest(priorityProject);
    assertRetrievedDossierMetaData(priorityProject.getDossierMetaData());
    assertNotNull("Key", priorityProject.getKey());
    assertEquals("Key dossier ID", priorityProject.getDossierMetaData().getDossierId(), priorityProject.getKey().getDossierId());
    for (final PrioritySubProject subProject : priorityProject.getSubProjects()) {
      assertRetrievedPrioritySubProject(subProject);
    }
  }

  protected void assertRetrievedPrioritySubProject(final PrioritySubProject prioritySubProject) {
    //tests if the retrieved permit has the bare minimum of information.
    assertRetrievedRequest(prioritySubProject);
    assertNotNull("Received date", prioritySubProject.getReceivedDate());
    assertNotNull("Parent key", prioritySubProject.getParentKey());
    assertNotNull("Parent key dossier ID", prioritySubProject.getParentKey().getDossierId());
    assertNotNull("Parent key authority code", prioritySubProject.getParentKey().getAuthorityCode());
  }

  protected void assertRetrievedRequest(final Request request) {
    //tests if the retrieved permit has the bare minimum of information.
    assertNotNull("Request reference", request.getReference());
    assertNotNull("Request status", request.getRequestState());
    assertNotNull("Request sector", request.getSector());
    assertNotNull("Request location", request.getPoint());
    assertNotNull("Request starting year", request.getStartYear());
    assertNotNull("Request scenario metadata", request.getScenarioMetaData());
    assertNotNull("Request project name", request.getScenarioMetaData().getProjectName());
    assertNotNull("Request corporation", request.getScenarioMetaData().getCorporation());
    assertNotNull("Request description", request.getScenarioMetaData().getDescription());
    assertNotNull("Request last modified", request.getLastModified());
    assertNotNull("Request authority", request.getAuthority());
  }

  protected void assertRetrievedDossierMetaData(final DossierMetaData dossierMetaData) {
    assertNotNull("Dossier metadata", dossierMetaData);
    assertNotNull("Dossier metadata dossier ID", dossierMetaData.getDossierId());
    assertNotNull("Dossier metadata received date", dossierMetaData.getReceivedDate());
  }

  protected void assertPermitComparison(final Permit original, final Permit permitFromDB) {
    assertEquals("dossier ID", original.getDossierMetaData().getDossierId(), permitFromDB.getDossierMetaData().getDossierId());

    assertEquals("Start date", original.getDossierMetaData().getReceivedDate(), permitFromDB.getDossierMetaData().getReceivedDate());
    assertEquals("Handler", original.getDossierMetaData().getHandler().getName(), permitFromDB.getDossierMetaData().getHandler().getName());
    assertEquals("Remarks", original.getDossierMetaData().getRemarks(), permitFromDB.getDossierMetaData().getRemarks());
    final EmissionValueKey keyNH3 = new EmissionValueKey(Substance.NH3);
    final EmissionValueKey keyNOX = new EmissionValueKey(Substance.NOX);
    assertEquals("NH3 emission", original.getProposedEmissionValues().getEmission(keyNH3),
        permitFromDB.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(keyNH3), 0.001);
    assertEquals("NOx emission", original.getProposedEmissionValues().getEmission(keyNOX),
        permitFromDB.getSituations().get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(keyNOX), 0.001);
  }

  protected UserProfile getExampleUserProfile() throws SQLException, AeriusException {
    return TestDomain.getDefaultExampleProfile(getRegConnection());
  }

  protected UserProfile getSecondExampleUserProfile() throws SQLException, AeriusException {
    final AdminUserProfile profile = new AdminUserProfile();
    profile.setName("Lewis");
    profile.setEmailAddress("somethingother@example.com");
    profile.setAuthority(TestDomain.getDefaultExampleAuthority());
    profile.getRoles().add(UserRepository.getRole(getRegConnection(), TestDomain.USERROLE_REGISTER_SUPERUSER));
    return TestDomain.getExampleUserProfile(getRegConnection(), profile);
  }

  protected UserProfile getLowLevelExampleUserProfile() throws SQLException, AeriusException {
    final AdminUserProfile profile = new AdminUserProfile();
    profile.setName("Murphy");
    profile.setEmailAddress("somethingotherstill@example.com");
    profile.setAuthority(TestDomain.getDefaultExampleAuthority());
    profile.getRoles().add(UserRepository.getRole(getRegConnection(), TestDomain.USERROLE_REGISTER_EDITOR));
    return TestDomain.getExampleUserProfile(getRegConnection(), profile);
  }

  protected Permit getExamplePermit() throws SQLException, AeriusException {
    final Permit permit = new Permit();
    TestDomain.fillExamplePermit(permit, UUID.randomUUID().toString(), getSecondExampleUserProfile());
    return permit;
  }

  protected Permit getInsertedExamplePermit() throws SQLException, AeriusException {
    final Permit permit = getExamplePermit();
    PermitModifyRepository.insertNewPermit(getRegConnection(), permit, getExampleUserProfile(), getFakeInsertRequestFile());
    return permit;
  }

  protected InsertRequestFile getFakeInsertRequestFile() {
    final InsertRequestFile insertRequestFile = new InsertRequestFile();
    insertRequestFile.setFileFormat(FileFormat.PDF);
    insertRequestFile.setFileContent(new byte[0]);
    insertRequestFile.setRequestFileType(RequestFileType.APPLICATION);
    return insertRequestFile;
  }

  protected Calculation getExampleCalculation() throws SQLException {
    final Calculation calculation = new Calculation();
    calculation.setYear(TestDomain.YEAR);
    CalculationRepository.insertCalculation(getRegConnection(), calculation, null);
    return calculation;
  }

  protected Permit getPermitWithCalculation() throws SQLException, AeriusException {
    final Permit permit = getInsertedExamplePermit();

    addCalculation(permit);

    return PermitRepository.getPermit(getRegConnection(), permit.getId());
  }

  protected void addCalculation(final Request request) throws SQLException {
    addCalculation(request, EMISSION_FACTOR, SituationType.PROPOSED);
  }

  protected int addCalculation(final Request request, final double emissionFactor, final SituationType situationType) throws SQLException {
    final Calculation calculation1 = getExampleCalculationWithResults(emissionFactor);
    RequestModifyRepository.insertSituationCalculation(getRegConnection(), situationType, calculation1.getCalculationId(), request);
    return calculation1.getCalculationId();
  }

  private Calculation getExampleCalculationWithResults(final double emissionFactor) throws SQLException {
    final Calculation calculation = getExampleCalculation();
    addCalculationResults(emissionFactor, calculation.getCalculationId());
    return calculation;
  }

  protected void addCalculationResults(final double emissionFactor, final int calculationId) throws SQLException {
    final List<AeriusResultPoint> result = getExampleResultPoints(emissionFactor);
    CalculationRepository.insertCalculationResults(getRegConnection(), calculationId, result);
    CalculationDevelopmentSpaceRepository.insertCalculationDemands(getRegConnection(), calculationId, 0, true);
  }

  protected List<AeriusResultPoint> getExampleResultPoints(final double emissionFactor) {
    final List<AeriusResultPoint> result = new ArrayList<>();
    final AeriusResultPoint point1 = TestDomain.getExamplePointBinnenveld();
    point1.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 1.23 * emissionFactor);
    result.add(point1);

    final AeriusResultPoint point2 = TestDomain.getExamplePointDwingelderveld();
    point2.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 1.10 * emissionFactor);
    point2.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 0.50 * emissionFactor);
    result.add(point2);
    return result;
  }

  protected void clearPermitsAndNoticesDevelopmentSpaces() throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(CLEAR_DEVELOPMENT_SPACES_PERMITS_NOTICES)) {
      ps.executeUpdate();
    }
    updateInitialAvailableDevelopmentSpacesSegment(SegmentType.PROJECTS);
    updateInitialAvailableDevelopmentSpacesSegment(SegmentType.PERMIT_THRESHOLD);
  }

  protected void clearExampleReceptorsDevelopmentSpaces() throws SQLException {
    final List<AeriusResultPoint> result = getExampleResultPoints(0);
    for (final AeriusResultPoint aeriusResultPoint : result) {
      if (aeriusResultPoint.getPointType() == AeriusPointType.RECEPTOR) {
        clearReceptorDevelopmentSpaces(aeriusResultPoint.getId());
      }
    }
  }

  protected void clearReceptorDevelopmentSpaces(final int receptorId) throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(CLEAR_DEVELOPMENT_SPACES_RECEPTOR)) {
      ps.setInt(1, receptorId);
      ps.executeUpdate();
    }
    updateInitialAvailableDevelopmentSpacesReceptor(receptorId);
  }

  protected void updateReceptorPermitsAndNoticesDevelopmentSpaces(final int receptorId, final double fractionSpace) throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(UPDATE_DEVELOPMENT_SPACES_RECEPTOR_PERMITS_NOTICES)) {
      ps.setDouble(1, fractionSpace);
      ps.setInt(2, receptorId);
      ps.executeUpdate();
    }
    updateInitialAvailableDevelopmentSpacesReceptor(receptorId);
  }

  protected void updateInitialAvailableDevelopmentSpacesSegment(final SegmentType segmentType) throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(UPDATE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES_SEGMENT)) {
      ps.setString(1, segmentType.getDbValue());
      ps.execute();
    }
  }

  protected void updateInitialAvailableDevelopmentSpacesReceptor(final int receptorId) throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(UPDATE_INITIAL_AVAILABLE_DEVELOPMENT_SPACES_RECEPTOR)) {
      ps.setInt(1, receptorId);
      ps.execute();
    }
  }

  protected void updateReservedDevelopmentSpaceReceptorTo(final int receptorId, final SegmentType segment, final double space) throws SQLException {
    try (final PreparedStatement stmt = getRegConnection().prepareStatement(RESERVED_DEVELOPMENT_SPACE_RECEPTOR_TO)) {
      QueryUtil.setValues(stmt, space, receptorId, segment.getDbValue());
      stmt.executeUpdate();
    }
  }

  protected double getReservedDevelopmentSpaceReceptor(final int receptorId, final SegmentType segment) throws SQLException {
    try (final PreparedStatement stmt = getRegConnection().prepareStatement(RETRIEVE_RESERVED_DEVELOPMENT_SPACE_RECEPTOR)) {
      QueryUtil.setValues(stmt, receptorId, segment.getDbValue());

      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        return rst.getDouble(1);
      }
    }

    fail("Couldn't fetch reserved development space for receptor");
    return 0;
  }

  protected double getInitialAvailableDevelopmentSpaceReceptor(final int receptorId, final SegmentType segment) throws SQLException {
    try (final PreparedStatement stmt = getRegConnection().prepareStatement(INITIAL_AVAILABLE_DEVELOPMENT_SPACES_RECEPTOR)) {
      QueryUtil.setValues(stmt, receptorId, segment.getDbValue());

      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        return rst.getDouble(1);
      }
    }

    fail("Couldn't fetch initial available development space for receptor");
    return 0;
  }

  protected void setReservedDevelopmentSpace(final int receptorId, final SegmentType segment, final double space) throws SQLException {
    try (final Connection con = getRegConnection()) {
      updateReservedDevelopmentSpaceReceptorTo(receptorId, segment, space);
      updateInitialAvailableDevelopmentSpacesReceptor(receptorId);
    }
  }

  protected void setReservedDevelopmentSpacesForResultPoints(final List<AeriusResultPoint> points, final SegmentType segment, final double space)
      throws SQLException {
    for (final AeriusResultPoint point : points) {
      setReservedDevelopmentSpace(point.getId(), segment, space);
    }
  }

  protected double getAssignedDevelopmentSpace(final int receptorId, final SegmentType segment) throws SQLException {
    double result = 0.0;
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(RETRIEVE_ASSIGNED_DEVELOPMENT_SPACE)) {
      ps.setInt(1, receptorId);
      ps.setString(2, segment.getDbValue());
      final ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        result = rs.getDouble("space");
      }
    }
    return result;
  }

  protected byte[] getPDFContent() throws IOException {
    final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    try (InputStream is = getFileInputStream("AERIUS_bijlage_test.pdf")) {

      int nRead;
      final byte[] data = new byte[16384];

      while ((nRead = is.read(data, 0, data.length)) != -1) {
        buffer.write(data, 0, nRead);
      }

      buffer.flush();
    }

    return buffer.toByteArray();
  }

}
