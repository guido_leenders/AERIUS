/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.db.common;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.Option;
import nl.overheid.aerius.shared.domain.OptionType;

/**
 *
 */
public class OptionsRepositoryTest extends BaseDBTest {

  @Test
  public void testGetOptions() throws SQLException {
    for (final OptionType optionType : OptionType.values()) {
      testReturnedOptions(optionType.name(), OptionsRepository.getOptions(getRegConnection(), optionType, optionType.getValueClass()));
    }
  }

  private <T extends Object> void testReturnedOptions(final String testDescription, final ArrayList<Option<T>> options) {
    assertNotNull(testDescription + ": Returned list shouldn't be null", options);
    assertFalse(testDescription + ": Returned list shouldn't be empty", options.isEmpty());
    boolean foundDefault = false;
    for (final Option<T> option : options) {
      if (option.isDefaultValue() && foundDefault) {
        fail(testDescription + ": Shouldn't find the default value twice");
      } else if (option.isDefaultValue()) {
        foundDefault = true;
      }
    }
    assertTrue(testDescription + ": Did not find the default value.", foundDefault);
  }

}
