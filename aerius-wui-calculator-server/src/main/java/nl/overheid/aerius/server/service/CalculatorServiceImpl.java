/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.conversion.EmissionsCalculator;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationPointRepository;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.geo.DetermineCalculationPointResult;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.HasEmissionValues;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.CalculatorService;

public class CalculatorServiceImpl implements CalculatorService {
  private static final Logger LOG = LoggerFactory.getLogger(CalculatorServiceImpl.class);

  private final PMF pmf;

  public CalculatorServiceImpl(final PMF pmf) {
    this.pmf = pmf;
  }

  @Override
  public DetermineCalculationPointResult determinePointsAutomatically(final int distanceFromSource, final ArrayList<EmissionSourceList> sourceLists)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return CalculationPointRepository.determinePointsAutomatically(con, distanceFromSource, sourceLists);
    } catch (final SQLException e) {
      LOG.error("SQL error in determinePointsAutomatically()", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public EmissionValues getEmissions(final HasEmissionValues emissionValues, final ArrayList<EmissionValueKey> keys, final EmissionSource parent)
      throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return EmissionsCalculator.getEmissions(con, emissionValues, parent.getGeometry(), keys, parent);
    } catch (final SQLException e) {
      LOG.error("SQL error in getEmissions()", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public ArrayList<InlandWaterwayType> suggestInlandShippingWaterway(final WKTGeometry geometry) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return ShippingRepository.suggestInlandShippingWaterway(con, geometry);
    } catch (final SQLException e) {
      LOG.error("SQL error in suggestInlandShippingWaterway()", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }
}
