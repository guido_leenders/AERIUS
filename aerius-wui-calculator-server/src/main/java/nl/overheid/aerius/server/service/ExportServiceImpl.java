/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.export.ExportTaskClient;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.domain.DownloadInfo;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportStatus;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.ExportService;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;
import nl.overheid.aerius.taskmanager.client.WorkerType;

public class ExportServiceImpl implements ExportService {
  private static final Logger LOG = LoggerFactory.getLogger(ExportServiceImpl.class);
  private static final String POSTFIX_DOWNLOAD_INFO = "-URL";

  private final ScenarioBaseSession session;

  public ExportServiceImpl(final CalculatorSession session) {
    this.session = session;
  }

  @Override
  public String exportCalculation(final ExportProperties exportProperties, final CalculatedScenario scenario) throws AeriusException {
    final String resultSessionKey = ExportStatus.KEY_PREFIX + UUID.randomUUID().toString();

    LOG.info("Export calculation: {}. Target key {}", exportProperties, resultSessionKey);

    final HttpSession httpSession = session.getSession();
    final ExportTaskResultCallback resultCallback = new ExportTaskResultCallback(httpSession, resultSessionKey);

    if (exportProperties != null && exportProperties.getExportType() != null) {
      // results should never be kept for Calculator at this time. Overwrite technically possible option by user.
      exportProperties.getAdditionalOptions().add(ExportAdditionalOptions.REMOVE_RESULTS);
      try {
        switch (exportProperties.getExportType()) {
        case PAA_OWN_USE:
          exportPAA(exportProperties, scenario, resultCallback);
          break;
//        case PAA_DEVELOPMENT_SPACES:
//        case PAA_DEMAND:
//          exportPAAWithoutCalculationPoints(exportProperties, scenario, resultCallback);
//          break;
        case OPS:
          exportOPS(exportProperties, scenario, resultCallback);
          break;
        case GML_SOURCES_ONLY:
        case GML_WITH_RESULTS:
        case GML_WITH_SECTORS_RESULTS:
          exportGML(exportProperties, scenario, resultCallback);
          break;
        default:
          throw new IllegalArgumentException("Enum value '" + exportProperties.getExportType().name() + "' isn't implemented yet!");
        }
        httpSession.setAttribute(resultSessionKey, ExportStatus.Status.RUNNING);
      } catch (final IOException e) {
        LOG.error("Error while trying to start export for key {}", resultSessionKey, e);
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }

    LOG.info("Export started with session key: {}", resultSessionKey);
    return resultSessionKey;
  }

//  private void exportPAAWithoutCalculationPoints(final ExportProperties exportProperties, final CalculatedScenario scenario,
//      final TaskResultCallback resultCallback) throws IOException {
//    scenario.getCalculationPoints().clear();
//    exportPAA(exportProperties, scenario, resultCallback);
//  }

  private void exportPAA(final ExportProperties exportProperties, final CalculatedScenario scenario, final TaskResultCallback resultCallback)
      throws IOException {
    // Make sure the calculation id's are not present, because they are recalculated by worker
    for (final Calculation calculation : scenario.getCalculations()) {
      calculation.setCalculationId(0);
    }
    // Start the actual PDF export
    ExportTaskClient.startPAAExport(TaskClientFactory.getInstance(), WorkerType.CALCULATOR, QueueEnum.PAA_EXPORT, exportProperties, scenario,
        resultCallback);
  }

  private void exportOPS(final ExportProperties exportProperties, final CalculatedScenario scenario, final TaskResultCallback resultCallback)
      throws IOException {
    ExportTaskClient.startOPSExport(TaskClientFactory.getInstance(), exportProperties, scenario, resultCallback);
  }

  private void exportGML(final ExportProperties exportProperties, final CalculatedScenario scenario, final TaskResultCallback resultCallback)
      throws IOException {
    ExportTaskClient.startGMLExport(TaskClientFactory.getInstance(), WorkerType.CALCULATOR, QueueEnum.GML_EXPORT, exportProperties, scenario,
        resultCallback);
  }

  @Override
  public ExportStatus getExportResult(final String key) throws AeriusException {
    final ExportStatus result = new ExportStatus();

    final HttpSession httpSession = session.getSession();
    if (!key.startsWith(ExportStatus.KEY_PREFIX) || httpSession.getAttribute(key) == null) {
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    result.setStatus((ExportStatus.Status) httpSession.getAttribute(key));
    result.setDownloadInfo((DownloadInfo) httpSession.getAttribute(key + POSTFIX_DOWNLOAD_INFO));
    return result;
  }

  private static class ExportTaskResultCallback implements TaskResultCallback {

    private final String resultSessionKey;
    private final HttpSession session;

    ExportTaskResultCallback(final HttpSession session, final String resultSessionKey) {
      this.session = session;
      this.resultSessionKey = resultSessionKey;
    }

    @Override
    public void onSuccess(final Object value, final String correlationId, final String messageId) {
      if (value instanceof ExportedData) {
        setSessionData(((ExportedData) value).getDownloadInfo());
      }
    }

    @Override
    public void onFailure(final Exception e, final String correlationId, final String messageId) {
      setSessionData(null);
    }

    private void setSessionData(final DownloadInfo di) {
      try {
        session.setAttribute(resultSessionKey, di == null ? ExportStatus.Status.ERROR : ExportStatus.Status.FINISHED);
        session.setAttribute(resultSessionKey + POSTFIX_DOWNLOAD_INFO, di);
      } catch (final IllegalStateException e) {
        // Eat error, but warn
        LOG.info("Export error eaten.", e);
      }
    }
  }
}
