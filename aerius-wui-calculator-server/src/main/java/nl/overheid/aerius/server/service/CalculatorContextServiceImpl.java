/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service with all methods related to contextual data. This is both global context data which is static during a user session as well as
 * user specific context data.
 */
public class CalculatorContextServiceImpl extends ScenarioBaseContextServiceImpl<CalculatorSession> {
  public CalculatorContextServiceImpl(final PMF pmf, final CalculatorSession session) {
    super(pmf, session);
  }

  @Override
  public CalculatorContext getContext() throws AeriusException {
    return (CalculatorContext) super.getContext();
  }

  @Override
  public CalculatorUserContext getUserContext() throws AeriusException {
    return (CalculatorUserContext) getUserContext(new CalculatorUserContext());
  }
}
