<%@page import="nl.overheid.aerius.server.servlet.FormPostUpload"%>
<%// Processes the gml when called via post method. If not a post nothing is shown.
// If it was a post the uuid is set and in the calculator this is picked up and
// retrieved from the server.
final String uuid = FormPostUpload.processFormPost(request, true, false);
if (uuid != null && !uuid.isEmpty()) {
  %><script> var aerius_upload_uuid = "<%=uuid%>";</script><%
}%>