/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory.CategoryUnit;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;

/**
 * Test for {@link PlanEmissionSource} class.
 */
public class PlanEmissionSourceTest {

  @Test
  public void testGetEmission() {
    final ArrayList<PlanCategory> categories = getCategories();

    final PlanEmission pe1 = new PlanEmission();
    pe1.setCategory(categories.get(0));
    pe1.setAmount(22);
    Assert.assertEquals("Emission for cat: COUNT", 44, pe1.getEmission(new EmissionValueKey(Substance.NOX)), 0.0001);

    final PlanEmission pe2 = new PlanEmission();
    pe2.setCategory(categories.get(4));
    pe2.setAmount(22);
    Assert.assertEquals("Emission for cat: NO_UNIT", 203, pe2.getEmission(new EmissionValueKey(Substance.NOX)), 0.0001);

    final PlanEmissionSource es3 = new PlanEmissionSource();
    final PlanEmission pe3 = new PlanEmission();
    pe3.setCategory(categories.get(2));
    pe3.setAmount(22);
    es3.getEmissionSubSources().add(pe3);
    Assert.assertEquals("Emission for cat: SQUARE_METERS", 308, es3.getEmission(new EmissionValueKey(Substance.NOXNH3)), 0.0001);
  }

  @Test
  public void testCopyAndHash() {
    final ArrayList<PlanCategory> categories = getCategories();
    final PlanEmissionSource pevs = new PlanEmissionSource();

    final PlanEmission pe1 = new PlanEmission();
    pe1.setCategory(categories.get(0));
    pe1.setAmount(22);
    pevs.getEmissionSubSources().add(pe1);

    final PlanEmission pe2 = new PlanEmission();
    pe2.setCategory(categories.get(4));
    pe2.setAmount(33);
    pevs.getEmissionSubSources().add(pe2);
    final PlanEmission pe3 = new PlanEmission();
    pe3.setCategory(categories.get(2));
    pe3.setAmount(49);
    pevs.getEmissionSubSources().add(pe3);
    final EmissionSource copy = pevs.copy();
    Assert.assertEquals("StateHash must be the same for Plan Emission Values", pevs.getStateHash(), copy.getStateHash());
  }

  private ArrayList<PlanCategory> getCategories() {
    final ArrayList<PlanCategory> categories = new ArrayList<PlanCategory>();
    // dummy data
    final PlanCategory pc0 = new PlanCategory();
    pc0.setCategoryUnit(CategoryUnit.COUNT);
    pc0.setId(1);
    pc0.setName("Consumenten:Appartement");
    pc0.setEmissionFactor(Substance.NOX, 2d);
    categories.add(pc0);

    final PlanCategory pc1 = new PlanCategory();
    pc1.setCategoryUnit(CategoryUnit.COUNT);
    pc1.setId(2);
    pc1.setName("Consumenten:Tussenwoning");
    pc1.setEmissionFactor(Substance.NOX, 3d);
    categories.add(pc1);

    final PlanCategory pc2 = new PlanCategory();
    pc2.setCategoryUnit(CategoryUnit.SQUARE_METERS);
    pc2.setId(3);
    pc2.setName("Glastuinbouw");
    pc2.setEmissionFactor(Substance.NH3, 4d);
    pc2.setEmissionFactor(Substance.NOX, 10d);
    categories.add(pc2);

    final PlanCategory pc3 = new PlanCategory();
    pc3.setCategoryUnit(CategoryUnit.MEGA_WATT_HOURS);
    pc3.setId(4);
    pc3.setName("Energie");
    pc3.setEmissionFactor(Substance.NOX, 6d);
    categories.add(pc3);

    final PlanCategory pc4 = new PlanCategory();
    pc4.setCategoryUnit(CategoryUnit.NO_UNIT);
    pc4.setId(5);
    pc4.setName("Bouwmaterialen:Gips (voor pleisterwek)");
    pc4.setEmissionFactor(Substance.NOX, 203d);
    categories.add(pc4);
    // dummy data
    return categories;

  }
}
