/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;

/**
 * Test class for {@link AeriusPoint}.
 */
public class AeriusPointTest {

  @Test
  public void testEquals() {
    final AeriusPoint ap1 = new AeriusPoint(1, AeriusPointType.POINT, 400, 500);
    final AeriusPoint ap2 = new AeriusPoint(2, AeriusPointType.POINT, 400, 500);
    final AeriusPoint ap3 = new AeriusPoint(2, AeriusPointType.POINT, 500, 400);
    final AeriusPoint ap4 = new AeriusPoint(3, AeriusPointType.POINT, 500, 400);
    final AeriusPoint ap5 = new AeriusPoint(3, AeriusPointType.RECEPTOR, 500, 400);

    assertNotEquals("id not same, x-y not", ap1, ap2);
    assertEquals("id the same, x-y not", ap2, ap3);
    assertNotEquals("id not same, x-y same", ap3, ap4);
    assertNotEquals("id same, x-y same, type not", ap4, ap5);
  }
}
