/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.ScenarioSectorUtil;
import nl.overheid.aerius.shared.domain.scenario.SectorEmissionSummary;
import nl.overheid.aerius.shared.domain.sector.ScenarioSectorInformation;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;

/**
 * Test class for {@link ScenarioSectorUtil}.
 */
public class ScenarioSectorUtilTest {

  /**
   * Test differences between lists with same content.
   * Should still return a difference for each sector, just with 0 for values.
   */
  @Test
  public void testGetDifferencesSameList() {
    final EmissionValueKey emissionValueKey = new EmissionValueKey(Substance.NH3);

    final EmissionSourceList list1 = new EmissionSourceList();
    final GenericEmissionSource list1Source1 = new GenericEmissionSource(1, new Point(1, 2));
    final Sector sector1 = new Sector(1700, SectorGroup.INDUSTRY, "Description1", null);
    list1Source1.setSector(sector1);
    list1Source1.setEmission(emissionValueKey, 10.0);
    list1.add(list1Source1);

    final EmissionSourceList list2 = new EmissionSourceList();
    final EmissionSource list2Source1 = list1Source1;
    list2.add(list2Source1.copy());

    final ArrayList<SectorEmissionSummary> differences =
        ScenarioSectorUtil.getDifferences(list1, list2, emissionValueKey);
    assertEquals("Differences in list", 1, differences.size());
    assertEquals("Sector of the difference", sector1, differences.get(0).getSector());
    assertEquals("Difference in number of sources", 0, differences.get(0).getNumberOfSources());
    assertEquals("Difference in emission", 0, differences.get(0).getSummedEmission(), 0.0001);
  }

  /**
   * Test differences between lists with same number of sources, but different emission values.
   */
  @Test
  public void testGetDifferencesEmission() {
    final EmissionValueKey emissionValueKey = new EmissionValueKey(Substance.NH3);

    final EmissionSourceList list1 = new EmissionSourceList();
    final GenericEmissionSource list1Source1 = new GenericEmissionSource(1, new Point(1, 2));
    final Sector sector1 = new Sector(1700, SectorGroup.INDUSTRY, "Description1", null);
    list1Source1.setSector(sector1);
    list1Source1.setEmission(emissionValueKey, 10.0);
    list1.add(list1Source1);

    final EmissionSourceList list2 = new EmissionSourceList();
    final GenericEmissionSource list2Source1 = new GenericEmissionSource(2, new Point(1, 2));
    list2Source1.setSector(sector1);
    list2Source1.setEmission(emissionValueKey, 5.0);
    //set emission for NOx as well to ensure total isn't taken when using EVK for NH3.
    list2Source1.setEmission(new EmissionValueKey(Substance.NOX), 10.0);
    list2.add(list2Source1);

    final ArrayList<SectorEmissionSummary> differences =
        ScenarioSectorUtil.getDifferences(list1, list2, emissionValueKey);
    assertEquals("Differences in list", 1, differences.size());
    assertEquals("Sector of the difference", sector1, differences.get(0).getSector());
    assertEquals("Difference in number of sources", 0, differences.get(0).getNumberOfSources());
    assertEquals("Difference in emission", -5, differences.get(0).getSummedEmission(), 0.0001);
  }

  /**
   * Test differences between lists with different number of sources, but same total emission values.
   */
  @Test
  public void testGetDifferencesNumberOfSources() {
    final EmissionValueKey emissionValueKey = new EmissionValueKey(Substance.NH3);

    final EmissionSourceList list1 = new EmissionSourceList();
    final GenericEmissionSource list1Source1 = new GenericEmissionSource(1, new Point(1, 2));
    final Sector sector1 = new Sector(1700, SectorGroup.INDUSTRY, "Description1", null);
    list1Source1.setSector(sector1);
    list1Source1.setEmission(emissionValueKey, 10.0);
    list1.add(list1Source1);

    final EmissionSourceList list2 = new EmissionSourceList();
    final GenericEmissionSource list2Source1 = new GenericEmissionSource(2, new Point(1, 2));
    list2Source1.setSector(sector1);
    list2Source1.setEmission(emissionValueKey, 5.0);
    list2.add(list2Source1);
    list2.add(list2Source1.copy());

    final ArrayList<SectorEmissionSummary> differences =
        ScenarioSectorUtil.getDifferences(list1, list2, emissionValueKey);
    assertEquals("Differences in list", 1, differences.size());
    assertEquals("Sector of the difference", sector1, differences.get(0).getSector());
    assertEquals("Difference in number of sources", 1, differences.get(0).getNumberOfSources());
    assertEquals("Difference in emission", 0, differences.get(0).getSummedEmission(), 0.0001);
  }

  /**
   * Test differences between lists with different number of sources, but same total emission values.
   */
  @Test
  public void testGetDifferencesSector() {
    final EmissionValueKey emissionValueKey = new EmissionValueKey(Substance.NH3);
    final EmissionSourceList list1 = new EmissionSourceList();
    final GenericEmissionSource list1Source1 = new GenericEmissionSource(1, new Point(1, 2));
    final Sector sector1 = new Sector(1700, SectorGroup.INDUSTRY, "Description1", null);
    list1Source1.setSector(sector1);
    list1Source1.setEmission(emissionValueKey, 10.0);
    list1.add(list1Source1);

    final GenericEmissionSource list1Source2 = new GenericEmissionSource(2, new Point(1, 2));
    final Sector sector2 = new Sector(2, SectorGroup.ENERGY, "Description2", null);
    list1Source2.setSector(sector2);
    list1Source2.setEmission(emissionValueKey, 100.0);
    list1.add(list1Source2);

    final EmissionSourceList list2 = new EmissionSourceList();
    final GenericEmissionSource list2Source1 = new GenericEmissionSource(2, new Point(1, 2));
    list2Source1.setSector(sector1);
    list2Source1.setEmission(emissionValueKey, 5.0);
    list2.add(list2Source1);
    list2.add(list2Source1.copy());
    list2.add(list2Source1.copy());

    final GenericEmissionSource list2Source2 = new GenericEmissionSource(4, new Point(1, 2));
    final Sector sector3 = new Sector(3, SectorGroup.AGRICULTURE, "Description3", null);
    list2Source2.setSector(sector3);
    list2Source2.setEmission(emissionValueKey, 50.0);
    list2.add(list2Source2);

    final ArrayList<SectorEmissionSummary> differences =
        ScenarioSectorUtil.getDifferences(list1, list2, emissionValueKey);
    assertEquals("Differences in list", 3, differences.size());
    //list should be ordered by emission, ordered from highest increase to highest decrease.
    //first should be sector3 with increase of 50.
    SectorEmissionSummary assertDifference = differences.get(0);
    assertEquals("Sector of the difference", sector3, assertDifference.getSector());
    assertEquals("Difference in number of sources", 1, assertDifference.getNumberOfSources());
    assertEquals("Difference in emission", 50, assertDifference.getSummedEmission(), 0.0001);
    //next sector1 with increase of 10 (but more sources).
    assertDifference = differences.get(1);
    assertEquals("Sector of the difference", sector1, assertDifference.getSector());
    assertEquals("Difference in number of sources", 2, assertDifference.getNumberOfSources());
    assertEquals("Difference in emission", 5, assertDifference.getSummedEmission(), 0.0001);
    //last sector 2 with decrease of 100.
    assertDifference = differences.get(2);
    assertEquals("Sector of the difference", sector2, assertDifference.getSector());
    assertEquals("Difference in number of sources", -1, assertDifference.getNumberOfSources());
    assertEquals("Difference in emission", -100, assertDifference.getSummedEmission(), 0.0001);
  }

  @Test
  public void testDetermineSectorInformation() {
    final EmissionValueKey emissionValueKey = new EmissionValueKey(Substance.NH3);

    final EmissionSourceList emissionSourceList = new EmissionSourceList();
    ScenarioSectorInformation sectorInformation = ScenarioSectorUtil.determineSectorInformation(emissionSourceList, 0);
    assertEquals("Main sector for empty list", Sector.SECTOR_DEFAULT, sectorInformation.getMainSector());
    assertFalse("Multiple sectors for empty list", sectorInformation.isMultipleSectors());

    final GenericEmissionSource source1 = new GenericEmissionSource(1, new Point(1, 2));
    final Sector sector1 = new Sector(1700, SectorGroup.INDUSTRY, "Description1", null);
    source1.setSector(sector1);
    source1.setEmission(emissionValueKey, 10.0);
    emissionSourceList.add(source1);

    sectorInformation = ScenarioSectorUtil.determineSectorInformation(emissionSourceList, 0);
    assertEquals("Main sector for 1 source", sector1, sectorInformation.getMainSector());
    assertFalse("Multiple sectors for 1 source", sectorInformation.isMultipleSectors());

    final GenericEmissionSource source2 = new GenericEmissionSource(2, new Point(0, 0));
    final Sector sector2 = new Sector(1800, SectorGroup.RAIL_TRANSPORTATION, "Description2", null);
    source2.setSector(sector2);
    source2.setEmission(emissionValueKey, 15.0);
    emissionSourceList.add(source2);

    sectorInformation = ScenarioSectorUtil.determineSectorInformation(emissionSourceList, 0);
    assertEquals("Main sector for 2 sources", sector2, sectorInformation.getMainSector());
    assertTrue("Multiple sectors for 2 sources", sectorInformation.isMultipleSectors());

    final GenericEmissionSource source3 = new GenericEmissionSource(3, new Point(-10, 200));
    source3.setSector(sector1);
    source3.setEmission(emissionValueKey, 2.0);
    emissionSourceList.add(source3);

    sectorInformation = ScenarioSectorUtil.determineSectorInformation(emissionSourceList, 0);
    assertEquals("Main sector for 3 sources", sector2, sectorInformation.getMainSector());
    assertTrue("Multiple sectors for 3 sources", sectorInformation.isMultipleSectors());

    final GenericEmissionSource source4 = new GenericEmissionSource(3, new Point(90, 200));
    source4.setSector(sector1);
    source4.setEmission(emissionValueKey, 3.0);
    emissionSourceList.add(source4);

    sectorInformation = ScenarioSectorUtil.determineSectorInformation(emissionSourceList, 0);
    assertEquals("Main sector for 4 sources", sector1, sectorInformation.getMainSector());
    assertTrue("Multiple sectors for 4 sources", sectorInformation.isMultipleSectors());
  }

}
