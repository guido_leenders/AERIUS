/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for {@link CriticalLevels}.
 */
public class CriticalLevelsTest {

  @Test
  public void testCriticalLevels() {
    final CriticalLevels criticalLevels = new CriticalLevels();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    Assert.assertTrue("Emission results are empty.", criticalLevels.isEmpty());
    Assert.assertEquals("Deposition NOx equals 0.", 0.0, criticalLevels.get(noxKey), 0.0001);
    Assert.assertEquals("Deposition NH3 equals 0.", 0.0, criticalLevels.get(nh3Key), 0.0001);
    Assert.assertEquals("Deposition NH3+NOx equals 0.", 0.0, criticalLevels.get(comboKey), 0.0001);
    Assert.assertFalse("result NOx", criticalLevels.hasResult(noxKey));
    Assert.assertFalse("result NH3", criticalLevels.hasResult(nh3Key));
    Assert.assertFalse("result combo", criticalLevels.hasResult(comboKey));

    criticalLevels.put(noxKey, 20.5);
    Assert.assertFalse("Emission results are not empty.", criticalLevels.isEmpty());
    Assert.assertTrue("result NOx", criticalLevels.hasResult(noxKey));
    Assert.assertFalse("result NH3", criticalLevels.hasResult(nh3Key));
    Assert.assertTrue("result combo", criticalLevels.hasResult(comboKey));
    criticalLevels.put(nh3Key, 10.5);
    Assert.assertFalse("Emission results are not empty.", criticalLevels.isEmpty());
    Assert.assertEquals("Deposition NOx equals 20.5.", 20.5, criticalLevels.get(noxKey), 0.0001);
    Assert.assertEquals("Deposition NH3 equals 10.5.", 10.5, criticalLevels.get(nh3Key), 0.0001);
    Assert.assertEquals("Deposition NH3+NOx equals 31.", 31.0, criticalLevels.get(comboKey), 0.0001);
    Assert.assertTrue("result NOx", criticalLevels.hasResult(noxKey));
    Assert.assertTrue("result NH3", criticalLevels.hasResult(nh3Key));
    Assert.assertTrue("result combo", criticalLevels.hasResult(comboKey));

    // Test reset
    criticalLevels.put(noxKey, 0.0);
    criticalLevels.put(nh3Key, 0.0);
    Assert.assertEquals("Deposition NOx equals 0.", 0.0, criticalLevels.get(noxKey), 0.0001);
    Assert.assertEquals("Deposition NH3 equals 0.", 0.0, criticalLevels.get(nh3Key), 0.0001);
    Assert.assertEquals("Deposition NH3+NOx equals 0.", 0.0, criticalLevels.get(comboKey), 0.0001);

    // Test explicit sum post-reset
    criticalLevels.putUnhatched(comboKey, 5.5);
    Assert.assertFalse("Emission results are not empty.", criticalLevels.isEmpty());
    Assert.assertEquals("Deposition NOx equals 0.", 0.0, criticalLevels.get(noxKey), 0.0001);
    Assert.assertEquals("Deposition NH3 equals 0.", 0.0, criticalLevels.get(nh3Key), 0.0001);
    Assert.assertEquals("Deposition NH3+NOx equals 5.5.", 5.5, criticalLevels.get(comboKey), 0.0001);
    Assert.assertTrue("result NOx", criticalLevels.hasResult(noxKey));
    Assert.assertTrue("result NH3", criticalLevels.hasResult(nh3Key));
    Assert.assertTrue("result combo", criticalLevels.hasResult(comboKey));
  }

  @Test
  public void testResultHatching() {
    final CriticalLevels criticalLevels = new CriticalLevels();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    criticalLevels.put(comboKey, 5.0);

    Assert.assertEquals("Concentration NOx equals 5.", 5, criticalLevels.get(noxKey), 0.0001);
    Assert.assertEquals("Concentration NH3 equals 5.", 5, criticalLevels.get(nh3Key), 0.0001);
  }

  @Test
  public void testResultAddition() {
    final CriticalLevels criticalLevels = new CriticalLevels();
    final EmissionResultKey noxKey = EmissionResultKey.NOX_DEPOSITION;
    final EmissionResultKey nh3Key = EmissionResultKey.NH3_DEPOSITION;
    final EmissionResultKey comboKey = EmissionResultKey.NOXNH3_DEPOSITION;

    criticalLevels.put(noxKey, 5.0);
    criticalLevels.put(nh3Key, 5.0);

    Assert.assertEquals("Deposition NOx equals 5.", 5.0, criticalLevels.get(noxKey), 0.0001);
    Assert.assertEquals("Deposition NH3 equals 5.", 5.0, criticalLevels.get(nh3Key), 0.0001);
    Assert.assertEquals("Deposition NH3+NOx equals 10.", 10.0, criticalLevels.get(comboKey), 0.0001);
    Assert.assertTrue("result NOx", criticalLevels.hasResult(noxKey));
    Assert.assertTrue("result NH3", criticalLevels.hasResult(nh3Key));
    Assert.assertTrue("result combo", criticalLevels.hasResult(comboKey));

    criticalLevels.put(comboKey, 5.0);

    // Must be 5, not 10
    Assert.assertEquals("Deposition NH3+NOx equals 5", 5.0, criticalLevels.get(comboKey), 0.0001);
    Assert.assertTrue("result NOx", criticalLevels.hasResult(noxKey));
    Assert.assertTrue("result NH3", criticalLevels.hasResult(nh3Key));
    Assert.assertTrue("result combo", criticalLevels.hasResult(comboKey));
  }
}
