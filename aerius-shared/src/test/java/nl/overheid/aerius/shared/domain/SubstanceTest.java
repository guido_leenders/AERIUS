/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test for class {@link Substance}.
 */
public class SubstanceTest {

  @Test
  public void testContains() {
    assertTrue("Test if NOx contains NOx", Substance.NOX.contains(Substance.NOX));
    assertFalse("Test if NOx doesn't contains NH3", Substance.NOX.contains(Substance.NH3));
    assertTrue("Test if NOXNH3 contains NOx", Substance.NOXNH3.contains(Substance.NOX));
    assertTrue("Test if NOXNH3 contains NH3", Substance.NOXNH3.contains(Substance.NH3));
    assertTrue("Test if NOXNH3 contains NOXNH3", Substance.NOXNH3.contains(Substance.NOXNH3));
    assertFalse("Test if NOX contains NOXNH3", Substance.NOX.contains(Substance.NOXNH3));
    assertFalse("Test if NH3 contains NOXNH3", Substance.NH3.contains(Substance.NOXNH3));
    assertFalse("Test if NOXNH3 doesn't contains PM10", Substance.NOX.contains(Substance.PM10));
  }

  @Test
  public void testContainsPartly() {
    assertTrue("Test if NOx contains NOx", Substance.NOX.containsCurrentOrGiven(Substance.NOX));
    assertFalse("Test if NOx doesn't contains NH3", Substance.NOX.containsCurrentOrGiven(Substance.NH3));
    assertTrue("Test if NOXNH3 contains NOx", Substance.NOXNH3.containsCurrentOrGiven(Substance.NOX));
    assertTrue("Test if NOXNH3 contains NH3", Substance.NOXNH3.containsCurrentOrGiven(Substance.NH3));
    assertTrue("Test if NOXNH3 contains NOXNH3", Substance.NOXNH3.containsCurrentOrGiven(Substance.NOXNH3));
    assertTrue("Test if NOX contains NOXNH3", Substance.NOX.containsCurrentOrGiven(Substance.NOXNH3));
    assertTrue("Test if NH3 contains NOXNH3", Substance.NH3.containsCurrentOrGiven(Substance.NOXNH3));
    assertFalse("Test if NOX doesn't contains PM10", Substance.NOX.containsCurrentOrGiven(Substance.PM10));
  }
}
