/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link InlandWaterwayTypeUtil}.
 */
public class InlandWaterwayTypeUtilTest {

  @Test
  public void testIsDirectionCorrect() {
    assertTrue("Empty InlandWaterwayType", InlandWaterwayTypeUtil.isDirectionCorrect(new InlandWaterwayType()));
    assertTrue("Category null", getWaterwayType((InlandWaterwayCategory) null, null));
    assertTrue("Direction null, but irrelevant", getWaterwayType((WaterwayDirection) null, WaterwayDirection.IRRELEVANT));
    assertTrue("Direction set, but irrelevant", getWaterwayType(WaterwayDirection.DOWNSTREAM, WaterwayDirection.IRRELEVANT));
    assertFalse("Direction null, but relevant", getWaterwayType((WaterwayDirection) null, WaterwayDirection.DOWNSTREAM, WaterwayDirection.UPSTREAM));
    assertFalse("Direction set to irrelevant, but relevant",
        getWaterwayType(WaterwayDirection.IRRELEVANT, WaterwayDirection.DOWNSTREAM, WaterwayDirection.UPSTREAM));
    assertTrue("Direction set to Downstream and relevant",
        getWaterwayType(WaterwayDirection.DOWNSTREAM, WaterwayDirection.DOWNSTREAM, WaterwayDirection.UPSTREAM));
  }

  private boolean getWaterwayType(final WaterwayDirection direction, final WaterwayDirection... categoryDirections) {
    return getWaterwayType(new InlandWaterwayCategory(), direction, categoryDirections);
  }

  private boolean getWaterwayType(final InlandWaterwayCategory category, final WaterwayDirection direction,
      final WaterwayDirection... categoryDirections) {
    final InlandWaterwayType iwt = new InlandWaterwayType();
    if (category != null) {
      category.setDirections(new ArrayList<>(Arrays.asList(categoryDirections)));
    }
    iwt.setWaterwayCategory(category);
    iwt.setWaterwayDirection(direction);
    return InlandWaterwayTypeUtil.isDirectionCorrect(iwt);
  }


  @Test
  public void testDetectInconclusiveRoute() {
    final String sourceId = "1";
    final List<InlandWaterwayType> list = new ArrayList<>();
    assertNull("No warning", InlandWaterwayTypeUtil.detectInconclusiveRoute(sourceId, list));
    addWaterwayType(list, "First");
    assertNull("No warning with 1 waterway", InlandWaterwayTypeUtil.detectInconclusiveRoute(sourceId, list));
    addWaterwayType(list, "Second");
    assertSame("Check Reason", Reason.INLAND_SHIPPING_WATERWAY_INCONCLUSIVE,
        InlandWaterwayTypeUtil.detectInconclusiveRoute(sourceId, list).getReason());
    addWaterwayType(list, "Third");
    final AeriusException ae = InlandWaterwayTypeUtil.detectInconclusiveRoute(sourceId, list);
    assertEquals("Check argument", "Second, Third", ae.getArgs()[2]);
  }

  private void addWaterwayType(final List<InlandWaterwayType> list, final String code) {
    final InlandWaterwayType iwt = new InlandWaterwayType();
    final InlandWaterwayCategory category = new InlandWaterwayCategory();
    category.setCode(code);
    iwt.setWaterwayCategory(category);
    list.add(iwt);
  }
}
