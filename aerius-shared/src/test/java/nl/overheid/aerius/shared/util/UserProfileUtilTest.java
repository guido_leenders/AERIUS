/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AuthorizationException;

/**
 *
 */
public class UserProfileUtilTest {

  private enum TestPermission implements Permission {

    TEST_PERMISSION_1, TEST_PERMISSION_2;

    @Override
    public String getName() {
      return name();
    }

  }

  @Test
  public void testHasPermission() {
    final UserProfile userProfile = new UserProfile();

    assertTrue("Userprofile with proper permission, checking no permissions", UserProfileUtil.hasPermission(userProfile));
    assertFalse("Userprofile without any permissions", UserProfileUtil.hasPermission(userProfile, TestPermission.TEST_PERMISSION_1));

    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    assertTrue("Userprofile with proper permission", UserProfileUtil.hasPermission(userProfile, TestPermission.TEST_PERMISSION_1));
    assertFalse("Userprofile with a permission, checking other permission",
        UserProfileUtil.hasPermission(userProfile, TestPermission.TEST_PERMISSION_2));
    assertTrue("Userprofile with proper permission, multiple permissions",
        UserProfileUtil.hasPermission(userProfile, TestPermission.TEST_PERMISSION_1, TestPermission.TEST_PERMISSION_2));
  }

  @Test
  public void testHasPermissionForAuthority() {
    final UserProfile userProfile = new UserProfile();
    final Authority userAuthority = new Authority();
    userAuthority.setId(1);
    userProfile.setAuthority(userAuthority);
    final Authority differentAuthority = new Authority();
    differentAuthority.setId(2);

    assertTrue("Userprofile with proper permission and authority and checking no permissions",
        UserProfileUtil.hasPermissionForAuthority(userProfile, userAuthority));
    assertFalse("Userprofile with proper permission checking no permissions, but wrong authority",
        UserProfileUtil.hasPermissionForAuthority(userProfile, differentAuthority));
    assertFalse("Userprofile without any permissions and proper authority",
        UserProfileUtil.hasPermissionForAuthority(userProfile, userAuthority, TestPermission.TEST_PERMISSION_1));
    assertFalse("Userprofile without any permissions and different authority",
        UserProfileUtil.hasPermissionForAuthority(userProfile, differentAuthority, TestPermission.TEST_PERMISSION_1));

    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    assertTrue("Userprofile with proper permission and authority",
        UserProfileUtil.hasPermissionForAuthority(userProfile, userAuthority, TestPermission.TEST_PERMISSION_1));
    assertFalse("Userprofile with proper permission, but wrong authority",
        UserProfileUtil.hasPermissionForAuthority(userProfile, differentAuthority, TestPermission.TEST_PERMISSION_1));
    assertFalse("Userprofile with a permission and proper authority, checking other permission",
        UserProfileUtil.hasPermissionForAuthority(userProfile, userAuthority, TestPermission.TEST_PERMISSION_2));
    assertFalse("Userprofile with a permission and wrong authority, checking other permission",
        UserProfileUtil.hasPermissionForAuthority(userProfile, differentAuthority, TestPermission.TEST_PERMISSION_2));
    assertTrue("Userprofile with proper permission and authority, multiple permissions",
        UserProfileUtil.hasPermissionForAuthority(userProfile, userAuthority, TestPermission.TEST_PERMISSION_1, TestPermission.TEST_PERMISSION_2));
    assertFalse("Userprofile with proper permission and wrong authority, multiple permissions",
        UserProfileUtil.hasPermissionForAuthority(userProfile, differentAuthority,
            TestPermission.TEST_PERMISSION_1, TestPermission.TEST_PERMISSION_2));
  }

  @Test
  public void testCheckPermission() throws AuthorizationException {
    final UserProfile userProfile = new UserProfile();
    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    UserProfileUtil.checkPermission(userProfile, TestPermission.TEST_PERMISSION_1);
    //nothing to assert really, the method throws or it doesn't...
  }

  @Test(expected = AuthorizationException.class)
  public void testCheckPermissionFail() throws AuthorizationException {
    final UserProfile userProfile = new UserProfile();
    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    UserProfileUtil.checkPermission(userProfile, TestPermission.TEST_PERMISSION_2);
  }

  @Test
  public void testCheckPermissionForAuthority() throws AuthorizationException {
    final UserProfile userProfile = new UserProfile();
    final Authority userAuthority = new Authority();
    userAuthority.setId(1);
    userProfile.setAuthority(userAuthority);
    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    UserProfileUtil.checkPermissionAndAuthority(userProfile, userAuthority, TestPermission.TEST_PERMISSION_1);
    //nothing to assert really, the method throws or it doesn't...
  }

  @Test(expected = AuthorizationException.class)
  public void testCheckPermissionForAuthorityNoPermission() throws AuthorizationException {
    final UserProfile userProfile = new UserProfile();
    final Authority userAuthority = new Authority();
    userAuthority.setId(1);
    userProfile.setAuthority(userAuthority);
    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    UserProfileUtil.checkPermissionAndAuthority(userProfile, userAuthority, TestPermission.TEST_PERMISSION_2);
  }

  @Test(expected = AuthorizationException.class)
  public void testCheckPermissionForAuthorityWrongAuthority() throws AuthorizationException {
    final UserProfile userProfile = new UserProfile();
    final Authority userAuthority = new Authority();
    userAuthority.setId(1);
    userProfile.setAuthority(userAuthority);
    final Authority differentAuthority = new Authority();
    differentAuthority.setId(2);
    userProfile.getPermissions().add(TestPermission.TEST_PERMISSION_1.getName());
    UserProfileUtil.checkPermissionAndAuthority(userProfile, differentAuthority, TestPermission.TEST_PERMISSION_1);
  }

}
