/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;

/**
 * Test class for {@link DepositionMarker}.
 */
public class DepositionMarkerTest {

  @Test
  public void testSelfCombine() {
    for (final DepositionMarkerType type : DepositionMarkerType.values()) {
      assertEquals("Self combine should be the same", type, type.combine(type));
    }
  }

  @Test
  public void testCombineOrder() {
    final DepositionMarkerType hcp = DepositionMarkerType.HIGHEST_CALCULATED_DEPOSITION;
    final DepositionMarkerType htd = DepositionMarkerType.HIGHEST_TOTAL_DEPOSITION;

    // Test combine of 2 base type.
    assertAndReverse(hcp, htd, DepositionMarkerType.HIGHEST_CALCULATED_AND_TOTAL);

    // Test combine of base type with combined, should give same result
    assertAndReverse(hcp, DepositionMarkerType.HIGHEST_CALCULATED_AND_TOTAL, DepositionMarkerType.HIGHEST_CALCULATED_AND_TOTAL);
    assertAndReverse(htd, DepositionMarkerType.HIGHEST_CALCULATED_AND_TOTAL, DepositionMarkerType.HIGHEST_CALCULATED_AND_TOTAL);
 }

  private void assertAndReverse(final DepositionMarkerType a, final DepositionMarkerType b, final DepositionMarkerType result) {
    assertEquals("Combine should be " + result, result, a.combine(b));
    assertEquals("Combine reverse should be " + result, result, b.combine(a));
  }
}
