/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;

/**
 * Base class for tests for SRM2 Emission Value classes (network or single).
 */
public class SRM2TestBase {

  protected static final EmissionValueKey KEY_2013_NH3 = new EmissionValueKey(2013, Substance.NH3);
  protected static final RoadEmissionCategory AUTO_BUS = createREC(RoadType.FREEWAY, VehicleType.AUTO_BUS, 120, KEY_2013_NH3, 2.0, 3.0);
  protected static final RoadEmissionCategory HEAVY_FREIGHT = createREC(RoadType.FREEWAY, VehicleType.HEAVY_FREIGHT, 120, KEY_2013_NH3, 4.0, 5.0);
  protected static final RoadEmissionCategory LIGHT_TRAFFIC = createREC(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, 120, KEY_2013_NH3, 4.0, 5.0);


  protected static RoadEmissionCategory createREC(final RoadType roadType, final VehicleType vt, final int maxSpeed, final EmissionValueKey evk,
      final double emissionFactor, final double stagnatedEmissionFactor) {
    final RoadEmissionCategory cat = new RoadEmissionCategory(roadType, vt, false, maxSpeed, null);

    cat.setEmissionFactor(evk, emissionFactor);
    cat.setStagnatedEmissionFactor(evk, stagnatedEmissionFactor);
    return cat;
  }
}
