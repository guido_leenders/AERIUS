/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

/**
 * Test class for {@link RoadEmissionCategories}.
 */
public class RoadEmissionCategoriesTest {
  private static final RoadEmissionCategory AB_100_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, false, 100, null);
  private static final String ONE_CATEGORY_ALWAYS = "With 1 category, this category should always be returned.";
  private static final RoadEmissionCategory URBAN_80_NOT_STRICT_C =
      new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 80, RoadSpeedType.C);
  private static final RoadEmissionCategory URBAN_80_NOT_STRICT_E =
      new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 80, RoadSpeedType.E);

  private static final RoadEmissionCategory LT_50_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 50, null);
  private static final RoadEmissionCategory LT_80_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 80, null);
  private static final RoadEmissionCategory LT_80_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 80, null);
  private static final RoadEmissionCategory LT_100_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 100,
      null);
  private static final RoadEmissionCategory LT_120_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 120,
      null);
  private static final RoadEmissionCategory LT_120_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 120, null);
  private static final RoadEmissionCategory LT_130_NOT_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 130,
      null);
  private static final RoadEmissionCategory LT_130_STRICT = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 130, null);

  private RoadEmissionCategories categories;

  @Before
  public void before() {
    categories = createData();
  }

  @Test
  public void testSpeedCategories() {
    final TreeSet<Integer> speeds = categories.getMaximumSpeedCategories(RoadType.FREEWAY);

    assertNotNull("No speed categories found.", speeds);
    assertEquals("Unexpected number of speed categories.", 5, speeds.size());

    for (final int speed : new int[] {50, 80, 100, 120, 130}) {
      assertEquals("Unexpected speed category for " + speed, Integer.valueOf(speed), speeds.pollFirst());
    }
  }

  /**
   * Tests input that is below the lowest speed category.
   */
  @Test
  public void testBelowCategory() {
    assertEquals("Lower strict should return strict lowest category", LT_50_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 30, null));
    assertEquals("Lower not strict should return not strict lowest category", LT_50_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 30, null));
  }

  /**
   * Tests input that matches speed category.
   */
  @Test
  public void testMatchCategory() {
    assertEquals("Match exact speed not strict", LT_100_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 100, null));
    assertEquals("Match exact speed strict", LT_80_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 80, null));
  }

  @Test
  public void testNoExactSpeedCategory() {
    assertEquals("No exact match not strict", LT_100_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 90, null));
    assertEquals("No exact match strict", LT_120_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 110, null));
    assertEquals("No exact match not strict higher", LT_130_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 130, null));
  }

  @Test
  public void testNoExactButLowerNonStictSpeedCategory() {
    assertEquals("No exact match not strict", LT_100_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 90, null));
  }

  @Test
  public void testSpeedAbove() {
    assertEquals("Find not strict but higher while looking for strict", LT_130_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 160, null));
    assertEquals("Find not strict but higher while looking for strict", LT_130_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 160, null));
    categories.add(LT_130_STRICT);
    assertEquals("Find strict when speed above lowest category below", LT_130_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 160, null));
  }

  @Test
  public void testFindClosestCategoryWithOne() {
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_C, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 30, RoadSpeedType.C));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_C, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 30, RoadSpeedType.C));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_C, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 90, RoadSpeedType.C));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_C, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 120, RoadSpeedType.C));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_C, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 135, RoadSpeedType.C));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_C, categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 160, RoadSpeedType.C));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_E,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 160, RoadSpeedType.E));
    assertEquals(ONE_CATEGORY_ALWAYS, URBAN_80_NOT_STRICT_E,
        categories.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 160, null));
  }

  @Test
  public void testFindNothing() {
    assertNull("Non urban road should not return a category", categories.findClosestCategory(RoadType.NON_URBAN_ROAD,
        VehicleType.LIGHT_TRAFFIC, true, 30, RoadSpeedType.E));
  }

  @Test
  public void testStrictInOnlyNonStrict() {
    assertEquals("In only non strict return closest if given is strict", AB_100_NOT_STRICT,
        categories.findClosestCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, true, 90, null));
  }

  private RoadEmissionCategories createData() {
    final RoadEmissionCategories categories = new RoadEmissionCategories();

    categories.add(URBAN_80_NOT_STRICT_C);
    categories.add(URBAN_80_NOT_STRICT_E);
    categories.add(new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, false, 80, null));
    categories.add(AB_100_NOT_STRICT);
    categories.add(LT_50_NOT_STRICT);
    categories.add(LT_80_STRICT);
    categories.add(LT_80_NOT_STRICT);
    categories.add(LT_100_NOT_STRICT);
    categories.add(LT_120_NOT_STRICT);
    categories.add(LT_120_STRICT);
    categories.add(LT_130_NOT_STRICT);
    return categories;
  }
}
