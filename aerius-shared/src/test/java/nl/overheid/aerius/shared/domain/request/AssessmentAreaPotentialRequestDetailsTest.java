/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.shared.domain.request.PotentialRequestDetails.AssessmentAreaPotentialRequestDetails;

/**
 * Test class for {@link AssessmentAreaPotentialRequestDetails}.
 */
@RunWith(Parameterized.class)
public class AssessmentAreaPotentialRequestDetailsTest {

  private static final double TEST_MELDING_THRESHOLD_VALUE = 0.05;
  private static final double TEST_DEFAULT_PERMIT_THRESHOLD_VALUE = 1;

  private static final Object[][] TEST_INPUT = {
      { "None", 0.01, 1.0, PotentialRequestType.NONE, },
      { "None, exact on threshold", 0.05, 1.0, PotentialRequestType.NONE, },
      { "None, lowered threshold", 0.01, 0.05, PotentialRequestType.NONE, },
      { "None, lowered threshold, equal to lowered value", 0.05, 0.05, PotentialRequestType.NONE, },
      { "Melding", 0.2, 1.0, PotentialRequestType.MELDING, },
      { "Melding, exact on threshold", 1.0, 1.0, PotentialRequestType.MELDING, },
      { "Melding, lowered threshold, equal to lowered value, but above drempelvalue", 0.3, 0.3, PotentialRequestType.MELDING, },
      { "Melding, lowered threshold, but below lowered", 0.2, 0.3, PotentialRequestType.MELDING, },
      { "Permit inside, insided lowered threshold", 0.2, 0.05, PotentialRequestType.PERMIT_INSIDE_MELDING_SPACE, },
      { "Permit inside, ", 0.5, 0.3, PotentialRequestType.PERMIT_INSIDE_MELDING_SPACE, },
      { "Permit", 1.2, 1.0, PotentialRequestType.PERMIT, },
      { "Permit, lowered threshold, normal permit", 1.2, 0.05, PotentialRequestType.PERMIT, },
      // next cases should never happen, as it violates the threshold ranges.
      { "Permit, even while threshold above permit threshold", 1.2, 2.0, PotentialRequestType.PERMIT, },
      { "None, even while threshold below none threshold, it should always be a none", 0.03, 0.01, PotentialRequestType.NONE, },
  };

  private final String description;
  private final double maxDemand;
  private final double permitThresholdValue;
  private final PotentialRequestType expectedType;

  public AssessmentAreaPotentialRequestDetailsTest(final String description, final double maxDemand, final double permitThresholdValue,
      final PotentialRequestType expectedType) {
    this.description = description;
    this.maxDemand = maxDemand;
    this.permitThresholdValue = permitThresholdValue;
    this.expectedType = expectedType;
  }

  @Parameters(name = "{0}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> objects = new ArrayList<>();
    for (final Object[] obj : TEST_INPUT) {
      objects.add(obj);
    }
    return objects;
  }

  @Test
  public void testGetPotentialRequestDetails() {
    final PotentialRequestDetails prd = new PotentialRequestDetails(TEST_MELDING_THRESHOLD_VALUE, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    final AssessmentAreaPotentialRequestDetails aapr = prd.addAssessmentAreaDetails(1, maxDemand, permitThresholdValue);
    assertSame(description, expectedType, aapr.getPotentialRequestType());
    assertSame(description, prd.getPotentialRequestType(), aapr.getPotentialRequestType());
  }

  @Test
  public void testCompareTo() {
    final PotentialRequestDetails prd = new PotentialRequestDetails(TEST_MELDING_THRESHOLD_VALUE, TEST_DEFAULT_PERMIT_THRESHOLD_VALUE);
    final AssessmentAreaPotentialRequestDetails aapr1 = prd.addAssessmentAreaDetails(1, maxDemand, permitThresholdValue);
    final AssessmentAreaPotentialRequestDetails aapr2 = prd.addAssessmentAreaDetails(1, maxDemand * 2, permitThresholdValue);
    assertTrue("1 always after 2", aapr1.compareTo(aapr2) < 0);
    assertEquals("1 should be same 1", 0, aapr1.compareTo(aapr1));
    assertTrue("2 always before 1", aapr2.compareTo(aapr1) > 0);
  }
}
