/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Test class for {@link CalculatedComparison}.
 */
public class CalculatedComparisonTest {

  @Test
  public void testGetCalculationId() {
    final CalculatedComparison cc = new CalculatedComparison();
    setOne(cc);
    assertEquals("Calculation id 1", 2L, cc.getCalculationId(20));
    assertEquals("Calculation non existing", 0, cc.getCalculationId(30));

    cc.setSourcesTwo(new EmissionSourceList());
    cc.getCalculationTwo().getSources().setId(30);
    cc.setCalculationIdTwo(3);
    assertEquals("Calculation id 1", 2L, cc.getCalculationId(20));
    assertEquals("Calculation id 2", 3L, cc.getCalculationId(30));
    assertEquals("Calculation non existing", 0, cc.getCalculationId(40));
  }

  @Test
  public void testSwap() {
    final CalculatedComparison cc = new CalculatedComparison();
    setOne(cc);
    setTwo(cc);
    assertEquals("Calculation id 1 before", 2L, cc.getCalculationId(20));
    assertEquals("Calculation id 2 before", 3L, cc.getCalculationId(30));
    assertEquals("name 1 before", "1", cc.getSourcesOne().getName());
    assertEquals("name 2 before", "2", cc.getSourcesTwo().getName());
    cc.swap();
    assertEquals("Calculation idOne after", 3, cc.getCalculationIdOne());
    assertEquals("Calculation idTwo after", 2, cc.getCalculationIdTwo());
    assertEquals("name 1 after", "2", cc.getSourcesOne().getName());
    assertEquals("name 2 after", "1", cc.getSourcesTwo().getName());
    assertEquals("Calculation id 1 after", 2L, cc.getCalculationId(20));
    assertEquals("Calculation id 2 after", 3L, cc.getCalculationId(30));
  }

  @Test
  public void testSync() {
    final CalculatedComparison cc = new CalculatedComparison();
    setOne(cc);
    setTwo(cc);
    cc.certify();
    assertTrue("CalculatedComparison not in sync", cc.isInSync());
    cc.swap();
    assertTrue("CalculatedComparison not in sync after swap", cc.isInSync());
    cc.setCalculationIdTwo(0);
    assertTrue("CalculatedComparison not in sync when calculation id changed", cc.isInSync());
  }

  private void setTwo(final CalculatedComparison cc) {
    cc.setSourcesTwo(new EmissionSourceList());
    cc.getCalculationTwo().getSources().setId(30);
    cc.getCalculationTwo().getSources().setName("2");
    cc.setCalculationIdTwo(3);
  }

  private void setOne(final CalculatedComparison cc) {
    cc.setSourcesOne(new EmissionSourceList());
    cc.getCalculationOne().getSources().setId(20);
    cc.getCalculationOne().getSources().setName("1");
    cc.setCalculationIdOne(2);
  }
}
