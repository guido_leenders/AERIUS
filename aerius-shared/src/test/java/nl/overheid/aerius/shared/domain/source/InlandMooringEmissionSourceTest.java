/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;

/**
 * Test class for {@link InlandMooringEmissionSource}.
 */
public class InlandMooringEmissionSourceTest {

  private static final EmissionValueKey KEY_NH3 = new EmissionValueKey(Substance.NH3);
  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);

  @Test
  public void testCopy() {
    final InlandMooringEmissionSource ship = createNewInlandMooring();
    final InlandMooringEmissionSource copy = ship.copy();
    assertNotSame("ship not same", ship, copy);
    assertNotNull("vessel copy not null", copy.getEmissionSubSources().get(1));
    assertNotSame("vessel not same", ship.getEmissionSubSources().get(1), copy.getEmissionSubSources().get(1));
    assertEquals("vessel count equal", ship.getEmissionSubSources().size(), copy.getEmissionSubSources().size());
    assertFalse("vessel routes not empty", copy.getEmissionSubSources().get(1).getRoutes().isEmpty());
    assertNotSame("vessel route not should be equal", ship.getEmissionSubSources().get(1).getRoutes().get(1).getRoute(),
        copy.getEmissionSubSources().get(1).getRoutes().get(1).getRoute());
    assertNotEquals("Midpoints shouldn't be at 0,0, test x", 0, MathUtil.round(copy.getInlandRoutes().get(1).getX()));
    assertNotEquals("Midpoints shouldn't be at 0,0, test y", 0, MathUtil.round(copy.getInlandRoutes().get(1).getY()));
    assertEquals("Midpoints X should be equal", ship.getInlandRoutes().get(1).getX(), copy.getInlandRoutes().get(1).getX(), 0.00001);
    assertEquals("Midpoints Y should be equal", ship.getInlandRoutes().get(1).getY(), copy.getInlandRoutes().get(1).getY(), 0.00001);
  }

  @Test
  public void testInlandRoute() {
    final InlandMooringEmissionSource ship = createNewInlandMooring();
    final InlandMooringEmissionSource copy = ship.copy();
    assertNotNull("vessel routes not null", copy.getEmissionSubSources().get(1).getRoutes());
    assertNotSame("vessel routes not equal", ship.getEmissionSubSources().get(1).getRoutes().get(0).getRoute(),
        copy.getEmissionSubSources().get(1).getRoutes().get(0).getRoute());
    assertNotNull("route not null", copy.getInlandRoutes().get(1));
    assertNotSame("route not equal", ship.getInlandRoutes().get(1), copy.getInlandRoutes().get(1));
    assertEquals("route id equal", ship.getInlandRoutes().get(ship.getInlandRoutes().size() - 1).getId(),
        copy.getInlandRoutes().get(ship.getInlandRoutes().size() - 1).getId());
    assertSame("vessel route in original equal 2 route", ship.getEmissionSubSources().get(1).getRoutes().get(0).getRoute(),
        ship.getInlandRoutes().get(2));
    assertSame("vessel route in copy equal 2 route ", copy.getEmissionSubSources().get(1).getRoutes().get(0).getRoute(),
        copy.getInlandRoutes().get(2));
    assertEquals("route count equal", ship.getInlandRoutes().size(), copy.getInlandRoutes().size());
    assertTrue("route should be used", copy.getInlandRoutes().get(1).isUsed());
  }

  @Test
  public void testStateHash() {
    final InlandMooringEmissionSource ship = createNewInlandMooring();
    final InlandMooringEmissionSource copy = ship.copy();
    assertEquals("StateHash should be equal", ship.getStateHash(), copy.getStateHash());
  }

  @Test
  public void testOnlyUsedBy() {
    final ShippingRoute route = createNewRoute(1);
    final ShippingRoute route2 = createNewRoute(2);
    final InlandMooringVesselGroup vesselGroup = createNewGroupAddRoute(1, route);
    assertTrue("Check route only used by vessels ",
        route.isOnlyUsedBy(vesselGroup.getRoutes().get(0)));
    assertFalse("Check route2 is not used by vessel ",
        route2.isOnlyUsedBy(vesselGroup.getRoutes().get(0)));
    final InlandMooringVesselGroup vesselGroup2 = createNewGroupAddRoute(2, route);
    assertFalse("Check route used by 2, check on vessels 1 ",
        route.isOnlyUsedBy(vesselGroup.getRoutes().get(0)));
    assertFalse("Check route used by 2, check on vessels 2 ",
        route.isOnlyUsedBy(vesselGroup2.getRoutes().get(0)));
  }

  @Test
  public void testDetach() {
    final ShippingRoute route = createNewRoute(1);
    final InlandMooringVesselGroup v = createNewGroupAddRoute(1, route);
    assertTrue("Route should be used before detach", route.isUsed());
    v.detachRoutes();
    assertFalse("Route should not be used after detach", route.isUsed());
  }

  /**
   * Test VesselGroupEmissionValuesValues copy. Specific for this is that the route in
   * the copy is the exact same route object as in the original.
   */
  @Test
  public void testCopyVesselGroupEmissionValuesValues() {
    final ShippingRoute route = createNewRoute(1);
    final InlandMooringVesselGroup v = createNewGroupAddRoute(1, route);
    final InlandMooringVesselGroup copy = v.copy();
    assertNotNull("vessel copy not null", copy);
    assertNotSame("vessel not equal", v, copy);
    assertNotNull("vessel route not null", copy.getRoutes());
    assertNotEquals("vessel route not equal", v.getRoutes(), copy.getRoutes());
    assertEquals("vessel route state hash equal", v.getStateHash(), copy.getStateHash());
    assertFalse("vessel route not be empty", copy.getRoutes().isEmpty());
    assertEquals("vessel maritime route equals statehash", v.getRoutes().get(0).getStateHash(), copy.getRoutes().get(0).getStateHash());
    assertEquals("vessel maritime route route are equal", v.getRoutes().get(0).getRoute(), copy.getRoutes().get(0).getRoute());
    assertEquals("vessel emissions", v.getEmission(KEY_NOX),
        copy.getEmission(KEY_NOX), 0.001);
  }

  @Test
  public void testInlandMooringRoute() {
    final ShippingRoute route = createNewRoute(1);
    final InlandMooringRoute imRoute = createIMRoute(1, route);
    assertEquals("Ships per year unladen", 12, imRoute.getShipsUnLadenPerYear(), 0000.1);
    imRoute.setPercentageLaden(30);
    assertEquals("Ships per year laden", 3.6, imRoute.getShipsLadenPerYear(), 0000.1);
    assertEquals("Ships per year unladen", 8.4, imRoute.getShipsUnLadenPerYear(), 0000.1);
  }

  /**
   * Test the SetEmissions functionality.
   * Assure it overwrites any existing emissions.
   */
  @Test
  public void testgetEmissionSubSources() {
    final ShippingRoute route = createNewRoute(1);
    final InlandMooringVesselGroup v = createNewGroupAddRoute(1, route);
    assertEquals("Shouldn't have emission at first.", 0.0, v.getEmission(KEY_NOX), 1E-3);
    final EmissionValues emissions = new EmissionValues();
    emissions.setEmission(KEY_NOX, 10.0);
    v.setEmissionValues(emissions);
    assertEquals("Emission after setting it.", emissions.getEmission(KEY_NOX), v.getEmission(KEY_NOX), 1E-3);
    final EmissionValues newEmissions = new EmissionValues();
    newEmissions.setEmission(KEY_NH3, 5.0);
    v.setEmissionValues(newEmissions);
    assertEquals("Emission after re-setting it for NOx.", 0.0, v.getEmission(KEY_NOX), 1E-3);
    assertEquals("Emission after re-setting it for NH3.", newEmissions.getEmission(KEY_NH3), v.getEmission(KEY_NH3), 1E-3);
    final InlandMooringVesselGroup copy = v.copy();
    assertEquals("Emission after copy for NOx.", v.getEmission(KEY_NOX), copy.getEmission(KEY_NOX), 1E-3);
    assertEquals("Emission after copy for NH3.", v.getEmission(KEY_NH3), copy.getEmission(KEY_NH3), 1E-3);

  }

  static InlandMooringEmissionSource createNewInlandMooring() {
    final InlandMooringEmissionSource sev = new InlandMooringEmissionSource();
    sev.getInlandRoutes().add(createNewRoute(1));
    sev.getInlandRoutes().add(createNewRoute(2));
    sev.getInlandRoutes().add(createNewRoute(3));
    sev.getEmissionSubSources().add(addRoutes(createNewGroup(1), sev.getInlandRoutes(), 0, 1));
    sev.getEmissionSubSources().add(addRoutes(createNewGroup(2), sev.getInlandRoutes(), 2, 0));
    sev.getEmissionSubSources().add(addRoutes(createNewGroup(3), sev.getInlandRoutes(), 1, 2, 0));
    return sev;
  }

  private static ShippingRoute createNewRoute(final int id) {
    final ShippingRoute route = new ShippingRoute();
    final int x1 = (id * 31) + 5;
    final int y1 = (id * 21) + 7;
    final int x2 = x1 * 11;
    final int y2 = y1 * 13;
    route.setGeometry(new WKTGeometry("LINE(" + x1 + " " + y1 + ", " + x2 + " " + y2 + ")"));
    route.setX(x1 + ((x2 - x1) / 2));
    route.setY(y1 + ((y2 - y1) / 2));
    route.setId(id);
    return route;
  }

  private static InlandShippingCategory createCategory(final int id) {
    final InlandShippingCategory c = new InlandShippingCategory();
    c.setName("ship name" + id);
    c.setDescription("ship description" + id);
    return c;
  }

  private static InlandMooringVesselGroup createNewGroupAddRoute(final int id, final ShippingRoute route) {
    return addRoute(createNewGroup(id), route);
  }

  private static InlandMooringVesselGroup createNewGroup(final int id) {
    final InlandMooringVesselGroup v = new InlandMooringVesselGroup();

    v.setId(id);
    v.setCategory(createCategory(id));
    v.setName("name" + id);
    v.setResidenceTime((id * 7) + 3);
    return v;
  }

  private static InlandMooringVesselGroup addRoutes(final InlandMooringVesselGroup v, final List<ShippingRoute> routes, final int... indexi) {
    for (int i = 0; i < indexi.length; i++) {
      addRoute(v, routes.get(indexi[i]));
    }
    return v;
  }

  private static InlandMooringVesselGroup addRoute(final InlandMooringVesselGroup v, final ShippingRoute route) {
    v.getRoutes().add(createIMRoute(v.getId(), route));
    return v;
  }

  private static InlandMooringRoute createIMRoute(final int id, final ShippingRoute route) {
    final InlandMooringRoute imr = new InlandMooringRoute();
    imr.setDirection((id % 2) == 1 ? NavigationDirection.ARRIVE : NavigationDirection.DEPART);
    imr.setShipMovements((id * 5) + 7, TimeUnit.YEAR);
    imr.setRoute(route);
    return imr;
  }
}
