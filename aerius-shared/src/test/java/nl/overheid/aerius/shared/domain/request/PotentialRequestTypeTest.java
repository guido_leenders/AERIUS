/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.request;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link PotentialRequestType}.
 */
public class PotentialRequestTypeTest {

  @Test
  public void testPotentialRequestTypeOrder() {
    assertEquals("Should be: MELDING > NONE", 1, PotentialRequestType.MELDING.compareTo(PotentialRequestType.NONE));
    assertEquals("Should be: PERMIT_INSIDE_MELDING_SPACE > MELDING", 1,
        PotentialRequestType.PERMIT_INSIDE_MELDING_SPACE.compareTo(PotentialRequestType.MELDING));
    assertEquals("Should be: PERMIT > PERMIT_INSIDE_MELDING_SPACE", 1,
        PotentialRequestType.PERMIT.compareTo(PotentialRequestType.PERMIT_INSIDE_MELDING_SPACE));
  }
}
