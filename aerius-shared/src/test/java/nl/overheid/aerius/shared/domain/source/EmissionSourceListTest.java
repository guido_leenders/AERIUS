/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;

public class EmissionSourceListTest {

  /**
   * Test if adding EmissionSources works.
   */
  @Test
  public void test1add() {
    final EmissionSourceList s = new EmissionSourceList();
    //Add first
    Assert.assertEquals("EmissionSource id", 1, add(s));
    Assert.assertEquals("Size emmissions", 1, s.size());
    Assert.assertEquals("EmissionSource id", 1, s.get(0).getId());
    //Add second
    Assert.assertEquals("EmissionSource id", 2, add(s));
    Assert.assertEquals("Size emmissions", 2, s.size());
    Assert.assertEquals("EmissionSource id", 2, s.get(1).getId());
    //Add third
    Assert.assertEquals("EmissionSource id", 3, add(s));
    Assert.assertEquals("Size emmissions", 3, s.size());
    Assert.assertEquals("EmissionSource id", 3, s.get(2).getId());
  }

  /**
   * Test if removing EmissionSource works.
   */
  @Test
  public void test2add() {
    final EmissionSourceList s = new EmissionSourceList();
    //Add 3 sources
    add(s);
    final GenericEmissionSource second = new GenericEmissionSource();
    s.add(second);
    add(s);
    s.remove(second);
    Assert.assertEquals("Size emmissions after removal", 2, s.size());
    Assert.assertEquals("EmissionSource id first", 1, s.get(0).getId());
    Assert.assertEquals("EmissionSource id second", 3, s.get(1).getId());
  }

  /**
   * Test if adding an EmissionSource works in an list where one was deleted.
   */
  @Test
  public void test3add() {
    final EmissionSourceList s = new EmissionSourceList();
    //Add 3 sources
    add(s);
    final GenericEmissionSource second = new GenericEmissionSource();
    s.add(second);
    add(s);
    s.remove(second);
    Assert.assertEquals("EmissionSource id new inserted", 2, add(s));
    Assert.assertEquals("Size emmissions", 3, s.size());
    Assert.assertEquals("EmissionSource id first", 1, s.get(0).getId());
    Assert.assertEquals("EmissionSource id second", 2, s.get(1).getId());
    Assert.assertEquals("EmissionSource id third", 3, s.get(2).getId());
  }

  /**
   * Test copy method
   */
  @Test
  public void testCopy() {
    final EmissionSourceList s = new EmissionSourceList();

    s.add(createEmissionSource(1, 100, 200, Substance.NH3, 9d));
    s.add(createEmissionSource(2, 200, 400, Substance.NH3, 18d));
    s.add(createEmissionSource(3, 300, 600, Substance.NH3, 36d));
    s.remove(1);
    final EmissionSourceList copy = s.copy();
    Assert.assertEquals("EmissionSourceList source count after copy", 2, copy.size());
    Assert.assertFalse("EmissionSourceList source not the same object", s == copy);
    Assert.assertFalse("EmissionSourceList source entry 0 not the same object", s.get(0) == copy.get(0));
    Assert.assertFalse("EmissionSourceList source entry 1 not the same object", s.get(1) == copy.get(1));
    Assert.assertEquals("EmissionSourceList source entry 1 id correct copied", 3, copy.get(1).getId());
  }

  /**
   * Test copy method with Maritime Mooring ships and maritime routes.
   */
  @Test
  public void testCopyMaritimeShips() {
    final EmissionSourceList s = new EmissionSourceList();
    final MaritimeMooringEmissionSource es = MaritimeMooringEmissionSourceTest.createNewMaritimeMooring();
    es.setId(1);
    es.setX(100);
    es.setY(200);
    s.add(es);
    for (final MooringMaritimeVesselGroup vg : es.getEmissionSubSources()) {
      for (final MaritimeRoute mr : vg.getMaritimeRoutes()) {
        if (!s.getMaritimeRoutes().contains(mr.getRoute())) {
          s.getMaritimeRoutes().add(mr.getRoute());
        }
      }
    }
    final EmissionSourceList copy = s.copy();
    Assert.assertEquals("EmissionSourceList routes count after copy", 3, copy.getMaritimeRoutes().size());
    Assert.assertNotSame("Check routes not same object", s.getMaritimeRoutes().get(0), copy.getMaritimeRoutes().get(0));
    Assert.assertEquals("Check route is connected to new copied route",
        ((MaritimeMooringEmissionSource) s.get(0)).getEmissionSubSources().get(0).getMaritimeRoutes().get(0).getRoute(),
        ((MaritimeMooringEmissionSource) copy.get(0)).getEmissionSubSources().get(0).getMaritimeRoutes().get(0).getRoute());
    copy.removeDestroy(es);
    for (final ShippingRoute route: copy.getMaritimeRoutes()) {
      Assert.assertFalse("Route should not be used anymore", route.isUsed());
    }
  }

  @Test
  public void testMergeSources() {
    final EmissionSourceList s = new EmissionSourceList();

    s.add(createEmissionSource(1, 100, 200, Substance.NH3, 9d));
    s.add(createEmissionSource(2, 200, 400, Substance.NH3, 18d));
    s.add(createEmissionSource(3, 300, 600, Substance.NH3, 36d));

    final EmissionSourceList mergeList = new EmissionSourceList();

    mergeList.add(createEmissionSource(1, 100, 200, Substance.NH3, 22d));
    mergeList.add(createEmissionSource(1, 200, 400, Substance.NOX, 44d));
    mergeList.add(createEmissionSource(1, 300, 700, Substance.NH3, 66d));

    //merge: 1: override value, 2: add new substance, 3: new entry
    final int overwritten = s.mergeSources(mergeList);
    Assert.assertEquals("EmissionSourceList source count after merge", 4, s.size());
    Assert.assertEquals("EmissionSourceList check override substance value after merge", 22d, s.get(0).getEmission(new EmissionValueKey(Substance.NH3)), 0.0001);
    Assert.assertEquals("EmissionSourceList check substance value on adding other substance after merge", 18d, s.get(1).getEmission(new EmissionValueKey(Substance.NH3)), 0.0001);
    Assert.assertEquals("EmissionSourceList check other substance value after merge", 44d, s.get(1).getEmission(new EmissionValueKey(Substance.NOX)), 0.0001);
    Assert.assertEquals("EmissionSourceList check other substance value after merge", 66d, s.get(3).getEmission(new EmissionValueKey(Substance.NH3)), 0.0001);
    Assert.assertEquals("EmissionSourceList overwritten sources", 2, overwritten);
  }

  @Test
  public void testRemoveSources() {
    final EmissionSourceList s = new EmissionSourceList();

    for (int i = 0; i < 20; i++) {
      s.add(createEmissionSource(i, 100 + i, 220 + i, Substance.NH3, i * 3.3d));
    }

    s.remove(new GenericEmissionSource(5, new Point()));
    Assert.assertEquals("EmissionSourceList check count emission sources 1 remove", 19, s.size());
    s.remove(new GenericEmissionSource(5, new Point()));
    Assert.assertEquals("EmissionSourceList check count emission sources 2nd remove", 19, s.size());
    s.remove(new GenericEmissionSource(14, new Point()));
    Assert.assertEquals("EmissionSourceList check count emission sources 3th remove", 18, s.size());
    s.remove(new GenericEmissionSource(1, new Point()));
    Assert.assertEquals("EmissionSourceList check count emission sources 4th remove", 17, s.size());
    s.remove(new GenericEmissionSource(19, new Point()));
    Assert.assertEquals("EmissionSourceList check count emission source 5th remove", 16, s.size());
    final GenericEmissionSource aes = createEmissionSource(1, 99, 99, Substance.NH3, 99d);

    s.add(aes);
    Assert.assertEquals("EmissionSourceList check correct insert", 1, aes.getId());
  }

  @Test
  public void testRemovingFromList() {
    final ArrayList<EmissionSourceList> lists = new ArrayList<EmissionSourceList>();
    final ArrayList<EmissionSourceList> forRemoval = new ArrayList<EmissionSourceList>();
    final EmissionSource es = new GenericEmissionSource(1, new Point());

    for (int i = 0; i < 20; i++) {
      final EmissionSourceList s = new EmissionSourceList();
      s.setId(i);
      s.add(es);
      lists.add(s);
      if ((i % 2) == 0) {
        forRemoval.add(s);
      }
    }

    Assert.assertEquals("Size of list before removing", 20, lists.size());
    lists.removeAll(forRemoval);
    Assert.assertEquals("Size of list after removing", 10, lists.size());
    for (final EmissionSourceList s : lists) {
      Assert.assertTrue("Id should be odd but was " + s.getId(), (s.getId() % 2) == 1);
    }
  }

  /**
   * test to get the total for NH3 or NOX
   */
  @Test
  public void testTotalEmission() {
    final EmissionSourceList s = new EmissionSourceList();
    s.add(createEmissionSource(1, 100, 200, Substance.NOXNH3, 100d));
    s.add(createEmissionSource(2, 100, 200, Substance.NH3, 210d));
    s.add(createEmissionSource(3, 100, 200, Substance.NOX, 200d));
    s.add(createEmissionSource(4, 100, 200, Substance.NOXNH3, 100d));
    s.add(createEmissionSource(4, 100, 200, Substance.NH3, 200d));
    s.add(createEmissionSource(4, 100, 200, Substance.NOX, 200d));
    final EmissionValueKey key = new EmissionValueKey(2020, Substance.NH3);
    final EmissionValueKey key2 = new EmissionValueKey(2020, Substance.NOX);
    Assert.assertTrue(s.getTotalEmission(key) == 410);
    Assert.assertTrue(s.getTotalEmission(key2) == 400);
  }

  private GenericEmissionSource createEmissionSource(final int id, final int x, final int y, final Substance s, final double emission) {
    final GenericEmissionSource es = new GenericEmissionSource(id, new Point(x, y));

    es.setEmission(s, emission);
    return es;
  }

  private int add(final EmissionSourceList list) {
    final EmissionSource t = new GenericEmissionSource();

    list.add(t);
    return t.getId();
  }
}
