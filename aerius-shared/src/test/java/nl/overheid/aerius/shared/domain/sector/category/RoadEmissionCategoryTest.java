/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for {@link RoadEmissionCategory}.
 */
public class RoadEmissionCategoryTest {

  @Test
  public void testEquals() {
    final RoadEmissionCategory rec = new RoadEmissionCategory();
    assertNotEquals("Equals on null should be false", rec, null);
    assertNotEquals("Equals on other object should be false", rec, new Object());
    assertEquals("Equals on same object should be true", rec, new RoadEmissionCategory());
  }

  @Test
  public void testEqualsType() {
    final RoadEmissionCategory rec = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, true, 100, null);
    assertFalse("EqualsType on null should be false", rec.equalsType(null));
    assertFalse("EqualsType: false on dfferent road and vehicle type",
        rec.equalsType(new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.HEAVY_FREIGHT, true, 100, null)));
    assertFalse("EqualsType: false on dfferent vehicle type",
        rec.equalsType(new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.HEAVY_FREIGHT, true, 100, null)));
    assertFalse("EqualsType: false on different road type",
        rec.equalsType(new RoadEmissionCategory(RoadType.URBAN_ROAD, VehicleType.AUTO_BUS, true, 100, null)));
    assertTrue("EqualsType: true on same road and vehicle, but different oter",
        rec.equalsType(new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.AUTO_BUS, false, 80, null)));
  }

  @Test
  public void testCompareToSame() {
    final RoadEmissionCategory base = createBase();
    assertEquals("Should compare to itself", base.compareTo(base), 0);
  }

  @Test
  public void testCompareToDifferentRoadType() {
    final RoadEmissionCategory base = createBase();
    final RoadEmissionCategory test = createBase();
    test.setRoadType(RoadType.NON_URBAN_ROAD);
    assertNotEquals("Should not be same on different road type", base.compareTo(test), 0);
  }

  @Test
  public void testCompareToDifferentVehicleType() {
    final RoadEmissionCategory base = createBase();
    final RoadEmissionCategory test = createBase();
    test.setVehicleType(VehicleType.AUTO_BUS);
    assertNotEquals("Should not be same on different vehicle type", base.compareTo(test), 0);
  }

  @Test
  public void testCompareToDifferentStrict() {
    final RoadEmissionCategory base = createBase();
    final RoadEmissionCategory test = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.NORMAL_FREIGHT, false, 100, null);
    assertTrue("Strict should be lower then non strict", base.compareTo(test) < 0);
  }

  @Test
  public void testCompareToStrictButLowerSpeed() {
    final RoadEmissionCategory base = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.NORMAL_FREIGHT, false, 100, null);
    final RoadEmissionCategory test = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.NORMAL_FREIGHT, true, 80, null);
    assertTrue("Strict should be lower then non strict", base.compareTo(test) > 0);
  }

  @Test
  public void testCompareToLowerSpeed() {
    final RoadEmissionCategory base = createBase();
    final RoadEmissionCategory test = createBase();
    test.setMaximumSpeed(80);
    assertTrue("Should return > 0 on lower speed", base.compareTo(test) > 0);
  }

  @Test
  public void testCompareToDifferentSpeedType() {
    final RoadEmissionCategory base = createBase();
    base.setSpeedType(RoadSpeedType.E);
    final RoadEmissionCategory test = createBase();
    test.setSpeedType(RoadSpeedType.C);
    assertEquals("Should return 0 for freeway, but with different speed type", 0, base.compareTo(test));
  }

  @Test
  public void testCompareToHigerSpeed() {
    final RoadEmissionCategory base = createBase();
    final RoadEmissionCategory test = createBase();
    test.setMaximumSpeed(120);
    assertTrue("Should return < 0 on higer speed", base.compareTo(test) < 0);
  }

  private RoadEmissionCategory createBase() {
    return new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.NORMAL_FREIGHT, true, 100, null);
  }
}
