/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;

/**
 * Test class for {@link MaritimeMooringEmissionSource}.
 */
public class MaritimeMooringEmissionSourceTest {

  private static final EmissionValueKey KEY_NH3 = new EmissionValueKey(Substance.NH3);
  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);

  @Test
  public void testCopy() {
    final MaritimeMooringEmissionSource ship = createNewMaritimeMooring();
    final MaritimeMooringEmissionSource copy = ship.copy();
    Assert.assertNotSame("ship not same", ship, copy);
    Assert.assertNotNull("vessel copy not null", copy.getEmissionSubSources().get(1));
    Assert.assertNotSame("vessel not same", ship.getEmissionSubSources().get(1), copy.getEmissionSubSources().get(1));
    Assert.assertEquals("vessel count equal", ship.getEmissionSubSources().size(), copy.getEmissionSubSources().size());
    Assert.assertFalse("vessel maritime route not empty", copy.getEmissionSubSources().get(1).getMaritimeRoutes().isEmpty());
    Assert.assertEquals("vessel maritime route should be equal (global)", ship.getEmissionSubSources().get(1).getMaritimeRoutes().get(0).getRoute(), copy.getEmissionSubSources().get(1).getMaritimeRoutes().get(0).getRoute());
    Assert.assertNotEquals("Midpoints shouldn't be at 0,0, test x", 0, MathUtil.round(copy.getInlandRoutes().get(1).getX()));
    Assert.assertNotEquals("Midpoints shouldn't be at 0,0, test y", 0, MathUtil.round(copy.getInlandRoutes().get(1).getY()));
    Assert.assertEquals("Midpoints X should be equal", ship.getInlandRoutes().get(1).getX(), copy.getInlandRoutes().get(1).getX(), 0.00001);
    Assert.assertEquals("Midpoints Y should be equal", ship.getInlandRoutes().get(1).getY(), copy.getInlandRoutes().get(1).getY(), 0.00001);
  }

  @Test
  public void testInlandRoute() {
    final MaritimeMooringEmissionSource ship = createNewMaritimeMooring();
    final MaritimeMooringEmissionSource copy = ship.copy();
    Assert.assertNotNull("vessel route not null", copy.getEmissionSubSources().get(1).getInlandRoute());
    Assert.assertNotSame("vessel route not equal", ship.getEmissionSubSources().get(1).getInlandRoute(), copy.getEmissionSubSources().get(1).getInlandRoute());
    Assert.assertNotNull("route not null", copy.getInlandRoutes().get(1));
    Assert.assertNotSame("route not equal", ship.getInlandRoutes().get(1), copy.getInlandRoutes().get(1));
    Assert.assertEquals("route id equal", ship.getInlandRoutes().get(ship.getInlandRoutes().size() - 1).getId(),
        copy.getInlandRoutes().get(ship.getInlandRoutes().size() - 1).getId());
    Assert.assertEquals("vessel route in original equal 2 inland route", ship.getEmissionSubSources().get(1).getInlandRoute(), ship.getInlandRoutes().get(1));
    Assert.assertEquals("vessel route in copy equal 2 inland route ", copy.getEmissionSubSources().get(1).getInlandRoute(), copy.getInlandRoutes().get(1));
    Assert.assertEquals("route count equal", ship.getInlandRoutes().size(), copy.getInlandRoutes().size());
    Assert.assertTrue("route used", copy.getInlandRoutes().get(1).isUsed());
  }

  @Test
  public void testStateHash() {
    final MaritimeMooringEmissionSource ship = createNewMaritimeMooring();
    final MaritimeMooringEmissionSource copy = ship.copy();
    Assert.assertEquals("StateHash should be equal", ship.getStateHash(), copy.getStateHash());
  }

  @Test
  public void testOnlyUsedBy() {
    for (final ShippingMovementType type : new ShippingMovementType[] {ShippingMovementType.INLAND, ShippingMovementType.MARITIME}) {
      final ShippingRoute route = createNewRoute(1);
      final ShippingRoute route2 = createNewRoute(2);
      final MooringMaritimeVesselGroup vesselGroup = createNewGroup(1, type, route);
      Assert.assertTrue("Check route only used by vessels " + type,
          route.isOnlyUsedBy(type == ShippingMovementType.INLAND ? vesselGroup : vesselGroup.getMaritimeRoutes().get(0)));
      Assert.assertFalse("Check route2 is not used by vessel " + type,
          route2.isOnlyUsedBy(type == ShippingMovementType.INLAND ? vesselGroup : vesselGroup.getMaritimeRoutes().get(0)));
      final MooringMaritimeVesselGroup vesselGroup2 = createNewGroup(2, type, route);
      Assert.assertFalse("Check route used by 2, check on vessels 1 " + type,
          route.isOnlyUsedBy(type == ShippingMovementType.INLAND ? vesselGroup : vesselGroup.getMaritimeRoutes().get(0)));
      Assert.assertFalse("Check route used by 2, check on vessels 2 " + type,
          route.isOnlyUsedBy(type == ShippingMovementType.INLAND ? vesselGroup : vesselGroup2.getMaritimeRoutes().get(0)));
    }
  }

  @Test
  public void testDetach() {
    final ShippingRoute route = createNewRoute(1);
    final MooringMaritimeVesselGroup v = createNewGroup(1, route);
    Assert.assertTrue("Route should be used before detach", route.isUsed());
    v.detachRoutes();
    Assert.assertFalse("Route should not be used after detach", route.isUsed());
  }

  /**
   * Test VesselGroupEmissionValuesValues copy. Specific for this is that the route in
   * the copy is the exact same route object as in the original.
   */
  @Test
  public void testCopyVesselGroupEmissionValuesValues() {
    final ShippingRoute route = createNewRoute(1);
    final MooringMaritimeVesselGroup v = createNewGroup(1, route);
    final MooringMaritimeVesselGroup copy = v.copy();
    Assert.assertNotNull("vessel copy not null", copy);
    Assert.assertNotSame("vessel not same", v, copy);
    Assert.assertNotNull("vessel route not null", copy.getInlandRoute());
    Assert.assertNotSame("vessel route not equal", v.getInlandRoute(), copy.getInlandRoute());
    Assert.assertEquals("vessel route state hash equal", v.getInlandRoute().getStateHash(), copy.getInlandRoute().getStateHash());
    Assert.assertFalse("vessel route not be empty", copy.getMaritimeRoutes().isEmpty());
    Assert.assertNotSame("vessel maritime route are equal", v.getMaritimeRoutes(), copy.getMaritimeRoutes());
    Assert.assertEquals("vessel maritime route equals statehash", v.getMaritimeRoutes().get(0).getStateHash(), copy.getMaritimeRoutes().get(0).getStateHash());
    Assert.assertEquals("vessel maritime route route are equal", v.getMaritimeRoutes().get(0).getRoute(), copy.getMaritimeRoutes().get(0).getRoute());
    Assert.assertEquals("vessel emissions", v.getEmission(KEY_NOX),
        copy.getEmission(KEY_NOX), 0.001);
  }

  /**
   * Test the SetEmissions functionality.
   * Assure it overwrites any existing emissions.
   * Assures a copy
   */
  @Test
  public void testGetEmissionSubSources() {
    final ShippingRoute route = createNewRoute(1);
    final MooringMaritimeVesselGroup v = createNewGroup(1, route);
    Assert.assertEquals("Shouldn't have emission at first.", 0.0, v.getEmission(KEY_NOX), 1E-3);
    final EmissionValues emissions = new EmissionValues();
    emissions.setEmission(KEY_NOX, 10.0);
    v.setEmissionValues(emissions);
    Assert.assertEquals("Emission after setting it.", emissions.getEmission(KEY_NOX), v.getEmission(KEY_NOX), 1E-3);
    final EmissionValues newEmissions = new EmissionValues();
    newEmissions.setEmission(KEY_NH3, 5.0);
    v.setEmissionValues(newEmissions);
    Assert.assertEquals("Emission after re-setting it for NOx.", 0.0, v.getEmission(KEY_NOX), 1E-3);
    Assert.assertEquals("Emission after re-setting it for NH3.", newEmissions.getEmission(KEY_NH3), v.getEmission(KEY_NH3), 1E-3);
    final MooringMaritimeVesselGroup copy = v.copy();
    Assert.assertEquals("Emission after copy for NOx.", v.getEmission(KEY_NOX), copy.getEmission(KEY_NOX), 1E-3);
    Assert.assertEquals("Emission after copy for NH3.", v.getEmission(KEY_NH3), copy.getEmission(KEY_NH3), 1E-3);
    newEmissions.setEmission(KEY_NH3, 10.0);
    Assert.assertEquals("Emission after re-setting it on original map for NH3.", newEmissions.getEmission(KEY_NH3), v.getEmission(KEY_NH3), 1E-3);
    Assert.assertEquals("Emission after re-setting it on original map for copy for NH3.", 5.0, copy.getEmission(KEY_NH3), 1E-3);
  }

  static MaritimeMooringEmissionSource createNewMaritimeMooring() {
    final MaritimeMooringEmissionSource sev = new MaritimeMooringEmissionSource();
    sev.getInlandRoutes().add(createNewRoute(1));
    sev.getInlandRoutes().add(createNewRoute(2));
    sev.getInlandRoutes().add(createNewRoute(3));
    sev.getEmissionSubSources().add(createNewGroup(1, sev.getInlandRoutes().get(0)));
    sev.getEmissionSubSources().add(createNewGroup(1, sev.getInlandRoutes().get(1)));
    sev.getEmissionSubSources().add(createNewGroup(2, sev.getInlandRoutes().get(2)));
    sev.getEmissionSubSources().add(createNewGroup(2, sev.getInlandRoutes().get(0)));
    sev.getEmissionSubSources().add(createNewGroup(3, sev.getInlandRoutes().get(1)));
    sev.getEmissionSubSources().add(createNewGroup(3, sev.getInlandRoutes().get(2)));
    sev.getEmissionSubSources().add(createNewGroup(3, sev.getInlandRoutes().get(0)));
    return sev;
  }

  private static ShippingRoute createNewRoute(final int id) {
    final ShippingRoute route = new ShippingRoute();
    final int x1 = (id * 31) + 5;
    final int y1 = (id * 21) + 7;
    final int x2 = x1 * 11;
    final int y2 = y1 * 13;
    route.setGeometry(new WKTGeometry("LINE(" + x1 + " " + y1 + ", " + x2 + " " + y2 + ")"));
    route.setX(x1 + ((x2 - x1) / 2));
    route.setY(y1 + ((y2 - y1) / 2));
    route.setId(id);
    return route;
  }

  private static MaritimeShippingCategory createCategory(final int id) {
    final MaritimeShippingCategory c = new MaritimeShippingCategory();
    c.setName("ship name" + id);
    c.setDescription("ship description" + id);
    return c;
  }

  private static MooringMaritimeVesselGroup createNewGroup(final int id, final ShippingRoute route) {
    return createNewGroup(id, null, route);
  }

  private static MooringMaritimeVesselGroup createNewGroup(final int id, final ShippingMovementType type, final ShippingRoute route) {
    final MooringMaritimeVesselGroup v = new MooringMaritimeVesselGroup();

    v.setId(id);
    v.setCategory(createCategory(id));
    v.setName("name" + id);
    v.setNumberOfShips((id * 13) + 5, TimeUnit.YEAR);
    v.setResidenceTime((id * 7) + 3);
    if (type == null || type == ShippingMovementType.INLAND) {
      v.setInlandRoute(route);
    }
    if (type == null || type == ShippingMovementType.MARITIME) {
      final MaritimeRoute mtr = new MaritimeRoute();
      mtr.setRoute(route);
      mtr.setShipMovements(v.getNumberOfShipsPerYear() * 2, v.getTimeUnit());
      v.getMaritimeRoutes().add(mtr);
    }
    return v;
  }
}
