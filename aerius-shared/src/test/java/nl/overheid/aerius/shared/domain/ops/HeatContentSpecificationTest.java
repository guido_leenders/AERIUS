/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for {@link HeatContentSpecification}.
 */
public class HeatContentSpecificationTest {

  @Test
  public void testHeatContentCalculation() {
    final HeatContentSpecification specification = new HeatContentSpecification();
    assertEquals(0.0, specification.getHeatContent(), 0.001);

    specification.setOutflowVelocity(5d);
    assertEquals(0.0, specification.getHeatContent(), 0.001);

    specification.setOutflowSurface(5d);
    assertEquals(0.0, specification.getHeatContent(), 0.001);

    specification.setEmissionTemperature(270d);
    assertEquals(8.2865361997125, specification.getHeatContent(), 0.001);
  }

  @Test
  public void testIsEmpty() {
    final HeatContentSpecification specification = new HeatContentSpecification();
    assertTrue("initial is empty", specification.isEmpty());
    specification.setEmissionTemperature(200);
    assertFalse("Not empty, higher Temperature", specification.isEmpty());
    specification.setEmissionTemperature(HeatContentSpecification.AVERAGE_SURROUNDING_TEMPERATURE);
    specification.setOutflowSurface(10);
    assertFalse("Not empty, OutflowSurface", specification.isEmpty());
    specification.setEmissionTemperature(HeatContentSpecification.AVERAGE_SURROUNDING_TEMPERATURE);
    specification.setOutflowSurface(00);
    specification.setOutflowVelocity(10);
    assertFalse("Not empty, OutflowVelocity", specification.isEmpty());
  }

  @Test
  public void testEquals() {
    final HeatContentSpecification s1 = new HeatContentSpecification();
    final HeatContentSpecification s2 = new HeatContentSpecification();
    assertNotEquals("Equals not same other object", s1, new Object());
    s1.setEmissionTemperature(1.0);
    assertNotEquals("Not equals EmissionTemperature", s1, s2);
    s2.setEmissionTemperature(1.0);
    assertEquals("Equals EmissionTemperature", s1, s2);
    s1.setOutflowSurface(1.0);
    assertNotEquals("Not equals OutflowSurface", s1, s2);
    s2.setOutflowSurface(1.0);
    assertEquals("Equals OutflowSurface", s1, s2);
    s1.setOutflowVelocity(1.0);
    assertNotEquals("Not equals OutflowVelocity", s1, s2);
    s2.setOutflowVelocity(1.0);
    assertEquals("Equals OutflowVelocity", s1, s2);
  }
}
