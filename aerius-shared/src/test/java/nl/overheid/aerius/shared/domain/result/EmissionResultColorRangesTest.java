/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link EmissionResultColorRanges}.
 */
public class EmissionResultColorRangesTest {

  @Test
  public void testGetColor0() {
    final String resString = EmissionResultColorRanges.DEPOSITION.getColor(0);
    assertEquals("Color deposition", "FFFDB3", resString);
  }

  @Test
  public void testGetColor1() {
    final String resString = EmissionResultColorRanges.DEPOSITION.getColor(10);
    assertEquals("Color deposition", "0093BD", resString);
  }

  @Test
  public void testGetColor2() {
    final String resString = EmissionResultColorRanges.DEPOSITION.getColor(11);
    assertEquals("Color deposition", "0093BD", resString);
  }

  @Test
  public void testGetColor3() {
    final String resString = EmissionResultColorRanges.DEPOSITION.getColor(200);
    assertEquals("Color deposition", "2A1612", resString);
  }

  @Test
  public void testGetColor4() {
    final String resString = EmissionResultColorRanges.CONCENTRATION.getColor(15);
    assertEquals("Color concentration", "A5CC80", resString);
  }

  @Test
  public void testGetColor5() {
    final String resString = EmissionResultColorRanges.CONCENTRATION.getColor(100);
    assertEquals("Color concentration", "110A05", resString);
  }

  @Test
  public void testGetColor6() {
    final String resString = EmissionResultColorRanges.NUM_EXCESS_DAY.getColor(8);
    assertEquals("Color number of excess days", "DD7777", resString);
  }

  @Test
  public void testGetColor7() {
    final String resString = EmissionResultColorRanges.NUM_EXCESS_DAY.getColor(100);
    assertEquals("Color number of excess days", "331111", resString);
  }
}
