/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for {@link CalculatorLimits}.
 */
public class CalculatorLimitsTest {

  @Test
  public void testMaxLineLength() {
    final CalculatorLimits cl = new CalculatorLimits();
    cl.setMaxLineLength(10);
    assertTrue("MaxLineLength", cl.isWithinLineLengthLimit(5));
    assertTrue("MaxLineLength", cl.isWithinLineLengthLimit(10));
    assertFalse("MaxLineLength", cl.isWithinLineLengthLimit(11));
    assertFalse("MaxLineLength", cl.isWithinLineLengthLimit(15));
  }

  @Test
  public void testMaxPolygonSurface() {
    final CalculatorLimits cl = new CalculatorLimits();
    cl.setMaxPolygonSurface(10);
    assertTrue("MaxPolygonSurface", cl.isWithinPolygonSurfaceLimit(5));
    assertTrue("MaxPolygonSurface", cl.isWithinPolygonSurfaceLimit(10));
    assertFalse("MaxPolygonSurface", cl.isWithinPolygonSurfaceLimit(11));
    assertFalse("MaxPolygonSurface", cl.isWithinPolygonSurfaceLimit(15));
  }

  @Test
  public void testMaxSources() {
    final CalculatorLimits cl = new CalculatorLimits();
    cl.setMaxSources(10);
    assertTrue("MaxSources", cl.isWithinMaxSourcesLimit(5));
    assertTrue("MaxSources", cl.isWithinMaxSourcesLimit(10));
    assertFalse("MaxSources", cl.isWithinMaxSourcesLimit(11));
    assertFalse("MaxSources", cl.isWithinMaxSourcesLimit(15));
  }
}
