/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.MachineryFuelType;

/**
 *
 */
public class OffRoadVehicleSpecificationTest {

  @Test
  public void testCalculateEmissionWithOperatingHours() {
    final OffRoadVehicleOperatingHoursSpecification specification = new OffRoadVehicleOperatingHoursSpecification();
    double emission = specification.getEmission();
    assertEquals("Emission NOx with operating hours: new", 0.0, emission, 0.0001);

    emission = specification.getEmission();
    specification.setPower(1281);
    assertEquals("Emission NOx with operating hours: only power", 0.0, emission, 0.0001);

    emission = specification.getEmission();
    specification.setLoad(80);
    assertEquals("Emission NOx with operating hours: only power and load", 0.0, emission, 0.0001);

    emission = specification.getEmission();
    specification.setUsage(3000);
    assertEquals("Emission NOx with operating hours: only power, load and usage", 0.0, emission, 0.0001);

    emission = specification.getEmission();
    specification.setEmissionFactor(8);
    emission = specification.getEmission();
    assertEquals("Emission NOx with operating hours: everything required set", 24595.2, emission, 0.0001);
  }

  @Test
  public void testCalculateEmissionWithFuelUsage() {
    final OffRoadVehicleConsumptionSpecification specification = new OffRoadVehicleConsumptionSpecification();
    double emission = specification.getEmission();
    assertEquals("Emission NOx with fuel usage: new", 0.0, emission, 0.0001);

    final MachineryFuelType fuelType = new MachineryFuelType();
    fuelType.setDensityFuel(0.84);
    specification.setFuelType(fuelType);

    emission = specification.getEmission();
    assertEquals("Emission NOx with fuel usage: with fuel", 0.0, emission, 0.0001);

    specification.setEnergyEfficiency(250);

    emission = specification.getEmission();
    assertEquals("Emission NOx with fuel usage: with fuel and energy efficiency ", 0.0, emission, 0.0001);

    specification.setEmissionFactor(8);
    emission = specification.getEmission();
    assertEquals("Emission NOx with fuel usage: with fuel, energy efficiency and emisison factor ", 0.0, emission, 0.0001);

    specification.setUsage(900000);
    emission = specification.getEmission();
    assertEquals("Emission NOx with fuel usage: everything required set", 24192, emission, 0.0001);
  }

}
