/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;

/**
 * Test class for {@link FarmEmissionSource}.
 */
public class FarmEmissionSourceTest {

  private static final int TEST_YEAR = 2013;

  @Test
  public void testGetEmission() {
    final FarmEmissionSource v = new FarmEmissionSource();

    final FarmLodgingStandardEmissions lodging1 = new FarmLodgingStandardEmissions();
    lodging1.setCategory(new FarmLodgingCategory(1, "", "", "", 2.0, null, null, false));
    lodging1.setAmount(200);
    v.getEmissionSubSources().add(lodging1);
    Assert.assertEquals("getEmission with year 0", v.getEmission(new EmissionValueKey(Substance.NH3)), 400.0, 0.0001);
    Assert.assertEquals("getEmission with year " + TEST_YEAR,
        v.getEmission(new EmissionValueKey(TEST_YEAR, Substance.NH3)), 400.0, 0.0001);
    Assert.assertEquals("getEmission other substance", v.getEmission(new EmissionValueKey(Substance.NOX)), 0.0, 0.0001);

    final FarmLodgingStandardEmissions lodging2 = new FarmLodgingStandardEmissions();
    lodging2.setCategory(new FarmLodgingCategory(2, "", "", "", 0.5, null, null, false));
    lodging2.setAmount(300);
    v.getEmissionSubSources().add(lodging2);
    Assert.assertEquals("getEmission 2 values with year 0", v.getEmission(new EmissionValueKey(Substance.NH3)), 550.0, 0.0001);
    Assert.assertEquals("getEmission 2 values with year " + TEST_YEAR,
        v.getEmission(new EmissionValueKey(TEST_YEAR, Substance.NH3)), 550.0, 0.0001);
    Assert.assertEquals("getEmission 2 values other substance", v.getEmission(new EmissionValueKey(Substance.NOX)), 0.0, 0.0001);
  }

  @Test
  public void testCopyAndHash() {
    final FarmEmissionSource v = new FarmEmissionSource();
    final FarmLodgingStandardEmissions lodging1 = new FarmLodgingStandardEmissions();
    lodging1.setCategory(new FarmLodgingCategory(1, "Rav 1", "", "", 2.0, null, null, false));
    lodging1.setAmount(200);
    v.getEmissionSubSources().add(lodging1);

    final FarmLodgingStandardEmissions lodging2 = new FarmLodgingStandardEmissions();
    lodging2.setCategory(new FarmLodgingCategory(2, "Rav 2", "", "", 0.5, null, null, false));
    lodging2.setAmount(300);
    v.getEmissionSubSources().add(lodging2);

    final FarmLodgingCustomEmissions lodging3 = new FarmLodgingCustomEmissions();
    lodging3.setAmount(300);
    lodging3.setDescription("Custom Rav1");
    lodging3.getEmissionFactors().setEmission(Substance.NH3, 0.5);
    v.getEmissionSubSources().add(lodging3);

    final EmissionSource copy = v.copy();
    Assert.assertEquals("StateHash must be the same for Farm Emission Values", v.getStateHash(), copy.getStateHash());
  }
}
