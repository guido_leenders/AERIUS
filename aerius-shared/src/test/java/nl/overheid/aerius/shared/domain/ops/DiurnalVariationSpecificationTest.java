/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

/**
 * Test class for {@link DiurnalVariationSpecification}.
 */
public class DiurnalVariationSpecificationTest {

  private static final String CODE = "code";

  @Test
  public void testEquals() {
    final DiurnalVariationSpecification d1 = new DiurnalVariationSpecification();
    final DiurnalVariationSpecification d2 = new DiurnalVariationSpecification();
    assertNotEquals("Equals not same other object", d1, new Object());
    d1.setCode(CODE);
    assertNotEquals("Not equals Code", d1, d2);
    d2.setCode(CODE);
    assertEquals("Equals Code", d1, d2);
    d1.setId(1);
    assertNotEquals("Not equals id", d1, d2);
    d2.setId(1);
    assertEquals("Equals id", d1, d2);
    d1.setName("Name");
    assertEquals("Equals even if name different", d1, d2);
    d1.setDescription("Description");
    assertEquals("Equals even if description different", d1, d2);
  }
}
