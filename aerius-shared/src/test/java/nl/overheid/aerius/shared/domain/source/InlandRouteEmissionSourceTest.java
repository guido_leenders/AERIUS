/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;

/**
 * Test class for {@link InlandRouteEmissionSource}.
 */
public class InlandRouteEmissionSourceTest {

  @Test
  public void testCopy() {
    final InlandRouteEmissionSource ship = createNew();
    final EmissionSource copy = ship.copy();
    Assert.assertTrue("ship not equal", copy instanceof InlandRouteEmissionSource);
    final InlandRouteEmissionSource copiedShip = (InlandRouteEmissionSource) copy;
    Assert.assertNotSame("ship not same", ship, copiedShip);
    Assert.assertNotNull("vessel copy not null", copiedShip.getEmissionSubSources().get(1));
    Assert.assertNotSame("vessel not same", ship.getEmissionSubSources().get(1), copiedShip.getEmissionSubSources().get(1));
    Assert.assertEquals("vessel count equal", ship.getEmissionSubSources().size(), copiedShip.getEmissionSubSources().size());
  }

  /**
   * Test VesselGroupEmissionValues copy. Specific for this is that the route in
   * the copy is the exact same route object as in the original.
   */
  @Test
  public void testCopyVesselGroupEmissionValues() {
    final RouteInlandVesselGroup v = createNewGroup(1);
    final RouteInlandVesselGroup copy = v.copy();
    Assert.assertNotNull("vessel copy not null", copy);
    Assert.assertNotSame("vessel not same", v, copy);
    Assert.assertEquals("vessel emissions", v.getEmission(new EmissionValueKey(Substance.NOX)),
        copy.getEmission(new EmissionValueKey(Substance.NOX)), 0.001);
  }

  private InlandRouteEmissionSource createNew() {
    final InlandRouteEmissionSource sev = new InlandRouteEmissionSource();
    sev.getEmissionSubSources().add(createNewGroup(1));
    sev.getEmissionSubSources().add(createNewGroup(1));
    sev.getEmissionSubSources().add(createNewGroup(2));
    sev.getEmissionSubSources().add(createNewGroup(2));
    sev.getEmissionSubSources().add(createNewGroup(3));
    sev.getEmissionSubSources().add(createNewGroup(3));
    sev.getEmissionSubSources().add(createNewGroup(3));
    return sev;
  }

  private InlandShippingCategory createCategory(final int id) {
    final InlandShippingCategory c = new InlandShippingCategory();
    c.setName("ship name" + id);

    return c;
  }

  private RouteInlandVesselGroup createNewGroup(final int id) {
    final RouteInlandVesselGroup v = new RouteInlandVesselGroup();

    v.setId(id);
    v.setCategory(createCategory(id));
    v.setName("name" + id);
    v.setNumberOfShipsAtoB(id * 13 + 5, TimeUnit.YEAR);
    return v;
  }
}
