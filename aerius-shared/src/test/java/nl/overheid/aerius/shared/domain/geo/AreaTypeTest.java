/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;


/**
 * Test class for {@link AreaTypeTest}
 */
public class AreaTypeTest {

  @Test
  public void testToDatabaseObjectName() {
    assertNull("Test HABITAT_TYPE", AreaType.HABITAT_TYPE.toDatabaseObjectName());
    assertNull("Test HABITAT_TYPE plural", AreaType.HABITAT_TYPE.toDatabasePluralObjectName());
    assertEquals("Test PROVINCE_AREA", "province_area", AreaType.PROVINCE_AREA.toDatabaseObjectName());
    assertEquals("Test PROVINCE_AREA plural", "province_areas", AreaType.PROVINCE_AREA.toDatabasePluralObjectName());
  }

  @Test
  public void testFromDatabaseObjectName() {
    assertNull("Test null", AreaType.fromDatabaseObjectName(null));
    assertNull("Test illegal input", AreaType.fromDatabaseObjectName("illegal"));
    assertSame("Test HABITAT_TYPE", AreaType.HABITAT_TYPE, AreaType.fromDatabaseObjectName("habitat_type"));
    assertSame("Test HABITAT_TYPE plural", AreaType.HABITAT_TYPE, AreaType.fromDatabaseObjectName("habitat_types"));
  }
}
