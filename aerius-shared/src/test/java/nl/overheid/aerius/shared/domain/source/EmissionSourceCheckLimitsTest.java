/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link EmissionSourceCheckLimits}.
 */
public class EmissionSourceCheckLimitsTest {

  @Test
  public void testAllOke() throws AeriusException {
    final CalculatorLimits limits = new CalculatorLimits();
    limits.setMaxSources(20);
    limits.setMaxLineLength(1000);
    limits.setMaxPolygonSurface(1000);

    final EmissionSourceList esl = new EmissionSourceList();
    final GenericEmissionSource es1 = new GenericEmissionSource();
    es1.setGeometry(new WKTGeometry("POLYGON()", 200));
    esl.add(es1);
    final GenericEmissionSource es2 = new GenericEmissionSource();
    esl.add(es2);
    es2.setGeometry(new WKTGeometry("LINE()", 200));
    // add SRM2NetworkEmissionSource
    final SRM2NetworkEmissionSource network = new SRM2NetworkEmissionSource();
    network.setGeometry(new WKTGeometry("POINT()", 1));
    esl.add(network);
    final SRM2EmissionSource network1 = new SRM2EmissionSource();
    network1.setGeometry(new WKTGeometry("LINE()", 200));
    network.getEmissionSources().add(network1);
    final SRM2EmissionSource network2 = new SRM2EmissionSource();
    network2.setGeometry(new WKTGeometry("LINE()", 200));
    network.getEmissionSources().add(network2);

    EmissionSourceCheckLimits.check(esl, limits);
  }

  @Test(expected = AeriusException.class)
  public void testCheckSize() throws AeriusException {
    final CalculatorLimits limits = new CalculatorLimits();
    limits.setMaxSources(2);
    final EmissionSourceList esl = new EmissionSourceList();
    esl.add(new GenericEmissionSource());
    esl.add(new GenericEmissionSource());
    esl.add(new GenericEmissionSource());
    final SRM2NetworkEmissionSource network = new SRM2NetworkEmissionSource();
    esl.add(network);
    network.getEmissionSources().add(new SRM2EmissionSource());
    network.getEmissionSources().add(new SRM2EmissionSource());
    network.getEmissionSources().add(new SRM2EmissionSource());
    network.getEmissionSources().add(new SRM2EmissionSource());
    try {
      EmissionSourceCheckLimits.check(esl, limits);
    } catch (final AeriusException e) {
      assertEquals("Number of sources counted", "7", e.getArgs()[1]);
      assertEquals("MaxSources", Reason.LIMIT_SOURCES_EXCEEDED, e.getReason());
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testCheckLineLimit() throws AeriusException {
    final CalculatorLimits limits = new CalculatorLimits();
    limits.setMaxSources(2);
    limits.setMaxLineLength(100);
    final EmissionSourceList esl = new EmissionSourceList();
    final EmissionSource es = new GenericEmissionSource();
    esl.add(es);
    es.setGeometry(new WKTGeometry("LINE()", 200));
    try {
      EmissionSourceCheckLimits.check(esl, limits);
    } catch (final AeriusException e) {
      assertEquals("MaxLineLength", Reason.LIMIT_LINE_LENGTH_EXCEEDED, e.getReason());
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testPolygonSurface() throws AeriusException {
    final CalculatorLimits limits = new CalculatorLimits();
    limits.setMaxSources(2);
    limits.setMaxPolygonSurface(100);
    final EmissionSourceList esl = new EmissionSourceList();
    final EmissionSource es = new GenericEmissionSource();
    esl.add(es);
    es.setGeometry(new WKTGeometry("POLYGON()", MathUtil.round(200 * SharedConstants.M2_TO_HA)));
    try {
      EmissionSourceCheckLimits.check(esl, limits);
    } catch (final AeriusException e) {
      assertEquals("MaxPolygonSurface", Reason.LIMIT_POLYGON_SURFACE_EXCEEDED, e.getReason());
      throw e;
    }
  }
}
