/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAnimalCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;

/**
 * Test class for {@link FarmLodgingStandardEmissions}.
 * Tests various combinations of lodging systems. The so called 'RAV stacking'.
 * Also test the constrained emission factors when a system's reduction irt its traditional system surpasses 70%.
 */
public class FarmLodgingStandardEmissionsTest {

  private static final double EPSILON = 1E-5;
  private static final EmissionValueKey EMISSION_VALUE_KEY = new EmissionValueKey(Substance.NH3);
  private static final FarmAnimalCategory ANIMAL_CAT_1 = new FarmAnimalCategory(1, "D1.1", "D1.1", "");
  private static final FarmAnimalCategory ANIMAL_CAT_2 = new FarmAnimalCategory(2, "D3", "D3", "");
  private static final FarmAnimalCategory ANIMAL_CAT_3 = new FarmAnimalCategory(3, "A4", "A4", "");

  private static int lastId;

  @Test
  public void testEmissionFactorsBase() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();

    assertResults(f, new double[] {3000, 3000, 3000, 3000, 3000, 3.0, 3.0, });
  }

  @Test
  public void testEmissionFactorsOneAdditional() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmAdditionalLodgingSystem sa1 = appendTestObjectAdditional1(f);

    assertResults(f, new double[] {3000, 3000, 4000, 4000, 4000, 3.0, 3.0, });
    assertUpToAdditional(f, sa1, 4000);
    assertEmissionSystem(sa1, 1000);
  }

  @Test
  public void testEmissionFactorsTwoAdditional() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmAdditionalLodgingSystem sa1 = appendTestObjectAdditional1(f);
    final FarmAdditionalLodgingSystem sa2 = appendTestObjectAdditional2(f);

    assertResults(f, new double[] {3000, 3000, 4250, 4250, 4250, 3.0, 3.0, });
    assertUpToAdditional(f, sa1, 4000);
    assertUpToAdditional(f, sa2, 4250);
    assertEmissionSystem(sa2, 250);
  }

  @Test
  public void testEmissionFactorsThreeAdditionalWithTwoSameCategory() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmAdditionalLodgingSystem sa1 = appendTestObjectAdditional1(f);
    final FarmAdditionalLodgingSystem sa2 = appendTestObjectAdditional2(f);
    final FarmAdditionalLodgingSystem sa3 = createFarmAdditionalLodgingSystem(f, sa2.getCategory(), 200);

    assertResults(f, new double[] {3000, 3000, 4750, 4750, 4750, 3.0, 3.0, });
    assertUpToAdditional(f, sa1, 4000);
    assertUpToAdditional(f, sa2, 4250);
    assertUpToAdditional(f, sa3, 4750);
    assertEmissionSystem(sa3, 500);
  }

  @Test
  public void testEmissionFactorsOneReductive() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmReductiveLodgingSystem sr1 = appendTestObjectReductive1(f);

    assertResults(f, new double[] {3000, 3000, 3000, 1500, 1500, 3.0, 3.0, });
    assertUpToReductive(f, sr1, 1500);
    assertEmissionFactorReductive(sr1, 0.5);
  }

  @Test
  public void testEmissionFactorsTwoReductive() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmReductiveLodgingSystem sr1 = appendTestObjectReductive1(f);
    final FarmReductiveLodgingSystem sr2 = appendTestObjectReductive2(f);

    assertResults(f, new double[] {3000, 3000, 3000, 75, 75, 3.0, 3.0, });
    assertUpToReductive(f, sr1, 1500);
    assertUpToReductive(f, sr2, 75);
    assertEmissionFactorReductive(sr2, 0.05);
  }

  @Test
  public void testEmissionFactorsThreeReductiveWithTwoSameCategory() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmReductiveLodgingSystem sr1 = appendTestObjectReductive1(f);
    final FarmReductiveLodgingSystem sr2 = createFarmReductiveLodgingSystem(f, sr1.getCategory());
    final FarmReductiveLodgingSystem sr3 = appendTestObjectReductive2(f);

    assertResults(f, new double[] {3000, 3000, 3000, 37.5, 37.5, 3.0, 3.0, });
    assertUpToReductive(f, sr1, 1500);
    assertUpToReductive(f, sr2, 750);
    assertUpToReductive(f, sr3, 37.5);
    assertEmissionFactorReductive(sr2, 0.5);
  }

  @Test
  public void testEmissionFactorsMixed() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    final FarmAdditionalLodgingSystem sa1 = appendTestObjectAdditional1(f);
    final FarmAdditionalLodgingSystem sa2 = appendTestObjectAdditional2(f);
    final FarmReductiveLodgingSystem sr1 = appendTestObjectReductive1(f);
    final FarmReductiveLodgingSystem sr2 = appendTestObjectReductive2(f);
    appendTestObjectFodder1(f);
    appendTestObjectFodder2(f);

    assertResults(f, new double[] {3000, 3000, 4250, 106.25, 58.4375, 3.0, 3.0, });
    assertUpToAdditional(f, sa1, 4000);
    assertUpToAdditional(f, sa2, 4250);
    assertUpToReductive(f, sr1, 2125);
    assertUpToReductive(f, sr2, 106.25);
    assertEquals(0.45, f.getReductionFactorCombinedFodderMeasures(EMISSION_VALUE_KEY), EPSILON);
  }

  @Test
  public void testEmissionFactorsOneReductiveConstrained() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseConstrained();
    final FarmReductiveLodgingSystem sr1 = appendTestObjectReductive1(f);

    assertResults(f, new double[] {1500, 1000, 1500, 750, 750, 1.5, 1.0, });
    assertUpToReductive(f, sr1, 750);

    sr1.getCategory().setScrubber(false);

    assertResults(f, new double[] {1000, 1000, 1000, 500, 500, 1.0, 1.0, });
    assertUpToReductive(f, sr1, 500);

    sr1.getCategory().setScrubber(true);
    f.getCategory().setScrubber(true);
    assertResults(f, new double[] {1000, 1000, 1000, 500, 500, 1.0, 1.0, });
    assertUpToReductive(f, sr1, 500);
  }

  @Test
  public void testEmissionFactorsOneAdditionalConstrained() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseConstrained();
    final FarmAdditionalLodgingSystem sa1 = appendTestObjectAdditional1(f);

    assertResults(f, new double[] {1500, 1000, 2500, 2500, 2500, 1.5, 1.0, });
    assertUpToAdditional(f, sa1, 2500);

    sa1.getCategory().setScrubber(false);
    assertResults(f, new double[] {1000, 1000, 2000, 2000, 2000, 1.0, 1.0, });

    sa1.getCategory().setScrubber(true);
    f.getCategory().setScrubber(true);
    assertResults(f, new double[] {1000, 1000, 2000, 2000, 2000, 1.0, 1.0, });
  }

  @Test
  public void testEmissionFactorsMixedConstrained() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseConstrained();
    final FarmAdditionalLodgingSystem sa1 = appendTestObjectAdditional1(f);
    final FarmAdditionalLodgingSystem sa2 = appendTestObjectAdditional2(f);
    final FarmReductiveLodgingSystem sr1 = appendTestObjectReductive1(f);
    final FarmReductiveLodgingSystem sr2 = appendTestObjectReductive2(f);

    // Constrained because reduction > 70%
    assertResults(f, new double[] {1500, 1000, 2750, 68.75, 68.75, 1.5, 1.0, });
    assertUpToAdditional(f, sa1, 2500);
    assertUpToAdditional(f, sa2, 2750);
    assertUpToReductive(f, sr1, 1375);
    assertUpToReductive(f, sr2, 68.75);

    // Not constrained because parent is a scrubber
    f.getCategory().setScrubber(true);
    assertResults(f, new double[] {1000, 1000, 2250, 56.25, 56.25, 1.0, 1.0, });

    // Constrained, parent is not a scrubber and at least one stacked one system is a scrubber
    f.getCategory().setScrubber(false);
    sa1.getCategory().setScrubber(false);
    sa2.getCategory().setScrubber(false);
    sr1.getCategory().setScrubber(false);
    sr2.getCategory().setScrubber(true);
    assertResults(f, new double[] {1500, 1000, 2750, 68.75, 68.75, 1.5, 1.0, });

    // Not constrained, nothing stacked one is a scrubber
    sr2.getCategory().setScrubber(false);
    assertResults(f, new double[] {1000, 1000, 2250, 56.25, 56.25, 1.0, 1.0, });
  }

  @Test
  public void testEmissionFactorsOneFodder() {
    final FarmLodgingStandardEmissions f1 = createTestObjectBaseUnconstrained();
    final FarmFodderMeasure fm1 = appendTestObjectFodder1(f1);
    assertResults(f1, new double[] {3000, 3000, 3000, 3000, 1950, 3.0, 3.0, });
    assertCellarFloor(fm1, new double[] {1 - 0.16, 1 - 0.40, 1 - 0.35, });
    assertCombinedFodder(f1, 0.35);

    // Test another
    final FarmLodgingStandardEmissions f2 = createTestObjectBaseUnconstrained();
    final FarmFodderMeasure fm2 = appendTestObjectFodder2(f2);

    assertResults(f2, new double[] {3000, 3000, 3000, 3000, 2400, 3.0, 3.0, });
    assertCellarFloor(fm2, new double[] {1 - 0.2, 1 - 0.2, 1 - 0.2, });
    assertCombinedFodder(f2, 0.2);

    // And another, animal category D1.1. Make sure combined reduction factor is not 40%, which is what
    // the formula would normally calculate for multiple measures.
    final FarmLodgingStandardEmissions f3 = createFarmLodgingStandardEmissions(createFarmLodgingCategory(null, 3.0, ANIMAL_CAT_1, null), 1000);
    final FarmFodderMeasure fm3 = appendTestObjectFodder1(f3);

    assertResults(f3, new double[] {3000, 3000, 3000, 3000, 1950, 3.0, 3.0, });
    assertCellarFloor(fm3, new double[] {1 - 0.16, 1 - 0.40, 1 - 0.35, });
    assertCombinedFodder(f3, 0.35);
  }

  @Test
  public void testEmissionFactorsThreeFodderWithTwoSameCategory() {
    final FarmLodgingStandardEmissions f = createTestObjectBaseUnconstrained();
    appendTestObjectFodder1(f);
    appendTestObjectFodder2(f);
    appendTestObjectFodder1(f);

    assertResults(f, new double[] {3000, 3000, 3000, 3000, 1050, 3.0, 3.0, });
    assertCombinedFodder(f, 0.65);
  }

  @Test
  public void testEmissionFactorsFodderInvalid() {
    final FarmLodgingCategory cat = createFarmLodgingCategory(null, 3.0, ANIMAL_CAT_3, null);
    final FarmLodgingStandardEmissions f = createFarmLodgingStandardEmissions(cat, 1000);
    final FarmFodderMeasure fm1 = appendTestObjectFodder1(f);

    // One measure: still accept the reduction percentages
    assertResults(f, new double[] {3000, 3000, 3000, 3000, 1950, 3.0, 3.0, });
    assertCellarFloor(fm1, new double[] {1 - 0.16, 1 - 0.4, 1 - 0.35, });
    assertCombinedFodder(f, 0.35);

    appendTestObjectFodder2(f);

    // Two measures: don't know what to do, so reduction percentage is 0%
    assertResults(f, new double[] {3000, 3000, 3000, 3000, 3000, 3.0, 3.0, });
    assertCombinedFodder(f, 0);
  }

  private static void assertResults(final FarmLodgingStandardEmissions f, final double[] expected) {
    assertEquals("Test flat emission", expected[0], f.getFlatEmission(EMISSION_VALUE_KEY), EPSILON);
    assertEquals("Test UnconstrainedFlat emission", expected[1], f.getUnconstrainedFlatEmission(EMISSION_VALUE_KEY), EPSILON);
    assertEquals("Test UpToAdditional emission", expected[2], f.getEmissionUpToAdditional(EMISSION_VALUE_KEY, null), EPSILON);
    assertEquals("Test UpToReductive emission", expected[3], f.getEmissionUpToReductive(EMISSION_VALUE_KEY, null), EPSILON);
    assertEquals("Test Emission", expected[4], f.getEmission(EMISSION_VALUE_KEY), EPSILON);
    assertEquals("Test Emssion Factor", expected[5], f.getEmissionFactor(EMISSION_VALUE_KEY), EPSILON);
    assertEquals("Test Unconstrained Emission Factor", expected[6], f.getUnconstrainedEmissionFactor(EMISSION_VALUE_KEY), EPSILON);
  }

  private static void assertUpToAdditional(final FarmLodgingStandardEmissions f, final FarmAdditionalLodgingSystem sa1, final double expected) {
    assertEquals("Test UpToAdditional for specific system", expected, f.getEmissionUpToAdditional(EMISSION_VALUE_KEY, sa1), EPSILON);
  }

  private static void assertUpToReductive(final FarmLodgingStandardEmissions f, final FarmReductiveLodgingSystem sr1, final double expected) {
    assertEquals("Test UpToReductive for specific system", expected, f.getEmissionUpToReductive(EMISSION_VALUE_KEY, sr1), EPSILON);
  }

  private static void assertEmissionSystem(final FarmAdditionalLodgingSystem sa1, final double expected) {
    assertEquals("Test emission additional system", expected, sa1.getEmission(EMISSION_VALUE_KEY), EPSILON);
  }

  private static void assertEmissionFactorReductive(final FarmReductiveLodgingSystem sr1, final double expected) {
    assertEquals("Test EmissionFactorReductive", expected, sr1.getEmissionFactor(EMISSION_VALUE_KEY), EPSILON);
  }

  private static void assertCellarFloor(final FarmFodderMeasure fm, final double[] expected) {
    assertEquals("Test factor floor", expected[0], fm.getEmissionFactorFloor(EMISSION_VALUE_KEY), EPSILON);
    assertEquals("Test factor cellar", expected[1], fm.getEmissionFactorCellar(EMISSION_VALUE_KEY), EPSILON);
    assertEquals("Test factor total", expected[2], fm.getEmissionFactorTotal(EMISSION_VALUE_KEY), EPSILON);
  }

  private static void assertCombinedFodder(final FarmLodgingStandardEmissions f, final double expected) {
    assertEquals("Test CombinedFodderMeasures", expected, f.getReductionFactorCombinedFodderMeasures(EMISSION_VALUE_KEY), EPSILON);
  }

  private static FarmLodgingCategory createFarmLodgingCategory(final String code, final double ef, final FarmAnimalCategory animal,
      final FarmLodgingCategory traditional) {
    return new FarmLodgingCategory(lastId++, code, code, "", ef, animal, traditional, false);
  }

  private static FarmAdditionalLodgingSystemCategory createFarmAdditionalLodgingSystemCategory(final String code, final double ef) {
    return new FarmAdditionalLodgingSystemCategory(lastId++, code, code, "", ef, true);
  }

  private static FarmReductiveLodgingSystemCategory createFarmReductiveLodgingSystemCategory(final String code, final double ef) {
    return new FarmReductiveLodgingSystemCategory(lastId++, code, code, "", ef, true);
  }

  private static FarmLodgingFodderMeasureCategory createFarmLodgingFodderMeasureCategory(final String code, final double efFloor,
      final double efCellar, final double efTotal) {
    final FarmLodgingFodderMeasureCategory cat = new FarmLodgingFodderMeasureCategory(lastId++, code, code, "", efFloor, efCellar, efTotal);
    cat.addAmmoniaProportion(ANIMAL_CAT_1, 0.1, 0.9);
    cat.addAmmoniaProportion(ANIMAL_CAT_2, 0.3, 0.7);
    return cat;
  }

  private static FarmLodgingStandardEmissions createFarmLodgingStandardEmissions(final FarmLodgingCategory cat, final int amount) {
    final FarmLodgingStandardEmissions f = new FarmLodgingStandardEmissions();
    f.setAmount(amount);
    f.setCategory(cat);
    return f;
  }

  private static FarmAdditionalLodgingSystem createFarmAdditionalLodgingSystem(final FarmLodgingStandardEmissions f,
      final FarmAdditionalLodgingSystemCategory cat, final int amount) {
    final FarmAdditionalLodgingSystem sa = new FarmAdditionalLodgingSystem();
    sa.setCategory(cat);
    sa.setAmount(amount);
    f.getLodgingAdditional().add(sa);
    return sa;
  }

  private static FarmReductiveLodgingSystem createFarmReductiveLodgingSystem(final FarmLodgingStandardEmissions f,
      final FarmReductiveLodgingSystemCategory cat) {
    final FarmReductiveLodgingSystem sr = new FarmReductiveLodgingSystem();
    sr.setCategory(cat);
    f.getLodgingReductive().add(sr);
    return sr;
  }

  private static FarmFodderMeasure createFarmFodderMeasure(final FarmLodgingStandardEmissions f, final FarmLodgingFodderMeasureCategory cat) {
    final FarmFodderMeasure fm = new FarmFodderMeasure();
    fm.setCategory(cat);
    f.getFodderMeasures().add(fm);
    return fm;
  }

  private static FarmLodgingStandardEmissions createTestObjectBaseUnconstrained() {
    final FarmLodgingCategory t1 = createFarmLodgingCategory(null, 5.0, null, null);
    final FarmLodgingCategory c1 = createFarmLodgingCategory(null, 3.0, ANIMAL_CAT_2, t1);
    return createFarmLodgingStandardEmissions(c1, 1000);
  }

  private static FarmLodgingStandardEmissions createTestObjectBaseConstrained() {
    final FarmLodgingCategory t1 = createFarmLodgingCategory(null, 5.0, null, null);
    final FarmLodgingCategory c1 = createFarmLodgingCategory(null, 1.0, ANIMAL_CAT_2, t1);
    return createFarmLodgingStandardEmissions(c1, 1000);
  }

  private static FarmAdditionalLodgingSystem appendTestObjectAdditional1(final FarmLodgingStandardEmissions f) {
    final FarmAdditionalLodgingSystemCategory a1 = createFarmAdditionalLodgingSystemCategory(null, 2.0);
    return createFarmAdditionalLodgingSystem(f, a1, 500);
  }

  private static FarmAdditionalLodgingSystem appendTestObjectAdditional2(final FarmLodgingStandardEmissions f) {
    final FarmAdditionalLodgingSystemCategory a2 = createFarmAdditionalLodgingSystemCategory(null, 2.5);
    return createFarmAdditionalLodgingSystem(f, a2, 100);
  }

  private static FarmReductiveLodgingSystem appendTestObjectReductive1(final FarmLodgingStandardEmissions f) {
    final FarmReductiveLodgingSystemCategory r1 = createFarmReductiveLodgingSystemCategory(null, 0.5);
    return createFarmReductiveLodgingSystem(f, r1);
  }

  private static FarmReductiveLodgingSystem appendTestObjectReductive2(final FarmLodgingStandardEmissions f) {
    final FarmReductiveLodgingSystemCategory r2 = createFarmReductiveLodgingSystemCategory(null, 0.95);
    return createFarmReductiveLodgingSystem(f, r2);
  }

  private static FarmFodderMeasure appendTestObjectFodder1(final FarmLodgingStandardEmissions f) {
    final FarmLodgingFodderMeasureCategory r1 = createFarmLodgingFodderMeasureCategory(null, 0.16, 0.4, 0.35);
    return createFarmFodderMeasure(f, r1);
  }

  private static FarmFodderMeasure appendTestObjectFodder2(final FarmLodgingStandardEmissions f) {
    final FarmLodgingFodderMeasureCategory r2 = createFarmLodgingFodderMeasureCategory(null, 0.2, 0.2, 0.2);
    return createFarmFodderMeasure(f, r2);
  }
}
