/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Test class for {@link EmissionSource} object.
 */
public class EmissionSourceTest {

  @Test
  public void testEmissionSourceNewEmissionSource() {
    final GenericEmissionSource es = new  GenericEmissionSource();

    Assert.assertEquals("Default undefined sector", Sector.SECTOR_UNDEFINED, es.getSector());
  }

  @Test
  public void testCopy() {
    final GenericEmissionSource es = new  GenericEmissionSource(1, new Point(2, 3));
    final GenericEmissionSource copy = es.copy();

    Assert.assertNotSame("Diffenent object", es, copy);
    Assert.assertEquals("same id", es.getId(), copy.getId());
    Assert.assertEquals("same Point x", es.getX(), copy.getX(), 0.0001);
    Assert.assertEquals("same Point y", es.getY(), copy.getY(), 0.0001);
  }

  @Test
  public void testEmissionSourceEquals() {
    Assert.assertEquals("Equals", true, new GenericEmissionSource(0, new Point(0, 0)).equals(new GenericEmissionSource(0, new Point(0, 0))));
    Assert.assertEquals("No equal id", false, new GenericEmissionSource(0, new Point(0, 0)).equals(new GenericEmissionSource(1, new Point(0, 0))));
    Assert.assertEquals("No equal id", false, new GenericEmissionSource(2, new Point(0, 0)).equals(new GenericEmissionSource(0, new Point(0, 0))));
    Assert.assertEquals("No equal id", false, new GenericEmissionSource(3, new Point(0, 0)).equals(new GenericEmissionSource(4, new Point(0, 0))));
    Assert.assertEquals("Equal, no 0 id", true, new GenericEmissionSource(5, new Point(1, 2)).equals(new GenericEmissionSource(5, new Point(1, 2))));
    Assert.assertEquals("id 0, No equal point", false, new GenericEmissionSource(0, new Point(1, 2)).equals(new GenericEmissionSource(0, new Point(2, 1))));
    Assert.assertEquals("id 1 0, No equal point", false, new GenericEmissionSource(0, new Point(1, 2)).equals(new GenericEmissionSource(5, new Point(2, 1))));
    Assert.assertEquals("id 2 0, No equal point", false, new GenericEmissionSource(5, new Point(1, 2)).equals(new GenericEmissionSource(0, new Point(2, 1))));
    Assert.assertEquals("Equal id, equal point", true, new GenericEmissionSource(5, new Point(1, 2)).equals(new GenericEmissionSource(5, new Point(1, 2))));
    Assert.assertEquals("No equals different point", false, new GenericEmissionSource(0, new Point(0, 0)).equals(new Point(0, 0)));
    Assert.assertEquals("No equals different other", false, new GenericEmissionSource(0, new Point(0, 0)).equals(Boolean.TRUE));
  }
}
