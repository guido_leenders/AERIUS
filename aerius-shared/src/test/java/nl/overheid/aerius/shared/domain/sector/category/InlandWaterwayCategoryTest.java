/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;

/**
 * Test class for InlandWaterwayCategory.
 */
public class InlandWaterwayCategoryTest {

  @Test
  public void testEquals() {
    final InlandWaterwayCategory c1 = new InlandWaterwayCategory();
    final InlandWaterwayCategory c2 = new InlandWaterwayCategory();
    c1.setId(1);
    c1.setDirections(new ArrayList<>(Arrays.asList(new WaterwayDirection[] {WaterwayDirection.IRRELEVANT, })));
    c2.setId(1);
    c2.setDirections(new ArrayList<>(Arrays.asList(new WaterwayDirection[] {WaterwayDirection.IRRELEVANT, })));
    assertEquals("categories equal", c1, c2);
    c2.setId(2);
    assertNotEquals("categories id not equal", c1, c2);
    c2.setId(1);
    c2.setDirections(new ArrayList<>(Arrays.asList(new WaterwayDirection[] {WaterwayDirection.DOWNSTREAM, })));
    assertNotEquals("categories directions not equal", c1, c2);
  }

  @Test
  public void testHashcode() {
    final InlandWaterwayCategory c1 = new InlandWaterwayCategory();
    final InlandWaterwayCategory c2 = new InlandWaterwayCategory();
    c1.setId(1);
    c1.setDirections(new ArrayList<>(Arrays.asList(new WaterwayDirection[] {WaterwayDirection.IRRELEVANT, })));
    c2.setId(1);
    c2.setDirections(new ArrayList<>(Arrays.asList(new WaterwayDirection[] {WaterwayDirection.IRRELEVANT, })));
    assertEquals("hashcode equal", c1.hashCode(), c2.hashCode());
    c2.setId(2);
    assertNotEquals("hashcode on id not equal", c1.hashCode(), c2.hashCode());
    c2.setId(1);
    c2.setDirections(new ArrayList<>(Arrays.asList(new WaterwayDirection[] {WaterwayDirection.DOWNSTREAM, })));
    assertNotEquals("hashCode on directions not equal", c1.hashCode(), c2.hashCode());
  }
}
