/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ConsecutiveNamedListTest {

  private static class TestHasId implements HasId, HasState {
    private int id;

    public TestHasId() {
      //no id set
    }

    public TestHasId(final int id) {
      this.id = id;
    }

    @Override
    public int getId() {
      return id;
    }

    @Override
    public void setId(final int id) {
      this.id = id;
    }

    @Override
    public boolean equals(final Object obj) {
      return obj instanceof HasId ? ((HasId) obj).getId() == id : false;
    }

    @Override
    public int hashCode() {
      return id;
    }

    @Override
    public int getStateHash() {
      return 0;
    }
  }

  @Test
  public void test1AddGet() {
    final ConsecutiveNamedList<TestHasId> list = new ConsecutiveNamedList<TestHasId>(1);
    //Add first
    assertEquals("TestHasId added id not 1", 1, addNewToList(list));
    assertEquals("Size emmission not 1s", 1, list.size());
    assertEquals("TestHasId id not 1", 1, list.get(0).getId());
    //Add second
    assertEquals("TestHasId added id not 2", 2, addNewToList(list));
    assertEquals("Size emmissions not 2", 2, list.size());
    assertEquals("TestHasId id not 2", 2, list.get(1).getId());
    //Add third
    assertEquals("TestHasId added id not 3", 3, addNewToList(list));
    assertEquals("Size emmissions not 3", 3, list.size());
    assertEquals("TestHasId id not 3", 3, list.get(2).getId());
  }

  /**
   * Test if removing TestHasId worklist.
   */
  @Test
  public void test2AddTestHasId() {
    final ConsecutiveNamedList<TestHasId> list = new ConsecutiveNamedList<TestHasId>(1);
    //Add 3 sources
    addNewToList(list);
    final TestHasId second = new TestHasId();
    list.add(second);
    addNewToList(list);
    list.remove(second);
    assertEquals("Size emmissions after removal", 2, list.size());
    assertEquals("TestHasId id first", 1, list.get(0).getId());
    assertEquals("TestHasId id second", 3, list.get(1).getId());
  }

  /**
   * Test if adding an TestHasId works in an list where one was deleted.
   */
  @Test
  public void test3AddTestHasId() {
    final ConsecutiveNamedList<TestHasId> list = new ConsecutiveNamedList<TestHasId>(1);
    //Add 3 sources
    addNewToList(list);
    final TestHasId second = new TestHasId();
    list.add(second);
    addNewToList(list);
    list.remove(second);
    assertEquals("TestHasId id new inserted", 2, addNewToList(list));
    assertEquals("Size emmissions", 3, list.size());
    assertEquals("TestHasId id first", 1, list.get(0).getId());
    assertEquals("TestHasId id second", 2, list.get(1).getId());
    assertEquals("TestHasId id third", 3, list.get(2).getId());
  }

  @Test
  public void testAdd() {
    for (int offset = 0; offset < 3; offset++) {
      for (int i = 0; i < 6; i++) {
        final ConsecutiveNamedList<TestHasId> list = getList(0, 6, offset, i);
        final TestHasId test = new TestHasId();

        list.add(test);
        assertEquals("Find idx(offset=" + offset + "):" + (i + offset), i + offset, test.getId());
      }
    }
  }

  @Test
  public void testAddAll() {
    final List<TestHasId> toAddList = new ArrayList<>();
    for (int i = 100; i > 0; i--) {
      toAddList.add(new TestHasId(i));
    }
    final ConsecutiveNamedList<TestHasId> list = new ConsecutiveNamedList<>();
    list.addAll(toAddList);
    assertFalse("Old list not empty", toAddList.isEmpty());
    assertEquals("Test if all are added", 100, list.size());
    assertEquals("Id third must be 2", 2, list.get(2).getId());
  }

  @Test
  public void testFindIndexById() {
    final ConsecutiveNamedList<TestHasId> list = new ConsecutiveNamedList<TestHasId>(1);
    addNewToList(list);
    assertNull("Item below index", list.getById(0));
    assertTrue("Item found", list.getById(1) instanceof TestHasId && list.getById(1).getId() == 1);
    assertNull("Item beyond index", list.getById(2));
  }

  @Test
  public void testFindById2() {
    final ConsecutiveNamedList<TestHasId> s = new ConsecutiveNamedList<TestHasId>();
    for (int i = 0; i < 4; i++) {
      addNewToList(s);
    }
    assertEquals("Test by id", s.getById(3).getId(), 3);
    s.remove(1);
    assertEquals("Test by id after one removed", s.getById(3).getId(), 3);
    assertNull("Item beyond index after remove", s.getById(5));
  }


  @Test
  public void testFindSources() {
    final ConsecutiveNamedList<TestHasId> s = new ConsecutiveNamedList<TestHasId>();

    for (int i = 4; i < 7; i++) {
      s.add(new TestHasId(i));
    }
    assertEquals("Test by id", s.getById(1).getId(), 1);
    s.remove(0);
    assertEquals("Test by id after remove", s.getById(1).getId(), 1);
    s.remove(0);
    assertEquals("Test by id after item actually deleted", s.getById(1), null);
  }

  @Test
  public void testReplace() {
    final ConsecutiveNamedList<TestHasId> s = new ConsecutiveNamedList<TestHasId>();
    for (int i = 0; i < 4; i++) {
      s.add(new TestHasId(i));
    }

    final TestHasId someObject = new TestHasId(1);
    s.replace(someObject);
    assertSame("Test object replaced", someObject, s.get(1));
    assertEquals("List still same size", 4, s.size());
  }

  @Test
  public void testRemoveSources() {
    final ConsecutiveNamedList<TestHasId> s = new ConsecutiveNamedList<TestHasId>();

    for (int i = 0; i < 20; i++) {
      s.add(new TestHasId());
    }

    s.remove(new TestHasId(5));
    assertEquals("ConsecutiveNamedList check count items not 19", 19, s.size());
    s.remove(new TestHasId(14));
    assertEquals("ConsecutiveNamedList check count items not 18", 18, s.size());
    s.remove(new TestHasId(1));
    assertEquals("ConsecutiveNamedList check count items not 17", 17, s.size());
    s.remove(new TestHasId(19));
    assertEquals("ConsecutiveNamedList check count items not 16", 16, s.size());
    final TestHasId aes = new TestHasId();

    s.add(aes);
    assertEquals("ConsecutiveNamedList check correct insert", 1, aes.getId());
  }

  private ConsecutiveNamedList<TestHasId> getList(final int start, final int end, final int offset, final int missing) {
    final ConsecutiveNamedList<TestHasId> list = new ConsecutiveNamedList<TestHasId>(offset);

    for (int i = start; i < end - 1; i++) {
      list.add(new TestHasId());
    }
    int i = start;
    for (int id = start; id < end; id++) {
      if (!(missing == id)) {
        list.get(i++).setId(id + offset);
      }
    }
    return list;
  }

  private int addNewToList(final ConsecutiveNamedList<TestHasId> list) {
    final TestHasId t = new TestHasId();

    list.add(t);
    return t.getId();
  }
}
