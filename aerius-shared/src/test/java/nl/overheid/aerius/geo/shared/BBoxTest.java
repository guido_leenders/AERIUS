/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo.shared;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Test class for {@link BBox}.
 */
public class BBoxTest {

  /**
   * Bounding box of the Netherlands receptor grid.
   */
  private static final BBox RECEPTOR_BBOX = new BBox(3604, 296800, 287959, 629300);

  @Test
  public void testIsPointWithinBoundingBox() {
    AeriusPoint point = new AeriusPoint();
    Assert.assertFalse("New point shouldn't be", RECEPTOR_BBOX.isPointWithinBoundingBox(point));
    point.setX(135583);
    Assert.assertFalse("Both coords need to be set", RECEPTOR_BBOX.isPointWithinBoundingBox(point));
    point = new AeriusPoint();
    point.setY(455387);
    Assert.assertFalse("Both coords need to be set", RECEPTOR_BBOX.isPointWithinBoundingBox(point));
    point.setX(135583);
    Assert.assertTrue("Both set", RECEPTOR_BBOX.isPointWithinBoundingBox(point));

    point.setX(RECEPTOR_BBOX.getMinX());
    point.setY(RECEPTOR_BBOX.getMinY());
    Assert.assertTrue("minimum should be included", RECEPTOR_BBOX.isPointWithinBoundingBox(point));
    point.setX(RECEPTOR_BBOX.getMaxX());
    point.setY(RECEPTOR_BBOX.getMaxY());
    Assert.assertTrue("maximum should be included", RECEPTOR_BBOX.isPointWithinBoundingBox(point));
  }
}
