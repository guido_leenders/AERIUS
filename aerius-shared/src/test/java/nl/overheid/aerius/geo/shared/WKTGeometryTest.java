/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo.shared;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link WKTGeometry}.
 */
public class WKTGeometryTest {

  @Test
  public void testGetPoints() {
    assertEquals("Test line points", "1 2,3 4", new WKTGeometry("LINESTRING(1 2,3 4)").getPoints());
    assertEquals("Test polygon points", "1 2,3 4", new WKTGeometry("POLYGON((1 2,3 4))").getPoints());
    assertEquals("Test line points invalid", null, new WKTGeometry("L1 2,3 4)").getPoints());
    assertEquals("Test line points empty", null, new WKTGeometry().getPoints());
  }
}
