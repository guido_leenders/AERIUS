/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo.shared;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Properties for a specific Layer. The properties contain additional configuration settings not available in other data.
 */
public class LayerProps implements Serializable {

  private static final long serialVersionUID = -3628001539944366369L;

  private int id;

  /**
   * The reference name of the layer.
   */
  private String name;

  /**
   * The minimum scale at which data will be visible. If scale becomes larger nothing will be shown. If the value is < 0 it means there is no min
   * scale.
   */
  private float minScale;

  /**
   * The maximum scale for which data is available. If scale comes below this value nothing will be shown. If the value is < 0 it means there is no
   * max scale.
   */
  private float maxScale;

  /**
   * The begin year at which data starts becoming visible. If not set, but end year is, everything &lt;= end year is allowed.
   */
  private Integer beginYear;

  /**
   * The end year after which data ceases to exist (we've already passed 2012), so year specific to this layer.
   * If not set, but begin year is, everything &gt;= begin year is allowed.
   */
  private Integer endYear;

  /**
   * Set if a custom legend is present. If null the legend via the capabilities information should be queried.
   */
  private Legend legend;

  /**
   * Opacity setting of the layer.
   */
  private float opacity = 1.0F;

  /**
   * Boolean if the layer is should be shown or just shown in the category list but not enabled.
   */
  private boolean enabled;

  /**
   * The title of the layer. Used to set a custom title on external layers.
   */
  private String title;


  private String attribution;

  /**
   * Returns true if the year is within the range of the begin and end year.
   *
   * @param year year to check
   * @return true if year within range
   */
  public boolean containsYear(final int year) {
    return (beginYear == null ? true : year >= beginYear) && (endYear == null ? true : year <= endYear);
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public float getMinScale() {
    return minScale;
  }

  public float getMaxScale() {
    return maxScale;
  }

  public Legend getLegend() {
    return legend;
  }

  public float getOpacity() {
    return opacity;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public String getAttribution() {
    return attribution;
  }

  public void setAttribution(final String attribution) {
    this.attribution = attribution;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public void setMinScale(final float minScale) {
    this.minScale = minScale;
  }

  public void setMaxScale(final float maxScale) {
    this.maxScale = maxScale;
  }

  public void setBeginYear(final Integer beginYear) {
    this.beginYear = beginYear;
  }

  public void setEndYear(final Integer endYear) {
    this.endYear = endYear;
  }

  public void setLegend(final Legend legend) {
    this.legend = legend;
  }

  public void setOpacity(final float opacity) {
    this.opacity = opacity;
  }

  public void setEnabled(final boolean enable) {
    this.enabled = enable;
  }

  @Override
  public String toString() {
    return "LayerProps [id=" + id + ", name=" + name + ", minScale=" + minScale + ", maxScale=" + maxScale + ", beginYear=" + beginYear
        + ", endYear=" + endYear + ", legend=" + legend + ", opacity=" + opacity + ", enabled=" + enabled + "]";
  }

  /**
   * Data class for properties of a layer legend with only a name.
   */
  public interface Legend {}

  /**
   * Data class for legend with a list of legend names with for each name
   * a colors.
   */
  public static class ColorRangesLegend implements Legend, Serializable {
    private static final long serialVersionUID = 179088884735859758L;

    private String[] legendNames;
    private String[] colors;
    private boolean hexagon;

    // Needed for GWT.
    public ColorRangesLegend() {}

    public ColorRangesLegend(final String[] legendNames, final String[] colors) {
      this(legendNames, colors, false);
    }

    public ColorRangesLegend(final String[] legendNames, final String[] colors, final boolean hexagon) {
      this.hexagon = hexagon;
      assert legendNames.length == colors.length : "Legend names list different size as colors list for " + this;
      this.legendNames = legendNames;
      this.colors = colors;
    }

    public String[] getColors() {
      return colors;
    }

    public String[] getLegendNames() {
      return legendNames;
    }

    /**
     * Returns if the legend items should be displayed as a hexagon picture.
     * @return true if should be shown as hexagon
     */
    public boolean isHexagon() {
      return hexagon;
    }

    public void setColors(final String[] colors) {
      this.colors = colors;
    }

    public void setLegendNames(final String[] legendNames) {
      this.legendNames = legendNames;
    }

    /**
     * Get the amount of legend items.
     * @return size
     */
    public int size() {
      return legendNames.length;
    }

    @Override
    public String toString() {
      return "ColorRangesLegend [legendNames=" + Arrays.toString(legendNames) + ", colors=" + Arrays.toString(colors)
          + ",hexagon:" + (hexagon ? "true" : "false") + "]" + super.toString();
    }
  }

  /**
   * Data class for a legend with a image representing the legend.
   */
  public static class ImageLegend implements Legend, Serializable {
    private static final long serialVersionUID = 3095825941472442382L;

    private String imageUrl;

    public ImageLegend() {}

    public ImageLegend(final String imageUrl) {
      this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
      return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
      this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
      return "ImageLegend [imageUrl=" + imageUrl + "]" + super.toString();
    }
  }

  /**
   * TODO There's currently no good way to localize this stuff. Ideally it _is_ done on the server, because
   * layers are all coming from the server (or otherwise externally) and the client need not know anything about
   * them. All i18n stuff is stored either in the database, or the web server. This object is typically
   * constructed in _common_, which only has access to the database i18n, I'm reluctant to put legend
   * information in there (layer config isn't in there either, yet)
   */
  public static class TextLegend implements Legend, Serializable {
    private static final long serialVersionUID = -7917547145929908026L;

    private String text;

    public TextLegend() {}

    public TextLegend(final String text) {
      this.text = text;
    }

    public String getText() {
      return text;
    }

    public void setText(final String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return "TextLegend [text=" + text + "]" + super.toString();
    }
  }
}
