/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo.shared;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 */
public class MultiLayerProps extends LayerProps implements Serializable {

  private static final long serialVersionUID = -4541973976245938252L;

  private ArrayList<LayerProps> layers = new ArrayList<LayerProps>();

  /**
   * @return The layers contained in this multi layer entity
   */
  public ArrayList<LayerProps> getLayers() {
    return layers;
  }

  /**
   * FIXME Opacities could be different for other layers..
   */
  @Override
  public float getOpacity() {
    return getLayers().isEmpty() ? 1.0f : getLayers().get(0).getOpacity();
  }

  /**
   * FIXME Enabled could be different for other layers..
   */
  @Override
  public boolean isEnabled() {
    boolean enable = false;
    for (final LayerProps layerProps : getLayers()) {
      enable |= layerProps.isEnabled();
    }
    return enable;
  }

  public void setLayers(final ArrayList<LayerProps> layers) {
    this.layers = layers;
  }
}
