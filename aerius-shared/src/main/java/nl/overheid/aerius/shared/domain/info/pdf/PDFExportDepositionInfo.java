/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info.pdf;

import java.io.Serializable;

/**
 *
 */
public class PDFExportDepositionInfo implements Serializable {

  private static final long serialVersionUID = 4722872463874696058L;

  private double maxDeltaDemand;
  private double currentDemandMaxDelta;
  private double proposedDemandMaxDelta;
  private double maxBackgroundDeposition;
  private double maxDeltaDemandOnlyExceeding;

  public double getMaxDeltaDemand() {
    return maxDeltaDemand;
  }

  public void setMaxDeltaDemand(final double maxDeltaDemand) {
    this.maxDeltaDemand = maxDeltaDemand;
  }

  public double getCurrentDemandMaxDelta() {
    return currentDemandMaxDelta;
  }

  public void setCurrentDemandMaxDelta(final double currentDemandMaxDelta) {
    this.currentDemandMaxDelta = currentDemandMaxDelta;
  }

  public double getMaxBackgroundDeposition() {
    return maxBackgroundDeposition;
  }

  public void setMaxBackgroundDeposition(final double maxBackgroundDeposition) {
    this.maxBackgroundDeposition = maxBackgroundDeposition;
  }

  public double getProposedDemandMaxDelta() {
    return proposedDemandMaxDelta;
  }

  public void setProposedDemandMaxDelta(final double proposedDemandMaxDelta) {
    this.proposedDemandMaxDelta = proposedDemandMaxDelta;
  }

  public double getMaxDeltaDemandOnlyExceeding() {
    return maxDeltaDemandOnlyExceeding;
  }

  public void setMaxDeltaDemandOnlyExceeding(final double maxDeltaDemandOnlyExceeding) {
    this.maxDeltaDemandOnlyExceeding = maxDeltaDemandOnlyExceeding;
  }

}
