/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import java.io.Serializable;
import java.util.Arrays;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.LandUse;

/**
 * Data class of ops receptor data.
 */
public class OPSReceptor extends AeriusPoint implements Serializable {

  private static final long serialVersionUID = 8140133462927088907L;

  /**
   * z0 - Average roughness(m).
   */
  private Double averageRoughness;

  /**
   * landuse - The dominant land use (enumerated value, 1-9).
   */
  private LandUse landUse;

  /**
   * Distribution of land uses (percentages).
   */
  private int[] landUses;

  /**
   * Constructor.
   */
  public OPSReceptor() {
    /* No-op. */ }

  public OPSReceptor(final int id) {
    super(id);
  }

  public OPSReceptor(final int id, final double x, final double y) {
    super(id, AeriusPointType.POINT, x, y);
  }

  public OPSReceptor(final AeriusPoint point) {
    super(point.getId(), point.getPointType(), point.getX(), point.getY());
    if (point instanceof OPSReceptor) {
      final OPSReceptor opsp = (OPSReceptor) point;
      averageRoughness = opsp.averageRoughness;
      landUse = opsp.landUse;
      landUses = opsp.landUses;
    }
  }

  public Double getAverageRoughness() {
    return averageRoughness;
  }

  public void setAverageRoughness(final Double averageRoughness) {
    this.averageRoughness = averageRoughness;
  }

  public LandUse getLandUse() {
    return landUse;
  }

  public void setLandUse(final LandUse landUse) {
    this.landUse = landUse;
  }

  public int[] getLandUses() {
    return landUses;
  }

  public void setLandUses(final int[] landUses) {
    if (landUses != null && landUses.length != SharedConstants.OPS_LAND_USE_CLASSES) {
      throw new IllegalArgumentException("The number of land uses must be exactly " + SharedConstants.OPS_LAND_USE_CLASSES
          + " but was " + landUses.length);
    }

    this.landUses = landUses;
  }

  public boolean hasNoTerrainData() {
    return averageRoughness == null || landUse == null;
  }

  public boolean hasTerrainData() {
    return !hasNoTerrainData();
  }

  @Override
  public String toString() {
    return "OPSReceptor [averageRoughness=" + averageRoughness + ", landUse=" + landUse + ", landUses="
        + Arrays.toString(landUses) + ", " + super.toString();
  }
}
