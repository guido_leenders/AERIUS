/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.constants;

/**
 * Global constants stored in the database and shared among the client and
 * server application. Constants NOT used in the client should be placed
 * in {@link nl.overheid.aerius.enums.ConstantsEnum}.
 *
 * <p>These can be accessed via the context's
 * {@link nl.overheid.aerius.shared.domain.context.Context#getSetting(SharedConstantsEnum)} method.</p>
 *
 * <p>The values of the entries are stored in the database tables <code>public.constants</code>
 * and <code>system.constants</code>.</p>
 */
public enum SharedConstantsEnum {
  /**
   * Internal url to the geoserver proxy server for the WMS stuff.
   */
  INTERNAL_WMS_PROXY_URL,
  /**
   * Url to the geoserver proxy server the WMS layers.
   */
  SHARED_WMS_PROXY_URL,
  /**
   * Url to the geoserver serving the WMS layers (not to be used by the client).
   */
  SHARED_WMS_URL,
  /**
   * Url to the webservice generating the sld for a WMS layer.
   */
  SHARED_SLD_URL,

  GEOSERVER_CAPIBILITIES_ORIGINAL_URL,

  AERIUS_HOST,

  WEBAPPNAME_CALCULATOR,

  /**
   * Analytics account id.
   */
  ANALYTICS_ACCOUNT_ID,

  /**
   * Domain name to report to Analytics.
   */
  ANALYTICS_DOMAIN_NAME,

  /**
   * Boolean that determines whether add Analytics code or not.
   */
  ANALYTICS_DISABLED,

  LAYER_SHIP_NETWORK,

  LAYER_MARITIME_SHIP_NETWORK,

  MIN_YEAR,
  MAX_YEAR,
  CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI,
  CALCULATE_EMISSIONS_DEFAULT_RADIUS_DISTANCE_UI,
  HELP_URL_TEMPLATE,

  /**
   * Maximum delay between concurrent chunks. The actual delay is calculated based on the maximum number of concurrent chunks. Time in milliseconds.
   */
  CALCULATION_MAX_DELAY_CHUNKS,
  /**
   * Checks if the amount of receptors returned by the chunker is lower then the number specified by this constant. If it is lower than the number
   * specified the timer is set. If a low number is returned if very likely means there are a lot of sources, and thus a lot of calculations will be
   * required. In such a case the delay between chunks will be more visible as per chunk, while with a large number of receptors the ui takes more
   * time to draw the receptors and therefore the delay hasn't a visible effect.
   */
  CALCULATION_MIN_RECEPTORS_FOR_DELAY,
  /**
   * Number used to calculate the approximate max number of receptors in any one tile.
   *
   * Math.pow(RECEPTORS_IN_UI_RECEPTOR_TILE / 1.5, 2) is an approximate max number of receptors in each 'bucket'
   *
   * Doubling this value will result in 2^2 times fewer tiles.
   *
   * Advised to be a multiple of 3. Default used to be 21.
   */
  CALCULATOR_LIMITS_RECEPTORS_RECEPTOR_TILE,

  /**
   * WKT string used for displaying the boundary in WUI calculator.
   * Within the boundary there are receptors and results should make sense. Outside the boundary this is not always true.
   */
  CALCULATOR_BOUNDARY,

  /**
   * URL to factsheet giving more information about OPS characteristics calculations.
   */
  OPS_FACTSHEET_URL,

  /**
   * Manual url.
   */
  MANUAL_URL,

  /**
   * URL to the pdok locatieserver.
   */
  PDOK_LOCATIESERVER_URL,

  /**
   * Whether melding is enabled or not.
   */
  MELDING_ENABLED,

  /**
   * Url to the melding start page.
   */
  MELDING_URL,

  /**
   * Path to the directory of the eHerkenning configuration files.
   */
  EHERKENNING_CONFIGDIR,

  /**
   * Full path name to the jks file.
   */
  EHERKENNING_JKSFILE,

  /**
   * Password of the jks file.
   */
  EHERKENNING_JKSPASS,

  /**
   * 95%.
   */
  PRONOUNCEMENT_SPACE_ALMOST_FULL_TRIGGER,

  /**
   * The time in between calls the client makes to the server to poll for calculation results.
   */
  CALCULATOR_POLLING_TIME,

  /**
   * Calculator fetch URL.
   */
  CALCULATOR_FETCH_URL,
  /**
   * URL to the explanation page of PAS permits/meldingen.
   */
  PAS_PERMIT_EXPLANATION_URL,

  /**
   * URL to the priority project list page.
   */
  PAS_PP_LIST_URL,

  /**
   * If an additional logout url must be called on an external authentication server the LOGOUT_URL should be set, else it can be omitted.
   */
  LOGOUT_URL,

  /**
   * Normally when finishing the drawing of a shipping route, the nearest shipping node (at sea) is appended
   * to the route.
   * When drawing an inland route for the shipping sector "maritime mooring", this will only happen when the
   * nearest shipping node is within the search distance given here.
   */
  MARITIME_MOORING_INLAND_ROUTE_SHIPPING_NODE_SEARCH_DISTANCE,

  /**
   * Polling time for system info message.
   * Used for the polling time for the request.
   */
  SYSTEM_INFO_POLLING_TIME,

  /**
   * PASSKEY a UUID secret string that is used to update system info messages in the databse
   * Used to update system info messages tables.
   */
  SYSTEM_INFO_PASSKEY,

  /**
   * Url for Connect API endpoint.
   * Used to call connect api services.
   */
  CONNECT_API_URL,
  
  /**
   * Delay to wait before notifying there is a queue.
   */
  CALCULATION_DELAY_NOTIFICATION_TIME;
}
