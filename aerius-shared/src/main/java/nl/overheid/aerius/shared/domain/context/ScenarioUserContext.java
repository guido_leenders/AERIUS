/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

/**
 * Data object loaded when the application start and contains use specific data.
 */
public class ScenarioUserContext extends ScenarioBaseUserContext {
  private static final long serialVersionUID = 4140839902320916921L;

  private String aPIKey;

  // Could replace with the jobkey, but there's really no point considering that state should be in the Place.
  private boolean followingJob;

  public boolean hasAPIKey() {
    return aPIKey != null && !aPIKey.isEmpty();
  }

  public String getAPIKey() {
    return aPIKey;
  }

  public void setAPIKey(final String aPIKey) {
    this.aPIKey = aPIKey;
  }

  public boolean isFollowingJob() {
    return followingJob;
  }

  public void setFollowingJob(final boolean followingJob) {
    this.followingJob = followingJob;
  }
}
