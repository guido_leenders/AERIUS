/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared;

import java.util.Date;

/**
 * Constants shared by the client and server.
 *
 * Contained in a concrete class to allow for static imports
 */
public final class SharedConstants {

  public static final String NBSP = "&nbsp;";

  /**
   * Arrow pointing downwards, used in listboxes and sorting.
   *
   * Unicode name: BLACK DOWN-POINTING SMALL TRIANGLE
   */
  public static final String ARROW_DOWN_SMALL = "&#9662;";

  /**
   * Arrow pointing upwards, used for sorting.
   *
   * Unicode name: BLACK UP-POINTING SMALL TRIANGLE
   */
  public static final String ARROW_UP_SMALL = "&#9652;";

  public static final String NATURE_AREAS = "wms_nature_areas_view";

  public static final int PAA_DESCRIPTION_LENGTH = 1000;
  /**
   * Minimum number of years a temp project can be specified.
   */
  public static final int MIN_TEMP_PROJECT_YEARS = 1;
  /**
   * Maximum number of years a temp project can be specified.
   */
  public static final int MAX_TEMP_PROJECT_YEARS = 5;

  /**
   * Locale The Netherlands.
   */
  public static final String LOCALE_NL = "nl";

  /**
   * The querystring key to store the locale in.
   */
  public static final String LOCALE_KEY = "locale";

  /**
   * The locale to use when nothing is specified. Fallback locale if you will.
   */
  public static final String DEFAULT_LOCALE = LOCALE_NL;

  /**
   * Name of the import HttpServlet.
   */
  public static final String IMPORT_SAVE_SERVLET = "save-import";

  /**
   * Name of the import permit HttpServlet.
   */
  public static final String IMPORT_PERMIT_SERVLET = "import-permit";

  /**
   * Name of the import priority project HttpServlet.
   */
  public static final String IMPORT_PRIORITYPROJECT_SERVLET = "import-pp";

  /**
   * Name of the factsheet HttpServlet.
   */
  public static final String IMPORT_FACTSHEET_SERVLET = "import-pp-factsheet";

  /**
   * Name of the actualisation HttpServlet.
   */
  public static final String IMPORT_ACTUALISATION_SERVLET = "import-pp-actualisation";

  /**
   * Name of the import subproject HttpServlet.
   */
  public static final String IMPORT_PP_SUBPROJECT_SERVLET = "import-pp-subproject";

  /**
   * Name of the import Scenario calculation HttpServlet.
   */
  public static final String IMPORT_CONNECT_CALCULATION_SERVLET = "import-connect-calculation";

  /**
   * Name of the substance form input value in the import dialog.
   */
  public static final String IMPORT_SUBSTANCE_FIELD_NAME = "substance";

  /**
   * Conflict action type form input value in the import dialog.
   */
  public static final String IMPORT_CONFLICT_ACTION_TYPE_FIELD_NAME = "conflictAction";

  /**
   * Name of the form input value in the import dialog.
   */
  public static final String IMPORT_FILE_FIELD_NAME = "importFile";

  /**
   * If true sources should be imported.
   */
  public static final String IMPORT_SOURCES_FIELD_NAME = "importSources";

  /**
   * Name of the date form input value in the import dialog.
   */
  public static final String IMPORT_DATE_FIELD_RECEIVEDDATE = "receivedDate";

  /**
   * If true results should be imported.
   */
  public static final String IMPORT_RESULTS_FIELD_NAME = "importResults";

  /**
   * If true GML should be put in the DB to be used for specific WMS layers.
   */
  public static final String IMPORT_CREATE_GEO_LAYERS_FIELD_NAME = "createUserLayers";

  /**
   * Name of the label form input value in the import dialog.
   */
  public static final String IMPORT_LABEL_FIELD_NAME = "label";

  /**
   * Prefix for error code when a more preferable exception mechanism cannot be used.
   */
  public static final String IMPORT_ERROR_PREFIX = "ERROR";

  /**
   * Prefix for success code when a more preferable exception mechanism cannot be used.
   */
  public static final String IMPORT_SUCCESS_PREFIX = "SUCCESS";

  /**
   * Number of months in a year.
   */
  public static final int NUMBER_MONTHS_PER_YEAR = 12;

  /**
   * Number of hours in a year.
   */
  public static final int NUMBER_HOURS_PER_YEAR = 365 * 24;

  /**
   * Number of seconds in a year.
   */
  public static final int NUMBER_SECONDS_PER_YEAR = NUMBER_HOURS_PER_YEAR * 60 * 60;

  /**
   * Number of milliseconds in a day.
   */
  public static final int MILLISECONDS_IN_DAY = 1000 * 60 * 60 * 24;

  /**
   * Feels like such a hassle to use some sort of serializer, so I vote it's fine like this.
   */
  public static final String IMPORT_ARGUMENT_SEPERATOR = "-_-";

  /**
   * Conversion from percentage to fraction.
   */
  public static final int PERCENTAGE_TO_FRACTION = 100;

  /**
   * Conversion from gram/meter to kilogram/kilometer.
   */
  public static final int GRAM_PER_KM_TO_KG_PER_METER = 1000 * 1000;

  /**
   * Conversion from kilogram/meter to kilogram/kilometer.
   */
  public static final double KG_PER_KM_TO_KG_PER_METER = 1000;

  /**
   * Conversion from gram to kilogram.
   */
  public static final int GRAM_TO_KILOGRAM = 1000;

  /**
   * Conversion from.
   */
  public static final int MICROGRAM_TO_GRAM = 1000 * 1000;

  /**
   * Conversion from centimeter to meter.
   */
  public static final int CM_TO_M = 100;

  /**
   * Conversion from meter to kilometer.
   */
  public static final int M_TO_KM = 1000;

  /**
   * Conversion from kilogram to kiloton.
   */
  public static final int KILO_TO_TON = 1000;

  /**
   * Conversion ratio of m2 to hectare.
   */
  public static final double M2_TO_HA = 100 * 100;

  /**
   * Conversion from inch to centimeters.
   */
  public static final double INCH_TO_CM = 2.54;

  /**
   * Conversion from bytes to MB.
   */
  public static final int BYTES_TO_MB = 1000 * 1000;

  /**
   * Distance in Google divided by this is the distance in RD new.
   */
  public static final double GOOGLE_TO_RDNEW = 1.68;

  /**
   * Minimum RDM X-coordinate according to wiki (NL).
   * X_COORDINATE_MINIMUM = -7;
   * RIVM: due to I8 format: min is -9999999
   * (this limit exceeds dutch territory by a fair bit)
   */
  public static final int X_COORDINATE_MINIMUM = -9999999;

  /**
   * Maximum RDM X-coordinate according to wiki (NL)
   * X_COORDINATE_MAXIMUM = 300000;
   * RIVM: due to I8 format: max is 99999999
   * testing: When coordinates are  > 9999999 (one 9 less) OPS starts to be unresponsive.
   * To avoid problems with workers becoming unresponsive, we use this as limit.
   * (this limit exceeds dutch territory by a fair bit)
   */
  public static final int X_COORDINATE_MAXIMUM = 9999999;

  /**
   * Minimum RDM Y-coordinate according to wiki (NL).
   * Y_COORDINATE_MINIMUM = 289000;
   * RIVM: due to I8 format: min is -9999999
   * (this limit exceeds dutch territory by a fair bit)
   */
  public static final int Y_COORDINATE_MINIMUM = -9999999;

  /**
   * Maximum RDM Y-coordinate according to wiki (NL)
   * Y_COORDINATE_MAXIMUM = 629000;
   * RIVM: due to I8 format: max is 99999999
   * testing: When coordinates are  > 9999999 (one 9 less) OPS starts to be unresponsive.
   * To avoid problems with workers becoming unresponsive, we use this as limit.
   * (this limit exceeds dutch territory by a fair bit)
   */
  public static final int Y_COORDINATE_MAXIMUM = 9999999;

  /**
   * The name of the hidden form field used to store some payload data when opening the
   * AERIUS Calculator via a form post from AERIUS Register. It's an 'array' so multiple files can be given.
   */
  public static final String FORM_POST_PAYLOAD = "payload[]";

  /**
   * Unique ID passed from calculator to melding module to identify data passed from calculator to melding application.
   */
  public static final String MELDING_UUID_PARAM = "relayState";

  /**
   * The name of the hidden form field used to store a proposed situation gml when opening
   * AERIUS Melding via a form post from AERIUS Calculator.
   */
  public static final String MELDING_GML_PROPOSED = "proposed_gml";

  /**
   * The name of the hidden form field used to the temp directory where uploaded files are stored.
   */
  public static final String MELDING_UPLOAD_DIRECTORY = "melding_upload_directory";

  /**
   * The name of the hidden form field used to store a list op uploaded files.
   */
  public static final String MELDING_UPLOAD_FILES = "melding_upload_files";

  /**
   * The name of the hidden form field used to store a current situation gml when opening
   * AERIUS Melding via a form post from AERIUS Calculator.
   */
  public static final String MELDING_GML_CURRENT = "current_gml";


  /**
   * The name of the hidden form field used to store the payload type when opening the
   * AERIUS Calculator via a form post from AERIUS Register.
   */
  public static final String FORM_POST_TYPE = "type";
  public static final String FORM_POST_TYPE_DEFAULT = "gml";

  public static final String IMPORT_SCENARIO_PREFIX = "import_scenario_";
  public static final String IMPORT_SCENARIO_CALCULATIONS_PREFIX = "import_scenario_calculations_";

  /**
   * The name of the hidden form field used to store the gml data when opening the
   * AERIUS Calculator via a form post from AERIUS Register.
   */
  public static final int OPS_LAND_USE_CLASSES = 9;

  /**
   * The amount of days in a year AERIUS uses by default.
   */
  public static final int DAYS_PER_YEAR = 365;

  /**
   * Regex used to a correct emailaddress.
   */
  public static final String VALID_EMAIL_ADDRESS_REGEX =
      "^[\\w\\u00C0-\\u02AF-\\+]+(\\.[\\w\\u00C0-\\u02AF-]+)*@[\\w\\u00C0-\\u02AF-]+(\\.[\\w\\u00C0-\\u02AF]+)*(\\.[A-Za-z]{2,})$";

  /**
   * Regex used to a correct api key.
   */
  public static final String VALID_UUID_REGEX = "^[0-9a-f]{32}$";

  /**
   * The parameter to use when supplying an internal SLD with a different name than the actual layer name.
   */
  public static final String PARAM_INTERNAL_SLD = "INTERNAL_SLD";

  /**
   * Http status code for expired logins.
   */
  public static final int AUTHENTICATION_EXPIRED = 419;

  private static final int YEAR_OFFSET = 1900;

  private SharedConstants() {
    // Util class
  }

  /**
   * Returns current year.
   *
   * Must use Date's deprecated .getYear() method because Calendar is not supported in GWT and GWT's SimpleDateFormat equivalent is not supported
   * in GWT.
   */
  public static int getCurrentYear() {
    return YEAR_OFFSET + new Date().getYear();
  }
}
