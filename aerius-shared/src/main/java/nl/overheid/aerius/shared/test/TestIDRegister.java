/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.test;

/**
 * TestID class for product Register.
 */
public final class TestIDRegister extends TestID {

  // Menu Basic Information.
  public static final String MENU_DASHBOARD = "menu_dashboard";
  // Menu Applications.
  public static final String MENU_APPLICATIONS = "menu_applications";
  // Menu Prioritary projects.
  public static final String MENU_PRIORITARY_PROJECTS = "menu_prioritary_projects";
  // Menu Messages.
  public static final String MENU_MESSAGES = "menu_messages";

  public static final String BUTTON_OPENINCALCULATOR_SUBMIT = "buttonOpenInCalculatorSubmit";

  public static final String BUTTON_NEW_APPLICATION = "newApplication";

  public static final String CELLTABLE_APPLICATIONS = "applicationsCellTable";
  public static final String APPLICATION_DOSSIER_ID_VIEWER = "applicationDossierIdViewer";
  public static final String APPLICATION_HANDLER_VIEWER = "applicationHandlerViewer";
  public static final String APPLICATION_REMARKS_VIEWER = "applicationRemarksViewer";
  public static final String PRIORITYPROJECT_NEW_BUTTON = "priorityProjectNewButton";
  public static final String ACTUALISATION_NEW_BUTTON = "actualisationNewButton";
  public static final String PRIORITYPROJECT_ASSIGNCOMPLETE_BUTTON = "priorityProjectAssignCompleteButton";
  public static final String PRIORITYPROJECT_EXPORT_BUTTON = "priorityProjectExportButton";
  public static final String PRIORITYPROJECT_DELETE_BUTTON = "priorityProjectDeleteButton";
  public static final String PRIORITYPROJECT_ACTUALIZATION_ADD_BUTTON = "priorityProjectActualizationAddButton";
  public static final String PRIORITYPROJECT_ACTUALIZATION_DELETE_BUTTON = "priorityProjectActualizationDeleteButton";
  public static final String PRIORITYPROJECT_ACTUALIZATION_REPLACE_BUTTON = "priorityProjectActualizationReplaceButton";
  public static final String PERMIT_DOSSIER_ID_EDITOR = "applicationDossierIdEditor";
  public static final String PERMIT_HANDLER_EDITOR = "applicationHandlerEditor";
  public static final String PERMIT_REMARKS_EDITOR = "applicationRemarksEditor";
  public static final String PERMIT_REFRESH_BUTTON = "permitRefreshButton";
  public static final String PERMIT_EXPORT_BUTTON = "permitExportButton";
  public static final String NOTICE_DELETE_BUTTON = "noticeDeleteButton";
  public static final String APPLICATION_EMISSION_VALUE_VIEWER = "applicationEmissionValueViewer";
  public static final String APPLICATION_DATE_VIEWER = "applicationDateViewer";
  public static final String APPLICATION_END_DATE_VIEWER = "applicationEndDateViewer";
  public static final String APPLICATION_REFERENCE_LINK_VIEWER = "applicationReferenceLinkerViewer";
  public static final String APPLICATION_CORPORATION_VIEWER = "applicationCorporationViewer";
  public static final String APPLICATION_PROJECT_VIEWER = "applicationProjectViewer";
  public static final String APPLICATION_GOTO_CALCULATOR_VIEWER = "applicationGotoCalculatorViewer";
  public static final String APPLICATION_REFRESH = "applicationRefreshButton";
  public static final String APPLICATION_EMISSION_SUBSTANCES_LISTBOX = "applicationEmissionSubstancesListbox";
  public static final String APPLICATION_EMISSION_VALUE_EDITOR = "applicationEmissionValueEditor";
  public static final String APPLICATION_DATE_EDITOR = "applicationDateEditor";
  public static final String APPLICATION_END_DATE_EDITOR = "applicationEndDateEditor";
  public static final String APPLICATION_REFERENCE_LINK_EDITOR = "applicationReferenceLinkEditor";
  public static final String APPLICATION_CORPORATION_EDITOR = "applicationCorporationEditor";
  public static final String APPLICATION_PROJECT_EDITOR = "applicationProjectEditor";
  public static final String APPLICATION_GOTO_CALCULATOR_EDITOR = "applicationGotoCalculatorEditor";
  public static final String APPLICATION_TAB_PANEL = "applicationTabPanel";
  public static final String APPLICATION_STATUS_BUTTON = "applicationStatusButton";
  public static final String APPLICATION_DELETE_BUTTON = "applicationDeleteButton";
  public static final String APPLICATION_MARK_BUTTON = "applicationMarkButton";
  public static final String APPLICATION_LOADING_WIDGET = "turboturbo";

  public static final String INPUT_FILTER_FROM = "filterFrom";
  public static final String INPUT_FILTER_TILL = "filterTill";
  public static final String INPUT_FILTER_LOCATION = "filterLocation";
  public static final String INPUT_FILTER_SECTOR = "filterSector";
  public static final String INPUT_FILTER_AREA = "filterArea";
  public static final String INPUT_FILTER_STATUS = "filterStatus";
  public static final String INPUT_FILTER_AUTHORITY = "filterAuthority";
  public static final String INPUT_FILTER_MARKED = "filterMarked";

  // Status aanpassen
  // public static enum APPLICATION_STATUS { queued =
  // 'gwt-debug-statusChange-ASSIGNED' };

  public static final String NOTICES_CONFIRM_BUTTON = "noticesConfirmButton";
  public static final String NOTICES_REFRESH_BUTTON = "noticesRefreshButton";
  public static final String NOTICES_EXPORT_BUTTON = "noticesExportButton";
  public static final String NOTICES_CELLTABLE = "noticesCellTable";

  public static final String INPUT_DOSSIER_ID = "dossierID";
  public static final String INPUT_IMPORT_ADDITIONAL_FIELD = "importAdditionalField";
  // public static final String BUTTON_REMOVE_APPLICATION =
  // "removeApplication";
  public static final String BUTTON_REMOVE_APPLICATION = "applicationDeleteButton";
  public static final String BUTTON_EDIT_APPLICATION = "editApplication";
  public static final String BUTTON_SAVE_APPLICATION = "saveApplication";
  public static final String BUTTON_CANCEL_EDIT_APPLICATION = "cancelEditApplication";
  public static final String BUTTON_DOWNLOAD_ANNEX_APPLICATION = "downloadAnnexApplication";

  /**
   * name() value of the enum type ApplicationState should be appended to this
   * value.
   *
   * eg. 'gwt-debug-applicationStateADMISSIBLE'
   */
  public static final String APPLICATION_STATE = "applicationState";
  public static final String APPLICATION_TAB_DETAILS = "ApplicationTabDetails";
  public static final String APPLICATION_TAB_PERMIT_RULES = "applicationTabPermitRules";
  public static final String APPLICATION_TAB_DEVELOPMENT_SPACE = "applicationTabDevelopmentSpace";

  public static final String INPUT_SELECT_PROVINCE_FILTER = "dashboardPermitProvinceFilter";
  public static final String INPUT_CHECKBOX_PROVINCE_SHORTAGE = "dashboardPermitProvinceCheckbox-SHORTAGE-input";
  public static final String INPUT_CHECKBOX_PROVINCE_NEAR_SHORTAGE = "dashboardPermitProvinceCheckbox-NEAR_SHORTAGE-input";
  public static final String INPUT_CHECKBOX_PROVINCE_NO_SHORTAGE = "dashboardPermitProvinceCheckbox-NO_SHORTAGE-input";

  public static final String APPLICATION_TAB_SUB_PROJECT_DOSSIER_DETAIL= "applicationTabSubProjectDossierDetail";
  public static final String APPLICATION_TAB_SUB_PROJECT_DOSSIER_REVIEW = "applicationTabSubProjectDossierReview";

  public static final String APPLICATION_TAB_REVIEW_VIEW_SWITCHER = "applicationTabReviewViewSwitcher";

  /**
   * Index of the collapsible item will be appended to this value.
   */
  public static final String BUTTON_LOGOUT = "buttonLogout";
  public static final String BUTTON_PROFILE = "buttonProfile";
  public static final String APPLICATION_TAB_DOSSIER = "dossierInformation";
  public static final String NOTICE_TAB_DOSSIER = "noticeTabDossier";
  public static final String NOTICE_TAB_CHART = "noticeTabChart";


  public static final String DIV_TABLE_RES_DEPOSITION = "depositionTable";
  public static final String DASHBOARD_PERMIT_PROVINCE_FILTER = "dashboardPermitProvinceFilter";
  public static final String DASHBOARD_PERMIT_CHECKBOX = "dashboardPermitProvinceCheckbox";
  public static final String LIST_PROVINCES_NOTICES = "listbox-provinces-notices";
  public static final String LIST_PROVINCES_PRIORITARY = "listbox-priority-project";
  public static final String LIST_MIN_DEP = "listbox-minimum-deposition";

  private TestIDRegister() {
    // Util class constructor
  }
}
