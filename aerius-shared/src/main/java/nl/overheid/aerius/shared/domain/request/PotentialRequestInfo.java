/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.request;

import java.io.Serializable;

/**
 * Info about the NB-wet calculation that is sent to the client. It holds the type of the
 * (potential) request that needs to be done, the relevant authority and for how may areas
 * the permit threshold is exceeded.
 */
public class PotentialRequestInfo implements Serializable {
  private static final long serialVersionUID = 286193601666989352L;

  private PotentialRequestType potentialRequestType;
  private int exceedingAssessmentAreaNum;

  public PotentialRequestType getPotentialRequestType() {
    return potentialRequestType;
  }

  public int getExceedingAssessmentAreaNum() {
    return exceedingAssessmentAreaNum;
  }

  public void setPotentialRequestType(final PotentialRequestType potentialRequestType) {
    this.potentialRequestType = potentialRequestType;
  }

  public void setExceedingAssessmentAreaNum(final int exceedingAssessmentAreaNum) {
    this.exceedingAssessmentAreaNum = exceedingAssessmentAreaNum;
  }
}
