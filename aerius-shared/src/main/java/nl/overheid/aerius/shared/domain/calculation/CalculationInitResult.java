/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The result after a calculation is initialized by the server.
 */
public class CalculationInitResult implements Serializable {

  private static final long serialVersionUID = 2884007431134737501L;

  private CalculationSetOptions calculationSetOptions;
  private ArrayList<Calculation> calculations;
  private String calculationKey;

  // Needed by GWT.
  public CalculationInitResult() {
  }

  /**
   * @param calculationSetOptions
   * @param calculationKeys
   */
  public CalculationInitResult(final CalculationSetOptions calculationSetOptions, final ArrayList<Calculation> calculations) {
    this.calculationSetOptions = calculationSetOptions;
    this.calculations = calculations;
  }

  /**
   * @return the calculationSetOptions
   */
  public CalculationSetOptions getCalculationSetOptions() {
    return calculationSetOptions;
  }

  public ArrayList<Calculation> getCalculations() {
    return calculations;
  }

  public String getCalculationKey() {
    return calculationKey;
  }

  public void setCalculationKey(final String calculationKey) {
    this.calculationKey = calculationKey;
  }

  @Override
  public String toString() {
    return "CalculationInitResult [calculationSetOptions=" + calculationSetOptions + ", calculatedScenario=" + calculations + "]";
  }
}
