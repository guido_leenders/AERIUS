/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Inland ship emission source for routes.
 * This class only works for ships of the InlandShippingCategory type, for Maritime versions, see {@link MaritimeRouteEmissionSource}.
 */
public class InlandRouteEmissionSource extends ShippingEmissionSource<RouteInlandVesselGroup> implements HasShippingRoute, Serializable {

  private static final long serialVersionUID = -7626005911103479067L;

  /**
   * Enum of ship laden or unladen state.
   */
  public enum ShippingLaden {
    /**
     * Ship laden.
     */
    LADEN,
    /**
     * Ship unladen.
     */
    UNLADEN;
  }

  public static class RouteInlandVesselGroup extends VesselGroupEmissionSubSource<InlandShippingCategory> {


    private static final long serialVersionUID = -7443847912294008919L;

    /**
     * Number of shipping movements by vessel type in the direction of travel. A to B means moving from first point (A) on the line
     * to the last point (B) on the line.
     */
    private int numberOfShipsAtoBperTimeUnit;
    private int numberOfShipsBtoAperTimeUnit;
    private TimeUnit timeUnitShipsAtoB = TimeUnit.DAY;
    private TimeUnit timeUnitShipsBtoA = TimeUnit.DAY;

    /**
     * Percentage of ships laden (0 - 1). Default value is 65%. Source: Toelichting Rekenapplicatie PRELUDE versie 1.0. Juni 2012.
     */
    private int percentageLadenAtoB = 65;
    private int percentageLadenBtoA = 65;

    @Override
    public RouteInlandVesselGroup copy() {
      return copyTo(new RouteInlandVesselGroup());
    }

    protected <V extends RouteInlandVesselGroup> V copyTo(final V copy) {
      super.copyTo(copy);
      copy.setNumberOfShipsAtoBperTimeUnit(numberOfShipsAtoBperTimeUnit);
      copy.setTimeUnitShipsAtoB(timeUnitShipsAtoB);
      copy.setNumberOfShipsBtoAperTimeUnit(numberOfShipsBtoAperTimeUnit);
      copy.setTimeUnitShipsBtoA(timeUnitShipsBtoA);
      copy.setPercentageLadenAtoB(percentageLadenAtoB);
      copy.setPercentageLadenBtoA(percentageLadenBtoA);
      return copy;
    }

    @Min(0)
    public int getNumberOfShipsAtoBperTimeUnit() {
      return numberOfShipsAtoBperTimeUnit;
    }

    @Min(0)
    public int getNumberOfShipsBtoAperTimeUnit() {
      return numberOfShipsBtoAperTimeUnit;
    }

    @NotNull
    public TimeUnit getTimeUnitShipsAtoB() {
      return timeUnitShipsAtoB;
    }

    @NotNull
    public TimeUnit getTimeUnitShipsBtoA() {
      return timeUnitShipsBtoA;
    }

    @Min(0)
    @Max(100)
    public int getPercentageLadenAtoB() {
      return percentageLadenAtoB;
    }

    @Min(0)
    @Max(100)
    public int getPercentageLadenBtoA() {
      return percentageLadenBtoA;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + super.getStateHash();
      result = (prime * result) + numberOfShipsAtoBperTimeUnit;
      result = (prime * result) + timeUnitShipsAtoB.ordinal();
      result = (prime * result) + numberOfShipsBtoAperTimeUnit;
      result = (prime * result) + timeUnitShipsBtoA.ordinal();
      result = (prime * result) + percentageLadenAtoB;
      result = (prime * result) + percentageLadenBtoA;
      return result;
    }

    public void setNumberOfShipsAtoB(final int numberOfShipsAtoBperTimeUnit, final TimeUnit timeUnit) {
      setNumberOfShipsAtoBperTimeUnit(numberOfShipsAtoBperTimeUnit);
      setTimeUnitShipsAtoB(timeUnit);
    }

    public void setNumberOfShipsBtoA(final int numberOfShipsBtoAperTimeUnit, final TimeUnit timeUnit) {
      setNumberOfShipsBtoAperTimeUnit(numberOfShipsBtoAperTimeUnit);
      setTimeUnitShipsBtoA(timeUnit);
    }

    public void setNumberOfShipsAtoBperTimeUnit(final int numberOfShipsAtoBperTimeUnit) {
      this.numberOfShipsAtoBperTimeUnit = numberOfShipsAtoBperTimeUnit;
    }

    public void setNumberOfShipsBtoAperTimeUnit(final int numberOfShipsBtoAperTimeUnit) {
      this.numberOfShipsBtoAperTimeUnit = numberOfShipsBtoAperTimeUnit;
    }

    public void setTimeUnitShipsAtoB(final TimeUnit timeUnitShipsAtoB) {
      this.timeUnitShipsAtoB = timeUnitShipsAtoB;
    }

    public void setTimeUnitShipsBtoA(final TimeUnit timeUnitShipsBtoA) {
      this.timeUnitShipsBtoA = timeUnitShipsBtoA;
    }

    public void setPercentageLadenAtoB(final int percentageLadenAtoB) {
      this.percentageLadenAtoB = percentageLadenAtoB;
    }

    public void setPercentageLadenBtoA(final int percentageLadenBtoA) {
      this.percentageLadenBtoA = percentageLadenBtoA;
    }

    public int getNumberOfShipsAtoBPerYear() {
      return timeUnitShipsAtoB == null ? 0 : timeUnitShipsAtoB.getPerYear(numberOfShipsAtoBperTimeUnit);
    }

    public int getNumberOfShipsBtoAPerYear() {
      return timeUnitShipsBtoA == null ? 0 : timeUnitShipsBtoA.getPerYear(numberOfShipsBtoAperTimeUnit);
    }

    public double getShipsAtoBPerYear(final ShippingLaden laden) {
      return getNumberOfShipsAtoBPerYear() * (laden == ShippingLaden.LADEN ? percentageLadenAtoB : 100 - percentageLadenAtoB) * 0.01;
    }

    public double getShipsBtoAPerYear(final ShippingLaden laden) {
      return getNumberOfShipsBtoAPerYear() * (laden == ShippingLaden.LADEN ? percentageLadenBtoA : 100 - percentageLadenBtoA) * 0.01;
    }

    @Override
    public String toString() {
      return "InlandVesselGroupEmissionValues [" + super.toString()
          + ", numberOfShipsAtoBperTimeUnit=" + numberOfShipsAtoBperTimeUnit + ", numberOfShipsBtoAperTimeUnit=" + numberOfShipsBtoAperTimeUnit
          + ", timeUnitShipsAtoB=" + timeUnitShipsAtoB + ", timeUnitShipsBtoA=" + timeUnitShipsBtoA
          + ", percentageLadenAtoB=" + percentageLadenAtoB + ", percentageLadenBtoA=" + percentageLadenBtoA + "]";
    }
  }

  private InlandWaterwayType inlandWaterwayType = new InlandWaterwayType();

  @Override
  public InlandWaterwayCategory getWaterwayCategory() {
    return inlandWaterwayType.getWaterwayCategory();
  }

  @Override
  public WaterwayDirection getWaterwayDirection() {
    return inlandWaterwayType.getWaterwayDirection();
  }

  @Override
  public void setWaterwayCategory(final InlandWaterwayCategory waterwayCategory) {
    inlandWaterwayType.setWaterwayCategory(waterwayCategory);
  }

  @Override
  public void setWaterwayDirection(final WaterwayDirection waterwayDirection) {
    inlandWaterwayType.setWaterwayDirection(waterwayDirection);
  }

  @Override
  public InlandWaterwayType getInlandWaterwayType() {
    return inlandWaterwayType;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    final int result = super.getStateHash();
    return (prime * result) + ((inlandWaterwayType == null) ? 0 : inlandWaterwayType.getStateHash());
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj) && Objects.equals(inlandWaterwayType, ((InlandRouteEmissionSource) obj).inlandWaterwayType);
  }

  @Override
  public int hashCode() {
    return super.hashCode() + 31 * inlandWaterwayType.hashCode();
  }

  @Override
  public InlandRouteEmissionSource copy() {
    final InlandRouteEmissionSource copy = copyTo(new InlandRouteEmissionSource());
    copy.setWaterwayCategory(inlandWaterwayType.getWaterwayCategory());
    copy.setWaterwayDirection(inlandWaterwayType.getWaterwayDirection());
    return copy;
  }

  @Override
  public String toString() {
    return "InlandRouteEmissionValues [" + super.toString() + ", waterwayType=" + inlandWaterwayType + "]";
  }

}
