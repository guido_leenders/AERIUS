/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasState;

/**
 * Interface calculate the emission values.
 */
public abstract class EmissionSubSource implements HasState, HasId, Serializable {

  private static final long serialVersionUID = 6078039089282682229L;

  private int id;
  private boolean emissionPerUnit;

  @Override
  public int getId() {
    return id;
  }

  /**
   * Makes a copy of the emission values, used to copy the object.
   *
   * @return new instance.
   */
  public abstract EmissionSubSource copy();

  /**
   * Fill a copy, used to copy the object.
   *
   * @return the same instance, filled for this particular class.
   */
  protected <U extends EmissionSubSource> U copyTo(final U copy) {
    copy.setId(id);
    return copy;
  }

  /**
   * Returns the emission for the given substance.
   *
   * @param key key to get specific emissions.
   * @return emission
   */
  abstract double getEmission(EmissionValueKey key);

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "id=" + id + ", emission per unit=" + emissionPerUnit;
  }
}
