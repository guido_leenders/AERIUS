/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.sector.category.ShippingCategory;

/**
 * Emissions for a group of vessels.
 * <p>Any emissions should be calculated on the serverside and set in here.
 * These emissions should be total for the vesselgroup, including docking and any routes linked to it.
 */
public abstract class VesselGroupEmissionSubSource<T extends ShippingCategory>
extends EmissionSubSource implements HasName, HasEmissionValues, Serializable {

  private static final long serialVersionUID = -8106978903299330404L;

  private String name;

  private T category;

  /**
   * The total emissions for this vessel group.
   */
  private EmissionValues emissions = new EmissionValues();

  protected <V extends VesselGroupEmissionSubSource<T>> V copyTo(final V copy) {
    super.copyTo(copy);
    copy.setName(getName());
    copy.setCategory(getCategory());
    copy.setEmissionValues(getEmissionValues().copy());
    return copy;
  }

  @Override
  public String getName() {
    return name;
  }

  @NotNull
  public T getCategory() {
    return category;
  }

  @Override
  double getEmission(final EmissionValueKey key) {
    return emissions.getEmission(key);
  }

  @Override
  public EmissionValues getEmissionValues() {
    return emissions;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((category == null) ? 0 : category.getStateHash());
    return result;
  }

  @Override
  public void setEmissionValues(final EmissionValues emissions) {
    this.emissions = emissions;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  public void setCategory(final T category) {
    this.category = category;
  }

  @Override
  public String toString() {
    return "ID=" + getId() + ", name=" + name + ", category=" + category;
  }
}
