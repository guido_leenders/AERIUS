/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.util.ArrayList;

/**
 *
 */
public enum WMSLayerType {

  /**
   * Layer containing habitat areas (per assessment area)
   */
  WMS_HABITATS_VIEW,

  /**
   * Layer containing habitat areas (per receptor)
   */
  WMS_RELEVANT_HABITAT_INFO_FOR_RECEPTOR_VIEW,

  /**
   * Layer containing the demand of a certain request.
   */
  WMS_PERMIT_DEMANDS_VIEW,

  /**
   * Layer containing depositions
   * per critical deposition area/receptor.
   */
  WMS_CRITICAL_DEPOSITION_AREA_RECEPTOR_DEPOSITIONS_VIEW,

  /**
   * Layer containing delta depositions (difference between base year and view year)
   * per critical deposition area/receptor.
   */
  WMS_CRITICAL_DEPOSITION_AREA_RECEPTOR_DELTA_DEPOSITIONS_VIEW,

  /**
   * Layer containing deviation from KDW
   * per critical deposition area/receptor.
   */
  WMS_CRITICAL_DEPOSITION_AREA_RECEPTOR_DEVIATIONS_VIEW,

  /**
   * Layer containing deposition results for a calculation for a substance.
   */
  WMS_CALCULATION_SUBSTANCE_DEPOSITION_RESULTS_VIEW,

  /**
   * Layer containing difference in deposition results between 2 calculations for a substance.
   */
  WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW,

  /**
   * Layer containing difference in deposition results between 2 calculations for a substance show as utilisation.
   */
  WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION,
  
  /**
   * Layer containing difference in deposition results between 2 calculations for a substance showing the value as label.
   */
  WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_LABEL,

  /**
   * Layer containing difference in deposition results between 2 calculations for a substance show as percentage of space left.
   */
  WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_PERCENTAGE,
  
  /**
   * Layer containing difference in deposition results between 2 calculations for a substance showing percentage value as a label.
   */
  WMS_CALCULATIONS_SUBSTANCE_DEPOSITION_RESULTS_DIFFERENCE_VIEW_UTILISATION_PERCENTAGE_LABEL,

  /**
   * Layer containing road total vehicles per day.
   */
  WMS_USER_ROAD_TOTAL_VEHICLES_PER_DAY,

  /**
   * Layer containing source label.
   */
  WMS_USER_SOURCE_LABELS,

  /**
   * Layer containing speed types.
   */
  WMS_USER_ROAD_SPEED_TYPES,

  /**
   * Layer containing road total vehicles per day.
   */
  WMS_USER_ROAD_TOTAL_VEHICLES_PER_DAY_LIGHT_TRAFFIC;
  
  public static final String WMS_USER_PREFIX = "WMS_USER_";

  public String getLayerName(final ProductType productType) {
    return productType.getLayerName(name().toLowerCase());
  }
  
  public ArrayList<String> getRangeValues() {
    return null;
  }

}
