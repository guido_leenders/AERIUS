/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultReady;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service providing deposition calculation functionality.
 */
@RemoteServiceRelativePath(ServiceURLConstants.CALCULATE_SERVICE_PATH)
public interface CalculateService extends RemoteService {

  /**
   * Initialize the calculation.
   * @param scenario The scenario to calculate
   * @param lastCalculationKey the of the last calculation started by the user
   * @return A unique string indicating this calculation. The key to use when fetching updates about the calculation.
   * @throws AeriusException on error
   */
  String startCalculation(CalculatedScenario scenario, String lastCalculationKey) throws AeriusException;

  /**
   *
   * @param lastCalculationKey key of the last calculation performed
   * @return A unique string indicating this calculation. The key to use when fetching updates about the calculation.
   * @throws AeriusException
   */
  String startCalculation(String lastCalculationKey) throws AeriusException;

  /**
   * Rather than start a calculation from scratch, retrieve the given Calculations the same way a startCalculation would return them.
   * @param calculations Calculations to track
   * @throws AeriusException on error
   */
  void trackCalculation(String calculationKey, ArrayList<Calculation> calculations) throws AeriusException;

  /**
   * Get the initialization result. Can be null while the calculation hasn't been initialized properly yet.
   * @param calculationKey the key of the last calculation started by the user
   * @return {@link CalculationInitResult}. The object containing information available after initialization (like calculation IDs).
   * @throws AeriusException on error
   */
  CalculationInitResult getInitCalculationResult(String calculationKey) throws AeriusException;

  /**
   * Cancel the calculation associated with the given calculation key.
   * @param calculationKey The key for the calculation.
   * @throws AeriusException on error.
   */
  void cancelCalculation(String calculationKey) throws AeriusException;

  /**
   * Returns status information about calculation progress in term of highest value calculated by distance.
   * @param calculationKey The key for the calculation.
   * @return returns the {@link ResultHighValuesByDistances} or null if nothing present.
   * @throws AeriusException on error.
   */
  ResultHighValuesByDistances getResultHighValuesByDistances(String calculationKey) throws AeriusException;

  /**
   * Check if there is a calculation result ready to be fetched.
   * @param calculationKey The key of the calculation.
   * @return {@link CalculationResultReady} if a result is ready to be fetched, null otherwise
   * @throws AeriusException on error
   */
  CalculationResultReady getCalculationResultReady(String calculationKey) throws AeriusException;

  /**
   * Return the calculation result points for the calculation. Only works for CUSTOM_POINTS calculations.
   * @param calculationKey The key for the calculation.
   * @return calculation result points for CUSTOM_POINTS calculations or an empty list otherwise.
   * @throws AeriusException on error
   */
  ArrayList<PartialCalculationResult> getCalculationResultPoints(String calculationKey) throws AeriusException;

  /**
   * Get the summary results for a calculation.
   *
   * @param calculationKey The key of the calculation to get summary results for.
   * @return summary object
   * @throws AeriusException exception in case of error
   */
  CalculationSummary getSummaryInfo(String calculationKey) throws AeriusException;

  /**
   * Get the summary results for a calculation or comparison calculation.
   * If both calculation id's > 0 then a comparison summary is created else a single calculation summary is returned.
   *
   * Unsure if this method will stay around, it might be removed when the workers get their own database
   * (in which case it'll be quite hard to do this unless we send along which database is used or sumsuch).
   *
   * @param calculationId1 calculation id of calculation 1
   * @param calculationId2 calculation id of calculation 2
   * @param substances substances to get summary results
   * @return summary object
   * @throws AeriusException exception in case of error
   */
  CalculationSummary getSummaryInfo(CalculationType calculationType, int calculationId1, int calculationId2,
      ArrayList<Substance> substances) throws AeriusException;
}
