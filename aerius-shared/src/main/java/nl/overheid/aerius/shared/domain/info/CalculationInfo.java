/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Object containing information about any specific calculation.
 *
 * - The receptor with the highest KDW exceeding deposition
 * - The receptor with the highest deposition
 */
public class CalculationInfo implements Serializable {
  private static final long serialVersionUID = -565396610529625582L;

  private int calculationId;

  private ArrayList<DepositionMarker> depositionMarkers;

  // Needed for GWT. 
  public CalculationInfo() {
  }

  public CalculationInfo(final int calculationId) {
    this.calculationId = calculationId;
  }

  public int getCalculationId() {
    return calculationId;
  }

  public ArrayList<DepositionMarker> getDepositionMarkers() {
    return depositionMarkers;
  }

  public void setDepositionMarkers(final ArrayList<DepositionMarker> depositionMarkers) {
    this.depositionMarkers = depositionMarkers;
  }
}
