/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import java.util.ArrayList;

/**
 *  Result info type.
 */
public enum ResultInfoType {
  /**
   * View by total deposition.
   */
  TOTAL_DEPOSITION(false, false, false),
  /**
   * View by maximum deposition (single variant only).
   */
  MAXIMUM_DEPOSITION(true, false, true),
  /**
   * View by maximum increase (comparison and utilisation only).
   */
  MAXIMUM_INCREASE(false, true, true),
  /**
   * View by average deposition.
   */
  AVERAGE_DEPOSITION(true, true, true),
  /**
   * View by percentage KDW.
   */
  PERCENTAGE_KDW(true, true, false),
  /**
   * View by Deposition space rules.
   */
  DEPOSITION_SPACE(true, true, false);

  private boolean useInSingle;
  private boolean useInComparison;
  private boolean useInUtilisation;

  private ResultInfoType(final boolean useInSingle, final boolean useInComparison, final boolean useInUtilisation) {
    this.useInSingle = useInSingle;
    this.useInComparison = useInComparison;
    this.useInUtilisation = useInUtilisation;
  }

  public static ResultInfoType[] getSingleValues() {
    return getValues(true, false, false);
  }

  public static ResultInfoType[] getComparisonValues() {
    return getValues(false, true, false);
  }

  public static ResultInfoType[] getUtilisationValues() {
    return getValues(false, false, true);
  }

  private static ResultInfoType[] getValues(final boolean useInSingle, final boolean useInComparison, final boolean useInUtilisation) {
    final ArrayList<ResultInfoType> result = new ArrayList<ResultInfoType>();
    for (int i = 0; i < values().length; ++i) {
      final ResultInfoType item = values()[i];
      boolean addItem = false;
      if (useInSingle && item.useInSingle) {
        addItem = true;
      } else if (useInComparison && item.useInComparison) {
        addItem = true;
      } else if (useInUtilisation && item.useInUtilisation) {
        addItem = true;
      }
      if (addItem) {
        result.add(item);
      }
    }

    return result.toArray(new ResultInfoType[result.size()]);
  }
}
