/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.user.UserProfile;

/**
 *
 */
public abstract class AdminUserContext implements UserContext {

  private static final long serialVersionUID = 4062944353631990733L;

  private UserProfile userProfile;

  public abstract <T extends Filter> T getFilter(Class<T> filterClass);

  public UserProfile getUserProfile() {
    return userProfile;
  }

  public void setUserProfile(final UserProfile userProfile) {
    this.userProfile = userProfile;
  }

}
