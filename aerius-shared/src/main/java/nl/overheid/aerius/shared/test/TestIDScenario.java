/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.test;

public final class TestIDScenario {
  public static final String CONNECT_LOGIN_TEXT = "connect-login-text";
  public static final String CONNECT_LOGIN_BUTTON = "connect-login-button";
  public static final String CONNECT_GENERATE_TEXT = "connect-generate-text";
  public static final String CONNECT_GENERATE_BUTTON = "connect-generate-button";

  public static final String CONNECT_OVERVIEW_NEXT = "connect-overview-next";
  public static final String CONNECT_OVERVIEW_PROJECT_KEY = "connect-overview-project-key";
  public static final String CONNECT_OVERVIEW_REQUEST_EXPORT = "connect-overview-request-export";
  public static final String CONNECT_OVERVIEW_TOGGLE_STATUS = "connect-overview-toggle-status";

  public static final String CONNECT_OVERVIEW_TABLE = "connect-overview-table";
  public static final String CONNECT_OVERVIEW_TABLE_INFO = "connect-overview-table-info";
  public static final String CONNECT_OVERVIEW_TABLE_STATE = "connect-overview-table-state";
  public static final String CONNECT_OVERVIEW_TABLE_JOB = "connect-overview-table-job";
  public static final String CONNECT_OVERVIEW_TABLE_CANCEL_JOB = "connect-overview-table-cancel-job";

  public static final String CONNECT_CONFIGURE_OPTIONS_COLLAPSIBLE = "connect-configure-options-collapsible";
  public static final String CONNECT_CONFIGURE_IMPORT = "connect-configure-import";
  public static final String CONNECT_CONFIGURE_CALCULATE = "connect-configure-calculate";
  public static final String CONNECT_CONFIGURE_CANCEL = "connect-configure-cancel";

  public static final String CONNECT_UTIL_SELECT = "connect-util-select-";
  public static final String CONNECT_UTIL_IMPORT = "connect-util-import";
  public static final String CONNECT_UTIL_OPTION_PANEL = "connect-util-option-panel";
  public static final String CONNECT_UTIL_OPTION_PANEL_LABEL = "connect-util-option-panel-label";
  public static final String CONNECT_UTIL_VALIDATE_AS_PP_CHECKBOX = "connect-util-validate-as-pp-checkbox";
  public static final String CONNECT_UTIL_ONLYINCREASE_CHECKBOX = "connect-util-onlyincrease-checkbox";
  public static final String CONNECT_UTIL_VALIDATE_LABEL = "connect-util-validate-label";
  public static final String CONNECT_UTIL_SUCCESS_LABEL = "connect-util-success-label";
  public static final String CONNECT_UTIL_WARNING_LABEL = "connect-util-warning-label";
  public static final String CONNECT_UTIL_ERROR_LABEL = "connect-util-error-label";
  public static final String CONNECT_UTIL_CANCEL = "connect-util-cancel";
  public static final String CONNECT_UTIL_NEXT = "connect-util-next";

  public static final String CALCULATION_CONFIGURATION_NAME = "calculation-configuration-name";
  public static final String CALCULATION_CONFIGURATION_FORM = "calculation-configuration-form";
  public static final String CALCULATION_CONFIGURATION_TYPE = "calculation-configuration-type";
  public static final String CALCULATION_CONFIGURATION_YEAR = "calculation-configuration-year";
  public static final String CALCULATION_CONFIGURATION_TEMPORARY_PROJECT = "calculation-configuration-checkbox-temporary-project";
  public static final String CALCULATION_CONFIGURATION_TEMPORARY_PROJECT_YEARS = "calculation-configuration-temporary-projectyears-listbox";
  public static final String CALCULATION_CONFIGURATION_PRIORITY_PERMIT = "calculation-configuration-permit-calculation-radius";
  public static final String CALCULATION_CONFIGURATION_PRIORITY_PERMIT_RADIUS =
      "calculation-configuration-permit-calculation-radius-listbox";

  public static final String CALCULATION_DETAIL_TABLE = "calculation-detail-table";
  public static final String CALCULATION_DETAIL_TABLE_FILENAME = "calculation-detail-table-filename";
  public static final String CALCULATION_DETAIL_TABLE_SITUATION = "calculation-detail-table-situation";

  private TestIDScenario() {}
}
