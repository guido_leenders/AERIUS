/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.util.ArrayList;
import java.util.HashSet;

import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 *
 */
public abstract class AdminContext extends AERIUS2Context {

  private static final long serialVersionUID = 7788321419159365984L;

  private ArrayList<Authority> authorities;
  private HashSet<AdminUserRole> roles = new HashSet<>();

  public ArrayList<Authority> getAuthorities() {
    return authorities;
  }

  public HashSet<AdminUserRole> getRoles() {
    return roles;
  }

  public void setRoles(final HashSet<AdminUserRole> roles) {
    this.roles = roles;
  }

  public void setAuthorities(final ArrayList<Authority> authorities) {
    this.authorities = authorities;
  }

  public abstract Permission[] getPermissions();
}
