/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.user;

/**
 * Data object of user profile, including admin specific properties.
 *
 * NOTE: There are no longer any admin specific properties, this does not mean this class is no longer relevant.
 */
public class AdminUserProfile extends UserProfile {
  private static final long serialVersionUID = -8513857709068762168L;

  private boolean enabled;

  @Override
  public AdminUserProfile copy() {
    return copyTo(new AdminUserProfile());
  }

  public <U extends AdminUserProfile> U copyTo(final U copy) {
    super.copyTo(copy);

    copy.setEnabled(enabled);

    return copy;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }

  @Override
  public String toString() {
    return "AdminUserProfile [enabled=" + enabled + "] " + super.toString();
  }
}
