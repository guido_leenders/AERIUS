/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Services for admin.
 */
@RemoteServiceRelativePath(ServiceURLConstants.ADMIN_SERVICE_PATH)
public interface AdminService extends RemoteService {
  /**
   * Update the given user.
   *
   * @param profile Profile to update.
   *
   * @throws AeriusException On error.
   */
  void updateUser(AdminUserProfile profile) throws AeriusException;

  /**
   * Add the given user to the set.
   *
   * @param profile Profile to add.
   *
   * @return UserProfile as it exists in the data store.
   *
   * @throws AeriusException On error.
   */
  AdminUserProfile createUser(AdminUserProfile profile) throws AeriusException;

  /**
   * Delete the given user.
   *
   * @param profile Profile to delete.
   *
   * @throws AeriusException On error.
   */
  void removeUser(AdminUserProfile profile) throws AeriusException;

  /**
   * Retrieve a list of users given a filter.
   *
   * @param filter The filter to filter users with.
   *
   * @return A list of users.
   *
   * @throws AeriusException On error.
   */
  ArrayList<AdminUserProfile> getUsers(UserManagementFilter filter) throws AeriusException;

  /**
   * Reset the password for the given user.
   *
   * @param profile UserProfile to reset the password for.
   *
   * @throws AeriusException On error.
   */
  void resetPassword(AdminUserProfile profile) throws AeriusException;

  AdminUserRole createRole(AdminUserRole role) throws AeriusException;

  void updateRole(AdminUserRole role) throws AeriusException;
}
