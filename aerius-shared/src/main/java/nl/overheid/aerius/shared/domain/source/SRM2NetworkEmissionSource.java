/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Composite EmissionSource object for Road networks. The object contains a list of the road emission sources.
 */
public class SRM2NetworkEmissionSource extends EmissionSource implements VisitableEmissionSource {

  private static final long serialVersionUID = -1785877722526525343L;

  private ArrayList<EmissionSource> sources = new ArrayList<EmissionSource>();

  public SRM2NetworkEmissionSource() {
    setYearDependent(true);
    setEmissionPerUnit(true);
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public SRM2NetworkEmissionSource copy() {
    return copyTo(new SRM2NetworkEmissionSource());
  }

  public <E extends SRM2NetworkEmissionSource> E copyTo(final E copy) {
    super.copyTo(copy);
    for (final EmissionSource emissionSource : sources) {
      copy.getEmissionSources().add(emissionSource.copy());
    }
    return copy;
  }

  @Override
  protected double getEmission(final EmissionValueKey key, final double weight) {
    double emission = 0;
    for (final EmissionSource emissionSource : sources) {
      emission += emissionSource.getEmission(key);
    }
    return emission;
  }

  public ArrayList<EmissionSource> getEmissionSources() {
    return sources;
  }

  @Override
  public boolean hasAnyEmissions() {
    return !sources.isEmpty();
  }
}
