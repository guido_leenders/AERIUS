/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Class for standard Farm Lodging emissions, based on a category.
 */
public class FarmLodgingStandardEmissions extends FarmLodgingEmissions {

  private static final long serialVersionUID = 7407413470930432816L;

  private static final double ROUND_FODDER = 5.0;

  private FarmLodgingCategory category;
  private FarmLodgingSystemDefinition systemDefinition;

  private ArrayList<FarmAdditionalLodgingSystem> lodgingAdditional = new ArrayList<FarmAdditionalLodgingSystem>();
  private ArrayList<FarmReductiveLodgingSystem> lodgingReductive = new ArrayList<FarmReductiveLodgingSystem>();
  private ArrayList<FarmFodderMeasure> fodderMeasures = new ArrayList<FarmFodderMeasure>();

  @NotNull
  public FarmLodgingCategory getCategory() {
    return category;
  }

  public void setCategory(final FarmLodgingCategory category) {
    this.category = category;
  }

  public FarmLodgingSystemDefinition getSystemDefinition() {
    return systemDefinition;
  }

  public void setSystemDefinition(final FarmLodgingSystemDefinition systemDefinition) {
    this.systemDefinition = systemDefinition;
  }

  public ArrayList<FarmAdditionalLodgingSystem> getLodgingAdditional() {
    return lodgingAdditional;
  }

  public void setLodgingAdditional(final ArrayList<FarmAdditionalLodgingSystem> lodgingAdditional) {
    this.lodgingAdditional = lodgingAdditional;
  }

  public ArrayList<FarmReductiveLodgingSystem> getLodgingReductive() {
    return lodgingReductive;
  }

  public void setLodgingReductive(final ArrayList<FarmReductiveLodgingSystem> lodgingReductive) {
    this.lodgingReductive = lodgingReductive;
  }

  public ArrayList<FarmFodderMeasure> getFodderMeasures() {
    return fodderMeasures;
  }

  public void setFodderMeasures(final ArrayList<FarmFodderMeasure> fodderMeasures) {
    this.fodderMeasures = fodderMeasures;
  }

  /**
   * Get emission of the base type (without additional or reductive lodging systems or fodder measures).
   */
  public double getFlatEmission(final EmissionValueKey key) {
    return getAmount() * getEmissionFactor(key);
  }

  /**
   * Get emission of the base type (without additional or reductive lodging systems or fodder measures)
   * and without any limiting applied.
   */
  public double getUnconstrainedFlatEmission(final EmissionValueKey key) {
    return getAmount() * getUnconstrainedEmissionFactor(key);
  }

  /**
   * Get emission upto the given additional lodging system (so still excludes all reductive systems and fodder measures).<br>
   * For use in the source popup.<br>
   * For the flat emission factor without any additional systems, use {@link #getFlatEmission(EmissionValueKey)}.
   */
  public double getEmissionUpToAdditional(final EmissionValueKey key, final FarmAdditionalLodgingSystem uptoSystem) {
    // Get emission of base type
    double emission = getFlatEmission(key);

    // Add emissions of additional types
    for (final FarmAdditionalLodgingSystem additionalSystem : lodgingAdditional) {
      emission += additionalSystem.getEmission(key);
      if (additionalSystem.equals(uptoSystem)) {
        return emission;
      }
    }

    return emission;
  }

  /**
   * Get emission upto the given reductive lodging system (so already includes all additional systems but not the fodder measures).<br>
   * For use in the source popup.<br>
   * For the emission factor without any reductive systems, use {@link #getEmissionUpToAdditional}.<br>
   * For the flat emission factor without any additional or reductive systems, use {@link #getFlatEmission(EmissionValueKey)}.
   */
  public double getEmissionUpToReductive(final EmissionValueKey key, final FarmReductiveLodgingSystem uptoSystem) {
    // Get emission of base type + additional types
    double emission = getEmissionUpToAdditional(key, null);

    // Reduce with emission factors of reductive types
    for (final FarmReductiveLodgingSystem reductiveSystem : lodgingReductive) {
      emission *= reductiveSystem.getEmissionFactor(key);
      if (reductiveSystem.equals(uptoSystem)) {
        return emission;
      }
    }

    return emission;
  }

  /**
   * Get reduction factor for the combination of all fodder measures in this lodging system. These can always only be considered
   * as a whole, not separate.<br>
   * Invalid fodder measures (not applicable to the animal category of the lodging type) are not taken into account because
   * then the proportion factors are lacking.<br>
   * Rounded to 5% if more than one measure.<br>
   * Also used in source popup.
   */
  public double getReductionFactorCombinedFodderMeasures(final EmissionValueKey key) {
    double reductionFactor;
    if (fodderMeasures.isEmpty()) {
      reductionFactor = 0.0;
    } else if (fodderMeasures.size() == 1) {
      return 1 - fodderMeasures.get(0).getEmissionFactorTotal(key);
    } else {
      // Only measures with the same animal category as the main category may be inputted by the user. Thus we can assume
      // the ammonia proportions are the same for each measure; we'll simply take 'm from the first.
      if (!fodderMeasures.get(0).getCategory().canApplyToFarmLodgingCategory(category)) {
        reductionFactor = 0.0;
      } else {
        double emissionFactorFloor = fodderMeasures.get(0).getProportionFloor(category);
        double emissionFactorCellar = fodderMeasures.get(0).getProportionCellar(category);
        for (final FarmFodderMeasure fodderMeasure : fodderMeasures) {
          emissionFactorFloor *= fodderMeasure.getEmissionFactorFloor(key);
          emissionFactorCellar *= fodderMeasure.getEmissionFactorCellar(key);
        }
        reductionFactor = 1 - emissionFactorFloor - emissionFactorCellar;

        // Round to 5%
        reductionFactor = Math.round(reductionFactor * SharedConstants.PERCENTAGE_TO_FRACTION / ROUND_FODDER)
            / (double) SharedConstants.PERCENTAGE_TO_FRACTION * ROUND_FODDER;
      }
    }

    return reductionFactor;
  }

  /**
   * Get full emissions (of the base type and including all additional and reductive lodging systems and fodder measures).
   *
   * Note that fodder measures cannot be considered per measure, only as one reduction factor for all measures at once,
   * so there is no 'getEmissionUpToFodderMeasure' method, it is included here.
   */
  @Override
  public double getEmission(final EmissionValueKey key) {
    return getEmissionUpToReductive(key, null) * (1 - getReductionFactorCombinedFodderMeasures(key));
  }

  /**
   * Whether the emission factor of the base type should be (is being) limited.
   */
  public boolean isEmissionFactorConstrained() {
    boolean isConstrained = false;

    // If the base system is not a scrubber, and has at least 70% lower emissions than its
    // corresponding traditional system ...
    if (!category.isScrubber() && category.shouldConstrainReductionFactor()) {
      // ... then in case any scrubbers are stacked on, limit that emission reduction (at 70%),
      // which in turn affects the emission factor.
      for (final FarmAdditionalLodgingSystem additionalSystem : lodgingAdditional) {
        isConstrained = isConstrained || additionalSystem.getCategory().isScrubber();
      }
      for (final FarmReductiveLodgingSystem reductiveSystem : lodgingReductive) {
        isConstrained = isConstrained || reductiveSystem.getCategory().isScrubber();
      }
    }

    return isConstrained;
  }

  /**
   * Always return the normal emission factor without any limiting applied.
   * For use in the source popup.
   */
  public double getUnconstrainedEmissionFactor(final EmissionValueKey key) {
    if (category != null && key != null && Substance.NH3 == key.getSubstance()) {
      return category.getEmissionFactor();
    } else {
      return 0;
    }
  }

  /**
   * Get emission factor of the base type (could be limited).
   */
  public double getEmissionFactor(final EmissionValueKey key) {
    final double emissionFactor;
    if (!isEmissionFactorConstrained()) {
      emissionFactor = getUnconstrainedEmissionFactor(key);
    } else if (category != null && key != null && Substance.NH3 == key.getSubstance()) {
      emissionFactor = category.getConstrainedEmissionFactor();
    } else {
      emissionFactor = 0;
    }
    return emissionFactor;
  }

  /**
   * Determine if all extra lodging systems or fodder measures are allowed for the chosen lodging category.
   * @return true if not all extra systems or measures are allowed, false if not.
   */
  public boolean isValid() {
    boolean valid = true;
    for (final FarmAdditionalLodgingSystem row : lodgingAdditional) {
      if (!getCategory().canStackAdditionalLodgingSystemCategory(row.getCategory())) {
        valid = false;
        break;
      }
    }
    for (final FarmReductiveLodgingSystem row : lodgingReductive) {
      if (!getCategory().canStackReductiveLodgingSystemCategory(row.getCategory())) {
        valid = false;
        break;
      }
    }
    for (final FarmFodderMeasure row : fodderMeasures) {
      if (!getCategory().canStackFodderMeasureCategory(row.getCategory())) {
        valid = false;
        break;
      }
    }
    return valid;
  }

  /**
   * Determine if their are any extra lodging systems or fodder measures.
   * @return true if there are any extra lodging systems or fodder measures, false if not.
   */
  public boolean hasStacking() {
    return !getLodgingAdditional().isEmpty() || !getLodgingReductive().isEmpty() || !getFodderMeasures().isEmpty();
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = super.getStateHash();
    result = prime * result + category.getStateHash();
    for (final FarmAdditionalLodgingSystem additionalSystem : lodgingAdditional) {
      result = prime * result + additionalSystem.getStateHash();
    }
    for (final FarmReductiveLodgingSystem reductiveSystem : lodgingReductive) {
      result = prime * result + reductiveSystem.getStateHash();
    }
    for (final FarmFodderMeasure fodderMeasure : fodderMeasures) {
      result = prime * result + fodderMeasure.getStateHash();
    }
    return result;
  }

  @Override
  public FarmLodgingStandardEmissions copy() {
    return copyTo(new FarmLodgingStandardEmissions());
  }

  protected <U extends FarmLodgingStandardEmissions> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setCategory(getCategory() == null ? null : getCategory());
    copy.setSystemDefinition(getSystemDefinition() == null ? null : getSystemDefinition().copy());

    for (final FarmAdditionalLodgingSystem additionalSystem : lodgingAdditional) {
      copy.getLodgingAdditional().add(additionalSystem.copy());
    }
    for (final FarmReductiveLodgingSystem reductiveSystem : lodgingReductive) {
      copy.getLodgingReductive().add(reductiveSystem.copy());
    }
    for (final FarmFodderMeasure fodderMeasure : fodderMeasures) {
      copy.getFodderMeasures().add(fodderMeasure.copy());
    }

    return copy;
  }

  @Override
  public <T> T accept(final EmissionSubSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "FarmLodgingCustomEmissions[" + super.toString() + ", category=" + category + ", lodgingAdditional=" + lodgingAdditional
        + ", lodgingReductive" + lodgingReductive + ", fodderMeasures" + fodderMeasures + "]";
  }
}
