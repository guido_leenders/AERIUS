/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;

public class DiurnalVariationSpecification extends AbstractCategory implements Serializable {
  private static final long serialVersionUID = -7613018060423657231L;

  public DiurnalVariationSpecification() {
    // Empty constructor to satisfy GWT
  }

  public DiurnalVariationSpecification(final int id, final String code) {
    super(id, code, null, null);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    final int result = super.hashCode();
    return (prime * result) + ((getCode() == null) ? 0 : getCode().hashCode());
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final DiurnalVariationSpecification other = (DiurnalVariationSpecification) obj;
    return getCode() != null && getCode().equals(other.getCode()) && super.equals(other);
  }

  /**
   * Copies object.
   *
   * @return new instance of this object
   */
  public DiurnalVariationSpecification copy() {
    return copyTo(new DiurnalVariationSpecification());
  }

  protected <C extends DiurnalVariationSpecification> C copyTo(final C copy) {
    copy.setId(getId());
    copy.setCode(getCode());
    copy.setName(getName());
    copy.setDescription(getDescription());
    return copy;
  }

  @Override
  public String toString() {
    return "DiurnalVariationSpecification [code=" + getCode() + ", id=" + getId() + "]";
  }
}
