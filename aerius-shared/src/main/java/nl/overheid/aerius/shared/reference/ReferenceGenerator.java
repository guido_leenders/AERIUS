/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

/**
 * Interface for implementations of reference generation algorithms. There can be different versions. This
 * version id is embedded in the reference string itself.
 */
interface ReferenceGenerator {

  /**
   * Return the ReferenceGenerator version id.
   */
  byte getGeneratorVersion();

  /**
   * Generates a reference of the given type.
   *
   * @throws IllegalArgumentException If the type is not supported.
   */
  String generateReference(final ReferenceType type);

  /**
   * Validates that a string is a valid reference according to this ReferenceGenerator:
   * <ul>
   * <li>It must contain the correct checksum of its data part</li>
   * <li>It must match the given reference type</li>
   * <li>It must match the version of the ReferenceGenerator</li>
   * <li>The reference type must be supported by this ReferenceGenerator</li>
   * </ul>
   */
  boolean validateReference(final ReferenceType type, final String reference);

  /**
   * Validates that a string is a valid reference according to this ReferenceGenerator:
   * <ul>
   * <li>It must contain the correct checksum of its data part</li>
   * <li>It must match the version of the ReferenceGenerator</li>
   * </ul>
   */
  boolean validateReference(final String reference);

  /**
   * Attempts to find the version used to generate the reference.
   * Note that when the version of the reference does not match the version of the ReferenceGenerator,
   * this function might throw an exception suggesting the reference is invalid.
   *
   * @throws InvalidReferenceException When the version could not be determined.
   */
  byte getVersion(final String reference) throws InvalidReferenceException;
}
