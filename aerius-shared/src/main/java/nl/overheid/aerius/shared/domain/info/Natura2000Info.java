/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.util.ArrayList;

import nl.overheid.aerius.geo.shared.WKTGeometry;

/**
 * Bean containing area information.
 */
public class Natura2000Info extends AssessmentArea {

  private static final long serialVersionUID = 6711325276797421764L;

  private int natura2000AreaId;
  private String contractor;
  private EcologyQualityType ecologyQualityType;
  private String environment;
  private long surface;
  private String protection;
  private String status;
  private WKTGeometry geometry;

  private ArrayList<HabitatInfo> habitats;

  public int getNatura2000AreaId() {
    return natura2000AreaId;
  }

  public void setNatura2000AreaId(final int natura2000AreaId) {
    this.natura2000AreaId = natura2000AreaId;
  }

  public String getContractor() {
    return contractor;
  }

  public void setContractor(final String contractor) {
    this.contractor = contractor;
  }

  public EcologyQualityType getEcologyQualityType() {
    return ecologyQualityType;
  }

  public void setEcologyQualityType(final EcologyQualityType ecologyQualityType) {
    this.ecologyQualityType = ecologyQualityType;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(final String environment) {
    this.environment = environment;
  }

  public long getSurface() {
    return surface;
  }

  public void setSurface(final long surface) {
    this.surface = surface;
  }

  public String getProtection() {
    return protection;
  }

  public void setProtection(final String protection) {
    this.protection = protection;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(final String status) {
    this.status = status;
  }

  public ArrayList<HabitatInfo> getHabitats() {
    return habitats;
  }

  public void setHabitats(final ArrayList<HabitatInfo> arrayList) {
    this.habitats = arrayList;
  }

  public WKTGeometry getGeometry() {
    return geometry;
  }

  public void setGeometry(final WKTGeometry geometry) {
    this.geometry = geometry;
  }
}
