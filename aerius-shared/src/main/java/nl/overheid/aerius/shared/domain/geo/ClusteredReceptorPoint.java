/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.geo.shared.Point;

/**
 * A {@link ClusteredReceptorPoint} is a point in the center of a list of points.
 * This point can also be snapped to the hexagon grid. The max distance is the
 * distance of the farthest distance point to the center point.
 */
public class ClusteredReceptorPoint<E extends Point> extends AeriusPoint implements Serializable {

  private static final long serialVersionUID = 8878187709344694922L;

  private ArrayList<E> points;

  private double maxDistance = -1;

  public ClusteredReceptorPoint() {
    // Default GWT constructor
  }

  public ClusteredReceptorPoint(final ArrayList<E> points, final ReceptorUtil receptorUtil) {
    this.points = points;
    double x = 0;
    double y = 0;

    for (final Point i : points) {
      x += i.getX();
      y += i.getY();
    }

    // (Exact) average location
    setX(x / points.size());
    setY(y / points.size());
    if (receptorUtil != null) {
      receptorUtil.attachReceptorToGrid(this);
    }
  }

  /**
   * Returns the distance between the center of the cluster and the farest point
   * in the cluster.
   *
   * @return farest distance in cluster from center
   */
  public double getMaxDistance() {
    if (maxDistance == -1) {
      maxDistance = maxDistanceToPoint(this);
    }
    return maxDistance;
  }

  public ArrayList<E> getPoints() {
    return points;
  }

  /**
   * Returns the distance between the point and the nearest point in the cluster.
   *
   * @param point point to determine distance to
   * @return nearest distance between point and cluster
   */
  public double minDistanceToPoint(final Point point) {
    int minDistance = Integer.MAX_VALUE;

    for (final Point b : points) {
      final int dist = (int) point.distance(b);

      if (dist < minDistance) {
        minDistance = dist;
      }
    }
    return minDistance;
  }

  /**
   * Returns the distance between the point and the farthest point in the cluster.
   *
   * @param point point to determine distance to
   * @return maximum distance between point and cluster
   */
  public double maxDistanceToPoint(final Point point) {
    int maxDistance = 0;

    for (final Point b : points) {
      final int dist = (int) point.distance(b);

      if (dist > maxDistance) {
        maxDistance = dist;
      }
    }
    return maxDistance;
  }
}
