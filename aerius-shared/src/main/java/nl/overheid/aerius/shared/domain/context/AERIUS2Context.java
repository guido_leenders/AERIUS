/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.context;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;

/**
 * Context containing data that is used by all AERIUS-II specific projects.
 */
public class AERIUS2Context extends Context {

  private static final long serialVersionUID = 5260371904390797488L;

  // Substance
  private ArrayList<Substance> substances;

  private SectorCategories categories;

  public ArrayList<Substance> getSubstances() {
    return substances;
  }

  public void setSubstances(final ArrayList<Substance> substances) {
    this.substances = substances;
  }

  public SectorCategories getCategories() {
    return categories;
  }

  public void setCategories(final SectorCategories categories) {
    this.categories = categories;
  }

  @Override
  public String toString() {
    return super.toString() + " AERIUS2Context [substances=" + substances + ", categories=" + categories + "]";
  }
}
