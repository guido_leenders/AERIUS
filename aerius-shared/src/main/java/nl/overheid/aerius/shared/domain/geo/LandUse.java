/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

/**
 * The landuse for a receptor or calculation point.
 */
public enum LandUse {

  /**
   * RIVM DEPAC class: gras.
   */
  GRASS(1, "grasland"),

  /**
   * RIVM DEPAC class: landbouwgrond.
   */
  ARABLE_LAND(2, "bouwland"),

  /**
   * RIVM DEPAC class: boomgaard.
   */
  PERMANENT_CROPS(3, "vaste gewassen"),

  /**
   * RIVM DEPAC class: naaldbos.
   */
  CONIFEROUS_FOREST(4, "naaldbos"),

  /**
   * RIVM DEPAC class: loofbos.
   */
  DECIDUOUS_FOREST(5, "loofbos"),

  /**
   * RIVM DEPAC class: water.
   */
  WATER(6, "water"),

  /**
   * RIVM DEPAC class: bebouwing.
   */
  URBAN(7, "bebouwing"),

  /**
   * RIVM DEPAC class: overige natuur.
   */
  OTHER_NATURE(8, "overige natuur"),

  /**
   * RIVM DEPAC class: kale grond.
   */
  DESERT(9, "kale grond");

  private final int option;
  private final String databaseName;

  private LandUse(final int option, final String databaseName) {
    this.option = option;
    this.databaseName = databaseName;
  }

  public int getOption() {
    return option;
  }

  public String getDatabaseName() {
    return databaseName;
  }

  /**
   * Safely returns a LandUse. It is case independent and returns null in
   * case the input was null or the String could not be found.
   *
   * @param value value to convert
   * @return LandUse or null if no valid input
   */
  public static LandUse safeValueOf(final String value) {
    LandUse returnLandUse = null;
    for (final LandUse landUse : values()) {
      if (landUse.databaseName.equalsIgnoreCase(value)) {
        returnLandUse = landUse;
      }
    }
    return returnLandUse;
  }

  /**
   * Find a LandUse by OPS option value. It returns null in
   * case the input could not be matched to an option.
   *
   * @param option The (OPS) option to get the landuse for.
   * @return LandUse or null if no valid input
   */
  public static LandUse getByOption(final Integer option) {
    LandUse returnLandUse = null;
    if (option != null) {
      for (final LandUse landUse : values()) {
        if (landUse.option == option) {
          returnLandUse = landUse;
        }
      }
    }
    return returnLandUse;
  }

}
