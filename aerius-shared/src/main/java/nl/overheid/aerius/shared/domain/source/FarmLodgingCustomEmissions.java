/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Class for custom Farm Lodging emissions, based on user input.
 */
public class FarmLodgingCustomEmissions extends FarmLodgingEmissions {

  private static final long serialVersionUID = 6445351886941632911L;

  private String description;
  private EmissionValues emissionFactors = new EmissionValues(false);

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public EmissionValues getEmissionFactors() {
    return emissionFactors;
  }

  /**
   * @return The NH3 emission factor. Used by UI.
   */
  public double getEmissionFactor() {
    return emissionFactors.getEmission(new EmissionValueKey(Substance.NH3));
  }

  /**
   * @param emissionFactor The NH3 emission. Used by UI.
   */
  public void setEmissionFactor(final double emissionFactor) {
    emissionFactors.setEmission(new EmissionValueKey(Substance.NH3), emissionFactor);
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = super.getStateHash();
    result = prime * result + emissionFactors.getStateHash();
    return result;
  }

  @Override
  public FarmLodgingCustomEmissions copy() {
    return copyTo(new FarmLodgingCustomEmissions());
  }

  protected <U extends FarmLodgingCustomEmissions> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setDescription(getDescription());
    copy.setEmissionFactor(getEmissionFactor());
    return copy;
  }

  @Override
  public double getEmission(final EmissionValueKey key) {
    return emissionFactors.getEmission(key) * getAmount();
  }

  @Override
  public <T> T accept(final EmissionSubSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public String toString() {
    return "FarmLodgingCustomEmissions[" + super.toString() + ", description=" + description + ", emissionValues=" + emissionFactors + "]";
  }

}
