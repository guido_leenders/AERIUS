/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Data class for calculation options.
 */
public class CalculationSetOptions implements Serializable {

  private static final long serialVersionUID = 8828076045889491934L;

  private int calculationSetOptionsId;
  private CalculationType calculationType = CalculationType.PAS;
  private double calculateMaximumRange;
  private ArrayList<Substance> substances = new ArrayList<>();
  private Set<EmissionResultKey> emissionResultKeys = new HashSet<>();
  private Integer temporaryProjectYears;
  private PermitCalculationRadiusType permitCalculationRadiusType;
  private boolean forceAggregation;
  private boolean roadOPS;

  /**
   * Convenience method to allow subclasses to easily implement copy.
   *
   * @return new instance
   */
  public CalculationSetOptions copy() {
    final CalculationSetOptions copy = new CalculationSetOptions();
    copy.setCalculationSetOptionsId(calculationSetOptionsId);
    copy.setCalculationType(calculationType);
    copy.setCalculateMaximumRange(calculateMaximumRange);
    copy.getSubstances().addAll(substances);
    copy.getEmissionResultKeys().addAll(emissionResultKeys);
    copy.setTemporaryProjectYears(temporaryProjectYears);
    copy.setPermitCalculationRadiusType(permitCalculationRadiusType);
    copy.setForceAggregation(forceAggregation);
    copy.setRoadOPS(roadOPS);
    return copy;
  }

  public int getCalculationSetOptionsId() {
    return calculationSetOptionsId;
  }

  public void setCalculationSetOptionsId(final int calculationSetOptionsId) {
    this.calculationSetOptionsId = calculationSetOptionsId;
  }

  public Profile getProfile() {
    return calculationType == CalculationType.NSL ? Profile.NSL : Profile.PAS;
  }

  public CalculationType getCalculationType() {
    return calculationType;
  }

  public ArrayList<Substance> getSubstances() {
    return substances;
  }

  public ArrayList<EmissionValueKey> getEmissionValueKeys(final int year) {
    return EmissionValueKey.getEmissionValueKeys(year, substances);
  }

  public Set<EmissionResultKey> getEmissionResultKeys() {
    return emissionResultKeys;
  }

  public void setCalculationType(final CalculationType calculationType) {
    this.calculationType = calculationType;
  }

  /**
   * Returns true if calculation is PAS calculation and Priority Project option enabled.
   *
   * @return true if condition met
   */
  public boolean isPASPriorityProject() {
    return getCalculationType() == CalculationType.PAS && getPermitCalculationRadiusType() != null;
  }

  /**
   * Returns true if the maximum range value is used by the calculation type. Meaning those types use the maximum range value to determine the
   * distance to calculate.
   * @return true if relevant
   */
  public boolean isMaximumRangeRelevant() {
    return calculationType == CalculationType.NATURE_AREA || calculationType == CalculationType.RADIUS;
  }

  /**
   * @return The maximum range for the calculation (in meters)
   */
  public double getCalculateMaximumRange() {
    return calculateMaximumRange;
  }

  /**
   * @param calculateMaximumRange The maximum range to set (in meters)
   */
  public void setCalculateMaximumRange(final double calculateMaximumRange) {
    this.calculateMaximumRange = calculateMaximumRange;
  }

  public boolean isTemporaryProjectImpact() {
    // AfterPAS hack
    return false;
  }

  public Integer getTemporaryProjectYears() {
    // AfterPAS hack
    return null;
  }

  public void setTemporaryProjectYears(final Integer temporaryProjectYears) {
    // AfterPAS hack: no-op
  }

  public PermitCalculationRadiusType getPermitCalculationRadiusType() {
    // AfterPAS hack
    return null;
  }

  /**
   * Return if the calculation has a valid RadiusType
   * @return
   */
  public boolean isPermitCalculationRadiusImpact() {
    // AfterPAS hack
    return false;
  }

  public void setPermitCalculationRadiusType(final PermitCalculationRadiusType permitCalculationRadiusType) {
    // AfterPAS hack: no-op
  }

  public boolean isForceAggregation() {
    return forceAggregation && calculationType == CalculationType.CUSTOM_POINTS;
  }

  /**
   * By default some sectors are exempt from aggregation. This forces aggregation even for those sectors.
   */
  public void setForceAggregation(final boolean forceAggregation) {
    this.forceAggregation = forceAggregation;
  }

  public boolean isRoadOPS() {
    return roadOPS && calculationType == CalculationType.CUSTOM_POINTS;
  }

  public void setRoadOPS(final boolean roadOPS) {
    this.roadOPS = roadOPS;
  }

  @Override
  public String toString() {
    return "CalculationSetOptions [calculationSetOptionsId=" + calculationSetOptionsId + ", calculationType=" + calculationType
        + ", calculateMaximumRange=" + calculateMaximumRange + ", substances=" + substances + ", emissionResultKeys=" + emissionResultKeys
        + ", temporaryProjectYears=" + temporaryProjectYears + ", permitCalculationRadiusType=" + permitCalculationRadiusType + ", forceAggregation="
        + forceAggregation + ", roadOPS=" + roadOPS + "]";
  }
}
