/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.ConsecutiveNamedList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * A collection of calculation points, named.
 *
 * CalculationPointsList keeps an internal id to make sure each point has a unique id. The {@link ConsecutiveNamedList} is exclusively to be
 * used in the client when a point is removed and a new point is added it gets inserted at the first empty slot in the list.
 */
public class CalculationPointList extends ConsecutiveNamedList<AeriusPoint> {

  private static final long serialVersionUID = -347163345676480415L;

  public CalculationPointList() {
    super(1);
  }

  /**
   * Copy this list. Will copy each calculationpoint and create a list that has the same ID for each of those copies.
   * @return A copy of this calculationpointlist.
   */
  public CalculationPointList copy() {
    final CalculationPointList copyList = new CalculationPointList();
    copyList.setName(getName());
    //don't alter the orginal list.
    final ArrayList<AeriusPoint> calculationPointList = new ArrayList<AeriusPoint>();
    for (final AeriusPoint point : this) {
      final AeriusPoint copy = point.copy();
      copy.setId(point.getId());
      calculationPointList.add(copy);
    }

    copyList.setAll(calculationPointList);
    return copyList;
  }

  public int merge(final ArrayList<AeriusPoint> result) {
    final ArrayList<AeriusPoint> removalDue = new ArrayList<AeriusPoint>();

    for (final AeriusPoint o : this) {
      for (final AeriusPoint p : result) {
        if (o.getX() == p.getX() && o.getY() == p.getY()) {
          removalDue.add(o);
          break;
        }
      }
    }

    // Remove duplicate locations out of the import list
    result.removeAll(removalDue);

    // Add new points
    for (final AeriusPoint p : result) {
      add(p);
    }

    return result.size();
  }

}
