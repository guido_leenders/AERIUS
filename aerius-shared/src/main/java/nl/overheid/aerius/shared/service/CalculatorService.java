/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.geo.DetermineCalculationPointResult;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.HasEmissionValues;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service providing miscellaneous functionality for the AERIUS calculator product.
 */
@RemoteServiceRelativePath(ServiceURLConstants.CALCULATOR_SERVICE_PATH)
public interface CalculatorService extends RemoteService {

  /**
   * Generate calculation points based on the properties.
   * <p>If the distance from sources is higher than 10 KM, calculation points will be based on natura-areas only.
   * Otherwise the combination of nature-areas and habitattypes will be used.
   *
   * @param distanceFromSource the distance from the sources
   * @param sourceLists the source list(s) to generate calculation points for
   * @return result
   * @throws AeriusException on error
   */
  DetermineCalculationPointResult determinePointsAutomatically(int distanceFromSource,
      ArrayList<EmissionSourceList> sourceLists) throws AeriusException;

  /**
   * Determine the emissions for an emissionvalues object.
   * <p>
   * Based on the actual EmissionValues class, the total emissions for the source will be determined.
   * The result can be used on client side to set the total emissions of the EmissionValues.
   *
   * @param emissionValues The EmissionValues to get the total emissions for.
   * @param keys The EmissionValueKeys to determine the total emissions for.
   * @param parent The emissionsource that is the parent of the EmissionValues supplied (needed for some cases)..
   * @return A map containing total emissions for each substance and year.
   * @throws AeriusException on error
   */
  EmissionValues getEmissions(HasEmissionValues emissionValues, ArrayList<EmissionValueKey> keys, EmissionSource parent)
      throws AeriusException;

  /**
   * Determine the waterway type based on the given geometry route.
   * <p>
   *
   * @param geometry The WKTGeometry to get waterway.
   * @return List of shipping inland waterway types.
   * @throws AeriusException on error
   */
  ArrayList<InlandWaterwayType> suggestInlandShippingWaterway(WKTGeometry geometry) throws AeriusException;
}
