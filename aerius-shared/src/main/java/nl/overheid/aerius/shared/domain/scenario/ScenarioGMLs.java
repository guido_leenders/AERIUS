/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import java.io.Serializable;

/**
 * Object contains the GML strings for a scenario.
 */
public class ScenarioGMLs implements Serializable {

  private static final long serialVersionUID = 7364175034750957423L;

  private String proposedGML;
  private String currentGML;

  public String getProposedGML() {
    return proposedGML;
  }

  public void setProposedGML(final String proposedGML) {
    this.proposedGML = proposedGML;
  }

  public String getCurrentGML() {
    return currentGML;
  }

  public void setCurrentGML(final String currentGML) {
    this.currentGML = currentGML;
  }

}
