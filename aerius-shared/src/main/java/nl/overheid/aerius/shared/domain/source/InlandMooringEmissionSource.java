/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Data class for Inland mooring ships.
 */
public class InlandMooringEmissionSource extends MooringEmissionSource<InlandMooringVesselGroup> implements Serializable {

  private static final long serialVersionUID = 8473348189332578312L;

  /**
   * Directions ships are moving. Arriving at the dock or departing from the dock.
   */
  public static enum NavigationDirection {
    /**
     * Ships arriving at the dock.
     */
    ARRIVE,
    /**
     * Ships departing from the dock.
     */
    DEPART,
  }

  /**
   * Data class for route information of a inland vessel group.
   */
  public static class InlandMooringRoute extends MooringRoute {

    private static final long serialVersionUID = 1464708323973128035L;

    private static final int HUNDRED_PERCENTAGE = 100;
    private static final double PERCENTAGE_TO_FRACTION = 0.01;

    private int percentageLaden;
    private NavigationDirection direction;

    @Override
    public InlandMooringRoute copy() {
      final InlandMooringRoute copy = copyTo(new InlandMooringRoute());
      copy.percentageLaden = percentageLaden;
      copy.direction = direction;
      return copy;
    }

    public int getPercentageLaden() {
      return percentageLaden;
    }

    public int getPercentageUnLaden() {
      return HUNDRED_PERCENTAGE - percentageLaden;
    }

    public void setPercentageLaden(final int percentageLaden) {
      this.percentageLaden = percentageLaden;
    }

    public NavigationDirection getDirection() {
      return direction;
    }

    public void setDirection(final NavigationDirection direction) {
      this.direction = direction;
    }

    /**
     * Convenience method to get the number of ships per year.
     * @param laden If true returns the number of laden ships, else unladen ships.
     * @return the number of ships per year.
     */
    public double getShipsPerYear(final boolean laden) {
      return laden ? getShipsLadenPerYear() : getShipsUnLadenPerYear();
    }

    public double getShipsLadenPerYear() {
      return getShipsPercentagePerYear(getPercentageLaden());
    }

    public double getShipsUnLadenPerYear() {
      return getShipsPercentagePerYear(getPercentageUnLaden());
    }

    double getShipsPercentagePerYear(final double percentage) {
      return getShipMovementsPerYear() * percentage * PERCENTAGE_TO_FRACTION;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + super.getStateHash();
      result = (prime * result) + percentageLaden;
      result = (prime * result) + direction.ordinal();
      return result;
    }

    @Override
    public String toString() {
      return "InlandMooringRoute [" + super.toString()
          + ", percentageLaden=" + percentageLaden + ", direction=" + direction + "]";
    }
  }

  /**
   * Data class for a group of inland vessels.
   */
  public static class InlandMooringVesselGroup extends VesselGroupEmissionSubSource<InlandShippingCategory> {

    private static final long serialVersionUID = -2141430148264069667L;

    /**
     * The number of hours per year ships reside at the dock.
     */
    private int residenceTime;

    private ArrayList<InlandMooringRoute> routes = new ArrayList<>();

    @Override
    public InlandMooringVesselGroup copy() {
      return copyTo(new InlandMooringVesselGroup());
    }

    protected <V extends InlandMooringVesselGroup> V copyTo(final V copy) {
      super.copyTo(copy);
      copy.setResidenceTime(residenceTime);
      for (final InlandMooringRoute route : routes) {
        copy.getRoutes().add(route.copy());
      }
      return copy;
    }

    public int getResidenceTime() {
      return residenceTime;
    }

    public void setResidenceTime(final int residenceTime) {
      this.residenceTime = residenceTime;
    }

    public void setRoutes(final ArrayList<InlandMooringRoute> routes) {
      this.routes = routes;
    }

    public ArrayList<InlandMooringRoute> getRoutes() {
      return routes;
    }

    public double getShipsResidenceTimeLadenPerYear() {
      return getShipsResidenceTimePerYear(true);
    }

    public double getShipsResidenceTimeUnLadenPerYear() {
      return getShipsResidenceTimePerYear(false);
    }

    // Convert ship movements to visits by dividing by 2 (1 arriving, 1 leaving)
    // Be sure to use a floating point division instead of the integer one.
    private double getShipsResidenceTimePerYear(final boolean isladen) {
      return (getShipsPerYear(isladen) * residenceTime) / 2.0;
    }

    private double getShipsPerYear(final boolean isladen) {
      double laden = 0;
      for (final InlandMooringRoute route : routes) {
        laden += route.getShipsPerYear(isladen);
      }
      return laden;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + super.getStateHash();
      result = (prime * result) + residenceTime;
      for (final InlandMooringRoute imr : routes) {
        result = (prime * result) + imr.getStateHash();
      }
      return result;
    }

    /**
     * Removes the route relation of this object. This method should be called when an object is deleted,
     * otherwise the relations are persisted.
     */
    public void detachRoutes() {
      for (final InlandMooringRoute imr : routes) {
        if (imr.getRoute() != null) {
          imr.getRoute().detachRoute(imr);
        }
      }
    }

    @Override
    public String toString() {
      return "InlandMooringVesselGroup [residenceTime=" + residenceTime + ", routes=" + routes.size() + "]";
    }
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public InlandMooringEmissionSource copy() {
    return copyTo(new InlandMooringEmissionSource());
  }

  public <E extends InlandMooringEmissionSource> E copyTo(final E copy) {
    super.copyTo(copy);
    for (final InlandMooringVesselGroup vgCopy : copy.getEmissionSubSources()) {
      for (final InlandMooringRoute imrCopy : vgCopy.getRoutes()) {
        for (int j = 0; j < copy.getInlandRoutes().size(); j++) {
          if ((imrCopy.getRoute() != null) && (imrCopy.getRoute().getId() == getInlandRoutes().get(j).getId())) {
            imrCopy.setRoute(copy.getInlandRoutes().get(j));
            break;
          }
        }
      }
    }
    return copy;
  }

  @Override
  public String toString() {
    return "InlandMooringEmissionSource [" + super.toString() + "]";
  }
}
