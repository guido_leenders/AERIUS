/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.user;

import java.util.HashSet;

public class AdminUserRole extends UserRole {
  private static final long serialVersionUID = -5972507500525337208L;

  private HashSet<String> permissions = new HashSet<>();

  public AdminUserRole() {}

  public AdminUserRole(final int id, final String name, final String color) {
    super(id, name, color);
  }

  @Override
  public AdminUserRole copy() {
    return copyTo(new AdminUserRole());
  }

  public <U extends AdminUserRole> U copyTo(final U copy) {
    super.copyTo(copy);

    copy.setPermissions(new HashSet<String>(permissions));

    return copy;
  }

  public HashSet<String> getPermissions() {
    return permissions;
  }

  public void setPermissions(final HashSet<String> permissions) {
    this.permissions = permissions;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AdminUserRole [permissions=" + permissions + "] " + super.toString();
  }
}
