/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import java.io.Serializable;

/**
 *
 */
public class HabitatFilterRangeContent implements Serializable {

  private static final long serialVersionUID = 4203936778393209737L;

  private boolean useNumberReceptors;
  private int numberOfReceptors;
  private double surface;

  public boolean isUseNumberReceptors() {
    return useNumberReceptors;
  }

  public void setUseNumberReceptors(final boolean useNumberReceptors) {
    this.useNumberReceptors = useNumberReceptors;
  }

  public int getNumberOfReceptors() {
    return numberOfReceptors;
  }

  public void setNumberOfReceptors(final int numberOfReceptors) {
    this.numberOfReceptors = numberOfReceptors;
  }

  public double getSurface() {
    return surface;
  }

  public void setSurface(final double surface) {
    this.surface = surface;
  }

  /**
   * @return number of receptors if it should use those and surface in m^2 if not.
   */
  public double getTotalValue() {
    return useNumberReceptors ? numberOfReceptors : surface;
  }

}
