/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector;



/**
 * Enum class representing the sectors for which an image is available.
 */
public enum SectorIcon {
  ABROAD,
  AVIATION,
  AVIATION_AERODROME,
  AVIATION_TAKE_OFF,
  AVIATION_TAXI,
  AVIATION_TOUCH_DOWN,
  BACKGROUND_DEPOSITION,
  CONSTRUCTION,
  CONSUMERS,
  ENERGY,
  ENERGY_POWER_PLANT,
  ENERGY_PRODUCTION_DISTRIBUTION,
  AGRICULTURE,
  FARM_FERTILIZER,
  FARM_LODGE,
  FARM_MANURE_STORAGE,
  FARM_OTHER,
  FARM_PASTURE,
  GREENHOUSE,
  HDO,
  INDUSTRY,
  INDUSTRY_BASE_MATERIAL,
  INDUSTRY_BUILDING_MATERIALS,
  INDUSTRY_CHEMICAL_INDUSTRY,
  INDUSTRY_FOOD_INDUSTRY,
  INDUSTRY_METAL_INDUSTRY,
  MEASUREMENT_CORRECTION,
  MINING,
  OFFICES_SHOPS,
  OTHER,
  PLAN,
  RAIL_EMPLACEMENT,
  RAIL_TRANSPORT,
  RECREATION,
  ROAD_CONSTRUCTION_INDUSTRY,
  ROAD_FARM,
  ROAD_CONSUMER,
  ROAD_FREEWAY,
  ROAD_OTHER,
  ROAD_NON_URBAN,
  ROAD_URBAN,
  ROAD_UNDERLYING_NETWORK,
  SHIPPING,
  SHIPPING_DOCK,
  SHIPPING_FISHING,
  SHIPPING_INLAND,
  SHIPPING_INLAND_DOCK,
  SHIPPING_MARITIME_MOORING,
  SHIPPING_MARITIME_NCP,
  SHIPPING_RECREATIONAL,
  TRAFFIC_AND_TRANSPORT,
  WASTE_MANAGEMENT,

  // Other deposition types:

  /**
   * The part of the deposition decline that is used for economic development (half the decline).
   */
  RETURNED_DEPOSITION_SPACE,
  /**
   * The part of the deposition decline that is used for economic development for Limburg (Limburg has it's own policy).
   */
  RETURNED_DEPOSITION_SPACE_LIMBURG,
  /**
   * Deposition space addition
   */
  DEPOSITION_SPACE_ADDITION;

  /**
   * Safely converts a string representation of the sector Image to this enum.
   * Used to convert values from the database into this enum. It returns OTHER
   * if the input is null or invalid.
   *
   * @param value value to convert
   * @return SectorIcon or OTHER if null or invalid input
   */
  public static SectorIcon safeValueOf(final String value) {
    if (value == null) {
      return OTHER;
    }
    try {
      return valueOf(value.toUpperCase());
    } catch (final Exception e) {
      return OTHER;
    }
  }
}
