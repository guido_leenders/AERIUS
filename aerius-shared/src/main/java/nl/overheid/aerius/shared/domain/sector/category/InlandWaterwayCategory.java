/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.Objects;

import nl.overheid.aerius.shared.domain.HasState;

/**
 * Category class for Inland Waterways.
 */
public class InlandWaterwayCategory extends AbstractCategory implements HasState {

  private static final long serialVersionUID = 2845097683238893074L;

  /**
   * The direction for a route at a point.
   */
  public enum WaterwayDirection {
    /**
     * Route is going upstream at this point.
     */
    UPSTREAM,
    /**
     * Route is going downstream at this point.
     */
    DOWNSTREAM,
    /**
     * Direction matters not for the route at this point.
     */
    IRRELEVANT;

    /**
     * @return The opposite of this direction.
     */
    public WaterwayDirection getOpposite() {
      final WaterwayDirection opposite;
      switch (this) {
      case DOWNSTREAM:
        opposite = UPSTREAM;
        break;
      case UPSTREAM:
        opposite = DOWNSTREAM;
        break;
      default:
        opposite = IRRELEVANT;
        break;
      }
      return opposite;
    }
  }

  private ArrayList<WaterwayDirection> directions = new ArrayList<>();

  public ArrayList<WaterwayDirection> getDirections() {
    return directions;
  }

  public void setDirections(final ArrayList<WaterwayDirection> directions) {
    this.directions = directions;
  }

  /**
   * Returns true if the direction on which ships move is relevant.
   * @return true if direction is relevant
   */
  public boolean isDirectionRelevant() {
    return directions.size() > 1;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    return (directions == null ? 1 : (prime * directions.hashCode())) + super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    final boolean isClass = obj != null && getClass() == obj.getClass();
    return isClass && Objects.equals(directions, ((InlandWaterwayCategory) obj).getDirections()) && super.equals(obj);
  }

  @Override
  public int getStateHash() {
    //this object is static so we can just use the id.
    final int prime = 31;
    final int result = 1;
    return prime * result + getId();
  }

  @Override
  public String toString() {
    return super.toString() + ", directions=" + directions + "]";
  }

}
