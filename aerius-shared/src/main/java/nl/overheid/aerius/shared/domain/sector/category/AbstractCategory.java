/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * Base class for categories.
 *
 * Each category should consist of the basic properties supplied in this class.
 */
public abstract class AbstractCategory implements Serializable, HasId, HasName, HasNameDescription {

  private static final long serialVersionUID = -4857284613520492584L;

  /**
   * Database key
   */
  private int id;
  /**
   * Unique code to be used as identifier in GML and such.
   */
  private String code;
  /**
   * User friendly display name
   */
  private String name;
  /**
   * (Optional) long description of the category
   */
  private String description;

  /**
   * Default public constructor.
   */
  public AbstractCategory() { /* No-op. */ }

  public AbstractCategory(final int id, final String code, final String name, final String description) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.description = description;
  }

  public String getCode() {
    return code;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public String getName() {
    return name;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "[id=" + id + ", code=" + code + ", name=" + name + ", description=" + description;
  }

  /**
   * @param categories The list of categories to search in.
   * @param id The ID of the category to get.
   * @return The proper category (or null if not found)
   */
  public static <T extends AbstractCategory> T determineById(final ArrayList<T> categories, final int id) {
    T rightCategory = null;

    // Find the correct category based on ID
    if (categories != null) {
      for (final T category : categories) {
        if (category.getId() == id) {
          rightCategory = category;
          break;
        }
      }
    }
    return rightCategory;
  }

  /**
   * @param categories The list of categories to search in.
   * @param code The (unique) code of the category to get.
   * @return The proper category (or null if not found)
   */
  public static <T extends AbstractCategory> T determineByCode(final ArrayList<T> categories, final String code) {
    T rightCategory = null;

    // Find the correct category based on code
    if (categories != null) {
      for (final T category : categories) {
        if (category.getCode().equalsIgnoreCase(code)) {
          rightCategory = category;
          break;
        }
      }
    }
    return rightCategory;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    final int result = 1;
    return prime * result + id;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && getClass() == obj.getClass() && id == ((AbstractCategory) obj).id;
  }

}
