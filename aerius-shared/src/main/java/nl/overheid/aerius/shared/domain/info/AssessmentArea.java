/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.io.Serializable;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasName;

/**
 * An assessment area, like a N2000 area.
 */
public class AssessmentArea implements Serializable, Comparable<AssessmentArea>, HasId, HasName {
  private static final long serialVersionUID = 5189013350117074019L;

  private int assessmentAreaId;
  private String name;
  private BBox bounds;

  @Override
  public int getId() {
    return assessmentAreaId;
  }

  @Override
  public void setId(final int id) {
    this.assessmentAreaId = id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  public BBox getBounds() {
    return bounds;
  }

  public void setBounds(final BBox bounds) {
    this.bounds = bounds;
  }

  @Override
  public int compareTo(final AssessmentArea o) {
    return name.compareTo(o.getName());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + assessmentAreaId;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && this.assessmentAreaId == ((AssessmentArea) obj).assessmentAreaId;
  }

  @Override
  public String toString() {
    return "AssessmentArea [assessmentAreaId=" + assessmentAreaId + ", name=" + name + ", bounds=" + bounds + "]";
  }
}
