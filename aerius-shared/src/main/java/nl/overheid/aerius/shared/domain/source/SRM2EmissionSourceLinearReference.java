/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;

public class SRM2EmissionSourceLinearReference implements Serializable {
  private static final long serialVersionUID = 9052212461634974885L;

  private double start;
  private double end;

  private Integer maxRoadSpeed;
  private Boolean strictEnforcement;

  private RoadElevation elevation;
  private Integer elevationHeight;
  private Boolean freeway;
  private Double tunnelFactor;

  private RoadSideBarrier barrierLeft;
  private RoadSideBarrier barrierRight;

  public SRM2EmissionSourceLinearReference() {
  }

  public SRM2EmissionSourceLinearReference(double start, double end) {
    this.start = start;
    this.end = end;
  }

  public double getStart() {
    return start;
  }

  public void setStart(double start) {
    this.start = start;
  }

  public double getEnd() {
    return end;
  }

  public void setEnd(double end) {
    this.end = end;
  }

  public Integer getMaxRoadSpeed() {
    return maxRoadSpeed;
  }

  public void setMaxRoadSpeed(Integer maxRoadSpeed) {
    this.maxRoadSpeed = maxRoadSpeed;
  }

  public Boolean getStrictEnforcement() {
    return strictEnforcement;
  }

  public void setStrictEnforcement(Boolean strictEnforcement) {
    this.strictEnforcement = strictEnforcement;
  }

  public RoadElevation getElevation() {
    return elevation;
  }

  public void setElevation(RoadElevation elevation) {
    this.elevation = elevation;
  }

  public Integer getElevationHeight() {
    return elevationHeight;
  }

  public void setElevationHeight(Integer elevationHeight) {
    this.elevationHeight = elevationHeight;
  }

  public Boolean getFreeway() {
    return freeway;
  }

  public void setFreeway(Boolean freeway) {
    this.freeway = freeway;
  }

  public Double getTunnelFactor() {
    return tunnelFactor;
  }

  public void setTunnelFactor(Double tunnelFactor) {
    this.tunnelFactor = tunnelFactor;
  }

  public RoadSideBarrier getBarrierLeft() {
    return barrierLeft;
  }

  public void setBarrierLeft(RoadSideBarrier barrierLeft) {
    this.barrierLeft = barrierLeft;
  }

  public RoadSideBarrier getBarrierRight() {
    return barrierRight;
  }

  public void setBarrierRight(RoadSideBarrier barrierRight) {
    this.barrierRight = barrierRight;
  }

  public SRM2EmissionSourceLinearReference copyTo(SRM2EmissionSourceLinearReference copy) {
    copy.setStart(start);
    copy.setEnd(end);
    copy.setMaxRoadSpeed(maxRoadSpeed);
    copy.setStrictEnforcement(strictEnforcement);
    copy.setElevation(elevation);
    copy.setElevationHeight(elevationHeight);
    copy.setFreeway(freeway);
    copy.setTunnelFactor(tunnelFactor);
    copy.setBarrierLeft(barrierLeft);
    copy.setBarrierRight(barrierRight);

    return copy;
  }

  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((barrierLeft == null) ? 0 : barrierLeft.getStateHash());
    result = prime * result + ((barrierRight == null) ? 0 : barrierRight.getStateHash());
    result = prime * result + ((elevation == null) ? 0 : elevation.ordinal());
    result = prime * result + ((elevationHeight == null) ? 0 : elevationHeight.hashCode());
    long temp;
    temp = Double.doubleToLongBits(end);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((freeway == null) ? 0 : freeway.hashCode());
    result = prime * result + ((maxRoadSpeed == null) ? 0 : maxRoadSpeed.hashCode());
    temp = Double.doubleToLongBits(start);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((strictEnforcement == null) ? 0 : strictEnforcement.hashCode());
    result = prime * result + ((tunnelFactor == null) ? 0 : tunnelFactor.hashCode());

    return result;
  }
}
