/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.developmentspace;

import java.util.ArrayList;

/**
 * A development rule result. It's a wrapper around a list of receptor id's that have
 * passed the rule check.
 *
 * TODO: it would be better if the arraylist was a member in this object...
 */
public class DevelopmentRuleResult extends ArrayList<Integer> {
  private static final long serialVersionUID = -2623369170981388287L;

  private DevelopmentRule rule;

  @SuppressWarnings("unused")
  private DevelopmentRuleResult() {
    //needed for GWT
  }

  /**
   * @param rule The rule for this result.
   */
  public DevelopmentRuleResult(final DevelopmentRule rule) {
    this.rule = rule;
  }

  public DevelopmentRule getRule() {
    return rule;
  }

  @Override
  public int hashCode() {
    return rule.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    return obj != null && this.getClass() == obj.getClass()
        && ((DevelopmentRuleResult) obj).rule.equals(rule) && super.equals(obj); // NOSONAR super.equals is perfectly fine
  }
}
