/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.admin;

import java.util.HashSet;

import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 * Data class with parameters to filter results when querying for users.
 */
public class UserManagementFilter implements Filter {
  private static final long serialVersionUID = 3556278666421251075L;

  private String username;
  private Boolean enabled;

  private HashSet<Authority> organisations = new HashSet<Authority>();
  private HashSet<AdminUserRole> roles = new HashSet<AdminUserRole>();

  @Override
  public void fillDefault() {
    username = null;
    enabled = null;
    organisations.clear();
    roles.clear();
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public HashSet<Authority> getOrganisations() {
    return organisations;
  }

  public void setOrganisations(final HashSet<Authority> authorities) {
    this.organisations = authorities;
  }

  public HashSet<AdminUserRole> getRoles() {
    return roles;
  }

  public void setRoles(final HashSet<AdminUserRole> roles) {
    this.roles = roles;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(final Boolean enabled) {
    this.enabled = enabled;
  }
}
