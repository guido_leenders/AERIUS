/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.developmentspace;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.info.AssessmentArea;

/**
 * Simple wrapper around a list of DevelopmentRuleResult objects.
 */
public class DevelopmentRuleResultList implements Serializable {
  private static final long serialVersionUID = -2745113259731421737L;

  private AssessmentArea assessmentArea;
  private ArrayList<DevelopmentRuleResult> developmentRuleResults;

  public AssessmentArea getAssessmentArea() {
    return assessmentArea;
  }

  public void setAssessmentArea(final AssessmentArea assessmentArea) {
    this.assessmentArea = assessmentArea;
  }

  public ArrayList<DevelopmentRuleResult> getDevelopmentRuleResults() {
    return developmentRuleResults;
  }

  public void setDevelopmentRuleResults(final ArrayList<DevelopmentRuleResult> developmentRuleResults) {
    this.developmentRuleResults = developmentRuleResults;
  }

}
