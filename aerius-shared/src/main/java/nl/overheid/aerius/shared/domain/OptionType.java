/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

/**
 *
 */
public enum OptionType {

  /**
   * The option for filtering minimum faction used. Values are fractions.
   * Use Double.class to get the options from DB.
   */
  FILTER_MIN_FRACTION_USED(Double.class);

  private final Class<?> valueClass;

  private OptionType(final Class<?> valueClass) {
    this.valueClass = valueClass;
  }

  public Class<? extends Object> getValueClass() {
    return valueClass;
  }

}
