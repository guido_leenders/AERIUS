/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.List;

/**
 * Data object for a farm lodging type (system) including its emission factor.<br>
 * The object is published in the RAV (Regeling Ammoniak en Veehouderij) list.<br>
 * <br>
 * A lodging type has an id, code, name, description, animal category and one or more system
 * definitions (BWL-numbers).<br>
 * <br>
 * There are additional and reductive farm lodging systems that can be "stacked on" this lodging type. These
 * either increase or decrease the emissions of the type.<br>
 * The set of such systems that are allowed according to the RAV, are known in this object.
 */
public class FarmLodgingCategory extends AbstractEmissionCategory implements HasFarmLodgingSystemDefinitions {

  private static final long serialVersionUID = 8387853904932006645L;

  private static final double REDUCTION_FACTOR_THRESHOLD = 0.7;

  /**
   * Emission factor per animal in kg NH3/year
   */
  private double emissionFactor;

  /**
   * Animal category for this lodging type
   */
  private FarmAnimalCategory animalCategory;

  /**
   * The list of system definitions that belong to this farm lodging type.
   */
  private List<FarmLodgingSystemDefinition> farmLodgingSystemDefinitions = new ArrayList<FarmLodgingSystemDefinition>();

  /**
   * The list of additional farm lodging systems that *may* be used in conjunction with (stacked on) this
   * farm lodging type according to the RAV. An additional farm lodging system generates extra emissions
   * on the farm lodging type.
   */
  private List<FarmAdditionalLodgingSystemCategory> farmAdditionalLodgingSystems = new ArrayList<FarmAdditionalLodgingSystemCategory>();

  /**
   * The list of reductive farm lodging systems that *may* be used in conjunction with (stacked on) this
   * farm lodging type according to the RAV. A reductive farm lodging system reduces emissions
   * on the farm lodging type.
   */
  private List<FarmReductiveLodgingSystemCategory> farmReductiveLodgingSystems = new ArrayList<FarmReductiveLodgingSystemCategory>();

  /**
   * The list of farm lodging fodder measures that *may* be used in conjunction with (stacked on) this
   * farm lodging type according to the RAV. A fodder measure reduces emissions on the farm lodging type.
   */
  private List<FarmLodgingFodderMeasureCategory> farmLodgingFodderMeasures = new ArrayList<FarmLodgingFodderMeasureCategory>();

  /**
   * The/an traditional lodging type within this animal category, e.g. "D3.100.1 overige huisvestingssystemen -
   * hokoppervlak maximaal 0,8 m2 per varken". (Note that there is not necessarily only one such type per
   * animal category.)
   * This reference may be null, but if it's not it could mean that the emission calculation will be different.
   */
  private FarmLodgingCategory traditionalFarmLodgingCategory;

  /**
   * Whether this system is an air scrubber (luchtwasser).
   */
  private boolean scrubber;

  /**
   * Default public constructor.
   */
  public FarmLodgingCategory() {
    // No-op.
  }

  public FarmLodgingCategory(final int id, final String code, final String name, final String description, final double emissionFactor,
      final FarmAnimalCategory animalCategory, final FarmLodgingCategory traditionalFarmLodgingCategory, final boolean scrubber) {
    super(id, code, name, description);
    this.emissionFactor = emissionFactor;
    this.animalCategory = animalCategory;
    this.traditionalFarmLodgingCategory = traditionalFarmLodgingCategory;
    this.scrubber = scrubber;
  }

  public double getEmissionFactor() {
    return emissionFactor;
  }

  public void setEmissionFactor(final double emissionFactor) {
    this.emissionFactor = emissionFactor;
  }

  public FarmAnimalCategory getAnimalCategory() {
    return animalCategory;
  }

  public void setAnimalCategory(final FarmAnimalCategory animalCategory) {
    this.animalCategory = animalCategory;
  }

  @Override
  public List<FarmLodgingSystemDefinition> getFarmLodgingSystemDefinitions() {
    return farmLodgingSystemDefinitions;
  }

  public void setFarmLodgingSystemDefinitions(final List<FarmLodgingSystemDefinition> farmLodgingSystemDefinitions) {
    this.farmLodgingSystemDefinitions = farmLodgingSystemDefinitions;
  }

  public List<FarmAdditionalLodgingSystemCategory> getFarmAdditionalLodgingSystemCategories() {
    return farmAdditionalLodgingSystems;
  }

  public void setFarmAdditionalLodgingSystemCategories(final List<FarmAdditionalLodgingSystemCategory> farmAdditionalLodgingSystems) {
    this.farmAdditionalLodgingSystems = farmAdditionalLodgingSystems;
  }

  public List<FarmReductiveLodgingSystemCategory> getFarmReductiveLodgingSystemCategories() {
    return farmReductiveLodgingSystems;
  }

  public void setFarmReductiveLodgingSystemCategories(final List<FarmReductiveLodgingSystemCategory> farmReductiveLodgingSystems) {
    this.farmReductiveLodgingSystems = farmReductiveLodgingSystems;
  }

  public List<FarmLodgingFodderMeasureCategory> getFarmLodgingFodderMeasures() {
    return farmLodgingFodderMeasures;
  }

  public void setFarmLodgingFodderMeasures(final List<FarmLodgingFodderMeasureCategory> farmLodgingFodderMeasures) {
    this.farmLodgingFodderMeasures = farmLodgingFodderMeasures;
  }

  public FarmLodgingCategory getTraditionalFarmLodgingCategory() {
    return traditionalFarmLodgingCategory;
  }

  public void setTraditionalFarmLodgingCategory(final FarmLodgingCategory traditionalFarmLodgingCategory) {
    this.traditionalFarmLodgingCategory = traditionalFarmLodgingCategory;
  }

  public boolean isScrubber() {
    return scrubber;
  }

  public void setScrubber(final boolean scrubber) {
    this.scrubber = scrubber;
  }

  /**
   * Returns the reduction factor of this lodging system in relation to the corresponding traditional system.
   * For example, if this is system D1.1.3.1 with emission factor 0.13, and the traditional system is
   * D1.1.100.1 with emission factor 0.6, then the reduction factor of D1.1.3.1 is 0.78 (78%).
   * Not all lodging systems have a corresponding traditional system.
   */
  public double getReductionFactor() {
    if (traditionalFarmLodgingCategory == null || traditionalFarmLodgingCategory.getEmissionFactor() == 0.0) {
      return 0;
    } else {
      return 1 - emissionFactor / traditionalFarmLodgingCategory.getEmissionFactor();
    }
  }

  /**
   * Whether the reduction factor is high enough that it should be limited.
   * This means it is above {@value #REDUCTION_FACTOR_THRESHOLD}.
   */
  public boolean shouldConstrainReductionFactor() {
    return getReductionFactor() > REDUCTION_FACTOR_THRESHOLD;
  }

  /**
   * Get the emission factor constrained so that the reduction in respect to its corresponding traditional
   * system will be no more than than {@value #REDUCTION_FACTOR_THRESHOLD}.
   */
  public double getConstrainedEmissionFactor() {
    if (shouldConstrainReductionFactor()) {
      return traditionalFarmLodgingCategory.getEmissionFactor() * (1 - REDUCTION_FACTOR_THRESHOLD);
    } else {
      return getEmissionFactor();
    }
  }

  /**
   * Returns true if the additionalLodgingSystemCategory is in the list of allowed additionalLodgingSystemCategories for this category.
   * @param additionalLodgingSystemCategory category to check
   * @return true if is allowed
   */
  public boolean canStackAdditionalLodgingSystemCategory(final FarmAdditionalLodgingSystemCategory additionalLodgingSystemCategory) {
    return additionalLodgingSystemCategory != null && getFarmAdditionalLodgingSystemCategories().contains(additionalLodgingSystemCategory);
  }

  /**
   * Returns true if the reductiveLodgingSystemCategory is in the list of allowed reductiveLodgingSystemCategories for this category.
   * @param reductiveLodgingSystemCategory category to check
   * @return true if is allowed
   */
  public boolean canStackReductiveLodgingSystemCategory(final FarmReductiveLodgingSystemCategory reductiveLodgingSystemCategory) {
    return reductiveLodgingSystemCategory != null && getFarmReductiveLodgingSystemCategories().contains(reductiveLodgingSystemCategory);
  }

  /**
   * Returns true if the fodderMeasureCategory has ammonia proportions for the animal category of this category.
   * @param fodderMeasureCategory category to check
   * @return true if is allowed
   */
  public boolean canStackFodderMeasureCategory(final FarmLodgingFodderMeasureCategory fodderMeasureCategory) {
    return fodderMeasureCategory != null && fodderMeasureCategory.canApplyToFarmLodgingCategory(this);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();

    sb.append("farmLodgingSystemDefinitions=[");
    for (final FarmLodgingSystemDefinition farmLodgingSystemDefinition : farmLodgingSystemDefinitions) {
      if (farmLodgingSystemDefinition != farmLodgingSystemDefinitions.get(0)) {
        sb.append("; ");
      }
      sb.append(farmLodgingSystemDefinition.getCode());
    }
    sb.append("], ");

    sb.append("farmAdditionalLodgingSystems=[");
    for (final FarmAdditionalLodgingSystemCategory farmAdditionalLodgingSystem : farmAdditionalLodgingSystems) {
      if (farmAdditionalLodgingSystem != farmAdditionalLodgingSystems.get(0)) {
        sb.append("; ");
      }
      sb.append(farmAdditionalLodgingSystem.getCode());
    }
    sb.append("], ");

    sb.append("farmReductiveLodgingSystems=[");
    for (final FarmReductiveLodgingSystemCategory farmReductiveLodgingSystem : farmReductiveLodgingSystems) {
      if (farmReductiveLodgingSystem != farmReductiveLodgingSystems.get(0)) {
        sb.append("; ");
      }
      sb.append(farmReductiveLodgingSystem.getCode());
    }
    sb.append("], ");

    sb.append("farmLodgingFodderMeasures=[");
    for (final FarmLodgingFodderMeasureCategory farmLodgingFodderMeasure : farmLodgingFodderMeasures) {
      if (farmLodgingFodderMeasure != farmLodgingFodderMeasures.get(0)) {
        sb.append("; ");
      }
      sb.append(farmLodgingFodderMeasure.getCode());
    }
    sb.append("]");

    return super.toString()
        + ", emissionFactor=" + emissionFactor
        + ", animalCategory=" + animalCategory.getCode()
        + ", traditionalFarmLodgingCategory=" + (traditionalFarmLodgingCategory == null ? "null" : traditionalFarmLodgingCategory.getCode())
        + ", scrubber=" + scrubber
        + ", " + sb.toString()
        + "]";
  }

}
