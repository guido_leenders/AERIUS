/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * CalculatedScenario object for Research Area calculations.
 */
public class ResearchAreaCalculationScenario extends CalculatedSingle {

  private static final long serialVersionUID = 2408285601518338450L;
  private EmissionSourceList researchAreaSources = new EmissionSourceList();

  /**
   * Returns the list of sources to be used to determine the research area.
   * @return
   */
  public EmissionSourceList getResearchAreaSources() {
    return researchAreaSources;
  }

  public void setResearchAreaSources(final EmissionSourceList researchAreaSources) {
    this.researchAreaSources.setAll(researchAreaSources);
  }

}
