/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.reference;

/**
 * Factory class that returns the correct {@link ReferenceGenerator} implementation based on the given
 * version. When no version is supplied, it returns the current (latest) version.
 */
class ReferenceGeneratorFactory {

  public static final byte CURRENT_VERSION = ReferenceGeneratorV2.VERSION_ID;

  private ReferenceGeneratorFactory() {
  }

  /**
   * Create an instance of the {@link ReferenceGenerator} with the given version.
   * @throws IllegalArgumentException When the version does not exist.
   */
  public static final ReferenceGenerator createReferenceGenerator(final byte version) {
    switch (version) {
    case ReferenceGeneratorV1.VERSION_ID:
      return new ReferenceGeneratorV1();
    case ReferenceGeneratorV2.VERSION_ID:
      return new ReferenceGeneratorV2();
    default:
      throw new IllegalArgumentException("ReferenceGenerator version not supported");
    }
  }

  /**
   * Create an instance of the current version of the {@link ReferenceGenerator}.
   */
  public static final ReferenceGenerator createCurrentReferenceGenerator() {
    return createReferenceGenerator(CURRENT_VERSION);
  }
}
