/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import java.io.Serializable;

/**
 * Data class for Heat content calculator.
 */
public class HeatContentSpecification implements Serializable {

  /**
   * Average surrounding temperature in celsius, default value.
   */
  public static final Double AVERAGE_SURROUNDING_TEMPERATURE = 11.85;

  private static final long serialVersionUID = 5533225369786456415L;

  private static final double KW_TO_MW = 0.001d;

  // Density of surrounding atmosphere
  private static final double PCP = 1.28398779d;
  private static final double ZERO_CELCIUS_IN_KELVIN = 273.15d;

  private double emissionTemperature = AVERAGE_SURROUNDING_TEMPERATURE;
  private double outflowSurface;
  private double outflowVelocity;

  public double getEmissionTemperature() {
    return emissionTemperature;
  }

  public void setEmissionTemperature(final double emissionTemperature) {
    this.emissionTemperature = emissionTemperature;
  }

  public double getOutflowSurface() {
    return outflowSurface;
  }

  public void setOutflowSurface(final double outflowSurface) {
    this.outflowSurface = outflowSurface;
  }

  public double getOutflowVelocity() {
    return outflowVelocity;
  }

  public void setOutflowVelocity(final double outflowVelocity) {
    this.outflowVelocity = outflowVelocity;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(emissionTemperature);
    result = (prime * result) + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(outflowSurface);
    result = (prime * result) + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(outflowVelocity);
    return (prime * result) + (int) (temp ^ (temp >>> 32));
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final HeatContentSpecification other = (HeatContentSpecification) obj;
    return Double.doubleToLongBits(emissionTemperature) == Double.doubleToLongBits(other.emissionTemperature)
        && Double.doubleToLongBits(outflowSurface) == Double.doubleToLongBits(other.outflowSurface)
        && Double.doubleToLongBits(outflowVelocity) == Double.doubleToLongBits(other.outflowVelocity);
  }

  /**
   * Returns true if no sensible value.
   *
   * @return
   */
  public boolean isEmpty() {
    return !((getEmissionTemperature() > HeatContentSpecification.AVERAGE_SURROUNDING_TEMPERATURE) || (getOutflowSurface() > 0)
        || (getOutflowVelocity() > 0));
  }

  /**
   * Copies object.
   *
   * @return new instance of this object
   */
  public HeatContentSpecification copy() {
    final HeatContentSpecification copy = new HeatContentSpecification();
    copy.setEmissionTemperature(emissionTemperature);
    copy.setOutflowSurface(outflowSurface);
    copy.setOutflowVelocity(outflowVelocity);
    return copy;
  }

  @Override
  public String toString() {
    return "HeatContentSpecification [emissionTemperature=" + emissionTemperature + ", outflowSurface=" + outflowSurface + ", outflowVelocity="
        + outflowVelocity + "]";
  }

  public double getHeatContent() {
    return PCP * outflowSurface * outflowVelocity
        * Math.max((emissionTemperature + ZERO_CELCIUS_IN_KELVIN) - (AVERAGE_SURROUNDING_TEMPERATURE + ZERO_CELCIUS_IN_KELVIN), 0) * KW_TO_MW;
  }
}
