/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.util.Date;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Represents a notification entry as in the NotificationPanel.
 */
public class Notification {

  private final Date dateTime;
  private final String message;
  private final Throwable exception;
  private final boolean error;
  private final String url;

  public Notification(final String msg) {
    this(msg, (Throwable) null, false);
  }

  public Notification(final Throwable ex) {
    this(null, ex, true);
  }

  public Notification(final String msg, final Throwable exception) {
    this(msg, exception, true);
  }

  public Notification(final String msg, final String url) {
    this(msg, url, (Throwable) null, false);
  }

  public Notification(final String msg, final boolean isError) {
    this(msg, (Throwable) null, isError);
  }

  private Notification(final String msg, final Throwable exception, final boolean error) {
    this(msg, (String) null, exception, error);
  }

  private Notification(final String msg, final String url, final Throwable exception, final boolean error) {
    this.message = msg;
    this.url = url;
    this.exception = exception;
    this.error = error;
    this.dateTime = new Date();
  }

  public Throwable getException() {
    return exception;
  }

  public String getMessage() {
    return message == null ? (exception == null ? "" : exception.getMessage()) : message;
  }

  public Date getDateTime() {
    return dateTime;
  }

  public String getUrl() {
    return url;
  }

  public boolean isError() {
    return error || exception != null;
  }

  public long getReference() {
    return exception instanceof AeriusException ? ((AeriusException) exception).getReference() : dateTime.getTime();
  }
}
