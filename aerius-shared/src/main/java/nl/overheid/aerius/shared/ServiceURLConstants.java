/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared;

/**
 * Constants for service url's.
 */
public final class ServiceURLConstants {

  /**
   * Base path for all services.
   */
  public static final String BASE_SERVICE_PATH = "service";

  /**
   * URL pattern for service dispatcher servlets.
   */
  public static final String DISPATCHER_SERVLET_MAPPING = "/aerius/" + BASE_SERVICE_PATH + "/*";

  /**
   * Context service path, relative to the base service path.
   */
  public static final String CONTEXT_SERVICE_RELATIVE_PATH = "/context";

  /**
   * Context service path.
   */
  public static final String CONTEXT_SERVICE_PATH = BASE_SERVICE_PATH + CONTEXT_SERVICE_RELATIVE_PATH;

  /**
   * Calculation service path, relative to the base service path.
   */
  public static final String CALCULATE_SERVICE_RELATIVE_PATH = "/aerius-calculate";

  /**
   * Calculation service path.
   */
  public static final String CALCULATE_SERVICE_PATH = BASE_SERVICE_PATH + CALCULATE_SERVICE_RELATIVE_PATH;

  /**
   * Calculation service path, relative to the base service path.
   */
  public static final String CALCULATOR_SERVICE_RELATIVE_PATH = "/aerius-calculator";

  /**
   * Calculation service path.
   */
  public static final String CALCULATOR_SERVICE_PATH = BASE_SERVICE_PATH + CALCULATOR_SERVICE_RELATIVE_PATH;

  /**
   * Export service path, relative to the base service path.
   */
  public static final String EXPORT_SERVICE_RELATIVE_PATH = "/aerius-export";

  /**
   * Export service path.
   */
  public static final String EXPORT_SERVICE_PATH = BASE_SERVICE_PATH + EXPORT_SERVICE_RELATIVE_PATH;

  /**
   * System message service path, relative to the base service path.
   */
  public static final String SYSTEM_INFO_SERVICE_RELATIVE_PATH = "/system-info";

  /**
   * System message service path.
   */
  public static final String SYSTEM_INFO_SERVICE_PATH = BASE_SERVICE_PATH + SYSTEM_INFO_SERVICE_RELATIVE_PATH;

  /**
   * Info (marker) service path, relative to the base service path.
   */
  public static final String INFO_SERVICE_RELATIVE_PATH = "/info";

  /**
   * Info (marker) service path.
   */
  public static final String INFO_SERVICE_PATH = BASE_SERVICE_PATH + INFO_SERVICE_RELATIVE_PATH;

  /**
   * Retrieve import, relative to the base service path.
   */
  public static final String RETRIEVE_IMPORT_SERVICE_RELATIVE_PATH = "/retrieve-import";

  /**
   * Retrieve import service path.
   */
  public static final String RETRIEVE_IMPORT_SERVICE_PATH = BASE_SERVICE_PATH + RETRIEVE_IMPORT_SERVICE_RELATIVE_PATH;

  /**
   * Register permit service path, relative to base service path.
   */
  public static final String REGISTER_PERMIT_SERVICE_RELATIVE_PATH = "/permit";

  /**
   * Register permit service path.
   */
  public static final String REGISTER_PERMIT_SERVICE_PATH = BASE_SERVICE_PATH + REGISTER_PERMIT_SERVICE_RELATIVE_PATH;

  /**
   * Melding service path, relative to base service path.
   */
  public static final String MELDING_SERVICE_RELATIVE_PATH = "/melding";

  /**
   * Melding service path.
   */
  public static final String MELDING_SERVICE_PATH = BASE_SERVICE_PATH + MELDING_SERVICE_RELATIVE_PATH;

  /**
   * Register notice service path, relative to base service path.
   */
  public static final String REGISTER_NOTICE_SERVICE_RELATIVE_PATH = "/notice";

  /**
   * Register notice service path.
   */
  public static final String REGISTER_NOTICE_SERVICE_PATH = BASE_SERVICE_PATH + REGISTER_NOTICE_SERVICE_RELATIVE_PATH;

  /**
   * Register priority project service path, relative to base service path.
   */
  public static final String REGISTER_PRIORITY_PROJECT_SERVICE_RELATIVE_PATH = "/priorityproject";

  /**
   * Register priority project service path.
   */
  public static final String REGISTER_PRIORITY_PROJECT_SERVICE_PATH = BASE_SERVICE_PATH + REGISTER_PRIORITY_PROJECT_SERVICE_RELATIVE_PATH;

  /**
   * Register search service path, relative to base service path.
   */
  public static final String REGISTER_SEARCH_SERVICE_RELATIVE_PATH = "/search";

  /**
   * Register search service path.
   */
  public static final String REGISTER_SEARCH_SERVICE_PATH = BASE_SERVICE_PATH + REGISTER_SEARCH_SERVICE_RELATIVE_PATH;

  /**
   * Register request service path, relative to base service path.
   */
  public static final String REGISTER_REQUEST_EXPORT_SERVICE_RELATIVE_PATH = "/request-export";

  /**
   * Register request export service path.
   */
  public static final String REGISTER_REQUEST_EXPORT_SERVICE_PATH = BASE_SERVICE_PATH + REGISTER_REQUEST_EXPORT_SERVICE_RELATIVE_PATH;

  /**
   * Admin service path, relative to base service path.
   */
  public static final String ADMIN_SERVICE_RELATIVE_PATH = "/admin";

  /**
   * Admin service path.
   */
  public static final String ADMIN_SERVICE_PATH = BASE_SERVICE_PATH + ADMIN_SERVICE_RELATIVE_PATH;

  /**
   * Scenario connect service path, relative to connect service path.
   */
  public static final String SCENARIO_CONNECT_SERVICE_RELATIVE_PATH = "/scenario-connect";

  /**
   * Scenario connect service path.
   */
  public static final String SCENARIO_CONNECT_SERVICE_PATH = BASE_SERVICE_PATH + SCENARIO_CONNECT_SERVICE_RELATIVE_PATH;

  private ServiceURLConstants() {
  }
}
