/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.geo;

import java.io.Serializable;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasState;

/**
 * Base class for all AERIUS points on the map. A point can represent a receptor, meaning on the hexagon grid or a free/normal point.
 */
public class AeriusPoint extends Point implements Serializable, HasId, HasState {

  private static final long serialVersionUID = 5292863928395270353L;

  /**
   * Identifies how this point should be interpreted.
   */
  public enum AeriusPointType {
    /**
     * Hexagon of 100 m2, point is at the center of the hexagon.
     */
    RECEPTOR,
    /**
     * Normal point.
     */
    POINT;
  }

  private int id;
  private AeriusPointType pointType;
  private String label;

  // Needed by GWT.
  public AeriusPoint() {
    pointType = AeriusPointType.RECEPTOR;
  }

  /**
   * Initializes this AeriusPoint with the given ID.
   *
   * @param id the ID
   */
  public AeriusPoint(final int id) {
    this(id, AeriusPointType.RECEPTOR);
  }

  public AeriusPoint(final AeriusPointType pointType) {
    this.pointType = pointType;
  }

  public AeriusPoint(final int id, final AeriusPointType pointType) {
    this.id = id;
    this.pointType = pointType;
  }


  /**
   * Initializes this AeriusPoint with the given location.
   *
   * @param x x coordinate
   * @param y y coordinate
   */
  public AeriusPoint(final double x, final double y) {
    this(0, x, y);
  }

  /**
   * Initializes this AeriusPoint with the given location.
   *
   * @param x x coordinate
   * @param y y coordinate
   * @param id the ID
   */
  protected AeriusPoint(final int id, final double x, final double y) {
    this(id, AeriusPointType.RECEPTOR, x, y);
  }

  /**
   * @param id
   * @param pointType
   * @param x
   * @param y
   */
  public AeriusPoint(final int id, final AeriusPointType pointType, final double x, final double y) {
    super(x, y);
    this.id = id;
    this.pointType = pointType;
  }

  public AeriusPoint copy() {
    return copy(new AeriusPoint());
  }

  protected AeriusPoint copy(final AeriusPoint copy) {
    copy.id = id;
    copy.pointType = pointType;
    copy.label = label;
    copy.setX(getX());
    copy.setY(getY());
    return copy;
  }

  @Override
  public int getStateHash() {
    return (pointType == AeriusPointType.POINT ? 1 : 2) + 31 * super.hashCode();
  }

  /**
   * Objects are equal if same type and same id or if obj is a point and the super is equal.
   */
  @Override
  public boolean equals(final Object obj) {
    return obj instanceof AeriusPoint ? pointType == ((AeriusPoint) obj).getPointType() && id == ((AeriusPoint) obj).getId() : super.equals(obj);
  }

  @Override
  public int hashCode() {
    return id + (31 * ((pointType.hashCode()) + (31 * super.hashCode())));
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public void setPointType(final AeriusPointType pointType) {
    this.pointType = pointType;
  }

  public AeriusPointType getPointType() {
    return pointType;
  }

  @Override
  public String toString() {
    return "AeriusPoint [id=" + id + "(" + pointType + "), " + super.toString() + "]";
  }
}
