/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

/**
 * Directive of the Nature area.
 */
public enum NatureAreaDirective {

  /**
   * Bird directive.
   */
  BIRD("VR"),
  /**
   * Habitat directive.
   */
  HABITAT("HR"),
  /**
   * Bird directive and habitat directive.
   */
  BIRD_AND_HABITAT("VR+HR");

  private final String databaseName;

  private NatureAreaDirective(final String databaseName) {
    this.databaseName = databaseName;
  }

  /**
   * Get the right directive based on database names (containing +'s for combinations).
   */
  public static NatureAreaDirective getFromDB(final String dbName) {
    NatureAreaDirective properDirective = null;
    for (final NatureAreaDirective directive : values()) {
      if (directive.databaseName.equalsIgnoreCase(dbName)) {
        properDirective = directive;
      }
    }
    return properDirective;
  }

}
