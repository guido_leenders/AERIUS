/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasEmission;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Extends OPSSource to support multiple emission values for different
 * substances for one source. It also adds the sector this source belongs to.
 *
 * <p>Because emissions are for some sectors calculated based on sector
 * specific data, the emission values are set via a generic interface
 * {@link EmissionSubSource}. Each sector specific calculation implements this
 * interface and sets it on the {@link EmissionSource} object.
 */
public abstract class EmissionSource extends Point implements HasEmission, HasState, HasId, VisitableEmissionSource, Serializable {
  private static final long serialVersionUID = -1121135813882019494L;

  private int id;

  private String label;

  private Sector sector;

  private WKTGeometry geometry;

  private boolean emissionPerUnit;

  private boolean yearDependent;

  /**
   * All source Characteristics.
   */
  @Valid
  private OPSSourceCharacteristics sourceCharacteristics = new OPSSourceCharacteristics();

  /**
   * Default constructor.
   */
  public EmissionSource() {
    // No-op.
  }

  /**
   * Initializes this EmissionSource with the given values.
   *
   * @param id id of the source
   * @param point (center) point of the location
   */
  public EmissionSource(final int id, final Point point) {
    setId(id);
    setSrid(point.getSrid());
    setX(point.getX());
    setY(point.getY());
  }

  /**
   * Creates a copy of this EmissionSource into a new object.
   * @return returns a copy
   */
  public abstract EmissionSource copy();

  /**
   * Convenience method to allow subclasses to easily implement copy.
   *
   * @param copy New object to copy
   * @return
   * @return
   */
  public <E extends EmissionSource> E copyTo(final E copy) {
    copy.setId(getId());
    copy.setSrid(getSrid());
    copy.setX(getX());
    copy.setY(getY());
    copy.setGeometry(geometry == null ? null : geometry.copy());
    copy.setSourceCharacteristics(getSourceCharacteristics() == null ? null : getSourceCharacteristics().copy());
    copy.setSector(sector);
    copy.setLabel(label);
    return copy;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() ? getId() == ((EmissionSource) obj).getId()
        && getSector().equals(((EmissionSource) obj).getSector())
        && (label == null && ((EmissionSource) obj).getLabel() == null || label != null && label.equals(((EmissionSource) obj).getLabel()))
        && super.equals(obj) : false;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    return getId() + 31 * super.hashCode();
  }

  @Override
  public final double getEmission(final EmissionValueKey key) {
    final double emission;
    if (key.getSubstance() == Substance.NOXNH3) {
      emission = getEmission(new EmissionValueKey(key.getYear(), Substance.NH3), getWeight())
          + getEmission(new EmissionValueKey(key.getYear(), Substance.NOX), getWeight());
    } else {
      emission = getEmission(key, getWeight());
    }
    return emission;
  }

  @NotNull
  public WKTGeometry getGeometry() {
    return geometry;
  }

  public String getLabel() {
    return label;
  }

  public OPSSourceCharacteristics getSourceCharacteristics() {
    return sourceCharacteristics;
  }

  /**
   * Returns the {@link Sector} for this object. If the {@link Sector} would be null
   * the value {@link Sector#SECTOR_UNDEFINED} is returned.
   *
   * @return The Sector
   */
  @NotNull
  public Sector getSector() {
    return sector == null ? Sector.SECTOR_UNDEFINED : sector;
  }

  protected double getWeight() {
    return isEmissionPerUnit() && geometry != null ? geometry.getMeasure() : 1.0;
  }

  /**
   * @return True if there are emissions for this source, false if not.
   */
  public abstract boolean hasAnyEmissions();

  /**
   * Returns true if the emission value returned by getEmission is per measure
   * unit. For example for roads, which is a line it means if it returns true
   * the emission is per meter road. If false would be returned the emission
   * would be for the total length of the road. If true to calculate the total
   * emission is calculated by multiplying the emission with the length of the
   * road.
   *
   * @return true if emission is per measuring unit
   */
  public boolean isEmissionPerUnit() {
    return emissionPerUnit;
  }

  /**
   * Returns true if the emission values are calculated differently for each year.
   * For instance, traffic has different emission factors and thus different emission
   * values based on the year.
   *
   * @return true if emission is per measuring unit
   */
  public boolean isYearDependent() {
    return yearDependent;
  }

  public void setGeometry(final WKTGeometry geometry) {
    this.geometry = geometry;
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  public void setSourceCharacteristics(final OPSSourceCharacteristics sourceCharacteristics) {
    this.sourceCharacteristics = sourceCharacteristics;
  }

  public void setSector(final Sector sector) {
    this.sector = sector;
  }

  protected abstract double getEmission(final EmissionValueKey key, final double weight);

  public void setEmissionPerUnit(final boolean emissionPerUnit) {
    this.emissionPerUnit = emissionPerUnit;
  }

  /**
   * If true the emission data can differ per year. If false the year value is
   * ignored in the {@link EmissionValueKey}.
   *
   * @param emissionPerYear true if emission different per year
   */
  public void setYearDependent(final boolean emissionPerYear) {
    this.yearDependent = emissionPerYear;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + getId();
    result = prime * result + (sector == null ? 0 : sector.getStateHash());
    result = prime * result + (geometry == null ? 0 : geometry.getStateHash());
    result = prime * result + (sourceCharacteristics == null ? 0 : sourceCharacteristics.getStateHash());
    result = prime * result + (emissionPerUnit ? 1 : 2);
    return result;
  }

  @Override
  public String toString() {
    return "EmissionSource [id=" + id + ", label=" + label + ", sector=" + sector + ", geometry=" + geometry + ", emissionPerUnit=" + emissionPerUnit
        + ", yearDependent=" + yearDependent + ", sourceCharacteristics=" + sourceCharacteristics + "]";
  }
}
