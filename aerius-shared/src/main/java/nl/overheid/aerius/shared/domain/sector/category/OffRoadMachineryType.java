/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.HasId;

/**
 * Machinery types for the machineries for which there are default values to calculate the
 * NOx emission. For each machine type, their might be several fuel types that may be used.
 */
public class OffRoadMachineryType extends AbstractCategory {

  public static final String OTHER_CODE = "OTHER";

  private static final long serialVersionUID = -8885967610935257392L;

  /**
   * class to store machinery fuel type data.
   */
  public static class MachineryFuelType extends AbstractCategory {
    private static final long serialVersionUID = -5467593800559801963L;

    private double densityFuel;

    @Override
    public boolean equals(final Object obj) {
      return (obj != null) && (obj.getClass() == getClass()) && (((MachineryFuelType) obj).getCode().equals(getCode()));
    }

    @Override
    public int hashCode() {
      return getCode().hashCode();
    }

    public Double getDensityFuel() {
      return densityFuel;
    }

    public void setDensityFuel(final Double densityFuel) {
      this.densityFuel = densityFuel;
    }

    @Override
    public String toString() {
      return "MachineryFuelType [" + super.toString() + " densityFuel=" + densityFuel + "]";
    }
  }

  /**
   * class to store the fueltype properties for every machinery type. These values may differ
   * per machinery-fuel pair. As not all fuel types exist for every machinery type, this
   * is a convenient way to represent this in the UI-context
   */
  public static class FuelTypeProperties implements HasId, Serializable {

    private static final long serialVersionUID = 8253057605529757729L;

    private int id;
    private MachineryFuelType fuelType;
    private int power;
    private double load;
    private double emissionFactor;
    private int energyEfficiencyInGramPerKWH;

    @Override
    public int getId() {
      return id;
    }

    @Override
    public void setId(final int id) {
      this.id = id;
    }

    public MachineryFuelType getFuelType() {
      return fuelType;
    }

    public void setFuelType(final MachineryFuelType fuelType) {
      this.fuelType = fuelType;
    }

    public int getPower() {
      return power;
    }

    public void setPower(final int power) {
      this.power = power;
    }

    public Double getLoad() {
      return load;
    }

    /**
     * @return the load in percentage, so it can easily be represented in the UI.
     */
    public int getLoadInPercentage() {
      return MathUtil.round(this.getLoad() * SharedConstants.PERCENTAGE_TO_FRACTION);
    }

    public void setLoad(final Double load) {
      this.load = load;
    }

    public Double getEmissionFactor() {
      return emissionFactor;
    }

    public void setEmissionFactor(final Double emissionFactor) {
      this.emissionFactor = emissionFactor;
    }

    public int getEnergyEfficiencyInGramPerKWH() {
      return energyEfficiencyInGramPerKWH;
    }

    public void setEnergyEfficiencyInGramPerKWH(final int energyEfficiencyInGramPerKWH) {
      this.energyEfficiencyInGramPerKWH = energyEfficiencyInGramPerKWH;
    }

    @Override
    public String toString() {
      return "FuelTypeProperties [id=" + id + ", fuelType=" + fuelType + ", power=" + power + ", load=" + load + ", emissionFactor=" + emissionFactor
          + ", energyEfficiencyInGramPerKWH=" + energyEfficiencyInGramPerKWH + "]";
    }
  }

  private int sectorId; // Chosen not to use Sector object because it's unneeded and hard to couple
  private ArrayList<FuelTypeProperties> fuelTypes = new ArrayList<FuelTypeProperties>();

  public int getSectorId() {
    return sectorId;
  }

  public void setSectorId(final int sectorId) {
    this.sectorId = sectorId;
  }

  public ArrayList<FuelTypeProperties> getFuelTypes() {
    return fuelTypes;
  }

  public void setFuelTypes(final ArrayList<FuelTypeProperties> fuelTypes) {
    this.fuelTypes = fuelTypes;
  }
}
