/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Calculation details for a single scenarion containing one {@link EmissionSourceList}
 * with calculation id.
 */
public class CalculatedSingle extends CalculatedScenario implements Serializable {

  private static final long serialVersionUID = -4214837406211502431L;

  public CalculatedSingle() {
    getCalculations().add(new Calculation());
    getScenario().addSources(new EmissionSourceList());
  }

  public int getCalculationId() {
    return getCalculation().getCalculationId();
  }

  @Override
  public int getCalculationId(final int eslId) {
    return getSources() != null && getSources().getId() == eslId ? getCalculationId() : 0;
  }

  @Override
  public boolean containsCalculationId(final int calculationId) {
    return calculationId != 0 && getCalculationId() == calculationId;
  }

  public Calculation getCalculation() {
    return getCalculations().get(0);
  }

  @Override
  public Calculation getCalculation(final int calculatorId) {
    return getCalculation().getCalculationId() == calculatorId ? getCalculation() : null;
  }

  @Override
  public EmissionSourceList getSources(final int calculatorId) {
    return getCalculationId() == calculatorId ? getSources() :  null;
  }

  public EmissionSourceList getSources() {
    return getScenario().getSourceLists().get(0);
  }

  public void setCalculationId(final int calculationId) {
    getCalculation().setCalculationId(calculationId);
  }

  public void setSources(final EmissionSourceList esl) {
    final EmissionSourceList actual = esl == null ? new EmissionSourceList() : esl;
    getScenario().getSourceLists().set(0, actual);
    getCalculation().setSources(actual);
  }

  @Override
  public ArrayList<AeriusResultPoint> getCalculationPointResultsList(final int calculatorId) {
    return getCalculationId() == calculatorId ? getCalculation().getCalculationPoints() : null;
  }

  @Override
  public boolean hasCalculations() {
    return getCalculationId() != 0;
  }
}
