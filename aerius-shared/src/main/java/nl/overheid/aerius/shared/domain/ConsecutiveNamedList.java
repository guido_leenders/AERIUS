/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A ConsecutiveNamedList in this context is a list with a name and id. And
 * each object in the also has an id. This list is consecutive in the sense that
 * the id of each object is in order of the list. When an object is added it is
 * added in ascending order at the first position an id is missing from the list.
 * <p>For example if the list contains 3 objects, where the id's of these objects
 * would be 1, 2, 4. In this case the added object will get id 3 and be placed
 * in between objects with id 2 and 4.
 * <p>If an object is removed the spot remains empty.
 * <p>The implementation uses a binary search algorithm to find missing id's in
 * Log(n) time.
 *
 * @param <T> object extending {@link HasId}
 */
public class ConsecutiveNamedList<T extends HasState & HasId> extends NamedArrayList<T> implements Serializable {
  private static final long serialVersionUID = 1562679812015586038L;

  private int offset;

  /**
   * Constructs a list with offset 0.
   *
   * @see ConsecutiveNamedList#ConsecutiveNamedList(int)
   */
  public ConsecutiveNamedList() {
    this(0);
  }

  /**
   * Constructor with offset. The offset will be the id of first object in the list.
   *
   * @param offset id of first object in the list
   */
  public ConsecutiveNamedList(final int offset) {
    this.offset = offset;
  }

  @Override
  public boolean add(final T e) {
    final int id = findFreeId(this, offset);

    e.setId(id);
    if (id == size() + offset) {
      super.add(e);
    } else {
      super.add(id - offset, e);
    }
    return true;
  }

  @Override
  public void add(final int index, final T element) {
    throw new UnsupportedOperationException("add with index not supported as it would change the order.");
  }

  @Override
  public boolean addAll(final Collection<? extends T> c) {
    final List<? extends T> copy = new ArrayList<>(c);
    final int max = size();
    while (!copy.isEmpty()) {
      if (max == 0 || listComplete(this, offset)) {
        super.addAll(copy);
        int id = max + offset;

        for (final T t : copy) {
          t.setId(id++);
        }
        return true;
      } else {
        add(copy.remove(0));
      }
    }
    return false;
  }

  /**
   * Clears the list and sets the collection without changing the id's of the
   * entries. Use this method to copy an existing list into this list. It will use the ArrayList's addAll.
   *
   * @param c List of indexed items
   */
  public void setAll(final Collection<? extends T> c) {
    super.clear();
    super.addAll(c);
  }

  @Override
  public boolean addAll(final int index, final Collection<? extends T> c) {
    throw new UnsupportedOperationException("add with index not supported as it would change the order.");
  }

  /**
   * Returns T by the id of the object.
   *
   * @param id id as set in the object
   * @return object if found or else null
   */
  public T getById(final int id) {
    final int index = findIndexById(id);

    return index == -1 ? null : get(index);
  }

  /**
   * Returns T using the index in the list. To get T by id use {@link #getById(int)}.
   * @param index index in list
   * @return T
   */
  @Override
  public T get(final int index) {
    return super.get(index);
  }

  @Override
  public boolean remove(final Object o) {
    return super.remove(o instanceof HasId ? getById(((HasId) o).getId()) : o);
  }

  /**
   * Replaces the current object in the list that has same id as the given id with given object.
   * @param element object to insert at the same position
   */
  public void replace(final T element) {
    final int index = findIndexById(element.getId());
    if (index >= 0) {
      super.set(index, element);
    }
  }

  @Override
  public T set(final int index, final T element) {
    final T old = super.set(index, element);

    element.setId(old.getId());
    return old;
  }

  /**
   * Finds the index in the list by searching using the id.
   *
   * @param id id to use to search through the list
   * @return index in the list or -1 if not found
   */
  private int findIndexById(final int id) {
    final int correctIndex;

    // if empty or id below the offset value the id is certainly not present.
    if (isEmpty() || id < offset) {
      correctIndex = -1;
    } else if (listComplete(this, offset)) {
      //if the list is complete (no gaps), the ID is easily obtained (just check that it actually fits)
      final int idWithinCompleteList = id - offset;
      correctIndex = idWithinCompleteList < size() ? idWithinCompleteList : -1;
    } else {
      //if there are gaps, it's a bit harder
      correctIndex = findIndexByIdHardWay(id);
    }
    return correctIndex;
  }

  private int findIndexByIdHardWay(final int id) {
    int min = 0;
    int max = size();
    while (min <= max && min < size()) {
      // min + half the delta of max - min
      final int mid = min + (max - min) / 2;

      if (get(mid).getId() > id) {
        max = mid - 1;
      } else if (get(mid).getId() < id) {
        min = mid + 1;
      } else {
        return mid;
      }
    }
    return -1;
  }

  public static <S extends HasState & HasId> int findFreeId(final List<S> list) {
    return findFreeId(list, 0);
  }

  /**
   * Find the first id that is not set on an object (starting from begin of the list)
   * or if all id's line up it returns the first new id to be used; size + offset
   *
   * @return first free id of the list
   */
  public static <S extends HasState & HasId> int findFreeId(final List<S> list, final int offset) {
    int freeId;

    if (list.isEmpty()) {
      // if empty the first free is the offset
      freeId = offset;
    } else if (listComplete(list, offset)) {
      // if no numbers missing the last entry should be the same as max
      freeId = offset + list.size();
    } else {
      // else start finding the free id in the gaps (filling those up).
      freeId = findFreeIdInGaps(list, offset);
    }

    return freeId;
  }

  private static <S extends HasState & HasId> int findFreeIdInGaps(final List<S> list, final int offset) {
    int min = 0;
    int max = list.size();
    // else start finding the free id
    while (min <= max) {
      // min + half the delta of max - min
      final int mid = min + (max - min) / 2;

      // if the id at mid matches mid no id is free below mid
      if (list.get(mid).getId() == mid + offset) {
        min = mid + 1;
      } else {
        max = mid - 1;
      }
    }
    return min + offset;
  }

  private static <S extends HasState & HasId> boolean listComplete(final List<S> list, final int offset) {
    final int max = list.size();

    return list.get(max - 1).getId() == offset + max - 1;
  }
}
