/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for FarmLodgingSystemCategories.
 */
public abstract class AbstractFarmLodgingSystemCategory extends AbstractEmissionCategory implements HasFarmLodgingSystemDefinitions {

  private static final long serialVersionUID = -342526970364827410L;

  /**
   * Whether this system is an air scrubber (luchtwasser).
   */
  private boolean scrubber;

  /**
   * The list of system definitions that belong to this farm lodging type.
   */
  private List<FarmLodgingSystemDefinition> farmLodgingSystemDefinitions = new ArrayList<FarmLodgingSystemDefinition>();

  /**
   * Default public constructor.
   */
  public AbstractFarmLodgingSystemCategory() {
    // No-op.
  }

  public AbstractFarmLodgingSystemCategory(final int id, final String code, final String name, final String description, final boolean scrubber) {
    super(id, code, name, description);
    this.scrubber = scrubber;
  }

  public boolean isScrubber() {
    return scrubber;
  }

  public void setScrubber(final boolean scrubber) {
    this.scrubber = scrubber;
  }

  @Override
  public List<FarmLodgingSystemDefinition> getFarmLodgingSystemDefinitions() {
    return farmLodgingSystemDefinitions;
  }

  public void setFarmLodgingSystemDefinitions(final List<FarmLodgingSystemDefinition> farmLodgingSystemDefinitions) {
    this.farmLodgingSystemDefinitions = farmLodgingSystemDefinitions;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append(super.toString());
    sb.append(", scrubber=");
    sb.append(scrubber);
    sb.append(", ");

    sb.append("farmLodgingSystemDefinitions=[");
    for (final FarmLodgingSystemDefinition farmLodgingSystemDefinition : farmLodgingSystemDefinitions) {
      if (farmLodgingSystemDefinition != farmLodgingSystemDefinitions.get(0)) {
        sb.append("; ");
      }
      sb.append(farmLodgingSystemDefinition.getCode());
    }
    sb.append("], ");

    return sb.toString();
  }

}
