/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.HasName;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory.CategoryUnit;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.shared.exception.AeriusException;

public class PlanEmissionSource extends EmissionSourceCollection<PlanEmission> {

  private static final long serialVersionUID = 7512971553378327969L;

  public static class PlanEmission extends CategorizedEmissionSubSource<PlanCategory> implements HasName, Serializable {

    private static final long serialVersionUID = -6584773810571231459L;

    private String name;

    public PlanEmission() {
      // Needed for GWT
    }

    @Override
    public PlanEmission copy() {
      return copyTo(new PlanEmission());
    }

    protected <U extends PlanEmission> U copyTo(final U copy) {
      super.copyTo(copy);
      copy.setName(name);

      return copy;
    }

    @Override
    public boolean equals(final Object obj) {
      return obj != null && this.getClass() == obj.getClass() ? getId() == ((PlanEmission) obj).getId() : false;
    }

    @Override
    double getEmission(final EmissionValueKey key) {
      return getCategory() == null ? 0
          : ((getCategory().getCategoryUnit() == CategoryUnit.NO_UNIT ? 1 : getAmount()) * getCategory().getEmissionFactor(key));
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public int hashCode() {
      return getId();
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = super.getStateHash();
      result = prime * result + getId();
      result = prime * result + ((name == null) ? 0 : name.hashCode());
      return result;
    }

    public void setEmissions(final EmissionValues emissions) {
      //NO-OP
    }

    @Override
    public void setName(final String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return "PlanEmission [id=" + getId() + ", name=" + name + ", category=" + getCategory() + ", amount=" + getAmount() + "]";
    }
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public PlanEmissionSource copy() {
    return copyTo(new PlanEmissionSource());
  }
}
