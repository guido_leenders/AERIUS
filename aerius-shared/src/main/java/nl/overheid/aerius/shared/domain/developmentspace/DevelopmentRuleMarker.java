/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.developmentspace;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;

/**
 *
 */
public class DevelopmentRuleMarker extends Point implements HasId {

  private static final long serialVersionUID = -8217690360268746277L;

  private int receptorId;
  private DevelopmentRule markerType;

  public int getReceptorId() {
    return receptorId;
  }

  public void setReceptorId(final int receptorId) {
    this.receptorId = receptorId;
  }

  public DevelopmentRule getMarkerType() {
    return markerType;
  }

  public void setMarkerType(final DevelopmentRule markerType) {
    this.markerType = markerType;
  }

  @Override
  public int getId() {
    return receptorId;
  }

  @Override
  public void setId(final int id) {
    this.receptorId = id;
  }

}
