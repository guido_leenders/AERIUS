/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Summmary class for Sector Summaries in a scenario.
 */
public class ScenarioSectorSummary implements Serializable {

  private static final long serialVersionUID = 5558380528842079354L;

  private Map<Sector, SectorEmissionSummary> current = new HashMap<>();
  private Map<Sector, SectorEmissionSummary> proposed = new HashMap<>();
  private Map<Sector, SectorEmissionSummary> delta = new HashMap<>();

  public void putCurrentSummary(final EmissionSourceList sourceList, final List<EmissionValueKey> keys) {
    putSources(current, sourceList, keys);
  }

  public void putProposedSummary(final EmissionSourceList sourceList, final List<EmissionValueKey> keys) {
    putSources(proposed, sourceList, keys);
  }

  public void calculateDelta() {
    fillMap(delta, current);
    fillMap(delta, proposed);
    for (final Entry<Sector, SectorEmissionSummary> deltaEntry : delta.entrySet()) {
      final Sector sector = deltaEntry.getKey();
      final SectorEmissionSummary deltaSES = deltaEntry.getValue();
      final SectorEmissionSummary currentSES = current.get(sector);
      final SectorEmissionSummary propSES = proposed.get(sector);

      if (propSES != null) {
        deltaSES.setNumberOfSources(propSES.getNumberOfSources());
        for(final Entry<EmissionValueKey, Double> propEmission : propSES.getEmissions()) {
          deltaSES.addEmission(propEmission.getKey(), propEmission.getValue());
        }
      }
      if (currentSES != null) {
        deltaSES.setNumberOfSources(deltaSES.getNumberOfSources() - currentSES.getNumberOfSources());
        for(final Entry<EmissionValueKey, Double> currentEmission : currentSES.getEmissions()) {
          final EmissionValueKey key = currentEmission.getKey();
          deltaSES.addEmission(key, -currentEmission.getValue());
        }
      }
    }
  }

  public Map<Sector, SectorEmissionSummary> getProposed() {
    return proposed;
  }

  public Map<Sector, SectorEmissionSummary> getDelta() {
    return delta;
  }

  private void putSources(final Map<Sector, SectorEmissionSummary> mapToFill, final EmissionSourceList sourceList,
      final List<EmissionValueKey> keys) {
    for (final EmissionSource source : sourceList) {
      final Sector sector = source.getSector();
      if (!mapToFill.containsKey(sector)) {
        mapToFill.put(sector, new SectorEmissionSummary(sector));
      }
      mapToFill.get(sector).addEmissions(source, keys);
    }
  }

  private void fillMap(final Map<Sector, SectorEmissionSummary> mapToInit, final Map<Sector, SectorEmissionSummary> summaryMap) {
    for (final Entry<Sector, SectorEmissionSummary> propEntry : summaryMap.entrySet()) {
      final Sector sector = propEntry.getKey();
      if (!mapToInit.containsKey(sector)) {
        mapToInit.put(sector, new SectorEmissionSummary(sector));
      }
    }
  }
}
