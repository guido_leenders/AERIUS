/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;

/**
 * Emission details for standard vehicle classes.
 */
public class VehicleStandardEmissions extends VehicleEmissions implements Serializable {

  private static final long serialVersionUID = 813240930188095089L;

  private RoadEmissionCategory emissionCategory;
  private double stagnationFraction;

  @Override
  public VehicleStandardEmissions copy() {
    return copyTo(new VehicleStandardEmissions());
  }

  protected <U extends VehicleStandardEmissions> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setEmissionCategory(emissionCategory);
    copy.setStagnationFraction(stagnationFraction);
    return copy;
  }

  public RoadEmissionCategory getEmissionCategory() {
    return emissionCategory;
  }

  @Override
  public double getEmission(final EmissionValueKey key) {
    final double emission;
    if (emissionCategory == null) {
      emission = 0;
    } else {
      emission = ((1 - stagnationFraction) * emissionCategory.getEmissionFactor(key)
          + stagnationFraction * emissionCategory.getStagnatedEmissionFactor(key)) * getVehiclesPerYear();
    }
    // Constants for conversion from emission per year per gram per kilometer to
    // emission per year per kg per meter.
    //
    // <p>Formula: emission / (1000.0 kg * 1000.0 m)
    // <p>Constants split over 2 variables to improve calculation precision.
    return emission / SharedConstants.GRAM_PER_KM_TO_KG_PER_METER;
  }

  @Min(0)
  @Max(1)
  public double getStagnationFraction() {
    return stagnationFraction;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = super.getStateHash();
    result = prime * result + ((emissionCategory == null) ? 0 : emissionCategory.getStateHash());
    result = (int) (prime * result + stagnationFraction);
    return result;
  }

  public void setEmissionCategory(final RoadEmissionCategory emissionCategory) {
    this.emissionCategory = emissionCategory;
  }

  public void setStagnationFraction(final double stagnationFraction) {
    this.stagnationFraction = stagnationFraction;
  }

  @Override
  public String toString() {
    return "RoadEmissionValues [" + super.toString() + ", emissionCategory=" + emissionCategory
        + ", stagnationFraction=" + stagnationFraction + "]";
  }
}
