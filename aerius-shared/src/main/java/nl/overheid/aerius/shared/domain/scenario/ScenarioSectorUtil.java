/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.ScenarioSectorInformation;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Contains various util methods to calculate aggregated information by sector on the emission sources within that sector.
 */
public final class ScenarioSectorUtil {

  // Not to be constructed.
  private ScenarioSectorUtil() {
  }

  /**
   * Determine the differences between 2 lists of emission sources, grouped by sector.
   * @param current The list that is used as the current situation .
   * @param proposed The list that is used as the proposed situation.
   * @param emissionValueKey The emissionValueKey to determine emission differences for.
   * @return The list of differences between the 2 lists.
   */
  public static ArrayList<SectorEmissionSummary> getDifferences(final EmissionSourceList current, final EmissionSourceList proposed,
      final EmissionValueKey emissionValueKey) {
    final ScenarioSectorSummary summary = new ScenarioSectorSummary();
    final List<EmissionValueKey> keys = emissionValueKey.hatch();

    summary.putCurrentSummary(current, keys);
    summary.putProposedSummary(proposed, keys);
    summary.calculateDelta();
    final ArrayList<SectorEmissionSummary> delta = new ArrayList<>(summary.getDelta().values());
    Collections.sort(delta, SectorEmissionSummary.COMPARATOR);
    return delta;
  }

  /**
   * @param sourceList The list of sources to determine the main sector for.
   * @param year the year the year for which total emission should be calculated.
   * @return The main sector (depends on total emissions for each sector and the number of sources).
   */
  public static ScenarioSectorInformation determineSectorInformation(final EmissionSourceList sourceList, final int year) {
    final ScenarioSectorSummary summary = new ScenarioSectorSummary();
    summary.putProposedSummary(sourceList, EmissionValueKey.getEmissionValueKeys(year, Substance.NOXNH3.hatch()));
        final ArrayList<SectorEmissionSummary> proposed = new ArrayList<SectorEmissionSummary>(summary.getProposed().values());
    Collections.sort(proposed, SectorEmissionSummary.COMPARATOR);
    return new ScenarioSectorInformation(proposed.isEmpty() ? Sector.SECTOR_DEFAULT : proposed.get(0).getSector(), proposed.size() > 1);
  }
}
