/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;

/**
 * Emissions for roads were the emission is specified in gram per meter per vehicle.
 */
public class VehicleCustomEmissions extends VehicleEmissions implements Serializable {

  private static final long serialVersionUID = 5036617458623275589L;

  private String description;
  private EmissionValues emissionValues = new EmissionValues(false);

  @Override
  public VehicleCustomEmissions copy() {
    return copyTo(new VehicleCustomEmissions());
  }

  protected <U extends VehicleCustomEmissions> U copyTo(final U copy) {
    super.copyTo(copy);

    copy.setDescription(description);
    copy.setEmissionValues(emissionValues.copy());
    return copy;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  @Override
  double getEmission(final EmissionValueKey key) {
    return emissionValues.getEmission(key) * getVehiclesPerYear() / SharedConstants.GRAM_PER_KM_TO_KG_PER_METER;
  }

  public EmissionValues getEmissionValues() {
    return emissionValues;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = super.getStateHash();
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + emissionValues.getStateHash();
    return result;
  }

  /**
   * Set emission for substance in total emission per vehicle per day.
   * @param substance substance
   * @param value emission
   */
  public void setEmission(final Substance substance, final double value) {
    emissionValues.setEmission(substance, value);
  }

  public void setEmissionValues(final EmissionValues emissionValues) {
    this.emissionValues = emissionValues;
  }
}
