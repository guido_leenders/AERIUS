/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

/**
 * Base class for road emissions.
 */
public abstract class VehicleEmissions extends EmissionSubSource implements Serializable {

  private static final long serialVersionUID = 8092105733074233767L;

  private double vehiclesPerTimeUnit;
  private TimeUnit timeUnit = TimeUnit.DAY;

  public void setVehicles(final double vehiclesPerTimeUnit, final TimeUnit timeUnit) {
    setVehiclesPerTimeUnit(vehiclesPerTimeUnit);
    setTimeUnit(timeUnit);
  }

  public double getVehiclesPerTimeUnit() {
    return vehiclesPerTimeUnit;
  }

  public void setVehiclesPerTimeUnit(final double vehiclesPerTimeUnit) {
    this.vehiclesPerTimeUnit = vehiclesPerTimeUnit;
  }

  public TimeUnit getTimeUnit() {
    return timeUnit;
  }

  public void setTimeUnit(final TimeUnit timeUnit) {
    this.timeUnit = timeUnit;
  }

  public double getVehiclesPerYear() {
    return timeUnit.getPerYear(vehiclesPerTimeUnit);
  }

  @Override
  public abstract VehicleEmissions copy();

  /**
   * Makes a copy of this object.
   * @return new instance
   */
  protected <U extends VehicleEmissions> U copyTo(final U copy) {
    super.copyTo(copy);
    copy.setVehiclesPerTimeUnit(vehiclesPerTimeUnit);
    copy.setTimeUnit(timeUnit);
    return copy;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = prime * getId();
    result = (int) (prime * result + vehiclesPerTimeUnit);
    result = (int) (prime * result + ((timeUnit == null) ? 0 : timeUnit.hashCode()));
    return result;
  }

  public boolean isYearDependent() {
    return true;
  }

  @Override
  public String toString() {
    return "id=" + getId() + ", vehiclesPerTimeUnit=" + vehiclesPerTimeUnit + ", timeUnit=" + timeUnit;
  }

}
