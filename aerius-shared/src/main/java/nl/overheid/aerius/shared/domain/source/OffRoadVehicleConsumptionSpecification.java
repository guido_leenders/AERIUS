/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

/**
 *
 */
public class OffRoadVehicleConsumptionSpecification extends OffRoadVehicleSpecification {

  private static final long serialVersionUID = -550034155441668822L;

  private int usage;
  private int energyEfficiency;
  private double emissionFactor;

  public double getEmissionFactor() {
    return emissionFactor;
  }

  public void setEmissionFactor(final double emissionFactor) {
    this.emissionFactor = emissionFactor;
  }

  public int getEnergyEfficiency() {
    return energyEfficiency;
  }

  public void setEnergyEfficiency(final int energyEfficiency) {
    this.energyEfficiency = energyEfficiency;
  }

  public int getUsage() {
    return usage;
  }

  public void setUsage(final int usage) {
    this.usage = usage;
  }

  /**
   * Formulas:
   *  energyEfficiencyInLitersPerKWH = 1 / DENSITY_FUEL * energyEfficiencyInGramPerKWH / 1000
   *  emissionWithFuelUsage = fuelUsage / energyEfficiencyInLitersPerKWH * (emissionFactorFuel / 1000);
   *
   * changed to improve performance
   *  1 / energyEfficiencyInLitersPerKWH = DENSITY_FUEL * 1000 / energyEfficiencyInGramPerKWH
   * Therefore:
   *  emissionWithFuelUsage = (usage * emissionFactor) / 1000 * (DENSITY_FUEL * 1000 / energyEfficiencyInGramPerKWH)
   *    = usage * emissionFactor * DENSITY_FUEL / energyEfficiencyInGramPerKWH
   *
   *    The calculation starts with the emission factor as that is a double and forces the calculation to be done with
   *    doubles. If we use another order, it will calculate in ints as long as it can and run over the limits resulting
   *    in negatives...
   *
   *
   * @return 0 when energyEfficiencyInGramPerKWH is 0 or no fuel is chosen, otherwise emission NOx in kilograms per year
   */
  @Override
  public double getEmission() {
    return (energyEfficiency == 0) || (getFuelType() == null) ? 0
        : (emissionFactor * usage * getFuelType().getDensityFuel()) / energyEfficiency;
  }

  @Override
  public OffRoadVehicleSpecification copy() {
    return copyTo(new OffRoadVehicleConsumptionSpecification());
  }

  public <U extends OffRoadVehicleConsumptionSpecification> U copyTo(final U copy) {
    super.copyTo(copy);

    copy.setUsage(usage);
    copy.setEnergyEfficiency(energyEfficiency);
    copy.setEmissionFactor(emissionFactor);

    return copy;
  }
}
