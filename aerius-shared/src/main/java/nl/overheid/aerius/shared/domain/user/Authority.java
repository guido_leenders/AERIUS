/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.user;

import java.io.Serializable;
import java.util.HashMap;

import nl.overheid.aerius.shared.domain.HasId;

/**
 * An authority (bevoegd gezag).
 * Can be a province, a ministry or some other party.
 */
public class Authority implements Serializable, HasId {

  private static final long serialVersionUID = 7904791426210521810L;

  private int id;
  private String code;
  private String description;
  private String emailAddress;
  private HashMap<AuthoritySetting, String> settings = new HashMap<AuthoritySetting, String>();

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(final String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(final String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public HashMap<AuthoritySetting, String> getSettings() {
    return settings;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && ((Authority) obj).id == this.id;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Authority [id=" + id + ", code=" + code + ", description=" + description + ", settings=" + settings + "]";
  }

}
