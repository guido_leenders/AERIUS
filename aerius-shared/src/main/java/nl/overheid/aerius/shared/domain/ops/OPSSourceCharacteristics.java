/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.ops;

import java.io.Serializable;
import java.util.Locale;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import nl.overheid.aerius.shared.domain.HasState;

/**
 * OPS source characteristics.
 */
public class OPSSourceCharacteristics implements Serializable, HasState {

  private static final long serialVersionUID = -2394958448814899734L;

  public enum HeatContentType {
    NOT_FORCED,
    FORCED;

    /**
     * @return Returns the name in lowercase.
     */
    @Override
    public String toString() {
      return name().toLowerCase(Locale.ENGLISH);
    }
  }

  public static final Double AVERAGE_SURROUNDING_TEMPERATURE = 11.85;

  private HeatContentType heatContentType = HeatContentType.NOT_FORCED;

  /**
   * heatContent - heat content (MW)
   */
  private double heatContent;

  /**
   * emissionTemperature
   */
  private double emissionTemperature = AVERAGE_SURROUNDING_TEMPERATURE;

  /**
   * outflowDiameter
   */
  private double outflowDiameter = OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM;

  /**
   * outflowVelocity
   */
  private double outflowVelocity;

  /**
   * OutflowDirection - Default value OutflowDirectionType.VERTICAL_FORCED
   */
  private OutflowDirectionType outflowDirection = OutflowDirectionType.VERTICAL;

  /**
   * heatContentSpecification - The specification of how the heat content was calculated.
   */
  private HeatContentSpecification heatContentSpecification;

  /**
   * emissionHeight - source emission height (m)
   */
  private double emissionHeight;

  /**
   * buildInfluence - enable length width and height fields
   */
  private boolean buildingInfluence;

  /**
   * buildingHeight - height of the building (m)
   */
  private double buildingHeight = OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM;

  /**
   * buildingLength - height of the building (m)
   */
  private double buildingLength = OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM;

  /**
   * buildingWidth - width of the building (m)
   */
  private double buildingWidth = OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM;

  /**
   * buildingOrientation - width of the building (degrees)
   */
  private Integer buildingOrientation = (int) OPSLimits.SCOPE_BUILDING_ORIENTATION_MINIMUM;

  /**
   * diameter - source diameter (m)
   */
  private int diameter;

  /**
   * spread - vertical spread of source height(s) (m)
   */
  private double spread;

  /**
   * diurnalVariationSpecification - specification for diurnal variation
   */
  private DiurnalVariationSpecification diurnalVariationSpecification;

  /**
   * particleSizeDistribution - code for distribution of particle sizes (0 for gasses, > 0 for particulate matter)
   */
  private int particleSizeDistribution;

  public OPSSourceCharacteristics() {
    diurnalVariationSpecification = new DiurnalVariationSpecification();
  }

  /**
   * @param heatContent height content (MW)
   * @param emissionHeight source height (m)
   * @param diameter source diameter (m)
   * @param spread vertical spread of source height(s) (m)
   * @param diurnalVariationSpecification diurnal variation specification
   * @param particleSizeDistribution code for particle-size distribution (>= 0)
   */
  public OPSSourceCharacteristics(final double heatContent, final double emissionHeight, final int diameter, final double spread,
      final DiurnalVariationSpecification diurnalVariationSpecification, final int particleSizeDistribution) {
    this.heatContent = heatContent;
    this.emissionHeight = emissionHeight;
    this.diameter = diameter;
    this.spread = spread;
    this.diurnalVariationSpecification = diurnalVariationSpecification == null ? new DiurnalVariationSpecification() : diurnalVariationSpecification;
    this.particleSizeDistribution = particleSizeDistribution;
  }

  /**
   * Copies object.
   *
   * @return new instance of this object
   */
  public OPSSourceCharacteristics copy() {
    final OPSSourceCharacteristics copy = new OPSSourceCharacteristics(
        heatContent, emissionHeight, diameter, spread, diurnalVariationSpecification.copy(), particleSizeDistribution);
    copy.setHeatContentType(getHeatContentType());
    copy.setEmissionTemperature(emissionTemperature);
    copy.setOutflowDiameter(outflowDiameter);
    copy.setOutflowDirection(outflowDirection);
    copy.setOutflowVelocity(outflowVelocity);
    copy.setBuildingInfluence(isBuildingInfluence());
    copy.setBuildingHeight(buildingHeight);
    copy.setBuildingLength(buildingLength);
    copy.setBuildingWidth(buildingWidth);
    copy.setBuildingOrientation(buildingOrientation);
    //
    copy.setHeatContentSpecification(heatContentSpecification == null ? null : heatContentSpecification.copy());
    return copy;
  }

  @Override
  public boolean equals(final Object obj) {
    if ((obj != null) && (this.getClass() == obj.getClass())) {
      final OPSSourceCharacteristics opsc = (OPSSourceCharacteristics) obj;

      return (Math.abs(heatContent - opsc.heatContent) < 0.00001)
          && (Math.abs(emissionHeight - opsc.emissionHeight) < 0.00001)
          && (diameter == opsc.diameter)
          && (Math.abs(spread - opsc.spread) < 0.00001)
          && (diurnalVariationSpecification.equals(opsc.diurnalVariationSpecification))
          && (particleSizeDistribution == opsc.particleSizeDistribution);
    }
    return false;
  }

  public double getEmissionTemperature() {
    return emissionTemperature;
  }

  public void setEmissionTemperature(final double emissionTemperature) {
    this.emissionTemperature = emissionTemperature;
  }

  public double getOutflowDiameter() {
    return outflowDiameter;
  }

  public void setOutflowDiameter(final double outflowDiameter) {
    this.outflowDiameter = outflowDiameter;
  }

  public double getOutflowVelocity() {
    return outflowVelocity;
  }

  public void setOutflowVelocity(final double outflowVelocity) {
    this.outflowVelocity = outflowVelocity;
  }

  public OutflowDirectionType getOutflowDirection() {
    return outflowDirection;
  }

  public void setOutflowDirection(final OutflowDirectionType outflowDirection) {
    this.outflowDirection = outflowDirection;
  }

  /**
   * @return the HeatContent
   */
  @Min(value = OPSLimits.SOURCE_HEAT_CONTENT_MINIMUM, message = "ops heat_content > " + OPSLimits.SOURCE_HEAT_CONTENT_MINIMUM)
  @Max(value = OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM, message = "ops heat_content < " + OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM)
  public double getHeatContent() {
    return heatContent;
  }

  public HeatContentSpecification getHeatContentSpecification() {
    return heatContentSpecification;
  }

  /**
   * @return the h
   */
  @Min(value = (int) OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, message = "ops height > " + OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM)
  @Max(value = (int) OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM, message = "ops height < " + OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM)
  public double getEmissionHeight() {
    return emissionHeight;
  }

  /**
   * @return the d
   */
  @Min(value = OPSLimits.SOURCE_DIAMETER_MINIMUM, message = "ops diameter > " + OPSLimits.SOURCE_DIAMETER_MINIMUM)
  @Max(value = OPSLimits.SOURCE_DIAMETER_MAXIMUM, message = "ops diameter < " + OPSLimits.SOURCE_DIAMETER_MAXIMUM)
  public int getDiameter() {
    return diameter;
  }

  /**
   * @return the s (Source height distribution)
   */
  @Min(value = OPSLimits.SOURCE_SPREAD_MINIMUM, message = "ops spread < " + OPSLimits.SOURCE_SPREAD_MINIMUM)
  @Max(value = OPSLimits.SOURCE_SPREAD_MAXIMUM, message = "ops spread > " + OPSLimits.SOURCE_SPREAD_MAXIMUM)
  public double getSpread() {
    return spread;
  }

  /**
   *
   * @return buildingHeight
   */
  public double getBuildingHeight() {
    return buildingHeight;
  }

  /**
   *
   * @return buildingWidth
   */
  public double getBuildingWidth() {
    return buildingWidth;
  }

  /**
   *
   * @return buildingLength
   */
  public double getBuildingLength() {
    return buildingLength;
  }

  public Integer getBuildingOrientation() {
    return buildingOrientation;
  }

  public DiurnalVariationSpecification getDiurnalVariationSpecification() {
    return diurnalVariationSpecification;
  }

  /**
   * @return the particle-size distribution
   */
  @Min(value = OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM,
      message = "ops particle_size_distribution > " + OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM)
  @Max(value = OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM,
  message = "ops particle_size_distribution < " + OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM)
  public int getParticleSizeDistribution() {
    return particleSizeDistribution;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + new Double(heatContent).hashCode();
    result = (prime * result) + new Double(emissionHeight).hashCode();
    result = (prime * result) + diameter;
    result = (prime * result) + new Double(spread).hashCode();
    result = (prime * result) + diurnalVariationSpecification.hashCode();
    result = (prime * result) + particleSizeDistribution;
    return result;
  }

  @Override
  public int hashCode() {
    return getStateHash();
  }

  /**
   * return the type of outlfow standard or custom
   *
   * @return outflowType
   */
  public HeatContentType getHeatContentType() {
    return heatContentType;
  }

  /**
   *
   * @param heatContentType
   */
  public void setHeatContentType(final HeatContentType heatContentType) {
    this.heatContentType = heatContentType;
  }

  /**
   * @param heatContent the heat content to set
   */
  public void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  public void setHeatContentSpecification(final HeatContentSpecification heatContentSpecification) {
    this.heatContentSpecification = heatContentSpecification;
  }

  /**
   * @param height the height to set
   */
  public void setEmissionHeight(final double height) {
    this.emissionHeight = height;
  }

  /**
   * @param diameter the diameter to set
   */
  public void setDiameter(final int diameter) {
    this.diameter = diameter;
  }

  /**
   * @param spread the spread to set
   */
  public void setSpread(final double spread) {
    this.spread = spread;
  }

  public void setDiurnalVariationSpecification(final DiurnalVariationSpecification diurnalVariationSpecification) {
    this.diurnalVariationSpecification = diurnalVariationSpecification == null ? new DiurnalVariationSpecification() : diurnalVariationSpecification;
  }

  /**
   * @param particleSizeDistribution
   *          the particle-size distribution to set
   */
  public void setParticleSizeDistribution(final int particleSizeDistribution) {
    this.particleSizeDistribution = particleSizeDistribution;
  }

  public void setBuildingHeight(final double buildingHeight) {
    this.buildingHeight = buildingHeight;
  }

  /**
   * The effective emission height could be a function of the building height, the emission height, or a combination of both.
   *
   * OPS only takes an emission height parameter and doesn't incorporate any building height, which
   * can - in practice - interfere with an emission's deposition. The formulae used here to correct the emission height 'appear' to
   * accurately correct how a building with a certain height affects deposition.
   *
   * The formula that used to be here was incorrect. Research is being done what the proper formula/method would be.
   * Until that research is done, we're just returning the emission height.
   *
   * @return The effective emission height
   */
  public double getEffectiveHeight() {
    return emissionHeight;
  }

  public boolean isUserdefinedHeatContent() {
    return getHeatContentType() == HeatContentType.NOT_FORCED;
  }

  public boolean isBuildingInfluence() {
    return buildingInfluence;
  }

  public void setBuildingInfluence(final boolean buildingInfluence) {
    this.buildingInfluence = buildingInfluence;
  }

  public void setBuildingWidth(final double buildingWidth) {
    this.buildingWidth = buildingWidth;
  }

  public void setBuildingLength(final double buildingLength) {
    this.buildingLength = buildingLength;
  }

  public void setBuildingOrientation(final Integer buildingOrientation) {
    this.buildingOrientation = buildingOrientation;
  }

  @Override
  public String toString() {
    return "OPSSourceCharacteristics ["
        + ", heatContentType=" + heatContentType + ", heatContent=" + heatContent
        + ", heatContentSpecification=" + heatContentSpecification
        + ", buildInfluence=" + isBuildingInfluence() + ",  buildingLength= " + buildingLength + ", buildingWidth=" + buildingWidth
        + ", buildingHeight=" + buildingHeight + ", buildingOrientation=" + buildingOrientation
        + ", emissionHeight=" + emissionHeight + ", diameter=" + diameter
        + ", spread=" + spread + ", diurnalVariationSpecification=" + diurnalVariationSpecification
        + ", particleSizeDistribution=" + particleSizeDistribution
        + "]";
  }



}
