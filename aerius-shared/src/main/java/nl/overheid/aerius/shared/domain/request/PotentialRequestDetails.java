/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import nl.overheid.aerius.shared.domain.HasId;

/**
 * The result of a NB-wet calculation can be expressed as some values per assessment area.
 * This class holds that info (on the server side) and can distill it into a PotentialRequestInfo
 * object as a lean way to send it to the client.
 * Most importantly, there are important checks in here to determine the request type, based on
 * the values per assessment area.
 */
public class PotentialRequestDetails implements Serializable {

  private static final long serialVersionUID = 3696205040498229835L;

  public class AssessmentAreaPotentialRequestDetails implements HasId, Comparable<AssessmentAreaPotentialRequestDetails>, Serializable {

    private static final long serialVersionUID = 347690446660399817L;

    private int assessmentAreaId;
    private final double maxDemand;
    private final double permitThresholdValue;

    /**
     * Object with demand for a specific assessment area by a calculation.
     * @param assessmentAreaId id of the assessment area.
     * @param maxDemand maximum demand by the calculation for the given assessment area.
     * @param permitThresholdValue threshold value; above this value it means a permit is required.
     */
    public AssessmentAreaPotentialRequestDetails(final int assessmentAreaId, final double maxDemand, final double permitThresholdValue) {
      this.assessmentAreaId = assessmentAreaId;
      this.maxDemand = maxDemand;
      this.permitThresholdValue = permitThresholdValue;
    }

    @Override
    public int getId() {
      return assessmentAreaId;
    }

    @Override
    public void setId(final int assessmentAreaId) {
      this.assessmentAreaId = assessmentAreaId;
    }

    public double getMaxDemand() {
      return maxDemand;
    }

    public double getPermitThresholdValue() {
      return permitThresholdValue;
    }

    /**
     * Compares first by {@link PotentialRequestType} and if the same compares the demand.
     */
    @Override
    public int compareTo(final AssessmentAreaPotentialRequestDetails obj) {
      final int requestTypeCompare = getPotentialRequestType().compareTo(obj.getPotentialRequestType());
      return requestTypeCompare == 0 ? Double.compare(maxDemand, obj.getMaxDemand()) : requestTypeCompare;
    }

    /**
     * Returns the {@link PotentialRequestType} needed for this assessment area.
     * @return minimum PotentialRequestType needed
     */
    public PotentialRequestType getPotentialRequestType() {
      final PotentialRequestType potentialRequestType;
      if (maxDemand > defaultPermitThresholdValue) {
        potentialRequestType = PotentialRequestType.PERMIT;
      } else if (maxDemand <= meldingThresholdValue) {
        potentialRequestType = PotentialRequestType.NONE;
      } else if (maxDemand > permitThresholdValue) {
        potentialRequestType = PotentialRequestType.PERMIT_INSIDE_MELDING_SPACE;
      } else {
        potentialRequestType = PotentialRequestType.MELDING;
      }
      return potentialRequestType;
    }
  }

  private final List<AssessmentAreaPotentialRequestDetails> assessmentAreaDetailsList = new ArrayList<>();
  private final double meldingThresholdValue;
  private final double defaultPermitThresholdValue;

  public PotentialRequestDetails(final double meldingThresholdValue, final double defaultPermitThresholdValue) {
    this.meldingThresholdValue = meldingThresholdValue;
    this.defaultPermitThresholdValue = defaultPermitThresholdValue;
  }

  public List<AssessmentAreaPotentialRequestDetails> getAssessmentAreaDetailsList() {
    return assessmentAreaDetailsList;
  }

  public AssessmentAreaPotentialRequestDetails addAssessmentAreaDetails(final int assessmentAreaId, final double maxDemand,
      final double permitThresholdValue) {
    final AssessmentAreaPotentialRequestDetails details = new AssessmentAreaPotentialRequestDetails(assessmentAreaId, maxDemand,
        permitThresholdValue);
    assessmentAreaDetailsList.add(details);
    return details;
  }

  public PotentialRequestInfo createPotentialRequestInfo() {
    final PotentialRequestInfo potentialRequestInfo = new PotentialRequestInfo();
    potentialRequestInfo.setPotentialRequestType(getPotentialRequestType());
    potentialRequestInfo.setExceedingAssessmentAreaNum(getExceedingAssessmentAreaNum());
    return potentialRequestInfo;
  }

  PotentialRequestType getPotentialRequestType() {
    final AssessmentAreaPotentialRequestDetails max = assessmentAreaDetailsList.isEmpty()
        ? null : Collections.max(assessmentAreaDetailsList);
    return max == null ? PotentialRequestType.NONE : max.getPotentialRequestType();
  }

  int getExceedingAssessmentAreaNum() {
    int count = 0;
    for (final AssessmentAreaPotentialRequestDetails areaRequestDetails : assessmentAreaDetailsList) {
      if (areaRequestDetails.getMaxDemand() > areaRequestDetails.getPermitThresholdValue()) {
        count++;
      }
    }
    return count;
  }

}
