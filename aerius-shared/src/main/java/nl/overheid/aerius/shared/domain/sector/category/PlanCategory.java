/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector.category;

import java.util.ArrayList;
import java.util.HashMap;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;

/**
 * Categories related to plans. A plan category consists of a unit and a factor
 * for the specific unit or no unit. Plans have no year specific factors.
 */
public class PlanCategory extends AbstractEmissionCategory {

  public enum CategoryUnit {
    HECTARE,
    GIGA_JOULE,
    MEGA_WATT_HOURS,
    COUNT,
    // No unit means the emission factor is 1.
    NO_UNIT,
    SQUARE_METERS,
    TONNES;
  }

  private static final long serialVersionUID = -2833586667092794398L;

  private SectorIcon sectorIcon;
  private OPSSourceCharacteristics characteristics;
  private CategoryUnit categoryUnit;
  private HashMap<Substance, Double> emissionFactors = new HashMap<Substance, Double>();

  public CategoryUnit getCategoryUnit() {
    return categoryUnit;
  }

  public OPSSourceCharacteristics getCharacteristics() {
    return characteristics;
  }


  public double getEmissionFactor(final EmissionValueKey key) {
    return emissionFactors.containsKey(key.getSubstance()) ? emissionFactors.get(key.getSubstance()) : 0;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() ? getId() == ((PlanCategory) obj).getId() : false;
  }

  public ArrayList<Substance> getKeys() {
    return new ArrayList<Substance>(emissionFactors.keySet());
  }

  public SectorIcon getSectorIcon() {
    return sectorIcon;
  }

  @Override
  public int hashCode() {
    return getId();
  }

  public void setCategoryUnit(final CategoryUnit categoryUnit) {
    this.categoryUnit = categoryUnit;
  }

  public void setCharacteristics(final OPSSourceCharacteristics characteristics) {
    this.characteristics = characteristics;
  }

  /**
   * @param substance the Substance to set the emission factor for.
   * @param emissionFactor the emission factor to set.
   */
  public void setEmissionFactor(final Substance substance, final Double emissionFactor) {
    this.emissionFactors.put(substance, emissionFactor);
  }

  public void setSectorIcon(final SectorIcon sectorIcon) {
    this.sectorIcon = sectorIcon;
  }

  @Override
  public String toString() {
    return super.toString() + ", characteristics=" + characteristics + ", categoryUnit=" + categoryUnit  + ", sectorIcon=" + sectorIcon;
  }
}
