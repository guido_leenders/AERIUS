/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario.connect;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;

public class ConnectCalculationInformation implements Serializable {

  private static final long serialVersionUID = -6574286295995722253L;

  public enum ExportType {
//    PDF_DEVELOPMENT_SPACES,
//    PDF_DEMAND,
    PDF_CALCULATION,
    GML_DELTA,
    GML,
    GML_PER_SECTOR;
  }

  public enum CalculationType {
    NBWET;
  }

  private String apiKey;
  private ExportType exportType;
  private CalculationType calculationType;
  private int year;
  private String name;
  private boolean temporaryProjectImpact;
  private int temporaryProjectYears;
  private boolean permitCalculationRadiusTypeImpact;
  private PermitCalculationRadiusType permitCalculationRadiusType;
  private ArrayList<ConnectCalculationFile> files = new ArrayList<>();

  public void add(final ConnectCalculationFile file) {
    files.add(file);
  }

  public ExportType getExportType() {
    return exportType;
  }

  public void setExportType(final ExportType exportType) {
    this.exportType = exportType;
  }

  public CalculationType getCalculationType() {
    return calculationType;
  }

  public void setCalculationType(final CalculationType calculationType) {
    this.calculationType = calculationType;
  }

  public int getYear() {
    return year;
  }

  public void setYear(final int year) {
    this.year = year;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(final String apiKey) {
    this.apiKey = apiKey;
  }

  public ArrayList<ConnectCalculationFile> getFiles() {
    return files;
  }

  public int getTemporaryProjectYears() {
    return temporaryProjectYears;
  }

  public void setTemporaryProjectYears(final int temporaryProjectYears) {
    this.temporaryProjectYears = temporaryProjectYears;
  }

  public boolean isTemporaryProjectImpact() {
    return temporaryProjectImpact;
  }

  public void setTemporaryProjectImpact(final boolean temporaryProjectImpact) {
    this.temporaryProjectImpact = temporaryProjectImpact;
  }

  public boolean isPermitCalculationRadiusTypeImpact() {
    return permitCalculationRadiusTypeImpact;
  }

  public void setPermitCalculationRadiusTypeImpact(final boolean permitCalculationRadiusTypeImpact) {
    this.permitCalculationRadiusTypeImpact = permitCalculationRadiusTypeImpact;
  }

  public PermitCalculationRadiusType getPermitCalculationRadiusType() {
    return permitCalculationRadiusType;
  }

  public void setPermitCalculationRadiusType(final PermitCalculationRadiusType permitCalculationRadiusType) {
    this.permitCalculationRadiusType = permitCalculationRadiusType;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "ScenarioCalculationInformation [apiKey=" + apiKey + ", exportType=" + exportType + ", calculationType=" + calculationType + ", year="
        + year + " name=" + name + ", temporaryProjectImpact=" + temporaryProjectImpact + " temporaryProjectYears=" + temporaryProjectYears
        + " permitCalculationRadiusTypeImpact=" + permitCalculationRadiusTypeImpact + " permitCalculationRadiusTypeImpact="
        + permitCalculationRadiusType + ", files=" + files + "]";
  }

}
