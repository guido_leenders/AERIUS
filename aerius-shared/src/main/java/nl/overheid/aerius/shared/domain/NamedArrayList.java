/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @param <T> List objects also extend {@link HasState} and {@link HasId}
 */
public class NamedArrayList<T extends HasState & HasId> extends ArrayList<T> implements HasId, HasName, HasState, Serializable  {

  private static final long serialVersionUID = -859909471749875849L;
  private int id;
  private String name;

  @Override
  public int getId() {
    return id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    for (final T src : this) {
      result = (prime * result) + id;
      result = (prime * result) + (src == null ? 0 : src.getStateHash());
    }
    // If the result is 0, which could happen in very limited cases, return 1
    return result == 0 ? 1 : result;
  }

  /**
   * List is considered equal if the id's are the same.
   */
  @Override
  public boolean equals(final Object obj) {
    //test for instanceof because subclasses should also be equal
    return (obj instanceof NamedArrayList<?>) && (id == ((HasId) obj).getId());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    return prime * id;
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }
}
