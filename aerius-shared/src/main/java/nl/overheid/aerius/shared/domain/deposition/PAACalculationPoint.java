/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Calculation point as used by the {@link CalculationPointsDataTable}.
 */
public class PAACalculationPoint extends AeriusResultPoint {

  private static final long serialVersionUID = -751963198059284485L;

  private double distanceToClosestSource;
  private double backgroundDeposition;

  /**
   * Convenience method returning background deposition + NOXNH3 deposition.
   * @return total deposition
   */
  public double getTotalDeposition() {
    //TODO I not likey this, but I am told this is the way we do this.. for now..
    return getBackgroundDeposition() + getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION);
  }

  public double getDistanceToClosestSource() {
    return distanceToClosestSource;
  }

  public void setDistanceToClosestSource(final double distanceToClosestSource) {
    this.distanceToClosestSource = distanceToClosestSource;
  }

  public double getBackgroundDeposition() {
    return backgroundDeposition;
  }

  public void setBackgroundDeposition(final double backgroundDeposition) {
    this.backgroundDeposition = backgroundDeposition;
  }

}
