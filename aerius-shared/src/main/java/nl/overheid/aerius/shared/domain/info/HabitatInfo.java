/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.result.CriticalLevels;

/**
 * Basic bean containing all habitat type info for a specific area (could be a receptor, a habitat area etc).
 */
public class HabitatInfo extends HabitatType {
  private static final long serialVersionUID = 2779326631916725984L;

  private CriticalLevels criticalLevels = new CriticalLevels();
  private boolean exceedingCritical;

  private double surface;
  private Map<HabitatSurfaceType, Double> additionalSurfaces = new HashMap<>();
  private double coverage;
  private double coverageRelevant;

  private boolean designated;
  private EcologyQualityType ecologyQuality;
  private HabitatGoal qualityGoal;
  private HabitatGoal extentGoal;

  public enum HabitatSurfaceType {
    TOTAL_MAPPED,
    TOTAL_CARTOGRAPHIC,
    RELEVANT_MAPPED,
    RELEVANT_CARTOGRAPHIC,
  }

  // Needed by GWT.
  public HabitatInfo() {
  }

  public boolean isMapped() {
    return surface > 0;
  }

  public CriticalLevels getCriticalLevels() {
    return criticalLevels;
  }

  public void setCriticalLevels(final CriticalLevels criticalLevels) {
    this.criticalLevels = criticalLevels;
  }

  public double getCoverage() {
    return coverage;
  }

  public void setCoverage(final double coverage) {
    this.coverage = coverage;
  }

  public double getCoverageRelevant() {
    return coverageRelevant;
  }

  public void setCoverageRelevant(final double coverageRelevant) {
    this.coverageRelevant = coverageRelevant;
  }

  public EcologyQualityType getEcologyQuality() {
    return ecologyQuality;
  }

  public void setEcologyQuality(final EcologyQualityType ecologyQuality) {
    this.ecologyQuality = ecologyQuality;
  }

  public boolean isExceedingCritical() {
    return exceedingCritical;
  }

  public void setExceedingCritical(final boolean exceedingCritical) {
    this.exceedingCritical = exceedingCritical;
  }

  public boolean isDesignated() {
    return designated;
  }

  public void setDesignated(final boolean designated) {
    this.designated = designated;
  }

  public double getSurface() {
    return surface; // Always in m2
  }

  public void setSurface(final double surface) {
    this.surface = surface;
  }

  public Double getAdditionalSurface(final HabitatSurfaceType habitatSurfaceType) {
    return additionalSurfaces.get(habitatSurfaceType);
  }

  public void setAdditionalSurface(final HabitatSurfaceType habitatSurfaceType, final Double surface) {
    additionalSurfaces.put(habitatSurfaceType, surface);
  }

  public double getRelevantFraction() {
    final Double relevantSurface = getAdditionalSurface(HabitatSurfaceType.RELEVANT_MAPPED);
    final Double totalSurface = getAdditionalSurface(HabitatSurfaceType.TOTAL_MAPPED);
    if ((relevantSurface == null) || (relevantSurface.doubleValue() == 0.0) || (totalSurface == null) || (totalSurface.doubleValue() == 0.0)) {
      return 0.0;
    } else {
      return relevantSurface.doubleValue() / totalSurface.doubleValue();
    }
  }

  public HabitatGoal getQualityGoal() {
    return qualityGoal;
  }

  public void setQualityGoal(final HabitatGoal qualityGoal) {
    this.qualityGoal = qualityGoal;
  }

  public HabitatGoal getExtentGoal() {
    return extentGoal;
  }

  public void setExtentGoal(final HabitatGoal surfaceGoal) {
    this.extentGoal = surfaceGoal;
  }

}
