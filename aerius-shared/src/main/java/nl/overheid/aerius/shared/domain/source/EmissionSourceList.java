/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

import javax.validation.Valid;

import nl.overheid.aerius.shared.domain.ConsecutiveNamedList;
import nl.overheid.aerius.shared.domain.EmissionValueKey;

/**
 * A list of emission sources and maritime shipping routes.
 */
public class EmissionSourceList extends ConsecutiveNamedList<EmissionSource> implements Serializable {

  private static final long serialVersionUID = 1429845417723917875L;
  private static final int NO_ID = -1;

  @Valid
  private ArrayList<ShippingRoute> maritimeRoutes = new ArrayList<>();

  /**
   * Creates a {@link EmissionSourceList} which starts on index 1 and with id -1.
   */
  public EmissionSourceList() {
    super(1);
    setId(NO_ID);
  }

  /**
   * Add all content of a {@link EmissionSourceList}, including maritime routes.
   * @param esl list to add
   * @return true if successful.
   */
  public boolean addAll(final EmissionSourceList esl) {
    return super.addAll(esl) && getMaritimeRoutes().addAll(esl.getMaritimeRoutes());
  }

  @Override
  public void clear() {
    for (int i = size() - 1; i >= 0; i--) {
      removeDestroy(get(i));
    }
    super.clear();
    maritimeRoutes.clear();
  }

  /**
   * Creates a copy of this {@link EmissionSourceList} into a new object.
   * @return returns a copy of the {@link EmissionSourceList}
   */
  public EmissionSourceList copy() {
    final EmissionSourceList copy = new EmissionSourceList();
    final ArrayList<ShippingRoute> copiedRoutes = new ArrayList<ShippingRoute>();
    for (final ShippingRoute route : maritimeRoutes) {
      copiedRoutes.add(route.copy());
    }
    copy.getMaritimeRoutes().addAll(copiedRoutes);
    copy.setId(NO_ID);
    copy.setName(getName());
    final ArrayList<EmissionSource> sources = new ArrayList<EmissionSource>();

    for (final EmissionSource source : this) {
      sources.add(source.copy());
    }
    copy.setAll(sources);
    return copy;
  }

  /**
   * Removes and detaches maritime routes if present. Use this to remove objects which are destroyed. A normal
   * remove doesn't detach the routes, so the relation would remain.
   * @param source source to remove
   */
  public void removeDestroy(final EmissionSource source) {
    remove(source);
    if (source instanceof MaritimeMooringEmissionSource) {
      ((MaritimeMooringEmissionSource) source).detachRoutes();
    }
  }

  @Override
  public void replace(final EmissionSource source) {
    final EmissionSource original = getById(source.getId());
    if (original != source && original instanceof MaritimeMooringEmissionSource) {
      ((MaritimeMooringEmissionSource) original).detachRoutes();
    }
    super.replace(source);
  }

  public ArrayList<ShippingRoute> getMaritimeRoutes() {
    return maritimeRoutes;
  }

  public void setMaritimeRoutes(final ArrayList<ShippingRoute> maritimeRoutes) {
    this.maritimeRoutes = maritimeRoutes;
  }

  /**
   * Returns the total emission of all sources for the {@link EmissionValueKey}.
   *
   * @param key {@link EmissionValueKey} to get total for
   * @return total emission
   */
  public double getTotalEmission(final EmissionValueKey key) {
    double total = 0;
    for (final EmissionSource source : this) {
      total += source.getEmission(key);
    }
    return total;
  }

  /**
   * Merges complete {@link EmissionSourceList} including maritime routes.
   * @param esl list to add
   * @return the number of sources that were actually merged/overwritten
   */
  public int mergeSources(final EmissionSourceList esl) {
    final int count = mergeSourceList(esl);
    getMaritimeRoutes().addAll(esl.getMaritimeRoutes());
    return count;
  }

  /**
   * Merges the given sources into those that already exist.
   *
   * <ul>
   * <li>If sources already exist and do not equal any location in the supplied sources, they will be kept.
   * <li>If a source's location is equivalent to a supplied source AND both that source's and the supplied's
   *     emission values are of the type GenericEmissionValues (typically industry), the values will be merged.
   * <li>In all other cases where a source's location is equivalent to a supplied source, emission values will be overwritten.
   * <li>If in the supplied 'merge' list multiple sources with an equal location exist (note: this is only in the supplied list), they will all
   *     be imported and kept
   *    (you will end up with multiple sources on the same location, the rule above still applies - existing sources will be merged or overwritten).
   * <li>Also note: if a source already exists on a given location, and _multiple_ supplied sources are on that same location, the _last_ supplied
   *     source's emission values takes precedence over the others
   * <li>By default, an imported source's sector will be 'industry - generic' with a sector id of 1800
   * </ul>
   *
   * @param supplied the sources to merge
   * @return the number of sources that were actually merged/overwritten
   */
  private int mergeSourceList(final ArrayList<EmissionSource> supplied) {
    // Will be filtered out later
    final HashSet<EmissionSource> removalDue = new HashSet<EmissionSource>();

    // First, merge sources that have the same location
    for (final EmissionSource original : this) {
      for (final EmissionSource newSource : supplied) {
        if (newSource.getX() == original.getX() && newSource.getY() == original.getY()) {
          // If the existing source has GenericEmissionValues (as opposed to other implementations such as FarmEmissionValues),
          // merge them, else overwrite them
          if (original instanceof GenericEmissionSource && newSource instanceof GenericEmissionSource) {
            //merge on emission values overwrites old values
            ((GenericEmissionSource) original).getEmissionValues().setOrReplace(((GenericEmissionSource) newSource).getEmissionValues());
            //set the merged values for the new source.
            ((GenericEmissionSource) newSource).setEmissionValues(((GenericEmissionSource) original).getEmissionValues());
          }

          removalDue.add(original);
        }
      }
    }

    //remove all originals
    this.removeAll(removalDue);
    // Then, simply add the the merged sources to the existing ones
    addAll(supplied);
    // Return the number of merges/overwrites (the sources that were removed from original list).
    return removalDue.size();
  }
}
