/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import nl.overheid.aerius.shared.domain.ColorRange;

/**
 *
 */
public enum EmissionResultColorRanges implements Serializable {

  /**
   * The default concentration color range.
   */
  CONCENTRATION(new ColorRange[] {
      new ColorRange(0, "FFFEB4"),
      new ColorRange(10, "FFF391"),
      new ColorRange(11, "FEE86E"),
      new ColorRange(12, "FECF6C"),
      new ColorRange(13, "FDB66A"),
      new ColorRange(14, "D2C15A"),
      new ColorRange(15, "A5CC80"),
      new ColorRange(16, "64A370"),
      new ColorRange(17, "237A60"),
      new ColorRange(18, "0D95BD"),
      new ColorRange(19, "0765B0"),
      new ColorRange(20, "0135A3"),
      new ColorRange(21, "8A70B1"),
      new ColorRange(22, "7E4E8F"),
      new ColorRange(23, "722C6D"),
      new ColorRange(25, "7F3B17"),
      new ColorRange(28, "3E2F10"),
      new ColorRange(40, "110A05")}),
  /**
   * The default deposition color range.
   */
  DEPOSITION(new ColorRange[] {
      new ColorRange(0, "FFFDB3"),
      new ColorRange(0.05, "FDE76A"),
      new ColorRange(1, "FEB66E"),
      new ColorRange(3, "A5CC46"),
      new ColorRange(5, "23A870"),
      new ColorRange(7, "5A7A32"),
      new ColorRange(10, "0093BD"),
      new ColorRange(15, "0D75B5"),
      new ColorRange(20, "6A70B1"),
      new ColorRange(30, "304594"),
      new ColorRange(50, "7F3B17"),
      new ColorRange(75, "5E2C8F"),
      new ColorRange(100, "3F2A84"),
      new ColorRange(150, "2A1612")}),
  /**
   * The default number-of-excess-days (PM10) color range.
   */
  NUM_EXCESS_DAY(new ColorRange[] {
      new ColorRange(6, "FFAAAA"),
      new ColorRange(8, "DD7777"),
      new ColorRange(10, "BB5555"),
      new ColorRange(12, "993333"),
      new ColorRange(15, "771111"),
      new ColorRange(20, "551111"),
      new ColorRange(30, "331111")});

  private ColorRange[] ranges;

  private EmissionResultColorRanges(final ColorRange[] ranges) {
    this.ranges = ranges;
  }

  /**
   * @param emissionResult The value of the result to get the color for.
   * @return The proper (hex) color for the range.
   */
  public String getColor(final double emissionResult) {
    return getRange(emissionResult).getColor();
  }

  /**
   * @param emissionResult The value of the result to get the ColorRange for.
   * @return the proper ColorRange based on input.
   */
  public ColorRange getRange(final double emissionResult) {
    return ranges[getIndex(emissionResult)];
  }

  /**
   * @param emissionResult The value of the result to get the index for.
   * @return The index the corresponding ColorRange object would have in the array returned by values(emissionResultType).
   */
  public int getIndex(final double emissionResult) {
    int index = 0;
    for (int i = ranges.length - 1; i >= 0; i--) {
      if (Double.compare(ranges[i].getLowerValue(), emissionResult) <= 0) {
        index = i;
        break;
      }
    }
    return index;
  }

  /**
   * @return All ranges for this EmissionResultColorRanges in an ArrayList, for your convenience.
   */
  public ArrayList<ColorRange> getRanges() {
    final ArrayList<ColorRange> rangesList = new ArrayList<ColorRange>();
    Collections.addAll(rangesList, ranges);
    return rangesList;
  }

  /**
   * @param resultType The EmissionResultType to determine the proper value of this enum for.
   * @return The right value.
   */
  public static EmissionResultColorRanges valueOf(final EmissionResultType resultType) {
    EmissionResultColorRanges colorEnum = null;
    switch (resultType) {
    case CONCENTRATION:
      colorEnum = CONCENTRATION;
      break;
    case NUM_EXCESS_DAY:
      colorEnum = NUM_EXCESS_DAY;
      break;
    default:
      colorEnum = DEPOSITION;
      break;
    }
    return colorEnum;
  }

}
