/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service containing calls related to fetching geometry from the database.
 */
@RemoteServiceRelativePath("aerius-fetch-geometry")
public interface FetchGeometryService extends RemoteService {

  /**
   * Retrieve a geometry with its boundingbox for an area in the database. The area is a type with an id.
   * @param areaType Type of the area to get the geometry for.
   * @param areaId Database id of the area to get the geometry for.
   * @return Simplified geometry including boundingbox of the geometry.
   * @throws AeriusException
   */
  WKTGeometryWithBox getGeometry(final AreaType areaType, final int areaId) throws AeriusException;

  /**
   * As getGeometry but with additional name guide to make sure the best fit is returned.
   * Use this when you have the geometry name available.
   * @param nameGuide Name of the geometry to look for
   */
  WKTGeometryWithBox getGeometry(final AreaType areaType, final int areaId, final String nameGuide)
      throws AeriusException;

  /**
   * As getGeometry but simplifies the geometry to improve performance.
   * @param tolerance Tolerance in meters for Douglas-Peuker algorithm.
   */
  WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final int areaId, final float tolerance)
      throws AeriusException;

  WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final int areaId, final float tolerance,
      final String nameGuide) throws AeriusException;

  /**
   * As getGeometry but with point within area instead of id.
   * @param pointInArea Point in the area.
   */
  WKTGeometryWithBox getGeometry(final AreaType areaType, final Point pointInArea) throws AeriusException;

  WKTGeometryWithBox getGeometry(final AreaType areaType, final Point pointInArea, final String nameGuide)
      throws AeriusException;

  WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final Point pointInArea, final float tolerance)
      throws AeriusException;

  WKTGeometryWithBox getSimplifiedGeometry(final AreaType areaType, final Point pointInArea, final float tolerance,
      final String nameGuide) throws AeriusException;

  /**
   * Fetches the capabilities from a remote server.
   * @param url to get capabilities.
   * @return capabilities content.
   * @throws AeriusException
   */
  String fetchGetCapabilities(String url) throws AeriusException;
}
