/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.shared.domain.EmissionValueKey;

/**
 * Abstract collection class for {@link EmissionSubSource}.
 * @param <EV> specific EmissionValues implementation
 */
public abstract class EmissionSourceCollection<EV extends EmissionSubSource> extends EmissionSource {

  private static final long serialVersionUID = 8676059750769209986L;

  private EmissionSubSourceList<EV> emissionSubSources = new EmissionSubSourceList<EV>();

  public <E extends  EmissionSourceCollection<EV>> E copyTo(final E copy) {
    super.copyTo(copy);
    copySubSources(copy);
    return copy;
  }

  private void copySubSources(final EmissionSourceCollection<EV> copy) {
    emissionSubSources.copyTo(copy.getEmissionSubSources());
  }

  public double getEmission(final EmissionSubSource emissionSubSource, final EmissionValueKey key) {
    return emissionSubSources.getEmission(emissionSubSource, key, getWeight());
  }

  @Override
  protected double getEmission(final EmissionValueKey key, final double weight) {
    return emissionSubSources.getTotalEmission(key, weight);
  }

  public EmissionSubSourceList<EV> getEmissionSubSources() {
    return emissionSubSources;
  }

  @Override
  public int getStateHash() {
    int state = 0;
    for (final EV item : emissionSubSources) {
      state += item.getStateHash();
    }
    return state;
  }

  @Override
  public boolean hasAnyEmissions() {
    return emissionSubSources != null && !emissionSubSources.isEmpty();
  }

  public void setEmissionSubSources(final EmissionSubSourceList<EV> emissionSubSources) {
    this.emissionSubSources = emissionSubSources;
  }

  @Override
  public String toString() {
    return "#emissionSubSources=" + (emissionSubSources == null ? "null" : emissionSubSources.size());
  }
}
