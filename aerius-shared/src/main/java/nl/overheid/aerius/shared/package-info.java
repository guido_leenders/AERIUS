/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

/**
 * The shared package is intended for objects and classes that are intended to
 * be used in all projects including the wui-client project. To be compatible 
 * with the wui-client project all code in this package must be able to be
 * compiled with GWT. This means there are some restrictions on the classes that
 * can be used and also means domain classes that can be used for communications
 * between client and server must adhere to the GWT specific requirements.
 */
package nl.overheid.aerius.shared;
