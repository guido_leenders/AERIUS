/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.scenario.connect;

import java.io.Serializable;
import java.util.ArrayList;

public class ConnectUtilInformation implements Serializable {

  private static final long serialVersionUID = -4761156576893953126L;

  public enum UtilType {
    VALIDATE,
    CONVERT,
    DELTA_VALUE,
    HIGHEST_VALUE,
    TOTAL_VALUE;
  }

  private UtilType utilType;

  private boolean validateAsPriorityProject;
  private boolean onlyIncreasement;

  private ArrayList<ConnectCalculationFile> files = new ArrayList<>();

  public UtilType getUtilType() {
    return utilType;
  }

  public void setUtilType(final UtilType utilType) {
    this.utilType = utilType;
  }

  public void add(final ConnectCalculationFile file) {
    files.add(file);
  }

  public ArrayList<ConnectCalculationFile> getFiles() {
    return files;
  }

  public void setOnlyIncreasement(final boolean onlyIncreasement) {
    this.onlyIncreasement = onlyIncreasement;
  }

  public boolean isOnlyIncreasement() {
    return onlyIncreasement;
  }

  public boolean isValidateAsPriorityProject() {
    return validateAsPriorityProject;
  }

  public void setValidateAsPriorityProject(final boolean validateAsPriorityProject) {
    this.validateAsPriorityProject = validateAsPriorityProject;
  }

  @Override
  public String toString() {
    return "ConnectUtilInformation [utilType=" + utilType + ", onlyIncreasement=" + onlyIncreasement
        + ", validateAsPriorityProject=" + validateAsPriorityProject + ", files=" + files + "]";
  }

}
