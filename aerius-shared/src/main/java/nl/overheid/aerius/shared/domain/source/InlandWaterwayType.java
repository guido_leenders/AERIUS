/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.Objects;

import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;

/**
 * Data object to store the waterway category and the direction of the type.
 */
public class InlandWaterwayType implements HasState, Serializable  {

  private static final long serialVersionUID = 6600689099002811953L;

  private InlandWaterwayCategory waterwayCategory;
  private WaterwayDirection waterwayDirection;

  public InlandWaterwayType() {
    // Empty constructor, initialize via methods.
  }

  public InlandWaterwayType(final InlandWaterwayCategory category, final WaterwayDirection directon) {
    waterwayCategory = category;
    waterwayDirection = directon;
  }

  public InlandWaterwayType copy() {
    return new InlandWaterwayType(waterwayCategory, waterwayDirection);
  }

  public InlandWaterwayCategory getWaterwayCategory() {
    return waterwayCategory;
  }

  public void setWaterwayCategory(final InlandWaterwayCategory waterwayCategory) {
    this.waterwayCategory = waterwayCategory;
  }

  public WaterwayDirection getWaterwayDirection() {
    return waterwayDirection;
  }

  public void setWaterwayDirection(final WaterwayDirection waterwayDirection) {
    this.waterwayDirection = waterwayDirection == null ? WaterwayDirection.IRRELEVANT : waterwayDirection;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    return Objects.equals(waterwayCategory, ((InlandWaterwayType) obj).getWaterwayCategory())
        && Objects.equals(waterwayDirection, ((InlandWaterwayType) obj).getWaterwayDirection());
  }

  @Override
  public int hashCode() {
    return Objects.hash(waterwayCategory, waterwayDirection);
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((waterwayCategory == null) ? 0 : waterwayCategory.getStateHash());
    return prime * result + ((waterwayDirection == null) ? 0 : waterwayDirection.ordinal());
  }

  @Override
  public String toString() {
    return "InlandWaterwayType [waterwayCategory=" + waterwayCategory + ", waterwayDirection=" + waterwayDirection + "]";
  }

}
