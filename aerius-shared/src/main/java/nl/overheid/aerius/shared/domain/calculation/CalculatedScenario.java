/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * Abstract scenario to store {@link EmissionSourceList} and calculation id's.
 * Specific implementations can be created for specific relations.
 */
public abstract class CalculatedScenario implements Serializable {
  private static final long serialVersionUID = 5548856192199167087L;

  private transient HashMap<Integer, Integer> stateHashes = new HashMap<>();
  private Integer calculationPointSetId;
  /**
   * Name of the ReceptorSet from the database that was used to provide the custom points.
   */
  private String receptorSetName = null;
  private ArrayList<Calculation> calculationList = new ArrayList<Calculation>();
  private Scenario scenario = new Scenario();

  /**
   * Returns the calculation id for the id of the emission source list. If the
   * calculation id is null or no match could be found null is returned.
   *
   * @param eslId The id of the emission source list to find the calculation id for
   * @return the calculation id or 0
   */
  public abstract int getCalculationId(final int eslId);

  /**
   * Check to see if a certain calculation ID is present for this calculated scenario.
   *
   * @param calculationId The id of the calculation ID to check for.
   * @return true if the calculation ID exists
   */
  public abstract boolean containsCalculationId(final int calculationId);

  public Scenario getScenario() {
    return scenario;
  }

  public Integer getCalculationPointSetId() {
    return calculationPointSetId;
  }

  public void setCalculationPointSetId(final Integer calculationPointSetId) {
    this.calculationPointSetId = calculationPointSetId;
  }

  /**
   * @return the receptorSetName
   */
  public String getReceptorSetName() {
    return receptorSetName;
  }

  /**
   * @param receptorSetName the receptorSetName to set
   */
  public void setReceptorSetName(final String receptorSetName) {
    this.receptorSetName = receptorSetName;
  }

  /**
   * Save the current hash state of the {@link EmissionSourceList}'s and calculation id's.
   */
  public void certify() {
    stateHashes = calculateStateHash();
  }

  /**
   * Return all {@link Calculation}'s.
   *
   * @return A listcontaining {@link Calculation}'s
   */
  public ArrayList<Calculation> getCalculations() {
    return calculationList;
  }

  /**
   * Returns the Calculation for the given calculationId.
   * @param calculatorId id to get
   * @return Calculation set or null if not present
   */
  public abstract Calculation getCalculation(final int calculatorId);

  /**
   * @param calculatorId calculation id to get source lists from
   * @return The sources used for the calculation.
   */
  public abstract EmissionSourceList getSources(final int calculatorId);

  public CalculationPointList getCalculationPoints() {
    return scenario.getCalculationPoints();
  }

  /**
   * @param calculatorId calculation id to get (user defined) calculation point lists from.
   * @return The AeriusResultPoints used for the calculation.
   */
  public abstract ArrayList<AeriusResultPoint> getCalculationPointResultsList(final int calculatorId);

  /**
   * @param calculationId calculation id to get calculation results for.
   * @return All result points for the calculation.
   */
  public ArrayList<AeriusResultPoint> getResultPoints(final int calculationId) {
    return getCalculation(calculationId) == null ? new ArrayList<AeriusResultPoint>()
        : scenario.getResultPoints(calculationList.indexOf(getCalculation(calculationId)));
  }

  public CalculationSetOptions getOptions() {
    return calculationList.isEmpty() ? null : calculationList.get(0).getOptions();
  }

  /**
   * Set the calculation options.
   * @param options options
   */
  public void setOptions(final CalculationSetOptions options) {
    for (final Calculation calculation : calculationList) {
      calculation.setOptions(options);
    }
  }

  /**
   * Sets the same calculation year for all calculations.
   * @param year year
   */
  public void setYear(final int year) {
    for (final Calculation calculation : getCalculations()) {
      calculation.setYear(year);
    }
  }

  /**
   * Returns true if the hash state saved with {@link #certify()} is still in
   * sync with the current state of the objects.
   *
   * @return True if the scenario is in sync. False if it is not.
   */
  public boolean isInSync() {
    boolean inSync = true;
    final HashMap<Integer, Integer> calculateStateHash = calculateStateHash();
    if (calculateStateHash().size() != stateHashes.size()) {
      inSync = false;
    } else {
      for (final Entry<Integer, Integer> entry : calculateStateHash.entrySet()) {
        if (entry.getValue().intValue() != stateHashes.get(entry.getKey()).intValue()) {
          inSync = false;
          break;
        }
      }
    }
    return inSync;
  }

  /**
   * Calculates the state hash.
   *
   * @return the calculated state hash.
   */
  private HashMap<Integer, Integer> calculateStateHash() {
    final HashMap<Integer, Integer> sh = new HashMap<>();
    for (final EmissionSourceList esl : getScenario().getSourceLists()) {
      sh.put(esl.getId(), esl.getStateHash());
    }
    return sh;
  }

  /**
   * Check to see if there are any results available.
   *
   * @return True if the scenario has calculation results, false if it does not.
   */
  public abstract boolean hasCalculations();

  @Override
  public String toString() {
    return "CalculatedScenario [calculationPointSetId=" + calculationPointSetId + ", calculationList=" + calculationList + ", scenario=" + scenario
        + "]";
  }

  public ScenarioMetaData getMetaData() {
    return scenario.getMetaData();
  }

  public void setMetaData(final ScenarioMetaData metaData) {
    scenario.setMetaData(metaData);
  }

  public boolean hasResultPoints() {
    return scenario.hasAnyResultPoints();
  }
}
