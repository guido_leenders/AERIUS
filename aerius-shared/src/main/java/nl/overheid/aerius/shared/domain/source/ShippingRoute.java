/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;

/**
 * Data class for ship routes. Maintains a list of which ship HasIds use this route.
 */
public class ShippingRoute extends Point implements HasShippingRoute, HasState, Serializable {

  public interface ShippingRouteReference extends HasId {

  }

  private static final long serialVersionUID = 2405909583112531725L;

  private int id;
  private WKTGeometry geometry;
  private Point endPoint;
  private ArrayList<ShippingRouteReference> references = new ArrayList<>();
  private InlandWaterwayType inlandWaterwayType = new InlandWaterwayType();

  /**
   * @param ref
   */
  public void attachRoute(final ShippingRouteReference ref) {
    if (!references.contains(ref)) {
      references.add(ref);
    }
  }

  /**
   * Copies the route, but doesn't copy the vessels using this route, because
   * they must copied after the vessels them self are copied..
   * @return new instance
   */
  public ShippingRoute copy() {
    final ShippingRoute copy = new ShippingRoute();
    copy.id = id;
    copy.geometry = geometry;
    copy.setX(getX());
    copy.setY(getY());
    copy.endPoint = endPoint == null ? null : new Point(endPoint.getX(), endPoint.getY());
    copy.inlandWaterwayType = inlandWaterwayType.copy();
    return copy;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public WKTGeometry getGeometry() {
    return geometry;
  }

  public Point getEndPoint() {
    return endPoint;
  }

  public ArrayList<ShippingRouteReference> getReferences() {
    return references;
  }

  @Override
  public InlandWaterwayCategory getWaterwayCategory() {
    return inlandWaterwayType.getWaterwayCategory();
  }

  @Override
  public WaterwayDirection getWaterwayDirection() {
    return inlandWaterwayType.getWaterwayDirection();
  }

  @Override
  public InlandWaterwayType getInlandWaterwayType() {
    return inlandWaterwayType;
  }

  @Override
  public void setWaterwayCategory(final InlandWaterwayCategory waterwayCategory) {
    inlandWaterwayType.setWaterwayCategory(waterwayCategory);
  }

  @Override
  public void setWaterwayDirection(final WaterwayDirection waterwayDirection) {
    inlandWaterwayType.setWaterwayDirection(waterwayDirection);
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + ((geometry == null) ? 0 : geometry.getStateHash());
    result = (prime * result) + ((inlandWaterwayType == null) ? 0 : inlandWaterwayType.getStateHash());
    return result;
  }

  /**
   * Returns true if any vesselGroup is using this route.
   * @return true if route is used
   */
  public boolean isUsed() {
    return !references.isEmpty();
  }

  /**
   * Determines if the route is only used by this ship.
   * @param ref
   * @return True if no other ships are using this route.
   */
  public boolean isOnlyUsedBy(final HasId ref) {
    return (ref != null) && (references.size() == 1) && (references.get(0).hashCode() == ref.hashCode());
  }

  public void detachRoute(final HasId ref) {
    references.remove(ref);
  }

  @Override
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public void setGeometry(final WKTGeometry geometry) {
    this.geometry = geometry;
  }

  public void setEndPoint(final Point endPoint) {
    this.endPoint = endPoint;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    return id == ((ShippingRoute) obj).getId() && super.equals(obj);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, geometry, inlandWaterwayType) + 31 * super.hashCode();
  }

  @Override
  public String toString() {
    return "ShippingRoute [id=" + id + ", endPoint=" + endPoint + ", geometry=" + geometry + ", midPoint="
        + super.toString() + ", inlandWaterwayType=" + inlandWaterwayType + ", ships=" + references.size() + "]";
  }
}
