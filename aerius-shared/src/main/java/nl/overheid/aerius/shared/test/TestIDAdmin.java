/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.test;

/**
 * TestID class for product Admin.
 */
public final class TestIDAdmin extends TestID {
  // Menu Admin
  public static final String MENU_USER_MANAGEMENT = "menu_user_management";
  public static final String MENU_ROLE_MANAGEMENT = "menu_role_management";
  public static final String USER_MANAGEMENT_NEW_USER = "admin_new_user";

  // User Properties Filter
  public static final String USER_MANAGEMENT_FILTER_ORGANISATION = "admin_filter_organisation";
  public static final String USER_MANAGEMENT_FILTER_ROLES = "admin_filter_roles";
  public static final String USER_MANAGEMENT_FILTER_USERNAME = "admin_filter_username";
  public static final String USER_MANAGEMENT_FILTER_ENABLED = "admin_filter_enabled";

  // User Overview
  public static final String USER_MANAGEMENT_OVERVIEW = "admin_overview";
  public static final String USER_MANAGEMENT_OVERVIEW_ROLES = "admin_overview_roles";
  public static final String USER_MANAGEMENT_OVERVIEW_DUPLICATE = "admin_overview_duplicate";

  // User Edit Form
  public static final String USER_MANAGEMENT_EDIT_DELETE = "admin_edit_delete";
  public static final String USER_MANAGEMENT_EDIT_CANCEL = "admin_edit_cancel";
  public static final String USER_MANAGEMENT_EDIT_SUBMIT = "admin_edit_submit";
  public static final String USER_MANAGEMENT_EDIT_NAME = "admin_edit_name";
  public static final String USER_MANAGEMENT_EDIT_INITIALS = "admin_edit_initials";
  public static final String USER_MANAGEMENT_EDIT_FIRSTNAME = "admin_edit_firstname";
  public static final String USER_MANAGEMENT_EDIT_LASTNAME = "admin_edit_lastname";
  public static final String USER_MANAGEMENT_EDIT_ORGANISATION = "admin_edit_organisation";
  public static final String USER_MANAGEMENT_EDIT_EMAIL = "admin_edit_email";
  public static final String USER_MANAGEMENT_EDIT_ROLES = "admin_edit_roles";
  public static final String USER_MANAGEMENT_EDIT_ENABLED = "admin_edit_enabled";

  public static final String USER_MANAGEMENT_EDIT_EDITOR = "admin_user_edit";
  public static final String USER_MANAGEMENT_ADD_EDITOR = "admin_user_add";

  public static final String USER_MANAGEMENT_EDIT = "admin_edit";

  private TestIDAdmin() {
    // Util class constructor
  }
}
