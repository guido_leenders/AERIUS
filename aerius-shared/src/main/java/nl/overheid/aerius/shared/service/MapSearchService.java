/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.SearchSuggestion;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Service containing search related calls.
 */
@RemoteServiceRelativePath("map-aerius-search")
public interface MapSearchService extends RemoteService {

  /**
   * Retrieve a list of {@link SearchSuggestion}'s for the given searchString.
   * @param searchString The string to search with.
   * @return List of SearchSuggestions, never null.
   * @throws AeriusException
   */
  ArrayList<MapSearchSuggestion> getSuggestions(String searchString) throws AeriusException;

  /**
   * Retrieve a list of {@link SearchSuggestion}'s for the given parent searchresult.
   * In practice these are nature areas within a certain distance of the parent town, municipality, etc.
   * The result is retrieved using a spatial query on the database, looking for nature areas nearby
   * parentSuggestion.getPoint().
   * @param parentSuggestion The "point" search result which is being expanded.
   * @return List of SearchSuggestions, never null.
   * @throws AeriusException
   */
  ArrayList<MapSearchSuggestion> getSubSuggestions(MapSearchSuggestion parentSuggestion) throws AeriusException;

}
