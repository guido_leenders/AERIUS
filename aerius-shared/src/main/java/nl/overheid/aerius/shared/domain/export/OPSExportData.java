/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;

/**
 * Data class to combine all data needed for an OPS export.
 */
public class OPSExportData extends ExportData {

  private static final long serialVersionUID = -1275394402368997903L;

  private CalculatedScenario scenario;

  public CalculatedScenario getScenario() {
    return scenario;
  }

  public void setScenario(final CalculatedScenario scenario) {
    this.scenario = scenario;
  }

  @Override
  public String toString() {
    return "OPSExportInputData [scenario=" + scenario + ", emailTo=" + getEmailAddress()
        + ", creationDate=" + getCreationDate() + "]";
  }

}
