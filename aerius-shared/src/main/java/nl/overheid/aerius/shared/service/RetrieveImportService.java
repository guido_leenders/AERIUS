/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

@RemoteServiceRelativePath(ServiceURLConstants.RETRIEVE_IMPORT_SERVICE_PATH)
public interface RetrieveImportService extends RemoteService {

  /**
   * Returns the result of an import that is stored in the user's session.
   *
   * @throws AeriusException
   */
  ImportResult getImportResult(String key) throws AeriusException;

  /**
   * Returns the calculations belonging to this import.
   *
   * @throws AeriusException
   */
  ArrayList<Calculation> getCalculations(String key) throws AeriusException;
}
