/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Data object for maritime ships on a maritime or inland route.
 * That's right, martime ships on inland routes. Inland ships have their own emissionvalues (see {@link InlandRouteEmissionSource}).
 */
public class MaritimeRouteEmissionSource extends ShippingEmissionSource<RouteMaritimeVesselGroup> implements Serializable {

  private static final long serialVersionUID = -6600223113778823202L;

  /**
   * Emissions for a category of vessels on a route.
   */
  public static class RouteMaritimeVesselGroup extends VesselGroupEmissionSubSource<MaritimeShippingCategory> {

    private static final long serialVersionUID = 4620235016380042545L;

    /**
     * Number of ship movements per time unit.
     */
    private int shipMovementsPerTimeUnit;
    private TimeUnit timeUnit = TimeUnit.YEAR;

    private ShippingMovementType movementType;

    protected RouteMaritimeVesselGroup() {
      // Needed for GWT.
    }

    /**
     * @param movementType
     */
    public RouteMaritimeVesselGroup(final ShippingMovementType movementType) {
      this.movementType = movementType;
    }

    @Override
    public RouteMaritimeVesselGroup copy() {
      return copyTo(new RouteMaritimeVesselGroup(movementType));
    }

    protected <V extends RouteMaritimeVesselGroup> V copyTo(final V copy) {
      super.copyTo(copy);
      copy.setShipMovementsPerTimeUnit(shipMovementsPerTimeUnit);
      copy.setTimeUnit(timeUnit);
      return copy;
    }

    @Min(0)
    public int getShipMovementsPerTimeUnit() {
      return shipMovementsPerTimeUnit;
    }

    public ShippingMovementType getMovementType() {
      return movementType;
    }

    @NotNull
    public TimeUnit getTimeUnit() {
      return timeUnit;
    }

    public int getShipMovementsPerYear() {
      return timeUnit.getPerYear(shipMovementsPerTimeUnit);
    }

    public void setShipMovements(final int numberOfShipsPerTimeUnit, final TimeUnit timeUnit) {
      setShipMovementsPerTimeUnit(numberOfShipsPerTimeUnit);
      setTimeUnit(timeUnit);
    }

    public void setShipMovementsPerTimeUnit(final int numberOfShipsPerTimeUnit) {
      this.shipMovementsPerTimeUnit = numberOfShipsPerTimeUnit;
    }

    public void setTimeUnit(final TimeUnit timeUnit) {
      this.timeUnit = timeUnit;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = prime * result + super.getStateHash();
      result = prime * result + ((movementType == null) ? 0 : movementType.ordinal() + 1);
      result = prime * result + shipMovementsPerTimeUnit;
      result = prime * result + timeUnit.ordinal();
      return result;
    }

    @Override
    public String toString() {
      return "VesselGroupEmissionValues [" + super.toString() + ", shipMovementsPerTimeUnit="
          + shipMovementsPerTimeUnit + ", timeUnit=" + timeUnit + "]";
    }
  }

  private ShippingMovementType movementType = ShippingMovementType.INLAND;

  public MaritimeRouteEmissionSource() {
    // Needed for GWT.
  }

  /**
   * @param movementType
   */
  public MaritimeRouteEmissionSource(final ShippingMovementType movementType) {
    this.movementType = movementType;
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public MaritimeRouteEmissionSource copy() {
    return copyTo(new MaritimeRouteEmissionSource());
  }

  public <E extends MaritimeRouteEmissionSource> E copyTo(final E copy) {
    super.copyTo(copy);
    copy.setMovementType(movementType);
    return copy;
  }

  public ShippingMovementType getMovementType() {
    return movementType;
  }

  public void setMovementType(final ShippingMovementType movementType) {
    this.movementType = movementType;
  }

  @Override
  public String toString() {
    return "MaritimeRouteEmissionValues [" + super.toString() + "]";
  }
}
