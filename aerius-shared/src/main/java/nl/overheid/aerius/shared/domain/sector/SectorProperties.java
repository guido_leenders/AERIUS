/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.sector;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.CalculationEngine;

/**
 * Class for properties of a sector which are used by the application to give
 * a representation of the sector and guide the application. For representation
 * a color and an image property can be set. Where image is not the actual image,
 * but an enum that indicates an image is available. The application
 * implementation should make the mapping between the enum and the actual image.
 * EmissionCalculationMethod is an indicator for the application to decide which type
 * of emission editor to use.
 */
public class SectorProperties implements Serializable {

  private static final long serialVersionUID = -6645359153670850396L;

  private String color;
  private SectorIcon icon;
  private EmissionCalculationMethod method;
  private CalculationEngine calculationEngine;
  private boolean buildingPossible;

  // Needed for GWT. 
  public SectorProperties() {
  }

  public SectorProperties(final String color, final String icon) {
    this(color, icon, null, null, false);
  }

  public SectorProperties(final String color, final String icon, final String method,
      final String calculationEngine, final boolean buildingPossible) {
    this(color, SectorIcon.safeValueOf(icon), EmissionCalculationMethod.safeValueOf(method),
        CalculationEngine.safeValueOf(calculationEngine), buildingPossible);
  }

  public SectorProperties(final String color, final SectorIcon icon, final EmissionCalculationMethod method,
      final CalculationEngine calculationEngine, final boolean buildingPossible) {
    this.color = color;
    this.icon = icon;
    this.method = method;
    this.calculationEngine = calculationEngine;
    this.buildingPossible = buildingPossible;
  }

  public CalculationEngine getCalculationEngine() {
    return calculationEngine;
  }

  public String getColor() {
    return color;
  }

  public SectorIcon getIcon() {
    return icon;
  }

  public EmissionCalculationMethod getMethod() {
    return method;
  }

  public boolean isBuildingPossible() {
    return buildingPossible;
  }

  public void setBuildingPossible(final boolean buildingPossible) {
    this.buildingPossible = buildingPossible;
  }

  public void setCalculationEngine(final CalculationEngine calculationEngine) {
    this.calculationEngine = calculationEngine;
  }

  public void setColor(final String color) {
    this.color = color;
  }

  public void setIcon(final SectorIcon icon) {
    this.icon = icon;
  }

  public void setMethod(final EmissionCalculationMethod method) {
    this.method = method;
  }

  @Override
  public String toString() {
    return "SectorProperties [color=" + color + ", icon=" + icon + ", method=" + method + ", calculationEngine="
        + calculationEngine + ", buildingPossible=" + buildingPossible + "]";
  }
}
