/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.shared.domain.calculation;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

/**
 * Contains the partial results of a calculation.
 */
public class PartialCalculationResult implements Serializable {

  private static final long serialVersionUID = 7273922309367795089L;

  // Id of the calculation this result belongs to.
  private int calculationId;
  // Id to uniquely identify this result and link it to the input.
  private String resultId;

  private ArrayList<AeriusResultPoint> results = new ArrayList<AeriusResultPoint>();

  /**
   * Add a result to the list. If the point (same id/x-y) already would be present it will still be added.
   * @param point point to add
   */
  public void add(final AeriusResultPoint point) {
    results.add(point);
  }

  public boolean isEmpty() {
    return results.isEmpty();
  }

  public ArrayList<AeriusResultPoint> getResults() {
    return results;
  }

  public int getCalculationId() {
    return calculationId;
  }

  public void setCalculationId(final int calculationId) {
    this.calculationId = calculationId;
  }

  public String getResultId() {
    return resultId;
  }

  public void setResultId(final String resultId) {
    this.resultId = resultId;
  }
}
