/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Classes operating on the emission source types can implement this interface to create a visitor pattern implementation.
 * @param <T> visitor specific data type
 */
public interface EmissionSourceVisitor<T> {

  T visit(FarmEmissionSource emissionSource) throws AeriusException;
  T visit(GenericEmissionSource emissionSource) throws AeriusException;
  T visit(InlandMooringEmissionSource emissionSource) throws AeriusException;
  T visit(InlandRouteEmissionSource emissionSource) throws AeriusException;
  T visit(MaritimeMooringEmissionSource emissionSource) throws AeriusException;
  T visit(MaritimeRouteEmissionSource emissionSource) throws AeriusException;
  T visit(OffRoadMobileEmissionSource emissionSource) throws AeriusException;
  T visit(PlanEmissionSource emissionSource) throws AeriusException;
  T visit(SRM2EmissionSource emissionSource) throws AeriusException;
  T visit(SRM2NetworkEmissionSource emissionSource) throws AeriusException;

}
