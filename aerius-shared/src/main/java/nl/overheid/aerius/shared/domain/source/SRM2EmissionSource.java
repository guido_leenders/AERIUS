/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasState;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.exception.AeriusException;

// TODO; if HasOPSSourceCharacteristics gets removed (which is planned), also do not forget to remove the check in SourceDetailPresenter#nextStep().
public class SRM2EmissionSource extends EmissionSourceCollection<VehicleEmissions> implements HasOPSSourceCharacteristics {

  private static final long serialVersionUID = -4183056223011387432L;

  // Urban road types are number below 5 -> 0 to 4.
  private static final int URBAN_ROAD_LIMIT = 5;

  /**
   * A barrier on a side of a road.
   */
  public static class RoadSideBarrier implements Serializable, HasState {

    private static final long serialVersionUID = 372333395740221205L;

    /**
     * The type of a barrier on the side of a road.
     */
    public enum RoadSideBarrierType {

      /**
       * The barrier is a screen (example: 'Geluidsscherm').
       */
      SCREEN(0.5),

      /**
       * The barrier is a wall (example: 'Geluidswal').
       */
      WALL(0.25);

      private double heightFactor;

      RoadSideBarrierType(final double heightFactor) {
        this.heightFactor = heightFactor;
      }

      public double getHeightFactor() {
        return this.heightFactor;
      }
    }

    /**
     * Type of the barrier.
     */
    private RoadSideBarrierType barrierType;
    /**
     * Height of the barrier.
     */
    private double height;
    /**
     * Distance from road to barrier.
     */
    private double distance;

    public RoadSideBarrierType getBarrierType() {
      return barrierType;
    }

    public void setBarrierType(final RoadSideBarrierType barrierType) {
      this.barrierType = barrierType;
    }

    public double getHeight() {
      return height;
    }

    public void setHeight(final double height) {
      this.height = height;
    }

    public double getDistance() {
      return distance;
    }

    public void setDistance(final double distance) {
      this.distance = distance;
    }

    /**
     * @return A copy of this object.
     */
    public RoadSideBarrier copy() {
      final RoadSideBarrier copy = new RoadSideBarrier();

      copy.barrierType = this.barrierType;
      copy.height = this.height;
      copy.distance = this.distance;

      return copy;
    }

    @Override
    public int getStateHash() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((barrierType == null) ? 0 : barrierType.ordinal());
      result = prime * result + (int) height;
      result = prime * result + (int) distance;
      return result;
    }

    @Override
    public String toString() {
      return "RoadSideBarrier [" + barrierType + ":" + height + ", " + distance + "]";
    }
  }

  /**
   * The elevation type of a road.
   */
  public enum RoadElevation {

    /**
     * The road has no elevation (or is elevated with angles < 20 degrees).
     */
    NORMAL(0.0),

    /**
     * The road lies on a normal wall or dyke (angles > 20 degrees and < 45 degrees).
     */
    NORMAL_DYKE(0.25),

    /**
     * The road lies on a steep wall or dyke (angles > 45 degrees).
     */
    STEEP_DYKE(0.5),

    /**
     * The road lies on a viaduct.
     */
    VIADUCT(1.0),

    /**
     * The road lies in a 'tunnelbak' (the part of the road just before/after a tunnel).
     */
    TUNNEL(0.5);

    private double heightFactor;

    RoadElevation(final double heightFactor) {
      this.heightFactor = heightFactor;
    }

    public double getHeightFactor() {
      return this.heightFactor;
    }
  }

  private int segmentId;
  private RoadElevation elevation = RoadElevation.NORMAL;
  private int elevationHeight;
  private boolean freeway;

  /**
   * Default tunnelfactor = 1.0 (normal road).
   * 0 would mean it is actually a tunnel (no emission).
   * Any other value would mean the road(section) is connected to a tunnel.
   * Tunnel should be > 100m long however to get a tunnelfactor.
   */
  private double tunnelFactor = 1.0;

  private RoadSideBarrier barrierLeft;
  private RoadSideBarrier barrierRight;

  private ArrayList<SRM2EmissionSourceLinearReference> dynamicSegments = new ArrayList<>();
  private int roadType;

  public SRM2EmissionSource() {
    setYearDependent(true);
    setEmissionPerUnit(true);
  }

  @Override
  public <T> T accept(final EmissionSourceVisitor<T> visitor) throws AeriusException {
    return visitor.visit(this);
  }

  @Override
  public SRM2EmissionSource copy() {
    return copyTo(new SRM2EmissionSource());
  }

  public <E extends SRM2EmissionSource> E copyTo(final E copy) {
    super.copyTo(copy);
    copy.setSegmentId(segmentId);
    copy.setElevation(elevation);
    copy.setElevationHeight(elevationHeight);
    copy.setFreeway(freeway);
    copy.setTunnelFactor(tunnelFactor);
    copy.setBarrierLeft(barrierLeft == null ? null : barrierLeft.copy());
    copy.setBarrierRight(barrierRight == null ? null : barrierRight.copy());

    final ArrayList<SRM2EmissionSourceLinearReference> dynamicSegmentsCopy = new ArrayList<>();

    for (final SRM2EmissionSourceLinearReference linearReference : dynamicSegments) {
      dynamicSegmentsCopy.add(linearReference.copyTo(new SRM2EmissionSourceLinearReference()));
    }

    copy.setDynamicSegments(dynamicSegmentsCopy);
    return copy;
  }

  public int getSegmentId() {
    return segmentId;
  }

  public void setSegmentId(final int segmentId) {
    this.segmentId = segmentId;
  }

  public boolean isUrbanRoad() {
    return roadType < URBAN_ROAD_LIMIT;
  }

  public void setRoadType(final int roadType) {
    this.roadType = roadType;
  }

  public RoadElevation getElevation() {
    return elevation;
  }

  public void setElevation(final RoadElevation elevation) {
    this.elevation = elevation;
  }

  public int getElevationHeight() {
    return elevationHeight;
  }

  public void setElevationHeight(final int elevationHeight) {
    this.elevationHeight = elevationHeight;
  }

  public boolean isFreeway() {
    return freeway;
  }

  public void setFreeway(final boolean freeway) {
    this.freeway = freeway;
  }

  public double getTunnelFactor() {
    return tunnelFactor;
  }

  public void setTunnelFactor(final double tunnelFactor) {
    this.tunnelFactor = tunnelFactor;
  }

  public RoadSideBarrier getBarrierLeft() {
    return barrierLeft;
  }

  public void setBarrierLeft(final RoadSideBarrier barrierLeft) {
    this.barrierLeft = barrierLeft;
  }

  public RoadSideBarrier getBarrierRight() {
    return barrierRight;
  }

  public void setBarrierRight(final RoadSideBarrier barrierRight) {
    this.barrierRight = barrierRight;
  }

  public ArrayList<SRM2EmissionSourceLinearReference> getDynamicSegments() {
    return dynamicSegments;
  }

  public void setDynamicSegments(final ArrayList<SRM2EmissionSourceLinearReference> dynamicSegments) {
    this.dynamicSegments = dynamicSegments;
  }

  @Override
  public int getStateHash() {
    final int prime = 31;
    int result = super.getStateHash();
    result = prime * result + ((elevation == null) ? 0 : elevation.ordinal());
    result = prime * result + elevationHeight;
    result = prime * result + (freeway ? 0 : 1);
    result = prime * result + ((barrierLeft == null) ? 0 : barrierLeft.getStateHash());
    result = prime * result + ((barrierRight == null) ? 0 : barrierRight.getStateHash());

    for (final SRM2EmissionSourceLinearReference linearReference : dynamicSegments) {
      result = prime * result + linearReference.getStateHash();
    }

    return result;
  }

  @Override
  public double getEmission(final EmissionValueKey key, final double weight) {
    final double result = super.getEmission(key, weight) * tunnelFactor;
    return result < 0 ? 0 : result;
  }
}
