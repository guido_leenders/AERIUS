/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.source;

import java.util.List;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;


/**
 * Util class to check limits.
 */
public final class EmissionSourceCheckLimits {

  private EmissionSourceCheckLimits() {
    // Util class.
  }

  /**
   * Checks if the given emission source list conforms to the limits. If a limit
   * is exceeded an ServerException is thrown.
   *
   * @param emissionSourceList the emission source list to check
   * @param limits the limits to check against.
   * @throws AeriusException throws exception if a limit is exceeded
   */
  public static void check(final EmissionSourceList emissionSourceList, final CalculatorLimits limits) throws AeriusException {
    final int numberOfSources = countNumberOfSources(emissionSourceList);
    if (!limits.isWithinMaxSourcesLimit(numberOfSources)) {
      throw new AeriusException(Reason.LIMIT_SOURCES_EXCEEDED, String.valueOf(limits.getMaxSources()), String.valueOf(numberOfSources));
    }
    for (final EmissionSource emissionSource : emissionSourceList) {
      final WKTGeometry geo = emissionSource.getGeometry();
      if (geo != null) {
        switch (geo.getType()) {
        case LINE:
          if (!limits.isWithinLineLengthLimit(geo.getMeasure())) {
            throw new AeriusException(Reason.LIMIT_LINE_LENGTH_EXCEEDED, emissionSource.getLabel(),
                String.valueOf(limits.getMaxLineLength()), String.valueOf(geo.getMeasure()));
          }
          break;
        case POLYGON:
          final int surfaceHa = MathUtil.round(geo.getMeasure() / SharedConstants.M2_TO_HA);
          if (!limits.isWithinPolygonSurfaceLimit(surfaceHa)) {
            throw new AeriusException(Reason.LIMIT_POLYGON_SURFACE_EXCEEDED, emissionSource.getLabel(),
                String.valueOf(limits.getMaxPolygonSurface()), String.valueOf(surfaceHa));
          }
          break;
        default:
          // no checks for other geometries.
        }
      }
    }
  }

  /**
   * Counts the number of sources in the list. If the source object is an composite source the subsources are counted also.
   * @param sources sources to count
   * @return number of sources
   */
  private static int countNumberOfSources(final List<EmissionSource> sources) {
    int cnt = 0;
    for (final EmissionSource es : sources) {
      if (es instanceof SRM2NetworkEmissionSource) {
        cnt += ((SRM2NetworkEmissionSource) es).getEmissionSources().size();
      } else {
        cnt++;
      }
    }
    return cnt;
  }
}
