/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.info;

import java.util.HashMap;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;

/**
 *
 */
public class DepositionMarker extends Point implements HasId {

  private static final long serialVersionUID = -8217690360268746277L;

  /**
   * Enum for a single marker type. Uses bit operations to determine combinations.
   */
  public enum DepositionMarkerType {
    /**
     * Highest calculated deposition.
     */
    HIGHEST_CALCULATED_DEPOSITION(1),

    /**
     * <p>Highest <i>total</i> deposition <i>if</i> exceeding.</p>
     *
     * <p>select receptor<br>
     * where (calculated deposition + background) > KDW</p>
     */
    HIGHEST_TOTAL_DEPOSITION(2),

    /**
     * Combination of HIGHEST_CALCULATED_DEPOSITION and HIGHEST_TOTAL_DEPOSITION.
     */
    HIGHEST_CALCULATED_AND_TOTAL(3);

    private int bit;

    private DepositionMarkerType(final int bit) {
      this.bit = bit;
    }

    int getBit() {
      return bit;
    }

    DepositionMarkerType determineByBit(final int bit) {
      DepositionMarkerType returnType = null;
      for (final DepositionMarkerType type : DepositionMarkerType.values()) {
        if (type.getBit() == bit) {
          returnType = type;
        }
      }
      return returnType;
    }

    /**
     * @param type The markertype to combine with this.
     * @return The markertype when combined. When combining already combined types, the 'highest' type will be returned.
     */
    public DepositionMarkerType combine(final DepositionMarkerType type) {
      return determineByBit(bit | (type == null ? 0 : type.getBit()));
    }
  }

  private int receptorId;
  private int assessmentAreaId;
  private DepositionMarkerType markerType;
  private HashMap<DepositionMarkerType, Double> markerValues = new HashMap<DepositionMarkerType, Double>();

  public int getReceptorId() {
    return receptorId;
  }

  public void setReceptorId(final int receptorId) {
    this.receptorId = receptorId;
  }

  public int getAssessmentAreaId() {
    return assessmentAreaId;
  }

  public void setAssessmentAreaId(final int assessmentAreaId) {
    this.assessmentAreaId = assessmentAreaId;
  }

  public DepositionMarkerType getMarkerType() {
    return markerType;
  }

  public void setMarkerType(final DepositionMarkerType markerType) {
    this.markerType = markerType;
  }

  public HashMap<DepositionMarkerType, Double> getMarkerValues() {
    return markerValues;
  }

  /**
   * @param markerType The marker type to combine with this marker.
   * If current markertype is null, the markertype will be set to the supplied markertype.
   */
  public void combine(final DepositionMarkerType markerType, final double value) {
    this.markerType = this.markerType == null ? markerType : this.markerType.combine(markerType);
    markerValues.put(markerType, value);
  }

  @Override
  public int getId() {
    return receptorId;
  }

  @Override
  public void setId(final int id) {
    this.receptorId = id;
  }

  /**
   * Get a copy of this marker for just the specified type (only deposition value for that type will be copied).
   * @param type The type to get the copy for.
   * @return A copy of this marker with the deposition values.
   */
  public DepositionMarker copyTypeOnly(final DepositionMarkerType type) {
    final DepositionMarker copy = new DepositionMarker();
    copy.setAssessmentAreaId(assessmentAreaId);
    copy.setReceptorId(receptorId);
    copy.setMarkerType(type);
    copy.setX(getX());
    copy.setY(getY());
    if (markerValues.get(type) != null) {
      copy.getMarkerValues().put(type, markerValues.get(type));
    }
    return copy;
  }
}