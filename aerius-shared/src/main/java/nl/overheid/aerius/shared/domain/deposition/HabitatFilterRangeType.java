/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.deposition;

/**
 * There are a few options for habitat filters. This enum contains those options.
 */
public enum HabitatFilterRangeType {

  /**
   * ranges based on total deposition on receptors.
   */
  TOTAL_DEPOSITION,
  /**
   * ranges based on delta deposition (difference in deposition between view year and the base year).
   */
  DELTA_DEPOSITION,
  /**
   * ranges based on deviation from critical deposition (difference between deposition and the critical deposition (KDW)).
   */
  DEVIATION_FROM_CRITICAL_DEPOSITION;

}
