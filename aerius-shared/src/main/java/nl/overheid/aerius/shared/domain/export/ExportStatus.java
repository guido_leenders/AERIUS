/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.export;

import java.io.Serializable;

import nl.overheid.aerius.shared.domain.DownloadInfo;

/**
 * Event indicating a export is finished.
 */
public class ExportStatus implements Serializable {
  // An export key will always be prefixed with this.
  public static final String KEY_PREFIX = "exportJob_";

  private static final long serialVersionUID = -707840708568512109L;
  private static final int THIRTYONE_PRIME = 31;
  private static final int SEVENTEEN_PRIME = 17;

  /**
   * Enum representing the status of the export.
   */
  public enum Status {
    // If the export is still running.
    RUNNING,
    ERROR,
    // If the export finished successfully.
    FINISHED
  }

  private String exportId;
  private DownloadInfo downloadInfo;
  private Status status;

  // Needed for GWT.
  public ExportStatus() { }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && exportId != null && exportId.equals(((ExportStatus) obj).exportId)
        && status != null && status.equals(((ExportStatus) obj).status);
  }

  @Override
  public int hashCode() {
    return (THIRTYONE_PRIME * (SEVENTEEN_PRIME + exportId.hashCode())) + (status == null ? 0 : status.ordinal());
  }

  public DownloadInfo getDownloadInfo() {
    return downloadInfo;
  }

  public String getExportId() {
    return exportId;
  }

  public Status getStatus() {
    return status;
  }

  public void setDownloadInfo(final DownloadInfo downloadInfo) {
    this.downloadInfo = downloadInfo;
  }

  public void setExportId(final String exportId) {
    this.exportId = exportId;
  }

  public void setStatus(final Status status) {
    this.status = status;
  }

}
