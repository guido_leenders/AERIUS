/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.domain.search;

import java.io.Serializable;
import java.util.ArrayList;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;

/**
 * POJO containing a Search suggestion to be used in the search widget.
 */
public class MapSearchSuggestion extends SearchSuggestion<MapSearchSuggestionType, MapSearchSuggestion> implements Serializable {

  private static final long serialVersionUID = 6485305781632653L;

  private float distance;
  private Point point;
  private BBox zoomBox;
  private ArrayList<MapSearchSuggestion> subItems;

  // Needed for GWT.
  public MapSearchSuggestion() {
    this(0, null, null);
  }

  public MapSearchSuggestion(final long id, final String name, final MapSearchSuggestionType type) {
    super((int) id, name, type);
  }

  @Override
  public ArrayList<MapSearchSuggestion> getSubItems() {
    if (subItems == null) {
      subItems = new ArrayList<>();
    }

    return subItems;
  }

  public float getDistance() {
    return distance;
  }

  public void setDistance(final float distance) {
    this.distance = distance;
  }

  public Point getPoint() {
    return point;
  }

  /**
   * Set the GeoCoder point which is returned from the GeoCoder webservice.
   * Usually means the zoombox is still unknown (null) and will be retrieved later (based on the point).
   * If zoombox is known, then GeoCoder point doesn't have to be set.
   * @param point Point returned from GeoCoder webservice
   */
  public void setPoint(final Point point) {
    this.point = point;
  }

  public BBox getZoomBox() {
    return zoomBox;
  }

  /**
   * Set the box to zoom to when the search item is clicked. Can be null when it is not yet known,
   * but in that case specify the GeoCoder point instead (setPoint).
   * @param zoomBox Bounding box to zoom to.
   */
  public void setZoomBox(final BBox zoomBox) {
    this.zoomBox = zoomBox;
  }

  @Override
  public String toString() {
    return "MapSearchSuggestion [distance=" + distance + ", point=" + point + ", zoomBox=" + zoomBox + "] " + super.toString();
  }
}
