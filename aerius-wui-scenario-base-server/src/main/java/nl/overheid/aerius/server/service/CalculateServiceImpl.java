/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculationSummaryFactory;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultReady;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.CalculateService;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * <p>
 * Implementation of the {@link CalculateService}.
 * </p>
 *
 * <p>
 * Service contract:
 * </p>
 *
 * <ol>
 * <li>The client starts a calculation using startCalculation(), a unique string is sent back that can be used to track the calculation. The
 * {@link ServerCalculationTracker} is created and is stored in the session.</li>
 * <li>The client starts asking for the {@link CalculationInitResult} using the key. This will contain information for the client like calculation
 * IDs.</li>
 * <li>When a CalculationInitResult is returned, the client starts polling to check if calculation results are available using
 * getCalculationResultReady(). If there are no results, but the calculation is still in progress, null is returned. In other cases, a
 * {@link CalculationResultReady} is returned, containing a specific ID (for uniqueness) and a marker specifying if the calculation is complete.</li>
 * <li>For each {@link CalculationResultReady} returned previously, the client will call getCalculationResult, providing the calculation key. The
 * server will send back the first CalculationResult in the queue.</li>
 * <li>If, and only if, the <i>last result has been retrieved</i> by the client, will a {@link CalculationResultReady} specify the calculation has
 * completed.</li>
 * </ol>
 */
public class CalculateServiceImpl implements CalculateService {
  private static final Logger LOG = LoggerFactory.getLogger(CalculateServiceImpl.class);

  private static final String CALCULATION_TRACKER_PREFIX = "CALCTRACKER_";

  private final ScenarioBaseSession session;
  private final PMF pmf;

  public CalculateServiceImpl(final PMF pmf, final ScenarioBaseSession session) {
    this.pmf = pmf;
    this.session = session;
  }

  @Override
  public String startCalculation(final String lastCalculationKey) throws AeriusException {
    cleanLastCalculation(lastCalculationKey);
    return UUID.randomUUID().toString();
  }

  @Override
  public String startCalculation(final CalculatedScenario scenario, final String lastCalculationKey) throws AeriusException {
    cleanLastCalculation(lastCalculationKey);
    final String calculationKey = UUID.randomUUID().toString();
    LOG.info("Starting calculation {}", calculationKey);

    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(scenario);
    inputData.setExportType(ExportType.CALCULATION_UI);
    inputData.setQueueName(QueueEnum.CALCULATION_UI.getQueueName());

    final ServerCalculationTracker tracker = new ServerCalculationTracker(calculationKey, scenario);
    setCalculationTracker(tracker);
    try {
      TaskClientFactory.getInstance().sendTask(inputData, calculationKey, tracker, WorkerType.CALCULATOR, QueueEnum.CALCULATION_UI.getQueueName());
    } catch (final IOException e) {
      LOG.error("Error while sending calculation task to queues", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return calculationKey;
  }

  @Override
  public void trackCalculation(final String calculationKey, final ArrayList<Calculation> calculations) throws AeriusException {
    LOG.trace("Tracking calculation {}", calculationKey);

    try (final Connection con = pmf.getConnection()) {
      final ServerCalculationTracker tracker = new ServerCalculationTracker(calculationKey, null);
      setCalculationTracker(tracker);

      final CalculationInitResult initResult = new CalculationInitResult(calculations.get(0).getOptions(), calculations);
      initResult.setCalculationKey(calculationKey);

      tracker.onSuccess(initResult, calculationKey, null);

      final CalculationSetOptions options = calculations.get(0).getOptions();

      final ArrayList<Substance> substances = options.getSubstances();
      final CalculationSummary summary = CalculationSummaryFactory.getSummary(con, options.getCalculationType(),
          calculations.get(0).getCalculationId(), calculations.size() > 1 ? calculations.get(1).getCalculationId() : -1, substances);

      tracker.onSuccess(summary, calculationKey, null);
    } catch (final SQLException e) {
      LOG.error("Error while tracking calculation results.", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  @Override
  public CalculationInitResult getInitCalculationResult(final String calculationKey) throws AeriusException {
    final ServerCalculationTracker tracker = getCalculationTracker(calculationKey);
    return tracker.getInitResult();
  }

  @Override
  public ResultHighValuesByDistances getResultHighValuesByDistances(final String calculationKey) throws AeriusException {
    final ServerCalculationTracker tracker = getCalculationTracker(calculationKey);
    return tracker.getResultHighValuesByDistances();
  }

  @Override
  public CalculationResultReady getCalculationResultReady(final String calculationKey) throws AeriusException {
    final ServerCalculationTracker tracker = getCalculationTracker(calculationKey);
    return tracker.getResultReady();
  }

  @Override
  public ArrayList<PartialCalculationResult> getCalculationResultPoints(final String calculationKey) throws AeriusException {
    final ServerCalculationTracker tracker = getCalculationTracker(calculationKey);
    return tracker.getCalculationResultPoints();
  }

  @Override
  public CalculationSummary getSummaryInfo(final String calculationKey) throws AeriusException {
    final ServerCalculationTracker tracker = getCalculationTracker(calculationKey);
    return tracker.getSummary();
  }

  @Override
  public void cancelCalculation(final String calculationKey) throws AeriusException {
    // TODO: there currently is no way yet to cancel a calculation running on the worker.
    // for now, just remove the calculationtracker from the session.
    removeCalculationTracker(calculationKey);
  }

  /**
   * Removes the last calculation from the database and clears the calculation session state.
   *
   * @param con
   *          Database connection
   * @param session
   *          Session object
   * @param lastCalculationId
   */
  public void cleanLastCalculation(final String lastCalculationKey) {
    removeCalculationTracker(lastCalculationKey);
  }

  @Override
  public CalculationSummary getSummaryInfo(final CalculationType calculationType, final int calculationId1, final int calculationId2,
      final ArrayList<Substance> substances) throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      return CalculationSummaryFactory.getSummary(con, calculationType, calculationId1, calculationId2, substances);
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private void setCalculationTracker(final ServerCalculationTracker tracker) {
    session.getSession().setAttribute(CALCULATION_TRACKER_PREFIX + tracker.getCalculationKey(), tracker);
  }

  private ServerCalculationTracker getCalculationTrackerFromSession(final String calculationKey) {
    return (ServerCalculationTracker) session.getSession().getAttribute(CALCULATION_TRACKER_PREFIX + calculationKey);
  }

  private ServerCalculationTracker getCalculationTracker(final String calculationKey) throws AeriusException {
    final ServerCalculationTracker tracker = getCalculationTrackerFromSession(calculationKey);
    if (tracker == null) {
      LOG.error("Could not find tracker for calculation key", calculationKey);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    return tracker;
  }

  private void removeCalculationTracker(final String calculationKey) {
    final ServerCalculationTracker tracker = getCalculationTrackerFromSession(calculationKey);
    if (tracker != null) {
      // Ensure it knows it shouldn't wait for more stuff however, freeing up some resources.
      tracker.cancel();
      session.getSession().removeAttribute(CALCULATION_TRACKER_PREFIX + calculationKey);
    }
  }
}
