/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultReady;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.TaskCancelListener;
import nl.overheid.aerius.taskmanager.client.TaskMultipleResultCallback;

/**
 * Class for the server to keep track of a calculation results.
 */
public class ServerCalculationTracker implements TaskMultipleResultCallback, Serializable {
  private static final long serialVersionUID = 2034142542805847572L;

  private static final Logger LOG = LoggerFactory.getLogger(ServerCalculationTracker.class);

  private final CalculatedScenario scenario;
  private final String calculationKey;
  private CalculationInitResult initResult;
  private boolean resultReady;
  private final Queue<PartialCalculationResult> resultPoints = new ConcurrentLinkedDeque<>();
  private ResultHighValuesByDistances resultHighValues;
  private CalculationSummary summary;
  private Exception exception;

  private transient TaskCancelListener cancelListener;


  ServerCalculationTracker(final String calculationKey, final CalculatedScenario scenario) {
    this.calculationKey = calculationKey;
    this.scenario = scenario;
  }

  @Override
  public void onSuccess(final Object value, final String correlationId, final String messageId) {
    synchronized (this) {
      if (calculationKey.equalsIgnoreCase(correlationId)) {
        if (value instanceof CalculationInitResult) {
          initResult = (CalculationInitResult) value;
        } else if (value instanceof ResultHighValuesByDistances) {
          resultHighValues = (ResultHighValuesByDistances) value;
        } else if (value instanceof PartialCalculationResult) {
          if (CalculationType.CUSTOM_POINTS == scenario.getOptions().getCalculationType()) {
            resultPoints.add((PartialCalculationResult) value);
          }
          resultReady = true;
        } else if (value instanceof CalculationSummary) {
          summary = (CalculationSummary) value;
        } else {
          LOG.error("Received unexpected object {} for {}.", value, calculationKey);
        }
      }
    }
  }

  @Override
  public void onFailure(final Exception exception, final String correlationId, final String messageId) {
    if (calculationKey.equalsIgnoreCase(correlationId)) {
      this.exception = exception;
    }
  }

  @Override
  public boolean isFinalResult(final Object value) {
    return value instanceof CalculationSummary;
  }

  String getCalculationKey() {
    return calculationKey;
  }

  ArrayList<Calculation> getCalculations() {
    final ArrayList<Calculation> calculations = new ArrayList<>();
    if (initResult != null) {
      calculations.addAll(initResult.getCalculations());
    }
    return calculations;
  }

  public CalculationInitResult getInitResult() throws AeriusException {
    checkForErrors();
    if (initResult != null) {
      initResult.setCalculationKey(calculationKey);
    }
    return initResult;
  }

  public CalculationSummary getSummary() throws AeriusException {
    checkForErrors();
    return summary;
  }

  public ResultHighValuesByDistances getResultHighValuesByDistances() throws AeriusException {
    checkForErrors();
    synchronized (this) {
      try {
        return resultHighValues;
      } finally {
        resultHighValues = null;
      }
    }
  }

  public CalculationResultReady getResultReady() throws AeriusException {
    checkForErrors();
    final CalculationResultReady resultReady;
    if (this.resultReady) {
      this.resultReady = false;
      resultReady = new CalculationResultReady(calculationKey, false, UUID.randomUUID().toString());
    } else if (summary == null) {
      resultReady = null;
    } else {
      resultReady = new CalculationResultReady(calculationKey, true, UUID.randomUUID().toString());
    }
    // if no results available or no summary around yet, we're expecting more results, return null.
    return resultReady;
  }

  public ArrayList<PartialCalculationResult> getCalculationResultPoints() throws AeriusException {
    checkForErrors();
    final ArrayList<PartialCalculationResult> results = new ArrayList<>(resultPoints);
    resultPoints.clear();
    return results;
   }

  private void checkForErrors() throws AeriusException {
    if (exception != null) {
      if (exception instanceof AeriusException) {
        throw (AeriusException) exception;
      } else {
        LOG.error("Retrieved a non-AERIUS exception", exception);
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }
  }

  public void cancel() {
    if (cancelListener != null) {
      cancelListener.cancelCurrentTask();
    }
  }

  @Override
  public void setTaskCancelListener(final TaskCancelListener listener) {
    this.cancelListener = listener;
  }

}
