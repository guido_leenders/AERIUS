/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.servlet;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.UserGeoLayerRepository;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.importer.layers.UserGeoLayerVisitor;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.i18n.Internationalizer;
import nl.overheid.aerius.server.service.ImportService;
import nl.overheid.aerius.server.service.ImportService.UploadOptions;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.UserGeoLayer;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Standard servlet to process file upload via HTTP POST (the only way to upload files to the server).
 */
@MultipartConfig
@WebServlet("/aerius/" + SharedConstants.IMPORT_SAVE_SERVLET)
public class ScenarioBaseUploadServlet extends HttpServlet {

  private static final long serialVersionUID = -7558481946838815602L;

  /**
   * Processes upload and returns success or failure as string. The actual result is stored in the session such that the client can retrieve it via an
   * RPC call.
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
    // Create unique uuid to tie the imported sources to
    final String uuid = UUID.randomUUID().toString();
    final String scenarioUuid = SharedConstants.IMPORT_SCENARIO_PREFIX + uuid;

    final Locale locale = Internationalizer.getLocaleFromCookie(request);
    final ImportService service = new ImportService(ServerPMF.getInstance(), locale);
    final boolean returnSources = Boolean.parseBoolean(request.getParameter(SharedConstants.IMPORT_SOURCES_FIELD_NAME));
    final boolean persistResults = Boolean.parseBoolean(request.getParameter(SharedConstants.IMPORT_RESULTS_FIELD_NAME));
    final boolean createGeoLayers = Boolean.parseBoolean(request.getParameter(SharedConstants.IMPORT_CREATE_GEO_LAYERS_FIELD_NAME));
    // if results are included we skip validation because if there are a lot of results it will take a long time to validate xml.
    final boolean validate = !persistResults;

    final HttpSession session = request.getSession(true);
    // Send the response key back to the client
    final boolean success = processUpload(request, scenarioUuid, service, returnSources, persistResults, validate, createGeoLayers)
        && (!persistResults || storeResultsInSession(session, uuid, service, createGeoLayers));
    session.setAttribute(scenarioUuid, success ? service.getResult() : service.getServerException());
    response.setStatus(HttpServletResponse.SC_CREATED);
    service.sendResponse(response, success, uuid);
  }

  /**
   * Process the import, storing a result object in the session
   * @param request
   * @param service
   * @param returnSources
   * @param persistResults
   * @param validate
   * @return
   */
  private boolean processUpload(final HttpServletRequest request, final String uuid, final ImportService service, final boolean returnSources,
      final boolean persistResults, final boolean validate, final boolean createGeoLayers) {
    final UploadOptions uo = new UploadOptions();
    uo.setReturnSources(returnSources);
    uo.setPersistResults(persistResults);
    uo.setValidate(validate);
    uo.setCreateGeoLayers(createGeoLayers);
    return service.processUpload(request, uuid, uo);
  }

  /**
   * Store things in session
   *
   * @param request
   * @param resultsUuid
   * @param service
   * @param createGeoLayers
   * @return
   */
  private boolean storeResultsInSession(final HttpSession session, final String uuid, final ImportService service, final boolean createGeoLayers) {
    boolean success = true;
    try (Connection con = ServerPMF.getInstance().getConnection()) {
      final ImportResult result = service.getResult();
      final int situationId2 = result.getSourceLists().size() == 2 ? 1 : -1;
      final CalculatedScenario calculatedScenario = CalculatedScenarioUtil.toCalculatedScenario(result, 0, situationId2);
      calculatedScenario.setYear(result.getImportedYear());
      calculatedScenario.setOptions(getOptions());
      calculatedScenario.getOptions().setTemporaryProjectYears(result.getImportedTemporaryPeriod());
      if (calculatedScenario.hasResultPoints()) {
        CalculationRepository.insertCalculationWithResults(con, calculatedScenario);
      }
      session.setAttribute(SharedConstants.IMPORT_SCENARIO_CALCULATIONS_PREFIX + uuid, calculatedScenario.getCalculations());
      // clear results points from import results, otherwise they are sent back to the client, they are send differently back.
      result.clearResultPoints();
      if (createGeoLayers) {
        createGeoLayers(con, result);
      }
    } catch (final SQLException e) {
      success = false;

      try {
        service.saveImportError(Reason.SQL_ERROR, new Exception("Could not insert calculation with results", e));
      } catch (final Exception ei) {
        // Enough logging for today.
      }
    } catch (final AeriusException e) {
      success = false;
      try {
        service.saveImportError(e.getReason(), e);
      } catch (final Exception ei) {
        // Enough logging for today.
      }
    }
    return success;
  }

  private static CalculationSetOptions getOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    options.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
    options.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    options.setCalculationType(CalculationType.PAS);
    return options;
  }

  protected void createGeoLayers(final Connection con, final ImportResult result) throws SQLException, AeriusException {
    final ArrayList<EmissionSourceList> sourceLists = result.getSourceLists();

    if (sourceLists.size() == 1) {
      final int importedImaerFileId = UserGeoLayerRepository.insertImaerFile(con, GMLWriter.LATEST_WRITER_VERSION.toString(), "Unknown");
      result.setImportedImaerFileId(importedImaerFileId);

      final GMLWriter writer = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(con));
      final UserGeoLayerVisitor visitor = new UserGeoLayerVisitor(con, writer, result.getImportedYear(), importedImaerFileId);

      for (final EmissionSource source : sourceLists.get(0)) {
        source.accept(visitor);
      }

      final ArrayList<UserGeoLayer> layers = result.getUserGeoLayers();
      layers.addAll(UserGeoLayerRepository.getUserGeoLayer(con, importedImaerFileId));
    }
  }
}
