/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.shared.domain.calculation.Calculation;

/**
 * Listen to CalculationJob object being destroyed and then delete all calculation results.
 */
@WebListener
public class ActiveCalculationTrackerListener implements HttpSessionAttributeListener {
  private static final Logger LOG = LoggerFactory.getLogger(ActiveCalculationTrackerListener.class);

  @Override
  public void attributeAdded(final HttpSessionBindingEvent se) {
    // no-op
  }

  @Override
  public void attributeRemoved(final HttpSessionBindingEvent se) {
    if (se.getValue() instanceof ServerCalculationTracker) {
      final ServerCalculationTracker tracker = (ServerCalculationTracker) se.getValue();
      try (Connection con = ServerPMF.getInstance().getConnection()) {
        for (final Calculation oldCalculation : tracker.getCalculations()) {
          CalculationRepository.removeCalculation(con, oldCalculation.getCalculationId());
        }
      } catch (final SQLException e) {
        LOG.error("attributeRemoved failed", e);
      }
    }
  }

  @Override
  public void attributeReplaced(final HttpSessionBindingEvent se) {
    // no-op
  }

}
