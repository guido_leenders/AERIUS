/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.menu;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.menu.MenuItem;
import nl.overheid.aerius.wui.main.ui.menu.MenuItemListWidget;
import nl.overheid.aerius.wui.main.ui.menu.SimpleMenuItemFactory;

public class ScenarioBaseMenuItemFactory extends SimpleMenuItemFactory {
  @Override
  public IsWidget createMenuItem(final PlaceController placeController, final MenuItem item, final String styleName) {
    if (item instanceof ScenarioBaseListMenuItem) {
      return new MenuItemListWidget<>(placeController, (ScenarioBaseListMenuItem) item, R.css().calculatorThemeSwitch());
    }

    return super.createMenuItem(placeController, item, styleName);
  }
}
