/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;

import nl.overheid.aerius.shared.domain.source.EmissionSubSource;
import nl.overheid.aerius.shared.domain.source.EmissionSubSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.wui.scenario.base.ui.source.EmissionSubSourceViewerVisitor.DriverViewerCombo;
import nl.overheid.aerius.wui.scenario.base.ui.source.FarmLodgingStandardEmissionViewer.FarmLodgingStandardEmissionDriver;

/**
 * Visitor pattern to select the editor to use.
 */
@SuppressWarnings("rawtypes")
class EmissionSubSourceViewerVisitor implements EmissionSubSourceVisitor<DriverViewerCombo> {

  static class DriverViewerCombo<S extends EmissionSubSource, E extends EmissionSubViewer<S>, D extends SimpleBeanEditorDriver> {
    E editor;
    D driver;

    @SuppressWarnings("unchecked")
    public DriverViewerCombo(final E editor, final D driver) {
      this.editor = editor;
      this.driver = driver;
      driver.initialize(editor);
    }

    public D getDriver() {
      return driver;
    }

    public E getEditor() {
      return editor;
    }
  }

  private final DriverViewerCombo<FarmLodgingStandardEmissions, FarmLodgingStandardEmissionViewer, FarmLodgingStandardEmissionDriver>
  farmLodgingStandardEmissionsViewer;

  public EmissionSubSourceViewerVisitor() {
    farmLodgingStandardEmissionsViewer =
        new DriverViewerCombo<FarmLodgingStandardEmissions, FarmLodgingStandardEmissionViewer, FarmLodgingStandardEmissionDriver>(
            new FarmLodgingStandardEmissionViewer(), (FarmLodgingStandardEmissionDriver) GWT.create(FarmLodgingStandardEmissionDriver.class));
  }

  @Override
  public DriverViewerCombo visit(final FarmLodgingStandardEmissions standardSubSource) throws AeriusException {
    return farmLodgingStandardEmissionsViewer;
  }

  @Override
  public DriverViewerCombo visit(final FarmLodgingCustomEmissions customSubSource) throws AeriusException {
    return null; // no popup
  }

  @Override
  public DriverViewerCombo visit(final FarmEmissionSource emissionSubSource) throws AeriusException {
    return null; // no popup
  }

  public void onEnsureDebugId(final String baseID) {
    farmLodgingStandardEmissionsViewer.getEditor().ensureDebugId(baseID);
  }

}
