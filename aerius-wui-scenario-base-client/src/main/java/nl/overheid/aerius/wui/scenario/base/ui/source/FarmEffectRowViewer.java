/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.dom.client.Style.TextDecoration;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;

/**
 * Abstract row class for objects having an effect on farm emissions (reducing them most likely).
 */
abstract class FarmEffectRowViewer<T extends Object> extends Composite implements LeafValueEditor<T> {

  protected static final EmissionValueKey EMISSION_VALUE_KEY = new EmissionValueKey(Substance.NH3);

  @UiField Label headerValue;
  @UiField Label amountValue;
  @UiField Label factorValue;
  @UiField Label effectValue;
  @UiField Label emissionValue;

  protected final FarmLodgingStandardEmissions source;

  public FarmEffectRowViewer(final FarmLodgingStandardEmissions source) {
    this.source = source;
  }

  @Override
  public void setValue(final T value) {
    if (value != null) {
      setValueTexts(value);

      if (!isLast(value)) {
        emissionValue.getElement().getStyle().setTextDecoration(TextDecoration.LINE_THROUGH);
      }
    }
  }

  protected abstract void setValueTexts(final T value);

  protected abstract boolean isLast(T value);

  @Override
  public T getValue() {
    // No-op
    return null;
  }

}
