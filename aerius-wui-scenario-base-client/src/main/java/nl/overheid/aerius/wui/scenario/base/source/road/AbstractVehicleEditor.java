/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;

/**
 *
 */
public abstract class AbstractVehicleEditor<V extends VehicleEmissions> extends Composite implements InputEditor<V> {

  @UiField DoubleValueBox vehiclesPerTimeUnit;
  @UiField(provided = true) ListBoxEditor<TimeUnit> timeUnitEditor = new ListBoxEditor<TimeUnit>() {
    @Override
    protected String getLabel(final TimeUnit value) {
      return M.messages().timeUnit(value);
    }
  };

  protected void afterBinding(final HelpPopupController hpC) {
    vehiclesPerTimeUnit.addValidator(new NotZeroValidator<Double>() {
      @Override
      protected String getMessage(final Double value) {
        return M.messages().ivRoadNumberOfVehiclesNotEmpty();
      }
    });

    hpC.addWidget(vehiclesPerTimeUnit, hpC.tt().ttRoadVehiclesPerTimeUnit());

    timeUnitEditor.addItems(TimeUnit.values());
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    // no-op
  }

  @Override
  public void resetPlaceholders() {
    vehiclesPerTimeUnit.resetPlaceHolder();
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return null;
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    vehiclesPerTimeUnit.ensureDebugId(TestID.INPUT_VEHICLES_PER_TIMEUNIT);
    timeUnitEditor.ensureDebugId(TestID.LIST_TIMEUNIT);
  }

}
