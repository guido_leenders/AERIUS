/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.HasFarmLodgingSystemDefinitions;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;

/**
 *
 */
class SystemDefinitionEditor extends ListBoxEditor<FarmLodgingSystemDefinition> {

  @Override
  protected String getLabel(final FarmLodgingSystemDefinition value) {
    return value == null ? "" : value.getName();
  }

  void updateSystemDefinitionList(final HasFarmLodgingSystemDefinitions hasFarmLodgingSystemDefinitions,
      final FarmLodgingSystemDefinition selectSystemDefinition) {
    clear();
    if (hasFarmLodgingSystemDefinitions != null) {
      addItems(hasFarmLodgingSystemDefinitions.getFarmLodgingSystemDefinitions());
      if (selectSystemDefinition == null) {
        // If single entry, select it, otherwise select nothing.
        setSelectedIndex(hasFarmLodgingSystemDefinitions.getFarmLodgingSystemDefinitions().size() == 1 ? 0 : -1);
      } else {
        setSelectedDefinition(hasFarmLodgingSystemDefinitions, selectSystemDefinition);
      }
    }
  }

  private void setSelectedDefinition(final HasFarmLodgingSystemDefinitions hasFarmLodgingSystemDefinitions,
      final FarmLodgingSystemDefinition selectSystemDefinition) {
    for (final FarmLodgingSystemDefinition definition : hasFarmLodgingSystemDefinitions.getFarmLodgingSystemDefinitions()) {
      if (definition.equals(selectSystemDefinition)) {
        setValue(definition);
      }
    }
  }

}
