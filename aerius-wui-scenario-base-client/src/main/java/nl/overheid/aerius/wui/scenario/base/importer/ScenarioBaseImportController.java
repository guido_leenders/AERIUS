/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.importer;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.RetrieveImportServiceAsync;
import nl.overheid.aerius.wui.main.event.ImportEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.ui.importer.ImportConflictAction;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.PermitCalculationRadiusChangeEvent;
import nl.overheid.aerius.wui.scenario.base.events.TemporaryPeriodChangeEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.processor.ImportOutputPollingAgent;

/**
 * Handles import dialog and updates data from the import in the ui. TODO move merge logic into separate util class.
 */
public abstract class ScenarioBaseImportController {
  protected final RetrieveImportServiceAsync service;
  protected ScenarioImportDialogController idController;

  private final PlaceController placeController;
  protected final EventBus eventBus;
  private final ScenarioBaseMapLayoutPanel map;

  private final ScenarioBaseAppContext<?, ?> context;
  private final HelpPopupController hpC;
  private final boolean includeResults;
  private final boolean includeSources;
  private final boolean createGeoLayers;
  private final boolean showUtilisationCheckBox;

  @Inject
  public ScenarioBaseImportController(final ScenarioBaseMapLayoutPanel map, final ScenarioBaseAppContext<?, ?> context,
      final RetrieveImportServiceAsync service, final HelpPopupController hpC, final boolean includeSources, final boolean includeResults,
      final boolean createGeoLayers, final boolean showUtilisationCheckBox) {
    this.map = map;
    this.context = context;
    this.includeSources = includeSources;
    this.includeResults = includeResults;
    this.createGeoLayers = createGeoLayers;
    this.showUtilisationCheckBox = showUtilisationCheckBox;
    this.placeController = context.getPlaceController();
    this.eventBus = context.getEventBus();
    this.service = service;
    this.hpC = hpC;
  }

  /**
   * Shows the import dialog.
   */
  public void showImportDialog() {
    showImportDialog(false);
  }

  public final void showImportDialog(final boolean dragMode) {
    if (!(placeController.getWhere() instanceof ScenarioBasePlace)) {
      return;
    }
    final ScenarioBasePlace place = (ScenarioBasePlace) placeController.getWhere();
    final ScenarioImportDialogPanel scenarioImportDialogPanel = new ScenarioImportDialogPanel(M.messages().importPanelText(), dragMode, hpC);
    scenarioImportDialogPanel.setIncludeSources(includeSources);
    scenarioImportDialogPanel.setIncludeResults(includeResults);
    scenarioImportDialogPanel.setCreateGeoLayers(createGeoLayers);
    scenarioImportDialogPanel.showUtilisationCheckBox(showUtilisationCheckBox);
    idController = getImportDialogController(context.getUserContext().getScenario(), place, scenarioImportDialogPanel);
    idController.show();
  }

  protected ScenarioImportDialogController getImportDialogController(final Scenario scenario, final ScenarioBasePlace place,
      final ScenarioImportDialogPanel scenarioImportDialogPanel) {
    return new ScenarioImportDialogController(scenario.getSources(place.getActiveSituationId()),
        scenario.getCalculationPoints(), new ImportOutputPollingAgent(service), scenarioImportDialogPanel) {

      @Override
      public void handleImportResult(final ImportResult result, final ImportConflictAction conflictAction, final String uuid) {
        ScenarioBaseImportController.this.handleImportResult(result, conflictAction, place, uuid);
      }
    };
  }

  public void processStartUpImport() {
    final String uuid = getUUID();
    if (uuid != null && !uuid.isEmpty()) {
      service.getImportResult(uuid, new AppAsyncCallback<ImportResult>() {
        @Override
        public void onSuccess(final ImportResult result) {
          result.setType(ImportType.GML);
          final ScenarioBasePlace place = (ScenarioBasePlace) placeController.getWhere();
          handleImportResult(result, ImportConflictAction.REMOVE_EXISTING, place, uuid);
        }
      });
    }
  }

  private native String getUUID() /*-{
    return $wnd.aerius_upload_uuid;
  }-*/;

  protected void handleImportResult(final ImportResult result, final ImportConflictAction conflictAction,
      final ScenarioBasePlace place, final String uuid) {
    String msg = M.messages().notificationImportComplete();
    int sid1 = place.getSid1();
    int sid2 = place.getSid2();
    Situation viewSituation = place.getSituation();
    final boolean hasVariantSources = result.getSourceLists().size() == 2;
    // in any case, remove all calculated results. Situation has changed.
    context.getUserContext().setCalculatedScenario(null);

    if (result.getType() == ImportType.PAA || hasVariantSources || cleanImport()) {
      msg += handleCleanImport(result);
      sid1 = 0;
      // If a comparison was imported (2 sets of imported sources), go to the comparison overview part.
      // else go to the normal overview part.
      sid2 = ScenarioBasePlace.NO_SID2;
      viewSituation = Situation.SITUATION_ONE;
      if (hasVariantSources) {
        viewSituation = Situation.COMPARISON;
        sid2 = 1;
      }
      msg += importCalculationPoints(result, conflictAction);
    } else {
      msg += handleNormalImport(result, conflictAction, place.getActiveSituationId());
    }
    NotificationUtil.broadcast(eventBus, new Notification(msg));

    for (final AeriusException warning : result.getWarnings()) {
      NotificationUtil.broadcast(eventBus, new Notification(M.getErrorMessage(warning)));
    }

    updateYear(result.getImportedYear());
    updateTemporaryYearPeriod(result.getImportedTemporaryPeriod());
    updatePriorityProjectRadius(result.getPermitCalculationRadiusType());
    updateMetaData(result.getMetaData());

    // Import complete, fire an event.
    eventBus.fireEvent(new ImportEvent(result));

    // If no sources, but with calculation points goto the calculation points page.
    final ScenarioBasePlace newPlace = getImportCompletePlace(sid1, sid2, result, viewSituation, uuid);

    if (newPlace.equals(place)) {
      // if we go to the same place the page isn't refreshed therefore a call
      // to set the markers must be made explicitly here.
      map.getMarkersLayer().setMarkers(context.getUserContext().getScenario(), place);
    }
    placeController.goTo(newPlace);
  }

  /**
   * @return Return true if a clean import must be enforced.
   */
  protected boolean cleanImport() {
    return false;
  }

  /**
   * Returns the new place to jump to. Should not return null.
   *
   * @param sid1
   * @param sid2
   * @param result
   * @param viewSituation
   * @return
   */
  protected abstract ScenarioBasePlace getImportCompletePlace(int sid1, int sid2, ImportResult result, Situation viewSituation, String uuid);

  private String handleNormalImport(final ImportResult result, final ImportConflictAction conflictAction, final int activeSId) {
    String msg = "";

    msg += importSourcesToCurrentSituation(result, conflictAction, activeSId);

    msg += importCalculationPoints(result, conflictAction);

    if (msg.isEmpty()) {
      msg += M.messages().notificationImportCompleteNoItems();
    }

    return msg;
  }

  private String importSourcesToCurrentSituation(final ImportResult result, final ImportConflictAction conflictAction, final int activeSId) {
    String msg = "";
    context.initEmissionSourceList(activeSId);
    final EmissionSourceList sourceList = context.getUserContext().getScenario().getSources(activeSId);
    final EmissionSourceList importedSources = result.getSources(0);

    if (importedSources != null && !importedSources.isEmpty()) {
      // imported sources list gets changed by merge (not a problem because it won't be used for anything else)
      final int importedSourcesSize = importedSources.size();
      int overwrittenSources = 0;

      switch (conflictAction) {
      case OVERWRITE:
        overwrittenSources = sourceList.mergeSources(importedSources);
        break;
      case REMOVE_EXISTING:
        // clear the old sourcelist before adding
        sourceList.clear();
        // don't add, let it fall through to the add option.
      case ADD:
        // add is the default case.
      default:
        // add to the existing list of sources for the selected situation.
        sourceList.addAll(importedSources);
        // If no name in the new list don't override the current name.
        if (importedSources.getName() != null && !importedSources.getName().isEmpty()) {
          setName(sourceList, importedSources);
        }
        break;
      }
      msg += M.messages().notificationImportCompleteSources(importedSourcesSize,
          overwrittenSources);
    }

    return msg;
  }

  private String importCalculationPoints(final ImportResult result, final ImportConflictAction conflictAction) {
    String msg = "";
    final CalculationPointList calculationPointList = context.getUserContext().getScenario().getCalculationPoints();
    final CalculationPointList scenarioCalcPointList = result.getCalculationPoints();

    if (scenarioCalcPointList != null && !scenarioCalcPointList.isEmpty()) {
      // imported points list gets changed by merge (not a problem because it won't be used for anything else)
      final int importedPoints = scenarioCalcPointList.size();
      int overwrittenPoints = 0;

      switch (conflictAction) {
      case OVERWRITE:
        overwrittenPoints = calculationPointList.merge(scenarioCalcPointList);
        break;
      case REMOVE_EXISTING:
        calculationPointList.clear();
        // don't break, let it fall through to the add option.
      case ADD:
      default:
        calculationPointList.addAll(scenarioCalcPointList);
        break;
      }

      msg += M.messages().notificationImportCompletePoints(importedPoints,
          overwrittenPoints);
    }
    return msg;
  }

  private String handleCleanImport(final ImportResult result) {
    String msg;
    if (result.getSources(0) != null) {
      // Ensure usercontext gets reset.
      context.getUserContext().getScenario().getSourceLists().clear();
      context.getUserContext().getScenario().getCalculationPoints().clear();
      int importedSources = addSources(result, 0);
      // Add imported sources for situation 2 if available.
      importedSources += addSources(result, 1);

      msg = M.messages().notificationImportCompleteSources(importedSources, 0);
    } else {
      msg = M.messages().notificationImportCompleteNoItems();
    }

    return msg;
  }

  private int addSources(final ImportResult result, final int index) {
    int importedSources = 0;
    final EmissionSourceList sources = result.getSources(index);
    if (sources != null) {
      importedSources += sources.size();
      // Add imported sources and set name.
      context.getUserContext().getScenario().addSources(sources);
      setName(sources, sources);
    }
    return importedSources;
  }

  /**
   * set name if imported list had a name or the default name. It overrides the name in the sourceList.
   *
   * @param sourceList sources object to set name
   * @param importedSources sources object to get name from
   * @param override if true
   */
  private void setName(final EmissionSourceList sourceList, final EmissionSourceList importedSources) {
    final String name = importedSources.getName();
    sourceList.setName(name == null || name.isEmpty() ? M.messages().situationDefaultName(importedSources.getId() + 1) : name);
  }

  public ScenarioImportDialogController getIdController() {
    return idController;
  }

  /**
   * Change the current calculation year based on imported year (don't notify/change if they are the same)
   *
   * @param importedYear year in imported file
   */
  private void updateYear(final int importedYear) {
    final int minYear = context.getContext().getSetting(SharedConstantsEnum.MIN_YEAR);
    final int maxYear = context.getContext().getSetting(SharedConstantsEnum.MAX_YEAR);
    final int newYear = importedYear < minYear ? minYear : importedYear > maxYear ? maxYear : importedYear;
    if (newYear != context.getUserContext().getEmissionValueKey().getYear()) {
      eventBus.fireEvent(new YearChangeEvent(newYear));
      NotificationUtil.broadcastMessage(eventBus, M.messages().notificationImportYearChanged());
    }
  }

  /**
   * Change the current calculation metadata (don't notify/change if they are the same)
   *
   * @param scenarioMetaData
   */
  private void updateMetaData(final ScenarioMetaData scenarioMetaData) {
    context.getUserContext().getScenario().setMetaData(scenarioMetaData);
  }

  /**
   * Change the current temporary project info based on import.
   *
   * @param temporaryYearPeriod period in imported file, null if it's a permanent project.
   */
  private void updateTemporaryYearPeriod(final Integer temporaryYearPeriod) {
    context.getCalculationOptions().setTemporaryProjectYears(temporaryYearPeriod);
    eventBus.fireEvent(new TemporaryPeriodChangeEvent(temporaryYearPeriod));
  }

  /**
   * Change the calculation radius info based on import.
   *
   * @param permitCalculationRadiusType the calculation radius type or null if no radius is specified.
   */
  private void updatePriorityProjectRadius(final PermitCalculationRadiusType permitCalculationRadiusType) {
    context.getCalculationOptions().setPermitCalculationRadiusType(permitCalculationRadiusType);
    eventBus.fireEvent(new PermitCalculationRadiusChangeEvent(permitCalculationRadiusType));
  }
}
