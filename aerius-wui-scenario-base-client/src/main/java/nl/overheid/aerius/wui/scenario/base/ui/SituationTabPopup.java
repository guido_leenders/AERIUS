/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Simple binder providing support for editing and deleting a situation.
 */
public class SituationTabPopup extends Composite {
  /**
   * Handler for this situation tab popup widget.
   */
  public interface SituationPopupHandler {
    /**
     * Change the situation name to the one specified.
     * 
     * @param name The new name.
     */
    void changeName(String name);

    /**
     * Remove this situation.
     */
    void delete();
  }

  interface SituationTabPopupUiBinder extends UiBinder<Widget, SituationTabPopup> {
  }

  private static SituationTabPopupUiBinder uiBinder = GWT.create(SituationTabPopupUiBinder.class);
  private final SituationPopupHandler handlers;
  private String originalName;

  @UiField TextBox nameField;
  @UiField Button deleteButton;
  @UiField Button okButton;

  /**
   * Situation tab popup, UI component for renaming and removing a situation.
   * 
   * @param handler handler doing the renaming and removing.
   */
  public SituationTabPopup(final SituationPopupHandler handler) {
    this.handlers = handler;
    initWidget(uiBinder.createAndBindUi(this));
    nameField.ensureDebugId(TestID.TAB_SIT_NAMEFIELD);
    deleteButton.ensureDebugId(TestID.TAB_SIT_DELETE);
    okButton.ensureDebugId(TestID.TAB_SIT_OK);
  }

  /**
   * Sets the popup situation name.
   * 
   * @param name The name to give this situation popup
   */
  public void setName(final String name) {
    originalName = name;
    nameField.setText(name);
  }

  /**
   * Sets whether or not this situation can be deleted.
   * 
   * @param canDelete true for aye, false for nay
   */
  public void setCanDelete(final boolean canDelete) {
    deleteButton.setVisible(canDelete);
  }

  @UiHandler("okButton")
  void onEditClick(final ClickEvent e) {
    if (nameField.getText() != null && !nameField.getText().isEmpty()) {
      handlers.changeName(nameField.getText());
    } else {
      handlers.changeName(originalName);
    }
  }

  @UiHandler("nameField")
  void onFocus(final FocusEvent e) {
    if (nameField.getText().startsWith(M.messages().situationDefaultNameEmpty())) {
      nameField.setText("");
    }
  }

  @UiHandler("deleteButton")
  void onDeleteClick(final ClickEvent e) {
    handlers.delete();
  }
}
