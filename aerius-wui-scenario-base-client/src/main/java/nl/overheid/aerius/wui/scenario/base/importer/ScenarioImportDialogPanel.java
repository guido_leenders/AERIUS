/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.importer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.File;
import com.google.gwt.dom.client.FileList;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.dom.client.DragEnterHandler;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragLeaveHandler;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DragOverHandler;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.dom.client.DropHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.RadioButtonGroupEditor;
import nl.overheid.aerius.wui.main.ui.importer.ImportConflictAction;
import nl.overheid.aerius.wui.main.ui.importer.ImportDialogPanel;
import nl.overheid.aerius.wui.main.ui.importer.ImportProperties;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

// Just an Editor because the ConfirmCancelDialog needs us to be, but don't create a driver/use @Ignore and such. Be fake. Be proud.
public class ScenarioImportDialogPanel extends Composite
implements ImportDialogPanel<ImportProperties>, Editor<ImportProperties>, DropHandler, DragEnterHandler, DragOverHandler, DragLeaveHandler {

  interface ScenarioImportDialogPanelUiBinder extends UiBinder<Widget, ScenarioImportDialogPanel> {}

  private static ScenarioImportDialogPanelUiBinder UI_BINDER = GWT.create(ScenarioImportDialogPanelUiBinder.class);

  interface Style extends CssResource {
    String extraPaddingForLastCounterLabel();

    String errorListDivEntry();

    String dragFieldHover();
  }

  @UiField(provided = true) DeckPanel contentSwitcher;
  @UiField Style style;
  @UiField Label subTitleDiv;
  @UiField Hidden includeSourcesField;
  @UiField Hidden includeResultsField;
  @UiField Hidden createGeoLayersField;
  @UiField FileUpload fileField;
  @UiField HTML errorField;
  @UiField Label counterLabel;
  @UiField Label counterConflictLabel;
  @UiField Label conflictLabel;
  @UiField FlowPanel importOptionsDiv;
  @UiField CheckBox utilisationCheckBox;
  @UiField Label importPanelCheckBoxUtilisationLabel;
  @UiField FlowPanel substanceDiv;
  @UiField(provided = true) RadioButton pm10RadioButton;
  @UiField(provided = true) RadioButton noxRadioButton;
  @UiField(provided = true) RadioButton nh3RadioButton;
  @UiField FlowPanel conflictDiv;
  @UiField(provided = true) RadioButton conflictOverwriteRadioButton;
  @UiField(provided = true) RadioButton conflictAddRadioButton;
  @UiField(provided = true) RadioButton conflictRemoveExistingRadioButton;
  @UiField FlowPanel lineErrorDiv;
  @UiField FlowPanel lineErrorListDiv;
  @UiField CheckBox errorContinueButton;
  @UiField FlowPanel dragField;
  @UiField Label regularImportSwitch;

  private final SpanElement errorContinueText = Document.get().createSpanElement();
  private final RadioButtonGroupEditor<Substance> substanceRadioButtonGroup =
      new RadioButtonGroupEditor<Substance>(SharedConstants.IMPORT_SUBSTANCE_FIELD_NAME) {

    @Override
    protected Substance string2Value(final String value) {
      return value == null ? null : Substance.substanceFromId(Integer.parseInt(value));
    }

    @Override
    protected String value2String(final Substance value) {
      return value == null ? null : String.valueOf(value.getId());
    }
  };
  private final RadioButtonGroupEditor<ImportConflictAction> conflictRadioButtonGroup =
      new RadioButtonGroupEditor<ImportConflictAction>(SharedConstants.IMPORT_CONFLICT_ACTION_TYPE_FIELD_NAME) {

    @Override
    protected ImportConflictAction string2Value(final String value) {
      return ImportConflictAction.safeValueOf(value);
    }

    @Override
    protected String value2String(final ImportConflictAction value) {
      return value == null ? null : value.name();
    }
  };
  private boolean includeResults;
  private boolean includeSources;
  private boolean createGeoLayers;

  public ScenarioImportDialogPanel(final String subTitle, final boolean dragMode, final HelpPopupController hpC) {
    pm10RadioButton = substanceRadioButtonGroup.createRadioButton(Substance.PM10.getName(), Substance.PM10);
    noxRadioButton = substanceRadioButtonGroup.createRadioButton(Substance.NOX.getName(), Substance.NOX);
    nh3RadioButton = substanceRadioButtonGroup.createRadioButton(Substance.NH3.getName(), Substance.NH3);

    conflictOverwriteRadioButton = conflictRadioButtonGroup.createRadioButton(
        M.messages().importPanelConflictOverwrite(), ImportConflictAction.OVERWRITE);
    conflictAddRadioButton = conflictRadioButtonGroup.createRadioButton(
        M.messages().importPanelConflictAdd(), ImportConflictAction.ADD);
    conflictRemoveExistingRadioButton = conflictRadioButtonGroup.createRadioButton(
        M.messages().importPanelConflictRemoveExisting(), ImportConflictAction.REMOVE_EXISTING);

    contentSwitcher = new DeckPanel() {
      @Override
      public void showWidget(final int index) {
        super.showWidget(index);

        ScenarioImportDialogPanel.this.fireEvent(new ChangeEvent() {});
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    contentSwitcher.showWidget(dragMode ? 1 : 0);

    addDomHandler(this, DragEnterEvent.getType());
    dragField.addDomHandler(this, DropEvent.getType());
    dragField.addDomHandler(this, DragOverEvent.getType());
    dragField.addDomHandler(this, DragLeaveEvent.getType());

    regularImportSwitch.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        contentSwitcher.showWidget(0);
      }
    });

    subTitleDiv.setText(subTitle);

    includeSourcesField.setName(SharedConstants.IMPORT_SOURCES_FIELD_NAME);
    includeResultsField.setName(SharedConstants.IMPORT_RESULTS_FIELD_NAME);
    createGeoLayersField.setName(SharedConstants.IMPORT_CREATE_GEO_LAYERS_FIELD_NAME);
    fileField.setName(SharedConstants.IMPORT_FILE_FIELD_NAME);
    errorContinueButton.getElement().appendChild(errorContinueText);
    errorContinueText.setInnerHTML(M.messages().importPanelContinueOnErrorNo());

    fileField.ensureDebugId(TestID.INPUT_IMPORTFILE);
    pm10RadioButton.ensureDebugId(TestID.RADIO_IMPORT_PM10);
    noxRadioButton.ensureDebugId(TestID.RADIO_IMPORT_NOX);
    nh3RadioButton.ensureDebugId(TestID.RADIO_IMPORT_NH3);
    conflictOverwriteRadioButton.ensureDebugId(TestID.RADIO_IMPORT_OVERWRITE);
    conflictAddRadioButton.ensureDebugId(TestID.RADIO_IMPORT_ADD);
    conflictRemoveExistingRadioButton.ensureDebugId(TestID.RADIO_IMPORT_REMOVE_EXISTING);
    errorContinueButton.ensureDebugId(TestID.CHECKBOX_IMPORT_ERRORCONTINUE);
    errorField.ensureDebugId(TestID.DIV_IMPORT_ERRORS);
    counterLabel.ensureDebugId(TestID.DIV_IMPORT_COUNT);
    counterConflictLabel.ensureDebugId(TestID.DIV_IMPORT_CONFLICTSCOUNT);
    utilisationCheckBox.ensureDebugId(TestID.CHECKBOX_UTILISATION);
    importPanelCheckBoxUtilisationLabel.ensureDebugId(TestID.LABEL_CHECKBOX_UTILISATION);

    pm10RadioButton.setVisible(false); // NO PM10
    hpC.addWidget(fileField, hpC.tt().ttImportFileField());

  }

  /**
   * Using RequestBuilder doesn't automatically set the proper multipart boundary headers.
   *
   * Sending the request natively does.
   *
   * @param fileName
   * @param file
   * @param includeSources
   * @param includeResults
   * @param createGeoLayers
   */
  private native void sendFile(String fileName, File file, boolean includeSources, boolean includeResults, boolean createGeoLayers) /*-{
    var x = this;

    var xhr = new XMLHttpRequest();
    xhr
        .open(
            'POST',
            @com.google.gwt.core.client.GWT::getModuleBaseURL()()
                + @nl.overheid.aerius.shared.SharedConstants::IMPORT_SAVE_SERVLET,
            true);

    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 201) {
        x.@nl.overheid.aerius.wui.scenario.base.importer.ScenarioImportDialogPanel::onResponseReceived(Ljava/lang/String;)(xhr.responseText)
      }
    };

    var formData = new FormData();
    formData.append(fileName, file);
    formData.append(@nl.overheid.aerius.shared.SharedConstants::IMPORT_SOURCES_FIELD_NAME, includeSources);
    formData.append(@nl.overheid.aerius.shared.SharedConstants::IMPORT_RESULTS_FIELD_NAME, includeResults);
    formData.append(@nl.overheid.aerius.shared.SharedConstants::IMPORT_CREATE_GEO_LAYERS_FIELD_NAME, createGeoLayers);
    x.@nl.overheid.aerius.wui.scenario.base.importer.ScenarioImportDialogPanel::onFileUploadStarted()();
    xhr.send(formData);
  }-*/;

  private void onFileUploadStarted() {
    // Switch back to normal import content
    contentSwitcher.showWidget(0);
    fireEvent(new SubmitEvent());
  }

  @Override
  public void onDragOver(final DragOverEvent event) {
    event.preventDefault();
    event.stopPropagation();

    // Yeah.... Due to dragEnter/dragLeave events firing unintuitively, we're doing this.
    dragField.setStyleName(style.dragFieldHover(), true);
  }

  @Override
  public void onDragEnter(final DragEnterEvent event) {
    event.preventDefault();
    event.stopPropagation();

    if (contentSwitcher.getVisibleWidget() != 1) {
      contentSwitcher.showWidget(1);
    }

    dragField.setStyleName(style.dragFieldHover(), true);
  }

  @Override
  public void onDragLeave(final DragLeaveEvent event) {
    event.preventDefault();
    event.stopPropagation();

    dragField.setStyleName(style.dragFieldHover(), false);
  }

  @Override
  public void onDrop(final DropEvent event) {
    event.preventDefault();
    event.stopPropagation();

    // TODO (optional) Add multiple file upload support (also for classic upload field)
    final FileList files = event.getDataTransfer().getFiles();
    if (files.length() > 0) {
      sendFile(SharedConstants.IMPORT_FILE_FIELD_NAME, files.getItem(0), includeSources, includeResults, createGeoLayers);
    }

    // See above todo
    //for (int i = 0; i < files.size(); i++) {
    //final File file = files.getFile(i);
    //
    //sendFile(SharedConstants.IMPORT_FILE_FIELD_NAME, file);
    //}
  }

  /**
   * This type of importing is kind of a hack, we should ideally get it out of this class
   * and move it somewhere more manageable.
   *
   * Mimicks a {@link SubmitCompleteEvent} of the form panel and sends it to the
   * import dialog, which will handle the full file import from there.
   *
   * @param response Response text sent by the server
   */
  private void onResponseReceived(final String response) {
    fireEvent(new SubmitCompleteEvent(response) {});
  }

  public void showError(final String error) {
    errorField.setHTML(error);
  }

  /**
   * Set to true if sources must be imported.
   * @param includeSources
   */
  public void setIncludeSources(final boolean includeSources) {
    this.includeSources = includeSources;
    includeSourcesField.setValue(String.valueOf(includeSources));
  }

  /**
   * Set to true if results must be imported.
   * @param includeResults the includeResults to set
   */
  public void setIncludeResults(final boolean includeResults) {
    this.includeResults = includeResults;
    includeResultsField.setValue(String.valueOf(includeResults));
  }

  /**
   * Set to true if Geo layers should be created.
   * @param createGeoLayers the createGeoLayers to set
   */
  public void setCreateGeoLayers(final boolean createGeoLayers) {
    this.createGeoLayers = createGeoLayers;
    createGeoLayersField.setValue(String.valueOf(createGeoLayers));
  }

  public boolean isUtilisation() {
    return utilisationCheckBox.getValue();
  }

  @UiHandler("importPanelCheckBoxUtilisationLabel")
  public void onImportPanelCheckBoxUtilisationLabelClick(final ClickEvent e) {
    utilisationCheckBox.setValue(!utilisationCheckBox.getValue(), true);
  }

  public void addFileUploadChangedHandler(final ChangeHandler changeHandler) {
    fileField.addChangeHandler(changeHandler);
  }

  public void addSubstanceValueChangeHandler(final ValueChangeHandler<Boolean> changeHandler) {
    substanceRadioButtonGroup.addValueChangeHandler(changeHandler);
  }

  public void addConflictValueChangeHandler(final ValueChangeHandler<Boolean> changeHandler) {
    conflictRadioButtonGroup.addValueChangeHandler(changeHandler);
  }

  public void addErrorOnContinueValueChangeHandler(final ValueChangeHandler<Boolean> changeHandler) {
    errorContinueButton.addValueChangeHandler(changeHandler);
  }

  public String getFileUploadFilename() {
    return fileField.getFilename();
  }

  public void setCounterData(final int amountOfSources, final int amountOfPoints, final int conflictsImported, final int conflictsExisting) {
    counterLabel.setText(M.messages().importPanelCounter(amountOfSources, amountOfPoints));
    counterLabel.getElement().getStyle().setDisplay(Display.BLOCK);
    counterLabel.setStyleName(style.extraPaddingForLastCounterLabel(), conflictsImported == 0);

    if (conflictsImported > 0) {
      counterConflictLabel.setText(M.messages().importPanelCounterConflict(conflictsImported, conflictsExisting));
      counterConflictLabel.getElement().getStyle().setDisplay(Display.BLOCK);
      counterConflictLabel.setStyleName(style.extraPaddingForLastCounterLabel(), conflictsImported > 0);
    }
  }

  @UiHandler("errorContinueButton")
  public void onContinueButtonClick(final ClickEvent e) {
    errorContinueText.setInnerHTML(
        errorContinueButton.getValue() ? M.messages().importPanelContinueOnErrorYes() : M.messages().importPanelContinueOnErrorNo());
  }

  public void clearErrorList() {
    lineErrorListDiv.clear();
  }

  public void addErrorListEntry(final String entry) {
    final Label lineEntry = new Label(entry);
    lineEntry.setStyleName(style.errorListDivEntry());
    lineErrorListDiv.add(lineEntry);
  }

  public void hideCounterData() {
    counterLabel.setVisible(false);
    counterConflictLabel.setVisible(false);
  }

  public void showLineErrorDiv(final boolean show) {
    lineErrorDiv.setVisible(show);
    errorContinueButton.setValue(false);
    errorContinueText.setInnerHTML(M.messages().importPanelContinueOnErrorNo());
  }

  public void showFileField(final boolean show) {
    fileField.setVisible(show);
  }

  public void showSubstanceDiv(final boolean show) {
    substanceDiv.setVisible(show);
  }

  public void showConflictDiv(final boolean show) {
    conflictRadioButtonGroup.resetSelection();
    conflictLabel.setText(M.messages().importPanelConflict());
    conflictDiv.setVisible(show);
  }

  public void showUtilisationCheckBox(final boolean shouldShowUtilisationOptions) {
    importOptionsDiv.setVisible(shouldShowUtilisationOptions);
  }

  public boolean isErrorContinueButtonChecked() {
    return errorContinueButton.getValue();
  }

  public boolean isSubstanceDivVisible() {
    return substanceDiv.isVisible();
  }

  public boolean isConflictDivVisibile() {
    return conflictDiv.isVisible();
  }

  public boolean isFileUploadPresent() {
    return fileField.getFilename() != null && !fileField.getFilename().isEmpty();
  }

  public boolean isSubstanceValueSet() {
    return substanceRadioButtonGroup.getValue() != null;
  }

  public boolean isConflictValueSet() {
    return conflictRadioButtonGroup.getValue() != null;
  }

  public ImportConflictAction getConflictAction() {
    final ImportConflictAction selected = conflictRadioButtonGroup.getValue();
    //default is add.
    return selected == null ? ImportConflictAction.ADD : selected;
  }

  public void enableSubstanceRadioButtons(final boolean yes) {
    substanceRadioButtonGroup.setEnabled(yes);
  }
}
