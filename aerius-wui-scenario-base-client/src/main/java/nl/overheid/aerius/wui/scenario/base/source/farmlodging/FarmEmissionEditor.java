/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.domain.FarmUtil;
import nl.overheid.aerius.wui.main.event.ListCancelButtonEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseOPSEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.farmlodging.FarmEmissionInputEditor.FarmLodgingEmissionInputDriver;

public class FarmEmissionEditor extends BaseOPSEmissionSourceEditor<FarmEmissionSource> {

  public interface FarmLodgingEmissionDriver extends SimpleBeanEditorDriver<FarmEmissionSource, FarmEmissionEditor> { }

  interface FarmEmissionEditorEventBinder extends EventBinder<FarmEmissionEditor> { }

  interface FarmEmissionEditorUiBinder extends UiBinder<Widget, FarmEmissionEditor> { }

  enum FarmLodgingType {
    STANDARD,
    CUSTOM;

    /**
     * @return Returns the name in lowercase.
     */
    @Override
    public String toString() {
      return name().toLowerCase();
    }

  }

  class RadioButtonResources implements RadioButtonContentResource<FarmLodgingType> {
    @Override
    public String getRadioButtonText(final FarmLodgingType value) {
      final String buttonTitle;
      switch (value) {
      case CUSTOM:
        buttonTitle = M.messages().farmLodgingAddCustomLodge();
        break;
      case STANDARD:
        buttonTitle = M.messages().farmLodgingAddLodge();
        break;
      default:
        buttonTitle = null;
        break;
      }
      return buttonTitle;
    }

  }
  private static final FarmEmissionEditorUiBinder UI_BINDER = GWT.create(FarmEmissionEditorUiBinder.class);
  private final FarmEmissionEditorEventBinder eventBinder = GWT.create(FarmEmissionEditorEventBinder.class);
  @UiField FlowPanel panel;
  @Ignore @UiField(provided = true) RadioButtonGroup<FarmLodgingType> farmLodgingTypeSelection;

  InputListEmissionEditor<FarmLodgingEmissions, FarmEmissionInputEditor> emissionSubSourcesEditor;

  private Integer editId;
  private final EventBus eventBus;

  @SuppressWarnings("unchecked")
  public FarmEmissionEditor(final EventBus eventBus, final Context context, final SectorCategories categories, final CalculatorServiceAsync service,
      final HelpPopupController hpC) {
    super(eventBus, context, categories, hpC, M.messages().farmLodgingTotalAnimals());
    this.eventBus = eventBus;
    farmLodgingTypeSelection = new RadioButtonGroup<FarmLodgingType>(new RadioButtonResources());
    farmLodgingTypeSelection.ensureDebugId(TestID.FARM_TOGGLE_BUTTON);
    farmLodgingTypeSelection.addButtons(FarmLodgingType.values());

    addContentPanel(UI_BINDER.createAndBindUi(this));
    emissionSubSourcesEditor = new InputListEmissionEditor<FarmLodgingEmissions, FarmEmissionInputEditor>(
        eventBus,
        new FarmEmissionInputEditor(eventBus, categories, hpC),
        hpC,
        new FarmMessages(hpC),
        FarmLodgingEmissions.class,
        "Farm",
        (SimpleBeanEditorDriver<FarmLodgingEmissions, InputEditor<FarmLodgingEmissions>>) GWT.create(FarmLodgingEmissionInputDriver.class),
        service,
        true) {

      @Override
      protected FarmLodgingEmissions createNewRowEmissionValues() {
        return new FarmLodgingStandardEmissions();
      }

      @Override
      protected void addNew(final FarmLodgingEmissions value) {
        super.addNew(value);
        editId = value.getId();
      }

      @Override
      public void edit(final FarmLodgingEmissions value) {
        super.edit(value);
        editId = value.getId();
        if (value instanceof FarmLodgingStandardEmissions) {
          farmLodgingTypeSelection.setValue(FarmLodgingType.STANDARD);
        } else if (value instanceof FarmLodgingCustomEmissions) {
          farmLodgingTypeSelection.setValue(FarmLodgingType.CUSTOM);
        }
      }

      @Override
      public void showEditor(final boolean visible) {
        showFarmSelection(visible);
        super.showEditor(visible);
      }

      @Override
      protected String getLabel(final FarmLodgingEmissions value) {
        final String label;
        if (value instanceof FarmLodgingStandardEmissions) {
          label = M.messages().farmLodgingOverviewEntry(((FarmLodgingStandardEmissions) value).getCategory().getName(), value.getAmount());
        } else if (value instanceof FarmLodgingCustomEmissions) {
          label = ((FarmLodgingCustomEmissions) value).getDescription();
        } else {
          label = "";
        }
        return label;
      }

      @Override
      protected ImageResource getImage(final FarmLodgingEmissions value) {
        return FarmUtil.getFarmImageResource(value);
      }
    };
    addContentPanel(emissionSubSourcesEditor);
    showFarmSelection(false);

    emissionSubSourcesEditor.ensureDebugId(TestID.LODGING);
    panel.ensureDebugId(TestID.GUI_COLLAPSEPANEL_FARM);

  }

  private void showFarmSelection(final boolean show) {
    panel.setVisible(show);
  }

  @EventHandler
  void onCancelButton(final ListCancelButtonEvent event) {
    emissionSubSourcesEditor.onCancel();
  }

  @UiHandler("farmLodgingTypeSelection")
  void onButtonChange(final ValueChangeEvent<FarmLodgingType> event) {
    final FarmLodgingEmissions farmLodgingEmissions;
    if (event.getValue() == FarmLodgingType.STANDARD) {
      farmLodgingEmissions = new FarmLodgingStandardEmissions();
    } else if (event.getValue() == FarmLodgingType.CUSTOM) {
      farmLodgingEmissions = new FarmLodgingCustomEmissions();
    } else {
      farmLodgingEmissions = null;
    }
    if (farmLodgingEmissions != null) {
      farmLodgingEmissions.setId(editId);
      emissionSubSourcesEditor.edit(farmLodgingEmissions);
    }

  }

  @Override
  public void postSetValue(final FarmEmissionSource source) {
    eventBinder.bindEventHandlers(this, eventBus);
    super.postSetValue(source);
    emissionSubSourcesEditor.postSetValue(source);
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    super.setEmissionValueKey(key);
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
  }
}
