/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.processor;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultReady;
import nl.overheid.aerius.shared.service.CalculateServiceAsync;
import nl.overheid.aerius.wui.main.retrievers.PollingAgentImpl;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;

/**
 * Calculation Polling Agent.
 */
public class CalculationPollingAgent extends PollingAgentImpl<String, CalculationResultReady> {
  private static final int POLL_WAIT_TIME = 300;

  private final CalculateServiceAsync service;

  /**
   * Constructor where the {@link CalculateServiceAsync} is being injected.
   * @param service the service being injected
   * @param appContext context
   */
  @Inject
  public CalculationPollingAgent(final CalculateServiceAsync service, final ScenarioBaseAppContext<?, ?> appContext) {
    super(Math.max((int) appContext.getContext().getSetting(SharedConstantsEnum.CALCULATOR_POLLING_TIME), POLL_WAIT_TIME));
    this.service = service;
  }

  @Override
  protected void callService(final String key, final AsyncCallback<CalculationResultReady> resultCallback) {
    service.getCalculationResultReady(key, resultCallback);
  }
}
