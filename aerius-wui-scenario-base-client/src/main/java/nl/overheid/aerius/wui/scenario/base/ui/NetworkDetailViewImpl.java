/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.LabelledValue;
import nl.overheid.aerius.wui.scenario.base.ui.overview.NetworkCellTable;

/**
 * The view implementation for details of a road network.
 */
public class NetworkDetailViewImpl extends Composite implements NetworkDetailView, Editor<EmissionSource>, RequiresResize {

  private static final String EMPTY_VALUE = "-";
  private static final String MULTIPLE_DIVIDER_VALUE = " / ";

  interface NetworkDetailDriver extends SimpleBeanEditorDriver<EmissionSource, NetworkDetailViewImpl> {}

  private static NetworkDetailViewImplUiBinder uiBinder = GWT.create(NetworkDetailViewImplUiBinder.class);

  interface NetworkDetailViewImplUiBinder extends UiBinder<Widget, NetworkDetailViewImpl> {}

  @UiField FlowPanel noDataPanel;
  @UiField HTMLPanel networkCellPanel;
  @UiField NetworkCellTable networkCellTable;
  @UiField FlowPanel detailPanel;

  @UiField Label name;
  @UiField FlowPanel trafficPart;
  @UiField LabelledValue tunnelFactor;
  @UiField LabelledValue elevation;
  @UiField LabelledValue roadSideBarrier;
  @UiField LabelledValue barrierDistance;
  @UiField LabelledValue barrierHeight;
  @UiField LabelledValue barrierType;
  @UiField LabelledValue totalEmission;
  @UiField Button okButton;

  private Presenter presenter;
  private EmissionValueKey emissionValueKey;
  private final SingleSelectionModel<EmissionSource> selectionModel;

  @Inject
  public NetworkDetailViewImpl(final HelpPopupController hpC) {
    initWidget(uiBinder.createAndBindUi(this));
    selectionModel = new SingleSelectionModel<>();
    networkCellTable.setSelectionModel(selectionModel);
    selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final EmissionSource selected = selectionModel.getSelectedObject();
        if (selected != null) {
          presenter.selectedEmissionSource(selected);
        }
      }
    });
    networkCellTable.ensureDebugId(TestID.CELLTABLE_NETWORK);
  }

  @Override
  public void setData(final ArrayList<EmissionSource> values, final EmissionValueKey key) {
    this.emissionValueKey = key;
    networkCellTable.setEmissionValueKey(key);
    selectionModel.clear();
    networkCellTable.setRowData(values);
    showNoData(values.isEmpty());
    detailPanel.setVisible(false);
  }

  private void showNoData(final boolean visible) {
    noDataPanel.setVisible(visible);
    networkCellPanel.setVisible(!visible);
  }

  @Override
  public void setDetail(final EmissionSource source) {
    updateTotalEmission(source);
    trafficPart.clear();
    final SRM2EmissionSource srm2Values = (SRM2EmissionSource) source;
    for (final VehicleEmissions value : srm2Values.getEmissionSubSources()) {
      if (value instanceof VehicleStandardEmissions) {
        final VehicleStandardEmissions vehicleValue = (VehicleStandardEmissions) value;
        addTrafficValues(vehicleValue);
      }
    }
    name.setText(source.getLabel());

    tunnelFactor.setValue(FormatUtil.toFixed(srm2Values.getTunnelFactor(), 2));
    elevation.setValue(M.messages().roadElevation(srm2Values.getElevation(), srm2Values.getElevationHeight()));
    roadSideBarrier.setValue(M.messages().labelRoadSideBarrierValue());
    barrierType.setValue(
        getBarrierTypeString(srm2Values.getBarrierLeft()) + MULTIPLE_DIVIDER_VALUE + getBarrierTypeString(srm2Values.getBarrierRight()));
    barrierDistance.setValue(
        getBarrierDistanceString(srm2Values.getBarrierLeft()) + MULTIPLE_DIVIDER_VALUE + getBarrierDistanceString(srm2Values.getBarrierRight()));
    barrierHeight.setValue(
        getBarrierHeightString(srm2Values.getBarrierLeft()) + MULTIPLE_DIVIDER_VALUE + getBarrierHeightString(srm2Values.getBarrierRight()));
    detailPanel.setVisible(true);
  }

  private void addTrafficValues(final VehicleStandardEmissions vehicleValue) {
    final LabelledValue amountWidget = new LabelledValue();
    amountWidget.setLabel(M.messages().labelTraffic(vehicleValue.getEmissionCategory().getVehicleType()));
    amountWidget.setValue(M.messages().valuePerTimeUnit(vehicleValue.getTimeUnit(), FormatUtil.toFixed(vehicleValue.getVehiclesPerTimeUnit(), 0)));
    trafficPart.add(amountWidget);
    if (vehicleValue.getEmissionCategory().isSpeedRelevant()) {
      final String strictEnforcement;
      if (vehicleValue.getEmissionCategory().isStrictEnforcement()) {
        strictEnforcement = " " + M.messages().valueShortStrictEnforcement();
      } else {
        strictEnforcement = "";
      }
      final LabelledValue speedWidget = new LabelledValue();
      speedWidget.setLabel(M.messages().labelRoadSpeed());
      speedWidget.setValue(FormatUtil.formatSpeedWithUnit(vehicleValue.getEmissionCategory().getMaximumSpeed()) + strictEnforcement);
      trafficPart.add(speedWidget);
    }
    final LabelledValue percentageWidget = new LabelledValue();
    percentageWidget.setLabel(M.messages().labelPercentStagnated());
    percentageWidget.setValue(FormatUtil.formatFactorAsPercentageWithUnit(vehicleValue.getStagnationFraction()));
    trafficPart.add(percentageWidget);
  }

  private String getBarrierTypeString(final RoadSideBarrier roadSideBarrier) {
    return roadSideBarrier == null ? EMPTY_VALUE : M.messages().roadSideBarrierType(roadSideBarrier.getBarrierType());
  }

  private String getBarrierDistanceString(final RoadSideBarrier roadSideBarrier) {
    return roadSideBarrier == null ? EMPTY_VALUE : FormatUtil.formatDistanceMetersWithUnit(roadSideBarrier.getDistance());
  }

  private String getBarrierHeightString(final RoadSideBarrier roadSideBarrier) {
    return roadSideBarrier == null ? EMPTY_VALUE : FormatUtil.formatDistanceMetersWithUnit(roadSideBarrier.getHeight());
  }


  private void updateTotalEmission(final EmissionSource source) {
    totalEmission.setValue(FormatUtil.formatEmissionWithUnit(source.getEmission(emissionValueKey)));
  }

  @Override
  public void updateEmissionValueKey(final EmissionSource source, final EmissionValueKey key) {
    this.emissionValueKey = key;
    networkCellTable.setEmissionValueKey(key);
    networkCellTable.redraw();
    if (detailPanel.isVisible()) {
      updateTotalEmission(source);
    }
  }

  @UiHandler("okButton")
  void onClickokButton(final ClickEvent e) {
    presenter.okButton();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public void setOkButtonVisible(final boolean visible) {
    okButton.setVisible(visible);
  }

  @Override
  public void setNoData(final boolean loading) {
    // show loading

  }

  @Override
  public void onResize() {
    // no-op
  }

  @Override
  public String getTitleText() {
    return M.messages().networkEmissionSource();
  }

}
