/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.dom.client.DragEnterHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.ui.ApplicationDisplay;
import nl.overheid.aerius.wui.main.widget.HeadingWidget;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.main.widget.progress.ProgressStatusWidget;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.ResultHighValuesByDistancesEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Main View for the Scenario application. Contains left panel + top buttons and map.
 */
public abstract class ScenarioBaseViewImpl extends Composite
    implements ApplicationDisplay, AcceptsOneWidget, DragEnterHandler, HasTitle, ScenarioBaseView {

  interface ScenarioBaseViewImplUiBinder extends UiBinder<Widget, ScenarioBaseViewImpl> {}

  private static final ScenarioBaseViewImplUiBinder uiBinder = GWT.create(ScenarioBaseViewImplUiBinder.class);

  interface ScenarioBaseViewImplEventBinder extends EventBinder<ScenarioBaseViewImpl> {}

  private static final ScenarioBaseViewImplEventBinder eventBinder = GWT.create(ScenarioBaseViewImplEventBinder.class);

  @UiField SwitchPanel switchPanel;

  @UiField SimplePanel westContentPanel;
  @UiField SimplePanel fullPagePanel;

  @UiField(provided = true) Widget fullPageNotificationPanel;

  @UiField ProgressStatusWidget progressStatusWidget;
  @UiField HeadingWidget titleHeaderFull;
  @UiField HeadingWidget titleHeaderWest;
  @UiField HeadingWidget subTitleHeader;
  @UiField(provided = true) ScenarioBaseTaskBar taskBar;
  @UiField(provided = true) ScenarioBaseMapLayoutPanel map;

  protected ScenarioBaseTheme theme;
  protected final ScenarioBaseAppContext<?, ?> appContext;
  private final ScenarioBaseImportController importController;

  public ScenarioBaseViewImpl(final ScenarioBaseAppContext<?, ?> appContext, final EventBus eventBus, final ScenarioBaseTaskBar taskBar,
      final ScenarioBaseMapLayoutPanel map, final ScenarioBaseImportController importController, final Widget fullPageNotificationPanel) {
    this.appContext = appContext;
    this.taskBar = taskBar;
    this.map = map;
    this.importController = importController;
    this.fullPageNotificationPanel = fullPageNotificationPanel;

    eventBinder.bindEventHandlers(this, eventBus);

    initWidget(uiBinder.createAndBindUi(this));

    final String titleText = getTitleText();
    final String titleColor = getTitleColor();

    titleHeaderWest.setText(titleText);
    titleHeaderWest.setStyleName(titleColor, true);
    titleHeaderFull.setText(titleText);
    titleHeaderFull.setStyleName(titleColor, true);

    addDomHandler(this, DragEnterEvent.getType());
  }

  protected abstract String getTitleColor();

  @Override
  public void initEmissionValueKey(final EmissionValueKey emissionValueKey) {
    taskBar.setYearCaption(emissionValueKey.getYear());
  }

  @Override
  public void setWidget(final IsWidget w) {
    if (w == null) {
      return;
    }

    if (w instanceof HasTitle) {
      subTitleHeader.setText(((HasTitle) w).getTitleText());
    }

    // From the pot in the kettle. Do it again, because the eventual widget may not be known at this time. FIXME
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        if (w instanceof HasTitle) {
          subTitleHeader.setText(((HasTitle) w).getTitleText());
        }
      }
    });

    final boolean fullPage = w instanceof FullPageView;

    final AcceptsOneWidget contentPanel = fullPage ? fullPagePanel : westContentPanel;
    contentPanel.setWidget(w.asWidget());
    switchPanel.showWidget(fullPage ? 1 : 0);
  }

  @Override
  public void onDragEnter(final DragEnterEvent event) {
    event.preventDefault();
    event.stopPropagation();
    importController.showImportDialog(true);
  }

  @EventHandler
  void onStartCalculation(final CalculationStartEvent event) {
    progressStatusWidget.start();
    progressStatusWidget.setVisible(true);
  }

  @EventHandler
  void onCancelCalculation(final CalculationCancelEvent event) {
    progressStatusWidget.stop();
  }

  @EventHandler
  void onCalculationFinish(final CalculationFinishEvent event) {
    progressStatusWidget.stop();
  }

  @EventHandler
  void onResultHighValuesByDistancesChanged(final ResultHighValuesByDistancesEvent event) {
    progressStatusWidget.updateState(event.getValue().getNumberOfResults());
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent event) {
    if (event.getValue() instanceof ScenarioBasePlace) {
      final ScenarioBasePlace cp = (ScenarioBasePlace) event.getValue();
      progressStatusWidget.showDetails(cp instanceof ResultPlace);
      if (cp.getTheme() != this.theme) {
        this.theme = cp.getTheme();
        appContext.initTheme(theme);
        final ScenarioBaseUserContext uc = appContext.getUserContext();
        final EmissionResultKey erk = theme.getThemeValidEmissionResultKey(uc.getEmissionResultKey());
        uc.setEmissionResultKey(erk);
        taskBar.setTheme(theme, erk.getEmissionResultType());
      }
    }
  }

  @Override
  public void onResize() {
    // No-op for calculator, flexbox fixed it.
  }
}
