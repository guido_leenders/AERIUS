/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.ArrayList;

import nl.overheid.aerius.shared.domain.ColorRange;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultColorRanges;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar.ValueRange;
import nl.overheid.aerius.wui.scenario.base.geo.VectorCalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.ui.overview.ReceptorPointDistributionUtil.RangeDistributor;

/**
 *
 */
public class HabitatSituationFilter extends AbstractScenarioBaseHabitatFilter<ColorRange> {

  //static because it's the cache for this particular filter. No negative nrs so lower starts at 0.
  private static IndexCache indexCache = new IndexCache(0, Integer.MAX_VALUE);

  /**
   * @param layer see {@link AbstractScenarioBaseHabitatFilter}
   * @param emissionResultKey see {@link AbstractScenarioBaseHabitatFilter}
   */
  public HabitatSituationFilter(final VectorCalculationResultLayerWrapper layer, final EmissionResultKey emissionResultKey) {
    super(layer, emissionResultKey, indexCache);
  }

  @Override
  protected RangeDistributor<ColorRange> getRangeDistributor() {
    return new RangeDistributor<ColorRange>() {

      @Override
      public ArrayList<ColorRange> getPossibleRanges() {
        return getCurrentColorRanges().getRanges();
      }

      @Override
      public ColorRange getRange(final AeriusResultPoint rp) {
        return getCurrentColorRanges().getRange(rp.getEmissionResult(getEmissionResultKey()));
      }
    };
  }

  @Override
  public int size() {
    return getCurrentColorRanges().getRanges().size();
  }

  private EmissionResultColorRanges getCurrentColorRanges() {
    return EmissionResultColorRanges.valueOf(getEmissionResultKey().getEmissionResultType());
  }

  @Override
  double determineUpperLimit(final ValueRange value) {
    final int lowerValue = value.getLowerValue() - 1;
    double upperLimit = Integer.MAX_VALUE;
    if (lowerValue >= 0) {
      final ColorRange lowerRange = getCurrentColorRanges().getRanges().get(size() - 1 - lowerValue);
      upperLimit = lowerRange.getLowerValue();
    }
    return upperLimit;
  }

  @Override
  double determineLowerLimit(final ValueRange value) {
    final int upperValue = Math.max(0, value.getUpperValue() - 1);
    final ColorRange upperRange = getCurrentColorRanges().getRanges().get(size() - 1 - upperValue);
    return upperRange.getLowerValue();
  }

  @Override
  protected int getIndex(final double val) {
    if (val == Integer.MAX_VALUE) {
      return 0;
    }
    return size() - (val == 0 ? 0 : getCurrentColorRanges().getIndex(Math.max(0, val)));
  }
}
