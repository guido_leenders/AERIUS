/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.progress;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.ComputedStyle;

/**
 * Widget to display the number of ha calculated.
 */
public class ProgressStatusWidget extends Composite {

  interface ProgressStatusWidgetUiBinder extends UiBinder<Widget, ProgressStatusWidget> { }

  private static final ProgressStatusWidgetUiBinder UI_BINDER = GWT.create(ProgressStatusWidgetUiBinder.class);

  private static final double MILISECONDS_TO_SECONDS = 1000d;
  private static final int ANIMATION_MAX_DELAY = 10;
  private static final double ANIMATION_MIN_DELAY = 0.5;
  private static final String SECOND_POSTFIX = "s";
  private static final int DELAY = 150;
  private static final double DELAY_SECOND = MILISECONDS_TO_SECONDS / DELAY;
  private static final int UPDATE_CALL_DELAY = 50;

  @UiField FlowPanel panel;
  @UiField Label numberOfHaSpan;
  @UiField(provided = true) Image waitingImage;

  private int numberOfHa;
  private double guessedNumberOfHa;
  private double delta;
  private long startTime;
  private long previousTime;

  private final Timer timer;
  private final Timer delayTimer;

  /**
   * Create widget.
   */
  public ProgressStatusWidget() {
    waitingImage = new Image(R.images().waiting());
    waitingImage.ensureDebugId(TestID.TURBOTURBO);
    initWidget(UI_BINDER.createAndBindUi(this));
    timer = new Timer() {
      @Override
      public void run() {
        guessedNumberOfHa += delta;
        setState((int) Math.round(guessedNumberOfHa));
      }
    };

    delayTimer = new Timer() {
      @Override
      public void run() {
        final double timeDiff = (new Date().getTime() - startTime) / MILISECONDS_TO_SECONDS;
        delta = numberOfHa / timeDiff / DELAY_SECOND;
        final double animationDuration = Math.min(Math.max(ANIMATION_MIN_DELAY, ANIMATION_MAX_DELAY / delta), ANIMATION_MAX_DELAY);
        setWaitingImage(animationDuration + SECOND_POSTFIX);
        ComputedStyle.forceStyleRender(waitingImage);

        guessedNumberOfHa = numberOfHa;
        setState(numberOfHa);
        previousTime = new Date().getTime();
      }
    };
  }

  /**
   * Starts the counter.
   */
  public void start() {
    reset();
    previousTime = new Date().getTime();
    startTime = previousTime;
    numberOfHa = 0;
    delta = 0;
    guessedNumberOfHa = 0;
    timer.scheduleRepeating(DELAY);
    panel.setVisible(true);
  }

  private void reset() {
    timer.cancel();
    delayTimer.cancel();
    setWaitingImage(null);
  }

  /**
   * Shows the details of the process.
   * @param visible if true shows details
   */
  public void showDetails(final boolean visible) {
    numberOfHaSpan.setVisible(visible);
  }

  /**
   * Stops the counter.
   */
  public void stop() {
    reset();
    setState(numberOfHa);
    panel.setVisible(false);
  }

  /**
   * Updates the state with the total number of Ha calculated.
   *
   * @param numberOfHa total number of ha calculated
   */
  public void updateState(final int numberOfHa) {
    this.numberOfHa = numberOfHa;
    delayTimer.cancel();
    delayTimer.schedule(UPDATE_CALL_DELAY);
  }

  private void setState(final int actualValue) {
    numberOfHaSpan.setText(M.messages().calculationResultHaCalculated(actualValue));
  }

  private void setWaitingImage(final String animationDuration) {
    waitingImage.getElement().getStyle().setProperty("animationDuration", animationDuration);
  }
}
