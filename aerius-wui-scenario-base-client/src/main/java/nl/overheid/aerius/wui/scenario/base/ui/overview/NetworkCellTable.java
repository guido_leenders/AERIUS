/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.List;

import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safecss.shared.SafeStylesUtils;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;

public class NetworkCellTable extends CellTable<EmissionSource> {
  static Templates templates = GWT.create(Templates.class);

  // total 320
  private static final String COLUMN_WIDTH_ICON = "28px";
  private static final String COLUMN_WIDTH_ROAD = "110px";
  private static final String COLUMN_WIDTH_LENGTH = "82px";
  private static final String COLUMN_WIDTH_EMISSION_RESULT = "100px";
  private static final String ROW_HEIGHT = "30px";

  private final SingleSelectionModel<EmissionSource> selectionModel = new SingleSelectionModel<EmissionSource>();

  private EmissionValueKey key;

  /**
   * The HTML templates used to render the cell.
   */
  interface Templates extends SafeHtmlTemplates {
    /**
     * The template for this Cell, which includes styles and a value.
     *
     * @param styles the styles to include in the style attribute of the div
     * @param value the safe value. Since the value type is {@link SafeHtml},
     *          it will not be escaped before including it in the template.
     *          Alternatively, you could make the value type String, in which
     *          case the value would be escaped.
     * @return a {@link SafeHtml} instance
     */
    @SafeHtmlTemplates.Template("<div class=\"{0}\" style=\"{1}\" title=\"{3}\">{2}</div>")
    SafeHtml cell(String className, SafeStyles style, SafeHtml value, String title);
    /**
     * The template for this Cell, which includes styles and a value.
     *
     * @param styles the styles to include in the style attribute of the div
     * @param value the safe value. Since the value type is {@link SafeHtml},
     *          it will not be escaped before including it in the template.
     *          Alternatively, you could make the value type String, in which
     *          case the value would be escaped.
     * @return a {@link SafeHtml} instance
     */
    @SafeHtmlTemplates.Template("<div class=\"{0}\">{1}</div>")
    SafeHtml cell(String className, SafeHtml value);
  }

  @Inject
  public NetworkCellTable() {
    super(10, R.cellTableResources(), new ProvidesKey<EmissionSource>() {
      @Override
      public Object getKey(final EmissionSource item) {
        return item.getId();
      }
    });
    setSelectionModel(selectionModel);
    setWidth("100%", false);

    // Sector icon column
    final Column<EmissionSource, ImageResource> columnIcon =
        new Column<EmissionSource, ImageResource>(new ImageResourceCell()) {
          @Override
          public ImageResource getValue(final EmissionSource object) {
            return SectorImageUtil.getSectorImageResource(object.getSector().getProperties().getIcon());
          }
        };
    addColumn(columnIcon);
    setColumnWidth(columnIcon, COLUMN_WIDTH_ICON);

    // Row number column
    final TextColumn<EmissionSource> columnRoad = new TextColumn<EmissionSource>() {
      @Override
      public void render(final com.google.gwt.cell.client.Cell.Context context, final EmissionSource value,
          final com.google.gwt.safehtml.shared.SafeHtmlBuilder sb) {
        final SafeStyles bgc = SafeStylesUtils.fromTrustedString("padding-left: 10px;line-height:"
            + ROW_HEIGHT + ";width:" + COLUMN_WIDTH_ROAD + ";");
        sb.append(templates.cell(R.css().textOverflowEllipsis(), bgc, SafeHtmlUtils.fromString(value.getLabel()), value.getLabel()));
      }

      @Override
      public String getValue(final EmissionSource emissionSource) {
        final String value = emissionSource.getLabel();
        return value;
      }
    };
    addColumn(columnRoad);
    setColumnWidth(columnRoad, COLUMN_WIDTH_ROAD);
    columnRoad.setCellStyleNames(R.css().cursorPointer());

    // length column
    final TextColumn<EmissionSource> columnLength = new TextColumn<EmissionSource>() {
      @Override
      public String getValue(final EmissionSource emissionSource) {
        final double value = emissionSource.getGeometry().getMeasure();
        return FormatUtil.formatDistanceMetersWithUnit(value);
      }
    };
    addColumn(columnLength);
    setColumnWidth(columnLength, COLUMN_WIDTH_LENGTH);
    columnLength.setCellStyleNames(R.css().sourcesListEmissionColumn() + " " + R.css().number() + " " + R.css().cursorPointer());

    // emission value
    final TextColumn<EmissionSource> columnEmissionResult = new TextColumn<EmissionSource>() {
      @Override
      public String getValue(final EmissionSource emissionSource) {
        return FormatUtil.formatEmissionWithUnit(emissionSource.getEmission(key));
      }
    };

    addColumn(columnEmissionResult);
    setColumnWidth(columnEmissionResult, COLUMN_WIDTH_EMISSION_RESULT);
    columnEmissionResult.setCellStyleNames(R.css().sourcesListEmissionColumn() + " " + R.css().number() + " " + R.css().cursorPointer());

    setVisible(false);
  }

  @Override
  public void setRowData(final int start, final List<? extends EmissionSource> values) {
    super.setRowData(start, values);
    setVisible(!values.isEmpty());
  }

  public void setEmissionValueKey(final EmissionValueKey key) {
    this.key = key;
  }

}

