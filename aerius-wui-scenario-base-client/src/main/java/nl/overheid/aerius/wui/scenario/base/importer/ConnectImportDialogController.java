/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.importer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.importer.ImportDialogController;
import nl.overheid.aerius.wui.main.ui.importer.ImportProperties;
import nl.overheid.aerius.wui.main.widget.event.CancelEvent;

/**
 * Controller for import dialog.
 */
public abstract class ConnectImportDialogController extends ImportDialogController<ImportProperties, ConnectImportDialogPanel, ImportResult> {
  private static final String UPLOAD_ACTION = GWT.getModuleBaseURL() + SharedConstants.IMPORT_CONNECT_CALCULATION_SERVLET;

  /**
   * Creates controller for importing sources.
   *
   * @param originalList list of current list of sources, can be null
   * @param originalCustomCalculationPoints
   * @param service
   * @param content
   */
  public ConnectImportDialogController(final ConnectImportDialogPanel content) {
    super(content, UPLOAD_ACTION);
  }

  @Override
  protected void submitClick() {
    // Nothing here
  }

  @Override
  public void onSubmit(final SubmitEvent event) {
    super.onSubmit(event);
    if (isInProgress()) {
      return;
    }

    setInProgress(true);
  }

  @Override
  public void onCancel(final CancelEvent event) {
    setInProgress(false);
  }

  @Override
  public void onFailure(final Throwable error) {
    setInProgress(false);
    content.showError(error == null ? null : M.getErrorMessage(error));
  }

  @Override
  public void onSuccess(final ImportResult result) {
    setInProgress(false);
  }
}
