/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.service.InfoServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultFilterPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class ResultFilterPresenter extends ResultWithSituationActivity implements ResultFilterView.Presenter {
  interface ResultFilterPresenterEventBinder extends EventBinder<ResultFilterPresenter> { }

  private final ResultFilterPresenterEventBinder eventBinder = GWT.create(ResultFilterPresenterEventBinder.class);

  private final ResultFilterView view;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final ResultFilterPlace place;
  private final InfoServiceAsync infoService;

  @Inject
  public ResultFilterPresenter(final PlaceController placeController, final CalculationController calculatorController,
      final ScenarioBaseAppContext<?, ?> appContext, @Assisted final ResultFilterPlace place,
      final SituationView sitView, final ResultView resultView, final ResultFilterView view, final ScenarioBaseMapLayoutPanel map,
      final InfoServiceAsync infoService) {
    super(placeController, calculatorController, appContext, place, sitView, resultView, ResultPlace.ScenarioBaseResultPlaceState.FILTER, map);
    this.appContext = appContext;
    this.place = place;
    this.view = view;
    this.infoService = infoService;
  }

  @Override
  public void startSituationChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    eventBinder.bindEventHandlers(this, eventBus);
    view.setPresenter(this);
    panel.setWidget(view);
  }

  @Override
  public void getHabitates(final int areaId, final int habitatId, final AsyncCallback<HashMap<Integer, Double>> receptorCallback) {
    final CalculatedScenario calcScenario = appContext.getUserContext().getCalculatedScenario();
    final int activeCalculationId = calcScenario == null ? -1 : calcScenario.getCalculationId(place.getActiveSituationId());
    if (activeCalculationId >= 0) {
      infoService.getCalculationAssessmentHabitatReceptors(activeCalculationId, areaId, habitatId, receptorCallback);
    }
  }

  @Override
  protected void refresh(final Situation sit, final EmissionValueKey key, final EmissionResultKey resultKey) {
    final CalculatedScenario calcScenario = appContext.getUserContext().getCalculatedScenario();
    final String noResultText = getNoResultText(place, sit, key, resultKey, calcScenario);
    final CalculationSummary summary = appContext.getCalculationSummary();
    if (noResultText == null) {
      if (sit == Situation.COMPARISON) {
        final int calcSitOne = calcScenario.getCalculationId(place.getSid1());
        final int calcSitTwo = calcScenario.getCalculationId(place.getSid2());
        view.setValue(sit, resultKey, summary, calcSitOne, calcSitTwo);
      } else {
        final int activeCalculationId = calcScenario.getCalculationId(place.getActiveSituationId());
        view.setValue(sit, resultKey, summary, activeCalculationId, -1);
      }
    } else {
      view.setNoData(appContext.isCalculationRunning() || summary == null, noResultText);
    }
  }

  @Override
  protected String getNoResultText(final ScenarioBasePlace place, final Situation sit, final EmissionValueKey key,
      final EmissionResultKey resultKey, final CalculatedScenario calcScenario) {
    final String noResults;
    final String superNoResults = super.getNoResultText(place, sit, key, resultKey, calcScenario);
    if (superNoResults == null && calcScenario.getOptions().getCalculationType() == CalculationType.CUSTOM_POINTS) {
      noResults = M.messages().calculationFilterNoCustomPoints();
    } else {
      noResults = superNoResults;
    }
    return noResults;
  }

  @Override
  public void onStop() {
    super.setVisibleHabitatLayer(false);
    super.onStop();
  }
}
