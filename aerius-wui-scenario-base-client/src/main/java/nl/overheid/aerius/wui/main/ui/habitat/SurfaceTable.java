/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.habitat;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.HabitatInfo.HabitatSurfaceType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

class SurfaceTable extends Composite {

  private static MonitorSurfaceTableBinder uiBinder = GWT.create(MonitorSurfaceTableBinder.class);

  private static final int SURFACE_NUM_DECIMALS = 1;

  interface MonitorSurfaceTableBinder extends UiBinder<Widget, SurfaceTable> {
  }

  @UiField Label surfaceTotalMapped;
  @UiField Label surfaceTotalCartographic;
  @UiField Label surfaceRelevantMapped;
  @UiField Label surfaceRelevantCartographic;

  @Inject
  public SurfaceTable(final HelpPopupController hpC) {
    initWidget(uiBinder.createAndBindUi(this));

    hpC.addWidget(surfaceTotalMapped, hpC.tt().ttHabitatsTableSurfaceTitleTotalMapped());
    hpC.addWidget(surfaceTotalCartographic, hpC.tt().ttHabitatsTableSurfaceTitleTotalCartographic());
    hpC.addWidget(surfaceRelevantMapped, hpC.tt().ttHabitatsTableSurfaceTitleRelevantMapped());
    hpC.addWidget(surfaceRelevantCartographic, hpC.tt().ttHabitatsTableSurfaceTitleRelevantCartographic());

    surfaceTotalMapped.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_SURFACE_TOTAL_MAPPED);
    surfaceTotalCartographic.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_SURFACE_TOTAL_CARTOGRAPHIC);
    surfaceRelevantMapped.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_SURFACE_RELEVANT_MAPPED);
    surfaceRelevantCartographic.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_SURFACE_RELEVANT_CARTOGRAPHIC);
  }

  public void setSurfaces(final HabitatInfo habitatInfo) {
    surfaceTotalMapped.setText(M.messages().unitSurface(FormatUtil.toFixed(habitatInfo.getAdditionalSurface(
        HabitatSurfaceType.TOTAL_MAPPED) / SharedConstants.M2_TO_HA, SURFACE_NUM_DECIMALS)));
    surfaceTotalCartographic.setText(M.messages().unitSurface(FormatUtil.toFixed(habitatInfo.getAdditionalSurface(
        HabitatSurfaceType.TOTAL_CARTOGRAPHIC) / SharedConstants.M2_TO_HA, SURFACE_NUM_DECIMALS)));
    surfaceRelevantMapped.setText(M.messages().unitSurface(FormatUtil.toFixed(habitatInfo.getAdditionalSurface(
        HabitatSurfaceType.RELEVANT_MAPPED) / SharedConstants.M2_TO_HA, SURFACE_NUM_DECIMALS)));
    surfaceRelevantCartographic.setText(M.messages().unitSurface(FormatUtil.toFixed(habitatInfo.getAdditionalSurface(
        HabitatSurfaceType.RELEVANT_CARTOGRAPHIC) / SharedConstants.M2_TO_HA, SURFACE_NUM_DECIMALS)));

    surfaceTotalCartographic.setTitle(M.messages().habitatsTableCoveragePercent(
        FormatUtil.formatPercentageWithUnit(habitatInfo.getCoverage() * SharedConstants.PERCENTAGE_TO_FRACTION)));
    surfaceRelevantCartographic.setTitle(M.messages().habitatsTableCoveragePercent(
        FormatUtil.formatPercentageWithUnit(habitatInfo.getCoverageRelevant() * SharedConstants.PERCENTAGE_TO_FRACTION)));
  }
}
