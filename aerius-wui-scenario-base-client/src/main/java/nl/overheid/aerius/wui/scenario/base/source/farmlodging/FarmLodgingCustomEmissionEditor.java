/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;

/**
 * Editor for FarmLodgingCustomEmissions object.
 */
class FarmLodgingCustomEmissionEditor extends Composite implements InputEditor<FarmLodgingCustomEmissions> {

  private static final FarmStandardEmissionLodgingCustomEditorUiBinder UI_BINDER = GWT.create(FarmStandardEmissionLodgingCustomEditorUiBinder.class);

  public interface FarmLodgingCustomEmissionDriver extends SimpleBeanEditorDriver<FarmLodgingCustomEmissions, FarmLodgingCustomEmissionEditor> { }

  interface FarmStandardEmissionLodgingCustomEditorUiBinder extends UiBinder<Widget, FarmLodgingCustomEmissionEditor> { }

  @UiField @Ignore TextBox descriptionTextBox;
  @UiField DoubleValueBox emissionFactorEditor;
  @UiField IntValueBox amountEditor;
  final ValidatedValueBoxEditor<String> descriptionEditor;

  public FarmLodgingCustomEmissionEditor(final EventBus eventBus, final SectorCategories categories, final HelpPopupController hpC) {
    initWidget(UI_BINDER.createAndBindUi(this));

    hpC.addWidget(descriptionTextBox, hpC.tt().ttFarmLodgingCustomLodgeDescription());
    hpC.addWidget(emissionFactorEditor, hpC.tt().ttFarmLodgingCustomLodgeEmissionFactor());
    hpC.addWidget(amountEditor, hpC.tt().ttFarmLodgingAmount());

    descriptionEditor = new ValidatedValueBoxEditor<String>(descriptionTextBox, M.messages().farmLodgingDescription()) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().ivFarmCustomNoEmptyDescription();
      }
    };
    descriptionEditor.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().ivFarmCustomNoEmptyDescription();
      }
    });

    descriptionTextBox.ensureDebugId(TestID.CUSTOMLODGING + "-" + TestID.DESCRIPTION_BOX);
    emissionFactorEditor.ensureDebugId(TestID.CUSTOMLODGING + "-" + TestID.FACTOR_BOX);
    amountEditor.ensureDebugId(TestID.CUSTOMLODGING + "-" + TestID.AMOUNT_BOX);
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return null;
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    // no-op
  }

  @Override
  public void resetPlaceholders() {
    emissionFactorEditor.resetPlaceHolder();
    amountEditor.resetPlaceHolder();
  }
}
