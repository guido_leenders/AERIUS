/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.table.SimpleContextualContent;

public class ConciseEmissionValuesWidget extends SimpleContextualContent<EmissionSource> {
  private final OPSSourceCharacteristicsViewer characteristics;
  private final EmissionSourceViewer viewer;
  private final EmissionSourceViewWrapper wrapper;
  private final UserContext userContext;

  @Inject
  public ConciseEmissionValuesWidget(final EmissionSourceViewWrapper wrapper, final EmissionSourceViewer viewer,
      final OPSSourceCharacteristicsViewer characteristics, final UserContext userContext) {
    super();

    this.wrapper = wrapper;
    this.viewer = viewer;
    this.characteristics = characteristics;
    this.userContext = userContext;

    initContent(wrapper);
  }

  /**
   * Returns true if the source can be previewed, false if it cannot.
   *
   * Will be phased out later on, while this component is under development it may return false sometimes
   */
  @Override
  public boolean view(final EmissionSource obj) {
    final Sector sector = obj.getSector();

    // TODO The Arrow doesn't align when animating the top position.
    // getElement().getStyle().setProperty("transition", "top 0.15s ease-out");

    // Set the sector (title)
    setTitleText(M.messages().emissionSourceViewerSectorTitle(M.messages().sectorGroup(sector.getSectorGroup()), sector.getName()));

    // Set the emission values
    viewer.setYear(userContext.getEmissionValueKey().getYear());
    viewer.setValue(obj);

    // Configure source/sector specific visibility
    if ((obj instanceof HasOPSSourceCharacteristics) && !(obj instanceof SRM2EmissionSource)) {
      final boolean pointGeo = obj.getGeometry().getType() == WKTGeometry.TYPE.POINT;
      final boolean polygonGeo = obj.getGeometry().getType() == WKTGeometry.TYPE.POLYGON;
      characteristics.setBuildingHeightVisible(pointGeo && sector.getProperties().isBuildingPossible());
      characteristics.setSpreadVisible(polygonGeo);
      characteristics.setDiurnalVariationVisible(obj instanceof GenericEmissionSource);
      characteristics.setParticleSizeDistributionVisible(false); // NO PM10
      // Set the source characteristics value
      characteristics.setValue(obj.getSourceCharacteristics());

      wrapper.setCharacteristics(characteristics);
    } else {
      characteristics.removeFromParent();
    }

    // Set the emission values
    final Widget widg = viewer.asWidget();
    wrapper.setEmissionValues(widg);

    // This method will return void when all value types are supported.
    return widg != null;
  }
}
