/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.plan;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseOPSEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.plan.PlanInputEditor.PlanInputDriver;

/**
 * Editor for {@link PlanEmissionValues}.
 */
public class PlanEmissionEditor extends BaseOPSEmissionSourceEditor<PlanEmissionSource> {

  public interface PlanEmissionDriver extends SimpleBeanEditorDriver<PlanEmissionSource, PlanEmissionEditor> {
  }

  private static class PlanMessages implements EditableDivTableMessages {
    private final HelpPopupController hpC;

    public PlanMessages(final HelpPopupController hpC) {
      this.hpC = hpC;
    }

    @Override
    public String add() {
      return M.messages().planSourceAddPlan();
    }

    @Override
    public HelpInfo editHelp() {
      return hpC.tt().ttPlanEdit();
    }

    @Override
    public HelpInfo copyHelp() {
      return hpC.tt().ttPlanCopy();
    }

    @Override
    public HelpInfo deleteHelp() {
      return hpC.tt().ttPlanDelete();
    }

    @Override
    public HelpInfo addHelp() {
      return hpC.tt().ttPlanAdd();
    }

    @Override
    public String customButtonText() {
      return null;
    }

    @Override
    public HelpInfo customButtonHelp() {
      return null;
    }
  }

  InputListEmissionEditor<PlanEmission, PlanInputEditor> emissionSubSourcesEditor;

  @SuppressWarnings("unchecked")
  public PlanEmissionEditor(final EventBus eventBus, final Context context, final SectorCategories categories, final CalculatorServiceAsync service,
      final HelpPopupController hpC) {
    super(eventBus, context, categories, hpC, M.messages().planEmissionsTitle());
    emissionSubSourcesEditor = new InputListEmissionEditor<PlanEmission, PlanInputEditor>(
        eventBus, new PlanInputEditor(categories, hpC), hpC, new PlanMessages(hpC),
        PlanEmission.class, "Plan", (SimpleBeanEditorDriver<PlanEmission, InputEditor<PlanEmission>>) GWT.create(PlanInputDriver.class), service,
        false) {

      @Override
      protected PlanEmission createNewRowEmissionValues() {
        return new PlanEmission();
      }
    };
    addContentPanel(emissionSubSourcesEditor);
    emissionSubSourcesEditor.ensureDebugId(TestID.DIVTABLE_EDITABLE);
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    super.setEmissionValueKey(key);
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  public void postSetValue(final PlanEmissionSource source) {
    super.postSetValue(source);
    emissionSubSourcesEditor.postSetValue(source);
  }
}
