/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.NameTextMachine;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;

/**
 * Editor for Specific Vehicle entries.
 */
class VehicleSpecificEditor extends AbstractVehicleEditor<VehicleSpecificEmissions> {

  interface VehicleSpecificDriver extends SimpleBeanEditorDriver<VehicleSpecificEmissions, VehicleSpecificEditor> {
  }

  interface VehicleSpecificEditorUiBinder extends UiBinder<Widget, VehicleSpecificEditor> {
  }

  private static final VehicleSpecificEditorUiBinder UI_BINDER = GWT.create(VehicleSpecificEditorUiBinder.class);

  private static final TextMachine<OnRoadMobileSourceCategory> MOBILE_CATEGORY_NAMER = new NameTextMachine<OnRoadMobileSourceCategory>() {
    @Override
    public String asError(final String input) {
      return M.messages().ivRoadCategoryNotSelected();
    }
  };

  @UiField(provided = true) SuggestBox<OnRoadMobileSourceCategory> categoryEditor;

  public VehicleSpecificEditor(final ArrayList<OnRoadMobileSourceCategory> categories, final HelpPopupController hpC) {
    categoryEditor = new SuggestBox<OnRoadMobileSourceCategory>(new MobileSourceCategorySuggestOracle(categories), MOBILE_CATEGORY_NAMER);
    categoryEditor.setListButtonVisible(true);
    categoryEditor.getValueBox().addStyleName(R.css().mobileEmissionListBox());
    categoryEditor.addStyleName(R.css().mobileEmissionListBoxContainer());
    initWidget(UI_BINDER.createAndBindUi(this));

    afterBinding(hpC);

    // Help widgets
    hpC.addWidget(categoryEditor, hpC.tt().ttMobileSourceCategory());
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    categoryEditor.getValueBox().ensureDebugId(TestID.INPUT_MOBILESOURCE_CATEGORY);
  }
}
