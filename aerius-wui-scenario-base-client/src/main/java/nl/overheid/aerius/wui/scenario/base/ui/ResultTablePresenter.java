/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.wui.main.event.HabitatClearEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultTablePlace;

/**
 * Presenter for displaying calculation results in a table.
 */
public class ResultTablePresenter extends ResultWithSituationActivity {

  interface ResultTableEventBinder extends EventBinder<ResultTablePresenter> {}

  private final ResultTableEventBinder eventBinder = GWT.create(ResultTableEventBinder.class);

  private final ScenarioBaseAppContext<?, ?> appContext;
  private final ResultTableView view;
  private final ResultTablePlace place;

  private EventBus eventBus;

  @Inject
  public ResultTablePresenter(final PlaceController placeController, final CalculationController calculatorController,
      final ScenarioBaseAppContext<?, ?> appContext, @Assisted final ResultTablePlace place, final SituationView sitView, final ResultView resultView,
      final ResultTableView view, final ScenarioBaseMapLayoutPanel map) {
    super(placeController, calculatorController, appContext, place, sitView, resultView, ResultPlace.ScenarioBaseResultPlaceState.TABLE, map);
    this.appContext = appContext;
    this.place = place;
    this.view = view;
  }

  @Override
  public void startSituationChild(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;

    eventBinder.bindEventHandlers(this, eventBus);
    panel.setWidget(view);
  }

  @Override
  public void onStop() {
    // Remove habitat layer artifacts
    eventBus.fireEvent(new HabitatClearEvent());

    super.onStop();
  }

  @Override
  protected void refresh(final Situation sit, final EmissionValueKey key, final EmissionResultKey resultKey) {
    final CalculatedScenario calcScenario = appContext.getUserContext().getCalculatedScenario();
    final String noResultText = getNoResultText(place, sit, key, resultKey, calcScenario);
    final CalculationSummary summary = appContext.getCalculationSummary();
    if (noResultText == null) {
      if (appContext.getUserContext().getEmissionResultKey().getEmissionResultType() == EmissionResultType.CONCENTRATION) {
        view.setNoValue(false, M.messages().errorCalculationResultNoConcentrationSummaryResults());
      } else if (calcScenario.getOptions().getCalculationType() == CalculationType.CUSTOM_POINTS) {
        final List<AeriusPoint> list = new ArrayList<AeriusPoint>(ScenarioUtil.getCalculatedCalculationPoints(calcScenario, place));
        view.setValue(resultKey, list, sit == Situation.COMPARISON);
      } else if (sit == Situation.COMPARISON) {
        final int calcSitOne = calcScenario.getCalculationId(place.getSid1());
        final int calcSitTwo = calcScenario.getCalculationId(place.getSid2());
        if (JobType.PRIORITY_PROJECT_UTILISATION == place.getJobType()) {
          view.setValue(resultKey, summary, calcSitTwo, calcSitOne, calcScenario.getSources(calcSitOne).getName(),
              calcScenario.getSources(calcSitTwo).getName());
        } else {
          view.setValue(resultKey, summary, calcSitOne, calcSitTwo, calcScenario.getSources(calcSitOne).getName(),
              calcScenario.getSources(calcSitTwo).getName());
        }
      } else {
        final int activeCalculationId = calcScenario.getCalculationId(place.getActiveSituationId());
        view.setValue(resultKey, summary, activeCalculationId, calcScenario.getSources(activeCalculationId).getName());
      }
    } else {
      view.setNoValue(appContext.isCalculationRunning() || (summary == null), noResultText);
    }
  }
}
