/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import com.google.gwt.place.shared.Place;

/**
 * Place for Calculator results.
 */
public abstract class ResultPlace extends ScenarioBasePlace {
  public static final ScenarioBaseResultPlaceState[] ACTIVE_VALUES = new ScenarioBaseResultPlaceState[] {
      ScenarioBaseResultPlaceState.GRAPHICS,
      ScenarioBaseResultPlaceState.TABLE,
//      ScenarioBaseResultPlaceState.FILTER, // TODO; disabled for now. Should be fixed in a later release, hopefully very soon
  };

  /**
   * States a calculation result for different result tab.
   */
  public static enum ScenarioBaseResultPlaceState {
    /**
     * Overview result.
     */
    OVERVIEW,
    /**
     * Graphics result.
     */
    GRAPHICS,
    /**
     * Table result.
     */
    TABLE,
    /**
     * Filter result.
     */
    FILTER;

    /**
     * Safely returns a ResultPlaceState. It is case independent and returns null in case the input was null or the state could not be found.
     *
     * @param value value to convert
     * @return ResultPlaceState or null if no valid input
     */
    public static ScenarioBaseResultPlaceState safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase());
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }

    /**
     * @return Returns the name in lowercase.
     */
    @Override
    public String toString() {
      return name().toLowerCase();
    }
  }

  public ResultPlace(final Place currentPlace) {
    super(currentPlace);
  }

  protected ResultPlace() {}
}
