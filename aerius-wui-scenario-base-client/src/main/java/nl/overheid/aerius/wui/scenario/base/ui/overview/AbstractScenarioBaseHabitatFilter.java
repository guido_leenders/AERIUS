/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Range;
import nl.overheid.aerius.shared.domain.SimpleFilter;
import nl.overheid.aerius.shared.domain.deposition.HabitatFilterRangeContent;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.filter.AbstractHabitatFilter;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar.ValueRange;
import nl.overheid.aerius.wui.scenario.base.geo.VectorCalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.ui.overview.ReceptorPointDistributionUtil.RangeDistributor;

/**
 * Abstract implementation of a habitat filter, typed to a ranging scheme.
 *
 * @param <E> Ranging scheme this filter is typed to.
 */
public abstract class AbstractScenarioBaseHabitatFilter<E extends Range> extends AbstractHabitatFilter<E> {

  //Not static because it should be cached per filter type (subclass).
  private final IndexCache indexCache;

  private final VectorCalculationResultLayerWrapper layer;
  private HashMap<AeriusResultPoint, Double> receptorsSurfaceMap;

  private final SimpleFilter<AeriusResultPoint> bandwidthFilter = new SimpleFilter<AeriusResultPoint>() {
    @Override
    public boolean doFilter(final AeriusResultPoint rp) {
      return receptorsSurfaceMap.containsKey(rp) && isWithinLimits(rp);
    }

    private boolean isWithinLimits(final AeriusResultPoint rp) {
      final double emissionResult = rp.getEmissionResult(getEmissionResultKey());
      return getLowerLimit() < emissionResult && emissionResult <= getUpperLimit();
    }
  };

  /**
   * Constructor, taking the result layer need to filter and the substance to filter for.
   *
   * @param layer Result layer
   * @param emissionResultKey Selected emissionResultKey
   */
  public AbstractScenarioBaseHabitatFilter(final VectorCalculationResultLayerWrapper layer, final EmissionResultKey emissionResultKey,
      final IndexCache indexCache) {
    super(emissionResultKey);
    this.layer = layer;
    this.indexCache = indexCache;
  }

  /**
   * Set the receptors that need to be drawn/filtered by this habitat filter.
   *
   * @param hashMap Map of receptors with surface.
   */
  public void setData(final HashMap<AeriusResultPoint, Double> hashMap) {
    this.receptorsSurfaceMap = hashMap;

    // Construct the graph
    constructGraph();

    // Attach bandwidth filter
    layer.setFilter(bandwidthFilter);
  }

  @Override
  protected void constructGraph() {
    super.constructGraph();
    setVerticalLabel(M.messages().habitatFilterDeposition());
  }

  protected void redrawLayer() {
    layer.refreshLayer(true);

    if (!layer.isVisible()) {
      layer.setVisible(true);
    }
  }

  @Override
  public void clear() {
    // Remove the layer filter, causing it to draw everything like normal
    layer.setFilter(null);
    redrawLayer();
  }

  protected abstract RangeDistributor<E> getRangeDistributor();

  @Override
  protected Map<E, HabitatFilterRangeContent> getRanges() {
    Map<E, HabitatFilterRangeContent> returnMap = null;
    if (receptorsSurfaceMap != null) {
      returnMap = ReceptorPointDistributionUtil.getRanges(receptorsSurfaceMap, getEmissionResultKey(), getRangeDistributor());
    }

    return returnMap;
  }

  @Override
  public void setValueRange(final ValueRange value) {
    // Determine and set both values
    setUpperLimit(determineUpperLimit(value));
    setLowerLimit(determineLowerLimit(value));

    // Redraw the layer, applying the updated filter
    redrawLayer();
  }

  abstract double determineUpperLimit(final ValueRange value);
  abstract double determineLowerLimit(final ValueRange value);

  @Override
  protected void setUpperLimit(final double upper) {
    super.setUpperLimit(upper);
    indexCache.setUpperCache(upper);
  }

  @Override
  protected void setLowerLimit(final double lower) {
    super.setLowerLimit(lower);
    indexCache.setLowerCache(lower);
  }

  @Override
  protected int getUpperCacheIdx() {
    return getIndex(indexCache.getUpperCache());
  }

  @Override
  protected int getLowerCacheIdx() {
    return getIndex(indexCache.getLowerCache());
  }

  abstract int getIndex(final double val);

  static class IndexCache {

    private double lowerCache;
    private double upperCache;

    IndexCache(final double startLowerCache, final double startUpperCache) {
      lowerCache = startLowerCache;
      upperCache = startUpperCache;
    }

    double getLowerCache() {
      return lowerCache;
    }

    void setLowerCache(final double lowerCache) {
      this.lowerCache = lowerCache;
    }

    double getUpperCache() {
      return upperCache;
    }

    void setUpperCache(final double upperCache) {
      this.upperCache = upperCache;
    }

  }
}
