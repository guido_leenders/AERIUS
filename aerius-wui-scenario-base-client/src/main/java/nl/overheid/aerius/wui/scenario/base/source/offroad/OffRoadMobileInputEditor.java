/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.offroad;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource.SourceCategoryType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.ListCancelButtonEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.DoubleRangeValidator;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.InputWithCalculatorWidget;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.scenario.base.source.calculators.OffRoadEmissionCalculator;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;

/**
 * Widget for editing off road mobile data.
 */
class OffRoadMobileInputEditor extends Composite implements ValueAwareEditor<OffRoadVehicleEmissionSubSource>, HasClickHandlers, InputEditor<OffRoadVehicleEmissionSubSource> {
  interface OffRoadMobileInputDriver extends SimpleBeanEditorDriver<OffRoadVehicleEmissionSubSource, OffRoadMobileInputEditor> {}

  interface OffRoadMobileInputEditorUiBinder extends UiBinder<Widget, OffRoadMobileInputEditor> {}

  private static final OffRoadMobileInputEditorUiBinder UI_BINDER = GWT.create(OffRoadMobileInputEditorUiBinder.class);

  @UiField @Ignore TextBox name;
  ValidatedValueBoxEditor<String> nameEditor;

  @Ignore @UiField(provided = true) RadioButtonGroup<SourceCategoryType> editorTypeToggle;

  @UiField(provided = true) ListBoxEditor<OffRoadMobileSourceCategory> stageCategoryEditor;
  LeafValueEditor<SourceCategoryType> categoryTypeEditor = new LeafValueEditor<SourceCategoryType>() {
    @Override
    public void setValue(final SourceCategoryType value) {
      editorTypeToggle.setValue(value == null ? SourceCategoryType.STAGE_CLASS : value);
      switchPanel.showWidget(value == SourceCategoryType.DEVIATING_CLASS ? 1 : 0);
    }

    @Override
    public SourceCategoryType getValue() {
      return editorTypeToggle.getValue();
    }
  };

  @UiField IntValueBox fuelageEditor;

  @UiField DoubleValueBox emissionHeightEditor;
  @UiField DoubleValueBox spreadEditor;
  @UiField DoubleValueBox heatContentEditor;
  @UiField @Ignore Label labelEmission;
  @UiField(provided = true) DoubleValueBox emissionNOXEditor;
  @UiField FlowPanel spreadRow;
  @UiField(provided = true) OffRoadEmissionCalculator vehicleSpecification;
  @UiField InputWithCalculatorWidget<OffRoadVehicleEmissionSubSource> inputCalculator;
  @UiField @Ignore Button cancelButton;
  @UiField @Ignore Button submitButton;
  @UiField DeckPanel switchPanel;

  private final EventBus eventBus;
  private OffRoadVehicleEmissionSubSource restoreEmissions;

  public OffRoadMobileInputEditor(final EventBus eventBus, final ScenarioBaseContext context,
      final RadioButtonGroup<SourceCategoryType> editorTypeToggle, final HelpPopupController hpC) {
    this.eventBus = eventBus;
    this.editorTypeToggle = editorTypeToggle;
    this.vehicleSpecification = new OffRoadEmissionCalculator(context);
    emissionNOXEditor =
        new DoubleValueBox(M.messages().sourceEmission() + ' ' + Substance.NOX.getName());
    stageCategoryEditor = new ListBoxEditor<OffRoadMobileSourceCategory>() {
      @Override
      protected String getLabel(final OffRoadMobileSourceCategory value) {
        return value == null ? M.messages().mobileSourceEmptyOption() : value.getName();
      }
    };
    initWidget(UI_BINDER.createAndBindUi(this));
    vehicleSpecification.setHpC(hpC);

    labelEmission.setText(M.messages().sourceEmission() + ' ' + Substance.NOX.getName());
    nameEditor = new ValidatedValueBoxEditor<String>(name, M.messages().mobileSourceName()) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().ivVehicleNoEmptyName();
      }
    };
    nameEditor.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().ivVehicleNoEmptyName();
      }
    });

    fuelageEditor.addValidator(new NotZeroValidator<Integer>() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivMobileSourceFuelageNotEmpty();
      }

      @Override
      public String validate(final Integer value) {
        return isCustomCategory() ? null : super.validate(value);
      }
    });
    heatContentEditor.addValidator(new DoubleRangeValidator(0d, Double.valueOf(OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM)) {
      @Override
      protected String getMessage(final Double value) {
        return M.messages().ivRangeBetween(M.messages().heatContentCalculatorHeatContent(), 0, OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM);
      }
    });
    stageCategoryEditor.addValidator(new Validator<OffRoadMobileSourceCategory>() {
      @Override
      public String validate(final OffRoadMobileSourceCategory value) {
        return isCustomCategory() ? null
            : value == null ? M.messages().ivRoadCategoryNotSelected() : null;
      }
    });
    stageCategoryEditor.addFirstEmptyItem();
    stageCategoryEditor.addItems(context.getCategories().getOffRoadMobileSourceCategories());
    emissionNOXEditor.addValueChangeHandler(new ValueChangeHandler<Double>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Double> event) {
      //When the user inputs the emission reset the calculated value.
        vehicleSpecification.setValue(null);
      }
    });
    // Help widgets
    hpC.addWidget(name, hpC.tt().ttMobileSourceName());
    hpC.addWidget(stageCategoryEditor, hpC.tt().ttMobileSourceCategory());
    hpC.addWidget(fuelageEditor, hpC.tt().ttMobileSourceFuelage());
    hpC.addWidget(emissionHeightEditor, hpC.tt().ttMobileOPSHeight());
    hpC.addWidget(spreadEditor, hpC.tt().ttMobileOPSSpread());
    hpC.addWidget(heatContentEditor, hpC.tt().ttMobileOPSHeatContent());
    hpC.addWidget(emissionNOXEditor, hpC.tt().ttMobileGenericEmissionNOX());
    hpC.addWidget(submitButton, hpC.tt().ttMobileSourceSave());
    hpC.addWidget(inputCalculator, hpC.tt().ttMobileShowCalculator());

    StyleUtil.I.setPlaceHolder(name, M.messages().mobileSourceName());
  }

  @UiHandler("cancelButton")
  void cancelButtonHandler(final ClickEvent e) {
    if (restoreEmissions != null) {
      setValue(restoreEmissions.copy());
      eventBus.fireEvent(new ListCancelButtonEvent());
    }
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    final boolean polygonGeo = source.getGeometry() != null && source.getGeometry().getType() == WKTGeometry.TYPE.POLYGON;
    spreadRow.setVisible(polygonGeo);
    vehicleSpecification.setSector(source.getSector());
  }

  /**
   * Force empty content on the DoubleValueBox widgets when a new data object is edited. Otherwise the initial value will be 0 and the placeholder text won't be visible.
   */
  @Override
  public void resetPlaceholders() {
    fuelageEditor.resetPlaceHolder();
    emissionNOXEditor.resetPlaceHolder();
  }

  @UiHandler("editorTypeToggle")
  void valueChangeEventHandler(final ValueChangeEvent<SourceCategoryType> event) {
    categoryTypeEditor.setValue(event.getValue());
  }

  private boolean isCustomCategory() {
    return editorTypeToggle.getValue() == SourceCategoryType.DEVIATING_CLASS;
  }

  @Override
  public void setDelegate(final EditorDelegate<OffRoadVehicleEmissionSubSource> delegate) {
    // no-op
  }

  @Override
  public void flush() {
    // no-op
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void setValue(final OffRoadVehicleEmissionSubSource value) {
    restoreEmissions = value.copy();
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    fuelageEditor.ensureDebugId(TestID.INPUT_MOBILESOURCE_FUELAMOUNT);
    name.ensureDebugId(TestID.INPUT_MOBILESOURCE_NAME);
    stageCategoryEditor.ensureDebugId(TestID.INPUT_MOBILESOURCE_CATEGORY);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
    cancelButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_CANCEL);

    emissionHeightEditor.ensureDebugId(TestID.INPUT_MOBILESOURCE_HEIGHT);
    spreadEditor.ensureDebugId(TestID.INPUT_MOBILESOURCE_SPREAD);
    heatContentEditor.ensureDebugId(TestID.INPUT_MOBILESOURCE_HEAT);
    emissionNOXEditor.ensureDebugId(TestID.INPUT_MOBILESOURCE_EMISSION);

    inputCalculator.getCalculatorButton().ensureDebugId(TestID.BUTTON_OPEN_CALCULATOR_MOBILE);

  }
}
