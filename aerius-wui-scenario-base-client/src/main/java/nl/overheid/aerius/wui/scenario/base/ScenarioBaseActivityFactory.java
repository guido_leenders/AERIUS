/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base;

import nl.overheid.aerius.wui.main.AsyncCapableActivityFactory;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseActivityMapper.PreActivity;
import nl.overheid.aerius.wui.scenario.base.ScenarioBaseActivityMapper.ScenarioBaseStartup;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public interface ScenarioBaseActivityFactory extends AsyncCapableActivityFactory {
  /**
   * Called only once during startup process.
   * @return
   */
  ScenarioBaseStartup startup();

  /**
   * Called prior to calling the actual activity presenter.
   * @param place current place
   * @return
   */
  PreActivity createPreActivity(ScenarioBasePlace place);
}