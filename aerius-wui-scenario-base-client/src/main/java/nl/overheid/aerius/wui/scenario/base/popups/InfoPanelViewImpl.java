/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.info.Natura2000Info;
import nl.overheid.aerius.shared.domain.info.ReceptorInfo;
import nl.overheid.aerius.wui.main.event.PreventDragMouseHandler;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.AttachedPopupBase;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

@Singleton
public class InfoPanelViewImpl extends Composite implements InfoPanelView {
  interface Presenter {
    void selectReceptor(double x, double y);
  }

  interface InfoPanelViewImplUiBinder extends UiBinder<Widget, InfoPanelViewImpl> {}

  private static final InfoPanelViewImplUiBinder UI_BINDER = GWT.create(InfoPanelViewImplUiBinder.class);

  @UiField AttachedPopupBase wrapper;

  @UiField Label defaultText;
  @UiField FlowPanel loadContainer;
  @UiField Label areaText;
  @UiField FlowPanel stackPanel;

  @UiField(provided = true) AreaInfoPanel<Natura2000Info, Natura2000InfoPanelItem> natura2000InfoPanel;
  @UiField(provided = true) HabitatTypePanel habitatTypePanel;
  @UiField(provided = true) EmissionResultPanel emissionResultPanel;
  @UiField InlineHTML receptorIdLabel;

  @UiField Label titleLabel;

  private final InfoVector infoVector;

  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  private boolean layoutScheduled;

  private final ResizeHandler resizeHandler = new ResizeHandler() {
    @Override
    public void onResize(final ResizeEvent event) {
      forceLayout();
    }
  };

  private HandlerRegistration resizeRegistration;

  @Inject
  public InfoPanelViewImpl(final ScenarioBaseMapLayoutPanel map, final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext,
      final EmissionResultPanel emissionResultPanel, final HelpPopupController hpC) {
    final ScenarioBaseUserContext userContext = appContext.getUserContext();

    this.emissionResultPanel = emissionResultPanel;
    final HexagonZoomLevel zoomLevel1 = map.getReceptorGridSettings().getZoomLevel1();
    habitatTypePanel =
        new HabitatTypePanel(eventBus, userContext.getEmissionResultKey(), userContext.getEmissionResultValueDisplaySettings(), zoomLevel1);

    natura2000InfoPanel = new AreaInfoPanel<Natura2000Info, Natura2000InfoPanelItem>() {
      @Override
      protected Natura2000InfoPanelItem getNewItem(final EventBus eventBus, final InfoVector infoVector, final Presenter presenter) {
        return new Natura2000InfoPanelItem(eventBus, userContext, infoVector, presenter, hpC);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    titleLabel.setText(M.messages().infoPanelTitleDefault());

    infoVector = new InfoVector("InfoVector", zoomLevel1);
    map.addLayer(infoVector);

    natura2000InfoPanel.setVectorLayer(infoVector);
    natura2000InfoPanel.setEventBus(eventBus);

    hpC.addWidget(areaText, hpC.tt().ttInfoPanelHabitatType());

    final PreventDragMouseHandler preventDragHandler = new PreventDragMouseHandler();
    preventDragHandler.addToWidget(titleLabel);
    preventDragHandler.addToWidget(receptorIdLabel);
  }

  @Override
  protected void onAttach() {
    super.onAttach();

    resizeRegistration = Window.addResizeHandler(resizeHandler);

    scheduledLayout();
  }

  @Override
  protected void onDetach() {
    super.onDetach();

    resizeRegistration.removeHandler();
  }

  @Override
  public void setPopup(final AttachedPopupPanel pp) {
    wrapper.setPopup(pp);
  }

  @Override
  public void updateCalculationInfo(final CalculationInfo calculationInfo) {
    natura2000InfoPanel.setCalculationInfo(calculationInfo);
  }

  @Override
  public void updateAreaInfo(final ReceptorInfo result, final CalculatedScenario scenario, final ScenarioBasePlace place) {
    loadContainer.setVisible(false);

    infoVector.clear();
    setPanelTitle(result.getReceptor());
    habitatTypePanel.setTypeInfo(result.getReceptor().getId(), result.getHabitatTypeInfo());

    // Only visible when the selected receptor is over a n2000 area.
    areaText.setVisible(!result.getNaturaInfo().isEmpty());

    natura2000InfoPanel.setAreaInfo(result.getNaturaInfo());
    emissionResultPanel.setEmissionResultInfo(result.getEmissionResultInfo(), scenario, place);
  }

  private void setPanelTitle(final AeriusPoint rec) {
    titleLabel.setText(M.messages().infoPanelLocation(MathUtil.round(rec.getX()), MathUtil.round(rec.getY())));
    receptorIdLabel.setHTML(M.messages().infoPanelReceptor(rec.getId()));
  }

  @Override
  public void dataPending(final AeriusPoint rec) {
    if (defaultText.getParent() != null) {
      defaultText.removeFromParent();
    }

    infoVector.clear();

    setPanelTitle(rec);
    loadContainer.setVisible(true);
    areaText.setVisible(false);
    natura2000InfoPanel.setAreaInfo(null);
    habitatTypePanel.setTypeInfo(0, null);
    emissionResultPanel.clear();
  }

  @Override
  public void clearEmissionResultInfo() {
    emissionResultPanel.clear();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    natura2000InfoPanel.setPresenter(presenter);
  }

  @Override
  public void onResize() {
    scheduledLayout();
  }

  private void scheduledLayout() {
    if (layoutScheduled) {
      return;
    }

    layoutScheduled = true;
    Scheduler.get().scheduleDeferred(layoutCmd);
  }

  private void forceLayout() {
    stackPanel.getElement().getStyle().setProperty("maxHeight", wrapper.calculateMaxHeightBasedOnScreen(), Unit.PX);
  }
}
