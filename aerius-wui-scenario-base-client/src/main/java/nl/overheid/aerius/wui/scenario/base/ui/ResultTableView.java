/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * View for results table.
 */
public interface ResultTableView extends IsWidget, HasTitle {

  /**
   *
   * @param emissionResultKey The emission result key
   * @param calculationPointList The calc point result list
   * @param diff Whether it's a diff result or not
   */
  void setValue(EmissionResultKey emissionResultKey, List<AeriusPoint> calculationPointList, boolean diff);

  /**
   * Set the table results of a comparison.
   *
   * @param emissionResultKey The emission result key
   * @param summary summary results
   * @param calcId1 calculation id of situation one
   * @param calcId2 calculation id of situation two
   * @param situationOneName Situation one name
   * @param situationTwoName Situation two name
   */
  void setValue(EmissionResultKey emissionResultKey, CalculationSummary summary, int calcId1, int calcId2,
      String situationOneName, String situationTwoName);

  /**
   * Set the table results of a single calculation.
   * @param emissionResultKey The emission result key
   * @param summary summary results
   * @param calcId calculation id of the situation
   * @param situationName The situation name
   */
  void setValue(EmissionResultKey emissionResultKey, CalculationSummary summary, int calcId, String situationName);

  /**
   * Call when no data available.
   * @param calculationRunning if true calculation is running
   * @param message message to show, will only be used if calculationRunning is false
   */
  void setNoValue(boolean calculationRunning, String message);
}
