/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.progress;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Widget;

class HorizontalLabels extends Composite {
  private static final double ZERO_POINT_FIVE = 0.5;
  private static final int LABEL_INTERVAL = 5;
  private static final int EXPANDED_LABEL_INTERVAL = 40;
  private static final int MAX_SINGLE_CHAR_NUM = 5;
  private static final double MAX_TEXT_WIDTH = 25;
  private static final int COMPACT_LABEL_THRESHOLD = 20;
  private static final int EXPANDED_LABEL_THRESHOLD = 50;

  private static HorizontalLabelsUiBinder uiBinder = GWT.create(HorizontalLabelsUiBinder.class);

  interface HorizontalLabelsUiBinder extends UiBinder<Widget, HorizontalLabels> {
  }

  @UiField FlowPanel labels;
  @UiField CustomStyle style;

  public interface CustomStyle extends CssResource {
    String label();

    String maxDistance();
  }

  public HorizontalLabels() {
    initWidget(uiBinder.createAndBindUi(this));
  }

  /**
   * Set the number of labels to distribute.
   * 
   * FIXME Sprint 9 - Sorry for this. I was very very tired when I wrote this and now it's too confusing to mend. It's too bloated and doesn't make sense anymore. Just completely recreate it.
   *
   * @param num number of bars
   * @param barWidth width of a single bar
   * @param max the max-distance bar (if any, otherwise 0)
   */
  public void setNumLabels(final int num, final double barWidth, final int max) {
    labels.clear();

    // Insert a mini-label spanning half the width of a column, this will result in labels being directly under a line instead of a bar
    final InlineLabel first = new InlineLabel("");
    first.setWidth((int) (barWidth * ZERO_POINT_FIVE) + "px");
    first.addStyleName(style.label());
    labels.add(first);

    // If the bars are too thin to put text in, display a label every 5 bars, otherwise every 1 bar(s)
    final int defaultMod = barWidth <= MAX_TEXT_WIDTH ? LABEL_INTERVAL : 1;
    final int expandedMod = barWidth <= MAX_TEXT_WIDTH ? EXPANDED_LABEL_INTERVAL : 1;
    final boolean compact = max <= COMPACT_LABEL_THRESHOLD;
    final boolean expanded = !compact && max > EXPANDED_LABEL_THRESHOLD;

    // Iterate over the num and insert a label for each
    for (int i = 1; i < num; i++) {
      final InlineLabel d = new InlineLabel((expanded
          ? (i % expandedMod == 0 || i == max)
          : (!compact
              ? (i % defaultMod == 0 || i <= MAX_SINGLE_CHAR_NUM || i == max)
              : (i % defaultMod == 0 || i == max)))
          ? String.valueOf(i)
          : "");
      d.setWidth(barWidth + "px");
      d.addStyleName(style.label());
      labels.add(d);

      if (i == max) {
        d.addStyleName(style.maxDistance());
      }
    }
  }
}
