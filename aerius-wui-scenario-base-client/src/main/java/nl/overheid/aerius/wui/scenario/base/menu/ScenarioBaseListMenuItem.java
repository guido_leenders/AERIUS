/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.menu;

import com.google.gwt.place.shared.Place;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.ui.menu.MenuItemList;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class ScenarioBaseListMenuItem extends MenuItemList<ScenarioBasePlace, ScenarioBaseTheme> {

  public ScenarioBaseListMenuItem(final String testId) {
    super(ScenarioBaseTheme.values(), testId);
  }

  protected final ScenarioBasePlace asCalculatorPlace(final Place place) {
    if (place instanceof ScenarioBasePlace) {
      return ((ScenarioBasePlace) place).copy();
    }
    return null;
  }

  @Override
  protected ScenarioBasePlace getPlaceFromSelection(final Place currentPlace, final ScenarioBaseTheme selection) {
    final ScenarioBasePlace place = asCalculatorPlace(currentPlace);
    if (place != null) {
      place.setTheme(selection);
    }
    return place;
  }

  @Override
  protected String getName(final ScenarioBaseTheme value) {
    return M.messages().calculatorMenuThemeSwitch(value);
  }

  @Override
  protected ScenarioBaseTheme getDefaultValue() {
    return ScenarioBaseTheme.NATURE_AREAS;
  }

  @Override
  protected ScenarioBaseTheme getValueFromPlace(final Place currentPlace) {
    return currentPlace instanceof ScenarioBasePlace ? ((ScenarioBasePlace) currentPlace).getTheme() : getDefaultValue();
  }
}
