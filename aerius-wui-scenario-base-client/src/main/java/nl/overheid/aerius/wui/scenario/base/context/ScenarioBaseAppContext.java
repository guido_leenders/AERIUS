/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.context;

import java.util.HashMap;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.ConsecutiveNamedList;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.wui.main.context.AppContext;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace.ScenarioBaseResultPlaceState;

public abstract class ScenarioBaseAppContext<C extends ScenarioBaseContext, UC extends ScenarioBaseUserContext> extends AppContext<C, UC> {
  private final HashMap<Integer, CalculationInfo> markerInfoMap = new HashMap<Integer, CalculationInfo>();
  private final HashMap<Integer, ConsecutiveNamedList<MaritimeRoute>> maritimeRoutes = new HashMap<Integer, ConsecutiveNamedList<MaritimeRoute>>();
  private final ReceptorUtil receptorUtil;
  private CalculationSummary calculationSummary;
  private EmissionSource editEmissionSource;
  private boolean calculationRunning;
  private boolean calculationCancelled;
  private transient CalculationSetOptions calculationOptions = new CalculationSetOptions();
  private CalculationState calculationState = CalculationState.INITIALIZED;
  private ResultPlace.ScenarioBaseResultPlaceState resultPlaceState = ResultPlace.ScenarioBaseResultPlaceState.GRAPHICS;

  /**
   * @param eventBus
   * @param placeController
   * @param scenarioBaseContextProvider
   */
  public ScenarioBaseAppContext(final EventBus eventBus, final PlaceController placeController, final C context, final UC userContext) {
    super(eventBus, placeController, context, userContext);

    final int d = context.getSetting(SharedConstantsEnum.CALCULATE_EMISSIONS_DEFAULT_RADIUS_DISTANCE_UI);
    initTheme(ScenarioBaseTheme.NATURE_AREAS);
    calculationOptions.setCalculateMaximumRange(d);
    receptorUtil = new ReceptorUtil(context.getReceptorGridSettings());
  }

  public ReceptorUtil getReceptorUtil() {
    return receptorUtil;
  }

  /**
   * If the id > -1 and the source list for the given id doesn't exist a source list is created with the given id. The name of the source list will
   * be post fixed with the given id.
   * @param eslId emission source list id to check and possible initialize
   */
  public void initEmissionSourceList(final int eslId) {
    if (eslId > -1 && userContext.getSources(eslId) == null) {
      final EmissionSourceList esl = new EmissionSourceList();
      esl.setId(eslId);
      userContext.getSourceLists().add(esl);
      esl.setName(M.messages().situationDefaultName(esl.getId() + 1)); // + 1 because situation names start with 1 (id 0 is situation 1)
    }
  }

  /**
   * Initializes the calculations options given a theme. These are substances and result types.
   * @param theme the new theme.
   */
  public void initTheme(final ScenarioBaseTheme theme) {
    final CalculationSetOptions options = getCalculationOptions();
    options.getSubstances().clear();
    options.getSubstances().addAll(theme.getCalculationSubstances());
    options.getEmissionResultKeys().clear();
    options.getEmissionResultKeys().addAll(theme.getSubstancesEmissionType());
  }

  /**
   * Deletes an {@link EmissionSourceList} and all related other storage objects.
   * @param userContext usercontext containing the {@link EmissionSourceList}
   * @param eslId id of the list to delete
   */
  public void deleteEmissionSourceList(final ScenarioBaseUserContext userContext, final int eslId) {
    userContext.removeSources(eslId);

    if (maritimeRoutes.containsKey(eslId)) {
      maritimeRoutes.remove(eslId);
    }

    // If calculation results present for a specific situation remove them.
    if (userContext.getCalculatedScenario() != null && eslId >= 0) {
      final int calculationId = userContext.getCalculatedScenario().getCalculationId(eslId);

      if (markerInfoMap.containsKey(calculationId)) {
        markerInfoMap.remove(calculationId);
      }
      // Remove calculated summary results.
      setCalculationSummary(null);

      if (userContext.getCalculatedScenario() instanceof CalculatedComparison) {
        final CalculatedComparison cc = (CalculatedComparison) userContext.getCalculatedScenario();
        final int otherId = cc.getCalculationIdOne() == calculationId ? cc.getCalculationIdTwo() : cc.getCalculationIdOne();

        // Rewrite the CalculatedScenario object into a CalculationSingle one
        final CalculatedSingle cs = new CalculatedSingle();
        cs.setCalculationId(otherId);
        cs.getCalculationPoints().addAll(cc.getCalculationPoints());
        cs.setSources(cc.getSources(cs.getCalculationId()));
        userContext.setCalculatedScenario(cs);
      }
      //TODO: Remove calculations from map
      //can still show it again by clicking the 'kaartlagen opties'.
      //      map.hideSubstanceLayers();
    }

  }

  public CalculationInfo getCalculationInfo(final Integer calculationId) {
    return calculationId != null && markerInfoMap.containsKey(calculationId)
        ? markerInfoMap.get(calculationId) : null;
  }

  public CalculationSummary getCalculationSummary() {
    return calculationSummary;
  }

  public void setCalculationInfo(final Integer calculationId, final CalculationInfo calculationInfo) {
    markerInfoMap.put(calculationId, calculationInfo);
  }

  public void setCalculationSummary(final CalculationSummary calculationSummary) {
    this.calculationSummary = calculationSummary;
  }

  /**
   * Removes the current object for a new {@link EmissionSource}. Use this to
   * clear the object when editing is done.
   */
  public void clearNewEmissionSource() {
    this.editEmissionSource = null;
  }

  /**
   * Returns whether a calculation is currently running.
   * <p>Note: Don't use this method in one of the onXCalculationEvents as it is undefined if the returned state then returns the actual state, and
   * it's necessary either since the event says something about the state.
   * @return true if calculation was finished.
   */
  public boolean isCalculationRunning() {
    return calculationRunning;
  }

  /**
   * Returns true if the last calculation was canceled.
   * <p>Note: Don't use this method in one of the onXCalculationEvents as it is undefined if the returned state then returns the actual state, and
   * it's necessary either since the event says something about the state.
   * @return true if calculation was canceled.
   */
  public boolean isCalculationCancelled() {
    return calculationCancelled;
  }

  /**
   * Returns the object which is used to store a newly {@link EmissionSource} object.
   * If the object was cleared before or didn't exist yet a new clean object is
   * returned.
   * @return new {@link EmissionSource} object.
   */
  public EmissionSource getNewEmissionSource() {
    if (editEmissionSource == null) {
      editEmissionSource = new GenericEmissionSource();
    }
    return editEmissionSource;
  }

  /**
   * Set to true when a calculation is in progress.
   * @param running true if calculation in progress.
   */
  public void setCalculationRunning(final boolean running) {
    this.calculationRunning = running;
    this.calculationCancelled = running ? false : calculationCancelled;
    resultPlaceState = ResultPlace.ScenarioBaseResultPlaceState.GRAPHICS;
  }

  /**
   * Set calculation cancelled state.
   * @param cancelled true if calculation is cancelled.
   */
  public void setCalculationCancelled(final boolean cancelled) {
    calculationCancelled = cancelled;
    this.calculationRunning = cancelled ? false : calculationRunning;
  }

  /**
   * Set currentCalculationState.
   * @param eSLId current calculation state.
   * @return {@link ConsecutiveNamedList} object.
   */
  public ConsecutiveNamedList<MaritimeRoute> getMaritimeRoutes(final int eSLId) {
    if (!maritimeRoutes.containsKey(eSLId)) {
      maritimeRoutes.put(eSLId, new ConsecutiveNamedList<MaritimeRoute>());
    }
    return maritimeRoutes.get(eSLId);
  }

  public CalculationSetOptions getCalculationOptions() {
    return calculationOptions;
  }

  public void setCalculationOptions(final CalculationSetOptions calculationOptions) {
    this.calculationOptions = calculationOptions;
  }

  /**
   * Set currentCalculationState.
   * @param calculationState current calculation state.
   */
  public void setCalculationState(final CalculationState calculationState) {
    this.calculationState = calculationState;
  }

  /**
   * Returns currentCalculationState.
   * @return new {@link CalculationState} object.
   */
  public CalculationState getCalculationState() {
    return this.calculationState;
  }

  /**
   * Returns resultPlaceState.
   * @return new {@link ResultPlace.ScenarioBaseResultPlaceState} object.
   */
  public ScenarioBaseResultPlaceState getResultPlaceState() {
    return resultPlaceState;
  }

  public void setResultPlaceState(final ScenarioBaseResultPlaceState resultPlaceState) {
    this.resultPlaceState = resultPlaceState;
  }

  /**
   * Temporary method to enable/disable habitat filter based on the product.
   * Due to refactoring of results in Scenario, we can't use the habitatfilter there anymore.
   */
  public abstract boolean enableHabitatFilter();

}
