/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DynamicEditorSource;
import nl.overheid.aerius.wui.main.ui.editor.DynamicListEditor;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SimpleFocusLabel;
import nl.overheid.aerius.wui.main.widget.SimpleTabPanel;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorHandlerImpl.EnableButtons;

/**
 * Editor for Inland Mooring detail.
 */
class InlandMooringInputEditor extends Composite implements Editor<InlandMooringVesselGroup>, InputEditor<InlandMooringVesselGroup>,
    HasClickHandlers, EnableButtons, ValueAwareEditor<InlandMooringVesselGroup> {

  interface InlandMooringInputDriver extends SimpleBeanEditorDriver<InlandMooringVesselGroup, InlandMooringInputEditor> {
  }

  interface InlandMooringRoutesDriver extends
      SimpleBeanEditorDriver<List<InlandMooringRoute>, DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor>> {
  }

  interface InlandMooringInputEditorUiBinder extends UiBinder<Widget, InlandMooringInputEditor> {
  }

  interface InlandMooringInputEditorEventBinder extends EventBinder<InlandMooringInputEditor> {
  }

  private static final InlandMooringInputEditorUiBinder UI_BINDER = GWT.create(InlandMooringInputEditorUiBinder.class);

  private static final TextMachine<InlandShippingCategory> TEXT_MACHINE = new ShippingCategoryTextMachine<InlandShippingCategory>();

  @UiField @Ignore TextBox name;
  @UiField(provided = true) SuggestBox<InlandShippingCategory> category;
  @UiField IntValueBox residenceTime;
  @UiField @Ignore SimpleTabPanel tabPanel;
  @UiField @Ignore SimpleFocusLabel tabRouteArrive;
  @UiField @Ignore SimpleFocusLabel tabRouteDepart;
  @UiField SwitchPanel routePanel;
  @UiField FlowPanel inlandArriveRoutesPanel;
  @UiField FlowPanel inlandDepartRoutesPanel;
  @UiField @Ignore Button submitButton;
  ValidatedValueBoxEditor<String> nameEditor;

  LeafValueEditor<ArrayList<InlandMooringRoute>> routesEditor = new LeafValueEditor<ArrayList<InlandMooringRoute>>() {
    @Override
    public void setValue(final ArrayList<InlandMooringRoute> routes) {
      setRoutes(routes, inlandRouteArriveDriver, NavigationDirection.ARRIVE);
      setRoutes(routes, inlandRouteDepartDriver, NavigationDirection.DEPART);
    }

    private void setRoutes(final List<InlandMooringRoute> routes, final InlandMooringRoutesDriver inlandRouteDriver,
        final NavigationDirection direction) {
      final List<InlandMooringRoute> filteredRoutes = new ArrayList<>();
      if (routes != null) {
        for (final InlandMooringRoute imr : routes) {
          if (imr.getDirection() == direction) {
            filteredRoutes.add(imr);
          }
        }
      }
      inlandRouteDriver.edit(filteredRoutes);
    }

    @Override
    public ArrayList<InlandMooringRoute> getValue() {
      final ArrayList<InlandMooringRoute> routes = new ArrayList<>();
      addRoutes(routes, inlandRouteArriveDriver);
      addRoutes(routes, inlandRouteDepartDriver);
      return routes;
    }

    private void addRoutes(final ArrayList<InlandMooringRoute> routes, final InlandMooringRoutesDriver driver) {
      routes.addAll(driver.flush());
      if (driver.hasErrors()) {
        for (final EditorError error : driver.getErrors()) {
          delegate.recordError(error.getMessage(), error.getValue(), error.getUserData());
        }
      }
    }
  };

  private final InlandMooringInputEditorEventBinder eventBinder = GWT.create(InlandMooringInputEditorEventBinder.class);

  private final EnableButtons enableButtons = new EnableButtons() {
    @Override
    public void enableButtons(final boolean enable) {
      InlandMooringInputEditor.this.enableButtons(enable);
    }
  };
  private ShippingRouteHandler inlandRouteHandler;
  private Point sourcePoint;
  private List<ShippingRoute> inlandRoutes;

  private EditorDelegate<InlandMooringVesselGroup> delegate;

  private final DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> routesArriveEditor;
  private final DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> routesDepartEditor;
  private final InlandMooringRoutesDriver inlandRouteArriveDriver;
  private final InlandMooringRoutesDriver inlandRouteDepartDriver;

  private final InlandShipCategorySuggestionOracle oracle;
  private final EventBus eventBus;
  private final SourceMapPanel map;
  private final InlandShippingCategories inlandShippingCategories;
  private final HelpPopupController hpC;

  public InlandMooringInputEditor(final EventBus eventBus, final SourceMapPanel map, final InlandShippingCategories inlandShippingCategories,
      final HelpPopupController hpC) {
    this.eventBus = eventBus;
    this.map = map;
    this.inlandShippingCategories = inlandShippingCategories;
    this.hpC = hpC;
    oracle = new InlandShipCategorySuggestionOracle(inlandShippingCategories);
    category = new SuggestBox<InlandShippingCategory>(oracle, TEXT_MACHINE);

    initWidget(UI_BINDER.createAndBindUi(this));

    routePanel.showWidget(0);

    inlandRouteArriveDriver = GWT.create(InlandMooringRoutesDriver.class);
    routesArriveEditor = createListEditor(createEditorSource(inlandArriveRoutesPanel, NavigationDirection.ARRIVE));
    inlandRouteArriveDriver.initialize(routesArriveEditor);
    inlandRouteDepartDriver = GWT.create(InlandMooringRoutesDriver.class);
    routesDepartEditor = createListEditor(createEditorSource(inlandDepartRoutesPanel, NavigationDirection.DEPART));
    inlandRouteDepartDriver.initialize(routesDepartEditor);

    nameEditor = new ShippingValidatedValueBoxEditor(name);
    residenceTime.addValidator(new NotZeroValidator<Integer>() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivShipResidenceTimeNotEmpty();
      }
    });
    eventBinder.bindEventHandlers(this, eventBus);

    // Help widgets
    hpC.addWidget(name, hpC.tt().ttInlandMooringShipName());
    hpC.addWidget(category, hpC.tt().ttInlandMooringShipType());
    hpC.addWidget(residenceTime, hpC.tt().ttInlandMooringShipResidenceTime());
    hpC.addWidget(submitButton, hpC.tt().ttInlandMooringShipSave());

    onEnsureDebugId("");
    tabRouteArrive.ensureDebugId(TestID.TAB_ROUTE_ARRIVE);
    tabRouteDepart.ensureDebugId(TestID.TAB_ROUTE_DEPART);

  }

  @UiHandler("tabPanel")
  void onTabSelection(final SelectionEvent<Integer> e) {
    routePanel.showWidget(e.getSelectedItem());
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    name.ensureDebugId(TestID.INPUT_SHIPPING_NAME);
    category.getValueBox().ensureDebugId(TestID.INPUT_SHIPPING_CATEGORY);
    residenceTime.ensureDebugId(TestID.INPUT_SHIPPING_RESIDENCETIME);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  public void refreshInlandRoutes(final List<ShippingRoute> routes) {
    this.inlandRoutes = routes;
    for (final InlandMooringRowEditor editor : routesArriveEditor.getEditors()) {
      editor.setRoutes(routes);
    }
    for (final InlandMooringRowEditor editor : routesDepartEditor.getEditors()) {
      editor.setRoutes(routes);
    }
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    sourcePoint = source;
    for (final InlandMooringRowEditor editor : routesArriveEditor.getEditors()) {
      editor.setDockingPoint(source);
    }
    for (final InlandMooringRowEditor editor : routesDepartEditor.getEditors()) {
      editor.setDockingPoint(source);
    }
  }

  /**
   * Force empty content on the IntValueBox widgets when a new data object is edited.
   * Otherwise the initial value will be 0 and the placeholder text won't be visible.
   */
  @Override
  public void resetPlaceholders() {
    residenceTime.resetPlaceHolder();
    routesArriveEditor.resetPlaceholders();
  }

  @Override
  public void enableButtons(final boolean enable) {
    submitButton.setEnabled(enable);
    for (final InlandMooringRowEditor editor : routesArriveEditor.getEditors()) {
      editor.enableButtons(enable);
    }
    for (final InlandMooringRowEditor editor : routesDepartEditor.getEditors()) {
      editor.enableButtons(enable);
    }
  }

  /**
   * Initializes the inland route handler and sets the route container.
   * @param inlandRouteHandler container for inland routes
   */
  public void setRoutesHandler(final ShippingRouteHandler inlandRouteHandler) {
    this.inlandRouteHandler = inlandRouteHandler;
  }

  @Override
  public void flush() {
    if (checkEmptyRoutes(routesArriveEditor) && checkEmptyRoutes(routesDepartEditor)) {
      delegate.recordError(M.messages().ivShippingNoRoute(), 0, routePanel);
    }
    validateRouteCategoryCombination(routesArriveEditor);
    validateRouteCategoryCombination(routesDepartEditor);
  }

  private void validateRouteCategoryCombination(final DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> routesEditor) {
    for (final InlandMooringRowEditor rowEditor : routesEditor.getEditors()) {
      final ShippingRoute route = rowEditor.routeEditor.getValue();
      final InlandShippingCategory categoryValue = category.getValue();
      if (route != null) {
        final InlandWaterwayCategory waterwayCategory = route.getWaterwayCategory();
         if (!inlandShippingCategories.isValidCombination(categoryValue, waterwayCategory)) {
          delegate.recordError(
              M.messages().shipWaterwayNotAllowedCombination(categoryValue.getCode(), waterwayCategory.getCode()), categoryValue, category);
        }
      }
    }
  }

  private boolean checkEmptyRoutes(final DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> routesEditor) {
    boolean hasNoRoutes = true;
    for (final InlandMooringRowEditor mr: routesEditor.getEditors()) {
      if (mr.routeEditor.getValue() != null) { // only count rows which have a route selected.
        hasNoRoutes = false;
        break;
      }
    }
    return hasNoRoutes;
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void setValue(final InlandMooringVesselGroup value) {
    // no-op
  }

  @Override
  public void setDelegate(final EditorDelegate<InlandMooringVesselGroup> delegate) {
    this.delegate = delegate;
  }

  private InlandMooringEditorSource createEditorSource(final FlowPanel inlandRoutesPanel, final NavigationDirection direction) {
    return new InlandMooringEditorSource(inlandRoutesPanel, direction) {
      @Override
      protected InlandMooringRowEditor createRowWidget() {
        return InlandMooringInputEditor.this.createRowWidget();
      }
    };
  }

  private DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> createListEditor(final InlandMooringEditorSource inlandRouteEditorSource) {
    final DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> dynamicListEditor =
        new DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor>(inlandRouteEditorSource) {
      @Override
      public InlandMooringRowEditor addEmptyRow() {
        final InlandMooringRowEditor row = super.addEmptyRow();
        row.resetPlaceHolders();
        return row;
      }
    };
    inlandRouteEditorSource.setListEditor(dynamicListEditor);
    return dynamicListEditor;
  }

  private InlandMooringRowEditor createRowWidget() {
    final InlandMooringRowEditor inlandRowEditor =
        new InlandMooringRowEditor(eventBus, map.getPanel(), inlandShippingCategories, hpC, inlandRouteHandler, enableButtons);
    inlandRowEditor.setDockingPoint(sourcePoint);
    inlandRowEditor.setRoutes(inlandRoutes);
    return inlandRowEditor;
  }

  private abstract static class InlandMooringEditorSource extends DynamicEditorSource<InlandMooringRoute, InlandMooringRowEditor> {

    private final NavigationDirection direction;
    private DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> routesListEditor;

    public InlandMooringEditorSource(final FlowPanel inlandRoutesPanel, final NavigationDirection direction) {
      super(inlandRoutesPanel);
      this.direction = direction;
      setDebugIdPrefix(direction.toString() + '-');
    }

    public void setListEditor(final DynamicListEditor<InlandMooringRoute, InlandMooringRowEditor> routesListEditor) {
      this.routesListEditor = routesListEditor;
    }

    @Override
    protected InlandMooringRoute createObject() {
      final InlandMooringRoute route = new InlandMooringRoute();
      route.setDirection(direction);
      return route;
    }

    @Override
    public void onRemove(final InlandMooringRoute value) {
      value.setRoute(null); // clear the route before removing it
      routesListEditor.getList().remove(value);
    }
  }

  public void setInlandWaterwayCategory(final InlandWaterwayCategory inlandWaterwayCategory) {
    oracle.setInlandWaterwayCategory(inlandWaterwayCategory);
  }
}