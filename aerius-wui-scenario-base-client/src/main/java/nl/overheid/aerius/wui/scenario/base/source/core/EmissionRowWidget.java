/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor.Ignore;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.InputWithUnitWidget;

/**
 * Row widgets for entering emission values.
 * Displays a 2 column widget with label and input field.
 */
public class EmissionRowWidget extends Composite implements HasValue<Double> {

  /**
   * The number of digits after the decimal point for emission values.
   */
  private static final int EMISSION_DIGITS_PRECISION = 1;

  private static final EmissionRowWidgetUiBinder UI_BINDER = GWT.create(EmissionRowWidgetUiBinder.class);

  interface EmissionRowWidgetUiBinder extends UiBinder<Widget, EmissionRowWidget> {}

  @UiField @Ignore Label label;
  @UiField @Ignore InputWithUnitWidget inputWidget;
  @UiField(provided = true) DoubleValueBox emission;

  public EmissionRowWidget(final Substance substance, final String unit, final HelpPopupController hpC, final HelpInfo helpInfo,
      final String debugId) {
    final String labelTxt = M.messages().sourceEmission() + ' ' + substance.getName();
    emission = new DoubleValueBox(labelTxt, EMISSION_DIGITS_PRECISION);
    initWidget(UI_BINDER.createAndBindUi(this));
    inputWidget.setUnit(unit);
    label.setText(labelTxt);
    hpC.addWidget(emission, helpInfo);
    emission.ensureDebugId(debugId);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
    return emission.addValueChangeHandler(handler);
  }

  @Override
  public Double getValue() {
    return emission.getValue();
  }

  @Override
  public void setValue(final Double value) {
    emission.setValue(value);
  }

  @Override
  public void setValue(final Double value, final boolean fireEvents) {
    emission.setValue(value, fireEvents);
  }
}
