/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.ShippingRoute.ShippingRouteReference;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DynamicRowEditor;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.ListButton;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeMooringEmissionEditor.ShipRouteDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorHandlerImpl.EnableButtons;

/**
 * Editor for single maritime mooring route at sea.
 */
class MaritimeMooringRowEditor extends DynamicRowEditor<MaritimeRoute> implements HasValueChangeHandlers<Integer> {

  private static final MaritimeMooringRowEditorUiBinder UI_BINDER = GWT.create(MaritimeMooringRowEditorUiBinder.class);

  interface MaritimeMooringRowEditorUiBinder extends UiBinder<Widget, MaritimeMooringRowEditor> { }

  @UiField(provided = true) ListButton maritimeRouteBox;
  RouteLeafValueEditor routeEditor;
  @UiField IntValueBox shipMovementsPerTimeUnit;
  private final ShipRouteDriver routesDriver = GWT.create(ShipRouteDriver.class);
  @UiField(provided = true) ListBoxEditor<TimeUnit> timeUnit;
  public MaritimeMooringRowEditor(final EventBus eventBus, final MapLayoutPanel map, final HelpPopupController hpC, final TakesValue<ShippingRoute> inlandRouteEditor,
      final ShippingRouteHandler maritimeRouteHandler, final EnableButtons enableButtons) {
    super(hpC, hpC.tt().ttDeleteButtonHelp());
    maritimeRouteBox = new ListButton(M.messages().shipMaritimeRouteShort());
    timeUnit = new ListBoxEditor<TimeUnit>() {
      @Override
      protected String getLabel(final TimeUnit value) {
        return M.messages().timeUnit(value);
      }

    };

    timeUnit.addItems(TimeUnit.values());
    getRow().insert(UI_BINDER.createAndBindUi(this), 0);
    routeEditor = new RouteLeafValueEditor(maritimeRouteBox, M.messages().shipMaritimeRouteSelect(),
        ShipRouteEditorSource.MARITIME_SHIP_ROUTENAME);
    final ShipRouteEditorSource maritimeRouteEditorSource = new ShipRouteEditorSource(eventBus, map, maritimeRouteBox,
        new ArrayList<ShippingNode>(), ShipRouteEditorSource.MARITIME_SHIP_ROUTENAME);
    routesDriver.initialize(ListEditor.of(maritimeRouteEditorSource));
    maritimeRouteEditorSource.setRouteChangedHandler(
        new ShipRouteEditorHandlerImpl(maritimeRouteBox, routeEditor, maritimeRouteHandler, enableButtons) {

          @Override
          public void onAdd(final ShippingRoute route) {
            super.onAdd(route);
            resetPlaceHolders();
          }

          @Override
          public void onSelect(final ShippingRoute route) {
            getShadowValue().setRoute(route);
            super.onSelect(route);
            ValueChangeEvent.fire(shipMovementsPerTimeUnit, shipMovementsPerTimeUnit.getValue());
          }

          @Override
          public void onRemove(final ShippingRoute route) {
            super.onRemove(route);
            // call setRoute after onRemove because setRoute will remove relation between route and ship
            // end then it's not possible to check if this route was only used by this ship.
            getShadowValue().setRoute(null);
          }

          @Override
          public Point onStartDrawNewRoute() {
            super.onStartDrawNewRoute();
            return inlandRouteEditor.getValue() == null ? null : inlandRouteEditor.getValue().getEndPoint();
          }

          @Override
          public ShippingRouteReference getRouteRef() {
            return getShadowValue();
          }
        });

    shipMovementsPerTimeUnit.addValueChangeHandler(new ValueChangeHandler<Integer>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Integer> event) {
        getShadowValue().setShipMovements(event.getValue(), TimeUnit.YEAR);
      }
    });
  }

  @Override
  protected boolean isEmpty() {
    return shipMovementsPerTimeUnit.getValue() == 0 && routeEditor.getValue() == null;
  }

  @Override
  protected void resetPlaceHolders() {
    super.resetPlaceHolders();
    shipMovementsPerTimeUnit.resetPlaceHolder();
  }

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    ensureDebugId(getElement(), baseID, TestID.PANEL_MARITIME_ROUTES);
    ensureDebugId(shipMovementsPerTimeUnit.getElement(), baseID, TestID.INPUT_SHIPPING_MOVEMENTS);
    ensureDebugId(maritimeRouteBox.getElement(), baseID, TestID.BUTTON_SHIPPING_MARITIME_ROUTES);
    ensureDebugId(timeUnit.getElement(), baseID, TestID.LIST_TIMEUNIT);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Integer> handler) {
    return shipMovementsPerTimeUnit.addValueChangeHandler(handler);
  }

  public void setRoutes(final List<ShippingRoute> routes) {
    routesDriver.edit(routes);
  }

  public void enableButtons(final boolean enabled) {
    maritimeRouteBox.setEnabled(enabled);
  }
}
