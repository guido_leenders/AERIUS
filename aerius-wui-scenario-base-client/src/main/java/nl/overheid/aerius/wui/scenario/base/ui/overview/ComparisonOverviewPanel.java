/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.deposition.ResultInfoType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.depositionspace.DepositionSpacePanel;
import nl.overheid.aerius.wui.main.ui.depositionspace.DepositionSpacePanel.DepositionSpaceDriver;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.ui.overview.CalculationResultInfoPanel.CalculationResultInfoDriver;

/**
 * Comparison panel used to display three types of comparison information.
 *
 * 2) Radius/N2000 calculation result differences 3) User input calculation result differences
 */
public class ComparisonOverviewPanel extends Composite {
  interface ComparisonOverviewPanelUiBinder extends UiBinder<Widget, ComparisonOverviewPanel> {}

  private static final ComparisonOverviewPanelUiBinder UI_BINDER = GWT.create(ComparisonOverviewPanelUiBinder.class);

  @UiField SwitchPanel labelResultDeckPanel;
  @UiField SwitchPanel comparisonDeckPanel;
  @UiField SwitchPanel comparisonResultDeckPanel;

  @UiField Label labelDepositionCompareList;
  @UiField Label labelDepositionSpaceCompareList;
  @UiField Label labelDepositionSpaceIndication;
  @UiField Label labelUnit;

  @UiField ListBox resultTypeListBox;

  @UiField(provided = true) CalculationPointDivTable calculatedCalculationPointDivTable;
  @UiField(provided = true) CalculationResultInfoPanel comparisonDepositionPanel;
  @UiField(provided = true) DepositionSpacePanel comparisonDepositionSpacePanel;

  private final CalculationResultInfoDriver driverInfo = GWT.create(CalculationResultInfoDriver.class);
  private final DepositionSpaceDriver driverDepositionSpace = GWT.create(DepositionSpaceDriver.class);

  private final UserContext userContext;
  private final PlaceController placeController;

  @Inject
  public ComparisonOverviewPanel(final HelpPopupController hpC, final CalculationResultInfoPanel comparisonDepositionPanel,
      final DepositionSpacePanel comparisonDepositionSpacePanel, final CalculationPointDivTable calculatedCalculationPointDivTable,
      final UserContext userContext, final PlaceController placeController) {
    this.comparisonDepositionPanel = comparisonDepositionPanel;
    this.calculatedCalculationPointDivTable = calculatedCalculationPointDivTable;
    this.comparisonDepositionSpacePanel = comparisonDepositionSpacePanel;
    this.userContext = userContext;
    this.placeController = placeController;

    initWidget(UI_BINDER.createAndBindUi(this));

    populateResultTypeListBox(true, true);
    driverInfo.initialize(comparisonDepositionPanel);
    driverDepositionSpace.initialize(comparisonDepositionSpacePanel);
    labelDepositionSpaceCompareList.setText(M.messages().situationDepositionListDepositionSpace());

    hpC.addWidget(resultTypeListBox, hpC.tt().ttResultTypeDropDown());

    resultTypeListBox.ensureDebugId(TestID.INPUT_DEPOSITION_COMPARISON_TYPE);
    comparisonDepositionPanel.ensureDebugId(TestID.DIV_TABLE_CALC_RES_DEPOSITION_COMPARISON);
    calculatedCalculationPointDivTable.ensureDebugId(TestID.DIV_TABLE_CALC_RES_CALCPOINT_COMPARISON);
  }

  /**
   * Single situation results.
   *
   * @param emissionResultKey EmissionResultKey
   * @param summary CalculationSummary
   * @param calcId int
   * @param situationName user name
   */
  public void setValue(final EmissionResultKey emissionResultKey, final CalculationSummary summary, final int calcId, final String situationName) {
    populateResultTypeListBox(false, summary.isDevelopmentResultAvailable());
    comparisonDeckPanel.showWidget(0);
    labelDepositionCompareList.setText(M.messages().situationDepositionListExplain(situationName));
    labelDepositionSpaceIndication.setText(M.messages().situationDepositionListDepositionSpaceIndication(summary.getDevelopmentSpaceSnapshotTime()));
    comparisonDepositionPanel.setComparisonResults(false);

    setData(summary, calcId, -1, emissionResultKey);
  }

  /**
   * Sets Comparison data.
   *
   * @param emissionResultKey
   * @param summary
   * @param calcId1
   * @param calcId2
   * @param situationOneName
   * @param situationTwoName
   */
  public void setValue(final EmissionResultKey emissionResultKey, final CalculationSummary summary, final int calcId1, final int calcId2,
      final String situation1Name, final String situation2Name) {
    final ScenarioBasePlace place = (ScenarioBasePlace) placeController.getWhere();
    populateResultTypeListBox(true, summary.isDevelopmentResultAvailable());
    comparisonDeckPanel.showWidget(0);
    final boolean isPriorityProjectUtilisation = JobType.PRIORITY_PROJECT_UTILISATION == place.getJobType();
    if (isPriorityProjectUtilisation) {
      labelDepositionCompareList.setText(M.messages().compareDepositionUtilisationListExplain(situation2Name, situation1Name));
    } else {
      labelDepositionCompareList.setText(M.messages().compareDepositionListExplain(situation1Name, situation2Name));
    }
    labelDepositionSpaceIndication.setText(M.messages().situationDepositionListDepositionSpaceIndication(summary.getDevelopmentSpaceSnapshotTime()));
    comparisonDepositionPanel.setComparisonResults(true);

    setData(summary, calcId1, calcId2, emissionResultKey);
  }

  /**
   * USER_INPUT comparison table.
   */
  public void setValue(final EmissionResultKey key, final List<AeriusPoint> calculationPoints, final boolean isDiff) {
    comparisonDeckPanel.showWidget(1);
    calculatedCalculationPointDivTable.setDiff(isDiff);
    calculatedCalculationPointDivTable.setEmissionResultKey(key);
    calculatedCalculationPointDivTable.asDataTable().setRowData(calculationPoints, true);
  }

  /**
   * @param summary
   * @param calcId1
   * @param calcId2
   * @param key2
   */
  private void setData(final CalculationSummary summary, final int calcId1, final int calcId2, final EmissionResultKey key) {
    comparisonDepositionPanel.setCalculationId(calcId1, calcId2);
    comparisonDepositionPanel.setEmissionResultKey(key);
    final List<CalculationAreaSummaryResult> areas = new ArrayList<>(summary.getSummaryResult().values());
    driverInfo.edit(areas);
    driverDepositionSpace.edit(areas);
  }

  @UiHandler("resultTypeListBox")
  void onDepositionComparisonListBoxChange(final ChangeEvent e) {
    final ResultInfoType displayType = ResultInfoType.valueOf(resultTypeListBox.getValue(resultTypeListBox.getSelectedIndex()));
    setSelectedType(displayType);
  }

  /*
   * @param isComparison
   *
   * @param isDepostionSpaceResultAvailable show DevelopmentSpace results
   */
  private void populateResultTypeListBox(final boolean isComparison, final boolean isDepostionSpaceResultAvailable) {
    final ScenarioBasePlace place = (ScenarioBasePlace) placeController.getWhere();
    final int selectedIndex = resultTypeListBox.getSelectedIndex();
    final ResultInfoType[] values;
    if (place.getJobType() == JobType.PRIORITY_PROJECT_UTILISATION) {
      values = ResultInfoType.getUtilisationValues();
    } else if (isComparison) {
      values = ResultInfoType.getComparisonValues();
    } else {
      values = ResultInfoType.getSingleValues();
    }

    resultTypeListBox.clear();
    for (final ResultInfoType val : values) {
      if (!((val == ResultInfoType.DEPOSITION_SPACE) && !isDepostionSpaceResultAvailable)) {
        resultTypeListBox.addItem(M.messages().calculationResultInfoType(val), val.name());
      }
    }

    if ((selectedIndex >= 0) && (selectedIndex <= (resultTypeListBox.getItemCount() - 1))) {
      resultTypeListBox.setSelectedIndex(selectedIndex);
    } else {
      // Set list to preferred values.
      selectListBoxValue(ResultInfoType.MAXIMUM_DEPOSITION, ResultInfoType.MAXIMUM_INCREASE);
    }
  }

  private void selectListBoxValue(final ResultInfoType... preferredValues) {
    for (int i = 0; i < resultTypeListBox.getItemCount(); i++) {
      final String value = resultTypeListBox.getValue(i);
      final ResultInfoType displayType = ResultInfoType.valueOf(value);
      if (Arrays.asList(preferredValues).contains(displayType)) {
        setSelectedItem(i, displayType);
        break;
      }
    }
  }

  private void setSelectedItem(final int i, final ResultInfoType infoType) {
    resultTypeListBox.setSelectedIndex(i);
    setSelectedType(infoType);
  }

  private void setSelectedType(final ResultInfoType displayType) {
    // set unit title
    switch (displayType) {
    case TOTAL_DEPOSITION:
      labelUnit.setText(M.messages().unitDepositionSingularY(userContext.getEmissionResultValueDisplaySettings().getDisplayType()));
      break;
    case PERCENTAGE_KDW:
      labelUnit.setText(M.messages().unitPercentageSingular());
      break;
    case DEPOSITION_SPACE:
      labelUnit.setText(M.messages().unitSurfaceSingular() + " " + M.messages().calculatorResultDepositionSpaceLabel());
      break;
    default:
      labelUnit.setText(M.messages().unitDepositionSingularHaY(userContext.getEmissionResultValueDisplaySettings().getDisplayType()));
      break;
    }
    // show table
    if (displayType == ResultInfoType.DEPOSITION_SPACE) {
      showDepositionSpace(displayType);
    } else {
      showDeposition(displayType);
    }
  }

  private void showDepositionSpace(final ResultInfoType displayType) {
    comparisonResultDeckPanel.showWidget(1);
    labelResultDeckPanel.showWidget(1);
    comparisonDepositionPanel.setResultDisplayType(displayType);
  }

  private void showDeposition(final ResultInfoType displayType) {
    comparisonResultDeckPanel.showWidget(0);
    labelResultDeckPanel.showWidget(0);
    comparisonDepositionPanel.setResultDisplayType(displayType);
  }

}
