/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import java.util.HashMap;
import java.util.HashSet;

import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.wui.geo.ReceptorFeature;

public class InfoVector extends Vector {
  private final static Style GEOMETRY_TYPE_STYLE = new Style();
  {
    GEOMETRY_TYPE_STYLE.setStrokeColor("#000");
    GEOMETRY_TYPE_STYLE.setFillOpacity(1d);
    GEOMETRY_TYPE_STYLE.setFillColor("#FFF000");
  }
  private static final Style HEXAGON_STYLE = new Style();
  {
    HEXAGON_STYLE.setFillColor("#F00");
    HEXAGON_STYLE.setFillOpacity(0.3d);
    HEXAGON_STYLE.setStrokeColor("#000");
  }

  private final HashMap<String, HashSet<WKTGeometry>> visibleGeometries = new HashMap<String, HashSet<WKTGeometry>>();
  private final HexagonZoomLevel zoomLevel1;

  public InfoVector(final String name, final HexagonZoomLevel zoomLevel1) {
    super(name);
    this.zoomLevel1 = zoomLevel1;
  }

  public void clear() {
    destroyFeatures();
    visibleGeometries.clear();
  }

  public void setGeometrySet(final String key, final HashSet<WKTGeometry> geoms) {
    destroyFeatures();

    visibleGeometries.put(key, geoms);

    drawGeometries();
  }

  public void removeGeometrySet(final String key) {
    destroyFeatures();

    visibleGeometries.remove(key);

    drawGeometries();
  }

  private void drawGeometries() {
    for (final HashSet<WKTGeometry> geoms : visibleGeometries.values()) {
      for (final WKTGeometry geom : geoms) {
        final VectorFeature vectorFeature = new VectorFeature(Geometry.fromWKT(geom.getWKT()));

        vectorFeature.setStyle(GEOMETRY_TYPE_STYLE);
        addFeature(vectorFeature);
      }
    }
  }

  public void drawReceptor(final AeriusPoint receptor) {
    destroyFeatures();

    // Draw the hexagon vector
    final ReceptorFeature feature = new ReceptorFeature(receptor, zoomLevel1);
    feature.setStyle(HEXAGON_STYLE);
    addFeature(feature);
    redraw();
  }
}
