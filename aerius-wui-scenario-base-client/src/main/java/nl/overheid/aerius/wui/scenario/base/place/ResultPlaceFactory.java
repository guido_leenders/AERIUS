/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import com.google.gwt.place.shared.Place;

import nl.overheid.aerius.wui.scenario.base.place.ResultPlace.ScenarioBaseResultPlaceState;

/**
 * Factory class to convert {@link ScenarioBaseResultPlaceState} to place object.
 *
 */
public final class ResultPlaceFactory {

  private ResultPlaceFactory() {
    // util
  }

  /**
   * Returns a place object given a result place state and a place object.
   * @param state state to get the place for
   * @param place place to base new place on
   * @return new place for given state
   */
  public static Place resultState2Place(final ScenarioBaseResultPlaceState state, final Place place) {
    final Place resultPlace;
    final ScenarioBaseResultPlaceState cs = state == null ? ScenarioBaseResultPlaceState.GRAPHICS : state;
    switch (cs) {
    case GRAPHICS:
      resultPlace = new ResultGraphicsPlace(place);
      break;
    case TABLE:
      resultPlace = new ResultTablePlace(place);
      break;
    case FILTER:
      resultPlace = new ResultFilterPlace(place);
      break;
    case OVERVIEW:
    default:
      resultPlace = new ResultOverviewPlace(place);
      break;
    }
    return resultPlace;
  }
}
