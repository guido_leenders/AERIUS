/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.LodgingStackType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.source.FarmFodderMeasure;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DynamicEditorSource;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.NameTextMachine;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.farmlodging.FarmRowEditor.FarmLodgingCategorySelection;

/**
 * Editor for FarmEmissionSource object.
 */
class FarmLodgingStandardEmissionEditor extends Composite implements InputEditor<FarmLodgingStandardEmissions>,
ValueAwareEditor<FarmLodgingStandardEmissions>, FarmLodgingCategorySelection {

  private static final FarmStandardEmissionEditorUiBinder UI_BINDER = GWT.create(FarmStandardEmissionEditorUiBinder.class);

  interface FarmLodgingStandardEmissionDriver extends SimpleBeanEditorDriver<FarmLodgingStandardEmissions, FarmLodgingStandardEmissionEditor> {
  }

  interface FarmStandardEmissionEditorUiBinder extends UiBinder<Widget, FarmLodgingStandardEmissionEditor> {
  }

  @UiField FlowPanel panel;
  @UiField FlowPanel lodgingPanel;
  @UiField(provided = true) SuggestBox<FarmLodgingCategory> categoryEditor;
  @UiField(provided = true) SystemDefinitionEditor systemDefinitionEditor;
  @UiField IntValueBox amountEditor;
  @UiField FlowPanel lodgingAdditionalPanel;
  @UiField FlowPanel lodgingReductivePanel;
  @UiField FlowPanel fodderMeasuresPanel;
  @UiField FlowPanel ravWarningPanelStacking;
  @UiField ListBox lodgingStackTypeListBox;

  ListEditor<FarmAdditionalLodgingSystem, FarmAdditionalRowEditor> lodgingAdditionalEditor;
  ListEditor<FarmReductiveLodgingSystem, FarmReductiveRowEditor> lodgingReductiveEditor;
  ListEditor<FarmFodderMeasure, FarmFodderMeasureRowEditor> fodderMeasuresEditor;

  private Integer lastAmount;

  private static final TextMachine<FarmLodgingCategory> FARM_NAMER = new NameTextMachine<FarmLodgingCategory>() {
    @Override
    public String asError(final String input) {
      return M.messages().ivFarmNoValidRavCode(input);
    }
  };

  private final ValueChangeHandler<FarmAdditionalLodgingSystemCategory> additionValueChangeHandler =
      new DefaultValueChangeHandler<>();
  private final ValueChangeHandler<FarmReductiveLodgingSystemCategory> reductiveValueChangeHandler =
      new DefaultValueChangeHandler<>();
  private final ValueChangeHandler<FarmLodgingFodderMeasureCategory> fodderMeasureValueChangeHandler =
      new DefaultValueChangeHandler<>();

  public FarmLodgingStandardEmissionEditor(final EventBus eventBus, final SectorCategories categories, final HelpPopupController hpC) {
    categoryEditor = new SuggestBox<FarmLodgingCategory>(new FarmSuggestOracle<>(
        categories.getFarmLodgingCategories().getFarmLodgingSystemCategories()), FARM_NAMER) {
      @Override
      protected void setValue(final FarmLodgingCategory value, final boolean fireEvents, final boolean soft) {
        super.setValue(value, fireEvents, soft);
        setTitle(value == null ? "" : value.getAnimalCategory().getDescription());
      }
    };
    systemDefinitionEditor = new SystemDefinitionEditor();

    initWidget(UI_BINDER.createAndBindUi(this));

    systemDefinitionEditor.addValidator(new Validator<FarmLodgingSystemDefinition>() {
      @Override
      public String validate(final FarmLodgingSystemDefinition value) {
        return value == null ? M.messages().ivFarmNoValidBWLCode() : null;
      }
    });

    categoryEditor.addValueChangeHandler(new ValueChangeHandler<FarmLodgingCategory>() {
      @Override
      public void onValueChange(final ValueChangeEvent<FarmLodgingCategory> event) {
        systemDefinitionEditor.updateSystemDefinitionList(event.getValue(), null);
        refreshWarnings();
      }
    });

    hpC.addWidget(categoryEditor, hpC.tt().ttFarmLodgingCategory());
    hpC.addWidget(systemDefinitionEditor, hpC.tt().ttFarmLodgingSystemDefinition());
    hpC.addWidget(amountEditor, hpC.tt().ttFarmLodgingAmount());

    amountEditor.addValueChangeHandler(new ValueChangeHandler<Integer>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Integer> event) {
        final Integer amount = event.getValue();
        for (final FarmAdditionalRowEditor editor : lodgingAdditionalEditor.getEditors()) {
          editor.updateAmount(lastAmount, amount);
        }
        lastAmount = amount;
      }
    });
    amountEditor.addValidator(new NotZeroValidator<Integer>() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivFarmNotEmptyAmount();
      }
    });

    lodgingAdditionalEditor = ListEditor.of(
        new DynamicEditorSource<FarmAdditionalLodgingSystem, FarmAdditionalRowEditor>(lodgingAdditionalPanel) {
          @Override
          public void onRemove(final FarmAdditionalLodgingSystem value) {
            lodgingAdditionalEditor.getList().remove(value);
            refreshWarnings();
          }

          @Override
          protected FarmAdditionalRowEditor createRowWidget() {
            final FarmAdditionalRowEditor re = new FarmAdditionalRowEditor(categories, hpC, FarmLodgingStandardEmissionEditor.this);
            re.ensureDebugId(lodgingAdditionalPanel.getWidgetCount() + "-" + TestID.ADDITONAL_MEASURE);
            re.addValueChangeHandler(additionValueChangeHandler);
            return re;
          }

          @Override
          protected FarmAdditionalLodgingSystem createObject() {
            return new FarmAdditionalLodgingSystem();
          }
        });

    lodgingReductiveEditor = ListEditor.of(
        new DynamicEditorSource<FarmReductiveLodgingSystem, FarmReductiveRowEditor>(lodgingReductivePanel) {
          @Override
          public void onRemove(final FarmReductiveLodgingSystem value) {
            lodgingReductiveEditor.getList().remove(value);
            refreshWarnings();
          }

          @Override
          protected FarmReductiveRowEditor createRowWidget() {
            final FarmReductiveRowEditor re = new FarmReductiveRowEditor(categories, hpC, FarmLodgingStandardEmissionEditor.this);
            re.ensureDebugId(lodgingAdditionalPanel.getWidgetCount() + "-" + TestID.REDUCTIVE_MEASURE);
            re.addValueChangeHandler(reductiveValueChangeHandler);
            return re;
          }

          @Override
          protected FarmReductiveLodgingSystem createObject() {
            return new FarmReductiveLodgingSystem();
          }
        });

    fodderMeasuresEditor = ListEditor.of(
        new DynamicEditorSource<FarmFodderMeasure, FarmFodderMeasureRowEditor>(fodderMeasuresPanel) {
          @Override
          public void onRemove(final FarmFodderMeasure value) {
            fodderMeasuresEditor.getList().remove(value);
            refreshWarnings();
          }

          @Override
          protected FarmFodderMeasureRowEditor createRowWidget() {
            final FarmFodderMeasureRowEditor re = new FarmFodderMeasureRowEditor(categories, hpC, FarmLodgingStandardEmissionEditor.this);
            re.ensureDebugId(fodderMeasuresPanel.getWidgetCount() + "-" + TestID.FODDER_MEASURE);
            re.addValueChangeHandler(fodderMeasureValueChangeHandler);
            return re;
          }

          @Override
          protected FarmFodderMeasure createObject() {
            return new FarmFodderMeasure();
          }
        });

    lodgingStackTypeListBox.addItem(M.messages().farmLodgingSelectType(), "");
    lodgingStackTypeListBox.addItem(M.messages().farmLodgingStackType(LodgingStackType.ADDITIONAL_MEASURE),
        LodgingStackType.ADDITIONAL_MEASURE.name());
    lodgingStackTypeListBox.addItem(M.messages().farmLodgingStackType(LodgingStackType.REDUCING_MEASURE),
        LodgingStackType.REDUCING_MEASURE.name());
    lodgingStackTypeListBox.addItem(M.messages().farmLodgingStackType(LodgingStackType.FODDER_MEASURE),
        LodgingStackType.FODDER_MEASURE.name());

    panel.ensureDebugId(TestID.GUI_COLLAPSEPANEL_FARM);
    lodgingStackTypeListBox.ensureDebugId(TestID.LODGING_STACK_TYPE);
  }

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    categoryEditor.getValueBox().ensureDebugId(baseID + "-" + TestID.RAV_SUGGEST_BOX);
    systemDefinitionEditor.ensureDebugId(baseID + "-" + TestID.BWL_SUGGEST_BOX);
    amountEditor.ensureDebugId(baseID + "-" + TestID.AMOUNT_BOX);
  }

  protected boolean isEmpty() {
    return categoryEditor.getValue() == null || amountEditor.getValue() == 0;
  }

  @UiHandler("lodgingStackTypeListBox")
  void onLodgingStackTypeListBox(final ChangeEvent event) {
    final String selectedValue = ((ListBox) event.getSource()).getSelectedValue();
    if (LodgingStackType.ADDITIONAL_MEASURE.name().equals(selectedValue)) {
      final FarmAdditionalLodgingSystem farmAdditionalLodgingSystem = new FarmAdditionalLodgingSystem();
      farmAdditionalLodgingSystem.setAmount(amountEditor.getValue());
      lodgingAdditionalEditor.getList().add(farmAdditionalLodgingSystem);
    } else if (LodgingStackType.REDUCING_MEASURE.name().equals(selectedValue)) {
      lodgingReductiveEditor.getList().add(new FarmReductiveLodgingSystem());
    } else if (LodgingStackType.FODDER_MEASURE.name().equals(selectedValue)) {
      fodderMeasuresEditor.getList().add(new FarmFodderMeasure());
    }
    lodgingStackTypeListBox.setSelectedIndex(0); // reset choice
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    // No-op
  }

  @Override
  public void resetPlaceholders() {
    amountEditor.resetPlaceHolder();
    for (final FarmAdditionalRowEditor editor : lodgingAdditionalEditor.getEditors()) {
      editor.resetPlaceHolders();
    }
    refreshWarnings();
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return null;
  }

  @Override
  public void setDelegate(final EditorDelegate<FarmLodgingStandardEmissions> delegate) {
    // nothing to do
  }

  @Override
  public void flush() {
    // nothing to do
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // nothing to do
  }


  @Override
  public FarmLodgingCategory getCurrentLodgingCategory() {
    return categoryEditor.getValue();
  }

  @Override
  public void setValue(final FarmLodgingStandardEmissions value) {
    systemDefinitionEditor.updateSystemDefinitionList(value.getCategory(), value.getSystemDefinition());
  }

  private void refreshWarnings() {
    boolean warning = false;
    for (final FarmAdditionalRowEditor editor : lodgingAdditionalEditor.getEditors()) {
      warning |= editor.refreshWarning(editor.getSelectedValue());
    }
    for (final FarmReductiveRowEditor editor : lodgingReductiveEditor.getEditors()) {
      warning |= editor.refreshWarning(editor.getSelectedValue());
    }
    for (final FarmFodderMeasureRowEditor editor : fodderMeasuresEditor.getEditors()) {
      warning |= editor.refreshWarning(editor.getSelectedValue());
    }
    ravWarningPanelStacking.setVisible(warning);
  }

  private class DefaultValueChangeHandler<T> implements ValueChangeHandler<T> {
    @Override
    public void onValueChange(final ValueChangeEvent<T> event) {
      refreshWarnings();
    }
  }

}
