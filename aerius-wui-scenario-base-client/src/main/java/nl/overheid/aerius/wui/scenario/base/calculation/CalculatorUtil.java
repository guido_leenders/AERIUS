/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.calculation;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.domain.ScenarioUtil;
import nl.overheid.aerius.wui.scenario.base.place.ResultGraphicsPlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;

/**
 * Utility class for starting a calculation.
 */
public final class CalculatorUtil {

  private CalculatorUtil() {
    // Util class.
  }

  /**
   * Start a calculation, send notification to the user, and goes to the progress page.
   *
   * @param eventBus eventBus to notify user in the process
   * @param placeController place controller to go to new page just before starting process
   * @param processor processor that manages the calculation process on the client
   * @param appContext application context
   * @param place current place
   */
  public static void calculate(final EventBus eventBus, final PlaceController placeController,  final ScenarioBaseCalculationProcessor processor,
      final ScenarioBaseAppContext<?, ?> appContext, final ScenarioBasePlace place) {
    final CalculationSetOptions options = appContext.getCalculationOptions().copy();
    final ScenarioBaseUserContext userContext = appContext.getUserContext();
    // Check for non-calculations and notify
    // - No calculation points if custom calculation is selected
    // - No total emission
    // - More to come later (will delegate to util)
    final CalculatedScenario scenario = ScenarioUtil.toCalculatedScenario(userContext.getScenario(), place);
    if (options.getCalculationType() == CalculationType.CUSTOM_POINTS) {
      if (appContext.getUserContext().getCalculationPoints().isEmpty()) {
        NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationStartNoValidCustomCalculationPoints());
        return;
      }
    } else {
      scenario.getCalculationPoints().clear();
    }
    if (substanceCheckAndCorrect(eventBus, scenario, appContext.getUserContext().getEmissionValueKey(),
        appContext.getUserContext().getEmissionResultKey(), placeController, place)) {
      appContext.getUserContext().setCalculatedScenario(null); // reset current set scenario
      appContext.setCalculationSummary(null);
      scenario.setOptions(options);
      for (final Calculation calculation : scenario.getCalculations()) {
        calculation.setYear(appContext.getUserContext().getEmissionValueKey().getYear());
      }
      NotificationUtil.broadcastMessage(eventBus,
          M.messages().notificationCalculationStarted(options.getCalculationType(),
              FormatUtil.formatDistanceWithUnit(options.getCalculateMaximumRange())));
      // NOTE: If ResultGraphicsPlace not would be goto-ed to the progressbars won't be initialized and not be correctly updated.
      final ResultGraphicsPlace pp = new ResultGraphicsPlace(place);
      if (scenario instanceof CalculatedComparison) {
        // if a comparison situation always show the difference tab.
        pp.setSituation(Situation.COMPARISON);
      }

      placeController.goTo(pp);
      // Call start after goTo to let new page receive events triggered by processor.
      appContext.getUserContext().setCalculatedScenario(scenario);
      processor.start(scenario);
    }
  }

  /**
   * Check if there are any emissions. If no emissions are present no calculation can be done. This is reported to the user.
   * If the current selected substance has no emission, but there are emissions the substance is set to the substances with emissions. This is also
   * reported to the user.
   * @param scenario the scenario to scan for emissions
   * @param eventBus the eventbus to report status messages to the user
   * @param placeController
   * @param place
   * @param emissionResultKey
   * @return true if there are emissions and calculation can be done.
   */
  private static boolean substanceCheckAndCorrect(final EventBus eventBus, final CalculatedScenario scenario, final EmissionValueKey key,
      final EmissionResultKey resultKey, final PlaceController placeController, final ScenarioBasePlace place) {
    // Try to get the detect the substance for which emissions are available and check if it is the same as the current selected substance
    final Substance detectSubstance = dectectSubstance(key, scenario, place.getTheme());
    if (detectSubstance == null) {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationCalculationStartNoValidSubstance());
      return false;
    } else if (detectSubstance != key.getSubstance()) {
      // Persist the corrected key (if any) and notify
      eventBus.fireEvent(new EmissionResultKeyChangeEvent(EmissionResultKey.safeValueOf(detectSubstance, resultKey.getEmissionResultType())));
      NotificationUtil.broadcastMessage(eventBus, M.messages().notificationCalculationStartSubstanceCorrected(detectSubstance.getName()));
    }
    return true;
  }

  /**
   * Checks if any of the sources have results and changes the substance to actual calculate based on if there are sources with emissions. The
   * algorithm used is:
   * <ul>
   * <li>If PM10 is selected and PM10 emissions are available, the returned substance is PM10.
   * <li>If NOx, NH3, or NH3+NOx is selected, the returned substance follows the availability of emissions.
   * <li>If no emissions are available for the selected substance, but emissions are available for another substance, then the other substance
   *     is returned.
   * </ul>
   * @param key current key to be supposed to be calculated
   * @param scenario scenario to check sources for
   * @param calculatorTheme
   * @return substance for which are emissions or null if no emission
   */
  static Substance dectectSubstance(final EmissionValueKey key, final CalculatedScenario scenario, final ScenarioBaseTheme calculatorTheme) {
    boolean pm10Valid = false;
    boolean noxValid = false;
    boolean nh3Valid = false;

    final int year = key.getYear();
    final EmissionValueKey pm10Key = new EmissionValueKey(year, Substance.PM10);
    final EmissionValueKey noxKey = new EmissionValueKey(year, Substance.NOX);
    final EmissionValueKey nh3Key = new EmissionValueKey(year, Substance.NH3);

    for (final Calculation calcESL : scenario.getCalculations()) {
      for (final EmissionSource src : calcESL.getSources()) {
        pm10Valid = pm10Valid || src.getEmission(pm10Key) != 0;
        noxValid = noxValid || src.getEmission(noxKey) != 0;
        nh3Valid = nh3Valid || src.getEmission(nh3Key) != 0;

        if (pm10Valid && noxValid && nh3Valid) {
          break;
        }
      }
    }
    final Substance substance;
    if (key.getSubstance() == Substance.PM10 && pm10Valid && calculatorTheme.getSubstances().contains(Substance.PM10)) {
      substance = Substance.PM10;
    } else if (nh3Valid && noxValid && calculatorTheme.getSubstances().contains(Substance.NH3)
        && calculatorTheme.getSubstances().contains(Substance.NOX)) {
      substance = Substance.NOXNH3;
    } else if (nh3Valid && calculatorTheme.getSubstances().contains(Substance.NH3)) {
      substance = Substance.NH3;
    } else if (noxValid && calculatorTheme.getSubstances().contains(Substance.NOX)) {
      substance = Substance.NOX;
    } else if (pm10Valid && calculatorTheme.getSubstances().contains(Substance.PM10)) {
      substance = Substance.PM10;
    } else {
      substance = null;
    }
    return substance;
  }
}
