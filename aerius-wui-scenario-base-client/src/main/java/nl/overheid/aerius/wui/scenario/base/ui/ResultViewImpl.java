/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace.ScenarioBaseResultPlaceState;

@Singleton
public class ResultViewImpl extends Composite implements ResultView {
  private static ResultViewImplUiBinder UI_BINDER = GWT.create(ResultViewImplUiBinder.class);

  interface ResultViewImplUiBinder extends UiBinder<Widget, ResultViewImpl> {}

  public class RadioButtonResources implements RadioButtonContentResource<ScenarioBaseResultPlaceState> {
    @Override
    public String getRadioButtonText(final ScenarioBaseResultPlaceState value) {
      final String text;
      switch (value) {
      case FILTER:
        text = M.messages().resultPlaceFilter();
        break;
      case GRAPHICS:
        text = M.messages().resultPlaceGraphics();
        break;
      case TABLE:
        text = M.messages().resultPlaceTable();
        break;
      case OVERVIEW:
      default:
        text = M.messages().resultPlaceOverview();
        break;
      }
      return text;
    }
  }

  @UiField(provided = true) RadioButtonGroup<ScenarioBaseResultPlaceState> resultSelectionField;

  @UiField SimplePanel contentPanel;
  @UiField SimplePanel buttonPanel;

  private Presenter presenter;
  private final boolean enableHabitatFilter;

  @Inject
  public ResultViewImpl(final HelpPopupController hpC, final ScenarioBaseAppContext<?, ?> context) {
    resultSelectionField = new RadioButtonGroup<ResultPlace.ScenarioBaseResultPlaceState>(new RadioButtonResources());
    resultSelectionField.ensureDebugId(TestID.RESULT_GROUPBUTTONS);

    resultSelectionField.addButtons(ResultPlace.ACTIVE_VALUES);

    resultSelectionField.setEnabled(ScenarioBaseResultPlaceState.FILTER, false);
    resultSelectionField.setEnabled(ScenarioBaseResultPlaceState.TABLE, false);
    resultSelectionField.setEnabled(ScenarioBaseResultPlaceState.GRAPHICS, context.isCalculationRunning());

    this.enableHabitatFilter = context.enableHabitatFilter();

    initWidget(UI_BINDER.createAndBindUi(this));
    // TODO Making the radio button generic breaks all this:
    //    hpC.addWidget(graphicsPlace, hpC.tt().ttResultGraphicsTab());
    //    hpC.addWidget(tablePlace, hpC.tt().ttResultTableTab());
    //    hpC.addWidget(filterPlace, hpC.tt().ttResultFilterTab());
  }

  @UiHandler("resultSelectionField")
  void onOverviewPlaceChange(final ValueChangeEvent<ScenarioBaseResultPlaceState> e) {
    presenter.goTo(e.getValue());
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    buttonPanel.setWidget(presenter.getCalculationButtons());
  }

  @Override
  public AcceptsOneWidget getContentPanel() {
    return contentPanel;
  }

  @Override
  public void setTab(final ResultPlace.ScenarioBaseResultPlaceState tabState) {
    resultSelectionField.setValue(tabState);
  }

  @Override
  public void calculationStarted() {
    resultSelectionField.setEnabled(ScenarioBaseResultPlaceState.GRAPHICS, true);
  }

  @Override
  public void calculationCancelled() {
    // no-op
  }

  @Override
  public void calculationFinished() {
    resultSelectionField.setValue(ScenarioBaseResultPlaceState.TABLE, true);
  }

  @Override
  public void calculationSummaryAvailable(final boolean status) {
    resultSelectionField.setEnabled(ScenarioBaseResultPlaceState.TABLE, status);
    resultSelectionField.setEnabled(ScenarioBaseResultPlaceState.FILTER, enableHabitatFilter && status);
  }

  @Override
  public String getTitleText() {
    return contentPanel.getWidget() instanceof HasTitle ? ((HasTitle) contentPanel.getWidget()).getTitleText() : "";
  }
}
