/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.calculators;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleConsumptionSpecification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;

/**
 * Editor class for Offroad emission calculation based on Consumption.
 */
class OffRoadEmissionConsumptionEditor extends OffRoadEmissionBaseEditor<OffRoadVehicleConsumptionSpecification>
    implements Editor<OffRoadVehicleConsumptionSpecification> {
  interface OffRoadEmissionConsumptionDriver
      extends SimpleBeanEditorDriver<OffRoadVehicleConsumptionSpecification, OffRoadEmissionConsumptionEditor> {}

  interface OffRoadEmissionCalculatorWidgetUiBinder extends UiBinder<Widget, OffRoadEmissionConsumptionEditor> {}

  private static final OffRoadEmissionCalculatorWidgetUiBinder UI_BINDER = GWT.create(OffRoadEmissionCalculatorWidgetUiBinder.class);

  @UiField IntValueBox usage;
  @UiField IntValueBox energyEfficiency;
  @UiField DoubleValueBox emissionFactor;

  /**
   * Constructor. initializes the widget and makes the checkBox into a toggle button
   */
  public OffRoadEmissionConsumptionEditor(final ScenarioBaseContext context) {
    super(context);
    initWidget(UI_BINDER.createAndBindUi(this));

    ensureDebugId(TestID.OFF_ROAD_CALCULATOR_CONSUMPTION);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    usage.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_FUEL_USAGE);
    energyEfficiency.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_EFFICIENCY);
    emissionFactor.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_EF_FUEL);
  }

  @UiHandler({"usage", "energyEfficiency", "emissionFactor"})
  void onChangeValueFuelUsage(final ChangeEvent e) {
    refresh();
  }

  @UiHandler({"usage", "energyEfficiency", "emissionFactor"})
  void onChangeValueFuelUsage(final FocusEvent e) {
    refresh();
  }

  @UiHandler({"usage", "energyEfficiency", "emissionFactor"})
  void onChangeValueFuelUsage(final KeyUpEvent e) {
    refresh();
  }

  @Override
  protected OffRoadVehicleConsumptionSpecification getOffRoadVehicleSpecification() {
    final OffRoadVehicleConsumptionSpecification spec = new OffRoadVehicleConsumptionSpecification();
    spec.setUsage(usage.getValue());
    spec.setEmissionFactor(emissionFactor.getValue());
    spec.setEnergyEfficiency(energyEfficiency.getValue());

    usage.asEditor().validate();
    emissionFactor.asEditor().validate();
    energyEfficiency.asEditor().validate();

    return spec;
  }

  @Override
  protected void setDefaultFuelProperties(final FuelTypeProperties value) {
    energyEfficiency.setValue(value.getEnergyEfficiencyInGramPerKWH());
    emissionFactor.setValue(value.getEmissionFactor());

    refresh();
  }
}