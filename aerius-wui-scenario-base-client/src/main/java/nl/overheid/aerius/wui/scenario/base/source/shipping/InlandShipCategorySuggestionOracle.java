/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;

/**
 * Suggestion box oracle for inland ship categories in relation to the waterway.
 * The suggestions returned are based on the following conditions:
 * <ul>
 * <li>If a waterway is set only ships that are valid for the given route are returned.</li>
 * <li>If no waterway is set all ships are returned.</li>
 * </ul>
 */
class InlandShipCategorySuggestionOracle extends ShipCategorySuggestionOracle<InlandShippingCategory> {

  private InlandWaterwayCategory inlandWaterwayCategory;
  private final InlandShippingCategories inlandShippingCategories;

  /**
   * @param inlandShippingCategories The list of categories to choose from.
   */
  public InlandShipCategorySuggestionOracle(final InlandShippingCategories inlandShippingCategories) {
    super(inlandShippingCategories.getShipCategories());
    this.inlandShippingCategories = inlandShippingCategories;
  }

  @Override
  protected Suggestion getSuggestion(final String query, final InlandShippingCategory cat) {
    final boolean validShipTypeForWaterway =
        inlandWaterwayCategory == null || inlandShippingCategories.isValidCombination(cat, inlandWaterwayCategory);
    return validShipTypeForWaterway ? super.getSuggestion(query, cat) : null;
  }

  public void setInlandWaterwayCategory(final InlandWaterwayCategory inlandWaterwayCategory) {
    this.inlandWaterwayCategory = inlandWaterwayCategory;
  }
}
