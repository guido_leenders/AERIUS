/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceEditorVisitor.DriverEditorCombo;
import nl.overheid.aerius.wui.scenario.base.source.GenericEmissionEditor.GenericEmissionsDriver;
import nl.overheid.aerius.wui.scenario.base.source.core.EmissionsEditor;
import nl.overheid.aerius.wui.scenario.base.source.farmlodging.FarmEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.offroad.OffRoadMobileEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.offroad.OffRoadMobileEmissionEditor.OffRoadMobileEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.plan.PlanEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.plan.PlanEmissionEditor.PlanEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.road.RoadEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.road.RoadEmissionEditor.RoadEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandMooringEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandMooringEmissionEditor.InlandMooringEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandRouteEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandRouteEmissionEditor.InlandRouteEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeMooringEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeMooringEmissionEditor.MaritimeMooringEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeRouteEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.shipping.MaritimeRouteEmissionEditor.MaritimeRouteEmissionDriver;

/**
 * Visitor pattern to select the editor to use.
 */
@SuppressWarnings("rawtypes")
class EmissionSourceEditorVisitor implements EmissionSourceVisitor<DriverEditorCombo> {

  @SuppressWarnings("unchecked")
  static class DriverEditorCombo<S extends EmissionSource, E extends EmissionsEditor<S> & Editor<S>, D extends SimpleBeanEditorDriver> {
    E editor;
    D driver;

    public DriverEditorCombo(final E editor, final D driver) {
      this.editor = editor;
      this.driver = driver;
      driver.initialize(editor);
    }

    public D getDriver() {
      return driver;
    }

    public E getEditor() {
      return editor;
    }
  }

  private final DriverEditorCombo<GenericEmissionSource, GenericEmissionEditor, GenericEmissionsDriver> genericDE;
  private final DriverEditorCombo<FarmEmissionSource, FarmEmissionEditor, FarmEmissionEditor.FarmLodgingEmissionDriver> farmDE;
  private final DriverEditorCombo<OffRoadMobileEmissionSource, OffRoadMobileEmissionEditor, OffRoadMobileEmissionDriver> offRoadMobileDE;
  private final DriverEditorCombo<PlanEmissionSource, PlanEmissionEditor, PlanEmissionDriver> planDE;
  private final DriverEditorCombo<SRM2EmissionSource, RoadEmissionEditor, RoadEmissionDriver> roadDE;
  private final DriverEditorCombo<MaritimeMooringEmissionSource, MaritimeMooringEmissionEditor, MaritimeMooringEmissionDriver> maritimeMooringDE;
  private final DriverEditorCombo<MaritimeRouteEmissionSource, MaritimeRouteEmissionEditor, MaritimeRouteEmissionDriver> maritimeRouteDE;
  private final DriverEditorCombo<InlandMooringEmissionSource, InlandMooringEmissionEditor, InlandMooringEmissionDriver> inlandMooringDE;
  private final DriverEditorCombo<InlandRouteEmissionSource, InlandRouteEmissionEditor, InlandRouteEmissionDriver> inlandRouteDE;

  public EmissionSourceEditorVisitor(final EventBus eventBus, final SourceMapPanel map, final CalculatorContext context,
      final CalculatorServiceAsync service, final HelpPopupController hpC) {
    final SectorCategories categories = context.getCategories();

    genericDE = new DriverEditorCombo<>(new GenericEmissionEditor(eventBus, context, categories, hpC),
        (GenericEmissionsDriver) GWT.create(GenericEmissionsDriver.class));
    farmDE = new DriverEditorCombo<>(new FarmEmissionEditor(eventBus, context, categories, service, hpC),
        (FarmEmissionEditor.FarmLodgingEmissionDriver) GWT.create(FarmEmissionEditor.FarmLodgingEmissionDriver.class));
    roadDE = new DriverEditorCombo<>(new RoadEmissionEditor(eventBus, categories, service, hpC),
        (RoadEmissionDriver) GWT.create(RoadEmissionDriver.class));
    offRoadMobileDE = new DriverEditorCombo<>(new OffRoadMobileEmissionEditor(eventBus, context, service, hpC),
        (OffRoadMobileEmissionDriver) GWT.create(OffRoadMobileEmissionDriver.class));
    planDE = new DriverEditorCombo<>(new PlanEmissionEditor(eventBus, context, categories, service, hpC),
        (PlanEmissionDriver) GWT.create(PlanEmissionDriver.class));
    maritimeMooringDE = new DriverEditorCombo<>(new MaritimeMooringEmissionEditor(eventBus, map, context, service, hpC),
        (MaritimeMooringEmissionDriver) GWT.create(MaritimeMooringEmissionDriver.class));
    maritimeRouteDE = new DriverEditorCombo<>(new MaritimeRouteEmissionEditor(eventBus, map, context, service, hpC),
        (MaritimeRouteEmissionDriver) GWT.create(MaritimeRouteEmissionDriver.class));
    inlandMooringDE = new DriverEditorCombo<>(new InlandMooringEmissionEditor(eventBus, categories, map, service, hpC),
        (InlandMooringEmissionDriver) GWT.create(InlandMooringEmissionDriver.class));
    inlandRouteDE = new DriverEditorCombo<>(new InlandRouteEmissionEditor(eventBus, map, context, service, hpC),
        (InlandRouteEmissionDriver) GWT.create(InlandRouteEmissionDriver.class));
  }

  public boolean isGenericEditor(final EmissionsEditor currentEmissionsEditor) {
    return currentEmissionsEditor == genericDE.getEditor();
  }

  public boolean isRoadEditor(final EmissionsEditor currentEmissionsEditor) {
    return currentEmissionsEditor == roadDE.getEditor();
  }

  @Override
  public DriverEditorCombo visit(final GenericEmissionSource emissionSource) throws AeriusException {
    return genericDE;
  }

  @Override
  public DriverEditorCombo visit(final FarmEmissionSource emissionSource) throws AeriusException {
    return farmDE;
  }

  @Override
  public DriverEditorCombo visit(final SRM2EmissionSource emissionSource) throws AeriusException {
    return roadDE;
  }

  @Override
  public DriverEditorCombo visit(final PlanEmissionSource emissionSource) throws AeriusException {
    return planDE;
  }

  @Override
  public DriverEditorCombo visit(final OffRoadMobileEmissionSource emissionSource) throws AeriusException {
    return offRoadMobileDE;
  }

  @Override
  public DriverEditorCombo visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    return inlandMooringDE;
  }

  @Override
  public DriverEditorCombo visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    return inlandRouteDE;
  }

  @Override
  public DriverEditorCombo visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    return maritimeMooringDE;
  }

  @Override
  public DriverEditorCombo visit(final MaritimeRouteEmissionSource emissionSource) throws AeriusException {
    return maritimeRouteDE;
  }

  @Override
  public DriverEditorCombo visit(final SRM2NetworkEmissionSource emissionSource) throws AeriusException {
    throw new AeriusException(Reason.INTERNAL_ERROR);
  }

  public void onEnsureDebugId(final String baseID) {
    genericDE.getEditor().ensureDebugId(baseID);
    farmDE.getEditor().ensureDebugId(baseID);
    offRoadMobileDE.getEditor().ensureDebugId(baseID);
    maritimeMooringDE.getEditor().ensureDebugId(baseID);
    maritimeRouteDE.getEditor().ensureDebugId(baseID);
    inlandMooringDE.getEditor().ensureDebugId(baseID);
    inlandRouteDE.getEditor().ensureDebugId(baseID);
    planDE.getEditor().ensureDebugId(baseID);
    roadDE.getEditor().ensureDebugId(baseID);
  }

}
