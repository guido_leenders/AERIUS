/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.List;

import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.layer.Markers;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;

import nl.overheid.aerius.geo.WktUtil;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.MarkerContainer;
import nl.overheid.aerius.wui.geo.SingleMarker;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Container class that manages the showing of a begin and end markers on a shipping inland route when the ship is being edited.
 */
public final class InlandRouteMarkerContainer implements MarkerContainer<EmissionSource> {
  private static final String ROUTE_BEGIN = "A";
  private static final String ROUTE_END = "B";

  private RouteMarkerItem beginRouteMarker;
  private RouteMarkerItem endRouteMarker;
  private Markers markersLayer;

  /**
   * Clears the markers and triggers an event to redraw the markers.
   */
  @Override
  public void clear() {
    if (beginRouteMarker != null) {
      markersLayer.removeMarker(beginRouteMarker.getMarker());
      beginRouteMarker = null;
    }
    if (endRouteMarker != null) {
      markersLayer.removeMarker(endRouteMarker.getMarker());
      endRouteMarker = null;
    }
  }

  /**
   * Initializes the class and binds the container to an actual layer and list of markers on the layer.
   * @param vectorLayer vectorLayer unused here
   * @param markersLayer layer to display routes begin and end markers
   */
  @Override
  public void init(final Vector vectorLayer, final Markers markersLayer) {
    this.markersLayer = markersLayer;
  }

  @Override
  public void add(final EmissionSource src) {
    // no-op not used.
  }

  @Override
  public void remove(final EmissionSource src) {
    // no-op not used.
  }

  @Override
  public void setMaritimeRoutes(final List<ShippingRoute> maritimeRoutes) {
    // no-op not used.
  }

  /**
   * Set the markers on the map given the emission source, but only if the source is a line.
   * @param es Emission source to use data from.
   */
  public void setMarkers(final EmissionSource es) {
    clear();
    if (markersLayer != null && es.getGeometry() != null && es.getGeometry().getType() == WKTGeometry.TYPE.LINE) {
      beginRouteMarker = new RouteMarkerItem(es, true);
      markersLayer.addMarker(beginRouteMarker.getMarker());
      endRouteMarker = new RouteMarkerItem(es, false);
      markersLayer.addMarker(endRouteMarker.getMarker());
      markersLayer.redraw();
    }
  }

  private static Point getPoint(final String wktString, final boolean first) {
    final LineString line = LineString.narrowToLineString(LineString.fromWKT(wktString).getJSObject());
    return WktUtil.fromPoint(org.gwtopenmaps.openlayers.client.geometry.Point.narrowToPoint(
        line.getComponent(first ? 0 : line.getNumberOfComponents() - 1)));
  }

  static class RouteMarkerItem extends SingleMarker<EmissionSource> {

    private static final long serialVersionUID = 4415391355204971299L;

    RouteMarkerItem(final EmissionSource ship, final boolean first) {
      super(getPoint(ship.getGeometry().getWKT(), first), ship, TYPE.SHIPPING_ROUTE_BOUNDARY);
      final DivElement label = Document.get().createDivElement();
      label.setInnerText(first ? ROUTE_BEGIN : ROUTE_END);
      getElement().addClassName(R.css().textOverflowEllipsis());
      getElement().addClassName(R.css().shipRouteMarker());
      getElement().appendChild(label);
      // point is top/left, but marker should show in the middle, there correcting the offset to the middel of the marker
      getElement().getStyle().setPosition(Position.ABSOLUTE);
      getElement().getStyle().setTop(-R.css().shippingRouteMarkerSize() / 2.0, Unit.PX);
      getElement().getStyle().setLeft(-R.css().shippingRouteMarkerSize() / 2.0, Unit.PX);
    }
  }
}
