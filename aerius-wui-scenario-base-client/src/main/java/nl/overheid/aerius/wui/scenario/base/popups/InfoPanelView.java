/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import com.google.gwt.user.client.ui.RequiresResize;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.CalculationInfo;
import nl.overheid.aerius.shared.domain.info.ReceptorInfo;
import nl.overheid.aerius.wui.main.widget.AttachedPopupContent;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl.Presenter;

/**
 * View for InfoPanel popup. The popup shows information related to the point
 * clicked on the map by the user.
 */
public interface InfoPanelView extends AttachedPopupContent, RequiresResize {

  /**
   * Called when the user clicked on the map, to set the state the data is being
   * retrieved from the server.
   * @param rec AeriusPoint clicked by the user
   */
  void dataPending(AeriusPoint rec);

  /**
   * Called when the calculation info for the selected receptor has changed (or situation selection has changed).
   * @param calculationInfo CalculationInfo from the server
   */
  void updateCalculationInfo(CalculationInfo calculationInfo);

  /**
   * Data retrieved from the server for the receptor point.
   * @param pi ReceptorInfo from the server
   * @param scenario the scenario
   * @param place
   */
  void updateAreaInfo(ReceptorInfo pi, CalculatedScenario scenario, ScenarioBasePlace place);

  /**
   * Bind the view to the popup it's shown on.
   * @param pp popup that shows the data
   */
  @Override
  void setPopup(AttachedPopupPanel pp);

  /**
   * Used when deposition info has been invalidated.
   */
  void clearEmissionResultInfo();

  /**
   * @param presenter Set the presenter for this view.
   */
  void setPresenter(Presenter presenter);
}
