/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;

/**
 * Viewer for emissions of multiple farm lodgings at a single location.
 */
public class FarmEmissionViewer extends EmissionViewer<FarmEmissionSource> {
  private static final FarmEmissionsViewerUiBinder UI_BINDER = GWT.create(FarmEmissionsViewerUiBinder.class);

  public interface FarmEmissionDriver extends SimpleBeanEditorDriver<FarmEmissionSource, FarmEmissionViewer> { }

  interface FarmEmissionsViewerUiBinder extends UiBinder<Widget, FarmEmissionViewer> { }

  @UiField(provided = true) SimpleDivTable<FarmLodgingEmissions> table;
  @UiField(provided = true) SimpleWidgetFactory<FarmLodgingEmissions> column;
  @UiField Label header;
  @UiField Label headerRight;

  public FarmEmissionViewer() {
    table = new SimpleDivTable<FarmLodgingEmissions>();
    table.ensureDebugId(TestID.TABLE_SOURCES_SECTOR_VIEWER);

    column = new SimpleWidgetFactory<FarmLodgingEmissions>() {
      @Override
      public Widget createWidget(final FarmLodgingEmissions object) {
        final FarmEmissionViewerRow row = new FarmEmissionViewerRow();
        row.setValue(object);
        return row;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    header.setText(M.messages().emissionSourceViewerFarmAnimals());
    headerRight.setText(M.messages().emissionSourceViewerFarmAnimalsUnit());
  }

  @Override
  public void setValue(final FarmEmissionSource value) {
    table.clear();

    table.addRowData(value.getEmissionSubSources());
  }

}
