/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.ShippingRoute.ShippingRouteReference;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DynamicEditorSource;
import nl.overheid.aerius.wui.main.ui.editor.DynamicListEditor;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.ListButton;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.geo.ShippingRouteContainer;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorHandlerImpl.EnableButtons;

/**
 * Widget for inputting single ship data.
 */
class MaritimeMooringInputEditor extends Composite implements Editor<MooringMaritimeVesselGroup>, InputEditor<MooringMaritimeVesselGroup>,
    HasClickHandlers, ValueAwareEditor<MooringMaritimeVesselGroup> {

  private static final int JUST_A_RANDOM_TIMEOUT = 350;

  interface MaritimeMooringInputDriver extends SimpleBeanEditorDriver<MooringMaritimeVesselGroup, MaritimeMooringInputEditor> {
  }

  interface MaritimeMooringInputEditorUiBinder extends UiBinder<Widget, MaritimeMooringInputEditor> {
  }

  interface MaritimeMooringInputEditorEventBinder extends EventBinder<MaritimeMooringInputEditor> {
  }

  private static final MaritimeMooringInputEditorUiBinder UI_BINDER = GWT.create(MaritimeMooringInputEditorUiBinder.class);

  private final MaritimeMooringInputEditorEventBinder eventBinder = GWT.create(MaritimeMooringInputEditorEventBinder.class);

  private static final TextMachine<MaritimeShippingCategory> TEXT_MACHINE = new ShippingCategoryTextMachine<>();

  @UiField @Ignore TextBox name;
  @UiField(provided = true) SuggestBox<MaritimeShippingCategory> category;
  @UiField IntValueBox numberOfShipsPerTimeUnit;
  @UiField IntValueBox residenceTime;
  @UiField(provided = true) ListBoxEditor<TimeUnit> timeUnit;
  @UiField(provided = true) ListButton inlandRouteBox;
  @UiField FlowPanel maritimeRoutesLabel;
  @UiField FlowPanel maritimeRoutesPanel;
  @UiField @Ignore Button submitButton;
  ValidatedValueBoxEditor<String> nameEditor;

  RouteLeafValueEditor inlandRouteEditor;
  DynamicListEditor<MaritimeRoute, MaritimeMooringRowEditor> maritimeRoutesEditor;

  private final ValueChangeHandler<Integer> shipMovementChangeHandler = new ValueChangeHandler<Integer>() {
    @Override
    public void onValueChange(final ValueChangeEvent<Integer> event) {
      // timer to give selenium time to clear and enter a new value....yes it's horrible....
      new Timer() {
        @Override
        public void run() {
          onShipNumbersValueChange();
        }
      }.schedule(JUST_A_RANDOM_TIMEOUT);
    }
  };

  private final EnableButtons buttonEnable = new EnableButtons() {
    @Override
    public void enableButtons(final boolean enable) {
      MaritimeMooringInputEditor.this.enableButtons(enable);
    }
  };

  private final ShipRouteEditorSource inlandRouteEditorSource;
  private ShippingRouteHandler maritimeRouteHandler;
  private Point sourcePoint;

  private EditorDelegate<MooringMaritimeVesselGroup> delegate;

  public MaritimeMooringInputEditor(final EventBus eventBus, final SourceMapPanel map, final CalculatorContext context,
      final HelpPopupController hpC) {
    final ShipCategorySuggestionOracle<MaritimeShippingCategory> oracle =
        new ShipCategorySuggestionOracle<>(context.getCategories().getMaritimeShippingCategories());
    category = new SuggestBox<>(oracle, TEXT_MACHINE);

    inlandRouteBox = new ListButton(M.messages().shipRouteShort());
    timeUnit = new ListBoxEditor<TimeUnit>() {
      @Override
      protected String getLabel(final TimeUnit value) {
        return M.messages().timeUnit(value);
      }
    };
    initWidget(UI_BINDER.createAndBindUi(this));

    inlandRouteEditor = new RouteLeafValueEditor(inlandRouteBox, M.messages().shipInlandRouteSelect(),
        ShipRouteEditorSource.INLAND_WATERS_SHIP_ROUTE_NAME) {
      @Override
      public void setValue(final ShippingRoute value) {
        super.setValue(value);
        maritimeRoutesPanel.setVisible(value != null);
        maritimeRoutesLabel.setVisible(value != null);
      }
    };
    inlandRouteEditorSource = new ShipRouteEditorSource(eventBus, map.getPanel(), inlandRouteBox,
        context.getCategories().getShippingSnappableNodes(), ShipRouteEditorSource.INLAND_WATERS_SHIP_ROUTE_NAME);
    final double searchDistance = context.getSetting(SharedConstantsEnum.MARITIME_MOORING_INLAND_ROUTE_SHIPPING_NODE_SEARCH_DISTANCE);
    inlandRouteEditorSource.setShippingNodeSearchDistance(searchDistance);

    final DynamicEditorSource<MaritimeRoute, MaritimeMooringRowEditor> maritimeRouteEditorSource =
        new DynamicEditorSource<MaritimeRoute, MaritimeMooringRowEditor>(maritimeRoutesPanel) {

      @Override
      protected MaritimeMooringRowEditor createRowWidget() {
        final MaritimeMooringRowEditor maritimeRowEditor =
            new MaritimeMooringRowEditor(eventBus, map.getPanel(), hpC, inlandRouteEditor, maritimeRouteHandler, buttonEnable);
        maritimeRowEditor.ensureDebugId(String.valueOf(maritimeRoutesPanel.getWidgetCount()));
        maritimeRowEditor.addValueChangeHandler(shipMovementChangeHandler);
        maritimeRowEditor.setRoutes(((ShippingRouteContainer) map.getMartimeMooringRouteContainer()).getMaritimeRoutes());
        return maritimeRowEditor;
      }

      @Override
      protected MaritimeRoute createObject() {
        return new MaritimeRoute();
      }

      @Override
      public void onRemove(final MaritimeRoute value) {
        value.setRoute(null); // clear the route before removing it
        maritimeRoutesEditor.getList().remove(value);
        onShipNumbersValueChange();
      }
    };
    maritimeRoutesEditor = new DynamicListEditor<MaritimeRoute, MaritimeMooringRowEditor>(maritimeRouteEditorSource) {
      @Override
      public MaritimeMooringRowEditor addEmptyRow() {
        final MaritimeMooringRowEditor row = super.addEmptyRow();
        row.resetPlaceHolders();
        return row;
      }
    };

    nameEditor = new ShippingValidatedValueBoxEditor(name);
    numberOfShipsPerTimeUnit.addValueChangeHandler(new ValueChangeHandler<Integer>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Integer> event) {
        onShipNumbersValueChange();
      }
    });
    numberOfShipsPerTimeUnit.addValidator(new NotZeroValidator<Integer>() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivShipNumberOfShipsNotEmpty();
      }
    });
    residenceTime.addValidator(new NotZeroValidator<Integer>() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivShipResidenceTimeNotEmpty();
      }
    });

    timeUnit.addItems(TimeUnit.values());
    eventBinder.bindEventHandlers(this, eventBus);
    // Help widgets
    hpC.addWidget(name, hpC.tt().ttShipName());
    hpC.addWidget(category, hpC.tt().ttShipType());
    hpC.addWidget(numberOfShipsPerTimeUnit, hpC.tt().ttShipAmount());
    hpC.addWidget(residenceTime, hpC.tt().ttShipResidenceTime());
    hpC.addWidget(timeUnit, hpC.tt().ttShippingTimeUnit());
    hpC.addWidget(inlandRouteBox, hpC.tt().ttShipRoute());
    hpC.addWidget(submitButton, hpC.tt().ttShipSave());
    onEnsureDebugId("");
  }

  /**
   * Called when the user changes the content of the number of visits or any number of ship movements on sea routes.
   */
  private void onShipNumbersValueChange() {
    if (inlandRouteEditor.getValue() == null) {
      return;
    }
    final int diff = calculateDiff();
    if (diff > 0) {
      final List<MaritimeMooringRowEditor> eRoutes = findRoutesZero();
      if (!eRoutes.isEmpty()) {
        eRoutes.get(0).shipMovementsPerTimeUnit.setValue(diff, false);
      }
      if (findEmptyRoutes().isEmpty()) {
        maritimeRoutesEditor.addEmptyRow();
      }
    }
  }

  /**
   * Calculates the differences between the number of visits * 2 - Total number of sea ship movements.
   * If a editor row has no route selected it is ignored.
   * @return
   */
  private int calculateDiff() {
    int shipMovements = 0;
    final List<MaritimeMooringRowEditor> editors = maritimeRoutesEditor.getEditors();
    for (final MaritimeMooringRowEditor mr : editors) {
      if (mr.routeEditor.getValue() != null) { // only count rows which have a route selected.
        shipMovements += mr.shipMovementsPerTimeUnit.getValue();
      }
    }
    return numberOfShipsPerTimeUnit.getValue() * 2 - shipMovements;
  }

  /**
   * Returns a list of editors for which the routes are set, but the ship movements is 0.
   * @return
   */
  private List<MaritimeMooringRowEditor> findRoutesZero() {
    final List<MaritimeMooringRowEditor> emptyRoutes = new ArrayList<>();
    for (final MaritimeMooringRowEditor mr : maritimeRoutesEditor.getEditors()) {
      if (mr.routeEditor.getValue() != null && mr.shipMovementsPerTimeUnit.getValue() == 0) {
        emptyRoutes.add(mr);
      }
    }
    return emptyRoutes;
  }

  /**
   * Returns a list of editors for which the routes are not set or the routes are set.
   * @return
   */
  private List<MaritimeMooringRowEditor> findEmptyRoutes() {
    final List<MaritimeMooringRowEditor> emptyRoutes = new ArrayList<>();
    for (final MaritimeMooringRowEditor mr : maritimeRoutesEditor.getEditors()) {
      if (mr.routeEditor.getValue() == null) {
        emptyRoutes.add(mr);
      }
    }
    return emptyRoutes;
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    name.ensureDebugId(TestID.INPUT_SHIPPING_NAME);
    category.getValueBox().ensureDebugId(TestID.INPUT_SHIPPING_CATEGORY);
    numberOfShipsPerTimeUnit.ensureDebugId(TestID.INPUT_SHIPPING_AMOUNT);
    residenceTime.ensureDebugId(TestID.INPUT_SHIPPING_RESIDENCETIME);
    inlandRouteBox.ensureDebugId(TestID.BUTTON_SHIPPING_ROUTES);
    timeUnit.ensureDebugId(TestID.LIST_TIMEUNIT);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  public ShipRouteEditorSource getInlandRouteEditorSource() {
    return inlandRouteEditorSource;
  }

  public void reeditMaritimeRoutes(final List<ShippingRoute> routes) {
    for (final MaritimeMooringRowEditor editor : maritimeRoutesEditor.getEditors()) {
      editor.setRoutes(routes);
    }
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    sourcePoint = source;
  }

  /**
   * Force empty content on the IntValueBox widgets when a new data object is edited.
   * Otherwise the initial value will be 0 and the placeholder text won't be visible.
   */
  @Override
  public void resetPlaceholders() {
    numberOfShipsPerTimeUnit.resetPlaceHolder();
    residenceTime.resetPlaceHolder();
    maritimeRoutesEditor.resetPlaceholders();
  }

  private void enableButtons(final boolean enable) {
    inlandRouteBox.setEnabled(enable);
    for (final MaritimeMooringRowEditor editor : maritimeRoutesEditor.getEditors()) {
      editor.enableButtons(enable);
    }
    submitButton.setEnabled(enable);
  }

  /**
   * Initializes the inland route handler and sets the sea route container.
   */
  public void setRoutesContainer(final ShippingRouteHandler inlandRouteHandler, final ShippingRouteHandler maritimeRouteHandler) {
    inlandRouteEditorSource.setRouteChangedHandler(new ShipRouteEditorHandlerImpl(inlandRouteBox, inlandRouteEditor,
        inlandRouteHandler, buttonEnable) {
      @Override
      public Point onStartDrawNewRoute() {
        super.onStartDrawNewRoute();
        return sourcePoint;
      }

      @Override
      public ShippingRouteReference getRouteRef() {
        return null;
      }
    });

    this.maritimeRouteHandler = maritimeRouteHandler;
  }

  @Override
  public void setDelegate(final EditorDelegate<MooringMaritimeVesselGroup> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    final int diff = calculateDiff();
    if (diff != 0 && findEmptyRoutes().size() != maritimeRoutesEditor.getEditors().size()) {
      delegate.recordError(M.messages().ivShipMaritimeNumberDontMatch(), diff, numberOfShipsPerTimeUnit.getParent());
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void setValue(final MooringMaritimeVesselGroup value) {
    // no-op
  }
}
