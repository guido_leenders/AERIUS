/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.builder.shared.TableCellBuilder;
import com.google.gwt.dom.builder.shared.TableRowBuilder;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safecss.shared.SafeStylesUtils;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.AbstractHeaderOrFooterBuilder;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.Header;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.cellview.client.TextHeader;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.view.client.ProvidesKey;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SectorEmissionSummary;
import nl.overheid.aerius.wui.main.domain.SectorImageUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Celltable for showing emission differences per sector.
 */
public class ComparisonDataGrid extends CellTable<SectorEmissionSummary> {

  /**
   * The HTML templates used to render the cell.
   */
  interface Templates extends SafeHtmlTemplates {
    /**
     * The template for this Cell, which includes styles and a value.
     *
     * @param styles the styles to include in the style attribute of the div
     * @param value the safe value. Since the value type is {@link SafeHtml},
     *          it will not be escaped before including it in the template.
     *          Alternatively, you could make the value type String, in which
     *          case the value would be escaped.
     * @return a {@link SafeHtml} instance
     */
    @SafeHtmlTemplates.Template("<div class=\"{0}\" style=\"{1}\">{2}</div>")
    SafeHtml cell(String className, SafeStyles style, SafeHtml value);

    /**
     * The template for this Cell, which includes styles and a value.
     *
     * @param styles the styles to include in the style attribute of the div
     * @param value the safe value. Since the value type is {@link SafeHtml},
     *          it will not be escaped before including it in the template.
     *          Alternatively, you could make the value type String, in which
     *          case the value would be escaped.
     * @return a {@link SafeHtml} instance
     */
    @SafeHtmlTemplates.Template("<div class=\"{0}\">{1}</div>")
    SafeHtml cell(String className, SafeHtml value);

  }

  static Templates templates = GWT.create(Templates.class);

  private static final int DEFAULT_PAGE_SIZE = 25;

  // The total should be 320 px
  private static final String COLUMN_WIDTH_ICON = "28px";
  private static final String COLUMN_WIDTH_NUMBER_OF_SOURCES = "100px";
  private static final String COLUMN_WIDTH_EMISSION = "100px";

  private EmissionValueKey key;

  public ComparisonDataGrid() {
    super(DEFAULT_PAGE_SIZE, R.cellTableResources(), new ProvidesKey<SectorEmissionSummary>() {
      @Override
      public Object getKey(final SectorEmissionSummary item) {
        return item.getSector();
      }
    });
    setWidth("100%", true);

    // Sector icon column
    final Column<SectorEmissionSummary, ImageResource> columnIcon =
        new Column<SectorEmissionSummary, ImageResource>(new ImageResourceCell()) {
      @Override
      public ImageResource getValue(final SectorEmissionSummary object) {
        return SectorImageUtil.getSectorImageResource(object.getSector().getProperties().getIcon());
      }
    };
    addColumn(columnIcon, M.messages().compareSourcesHeaderType());
    setColumnWidth(columnIcon, COLUMN_WIDTH_ICON);

    // Label column
    final Column<SectorEmissionSummary, SectorEmissionSummary> columnLabel =
        new Column<SectorEmissionSummary, SectorEmissionSummary>(
            new AbstractCell<SectorEmissionSummary>() {
              @Override
              public void render(final com.google.gwt.cell.client.Cell.Context context, final SectorEmissionSummary value,
                  final SafeHtmlBuilder sb) {
                final SafeStyles bgc = SafeStylesUtils.fromTrustedString("padding-left: 10px;");
                final String label = value.getSector().getDescription();

                sb.append(templates.cell(R.css().textOverflowEllipsis(), bgc, SafeHtmlUtils.fromString(String.valueOf(label))));
              }
            }) {
      @Override
      public SectorEmissionSummary getValue(final SectorEmissionSummary object) {
        return object;
      }
    };
    addColumn(columnLabel);

    // number of sources column
    final TextColumn<SectorEmissionSummary> columnNumberOfSources = new TextColumn<SectorEmissionSummary>() {
      @Override
      public String getValue(final SectorEmissionSummary object) {
        return (object.getNumberOfSources() > 0 ? "+" : "") + object.getNumberOfSources();
      }

      @Override
      public String getCellStyleNames(final Context context, final SectorEmissionSummary object) {
        return super.getCellStyleNames(context, object) + " " + (object.getNumberOfSources() <= 0 ? "" : R.css().comparisonListNegative());
      }
    };
    columnNumberOfSources.setCellStyleNames(R.css().comparisonListNumberSources());
    final Header<String> numberOfSourcesHeader = new TextHeader(M.messages().compareSourcesHeaderNumberOfSources()) {

      @Override
      public void render(final Context context, final SafeHtmlBuilder sb) {
        sb.append(templates.cell(R.css().comparisonListNumberSources(), SafeHtmlUtils.fromString(getValue())));
      }

    };
    addColumn(columnNumberOfSources, numberOfSourcesHeader);
    setColumnWidth(columnNumberOfSources, COLUMN_WIDTH_NUMBER_OF_SOURCES);

    // Emission column
    final TextColumn<SectorEmissionSummary> columnEmission = new TextColumn<SectorEmissionSummary>() {
      @Override
      public String getValue(final SectorEmissionSummary object) {
        return getKey().getSubstance() == Substance.NOXNH3 ? "" : getEmissionString(object.getEmission(getKey()));
      }

      @Override
      public String getCellStyleNames(final Context context, final SectorEmissionSummary object) {
        return super.getCellStyleNames(context, object) + " " + (getValue(object).startsWith("-") ? R.css().comparisonListNegative() : "");
      }
    };
    columnEmission.setCellStyleNames(R.css().comparisonListEmission());

    final Header<String> emissionHeader = new TextHeader(M.messages().compareSourcesHeaderEmission()) {

      @Override
      public void render(final Context context, final SafeHtmlBuilder sb) {
        sb.append(templates.cell(R.css().comparisonListEmission(), SafeHtmlUtils.fromString(getValue())));
      }
    };

    addColumn(columnEmission, emissionHeader);
    setColumnWidth(columnEmission, COLUMN_WIDTH_EMISSION);

    setFooterBuilder(new CustomFooterBuilder(this));
  }

  public void setKey(final EmissionValueKey key) {
    this.key = key;
  }

  public EmissionValueKey getKey() {
    return this.key;
  }

  private String getEmissionString(final double emission) {
    return (emission > 0 ? "+" : "") + FormatUtil.formatEmissionWithUnitSmart(emission);
  }

  private class CustomFooterBuilder extends AbstractHeaderOrFooterBuilder<SectorEmissionSummary> {

    private final ComparisonDataGrid grid;

    public CustomFooterBuilder(final ComparisonDataGrid grid) {
      super(grid, true);
      this.grid = grid;
    }

    @Override
    protected boolean buildHeaderOrFooterImpl() {
      final List<SectorEmissionSummary> items = grid.getVisibleItems();
      if (!items.isEmpty()) {
        if (key.getSubstance().equals(Substance.NOXNH3)) {
          buildCombineFooterLabels();
          buildCombineFooterValues();
        } else {
          buildSingleFooterLabels(key.getSubstance().getName());
          buildSingleFooterValues();
        }
      }
      return true;
    }

    private void buildCombineFooterLabels() {
      final String footerStyle = grid.getResources().style().footer();

      final TableRowBuilder tr = startRow();
      buildCellTitle(footerStyle, 2, tr, "");
      for (final EmissionValueKey entry : key.hatch()) {
        buildCellValue(footerStyle, 1, tr, entry.getSubstance().getName());
      }
      tr.endTR();
    }

    private void buildCombineFooterValues() {
      final String footerStyle = grid.getResources().style().footer();

      final TableRowBuilder tr = startRow();
      buildCellTitle(footerStyle, 2, tr, M.messages().compareSourcesFooterLabel());
      for (final EmissionValueKey entry : key.hatch()) {
        buildCellValue(footerStyle, 1, tr, getTotalEmissionLabel(entry));
      }
      tr.endTR();
    }

    private void buildSingleFooterLabels(final String label) {
      final String footerStyle = grid.getResources().style().footer();

      final TableRowBuilder tr = startRow();
      buildCellTitle(footerStyle, 2, tr, "");
      buildCellValue(footerStyle, 2, tr, label);
      tr.endTR();
    }

    private void buildSingleFooterValues() {
      final String footerStyle = grid.getResources().style().footer();

      final TableRowBuilder tr = startRow();
      buildCellTitle(footerStyle, 2, tr, M.messages().compareSourcesFooterLabel());
      buildCellValue(footerStyle, 2, tr, getTotalEmissionLabel(key));
      tr.endTR();
    }

    /**
     *
     * @param footerStyle String Resources for footer.
     * @param collSpan int column to span.
     * @param tr TableRowBuilder to add the column.
     * @param label String to add to column.
     * @return
     */
    private void buildCellTitle(final String footerStyle, final int collSpan, final TableRowBuilder tr, final String label) {
      final TableCellBuilder th = tr.startTH().colSpan(collSpan).className(footerStyle).align(HasHorizontalAlignment.ALIGN_LEFT.getTextAlignString());
      th.text(label);
      th.endTH();
    }
    /**
     *
     * @param footerStyle String Resources for footer.
     * @param collSpan int column to span.
     * @param tr TableRowBuilder to add the column.
     * @param label String to add to column.
     * @return
     */
    private void buildCellValue(final String footerStyle, final int collSpan, final TableRowBuilder tr, final String label) {
      final TableCellBuilder th = tr.startTH().colSpan(collSpan).className(footerStyle + " " + R.css().comparisonListEmission())
          .align(HasHorizontalAlignment.ALIGN_RIGHT.getTextAlignString());
      th.text(label);
      th.endTH();
    }

    /**
     * Returns the emission totals as string.
     *
     * @param substance Substance(s) to output.
     * @return
     */
    private String getTotalEmissionLabel(final EmissionValueKey key) {
      final String totalEmissionStr;
      final List<SectorEmissionSummary> items = grid.getVisibleItems();

      if (items.isEmpty()) {
        totalEmissionStr = "";
      } else {
        double total = 0;
        for (final SectorEmissionSummary item : items) {
          total += item.getEmission(key);
        }
        totalEmissionStr = total > 0 ? getEmissionString(total) : "";
      }
      return totalEmissionStr;
    }

  }
}
