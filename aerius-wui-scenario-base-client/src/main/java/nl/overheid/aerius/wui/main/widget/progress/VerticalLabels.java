/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.progress;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.scaling.ScalingUtil;

class VerticalLabels extends Composite {
  interface VerticalLabelsUiBinder extends UiBinder<Widget, VerticalLabels> {
  }

  private static VerticalLabelsUiBinder uiBinder = GWT.create(VerticalLabelsUiBinder.class);

  public interface CustomStyle extends CssResource {
    String label();
  }

  @UiField CustomStyle style;
  @UiField FlowPanel labels;
  @UiField HTMLPanel verticalLabel;

  private final ScalingUtil scaler;
  private final ScenarioBaseUserContext userContext;
  private double overallMaxEmissionResult;

  public VerticalLabels(final ScalingUtil scaler, final ScenarioBaseUserContext userContext) {
    this.scaler = scaler;
    this.userContext = userContext;

    initWidget(uiBinder.createAndBindUi(this));

    setVerticalLabelText();
  }

  public void setEmissionResultType(final EmissionResultType emissionResultType) {
    setVerticalLabelText();
  }

  public void updateLabels() {
    updateLabels(overallMaxEmissionResult);
  }

  public void updateLabels(final double overallMaxEmissionResult) {
    this.overallMaxEmissionResult = overallMaxEmissionResult;

    labels.clear();

    final int numLabels = 6;

    for (int i = 0; i < numLabels; i++) {
      final double emissionResult = scaler.scaleReverse((double) i / (double) (numLabels - 1),
          FormatUtil.convertEmissionResult(userContext.getEmissionResultKey().getEmissionResultType(), overallMaxEmissionResult,
              userContext.getEmissionResultValueDisplaySettings()));
      final HTMLPanel panel = new HTMLPanel(FormatUtil.toFixed(emissionResult, 1));
      panel.addStyleName(style.label());
      labels.insert(panel, 0);
    }
  }

  private void setVerticalLabelText() {
    verticalLabel.getElement().setInnerText(M.messages().emissionResultQuantityAndUnit(userContext.getEmissionResultKey().getEmissionResultType(),
        userContext.getEmissionResultValueDisplaySettings().getDisplayType()));
  }
}
