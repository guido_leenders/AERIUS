/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.context.CalculatorUserContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.scenario.base.events.ImportedImaerFeaturesChangeEvent;
import nl.overheid.aerius.wui.scenario.base.place.NetworkDetailPlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;

/**
 * The presenter for details of a network.
 */
public class NetworkDetailPresenter extends AbstractActivity implements NetworkDetailView.Presenter {

  private static final int UNUSED_ESLID = -9999;

  interface NetworkDetailPresenterEventBinder extends EventBinder<NetworkDetailPresenter> { }

  private final NetworkDetailPresenterEventBinder eventBinder = GWT.create(NetworkDetailPresenterEventBinder.class);

  private EventBus eventBus;
  private final NetworkDetailView view;
  private final PlaceController placeController;
  private final CalculatorUserContext userContext;
  private final NetworkDetailPlace place;
  private final ArrayList<EmissionSource> emissionSources;
  private EmissionSource selectedEmissionSource;

  @Inject
  public NetworkDetailPresenter(final PlaceController placeController, final NetworkDetailView view, final SituationView parentView,
      final CalculatorUserContext userContext, @Assisted final NetworkDetailPlace place, @Assisted final ArrayList<EmissionSource> emissionSources) {
    this.placeController = placeController;
    this.view = view;
    this.userContext = userContext;
    this.place = place;
    this.emissionSources = emissionSources;
    view.setOkButtonVisible(true);
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    panel.setWidget(view);
    this.eventBus = eventBus;
    view.setPresenter(this);
    view.setData(emissionSources, userContext.getEmissionValueKey());
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @Override
  public void selectedEmissionSource(final EmissionSource selected) {
    view.setDetail(selected);
    if (selectedEmissionSource != null) {
      eventBus.fireEvent(new EmissionSourceChangeEvent(UNUSED_ESLID, selectedEmissionSource, CHANGE.REMOVE, true));
    }
    eventBus.fireEvent(new EmissionSourceChangeEvent(UNUSED_ESLID, selected, CHANGE.ADD, true));

    selectedEmissionSource = selected;
  }

  @Override
  public void okButton() {
    placeController.goTo(new SourcesPlace(place));
  }

  @EventHandler
  public void onImportedImaerFeaturesChangeEvent(final ImportedImaerFeaturesChangeEvent event) {
    view.setData(event.getValue(), userContext.getEmissionValueKey());
    refresh();
  }

  @EventHandler
  void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent event) {
    refresh();
  }

  @EventHandler
  void onYearChange(final YearChangeEvent event) {
    refresh();
  }

  protected void refresh() {
    if (!emissionSources.isEmpty()) {
      final EmissionSource emissionSource = emissionSources.get(0);
      view.updateEmissionValueKey(emissionSource, userContext.getEmissionValueKey());
    }
  }
}
