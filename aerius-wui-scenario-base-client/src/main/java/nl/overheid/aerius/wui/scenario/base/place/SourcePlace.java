/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import java.util.Map;

/**
 * Abstract place for emissions source in source. Not to be used directly.
 */
public abstract class SourcePlace extends ScenarioBasePlace {

  private static final String SOURCE_ID = "sourceId";

  /**
   * Tokenizer for {@link CalculatorPlace}.
   */
  public abstract static class Tokenizer<P extends SourcePlace> extends ScenarioBasePlace.Tokenizer<P> {

    @Override
    protected void updatePlace(final Map<String, String> tokens, final P place) {
      super.updatePlace(tokens, place);
      place.setSourceId(intValue(tokens, SOURCE_ID));
    }

    @Override
    protected void setTokenMap(final P place, final Map<String, String> tokens) {
      super.setTokenMap(place, tokens);
      put(tokens, SOURCE_ID, place.getSourceId());
    }
  }

  private int sourceId;

  public SourcePlace(final SourcePlace currentPlace) {
    this(currentPlace.sourceId, currentPlace);
  }

  public SourcePlace(final int sourceId, final ScenarioBasePlace currentPlace) {
    super(currentPlace);
    this.sourceId = sourceId;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && sourceId == ((SourcePlace) obj).sourceId
        && super.equals(obj);
  }

  public int getSourceId() {
    return sourceId;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + 17 * sourceId;
    result = 31 * result + super.hashCode();
    return result;
  }

  public void setSourceId(final int sourceId) {
    this.sourceId = sourceId;
  }

  @Override
  public String toString() {
    return "SourcePlace [sourceId=" + sourceId + "], " + super.toString();
  }
}
