/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.calculators;

import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleSpecification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * TODO Inject, inject, inject
 */
public class OffRoadEmissionCalculator extends AbstractInputCalculator<Double, OffRoadVehicleSpecification> {
  private final OffRoadEmissionEditorWrapper offRoadEmissionCalculatorWidget;

  public OffRoadEmissionCalculator(final ScenarioBaseContext context) {
    offRoadEmissionCalculatorWidget = new OffRoadEmissionEditorWrapper(context);
    offRoadEmissionCalculatorWidget.ensureDebugId(TestID.OFF_ROAD_CALCULATOR);
  }

  @Override
  protected Widget createDialog() {
    offRoadEmissionCalculatorWidget.setValue(super.getValue());
    return offRoadEmissionCalculatorWidget;
  }

  @Override
  protected OffRoadVehicleSpecification getConfirmValue() {
    return offRoadEmissionCalculatorWidget.getValue();
  }

  @Override
  protected String getTitle() {
    return M.messages().offRoadEmissionCalculatorTitle();
  }

  @Override
  protected Double getInputValue(final OffRoadVehicleSpecification value) {
    return value == null ? null : value.getEmission();
  }

  public void setHpC(final HelpPopupController hpC) {
    offRoadEmissionCalculatorWidget.setHpC(hpC);
  }

  public void setSector(final Sector sector) {
    offRoadEmissionCalculatorWidget.setSector(sector);
  }
}
