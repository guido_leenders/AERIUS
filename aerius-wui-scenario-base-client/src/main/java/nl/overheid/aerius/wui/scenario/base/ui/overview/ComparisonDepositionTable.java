/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.HabitatHoverType;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.HabitatTypeDisplayEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeEvent.HabitatDisplayType;
import nl.overheid.aerius.wui.main.event.HabitatTypeHoverEvent;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveDivTableRow;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

/**
 *
 */
public abstract class ComparisonDepositionTable extends Composite implements IsInteractiveDataTable<HabitatSummaryResults> {
  interface ComparisonDepositionTableUiBinder extends UiBinder<Widget, ComparisonDepositionTable> {}

  private static final ComparisonDepositionTableUiBinder UI_BINDER = GWT.create(ComparisonDepositionTableUiBinder.class);

//  private static final double SHOW_VALUE_MARGIN = 0.005;

  @UiField(provided = true) SimpleInteractiveClickDivTable<HabitatSummaryResults> divTable;

  @UiField(provided = true) TextColumn<HabitatSummaryResults> habitatCodeColumn;
  @UiField(provided = true) TextColumn<HabitatSummaryResults> labelColumn;
  @UiField(provided = true) TextColumn<HabitatSummaryResults> valueColumn;

  private final EventBus eventBus;
  private final int assessmentAreaId;

  /**
   * Grid for comparing depositions.
   * @param eventBus
   */
  public ComparisonDepositionTable(final EventBus eventBus, final int assessmentAreaId, final boolean comparisonResults) {
    this.eventBus = eventBus;
    this.assessmentAreaId = assessmentAreaId;

    divTable = new SimpleInteractiveClickDivTable<HabitatSummaryResults>() {
      @Override
      public void applyRowOptions(final SimpleInteractiveDivTableRow row, final HabitatSummaryResults item) {
        super.applyRowOptions(row, item);

        row.addDomHandler(new MouseOverHandler() {
          @Override
          public void onMouseOver(final MouseOverEvent event) {
            eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatHoverType.NATURE_AREA, assessmentAreaId, item.getId()));
          }
        }, MouseOverEvent.getType());
        row.addDomHandler(new MouseOutHandler() {
          @Override
          public void onMouseOut(final MouseOutEvent event) {
            eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatDisplayType.REMOVE));
          }
        }, MouseOutEvent.getType());
      }
    };
    divTable.setSelectionModel(new MultiSelectionModel<HabitatSummaryResults>());
    divTable.addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        updateHoverSelection();
      }
    });
    divTable.ensureDebugId(TestID.INNER_TABLE);

    habitatCodeColumn = new TextColumn<HabitatSummaryResults>(true) {
      @Override
      public String getValue(final HabitatSummaryResults object) {
        return object.getCode();
      }
    };
    habitatCodeColumn.ensureDebugId(TestID.CODE);
    labelColumn = new TextColumn<HabitatSummaryResults>() {
      @Override
      public String getValue(final HabitatSummaryResults object) {
        return object.getName();
      }
    };
    labelColumn.ensureDebugId(TestID.LABEL);
    valueColumn = new TextColumn<HabitatSummaryResults>() {
      @Override
      public void applyCellOptions(final Widget cell, final HabitatSummaryResults object) {
        super.applyCellOptions(cell, object);

//        if (comparisonResults && ComparisonDepositionTable.this.getValue(object) >= SHOW_VALUE_MARGIN) {
//          cell.setStyleName(R.css().comparisonListNegative(), true);
//        }
      }

      @Override
      public String getValue(final HabitatSummaryResults object) {
        return ComparisonDepositionTable.this.getValueAsString(object);
      }
    };
    valueColumn.ensureDebugId(TestID.VALUE);

    initWidget(UI_BINDER.createAndBindUi(this));

  }

  protected abstract String getValueAsString(final HabitatSummaryResults object);

  protected abstract double getValue(final HabitatSummaryResults object);

  @Override
  public SimpleInteractiveClickDivTable<HabitatSummaryResults> asDataTable() {
    return divTable;
  }

  public MultiSelectionModel<HabitatSummaryResults> getSelectionModel() {
    return (MultiSelectionModel<HabitatSummaryResults>) divTable.getSelectionModel();
  }

  public HandlerRegistration addSelectionChangeHandler(final Handler handler) {
    return divTable.addSelectionChangeHandler(handler);
  }

  private static Set<Integer> getHabitatTypeIds(final Set<HabitatSummaryResults> habitats) {
    final Set<Integer> ids = new HashSet<>();
    for (final HabitatSummaryResults result : habitats) {
      ids.add(result.getId());
    }
    return ids;
  }

  public void updateHoverSelection() {
    final Set<HabitatSummaryResults> selectedSet = getSelectionModel().getSelectedSet();

    eventBus.fireEvent(new HabitatTypeDisplayEvent(HabitatHoverType.NATURE_AREA, assessmentAreaId, getHabitatTypeIds(selectedSet)));
  }
}
