/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.ArrayList;
import java.util.Collections;

import nl.overheid.aerius.shared.domain.deposition.ComparisonEnum;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar.ValueRange;
import nl.overheid.aerius.wui.scenario.base.geo.VectorCalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.ui.overview.ReceptorPointDistributionUtil.RangeDistributor;

/**
 * A {@link HabitatComparisonFilter} extends the {@link AbstractScenarioBaseHabitatFilter} to make
 * it specific to the {@link ComparisonEnum} ranging scheme.
 */
public class HabitatComparisonFilter extends AbstractScenarioBaseHabitatFilter<ComparisonEnum> {
  private static final int LENGTH = ComparisonEnum.values().length;

  //static because it's the cache for this particular filter
  private static IndexCache indexCache = new IndexCache(Integer.MIN_VALUE, Integer.MAX_VALUE);

  /**
   * @param layer see {@link AbstractScenarioBaseHabitatFilter}
   * @param emissionResultKey see {@link AbstractScenarioBaseHabitatFilter}
   */
  public HabitatComparisonFilter(final VectorCalculationResultLayerWrapper layer, final EmissionResultKey emissionResultKey) {
    super(layer, emissionResultKey, indexCache);
  }

  @Override
  protected RangeDistributor<ComparisonEnum> getRangeDistributor() {
    return new RangeDistributor<ComparisonEnum>() {

      @Override
      public ArrayList<ComparisonEnum> getPossibleRanges() {
        final ArrayList<ComparisonEnum> ranges = new ArrayList<ComparisonEnum>();
        Collections.addAll(ranges, ComparisonEnum.values());
        return ranges;
      }

      @Override
      public ComparisonEnum getRange(final AeriusResultPoint rp) {
        return ComparisonEnum.getRange(rp.getEmissionResult(getEmissionResultKey()));
      }
    };
  }

  @Override
  public int size() {
    return LENGTH;
  }

  /**
   * The {@link ComparisonEnum} filter graph must provide an additional bar that
   * represents results that are equal to exactly zero (0). To more easily mock this
   * semantic range, the ComparisonEnum introduced 2 additional values that are
   * negligibly close to zero which we will take advantage of here.
   *
   * See the convertIdx(idx) method to see this conversion and mocking taking place.
   *
   * @param value The ValueRange to set.
   */
  @Override
  double determineUpperLimit(final ValueRange value) {
    double upperLimit = Integer.MAX_VALUE;

    final int upperIdx = convertIdx(value.getLowerValue() - 1);
    if (upperIdx >= 0) {
      upperLimit = ComparisonEnum.values()[LENGTH - upperIdx - 1].getValue();
    }

    return upperLimit;
  }

  @Override
  double determineLowerLimit(final ValueRange value) {
    double lowerLimit = Integer.MIN_VALUE;

    final int lowerIdx = convertIdx(value.getUpperValue() - 1);
    if (lowerIdx < LENGTH) {
      lowerLimit = ComparisonEnum.values()[LENGTH - lowerIdx - 1].getValue();
    }

    return lowerLimit;
  }

  /**
   * Convert the given id to another that represents a scheme where a bar that
   * represents 0 is present. If the id is larger than the number if ranges
   * above 0, return that id summed by 1, else return the id as-is.
   *
   * @param idx id
   * @return converted id.
   */
  private int convertIdx(final int idx) {
    return idx > ComparisonEnum.ABOVE_ZERO.length ? idx + 1 : idx;
  }

  /**
   * Get the range index for the given value.
   *
   * @param val A value inside the range.
   *
   * @return Index of the range the value is in.
   */
  @Override
  protected int getIndex(final double val) {
    int returnValue = 0;
    if (val == Integer.MIN_VALUE) {
      returnValue = LENGTH;
    } else if (val == Integer.MAX_VALUE) {
      returnValue = 0;
    } else {
      final ComparisonEnum range = ComparisonEnum.getRange(val);

      for (int i = 0; i < LENGTH; i++) {
        if (range == ComparisonEnum.values()[i]) {
          returnValue = LENGTH - i - (i > ComparisonEnum.ABOVE_ZERO.length ? 0 : 1);
        }
      }
    }

    return returnValue;
  }
}
