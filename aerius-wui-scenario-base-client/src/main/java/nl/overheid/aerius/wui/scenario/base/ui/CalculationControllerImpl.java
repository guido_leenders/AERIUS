/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.calculation.CalculatorUtil;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.export.ExportDialogController;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.processor.ScenarioBaseCalculationProcessor;

public class CalculationControllerImpl implements CalculationController {
  private final CalculatorButtonGroup buttons;
  private final PlaceController placeController;
  private final EventBus eventBus;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final ExportDialogController edController;
  private final ScenarioBaseCalculationProcessor processor;

  @Inject
  public CalculationControllerImpl(final EventBus eventBus, final CalculatorButtonGroup buttons, final HelpPopupController hpC,
      final ExportDialogController edController, final PlaceController placeController, final ScenarioBaseCalculationProcessor processor,
      final ScenarioBaseAppContext<?, ?> appContext) {
    this.buttons = buttons;
    this.processor = processor;
    this.eventBus = eventBus;
    this.placeController = placeController;
    this.appContext = appContext;
    this.edController = edController;

    buttons.setPresenter(this);
  }

  @Override
  public void doCalculate() {
    if (appContext.isCalculationRunning()) {
      // Cancel if we're already calculating.
      eventBus.fireEvent(new CalculationCancelEvent(true));
    } else {
      // Fire up a calculation
      CalculatorUtil.calculate(eventBus, placeController, processor, appContext, (ScenarioBasePlace) placeController.getWhere());
    }
  }

  @Override
  public void showExportDialog() {
    if (appContext.getUserContext().hasSources()) {
      edController.show(appContext.getUserContext(), placeController.getWhere(), appContext.getUserContext().getEmissionValueKey().getYear());
    } else {
      NotificationUtil.broadcastError(eventBus, M.messages().notificationExportInvalid());
    }
  }

  @Override
  public void setButtonState(final boolean calculationRunning, final boolean visible) {
    buttons.setButtonState(calculationRunning, visible);
  }

  @Override
  public IsWidget getCalculationButtons() {
    return buttons;
  }
}
