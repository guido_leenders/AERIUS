/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.export;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.validation.EmailValidator;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.util.ParamUtil;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Export Dialog content panel with export options.
 */
class ExportDialogPanel extends Composite implements Editor<ExportProperties>, HasValueChangeHandlers<Boolean> {

  interface ExportDialogDriver extends SimpleBeanEditorDriver<ExportProperties, ExportDialogPanel> { }

  private static final ExportDialogPanelUiBinder UI_BINDER = GWT.create(ExportDialogPanelUiBinder.class);

  interface ExportDialogPanelUiBinder extends UiBinder<Widget, ExportDialogPanel> { }

  @UiField FlowPanel radioButtons;
  @UiField @Ignore RadioButton buttonGISOnlyInput;
  @UiField @Ignore RadioButton buttonGISAll;
//  @UiField @Ignore RadioButton buttonPAADevelopmentSpaces;
//  @UiField @Ignore RadioButton buttonPAADemand;
  @UiField @Ignore RadioButton buttonPAAOwnUse;
  @UiField @Ignore RadioButton buttonOPS;
//  @UiField @Ignore Label labelExplainSingleSituation;
  @UiField(provided = true) ScenarioMetaDataPanel scenarioMetaDataEditor;

  @UiField TextValueBox emailAddress;

  LeafValueEditor<ExportType> exportTypeEditor = new LeafValueEditor<ExportType>() {

    @Override
    public ExportType getValue() {
      final ExportType et;
      if (buttonGISOnlyInput.getValue()) {
        et = ExportType.GML_SOURCES_ONLY;
      } else if (buttonGISAll.getValue()) {
        et = ExportType.GML_WITH_RESULTS;
//      } else if (buttonPAADevelopmentSpaces.getValue()) {
//        et = ExportType.PAA_DEVELOPMENT_SPACES;
//      } else if (buttonPAADemand.getValue()) {
//        et = ExportType.PAA_DEMAND;
      } else if (buttonPAAOwnUse.getValue()) {
        et = ExportType.PAA_OWN_USE;
      } else if (buttonOPS.getValue()) {
        et = ExportType.OPS;
      } else {
        et = null;
      }
      return et;
    }

    @Override
    public void setValue(final ExportType value) {
      setValueInternal(value, false);
    }
  };

  public ExportDialogPanel(final HelpPopupController hpC) {
    scenarioMetaDataEditor = new ScenarioMetaDataPanel(hpC);
    initWidget(UI_BINDER.createAndBindUi(this));

    emailAddress.addValidator(new NotEmptyValidator(M.messages().emailAddress()) {
      @Override
      protected String getMessage(final String value) {
        return M.messages().ivExportNoEmptyValue(M.messages().emailAddress());
      }
    });
    emailAddress.addValidator(new EmailValidator());

    buttonOPS.setVisible(ParamUtil.inOPS());

    scenarioMetaDataEditor.setVisible(false);
    StyleUtil.I.setPlaceHolder(emailAddress, M.messages().emailAddress());

    emailAddress.ensureDebugId(TestID.INPUT_EXPORT_EMAILADRESS);
    buttonGISOnlyInput.ensureDebugId(TestID.RADIO_EXPORT_GISONLY);
    buttonGISAll.ensureDebugId(TestID.RADIO_EXPORT_GISALL);
//    buttonPAADevelopmentSpaces.ensureDebugId(TestID.RADIO_EXPORT_PAA_DEVELOPMENT_SPACES);
//    buttonPAADemand.ensureDebugId(TestID.RADIO_EXPORT_PAA_DEMAND);
    buttonPAAOwnUse.ensureDebugId(TestID.RADIO_EXPORT_PAA_OWNUSE);

    hpC.addWidget(emailAddress, hpC.tt().ttExportEmail());
    hpC.addWidget(radioButtons, hpC.tt().ttExport());
  }

  public void setValueInternal(final ExportType value, final boolean fireEvent) {
    if (value == null) {
      buttonGISOnlyInput.setValue(true, fireEvent);
    } else {
      switch (value) {
      case GML_WITH_RESULTS:
        buttonGISAll.setValue(true, fireEvent);
        break;
//      case PAA_DEVELOPMENT_SPACES:
//        buttonPAADevelopmentSpaces.setValue(true, fireEvent);
//        break;
//      case PAA_DEMAND:
//        buttonPAADemand.setValue(true, fireEvent);
//        break;
      case PAA_OWN_USE:
        buttonPAAOwnUse.setValue(true, fireEvent);
        break;
      case OPS:
        buttonOPS.setValue(true, fireEvent);
        break;
      case GML_SOURCES_ONLY:
      default:
        buttonGISOnlyInput.setValue(true, fireEvent);
        break;
      }
    }
//    if (buttonPAADevelopmentSpaces.getValue() || buttonPAADemand.getValue()) {
//      scenarioMetaDataEditor.addPAAValidators();
//    } else {
//      scenarioMetaDataEditor.removePAAValidators();
//    }
  }

  public void setDefaultTypeSelection(final ExportType defaultType) {
    if (defaultType != null) {
      setValueInternal(defaultType, true);
      radioButtons.setVisible(false);
    } else {
      radioButtons.setVisible(true);
    }
  }

  public void setSituationSelection(final boolean has2Situations) {
//    labelExplainSingleSituation.setVisible(has2Situations);
//    buttonPAADemand.setEnabled(!has2Situations);
//
//    // change selected value to default if it's disabled - to make sure the UI works properly in such a case
//    if (!buttonPAADemand.isEnabled() && buttonPAADemand.getValue()) {
//      setValueInternal(null, true);
//    }
  }

//  @UiHandler({"buttonPAADevelopmentSpaces", "buttonPAADemand"})
//  void onSelectionChangePAA(final ValueChangeEvent<Boolean> e) {
//    scenarioMetaDataEditor.setVisible(true);
//    scenarioMetaDataEditor.addPAAValidators();
//  }

  @UiHandler("buttonPAAOwnUse")
  void onSelectionChangePAAOwnUse(final ValueChangeEvent<Boolean> e) {
    scenarioMetaDataEditor.setVisible(true);
    scenarioMetaDataEditor.addPAAValidators();
  }

  @UiHandler({"buttonOPS", "buttonGISAll", "buttonGISOnlyInput"})
  void onSelectionChangeNonPAA(final ValueChangeEvent<Boolean> e) {
    scenarioMetaDataEditor.setVisible(false);
    scenarioMetaDataEditor.removePAAValidators();
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    buttonGISOnlyInput.addValueChangeHandler(handler);
    buttonGISAll.addValueChangeHandler(handler);
//    buttonOPS.addValueChangeHandler(handler);
    buttonPAAOwnUse.addValueChangeHandler(handler);
//    buttonPAADevelopmentSpaces.addValueChangeHandler(handler);
//    return buttonPAADemand.addValueChangeHandler(handler);
    return buttonOPS.addValueChangeHandler(handler);
  }
}
