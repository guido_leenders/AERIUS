/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.context;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceListNameChangeEvent;
import nl.overheid.aerius.wui.main.event.RefreshMenuEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInfoReceivedEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationSummaryChangedEvent;

/**
 * Event handler for handling global events.
 */
public final class ScenarioBaseGlobalEventHandler {
  interface ScenarioBaseGlobalEventBinder extends EventBinder<ScenarioBaseGlobalEventHandler> { }

  private final ScenarioBaseGlobalEventBinder eventBinder = GWT.create(ScenarioBaseGlobalEventBinder.class);
  private final EventBus eventBus;
  private final ScenarioBaseAppContext<?, ?> appContext;

  @Inject
  public ScenarioBaseGlobalEventHandler(final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext) {
    this.eventBus = eventBus;
    this.appContext = appContext;
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onYearChange(final YearChangeEvent event) {
    appContext.getUserContext().getEmissionValueKey().setYear(event.getValue());
  }

  @EventHandler
  void onSubstanceChange(final EmissionResultKeyChangeEvent event) {
    appContext.getUserContext().getEmissionValueKey().setSubstance(event.getValue().getSubstance());
    appContext.getUserContext().setEmissionResultKey(event.getValue());
  }

  @EventHandler
  void onNameChange(final EmissionSourceListNameChangeEvent event) {
    final EmissionSourceList esl = appContext.getUserContext().getScenario().getSources(event.getId());
    if (esl != null) {
      esl.setName(event.getName());
    }
  }

  @EventHandler
  void onCalculationStartEvent(final CalculationStartEvent event) {
    appContext.setCalculationRunning(true);
    appContext.setCalculationState(CalculationState.RUNNING);
    // when calculation is started there is a place change event to graphics.
    eventBus.fireEvent(new RefreshMenuEvent());
  }

  @EventHandler
  void onCalculationCancelEvent(final CalculationCancelEvent event) {
    appContext.setCalculationCancelled(true);
    appContext.setCalculationRunning(false);
    appContext.setCalculationState(CalculationState.CANCELLED);
  }

  @EventHandler
  void onCalculationFinishEvent(final CalculationFinishEvent event) {
    appContext.setCalculationRunning(false);
    if (appContext.getCalculationState() != CalculationState.CANCELLED) {
      appContext.setCalculationState(CalculationState.COMPLETED);
    }
  }

  @EventHandler
  void onCalculationInfoReceivedEvent(final CalculationInfoReceivedEvent event) {
    appContext.setCalculationInfo(event.getValue().getCalculationId(), event.getValue());
  }

  @EventHandler
  void onCalculationSummaryReceivedEvent(final CalculationSummaryChangedEvent event) {
    appContext.setCalculationSummary(event.getValue());
    // when calculation is started there is a place change event to table/filters.
    eventBus.fireEvent(new RefreshMenuEvent());
  }

  public static void bind(final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext) {
    new ScenarioBaseGlobalEventHandler(eventBus, appContext);
  }
}
