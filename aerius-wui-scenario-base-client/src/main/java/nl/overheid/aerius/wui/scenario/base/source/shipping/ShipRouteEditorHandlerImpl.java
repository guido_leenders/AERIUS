/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.wui.main.ui.editor.LeafValueAwareEditor;
import nl.overheid.aerius.wui.main.widget.ListButton;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorSource.ShipRouteEditorHandler;

/**
 * Implementation of {@link ShipRouteEditorHandler}. This class manages the update of ui and route on the map.
 */
abstract class ShipRouteEditorHandlerImpl implements ShipRouteEditorHandler {

  /**
   * Interface to separate access to class setting the buttons.
   */
  interface EnableButtons {
    /**
     * Set state of the route buttons. Used to control state when route is created or edited.
     * @param enable
     */
    void enableButtons(boolean enable);
  }

  private final ListButton listButton;
  private final LeafValueAwareEditor<ShippingRoute> routeEditor;
  private final ShippingRouteHandler routeContainer;
  private final EnableButtons enableButtons;

  public ShipRouteEditorHandlerImpl(final ListButton listButton, final LeafValueAwareEditor<ShippingRoute> routeEditor,
      final ShippingRouteHandler routeHandler, final EnableButtons enableButtons) {
    this.listButton = listButton;
    this.routeEditor = routeEditor;
    this.routeContainer = routeHandler;
    this.enableButtons = enableButtons;
  }

  @Override
  public void onAdd(final ShippingRoute route) {
    routeContainer.add(route);
  }

  @Override
  public void onUpdate(final ShippingRoute route) {
    routeContainer.refresh(route);
    enableButtons.enableButtons(true);
  }

  @Override
  public void onRemove(final ShippingRoute route) {
    // If only used by this ship we set the route to null
    // If the user deletes another route from the list that is not used the current relation
    // between ship and route should not be destroyed.
    final HasId routeRef = getRouteRef();
    if (routeEditor.getValue() != null && routeRef != null && route.isOnlyUsedBy(routeRef)) {
      routeEditor.setValue(null);
      routeContainer.remove(route);
      listButton.hide();
      return;
    }
    // At this point the route to be removed is not used by the current ship, so just
    // check if it is used att all, and if not the route can be removed.
    if (!route.isUsed()) {
      routeContainer.remove(route);
    }
  }

  @Override
  public Point onStartDrawNewRoute() {
    routeEditor.setValue(null);
    enableButtons.enableButtons(false);
    return null;
  }

  @Override
  public void onSelect(final ShippingRoute route) {
    routeEditor.setValue(route);
    listButton.hide();
    enableButtons.enableButtons(true);
  }

  @Override
  public void onCancel() {
    enableButtons.enableButtons(true);
    routeEditor.setValue(null);
  }
}
