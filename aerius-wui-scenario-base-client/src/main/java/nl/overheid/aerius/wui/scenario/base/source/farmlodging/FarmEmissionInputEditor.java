/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.ListCancelButtonEvent;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.farmlodging.FarmLodgingCustomEmissionEditor.FarmLodgingCustomEmissionDriver;
import nl.overheid.aerius.wui.scenario.base.source.farmlodging.FarmLodgingStandardEmissionEditor.FarmLodgingStandardEmissionDriver;

class FarmEmissionInputEditor extends Composite implements ValueAwareEditor<FarmLodgingEmissions>,
HasClickHandlers, InputEditor<FarmLodgingEmissions> {

  interface FarmLodgingEmissionInputDriver extends SimpleBeanEditorDriver<FarmLodgingEmissions, FarmEmissionInputEditor> { }

  interface FarmEmissionInputEditorUiBinder extends UiBinder<Widget, FarmEmissionInputEditor> { }

  private static final FarmEmissionInputEditorUiBinder UI_BINDER = GWT.create(FarmEmissionInputEditorUiBinder.class);

  private final FarmLodgingStandardEmissionEditor vStandardEditor;
  private final FarmLodgingStandardEmissionDriver vStandardDriver = GWT.create(FarmLodgingStandardEmissionDriver.class);

  private final FarmLodgingCustomEmissionEditor vCustomEditor;
  private final FarmLodgingCustomEmissionDriver vCustomDriver = GWT.create(FarmLodgingCustomEmissionDriver.class);

  @SuppressWarnings("rawtypes")
  private SimpleBeanEditorDriver driver;
  @SuppressWarnings("rawtypes")
  private InputEditor editor;

  @UiField SimplePanel panel;
  @UiField Button cancelButton;
  @UiField Button submitButton;

  private final EventBus eventBus;

  private EditorDelegate<FarmLodgingEmissions> delegate;
  private FarmLodgingEmissions cancelEmissions;

  public FarmEmissionInputEditor(final EventBus eventBus, final SectorCategories categories, final HelpPopupController hpC) {
    this.eventBus = eventBus;
    initWidget(UI_BINDER.createAndBindUi(this));

    vStandardEditor = new FarmLodgingStandardEmissionEditor(eventBus, categories, hpC);
    vStandardDriver.initialize(vStandardEditor);

    vCustomEditor = new FarmLodgingCustomEmissionEditor(eventBus, categories, hpC);
    vCustomDriver.initialize(vCustomEditor);
  }

  @Override
  public void setDelegate(final EditorDelegate<FarmLodgingEmissions> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    //no-op
  }

  @Override
  public void resetPlaceholders() {
    if (editor != null) {
      editor.resetPlaceholders();
    }
  }


  @UiHandler("cancelButton")
  void cancelButtonHandler(final ClickEvent e) {
    if (cancelEmissions != null) {
      setValue(cancelEmissions.copy());
      eventBus.fireEvent(new ListCancelButtonEvent());
    }
  }


  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  @Override
  public void flush() {
    if (driver != null) {
      driver.flush();
      if (driver.hasErrors()) {
        for (final Object obj : driver.getErrors()) {
          final EditorError error = (EditorError) obj;
          delegate.recordError(error.getMessage(), error.getValue(), error.getUserData());
          error.setConsumed(true);
        }
      }
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @SuppressWarnings("unchecked")
  @Override
  public void setValue(final FarmLodgingEmissions value) {
    // EDIT mode
    if (value instanceof FarmLodgingStandardEmissions) {
      driver = vStandardDriver;
      editor = vStandardEditor;
    } else if (value instanceof FarmLodgingCustomEmissions) {
      driver = vCustomDriver;
      editor = vCustomEditor;
    } else if (value == null) {
      setValue(new FarmLodgingStandardEmissions());
      resetPlaceholders();
      return;
    }

    if (driver != null) {
      driver.edit(value);
      resetPlaceholders();
      // Reset errors styles on widgets
      ErrorPopupController.clearWidgets();
    }
    panel.setWidget(editor);
    cancelEmissions = value.copy();
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
    cancelButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_CANCEL);
    vStandardEditor.ensureDebugId(baseID);
    vCustomEditor.ensureDebugId(baseID);
  }
}
