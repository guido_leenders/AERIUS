/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.wui.main.place.Situation;


/**
 * View for pages with situation tabs.
 */
public interface SituationView extends IsWidget, HasTitle {

  /**
   * Presenter for {@link SituationView}.
   */
  interface Presenter extends SituationTabPanel.SituationChangeHandler {
    IsWidget getCalculationButtons();
  }

  /**
   * Set the presenter.
   *
   * @param presenter Presenter
   */
  void setPresenter(Presenter presenter);

  /**
   * Set the name for a specific tab. If tab isn't present yet, it will be created by calling this.
   *
   * @param situation The situation to set the name for.
   * @param id of the situation
   * @param name The name for the tab to use.
   */
  void setTabName(Situation situation, int id, String name);

  /**
   * Set the tab in focus.
   * @param situation Situation representing tab to set focus
   */
  void setFocus(Situation situation);

  /**
   * Returns the content panel where the sub panel should be places on.
   * @return panel to set sub panel on.
   */
  AcceptsOneWidget getContentPanel();

  /**
   * Sets the state of the tabs. If they are in edit mode or not. In edit mode user can add/remove situations.
   * @param enable if true in edit mode
   */
  void setEditable(boolean enable);
}
