/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.CollapsibleStackPanel;

/**
 * Base class for EmissionSource Editors.
 */
public abstract class BaseEmissionSourceEditor<E extends EmissionSource> extends Composite implements EmissionsEditor<E>, ValueAwareEditor<E> {
  private final CollapsiblePanel contentPanel = new CollapsiblePanel();
  private final CollapsibleStackPanel panel = new CollapsibleStackPanel();
  private final TextBox labelEditor = new TextBox();

  public BaseEmissionSourceEditor(final String title) {
    initWidget(panel);
    contentPanel.setTitleText(title);
    contentPanel.addStyleName(R.css().flex());
    contentPanel.addStyleName(R.css().columnsClean());
    contentPanel.addStyleName(R.css().layoutRow());
    contentPanel.addStyleName(R.css().collapsiblePanel());
    contentPanel.ensureDebugId(TestID.DIV_COLLAPSEPANEL_EMISSION_SOURCE_DETAILS);
  }

  protected void addContentPanel(final Widget widget) {
    widget.addStyleName(R.css().flex());
    widget.addStyleName(R.css().columnsClean());
    widget.addStyleName(R.css().grow());
    contentPanel.add(widget);
    panel.add(contentPanel);
  }

  /**
   * @return the panel
   */
  public CollapsibleStackPanel getMainPanel() {
    return panel;
  }

  @Path("label")
  public TextBox getLabelEditor() {
    return labelEditor;
  }

  @Override
  public abstract void setEmissionValueKey(final EmissionValueKey key);

  @Override
  public void postSetValue(final E source) {
    //no-op
  }

  @Override
  public void setDelegate(final EditorDelegate<E> delegate) {
    // no-op
  }

  @Override
  public void flush() {
    // no-op
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void focusOnErrors() {
    // no-op
  }

  @Override
  public void setValue(final E source) {
    contentPanel.setValue(true);
    contentPanel.setVisible(true);
  }
}
