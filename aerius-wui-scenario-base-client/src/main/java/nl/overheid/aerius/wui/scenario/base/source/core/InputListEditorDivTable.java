/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSubSource;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.ContextDivTable;
import nl.overheid.aerius.wui.main.widget.table.ContextualContent;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

/**
 * Generic Table.
 */
public abstract class InputListEditorDivTable<T extends EmissionSubSource & HasId> extends Composite implements IsInteractiveDataTable<T> {
  interface InputListEditorDivTableUiBinder extends UiBinder<Widget, InputListEditorDivTable<?>> {}

  private static final InputListEditorDivTableUiBinder UI_BINDER = GWT.create(InputListEditorDivTableUiBinder.class);

  public interface CustomStyle extends CssResource {
    String iconColumn();
  }

  private final SingleSelectionModel<T> selectionModel = new SingleSelectionModel<T>();

  private EmissionValueKey key;

  @UiField SimpleInteractiveClickDivTable<T> divTable;
  @UiField(provided = true) TextColumn<T> labelColumn;
  @UiField(provided = true) TextColumn<T> emissionColumn;

  @UiField CustomStyle style;

  public InputListEditorDivTable(final HelpPopupController hpC, final Boolean enableImage, final ContextualContent<T> context) {
    // Label column
    labelColumn = new TextColumn<T>() {
      @Override
      public String getValue(final T value) {
        return getLabel(value);
      }
    };

    // Emission column
    emissionColumn = new TextColumn<T>() {
      @Override
      public String getValue(final T value) {
        return key.getSubstance() == Substance.NOXNH3 ? null : FormatUtil.formatEmissionWithUnit(getEmission(value, key));
      }
    };

    labelColumn.ensureDebugId(TestID.LABEL);
    emissionColumn.ensureDebugId(TestID.EMISSION);

    initWidget(UI_BINDER.createAndBindUi(this));

    ContextDivTable.wrap(divTable, context);

    // Optional Image column
    if (enableImage) {
      final ImageColumn<T> imageColumn = new ImageColumn<T>() {
        @Override
        public ImageResource getValue(final T object) {
          return getImage(object);
        }
      };

      imageColumn.ensureDebugId(TestID.ICON);

      divTable.insertColumn(0, imageColumn, style.iconColumn());
    }

    divTable.setSelectionModel(selectionModel);
  }

  public void setEmissionValueKey(final EmissionValueKey key) {
    this.key = key;
  }

  protected abstract double getEmission(T value, EmissionValueKey key);

  protected abstract String getLabel(final T object);

  protected abstract ImageResource getImage(final T object);

  @Override
  public SimpleInteractiveClickDivTable<T> asDataTable() {
    return divTable;
  }
}
