/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.event.DeleteEvent;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.NotEditCancellable;
import nl.overheid.aerius.wui.scenario.base.source.geo.ShippingRouteContainer;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandMooringInputEditor.InlandMooringInputDriver;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorSource.TransferRouteRowEditor;

/**
 * Editor for showing a list of inland mooring ship groups and a edit field for a single ship.
 */
public class InlandMooringEmissionEditor extends BaseEmissionSourceEditor<InlandMooringEmissionSource> implements NotEditCancellable {

  public interface InlandMooringEmissionDriver extends SimpleBeanEditorDriver<InlandMooringEmissionSource, InlandMooringEmissionEditor> {
  }

  interface ShipRouteDriver extends SimpleBeanEditorDriver<List<ShippingRoute>, ListEditor<ShippingRoute, TransferRouteRowEditor>> {
  }

  interface InlandMooringEmissionEditorEventBinder extends EventBinder<InlandMooringEmissionEditor> { }

  InputListEmissionEditor<InlandMooringVesselGroup, InlandMooringInputEditor> emissionSubSourcesEditor;
  InlandMooringRoutesEditor inlandRoutesEditor;

  private final InlandMooringEmissionEditorEventBinder eventBinder = GWT.create(InlandMooringEmissionEditorEventBinder.class);
  private final InlandMooringInputEditor inlandMooringInputEditor;
  private final EventBus eventBus;
  private final InlandWaterwayTypePanel inlandWaterwayTypePanel;
  private final InlandShippingCategories inlandShippingCategories;
  private EditorDelegate<InlandMooringEmissionSource> delegate;

  @SuppressWarnings("unchecked")
  public InlandMooringEmissionEditor(final EventBus eventBus, final SectorCategories categories, final SourceMapPanel map,
      final CalculatorServiceAsync service, final HelpPopupController hpC) {
    super(M.messages().shipPanelTitle());
    this.eventBus = eventBus;
    inlandShippingCategories = categories.getInlandShippingCategories();

    inlandMooringInputEditor = new InlandMooringInputEditor(eventBus, map, inlandShippingCategories, hpC);
    emissionSubSourcesEditor = new InputListEmissionEditor<InlandMooringVesselGroup, InlandMooringInputEditor>(
        eventBus, inlandMooringInputEditor, hpC, new ShipMessages(hpC),
        InlandMooringVesselGroup.class, "IMooring",
        (SimpleBeanEditorDriver<InlandMooringVesselGroup, InputEditor<InlandMooringVesselGroup>>) GWT.create(InlandMooringInputDriver.class),
        service, false) {

      @Override
      protected InlandMooringVesselGroup createNewRowEmissionValues() {
        return new InlandMooringVesselGroup();
      }

      @Override
      public void onDelete(final DeleteEvent<InlandMooringVesselGroup> event) {
        event.getValue().detachRoutes();
        super.onDelete(event);
      }
    };

    final ShippingRouteContainer shippingRouteContainer = (ShippingRouteContainer) map.getMartimeMooringRouteContainer();
    inlandWaterwayTypePanel = new InlandWaterwayTypePanel(true, M.messages().shipWaterwayDirectionLabel());
    inlandRoutesEditor = new InlandMooringRoutesEditor(inlandWaterwayTypePanel, eventBus, hpC, service, inlandShippingCategories);
    final ShippingRouteHandler inlandRouteHandler = new ShippingRouteHandler(inlandRoutesEditor, shippingRouteContainer) {
      @Override
      protected void onAdd(final ShippingRoute obj) {
        refreshInlandRoutes();
      }

      @Override
      protected void onRemove(final ShippingRoute obj) {
        refreshInlandRoutes();
      }
    };

    getMainPanel().add(inlandWaterwayTypePanel);
    emissionSubSourcesEditor.getInputEditor().setRoutesHandler(inlandRouteHandler);
    addContentPanel(emissionSubSourcesEditor);
    emissionSubSourcesEditor.ensureDebugId(TestID.DIVTABLE_EDITABLE);
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  public void postSetValue(final InlandMooringEmissionSource source) {
    eventBinder.bindEventHandlers(this, eventBus);
    super.postSetValue(source);
    emissionSubSourcesEditor.postSetValue(source);
    refreshInlandRoutes();
  }

  @EventHandler
  void onInlandWaterwayTypeUpdateEvent(final InlandWaterwayTypeUpdateEvent event) {
    updateInlandWaterType(event.getRouteId(), event.getInlandWaterwayType());
  }

  @Override
  public void setDelegate(final EditorDelegate<InlandMooringEmissionSource> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    for (final InlandMooringVesselGroup rivg : emissionSubSourcesEditor.getValue()) {
      for (final InlandMooringRoute inlandRoute : rivg.getRoutes()) {
        final ShippingRoute route = inlandRoute.getRoute();
        final InlandShippingCategory category = rivg.getCategory();
        if (route != null) {
          final InlandWaterwayCategory waterwayCategory = route.getWaterwayCategory();
          if (!inlandShippingCategories.isValidCombination(category, waterwayCategory)) {
            delegate.recordError(M.messages().shipWaterwayNotAllowedCombination(category.getCode(), waterwayCategory.getCode()),
                category, inlandWaterwayTypePanel);
          }
        }
      }
    }
  }

  /**
   * Updates the inlandWaterwayType for all sub sources so the new emissions can be calculated.
   * @param routeId
   * @param inlandWaterwayType
   */
  private void updateInlandWaterType(final int routeId, final InlandWaterwayType inlandWaterwayType) {
    for (final ShippingRoute sr : inlandRoutesEditor.getList()) {
      if (sr.getId() == routeId) {
        final InlandWaterwayCategory category = inlandWaterwayType.getWaterwayCategory();
        sr.setWaterwayCategory(category);
        final WaterwayDirection direction = inlandWaterwayType.getWaterwayDirection();
        sr.setWaterwayDirection(direction);
        if (InlandWaterwayTypeUtil.isDirectionCorrect(category, direction)) {
          emissionSubSourcesEditor.forceRefresh();
        }
        inlandMooringInputEditor.setInlandWaterwayCategory(category);
      }
    }
  }

  private void refreshInlandRoutes() {
    // Hide panel with routes until a least one route was added.
    inlandWaterwayTypePanel.setVisible(!inlandRoutesEditor.getList().isEmpty());
    emissionSubSourcesEditor.getInputEditor().refreshInlandRoutes(inlandRoutesEditor.getList());
  }
}
