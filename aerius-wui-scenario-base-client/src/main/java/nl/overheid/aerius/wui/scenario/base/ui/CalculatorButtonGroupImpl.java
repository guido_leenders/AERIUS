/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Shows a button group based on the context of the calculator.
 */
public class CalculatorButtonGroupImpl extends Composite implements CalculatorButtonGroup {
  private static ContexedCalculatorButtonGroupUiBinder UI_BINDER = GWT.create(ContexedCalculatorButtonGroupUiBinder.class);

  interface ContexedCalculatorButtonGroupUiBinder extends UiBinder<Widget, CalculatorButtonGroupImpl> {}

  @UiField Button calculate;
  @UiField Button buttonExport;

  private CalculationController presenter;

  private final HelpPopupController hpC;

  @Inject
  public CalculatorButtonGroupImpl(final HelpPopupController hpC) {
    this.hpC = hpC;
    initWidget(UI_BINDER.createAndBindUi(this));

    buttonExport.ensureDebugId(TestID.BUTTON_EXPORT);
    calculate.ensureDebugId(TestID.BUTTON_CALCULATE);

    hpC.addWidget(buttonExport, hpC.tt().ttOverviewButtonExport());
  }

  public void setPresenter(final CalculationController presenter) {
    if (this.presenter != null) {
      throw new IllegalStateException("Presenter already set.");
    }

    this.presenter = presenter;
  }

  public void setButtonState(final boolean calculationRunning, final boolean visibleCalculate) {
    buttonExport.setEnabled(!calculationRunning);
    calculate.setVisible(visibleCalculate || calculationRunning);
    calculate.setText(calculationRunning ? M.messages().calculatorStop() : M.messages().calculatorCalcDeposition());
    if (calculationRunning) {
      hpC.addWidget(calculate, hpC.tt().ttProgressButtonStop());
    } else {
      hpC.addWidget(calculate, hpC.tt().ttOverviewButtonCalculate());
    }
  }

  @UiHandler("calculate")
  void onCalculateClick(final ClickEvent e) {
    presenter.doCalculate();
  }

  @UiHandler("buttonExport")
  void onExportClick(final ClickEvent e) {
    presenter.showExportDialog();
  }
}
