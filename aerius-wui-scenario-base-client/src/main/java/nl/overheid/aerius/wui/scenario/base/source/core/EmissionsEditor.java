/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.source.EmissionSource;

/**
 * Interface for all sector specific emission editors. Editors implementing this
 * interface should probably be added to the Controlling {EmissionValuesEditor}.
 */
public interface EmissionsEditor<E extends EmissionSource> extends IsWidget {

  /**
   * This method is called in the setValue when the driver edit method is called to do
   * additional initialization that requires the editor to be run first.
   * @param source the emission source
   */
  void postSetValue(E source);

  /**
   * Set the emissionvaluekey for this editor.
   * @param key The EmissionValueKey to set.
   */
  void setEmissionValueKey(EmissionValueKey key);

  void focusOnErrors();
}
