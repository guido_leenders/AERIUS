/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import org.gwtopenmaps.openlayers.client.Style;

import nl.overheid.aerius.shared.domain.result.EmissionResultColorRanges;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.util.ColorUtil;

final class ReceptorStyleUtil {

  private static final double DEFAULT_STOKE_WIDTH = 1.5;

  private ReceptorStyleUtil() {
  }

  public static Style getStyle(final EmissionResultType emissionResultType, final double emissionResult) {
    final String color = EmissionResultColorRanges.valueOf(emissionResultType).getColor(emissionResult);

    final Style style = new Style();
    style.setStrokeWidth(DEFAULT_STOKE_WIDTH);
    style.setStrokeColor("#FFFFFF");
    style.setFillColor(ColorUtil.webColor(color));
    style.setFillOpacity(1.0);

    return style;
  }
}
