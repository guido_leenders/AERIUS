/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.VisitableEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.wui.scenario.base.ui.source.EmissionSourceViewerVisitor.DriverViewerCombo;

/**
 * <p>Wrapper class for emission value viewers, uses the editor mechanism but doesn't do any editing.</p>
 *
 * <p>Views do not have input components, and content is never flushed.</p>
 */
@SuppressWarnings("unchecked")
public class EmissionSourceViewer implements IsWidget, TakesValue<EmissionSource> {
  private int year;

  private final EmissionSourceViewerVisitor viewVisitor;

  @SuppressWarnings("rawtypes")
  private DriverViewerCombo currentDriverViewerCombo;

  public EmissionSourceViewer() {
    viewVisitor = new EmissionSourceViewerVisitor();
  }

  /**
   * Sets the value and sets the emission value viewer.
   *
   * @param value the {@link EmissionSource} object that's to be viewed
   */
  @Override
  public void setValue(final EmissionSource value) {
    try {
      currentDriverViewerCombo = ((VisitableEmissionSource) value).accept(viewVisitor);
    } catch (final AeriusException e) {
      currentDriverViewerCombo = null;
    }

    if (currentDriverViewerCombo != null) {
      currentDriverViewerCombo.getEditor().setYear(year);
      currentDriverViewerCombo.getDriver().edit(value);
    }
  }

  @Override
  public Widget asWidget() {
    return currentDriverViewerCombo == null ? null : currentDriverViewerCombo.getEditor();
  }

  public void setYear(final int year) {
    this.year = year;
  }

  @Override
  public EmissionSource getValue() {
    return null;
  }
}
