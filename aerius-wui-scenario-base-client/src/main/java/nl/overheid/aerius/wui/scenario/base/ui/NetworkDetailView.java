/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.source.EmissionSource;

/**
 * View for displaying and editing network emission source details.
 */
public interface NetworkDetailView extends IsWidget, Editor<EmissionSource>, HasTitle {

  /**
   * Presenter for the {@link NetworkDetailView}.
   */
  interface Presenter extends Activity {

    void okButton();

    void selectedEmissionSource(EmissionSource selected);

  }

  /**
   * Set the presenter for the view.
   *
   * @param presenter Presenter to use
   */
  void setPresenter(Presenter presenter);

  void setOkButtonVisible(boolean visible);

  void setNoData(boolean loading);

  void setDetail(EmissionSource values);

  void setData(ArrayList<EmissionSource> values, EmissionValueKey key);

  /**
   * @param source
   * @param key
   */
  void updateEmissionValueKey(EmissionSource source, EmissionValueKey key);


}
