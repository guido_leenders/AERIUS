/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.source.HasShippingRoute;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;

/**
 * Class to query the inland shipping waterway category given the geometry of a route.
 */
public abstract class DetermineWaterway implements AsyncCallback<ArrayList<InlandWaterwayType>> {
  private final EventBus eventBus;
  private final CalculatorServiceAsync service;
  private final InlandShippingCategories inlandShippingCategories;

  public DetermineWaterway(final EventBus eventBus, final CalculatorServiceAsync service,final InlandShippingCategories inlandShippingCategories) {
    this.eventBus = eventBus;
    this.service = service;
    this.inlandShippingCategories = inlandShippingCategories;
  }

  /**
   * Find the waterway category given the location via the server.
   * @param geometry the geometry to search the waterway type for
   */
  public void determineWaterwayIf(final HasShippingRoute shipRoute) {
    if (shipRoute.getGeometry() != null && shipRoute.getWaterwayCategory() == null) {
      determineWaterway(shipRoute.getGeometry());
    }
  }

  private void determineWaterway(final WKTGeometry geometry) {
    service.suggestInlandShippingWaterway(geometry, this);
  }

  @Override
  public final void onSuccess(final ArrayList<InlandWaterwayType> result) {
    if (!result.isEmpty()) {
      final InlandWaterwayType iwt = result.get(0);
      iwt.setWaterwayCategory(inlandShippingCategories.getWaterwayCategoryByCode(iwt.getWaterwayCategory().getCode()));
      updateWaterwayType(iwt);
    }
    notifyInconclusiveRoute(result);
  }

  private void notifyInconclusiveRoute(final ArrayList<InlandWaterwayType> result) {
    final AeriusException ae = InlandWaterwayTypeUtil.detectInconclusiveRoute("", result);
    if (ae != null) {
      NotificationUtil.broadcast(eventBus, new Notification(ae));
    }
  }

  /**
   * Called when the inland route category was found. The user of this class should make sure the returned geometry is bound to the correct route
   * object as this class doesn't pass the route object.
   * @param InlandWaterwayType found waterway
   */
  protected abstract void updateWaterwayType(final InlandWaterwayType waterwayType);

  @Override
  public final void onFailure(final Throwable caught) {
    NotificationUtil.broadcast(eventBus, new Notification(M.messages().shipWaterwayNotDetermined()));
  }
}
