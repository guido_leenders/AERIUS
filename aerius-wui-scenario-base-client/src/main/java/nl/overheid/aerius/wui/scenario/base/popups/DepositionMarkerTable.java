/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.dom.client.BrowserEvents;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.CellPreviewEvent;
import com.google.gwt.view.client.ProvidesKey;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.wui.main.domain.MarkerUtil;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl.Presenter;

public class DepositionMarkerTable extends CellTable<DepositionMarker> {
  private static final String COLUMN_WIDTH_IMAGE = "20px";

  private final UserContext userContext;

  public DepositionMarkerTable(final EventBus eventBus, final UserContext userContext, final InfoVector infoVector, final Presenter presenter) {
    super(10, R.cellTableResources(), new ProvidesKey<DepositionMarker>() {
      @Override
      public Object getKey(final DepositionMarker item) {
        return item.getMarkerType();
      }
    });
    this.userContext = userContext;

    // Marker image column
    final Column<DepositionMarker, ImageResource> columnImage =
        new Column<DepositionMarker, ImageResource>(new ImageResourceCell()) {
      @Override
      public ImageResource getValue(final DepositionMarker marker) {
        return MarkerUtil.getMarkerImageResourceSmall(marker.getMarkerType());
      }
    };
    columnImage.setCellStyleNames(R.css().cursorPointer());
    addColumn(columnImage);
    setColumnWidth(columnImage, COLUMN_WIDTH_IMAGE);

    final TextColumn<DepositionMarker> columnValue = new TextColumn<DepositionMarker>() {
      @Override
      public String getValue(final DepositionMarker obj) {
        return getExplanation(obj);
      }
    };
    addColumn(columnValue);

    // Click handlers for each cell. Unfortunately this is the only way.
    addCellPreviewHandler(new CellPreviewEvent.Handler<DepositionMarker>() {
      @Override
      public void onCellPreview(final CellPreviewEvent<DepositionMarker> event) {
        final DepositionMarker marker = event.getValue();

        //when clicking on image, go to the location of the marker.
        if ((event.getColumn() == 0) && BrowserEvents.CLICK.equals(event.getNativeEvent().getType())) {
          eventBus.fireEvent(new LocationChangeEvent(new BBox(marker.getX(), marker.getY(), 0)));
          presenter.selectReceptor(marker.getX(), marker.getY());
        }
        //when hovering over a cell, highlight it...
        if ("mouseover".equals(event.getNativeEvent().getType())) {
          //highlight the receptor where the marker is placed.
          infoVector.drawReceptor(new AeriusPoint(marker.getX(), marker.getY()));
        } else if ("mouseout".equals(event.getNativeEvent().getType())) {
          infoVector.destroyFeatures();
        }
      }
    });
  }

  private String getExplanation(final DepositionMarker marker) {
    final StringBuilder builder = new StringBuilder();
    for (final DepositionMarkerType markerType : marker.getMarkerValues().keySet()) {
      builder.append(markerSpecificString(markerType, marker.getMarkerValues().get(markerType)));
    }
    return builder.toString();
  }

  private String markerSpecificString(final DepositionMarkerType markerType, final double value) {
    String str = "";
    String valueString = "";
    switch (markerType) {
    case HIGHEST_CALCULATED_DEPOSITION:
      valueString = FormatUtil.formatDepositionPASProofWithUnit(value, userContext.getEmissionResultValueDisplaySettings());
      str = M.messages().infoPanelMarkerCalculatedDeposition(valueString);
      break;
    case HIGHEST_TOTAL_DEPOSITION:
    default:
      valueString = FormatUtil.formatDepositionPASProofWithUnit(value, userContext.getEmissionResultValueDisplaySettings());
      str = M.messages().infoPanelMarkerTotalDeposition(valueString);
      break;
    }
    return str;
  }

}
