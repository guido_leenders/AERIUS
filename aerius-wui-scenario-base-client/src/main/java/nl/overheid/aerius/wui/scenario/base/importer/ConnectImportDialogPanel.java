/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.importer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.File;
import com.google.gwt.dom.client.FileList;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.dom.client.DragEnterHandler;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragLeaveHandler;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DragOverHandler;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.dom.client.DropHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.ui.importer.ImportDialogPanel;
import nl.overheid.aerius.wui.main.ui.importer.ImportProperties;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;

// Just an Editor because the ConfirmCancelDialog needs us to be, but don't create a driver/use @Ignore and such. Be fake. Be proud.
public class ConnectImportDialogPanel extends Composite
implements ImportDialogPanel<ImportProperties>, DropHandler, DragEnterHandler, DragOverHandler, DragLeaveHandler {

  interface ConnectImportDialogPanelUiBinder extends UiBinder<Widget, ConnectImportDialogPanel> {}

  private static final ConnectImportDialogPanelUiBinder UI_BINDER = GWT.create(ConnectImportDialogPanelUiBinder.class);

  interface Style extends CssResource {
    String dragFieldHover();
  }

  @UiField SwitchPanel contentSwitcher;
  @UiField Style style;
  @UiField Label subTitleDiv;
  @UiField FileUpload fileField;
  @UiField HTML errorField;
  @UiField FlowPanel dragField;
  @UiField Label regularImportSwitch;

  private String dragImportFilename = "";

  public ConnectImportDialogPanel(final String subTitle, final boolean dragMode) {
    initWidget(UI_BINDER.createAndBindUi(this));

    addDomHandler(this, DragEnterEvent.getType());
    dragField.addDomHandler(this, DropEvent.getType());
    dragField.addDomHandler(this, DragOverEvent.getType());
    dragField.addDomHandler(this, DragLeaveEvent.getType());

    regularImportSwitch.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        contentSwitcher.showWidget(0);
      }
    });

    subTitleDiv.setText(subTitle);

    fileField.setName(SharedConstants.IMPORT_FILE_FIELD_NAME);
    fileField.ensureDebugId(TestID.INPUT_IMPORTFILE);
    errorField.ensureDebugId(TestID.DIV_IMPORT_ERRORS);

    contentSwitcher.showWidget(dragMode ? 1 : 0);
  }

  /**
   * Using RequestBuilder doesn't automatically set the proper multipart boundary headers.
   *
   * Sending the request natively does.
   *
   * @param fileName
   * @param file
   * @param realName
   */
  //@formatter:off
  private native void sendFile(final String fileName, final File file, String realName) /*-{
    var x = this;

    x.@nl.overheid.aerius.wui.scenario.base.importer.ConnectImportDialogPanel::setDragImportFilename(Ljava/lang/String;)(realName);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', @com.google.gwt.core.client.GWT::getModuleBaseURL()() + @nl.overheid.aerius.shared.SharedConstants::IMPORT_CONNECT_CALCULATION_SERVLET, true);

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 201) {
          x.@nl.overheid.aerius.wui.scenario.base.importer.ConnectImportDialogPanel::onResponseReceived(Ljava/lang/String;)(xhr.responseText)
        }
    };

    var formData = new FormData();
    formData.append(fileName, file);
    x.@nl.overheid.aerius.wui.scenario.base.importer.ConnectImportDialogPanel::onFileUploadStarted()();
    xhr.send(formData);
  }-*/;
  //@formatter:on

  private void onFileUploadStarted() {
    // Switch back to normal import content
    contentSwitcher.showWidget(0);
    fireEvent(new SubmitEvent());
  }

  @Override
  public void onDragOver(final DragOverEvent event) {
    event.preventDefault();
    event.stopPropagation();

    // Yeah.... Due to dragEnter/dragLeave events firing unintuitively, we're
    // doing this.
    dragField.setStyleName(style.dragFieldHover(), true);
  }

  @Override
  public void onDragEnter(final DragEnterEvent event) {
    event.preventDefault();
    event.stopPropagation();

    if (contentSwitcher.getVisibleWidget() != 1) {
      contentSwitcher.showWidget(1);
    }

    dragField.setStyleName(style.dragFieldHover(), true);
  }

  @Override
  public void onDragLeave(final DragLeaveEvent event) {
    event.preventDefault();
    event.stopPropagation();

    dragField.setStyleName(style.dragFieldHover(), false);
  }

  @Override
  public void onDrop(final DropEvent event) {
    event.preventDefault();
    event.stopPropagation();

    // TODO (optional) Add multiple file upload support (also for classic upload
    // field)
    final FileList files = event.getDataTransfer().getFiles();
    if (files.length() > 0) {
      sendFile(SharedConstants.IMPORT_FILE_FIELD_NAME, files.getItem(0), files.getItem(0).getName());
    }

    // See above todo
    // for (int i = 0; i < files.size(); i++) {
    // final File file = files.getFile(i);
    //
    // sendFile(SharedConstants.IMPORT_FILE_FIELD_NAME, file);
    // }
  }

  /**
   * This type of importing is kind of a hack, we should ideally get it out of this class and move it somewhere more manageable.
   *
   * Mimicks a {@link SubmitCompleteEvent} of the form panel and sends it to the import dialog, which will handle the full file import from there.
   *
   * @param response Response text sent by the server
   */
  private void onResponseReceived(final String response) {
    fireEvent(new SubmitCompleteEvent(response) {});
  }

  public void showError(final String error) {
    errorField.setHTML(error);
  }

  public void addFileUploadChangedHandler(final ChangeHandler changeHandler) {
    fileField.addChangeHandler(changeHandler);
  }

  public String getFileUploadFilename() {
    String filename = fileField.getFilename();
    if (filename == null || "".equals(filename)) {
      filename = dragImportFilename;
    }

    return filename;
  }

  public void setDragImportFilename(final String dragImportFilename) {
    this.dragImportFilename = dragImportFilename;
  }
}
