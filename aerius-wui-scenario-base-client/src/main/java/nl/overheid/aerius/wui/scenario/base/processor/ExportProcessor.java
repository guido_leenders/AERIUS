/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.processor;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.DownloadInfo;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportStatus;
import nl.overheid.aerius.shared.service.ExportServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.NotificationUtil;

/**
 * Processes export specific messages from the server while the export is running.
 */
@Singleton
public class ExportProcessor {

  private final ExportServiceAsync service;

  private final AsyncCallback<String> initCallback = new AsyncCallback<String>() {
    @Override
    public void onSuccess(final String result) {
      NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportStarted());

      agent.start(result, resultCallback);
    }

    @Override
    public void onFailure(final Throwable caught) {
      onServiceFailure(caught);
    }
  };
  /**
   * When a ExportAgent has retrieved ExportStatus, this callback will be used to process them.
   */
  private final AsyncCallback<ExportStatus> resultCallback = new AsyncCallback<ExportStatus>() {
    @Override
    public void onSuccess(final ExportStatus result) {
      if (result != null) {
        switch (result.getStatus()) {
        case FINISHED:
          final DownloadInfo di = result.getDownloadInfo();
          final String url = di.getUrl() == null ? ""
              : "<a target='_blank' href='" +  SafeHtmlUtils.fromString(di.getUrl()).asString() + "'>"
                  + M.messages().notificationExportDownload() + "</a>";
          if (di.isEmailSuccess()) {
            NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportFinished(), url);
          } else {
            NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportFinishedButNoEmail(di.getEmailAddress()), url);
          }
          finish();
          break;
        case ERROR:
          NotificationUtil.broadcastMessage(eventBus, M.messages().notificationExportFailed());
          finish();
          break;
        case RUNNING:
          break;
        default:
          throw new RuntimeException("Enum value '" + result.getStatus() + "' not implemented.");
        }
      }
    }

    @Override
    public void onFailure(final Throwable caught) {
      onServiceFailure(caught);
    }
  };

  /**
   * ExportAgent used to track the export status
   */
  private final ExportPollingAgent agent;

  /**
   * EventBus used to broadcast events over
   */
  private final EventBus eventBus;

  /**
   * Initializes the ExportProcessor with the given service and callback.
   *
   * @param service service used to export
   * @param agent agent used to get data
   * @param eventBus event bus
   */
  @Inject
  public ExportProcessor(final ExportServiceAsync service, final ExportPollingAgent agent, final EventBus eventBus) {
    this.service = service;
    this.agent = agent;
    this.eventBus = eventBus;
  }

  /**
   * Initializes the processor by passing the EventBus that Notifications will be broadcasted over.
   * Start export for the given properties..
   *
   * @param exportProperties the properties to export with
   * @param scenario the scenario to export with
   */
  public void start(final ExportProperties exportProperties, final CalculatedScenario scenario) {
    service.exportCalculation(exportProperties, scenario, initCallback);
  }

  public void finish() {
    agent.stop();
  }

  private void onServiceFailure(final Throwable caught) {
    // Ignore, the user will get an e-mail with the export anyway. 
  }
}
