/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.List;

import com.google.gwt.editor.client.adapters.EditorSource;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.user.client.TakesValue;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Editor for the showing and editing the water type of all inland shippingRoute's for inland mooring.
 */
class InlandMooringRoutesEditor extends ListEditor<ShippingRoute, InlandWaterwayTypeEditor> implements TakesValue<List<ShippingRoute>> {

  public InlandMooringRoutesEditor(final InlandWaterwayTypePanel panel, final EventBus eventBus, final HelpPopupController hpC,
      final CalculatorServiceAsync service, final InlandShippingCategories categories) {
    super(new EditorSource<InlandWaterwayTypeEditor>() {
      @Override
      public InlandWaterwayTypeEditor create(final int index) {
       final InlandWaterwayTypeEditor editor = new InlandWaterwayTypeEditor(eventBus, hpC, service, categories, true);
       panel.getContentPanel().insert(editor, index);
       return editor;
      }

      @Override
      public void dispose(final InlandWaterwayTypeEditor subEditor) {
        panel.getContentPanel().remove(subEditor);
      }
    });
  }

  @Override
  public List<ShippingRoute> getValue() {
    return getList();
  }
}
