/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.export;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.TextValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Panel for editing {@link ScenarioMetaData}.
 */
class ScenarioMetaDataPanel extends Composite implements Editor<ScenarioMetaData> {

  private static final ScenarioMetaDataPanelUiBinder UI_BINDER = GWT.create(ScenarioMetaDataPanelUiBinder.class);

  interface ScenarioMetaDataPanelUiBinder extends UiBinder<Widget, ScenarioMetaDataPanel> { }

  @UiField TextValueBox corporation;
  @UiField TextValueBox projectName;
  @UiField TextValueBox streetAddress;
  @UiField TextValueBox postcode;
  @UiField TextValueBox city;

  @UiField @Ignore TextArea description;
  ValidatedValueBoxEditor<String> descriptionEditor;

  private final Validator<String> corporationValidator = new NotEmptyValidator(M.messages().exportCorporation()) {
    @Override
    protected String getMessage(final String value) {
      return M.messages().ivExportNoEmptyValue(M.messages().exportCorporation());
    }
  };
  private final Validator<String> projectValidator = new NotEmptyValidator(M.messages().exportProjectName()) {
    @Override
    protected String getMessage(final String value) {
      return M.messages().ivExportNoEmptyValue(M.messages().exportProjectName());
    }
  };
  private final Validator<String> streetAddressValidator = new NotEmptyValidator(M.messages().exportStreetAddress()) {
    @Override
    protected String getMessage(final String value) {
      return M.messages().ivExportNoEmptyValue(M.messages().exportStreetAddress());
    }
  };
  private final Validator<String> postcodeValidator = new NotEmptyValidator(M.messages().exportPostcode()) {
    @Override
    protected String getMessage(final String value) {
      return M.messages().ivExportNoEmptyValue(M.messages().exportPostcode());
    }
  };
  private final Validator<String> cityValidator = new NotEmptyValidator(M.messages().exportCity()) {
    @Override
    protected String getMessage(final String value) {
      return M.messages().ivExportNoEmptyValue(M.messages().exportCity());
    }
  };

  private final Validator<String> descriptionValidator = new NotEmptyValidator(M.messages().exportDescription()) {
    @Override
    protected String getMessage(final String value) {
      return M.messages().ivExportNoEmptyValue(M.messages().exportDescription());
    }
  };

  public ScenarioMetaDataPanel(final HelpPopupController hpC) {
    initWidget(UI_BINDER.createAndBindUi(this));
    descriptionEditor = new ValidatedValueBoxEditor<String>(description, M.messages().exportDescription()) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().ivNoValidInput(M.messages().exportDescription(), value);
      }
    };
    description.getElement().setAttribute("maxlength", String.valueOf(SharedConstants.PAA_DESCRIPTION_LENGTH));
    StyleUtil.I.setPlaceHolder(corporation, M.messages().exportCorporation());
    StyleUtil.I.setPlaceHolder(projectName, M.messages().exportProjectName());
    StyleUtil.I.setPlaceHolder(streetAddress, M.messages().exportStreetAddress());
    StyleUtil.I.setPlaceHolder(postcode, M.messages().exportPostcode());
    StyleUtil.I.setPlaceHolder(city, M.messages().exportCity());
    StyleUtil.I.setPlaceHolder(description, M.messages().exportDescription());
    corporation.ensureDebugId(TestID.INPUT_EXPORT_COORPORATION);
    projectName.ensureDebugId(TestID.INPUT_EXPORT_PROJECTNAME);
    streetAddress.ensureDebugId(TestID.INPUT_EXPORT_STREET_ADDRESS);
    postcode.ensureDebugId(TestID.INPUT_EXPORT_POSTCODE);
    city.ensureDebugId(TestID.INPUT_EXPORT_CITY);
    description.ensureDebugId(TestID.INPUT_EXPORT_DESCRIPTION);
    hpC.addWidget(corporation, hpC.tt().ttExportCorporation());
    hpC.addWidget(projectName, hpC.tt().ttExportProjectName());
    hpC.addWidget(streetAddress, hpC.tt().ttExportLocation());
    hpC.addWidget(postcode, hpC.tt().ttExportLocation());
    hpC.addWidget(city, hpC.tt().ttExportLocation());
    hpC.addWidget(description, hpC.tt().ttExportDescription());
  }

  /**
   * Add the validators on the paa field.
   */
  public void addPAAValidators() {
    projectName.addValidator(projectValidator);
    corporation.addValidator(corporationValidator);
    streetAddress.addValidator(streetAddressValidator);
    postcode.addValidator(postcodeValidator);
    city.addValidator(cityValidator);
    descriptionEditor.addValidator(descriptionValidator);
  }

  /**
   * Reemove the validators on the paa field.
   */
  public void removePAAValidators() {
    projectName.removeValidator(projectValidator);
    corporation.removeValidator(corporationValidator);
    streetAddress.removeValidator(streetAddressValidator);
    postcode.removeValidator(postcodeValidator);
    city.removeValidator(cityValidator);
    descriptionEditor.removeValidator(descriptionValidator);
  }
}
