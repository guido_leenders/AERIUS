/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.Composite;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.source.HasShippingRoute;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Puts {@link InlandRouteWaterwayTypeEditor} in {@link CollapsiblePanel}. Route has a single route.
 */
class InlandRouteWaterwayTypeEditor extends Composite implements Editor<HasShippingRoute> {

  @Path("")
  final InlandWaterwayTypeEditor inlandWaterwayTypeEditor;
  private InlandWaterwayTypePanel panel;

  public InlandRouteWaterwayTypeEditor(final EventBus eventBus, final HelpPopupController hpC, final CalculatorServiceAsync service,
      final InlandShippingCategories categories) {
    panel = new InlandWaterwayTypePanel(false, M.messages().inlandShipWaterwayDirection());

    inlandWaterwayTypeEditor = new InlandWaterwayTypeEditor(eventBus, hpC, service, categories, false);
    panel.getContentPanel().add(inlandWaterwayTypeEditor);
    inlandWaterwayTypeEditor.ensureDebugId(TestID.DIV_COLLAPSEPANEL_SOURCECHARACTERISTICS);
    initWidget(panel);
  }

  public void setDirectionLabelVisible(final boolean visible) {
    panel.setDirectionLabelVisible(visible);
  }
}
