/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.place.Situation;

public interface ResultFilterView extends IsWidget, HasTitle {

  interface Presenter {

    /**
     * @param id
     * @param selectedHabitatId
     * @param receptorCallback
     */
    void getHabitates(int id, int selectedHabitatId, AsyncCallback<HashMap<Integer, Double>> receptorCallback);

  }

  void setPresenter(Presenter presenter);

  void setEmissionResultKey(EmissionResultKey value);

  /**
   * Sets the "no data" state. If loading true, it means the data is still being processed. If false there is no data to display. Where noDataText is
   * the reason why there is no data
   * @param loading state if still loading data
   * @param noDataText if loading is false this is the reason no data is available.
   */
  void setNoData(boolean loading, String noDataText);

  /**
   * Set the data for the habitat filter panel.
   *
   * Does not allow null values.
   *
   * @param sit situation
   * @param resultKey emission results key
   * @param summary summary data
   * @param calcId1 id of calculation 1
   * @param calcId2 id of calculation 2, -1 if situation isn't a comparison situation
   */
  void setValue(Situation sit, EmissionResultKey resultKey, CalculationSummary summary, int calcId1, int calcId2);

  /**
   * @param loading
   * @param noDataText
   */

}
