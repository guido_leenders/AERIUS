/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gwtopenmaps.openlayers.client.Bounds;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.geo.ReceptorTileSet;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Calculation result manager holding a reference to all results that have
 * accumulated on the client so far.
 *
 * <p>TODO This class currently depends on CalculatorPlace. It is probably better to
 * remove this dependency as it should not be dependent on a specific place object.
 * This will be relevant when implementing  AERIUS Scenario project when more than
 * 2 results could be present. Current implementation is acceptable till then.
 */
@Singleton
public class ReceptorPointsContainer {
  /**
   * Class to dynamically calculate the deposition difference from 2 situations.
   */
  @SuppressWarnings("serial")
  private class DiffAeriusPoint extends AeriusResultPoint {

    Map<Integer, AeriusResultPoint> points = new HashMap<>();

    public DiffAeriusPoint(final int id, final double x, final double y) {
      super(id, AeriusPointType.POINT, x, y);
    }

    public void addPoint(final Integer id, final AeriusResultPoint rp) {
      points.put(id, rp);
    }

    @Override
    public double getEmissionResult(final EmissionResultKey evk) {
      final AeriusResultPoint rp2 = points.get(place.getSid2());
      final AeriusResultPoint rp1 = points.get(place.getSid1());
      return (rp2 == null ? 0 : rp2.getEmissionResult(evk)) - (rp1 == null ? 0 : rp1.getEmissionResult(evk));
    }
  }

  private final int receptorsInTile;

  private final Map<Integer, HashMap<HexagonZoomLevel, ReceptorTileSet>> results;
  private final Map<HexagonZoomLevel, ReceptorTileSet> diffResults;
  private final ReceptorUtil receptorUtil;
  private final BBox receptorBounding;
  private final List<HexagonZoomLevel> zoomLevels;
  private final HexagonZoomLevel zoomLevel1;
  private CalculatedScenario scenario;
  private ScenarioBasePlace place;

  /**
   * Default constructor.
   */
  @Inject
  public ReceptorPointsContainer(final Context context) {
    receptorsInTile = context.getSetting(SharedConstantsEnum.CALCULATOR_LIMITS_RECEPTORS_RECEPTOR_TILE);
    results = new HashMap<>();
    diffResults = new HashMap<>();
    final ReceptorGridSettings rgs = context.getReceptorGridSettings();
    receptorUtil = new ReceptorUtil(rgs);
    receptorBounding = rgs.getBoundingBox();
    zoomLevel1 = rgs.getZoomLevel1();
    zoomLevels = rgs.getHexagonZoomLevels();
  }

  public String reasonNoResults(final int year, final EmissionResultKey key) {
    return "";
  }

  /**
   * Clear the result and refill with base information.
   *
   * @param scenario new scenario
   */
  public void setCalculations(final CalculatedScenario scenario) {
    this.scenario = scenario;
    if (scenario == null) {
      results.clear();
    } else {
      for (final Calculation list: scenario.getCalculations()) {
        final HashMap<HexagonZoomLevel, ReceptorTileSet> map = new HashMap<>();
        results.put(list.getSources().getId(), map);
        for (final HexagonZoomLevel level : zoomLevels) {
          //ensure all results are cleared, not just the current situation being calculated,
          //we could end up with a comparison field which does not match the current situation 1/2.
          map.put(level, new ReceptorTileSet(receptorUtil, receptorsInTile, receptorBounding, level));
        }
      }
      for (final HexagonZoomLevel level : zoomLevels) {
        //ensure all results are cleared, not just the current situation being calculated,
        //we could end up with a comparison field which does not match the current situation 1/2.
        diffResults.put(level, new ReceptorTileSet(receptorUtil, receptorsInTile, receptorBounding, level));
      }
    }
  }

  /**
   * Distributes the AeriusPoints into the given HashMap.
   * So long as the AeriusPoint is at a specific ZoomLevel, will it be added to the Map.
   * This method requires that all ZoomLevels be present in the Map before it is passed.
   *
   * @param calculationId id of the calculation
   * @param values Receptors that are to be distributed into the AeriusPointsContainer
   */
  public void distributeReceptors(final int calculationId, final HashSet<AeriusResultPoint> values) {
    final EmissionSourceList esl = scenario == null ? null : scenario.getSources(calculationId);
    if (esl == null) {
      // ignore results... probably old calculations.
      return;
    }
    final HashMap<HexagonZoomLevel, ReceptorTileSet> map = results.get(esl.getId());

    for (final AeriusResultPoint point : values) {
      if (!GWT.isScript() && point == null) {
        // This will not happen in production, but due to glitches in Dev mode might. (log it anyway)
        GWT.log("AeriusPoint is null", new IllegalStateException("AeriusPoint is null which is something that can never happen."));
        continue;
      }

      DiffAeriusPoint diff = null;
      final boolean diffFound;
      if (scenario instanceof CalculatedComparison) {
        // Get the EmissionSourceList of comparing situation
        diff = findDiffAeriusPoint(point.getId());
        if (diff == null) {
          diffFound = false;
          diff = new DiffAeriusPoint(point.getId(), point.getX(), point.getY());
          diff.addPoint(esl.getId(), point);
        } else {
          diff.addPoint(esl.getId(), point);
          diffFound = true;
        }
      } else {
        diffFound = false;
      }

      for (final HexagonZoomLevel level : zoomLevels) {
        // If the receptor is NOT at zoom level 1 AND the receptor is NOT present in the given zoom level, stop adding
        if (!level.equals(zoomLevel1) && !receptorUtil.isReceptorAtZoomLevel(point, level)) {
          break;
        }

        // This is safe by contract
        map.get(level).add(point);
        if (diff != null && !diffFound) {
          diffResults.get(level).add(diff);
        }
      }
    }
  }

  /**
   * Finds a receptor point by looping over the entire list of calculated points.
   *
   * To be used internally, where usage of Situation and its ID can be controlled.
   *
   * @param recId the receptor ID to find
   *
   * @return A {@link AeriusPoint} with the given ID
   */
  private AeriusResultPoint findAeriusPoint(final ScenarioBasePlace place, final int recId) {
    if (place.getSituation() == Situation.COMPARISON) {
      return findDiffAeriusPoint(recId);
    } else {
      return findAeriusPoint(place.getActiveSituationId(), recId);
    }
  }

  private DiffAeriusPoint findDiffAeriusPoint(final int recId) {
    final AeriusResultPoint arp = diffResults.get(zoomLevel1).findAeriusPoint(recId);
    return arp instanceof DiffAeriusPoint ? (DiffAeriusPoint) arp : null;
  }

  private AeriusResultPoint findAeriusPoint(final int emissionSourceListId, final int recId) {
    if (results.containsKey(emissionSourceListId)) {
      return results.get(emissionSourceListId).get(zoomLevel1).findAeriusPoint(recId);
    }

    return null;
  }

  /**
   * Returns an {@link Iterator} of {@link AeriusResultPoint}s that are in a given {@link HexagonZoomLevel} and reside
   * in the given {@link Bounds}.
   *
   * @param level {@link HexagonZoomLevel} the {@link Point}s should be in
   * @param extent {@link Bounds} the {@link Point}s should be in
   *
   * @return {@link Iterator} with {@link AeriusResultPoint}s
   */
  public Iterable<AeriusResultPoint> getReceptorTilesForLevel(final HexagonZoomLevel level, final Bounds extent) {
    final Point topLeftReceptor = new Point(extent.getLowerLeftX(), extent.getLowerLeftY());
    final Point bottomRightReceptor = new Point(extent.getUpperRightX(), extent.getUpperRightY());
    final Map<HexagonZoomLevel, ReceptorTileSet> sitResults = place.getSituation() == Situation.COMPARISON
        ? diffResults : results.get(place.getActiveSituationId());

    if (sitResults == null) {
      return null;
    }

    return sitResults.get(level).getTileSet(topLeftReceptor, bottomRightReceptor);
  }

  /**
   * Sets the current place.
   * @param place place
   */
  public void setPlace(final ScenarioBasePlace place) {
    this.place = place;
  }

  /**
   * Gets filtered receptors from the current place.
   *
   * @param recWithSurfaces receptor id's to get
   * @return new set with filtered receptors.
   */
  public HashMap<AeriusResultPoint, Double> getFilteredReceptors(final HashMap<Integer, Double> recWithSurfaces) {
    final HashMap<AeriusResultPoint, Double> filtered = new HashMap<>();

    for (final Entry<Integer, Double> entry : recWithSurfaces.entrySet()) {
      final AeriusResultPoint rec = findAeriusPoint(place, entry.getKey());
      if (rec == null) {
        continue;
      }

      filtered.put(rec, entry.getValue());
    }

    return filtered;
  }
}
