/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.UseBuildingChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.DoubleRangeValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.InputWithUnitWidget;

/**
 * Editor for {@link OPSSourceCharacteristics}.
 */
public class OPSSourceOutflowForcedEditor extends Composite implements Editor<OPSSourceCharacteristics>, HasValueChangeHandlers<Boolean> {

  public interface OPSSourceOutflowForcedViewDriver extends SimpleBeanEditorDriver<OPSSourceCharacteristics, OPSSourceOutflowForcedEditor> {}

  interface OPSSourceOutflowForcedEventBinder extends EventBinder<OPSSourceOutflowForcedEditor> {}
  interface OPSSourceOutflowWidgetUiBinder extends UiBinder<Widget, OPSSourceOutflowForcedEditor> {}

  private static final OPSSourceOutflowForcedEventBinder EVENT_BINDER = GWT.create(OPSSourceOutflowForcedEventBinder.class);
  private static final OPSSourceOutflowWidgetUiBinder UI_BINDER = GWT.create(OPSSourceOutflowWidgetUiBinder.class);

  @UiField(provided = true) @Path("") OPSSourceBuildingEditor buildingEditor;

  @UiField @Ignore InputWithUnitWidget emissionHeightWidget;
  @UiField @Ignore InputWithUnitWidget outflowDiameterWidget;
  @UiField(provided = true) ListBoxEditor<OutflowDirectionType> outflowDirectionEditor = new ListBoxEditor<OutflowDirectionType>() {
    @Override
    protected String getLabel(final OutflowDirectionType value) {
      return M.messages().sourceOutflowDirectionType(value);
    }
  };
  @UiField @Ignore InputWithUnitWidget outflowVelocityWidget;

  @UiField(provided = true) DoubleValueBox emissionHeightEditor = new DoubleValueBox(M.messages().sourceHeight(),
      OPSLimits.SOURCE_HEIGHT_DIGITS_PRECISION);
  @UiField(provided = true) DoubleValueBox emissionTemperature = new DoubleValueBox(M.messages().sourceTemperature(),
      OPSLimits.SOURCE_TEMPERATURE_DIGITS_PRECISION);
  @UiField(provided = true) DoubleValueBox outflowDiameter = new DoubleValueBox(M.messages().sourceOutflowDiameter(),
      OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION);
  @UiField(provided = true) DoubleValueBox outflowVelocity = new DoubleValueBox(M.messages().sourceOutflowVelocity(),
      OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION);

  private final DoubleRangeValidator emissionHeightValidator = new DoubleRangeValidator(OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
      OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivStringRangeBetween(M.messages().sourceHeight(),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, 0),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM, OPSLimits.SOURCE_HEIGHT_DIGITS_PRECISION));
    }
  };

  private final DoubleRangeValidator emissionHeightValidatorWarning = new DoubleRangeValidator(OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
      OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceHeight(),
          OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
          OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM);
    }
  };

  private final DoubleRangeValidator emissionTemperatureValidator = new DoubleRangeValidator(0D, OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivStringRangeBetween(M.messages().sourceTemperature(),
          M.messages().decimalNumberFixed(0D, OPSLimits.SOURCE_TEMPERATURE_DIGITS_PRECISION),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_EMISSION_TEMPERATURE_MAXIMUM, OPSLimits.SOURCE_TEMPERATURE_DIGITS_PRECISION));
    }
  };
  private final DoubleRangeValidator outflowDiameterValidator = new DoubleRangeValidator(OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM,
      OPSLimits.SOURCE_OUTFLOW_DIAMETER_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivStringRangeBetween(M.messages().sourceOutflowDiameter(),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM, OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_OUTFLOW_DIAMETER_MAXIMUM, OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION));
    }
  };
  private final DoubleRangeValidator outflowDiameterValidatorWarning =
      new DoubleRangeValidator(OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM, OPSLimits.SOURCE_IS_BUILDING_OUTFLOW_DIAMETER_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceOutflowDiameter(),
          OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM,
          OPSLimits.SOURCE_IS_BUILDING_OUTFLOW_DIAMETER_MAXIMUM);
    }
  };
  private final DoubleRangeValidator outflowVelocityValidator = new DoubleRangeValidator(OPSLimits.SOURCE_UI_OUTFLOW_VELOCITY_MINIMUM,
      OPSLimits.SOURCE_OUTFLOW_VELOCITY_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivStringRangeBetween(M.messages().sourceOutflowVelocity(),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_UI_OUTFLOW_VELOCITY_MINIMUM, 0),
          M.messages().decimalNumberFixed(OPSLimits.SOURCE_OUTFLOW_VELOCITY_MAXIMUM, OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION));
    }
  };
  private final DoubleRangeValidator outflowVelocityValidatorWarning =
      new DoubleRangeValidator(OPSLimits.SOURCE_UI_OUTFLOW_VELOCITY_MINIMUM, OPSLimits.SOURCE_IS_BUILDING_OUTFLOW_VELOCITY_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceOutflowVelocity(), OPSLimits.SOURCE_UI_OUTFLOW_VELOCITY_MINIMUM,
          OPSLimits.SOURCE_IS_BUILDING_OUTFLOW_VELOCITY_MAXIMUM);
    }
  };

  private boolean buildingInfluence;
  private Double oldTemperature;

  /**
   * The characteristics of calculated heatcontent and building sources may be set in this editor.
   *
   * @param hpC the popup-controller.
   * @param opsFactsheetUrl
   */
  public OPSSourceOutflowForcedEditor(final EventBus eventBus, final HelpPopupController hpC, final String opsFactsheetUrl) {
    buildingEditor = new OPSSourceBuildingEditor(eventBus, hpC, opsFactsheetUrl);
    initWidget(UI_BINDER.createAndBindUi(this));
    outflowDirectionEditor.addItems(OutflowDirectionType.values());

    hpC.addWidget(emissionHeightEditor, hpC.tt().ttOPSHeight());
    emissionHeightEditor.ensureDebugId(TestID.INPUT_OPS_HEIGHT);
    emissionTemperature.ensureDebugId(TestID.INPUT_EMISSION_TEMPERATURE);
    outflowDiameter.ensureDebugId(TestID.INPUT_OUTFLOW_DIAMETER);
    outflowDirectionEditor.ensureDebugId(TestID.INPUT_OPS_OUTFLOW_DIRECTION);
    outflowVelocity.ensureDebugId(TestID.INPUT_OUTFLOW_VELOCITY);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @EventHandler
  void onBuildingChange(final UseBuildingChangeEvent e) {
    setBuildingInfluence(e.getValue());
  }

  @UiHandler({"emissionHeightEditor", "emissionTemperature", "outflowDiameter", "outflowVelocity"})
  void onChangeEvent(final ChangeEvent e) {
    validateWarnings();
  }

  private void validateWarnings() {
    emissionHeightEditor.setValue(emissionHeightEditor.getValue());
    if (emissionTemperature.getValue() > 0) {
      emissionTemperature.setValue(emissionTemperature.getValue());
    }
    outflowDiameter.setValue(outflowDiameter.getValue());
    outflowVelocity.setValue(outflowVelocity.getValue());

    if (buildingInfluence) {
      final boolean validateWarningHeight = emissionHeightValidatorWarning.validate(emissionHeightEditor.getValue()) != null;
      emissionHeightWidget.setStyleName(R.css().suggestionWarningBox(), validateWarningHeight);

      final boolean validateWarningOutflow = outflowDiameterValidatorWarning.validate(outflowDiameter.getValue()) != null;
      outflowDiameterWidget.setStyleName(R.css().suggestionWarningBox(), validateWarningOutflow);

      final boolean validateWarningVelocity = outflowVelocityValidatorWarning.validate(outflowVelocity.getValue()) != null;
      outflowVelocityWidget.setStyleName(R.css().suggestionWarningBox(), validateWarningVelocity);

      buildingEditor.setOtherWarning(validateWarningHeight || validateWarningOutflow || validateWarningVelocity);
    } else {
      emissionHeightWidget.setStyleName(R.css().suggestionWarningBox(), false);
      outflowDiameterWidget.setStyleName(R.css().suggestionWarningBox(), false);
      outflowVelocityWidget.setStyleName(R.css().suggestionWarningBox(), false);
    }
  }

  public void setBuildingEditorVisible(final boolean visible) {
    buildingEditor.setVisible(visible);
  }

  public void setEventBus(final EventBus eventBus) {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }

  public void setBuildingInfluence(final boolean buildingInfluence) {
    this.buildingInfluence = buildingInfluence;
    if (buildingInfluence) {
      emissionHeightEditor.removeValidator(emissionHeightValidator);
      emissionTemperature.removeValidator(emissionTemperatureValidator);
      outflowDiameter.removeValidator(outflowDiameterValidator);
      outflowVelocity.removeValidator(outflowVelocityValidator);
      oldTemperature = emissionTemperature.getValue();
      emissionTemperature.setValue(OPSSourceCharacteristics.AVERAGE_SURROUNDING_TEMPERATURE);
      if (emissionHeightEditor.getValue() > OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM) {
        emissionHeightEditor.setValue(OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM);
      }
    } else {
      emissionHeightEditor.addValidator(emissionHeightValidator);
      emissionTemperature.addValidator(emissionTemperatureValidator);
      outflowDiameter.addValidator(outflowDiameterValidator);
      outflowVelocity.addValidator(outflowVelocityValidator);
      if (oldTemperature != null) {
        emissionTemperature.setValue(oldTemperature);
      }
      buildingEditor.setOtherWarning(false);
    }
    emissionTemperature.setEnabled(!buildingInfluence);
    validateWarnings();
  }

}

