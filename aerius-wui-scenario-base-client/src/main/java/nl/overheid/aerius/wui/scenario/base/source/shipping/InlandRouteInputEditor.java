/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.HasEditorDelegate;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategories;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.editor.ListBoxEditor;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.PercentageRangeValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;

/**
 * Editor for single Inland ship group.
 */
class InlandRouteInputEditor extends Composite implements Editor<RouteInlandVesselGroup>, HasEditorDelegate<RouteInlandVesselGroup>,
    InputEditor<RouteInlandVesselGroup> {

  interface InlandRouteInputDriver extends SimpleBeanEditorDriver<RouteInlandVesselGroup, InlandRouteInputEditor> { }

  interface InlandShipInputEditorUiBinder extends UiBinder<Widget, InlandRouteInputEditor> { }

  private static final InlandShipInputEditorUiBinder UI_BINDER = GWT.create(InlandShipInputEditorUiBinder.class);

  private static final TextMachine<InlandShippingCategory> TEXT_MACHINE = new ShippingCategoryTextMachine<>();

  @UiField @Ignore TextBox name;
  @UiField(provided = true) SuggestBox<InlandShippingCategory> category;
  @UiField(provided = true) ListBoxEditor<TimeUnit> timeUnitShipsAtoB;
  @UiField(provided = true) ListBoxEditor<TimeUnit> timeUnitShipsBtoA;
  @UiField IntValueBox numberOfShipsAtoBperTimeUnit;
  @UiField IntValueBox numberOfShipsBtoAperTimeUnit;
  @UiField IntValueBox percentageLadenAtoB;
  @UiField IntValueBox percentageLadenBtoA;
  @UiField @Ignore Button submitButton;
  ValidatedValueBoxEditor<String> nameEditor;

  private final InlandShippingCategories inlandShippingCategories;
  private final InlandRouteMarkerContainer inlandRouteMarkerContainer;
  private final InlandShipCategorySuggestionOracle oracle;
  private Widget inlandRouteDirectionWidget;

  private EditorDelegate<RouteInlandVesselGroup> delegate;
  private InlandWaterwayType waterwayType;


  public InlandRouteInputEditor(final EventBus eventBus, final SourceMapPanel map, final InlandShippingCategories inlandShippingCategories,
      final HelpPopupController hpC) {
    this.inlandShippingCategories = inlandShippingCategories;
    inlandRouteMarkerContainer = (InlandRouteMarkerContainer) map.getInlandRouteContainer();
    oracle = new InlandShipCategorySuggestionOracle(inlandShippingCategories);
    category = new SuggestBox<>(oracle, TEXT_MACHINE);
    timeUnitShipsAtoB = new ListBoxEditor<TimeUnit>() {
      @Override
      protected String getLabel(final TimeUnit value) {
        return M.messages().timeUnit(value);
      }
    };

    timeUnitShipsBtoA = new ListBoxEditor<TimeUnit>() {
      @Override
      protected String getLabel(final TimeUnit value) {
        return M.messages().timeUnit(value);
      }
    };
    initWidget(UI_BINDER.createAndBindUi(this));
    nameEditor = new ShippingValidatedValueBoxEditor(name);
    timeUnitShipsAtoB.addItems(TimeUnit.values());
    timeUnitShipsBtoA.addItems(TimeUnit.values());
    percentageLadenAtoB.addValidator(new PercentageRangeValidator() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivInlandShippingPercentageLaden(value);
      }
    });
    percentageLadenBtoA.addValidator(new PercentageRangeValidator() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivInlandShippingPercentageLaden(value);
      }
    });

    // Help widgets
    hpC.addWidget(name, hpC.tt().ttInlandShipName());
    hpC.addWidget(category, hpC.tt().ttInlandShipType());

    hpC.addWidget(timeUnitShipsAtoB, hpC.tt().ttInlandShipTimeUnitAtoB());
    hpC.addWidget(timeUnitShipsBtoA, hpC.tt().ttInlandShipTimeUnitBtoA());

    hpC.addWidget(numberOfShipsAtoBperTimeUnit, hpC.tt().ttInlandShipAmountAtoB());
    hpC.addWidget(numberOfShipsBtoAperTimeUnit, hpC.tt().ttInlandShipAmountBtoA());
    hpC.addWidget(percentageLadenAtoB, hpC.tt().ttInlandShipPercentageLadenAtoB());
    hpC.addWidget(percentageLadenBtoA, hpC.tt().ttInlandShipPercentageLadenBtoA());
    hpC.addWidget(submitButton, hpC.tt().ttInlandShipSave());

    name.ensureDebugId(TestID.INPUT_SHIPPING_NAME);
    category.getValueBox().ensureDebugId(TestID.INPUT_SHIPPING_CATEGORY);

    timeUnitShipsAtoB.ensureDebugId(TestID.INPUT_INLAND_TIMEUNIT_ATOB);
    timeUnitShipsBtoA.ensureDebugId(TestID.INPUT_INLAND_TIMEUNIT_BTOA);
    numberOfShipsAtoBperTimeUnit.ensureDebugId(TestID.INPUT_INLAND_SHIPPING_AMOUNT_ATOB);
    numberOfShipsBtoAperTimeUnit.ensureDebugId(TestID.INPUT_INLAND_SHIPPING_AMOUNT_BTOA);
    percentageLadenAtoB.ensureDebugId(TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_ATOB);
    percentageLadenBtoA.ensureDebugId(TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_BTOA);
    submitButton.ensureDebugId(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  @Override
  public HandlerRegistration addClickHandler(final ClickHandler handler) {
    return submitButton.addClickHandler(handler);
  }

  @Override
  public void postSetValue(final EmissionSource source) {
    inlandRouteMarkerContainer.setMarkers(source);
    final InlandRouteEmissionSource ires = (InlandRouteEmissionSource) source;
    setInlandWaterwayType(ires.getWaterwayCategory(), ires.getWaterwayDirection());
  }

  /**
   * Add a reference to the route direction widget to be able to report it if the direction is not correctly set.
   * @param widget
   */
  public void setInlandRouteDirectionWidget(final Widget widget) {
    this.inlandRouteDirectionWidget = widget;
  }

  public void setInlandWaterwayType(final InlandWaterwayCategory category, final WaterwayDirection direction) {
    oracle.setInlandWaterwayCategory(category);
    waterwayType = new InlandWaterwayType(category, direction);
  }

  /**
   * Force empty content on the IntValueBox widgets when a new data object is edited.
   * Otherwise the initial value will be 0 and the placeholder text won't be visible.
   */
  @Override
  public void resetPlaceholders() {
    numberOfShipsAtoBperTimeUnit.resetPlaceHolder();
    numberOfShipsBtoAperTimeUnit.resetPlaceHolder();
    percentageLadenAtoB.resetPlaceHolder();
    percentageLadenBtoA.resetPlaceHolder();
  }

  @Override
  protected void onUnload() {
    super.onUnload();
    inlandRouteMarkerContainer.clear();
  }

  @Override
  public void setDelegate(final EditorDelegate<RouteInlandVesselGroup> delegate) {
    this.delegate = delegate;
  }

  // When clicking to save validate if there is a valid direction set, it's needed to determine the emission.
  // If it's not correct record it as an error so the flushing will stop on this error.
  @UiHandler("submitButton")
  void onClickSubmitButton(final ClickEvent e) {
    if (delegate == null) {
      return;
    }
    if (waterwayType != null && !InlandWaterwayTypeUtil.isDirectionCorrect(waterwayType)) {
      delegate.recordError(M.messages().ivInlandShipDirectionNotEmpty(), null, inlandRouteDirectionWidget);
    }
    final InlandShippingCategory categoryValue = category.getValue();
    if (waterwayType != null && categoryValue != null
        && !inlandShippingCategories.isValidCombination(categoryValue, waterwayType.getWaterwayCategory())) {
      delegate.recordError(M.messages().shipWaterwayNotAllowedCombination(
          categoryValue.getCode(), waterwayType.getWaterwayCategory().getCode()), categoryValue, category);
    }

    if (numberOfShipsAtoBperTimeUnit.getValue() == 0) {
      delegate.recordError(M.messages().ivInlandShipMovementsFromAToBNotEmpty(), 0, numberOfShipsAtoBperTimeUnit);
    }

    if (numberOfShipsBtoAperTimeUnit.getValue() == 0) {
      delegate.recordError(M.messages().ivInlandShipMovementsFromBToANotEmpty(), 0, numberOfShipsBtoAperTimeUnit);
    }
  }

}
