/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.ListCancelButtonEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.road.RoadEmissionInputEditor.RoadEmissionInputDriver;

/**
 * Editor for a source with list of vehicles sources.
 */
public class RoadEmissionEditor extends BaseEmissionSourceEditor<SRM2EmissionSource> implements Editor<SRM2EmissionSource> {

  public interface RoadEmissionDriver extends SimpleBeanEditorDriver<SRM2EmissionSource, RoadEmissionEditor> {}

  interface RoadEmissionEditorEventBinder extends EventBinder<RoadEmissionEditor> {}

  private static final int STANDARD = 0;
  private static final int SPECIFIC = 1;
  private static final int USER_SPECIFIED = 2;

  private static class VehiclesMessages implements EditableDivTableMessages {
    private final HelpPopupController hpC;

    public VehiclesMessages(final HelpPopupController hpC) {
      this.hpC = hpC;
    }

    @Override
    public String add() {
      return M.messages().roadVehicleAdd();
    }

    @Override
    public HelpInfo editHelp() {
      return hpC.tt().ttRoadVehicleEdit();
    }

    @Override
    public HelpInfo copyHelp() {
      return hpC.tt().ttRoadVehicleCopy();
    }

    @Override
    public HelpInfo deleteHelp() {
      return hpC.tt().ttRoadVehicleDelete();
    }

    @Override
    public HelpInfo addHelp() {
      return hpC.tt().ttRoadVehicleAdd();
    }

    @Override
    public String customButtonText() {
      return null; // no custom button
    }

    @Override
    public HelpInfo customButtonHelp() {
      return null; // no custom button
    }
  }

  InputListEmissionEditor<VehicleEmissions, RoadEmissionInputEditor> emissionSubSourcesEditor;
  LeafValueEditor<Boolean> freewayEditor = new LeafValueEditor<Boolean>() {

    @Override
    public void setValue(final Boolean value) {
      // no-op
    }

    @Override
    public Boolean getValue() {
      return roadType == RoadType.FREEWAY;
    }
  };

  private final ValueChangeHandler<Integer> selectTypeChangeHandler = new ValueChangeHandler<Integer>() {
    @Override
    public void onValueChange(final ValueChangeEvent<Integer> event) {
      final VehicleEmissions ve;

      switch (event.getValue()) {
      case STANDARD:
        ve = new VehicleStandardEmissions();
        break;
      case SPECIFIC:
        ve = new VehicleSpecificEmissions();
        ((VehicleSpecificEmissions) ve).setRoadType(roadType);
        break;
      case USER_SPECIFIED:
        ve = new VehicleCustomEmissions();
        break;
      default:
        ve = null;
        break;
      }
      if (ve != null) {
        ve.setId(editId);
        emissionSubSourcesEditor.edit(ve);
      }
    }
  };

  private final RoadEmissionEditorEventBinder eventBinder = GWT.create(RoadEmissionEditorEventBinder.class);
  private final EventBus eventBus;
  private final RadioButtonContentResource<Integer> resourceBundle = new RadioButtonContentResource<Integer>() {
    @Override
    public String getRadioButtonText(final Integer value) {
      final String txt;

      switch (value) {
      case STANDARD:
        txt = M.messages().roadStandardVehicle();
        break;
      case SPECIFIC:
        txt = M.messages().roadSpecificVehicle();
        break;
      case USER_SPECIFIED:
        txt = M.messages().roadUserSpecifiedVehicle();
        break;
      default:
        txt = "";
        break;
      }

      return txt;
    }
  };
  private RoadType roadType;
  private Integer editId;

  @SuppressWarnings("unchecked")
  public RoadEmissionEditor(final EventBus eventBus, final SectorCategories categories, final CalculatorServiceAsync service,
      final HelpPopupController hpC) {
    super(M.messages().roadEmissionsTitle());
    this.eventBus = eventBus;

    final RadioButtonGroup<Integer> radioButtonGroup = new RadioButtonGroup<>(resourceBundle);
    radioButtonGroup.ensureDebugId(TestID.ROAD_TOGGLE_BUTTON);
    radioButtonGroup.addButton(STANDARD);
    radioButtonGroup.addButton(SPECIFIC);
    radioButtonGroup.addButton(USER_SPECIFIED);
    radioButtonGroup.addValueChangeHandler(selectTypeChangeHandler);

    emissionSubSourcesEditor = new InputListEmissionEditor<VehicleEmissions, RoadEmissionInputEditor>(eventBus,
        new RoadEmissionInputEditor(eventBus, categories, hpC, radioButtonGroup), hpC, new VehiclesMessages(hpC), VehicleEmissions.class, "Road",
        (SimpleBeanEditorDriver<VehicleEmissions, InputEditor<VehicleEmissions>>) GWT.create(RoadEmissionInputDriver.class), service, false) {

      @Override
      protected VehicleEmissions createNewRowEmissionValues() {
        return new VehicleStandardEmissions();
      }

      @Override
      protected void addNew(final VehicleEmissions value) {
        super.addNew(value);
        editId = value.getId();
      }

      @Override
      public void edit(final VehicleEmissions value) {
        super.edit(value);
        editId = value.getId();
        if (value instanceof VehicleStandardEmissions) {
          radioButtonGroup.setValue(STANDARD);
        } else if (value instanceof VehicleSpecificEmissions) {
          radioButtonGroup.setValue(SPECIFIC);
        } else if (value instanceof VehicleCustomEmissions) {
          radioButtonGroup.setValue(USER_SPECIFIED);
        } // else? there is no else...
      }

      @Override
      protected String getLabel(final VehicleEmissions value) {
        final String label;
        if (value instanceof VehicleStandardEmissions) {
          label = M.messages().roadVehicleType(((VehicleStandardEmissions) value).getEmissionCategory().getVehicleType());
        } else if (value instanceof VehicleSpecificEmissions) {
          label = ((VehicleSpecificEmissions) value).getCategory().getName();
        } else if (value instanceof VehicleCustomEmissions) {
          label = ((VehicleCustomEmissions) value).getDescription();
        } else {
          label = super.getLabel(value);
        }
        return label;
      }

    };

    addContentPanel(emissionSubSourcesEditor);

    emissionSubSourcesEditor.ensureDebugId("");
    radioButtonGroup.ensureDebugId(TestID.LIST_ROAD_SELECT_TYPE);
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @EventHandler
  void onCancelButton(final ListCancelButtonEvent event) {
    emissionSubSourcesEditor.onCancel();
  }

  @Override
  public void postSetValue(final SRM2EmissionSource source) {
    eventBinder.bindEventHandlers(this, eventBus);
    super.postSetValue(source);
    roadType = RoadType.valueFromSectorId(source.getSector().getSectorId());
    emissionSubSourcesEditor.postSetValue(source);
  }

}
