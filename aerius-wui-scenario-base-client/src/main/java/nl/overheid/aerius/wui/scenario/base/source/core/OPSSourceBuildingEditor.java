/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.core;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.UseBuildingChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.validation.DoubleRangeValidator;
import nl.overheid.aerius.wui.main.ui.validation.IntegerRangeValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.InputWithUnitWidget;

/**
 * Editor for {@link OPSSourceCharacteristics}.
 */
public class OPSSourceBuildingEditor extends Composite implements Editor<OPSSourceCharacteristics> {

  private static final OPSSourceBuildingEditorUiBinder UI_BINDER = GWT.create(OPSSourceBuildingEditorUiBinder.class);

  interface OPSSourceBuildingEditorUiBinder extends UiBinder<Widget, OPSSourceBuildingEditor> {}

  @UiField FlowPanel warningPanel;
  @UiField @Ignore HTML warningLabel;
  @UiField @Ignore FlowPanel buildingInfluenceDetailPanel;
  @UiField @Ignore InputWithUnitWidget buildingLengthWidget;
  LeafValueEditor<Boolean> buildingInfluence = new LeafValueEditor<Boolean>() {
    @Override
    public void setValue(final Boolean value) {
      buildingInfluenceCheckBox.setValue(value);
      buildingInfluenceDetailPanel.setVisible(value);
    }

    @Override
    public Boolean getValue() {
      return buildingInfluenceCheckBox.getValue();
    }
  };
  @UiField @Ignore CheckBox buildingInfluenceCheckBox;

  @UiField @Ignore Label buildingInfluenceLabel;
  @UiField @Ignore InputWithUnitWidget buildingWidthWidget;
  @UiField @Ignore InputWithUnitWidget buildingHeightWidget;
  @UiField @Ignore InputWithUnitWidget buildingOrientationWidget;

  @UiField(provided = true) DoubleValueBox buildingLengthEditor = new DoubleValueBox(M.messages().sourceBuildingHeight(),
      OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION);
  @UiField(provided = true) DoubleValueBox buildingWidthEditor = new DoubleValueBox(M.messages().sourceBuildingHeight(),
      OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION);
  @UiField(provided = true) DoubleValueBox buildingHeightEditor = new DoubleValueBox(M.messages().sourceBuildingHeight(),
      OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION);
  @UiField(provided = true) IntValueBox buildingOrientationEditor = new IntValueBox(M.messages().sourceBuildingOrientation());

  private final DoubleRangeValidator buildingLengthValidatorWarning = new DoubleRangeValidator(OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM,
      OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceBuildingLength(), OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM,
          OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM);
    }
  };
  private final DoubleRangeValidator buildingWidthLengthRatioValidatorWarning =
      new DoubleRangeValidator(OPSLimits.SCOPE_BUILDING_WIDTH_LENGTH_RATIO_MINIMUM, OPSLimits.SCOPE_BUILDING_WIDTH_LENGTH_RATIO_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceBuildingWidth(), OPSLimits.SCOPE_BUILDING_WIDTH_LENGTH_RATIO_MINIMUM,
          OPSLimits.SCOPE_BUILDING_WIDTH_LENGTH_RATIO_MAXIMUM);
    }
  };
  private final DoubleRangeValidator buildingHeightValidatorWarning = new DoubleRangeValidator(OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM,
      OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM) {
    @Override
    protected String getMessage(final Double value) {
      return M.messages().ivDoubleRangeBetween(M.messages().sourceBuildingHeight(), OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM,
          OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM);
    }
  };
  private final IntegerRangeValidator buildingOrientationValidator =
      new IntegerRangeValidator((int) OPSLimits.SCOPE_BUILDING_ORIENTATION_MINIMUM, (int) OPSLimits.SCOPE_BUILDING_ORIENTATION_MAXIMUM) {
    @Override
    protected String getMessage(final Integer value) {
      return M.messages().ivRangeBetween(M.messages().sourceBuildingOrientation(), (int) OPSLimits.SCOPE_BUILDING_ORIENTATION_MINIMUM,
          (int) OPSLimits.SCOPE_BUILDING_ORIENTATION_MAXIMUM);
    }
  };

  private boolean otherWarning;
  private final EventBus eventBus;

  /**
   * The characteristics of OPS-sources may be set in this editor.
   * @param eventBus.
   * @param hpC the popup-controller.
   * @param opsFactsheetUrl string to documentation.
   */
  public OPSSourceBuildingEditor(final EventBus eventBus, final HelpPopupController hpC, final String opsFactsheetUrl) {
    this.eventBus = eventBus;

    initWidget(UI_BINDER.createAndBindUi(this));

    warningPanel.setVisible(false);
    warningLabel.setHTML(M.messages().sourceValuesOutSideRangeWarning(opsFactsheetUrl));
    buildingOrientationEditor.addValidator(buildingOrientationValidator);

    hpC.addWidget(buildingInfluenceCheckBox, hpC.tt().ttOPSBuildingInfluence());
    hpC.addWidget(buildingLengthEditor, hpC.tt().ttOPSBuildingLength());
    hpC.addWidget(buildingWidthEditor, hpC.tt().ttOPSBuildingWidth());
    hpC.addWidget(buildingHeightEditor, hpC.tt().ttOPSBuildingHeight());
    hpC.addWidget(buildingOrientationEditor, hpC.tt().ttOPSBuildingOrientation());

    warningLabel.ensureDebugId(TestID.INPUT_OPS_BUILDING_WARNING_LABEL);
    buildingInfluenceCheckBox.ensureDebugId(TestID.INPUT_OPS_BUILDING_INFLUENCE_CHECKBOX);
    buildingLengthEditor.ensureDebugId(TestID.INPUT_OPS_BUILDING_LENGTH);
    buildingHeightEditor.ensureDebugId(TestID.INPUT_OPS_BUILDING_HEIGHT);
    buildingWidthEditor.ensureDebugId(TestID.INPUT_OPS_BUILDING_WIDTH);
    buildingOrientationEditor.ensureDebugId(TestID.INPUT_OPS_BUILDING_ORIENTATION);
  }

  public Boolean isBuildingEnabled() {
    return buildingInfluenceCheckBox.getValue();
  }

  public void setBuildingInfluence(final boolean visible) {
    buildingInfluenceCheckBox.setValue(visible);
    buildingInfluenceDetailPanel.setVisible(visible);
  }

  @UiHandler("buildingInfluenceLabel")
  public void onBuildingInfluenceLabelClick(final ClickEvent e) {
    buildingInfluenceCheckBox.setValue(!buildingInfluenceCheckBox.getValue());
    onBuildingInfluenceToggle();
  }

  @UiHandler("buildingInfluenceCheckBox")
  void onToggleButtonClick(final ValueChangeEvent<Boolean> e) {
    onBuildingInfluenceToggle();
  }

  private void onBuildingInfluenceToggle() {
    buildingInfluenceDetailPanel.setVisible(buildingInfluenceCheckBox.getValue());
    validateBuildingAndOutflowInputForWarning();
    eventBus.fireEvent(new UseBuildingChangeEvent(buildingInfluenceCheckBox.getValue()));
  }

  @UiHandler({"buildingLengthEditor", "buildingWidthEditor", "buildingHeightEditor", "buildingOrientationEditor"})
  void onChangeEvent(final ChangeEvent e) {
    validateBuildingAndOutflowInputForWarning();
  }

  /**
   * keep track if values are within range if not highlight field with border and show special warning text.
   * @return
   *
   */
  private void validateBuildingAndOutflowInputForWarning() {
    // reset warning signal
    final List<String> warningDetailList = new ArrayList<String>();
    // validate the input on range if outside range highlight color and show warning
    if (buildingInfluenceCheckBox.getValue()) {

      buildingLengthEditor.setValue(buildingLengthEditor.getValue());
      buildingWidthEditor.setValue(buildingWidthEditor.getValue());
      buildingHeightEditor.setValue(buildingHeightEditor.getValue());
      buildingLengthWidget.setStyleName(R.css().suggestionWarningBox(),
          isOutsideRange(buildingLengthValidatorWarning, buildingLengthEditor.getValue(), warningDetailList));
      buildingWidthWidget.setStyleName(R.css().suggestionWarningBox(),
          isOutsideRange(buildingWidthLengthRatioValidatorWarning, buildingWidthEditor.getValue() / buildingLengthEditor.getValue(),
          warningDetailList));
      buildingHeightWidget.setStyleName(R.css().suggestionWarningBox(),
          isOutsideRange(buildingHeightValidatorWarning, buildingHeightEditor.getValue(), warningDetailList));
    } else {
      otherWarning = false;
    }
    warningPanel.setVisible(!warningDetailList.isEmpty() || otherWarning);
  }

  /**
   * enable warning when isbuilding and validation fails for x fields
   *
   * @param validator
   * @param value
   * @param warningDetail
   * @return
   */
  private boolean isOutsideRange(final DoubleRangeValidator validator, final double value, final List<String> warningDetail) {
    final String msg = validator.validate(value);
    final boolean outsideRange = msg != null;

    if (outsideRange) {
      warningDetail.add(msg);
    }
    return outsideRange;
  }

  public void setOtherWarning(final boolean otherWarning) {
    this.otherWarning = otherWarning;
    validateBuildingAndOutflowInputForWarning();
  }
}
