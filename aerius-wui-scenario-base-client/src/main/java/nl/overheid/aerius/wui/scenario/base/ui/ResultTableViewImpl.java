/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.scenario.base.ui.overview.ComparisonOverviewPanel;

/**
 * View implementation for the results Table.
 */
@Singleton
public class ResultTableViewImpl extends Composite implements ResultTableView {

  interface ResultTableViewImplUiBinder extends UiBinder<Widget, ResultTableViewImpl> {}

  @UiField SwitchPanel panel;
  @UiField(provided = true) ComparisonOverviewPanel compPanel;
  @UiField HTML messageText;

  private static final ResultTableViewImplUiBinder UI_BINDER = GWT.create(ResultTableViewImplUiBinder.class);

  @Inject
  public ResultTableViewImpl(final ComparisonOverviewPanel compPanel) {
    this.compPanel = compPanel;
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setValue(final EmissionResultKey emissionResultKey, final List<AeriusPoint> calculationPointList, final boolean isDiff) {
    compPanel.setValue(emissionResultKey, calculationPointList, isDiff);
    panel.showWidget(0);
  }

  @Override
  public void setValue(final EmissionResultKey emissionResultKey, final CalculationSummary summary, final int calcId1, final int calcId2,
      final String situationOneName, final String situationTwoName) {
    compPanel.setValue(emissionResultKey, summary, calcId1, calcId2, situationOneName, situationTwoName);
    panel.showWidget(0);
  }

  @Override
  public void setValue(final EmissionResultKey emissionResultKey, final CalculationSummary summary, final int calcId,
      final String situationName) {
    compPanel.setValue(emissionResultKey, summary, calcId, situationName);
    panel.showWidget(0);
  }

  @Override
  public void setNoValue(final boolean calculationRunning, final String message) {
    panel.showWidget(calculationRunning ? 2 : 1);
    if (!calculationRunning) {
      messageText.setText(message);
    }
  }

  @Override
  public String getTitleText() {
    return M.messages().calculatorMenuResults();
  }
}
