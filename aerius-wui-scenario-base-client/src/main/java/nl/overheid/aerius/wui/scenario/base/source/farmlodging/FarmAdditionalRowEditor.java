/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import java.util.ArrayList;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.LodgingStackType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.validation.NotZeroValidator;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox.NameTextMachine;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;

class FarmAdditionalRowEditor extends FarmRowEditor<FarmAdditionalLodgingSystemCategory, FarmAdditionalLodgingSystem> {

  private static final FarmAdditionalRowEditorUiBinder UI_BINDER = GWT.create(FarmAdditionalRowEditorUiBinder.class);

  interface FarmAdditionalRowEditorUiBinder extends UiBinder<Widget, FarmAdditionalRowEditor> {
  }

  @UiField(provided = true) SystemDefinitionEditor systemDefinitionEditor;
  @UiField IntValueBox amountEditor;

  private static final TextMachine<FarmAdditionalLodgingSystemCategory> ADDITIONAL_NAMER =
      new NameTextMachine<FarmAdditionalLodgingSystemCategory>() {
        @Override
        public String asError(final String input) {
          return M.messages().ivFarmNoValidRavCode(input);
        }
      };



  public FarmAdditionalRowEditor(final SectorCategories categories, final HelpPopupController hpC,
      final FarmLodgingCategorySelection categorySelection) {
    super(categories, hpC, new ValidateLodgingSystemHandler<FarmAdditionalLodgingSystemCategory>(categorySelection) {
      @Override
      boolean canStack(final FarmLodgingCategory lodgingCategory, final FarmAdditionalLodgingSystemCategory value) {
        return lodgingCategory.canStackAdditionalLodgingSystemCategory(value);
      }
    }, ADDITIONAL_NAMER);

    systemDefinitionEditor = new SystemDefinitionEditor();

    getRow().insert(UI_BINDER.createAndBindUi(this), 0);

    systemDefinitionEditor.addValidator(new Validator<FarmLodgingSystemDefinition>() {
      @Override
      public String validate(final FarmLodgingSystemDefinition value) {
        return value == null ? M.messages().ivFarmNoValidBWLCode() : null;
      }
    });

    amountEditor.addValidator(new NotZeroValidator<Integer>() {
      @Override
      protected String getMessage(final Integer value) {
        return M.messages().ivFarmNotEmptyAmount();
      }
    });

    hpC.addWidget(categoryEditor, hpC.tt().ttFarmLodgingAdditionalCategory());
    hpC.addWidget(systemDefinitionEditor, hpC.tt().ttFarmLodgingSystemDefinition());
    hpC.addWidget(amountEditor, hpC.tt().ttFarmLodgingAmount());

    StyleUtil.I.setPlaceHolder(categoryEditor.getValueBox(), M.messages().farmLodgingStackType(LodgingStackType.ADDITIONAL_MEASURE));
    StyleUtil.I.setPlaceHolder(amountEditor, M.messages().farmLodgingAmount());
  }

  @Override
  ArrayList<FarmAdditionalLodgingSystemCategory> getCorrectCategories(final SectorCategories categories) {
    return categories.getFarmLodgingCategories().getFarmAdditionalLodgingSystemCategories();
  }

  @Override
  void onCategoryValueChanged(final FarmAdditionalLodgingSystemCategory newCategory) {
    systemDefinitionEditor.updateSystemDefinitionList(newCategory, null);
  }

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    systemDefinitionEditor.ensureDebugId(baseID + "-" + TestID.BWL_SUGGEST_BOX);
    amountEditor.ensureDebugId(baseID + "-" + TestID.AMOUNT_BOX);
  }

  @Override
  protected boolean isEmpty() {
    return amountEditor.getValue() == 0 || super.isEmpty();
  }

  @Override
  protected void resetPlaceHolders() {
    super.resetPlaceHolders();
    amountEditor.resetPlaceHolder();
  }

  @Override
  public void setValue(final FarmAdditionalLodgingSystem value) {
    super.setValue(value);
    systemDefinitionEditor.updateSystemDefinitionList(value.getCategory(), value.getSystemDefinition());
  }

  /**
   * Update the amount value if the lastAmount is the same as the amount in this editor.
   * @param lastAmount previous amount
   * @param newAmount new amount
   */
  public void updateAmount(final Integer lastAmount, final Integer newAmount) {
    if (amountEditor.getValue() == null || amountEditor.getValue().equals(lastAmount)) {
      amountEditor.setValue(newAmount);
    }
  }

}
