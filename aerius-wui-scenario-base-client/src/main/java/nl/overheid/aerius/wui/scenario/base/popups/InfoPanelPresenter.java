/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.event.MapClickListener;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.info.InformationReceptorPoint;
import nl.overheid.aerius.shared.domain.info.ReceptorInfo;
import nl.overheid.aerius.shared.service.InfoServiceAsync;
import nl.overheid.aerius.wui.main.event.InformationPointChangeEvent;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationFinishEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInfoReceivedEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelViewImpl.Presenter;

/**
 * Presenter for showing deposition results of a single receptor in the info popup.
 */
@Singleton
public class InfoPanelPresenter implements Presenter {
  interface InfoPanelEventBinder extends EventBinder<InfoPanelPresenter> {}

  private final InfoPanelEventBinder eventBinder = GWT.create(InfoPanelEventBinder.class);

  private final InfoServiceAsync infoService;
  private final InfoPanelViewImpl view;
  private boolean running;
  private AeriusPoint lastReceptor;
  private AeriusPoint currentReceptor;

  private ScenarioBasePlace place;

  private final class EventHandlers extends AppAsyncCallback<ReceptorInfo> implements MapClickListener {
    @Override
    public void onSuccess(final ReceptorInfo result) {
      running = false;

      // If the user clicked while retrieving receptor info, go back to the server
      if (lastReceptor != null) {
        updateInfo(lastReceptor);
        lastReceptor = null;
        return;
      }

      updateCalculationInfo();
      view.updateAreaInfo(result, appContainer.getUserContext().getCalculatedScenario(), place);
      lastReceptor = null;
    }

    @Override
    public void onFailure(final Throwable result) {
      running = false;
      super.onFailure(result);
    }

    @Override
    public void onClick(final MapClickEvent e) {
      // Select the receptor
      final LonLat ll = e.getLonLat();
      selectReceptor(ll.lon(), ll.lat());
    }
  }

  private final ScenarioBaseAppContext<?, ?> appContainer;
  private final EventHandlers handlers = new EventHandlers();
  private final EventBus eventBus;

  @Inject
  public InfoPanelPresenter(final ScenarioBaseAppContext<?, ?> appContainer, final InfoPanelViewImpl view, final InfoServiceAsync infoService,
      final ScenarioBaseMapLayoutPanel map) {
    this.appContainer = appContainer;
    this.eventBus = appContainer.getEventBus();
    this.view = view;
    this.infoService = infoService;

    map.getDefaultInteractionLayer().addMapClickListener(handlers);
    view.setPresenter(this);

    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onCalculationFinish(final CalculationFinishEvent e) {
    updateInfo(currentReceptor);
  }

  @EventHandler
  void onYearChange(final YearChangeEvent e) {
    updateInfo(currentReceptor);
  }

  @EventHandler
  void onPlaceChange(final PlaceChangeEvent event) {
    if (event.getValue() instanceof ScenarioBasePlace) {
      place = (ScenarioBasePlace) event.getValue();
      updateCalculationInfo();
    } else {
      place = null;
    }
  }

  @EventHandler
  void onCalculationInfoReceived(final CalculationInfoReceivedEvent event) {
    updateCalculationInfo();
  }

  @EventHandler
  public void onStartCalculation(final CalculationStartEvent event) {
    view.clearEmissionResultInfo();
  }

  @Override
  public void selectReceptor(final double x, final double y) {
    // Create the receptor
    final InformationReceptorPoint receptor = new InformationReceptorPoint(x, y);
    //ensure the proper ID is used, and the infomarker uses the proper coordinates for that point.
    appContainer.getReceptorUtil().attachReceptorToGrid(receptor);
    //assure the info marker is put on the map.
    eventBus.fireEvent(new InformationPointChangeEvent(receptor));

    if (running) {
      lastReceptor = receptor;
    } else {
      // Notify the view new information is being retrieved
      view.dataPending(receptor);
      // Get the area info
      running = true;
      updateInfo(receptor);
    }
  }

  private void updateCalculationInfo() {
    final CalculatedScenario calculatedScenario = getCalculatedScenario();
    //get the right info from map, by calculation ID.
    final int calculationId = calculatedScenario == null || place == null || place.getSituation() == Situation.COMPARISON
        ? -1 : calculatedScenario.getCalculationId(place.getActiveSituationId());

    view.updateCalculationInfo(appContainer.getCalculationInfo(calculationId));
  }

  private void updateInfo(final AeriusPoint rp) {
    if (rp == null) {
      // Receptor can be null when no info marker has been selected (this happens on automatic refresh, such as when a calculation completes)
      return;
    }

    // Save for future automatic updates
    this.currentReceptor = rp;

    final CalculatedScenario calculatedScenario = getCalculatedScenario();

    final int calculatorIdOne = place == null || calculatedScenario == null ? 0 : calculatedScenario.getCalculationId(place.getSid1());
    final int calculatorIdTwo = place == null || calculatedScenario == null ? 0 : calculatedScenario.getCalculationId(place.getSid2());

    infoService.getAreaInfo(rp, calculatorIdOne, calculatorIdTwo, appContainer.getUserContext().getEmissionValueKey().getYear(), handlers);

  }

  private CalculatedScenario getCalculatedScenario() {
    return appContainer.getUserContext().getCalculatedScenario();
  }

  public InfoPanelViewImpl getView() {
    return view;
  }
}
