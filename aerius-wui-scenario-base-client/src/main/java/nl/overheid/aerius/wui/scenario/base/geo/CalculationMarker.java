/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;

import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.main.domain.MarkerUtil;

/**
 * Calculation marker capable of showing multiple markers on one location.
 */
@SuppressWarnings("serial")
public class CalculationMarker extends MarkerItem<DepositionMarker> {

  private DepositionMarkerType markerType;

  /**
   * Constructor.
   *
   * @param point The location of this marker.
   * @param type The marker type.
   */
  public CalculationMarker(final DepositionMarker point, final TYPE type) {
    super(point, type);
    getElement().getStyle().setPosition(Position.ABSOLUTE);
  }

  @Override
  public int hashCode() {
    return getId();
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() && getId() == ((CalculationMarker) obj).getId();
  }

  /**
   * Adds to this marker's combination. Server should already combine, but ensure markers are combined if needed.
   *
   * @param type the marker type.
   */
  public void addMarkerType(final DepositionMarkerType type) {
    markerType = markerType == null ? type : markerType.combine(type);
  }

  /**
   * Draws the actual markers with all markerTypes combined.
   */
  public void drawMarker() {
    getElement().setInnerHTML(null);
    final Image i = new Image(MarkerUtil.getMarkerImageResource(markerType));
    getElement().appendChild(i.getElement());
    getElement().getStyle().setPosition(Position.ABSOLUTE);
    getElement().getStyle().setTop(-i.getHeight(), Unit.PX);
    getElement().getStyle().setLeft(-i.getWidth() / 2.0, Unit.PX);
  }
}
