/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyValue;
import nl.overheid.aerius.wui.main.widget.table.TextKeyValueDivTable;

public class OPSSourceCharacteristicsViewer extends Composite implements Editor<OPSSourceCharacteristics> {
  public static final OPSSourceCharacteristicsViewerUiBinder uiBinder = GWT.create(OPSSourceCharacteristicsViewerUiBinder.class);

  @UiTemplate("KeyValueTableViewer.ui.xml")
  interface OPSSourceCharacteristicsViewerUiBinder extends UiBinder<Widget, OPSSourceCharacteristicsViewer> {
  }

  @UiField TextKeyValueDivTable table;
  @UiField Label header;

  private boolean buildingHeightVisible;
  private boolean spreadVisible;
  private boolean diurnalVariationVisible;
  private boolean particleSizeDistributionVisible;

  @Inject
  public OPSSourceCharacteristicsViewer() {
    initWidget(uiBinder.createAndBindUi(this));

    table.ensureDebugId(TestID.TABLE_SOURCES_CHARACTERISTICS_VIEWER);
    header.setText(M.messages().emissionSourceViewerCharacteristicsTitle());
  }

  /**
   * Sets the characteristics in the widgets. This method should only be used for initializing the sub widgets with new values. Normal usage should
   * via a the Editor technique, where setting is done through a driver. It's not clear if this method is necessary at all, but it's unknown how to do
   * it differently, when using this Editor as a sub Editor.
   *
   * @param characteristics characteristics to set
   */
  public void setValue(final OPSSourceCharacteristics characteristics) {
    table.clear();

    // Add the building influence
    if (characteristics.isBuildingInfluence()) {
      final String usedBuildingInfluence = M.messages().unitBuildingDimensions(
          FormatUtil.toFixed(determineMinMax(characteristics.getBuildingLength(), OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM,
              OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION),
          FormatUtil.toFixed(determineMinMax(characteristics.getBuildingWidth(),
              OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM, OPSLimits.SCOPE_BUILDING_WIDTH_MAXIMUM),
              OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION),
          FormatUtil.toFixed(determineMinMax(characteristics.getBuildingHeight(),
              OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM, OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION),
          FormatUtil.toFixed(characteristics.getBuildingOrientation(), 0));

      final String inputBuildingInfluence = M.messages().unitBuildingDimensions(
          FormatUtil.toFixed(characteristics.getBuildingLength(), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION),
          FormatUtil.toFixed(characteristics.getBuildingWidth(), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION),
          FormatUtil.toFixed(characteristics.getBuildingHeight(), OPSLimits.SOURCE_BUILDING_DIGITS_PRECISION),
          FormatUtil.toFixed(characteristics.getBuildingOrientation(), 0));
      if (inputBuildingInfluence.equals(usedBuildingInfluence)) {
        addValue(M.messages().sourceBuildingDimensionLabel(), inputBuildingInfluence);
      } else {
        addValue(M.messages().sourceBuildingDimensionLabel(), M.messages().sourceOPSUsedValue(inputBuildingInfluence, usedBuildingInfluence));
      }
    }

    // Add the source height
    final Double inputEmissionHeight = characteristics.getEmissionHeight();
    final Double usedEmissionHeight = determineMinMax(inputEmissionHeight,
        OPSLimits.SCOPE_OUTFLOW_HEIGHT_MINIMUM, OPSLimits.SCOPE_OUTFLOW_HEIGHT_MAXIMUM);
    if (characteristics.isBuildingInfluence() && inputEmissionHeight.equals(usedEmissionHeight)) {
      addValue(M.messages().sourceHeight(), M.messages().unitM(FormatUtil.toFixed(inputEmissionHeight, 1)));
    } else {
      addValue(M.messages().sourceHeight(), M.messages().sourceOPSUsedValue(
          M.messages().unitM(FormatUtil.toFixed(inputEmissionHeight, 1)),
          M.messages().unitM(FormatUtil.toFixed(usedEmissionHeight, 1))));
    }

    // Add the spread
    if (spreadVisible) {
      addValue(M.messages().sourceSpread(), M.messages().unitM(FormatUtil.toFixed(characteristics.getSpread(), 0)));
    }

    if (characteristics.isUserdefinedHeatContent()) {
      // Add the heat content
      addValue(M.messages().sourceHeatContent(), M.messages().unitMW(FormatUtil.toFixed(characteristics.getHeatContent(),
          OPSLimits.SOURCE_HEAT_CONTENT_DIGITS_PRECISION)));
    } else {
      // Add Emission Temperature
      addValue(M.messages().sourceTemperature(), M.messages().unitC(FormatUtil.toFixed(characteristics.getEmissionTemperature(), 2)));

      // Add exitSurface
      final Double inputOutflowDiameter = characteristics.getOutflowDiameter();
      final Double usedOutflowDiameter = determineMinMax(inputOutflowDiameter,
          OPSLimits.SCOPE_OUTFLOW_DIAMETER_MINIMUM, OPSLimits.SCOPE_OUTFLOW_DIAMETER_MAXIMUM);
      if (inputOutflowDiameter.equals(usedOutflowDiameter)) {
        addValue(M.messages().sourceOutflowDiameter(), M.messages().unitM(FormatUtil.toFixed(inputOutflowDiameter,
            OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION)));
      } else {
        addValue(M.messages().sourceOutflowDiameter(), M.messages().sourceOPSUsedValue(
                M.messages().unitM(FormatUtil.toFixed(inputOutflowDiameter, OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION)),
                M.messages().unitM(FormatUtil.toFixed(usedOutflowDiameter, OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION))));
      }

      // Add exitDirection
      addValue(M.messages().sourceOutflowDirection(), M.messages().sourceOutflowDirectionType(characteristics.getOutflowDirection()));

      // Add exitVelocity
      final Double inputOutflowVelocity = characteristics.getOutflowVelocity();
      final Double usedOutflowVelocity = determineMinMax(inputOutflowVelocity,
          OPSLimits.SCOPE_OUTFLOW_VELOCITY_MINIMUM, OPSLimits.SCOPE_OUTFLOW_VELOCITY_MAXIMUM);
      if (inputOutflowVelocity.equals(usedOutflowVelocity)) {
        addValue(M.messages().sourceOutflowVelocity(), M.messages().unitM(FormatUtil.toFixed(inputOutflowVelocity,
            OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION)));
      } else {
        addValue(M.messages().sourceOutflowVelocity(), M.messages().sourceOPSUsedValue(
            M.messages().unitM(FormatUtil.toFixed(inputOutflowVelocity, OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION)),
            M.messages().unitM(FormatUtil.toFixed(usedOutflowVelocity, OPSLimits.SOURCE_OUTFLOW_VELOCITY_DIGITS_PRECISION))));
      }
    }

    // Add the diurnal variation
    if (diurnalVariationVisible) {
      final DiurnalVariationSpecification spec = characteristics.getDiurnalVariationSpecification();
      addValue(M.messages().sourceDiurnalVariation(), spec == null ? "" : spec.getName());
    }

    // Add the particle size distribution
    if (particleSizeDistributionVisible) {
      addValue(M.messages().sourceParticleSizeDistribution(), String.valueOf(characteristics.getParticleSizeDistribution()));
    }
  }

  private double determineMinMax(final double value, final double min, final double max) {
    return Math.max(min, Math.min(max, value));
  }

  private void addValue(final String key, final String value) {
    table.addRowData(new DivTableKeyValue<>(key, value));
  }

  public void setBuildingHeightVisible(final boolean buildingHeightVisible) {
    this.buildingHeightVisible = buildingHeightVisible;
  }

  public void setSpreadVisible(final boolean spreadVisible) {
    this.spreadVisible = spreadVisible;
  }

  public void setDiurnalVariationVisible(final boolean diurnalVariationVisible) {
    this.diurnalVariationVisible = diurnalVariationVisible;
  }

  public void setParticleSizeDistributionVisible(final boolean particleSizeDistributionVisible) {
    this.particleSizeDistributionVisible = particleSizeDistributionVisible;
  }
}
