/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.progress;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.wui.main.event.EmissionResultDisplaySettingChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.util.scaling.ScalingUtil;
import nl.overheid.aerius.wui.scenario.base.events.ResultHighValuesByDistancesEvent;

public class ProgressPanel extends Composite {
  interface ResultGraphicsEventBinder extends EventBinder<ProgressPanel> {}

  private final ResultGraphicsEventBinder eventBinder = GWT.create(ResultGraphicsEventBinder.class);

  private static final double MAXIMUM_DEPOSITION_HEIGHT_MARGIN = 1.3;

  public static final double GRAPH_WIDTH = 260;
  public static final double GRAPH_HEIGHT = 150;

  private static final int BARS_DEFAULT_NUM = 22;
  private static final int AUTO_EXPAND_NUM = 2;

  interface ProgressPanelUiBinder extends UiBinder<Widget, ProgressPanel> {}

  private static ProgressPanelUiBinder uiBinder = GWT.create(ProgressPanelUiBinder.class);

  @UiField DivElement barContainer;
  @UiField HorizontalLabels horizontalLabels;
  @UiField(provided = true) VerticalLabels verticalLabels;

  private final ArrayList<ProgressPanelBar> bars = new ArrayList<>();

  private int maxDistanceThreshold;

  private final ScenarioBaseUserContext userContext;
  private final ScalingUtil scaler;
  private final long calculationId;
  private final HandlerRegistration hr;

  private int lastDistance;

  public ProgressPanel(final ScenarioBaseUserContext userContext, final EventBus eventBus, final ScalingUtil scaler, final long calculationId) {
    this.userContext = userContext;
    this.scaler = scaler;
    this.calculationId = calculationId;

    verticalLabels = new VerticalLabels(scaler, userContext);

    initWidget(uiBinder.createAndBindUi(this));

    addBars(BARS_DEFAULT_NUM);
    hr = eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onProgressChanged(final ResultHighValuesByDistancesEvent event) {
    final ResultHighValuesByDistances value = event.getValue();
    for (final Entry<Integer, HashMap<Integer, EmissionResults>> entry : value.getResults().entrySet()) {
      final Integer resultsCalculationId = entry.getKey();
      if (resultsCalculationId == calculationId) {
        addProgressInformation(entry.getValue(), value.getHighestDistance(resultsCalculationId));
      } // else ignore; not for this instance
    }
  }

  @EventHandler
  void onEmissionResultDisplaySettingChanged(final EmissionResultDisplaySettingChangeEvent event) {
    updateEmissionResultDisplaySetting(event.getValue());
  }

  void updateEmissionResultDisplaySetting(final EmissionResultValueDisplaySettings erk) {
    for (final ProgressPanelBar bar : bars) {
      bar.setEmissionResultValueDisplaySettings(erk);
    }

    verticalLabels.updateLabels();
  }

  @EventHandler
  void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent event) {
    updateEmisionResultType(event.getValue());
  }

  void updateEmisionResultType(final EmissionResultKey erk) {
    for (final ProgressPanelBar bar : bars) {
      bar.setEmissionResultKey(erk);
    }
    verticalLabels.setEmissionResultType(erk == null ? null : erk.getEmissionResultType());
    updateBarHeight();
  }

  /**
   * Sets the maximum distance in meters.
   *
   * @param distance Maximum distance in meters.
   */
  public void setMaxDistance(final int distance) {
    if (maxDistanceThreshold != 0) {
      bars.get(maxDistanceThreshold).setMaxDistanceThreshold(false);
    }

    if (distance > (bars.size() - AUTO_EXPAND_NUM)) {
      addBars((distance - bars.size()) + AUTO_EXPAND_NUM);
    }

    maxDistanceThreshold = distance;
    bars.get(distance).setMaxDistanceThreshold(distance != 0);

    updateBarWidth();
    updateBarHeight();
  }

  public void clear() {
    hr.removeHandler();
  }

  private void addProgressInformation(final HashMap<Integer, EmissionResults> resultsMap, final int highestDistance) {
    if (lastDistance < highestDistance) {
      lastDistance = highestDistance;
    }
    if (highestDistance > (bars.size() - AUTO_EXPAND_NUM)) {
      addBars((highestDistance - bars.size()) + AUTO_EXPAND_NUM);
    }
    for (final Entry<Integer, EmissionResults> entry : resultsMap.entrySet()) {
      // The Math.max is used to make sure a distance of 0 is also shown (as part of distance = 1) and doesn't trigger an UI crash.
      // A distance of 0 doesn't occur often though as the source has the be dead centre of a receptor.
      final ProgressPanelBar bar = bars.get(Math.max(0, entry.getKey() - 1));
      for (final Entry<EmissionResultKey, Double> resultEntry : entry.getValue().entrySet()) {
        bar.setMaxEmissionResult(resultEntry.getKey(), resultEntry.getValue());
      }
    }
    updateBarHeight();
  }

  private void addBars(final int num) {
    for (int i = 0; i < num; i++) {
      final ProgressPanelBar progressPanelBar = new ProgressPanelBar(userContext.getEmissionResultKey(),
          userContext.getEmissionResultValueDisplaySettings());

      if (bars.isEmpty()) {
        progressPanelBar.notifyFirst(true);
      }

      bars.add(progressPanelBar);
      barContainer.appendChild(progressPanelBar.getElement());
    }

    updateBarWidth();
  }

  public void updateBarHeight() {
    double overallMaxEmissionResult = 1;

    for (final ProgressPanelBar bar : bars) {
      if (overallMaxEmissionResult < (bar.getEmissionResult() * MAXIMUM_DEPOSITION_HEIGHT_MARGIN)) {
        overallMaxEmissionResult = bar.getEmissionResult() * MAXIMUM_DEPOSITION_HEIGHT_MARGIN;
      }
    }

    for (final ProgressPanelBar bar : bars) {
      bar.setBarHeight(GRAPH_HEIGHT * scaler.scale(bar.getEmissionResult(), overallMaxEmissionResult));
    }

    verticalLabels.updateLabels(overallMaxEmissionResult);
  }

  private void updateBarWidth() {
    // Account for borders
    final double barWidth = (GRAPH_WIDTH - 2d) / bars.size();

    for (final ProgressPanelBar bar : bars) {
      bar.setWidth(barWidth);
    }

    horizontalLabels.setNumLabels(bars.size(), barWidth, bars.size());
  }

  public int getLastDistance() {
    return lastDistance;
  }

  public double getLastEmissionResult(final EmissionValueKey emissionValueKey) {
    return lastDistance == 0 ? 0 : bars.get(lastDistance - 1).getEmissionResult();
  }

  public void finish() {
    for (int i = lastDistance; i < bars.size(); i++) {
      bars.get(i).setActive(false);
    }

    // Floor the width of the last bar. Bug in Firefox causes it to snap to the next line due to improper rounding.
    final ProgressPanelBar lastBar = bars.get(bars.size() - 1);
    lastBar.setWidth(Math.floor(lastBar.getOffsetWidth()) - 1);
  }
}
