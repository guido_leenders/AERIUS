/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Row for {@link FarmReductiveLodgingSystem} for emission of a reductive measurements.
 */
class FarmReductiveRowViewer extends FarmEffectRowViewer<FarmReductiveLodgingSystem> {

  private static final FarmReductiveRowViewerUiBinder UI_BINDER = GWT.create(FarmReductiveRowViewerUiBinder.class);

  @UiTemplate("FarmEffectRowViewer.ui.xml")
  interface FarmReductiveRowViewerUiBinder extends UiBinder<Widget, FarmReductiveRowViewer> {
  }

  public FarmReductiveRowViewer(final FarmLodgingStandardEmissions source) {
    super(source);

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  protected void setValueTexts(final FarmReductiveLodgingSystem value) {
    headerValue.setText(value.getCategory().getName());
    amountValue.setText(M.messages().farmNoValue());
    factorValue.setText(M.messages().farmNoValue());

    final double reductionPct = value.getCategory().getReductionFactor() * SharedConstants.PERCENTAGE_TO_FRACTION;
    effectValue.setText(M.messages().unitPercentage(FormatUtil.toWhole(reductionPct)));

    // emission up to this row
    final double emission = source.getEmissionUpToReductive(EMISSION_VALUE_KEY, value);
    emissionValue.setText(FormatUtil.formatEmissionWithUnit(emission));
  }

  @Override
  protected boolean isLast(final FarmReductiveLodgingSystem value) {
    return source.getFodderMeasures().isEmpty()
        && source.getLodgingReductive().indexOf(value) == source.getLodgingReductive().size() - 1;
  }

}
