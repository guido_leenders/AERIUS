/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.wui.main.event.EmissionResultDisplaySettingChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.HabitatClearEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;

public class HabitatTypePanel extends Composite implements IsCollapsiblePanel {

  interface HabitatTypePaneEventBinder extends EventBinder<HabitatTypePanel> {}

  private static final HabitatTypePaneEventBinder EVENT_BINDER = GWT.create(HabitatTypePaneEventBinder.class);

  interface HabitatTypePanelUiBinder extends UiBinder<Widget, HabitatTypePanel> {}

  private static final HabitatTypePanelUiBinder UI_BINDER = GWT.create(HabitatTypePanelUiBinder.class);

  @UiField CollapsiblePanel typeInfo;
  @UiField(provided = true) HabitatTypeCalculatorTable typeTable;
  @UiField Label typesTextLabel;
  private final EventBus eventBus;
  private ArrayList<HabitatInfo> habitatTypes;

  public HabitatTypePanel(final EventBus eventBus, final EmissionResultKey emissionResultKey,
      final EmissionResultValueDisplaySettings displaySettings, final HexagonZoomLevel zoomLevel1) {
    this.eventBus = eventBus;

    typeTable = new HabitatTypeCalculatorTable(eventBus, emissionResultKey, displaySettings);

    EVENT_BINDER.bindEventHandlers(this, eventBus);

    initWidget(UI_BINDER.createAndBindUi(this));
    typesTextLabel.setText(M.messages().infoPanelHabitatTypesText((int) Math.round(zoomLevel1.getSurfaceLevel1() / SharedConstants.M2_TO_HA)));
  }

  public void setTypeInfo(final int receptorId, final ArrayList<HabitatInfo> habitatTypes) {
    this.habitatTypes = habitatTypes;

    eventBus.fireEvent(new HabitatClearEvent());

    final boolean hasResults = habitatTypes != null;

    typeInfo.setVisible(hasResults);
    if (!hasResults) {
      return;
    }

    typeTable.setReceptor(receptorId);
    updateTableData();
  }

  private void updateTableData() {
    typeTable.asDataTable().setRowData(habitatTypes);
  }

  @EventHandler
  void onEmissionResultDisplaySettingChange(final EmissionResultDisplaySettingChangeEvent e) {
    typeTable.setDisplaySettings(e.getValue());
    updateTableData();
  }

  @EventHandler
  void onSubstanceChange(final EmissionResultKeyChangeEvent e) {
    typeTable.setEmissionResultKey(e.getValue());
    updateTableData();
  }

  @Override
  public CollapsiblePanel asCollapsible() {
    return typeInfo;
  }
}
