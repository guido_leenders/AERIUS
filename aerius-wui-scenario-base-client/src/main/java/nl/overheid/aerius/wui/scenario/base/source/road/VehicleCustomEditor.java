/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.road;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.core.EmissionRowWidget;

class VehicleCustomEditor extends AbstractVehicleEditor<VehicleCustomEmissions> {

  interface VehicleCustomDriver extends SimpleBeanEditorDriver<VehicleCustomEmissions, VehicleCustomEditor> {
  }

  interface VehicleCustomEditorUiBinder extends UiBinder<Widget, VehicleCustomEditor> {
  }

  private static final VehicleCustomEditorUiBinder UI_BINDER = GWT.create(VehicleCustomEditorUiBinder.class);

  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);
  private static final EmissionValueKey KEY_NO2 = new EmissionValueKey(Substance.NO2);
  private static final EmissionValueKey KEY_NH3 = new EmissionValueKey(Substance.NH3);
  private static final EmissionValueKey KEY_PM10 = new EmissionValueKey(Substance.PM10);

  @UiField @Ignore TextBox descriptionTextBox;
  final ValidatedValueBoxEditor<String> descriptionEditor;

  @UiField(provided = true) @Ignore EmissionRowWidget emissionNOx;
  @UiField(provided = true) @Ignore EmissionRowWidget emissionNO2;
  @UiField(provided = true) @Ignore EmissionRowWidget emissionNH3;
  @UiField(provided = true) @Ignore EmissionRowWidget emissionPM10;

  LeafValueEditor<EmissionValues> emissionValuesEditor = new LeafValueEditor<EmissionValues>() {
    private EmissionValues emissionValues;

    @Override
    public EmissionValues getValue() {
      if (emissionValues == null) {
        emissionValues = new EmissionValues(false);
      }
      emissionValues.setEmission(Substance.NOX, emissionNOx.getValue());
      emissionValues.setEmission(Substance.NO2, emissionNO2.getValue());
      emissionValues.setEmission(Substance.NH3, emissionNH3.getValue());
      emissionValues.setEmission(Substance.PM10, emissionPM10.getValue());
      return emissionValues;
    }

    @Override
    public void setValue(final EmissionValues value) {
      this.emissionValues = value;
      emissionNOx.setValue(emissionValues == null ? 0 : emissionValues.getEmission(KEY_NOX));
      emissionNO2.setValue(emissionValues == null ? 0 : emissionValues.getEmission(KEY_NO2));
      emissionNH3.setValue(emissionValues == null ? 0 : emissionValues.getEmission(KEY_NH3));
      emissionPM10.setValue(emissionValues == null ? 0 : emissionValues.getEmission(KEY_PM10));
    }
  };

  public VehicleCustomEditor(final HelpPopupController hpC) {
    final String unit = M.messages().roadCustomEmissionUnit();
    emissionNOx = new EmissionRowWidget(Substance.NOX, unit, hpC, hpC.tt().ttRoadCustomEmission(), TestID.INPUT_EMISSION_VEHICLE_NOX);
    emissionNO2 = new EmissionRowWidget(Substance.NO2, unit, hpC, hpC.tt().ttRoadCustomEmission(), TestID.INPUT_EMISSION_VEHICLE_NO2);
    emissionNH3 = new EmissionRowWidget(Substance.NH3, unit, hpC, hpC.tt().ttRoadCustomEmission(), TestID.INPUT_EMISSION_VEHICLE_NH3);
    emissionPM10 = new EmissionRowWidget(Substance.PM10, unit, hpC, hpC.tt().ttRoadCustomEmission(), TestID.INPUT_EMISSION_VEHICLE_PM10);
    initWidget(UI_BINDER.createAndBindUi(this));

    afterBinding(hpC);

    descriptionEditor = new ValidatedValueBoxEditor<String>(descriptionTextBox, M.messages().roadCustomDescription()) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().ivRoadCustomDescriptionNotEmpty();
      }
    };
    descriptionEditor.addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().ivRoadCustomDescriptionNotEmpty();
      }
    });

    // Help widgets
    hpC.addWidget(descriptionTextBox, hpC.tt().ttRoadCustomDescription());
    emissionPM10.setVisible(false); // NO PM10
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    descriptionTextBox.ensureDebugId(TestID.INPUT_VEHICLE_CUSTOM_DESCRIPTION);
  }
}
