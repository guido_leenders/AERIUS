/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.TextDecoration;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.source.FarmFodderMeasure;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;

/**
 * Viewer for emissions of multiple farm lodgings at a single location.
 */
public class FarmLodgingStandardEmissionViewer extends EmissionSubViewer<FarmLodgingStandardEmissions> {

  private static final FarmLodgingStandardEmissionsViewerUiBinder UI_BINDER = GWT.create(FarmLodgingStandardEmissionsViewerUiBinder.class);

  public interface FarmLodgingStandardEmissionDriver extends SimpleBeanEditorDriver<FarmLodgingStandardEmissions, FarmLodgingStandardEmissionViewer> {
  }

  interface FarmLodgingStandardEmissionsViewerUiBinder extends UiBinder<Widget, FarmLodgingStandardEmissionViewer> {
  }

  private static final EmissionValueKey EMISSION_VALUE_KEY = new EmissionValueKey(Substance.NH3);

  @UiField(provided = true) SimpleDivTable<FarmAdditionalLodgingSystem> tableAdditional;
  @UiField(provided = true) SimpleWidgetFactory<FarmAdditionalLodgingSystem> columnAdditional;
  @UiField(provided = true) SimpleDivTable<FarmReductiveLodgingSystem> tableReductive;
  @UiField(provided = true) SimpleWidgetFactory<FarmReductiveLodgingSystem> columnReductive;
  @UiField(provided = true) SimpleDivTable<FarmFodderMeasure> tableFodderMeasures;
  @UiField(provided = true) SimpleWidgetFactory<FarmFodderMeasure> columnFodderMeasures;

  @UiField Label headerValue;
  @UiField Label amountValue;
  @UiField Label factorValue;
  @UiField Label factorValueConstrained;
  @UiField Label emissionValue;
  @UiField Label emissionValueConstrained;
  @UiField Label detailCodeValue;
  @UiField Label detailValue;
  @UiField Label footNoteRef;
  @UiField Label footNote;
  @UiField FlowPanel constrainedValuesRow;

  private FarmLodgingStandardEmissions source;

  public FarmLodgingStandardEmissionViewer() {
    tableAdditional = new SimpleDivTable<FarmAdditionalLodgingSystem>();
    columnAdditional = new SimpleWidgetFactory<FarmAdditionalLodgingSystem>() {
      @Override
      public Widget createWidget(final FarmAdditionalLodgingSystem object) {
        final FarmAdditionalRowViewer row = new FarmAdditionalRowViewer(source);
        row.setValue(object);
        return row;
      }
    };
    tableReductive = new SimpleDivTable<FarmReductiveLodgingSystem>();
    columnReductive = new SimpleWidgetFactory<FarmReductiveLodgingSystem>() {
      @Override
      public Widget createWidget(final FarmReductiveLodgingSystem object) {
        final FarmReductiveRowViewer row = new FarmReductiveRowViewer(source);
        row.setValue(object);
        return row;
      }
    };
    tableFodderMeasures = new SimpleDivTable<FarmFodderMeasure>();
    columnFodderMeasures = new SimpleWidgetFactory<FarmFodderMeasure>() {
      @Override
      public Widget createWidget(final FarmFodderMeasure object) {
        final FarmFodderMeasureRowViewer row = new FarmFodderMeasureRowViewer(source);
        row.setValue(object);
        return row;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  public void setValue(final FarmLodgingStandardEmissions source) {
    this.source = source;

    headerValue.setText(source.getCategory().getName());
    amountValue.setText(String.valueOf(source.getAmount()));
    detailCodeValue.setText(source.getCategory().getName());
    detailValue.setText(source.getCategory().getDescription());

    final boolean constrained = source.isEmissionFactorConstrained();

    if (constrained) {
      factorValue.setText(FormatUtil.toFixed(source.getUnconstrainedEmissionFactor(EMISSION_VALUE_KEY), 3));
      factorValueConstrained.setText(FormatUtil.toFixed(source.getEmissionFactor(EMISSION_VALUE_KEY), 3));
      emissionValue.setText(FormatUtil.formatEmissionWithUnit(source.getUnconstrainedFlatEmission(EMISSION_VALUE_KEY)));
      emissionValueConstrained.setText(FormatUtil.formatEmissionWithUnit(source.getFlatEmission(EMISSION_VALUE_KEY)));

      factorValue.getElement().getStyle().setTextDecoration(TextDecoration.LINE_THROUGH);
      emissionValue.getElement().getStyle().setTextDecoration(TextDecoration.LINE_THROUGH);
    } else {
      factorValue.setText(FormatUtil.toFixed(source.getEmissionFactor(EMISSION_VALUE_KEY), 3));
      emissionValue.setText(FormatUtil.formatEmissionWithUnit(source.getFlatEmission(EMISSION_VALUE_KEY)));

      factorValue.getElement().getStyle().setTextDecoration(TextDecoration.NONE);
      emissionValue.getElement().getStyle().setTextDecoration(source.hasStacking() ? TextDecoration.LINE_THROUGH : TextDecoration.NONE);
    }

    constrainedValuesRow.setVisible(constrained);
    footNote.setVisible(constrained);
    footNoteRef.setVisible(constrained);

    tableAdditional.setRowData(source.getLodgingAdditional(), true);
    tableReductive.setRowData(source.getLodgingReductive(), true);
    tableFodderMeasures.setRowData(source.getFodderMeasures(), true);
  }
}
