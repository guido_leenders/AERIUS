/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.HasTitle;
import nl.overheid.aerius.wui.main.place.Situation;

/**
 * View for Situation tabs.
 */
@Singleton
public class SituationViewImpl extends Composite implements SituationView {
  private static SituationViewImplUiBinder uiBinder = GWT.create(SituationViewImplUiBinder.class);

  interface SituationViewImplUiBinder extends UiBinder<Widget, SituationViewImpl> {}

  @UiField(provided = true) SituationTabPanel tabHeader;
  @UiField SimplePanel buttonPanel;
  @UiField SimplePanel contentPanel;

  @Inject
  public SituationViewImpl(final SituationTabPanel tabHeader) {
    this.tabHeader = tabHeader;

    initWidget(uiBinder.createAndBindUi(this));
  }

  /**
   * Set the name for a specific tab. If tab isn't present yet, it will be created by calling this.
   *
   * @param situation The situation to set the name for
   * @param id id of the situation
   * @param name The name for the tab to use
   */
  @Override
  public void setTabName(final Situation situation, final int id, final String name) {
    tabHeader.setSituationName(situation, id, name);
  }

  @Override
  public void setFocus(final Situation situation) {
    tabHeader.setFocus(situation);
  }

  @Override
  public AcceptsOneWidget getContentPanel() {
    return contentPanel;
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    tabHeader.setHandler(presenter);
    buttonPanel.setWidget(presenter.getCalculationButtons());
    setEditable(true);
  }

  @Override
  public void setEditable(final boolean enable) {
    tabHeader.setEditable(enable);
  }

  @Override
  public String getTitleText() {
    return contentPanel.getWidget() instanceof HasTitle ? ((HasTitle) contentPanel.getWidget()).getTitleText() : "";
  }
}
