/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.habitat;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.info.HabitatInfo.HabitatSurfaceType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.LabelledValue;

class HabitatInfoItem extends Composite implements Editor<HabitatInfo> {

  private static final HabitatInfoItemBinder UI_BINDER = GWT.create(HabitatInfoItemBinder.class);

  interface HabitatInfoItemBinder extends UiBinder<Widget, HabitatInfoItem> {}

  @UiField LabelledValue ecologyQuality;
  @UiField(provided = true) SurfaceTable surfaceTable;
  @UiField LabelledValue relevantMappedSurface;
  @UiField LabelledValue relevantCartographicSurface;
  @UiField LabelledValue extentGoal;
  @UiField LabelledValue qualityGoal;
  @UiField LabelledValue criticalLevel;

  public HabitatInfoItem(final HabitatInfo info, final HelpPopupController hpC, final boolean relevantSurfacesOnly,
      final EmissionResultKey emissionResultKey, final EmissionResultValueDisplaySettings displaySettings) {
    this.surfaceTable = new SurfaceTable(hpC);
    initWidget(UI_BINDER.createAndBindUi(this));

    if (relevantSurfacesOnly) {
      surfaceTable.removeFromParent();
      relevantMappedSurface.setValue(FormatUtil.formatSurfaceWithUnit(info.getAdditionalSurface(HabitatSurfaceType.RELEVANT_MAPPED)));
      relevantCartographicSurface.setValue(FormatUtil.formatSurfaceWithUnit(info.getAdditionalSurface(HabitatSurfaceType.RELEVANT_CARTOGRAPHIC)));
    } else {
      relevantMappedSurface.removeFromParent();
      relevantCartographicSurface.removeFromParent();
      surfaceTable.setSurfaces(info);
    }

    ecologyQuality.setValue(M.messages().infoPanelAreaInfoLabelCategoryValue(info.getEcologyQuality()));
    extentGoal.setValue(M.messages().habitatsTableHabitatGoal(info.getExtentGoal()));
    qualityGoal.setValue(M.messages().habitatsTableHabitatGoal(info.getQualityGoal()));
    criticalLevel.setValue(FormatUtil.formatEmissionResult(emissionResultKey, info.getCriticalLevels(), displaySettings));

    if (info.getEcologyQuality() == null) {
      hpC.addWidget(ecologyQuality, hpC.tt().ttEcologicalQualityNone());
    } else {
      switch (info.getEcologyQuality()) {
      case ONE_A:
        hpC.addWidget(ecologyQuality, hpC.tt().ttEcologicalQuality1a());
        break;
      case ONE_B:
        hpC.addWidget(ecologyQuality, hpC.tt().ttEcologicalQuality1b());
        break;
      case TWO:
        hpC.addWidget(ecologyQuality, hpC.tt().ttEcologicalQuality2());
        break;
      default:
        hpC.addWidget(ecologyQuality, hpC.tt().ttEcologicalQualityNone());
        break;
      }
    }

    ecologyQuality.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_ECOLOGICAL_QUALITY);
    surfaceTable.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_SURFACE);
    extentGoal.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_EXTENT);
    qualityGoal.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_QUALITY_GOAL);
    criticalLevel.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_CRITICAL_DEPOSITION);
  }
}
