/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

/**
 * Wraps the inland waterway's in a collapsible panel. Used for both route source as inland mooring source
 * with multiple routes.
 */
class InlandWaterwayTypePanel extends Composite {

  private static final InlandWaterwayTypePanelUiBinder UI_BINDER = GWT.create(InlandWaterwayTypePanelUiBinder.class);

  interface InlandWaterwayTypePanelUiBinder extends UiBinder<Widget, InlandWaterwayTypePanel> {
  }

  interface CustomStyle extends CssResource {
    String headerColumn();
    String headerColumnHalf();
  }

  @UiField Label labelId;
  @UiField Label labelWaterway;
  @UiField Label labelWaterwayDirection;
  @UiField FlowPanel contentPanel;

  @UiField CustomStyle style;

  public InlandWaterwayTypePanel(final boolean showLabel, final String directionLabel) {
    initWidget(UI_BINDER.createAndBindUi(this));
    setLabels(showLabel);
    labelWaterwayDirection.setText(directionLabel);
    setDirectionLabelVisible(false);
  }

  private void setLabels(final boolean showLabel) {
    labelId.setVisible(showLabel);
    if (showLabel) {
      labelWaterway.addStyleName(R.css().sourceDetailCol1Var());
      labelWaterway.addStyleName(style.headerColumn());
    } else {
      labelWaterway.addStyleName(R.css().sourceDetailCol1());
    }
  }

  public void setDirectionLabelVisible(final boolean visible) {
    labelWaterwayDirection.setVisible(visible);
    labelWaterway.setStyleName(style.headerColumnHalf(), visible);
    labelWaterway.setStyleName(style.headerColumn(), !visible);
  }

  public FlowPanel getContentPanel() {
    return contentPanel;
  }
}
