/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source;

import java.util.List;

import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.ResettableEventBus;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.CalculatorContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.VisitableEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.source.EmissionSourceEditorVisitor.DriverEditorCombo;

/**
 * Editor for {@link EmissionSource} object. This object will update to {@link EmissionValueKey} change events.
 */
public class EmissionSourceEditor extends Composite implements ValueAwareEditor<EmissionSource>, TakesValue<EmissionSource> {

  public interface EmissionSourceDriver extends SimpleBeanEditorDriver<EmissionSource, EmissionSourceEditor> { }

  private final SimplePanel panel = new SimplePanel();
  private final EmissionSourceEditorVisitor editorVisitor;
  private final ResettableEventBus resettableEventBus;
  private EditorDelegate<EmissionSource> delegate;
  private DriverEditorCombo currentDriverEditorCombo;
  private EmissionSource currentEmissionSource;
  final TextBox labelEditor = new TextBox();
  private EmissionValueKey emissionValueKey;

  public EmissionSourceEditor(final EventBus eventBus, final SourceMapPanel map, final CalculatorContext context,
      final CalculatorServiceAsync service, final HelpPopupController hpC) {
    resettableEventBus = new ResettableEventBus(eventBus);
    editorVisitor = new EmissionSourceEditorVisitor(resettableEventBus, map, context, service, hpC);

    initWidget(panel);
    addStyleName(StyleUtil.joinStyles(R.css().flex(), R.css().grow()));
  }

  @Override
  public EmissionSource getValue() {
    return currentEmissionSource;
  }

  @Ignore
  public TextBox getLabelEditor() {
    return labelEditor;
  }

  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionValueKey = key;
  }

  public void updateEmissionValueKey(final EmissionValueKey key) {
    setEmissionValueKey(key);
    if (currentDriverEditorCombo != null) {
      currentDriverEditorCombo.getEditor().setEmissionValueKey(key);
    }
  }

  @Override
  public void setDelegate(final EditorDelegate<EmissionSource> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void flush() {
    if (currentDriverEditorCombo != null) {
      final EmissionSource emissionSource = (EmissionSource) currentDriverEditorCombo.getDriver().flush();
      emissionSource.setLabel(labelEditor.getText());
      if (currentDriverEditorCombo.getDriver().hasErrors()) {
        currentDriverEditorCombo.getEditor().focusOnErrors();
        for (final EditorError error : (List<EditorError>) currentDriverEditorCombo.getDriver().getErrors()) {
          delegate.recordError(error.getMessage(), error.getValue(), error.getUserData());
        }
      } else {
        currentEmissionSource = emissionSource;

      }
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void setValue(final EmissionSource source) {
    currentEmissionSource = source;
    resettableEventBus.removeHandlers();
    try {
      currentDriverEditorCombo = ((VisitableEmissionSource) source).accept(editorVisitor);
    } catch (final AeriusException e) {
      currentDriverEditorCombo = null;
    }
    if (currentDriverEditorCombo == null) {
      panel.setVisible(false);
    } else {
      currentDriverEditorCombo.getEditor().asWidget().addStyleName(R.css().flex());
      currentDriverEditorCombo.getEditor().asWidget().addStyleName(R.css().grow());
      currentDriverEditorCombo.getEditor().asWidget().addStyleName(R.css().columnsClean());
      panel.setWidget(currentDriverEditorCombo.getEditor());
      updateEmissionValueKey(emissionValueKey);

      currentDriverEditorCombo.getDriver().edit(source);
      // Always call the postSetValue after the edit because postSetValue
      // implementations depend on the driver being initialized.
      // The driver _should not_ be flushed (this includes calls to getValue()) after
      // this postSetValue call, except when editing has completed.
      currentDriverEditorCombo.getEditor().postSetValue(source);
    }
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    editorVisitor.onEnsureDebugId(baseID);
  }
}
