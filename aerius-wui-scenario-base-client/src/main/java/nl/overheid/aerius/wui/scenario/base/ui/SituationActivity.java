/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.ImportEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.ui.HasChildActivity;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationCancelEvent;
import nl.overheid.aerius.wui.scenario.base.events.SituationSwitchEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public abstract class SituationActivity extends AbstractActivity implements SituationView.Presenter, HasChildActivity {
  interface SituationActivityEventBinder extends EventBinder<SituationActivity> {}

  private final SituationActivityEventBinder eventBinder = GWT.create(SituationActivityEventBinder.class);

  protected final PlaceController placeController;
  protected final ScenarioBaseAppContext<?, ?> appContext;
  private final ScenarioBasePlace place;
  private final CalculationController calculatorController;
  private final SituationView view;

  private EventBus eventBus;

  public SituationActivity(final PlaceController placeController, final CalculationController calculatorController,
      final ScenarioBaseAppContext<?, ?> appContext, final ScenarioBasePlace place, final SituationView view) {
    this.placeController = placeController;
    this.calculatorController = calculatorController;
    this.appContext = appContext;
    this.place = place;
    this.view = view;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    view.setPresenter(this);
    this.eventBus = eventBus;
    panel.setWidget(view);
    setTabs();
    setCalculationButtonVisible(true);
    startChild(view.getContentPanel(), eventBus);

    refresh();
  }

  public boolean isEditableSituations() {
    return true;
  }

  protected void initEventBinders(final EventBus eventBus) {
    eventBinder.bindEventHandlers(this, eventBus);
  }

  /**
   * set the visibility state of the calculation button. By default the button is set visible in start, so basically method is only necessary to hide
   * the button.
   * <p>Don't call this method from an onCalculationXEvent because it uses {@link CalculatorAppContext#isCalculationRunning}.
   * @param visible if true button is visible.
   */
  protected void setCalculationButtonVisible(final boolean visible) {
    calculatorController.setButtonState(appContext.isCalculationRunning(), visible);
  }

  private void setTabs() {
    final boolean isPriorityProjectUtilisation = place.getJobType() == JobType.PRIORITY_PROJECT_UTILISATION;

    if (appContext.getUserContext().getScenario().getSources(place.getSid1()).getName() == null) {
      final String situationName = isPriorityProjectUtilisation ? M.messages().situationUtilisationReserved() : M.messages().situationDefaultName(1);
      appContext.getUserContext().getScenario().getSources(place.getSid1()).setName(situationName);
    }
    view.setTabName(Situation.SITUATION_ONE, place.getSid1(), appContext.getUserContext().getScenario().getSources(place.getSid1()).getName());

    if (place.has2Situations()) {
      if (appContext.getUserContext().getScenario().getSources(place.getSid2()).getName() == null) {
        final String situationName = isPriorityProjectUtilisation ? M.messages().situationUtilisationUtilised() : M.messages()
            .situationDefaultName(2);
        appContext.getUserContext().getScenario().getSources(place.getSid2()).setName(situationName);
      }
      view.setTabName(Situation.SITUATION_TWO, place.getSid2(), appContext.getUserContext().getScenario().getSources(place.getSid2()).getName());
    } else {
      view.setTabName(Situation.SITUATION_TWO, -1, null);
    }

    view.setEditable(isEditableSituations());
  }

  @Override
  public void onDelete(final Situation situation) {
    int placeId = 0;
    int removedPlaceId = -1;
    if (situation == Situation.SITUATION_TWO) {
      placeId = place.getSid1();
      removedPlaceId = place.getSid2();
    } else if (situation == Situation.SITUATION_ONE && appContext.getUserContext().getScenario().getSources(place.getSid2()) != null) {
      //make situation 2 the situation 1.
      final EmissionSourceList sourceListSituation2 = appContext.getUserContext().getScenario().getSources(place.getSid2());
      //ensure the default name gets set to situation 1 if needed.
      if (M.messages().situationTwo().equals(sourceListSituation2.getName())) {
        sourceListSituation2.setName(M.messages().situationOne());
      }
      placeId = place.getSid2();
      removedPlaceId = place.getSid1();
    }
    appContext.deleteEmissionSourceList(appContext.getUserContext(), removedPlaceId);
    placeController.goTo(createPlace(place, placeId, ScenarioBasePlace.NO_SID2, null));
  }

  @Override
  public void switchSituations() {
    // Simply swap the situation id's and update which situation is shown so the current one remains the one shown.
    // If the current situation is comparison keep that situation
    final Situation situation = place.getSituation() == Situation.COMPARISON ? Situation.COMPARISON
        : place.getSituation() == Situation.SITUATION_ONE ? Situation.SITUATION_TWO : Situation.SITUATION_ONE;
    final CalculatedScenario calcScenario = appContext.getUserContext().getCalculatedScenario();
    if (calcScenario instanceof CalculatedComparison) {
      ((CalculatedComparison) calcScenario).swap();
    }
    placeController.goTo(createPlace(place, place.getSid2(), place.getSid1(), situation));
    // update results
    eventBus.fireEvent(new SituationSwitchEvent());
  }

  @Override
  public void onAdd(final Situation situation) {
    if (situation == Situation.SITUATION_TWO) {
      final EmissionSourceList copy = appContext.getUserContext().getSources(place.getSid1()).copy();
      appContext.getUserContext().addSources(copy);
      copy.setName(M.messages().situationDefaultName(copy.getId() + 1));
      placeController.goTo(createPlace(place, place.getSid1(), copy.getId(), Situation.SITUATION_TWO));
    }
  }

  @Override
  public void onSelect(final Situation situation) {
    placeController.goTo(createPlace(place, situation));
  }

  @EventHandler
  public void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent event) {
    refresh();
  }

  @EventHandler
  public void onYearChange(final YearChangeEvent event) {
    refresh();
  }

  @EventHandler
  public void onImport(final ImportEvent event) {
    refresh();
  }

  @EventHandler
  public void onCancelCalculation(final CalculationCancelEvent event) {
    calculatorController.setButtonState(false, true);
  }

  @Override
  public IsWidget getCalculationButtons() {
    return calculatorController == null ? null : calculatorController.getCalculationButtons();
  }

  protected ScenarioBasePlace createPlace(final ScenarioBasePlace currentPlace, final Situation situation) {
    final ScenarioBasePlace copy = currentPlace.copy();
    copy.setSituation(situation);
    return copy;
  }

  private Place createPlace(final ScenarioBasePlace currentPlace, final int placeSid1, final int placeSid2, final Situation situation) {
    final ScenarioBasePlace copy = currentPlace.copy();
    copy.setsId1(placeSid1);
    copy.setsId2(placeSid2);
    copy.setSituation(situation);
    return copy;
  }

  /**
   * Called when an event is triggered for which the content of the page needs to be updated. Subclasses should implement the
   * {@link #refresh(Situation, EmissionValueKey, long)} method, which is called from this method.
   */
  protected final void refresh() {
    final EmissionValueKey key = appContext.getUserContext().getEmissionValueKey();
    final EmissionResultKey resultKey = appContext.getUserContext().getEmissionResultKey();
    final Situation situation = place.getSituation();

    refresh(situation, key, resultKey);
    view.setFocus(situation);
  }

  /**
   * Called when an event is triggered for which the content of the page needs to be updated. Subclasses should implement updating their own view.
   * @param sit current situation
   * @param key current {@link EmissionValueKey}
   */
  protected abstract void refresh(Situation sit, EmissionValueKey key, EmissionResultKey resultKey);
}
