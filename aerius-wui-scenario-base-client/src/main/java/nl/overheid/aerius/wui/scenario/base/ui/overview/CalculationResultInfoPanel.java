/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.deposition.ResultInfoType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.table.DataProvider;
import nl.overheid.aerius.wui.main.widget.table.SimpleCollapsibleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TableColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

/**
 * Stacked panel with result per nature area per habitat.
 */
public class CalculationResultInfoPanel extends Composite implements Editor<List<CalculationAreaSummaryResult>> {
  public interface CalculationResultInfoDriver
      extends SimpleBeanEditorDriver<List<CalculationAreaSummaryResult>, CalculationResultInfoPanel> { }

  interface CalculationResultInfoPanelUiBinder extends UiBinder<Widget, CalculationResultInfoPanel> { }

  private static final CalculationResultInfoPanelUiBinder UI_BINDER = GWT.create(CalculationResultInfoPanelUiBinder.class);

//  private static final double SHOW_VALUE_MARGIN = 0.005;

  @Path("") final DataProvider<List<CalculationAreaSummaryResult>, CalculationAreaSummaryResult> dataProvider;
  @UiField SimpleCollapsibleDivTable<CalculationAreaSummaryResult> divTable;

  @UiField(provided = true) SimpleWidgetFactory<CalculationAreaSummaryResult> zoomColumn;
  @UiField(provided = true) TextColumn<CalculationAreaSummaryResult> labelColumn;
  @UiField(provided = true) TableColumn<HabitatSummaryResults, CalculationAreaSummaryResult, ComparisonDepositionTable> tableContent;

  private ResultInfoType resultType;
  private boolean comparisonResults;
  private EmissionResultKey emissionResultKey;
  private int calcId1;
  private int calcId2;

  private final Comparator<CalculationAreaSummaryResult> areaCompator = new Comparator<CalculationAreaSummaryResult>() {
    @Override
    public int compare(final CalculationAreaSummaryResult casr1, final CalculationAreaSummaryResult casr2) {
      return Double.compare(getMax(casr2), getMax(casr1));
    }

    private double getMax(final CalculationAreaSummaryResult casr) {
      double max = Double.NEGATIVE_INFINITY;
      for (final Entry<Integer, HabitatSummaryResults> entry : casr.getHabitats().entrySet()) {
        max = Math.max(max, CalculationResultInfoPanel.this.getValue(entry.getValue()));
      }
      return max;
    }
  };

  private final SingleSelectionModel<CalculationAreaSummaryResult> selectionModel =
      new SingleSelectionModel<>();

  private final UserContext userContext;

  @Inject
  public CalculationResultInfoPanel(final EventBus eventBus, final UserContext userContext) {
    this.userContext = userContext;
    zoomColumn = new SimpleWidgetFactory<CalculationAreaSummaryResult>() {
      @Override
      public void applyCellOptions(final Widget cell, final CalculationAreaSummaryResult object) {
        super.applyCellOptions(cell, object);

        cell.addDomHandler(new ClickHandler() {
          @Override
          public void onClick(final ClickEvent event) {
            event.stopPropagation();
            eventBus.fireEvent(new LocationChangeEvent(object.getArea().getBounds()));
          }
        }, ClickEvent.getType());
      }
    };
    labelColumn = new TextColumn<CalculationAreaSummaryResult>() {
      @Override
      public String getValue(final CalculationAreaSummaryResult object) {
        return object.getArea().getName();
      }
    };
    labelColumn.ensureDebugId(TestID.LABEL);

    tableContent = new TableColumn<HabitatSummaryResults, CalculationAreaSummaryResult, ComparisonDepositionTable>() {
      @Override
      public Collection<HabitatSummaryResults> getRowData(final CalculationAreaSummaryResult object) {
        final List<HabitatSummaryResults> value = new ArrayList<>(object.getHabitats().values());
        Collections.sort(value, new Comparator<HabitatSummaryResults>() {
          @Override
          public int compare(final HabitatSummaryResults o1, final HabitatSummaryResults o2) {
            return Double.compare(CalculationResultInfoPanel.this.getValue(o2), CalculationResultInfoPanel.this.getValue(o1));
          }
        });
        return value;
      }

      @Override
      public ComparisonDepositionTable createDataTable(final CalculationAreaSummaryResult origin) {
        final int assessmentAreaId = origin.getArea().getId();
        return new ComparisonDepositionTable(eventBus, assessmentAreaId, comparisonResults) {

          @Override
          protected String getValueAsString(final HabitatSummaryResults object) {
            return CalculationResultInfoPanel.this.getValueAsString(object);
          }

          @Override
          protected double getValue(final HabitatSummaryResults object) {
            return CalculationResultInfoPanel.this.getValue(object);
          }
        };
      }

      @Override
      public void applyRowOptions(final Widget rowContainer, final ComparisonDepositionTable widget) {
        super.applyRowOptions(rowContainer, widget);

        rowContainer.addHandler(new ValueChangeHandler<Boolean>() {
          @Override
          public void onValueChange(final ValueChangeEvent<Boolean> event) {
            widget.updateHoverSelection();
          }
        }, ValueChangeEvent.getType());
      }

    };

    initWidget(UI_BINDER.createAndBindUi(this));

    dataProvider = new DataProvider<>();
    dataProvider.addDataDisplay(divTable);
    dataProvider.setComparator(areaCompator);
    divTable.setSelectionModel(selectionModel);
    divTable.ensureDebugId(TestID.TABLE);
  }

  protected String getValueAsString(final HabitatSummaryResults object) {
    final double value = getValue(object);

    String labelValue;
    switch (resultType) {
    case PERCENTAGE_KDW:
      labelValue = FormatUtil.formatPercentage(value);
      break;
    case TOTAL_DEPOSITION:
    default:
      labelValue = FormatUtil.formatDepositionPASProof(value, userContext.getEmissionResultValueDisplaySettings());
      break;
    }

    // if (value >= SHOW_VALUE_MARGIN) {
      // no + sign
      // labelValue = (comparisonResults ? "+" : "") + labelValue;
    // }
    return labelValue;
  }

  /**
   * Returns the value for the active {@link ResultInfoType}.
   * @param object object to get value from
   * @return result value
   */
  protected double getValue(final HabitatSummaryResults object) {
    final HabitatSummaryResult result = comparisonResults ? object.getComparisonSummary(calcId1, calcId2) : object.getHabitatSummaryResults(calcId1);
    final double value;
    if (result == null) {
      value = 0.0d;
    } else {
      switch (resultType) {
      case PERCENTAGE_KDW:
        value = result.getPercentageKDW().get(emissionResultKey);
        break;
      case AVERAGE_DEPOSITION:
        value = result.getAverage().get(emissionResultKey);
        break;
      case MAXIMUM_DEPOSITION:
      case MAXIMUM_INCREASE:
        value = result.getMax().get(emissionResultKey);
        break;
      case TOTAL_DEPOSITION:
      default:
        value = result.getTotal().get(emissionResultKey);
        break;
      }
    }
    return value;
  }

  public void setCalculationId(final int calcId1, final int calcId2) {
    this.calcId1 = calcId1;
    this.calcId2 = calcId2;
    ensureAreaSelectionRemains();
  }

  private void ensureAreaSelectionRemains() {
    //ensure the selected area stays un-collapsed.
    final CalculationAreaSummaryResult selected = selectionModel.getSelectedObject();
    if (selected != null) {
      dataProvider.refresh();
      selectionModel.setSelected(selected, true);
    }
  }

  /**
   * @param resultType The view to use for habitat deposition values.
   */
  public void setResultDisplayType(final ResultInfoType resultType) {
    this.resultType = resultType;
    ensureAreaSelectionRemains();
    dataProvider.refresh();
  }

  /**
   * Set if the table will display comparison results. The comparison results are shown with + and - signs.
   *
   * @param comparisonResults true if results are comparison results
   */
  public void setComparisonResults(final boolean comparisonResults) {
    this.comparisonResults = comparisonResults;
  }

  /**
   * Sets the active emission value key.
   *
   * @param emissionResultKey key to use to display values.
   */
  public void setEmissionResultKey(final EmissionResultKey emissionResultKey) {
    this.emissionResultKey = emissionResultKey;
  }
}
