/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.scenario.SectorEmissionSummary;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.SwitchPanel;
import nl.overheid.aerius.wui.scenario.base.ui.overview.ComparisonDataGrid;
import nl.overheid.aerius.wui.scenario.base.ui.overview.SourcesPanel;

/**
 * Implementation for {@link EmissionSourcesView}.
 */
@Singleton
public class EmissionSourcesViewImpl extends Composite implements EmissionSourcesView {
  private static final EmissionSourcesViewImplUiBinder UI_BINDER = GWT.create(EmissionSourcesViewImplUiBinder.class);

  interface EmissionSourcesViewImplUiBinder extends UiBinder<Widget, EmissionSourcesViewImpl> {}

  @UiField SwitchPanel contentDeckPanel;
  @UiField(provided = true) SourcesPanel sourceOverview;
  @UiField Label labelEmissionCompareList;
  @UiField ComparisonDataGrid comparisonDataGrid;

  @Inject
  public EmissionSourcesViewImpl(final SourcesPanel sourcesView) {
    sourceOverview = sourcesView;
    initWidget(UI_BINDER.createAndBindUi(this));

    comparisonDataGrid.ensureDebugId(TestID.TABLE_COMPARISON_OVERVIEW);
  }

  @Override
  public void setValue(final EmissionValueKey key, final ArrayList<SectorEmissionSummary> differences, final String explainText) {
    contentDeckPanel.showWidget(1);
    labelEmissionCompareList.setText(explainText);
    comparisonDataGrid.setRowCount(differences.size());
    comparisonDataGrid.setKey(key);
    comparisonDataGrid.setRowData(0, differences);
    comparisonDataGrid.redraw();
  }

  @Override
  public void setValue(final EmissionValueKey key, final boolean enableAdding, final EmissionSourceList sources, final double total,
      final double totalExtra) {
    contentDeckPanel.showWidget(0);
    sourceOverview.setData(sources, key, enableAdding);
    if (key.getSubstance().contains(Substance.NOXNH3)) {
      sourceOverview.setTotalEmissionDouble(total, totalExtra);
    } else {
      sourceOverview.setTotalEmission(total, key.getSubstance());
    }
  }

  @Override
  public String getTitleText() {
    return M.messages().calculationListEmissionSourceNameHeader();
  }
}
