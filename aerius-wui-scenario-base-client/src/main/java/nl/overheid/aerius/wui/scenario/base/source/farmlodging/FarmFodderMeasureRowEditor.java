/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.LodgingStackType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.FarmFodderMeasure;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.ValidatedValueBoxEditor;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox.NameTextMachine;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;

class FarmFodderMeasureRowEditor extends FarmRowEditor<FarmLodgingFodderMeasureCategory, FarmFodderMeasure> {
  private static final FarmFodderMeasureRowEditorUiBinder UI_BINDER = GWT.create(FarmFodderMeasureRowEditorUiBinder.class);

  interface FarmFodderMeasureRowEditorUiBinder extends UiBinder<Widget, FarmFodderMeasureRowEditor> {
  }

  @Path("category") ValidatedValueBoxEditor<FarmLodgingFodderMeasureCategory> validatedCategoryEditor;

  private static final TextMachine<FarmLodgingFodderMeasureCategory> FODDER_MEASURE_NAMER = new NameTextMachine<FarmLodgingFodderMeasureCategory>() {
    @Override
    public String asError(final String input) {
      return M.messages().ivFarmNoValidMeasure(input);
    }
  };

  public FarmFodderMeasureRowEditor(final SectorCategories categories, final HelpPopupController hpC,
      final FarmLodgingCategorySelection categorySelection) {
    super(categories, hpC, new ValidateLodgingSystemHandler<FarmLodgingFodderMeasureCategory>(categorySelection) {
      @Override
      boolean canStack(final FarmLodgingCategory lodgingCategory, final FarmLodgingFodderMeasureCategory value) {
        return lodgingCategory.canStackFodderMeasureCategory(value);
      }
    }, FODDER_MEASURE_NAMER);

    validatedCategoryEditor = new ValidatedValueBoxEditor<FarmLodgingFodderMeasureCategory>(categoryEditor.getValueBox(), "", true) {
      @Override
      protected String noValidInput(final String value) {
        return M.messages().ivFarmInvalidFodder(value);
      }
    };
    validatedCategoryEditor.addValidator(new Validator<FarmLodgingFodderMeasureCategory>() {
      @Override
      public String validate(final FarmLodgingFodderMeasureCategory value) {
        return isInvalidCategory(value) ? M.messages().ivFarmInvalidFodder(FODDER_MEASURE_NAMER.asText(value)) : null;
      }
    });
    validatedCategoryEditor.addValidator(new Validator<FarmLodgingFodderMeasureCategory>() {
      @Override
      public String validate(final FarmLodgingFodderMeasureCategory value) {
        return value == null ? M.messages().ivFarmEmptyFodder() : null;
      }
    });

    getRow().insert(UI_BINDER.createAndBindUi(this), 0);

    StyleUtil.I.setPlaceHolder(categoryEditor.getValueBox(), M.messages().farmLodgingStackType(LodgingStackType.FODDER_MEASURE));
    hpC.addWidget(categoryEditor, hpC.tt().ttFarmLodgingFodderMeasure());
  }

  @Override
  ArrayList<FarmLodgingFodderMeasureCategory> getCorrectCategories(final SectorCategories categories) {
    return categories.getFarmLodgingCategories().getFarmLodgingFodderMeasureCategories();
  }

  @Override
  void onCategoryValueChanged(final FarmLodgingFodderMeasureCategory newCategory) {
    // NO-OP
  }

}
