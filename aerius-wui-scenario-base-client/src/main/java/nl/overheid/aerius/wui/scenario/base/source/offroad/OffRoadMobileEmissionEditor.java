/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.offroad;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.adapters.SimpleEditor;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource.SourceCategoryType;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.service.CalculatorServiceAsync;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.ListCancelButtonEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup;
import nl.overheid.aerius.wui.main.widget.RadioButtonGroup.RadioButtonContentResource;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;
import nl.overheid.aerius.wui.scenario.base.source.core.BaseEmissionSourceEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor;
import nl.overheid.aerius.wui.scenario.base.source.core.InputListEmissionEditor.InputEditor;
import nl.overheid.aerius.wui.scenario.base.source.offroad.OffRoadMobileInputEditor.OffRoadMobileInputDriver;

/**
 * Editor for emissions of multiple off road mobile sources at a single location.
 */
public class OffRoadMobileEmissionEditor extends BaseEmissionSourceEditor<OffRoadMobileEmissionSource> {

  public interface OffRoadMobileEmissionDriver extends SimpleBeanEditorDriver<OffRoadMobileEmissionSource, OffRoadMobileEmissionEditor> { }

  interface OffRoadMobileEmissionEditorEventBinder extends EventBinder<OffRoadMobileEmissionEditor> { }

  private static class OffRoadMobileMessages implements EditableDivTableMessages {
    private final HelpPopupController hpC;

    public OffRoadMobileMessages(final HelpPopupController hpC) {
      this.hpC = hpC;
    }

    @Override
    public String add() {
      return M.messages().mobileSourceAddVehicle();
    }

    @Override
    public HelpInfo editHelp() {
      return hpC.tt().ttMobileSourceEdit();
    }

    @Override
    public HelpInfo copyHelp() {
      return hpC.tt().ttMobileSourceCopy();
    }

    @Override
    public HelpInfo deleteHelp() {
      return hpC.tt().ttMobileSourceDelete();
    }

    @Override
    public HelpInfo addHelp() {
      return hpC.tt().ttMobileSourceAdd();
    }

    @Override
    public String customButtonText() {
      return null;
    }

    @Override
    public HelpInfo customButtonHelp() {
      return null;
    }
  }

  @Path("emissionSubSources") InputListEmissionEditor<OffRoadVehicleEmissionSubSource, OffRoadMobileInputEditor> emissionSubSourcesEditor;

  LeafValueEditor<Sector> sectorEditor = SimpleEditor.of();

  private final OffRoadMobileEmissionEditorEventBinder eventBinder = GWT.create(OffRoadMobileEmissionEditorEventBinder.class);
  private final EventBus eventBus;

  public OffRoadMobileEmissionEditor(final EventBus eventBus, final ScenarioBaseContext context, final CalculatorServiceAsync service,
      final HelpPopupController hpC) {
    super(M.messages().mobileEmissionsOffRoadTitle());
    this.eventBus = eventBus;
    final RadioButtonGroup<SourceCategoryType> radioButtonGroup = new RadioButtonGroup<SourceCategoryType>(new RadioButtonContentResource<SourceCategoryType>() {
      @Override
      public String getRadioButtonText(final SourceCategoryType value) {
        return value == SourceCategoryType.STAGE_CLASS
            ? M.messages().mobileSourceStageCategory()
                : M.messages().mobileSourceCustomCategory();
      }
    });
    radioButtonGroup.ensureDebugId(TestID.INPUT_MOBILESOURCE_TYPE_TOGGLE);
    radioButtonGroup.addButtons(SourceCategoryType.values());
    emissionSubSourcesEditor = new InputListEmissionEditor<OffRoadVehicleEmissionSubSource, OffRoadMobileInputEditor>(
        eventBus, new OffRoadMobileInputEditor(eventBus, context, radioButtonGroup, hpC), hpC, new OffRoadMobileMessages(hpC),
        OffRoadVehicleEmissionSubSource.class, "OffRoad",
        (SimpleBeanEditorDriver<OffRoadVehicleEmissionSubSource, InputEditor<OffRoadVehicleEmissionSubSource>>)
        GWT.create(OffRoadMobileInputDriver.class), service, false) {
      @Override
      protected OffRoadVehicleEmissionSubSource createNewRowEmissionValues() {
        final OffRoadVehicleEmissionSubSource subSource = new OffRoadVehicleEmissionSubSource();
        if (sectorEditor.getValue() != null) {
          subSource.setCharacteristics(sectorEditor.getValue().getDefaultCharacteristics());
        }
        return subSource;
      }
      @Override
      public void edit(final OffRoadVehicleEmissionSubSource value) {
        super.edit(value);
        if (value.getCategoryType() == null) {
          radioButtonGroup.setValue(SourceCategoryType.STAGE_CLASS);
        } else {
          radioButtonGroup.setValue(value.getCategoryType());
        }

      }
    };
    addContentPanel(emissionSubSourcesEditor);
    emissionSubSourcesEditor.ensureDebugId(TestID.DIVTABLE_EDITABLE);
  }

  @EventHandler
  void onCancelButton(final ListCancelButtonEvent event) {
    emissionSubSourcesEditor.onCancel();
  }

  @Override
  public void setEmissionValueKey(final EmissionValueKey key) {
    emissionSubSourcesEditor.setEmissionValueKey(key);
  }

  @Override
  public void postSetValue(final OffRoadMobileEmissionSource source) {
    eventBinder.bindEventHandlers(this, eventBus);
    super.postSetValue(source);
    emissionSubSourcesEditor.postSetValue(source);
  }

  @Override
  public void setValue(final OffRoadMobileEmissionSource source) {
    super.setValue(source);
  }
}
