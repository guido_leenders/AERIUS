/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.source;

import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.source.EmissionSource;

/**
 * An emission viewer is meant to view emission values but not edit them.
 *
 * @param <T> EmissionValues type to view
 */
public abstract class EmissionViewer<T extends EmissionSource> extends Composite implements IsWidget, LeafValueEditor<T> {
  protected int year;

  @Override
  public T getValue() {
    return null;
  }

  public void setYear(final int year) {
    this.year = year;
  }
}
