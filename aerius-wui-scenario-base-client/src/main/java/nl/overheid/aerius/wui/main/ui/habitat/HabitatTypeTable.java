/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.habitat;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.HabitatHoverType;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.info.HabitatInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.HabitatTypeDisplayEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeEvent.HabitatDisplayType;
import nl.overheid.aerius.wui.main.event.HabitatTypeHoverEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.ContextDivTable;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveDivTableRow;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveDivTableRowDecorator;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class HabitatTypeTable extends Composite implements IsInteractiveDataTable<HabitatInfo> {
  private static final double HIGHER_VALUE = 0.9999;
  private static final double LOWER_VALUE = 0.0001;

  interface HabitatTypeTableUiBinder extends UiBinder<Widget, HabitatTypeTable> {}

  private static final HabitatTypeTableUiBinder UI_BINDER = GWT.create(HabitatTypeTableUiBinder.class);

  @UiField FlowPanel header;

  @UiField SimpleInteractiveClickDivTable<HabitatInfo> table;
  @UiField(provided = true) TextColumn<HabitatInfo> nameCodeColumn;
  @UiField(provided = true) ImageColumn<HabitatInfo> categoryColumn;

  private int areaId;
  private HabitatHoverType type;

  private final HabitatInfoPopup context;

  public HabitatTypeTable(final EventBus eventBus, final HelpPopupController hpC, final EmissionResultKey emissionResultKey,
      final UserContext userContext) {
    context = new HabitatInfoPopup(hpC, emissionResultKey, userContext);

    nameCodeColumn = new TextColumn<HabitatInfo>() {
      @Override
      public String getValue(final HabitatInfo object) {
        return M.messages().infoPanelHabitatTypesDataCode(object.getCode(), object.getName());
      }
    };

    categoryColumn = new ImageColumn<HabitatInfo>() {
      @Override
      public ImageResource getValue(final HabitatInfo object) {
        ImageResource value;
        if (object.getRelevantFraction() < LOWER_VALUE) {
          value = R.images().habitatRelevantNone();
        } else if (object.getRelevantFraction() > HIGHER_VALUE) {
          value = R.images().habitatRelevantFull();
        } else {
          value = R.images().habitatRelevantPartial();
        }

        return value;
      }

      @Override
      public Image createWidget(final HabitatInfo object) {
        // Add a title (tooltip) to this header showing the relevancy
        final Image image = super.createWidget(object);
        image.setTitle(FormatUtil.formatPercentageWithUnit(
            object.getRelevantFraction() * SharedConstants.PERCENTAGE_TO_FRACTION));
        image.ensureDebugId(TestID.AREAINFORMATION_TYPETABLE_RELEVANT_COVERAGE);
        return image;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    // Wrap the table to give context.
    ContextDivTable.wrap(table, context);

    table.setSelectionModel(new MultiSelectionModel<HabitatInfo>());
    table.addSelectionChangeHandler(new Handler() {

      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final Set<HabitatInfo> selectedSet = ((MultiSelectionModel<HabitatInfo>) table.getSelectionModel()).getSelectedSet();

        final HashSet<Integer> habitatTypeIds = new HashSet<>();
        for (final HabitatInfo info : selectedSet) {
          habitatTypeIds.add(info.getId());
        }

        eventBus.fireEvent(new HabitatTypeDisplayEvent(HabitatDisplayType.REPLACE, type, areaId, habitatTypeIds));
      }
    });
    table.addRowDecorator(new SimpleInteractiveDivTableRowDecorator<HabitatInfo>() {
      @Override
      public void applyRowOptions(final SimpleInteractiveDivTableRow row, final HabitatInfo item) {
        row.asWidget().addDomHandler(new MouseOverHandler() {
          @Override
          public void onMouseOver(final MouseOverEvent event) {
            eventBus.fireEvent(new HabitatTypeHoverEvent(type, areaId, item.getId()));
          }
        }, MouseOverEvent.getType());
        row.asWidget().addDomHandler(new MouseOutHandler() {
          @Override
          public void onMouseOut(final MouseOutEvent event) {
            eventBus.fireEvent(new HabitatTypeHoverEvent(HabitatDisplayType.REMOVE));
          }
        }, MouseOutEvent.getType());
      }
    });
  }

  /**
   * Set whether the table header is visible or not.
   *
   * @param visible True for visible, false for invisible.
   */
  public void setHeaderVisible(final boolean visible) {
    table.setHeaderVisible(visible);
  }

  /**
   * Set whether the table should display/generate the relevance column.
   * @param visible True for yes, false for nope.
   */
  public void setRelevanceVisible(final boolean visible) {
    if (!visible) {
      table.removeColumn(categoryColumn);
    }
  }

  /**
   * Set whether the habitattype info popup should only show relevant surface areas.
   * In case true, this also calls setRelevanceVisible(false).
   */
  public void setRelevantSurfacesOnly(final boolean relevantSurfacesOnly) {
    context.setRelevantSurfacesOnly(relevantSurfacesOnly);
    if (relevantSurfacesOnly) {
      setRelevanceVisible(false);
    }
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    table.ensureDebugId(baseID);
  }

  @Override
  public SimpleInteractiveClickDivTable<HabitatInfo> asDataTable() {
    return table;
  }

  public void setAreaInfo(final HabitatHoverType type, final int areaId) {
    this.type = type;
    this.areaId = areaId;
  }
}
