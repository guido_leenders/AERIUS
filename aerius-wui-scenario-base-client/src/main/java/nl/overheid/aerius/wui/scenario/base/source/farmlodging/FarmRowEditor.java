/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.farmlodging;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;

import nl.overheid.aerius.shared.domain.sector.category.AbstractEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.LodgingStackType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.editor.DynamicRowEditor;
import nl.overheid.aerius.wui.main.util.StyleUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.SuggestBox;
import nl.overheid.aerius.wui.main.widget.SuggestBox.TextMachine;

abstract class FarmRowEditor<C extends AbstractEmissionCategory, T> extends DynamicRowEditor<T> implements HasValueChangeHandlers<C> {

  @UiField(provided = true) SuggestBox<C> categoryEditor;

  private final ValidateLodgingSystemHandler<C> validateLodgingSystemHandler;

  public FarmRowEditor(final SectorCategories categories, final HelpPopupController hpC,
      final ValidateLodgingSystemHandler<C> validateLodgingSystemHandler, final TextMachine<C> namer) {
    super(hpC, hpC.tt().ttDeleteButtonHelp(), false);
    this.validateLodgingSystemHandler = validateLodgingSystemHandler;

    categoryEditor = new SuggestBox<C>(
        new FarmSuggestOracle<C>(getCorrectCategories(categories)) {
          @Override
          public String getStyleName(final C category) {
            // Invalid entries in orange
            return isInvalidCategory(category) ? R.css().suggestionWarning() : null;
          }

          @Override
          public void customSort() {
            // Invalid (orange) entries at the bottom of the list
            Collections.sort(super.objects, new Comparator<C>() {
              @Override
              public int compare(final C o1, final C o2) {
                return Boolean.compare(isInvalidCategory(o1), isInvalidCategory(o2));
              }
            });
          }
        },
        namer) {
          @Override
          protected void setValue(final C value, final boolean fireEvents, final boolean soft) {
            super.setValue(value, fireEvents, soft);
            setTitle(value == null ? "" : value.getCode() + ' ' + value.getDescription());
          }
    };
  }

  protected final void afterBinding(final HelpPopupController hpC) {
    hpC.addWidget(categoryEditor, hpC.tt().ttFarmLodgingAdditionalCategory());

    StyleUtil.I.setPlaceHolder(categoryEditor.getValueBox(), M.messages().farmLodgingStackType(LodgingStackType.ADDITIONAL_MEASURE));
  }

  abstract ArrayList<C> getCorrectCategories(final SectorCategories categories);

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<C> handler) {
    return categoryEditor.addValueChangeHandler(handler);
  }

  @UiHandler("categoryEditor")
  public void onCategoryValueChange(final ValueChangeEvent<C> event) {
    onCategoryValueChanged(event.getValue());
  }

  abstract void onCategoryValueChanged(C newCategory);

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    categoryEditor.getValueBox().ensureDebugId(baseID + "-" + TestID.DESCRIPTION_BOX);
  }

  @Override
  protected boolean isEmpty() {
    return categoryEditor.getValue() == null;
  }

  public C getSelectedValue() {
    return categoryEditor.getValue();
  }

  public boolean refreshWarning(final C category) {
    final boolean warning = isInvalidCategory(category);
    categoryEditor.setStyleName(R.css().suggestionWarningBox(), warning);
    return warning;
  }

  protected boolean isInvalidCategory(final C value) {
    return value != null && validateLodgingSystemHandler.isInvalid(value);
  }

  interface FarmLodgingCategorySelection {

    FarmLodgingCategory getCurrentLodgingCategory();

  }

  abstract static class ValidateLodgingSystemHandler<T extends AbstractEmissionCategory> {

    private final FarmLodgingCategorySelection categorySelection;

    ValidateLodgingSystemHandler(final FarmLodgingCategorySelection categorySelection) {
      this.categorySelection = categorySelection;
    }

    boolean isInvalid(final T value) {
      return categorySelection.getCurrentLodgingCategory() != null && value != null
          && !canStack(categorySelection.getCurrentLodgingCategory(), value);
    }

    abstract boolean canStack(final FarmLodgingCategory lodgingCategory, final T value);
  }

}
