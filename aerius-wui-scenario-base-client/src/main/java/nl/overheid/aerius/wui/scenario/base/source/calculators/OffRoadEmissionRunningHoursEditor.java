/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.calculators;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseContext;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleOperatingHoursSpecification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.editor.DoubleValueBox;
import nl.overheid.aerius.wui.main.ui.editor.IntValueBox;
import nl.overheid.aerius.wui.main.ui.validation.IntegerRangeValidator;
import nl.overheid.aerius.wui.main.ui.validation.PercentageRangeValidator;

class OffRoadEmissionRunningHoursEditor extends OffRoadEmissionBaseEditor<OffRoadVehicleOperatingHoursSpecification>
    implements Editor<OffRoadVehicleOperatingHoursSpecification> {
  interface OffRoadEmissionRunningHoursDriver extends SimpleBeanEditorDriver<OffRoadVehicleOperatingHoursSpecification, OffRoadEmissionRunningHoursEditor> {}

  interface OffRoadEmissionCalculatorWidgetUiBinder extends UiBinder<Widget, OffRoadEmissionRunningHoursEditor> {}

  private final OffRoadEmissionCalculatorWidgetUiBinder UI_BINDER = GWT.create(OffRoadEmissionCalculatorWidgetUiBinder.class);

  @UiField IntValueBox power;
  @UiField IntValueBox load;
  @UiField IntValueBox usage;
  @UiField DoubleValueBox emissionFactor;

  private final PercentageRangeValidator loadValidator = new PercentageRangeValidator() {

    @Override
    protected String getMessage(final Integer value) {
      return M.messages().ivOffRoadEmissionCalculatorLoadValidator();
    }
  };

  private final IntegerRangeValidator usageValidator = new IntegerRangeValidator(0, SharedConstants.NUMBER_HOURS_PER_YEAR) {
    @Override
    protected String getMessage(final Integer value) {
      return M.messages().ivOffRoadEmissionCalculatorUsageValidator();
    }
  };

  /**
   * Constructor. initializes the widget and makes the checkBox into a toggle button
   */
  @Inject
  public OffRoadEmissionRunningHoursEditor(final ScenarioBaseContext context) {
    super(context);

    initWidget(UI_BINDER.createAndBindUi(this));

    ensureDebugId(TestID.OFF_ROAD_CALCULATOR_RUNNING_HOURS);

    load.addValidator(loadValidator);
    usage.addValidator(usageValidator);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    power.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_POWER);
    load.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_LOAD);
    usage.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_USAGE);
    emissionFactor.ensureDebugId(baseID + "-" + TestID.OFF_ROAD_CALCULATOR_EF_MACHINERY);
  }

  @UiHandler({"power", "load", "usage", "emissionFactor"})
  void onValueChange(final ChangeEvent e) {
    refresh();
  }

  @UiHandler({"power", "load", "usage", "emissionFactor"})
  void onValueFocus(final FocusEvent e) {
    refresh();
  }

  @UiHandler({"power", "load", "usage", "emissionFactor"})
  void onValueKeyUp(final KeyUpEvent e) {
    refresh();
  }

  @Override
  protected void setDefaultFuelProperties(final FuelTypeProperties value) {
    power.setValue(value.getPower());
    load.setValue(value.getLoadInPercentage());
    emissionFactor.setValue(value.getEmissionFactor());

    refresh();
  }

  @Override
  protected OffRoadVehicleOperatingHoursSpecification getOffRoadVehicleSpecification() {
    final OffRoadVehicleOperatingHoursSpecification spec = new OffRoadVehicleOperatingHoursSpecification();

    spec.setEmissionFactor(emissionFactor.getValue());
    spec.setPower(power.getValue());
    spec.setUsage(usage.getValue());
    spec.setLoad(load.getValue());

    emissionFactor.asEditor().validate();
    power.asEditor().validate();
    usage.asEditor().validate();
    load.asEditor().validate();

    return spec;
  }
}