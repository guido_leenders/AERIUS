/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.SimpleFilter;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationResultsRetrievedEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationStartEvent;
import nl.overheid.aerius.wui.scenario.base.events.ReceptorChangeEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Wrapper to manage one calculation result at one time.
 */
@Singleton
public class VectorCalculationResultLayerWrapper implements CalculationResultLayerWrapper {
  interface CalculationResultLayerWrapperEventBinder extends EventBinder<VectorCalculationResultLayerWrapper> {}

  private final CalculationResultLayerWrapperEventBinder eventBinder = GWT.create(CalculationResultLayerWrapperEventBinder.class);

  private static final float DEFAULT_OPACITY = 0.8F;

  private EmissionResultLayer activeLayer;
  private MapLayoutPanel map;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final PlaceController placeController;
  private final ReceptorPointsContainer container;

  private float previousOpacity = DEFAULT_OPACITY;

  /**
   * Create the {@link EmissionResultLayer} on the given map. For the given {@link CalculatorPlace}, with the given name, using the given
   * {@link AeriusPointsContainer} and for values of the given {@link nl.overheid.aerius.shared.domain.result.EmissionResultKey}.
   *
   * @param eventBus {@link EventBus} to get result change events from.
   * @param placeController {@link PlaceController} to get the place from.
   * @param container {@link AeriusPointsContainer} to get results from.
   * @param userContext {@link ScenarioBaseAppContext} to get the scenario's out of.
   */
  @Inject
  public VectorCalculationResultLayerWrapper(final EventBus eventBus, final PlaceController placeController,
      final ReceptorPointsContainer container, final ScenarioBaseAppContext<?, ?> appContext) {
    this.placeController = placeController;
    this.appContext = appContext;
    this.container = container;

    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onReceptorsChanged(final ReceptorChangeEvent event) {
    if (activeLayer != null) {
      activeLayer.setValues(event.getValue().getCalculationId(), new HashSet<>(event.getValue().getResults()));
    }
  }

  @EventHandler
  void onCalculationResultsRetrieved(final CalculationResultsRetrievedEvent event) {
    if (activeLayer != null) {
      activeLayer.refreshLayer();
    }
  }

  @EventHandler
  void onEmissionSourceChangeEvent(final EmissionSourceChangeEvent event) {
    if ((appContext.getUserContext().getCalculatedScenario() != null) && !appContext.getUserContext().getCalculatedScenario().isInSync()) {
      container.setCalculations(null);
    }
  }

  @EventHandler
  void onStartCalculation(final CalculationStartEvent event) {
    container.setCalculations(appContext.getUserContext().getCalculatedScenario());
    if (activeLayer != null) {
      map.removeLayer((MapLayer) activeLayer);
      previousOpacity = activeLayer.getOpacity();
      activeLayer = null;
    }
  }

  @EventHandler
  void onInitCalculation(final CalculationInitEvent event) {
    final CalculationType calculationType = event.getInitResult().getCalculationSetOptions().getCalculationType();
    switch (calculationType) {
    case PAS:
    case NATURE_AREA:
    case RADIUS:
      activeLayer = new ReceptorLayer(map, (ScenarioBasePlace) placeController.getWhere(), container,
          appContext.getUserContext().getEmissionResultKey(), appContext.getUserContext().getEmissionResultValueDisplaySettings());
      break;
    case CUSTOM_POINTS:
      activeLayer = new CustomEmissionResultLayer(map, (ScenarioBasePlace) placeController.getWhere(), appContext.getUserContext(),
          appContext.getUserContext().getEmissionResultKey(), appContext.getUserContext().getEmissionResultValueDisplaySettings());
      break;
    default:
    }
    if (activeLayer != null) {
      activeLayer.updateMap();
      activeLayer.setOpacity(previousOpacity);
      map.addLayer((MapLayer) activeLayer);
    }
  }

  public boolean isVisible() {
    return (activeLayer != null) && activeLayer.isVisible();
  }

  @Override
  public void setMap(final MapLayoutPanel map) {
    this.map = map;
  }

  /**
   * Set the active layer to be visible or not.
   *
   * @param visible true for yay, false for nay
   */
  @Override
  public void setVisible(final boolean visible) {
    if (activeLayer == null) {
      return;
    }

    map.setVisible(activeLayer, visible);
  }

  /**
   * Set a filter to only show specific receptors (if supported by the active layer).
   *
   * @param filter Filter to apply
   */
  public void setFilter(final SimpleFilter<AeriusResultPoint> filter) {
    if (activeLayer == null) {
      return;
    }

    activeLayer.setFilter(filter);
  }

  /**
   * Refresh the layer.
   *
   * @param force True to force a refresh immediately. Otherwise a delay may occur before a refresh.
   */
  public void refreshLayer(final boolean force) {
    activeLayer.refreshLayer(force);
  }
}
