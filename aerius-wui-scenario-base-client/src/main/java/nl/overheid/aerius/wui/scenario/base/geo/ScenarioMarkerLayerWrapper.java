/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import java.util.ArrayList;
import java.util.HashSet;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

public class ScenarioMarkerLayerWrapper extends MarkerLayerWrapper {
  private HashSet<Integer> selection;

  public ScenarioMarkerLayerWrapper(final MapLayoutPanel map, final String name) {
    super(map, name);
  }

  /**
   * Set the markers.
   *
   * @param scenario {@link Scenario} that contains items we want to have marked on the map.
   * @param place {@link CalculatorPlace} that is used to determine which markers in the {@link Scenario} are relevant.
   */
  public void setMarkers(final Scenario scenario, final ScenarioBasePlace place) {
    if (place.getSituation() == Situation.COMPARISON) {
      final ArrayList<EmissionSourceList> sourceLists = new ArrayList<EmissionSourceList>();
      sourceLists.add(scenario.getSources(place.getSid1()));
      sourceLists.add(scenario.getSources(place.getSid2()));
      setMarkers(sourceLists, scenario.getCalculationPoints());
    } else {
      final EmissionSourceList sources = scenario.getSources(place.getActiveSituationId());
      setMarkers(sources, scenario.getCalculationPoints());
    }
  }

  public void setSelection(final HashSet<Integer> selection) {
    this.selection = selection;

    redraw(false);
  }

  @Override
  protected ArrayList<MarkerItem<?>> filterMarkerItems(final ArrayList<MarkerItem<?>> markerItems) {
    if (selection == null) {
      return markerItems;
    }

    final ArrayList<MarkerItem<?>> filtered = new ArrayList<>();
    for (final MarkerItem<?> item : markerItems) {
      if (selection.contains(item.getId())) {
        filtered.add(item);
      }
    }

    return filtered;
  }
}
