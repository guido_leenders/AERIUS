/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.info.EmissionResultInfo;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.wui.main.event.EmissionResultDisplaySettingChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.SituationSwitchEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Information panel showing emission results at the currently selected point. Content depends on the selected substance and emission result type.
 */
public class EmissionResultPanel extends Composite implements IsCollapsiblePanel {
  private static final String EMPTY_VALUE = "-";

  interface EmissionResultEventBinder extends EventBinder<EmissionResultPanel> {}

  private static final EmissionResultEventBinder EVENT_BINDER = GWT.create(EmissionResultEventBinder.class);

  interface EmissionResultPanelUiBinder extends UiBinder<Widget, EmissionResultPanel> {}

  private static final EmissionResultPanelUiBinder UI_BINDER = GWT.create(EmissionResultPanelUiBinder.class);

  @UiField CollapsiblePanel panel;
  @UiField SpanElement backgroundLabel1;
  @UiField SpanElement backgroundValue1;
  @UiField DivElement backgroundDiv2;
  @UiField SpanElement backgroundLabel2;
  @UiField SpanElement backgroundValue2;
  @UiField DivElement calculatedDiv1;
  @UiField DivElement calculatedTitle;
  @UiField SpanElement calculatedLabel1;
  @UiField SpanElement calculatedValue1;
  @UiField DivElement calculatedDiv2;
  @UiField SpanElement calculatedLabel2;
  @UiField SpanElement calculatedValue2;
  @UiField SpanElement comparisonLabel;
  @UiField SpanElement comparisonValue;
  @UiField DivElement calculatedPercentageDiv1;
  @UiField SpanElement calculatedPercentageLabel1;
  @UiField SpanElement calculatedPercentageValue1;

  private EmissionResultInfo emissionResultInfo;
  private CalculatedScenario scenario;
  private ScenarioBasePlace place;
  private final PlaceController placeController;

  private EmissionResultKey key;
  private EmissionResultValueDisplaySettings displaySettings;

  @Inject
  public EmissionResultPanel(final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext, final PlaceController placeController) {
    this.placeController = placeController;
    initWidget(UI_BINDER.createAndBindUi(this));

    key = appContext.getUserContext().getEmissionResultKey();
    displaySettings = appContext.getUserContext().getEmissionResultValueDisplaySettings();

    EVENT_BINDER.bindEventHandlers(this, eventBus);

    setTitles();
  }

  @EventHandler
  void onYearChange(final YearChangeEvent e) {
    updateEmissionResult();
  }

  @EventHandler
  void onSituationSwitchEvent(final SituationSwitchEvent event) {
    final Place place = placeController.getWhere();
    if (place instanceof ScenarioBasePlace) {
      setEmissionResultInfo(emissionResultInfo, scenario, (ScenarioBasePlace) place);
    } else {
      clear();
    }
  }

  @EventHandler
  void onEmissionResultDisplaySettingChange(final EmissionResultDisplaySettingChangeEvent e) {
    displaySettings = e.getValue();

    updateEmissionResult();
  }

  @EventHandler
  void onSubstanceChange(final EmissionResultKeyChangeEvent e) {
    key = e.getValue();

    updateEmissionResult();
  }

  @Override
  public CollapsiblePanel asCollapsible() {
    return panel;
  }

  /**
   * Clears the panel.
   */
  public void clear() {
    this.emissionResultInfo = null;
    this.scenario = null;
    this.place = null;
    panel.setVisible(false);
  }

  public void setEmissionResultInfo(final EmissionResultInfo emissionResultInfo, final CalculatedScenario scenario, final ScenarioBasePlace place) {
    final boolean hasResults = emissionResultInfo != null;

    panel.setVisible(hasResults);
    if (!hasResults) {
      return;
    }

    this.emissionResultInfo = emissionResultInfo;
    this.scenario = scenario;
    this.place = place;

    setName(scenario, place.getSid1(), calculatedLabel1, calculatedDiv1);
    if (place.has2Situations()) {
      setName(scenario, place.getSid2(), calculatedLabel2, calculatedDiv2);
    } else {
      calculatedLabel2.setInnerText(SharedConstants.NBSP);
      UIObject.setVisible(calculatedDiv2, false);
    }
    UIObject.setVisible(calculatedPercentageDiv1, isPriorityProjectUtilisation());
    updateEmissionResult();
  }

  private void setName(final CalculatedScenario scenario, final int sid, final SpanElement label, final DivElement div) {
    final int calculationId1 = scenario == null ? 0 : scenario.getCalculationId(sid);
    String name = scenario == null || calculationId1 == 0 ? "" : scenario.getSources(calculationId1).getName();
    if (name == null) {
      name = "";
    }

    label.setInnerText(name);
    UIObject.setVisible(div, !name.isEmpty());
  }

  private void updateEmissionResult() {
    if (emissionResultInfo == null) {
      return;
    }

    updateCalculatedEmissionResult();

    updateBackgroundEmissionResult();

    setTitles();
  }

  private void updateCalculatedEmissionResult() {
    if (emissionResultInfo != null && scenario != null && place != null) {
      final double e1 = setEmissionValue(place.getSid1(), calculatedValue1);
      final double e2 = setEmissionValue(place.getSid2(), calculatedValue2);
      final double delta = isPriorityProjectUtilisation() ? e1 - e2 : (e2 - e1);

      comparisonValue.setInnerText(FormatUtil.formatEmissionResultWithUnit(key.getEmissionResultType(), delta, displaySettings));

      // Will always be set, though only shown in case of a priority project utilisation
      calculatedPercentageValue1.setInnerText(delta > 0 ? FormatUtil.formatFactorAsPercentageWithUnit(delta / e1) : FormatUtil
          .formatFactorAsPercentageWithUnit(0));
    }
  }

  private double setEmissionValue(final int sid, final SpanElement span) {
    final int calculationId = scenario == null ? 0 : scenario.getCalculationId(sid);

    final double emissionResult;
    if (calculationId > 0 && emissionResultInfo.getEmissionResults(calculationId) != null) {
      emissionResult = emissionResultInfo.getEmissionResults(calculationId).get(key);
      span.setInnerText(FormatUtil.formatEmissionResultWithUnit(key.getEmissionResultType(), emissionResult, displaySettings));
    } else {
      emissionResult = 0;
    }
    return emissionResult;
  }

  private void updateBackgroundEmissionResult() {
    if (emissionResultInfo.getBackgroundEmissionResults() == null) {
      return;
    }

    final Substance substance = key.getSubstance();
    final EmissionResultType type = key.getEmissionResultType();

    final EmissionResultKey background1Key;
    final EmissionResultKey background2Key;
    if ((substance == Substance.NOX || substance == Substance.NH3) && type == EmissionResultType.DEPOSITION) {
      background1Key = EmissionResultKey.NOXNH3_DEPOSITION;
      background2Key = null;
    } else if ((substance == Substance.NOX || substance == Substance.NO2) && type == EmissionResultType.CONCENTRATION) {
      background1Key = key;
      background2Key = EmissionResultKey.O3_CONCENTRATION;
    } else {
      background1Key = key;
      background2Key = null;
    }

    backgroundLabel1.setInnerText(M.messages().infoPanelEmissionResultLabelBackground(type) + " "
        + (background1Key.equals(key) ? "" : background1Key.getSubstance().getName()));
    setBackgroundValue(backgroundValue1, emissionResultInfo.getBackgroundEmissionResults(), background1Key);

    UIObject.setVisible(backgroundDiv2, background2Key != null);
    if (background2Key != null) {
      backgroundLabel2.setInnerText(M.messages().infoPanelEmissionResultLabelBackground(type) + " " + background2Key.getSubstance().getName());
      setBackgroundValue(backgroundValue2, emissionResultInfo.getBackgroundEmissionResults(), background2Key);
    }
  }

  private void setBackgroundValue(final SpanElement element, final EmissionResults results, final EmissionResultKey key) {
    if (results.hasResult(key)) {
      element.setInnerText(FormatUtil.formatEmissionResultWithUnit(key.getEmissionResultType(), results.get(key), displaySettings));
    } else {
      element.setInnerText(EMPTY_VALUE);
    }
  }

  private void setTitles() {
    panel.setTitleText(M.messages().emissionResultType(key.getEmissionResultType()));
    if (isPriorityProjectUtilisation()) {
      calculatedTitle.setInnerText(M.messages().infoPanelEmissionResultUtilisationTitle());
      calculatedPercentageLabel1.setInnerText(M.messages().infoPanelEmissionResultUtilisationLabel());
    } else {
      calculatedTitle.setInnerText(M.messages().infoPanelEmissionResultLabelCalculatedSources(key.getEmissionResultType()));
    }
    comparisonLabel.setInnerText(M.messages().infoPanelEmissionResultLabelDifference(key.getEmissionResultType()));
  }

  private boolean isPriorityProjectUtilisation() {
    return place != null && place.getJobType() != null && place.getJobType() == JobType.PRIORITY_PROJECT_UTILISATION;
  }
}
