/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.BoundaryLayer;
import nl.overheid.aerius.geo.LayerFactory;
import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.NavigationWidget;
import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.geo.client.events.LayerChangeEvent.CHANGE;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.geo.HabitatTypeLayerWrapper;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.MarkerContainer;
import nl.overheid.aerius.wui.geo.SourceMapPanel;
import nl.overheid.aerius.wui.geo.search.MapSearchPanel;
import nl.overheid.aerius.wui.main.event.DevelopmentRulesEvent;
import nl.overheid.aerius.wui.main.event.ImportEvent;
import nl.overheid.aerius.wui.main.event.InformationPointChangeEvent;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.NotificationPanel;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.source.geo.ShippingRouteContainer;
import nl.overheid.aerius.wui.scenario.base.source.shipping.InlandRouteMarkerContainer;

/**
 * Special MapLayoutPanel for showing calculation results. It maintains a layer for each supported substance and listens to events with new map data.
 */
@Singleton
public class ScenarioBaseMapLayoutPanel extends MapLayoutPanel implements SourceMapPanel {
  private static final String HUNDRED_PROCENT = "100%";

  interface CalculatorMapLayoutPanelEventBinder extends EventBinder<ScenarioBaseMapLayoutPanel> {}

  private final CalculatorMapLayoutPanelEventBinder eventBinder = GWT.create(CalculatorMapLayoutPanelEventBinder.class);

  // Calculation result layer. *
  private final CalculationResultLayerWrapper calculationResultLayerWrapper;

  // Calculation marker layer
  private final CalculationMarkerLayerWrapper calculationMarkerLayerWrapper;

  // Calculation development rules layer
  private final CalculatorDevelopmentRuleLayerWrapper calculationDevelopmentRuleLayerWrapper;

  // Calculation marker layer
  private final HabitatTypeLayerWrapper habitatTypeLayerWrapper;

  // Info/source/calculation point marker layer *
  private final ScenarioMarkerLayerWrapper markersLayer;

  private final BoundaryLayer calculatorBoundaryLayer;

  final ShippingRouteContainer shippingRouteContainer;

  final InlandRouteMarkerContainer inlandRouteContainer;

  @Inject
  public ScenarioBaseMapLayoutPanel(final LayerFactory lf, final EventBus eventBus, final FetchGeometryServiceAsync service,
      final MapSearchPanel searchPanel, final ScenarioBaseAppContext<?, ?> appContext, final NotificationPanel notificationPanel,
      final CalculationResultLayerWrapper activeResultLayerWrapper, final CalculatorDevelopmentRuleLayerWrapper developmentRuleLayerWrapper,
      final HabitatTypeLayerWrapper activeHabitatTypeLayerWrapper, final CalculationMarkerLayerWrapper activeMarkerLayerWrapper,
      final HelpPopupController hpC) {
    super(lf, eventBus, appContext.getContext().getReceptorGridSettings());

    this.calculationResultLayerWrapper = activeResultLayerWrapper;
    this.calculationMarkerLayerWrapper = activeMarkerLayerWrapper;
    this.calculationDevelopmentRuleLayerWrapper = developmentRuleLayerWrapper;
    this.habitatTypeLayerWrapper = activeHabitatTypeLayerWrapper;

    calculationResultLayerWrapper.setMap(this);
    calculationMarkerLayerWrapper.setMap(this);
    //calculationDevelopmentRuleLayerWrapper.setMap(this); // skip the developmentResult do not add layer
    habitatTypeLayerWrapper.setMap(this);

    shippingRouteContainer = new ShippingRouteContainer();
    inlandRouteContainer = new InlandRouteMarkerContainer();
    setHeight(HUNDRED_PROCENT);
    setWidth(HUNDRED_PROCENT);
    setNavigationWidget(new NavigationWidget(this, hpC));
    markersLayer = new ScenarioMarkerLayerWrapper(this, "markers");
    calculatorBoundaryLayer = new BoundaryLayer(this, (String) appContext.getContext().getSetting(SharedConstantsEnum.CALCULATOR_BOUNDARY));

    searchPanel.addStyleName(R.css().mapSearchPanel());
    panel.add(searchPanel);
    panel.add(notificationPanel);

    eventBinder.bindEventHandlers(this, eventBus);

    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        setDefaultExtent();

        // Zoom twice twice to extent because the first call doesn't zoom for an unknown reason.
        // Possible a timing problem where actual map isn't finished loading. It is only a
        // problem when the map is created, further calls to zoomToExtent work correctly.
        // The current solution is an acceptable workaround.
        zoomToExtent();
        zoomToExtent();

        // Prepare and add the base layer
        LayerPreparationUtil.prepareLayer(service, appContext.getUserContext().getBaseLayer(), new AppAsyncCallback<LayerProps>() {
          @Override
          public void onSuccess(final LayerProps result) {
            addLayer(result);
            addLayer((MapLayer) calculatorBoundaryLayer);
          }
        });

        setVisible(calculatorBoundaryLayer, true);

        for (final LayerProps item : appContext.getUserContext().getLayers()) {
          LayerPreparationUtil.addLayerDeferred(service, ScenarioBaseMapLayoutPanel.this, item, eventBus);
        }

        searchPanel.setMap(ScenarioBaseMapLayoutPanel.this);
        markersLayer.attach();
      }
    });
  }

  @EventHandler
  protected void onImportEvent(final ImportEvent e) {
    // after import, ensure old results are hidden and layers are refreshed.
    setVisibleSubstanceLayers(true);
    refreshLayers();
  }

  @EventHandler
  protected void onInformationPointChange(final InformationPointChangeEvent event) {
    // noop, for override purposes
  }

  @Override
  public MapLayoutPanel getPanel() {
    return this;
  }

  @Override
  public MarkerContainer<EmissionSource> getInlandRouteContainer() {
    return inlandRouteContainer;
  }

  @Override
  public MarkerContainer<EmissionSource> getMartimeMooringRouteContainer() {
    return shippingRouteContainer;
  }

  public ScenarioMarkerLayerWrapper getMarkersLayer() {
    return markersLayer;
  }

  /**
   * Hides all substance layers.
   *
   * @param visible set visibility
   */
  public void setVisibleSubstanceLayers(final boolean visible) {
    getCalculationResultLayerWrapper().setVisible(visible);
    getCalculationMarkerLayerWrapper().setVisible(visible);
    getCalculationDevelopmentRuleLayerWrapper().setVisible(visible);
  }

  @EventHandler
  void onDevelopmentRuleEvent(final DevelopmentRulesEvent e) {
    getCalculationDevelopmentRuleLayerWrapper().updateDevelopmentRulesAndDraw(e);
    refreshLayers();
  }

  @Override
  public void setVisible(final MapLayer layer, final boolean visible) {
    super.setVisible(layer, visible);
    eventBus.fireEvent(new LayerChangeEvent(layer, visible ? CHANGE.ENABLED : CHANGE.DISABLED));
  }

  public CalculationResultLayerWrapper getCalculationResultLayerWrapper() {
    return calculationResultLayerWrapper;
  }

  public CalculationMarkerLayerWrapper getCalculationMarkerLayerWrapper() {
    return calculationMarkerLayerWrapper;
  }

  public CalculatorDevelopmentRuleLayerWrapper getCalculationDevelopmentRuleLayerWrapper() {
    return calculationDevelopmentRuleLayerWrapper;
  }

  public HabitatTypeLayerWrapper getHabitatTypeLayerWrapper() {
    return habitatTypeLayerWrapper;
  }

  /**
   * Hides all substance layers.
   *
   * @param visible set visibility
   */
  public void setVisibleHabitatLayer(final boolean visible) {
    getHabitatTypeLayerWrapper().setVisible(visible);
  }

}
