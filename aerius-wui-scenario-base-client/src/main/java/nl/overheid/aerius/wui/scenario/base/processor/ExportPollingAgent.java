/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.processor;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.export.ExportStatus;
import nl.overheid.aerius.shared.service.ExportServiceAsync;
import nl.overheid.aerius.wui.main.retrievers.PollingAgentImpl;

/**
 * Polling Agent for Export.
 */
public class ExportPollingAgent extends PollingAgentImpl<String, ExportStatus> {
  private final ExportServiceAsync service;

  /**
   * Constructor where the {@link ExportServiceAsync} is being injected.
   * @param service the service being injected
   */
  @Inject
  public ExportPollingAgent(final ExportServiceAsync service) {
    this.service = service;
  }

  @Override
  protected void callService(final String key, final AsyncCallback<ExportStatus> resultCallback) {
    service.getExportResult(key, resultCallback);
  }
}
