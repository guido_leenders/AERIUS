/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.validation.constraints.NotNull;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Util class for Scenario.
 */
public final class ScenarioUtil {

  private ScenarioUtil() {}

  /**
   * Creates a {@link CalculatedScenario} from a {@link Scenario} using the information
   * in the place. If the place contains 2 id's it is a comparison calculation else it
   * is a single calculation.
   *
   * @param scenario Scenario to get sources from
   * @param place place containing id's of the sources to put into the new object
   * @return new Calculated Scenario object
   */
  public static CalculatedScenario toCalculatedScenario(final Scenario scenario, final ScenarioBasePlace place) {
    return CalculatedScenarioUtil.toCalculatedScenario(scenario, place.getSid1(), place.has2Situations() ? place.getSid2() : -1);
  }

  /**
   * Get the amount of duplicates (duplicate = same position) in the given 2 lists.
   * The amount of conflicts is based on the newPoints list:
   * If multiple points in original list conflict with 1 point in the newpoints list, that will be counted as multiple.
   * Other way around, 1 point in original list conflicts with multiple in newpoints: that will be counted as one.
   *
   * @param points The first point list.
   * @param newPoints The second point list.
   * @return amount of duplicates
   */
  public static int getAmountOfDuplicates(final List<? extends Point> points, final List<? extends Point> newPoints) {
    int counter = 0;
    if (points != null && newPoints != null) {
      for (final Point original : points) {
        for (final Point newSource : newPoints) {
          if (newSource.getX() == original.getX() && newSource.getY() == original.getY()) {
            counter++;
            break;
          }
        }
      }
    }
    return counter;
  }

  /**
   * Get the list of calculation points with the deposition differences between the calculation points of 2 situations.
   * Difference = deposition situation 2 - deposition situation 1
   * @param scenario Scenario to get data from.
   * @param place The place to determine the points for.
   * @return The list of calculation points including the difference in deposition.
   */
  @NotNull
  public static List<AeriusResultPoint> getCalculatedCalculationPoints(final CalculatedScenario scenario, final ScenarioBasePlace place) {
    final List<AeriusResultPoint> list;
    if (place.getSituation() == Situation.COMPARISON) {
      list = getCalculatedCalculationPointsDifferences(scenario, place.getSid1(), place.getSid2());
    } else {
      list = getCalculationPoints(scenario, place.getActiveSituationId());
    }
    return list == null ? new ArrayList<AeriusResultPoint>() : list;
  }

  /**
   * Set the results for the current calculated scenario in given scenario.
   * @param scenario Scenario to set the data from.
   * @param calculationId The calculation ID to set the results for.
   * @param newResults The calculation points containing the results.
   */
  public static void setCalculatedCalculationPoints(final CalculatedScenario scenario, final int calculationId,
      final List<AeriusResultPoint> newResults) {
    final List<AeriusResultPoint> calculationPointList = scenario.getCalculationPointResultsList(calculationId);
    if (calculationPointList == null) {
      return; // if not list found the calculationid doesn't match the scenario, so no results should be updated.
    }
    //ensure the list in the calculated scenario has all custom calculation points
    //these are not set when a calculation is started.
    if (calculationPointList.isEmpty()) {
      for (final AeriusPoint point : scenario.getCalculationPoints()) {
        final AeriusResultPoint arp = new AeriusResultPoint(point.getId(), point.getPointType(), point.getX(), point.getY());
        arp.setLabel(point.getLabel());
        calculationPointList.add(arp);
      }
    }
    //ensure the right point has the right deposition.
    for (final AeriusResultPoint point : calculationPointList) {
      for (final AeriusResultPoint calcResult : newResults) {
        //can't test on ID (not guaranteed that the server will return same ID's for each object)
        //so test for coordinates (on an int level, the coordinates are set as an int anyway)
        //if there are 2 points at the exact same spot it'll overwrite, but that's ok, cuz they're on the same spot...
        if (calcResult.getPointType() == AeriusPointType.POINT && point.getId() == calcResult.getId()) {
          calcResult.setLabel(point.getLabel());
          for (final Entry<EmissionResultKey, Double> entry : calcResult.getEmissionResults().entrySet()) {
            point.setEmissionResult(entry.getKey(), calcResult.getEmissionResult(entry.getKey()));
          }
        }
      }
    }
  }

  /**
   * Get the list of calculation points, based on the original list and the results available in a calculated scenario.
   * @param userContext Scenario to get data from.
   * @param eslId The id of the emissionsourcelist to get the calculation point results for.
   * @return The calculated calculation point list (null if no results or no list could be found for the situation).
   */
  private static ArrayList<AeriusResultPoint> getCalculationPoints(final CalculatedScenario scenario, final int eslId) {
    final ArrayList<AeriusResultPoint> calculatedList;
    if (scenario != null && scenario.isInSync()) {
      calculatedList = scenario.getCalculationPointResultsList(scenario.getCalculationId(eslId));
    } else {
      calculatedList = null;
    }
    return calculatedList;
  }

  /**
   * Get the list of calculation points with the deposition differences between the calculation points of 2 situations.
   * Difference = deposition situation 2 - deposition situation 1
   * @param userContext Scenario to get data from.
   * @param eslId1 The id of situation 1.
   * @param eslId2 The id of situation 2.
   * @return The list of calculation points including the difference in deposition.
   */
  private static List<AeriusResultPoint> getCalculatedCalculationPointsDifferences(final CalculatedScenario scenario,
      final int eslId1, final int eslId2) {
    final List<AeriusResultPoint> list1 = getCalculationPoints(scenario, eslId1);
    final List<AeriusResultPoint> list2 = getCalculationPoints(scenario, eslId2);
    final List<AeriusResultPoint> diffList;
    if (list1 == null || list2 == null) {
      diffList = new ArrayList<AeriusResultPoint>();
    } else {
      diffList = getDifferences(list1, list2);
    }
    return diffList;
  }

  /**
   * Get the differences in deposition compared to another calculation point list.
   * Difference is: deposition of point in compareList - deposition of point in this list.
   *
   * Each point in return list is a copy of the original.
   * @param compareList the List to determine the differences from.
   * @return List of the differences in deposition.
   */
  private static List<AeriusResultPoint> getDifferences(final List<AeriusResultPoint> list1, final List<AeriusResultPoint> list2) {
    final List<AeriusResultPoint> diffList = new ArrayList<AeriusResultPoint>();
    for (final AeriusResultPoint point2 : list2) {
      for (final AeriusResultPoint point1 : list1) {
        if (point1.getId() == point2.getId()) {
          final AeriusResultPoint copy = point2.copy();
          copy.setId(point2.getId());
          for (final Entry<EmissionResultKey, Double> entry : point2.getEmissionResults().entrySet()) {
            copy.setEmissionResult(entry.getKey(), entry.getValue() - point1.getEmissionResult(entry.getKey()));
          }
          diffList.add(copy);
          break;
        }
      }
    }
    return diffList;
  }
}
