/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

/**
 * Place were the application starts.
 */
public class StartUpPlace extends ScenarioBasePlace {
  private static final String PLACE_CONST = "start";

  /**
   * Tokenizer for {@link StartUpPlace}.
   */
  @Prefix(PLACE_CONST)
  public static class Tokenizer extends ScenarioBasePlace.Tokenizer<StartUpPlace> implements PlaceTokenizer<StartUpPlace> {
    @Override
    protected StartUpPlace createPlace() {
      return new StartUpPlace();
    }
  }

  public StartUpPlace() {
    this(null);
  }

  public StartUpPlace(final Place place) {
    super(place);
  }

  @Override
  public ScenarioBasePlace copy() {
    return copyTo(new StartUpPlace());
  }
}
