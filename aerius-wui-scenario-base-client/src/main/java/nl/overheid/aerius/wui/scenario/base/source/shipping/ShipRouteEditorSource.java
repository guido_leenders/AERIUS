/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.source.shipping;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.geometry.MultiLineString;
import org.gwtopenmaps.openlayers.client.handler.PathHandler;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.editor.client.adapters.EditorSource;
import com.google.gwt.editor.client.adapters.SimpleEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.DrawFeatureInteractionLayer;
import nl.overheid.aerius.geo.DrawFeatureInteractionLayer.DrawFeatureInteractionLayerListener;
import nl.overheid.aerius.geo.LineUtil;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.ModifyFeatureInteractionLayer;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.sector.ShippingNode;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.ShippingRoute.ShippingRouteReference;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.util.FormatUtil;
import nl.overheid.aerius.wui.geo.DrawSourceStyleOptions;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.EditableListButtonItem;
import nl.overheid.aerius.wui.main.widget.ListButton;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.CancelEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.scenario.base.source.shipping.ShipRouteEditorSource.TransferRouteRowEditor;

/**
 * {@link EditorSource} for ship transfer routes. This class takes care of the
 * row entries in the routes list.
 */
class ShipRouteEditorSource extends EditorSource<TransferRouteRowEditor> implements DrawFeatureInteractionLayerListener {

  interface ShipRouteEditorSourceEventBinder extends EventBinder<ShipRouteEditorSource> {
  }

  private static final ShipRouteEditorSourceEventBinder EVENT_BINDER = GWT.create(ShipRouteEditorSourceEventBinder.class);

  interface TransferRouteDriver extends SimpleBeanEditorDriver<ShippingRoute, LeafValueEditor<ShippingRoute>> {
  }

  /**
   * Interface to get the user friendly route name in the input ui.
   */
  public interface ShipRouteName {

    /**
     * Returns a user friendly reference to the route id.
     * @param id id to display as route id
     * @return user friendly reference to the route
     */
    String getRouteName(Integer id);
  }

  public interface ShipRouteEditorHandler {
    void onAdd(ShippingRoute route);

    /**
     * Take an action after the route has been updated.
     * @param route The route that was updated.
     */
    void onUpdate(ShippingRoute route);

    /**
     * Called a request to remove a route is done. The method should check if the route is in use, and if so return false.
     * If the route is not in use or only used by the current selected one it should return true.
     * @param route The route to remove.
     */
    void onRemove(ShippingRoute route);

    /**
     * Called when the mode switched to starting drawing a new route.
     * @return returns the starting point
     */
    Point onStartDrawNewRoute();

    /**
     * Called when the user selects a route from the list or a new route is created.
     * @param route selected or new route
     */
    void onSelect(ShippingRoute route);

    ShippingRouteReference getRouteRef();

    void onCancel();
  }

  /**
   * Maritime route name.
   */
  public static final ShipRouteName MARITIME_SHIP_ROUTENAME = new ShipRouteName() {
    @Override
    public String getRouteName(final Integer id) {
      return id == null ? M.messages().shipMaritimeRouteSelect()
          : M.messages().shipMaritimeRouteName(FormatUtil.formatAlphabeticalExactUpperCase(id));
    }
  };

  /**
   * Inland Waters route name.
   */
  public static final ShipRouteName INLAND_WATERS_SHIP_ROUTE_NAME = new ShipRouteName() {
    @Override
    public String getRouteName(final Integer id) {
      return id == null ? M.messages().shipInlandRouteSelect()
          : M.messages().shipRouteName(FormatUtil.formatAlphabeticalExactUpperCase(id));
    }
  };

  /**
   * Row editor for a single shipping transfer route.
   */
  static class TransferRouteRowEditor extends EditableListButtonItem implements LeafValueEditor<ShippingRoute> {

    private final ShipRouteName shipRouteName;

    public TransferRouteRowEditor(final ShipRouteName shipRouteName) {
      this.shipRouteName = shipRouteName;
      addStyleName(R.css().shipRouteListItem());
    }

    private ShippingRoute value;

    @Override
    public void setValue(final ShippingRoute route) {
      this.value = route;
      setText(getRouteName());
    }

    @Override
    public ShippingRoute getValue() {
      return value;
    }

    @Override
    protected void onEnsureDebugId(final String baseID) {
      super.onEnsureDebugId(baseID);
      ensureDebugId(TestID.BUTTON_SHIPPING_EXISTINGROUTE + "-" + getRouteName());
      ensureDebugIdDeleteButton(TestID.BUTTON_SHIPPING_DELETEROUTE + "-" + getRouteName());
      ensureDebugIdEditButton(TestID.BUTTON_SHIPPING_EDITROUTE + "-" + getRouteName());
    }

    private String getRouteName() {
      return value == null ? "" : shipRouteName.getRouteName(value.getId());
    }
  }

  private final FlowPanel panel;
  private final ModifyFeatureInteractionLayer mfInteractionLayer;
  private final TransferRouteDriver transferRouteDriver = GWT.create(TransferRouteDriver.class);
  private final ConfirmCancelDialog<ShippingRoute, LeafValueEditor<ShippingRoute>> ccd =
      new ConfirmCancelDialog<ShippingRoute, LeafValueEditor<ShippingRoute>>(
          transferRouteDriver, M.messages().shipRouteEditConfirm(), M.messages().cancelButton());
  private VectorFeature feature;
  private final DrawFeatureInteractionLayer drawPathLayer;
  private ShipRouteEditorHandler handler;
  private final ShipRouteName shipRouteName;
  private final SelectionHandler<Integer> listSelectionHandler = new SelectionHandler<Integer>() {
    @Override
    public void onSelection(final SelectionEvent<Integer> event) {
      final Integer selectedItem = event.getSelectedItem();

      if ((selectedItem == null) || (selectedItem == 0)) {
        //first call onStartDrawNewRoute as it could change the startpoint
        startDrawingNewRoute(handler.onStartDrawNewRoute());
      } else {
        handler.onSelect(((TransferRouteRowEditor) panel.getWidget(selectedItem)).getValue());
      }
    }
  };

  private final ListButton listButton;

  public ShipRouteEditorSource(final EventBus eventBus, final MapLayoutPanel map, final ListButton listButton,
      final ArrayList<ShippingNode> shippingNodes, final ShipRouteName shipRouteName) {
    this.listButton = listButton;
    this.shipRouteName = shipRouteName;
    drawPathLayer = new DrawFeatureInteractionLayer(map, new PathHandler(), DrawSourceStyleOptions.getElasticStyleMap(), this);
    final ArrayList<Point> shippingEndPoints = new ArrayList<Point>();
    shippingEndPoints.addAll(shippingNodes);
    drawPathLayer.attachOneToEnd(shippingEndPoints);
    panel = listButton.getListPanel();
    if (panel.getWidgetCount() == 0) {
      final Label newRouteLabel = new Label(M.messages().shipNew());
      newRouteLabel.ensureDebugId(TestID.BUTTON_SHIPPING_NEWROUTE);
      newRouteLabel.addStyleName(R.css().shipRouteListItem());
      listButton.getListPanel().add(newRouteLabel);
    }
    listButton.addSelectionHandler(listSelectionHandler);
    transferRouteDriver.initialize(SimpleEditor.<ShippingRoute>of());
    mfInteractionLayer = new ModifyFeatureInteractionLayer(map, null, null, null);
    ccd.setText(M.messages().shipRouteEditTitle());
    ccd.setModal(false);
    ccd.setGlassEnabled(false);
    ccd.addCancelHandler(new CancelEvent.Handler() {
      @Override
      public void onCancel(final CancelEvent event) {
        mfInteractionLayer.deactivate();
        //re-add the route to the global layer
        handler.onUpdate(transferRouteDriver.flush());
      }
    });
    ccd.addConfirmHandler(new ConfirmHandler<ShippingRoute>() {
      @Override
      public void onConfirm(final ConfirmEvent<ShippingRoute> event) {
        endEditingRoute(transferRouteDriver.flush());
      }
    });

    EVENT_BINDER.bindEventHandlers(this, eventBus);

    listButton.getListPanel().ensureDebugId(TestID.PANEL_SHIPPING_ROUTESLIST);
  }

  @Override
  public TransferRouteRowEditor create(final int index) {
    final TransferRouteRowEditor row = new TransferRouteRowEditor(shipRouteName);

    setIndex(row, index);
    row.addDeleteHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        handler.onRemove(row.getValue());
      }
    });
    row.addEditHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        startEditingRoute(row.getValue());
      }
    });
    return row;
  }

  public void setRouteChangedHandler(final ShipRouteEditorHandler handler) {
    this.handler = handler;
  }

  @Override
  public void dispose(final TransferRouteRowEditor subEditor) {
    panel.remove(subEditor);
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent event) {
    drawPathLayer.deactivate();
    mfInteractionLayer.asLayer().destroyFeatures();
    handler.onCancel();
  }

  /**
   * Called when a new route drawing finished by user.
   */
  @Override
  public void onDrawEnd(final Geometry geometry, final double measure, final double x, final double y, final Point endPoint) {
    drawPathLayer.deactivate();
    final ShippingRoute route = new ShippingRoute();
    route.setX(x);
    route.setY(y);
    route.setGeometry(new WKTGeometry(geometry.toString(), measure));
    route.setEndPoint(endPoint);
    handler.onAdd(route);
    handler.onSelect(route);
  }

  @Override
  public void setIndex(final TransferRouteRowEditor editor, final int index) {
    panel.insert(editor, index + 1);
  }

  public void setShippingNodeSearchDistance(final double shippingNodeSearchDistance) {
    drawPathLayer.setEndPointSearchDistance(shippingNodeSearchDistance);
  }

  /**
   * Call when an existing route is edited.
   * @param route to edit
   */
  private void startEditingRoute(final ShippingRoute route) {
    transferRouteDriver.edit(route);
    mfInteractionLayer.asLayer().destroyFeatures();
    feature = new VectorFeature(Geometry.fromWKT(route.getGeometry().getWKT()));
    mfInteractionLayer.modifyFeature(feature);
    ccd.setPopupPosition(listButton.getAbsoluteLeft(), listButton.getAbsoluteTop());
    ccd.show();
  }

  /**
   * Call when a new route is to be drawn.
   * @param point starting point
   */
  private void startDrawingNewRoute(final Point point) {
    drawPathLayer.asLayer().destroyFeatures();
    drawPathLayer.setStartPoint(point.getX(), point.getY());
    drawPathLayer.activate();
  }

  /**
   * Called when route editing is ended and changes should be stored.
   */
  private void endEditingRoute(final ShippingRoute route) {
    ccd.hide();
    mfInteractionLayer.deactivate();
    final MultiLineString line = MultiLineString.narrowToMultiLineString(feature.getGeometry().getJSObject());
    final org.gwtopenmaps.openlayers.client.geometry.Point point = LineUtil.middleOfMultiLineString(line);
    route.setX(point.getX());
    route.setY(point.getY());
    route.setGeometry(new WKTGeometry(feature.getGeometry().toString(), line.getLength()));
    handler.onUpdate(route);
  }

}
