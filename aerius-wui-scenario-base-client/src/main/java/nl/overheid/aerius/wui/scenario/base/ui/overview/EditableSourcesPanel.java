/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.ui.overview;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.context.ScenarioBaseUserContext;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.geo.MarkerItem;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.LabelVisibilityChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourcesPurgeEvent;
import nl.overheid.aerius.wui.main.event.SimpleGenericEvent.CHANGE;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable;
import nl.overheid.aerius.wui.main.widget.table.EditableDivTable.EditableDivTableMessages;
import nl.overheid.aerius.wui.main.widget.table.InteractiveDivTable;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.importer.ScenarioBaseImportController;

/**
 * Panel to display and edit emission sources list in a table wrapped by editing buttons.
 */
public class EditableSourcesPanel extends Composite implements SourcesPanel {
  final class EditableDivTableExtension extends EditableDivTable<EmissionSource> {
    private final ScenarioBaseImportController importController;

    EditableDivTableExtension(final InteractiveDivTable<EmissionSource, ?> divTable, final HelpPopupController helpPopupController,
        final EditableDivTableMessages messages, final ScenarioBaseImportController importController) {
      super(divTable, helpPopupController, messages);
      this.importController = importController;
    }

    @Override
    public Class<EmissionSource> getObjectClass() {
      return EmissionSource.class;
    }

    @Override
    public void handleCustomButton() {
      importController.showImportDialog();
    }
  }

  static final class EditableSourcesPanelMessages implements EditableDivTableMessages {
    private final HelpPopupController hpC;

    @Inject
    EditableSourcesPanelMessages(final HelpPopupController hpC) {
      this.hpC = hpC;
    }

    @Override
    public String add() {
      return M.messages().calculatorAddSource();
    }

    @Override
    public HelpInfo editHelp() {
      return hpC.tt().ttOverviewButtonEdit();
    }

    @Override
    public HelpInfo copyHelp() {
      return hpC.tt().ttOverviewButtonCopy();
    }

    @Override
    public HelpInfo deleteHelp() {
      return hpC.tt().ttOverviewButtonDelete();
    }

    @Override
    public HelpInfo addHelp() {
      return hpC.tt().ttOverviewButtonAddSource();
    }

    @Override
    public String customButtonText() {
      return M.messages().calculatorImportSource();
    }

    @Override
    public HelpInfo customButtonHelp() {
      return null;
    }
  }

  interface EditableSourcesPanelUiBinder extends UiBinder<Widget, EditableSourcesPanel> {
  }
  interface EditableSourcesPanelEventBinder extends EventBinder<EditableSourcesPanel> {
  }

  private final EditableSourcesPanelEventBinder eventBinder = GWT.create(EditableSourcesPanelEventBinder.class);
  private static final EditableSourcesPanelUiBinder UI_BINDER = GWT.create(EditableSourcesPanelUiBinder.class);

  @UiField CheckBox toggleButton;
  private final SpanElement toggleText = Document.get().createSpanElement();

  private final SourcesDivTable sourcesDivTable;
  @UiField Button purgeButton;
  @UiField(provided = true) EditableDivTable<EmissionSource> editableDivTable;
  @UiField DeckPanel switchPanel;
  @UiField Label labelSubstance;
  @UiField Label totalEmission;
  @UiField Label labelNOX;
  @UiField Label labelNH3;
  @UiField Label totalNOXEmission;
  @UiField Label totalNH3Emission;

  private final EventBus eventBus;

  private int emissionSourceListId;

  private final ScenarioBaseUserContext userContext;

  private final ConfirmCancelDialog<Void, ?> confirmCancelDialog;
  private final ConfirmHandler<Void> confirmHandler;

  @Inject
  public EditableSourcesPanel(final HelpPopupController hpC, final SourcesDivTable sourcesCellTable,
      final ScenarioBaseImportController importController, final EventBus eventBus, final ScenarioBaseAppContext<?, ?> appContext,
      final EditableSourcesPanelMessages messages) {
    this.sourcesDivTable = sourcesCellTable;
    this.eventBus = eventBus;
    this.userContext = appContext.getUserContext();
    editableDivTable = new EditableDivTableExtension(sourcesCellTable.asDataTable(), hpC, messages, importController);
    editableDivTable.setEventBus(eventBus);
    eventBinder.bindEventHandlers(this, eventBus);
    initWidget(UI_BINDER.createAndBindUi(this));

    confirmCancelDialog =
        new ConfirmCancelDialog<>(M.messages().okButton(), M.messages().cancelButton());
    confirmCancelDialog.setText(M.messages().calculationListEmissionSourcePurgeAllConfirm());
    confirmHandler = new ConfirmHandler<Void>() {
      @Override
      public void onConfirm(final ConfirmEvent<Void> event) {
        userContext.getSources(emissionSourceListId).clear();
        eventBus.fireEvent(new EmissionSourcesPurgeEvent(emissionSourceListId));
        confirmCancelDialog.hide();
      }
    };
    confirmCancelDialog.addConfirmHandler(confirmHandler);

    toggleButton.getElement().appendChild(toggleText);
    toggleText.setInnerHTML(M.messages().labelOff());

    labelNOX.setText(Substance.NOX.getName());
    labelNH3.setText(Substance.NH3.getName());

    purgeButton.ensureDebugId(TestID.BUTTON_PURGE_SOURCES);
    toggleButton.ensureDebugId(TestID.TOGGLE_SOURCE_LABELS);
    editableDivTable.ensureDebugId(TestID.DIVTABLE_SOURCE);
    labelSubstance.ensureDebugId(TestID.DIV_SUBSTANCE_EMISSION);
    totalEmission.ensureDebugId(TestID.DIV_TOTAL_EMISSION);
    totalNOXEmission.ensureDebugId(TestID.DIV_TOTAL_NOX_EMISSION);
    totalNH3Emission.ensureDebugId(TestID.DIV_TOTAL_NH3_EMISSION);
  }

  @UiHandler("toggleButton")
  void onLabelButtonClick(final ClickEvent e) {
    toggleText.setInnerHTML(toggleButton.getValue() ? M.messages().labelOn() : M.messages().labelOff());
    if (eventBus != null) {
      eventBus.fireEvent(new LabelVisibilityChangeEvent(toggleButton.getValue(), MarkerItem.TYPE.EMISSION_SOURCE));
    }
  }

  @UiHandler("purgeButton")
  void onPurgeButtonClick(final ClickEvent e) {
    confirmCancelDialog.center();
  }

  /**
   * set the data for the source.
   *
   * @param sources list of sources.
   * @param key current active key.
   * @param enableAdd allow add button.
   */
  @Override
  public void setData(final EmissionSourceList sources, final EmissionValueKey key, final boolean enableAdd) {
    emissionSourceListId = sources.getId();
    sourcesDivTable.asDataTable().setRowData(sources, true);
    editableDivTable.updateTableEnabled();
    editableDivTable.setAddingEnabled(enableAdd);
  }

  /**
   * set the total for all deposition.
   *
   * @param total value.
   */
  @Override
  public void setTotalEmission(final double total, final Substance substance) {
    switchPanel.showWidget(0);
    labelSubstance.setText(substance.getName());
    totalEmission.setText(FormatUtil.formatEmissionWithUnitSmart(total));
  }

  /**
   * set the total and add the substance type.
   *
   * @param noxTotal noxTotal.
   * @param nh3Total nh3Total
   */
  @Override
  public void setTotalEmissionDouble(final double noxTotal, final double nh3Total) {
    switchPanel.showWidget(1);
    totalNOXEmission.setText(FormatUtil.formatEmissionWithUnitSmart(noxTotal));
    totalNH3Emission.setText(FormatUtil.formatEmissionWithUnitSmart(nh3Total));
  }

  /**
   * Set the maximum height of the table.
   *
   * @param maxHeight in pixel
   */
  public void setMaxHeight(final int maxHeight) {
    editableDivTable.setMaxHeight(maxHeight);
  }

  /**
   * Handler for selected row during adding and update the row.
   * The Selected row are highlighted and stayed focused even after
   * the event ended.
   * @param event
   */
  @EventHandler
  void onEmissionSourceChange(final EmissionSourceChangeEvent event) {
    final EmissionSource selectedEmissionSource = event.getValue();
    if (CHANGE.UPDATE == event.getChange()) {
      highlightSourceAfterEditEvent(selectedEmissionSource);
    }
  }

  /**
   * Edit an emission source and highlight it.
   * @param selectedEmissionSource
   */
  private void highlightSourceAfterEditEvent(final EmissionSource selectedEmissionSource) {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        sourcesDivTable.asDataTable().setSelectedItem(selectedEmissionSource, true);
        sourcesDivTable.asDataTable().scrollIntoView(selectedEmissionSource);
      }
    });
  }
}
