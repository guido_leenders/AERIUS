/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.base.place.TestScenarioBasePlace;

/**
 * Test for {@link ScenarioUtil} class.
 */
public class ScenarioUtilTest {

  private static final EmissionResultKey NH3_KEY = EmissionResultKey.NH3_DEPOSITION;

  @Test
  public void testTetAmountOfDuplicates() {
    assertEquals("point list 1 is null, ", 0, ScenarioUtil.getAmountOfDuplicates(null, new ArrayList<Point>()));
    assertEquals("point list 2 is null, ", 0, ScenarioUtil.getAmountOfDuplicates(new ArrayList<Point>(), null));
    assertEquals("both lists are null, ", 0, ScenarioUtil.getAmountOfDuplicates(null, null));
    final Point a = new AeriusPoint(1, 2);
    final Point b1 = new AeriusPoint(2, 4);
    final Point b2 = new AeriusPoint(2, 4);
    final Point c = new AeriusPoint(2, 3);
    final Point d = new AeriusPoint(1, 3);
    final Point e = new AeriusPoint(5, 5);
    final List<Point> l1 = new ArrayList<Point>();
    final List<Point> l2 = new ArrayList<Point>();
    l1.add(a);
    l1.add(b1);
    l1.add(e);
    l2.add(c);
    l2.add(b2);
    l2.add(d);
    assertEquals("Duplicate check, ", 1, ScenarioUtil.getAmountOfDuplicates(l1, l2));
  }

  /**
   * Test if results are correctly added to both a incorrect calculation id and a correct calculation id.
   */
  @Test
  public void testCalculatedCalculationPoints() {
    final CalculatedSingle cs = createCalculatedSingle();
    cs.setCalculationId(1);
    cs.getCalculationPoints().addAll(createCalculationPointsResults(5));
    final List<AeriusResultPoint> resultPoints = createCalculationPointsResults(5);
    //Here we try to add store results for invalid calcid. This should not succeed
    ScenarioUtil.setCalculatedCalculationPoints(cs, 2, resultPoints);
    assertTrue("Test if results is empty", cs.getCalculation().getCalculationPoints().isEmpty());
    ScenarioUtil.setCalculatedCalculationPoints(cs, 1, resultPoints);
    final ScenarioBasePlace cp = new TestScenarioBasePlace(1, ScenarioBasePlace.NO_SID2, Situation.SITUATION_ONE);
    assertEquals("Test if results are set", 20.0,
        ScenarioUtil.getCalculatedCalculationPoints(cs, cp).get(1).getEmissionResult(NH3_KEY), 0.01);
  }

  /**
   * Bug triggered with following steps:
   * 1) Create new list
   * 2) Calculate
   * 3) Remove point in list
   * 4) Calculate again
   * 5) Add 2 new points, on last add exception triggered.
   */
  @Test
  public void testCalcultedCalculationPointBug() {
    //Create new list
    final CalculatedSingle cs = createCalculatedSingle();
    cs.getCalculationPoints().addAll(createCalculationPointsResults(5));
    cs.setCalculationId(1);
    // Calculate
    final List<AeriusResultPoint> resultPoints = createCalculationPointsResults(5);
    ScenarioUtil.setCalculatedCalculationPoints(cs, 1, resultPoints);
    // Remove point
    cs.getCalculationPoints().remove(3);
    // Calculate again
    final List<AeriusResultPoint> resultPoints2 = createCalculationPointsResults(5);
    // remove point from result list
    resultPoints2.remove(3);
    ScenarioUtil.setCalculatedCalculationPoints(cs, 1, resultPoints2);
    // add new points
    cs.getCalculationPoints().add(new AeriusPoint(0, AeriusPointType.POINT, 300, 300));
    cs.getCalculationPoints().add(new AeriusPoint(0, AeriusPointType.POINT, 400, 400));
    // simply getting the list triggers the bug.
    final ScenarioBasePlace cp = new TestScenarioBasePlace(1, ScenarioBasePlace.NO_SID2, Situation.SITUATION_ONE);
    final List<AeriusResultPoint> ucpList = ScenarioUtil.getCalculatedCalculationPoints(cs, cp);
    assertFalse("List should not be empty", ucpList.isEmpty());
  }

  private CalculatedSingle createCalculatedSingle() {
    final CalculatedSingle cSingle = new CalculatedSingle();
    final EmissionSourceList esl = new EmissionSourceList();
    esl.setId(1);
    cSingle.setCalculationId(1);
    cSingle.setSources(esl);
    cSingle.certify();
    return cSingle;
  }

  private List<AeriusResultPoint> createCalculationPointsResults(final int size) {
    final List<AeriusResultPoint> calcPoints = new ArrayList<AeriusResultPoint>();
    for (int i = 1; i <= 5; i++) {
      final AeriusResultPoint cp = new AeriusResultPoint(i, AeriusPointType.POINT, 10 * i, 20 * i);
      cp.setLabel("L" + i);
      cp.setEmissionResult(NH3_KEY, i * 10.0);
      calcPoints.add(cp);
    }
    return calcPoints;

  }
}
