/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.place;

import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.place.Situation;

/**
 * Test implementation of {@link CalculatorPlace}. Only used in tests, because the base class {@link CalculatorPlace} isn't a used place, but only
 * subclasses with actual place should be used.
 */
public final class TestScenarioBasePlace extends ScenarioBasePlace {

  public static class TestCalculatorPlaceTokenizer extends Tokenizer<TestScenarioBasePlace> {

    @Override
    protected TestScenarioBasePlace createPlace() {
      return new TestScenarioBasePlace();
    }
  }

  public TestScenarioBasePlace() {
    super();
  }

  public TestScenarioBasePlace(final ScenarioBasePlace place) {
    super(place);
  }

  public TestScenarioBasePlace(final int sId1, final int sId2, final Situation situation) {
    setsId1(sId1);
    setsId2(sId2);
    setSituation(situation);
  }

  public TestScenarioBasePlace(final int sId1, final int sId2, final Situation situation, final ScenarioBaseTheme theme) {
    this(sId1, sId2, situation);
    setTheme(theme);
  }

  public TestScenarioBasePlace(final int sId1, final int sId2, final Situation situation, final String jobKey, final JobType jobType) {
    this(sId1, sId2, situation);
    setJobKey(jobKey);
    setJobType(jobType);
  }

  @Override
  public ScenarioBasePlace copy() {
    return this;
  }
}
