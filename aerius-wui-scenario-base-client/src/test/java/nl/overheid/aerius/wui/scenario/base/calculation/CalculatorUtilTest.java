/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.base.calculation;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;

/**
 * Test for {@link CalculatorUtil} class.
 */
public class CalculatorUtilTest {

  @Test
  public void testCorrectSubstancePM10() {
    final ScenarioBaseTheme themeNature = ScenarioBaseTheme.NATURE_AREAS;
    final EmissionValueKey pm10 = new EmissionValueKey(Substance.PM10);
    final EmissionValueKey other = new EmissionValueKey(Substance.NH3);
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(new EmissionSourceList());
    Assert.assertNull("Test null if pm10 and no emission", CalculatorUtil.dectectSubstance(pm10, scenario, themeNature));
    Assert.assertNull("Test null if other and no emission", CalculatorUtil.dectectSubstance(other, scenario, themeNature));
    final GenericEmissionSource es = new GenericEmissionSource();
    es.setEmission(pm10, 20);
    scenario.getSources().add(es);
    final ScenarioBaseTheme themeAir = ScenarioBaseTheme.AIR_QUALITY;
    Assert.assertEquals("Test if pm10 and emission", Substance.PM10, CalculatorUtil.dectectSubstance(pm10, scenario, themeAir));
    Assert.assertEquals("Test if pm10 and other", Substance.PM10, CalculatorUtil.dectectSubstance(other, scenario, themeAir));
  }

  @Test
  public void testCorrectSubstanceNH3NOx() {
    final ScenarioBaseTheme themeNature = ScenarioBaseTheme.NATURE_AREAS;
    final EmissionValueKey nh3 = new EmissionValueKey(Substance.NH3);
    final EmissionValueKey nox = new EmissionValueKey(Substance.NOX);
    final EmissionValueKey other = new EmissionValueKey(Substance.PM10);
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(new EmissionSourceList());
    Assert.assertNull("Test null if nh3 and no emission", CalculatorUtil.dectectSubstance(nh3, scenario, themeNature));
    Assert.assertNull("Test null if other and no emission", CalculatorUtil.dectectSubstance(other, scenario, themeNature));
    final GenericEmissionSource es = new GenericEmissionSource();
    es.setEmission(nh3, 20);
    scenario.getSources().add(es);
    Assert.assertEquals("Test if nh3 and emission", Substance.NH3, CalculatorUtil.dectectSubstance(nh3, scenario, themeNature));
    Assert.assertEquals("Test if nh3 and other.", Substance.NH3, CalculatorUtil.dectectSubstance(other, scenario, themeNature));
    es.setEmission(nox, 20);
    Assert.assertEquals("Test if nh3+nox and emission", Substance.NOXNH3, CalculatorUtil.dectectSubstance(nh3, scenario, themeNature));
    Assert.assertEquals("Test if nh3+nox and emission", Substance.NOXNH3, CalculatorUtil.dectectSubstance(other, scenario, themeNature));
  }
}
