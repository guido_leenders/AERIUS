/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register.export;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData.PriorityProjectExportType;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RequestResultFileExporter}.
 */
public class RequestResultFileExporterTest extends BaseDBTest {

  private static final PriorityProjectKey DEFAULT_PROJECT_KEY = new PriorityProjectKey(null, null);
  private static final String TEST_SUB_PROJECT_REFERENCE = "pdp_Zeeland_12_8200";
  private static final String TEST_PRIORITY_PROJECT_DOSSIER_ID = "pp_Friesland_4_3111";
  private static final String TEST_PRIORITY_PROJECT_AUTHORITY = "Onbekend";
  private static final String TEST_PRIORITY_PROJECT_REFERENCE = "pp_Friesland_4_3111";
  private static final String TEST_UNKNOWN_REFERENCE = "SureToBeUnknown";
  private static final String TEST_UNKNOWN_DOSSIER_ID = "SureToBeUnknown";
  private static final String TEST_UNKNOWN_AUTHORITY = "SureToBeUnknown";

  private List<FileWrapper> generatedFiles;

  @Override
  @After
  public void tearDown() throws Exception {
    super.tearDown();
    cleanupGeneratedFiles();
  }

  @Test
  public void testGenerateSubPriorityAssignOverview() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    byte[] result;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      exporter.generateSubPriorityAssignOverview(baos, getRegConnection(),
          new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_SUB_PROJECT_REFERENCE));
      result = baos.toByteArray();
    }
    assertNotNull("There should be a result", result);
    assertNotEquals("Result length", 0, result.length);
  }

  @Test(expected = AeriusException.class)
  public void testGenerateSubPriorityAssignOverviewUnknown() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      exporter.generateSubPriorityAssignOverview(baos, getRegConnection(), new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_UNKNOWN_REFERENCE));
    }
  }

  @Test
  public void testGenerateSubPriorityFiles() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    generatedFiles = exporter.generatePrioritySubProjectFiles(getRegConnection(), new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_SUB_PROJECT_REFERENCE));
    assertNotNull("There should be files generated", generatedFiles);
    assertEquals("NR of generated files", 2, generatedFiles.size());
  }

  @Test
  public void testGeneratePriorityProjectExport() throws AeriusException, SQLException, IOException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = getExamplePriorityProjectData();
    byte[] result;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      exporter.generatePriorityProjectExport(baos, getRegConnection(), exportData);
      result = baos.toByteArray();
    }
    assertNotNull("There should be a result", result);
    assertNotEquals("Result length", 0, result.length);
  }

  @Test(expected = AeriusException.class)
  public void testGeneratePriorityProjectExportUnknown() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = new PriorityProjectExportData();
    exportData.setPriorityProjectKey(new PriorityProjectKey(TEST_UNKNOWN_REFERENCE, TEST_PRIORITY_PROJECT_AUTHORITY));
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      exporter.generatePriorityProjectExport(baos, getRegConnection(), exportData);
    }
  }

  @Test
  public void testGeneratePriorityFiles() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = getExamplePriorityProjectData();
    generatedFiles = exporter.generateFiles(getRegConnection(), exportData);
    assertNotNull("There should be files generated", generatedFiles);
    assertEquals("NR of generated files", 8, generatedFiles.size());
  }

  @Test
  public void testGenerateUtilisationPriorityFiles() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = getExampleUtilisationPriorityProjectData();
    generatedFiles = exporter.generateFiles(getRegConnection(), exportData);
    assertNotNull("There should be files generated", generatedFiles);
    assertEquals("NR of generated files", 4, generatedFiles.size());
  }

  @Test
  public void testGeneratePriorityFilesNoHits() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = getExamplePriorityProjectData();
    exportData.setExportTypes(new ArrayList<PriorityProjectExportType>());
    generatedFiles = exporter.generateFiles(getRegConnection(), exportData);
    assertEquals("NR of generated files", 0, generatedFiles.size());
  }

  @Test
  public void testGeneratePriorityFilesUnknownDossier() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = getExamplePriorityProjectData();
    exportData.getPriorityProjectKey().setDossierId(TEST_UNKNOWN_DOSSIER_ID);
    generatedFiles = exporter.generateFiles(getRegConnection(), exportData);
    assertEquals("NR of generated files", 0, generatedFiles.size());
  }

  @Test
  public void testGeneratePriorityFilesUnknownAuthority() throws AeriusException, IOException, SQLException {
    final RequestResultFileExporter exporter = new RequestResultFileExporter(getRegPMF());
    final PriorityProjectExportData exportData = getExamplePriorityProjectData();
    exportData.getPriorityProjectKey().setAuthorityCode(TEST_UNKNOWN_AUTHORITY);
    generatedFiles = exporter.generateFiles(getRegConnection(), exportData);
    assertEquals("NR of generated files", 0, generatedFiles.size());
  }

  private PriorityProjectExportData getExamplePriorityProjectData() {
    final PriorityProjectExportData exportData = new PriorityProjectExportData();
    exportData.setPriorityProjectKey(new PriorityProjectKey(TEST_PRIORITY_PROJECT_DOSSIER_ID, TEST_PRIORITY_PROJECT_AUTHORITY));
    exportData.getExportTypes().add(PriorityProjectExportType.GML_FILES);
    exportData.getExportTypes().add(PriorityProjectExportType.PDF_FILES);
    return exportData;
  }

  private PriorityProjectExportData getExampleUtilisationPriorityProjectData() throws SQLException {
    final PriorityProjectExportData exportData = new PriorityProjectExportData();
    exportData.setPriorityProjectReference(TEST_PRIORITY_PROJECT_REFERENCE);
    exportData.setPriorityProjectKey(PriorityProjectRepository.getPriorityProjectKey(getRegConnection(), exportData.getPriorityProjectReference()));
    exportData.getExportTypes().add(PriorityProjectExportType.UTILISATION_FILES);
    return exportData;
  }

  /**
   * Use this method if you want to have a look at the actual output of an export.
   * Not needed for the unittest, but it does come in handy when manually testing.
   */
  private void convenienceExport(final byte[] result) throws IOException {
    final File file = new File("ExporterTest.zip");
    try (FileOutputStream exportFileStream = new FileOutputStream(file, false)) {
      exportFileStream.write(result);
    }
  }

  private void cleanupGeneratedFiles() {
    if (generatedFiles != null) {
      for (final FileWrapper wrapper : generatedFiles) {
        wrapper.getFile().delete();
      }
    }
  }

}
