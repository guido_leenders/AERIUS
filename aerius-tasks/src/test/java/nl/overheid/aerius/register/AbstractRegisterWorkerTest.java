/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jfree.util.Log;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.register.PermitModifyRepository;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.db.register.RequestTestBase;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.importer.ImportInput;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.register.DossierMetaData;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PermitKey;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.reference.ReferenceUtil;
import nl.overheid.aerius.worker.ImportWorker;

/**
 *
 */
public abstract class AbstractRegisterWorkerTest extends RequestTestBase {

  private static final String PERMIT_CALCULATION_WORKER_TESTER = "PermitWorkerTester";

  @Test(timeout = 200000)
  public void testRunWorkerOn1Situation() throws Exception {
    final String filename = "AERIUS_bijlage_1_situation.pdf";
    final Permit permit = getExamplePermit(filename, UUID.randomUUID().toString());
    try {
      runTest(permit, filename);
      assertPermit(permit);
    } finally {
      assertNull("Delete should not fail", cleanUp(permit));
    }
  }

  @Test(timeout = 300000)
  public void testRunWorkerOn2Situations() throws Exception {
    final String filename = "AERIUS_bijlage_2_situations.pdf";
    final Permit permit = getExamplePermit(filename, UUID.randomUUID().toString());
    try {
      runTest(permit, filename);
      assertPermit(permit);
    } finally {
      assertNull("Delete should not fail", cleanUp(permit));
    }
  }

  private void runTest(final Permit permit, final String filename) throws Exception {
    cleanUp(permit);
    final Permit insertedPermit;
    try (Connection con = getRegConnection()) {
      final InsertRequestFile insertRequestFile = new InsertRequestFile();
      insertRequestFile.setFileContent(getPDFContent(filename));
      insertRequestFile.setFileFormat(FileFormat.PDF);
      insertRequestFile.setRequestFileType(RequestFileType.APPLICATION);
      insertedPermit = PermitModifyRepository.insertNewPermit(con, permit, getExampleUserProfile(), insertRequestFile);
    }
    runActualTest(insertedPermit);
  }

  protected abstract void runActualTest(final Permit permit) throws Exception;

  protected abstract void assertPermit(final Permit permit) throws SQLException;

  /**
   * Removes permit from database.
   * @param reference reference of the permit to remove
   */
  protected Exception cleanUp(final Permit permit) {
    Exception exception = null;
    try (Connection con = getRegConnection()) {
      if (PermitRepository.permitExists(con, permit.getPermitKey())) {
        deletePermit(con, permit.getPermitKey());
      }
      final PermitKey permitKeyForReference = PermitRepository.getPermitKey(con, permit.getReference());
      if (permitKeyForReference != null) {
        deletePermit(con, permitKeyForReference);
      }
    }  catch (final SQLException e) {
      exception = e;
    }  catch (final AeriusException e) {
      exception = e.getReason() == Reason.PERMIT_UNKNOWN ? null : e;
    }
    return exception;
  }

  private void deletePermit(final Connection con, final PermitKey permitKey) throws SQLException, AeriusException {
      final Permit deletePermit = new Permit();
      final DossierMetaData dossierMetaData = new DossierMetaData();
      deletePermit.setDossierMetaData(dossierMetaData);
      dossierMetaData.setDossierId(permitKey.getDossierId());
      final Authority authority = new Authority();
      authority.setCode(permitKey.getAuthorityCode());
      deletePermit.setAuthority(authority);
      dossierMetaData.setHandler(getExampleUserProfile());
      PermitModifyRepository.deletePermit(con, deletePermit, dossierMetaData.getHandler());
  }

  @Override
  protected UserProfile getExampleUserProfile() throws SQLException, AeriusException {
    UserProfile profile;
    try {
      profile = UserRepository.getUserProfileByName(getRegConnection(), PERMIT_CALCULATION_WORKER_TESTER);
    } catch (final AeriusException e) {
      if (e.getReason() == Reason.USER_DOES_NOT_EXIST) {
        // If the user doesn't exist, create the testuser.
        final AdminUserProfile createTestUser = new AdminUserProfile();
        createTestUser.setName(PERMIT_CALCULATION_WORKER_TESTER);
        createTestUser.setEmailAddress(PERMIT_CALCULATION_WORKER_TESTER + "@aerius.nl");
        createTestUser.setAuthority(TestDomain.getDefaultExampleAuthority());
        createTestUser.getRoles().add(UserRepository.getRole(getRegConnection(), TestDomain.USERROLE_REGISTER_SUPERUSER));
        try {
          UserRepository.createUser(getRegConnection(), createTestUser, "Tester");
        } catch (final Exception e2) {
          Log.error("User already exists, but we don't care", e2);
        }
        profile = UserRepository.getUserProfileByName(getRegConnection(), PERMIT_CALCULATION_WORKER_TESTER);
      } else {
        throw e;
      }
    }
    return profile;
  }

  protected Permit getExamplePermit(final String filename, final String dossierId) throws Exception {
    try (final InputStream inputStream = getClass().getResourceAsStream(filename)) {
      final ImportWorker importWorker = new ImportWorker(getCalcPMF());
      final ImportInput input = new ImportInput(filename, ImportType.determineByFilename(filename), IOUtils.toByteArray(inputStream));
      final ImportOutput output = importWorker.run(input, null, null);
      final Permit permit = RegisterImportUtil.convertToPermit(output);
      permit.getScenarioMetaData().setReference(ReferenceUtil.generatePermitReference());
      permit.getDossierMetaData().setDossierId(dossierId);
      permit.getDossierMetaData().setHandler(getExampleUserProfile());
      permit.setAuthority(TestDomain.getDefaultExampleAuthority());
      return permit;
    }
  }

  protected byte[] getPDFContent(final String filename) throws IOException {
    final File file = new File(getClass().getResource(filename).getFile());
    return FileUtils.readFileToByteArray(file);
  }

}
