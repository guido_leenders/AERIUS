/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.EnumSet;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;

import com.lowagie.text.FontFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.db.register.NoticeRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.mock.MockCloseableHttpClientUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.DownloadInfo;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.melding.BeneficiaryInformation;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.domain.melding.OrganisationInformation;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.worker.BaseDBWorkerTest;
import nl.overheid.aerius.worker.MailWorkerTestHelper;
import nl.overheid.aerius.worker.MeldingWorkerFactory;

/**
 * Test class for MeldingWorker.
 */
public class MeldingWorkerTest extends BaseDBWorkerTest<MeldingWorker> {

  private static final String TEST_MELDING_GML = "AERIUS_test_melding.gml";
  private static final String TEST_MELDING_ATTACHMENT = "melding_test_attachment.zip";

  private static final String TRUNCATE_DEVELOPMENT_SPACES =
      "TRUNCATE TABLE development_spaces";

  private static final String UPDATE_PERMIT_THRESHOLD_VALUES =
      "UPDATE permit_threshold_values SET VALUE = 1";

  private static final int NATURE_AREA_ID = 83; // Botshol
  private static final double CURRENT_THRESHOLD = 0.00499999;
  private static final String LOWER_PERMIT_THRESHOLD_VALUE_BOTSHOL =
      "UPDATE permit_threshold_values SET VALUE = " + CURRENT_THRESHOLD + " WHERE assessment_area_id = " + NATURE_AREA_ID;

  private static final double DEPOSITION_START_VALUE = 0.2;
  private static final double RESULT_DECREMENT_FACTOR = 0.9;

  private static CalculatorBuildDirector calculatorBuildDirector;

  private final MailWorkerTestHelper mailHelper = new MailWorkerTestHelper();

  private Reason errorReason;

  @Override
  @Before
  public void setUp() throws Exception {
    // We need to set autoCommit to true, because the worker does a lot of database work even when
    // exceptions occur, and without this it would try to do it in an aborted transaction.
    // Thus, this unit test should revert all the work the worker does on tearDown (unit test
    // database state should never change).
    getRegPMF().setAutoCommit(true);
    super.setUp();
    factory.setDepostionStartValue(DEPOSITION_START_VALUE);
    factory.setResultDecrementFactor(RESULT_DECREMENT_FACTOR);
    prepareDB();
    FontFactory.registerDirectory(MeldingWorkerTest.class.getResource("/fonts/").getFile());
  }

  @Override
  protected MeldingWorker createWorker() throws IOException, SQLException {
    calculatorBuildDirector = new CalculatorBuildDirector(getRegPMF(), factory);
    return new MeldingWorker(calculatorBuildDirector, getRegPMF(), new MeldingWorkerFactory().createConfiguration(properties), factory) {
      @Override
      protected boolean sendMail(final MailMessageData mailMessageData) {
        // No mail is send, just return true.
        mailHelper.sendMail(mailMessageData);
        return true;
      }

      @Override
      protected ImmediateExportData handleExport(final MeldingInputData inputData, final java.util.ArrayList<StringDataSource> backupAttachments,
          final String correlationId) throws Exception {
        // make it possible to test with throw errors
        if (errorReason == null) {
          return super.handleExport(inputData, backupAttachments, correlationId);
        } else {
          throw new AeriusException(errorReason);
        }
      };

      @Override
      protected CloseableHttpClient createHttpClient() {
        return MockCloseableHttpClientUtil.create();
      }
    };
  }

  @Test
  public void testHandle() throws Exception {
    //be sure to be within melding boundaries
    factory.setDepostionStartValue(0.2);
    final MeldingInputData data = getExampleInputData();
    final DownloadInfo result = worker.run(data, null).getDownloadInfo();
    assertNotNull("Result shouldn't be null", result);
    assertNotNull("Result URL wasn't null", result.getUrl());

    validateNoticeInDatabase(data.getReference(), true);

    mailHelper.validateMailsSend(2);
    //authority
    mailHelper.validateSubject(0, MessagesEnum.MELDING_AUTHORITY_MAIL_SUBJECT);
    mailHelper.validateContent(0, MessagesEnum.MELDING_AUTHORITY_MAIL_CONTENT);
    validateMailToAuthorities(mailHelper.getMail(0).getMailTo(), false);
    mailHelper.validateReplacementTokens(0, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.DOWNLOAD_LINK,
        ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.AUTHORITY);

    //user
    mailHelper.validateSubject(1, MessagesEnum.MELDING_REGISTERED_USER_MAIL_SUBJECT);
    mailHelper.validateContent(1, MessagesEnum.MELDING_REGISTERED_USER_MAIL_CONTENT);
    validateMailToUser(mailHelper.getMail(1).getMailTo());
    mailHelper.validateReplacementTokens(1, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK, ReplacementToken.MAIL_SIGNATURE, ReplacementToken.PROJECT_NAME,
        ReplacementToken.AERIUS_REFERENCE, ReplacementToken.AUTHORITY);
  }

  @Test
  public void testHandleWithDocuments() throws Exception {
    //be sure to be within melding boundaries
    factory.setDepostionStartValue(0.2);
    final MeldingInputData data = getExampleInputData();
    data.setZipAttachment(getActualZipAttachment());

    final ExportedData result = worker.run(data, null);
    assertNotNull("Result shouldn't be null", result);
    assertNotNull("Result download info", result.getDownloadInfo());
    assertNotNull("Result URL wasn't null", result.getDownloadInfo().getUrl());

    validateNoticeInDatabase(data.getReference(), true);

    mailHelper.validateMailsSend(4);

    //authority mail about melding
    mailHelper.validateSubject(0, MessagesEnum.MELDING_AUTHORITY_MAIL_SUBJECT);
    mailHelper.validateContent(0, MessagesEnum.MELDING_AUTHORITY_MAIL_CONTENT);
    validateMailToAuthorities(mailHelper.getMail(0).getMailTo(), false);
    mailHelper.validateReplacementTokens(0, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.DOWNLOAD_LINK,
        ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.AUTHORITY);

    //authority mail about documents for melding
    mailHelper.validateSubject(1, MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBJECT);
    mailHelper.validateContent(1, MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_CONTENT);
    validateMailToAuthorities(mailHelper.getMail(1).getMailTo(), true);
    mailHelper.validateReplacementTokens(1, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.MELDING_SUBSTANTIATION, ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.AUTHORITY,
        ReplacementToken.DOCUMENT_MAIL_NR, ReplacementToken.MAX_DOCUMENT_MAIL_NR);
    mailHelper.validateDBReplacementToken(1, ReplacementToken.MAIL_SUBSTANTIATION, MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBSTANTIATION);
    assertEquals("Nr of attachments for document mail", 1, mailHelper.getMail(1).getAttachments().size());

    //authority mail overview for documents
    mailHelper.validateSubject(2, MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_SUBJECT);
    mailHelper.validateContent(2, MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_CONTENT);
    validateMailToAuthorities(mailHelper.getMail(1).getMailTo(), true);
    mailHelper.validateReplacementTokens(2, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE,
        ReplacementToken.AUTHORITY);
    mailHelper.validateDBReplacementToken(2, ReplacementToken.SEND_OK, MessagesEnum.MELDING_ATTACHMENTS_SEND_STATUS_OK);
    mailHelper.validateDBReplacementToken(2, ReplacementToken.SEND_FAIL, MessagesEnum.MELDING_ATTACHMENTS_SEND_STATUS_FAIL);

    //user
    mailHelper.validateSubject(3, MessagesEnum.MELDING_REGISTERED_USER_MAIL_SUBJECT);
    mailHelper.validateContent(3, MessagesEnum.MELDING_REGISTERED_USER_MAIL_WITH_ATTACHMENTS_CONTENT);
    validateMailToUser(mailHelper.getMail(3).getMailTo());
    mailHelper.validateReplacementTokens(3, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK, ReplacementToken.MAIL_SIGNATURE, ReplacementToken.PROJECT_NAME,
        ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.AUTHORITY);
  }

  @Test
  public void testHandleOverLimit() throws Exception {
    // be sure to get an over-limit-exception
    lowerBotsholThreshold();
    factory.setDepostionStartValue(2.0);
    final MeldingInputData data = getExampleInputData();
    try {
      // remove current because with 2 same situations the delta deposition will be 0, while we want a value above permit threshold
      data.setCurrentGML(null);
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.MELDING_ABOVE_PERMIT_THRESHOLD, e.getReason());
    }

    validateNoticeInDatabase(data.getReference(), false);

    mailHelper.validateMailsSend(2);
    validateMailToUser(mailHelper.getMail(0).getMailTo());

    // test BG mail
    validateMailToAuthorities(mailHelper.getMail(1).getMailTo(), false);
  }

  @Test
  public void testHandleDoesNotFitMailContent() throws Exception {
    final MeldingInputData data = getExampleInputData();
    try {
      setErrorReason(Reason.MELDING_DOES_NOT_FIT);
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.MELDING_DOES_NOT_FIT, e.getReason());
    }

    validateNoticeInDatabase(data.getReference(), false);
    validateErrorMails(MessagesEnum.MELDING_DOES_NOT_FIT);
  }

  @Test
  public void testHandleAboveThresholdMailContent() throws Exception {
    final MeldingInputData data = getExampleInputData();
    try {
      setErrorReason(Reason.MELDING_ABOVE_PERMIT_THRESHOLD);
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.MELDING_ABOVE_PERMIT_THRESHOLD, e.getReason());
    }

    validateNoticeInDatabase(data.getReference(), false);
    validateErrorMails(MessagesEnum.MELDING_ABOVE_PERMIT_THRESHOLD);
  }

  @Test
  public void testHandleRejectMailContent() throws Exception {
    final MeldingInputData data = getExampleInputData();
    try {
      setErrorReason(Reason.MELDING_ABOVE_PERMIT_THRESHOLD);
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.MELDING_ABOVE_PERMIT_THRESHOLD, e.getReason());
    }

    validateNoticeInDatabase(data.getReference(), false);
    validateErrorMails(MessagesEnum.MELDING_ABOVE_PERMIT_THRESHOLD);
  }

  @Test
  public void testHandleSqlErrorMailContent() throws Exception {
    final MeldingInputData data = getExampleInputData();
    try {
      setErrorReason(Reason.SQL_ERROR);
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.SQL_ERROR, e.getReason());
    }

    validateNoticeInDatabase(data.getReference(), false);
    validateTechnicalIssueErrorMailContents(MessagesEnum.MELDING_TECHNICAL_ISSUES);
  }

  @Test
  public void testHandleGmlValidationFailedErrorMailContent() throws Exception {
    final MeldingInputData data = getExampleInputData();
    data.setProposedGML(""); // invalidate gml
    try {
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.GML_GENERIC_PARSE_ERROR, e.getReason());
    }
    //user mail only in this case
    mailHelper.validateMailsSend(1);
    validateNoticeInDatabase(data.getReference(), false);

    validateMailToUser(mailHelper.getMail(0).getMailTo());

    mailHelper.validateDBReplacementTokens(EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SUBJECT,
        ReplacementToken.REASON_NOT_REGISTERED_MELDING, ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING));
    mailHelper.validateDBReplacementToken(ReplacementToken.REASON_NOT_REGISTERED_MELDING, MessagesEnum.MELDING_TECHNICAL_ISSUES);

    mailHelper.validateReplacementTokens(0, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION, ReplacementToken.MAIL_SIGNATURE,
        ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE);
    mailHelper.validateSubject(0, MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT);
    mailHelper.validateContent(0, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_CONTENT);
    mailHelper.validateDBReplacementToken(0, ReplacementToken.MAIL_SUBJECT, MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT);
    mailHelper.validateDBReplacementToken(0, ReplacementToken.MAIL_CONTENT, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_CONTENT);
  }

  @Test
  public void testHandleGmlValidationFailedErrorMailContentWithDocuments() throws Exception {
    final MeldingInputData data = getExampleInputData();
    data.setZipAttachment(getActualZipAttachment());
    data.setProposedGML(""); // invalidate gml
    try {
      worker.run(data, null);
      fail("Expected exception at this point");
    } catch (final AeriusException e) {
      assertEquals("Expected reason", Reason.GML_GENERIC_PARSE_ERROR, e.getReason());
    }
    //user mail only in this case
    mailHelper.validateMailsSend(1);
    validateNoticeInDatabase(data.getReference(), false);

    validateMailToUser(mailHelper.getMail(0).getMailTo());

    mailHelper.validateDBReplacementTokens(EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SUBJECT,
        ReplacementToken.REASON_NOT_REGISTERED_MELDING, ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING));
    mailHelper.validateDBReplacementToken(ReplacementToken.REASON_NOT_REGISTERED_MELDING, MessagesEnum.MELDING_TECHNICAL_ISSUES);

    mailHelper.validateReplacementTokens(0, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION, ReplacementToken.MAIL_SIGNATURE,
        ReplacementToken.PROJECT_NAME, ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, ReplacementToken.AERIUS_REFERENCE);
    mailHelper.validateSubject(0, MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT);
    mailHelper.validateContent(0, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_ATTACHMENTSLIST_CONTENT);
    mailHelper.validateDBReplacementToken(0, ReplacementToken.MAIL_SUBJECT, MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT);
    mailHelper.validateDBReplacementToken(0, ReplacementToken.MAIL_CONTENT, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_ATTACHMENTSLIST_CONTENT);
  }

  private void validateErrorMails(final MessagesEnum expectedErrorMessage) {
    mailHelper.validateMailsSend(2);

    // user mail
    validateMailToUser(mailHelper.getMail(0).getMailTo());
    validateDefaultErrorMailContents(expectedErrorMessage);

    // BG mail
    validateMailToAuthorities(mailHelper.getMail(1).getMailTo(), false);
    validateDefaultErrorMailContents(expectedErrorMessage);
  }

  private void validateMailToUser(final MailTo mailToUser) {
    assertEquals("Reject mail should be send to 1 (user)", 1, mailToUser.getToAddresses().size());
    assertEquals("Reject mail should be CC-ed to 2 (user)", 2, mailToUser.getCcAddresses().size());
    assertTrue("Reject shouldn't be BCC send to anyone (user)", mailToUser.getBccAddresses().isEmpty());
  }

  private void validateMailToAuthorities(final MailTo mailToAuthority, final boolean hasBCC) {
    assertEquals("BG reject mail should be send to 1 (authority)", 1, mailToAuthority.getToAddresses().size());
    assertTrue("BG reject mail should not be CC-ed to anyone (authority)", mailToAuthority.getCcAddresses().isEmpty());
    if (hasBCC) {
      assertEquals("BG reject mail should be BCC-ed to 1 (central authority)", 1, mailToAuthority.getBccAddresses().size());
    } else {
      assertTrue("BG reject mail should not be BCC send to anyone (authority)", mailToAuthority.getBccAddresses().isEmpty());
    }
  }

  private void validateDefaultErrorMailContents(final MessagesEnum expectedErrorMessage) {
    mailHelper.validateSubject(0, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_SUBJECT);
    mailHelper.validateContent(0, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_CONTENT);
    //all mails DB tokens
    mailHelper.validateDBReplacementTokens(EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SUBJECT,
        ReplacementToken.REASON_NOT_REGISTERED_MELDING));
    //user mail
    mailHelper.validateReplacementTokens(0, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION,
        ReplacementToken.MAIL_SIGNATURE, ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE,
        ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING);
    validateCommonErrorMailContents(expectedErrorMessage);
  }

  private void validateTechnicalIssueErrorMailContents(final MessagesEnum expectedErrorMessage) {
    mailHelper.validateSubject(0, MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_ISSUE_USER_MAIL_SUBJECT);
    mailHelper.validateContent(0, MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_CONTENT);

    //all mails DB tokens
    mailHelper.validateDBReplacementTokens(0, EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SUBJECT,
        ReplacementToken.REASON_NOT_REGISTERED_MELDING,
        ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING));
    mailHelper.validateDBReplacementTokens(1, EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SUBJECT,
        ReplacementToken.REASON_NOT_REGISTERED_MELDING));
    //user mail
    mailHelper.validateReplacementTokens(0, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION,
        ReplacementToken.MAIL_SIGNATURE, ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE);

    validateCommonErrorMailContents(expectedErrorMessage);
  }

  private void validateCommonErrorMailContents(final MessagesEnum expectedErrorMessage) {
    //all mails DB tokens
    mailHelper.validateDBReplacementToken(ReplacementToken.REASON_NOT_REGISTERED_MELDING, expectedErrorMessage);

    //authority mail
    mailHelper.validateReplacementTokens(1, ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION,
        ReplacementToken.MAIL_SIGNATURE, ReplacementToken.PROJECT_NAME, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.AUTHORITY);
    mailHelper.validateSubject(1, MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_SUBJECT);
    mailHelper.validateContent(1, MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_CONTENT);
    mailHelper.validateDBReplacementToken(1, ReplacementToken.MAIL_SUBJECT, MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_SUBJECT);
    mailHelper.validateDBReplacementToken(1, ReplacementToken.MAIL_CONTENT, MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_CONTENT);
  }

  private void validateNoticeInDatabase(final String reference, final boolean expected) throws SQLException {
    final int noticeId = NoticeRepository.getNoticeIdByReference(getRegConnection(), reference);
    assertEquals("A notice with the right reference", expected, noticeId > 0);
  }

  public void setErrorReason(final Reason reason) {
    this.errorReason = reason;
  }

  private MeldingInputData getExampleInputData() throws SQLException, AeriusException, IOException {
    final MeldingInputData data = new MeldingInputData();
    data.setEmailAddress("aerius@example.com");
    data.setLocale(SharedConstants.LOCALE_NL);
    data.setReference("NoordHolland_" + new Date());
    final Scenario scenario = new Scenario();
    scenario.addSources(TestDomain.getExampleSourceList());
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setReference("SomeRef");
    metaData.setCorporation("SomeCorp");
    metaData.setDescription("SomeDescr");
    metaData.setProjectName("SomeProj");
    scenario.setMetaData(metaData);
    data.setProposedGML(getFileContent(TEST_MELDING_GML));
    data.setCurrentGML(getFileContent(TEST_MELDING_GML));
    data.setMeldingInformation(getExampleMeldingInformation());
    data.setZipAttachment(getZipAttachment());
    return data;
  }

  private MeldingInformation getExampleMeldingInformation() {
    final MeldingInformation info = new MeldingInformation();
    info.setBenefactorInformation(getExampleOrganisationInfo("benefactor"));
    info.setBeneficiaryInformation(getExampleBeneficiaryInfo("beneficiary"));
    info.setBenefactorEmailConfirmation(true);
    info.setExecutorInformation(getExampleOrganisationInfo("executor"));
    info.setExecutorEmailConfirmation(true);
    info.setSubstantiation("Something something bla bla");
    return info;
  }

  private byte[] getZipAttachment() {
    return null;
  }

  private byte[] getActualZipAttachment() throws IOException {
    final Path path = new File(getClass().getResource(TEST_MELDING_ATTACHMENT).getFile()).toPath();
    return Files.readAllBytes(path);
  }

  private OrganisationInformation getExampleOrganisationInfo(final String descr) {
    final OrganisationInformation info = new OrganisationInformation();
    fillOrganisationInfo(info, descr);
    return info;
  }

  private BeneficiaryInformation getExampleBeneficiaryInfo(final String descr) {
    final BeneficiaryInformation info = new BeneficiaryInformation();
    fillOrganisationInfo(info, descr);
    return info;
  }

  private void fillOrganisationInfo(final OrganisationInformation info, final String descr) {
    info.setOrganisationName("OrganizName" + descr);
    info.setOrganisationContactPerson("OrganizPerson" + descr);
    info.setOrganisationAddress("OrganizAddr" + descr);
    info.setOrganisationCity("OrganizCity" + descr);
    info.setOrganisationPostcode("OrganizPostcode" + descr);
    info.setOrganisationEmail("aerius_organization" + descr + "@example.com");
  }

  private String getFileContent(final String fileName) throws IOException {
    final File file = new File(getClass().getResource(fileName).getFile());
    final StringBuilder result = new StringBuilder();
    try (final BufferedReader br = new BufferedReader(new FileReader(file))) {
      String line;

      while ((line = br.readLine()) != null) {
        result.append(line);
      }
    }
    return result.toString();
  }

  private void prepareDB() throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement truncate = con.prepareStatement(TRUNCATE_DEVELOPMENT_SPACES);
        final PreparedStatement update = con.prepareStatement(UPDATE_PERMIT_THRESHOLD_VALUES)) {
      truncate.executeUpdate();
      update.executeUpdate();
    }
  }

  private void lowerBotsholThreshold() throws SQLException {
    try (final Connection con = getRegConnection();
        final PreparedStatement update = con.prepareStatement(LOWER_PERMIT_THRESHOLD_VALUE_BOTSHOL)) {
      update.executeUpdate();
    }
  }
}
