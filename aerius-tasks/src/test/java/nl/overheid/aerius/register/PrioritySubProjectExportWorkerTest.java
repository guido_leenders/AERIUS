/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.export.PrioritySubProjectExportData;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.BaseDBWorkerTest;
import nl.overheid.aerius.worker.CalculatorWorkerFactory;

/**
 *  Test class for {@link PrioritySubProjectExportWorker}.
 */
public class PrioritySubProjectExportWorkerTest extends BaseDBWorkerTest<PrioritySubProjectExportWorker> {

  private static final String TEST_REFERENCE = "SomeRef";
  private static final byte[] TEST_FILE_CONTENT = new byte[] { 0, 1, 0, };
  private static final PriorityProjectKey DEFAULT_PROJECT_KEY = new PriorityProjectKey(null, null);

  @Override
  protected PrioritySubProjectExportWorker createWorker() throws IOException {
    final CalculatorWorkerFactory workerFactory = new CalculatorWorkerFactory(WorkerType.REGISTER);
    return new PrioritySubProjectExportWorker(getRegPMF(), workerFactory.createConfiguration(properties), factory) {

      @Override
      protected boolean sendMail(final MailMessageData mailMessageData) {
        mailHelper.sendMail(mailMessageData);
        return true;
      }

      @Override
      protected ImmediateExportData generateZip(final PrioritySubProjectExportData inputData) throws SQLException, AeriusException, IOException {
        return new ImmediateExportData(TEST_FILE_CONTENT);
      }

    };
  }

  @Test
  public void testRun() throws Exception {
    final PrioritySubProjectExportData inputData = new PrioritySubProjectExportData();
    inputData.setPrioritySubProjectKey(new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_REFERENCE));
    final ExportedData exportedData = worker.run(inputData, null);
    assertExportedData(exportedData, inputData);
    assertEquals("Returned file content", TEST_FILE_CONTENT, exportedData.getFileContent());
    assertTrue("Returned file name should contain reference", exportedData.getFileName().contains(TEST_REFERENCE));
  }

  @Test
  public void testGenerateName() throws ParseException {
    final PrioritySubProjectExportData inputData = new PrioritySubProjectExportData();
    final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    final Date date = dateFormat.parse("16-09-2015");
    inputData.setCreationDate(date);
    inputData.setPrioritySubProjectKey(new PrioritySubProjectKey(DEFAULT_PROJECT_KEY, TEST_REFERENCE));
    assertEquals("Generated file name", "AERIUS_toetsing_20150916000000_SomeRef.zip", worker.getFileName(inputData));
  }

}
