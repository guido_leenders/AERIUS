/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;

import org.junit.After;
import org.junit.Before;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.OPSMockConnection;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfiguration;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.base.AbstractDBWorker;
import nl.overheid.aerius.worker.configuration.BrokerConfiguration;

/**
 * Base class for Worker tests.
 */
public abstract class BaseDBWorkerTest<T extends AbstractDBWorker<?, ?>> extends BaseDBTest {

  protected final MailWorkerTestHelper mailHelper = new MailWorkerTestHelper();

  protected CalculatorBuildDirector calculatorBuildDirector;

  protected T worker;
  protected TestBrokerConnectionFactory factory;
  protected Properties properties;

  @Override
  @Before
  public void setUp() throws Exception {
    properties = OSUtils.isWindows() ? PropertiesUtil.getFromPropertyFile("worker.properties")
        : PropertiesUtil.getFromPropertyFile("worker_test_linux.properties");
    factory = new TestBrokerConnectionFactory(EXECUTOR, new BrokerConfiguration(properties));
    worker = createWorker();
  }

  @Override
  @After
  public void tearDown() throws Exception {
    super.tearDown();
    factory.shutdown();
  }

  protected void initCalculator(final PMF pmf, final BrokerConnectionFactory factory) {
    try {
      if (calculatorBuildDirector == null) {
        calculatorBuildDirector = new CalculatorBuildDirector(pmf, factory);
      }
    } catch (final SQLException e) {
      throw new RuntimeException(e);
    }
  }

  protected abstract T createWorker() throws IOException, SQLException;

  protected void assertExportedData(final ExportedData taskResult, final ExportData inputData) {
    assertNotNull("Result", taskResult);
    assertEquals("Result download info",  inputData.isEmailUser(), taskResult.getDownloadInfo() != null);
    assertEquals("Result file content", inputData.isReturnFile(), taskResult.getFileContent() != null);
    assertEquals("Result file name", inputData.isReturnFile(), taskResult.getFileName() != null);
    if (inputData.isEmailUser()) {
      final String prefix = "https://";
      assertTrue("Result should start with https://, but was " + taskResult.getDownloadInfo().getUrl(),
          taskResult.getDownloadInfo().getUrl().startsWith(prefix));
    }
  }

  protected static class TestBrokerConnectionFactory extends BrokerConnectionFactory {
    //does both nox and nh3..
    private double depositionStartValue = 1.0;
    private double resultDecrementFactor = 0.5;

    public TestBrokerConnectionFactory(final ExecutorService executorService, final ConnectionConfiguration connectionConfiguration) {
      super(executorService, connectionConfiguration);
    }

    @Override
    protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
      return new OPSMockConnection(depositionStartValue, resultDecrementFactor);
    }

    public void setDepostionStartValue(final double depositionStartValue) {
      this.depositionStartValue = depositionStartValue;
    }

    public void setResultDecrementFactor(final double resultDecrementFactor) {
      this.resultDecrementFactor = resultDecrementFactor;
    }

  }
}
