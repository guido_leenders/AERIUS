/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.export.OPSExportData;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link OPSExportWorker}.
 */
public class OPSExportWorkerTest extends BaseDBWorkerTest<OPSExportWorker> {

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";

  @Override
  protected OPSExportWorker createWorker() throws IOException {
    final DatabaseWorkerFactory workerFactory = new CalculatorWorkerFactory(WorkerType.CALCULATOR);
    return new OPSExportWorker(getCalcPMF(), workerFactory.createConfiguration(properties), factory) {
      @Override
      protected boolean sendMail(final MailMessageData mailMessageData) {
        mailHelper.sendMail(mailMessageData);
        return true;
      }
    };
  }

  @Test
  public void testHandleWorkLoad() throws Exception {
    try (final Connection connection = getCalcConnection()) {
      final Calculation calculation = new Calculation();
      calculation.setYear(TestDomain.YEAR);
      CalculationRepository.insertCalculation(connection, calculation, null);

      calculation.getOptions().setCalculateMaximumRange(10);
      calculation.getOptions().setCalculationType(CalculationType.NATURE_AREA);
      calculation.getOptions().getSubstances().add(Substance.NOXNH3);
      calculation.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);

      final List<AeriusResultPoint> results = new ArrayList<>();
      final AeriusResultPoint point = new AeriusResultPoint(100, AeriusPointType.POINT, 100, 1);
      point.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 100.1);
      point.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 20.0);
      results.add(point);
      CalculationRepository.insertCalculationResults(connection, calculation.getCalculationId(), results);
      final OPSExportData inputData = new OPSExportData();
      inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
      final CalculatedSingle scenario = new CalculatedSingle();
      scenario.setCalculationId(calculation.getCalculationId());
      scenario.setSources(TestDomain.getExampleSourceList());
      scenario.getSources().setName("TestSituation");
      scenario.getCalculation().setYear(TestDomain.YEAR);
      scenario.getOptions().getSubstances().add(Substance.NH3);
      scenario.getOptions().getSubstances().add(Substance.NOX);
      inputData.setScenario(scenario);

      final ExportedData taskResult = worker.run(inputData, null);
      assertExportedData(taskResult, inputData);
    }
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultExportMails();
  }

}
