/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
//package nl.overheid.aerius.worker;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//import java.io.IOException;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import com.lowagie.text.FontFactory;
//
//import nl.overheid.aerius.TestDomain;
//import nl.overheid.aerius.calculation.domain.CalculationInputData;
//import nl.overheid.aerius.db.common.BaseDBTest;
//import nl.overheid.aerius.geo.shared.Point;
//import nl.overheid.aerius.geo.shared.WKTGeometry;
//import nl.overheid.aerius.mail.MailMessageData;
//import nl.overheid.aerius.mail.ReplacementToken;
//import nl.overheid.aerius.mock.MockCloseableHttpClientUtil;
//import nl.overheid.aerius.paa.PAAExportWorker;
//import nl.overheid.aerius.shared.SharedConstants;
//import nl.overheid.aerius.shared.domain.CalculationEngine;
//import nl.overheid.aerius.shared.domain.Substance;
//import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
//import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
//import nl.overheid.aerius.shared.domain.calculation.CalculationType;
//import nl.overheid.aerius.shared.domain.export.ExportType;
//import nl.overheid.aerius.shared.domain.export.ExportedData;
//import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
//import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
//import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
//import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
//import nl.overheid.aerius.shared.domain.sector.Sector;
//import nl.overheid.aerius.shared.domain.sector.SectorGroup;
//import nl.overheid.aerius.shared.domain.sector.SectorIcon;
//import nl.overheid.aerius.shared.domain.sector.SectorProperties;
//import nl.overheid.aerius.shared.domain.sector.ShippingNode;
//import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
//import nl.overheid.aerius.shared.domain.source.EmissionSource;
//import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
//import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
//import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
//import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
//import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
//import nl.overheid.aerius.shared.domain.source.ShippingRoute;
//import nl.overheid.aerius.shared.domain.source.TimeUnit;
//import nl.overheid.aerius.shared.exception.AeriusException;
//import nl.overheid.aerius.shared.exception.AeriusException.Reason;
//import nl.overheid.aerius.taskmanager.client.WorkerType;
//import nl.overheid.aerius.util.GeometryUtil;
//
///**
// * Test class for {@link PAAExportWorker} in combination with {@link CalculatorExportWorker}.
// *
// * Relies heavily on filled AERIUS database.
// */
//public class PAAExportWorkerTest extends BaseDBWorkerTest<CalculatorExportWorker> {
//
//  private static final String LINESTRING_PREFIX = "LINESTRING(";
//  //source X:186277 Y:474477
//  private static final int XCOORD = 114051;
//  private static final int YCOORD = 395806;
//
//  private static TestDomain testDomain;
//
//  @BeforeClass
//  public static void setUpBeforeClass() throws IOException, SQLException {
//    BaseDBTest.setUpBeforeClass();
//    testDomain = new TestDomain(getCalcPMF());
//  }
//
//  @Override
//  @Before
//  public void setUp() throws Exception {
//    getCalcPMF().setAutoCommit(true);
//    super.setUp();
//    FontFactory.registerDirectory(PAAExportWorkerTest.class.getResource("/fonts/").getFile());
//  }
//
//  @Override
//  protected CalculatorExportWorker createWorker() throws IOException, SQLException {
//    initCalculator(getCalcPMF(), factory);
//    final CalculatorWorkerFactory workerFactory = new CalculatorWorkerFactory(WorkerType.CALCULATOR);
//    final ExportWorker paaExportWorker = new PAAExportWorker(getCalcPMF()) {
//      @Override
//      protected CloseableHttpClient createHttpClient() {
//        return MockCloseableHttpClientUtil.create();
//      }
//    };
//
//    return new CalculatorExportWorker(calculatorBuildDirector, getCalcPMF(), factory, workerFactory.createConfiguration(properties), paaExportWorker) {
//      @Override
//      protected boolean sendMail(final MailMessageData mailMessageData) {
//        mailHelper.sendMail(mailMessageData);
//        return true;
//      }
//    };
//  }
//
//  @Test(expected = AeriusException.class)
//  public void testHandleWorkLoadPAAInvalidInputData() throws Exception {
//    try {
//      final CalculationInputData paaInputData = new CalculationInputData();
//      paaInputData.setQueueName("I_would_like_to_thank_my_parents");
//      paaInputData.setEmailAddress("aerius@example.com");
//      paaInputData.setExportType(ExportType.PAA_DEVELOPMENT_SPACES);
//      paaInputData.setLocale(SharedConstants.LOCALE_NL);
//
//      final CalculatedSingle scenario = new CalculatedSingle();
//      scenario.setSources(getExampleEmissionSources());
//      //year 1 won't play well with DB.
//      scenario.getCalculation().setYear(1);
//      scenario.getOptions().getSubstances().add(Substance.NOXNH3);
//      scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
//      paaInputData.setScenario(scenario);
//
//      final Object taskResult = worker.run(paaInputData, null);
//      assertTrue("Result instance of OPSOutputData, was: " + taskResult, taskResult instanceof SQLException);
//    } catch (final AeriusException e) {
//      assertEquals("Reason for exception", Reason.SQL_ERROR, e.getReason());
//      mailHelper.validateMailsSend(1);
//      mailHelper.validateDefaultErrorMails();
//      throw e;
//    }
//  }
//
//  @Test
//  public void testHandleWorkLoadPAAInputData() throws Exception {
//    final CalculationInputData paaInputData = new CalculationInputData();
//    paaInputData.setQueueName("I_really_do_not_care");
//    paaInputData.setEmailAddress("aerius@example.com");
//
//    paaInputData.setLocale(SharedConstants.LOCALE_NL);
//    paaInputData.setExportType(ExportType.PAA_OWN_USE);
//
//    final CalculatedComparison scenario = new CalculatedComparison();
//    final EmissionSourceList sourcesOne = getExampleEmissionSources();
//    sourcesOne.setName("Death star two");
//    scenario.setSourcesOne(sourcesOne);
//    scenario.getCalculationOne().setYear(TestDomain.YEAR);
//    final EmissionSourceList sourcesTwo = new EmissionSourceList();
//    sourcesTwo.addAll(getExampleEmissionSources().subList(0, 2));
//    sourcesTwo.setName("Death star three");
//    scenario.setSourcesTwo(sourcesTwo);
//    scenario.getCalculationTwo().setYear(TestDomain.YEAR);
//    scenario.getCalculationPointsOne().add(createResultPoint("Rebel Camp", 10.2));
//    scenario.getCalculationPointsOne().add(createResultPoint("The Trap", 10.3));
//    scenario.getCalculationPointsTwo().add(createResultPoint("Rebel Camp", 10.4));
//    scenario.getCalculationPointsTwo().add(createResultPoint("The Trap", 10.5));
//    scenario.getMetaData().setDescription("Building the third death star");
//    scenario.getMetaData().setCorporation("The Galactic Empire");
//    scenario.getMetaData().setProjectName("A death star");
//
//    scenario.getOptions().setTemporaryProjectYears(3);
//    scenario.getOptions().setCalculationType(CalculationType.PAS);
//
//    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
//    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
//    paaInputData.setScenario(scenario);
//
//    final ExportedData taskResult = worker.run(paaInputData, null);
//    assertExportedData(taskResult, paaInputData);
//
//    mailHelper.validateMailsSend(1);
//    mailHelper.validateDefaultExportMails(ReplacementToken.PROJECT_NAME);
//  }
//
//  private AeriusResultPoint createResultPoint(final String name, final double value) {
//    final AeriusResultPoint calculationPoint = new AeriusResultPoint(XCOORD + 1000, YCOORD - 1000);
//    calculationPoint.setLabel(name);
//    calculationPoint.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 10.3);
//    return calculationPoint;
//  }
//
//  private EmissionSourceList getExampleEmissionSources() throws AeriusException {
//    final EmissionSourceList sources = new EmissionSourceList();
//
//    final int xCoord = XCOORD;
//    final int yCoord = YCOORD;
//
//    final WKTGeometry geometrySource = new WKTGeometry("POINT(" + xCoord + " " + yCoord + ")", 1);
//    final EmissionSource source1 = getSource(1, geometrySource, "SomeSource", TestDomain.getGenericEmissionSource());
//    sources.add(source1);
//
//    final int xCoord1 = xCoord + 100;
//    final int yCoord1 = yCoord + 100;
//
//    final WKTGeometry geometrySource1 = new WKTGeometry("POINT(" + xCoord1 + " " + yCoord1 + ")", 1);
//    final EmissionSource source2 = getSource(2, geometrySource1, "SomeFarmSource", testDomain.getFarmEmissionSource());
//    source2.setSourceCharacteristics(TestDomain.getNonDefaultCharacteristics());
//    source2.getSourceCharacteristics().setBuildingHeight(10.0);
//    final Sector farmSector = new Sector(4110, SectorGroup.AGRICULTURE, "Veehouderij: Stalemissies",
//        new SectorProperties("123456", SectorIcon.FARM_LODGE, EmissionCalculationMethod.FARM_LODGE, CalculationEngine.OPS, true));
//    source2.setSector(farmSector);
//    sources.add(source2);
//
//    final int xCoord2 = xCoord;
//    final int yCoord2 = yCoord1;
//
//    final WKTGeometry geometrySource2 = new WKTGeometry("POLYGON(("
//        + xCoord + " " + yCoord + ","
//        + xCoord1 + " " + yCoord1 + ","
//        + xCoord2 + " " + yCoord2 + ","
//        + xCoord + " " + yCoord + "))", 100000);
//
//    final EmissionSource source3 =
//        getSource(3, geometrySource2, "SomeOffRoadSource", testDomain.getOffRoadMobileEmissionSource(new OffRoadMobileEmissionSource()));
//    final Sector sector = Sector.SECTOR_DEFAULT;
//    source3.setSector(sector);
//    sources.add(source3);
//
//    final WKTGeometry geometrySource3 = new WKTGeometry(LINESTRING_PREFIX
//        + xCoord + " " + yCoord + ","
//        + xCoord1 + " " + yCoord1 + ","
//        + xCoord2 + " " + yCoord2 + ")", 5000);
//
//    final EmissionSource source4 = getSource(3, geometrySource3, "SomeRoadSource", testDomain.getSRM2EmissionSource());
//    source4.setSector(sector);
//    sources.add(source4);
//
//    final int xCoord3 = xCoord2 + 100;
//    final int yCoord3 = yCoord2 + 100;
//
//    final int nodeXCoord = xCoord2 + 30;
//    final int nodeYCoord = yCoord2 + 30;
//    final WKTGeometry geometrySource4 = new WKTGeometry(LINESTRING_PREFIX
//        + xCoord2 + " " + yCoord2 + ","
//        + xCoord3 + " " + yCoord3 + ")", 100000);
//    final WKTGeometry routeGeometry = new WKTGeometry(LINESTRING_PREFIX
//        + nodeXCoord + " " + nodeYCoord + ","
//        + (nodeXCoord - 1000) + " " + (nodeYCoord + 1000) + ")", 10000);
//    final WKTGeometry maritimeRouteGeometry = new WKTGeometry(LINESTRING_PREFIX
//        + (nodeXCoord - 1000) + " " + (nodeYCoord + 1000) + ","
//        + (nodeXCoord - 1000) + " " + (nodeYCoord + 1500) + ")", 10000);
//    final EmissionSource source5 = getSource(4, geometrySource4, "SomeShipSource", getShipEmissionValues(routeGeometry, maritimeRouteGeometry));
//    sources.add(source5);
//
//    final EmissionSource source6 = getSource(5, geometrySource2, "SomePlanSource", testDomain.getPlanEmissionSource());
//    source6.setSector(new Sector(9000, SectorGroup.PLAN, "",
//        new SectorProperties("6B15CB", SectorIcon.INDUSTRY, EmissionCalculationMethod.PLAN, CalculationEngine.OPS, false)));
//    sources.add(source6);
//
//    final int xCoord4 = xCoord3 + 100;
//    final int yCoord4 = yCoord3 - 100;
//
//    final WKTGeometry geometrySource6 = new WKTGeometry(LINESTRING_PREFIX
//        + xCoord3 + " " + yCoord3 + ","
//        + xCoord4 + " " + yCoord4 + ")", 100000);
//    final EmissionSource source7 = getSource(6, geometrySource6, "SomeInlandRouteSource", testDomain.getInlandRouteEmissionValues());
//    source7.setSector(sector);
//    sources.add(source7);
//
//    final int xCoord5 = xCoord4 + 100;
//    final int yCoord5 = yCoord4 + 100;
//
//    final int nodeXCoord1 = xCoord4 + 30;
//    final int nodeYCoord1 = yCoord4 + 30;
//
//    final WKTGeometry geometrySource7 = new WKTGeometry(LINESTRING_PREFIX
//        + xCoord4 + " " + yCoord4 + ","
//        + xCoord5 + " " + yCoord5 + ")", 10000);
//    final WKTGeometry inlandMooringRouteGeometry = new WKTGeometry(LINESTRING_PREFIX
//        + nodeXCoord1 + " " + nodeYCoord1 + ","
//        + (nodeXCoord1 - 1000) + " " + (nodeYCoord1 + 1000) + ")", 10000);
//    final ShippingRoute shippingMooringRoute = new ShippingRoute();
//    shippingMooringRoute.setGeometry(inlandMooringRouteGeometry);
//    final Point middleOfGeometry = GeometryUtil.middleOfGeometry(inlandMooringRouteGeometry);
//    shippingMooringRoute.setX(middleOfGeometry.getX());
//    shippingMooringRoute.setY(middleOfGeometry.getY());
//    final EmissionSource source8 = getSource(7, geometrySource7, "SomeInlandMooringSource",
//        testDomain.getInlandMooringEmissionSource(shippingMooringRoute));
//    source8.setSector(sector);
//    sources.add(source8);
//
//    return sources;
//  }
//
//  private MaritimeMooringEmissionSource getShipEmissionValues(final WKTGeometry routeGeometry, final WKTGeometry maritimeRouteGeometry)
//      throws AeriusException {
//    final ArrayList<MaritimeShippingCategory> shipCategories = new ArrayList<>();
//    final MaritimeShippingCategory shipEmissionCategory1 = new MaritimeShippingCategory();
//    shipEmissionCategory1.setId(1);
//    shipEmissionCategory1.setCode("TestShip");
//    shipEmissionCategory1.setName("TestShip");
//    shipEmissionCategory1.setDescription("TestShip sails automagically");
//    final Sector sector = Sector.SECTOR_DEFAULT;
//    sector.setDefaultCharacteristics(new OPSSourceCharacteristics(1, 1, 1, 1, TestDomain.DV_INDUSTRIAL_ACTIVITY, 1));
//    shipCategories.add(shipEmissionCategory1);
//    final MaritimeShippingCategory shipEmissionCategory2 = new MaritimeShippingCategory();
//    shipEmissionCategory2.setId(2);
//    shipEmissionCategory2.setCode("TestBarge");
//    shipEmissionCategory2.setName("TestBarge");
//    shipEmissionCategory2.setDescription("TestBarge does not cause nausea");
//    shipCategories.add(shipEmissionCategory2);
//
//    final ShippingRoute route = new ShippingRoute();
//    route.setId(1);
//    final Point middleOfGeometry = GeometryUtil.middleOfGeometry(routeGeometry);
//    route.setX(middleOfGeometry.getX());
//    route.setY(middleOfGeometry.getY());
//    route.setGeometry(routeGeometry);
//    final ShippingNode node = new ShippingNode(5, XCOORD, YCOORD);
//    route.setEndPoint(node);
//
//    final ShippingRoute maritimeShippingRoute = new ShippingRoute();
//    maritimeShippingRoute.setId(2);
//    final Point middleOfGeometry2 = GeometryUtil.middleOfGeometry(maritimeRouteGeometry);
//    maritimeShippingRoute.setX(middleOfGeometry2.getX());
//    maritimeShippingRoute.setY(middleOfGeometry2.getY());
//    maritimeShippingRoute.setGeometry(maritimeRouteGeometry);
//
//    final MaritimeMooringEmissionSource emissionValues = new MaritimeMooringEmissionSource();
//    final MooringMaritimeVesselGroup vesselGroupEmissionValues = new MooringMaritimeVesselGroup();
//    vesselGroupEmissionValues.setId(1);
//    vesselGroupEmissionValues.setCategory(shipEmissionCategory1);
//    vesselGroupEmissionValues.setNumberOfShips(30000, TimeUnit.YEAR);
//    vesselGroupEmissionValues.setName("Burgundy");
//    vesselGroupEmissionValues.setResidenceTime(300);
//    vesselGroupEmissionValues.setInlandRoute(route);
//    final MaritimeRoute maritimeRoute = new MaritimeRoute();
//    maritimeRoute.setRoute(maritimeShippingRoute);
//    maritimeRoute.setShipMovements(100, TimeUnit.YEAR);
//    vesselGroupEmissionValues.getMaritimeRoutes().add(maritimeRoute);
//    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues);
//    final MooringMaritimeVesselGroup vesselGroupEmissionValues1 = new MooringMaritimeVesselGroup();
//    vesselGroupEmissionValues1.setId(2);
//    vesselGroupEmissionValues1.setCategory(shipEmissionCategory2);
//    vesselGroupEmissionValues1.setName("Flying dutchmen");
//    vesselGroupEmissionValues1.setNumberOfShips(15000, TimeUnit.YEAR);
//    vesselGroupEmissionValues1.setResidenceTime(100);
//    vesselGroupEmissionValues1.setInlandRoute(route);
//    final MaritimeRoute maritimeRoute2 = new MaritimeRoute();
//    maritimeRoute2.setRoute(maritimeShippingRoute);
//    maritimeRoute2.setShipMovements(5000, TimeUnit.YEAR);
//    vesselGroupEmissionValues1.getMaritimeRoutes().add(maritimeRoute2);
//    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues1);
//    return emissionValues;
//  }
//
//  private <E extends EmissionSource> E getSource(final int id, final WKTGeometry geometry, final String label, final E orgSource)
//      throws AeriusException {
//    final E source = TestDomain.getSource(id, geometry, label, orgSource);
//    final Point midPoint = GeometryUtil.middleOfGeometry(geometry);
//    source.setX(midPoint.getX());
//    source.setY(midPoint.getY());
//    return source;
//  }
//
//}
