/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.shared.domain.sector.SectorIcon;

/**
 * Test for {@link SectorIconUtil} class.
 */
public class SectorUtilTest {

  private static final Logger LOG = LoggerFactory.getLogger(SectorUtilTest.class);

  /**
   * Test if all SectorIcon values have a matching image supplied.
   *
   * @throws DocumentException
   * @throws IOException
   */
  @Test
  public void testGetIconFilename() throws DocumentException, IOException {
    final Set<SectorIcon> faultyIcons = new HashSet<>();
    for (final SectorIcon sectorIcon: SectorIcon.values()) {
      try {
        PDFUtils.getImage(SectorIconUtil.getIconFilename(sectorIcon), null);
      } catch (final IllegalArgumentException e) {
        LOG.error("Error getting image for sector {}", sectorIcon, e);
        faultyIcons.add(sectorIcon);
      }
    }
    assertTrue("SectorIcons that did not have a image: " + StringUtils.join(faultyIcons, ", "), faultyIcons.isEmpty());
  }
}
