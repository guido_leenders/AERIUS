/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import static org.junit.Assert.assertEquals;

import java.awt.Dimension;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;

/**
 * Test class for {@link WMSUtils}.
 */
public class WMSUtilsTest extends BaseDBTest {

  private static final int M1 = 12;
  private static final int M2 = 1553;
  private static final int M3 = 1759;
  private static final int M4 = 21999;
  private static final int[] DIM_VALUES = {M1, M2, M3, M4};

  @Test
  public void testGetFixedDimension() {
    for (final int maxWidth : DIM_VALUES) {
      for (final int maxHeight : DIM_VALUES) {
        for (final int bboxWidth : DIM_VALUES) {
          for (final int bboxHeight : DIM_VALUES) {
            final boolean wLarger = bboxWidth > maxWidth;
            final boolean hLarger = bboxHeight > maxHeight;
            final boolean wRatioLarger = bboxWidth / ((double) maxWidth) > bboxHeight / ((double) maxHeight);
            final int expectedWidth;
            final int expectedHeight;

            if (wLarger && !hLarger || wLarger && wRatioLarger) {
              // bbox width larger then max, but height not OR height is also larger, but ratio width is larger
              expectedWidth = maxWidth;
              expectedHeight = (int) Math.round((bboxHeight * maxWidth) / ((double) bboxWidth));
            } else if (!wLarger && hLarger || hLarger && !wRatioLarger) {
              // bbox height larger then max, but width not OR width is also larger, but ratio height is larger
              expectedWidth = (int) Math.round((bboxWidth * maxHeight) / ((double) bboxHeight));
              expectedHeight = maxHeight;
            } else if (bboxHeight == maxHeight || bboxWidth == maxWidth) {
              // the same sizes.
              expectedWidth = bboxWidth;
              expectedHeight = bboxHeight;
            } else if (bboxHeight < bboxWidth) {
              // not larger, not the same size
              final int expectedGuessedHeight = (int) Math.round((bboxHeight * maxWidth) / ((double) bboxWidth));
              if (expectedGuessedHeight > maxHeight) {
                // even while bbox width larger, due to ratio height should be max, otherwise height beyond max
                expectedWidth = (int) Math.round((bboxWidth * maxHeight) / ((double) bboxHeight));
                expectedHeight = maxHeight;
              } else {
                // width is max, and height is scaled.
                expectedWidth = maxWidth;
                expectedHeight = expectedGuessedHeight;
              }
              continue;
            } else if (bboxHeight > bboxWidth) {
              final int expectedGuessedWidth = (int) Math.round((bboxWidth * maxHeight) / ((double) bboxHeight));
              if (expectedGuessedWidth > maxWidth) {
                // even while bbox height larger, due to ratio width should be max, otherwise width beyond max
                expectedWidth = maxWidth;
                expectedHeight = (int) Math.round((bboxHeight * maxWidth) / ((double) bboxWidth));
              } else {
                // height is max and width is scaled.
                expectedWidth = expectedGuessedWidth;
                expectedHeight = maxHeight;
              }
            } else {
              // max not same size, but bbox is square. width and height should min max value.
              expectedWidth = Math.min(maxWidth, maxHeight);
              expectedHeight = expectedWidth;
            }
            final String message = "maxWidth:" + maxWidth + ", maxHeight:" + maxHeight
                + ", bboxWidth:" + bboxWidth + ", bboxHeight:" + bboxHeight
                + ", wLarger:" + wLarger + ", hLarger:" + hLarger + ", wRatioLarger:" + wRatioLarger;
            assertGetFixedDimension(message, maxWidth, maxHeight, bboxWidth, bboxHeight, expectedWidth, expectedHeight);
          }
        }
      }
    }
  }

  private void assertGetFixedDimension(final String message, final int maxWidth, final int maxHeight,
      final double bboxWidth, final double bboxHeight,
      final int expectedX, final int expectedY) {
    final Dimension boxWidthHeightLarger = WMSUtils.getFixedDimension(maxWidth, maxHeight, bboxWidth, bboxHeight);
    assertEquals(message + ": width", expectedX, boxWidthHeightLarger.width);
    assertEquals(message + ": height", expectedY, boxWidthHeightLarger.height);
  }
}
