/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Test class for {@link GMLExportWorker} in combination with {@link CalculatorExportWorker}.
 */
public class GMLExportWorkerTest extends BaseDBWorkerTest<CalculatorExportWorker> {

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";

  private ImmediateExportData handleExport;

  @Override
  @Before
  public void setUp() throws Exception {
    getCalcPMF().setAutoCommit(true);
    super.setUp();
  }

  @Override
  protected CalculatorExportWorker createWorker() throws IOException, SQLException {
    initCalculator(getCalcPMF(), factory);
    handleExport = null;
    final CalculatorWorkerFactory workerFactory = new CalculatorWorkerFactory(WorkerType.CALCULATOR);
    return new CalculatorExportWorker(calculatorBuildDirector, getCalcPMF(), factory, workerFactory.createConfiguration(properties),
        new GMLExportWorker(getCalcPMF())) {

      @Override
      protected nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData handleExport(final CalculationInputData inputData,
          final ArrayList<StringDataSource> backupAttachments, final String correlationId) throws Exception {
        handleExport = super.handleExport(inputData, backupAttachments, correlationId);
        return handleExport;
      }

      @Override
      protected boolean sendMail(final MailMessageData mailMessageData) {
        mailHelper.sendMail(mailMessageData);
        return true;
      }

    };
  }

  @Test
  public void testHandleWorkLoadGMLInputDataWithError() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(getExampleEmissionSources());
    scenario.getSources().get(0).getGeometry().setWkt("INVALID GEOMETRY");
    inputData.setScenario(scenario);
    try {
      worker.run(inputData, null); // should throw an exception
      fail("expected Exception");
    } catch (final AeriusException e) {
      mailHelper.validateMailsSend(1);
      mailHelper.validateDefaultErrorMails();
    }
  }

  @Test(expected = AeriusException.class)
  public void testHandleWorkLoadGMLInputDataWithoutCalculation() throws Exception {
    final CalculationInputData inputData = new CalculationInputData();
    inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(getExampleEmissionSources());
    inputData.setScenario(scenario);
    inputData.setExportType(ExportType.GML_WITH_RESULTS);

    try {
      worker.run(inputData, null);
      fail("expected Exception");
    } catch (final AeriusException e) {
      mailHelper.validateMailsSend(1);
      mailHelper.validateDefaultErrorMails();
      throw e;
    }
  }

  private EmissionSourceList getExampleEmissionSources() {
    final EmissionSourceList sources = new EmissionSourceList();
    sources.add(createExampleEmissionSources(Sector.SECTOR_DEFAULT));
    return sources;
  }

  @Test
  public void testHandleWorkLoadGMLInputData() throws Exception {
    //test the GML worker by letting it send a mail for an existing calculation.
    final CalculationInputData inputData = createExportData();

    final String propsCorrelationId = UUID.randomUUID().toString();
    final ExportedData taskResult = worker.run(inputData, propsCorrelationId);
    assertExportedData(taskResult, inputData);
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultExportMails(ReplacementToken.AERIUS_REFERENCE);
  }

  @Test
  public void testHandleWorkLoadGMLInputDataWithJobName() throws Exception {
    //test the GML worker by letting it send a mail for an existing calculation and exisiting job name
    final CalculationInputData inputData = createExportData();
    inputData.setName("calculation name"); // add a name triggers different mail subject

    final String propsCorrelationId = UUID.randomUUID().toString();
    final ExportedData taskResult = worker.run(inputData, propsCorrelationId);
    assertExportedData(taskResult, inputData);
    mailHelper.validateMailsSend(1);
    mailHelper.validateDefaultExportMails(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.JOB);
  }

  @Test
  public void testHandleWorkLoadGMLMultipleSectors() throws Exception {
    final CalculationInputData inputData = createExportData();
    inputData.setExportType(ExportType.GML_WITH_SECTORS_RESULTS);
    worker.run(inputData, null);
    assertEquals("Incorrect number of files zip ", 3, handleExport.getFilesInZip());
  }

  private CalculationInputData createExportData() throws SQLException {
    final SectorCategories sc = SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale());
    try (final Connection connection = getCalcConnection()) {
      final CalculationInputData inputData = new CalculationInputData();
      inputData.setEmailAddress(DUMMY_EMAIL_ADDRESS);
      inputData.setLocale(SharedConstants.LOCALE_NL);
      inputData.setExportType(ExportType.GML_WITH_RESULTS);
      inputData.setQueueName("Never_gonna_find_me");


      final CalculatedSingle scenario = new CalculatedSingle();
      final EmissionSourceList sources = new EmissionSourceList();
      sources.add(createExampleEmissionSources(sc.getSectorById(1800)));
      sources.add(createExampleEmissionSources(sc.getSectorById(1100)));
      //sources.add(createExampleEmissionSources(sc.getSectorById(3111)));
      scenario.setSources(sources);
      scenario.getCalculation().setYear(1);
      scenario.getSources().setName("TestSituation");
      scenario.getCalculation().setYear(TestDomain.YEAR);
      scenario.getOptions().setCalculationType(CalculationType.PAS);
      scenario.getOptions().getSubstances().add(Substance.NOXNH3);
      scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
      inputData.setScenario(scenario);

      return inputData;
    }
  }

  private EmissionSource createExampleEmissionSources(final Sector sector) {
    //source x: 122442 y: 473298
    final int xCoord = 122442;
    final int yCoord = 473298;
    final GenericEmissionSource source = new GenericEmissionSource(1, new Point(xCoord, yCoord));
    source.setGeometry(new WKTGeometry("POINT(" + xCoord + " " + yCoord + ")", 1));
    source.setLabel("SomeSource" + sector.getSectorId());
    source.setSector(sector);
    final OPSSourceCharacteristics characteristics = new OPSSourceCharacteristics(0.22, 40, 0, 20, TestDomain.DV_INDUSTRIAL_ACTIVITY, 1);
    source.setSourceCharacteristics(characteristics);
    source.setEmission(Substance.NOX, 1000);
    source.setEmission(Substance.NH3, 1000);
    return source;
  }
}
