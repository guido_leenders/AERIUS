/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.importer.ImportInput;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link ImportWorker}.
 */
public class ImportWorkerTest extends BaseDBWorkerTest<ImportWorker> {

  @Override
  protected ImportWorker createWorker() throws IOException {
    return new ImportWorker(getCalcPMF()) {
      @Override
      protected CalculatorLimits getLimits(final boolean checkLimits) throws SQLException {
        final CalculatorLimits limits = super.getLimits(checkLimits);

        if (limits != null) {
          limits.setMaxSources(0);
        }

        return limits;
      }
    };
  }

  @Test
  public void testPDFImport() throws Exception {
    final String filename = "AERIUS_bijlage_test.pdf";
    final byte[] fileContent = getFileContent(filename);
    final ImportInput input = new ImportInput(filename, ImportType.PAA, fileContent);
    final ImportOutput output = worker.run(input, null, null);
    assertNotNull("Should have some result", output);
    assertNotNull("Should be a calculated scenario", output.getCalculatedScenario());
    assertEquals("Nr of source lists", 2, output.getCalculatedScenario().getScenario().getSourceLists().size());
    assertEquals("Nr of sources", 1, output.getCalculatedScenario().getScenario().getSourceLists().get(0).size());
    assertEquals("Shouldn't have a valid calculation ID (no results/nothing persisted)", 0,
        output.getCalculatedScenario().getCalculations().get(0).getCalculationId());
  }

  @Test
  public void testGMLImportWithCalculationResults() throws Exception {
    final String filename = "with_calculation_results.gml";
    final byte[] fileContent = getFileContent(filename);
    final ImportInput input = new ImportInput(filename, ImportType.GML, fileContent);
    ImportOutput output = worker.run(input, null, null);
    assertNotNull("Should have some result", output);
    assertNotNull("Should be a calculated scenario", output.getCalculatedScenario());
    assertEquals("Nr of calculations", 1, output.getCalculatedScenario().getCalculations().size());
    assertEquals("Shouldn't have a valid calculation ID (nothing persisted)", 0,
        output.getCalculatedScenario().getCalculations().get(0).getCalculationId());
    assertEquals("Nr of source lists", 1, output.getCalculatedScenario().getScenario().getSourceLists().size());
    assertEquals("Nr of sources", 1, output.getCalculatedScenario().getScenario().getSourceLists().get(0).size());

    input.setPersistResults(true);
    output = worker.run(input, null, null);
    assertNotNull("Should have some result", output);
    assertNotNull("Should be a calculated scenario", output.getCalculatedScenario());
    assertEquals("Nr of calculations", 1, output.getCalculatedScenario().getCalculations().size());
    assertNotEquals("Should have a valid calculation ID", 0, output.getCalculatedScenario().getCalculations().get(0).getCalculationId());
    assertEquals("Nr of source lists", 1, output.getCalculatedScenario().getScenario().getSourceLists().size());
  }

  @Test
  public void testGMLImportCalculationLimits() throws Exception {
    final String filename = "AERIUS_bijlage_test.pdf";
    final byte[] fileContent = getFileContent(filename);
    final ImportInput input = new ImportInput(filename, ImportType.GML, fileContent);
    input.setCheckLimits(false);

    ImportOutput output = worker.run(input, null, null);
    assertEquals("Nr of source lists", 2, output.getCalculatedScenario().getScenario().getSourceLists().size());
    assertEquals("Nr of sources", 1, output.getCalculatedScenario().getScenario().getSourceLists().get(0).size());

    input.setCheckLimits(true);

    output = worker.run(input, null, null);

    assertEquals("There should be 1 exception.", 1, output.getExceptions().size());
    assertEquals("Limit source check should have been exceeded.", Reason.LIMIT_SOURCES_EXCEEDED, output.getExceptions().get(0).getReason());
  }

  private byte[] getFileContent(final String filename) throws FileNotFoundException, IOException {
    final File file = new File(getClass().getResource(filename).getFile());
    final byte[] fileContent;
    try (InputStream inputStream = new FileInputStream(file)) {
      fileContent = IOUtils.toByteArray(inputStream);
    }
    return fileContent;
  }
}
