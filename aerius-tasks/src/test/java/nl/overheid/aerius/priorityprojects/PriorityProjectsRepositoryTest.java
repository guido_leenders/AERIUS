/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.priorityprojects;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;

/**
 * Test class for {@link PriorityProjectsRepository}
 */
public class PriorityProjectsRepositoryTest extends BaseDBTest {

  private static final String TRUNCATE_SITES =
      "TRUNCATE TABLE sites CASCADE";

  private static final int PRIORITY_PROJECT_ORIGIN_ID = 1;
  private static final AtomicInteger PRIORITY_PROJECT_SITE_ID = new AtomicInteger(901901);
  private static final AtomicInteger PRIORITY_PROJECT_SOURCE_ID = new AtomicInteger(42424242);

  @Before
  public void setUp() throws SQLException {
    truncateSites();
  }

  @Test
  public void testInsertPriorityProjectOrigin() throws SQLException {
    PriorityProjectsRepository.insertPriorityProjectOrigin(getRegConnection(), PRIORITY_PROJECT_ORIGIN_ID, "Ab temnaar", "utrecht");
    //inserting same origin ID twice has no effect.
    PriorityProjectsRepository.insertPriorityProjectOrigin(getRegConnection(), PRIORITY_PROJECT_ORIGIN_ID, "Met Rabana", "flevoland");
  }

  @Test
  public void testInsertPriorityProject() throws SQLException {
    final int siteId = PRIORITY_PROJECT_SITE_ID.getAndIncrement();
    PriorityProjectsRepository.insertPriorityProject(
        getRegConnection(), siteId, 2020, "utrecht", "project-" + new Random().nextInt(), "description", "reference");
  }

  @Test
  public void testInsertPriorityProjectFile() throws SQLException, IOException {
    final int siteId = PRIORITY_PROJECT_SITE_ID.getAndIncrement();
    PriorityProjectsRepository.insertPriorityProject(
        getRegConnection(), siteId, 2020, "utrecht", "project-" + new Random().nextInt(), "description", "reference");
    PriorityProjectsRepository.insertPriorityProjectFile(
        getRegConnection(), siteId, new File("nonexistant"), "yes", new byte[0]);
  }

  @Test
  public void testInsertPriorityProjectSource() throws SQLException, IOException {
    final int siteId = PRIORITY_PROJECT_SITE_ID.getAndIncrement();
    final int sourceId = PRIORITY_PROJECT_SOURCE_ID.getAndIncrement();
    PriorityProjectsRepository.insertPriorityProjectOrigin(getRegConnection(), PRIORITY_PROJECT_ORIGIN_ID, "Ab temnaar", "utrecht");
    PriorityProjectsRepository.insertPriorityProject(
        getRegConnection(), siteId, 2020, "utrecht", "project-" + new Random().nextInt(), "description", "reference");
    PriorityProjectsRepository.insertPriorityProjectSource(
        getRegConnection(), siteId, sourceId, 1800, PRIORITY_PROJECT_ORIGIN_ID, "SomeName", new WKTGeometry("POINT(123133 401231)"));
    PriorityProjectsRepository.insertPriorityProjectSourceCharacteristics(
        getRegConnection(), sourceId, new OPSSourceCharacteristics());
    final Map<Substance, Double> emissionValues = new HashMap<>();
    emissionValues.put(Substance.NH3, 213.2);
    emissionValues.put(Substance.NOX, 453.5);
    PriorityProjectsRepository.insertPriorityProjectSourceEmissions(
        getRegConnection(), sourceId, emissionValues);
  }

  private void truncateSites() throws SQLException {
    // Much faster than clearDevelopmentSpaces(), but not a very good testcase.
    // We'll have to use it in the updateState tests because of performance reasons.
    try (final Connection con = getRegConnection();
        final PreparedStatement ps = con.prepareStatement(TRUNCATE_SITES)) {
      ps.executeUpdate();
    }
  }
}
