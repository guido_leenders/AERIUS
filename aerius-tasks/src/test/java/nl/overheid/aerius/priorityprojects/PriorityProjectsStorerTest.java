/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.priorityprojects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.priorityprojects.PriorityProjectsStorer.FileData;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link PriorityProjectsStorer}.
 */
public class PriorityProjectsStorerTest extends BaseDBTest {

  private static final String UTRECHT = "utrecht";

  @Test
  public void testScanFiles() throws IOException, SQLException {
    final PriorityProjectsStorer storer = new PriorityProjectsStorer(getRegPMF(), LocaleUtils.getDefaultLocale());
    final List<FileData> files = storer.scanFiles(PriorityProjectsStorerTest.class.getResource(UTRECHT).getPath());
    assertEquals("Check files found count", 4, files.size());
    assertEquals("Check file found is gml", ImportType.GML, files.get(1).getType());
  }

  @Test
  public void testGetImportResult() throws IOException, SQLException, AeriusException {
    final PriorityProjectsStorer storer = new PriorityProjectsStorer(getRegPMF(), LocaleUtils.getDefaultLocale());
    final List<FileData> files = storer.scanFiles(PriorityProjectsStorerTest.class.getResource(UTRECHT).getPath());
    final ImportResult result = storer.getImportResult(files.get(0).getFile().getPath());
    assertNotNull("Check import results not null", result);
  }

  @Test
  public void testStoreFilesOnPath() throws IOException, SQLException, AeriusException {
    //we don't have methods to check if the stuff is imported the right way.
    //if you do want to check, uncomment next line and check the DB... (truncate sites cascade before running it again, it doesn't overwrite existing stuff)
    //getRegPMF().setAutoCommit(true);
    final PriorityProjectsStorer storer = new PriorityProjectsStorer(getRegPMF(), LocaleUtils.getDefaultLocale());
    final List<Substance> substances = new ArrayList<>();
    substances.add(Substance.NOX);
    storer.storeFilesOnPath(PriorityProjectsStorerTest.class.getResource(UTRECHT).getPath(), 1, 390000, 390000, substances);
  }
}
