/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.GetResponse;

import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.MockConnection;
import nl.overheid.aerius.taskmanager.client.util.QueueHelper;
import nl.overheid.aerius.util.OSUtils;
import nl.overheid.aerius.worker.PropertiesUtil;
import nl.overheid.aerius.worker.configuration.BrokerConfiguration;

/**
 * Base class for Intercept Test classes.
 */
public class InterceptTestBase {

  private static ExecutorService executor;

  protected Properties properties;
  protected BrokerConnectionFactory factory;
  protected Channel channel;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    executor = Executors.newSingleThreadExecutor();
  }

  @AfterClass
  public static void afterClass() {
    executor.shutdown();
  }

  @Before
  public void before() throws IOException {
    properties = OSUtils.isWindows() ? PropertiesUtil.getFromPropertyFile("worker.properties")
        : PropertiesUtil.getFromPropertyFile("worker_test_linux.properties");
    channel = Mockito.mock(Channel.class);
    factory = new BrokerConnectionFactory(executor, new BrokerConfiguration(properties)) {
      @Override
      protected com.rabbitmq.client.Connection createNewConnection() throws IOException {
        return new MockConnection() {
          @Override
          public Channel createChannel() throws IOException {
            return channel;
          }
        };
      }
    };
  }

  @After
  public void tearDown() throws Exception {
    factory.shutdown();
  }

  public String getTmpPath() {
    return getClass().getResource(".").getPath();
  }

  protected Semaphore mockConsume(final String result, final String inQueue) throws IOException {
    final Semaphore lock = new Semaphore(0);
    Mockito.when(channel.basicGet(eq(inQueue), anyBoolean())).then(new Answer<GetResponse>() {
      @Override
      public GetResponse answer(final InvocationOnMock invocation) throws Throwable {
        final GetResponse response =
            new GetResponse(new Envelope(1, false, "", inQueue), new BasicProperties(), QueueHelper.objectToBytes(result), 1);
        lock.release();
        return response;

      }
    });
    return lock;
  }

  protected AtomicBoolean mockAck(final boolean ackValue) throws IOException {
    final AtomicBoolean calledAck = new AtomicBoolean(!ackValue);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        calledAck.set(ackValue);
        return null;
      }
    }).when(channel).basicAck(anyLong(), anyBoolean());
    return calledAck;
  }
}
