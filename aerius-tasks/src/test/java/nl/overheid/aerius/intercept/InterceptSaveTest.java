/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.junit.Assert.assertFalse;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;

import nl.overheid.aerius.intercept.InterceptSave.InterceptSaveCommand;


/**
 * Test class for {@link InterceptSave}.
 */
public class InterceptSaveTest extends InterceptTestBase {

  @Test
  public void testSave() throws Exception {
    final String result = "result";
    final String inQueue = "inQueue";
    final InterceptSave save = new InterceptSave(new InterceptSaveCommand(new String[] {inQueue, getTmpPath()}));
    mockConsume(result, inQueue);
    final AtomicBoolean calledAck = mockAck(true);
    save.save(factory);
    assertFalse("No ack should be called", calledAck.get());
  }
}
