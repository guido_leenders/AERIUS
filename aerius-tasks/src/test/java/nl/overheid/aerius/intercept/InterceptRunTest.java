/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import nl.overheid.aerius.intercept.InterceptRun.InterceptRunCommand;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerConfiguration;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Test class for {@link InterceptRun}.
 */
public class InterceptRunTest extends InterceptTestBase {

  @Test
  public void testRun() throws Exception {
    final String result = "result";
    final String inQueue = "inQueue";
    final WorkerFactory<?>[] factories = new WorkerFactory[] { new TestWorkerFactory() };
    final InterceptRun run = new InterceptRun(new InterceptRunCommand(new String[] {inQueue, "ops"}), factories);
    final Semaphore lock = mockConsume(result, inQueue);
    run.run(factory, properties);
    assertTrue("Test intercept run possible in deadlock, lock time elapsed", lock.tryAcquire(1, TimeUnit.SECONDS));
  }

  private static class TestWorkerConfiguration extends WorkerConfiguration {
     public TestWorkerConfiguration(final Properties properties) {
       super(properties, "test");
    }

    @Override
    protected List<String> getValidationErrors() {
      return new ArrayList<String>();
    }
  }

  private static class TestWorkerFactory implements WorkerFactory<TestWorkerConfiguration> {

    @Override
    public TestWorkerConfiguration createConfiguration(final Properties properties) {
      return new TestWorkerConfiguration(properties);
    }

    @Override
    public WorkerHandler createWorkerHandler(final TestWorkerConfiguration configuration, final BrokerConnectionFactory factory)
        throws IOException, AeriusException {
      return new WorkerHandler() {
        @Override
        public Serializable handleWorkLoad(final Serializable input, final WorkerIntermediateResultSender resultSender, final String correlationId)
            throws Exception {
          return input;
        }
      };
    }

    @Override
    public WorkerType getWorkerType() {
      return WorkerType.OPS;
    }
  }
}
