/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

import nl.overheid.aerius.intercept.InterceptRedirect.InterceptRedirectCommand;
import nl.overheid.aerius.intercept.InterceptRemove.InterceptRemoveCommand;
import nl.overheid.aerius.intercept.InterceptRun.InterceptRunCommand;
import nl.overheid.aerius.intercept.InterceptSave.InterceptSaveCommand;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test class for {@link InterceptCommandLineOptions}.
 */
public class InterceptCommandLineOptionsTest {

  @Test
  public void testInterceptRedirect() throws ParseException, FileNotFoundException {
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptRedirect", "inQuename", "outQuename"});
    final InterceptRedirectCommand opts = (InterceptRedirectCommand) cmd.getInterceptOptions();
    assertEquals("redirect inQueue", "inQuename", opts.getInQueueName());
    assertEquals("redirect outQuename", "outQuename", opts.getOutQueueName());
    assertTrue("redirect intercept", cmd.isIntercept());
  }

  @Test
  public void testInterceptRun() throws ParseException, FileNotFoundException {
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptRun", "myQueue", "ops"});
    final InterceptRunCommand opts = (InterceptRunCommand) cmd.getInterceptOptions();
    assertEquals("run qeuename", "myQueue", opts.getQueueName());
    assertSame("run worker type", WorkerType.OPS, opts.getWorkerType());
    assertTrue("run intercept", cmd.isIntercept());
  }

  @Test
  public void testInterceptSave() throws ParseException, FileNotFoundException {
    final String path = new File(getClass().getResource(".").getPath()).getAbsolutePath();
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptSave", "queue", path});
    final InterceptSaveCommand opts = (InterceptSaveCommand) cmd.getInterceptOptions();
    assertEquals("save qeuename", "queue", opts.getQueueName());
    assertEquals("save path", path, opts.getPath().getAbsolutePath());
    assertTrue("save intercept", cmd.isIntercept());
  }

  @Test
  public void testInterceptRemove() throws ParseException, FileNotFoundException {
    final String path = new File(getClass().getResource(".").getPath()).getAbsolutePath();
    final InterceptCommandLineOptions cmd = getOptions(new String[]{"-interceptRemove", "queue", path});
    final InterceptRemoveCommand opts = (InterceptRemoveCommand) cmd.getInterceptOptions();
    assertEquals("remove qeuename", "queue", opts.getQueueName());
    assertEquals("remove path", path, opts.getPath().getAbsolutePath());
    assertTrue("remove intercept", cmd.isIntercept());
  }

  private InterceptCommandLineOptions getOptions(final String[] args) throws ParseException {
    final Options options = new Options();
    options.addOptionGroup(InterceptCommandLineOptions.getOptions());
    final CommandLine cmd = new DefaultParser().parse(options, args);
    return new InterceptCommandLineOptions(cmd);
  }
}
