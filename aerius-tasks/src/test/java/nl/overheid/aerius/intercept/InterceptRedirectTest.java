/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.MissingArgumentException;
import org.junit.Test;

import nl.overheid.aerius.intercept.InterceptRedirect.InterceptRedirectCommand;

/**
 * Test class for {@link InterceptRedirect}.
 */
public class InterceptRedirectTest extends InterceptTestBase {

  @Test(timeout = 10000)
  public void testRedirect() throws IOException, InterruptedException, MissingArgumentException {
    final String result = "result";
    final String inQueue = "inQueue";
    final InterceptRedirect redirect = new InterceptRedirect(new InterceptRedirectCommand(new String[] {inQueue, "outQueue"}));
    final Semaphore lock = mockConsume(result, inQueue);
    redirect.redirect(factory);
    assertTrue("Test intercept redirect possible in deadlock, lock time elapsed", lock.tryAcquire(1, TimeUnit.SECONDS));
  }
}
