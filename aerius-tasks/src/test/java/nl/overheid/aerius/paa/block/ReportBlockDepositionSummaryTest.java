/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.PAARepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.mock.MockCloseableHttpClientUtil;
import nl.overheid.aerius.paa.calculation.CalculationPAAContext;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link ReportBlockDeposition}.
 */
public class ReportBlockDepositionSummaryTest extends BaseDBTest {

  @Test
  public void testGetDepositionImageWithAlotOSources() throws AeriusException, IOException, SQLException, URISyntaxException {
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(getEmissionSourceList());

    final CalculationInputData inputData = new CalculationInputData();
    inputData.setScenario(scenario);
    final List<StringDataSource> gmlStrings = new ArrayList<>();
    gmlStrings.add(new StringDataSource("nonsense", null, null));

    try (CloseableHttpClient client = MockCloseableHttpClientUtil.create()) {
      final CalculationPAAContext paaContext = new CalculationPAAContext(getCalcPMF(), client, Locale.getDefault(), RECEPTOR_GRID_SETTINGS.getEpsg(),
          null, SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale()), scenario,
          PAARepository.determineExportInfo(getCalcConnection(), scenario.getCalculationId(), null, SegmentType.PROJECTS, true));
      final ReportBlockDepositionImage summary = new ReportBlockDepositionImage(null, paaContext);
      final BufferedImage image = summary.getDepositionImage(scenario.getSources(), null);
      final File outputFile = File.createTempFile("savedDepositionImage", ".png");
      outputFile.deleteOnExit();
      ImageIO.write(image, "png", outputFile);
      assertTrue("", outputFile.exists());
    }
  }

  private EmissionSourceList getEmissionSourceList() {
    final EmissionSourceList sourceList = new EmissionSourceList();
    final StringBuilder wkt = new StringBuilder("LINESTRING(");
    for (int i = 0; i < 500; i++) {
      if (i != 0) {
        wkt.append(",");
      }
      wkt.append(i);
      wkt.append(" ");
      wkt.append(i);
    }
    wkt.append(")");
    final EmissionSource source = new GenericEmissionSource(1, new Point(1, 1));
    source.setGeometry(new WKTGeometry(wkt.toString()));
    sourceList.add(source);
    return sourceList;
  }

}
