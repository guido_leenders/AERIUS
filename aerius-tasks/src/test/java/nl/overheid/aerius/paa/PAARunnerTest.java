/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.lowagie.text.DocumentException;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.profile.pas.PASUtil;
import nl.overheid.aerius.db.TestPMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.CalculationRepositoryTestBase;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.mock.MockCloseableHttpClientUtil;
import nl.overheid.aerius.paa.base.PAAExport;
import nl.overheid.aerius.paa.calculation.PAACalculationExport;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.paa.decree.DecreeInformation;
import nl.overheid.aerius.paa.decree.DecreePAAExport;
import nl.overheid.aerius.paa.decree.DetailDecreePAAExport;
import nl.overheid.aerius.paa.melding.PAAMeldingExport;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.melding.BeneficiaryInformation;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.domain.melding.OrganisationInformation;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Unit test for the shared PDF's class. Generates PDF's for each type in {@link PAAReportType}.
 */
@RunWith(Parameterized.class)
public class PAARunnerTest extends CalculationRepositoryTestBase {

  private static final String PAA_EXPORT_SINGLE_TEST = "PAAExportSingleTest";
  private static final String PAA_EXPORT_COMPARISON_TEST = "PAAExportComparisonTest";
  // default page count
  private static final int NUMBER_OF_PAGES_SINGLE = 13;
  private static final int NUMBER_OF_PAGES_COMPARISON = 15;
  // extra pages per type
  private static final int NUMBER_OF_CALCULATION_PAGES = 2;
  private static final int NUMBER_OF_EXTRA_PERMIT_DEMAND_PAGES = 2;
  private static final int NUMBER_OF_EXTRA_PERMIT_DEVELOPMENT_SPACES_PAGES = 2;
  private static final int NUMBER_OF_EXTRA_MELDING_PAGES = 2;
  //deposition overview page = 1
  private static final int NUMBER_OF_EXTRA_DECREE_PAGES = 2;
  private static final int NUMBER_OF_DECREE_DETAIL_PAGES = 6;
  private static final String SINGLE_SCENARIO_GML = "PAAExportSingleTest.zip";
  private static final String SINGLE_COMPARISON_GML = "PAAExportComparisonTest.zip";
  private static final double MAX_RANGE_DISTANCE = 0;
  private final Locale locale;
  private final PAAReportType reportType;
  private Importer importer;
  private CalculationSetOptions options;

  public PAARunnerTest(final PAAReportType reportType, final Locale locale) {
    this.locale = locale;
    this.reportType = reportType;
  }

  @Parameters(name = "{0}-{1}")
  public static List<Object[]> data() {
    final List<Object[]> data = new ArrayList<>();
    for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
      for (final PAAReportType reportType : PAAReportType.values()) {
        data.add(new Object[] {reportType, locale});
      }
    }
    return data;
  }

  @BeforeClass
  public static void beforeClass() {
    // Load fonts to be used for PDF's // temporary for testing in main - is normally done when starting tasks
    FontFactory.registerDirectory(PAARunnerTest.class.getResource("/fonts/").getFile());
  }

  @Before
  @Override
  public void setUp() throws SQLException, AeriusException {
    importer = new Importer(getPMF());
    options = new CalculationSetOptions();
    options.setCalculateMaximumRange(MAX_RANGE_DISTANCE);
    options.setCalculationType(CalculationType.PAS);
    options.getSubstances().add(Substance.NOXNH3);
  }

  @Test(timeout = 60000)
  public void testCreatePdfSingleScenario() throws IOException, AeriusException, SQLException, DocumentException {
    final CalculatedSingle scenario = new CalculatedSingle();

    try (final InputStream is = getFileInputStream(SINGLE_SCENARIO_GML)) {
      final ImportResult result = importer.convertInputStream2ImportResult(SINGLE_SCENARIO_GML, is);
      scenario.setSources(result.getSourceLists().get(0));
      scenario.getCalculationPoints().addAll(result.getCalculationPoints());
      scenario.getCalculation().setYear(TestDomain.YEAR);
    }
    scenario.setOptions(options);
    final int calculationPointSetId = CalculationRepository.insertCalculationPointSet(getConnection(), scenario.getCalculationPoints());
    createCalculation(calculationPointSetId);
    final PartialCalculationResult res = insertCalculationResultsNH3();
    scenario.getCalculation().setCalculationId(res.getCalculationId());
    insertExamplePointResults(scenario.getCalculationPoints(), res.getCalculationId());

    // create developmentSpaceDemands
    final CalculationJob calculatorJob = new CalculationJob(UUID.randomUUID(), scenario, "queue");
    calculatorJob.setProvider(new DefaultCalculationEngineProvider());
    PASUtil.insertCalculationDevelopmentSpaceDemands(getPMF(), calculatorJob);

    final int nrOfPages = getNrOfPages(generatePaaExport(scenario, PAA_EXPORT_SINGLE_TEST));
    assertEquals("Single export nr of pages (" + locale + ") generated doesn't match",
        getNrOfPages(NUMBER_OF_PAGES_SINGLE, PAA_EXPORT_SINGLE_TEST), nrOfPages);
  }

  @Test(timeout = 60000)
  public void testCreatePdfComparisonScenario() throws IOException, AeriusException, SQLException, DocumentException {
    final CalculatedComparison scenario = new CalculatedComparison();
    try (final InputStream is = getFileInputStream(SINGLE_COMPARISON_GML)) {
      final ImportResult result = importer.convertInputStream2ImportResult(SINGLE_COMPARISON_GML, is);
      scenario.setSourcesOne(result.getSourceLists().get(0));
      scenario.setSourcesTwo(result.getSourceLists().get(1));
      scenario.getCalculationPoints().addAll(result.getCalculationPoints());
      scenario.getCalculationOne().setYear(TestDomain.YEAR);
      scenario.getCalculationTwo().setYear(TestDomain.YEAR);
    }
    scenario.setOptions(options);

    final int calculationPointSetId = CalculationRepository.insertCalculationPointSet(getConnection(), scenario.getCalculationPoints());
    createCalculation(calculationPointSetId);
    final PartialCalculationResult res1 = insertCalculationResultsNH3();
    scenario.getCalculationOne().setCalculationId(res1.getCalculationId());
    insertExamplePointResults(scenario.getCalculationPoints(), res1.getCalculationId());

    //Because only 1 calculation is initialized in Before, a second is forced here by letting createCalculation
    //think there doesn't exist one.
    calculation = null;
    calculationResultScale = 1.5;
    createCalculation(calculationPointSetId);
    final PartialCalculationResult res2 = insertCalculationResultsNH3();
    scenario.getCalculationTwo().setCalculationId(res2.getCalculationId());
    insertExamplePointResults(scenario.getCalculationPoints(), res2.getCalculationId());

    // create developmentSpaceDemands
    final CalculationJob calculatorJob2 = new CalculationJob(UUID.randomUUID(), scenario, "queue");
    calculatorJob2.setProvider(new DefaultCalculationEngineProvider());
    PASUtil.insertCalculationDevelopmentSpaceDemands(getPMF(), calculatorJob2);

    try {
      final int nrOfPages = getNrOfPages(generatePaaExport(scenario, PAA_EXPORT_COMPARISON_TEST));
      assertEquals("Comparison export nr of pages (" + locale + ") generated doesn't match",
          getNrOfPages(NUMBER_OF_PAGES_COMPARISON, PAA_EXPORT_COMPARISON_TEST), nrOfPages);
    } catch (final AeriusException e) {
      // No comparisons can be made for PERMIT_DEMAND, expect internal errors
      fail("Unexpected AeriusException");
//      assertTrue("Unexpected AeriusException", PAAReportType.PERMIT_DEMAND == reportType && Reason.INTERNAL_ERROR == e.getReason());
    }
  }

  private int getNrOfPages(final int baseNrOfPages, final String testType) {
    int nrOfPages = baseNrOfPages;
    switch (reportType) {
//    case PERMIT_DEMAND:
//      nrOfPages += NUMBER_OF_EXTRA_PERMIT_DEMAND_PAGES;
//      break;
//    case PERMIT_DEVELOPMENT_SPACES:
//      nrOfPages += NUMBER_OF_EXTRA_PERMIT_DEVELOPMENT_SPACES_PAGES;
//      break;
    case CALCULATION:
      nrOfPages += NUMBER_OF_CALCULATION_PAGES;
      break;
    case DECREE:
      nrOfPages += NUMBER_OF_EXTRA_DECREE_PAGES;
      break;
    case MELDING:
      nrOfPages += NUMBER_OF_EXTRA_MELDING_PAGES;
      break;
    case DETAIL_DECREE:
      //decree detail always has same nr of pages.
      nrOfPages = NUMBER_OF_DECREE_DETAIL_PAGES;
      break;
    default:
      break;
    }
    return nrOfPages;
  }

  private void insertExamplePointResults(final CalculationPointList calculationPoints, final int calculationId)
      throws SQLException {
    final List<AeriusResultPoint> results = new ArrayList<>();
    for (final AeriusPoint p : calculationPoints) {
      final AeriusResultPoint arp = new AeriusResultPoint(p.getId(), p.getPointType(), p.getX(), p.getY());
      arp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, p.getX() * calculationResultScale / 1000);
      arp.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, p.getY() * calculationResultScale / 1000);
      results.add(arp);
    }
    CalculationRepository.insertCalculationResults(getConnection(), calculationId, results);
  }

  private PdfReader generatePaaExport(final CalculatedScenario scenario, final String exportFilename) throws
      IOException, SQLException, DocumentException, AeriusException {
    final CalculationInputData data = getExampleData(scenario);
    final List<StringDataSource> gmlStrings = new ArrayList<>();
    gmlStrings.add(new StringDataSource("In the jungle.. the mighty jungle.. the metadata sleeps tonight...",
        "filename", "mimetype"));

    try (CloseableHttpClient client = MockCloseableHttpClientUtil.create()) {
      final PAAExport<?> export;
      switch (reportType) {
      case CALCULATION:
        export = new PAACalculationExport(getPMF(), client, data, gmlStrings);
        break;
      case MELDING:
        export = getMeldingExport(client, data, gmlStrings);
        break;
//      case PERMIT_DEVELOPMENT_SPACES:
//        export = new PAAPermitDevelopmentSpacesExport(getPMF(), client, data, gmlStrings);
//        break;
//      case PERMIT_DEMAND:
//        export = new PAAPermitDemandExport(getPMF(), client, data, gmlStrings);
//        break;
      case DECREE:
        export = new DecreePAAExport(getPMF(), client, data, gmlStrings, getDecreeExample());
        break;
      case DETAIL_DECREE:
        export = new DetailDecreePAAExport(getPMF(), client, data, gmlStrings, getDecreeExample());
        break;
      default:
        throw new IllegalArgumentException("Unimplemented report in testcase.");
      }
      export.setWatermark(ReleaseType.CONCEPT);

      final byte[] content = writeToDisk(export, exportFilename);
      return new PdfReader(content, export.getOwnerPassword());
    }
  }

  private byte[] writeToDisk(final PAAExport<?> export, final String exportFilename) throws IOException, SQLException, AeriusException {
    final byte[] content;
    try (final FileOutputStream writer =
        new FileOutputStream(getClass().getResource(".").getPath() + exportFilename + "_" + locale + "_" + reportType.name() + ".pdf")) {
      content = export.generatePDF();
      writer.write(content);
      writer.flush();
    }
    return content;
  }

  private CalculationInputData getExampleData(final CalculatedScenario scenario) {
    final CalculationInputData data = new CalculationInputData();
    data.setScenario(scenario);
    scenario.getMetaData().setCorporation("Van Dam installaties bv");
    scenario.getMetaData().setProjectName("Uitbreiding Oostzijde èéê");
    scenario.getMetaData().setReference("SomeReference");
    data.setLocale(locale.getLanguage());
    scenario.getMetaData().setDescription("èéê Dit is de tekst die door de gebruiker kan worden toegevoegd aan de "
        + "berekening. Lorem ipsum dolor sit amet, consectetur; adipiscing elit. Cras ultricies nibh ut est fermentum "
        + "a sagiis urna volutpat. Praesent non lectus et nulla hendrerit eleifend. Proin libero enim, tempor ut "
        + "ultrices in, interdum eu diam. Praesent non lectus et nulla hendrerit eleifend. Proin libero enim, tempor "
        + "ut ultrices in, interdum eu diam. Nullam luctus, magna vel aliquet pulvinar, dui purus dapibus dui, sit "
        + "amet sollicitudin nunc lorem vestibulum neque. Sed et imperdiet leo. In ut mollis lectus. Quisque at ipsum "
        + "augue. Curabitur euismod volutpat erat, quis ullamcorper nibh scelerisque ut. Sed quis ipsum et metus mais "
        + "luctus in vitae orci. Quisque velit enim, adipiscing in mollis in, viverra eu ante. Quisque id ligula "
        + "dolor, quis rhoncus erat. Mauris pulvinar ultrices mollis. Aliquam sit amet fermentum massa. Sed ultrices "
        + "suscipit risus eu aliquet.");
    return data;
  }

  private PAAMeldingExport getMeldingExport(final CloseableHttpClient client, final CalculationInputData data,
      final List<StringDataSource> gmlStrings) throws SQLException, AeriusException {
    final MeldingInformation meldingInfo = new MeldingInformation();
    meldingInfo.setOrganizationOin("12763483");

    final BeneficiaryInformation beneficiaryOrg = getExampleBeneficiaryInfo("Beneficiary");
    meldingInfo.setBenefactorCustom(true);
    meldingInfo.setBeneficiaryInformation(beneficiaryOrg);

    final OrganisationInformation benefactorOrg = getExampleOrganisationInfo("Benefactor");
    meldingInfo.setBenefactorCustom(true);
    meldingInfo.setBenefactorInformation(benefactorOrg);

    final OrganisationInformation executorOrg = getExampleOrganisationInfo("Executor");
    meldingInfo.setExecutorCustom(true);
    meldingInfo.setExecutorInformation(executorOrg);

    meldingInfo.setPreviousMeldingPresent(true);
    meldingInfo.setPreviousMeldingReference("aaaaa3443");

    return new PAAMeldingExport(getPMF(), client, data.getScenario(), data, gmlStrings, meldingInfo);
  }

  private DecreeInformation getDecreeExample()
      throws SQLException {
    final DecreeInformation decreeInfo = new DecreeInformation();
    final Authority decreeAuthority = new Authority();
    decreeAuthority.setCode("NOBRA");
    decreeAuthority.setDescription("Provincie Noord-Brabant");
    decreeInfo.setAuthority(decreeAuthority);
    final Sector sector = new Sector();
    sector.setSectorGroup(SectorGroup.AGRICULTURE);
    sector.setName("Stalemissies");
    decreeInfo.setSector(sector);
    decreeInfo.setRequestState(RequestState.ASSIGNED);
    decreeInfo.setSegment(SegmentType.PRIORITY_PROJECTS);
    decreeInfo.setParentProjectName("Zeer belangrijk hoog prioritair project");
    return decreeInfo;
  }

  private OrganisationInformation getExampleOrganisationInfo(final String organisation) {
    final OrganisationInformation organisationInfo = new OrganisationInformation();
    organisationInfo.setOrganisationName(organisation);
    organisationInfo.setOrganisationAddress(organisation + " address");
    organisationInfo.setOrganisationPostcode(organisation + " postcode");
    organisationInfo.setOrganisationCity(organisation + " city");
    organisationInfo.setOrganisationContactPerson(organisation + " contact person");
    organisationInfo.setOrganisationEmail(organisation + " email");
    return organisationInfo;
  }

  private BeneficiaryInformation getExampleBeneficiaryInfo(final String organisation) {
    final BeneficiaryInformation organisationInfo = new BeneficiaryInformation();
    organisationInfo.setOrganisationName(organisation);
    organisationInfo.setOrganisationAddress(organisation + " address");
    organisationInfo.setOrganisationPostcode(organisation + " postcode");
    organisationInfo.setOrganisationCity(organisation + " city");
    organisationInfo.setOrganisationContactPerson(organisation + " contact person");
    organisationInfo.setOrganisationEmail(organisation + " email");
    return organisationInfo;
  }

  private int getNrOfPages(final PdfReader reader) throws DocumentException, IOException {
    try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      final PdfStamper stamper = new PdfStamper(reader, out);
      final int numberOfPages = stamper.getReader().getNumberOfPages();

      stamper.close();
      return numberOfPages;
    } finally {
      reader.close();
    }
  }

  private TestPMF getPMF() {
    final TestPMF pmf;

    switch (reportType) {
    case CALCULATION:
//    case PERMIT_DEVELOPMENT_SPACES:
//    case PERMIT_DEMAND:
      pmf = getCalcPMF();
      break;
    case MELDING:
    case DECREE:
    case DETAIL_DECREE:
      pmf = getRegPMF();
      break;
    default:
      throw new IllegalArgumentException("Unimplemented report in testcase.");
    }

    return pmf;
  }

  @Override
  protected Connection getConnection() throws SQLException {
    return getPMF().getConnection();
  }
}
