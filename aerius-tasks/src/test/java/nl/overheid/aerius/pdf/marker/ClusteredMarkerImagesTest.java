/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.marker;

import static org.junit.Assert.assertTrue;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.pdf.marker.Marker.MarkerDecoration;

/**
 * Unit test for MarkerImages used for PDF.
 *
 * Test if marker images are generated. It only tests if the images were generated, not if the content was correct.
 */
public class ClusteredMarkerImagesTest {

  private static final int SCALE = 10;

  @Test
  public void testGetMarkersImageWithTriangle() throws IOException, FontFormatException {
    final ArrayList<Marker> markers = getExampleMarkers();
    final Font fontText = getFont();
    final MarkerImages clusteredMarkers = MarkerImagesFactory.createSourceMarkerImage(markers, fontText, SCALE);
    final BufferedImage image = clusteredMarkers.getMarkersWithTriangleImage();
    final File outputfile = getTmpFile("savedWithTriangle.png");
    ImageIO.write(image, "png", outputfile);
    assertTrue("savedWithTriangle.png doesn't exists", outputfile.exists());
    outputfile.delete();
  }

  @Test
  public void testGetMarkersImage() throws IOException, FontFormatException {
    final ArrayList<Marker> markers = getExampleMarkers();
    final Font fontText = getFont();
    final MarkerImages clusteredMarkers = MarkerImagesFactory.createSourceMarkerImage(markers, fontText, SCALE);
    final BufferedImage image = clusteredMarkers.getMarkersImage();
    final File outputfile = getTmpFile("saved.png");
    ImageIO.write(image, "png", outputfile);
    assertTrue("saved.png doesn't exists", outputfile.exists());
    outputfile.delete();
  }

  @Test
  public void testGetRouteMarkersImage() throws IOException, FontFormatException {
    final ArrayList<Marker> markers = getExampleMarkers();
    final Font fontText = getFont();
    final MarkerImages clusteredMarkers = MarkerImagesFactory.createRouteMarkerImage(markers, fontText, SCALE);
    final BufferedImage image = clusteredMarkers.getMarkersImage();
    final File outputfile = getTmpFile("saved_route.png");
    ImageIO.write(image, "png", outputfile);
    assertTrue("saved_route.png doesn't exists", outputfile.exists());
    outputfile.delete();
  }

  private ArrayList<Marker> getExampleMarkers() {
    final ArrayList<Marker> markers = new ArrayList<>();

    markers.add(new Marker(new Point(1, 1), "1", "654641"));

    final Marker marker2 = new Marker(new Point(2, 2), "a", "ABCDEF");
    marker2.addDecoration(MarkerDecoration.UNDERLINE);
    markers.add(marker2);

    final Marker marker3 = new Marker(new Point(3, 3), "A", "F100F1");
    marker3.addDecoration(MarkerDecoration.DOTTED_UNDERLINE);
    markers.add(marker3);

    final Marker marker4 = new Marker(new Point(4, 4), "2", "FDE001");
    marker4.addDecoration(MarkerDecoration.DOTTED_UNDERLINE);
    marker4.addDecoration(MarkerDecoration.UNDERLINE);
    markers.add(marker4);

    return markers;
  }

  private Font getFont() throws FontFormatException, IOException {
    //if fonts are installed on the PC/server, just new Font can be used.
    //return new Font(F.RIJKSOVERHEID_SANS_LF, Font.BOLD, 12);
    //if not, it has to be created through Font.createFont().
    return Font.createFont(Font.TRUETYPE_FONT,
        getClass().getResourceAsStream("/fonts/rijksoverheidsanslf-bold.ttf"));
  }

  private File getTmpFile(final String name) {
    return new File(getClass().getResource(".").getFile(), UUID.randomUUID().toString() + name);
  }
}
