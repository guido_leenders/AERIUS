/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.mock.MockCloseableHttpClientUtil;
import nl.overheid.aerius.worker.util.FallBackImageUtil;

/**
 * Unit test for TMSLayer2Image class.
 */
public class TMSLayer2ImageTest extends TiledLayer2ImageTestBase {

  private static final EPSG EPSG = EPSGProxy.getEPSG(RDNew.SRID);
  private static final double[] RESOLUTIONS = EPSG.getResolutions();
  private static final BBox BOUNDS = EPSG.getBounds();

  @Before
  public void before() throws URISyntaxException, IOException {
    tl = new TMSLayer2Image(EPSG, new URL("https://pdok.aerius.nl/pdok/tms/"), "brtachtergrondkaart@EPSG:28992@png8", getClient()) {
      @Override
      protected Image getTileImageFromUri(final URI uri) {
        try {
        final Image image;
        final File file = new File(getClass().getResource("").getFile() + uri.getPath().replace("@", "").replace(":", ""));

        if (CREATE) {
          file.getParentFile().mkdirs();
          image = super.getTileImageFromUri(uri);
          ImageIO.write((RenderedImage) image, "png", file);
        } else {
          image = ImageIO.read(new FileImageInputStream(file));
        }
        return image;
        } catch (final IOException e) {
          throw new AssertionError(e);
        }
      }
    };
  }

  @Test
  public void testGetImage() throws Exception {
    assertGetImage(new BBox(166387.0959375, 444382.768125, 171330.0559375, 449325.72812499997), 370, 370, "tms_reference_1.png", tl);
    assertGetImage(new BBox(166387.0959375, 444382.768125, 171330.0559375, 449325.72812499997), 500, 500, "tms_reference_2.png", tl);
    assertGetImage(new BBox(166387.0959375, 444382.768125, 171330.0559375, 449325.72812499997), 5000, 5000, "tms_reference_3.png", tl);
    assertGetImage(new BBox(166387.0959375, 444382.768125, 174330.0559375, 449325.72812499997), 538, 336, "tms_reference_4.png", tl);
  }

  @Test
  public void testFallbackGetImage() throws Exception {
    FallBackImageUtil.initFallBackImage();
    final TiledLayer2Image tlFail = new TMSLayer2Image(EPSG, new URL("https://pdokxx.aerius.nl/pdok/tms/"), "brtachtergrondkaart@EPSG:28992@png8",
        MockCloseableHttpClientUtil.create());
    assertGetImage(new BBox(166387.0959375, 444382.768125, 171330.0559375, 449325.72812499997), 370, 370, "tms_reference_fallback.png", tlFail);
  }

  @Test
  public void testGetZoomLevel() {
    //1:1 width to bbox width, 100:256 width to tilewidth * tiles, should show as much detail as possible
    Assert.assertEquals("Zoomlevel 13", 13, tl.getZoomLevel(RESOLUTIONS, 100, new BBox(0, 0, 100, 100)));
    //1:1000 width to bbox width, 100:256 width to tilewidth * tiles, should show very few details
    Assert.assertEquals("Zoomlevel 3", 3, tl.getZoomLevel(RESOLUTIONS, 100, new BBox(0, 0, 100000, 100000)));
    //1:1 width to bbox width, 1000:(256*4) width to tilewidth * tiles, should show OK details
    Assert.assertEquals("Zoomlevel 11", 11, tl.getZoomLevel(RESOLUTIONS, 1000, new BBox(0, 0, 1000, 1000)));
    //1:1 width to bbox width, 10000:(256*40) width to tilewidth * tiles, should show OK details
    Assert.assertEquals("Zoomlevel 11", 11, tl.getZoomLevel(RESOLUTIONS, 10000, new BBox(0, 0, 10000, 10000)));
  }

  @Test
  public void testTileCoordinate() {
    Assert.assertEquals("TileOffset", -65200, tl.tileCoordinate(tl.mapTileSize(RESOLUTIONS, 2), 1, BOUNDS.getMinX()));
    Assert.assertEquals("TileOffset", 292625, tl.tileCoordinate(tl.mapTileSize(RESOLUTIONS, 5), 21, BOUNDS.getMinX()));
    Assert.assertEquals("TileOffset", 135646, tl.tileCoordinate(tl.mapTileSize(RESOLUTIONS, 11), 979, BOUNDS.getMinX()));
    Assert.assertEquals("TileOffset", 166182, tl.tileCoordinate(tl.mapTileSize(RESOLUTIONS, 10), 525, BOUNDS.getMinX()));
  }

  @Test
  public void testToTile() {
    Assert.assertEquals("ToTile", 1, tl.toTile(tl.mapTileSize(RESOLUTIONS, 2), -65200, BOUNDS.getMinX()));
    Assert.assertEquals("ToTile", 20, tl.toTile(tl.mapTileSize(RESOLUTIONS, 5), 292625, BOUNDS.getMinX()));
    Assert.assertEquals("ToTile", 9, tl.toTile(tl.mapTileSize(RESOLUTIONS, 5), 297849, BOUNDS.getMinY()));
    Assert.assertEquals("ToTile", 978, tl.toTile(tl.mapTileSize(RESOLUTIONS, 11), 135646, BOUNDS.getMinX()));
    Assert.assertEquals("ToTile", 1010, tl.toTile(tl.mapTileSize(RESOLUTIONS, 11), 457408, BOUNDS.getMinY()));
    Assert.assertEquals("ToTile", 131, tl.toTile(tl.mapTileSize(RESOLUTIONS, 8), 166387, BOUNDS.getMinX()));
    Assert.assertEquals("ToTile", 122, tl.toTile(tl.mapTileSize(RESOLUTIONS, 8), 444382, BOUNDS.getMinY()));
  }
}
