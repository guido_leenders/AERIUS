/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;

import org.apache.http.HttpException;
import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.geo.shared.BNGrid;
import nl.overheid.aerius.geo.shared.EPSGProxy;

/**
 * Test class for {@link WMSLayer2Image}.
 */
public class WMSLayer2ImageTest extends TiledLayer2ImageTestBase {

  private static final EPSG EPSG = EPSGProxy.getEPSG(BNGrid.SRID);
  private static final String UKBARS = "https://test.aerius.nl/ukbars/map";
  private static final String UK_LAYER = "OS-Scale-Dependent";
  private static final int TILE_SIZE = 250;

  @Before
  public void before() throws MalformedURLException, URISyntaxException {
    tl = new WMSLayer2Image(EPSG, new URL(UKBARS), UK_LAYER, getClient(), TILE_SIZE) {
      @Override
      protected Image getTileImage(final int x, final int y, final int z, final int xMin, final int yMin, final int xMax, final int yMax)
          throws URISyntaxException, ClientProtocolException, IOException, HttpException {
        final Image image;

        final File file = new File(getClass().getResource("").getFile()
            + getURL(x, y, z, xMin, yMin, xMax, yMax).getPath() + '-' + z + '-' + xMin + '-' + yMin);

        if (CREATE) {
          file.getParentFile().mkdirs();

          image = super.getTileImage(x, y, z, xMin, yMin, xMax, yMax);
          ImageIO.write((RenderedImage) image, "png", file);
        } else {
          image = ImageIO.read(new FileImageInputStream(file));
        }
        return image;
      }
    };
  }

  @Test
  public void testGetImage() throws Exception {
    assertGetImage(new BBox(176679.69265, 701435.0109, 179439.69265, 703361.0109), 1380, 963, "wms_reference_1.png", tl);
  }
}
