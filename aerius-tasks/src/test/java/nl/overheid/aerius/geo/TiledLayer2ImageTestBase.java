/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.After;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.util.HttpClientManager;

/**
 * Base class for testing {@link TiledLayer2Image} subclasses.
 */
public class TiledLayer2ImageTestBase {

  private static final Logger LOGGER = LoggerFactory.getLogger(TiledLayer2ImageTestBase.class);

  /**
   * Set this variable to true to create new test cases. On true the test case
   * will query the service and save the images to the test directory. Copy those
   * files to the resources directory to add them as test cases. If false the
   * unit test will read the tile images from file system instead of calling
   * the service.
   */
  protected static final boolean CREATE = false;
  protected TiledLayer2Image tl;

  private CloseableHttpClient httpClient;

  protected CloseableHttpClient getClient() {
    httpClient = HttpClientManager.getRetryingHttpClient(-1);
    return httpClient;
  }

  @After
  public void after() throws IOException {
    httpClient.close();
  }

  protected void assertGetImage(final BBox bbox, final int width, final int height, final String imageName, final TiledLayer2Image tiledLayer2Image)
      throws Exception {
    final Image image = tiledLayer2Image.getImage(bbox, width, height);
    final File file1 = new File(getClass().getResource(imageName).getFile());

    if (CREATE) {
      ImageIO.write((RenderedImage) image, "png", file1);
      LOGGER.info("Created file: '{}'", file1);
    } else {
      final byte[] expectedBytes = Files.readAllBytes(file1.toPath());
      try (final ByteArrayOutputStream actualBAOS = new ByteArrayOutputStream()) {
        ImageIO.write((RenderedImage) image, "png", actualBAOS);
        Assert.assertArrayEquals("GetImage " + imageName, expectedBytes, actualBAOS.toByteArray());
      }
    }
  }
}
