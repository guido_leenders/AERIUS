/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link MailMessageWorker}.
 */
public class MailMessageWorkerTest extends BaseDBTest {

  private static final String DUMMY_EMAIL_ADDRESS = "aerius@example.com";
  private MailMessageWorker worker;
  private MockMailWrapper mailWrapper;

  @Override
  @Before
  public void setUp() throws Exception {
    worker = new MailMessageWorker(getCalcPMF()) {
      @Override
      protected MailWrapper getMailWrapper(final Locale locale) {
        mailWrapper = new MockMailWrapper(getCalcPMF(), locale);
        return mailWrapper;
      }
    };
  }

  @Test
  public void testRun() throws Exception {
    final MailTo mailTo = new MailTo(DUMMY_EMAIL_ADDRESS);
    final Locale locale = LocaleUtils.getDefaultLocale();
    final MailMessageData data = new MailMessageData(MessagesEnum.DEFAULT_FILE_MAIL_SUBJECT, MessagesEnum.DEFAULT_FILE_MAIL_CONTENT,
        locale, mailTo);
    final StringDataSource attachment = new StringDataSource("test", "unimportant", "same");
    data.addAttachment(attachment);
    final boolean send = worker.run(data, null, null);
    assertTrue("Worker result", send);
    assertNotNull("MockMailWrapper should be created", mailWrapper);
    assertTrue("MailWrapper should have send", mailWrapper.isMailSend());
    assertEquals("Mail to used", mailTo, mailWrapper.getEmailTo());
    assertEquals("Mail subject", MessageRepository.getString(getCalcPMF(), MessagesEnum.DEFAULT_FILE_MAIL_SUBJECT, locale), mailWrapper.getSubject());
    //enable next line for quick check on content. It's templated though, so should always fail.
    //assertEquals("Mail content", MessageRepository.getString(getCalcPMF(), MessagesEnum.DEFAULT_FILE_MAIL_CONTENT, locale), mailWrapper.getBody());
    assertTrue("Mail content should be in the body.",
        mailWrapper.getBody().contains(MessageRepository.getString(getCalcPMF(), MessagesEnum.DEFAULT_FILE_MAIL_CONTENT, locale)));
    assertEquals("Mail attachments list size.", 1, mailWrapper.getAttachments().size());
    assertEquals("Mail attachments list size.", attachment, mailWrapper.getAttachments().get(0));
  }

  private static class MockMailWrapper extends MailWrapper {

    private boolean mailSend;
    private MailTo emailTo;
    private List<StringDataSource> attachments;
    private String subject;
    private String body;

    public MockMailWrapper(final PMF pmf, final Locale locale) {
      super(pmf, locale);
    }

    @Override
    protected boolean sendMail(final MailTo emailTo, final List<StringDataSource> attachments, final String actualSubject, final String body)
        throws IOException {
      // No mail is actually send, just return true.
      mailSend = true;
      this.emailTo = emailTo;
      this.attachments = attachments;
      this.subject = actualSubject;
      this.body = body;
      return mailSend;
    }

    boolean isMailSend() {
      return mailSend;
    }

    MailTo getEmailTo() {
      return emailTo;
    }

    List<StringDataSource> getAttachments() {
      return attachments;
    }

    String getSubject() {
      return subject;
    }

    public String getBody() {
      return body;
    }

  }

}
