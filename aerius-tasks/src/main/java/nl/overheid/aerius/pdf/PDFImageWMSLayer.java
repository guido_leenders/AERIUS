/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.worker.util.WMSUtils;
import nl.overheid.aerius.worker.util.WMSUtils.LayerParam;

/**
 * A WMS layer to be used in an image inside a PDF.
 */
public class PDFImageWMSLayer extends PDFImageLayer {

  private final CloseableHttpClient httpClient;
  private final String layerName;
  private final String internalSLD;
  private final String baseURL;

  private final Map<LayerParam, String> layerParameters = new HashMap<>();
  private final EPSG epsg;

  /**
   * @param baseURL The base URL to use when retrieving the wms layer to use for queries.
   * @param httpClient HTTP client to use
   * @param layerName The layer to fetch.
   * @param epsg the coordinate system of the layer.
   * @param intSLD Optional internal sld to use. If not supplied, layername will be used as SLD.
   */
  public PDFImageWMSLayer(final String baseURL, final CloseableHttpClient httpClient, final String layerName, final EPSG epsg, final String intSLD) {
    this.baseURL = baseURL;
    this.httpClient = httpClient;
    this.layerName = layerName;
    this.epsg = epsg;
    this.internalSLD = intSLD;
  }

  @Override
  protected BufferedImage getBaseImage(final BBox bbox, final int width, final int height) throws IOException {
    final BufferedImage wmsImage = WMSUtils.getLayerAsImage(baseURL, getHttpClient(), true, getLayerName(), width, height, bbox, epsg,
        layerParameters, internalSLD);
    if (wmsImage == null) {
      throw new IllegalArgumentException("Could not retrieve wms image " + getLayerName());
    }

    return wmsImage;
  }

  public String getLayerName() {
    return layerName;
  }

  public CloseableHttpClient getHttpClient() {
    return httpClient;
  }

  public void setLayerParameter(final LayerParam key, final String value) {
    layerParameters.put(key, value);
  }

  public void setLayerParameter(final LayerParam key, final Number value) {
    layerParameters.put(key, String.valueOf(value));
  }

}
