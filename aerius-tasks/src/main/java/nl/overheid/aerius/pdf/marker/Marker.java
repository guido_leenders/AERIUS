/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.marker;

import java.util.HashSet;
import java.util.Set;

import nl.overheid.aerius.geo.shared.Point;

/**
 * Class to be used for drawing markers.
 */
public class Marker {

  /**
   * Decoration of the text of the marker.
   */
  public enum MarkerDecoration {
    /**
     * Dotted underline for text.
     */
    DOTTED_UNDERLINE,
    /**
     * Full underline for text.
     */
    UNDERLINE
  }

  private final Set<MarkerDecoration> decorations;
  private String content;
  private String color;
  private Point point;

  /**
   * @param point 
   * @param content 
   * @param color 
   */
  public Marker(final Point point, final String content, final String color) {
    this.point = point;
    this.content = content;
    this.color = color;
    this.decorations = new HashSet<MarkerDecoration>();
  }

  public String getContent() {
    return content;
  }

  public void setContent(final String content) {
    this.content = content;
  }

  public String getColor() {
    return color;
  }

  public void setColor(final String color) {
    this.color = color;
  }

  public Point getPoint() {
    return point;
  }

  public void setPoint(final Point point) {
    this.point = point;
  }

  public Set<MarkerDecoration> getDecorations() {
    return decorations;
  }

  /**
   * @param decoration The decoration to add to the marker.
   */
  public void addDecoration(MarkerDecoration decoration) {
    decorations.add(decoration);
  }

  /**
   * @param decoration The decoration to remove from the marker (if present).
   */
  public void removeDecoration(MarkerDecoration decoration) {
    decorations.remove(decoration);
  }

  /**
   * Remove all decorations from this marker.
   */
  public void removeAllDecorations() {
    decorations.clear();
  }

}
