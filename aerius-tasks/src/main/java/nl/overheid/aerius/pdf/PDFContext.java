/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import org.apache.http.impl.client.CloseableHttpClient;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.LayerRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.TMSLayer2Image;
import nl.overheid.aerius.geo.TiledImageCache;
import nl.overheid.aerius.geo.TiledLayer2Image;
import nl.overheid.aerius.geo.WMSLayer2Image;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerTMSProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.geo.shared.MultiLayerProps;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.util.PDFUtils;
import nl.overheid.aerius.worker.util.TextUtils;

/**
 * Data class containing generic stuff used by pretty much all PDF's.
 */
public abstract class PDFContext implements PDFProvidesTextResources {

  private final PMF pmf;
  private final CloseableHttpClient httpClient;
  private final TiledImageCache tiledImageCache;
  private final String baseURLWMS;
  private final Locale locale;
  private final EPSG epsg;

  /**
   * @param pmf The PMF.
   * @param httpClient The httpClient to use during one whole PDF generation.
   * @param locale the locale used to generate the pdf text.
   * @throws SQLException
   * @throws AeriusException
   */
  public PDFContext(final PMF pmf, final CloseableHttpClient httpClient, final Locale locale, final EPSG epsg) throws SQLException, AeriusException {
    this.pmf = pmf;
    this.httpClient = httpClient;
    this.locale = locale;
    this.epsg = epsg;

    tiledImageCache = createTiledImageCache(pmf, httpClient, epsg);
    this.baseURLWMS = ConstantRepository.getString(pmf, SharedConstantsEnum.INTERNAL_WMS_PROXY_URL);
  }

  private TiledImageCache createTiledImageCache(final PMF pmf, final CloseableHttpClient httpClient, final EPSG epsg)
      throws SQLException, AeriusException {
    final LayerProps baseLayer = getBaseLayer(pmf, new DBMessagesKey(pmf.getProductType(), locale));
    final String name = baseLayer.getName();
    try {
      final TiledLayer2Image tl2i;
      if (baseLayer instanceof LayerTMSProps) {
        tl2i = new TMSLayer2Image(epsg, new URL(((LayerTMSProps) baseLayer).getBaseUrl()), name, httpClient);
      } else if (baseLayer instanceof LayerWMSProps) {
        tl2i = new WMSLayer2Image(epsg, new URL(((LayerWMSProps) baseLayer).getCapabilities().getUrl()), name, httpClient,
            ((LayerWMSProps) baseLayer).getTileSize());
      } else {
        throw new IOException("Trying to load unsupported base layer for pdf:" + baseLayer);
      }
      return new TiledImageCache(tl2i);
    } catch (IOException | URISyntaxException e) {
      throw new IllegalArgumentException("Error creating base layer for pdf for layer + '" + name + "'. Malformed URL in the db?", e);
    }
  }

  private LayerProps getBaseLayer(final PMF pmf, final DBMessagesKey messagesKey) throws SQLException, AeriusException {
    final int paaId = ConstantRepository.getInteger(pmf, ConstantsEnum.PAA_BACKGROUND_MAP_ID);
    try (Connection con = pmf.getConnection()) {
      final MultiLayerProps baseLayer = LayerRepository.getBaseLayer(con, messagesKey);
      for (final LayerProps lp : baseLayer.getLayers()) {
        if (lp.getId() == paaId) {
          return lp;
        }
      }
    }
    throw new IllegalArgumentException("Constant PAA_BACKGROUND_MAP_ID '" + paaId + "' doesn't match any layer in the database");
  }

  public PMF getPmf() {
    return pmf;
  }

  public CloseableHttpClient getHttpClient() {
    return httpClient;
  }

  public Locale getLocale() {
    return locale;
  }

  public EPSG getEpsg() {
    return epsg;
  }

  public TiledImageCache getTiledImageCache() {
    return tiledImageCache;
  }

  public String getBaseURLWMS() {
    return baseURLWMS;
  }

  public DBMessagesKey getDbMessagesKey() {
    return new DBMessagesKey(pmf.getProductType(), locale);
  }

  @Override
  public String getText(final String key) {
    return TextUtils.getText(key, locale);
  }

  @Override
  public String getText(final String key, final Enum<?> enumValue) {
    return TextUtils.getText(key, enumValue, locale);
  }

  public Image getImage(final String filename) throws DocumentException, IOException {
    return PDFUtils.getImage(getResourcesDir() + filename, getLocale());
  }

  public abstract String getResourcesDir();

}
