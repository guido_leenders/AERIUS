/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.worker.util.TextUtils;

/**
 * The base of a PDF. Allows quick creation of PDF files. Should be called in a single-threaded fashion.
 * Calling methods like {@link #getHTTPClient()} in multiple threads might create multiple instances.
 */
public abstract class PDFDocumentBase<T extends PDFContext> {

  private static final Logger LOG = LoggerFactory.getLogger(PDFDocumentBase.class);

  private static final int PAGE_MARGIN_DEFAULT = 36;
  private static final int DEFAULT_PERMISSIONS =
      PdfWriter.ALLOW_PRINTING
          | PdfWriter.ALLOW_DEGRADED_PRINTING
          | PdfWriter.ALLOW_COPY
          | PdfWriter.ALLOW_SCREENREADERS;

  protected PDFDocumentWrapper pdfDocument;

  private PDFMetaData metaData;
  private Rectangle pageSize = PageSize.A4;
  private PdfPageEventHelper headerFooterHelper;
  private int permissions = DEFAULT_PERMISSIONS;
  private float pageMarginLeft = PAGE_MARGIN_DEFAULT;
  private float pageMarginRight = PAGE_MARGIN_DEFAULT;
  private float pageMarginTop = PAGE_MARGIN_DEFAULT;
  private float pageMarginBottom = PAGE_MARGIN_DEFAULT;
  private final T context;

  protected PDFDocumentBase(final T context) {
    this.context = context;
  }

  /**
   * Start generating the PDF.
   * @return returns the pdf as bytecode.
   * @throws DocumentException on PDF errors
   * @throws IOException on I/O errors
   * @throws SQLException on DB specific errors
   * @throws AeriusException on AERIUS validations failing
   */
  public byte[] generatePDF() throws IOException, SQLException, AeriusException {
    try (final PDFDocumentWrapper pdfdoc =
        new PDFDocumentWrapper(pageSize, pageMarginLeft, pageMarginRight, pageMarginTop, pageMarginBottom, permissions)) {
      pdfDocument = pdfdoc; // Because can't assign to protected in try, and want auto close via try.
      metaData = pdfDocument.getMetaData();

      addDefaultMetadata();

      pdfDocument.useHeaderFooterHelper(headerFooterHelper);

      organizeContent();

      addMetaData(metaData);
      addFrontPage();
      newPage();
      addContentPages();

      pdfDocument.finish();

      return postProcess(pdfDocument.getBytes());
    } catch (final Exception e) {
      LOG.error("Error generating PDF", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  /**
   * Add meta data to the PDF in this method. Use the PDFMetaData object to add meta data.
   * By default adds some base fields only.
   */
  protected void addMetaData(final PDFMetaData metaData) {
    addBaseMetaData(getContext().getLocale());
  }

  protected void addBaseMetaData(final Locale locale) {
    metaData.addMetadataTitle(getMetadataTitle(locale));
    metaData.addMetadataAuthor(TextUtils.getText("metadata_author", locale));
    metaData.addMetadataSubject(getMetadataSubject(locale));
    final String metadataKeywords = TextUtils.getText("metadata_keywords", locale);
    if (!StringUtils.isEmpty(metadataKeywords)) {
      metaData.addMetadataKeywords(metadataKeywords);
    }
  }

  protected String getMetadataTitle(final Locale locale) {
    return TextUtils.getText("metadata_title", locale);
  }

  protected String getMetadataSubject(final Locale locale) {
    return TextUtils.getText("metadata_subject", locale);
  }

  /**
   * Method to override when PDF has to do something before generating the front-page
   * (like determining which blocks are used).
   */
  protected void organizeContent() {
    //NO-OP by default
  }

  /**
   * Add a front page to the document. No need to call newPage as default a new page is added.
   */
  protected abstract void addFrontPage() throws SQLException;

  /**
   * Add the content pages to the document. Here all specific pages should be added.
   */
  protected abstract void addContentPages() throws DocumentException, IOException, SQLException, AeriusException;

  /**
   * Post process the generated PDF. This can be used to add page numbering to each page, as this information is only available after the whole
   * document is generated.
   * @param content generated PDF in byte array
   * @return updated PDF in byte array
   */
  private byte[] postProcess(final byte[] content) throws DocumentException, IOException {
    final PdfReader reader = new PdfReader(content, getOwnerPassword());
    try (final ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      final PdfStamper stamper = new PdfStamper(reader, out);

      postProcess(stamper);
      stamper.close();
      return out.toByteArray();
    } finally {
      reader.close();
    }
  }

  /**
   * Post process the generated PDF. This can be used to add page numbering to each page, as this information is only available after the whole
   * document is generated. The default implementation does nothing
   * @param stamper generated PDF in a stamper to be able to add some changes to the PDF
   * @throws DocumentException
   * @throws IOException
   */
  protected void postProcess(final PdfStamper stamper) throws DocumentException, IOException {
    /* No-op by default. */
  }

  private void addDefaultMetadata() {
    metaData.addMetadataCreator("AERIUS " + AeriusVersion.getVersionNumber()); // TODO; going to make AERIUS variable?
  }

  /**
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise.
   * @param headerFooterHelper The header footer helper to set.
   */
  protected void setHeaderFooterHelper(final PdfPageEventHelper headerFooterHelper) {
    this.headerFooterHelper = headerFooterHelper;
  }

  /**
   * Create a new page.
   *
   * @return Whether a new page has been added, see {@link Document#newPage()} for more info.
   */
  public boolean newPage() {
    return pdfDocument.newPage();
  }

  /**
   * Get the writer of the PDF. Only use this if you really know what you are doing!
   * @return {@link PdfWriter}
   */
  public PdfWriter getPdfWriter() {
    return pdfDocument.getPdfWriter();
  }

  /**
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise. Defaults to A4.
   * Common pageSizes can be retrieved using {@link PageSize}.
   * @param pageSize Page size to set.
   */
  public void setPageSize(final Rectangle pageSize) {
    this.pageSize = pageSize;
  }

  /**
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise.
   * @param pageMarginLeft page margin left
   */
  public void setPageMarginLeft(final float pageMarginLeft) {
    this.pageMarginLeft = pageMarginLeft;
  }

  /**
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise.
   * @param pageMarginRight page margin right
   */
  public void setPageMarginRight(final float pageMarginRight) {
    this.pageMarginRight = pageMarginRight;
  }

  /**
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise.
   * @param pageMarginTop page margin top
   */
  public void setPageMarginTop(final float pageMarginTop) {
    this.pageMarginTop = pageMarginTop;
  }

  /**
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise.
   * @param pageMarginBottom page margin bottom
   */
  public void setPageMarginBottom(final float pageMarginBottom) {
    this.pageMarginBottom = pageMarginBottom;
  }

  /**
   * Override the default open permissions (DEFAULT_PERMISSIONS).
   * Must be called before {@link #generatePDF()} is called. Will have no effect otherwise.
   * @param permissions The open permissions for the document.
   */
  protected void setPermissions(final int[] permissions) {
    /* ORing the permissions. */
    this.permissions = 0;
    for (final int p : permissions) {
      this.permissions = this.permissions | p;
    }
  }

  public float getPageMarginLeft() {
    return pageMarginLeft;
  }

  public float getPageMarginRight() {
    return pageMarginRight;
  }

  public float getPageMarginTop() {
    return pageMarginTop;
  }

  public float getPageMarginBottom() {
    return pageMarginBottom;
  }

  public byte[] getOwnerPassword() {
    return pdfDocument.getOwnerPassword();
  }

  public T getContext() {
    return context;
  }

  public PdfPageEventHelper getHeaderFooterHelper() {
    return headerFooterHelper;
  }
}
