/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 */
public class PDFClarifications {

  private final String clarificationMark;
  private final String clarificationSeparator;
  private final Map<String, Integer> clarifications = new LinkedHashMap<>();

  /**
   * Keep track of clarifications that have to be printed on a page.
   * @param clarificationMark The string to use as a mark for the clarification (like *)
   * @param clarificationSeparator The separator to use between marks and the actual clarification (like )
   */
  public PDFClarifications(final String clarificationMark, final String clarificationSeparator) {
    this.clarificationMark = clarificationMark;
    this.clarificationSeparator = clarificationSeparator;
  }

  /**
   * Add a clarification to the current list and retrieve a label with the right number of clarification marks.
   * Ensures a clarification will only be kept once and if there are multiple labels with the same clarification string,
   * the same number of clarification marks will be used.
   *
   * @param label The label string that needs to be clarified.
   * Should contain one *, which will be replaced by the right number of clarification marks.
   * @param clarification The clarification string (without marks).
   * @return The label with the right number of marks.
   */
  public String addClarification(final String label, final String clarification) {
    Integer marks = clarifications.get(clarification);
    if (marks == null) {
      marks = clarifications.size() + 1;
      clarifications.put(clarification, marks);
    }
    return getClarifiedLabel(label, marks);
  }

  /**
   * @return The list of current clarificatons.
   */
  public List<String> getClarificationTexts() {
    final List<String> actualClarifications = new ArrayList<>();
    for (final Entry<String, Integer> entry : clarifications.entrySet()) {
      actualClarifications.add(getClarificationPrefix(entry.getValue()) + entry.getKey());
    }
    return actualClarifications;
  }

  /**
   * Reset the current clarifications.
   */
  public void reset() {
    clarifications.clear();
  }

  private String getClarifiedLabel(final String label, final int numberOfMarks) {
    final StringBuilder sb = new StringBuilder();
    for (int i = 1; i <= numberOfMarks; i++) {
      sb.append(clarificationMark);
    }
    return label.replaceAll("\\*", sb.toString());
  }

  private String getClarificationPrefix(final int numberOfMarks) {
    final StringBuilder sb = new StringBuilder();
    for (int i = 1; i <= numberOfMarks; i++) {
      sb.append(clarificationMark);
    }
    sb.append(clarificationSeparator);
    sb.append(' ');
    return sb.toString();
  }

}
