/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.lowagie.text.Font;
import com.lowagie.text.Phrase;

import nl.overheid.aerius.pdf.font.F;
import nl.overheid.aerius.worker.util.TextUtils;

/**
 *
 */
public abstract class TextPDFColumn<T> extends PhrasePDFColumn<T> {

  private static final Font DEFAULT_FONT = F.getFont(F.RIJKSOVERHEID_SANS_TEXT);
  private static final String DEFAULT_EMPTY_STRING = " ";

  private Font font = DEFAULT_FONT;
  private String emptyStringLabel = DEFAULT_EMPTY_STRING;

  protected final Locale locale;

  public TextPDFColumn(final Locale locale, final PDFColumnProperties columnProperties) {
    super(columnProperties);
    this.locale = locale;
  }

  @Override
  protected Phrase getPhrase(final T data) {
    final String text = getText(data);
    return new Phrase(StringUtils.isEmpty(text) ? emptyStringLabel : text, font);
  }

  protected abstract String getText(T data);

  public Font getFont() {
    return font;
  }

  public void setFont(final Font font) {
    this.font = font;
  }

  public String getEmptyStringLabel() {
    return emptyStringLabel;
  }

  public void setEmptyStringLabel(final String emptyStringLabel) {
    this.emptyStringLabel = emptyStringLabel;
  }

  protected String getText(final String key) {
    return TextUtils.getText(key, locale);
  }

  protected String getText(final String key, final Enum<?> enumValue) {
    return TextUtils.getText(key, enumValue, locale);
  }

}
