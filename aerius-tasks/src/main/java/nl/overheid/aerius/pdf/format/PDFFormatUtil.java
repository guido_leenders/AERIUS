/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.format;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.deposition.PasValueUtil;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 *
 */
public final class PDFFormatUtil {

  private static final double BORDER_DISTANCE_VALUE_TOO_LOW = 0.1;
  // Switch from meters to kilometers if this value or higher.
  private static final double BORDER_DISTANCE_VALUE_KM = 10000.0d;

  private static final String UNIT_METER = "m";
  private static final String UNIT_KILOMETER = "km";

  private static final String BELOW_BORDER_VALUE_PREFIX = "< ";

  private PDFFormatUtil() {
    //util class
  }

  /**
   * Format the given value. If below the border value, it will be displayed as '< [borderValueLow]'.
   * @param value value
   * @return formatted number
   */
  public static String format(final Number value, final double borderValueLow, final NumberFormat formatter) {
    String returnValue = "";

    if (value != null) {
      final Number numValue = value;

      if (numValue.doubleValue() > 0 && numValue.doubleValue() < borderValueLow) {
        returnValue = BELOW_BORDER_VALUE_PREFIX + formatter.format(borderValueLow);
      } else if (numValue.doubleValue() < 0 && numValue.doubleValue() > -1 * borderValueLow) {
        returnValue = BELOW_BORDER_VALUE_PREFIX + formatter.format(-borderValueLow);
      } else {
        returnValue = formatter.format(numValue.doubleValue());
      }
    }

    return returnValue;
  }

  /**
   * Format as deposition value.
   * @param locale The locale to use when formatting.
   * @param value value to format.
   * @return formatted value
   */
  public static String formatDeposition(final Locale locale, final double value) {
    final NumberFormat formatter = PDFUtils.getNumberFormat(locale, 2);
    formatter.setRoundingMode(RoundingMode.HALF_UP);
    return PasValueUtil.pasProofValue(value) + formatter.format(value);
  }

  /**
   * Format demand deposition value, show new sign for values below demand_tresholds
   * @param locale The locale to use when formatting.
   * @param value value to format.
   * @return formatted value
   */
  public static String formatDemandDeposition(final Locale locale, final double value) {
    final NumberFormat formatter = PDFUtils.getNumberFormat(locale, 2);
    formatter.setRoundingMode(RoundingMode.HALF_UP);
    return PasValueUtil.pasProofValue(value, true) + formatter.format(PasValueUtil.pasProofValueOverwrite(value));
  }

  /**
   * Format as delta deposition value.
   * @param locale The locale to use when formatting.
   * @param value value to format.
   * @return formatted value
   */
  public static String formatDeltaDeposition(final Locale locale, final double value) {
    final String preText = value > 0 ? "+ " : "";
    return preText + formatDeposition(locale, value).replaceAll("< \\-", "-< ").replaceAll("\\-", "- ");
  }

  /**
   * Format as delta deposition value.
   * @param locale The locale to use when formatting.
   * @param value value to format.
   * @return formatted value
   */
  public static String formatDepositionDeltaToSignlessZero(final Locale locale, final double value) {
    final String innerText = formatDeltaDeposition(locale, value);
    final String localizedZero = formatDeposition(locale, 0);
    return innerText.matches("[+-] " + localizedZero + "$") ? localizedZero : innerText;
  }

  /**
   * Format as distance (adjusting for
   * @param locale
   * @param value
   * @return
   */
  public static String formatDistance(final Locale locale, final double value) {
    final StringBuilder output = new StringBuilder();
    boolean isInMeters = true;

    if (value > 0 && value < BORDER_DISTANCE_VALUE_TOO_LOW) {
      output.append(BELOW_BORDER_VALUE_PREFIX + formatMeters(locale, BORDER_DISTANCE_VALUE_TOO_LOW));
    } else if (value < 0 && value > -1 * BORDER_DISTANCE_VALUE_TOO_LOW) {
      output.append(BELOW_BORDER_VALUE_PREFIX + formatMeters(locale, -BORDER_DISTANCE_VALUE_TOO_LOW));
    } else if (value >= BORDER_DISTANCE_VALUE_KM) {
      output.append(formatKilometers(locale, value / SharedConstants.M_TO_KM));
      isInMeters = false;
    } else {
      output.append(formatMeters(locale, value));
    }

    output.append(' ').append(isInMeters ? UNIT_METER : UNIT_KILOMETER);

    return output.toString();
  }

  public static String formatMeters(final Locale locale, final double value) {
    return PDFUtils.getNumberFormat(locale, 0).format(value);
  }

  public static String formatKilometers(final Locale locale, final double value) {
    return PDFUtils.getNumberFormat(locale, 1).format(value);
  }

 /**
  * Formats the exceedingDeposition value. Ouputs it only when the value differs from the the deposition value.
  *
  * @param locale
  * @param deposition
  * @param exceedingDeposition
  * @param delta
  * @return
  */
  public static String formatAndFilterExceedingDeposition(final Locale locale, final double deposition, final Double exceedingDeposition,
      final boolean delta) {
    final String depositionString = delta ? PDFFormatUtil.formatDeltaDeposition(locale, deposition)
      : PDFFormatUtil.formatDeposition(locale, deposition);
    final String exceedingDepositionString;
    if (exceedingDeposition == null || exceedingDeposition == 0) {
      exceedingDepositionString = null;
    } else {
      exceedingDepositionString =
        delta ? PDFFormatUtil.formatDeltaDeposition(locale, exceedingDeposition)
          : PDFFormatUtil.formatDeposition(locale, exceedingDeposition);
    }

    String result = "";
    // yes, the equality check is done on the rounded/formatted values (i.e. String) on purpose!
    if (exceedingDepositionString != null && !depositionString.equals(exceedingDepositionString)) {
      result = exceedingDepositionString;
    }

    return result;
  }

}
