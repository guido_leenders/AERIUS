/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.http.impl.client.CloseableHttpClient;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Contains the core needed for a report block.
 */
public abstract class ReportBlock<T extends PDFContext> {

  protected final T context;
  protected final PDFDocumentWrapper pdfDocument;

  protected ReportBlock(final PDFDocumentWrapper pdfDocument, final T pdfContext) {
    this.pdfDocument = pdfDocument;
    this.context = pdfContext;
  }

  protected abstract void generate() throws AeriusException, DocumentException, IOException, SQLException;

  public PMF getPmf() {
    return context.getPmf();
  }

  public CloseableHttpClient getHttpClient() {
    return context.getHttpClient();
  }

  protected PDFImageWMSLayer getWMSLayer(final String layerName, final String sldName) {
    return new PDFImageWMSLayer(context.getBaseURLWMS(), getHttpClient(), layerName, context.getEpsg(), sldName);
  }

  protected PDFImageWMSLayer getProductWMSLayer(final String layerName, final String sldName) {
    return new PDFImageWMSLayer(context.getBaseURLWMS(), getHttpClient(), getPmf().getProductType().getLayerName(layerName), context.getEpsg(),
        sldName == null ? null : getPmf().getProductType().getLayerName(sldName));
  }

  protected PDFImageWMSLayer getProductReportWMSLayer(final String layerName, final String sldName) {
    return new PDFImageWMSLayer(context.getBaseURLWMS(), getHttpClient(), getPmf().getProductType().getReportLayerName(layerName), context.getEpsg(),
        sldName == null ? null : getPmf().getProductType().getLayerName(sldName));
  }

}
