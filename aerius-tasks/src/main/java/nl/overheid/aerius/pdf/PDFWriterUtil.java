/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import java.awt.Color;
import java.text.MessageFormat;

import com.lowagie.text.Chunk;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

import nl.overheid.aerius.pdf.font.PDFFonts;

/**
 * Util class for PdfWriter related methods.
 */
public final class PDFWriterUtil {

  private PDFWriterUtil() {
    //util class
  }

  public static void addImage(final PdfWriter writer, final Image image, final float posX, final float posY) {
    addPhrase(writer, new Phrase(new Chunk(image, 0, 0)), posX, posY);
  }

  public static void addText(final PdfWriter writer, final String text, final PDFFonts font, final float posX, final float posY) {
    addPhrase(writer, new Phrase(text, font.get()), posX, posY);
  }

  public static void addText(final PdfWriter writer, final String text, final PDFFonts font, final float posX, final float posY,
      final PDFAlignment alignment) {
    addPhrase(writer, new Phrase(text, font.get()), posX, posY, alignment);
  }

  /**
   * Uses default LEFT alignment.
   * @param writer Writer to use.
   * @param phrase The phrase to display.
   * @param posX The position to start from, x coordinate on page.
   * @param posY The position to start from, y coordinate on page.
   */
  public static void addPhrase(final PdfWriter writer, final Phrase phrase, final float posX, final float posY) {
    addPhrase(writer, phrase, posX, posY, PDFAlignment.LEFT);
  }

  /**
   * @param writer Writer to use.
   * @param phrase The phrase to display.
   * @param posX The position to start from, x coordinate on page.
   * @param posY The position to start from, y coordinate on page.
   * @param alignment The alignment to use for the element.
   */
  public static void addPhrase(final PdfWriter writer, final Phrase phrase, final float posX, final float posY, final PDFAlignment alignment) {
    addPhrase(writer.getDirectContent(), phrase, posX, posY, alignment);
  }

  /**
   * @param writer Writer to use.
   * @param phrase The phrase to display.
   * @param posX The position to start from, x coordinate on page.
   * @param posY The position to start from, y coordinate on page.
   * @param alignment The alignment to use for the element.
   */
  public static void addPhrase(final PdfContentByte contentByte, final Phrase phrase, final float posX, final float posY, final PDFAlignment alignment) {
    ColumnText.showTextAligned(
        contentByte,
        alignment.getAlignOption(),
        phrase,
        posX,
        posY,
        0);
  }

  /**
   *  Add age numbers for all pages, starting at fromPage. Will be something like {page}/{totalPages}.
   */
  public static void addPageNumbers(final PdfStamper stamper, final int fromPage, final PDFProvidesTextResources textResources, final PDFFonts font,
      final float posX, final float posY) {
    for (int i = fromPage; i <= stamper.getReader().getNumberOfPages(); ++i) {
      addPageNumber(stamper, i, textResources, font, posX, posY);
    }
  }

  /**
   * Add a page number for a single page. Will be something like {page}/{totalPages}.
   */
  public static void addPageNumber(final PdfStamper stamper, final int page, final PDFProvidesTextResources textResources,
      final PDFFonts font, final float posX, final float posY) {
    final int totalPages = stamper.getReader().getNumberOfPages();
    final PdfContentByte contentByte = stamper.getOverContent(page);

    ColumnText.showTextAligned(
        contentByte,
        Element.ALIGN_RIGHT,
        new Phrase(MessageFormat.format(textResources.getText("footer_page"), page, totalPages),
            font.get()),
        posX,
        posY,
        0);
  }

  /**
   * Draws a colored rectangle.
   * @param writer the writer to use to draw the rectangle
   * @param border whether to add border
   * @param color the Color
   * @param x the X coordinate
   * @param y the Y coordinate
   * @param width the width of the rectangle
   * @param height the height of the rectangle
   */
  public static void addColorRectangle(final PdfWriter writer, final boolean border, final Color color, final float x, final float y,
      final float width, final float height) {
    final PdfContentByte canvas = writer.getDirectContent();
    canvas.saveState();
    canvas.setColorFill(color);
    canvas.rectangle(x, y, width, height);
    if (border) {
      canvas.fillStroke();
    } else {
      canvas.fill();
    }
    canvas.restoreState();
  }
}
