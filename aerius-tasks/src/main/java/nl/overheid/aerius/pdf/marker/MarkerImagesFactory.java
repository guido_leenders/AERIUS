/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.marker;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

/**
 * Factory to create various MarkerImages.
 */
public final class MarkerImagesFactory {

  private static final int SOURCE_MARKER_HEIGHT = 36;
  private static final int SOURCE_MARKER_WIDTH = 42;
  private static final int SOURCE_BORDER = 3;
  private static final int SOURCE_SPACING = 2;
  private static final int SOURCE_ARC_WIDTH = 15;
  private static final int SOURCE_ARC_HEIGHT = 15;
  private static final Color SOURCE_BACKGROUND_COLOR = Color.BLACK;
  private static final Color SOURCE_TEXT_COLOR = Color.WHITE;
  private static final int SOURCE_FONT_SIZE = 19;
  private static final int SOURCE_DECORATION_HEIGHT = 1;
  private static final int SOURCE_DECORATION_SPACING = 2;
  private static final int SOURCE_DECORATION_DOTTED_SPACING = 4;
  private static final int SOURCE_TRIANGLE_DISPLACEMENT = 6;

  private static final int ROUTE_MARKER_HEIGHT = 36;
  private static final int ROUTE_MARKER_WIDTH = 36;
  private static final int ROUTE_BORDER = 0;
  private static final int ROUTE_SPACING = 0;
  private static final int ROUTE_ARC_WIDTH = 36;
  private static final int ROUTE_ARC_HEIGHT = 36;
  private static final Color ROUTE_BACKGROUND_COLOR = Color.BLUE;
  private static final Color ROUTE_TEXT_COLOR = Color.WHITE;
  private static final int ROUTE_FONT_SIZE = 19;
  private static final int ROUTE_DECORATION_HEIGHT = 1;
  private static final int ROUTE_DECORATION_SPACING = 2;
  private static final int ROUTE_DECORATION_DOTTED_SPACING = 4;
  private static final int ROUTE_TRIANGLE_DISPLACEMENT = 6;

  // Not to be constructed.
  private MarkerImagesFactory() { }

  /**
   * @param markers The markers to use.
   * @param font To font to use.
   * @param scale The scale to use.
   * @return Source MarkerImages.
   */
  public static MarkerImages createSourceMarkerImage(final List<Marker> markers, final Font font, final int scale) {
    final MarkerImages markerImages = new MarkerImages(markers, font, scale);
    setSourceProperties(markerImages);
    return markerImages;
  }

  /**
   * @param marker The marker to use.
   * @param font To font to use.
   * @param scale The scale to use.
   * @return Source MarkerImages.
   */
  public static MarkerImages createSourceMarkerImage(final Marker marker, final Font font, final int scale) {
    final MarkerImages markerImages = new MarkerImages(marker, font, scale);
    setSourceProperties(markerImages);
    return markerImages;
  }

  /**
   * @param markers The markers to use.
   * @param font To font to use.
   * @param scale The scale to use.
   * @return Route MarkerImages.
   */
  public static MarkerImages createRouteMarkerImage(final List<Marker> markers, final Font font, final int scale) {
    final MarkerImages markerImages = new MarkerImages(markers, font, scale);
    setRouteProperties(markerImages);
    return markerImages;
  }

  /**
   * @param marker The marker to use.
   * @param font To font to use.
   * @param scale The scale to use.
   * @return Route MarkerImages.
   */
  public static MarkerImages createRouteMarkerImage(final Marker marker, final Font font, final int scale) {
    final MarkerImages markerImages = new MarkerImages(marker, font, scale);
    setRouteProperties(markerImages);
    return markerImages;
  }

  private static void setSourceProperties(final MarkerImages markerImages) {
    markerImages.setMarkerHeight(SOURCE_MARKER_HEIGHT);
    markerImages.setMarkerWidth(SOURCE_MARKER_WIDTH);
    markerImages.setBorder(SOURCE_BORDER);
    markerImages.setSpacing(SOURCE_SPACING);
    markerImages.setArcWidth(SOURCE_ARC_WIDTH);
    markerImages.setArcHeight(SOURCE_ARC_HEIGHT);
    markerImages.setBackgroundColor(SOURCE_BACKGROUND_COLOR);
    markerImages.setTextColor(SOURCE_TEXT_COLOR);
    markerImages.setFontSize(SOURCE_FONT_SIZE);
    markerImages.setDecorationHeight(SOURCE_DECORATION_HEIGHT);
    markerImages.setDecorationSpacing(SOURCE_DECORATION_SPACING);
    markerImages.setDecorationDottedSpacing(SOURCE_DECORATION_DOTTED_SPACING);
    markerImages.setTriangleDisplacement(SOURCE_TRIANGLE_DISPLACEMENT);
  }

  private static void setRouteProperties(final MarkerImages markerImages) {
    markerImages.setMarkerHeight(ROUTE_MARKER_HEIGHT);
    markerImages.setMarkerWidth(ROUTE_MARKER_WIDTH);
    markerImages.setBorder(ROUTE_BORDER);
    markerImages.setSpacing(ROUTE_SPACING);
    markerImages.setArcWidth(ROUTE_ARC_WIDTH);
    markerImages.setArcHeight(ROUTE_ARC_HEIGHT);
    markerImages.setBackgroundColor(ROUTE_BACKGROUND_COLOR);
    markerImages.setTextColor(ROUTE_TEXT_COLOR);
    markerImages.setFontSize(ROUTE_FONT_SIZE);
    markerImages.setDecorationHeight(ROUTE_DECORATION_HEIGHT);
    markerImages.setDecorationSpacing(ROUTE_DECORATION_SPACING);
    markerImages.setDecorationDottedSpacing(ROUTE_DECORATION_DOTTED_SPACING);
    markerImages.setTriangleDisplacement(ROUTE_TRIANGLE_DISPLACEMENT);
  }
}
