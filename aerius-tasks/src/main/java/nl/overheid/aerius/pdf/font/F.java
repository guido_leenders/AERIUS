/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.font;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.pdf.BaseFont;

/**
 * PDF font utility. Will be used a LOT, that's why this class name was chosen.
 */
public final class F {

  /** Bold style. */
  public static final String FONT_STYLE_BOLD = "-Bold";
  /**  Italic style. */
  public static final String FONT_STYLE_ITALIC = "-Italic";
  /**  Bold-Italic style. */
  public static final String FONT_STYLE_BOLDITALIC = "-BoldItalic";

  /**  Rijksoverheid Sans Heading. */
  public static final String RIJKSOVERHEID_SANS_HEADING = "RijksoverheidSansHeading";
  /**  Rijksoverheid Sans Heading - Bold. */
  public static final String RIJKSOVERHEID_SANS_HEADING_BOLD = RIJKSOVERHEID_SANS_HEADING + FONT_STYLE_BOLD;

  /**  Rijksoverheid Sans Text. */
  public static final String RIJKSOVERHEID_SANS_TEXT = "RijksoverheidSansText";
  /**  Rijksoverheid Sans Text - Italic. */
  public static final String RIJKSOVERHEID_SANS_TEXT_ITALIC = RIJKSOVERHEID_SANS_TEXT + FONT_STYLE_ITALIC;

  /**  Rijksoverheid Sans LF. */
  public static final String RIJKSOVERHEID_SANS_LF = "Rijksoverheid SansLF";

  private static final String FONT_ENCODING = "Cp1252";

  /**  Not to be constructed or extended. */
  private F() { }

  /**
   * Get the Font for given font name.
   * @param fontName The font to retrieve
   * @return The font.
   */
  public static Font getFont(final String fontName) {
    final Font font = FontFactory.getFont(fontName, FONT_ENCODING, BaseFont.EMBEDDED);
    if (font.getBaseFont() == null) {
      throw new IllegalArgumentException("Font does not exist: " + fontName);
    }

    return font;
  }

}
