/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

import nl.overheid.aerius.pdf.PDFAlignment;
import nl.overheid.aerius.pdf.font.F;
import nl.overheid.aerius.pdf.table.cell.AlignCellProcessor;
import nl.overheid.aerius.pdf.table.cell.CellProcessor;
import nl.overheid.aerius.pdf.table.cell.TableRow;
import nl.overheid.aerius.pdf.table.cell.TableRowProcessor;
import nl.overheid.aerius.pdf.table.cell.TableRowProcessors;
import nl.overheid.aerius.worker.util.PDFUtils;
import nl.overheid.aerius.worker.util.TextUtils;

/**
 * The base table. Settings configurable here should be called in the constructor.
 */
public abstract class PDFColumnTable<T> implements IsPDFTable {

  private static final String BACKGROUNDCOLOR_HEADER = "F3F3F4";
  private static final String BACKGROUNDCOLOR_ALTERNATING_ROW = "FAFAFB";

  private static final float HEADER_CELL_PADDING_BOTTOM = 7;

  private static final float DEFAULT_CELL_PADDING_TOP = 6;
  private static final float DEFAULT_CELL_PADDING_BOTTOM = 12;
  private static final float DEFAULT_CELL_PADDING_LEFT = 4;
  private static final float DEFAULT_CELL_PADDING_RIGHT = 4;

  private static final float DEFAULT_LEADING = 1.3f;

  private final TableRowProcessors cellProcessors = new TableRowProcessors();

  private final Locale locale;

  /* The header font to use. */
  private Font headerFont = F.getFont(F.RIJKSOVERHEID_SANS_HEADING_BOLD);

  /** Whether to alternate rows using a funky color. Defaults to true.
   *  Use {@link #setAlternatingRowColor(String)} to change the color. */
  private boolean alternatingRows = true;

  /* The color to use when alternating rows. In format: "#code" i.e. "#FFFFFF" etc. Defaults to: {@value #BACKGROUNDCOLOR_ALTERNATING_ROW}. */
  private String alternatingRowColor = BACKGROUNDCOLOR_ALTERNATING_ROW;

  private final List<AbstractPDFColumn<T>> columns = new ArrayList<>();
  private List<T> data = new ArrayList<>();
  private boolean useHeader = true;

  /**
   * @param locale The locale to use.
   */
  public PDFColumnTable(final Locale locale) {
    this.locale = locale;
    init();
  }

  protected void init() {
    // Add to all headers.
    addRowProcessor(new TableRowProcessor(new CellProcessor() {

      @Override
      public void processCell(final PdfPCell cell) {
        cell.setBackgroundColor(PDFUtils.getColor(BACKGROUNDCOLOR_HEADER));
        cell.setBorderWidth(0);
        cell.setPaddingLeft(DEFAULT_CELL_PADDING_LEFT);
        cell.setPaddingTop(3);
        cell.setPaddingBottom(HEADER_CELL_PADDING_BOTTOM);
      }
    }, TableRow.HEADER));

    // Add to all normal cells - default styling and such.
    addRowProcessor(new TableRowProcessor(new CellProcessor() {

      @Override
      public void processCell(final PdfPCell cell) {
        applyDefaultCellSettings(cell);
        cell.setLeading(0, DEFAULT_LEADING);
      }
    }, TableRow.NON_HEADER));
  }

  protected String getText(final String key) {
    return getText(key, locale);
  }

  protected String getText(final String key, final Enum<?> enumValue) {
    return getText(key, enumValue, locale);
  }

  protected static String getText(final String key, final Locale locale) {
    return TextUtils.getText(key, locale);
  }

  protected static String getText(final String key, final Enum<?> enumValue, final Locale locale) {
    return TextUtils.getText(key, enumValue, locale);
  }

  public List<T> getData() {
    return data;
  }

  public void setData(final List<T> data) {
    this.data = data;
  }

  public void addColumnRightAligned(final AbstractPDFColumn<T> column) {
    column.addProcessor(new AlignCellProcessor(PDFAlignment.RIGHT));
    addColumn(column);
  }

  public void addColumn(final AbstractPDFColumn<T> column) {
    columns.add(column);
  }

  public int getColumnCount() {
    return columns.size();
  }

  private void applyDefaultCellSettings(final PdfPCell cell) {
    cell.setBorderWidth(0);
    cell.setPaddingTop(DEFAULT_CELL_PADDING_TOP);
    cell.setPaddingBottom(DEFAULT_CELL_PADDING_BOTTOM);
    cell.setPaddingLeft(DEFAULT_CELL_PADDING_LEFT);
    cell.setPaddingRight(DEFAULT_CELL_PADDING_RIGHT);
  }

  /**
   * Manually apply all header cells. Use this only if you have overridden {@link #addHeaders(PDFSimpleTable)} and want to apply the styles after.
   * @param table The table to apply this on.
   */
  protected void applyHeaderCellStyleManually(final PDFSimpleTable table) {
    final PdfPCell[] headerCells = table.get().getRow(0).getCells();
    for (int i = 0; i < headerCells.length; ++i) {
      if (headerCells[i] != null) {
        cellProcessors.processCell(headerCells[i], TableRow.HEADER);
      }
    }
  }

  /**
   * Creates a {@link PdfPTable} with the data/styling and such.
   * @return The table.
   */
  @Override
  public PdfPTable get() {
    final PDFSimpleTable table = new PDFSimpleTable(determineRelativeWidths());

    /* Apply the default cell settings we're going to use to the default cell of the table also. The default
     *  cell of the table is used for pdfPTable.addCell() except for the one where a PdfPCell is given. */
    applyDefaultCellSettings(table.get().getDefaultCell());

    beforeGenerating();

    fillTable(table);

    return table.get();
  }

  @Override
  public PdfPCell getAsCell() {
    return PDFSimpleTable.applyDefaultCellSettings(new PdfPCell(get()));
  }

  protected abstract void beforeGenerating();

  /**
   * Set fonts for all columns.
   * Use after adding all columns.
   * @param font The font to set for cells.
   */
  protected void setCellFont(final Font font) {
    for (final AbstractPDFColumn<T> column : columns) {
      if (column instanceof TextPDFColumn) {
        ((TextPDFColumn<T>) column).setFont(font);
      }
    }
  }

  private PDFRelativeWidths determineRelativeWidths() {
    final float[] relativeWidths = new float[columns.size()];
    int i = 0;
    for (final AbstractPDFColumn<T> column : columns) {
      relativeWidths[i++] = column.getRelativeWidth();
    }
    return new PDFRelativeWidths(relativeWidths);
  }

  /**
   * Fill the table with the data. Also use the {@link PDFCellProcessor}'s to format/style them.
   * @param table The {@link PDFSimpleTable} to fill.
   * @param data The data to use.
   */
  private void fillTable(final PDFSimpleTable table) {
    if (data == null) {
      return;
    }

    addHeaders(table);

    addTableContent(table);

    addFooters(table);
  }

  private <E extends Enum<?>> void addTableContent(final PDFSimpleTable table) {
    boolean alternating = false;
    for (final T row : data) {
      for (final AbstractPDFColumn<T> column : columns) {
        final PdfPCell cell = column.getCell(row);

        if (alternatingRows && alternating) {
          cell.setBackgroundColor(PDFUtils.getColor(alternatingRowColor));
        }

        processContentCell(cell, TableRow.NON_HEADER, column);

        if (cell.getColspan() > 0) {
          table.add(cell);
        }
      }

      alternating ^= true;
    }
  }

  protected void addHeaders(final PDFSimpleTable table) {
    // Add headers if present.
    if (useHeader) {
      table.setHeaderRows(1); // will make sure the header is repeated on every page if the table is continued on multiple pages.

      for (final AbstractPDFColumn<T> column : columns) {
        final String headerText = StringUtils.isEmpty(column.getHeaderI18nKey()) ? "" : getText(column.getHeaderI18nKey());
        final PdfPCell headerCell = createAndGetHeaderCell(column, headerText);

        if (headerCell.getColspan() > 0) {
          table.add(headerCell);
        }
      }
    }
  }

  protected void addFooters(final PDFSimpleTable table) {
    //NO-OP by default.
  }

  protected PdfPCell createAndGetHeaderCell(final AbstractPDFColumn<T> column, final String text) {
    final PdfPCell headerCell = PDFSimpleTable.getNewCellWithDefaultSettings();
    headerCell.setPhrase(new Phrase(text, headerFont));
    cellProcessors.processCell(headerCell, TableRow.HEADER, column);

    return headerCell;
  }

  protected void processContentCell(final PdfPCell cell, final TableRow row, final AbstractPDFColumn<T> column) {
    cellProcessors.processCell(cell, row, column);
  }

  /**
   * Add a cell processor.
   * @param processor The processor to add.
   */
  public void addRowProcessor(final TableRowProcessor processor) {
    cellProcessors.addProcessor(processor);
  }

  public boolean isAlternatingRows() {
    return alternatingRows;
  }

  public void setAlternatingRows(final boolean alternatingRows) {
    this.alternatingRows = alternatingRows;
  }

  public String getAlternatingRowColor() {
    return alternatingRowColor;
  }

  public void setAlternatingRowColor(final String alternatingRowColor) {
    this.alternatingRowColor = alternatingRowColor;
  }

  public void setHeaderFont(final Font headerFont) {
    this.headerFont = headerFont;
  }

  public Font getHeaderFont() {
    return headerFont;
  }

  public boolean isUseHeader() {
    return useHeader;
  }

  public void setUseHeader(final boolean useHeader) {
    this.useHeader = useHeader;
  }

  public Locale getLocale() {
    return locale;
  }

}
