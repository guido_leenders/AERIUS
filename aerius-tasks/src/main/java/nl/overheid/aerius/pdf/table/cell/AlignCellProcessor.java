/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.table.cell;

import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.pdf.PDFAlignment;

/**
 * Convenience cell processor as this logic will be used often.
 * Set the alignment for the cells to the given alignment.
 */
public class AlignCellProcessor implements CellProcessor {

  private final PDFAlignment alignment;

  /**
   * @param column Column to process
   * @param row Row to process
   * @param alignment The alignment for the process.
   */
  public AlignCellProcessor(final PDFAlignment alignment) {
    this.alignment = alignment;
  }

  @Override
  public void processCell(final PdfPCell cell) {
    cell.setHorizontalAlignment(alignment.getAlignOption());
  }

}
