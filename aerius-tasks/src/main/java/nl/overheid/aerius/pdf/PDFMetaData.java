/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfString;
import com.lowagie.text.pdf.PdfWriter;

/**
 *
 */
public class PDFMetaData {

  private final Document document;
  private final PdfWriter pdfWriter;

  /**
   * Default constructor.
   * @param pdfWriter The writer to add metadata to.
   * @param document The document to add metadata to.
   */
  public PDFMetaData(final PdfWriter pdfWriter, final Document document) {
    this.pdfWriter = pdfWriter;
    this.document = document;
  }

  /**
   * Add title to metadata.
   * @param title The title to add.
   */
  public void addMetadataTitle(final String title) {
    document.addTitle(title);
  }

  /**
   * Add author to metadata.
   * @param author The author to add.
   */
  public void addMetadataAuthor(final String author) {
    document.addAuthor(author);
  }

  /**
   * Add subject to metadata.
   * @param subject The subject to add.
   */
  public void addMetadataSubject(final String subject) {
    document.addSubject(subject);
  }

  /**
   * Add keywords to metadata.
   * @param keywords The keywords to add.
   */
  public void addMetadataKeywords(final String keywords) {
    document.addKeywords(keywords);
  }

  /**
   * Add creator to metadata.
   * @param creator The creator to add.
   */
  public void addMetadataCreator(final String creator) {
    document.addCreator(creator);
  }

  /**
   * Add custom field to the metadata.
   * @param key The key to add.
   * @param value The value to add.
   */
  public void addMetadataCustomField(final String key, final String value) {
    pdfWriter.getInfo().put(new PdfName(key), new PdfString(value, PdfObject.TEXT_UNICODE));
  }
}
