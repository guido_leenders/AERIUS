/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.pdf.marker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.pdf.marker.Marker.MarkerDecoration;
import nl.overheid.aerius.worker.util.FontUtils;
import nl.overheid.aerius.worker.util.ImageUtils;

/**
 *
 */
public class MarkerImages {

  private enum RoundedSide {
    NONE, LEFT, RIGHT, BOTH;
  }

  private final List<Marker> markers;

  private Font font;

  private Color textColor;
  private Color backgroundColor;
  private int fontSize;
  private int markerHeight;
  private int markerWidth;
  private int border;
  private int spacing;
  private int markerBackgroundWidth;
  private int markerBackgroundHeight;
  private int arcWidth;
  private int arcHeight;
  private int decorationHeight;
  private int decorationSpacing;
  private int decorationDottedSpacing;
  private int triangleDisplacement;
  private int scale;

  /**
   * Create clustered markers using the markers and font supplied.
   * <br><br>
   * <b>Note:</b> Marker has a fixed width, if the content would be bigger than the marker it'll get messy.
   * @param markers The markers to be used for the clustered markers image.
   * The markers will be displayed in the order used in the list.
   * @param font The font to use for the content of the markers.
   * @param scale The scale to use for the markers. Scale 1 will give a marker with height 30.
   */
  MarkerImages(final List<Marker> markers, final Font font, final int scale) {
    if (markers == null) {
      throw new IllegalArgumentException("markers not allowed to be null");
    }
    this.markers = markers;
    this.font = font;
    setScale(scale);
  }

  /**
   * Create a marker using the marker and font supplied.
   * <b>Note:</b>Marker has a fixed width, if the content would be bigger than the marker it'll get messy.
   *
   * @param marker The marker to be used for the marker image.
   * @param font The font to use for the content of the markers.
   * @param scale The scale to use for the markers. Scale 1 will give a marker with height 30.
   */
  MarkerImages(final Marker marker, final Font font, final int scale) {
    if (marker == null) {
      throw new IllegalArgumentException("markers not allowed to be null");
    }
    this.markers = new ArrayList<>();
    markers.add(marker);
    this.font = font;
    setScale(scale);
  }

  /**
   * Get the image for the clustered markers.
   * @return A buffered image containing the image for the markers.
   */
  public BufferedImage getMarkersImage() {
    //height is MARKER_HEIGHT.
    //width is composed of: marker_width * #markers + BORDER - spacing.
    final int imagewidth = markerWidth * markers.size() + 2 * border - spacing;

    final BufferedImage image = new BufferedImage(imagewidth, markerHeight,
        BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g = getGraphicsObject(image);

    //just draw the markers with full rounded edge.
    drawMarkers(g, 0, RoundedSide.BOTH);

    // release the graphics object.
    g.dispose();

    return image;
  }

  /**
   * Get the image for the clustered markers.
   * Includes a triangle on the left part of the image.
   * @return A buffered image containing the image for the markers.
   */
  public BufferedImage getMarkersWithTriangleImage() {
    //height is MARKER_HEIGHT.
    //width is composed of: triangle width + marker_width * #markers + BORDER - spacing.
    //using triangle width == triangle height == marker height
    final int imagewidth = markerHeight + markerWidth * markers.size() + 2 * border - spacing - triangleDisplacement;

    final BufferedImage image = new BufferedImage(imagewidth, markerHeight,
        BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g = getGraphicsObject(image);

    //first up: triangle
    drawTriangle(g);

    //next: markers (only right side should be rounded, left side has the triangle).
    drawMarkers(g, markerHeight - triangleDisplacement, RoundedSide.RIGHT);

    // release the graphics object.
    g.dispose();

    return image;
  }

  private Graphics2D getGraphicsObject(final BufferedImage image) {
    final Graphics2D g = image.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g.setStroke(new BasicStroke(0));
    g.setFont(font);
    return g;
  }

  /**
   * Set a new scale.
   * @param scale The scale to set. If lower then 1, 1 is used.
   */
  private void setScale(final int scale) {
    this.scale = scale;
    if (this.scale < 1) {
      this.scale = 1;
    }
  }

  private void drawMarkers(final Graphics2D g, final int posX, final RoundedSide backgroundRoundedSide) {
    //first,draw the background for the markers.
    drawMarkersBackground(g, backgroundRoundedSide, posX);

    //next draw all markers.
    for (int i = 0; i < markers.size(); i++) {
      final int markerPositionX = posX + markerWidth * i - spacing / 2 + border;
      //default marker has no rounded sides, only the markers on the edges have rounded sides.
      RoundedSide roundedSide = RoundedSide.NONE;
      if (markers.size() == 1) {
        roundedSide = RoundedSide.BOTH;
      } else if (i == 0) {
        roundedSide = RoundedSide.LEFT;
      } else if (i == markers.size() - 1) {
        roundedSide = RoundedSide.RIGHT;
      }
      drawMarker(g, markers.get(i), markerPositionX, roundedSide);
    }
  }

  private void drawTriangle(final Graphics2D g) {
    g.setColor(getBackgroundColor());
    final Polygon triangle = new Polygon();
    triangle.addPoint(0, 0);
    triangle.addPoint(markerHeight - border, 0);
    triangle.addPoint(markerHeight - border, markerHeight - border);
    g.fillPolygon(triangle);
  }

  private void drawMarkersBackground(final Graphics2D g, final RoundedSide roundedSide, final int xPos) {
    g.setColor(getBackgroundColor());
    //first draw the rect with rounded edge,
    //then draw the left half to remove the rounded edge.
    final int backgroundBorder = 2 * border;
    drawRectWithRoundedCorner(g, RoundedSide.BOTH, xPos, 0,
        markerWidth * markers.size() + backgroundBorder - spacing, markerHeight, arcWidth + border, arcHeight + border);
    if (roundedSide != RoundedSide.BOTH) {
      drawRectWithRoundedCorner(g, RoundedSide.NONE, xPos, 0,
          markerWidth / 2 , markerHeight / 2, 0, 0);
    }
  }

  private void drawMarker(final Graphics2D g, final Marker marker, final int xPos, final RoundedSide roundedSide) {
    //Draw the marker.
    //first the 'background'
    g.setColor(ImageUtils.getColorByHexValue(marker.getColor()));
    final int backgroundPosX = xPos + spacing / 2;
    drawRectWithRoundedCorner(g, roundedSide, backgroundPosX, border, markerBackgroundWidth, markerBackgroundHeight,
        arcWidth, arcHeight);

    //last: text.
    g.setColor(textColor);
    final Rectangle2D rect = FontUtils.getBoundsOfString(font, g.getFontRenderContext(), marker.getContent());
    final int contentPosX = xPos + markerWidth / 2
        - (int) rect.getCenterX();
    final int contentPosY = markerHeight / 2 - (int) rect.getCenterY();
    g.drawString(marker.getContent(), contentPosX, contentPosY);

    //and actual last: decorations (if any)
    for (final MarkerDecoration decoration : marker.getDecorations()) {
      final int decorationPosY = contentPosY
          + (decoration == MarkerDecoration.DOTTED_UNDERLINE ? decorationDottedSpacing : decorationSpacing);
      drawDecoration(g, contentPosX, decorationPosY, (int) rect.getWidth(), decoration);
    }
  }

  private void drawDecoration(final Graphics2D g, final int xPos, final int yPos, final int width, final MarkerDecoration decoration) {
    g.setColor(textColor);
    switch (decoration) {
    case DOTTED_UNDERLINE:
      final int numberOfDots = width / (2 * decorationHeight) + 1;
      final int dotWidth = (width / numberOfDots) / 2 + 1;
      int xPosDot = xPos;
      for (int i = 0; i < numberOfDots; i++) {
        g.fillRect(xPosDot, yPos - decorationHeight / 2, dotWidth, decorationHeight);
        xPosDot += 2 * dotWidth;
      }
      break;
    case UNDERLINE:
      g.fillRect(xPos, yPos - decorationHeight / 2, width, decorationHeight);
      break;
    default:
      throw new IllegalArgumentException("Not yet implemented!");
    }
  }

  private void drawRectWithRoundedCorner(final Graphics2D g, final RoundedSide roundedSide, final int xPos, final int yPos,
      final int width, final int height, final int arcWidth, final int arcHeight) {
    g.fillRoundRect(xPos, yPos, width, height,
        arcWidth, arcHeight);
    switch (roundedSide) {
    case NONE:
      g.fillRect(xPos, yPos, width, height);
      break;
    case RIGHT:
      g.fillRect(xPos, yPos, width / 2, height);
      break;
    case LEFT:
      g.fillRect(xPos + width / 2 + 1, yPos, width / 2, height);
      break;
    case BOTH:
    default:
      break;
    }
  }

  private void updateFontProperties() {
    this.font = font.deriveFont(Font.BOLD, getFontSize() * scale);
  }

  private void updateMarkerBackgroundWidth() {
    this.markerBackgroundWidth = getMarkerWidth() - getSpacing();
  }

  private void updateMarkerBackgroundheight() {
    this.markerBackgroundHeight = getMarkerHeight() - 2 * getBorder();
  }

  public Color getTextColor() {
    return textColor;
  }

  public void setTextColor(final Color textColor) {
    this.textColor = textColor;
  }

  public Font getFont() {
    return font;
  }

  public int getMarkerHeight() {
    return markerHeight;
  }

  public int getMarkerWidth() {
    return markerWidth;
  }

  public int getBorder() {
    return border;
  }

  public int getSpacing() {
    return spacing;
  }

  public int getMarkerBackgroundWidth() {
    return markerBackgroundWidth;
  }

  public int getMarkerBackgroundHeight() {
    return markerBackgroundHeight;
  }

  public int getArcWidth() {
    return arcWidth;
  }

  public int getArcHeight() {
    return arcHeight;
  }

  public int getDecorationHeight() {
    return decorationHeight;
  }

  public int getDecorationSpacing() {
    return decorationSpacing;
  }

  public int getDecorationDottedSpacing() {
    return decorationDottedSpacing;
  }

  public int getTriangleDisplacement() {
    return triangleDisplacement;
  }

  public Color getBackgroundColor() {
    return backgroundColor;
  }

  public int getFontSize() {
    return fontSize;
  }

  /**
   * Set font and update font properties based on scale and such.
   * @param font The font to set.
   */
  public void setFont(final Font font) {
    this.font = font;
    updateFontProperties();
  }

  /**
   * Set font size and update font properties based on scale and such.
   * @param fontSize The font size to set.
   */
  public void setFontSize(final int fontSize) {
    this.fontSize = fontSize;
    updateFontProperties();
  }

  public void setBackgroundColor(final Color backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  /**
   * Set marker height and update the marker background height.
   * @param markerHeight The marker height to set.
   */
  public void setMarkerHeight(final int markerHeight) {
    this.markerHeight = markerHeight * scale;
    updateMarkerBackgroundheight();
  }

  /**
   * Set marker width and update the marker background width.
   * @param markerWidth The marker width to set.
   */
  public void setMarkerWidth(final int markerWidth) {
    this.markerWidth = markerWidth * scale;
    updateMarkerBackgroundWidth();
  }

  /**
   * Set border and update the marker background height.
   * @param border The border to set.
   */
  public void setBorder(final int border) {
    this.border = border * scale;
    updateMarkerBackgroundheight();
  }

  /**
   * Set spacing and update the marker background width.
   * @param spacing The spacing to set.
   */
  public void setSpacing(final int spacing) {
    this.spacing = spacing * scale;
    updateMarkerBackgroundWidth();
  }

  public void setArcWidth(final int arcWidth) {
    this.arcWidth = arcWidth * scale;
  }

  public void setArcHeight(final int arcHeight) {
    this.arcHeight = arcHeight * scale;
  }

  public void setDecorationHeight(final int decorationHeight) {
    this.decorationHeight = decorationHeight * scale;
  }

  public void setDecorationSpacing(final int decorationSpacing) {
    this.decorationSpacing = decorationSpacing * scale;
  }

  public void setDecorationDottedSpacing(final int decorationDottedSpacing) {
    this.decorationDottedSpacing = decorationDottedSpacing * scale;
  }

  public void setTriangleDisplacement(final int triangleDisplacement) {
    this.triangleDisplacement = triangleDisplacement * scale;
  }

}
