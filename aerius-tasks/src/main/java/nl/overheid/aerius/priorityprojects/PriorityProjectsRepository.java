/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.priorityprojects;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;

/**
 * Database classes for priority projects.
 */
final class PriorityProjectsRepository {

  private static final String INSERT_ORIGIN = "INSERT INTO origins (origin_id, name, authority) "
      +  "SELECT ?,?,? "
      + " WHERE NOT EXISTS (SELECT origin_id FROM origins WHERE origin_id = ? LIMIT 1) ";
  private static final String INSERT_PRIORITY_PROJECT_SITE = "INSERT INTO sites "
      + "(site_id, name, reference) VALUES (?, ?, ?)";
  private static final String INSERT_PRIORITY_PROJECT = "INSERT INTO priority_project_sites "
      + "(site_id, realisation_year, authority, description, comment) VALUES (?, ?, ?, ?, ?)";
  private static final String INSERT_PRIORITY_PROJECT_FILE = "INSERT INTO priority_project_site_files "
      + "(site_id, type, filename, content) VALUES (?, ?, ?, ?)";
  private static final String INSERT_PRIORITY_PROJECT_SOURCE = "INSERT INTO sources "
      + "(site_id, source_id, sector_id, origin_id, reference, geometry) VALUES (?, ?, ?, ?, ?, ST_PointFromText(?, ae_get_srid()))";
  private static final String INSERT_PRIORITY_PROJECT_EMISSION = "INSERT INTO source_emissions "
      + "(source_id, substance_id, emission) VALUES (?, ?, ?)";
  private static final String INSERT_PRIORITY_PROJECT_CHARACTERISTICS = "INSERT INTO source_source_characteristics "
      + "(source_id, heat_content, height, spread, emission_diurnal_variation_id, diameter) "
      + " VALUES (?, ?, ?, ?, ?, 0)";

  private PriorityProjectsRepository() {
    // util class
  }

  /**
   * Store origin information in database. Origins have to be available for sources to be added to the DB for a site.
   * They contain information about 'bronhouder', the person/authority responsible for the data.
   * @param con The connection to use.
   * @param originId The ID of the origin to use. Must be unique and new to the database.
   * @param authority The authority responsible for the project.
   * @param name The name of the project.
   * @throws SQLException In case of DB errors.
   */
  public static void insertPriorityProjectOrigin(final Connection con, final int originId, final String name, final String authority)
      throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_ORIGIN)) {
      stmt.setInt(1, originId);
      stmt.setString(2, name);
      stmt.setString(3, authority);
      stmt.setInt(4, originId);
      stmt.executeUpdate();
    }
  }

  /**
   * Store project information in database.
   * @param con The connection to use.
   * @param siteId the Site id to use. Must be unique and new to the database.
   * @param realisationYear The year the project should be realised.
   * @param authority The authority responsible for the project.
   * @param name The name of the project.
   * @param description The description of the project.
   * @param comment Any other comment about the project.
   * @throws SQLException In case of DB errors.
   */
  public static void insertPriorityProject(final Connection con, final int siteId, final int realisationYear, final String authority,
      final String name, final String description, final String comment) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT_SITE)) {
      stmt.setInt(1, siteId);
      stmt.setString(2, name);
      stmt.setString(3, description);
      stmt.executeUpdate();
    }
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT)) {
      stmt.setInt(1, siteId);
      stmt.setInt(2, realisationYear);
      stmt.setString(3, authority);
      stmt.setString(4, description);
      stmt.setString(5, comment);
      stmt.executeUpdate();
    }
  }

  /**
   * Store the file that is used for a priority project in database.
   * @param con The connection to use.
   * @param siteId the Site ID that fits the priority project.
   * @param file The file used.
   * @param type The type of the file.
   * @param content The actual content of the file.
   * @throws SQLException In case of DB errors.
   * @throws IOException When reading the content failed for some reason.
   */
  public static void insertPriorityProjectFile(final Connection con, final int siteId, final File file, final String type, final byte[] content)
      throws SQLException, IOException {
    try (final InputStream cis = new ByteArrayInputStream(content);
        final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT_FILE)) {
      stmt.setInt(1, siteId);
      stmt.setString(2, type);
      stmt.setString(3, file.getName());
      stmt.setBinaryStream(4, cis, content.length);
      stmt.executeUpdate();
    }
  }

  /**
   * Store emission source information of a specific project and source in the database.
   * @param con The connection to use.
   * @param siteId The site ID to which the source belongs.
   * @param sourceId The source ID to use.
   * @param sectorId The ID of the sector that the source belongs to.
   * @param originId The ID of the data origin (@see insertOrigin)
   * @param name The name of the source.
   * @param geometry The geometry of the source.
   * @throws SQLException In case of DB errors.
   */
  public static void insertPriorityProjectSource(final Connection con, final int siteId, final int sourceId, final int sectorId,
      final int originId, final String name, final WKTGeometry geometry) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT_SOURCE)) {
      stmt.setInt(1, siteId);
      stmt.setInt(2, sourceId);
      stmt.setInt(3, sectorId);
      stmt.setInt(4, originId);
      stmt.setString(5, name);
      stmt.setString(6, geometry.getWKT());
      stmt.executeUpdate();
    }
  }

  /**
   * Store emissions of a specific source in the database.
   * @param con The connection to use.
   * @param sourceId The ID of the source the emission values belong to.
   * @param emissions The Map containing substance, emission value pairs.
   * @throws SQLException In case of DB errors.
   */
  public static void insertPriorityProjectSourceEmissions(final Connection con, final int sourceId,
      final Map<Substance, Double> emissions) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT_EMISSION)) {
      for (final Entry<Substance, Double> entry : emissions.entrySet()) {
        stmt.setInt(1, sourceId);
        stmt.setInt(2, entry.getKey().getId());
        stmt.setFloat(3, entry.getValue().floatValue());
        stmt.executeUpdate();
      }
    }
  }

  /**
   * Store emission characteristics of a specific source in the database.
   * @param con The connection to use.
   * @param sourceId The ID of the source the characteristics belong to.
   * @param characteristics The emission characteristics of the source.
   * @throws SQLException In case of DB errors.
   */
  public static void insertPriorityProjectSourceCharacteristics(final Connection con, final int sourceId,
      final OPSSourceCharacteristics characteristics) throws SQLException {
    try (final PreparedStatement stmt = con.prepareStatement(INSERT_PRIORITY_PROJECT_CHARACTERISTICS)) {
      //source_id, heat_content, height, spread, diurnal_variation
      stmt.setInt(1, sourceId);
      stmt.setDouble(2, characteristics.getHeatContent());
      stmt.setDouble(3, characteristics.getEmissionHeight());
      stmt.setDouble(4, characteristics.getSpread());
      stmt.setInt(5, characteristics.getDiurnalVariationSpecification().getId());
      stmt.executeUpdate();
    }
  }
}
