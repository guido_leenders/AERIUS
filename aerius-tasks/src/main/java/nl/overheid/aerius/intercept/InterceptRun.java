/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.jfree.util.Log;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerClient;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerConfiguration;
import nl.overheid.aerius.worker.WorkerFactory;

/**
  * Intercepts the queue and run the task at the head.
 */
class InterceptRun {
  private static final String INTERCEPT_RUN = "interceptRun";
  private static final Option INTERCEPT_RUN_OPTION =
      Option.builder(INTERCEPT_RUN).argName("queue> <workerType").numberOfArgs(2)
          .desc("Intercepts and runs the task at the head of the queue manually. Removes task from queue when successful executed. "
              + "Crashes will ensue if this worker isn't configured correctly to handle the intercepted task.").build();
  private static final int IDX_QUEUE_NAME = 0;
  private static final int IDX_WORKER_TYPE = 1;

  public static final InterceptCommandOption<InterceptRunCommand> INTERCEPT_RUN_OPTIONS =
      new InterceptCommandOption<InterceptRunCommand>(INTERCEPT_RUN, INTERCEPT_RUN_OPTION) {
        @Override
        protected InterceptRunCommand create(final String... options) throws MissingArgumentException {
          return new InterceptRunCommand(options);
        }
  };

  private final String queueName;
  private final WorkerType workerType;
  private final WorkerFactory<?>[] workerFactories;

  public InterceptRun(final InterceptRunCommand options, final WorkerFactory<?>... workerFactories) {
    queueName = options.getQueueName();
    workerType = options.getWorkerType();
    this.workerFactories = workerFactories;
  }

  @SuppressWarnings("unchecked")
  public void run(final BrokerConnectionFactory factory, final Properties props) throws Exception {
    final WorkerFactory<WorkerConfiguration> workerFactory = InterceptUtil.determineWorkerFactory(workerFactories, workerType);
    final WorkerConfiguration wc = workerFactory.createConfiguration(props);
    wc.validate();
    final WorkerHandler runWorkerHandler = workerFactory.createWorkerHandler(wc, factory);
    final WorkerHandler workerHandler = new WorkerHandler() {
      @Override
      public Serializable handleWorkLoad(final Serializable input, final WorkerIntermediateResultSender resultSender, final String correlationId)
          throws Exception {
        try {
          final Serializable result = runWorkerHandler.handleWorkLoad(input, resultSender, correlationId);
          if (result instanceof Exception) {
            throw (Exception) result;
          }
          return result;
        } catch (AeriusException | RuntimeException e) {
          Log.error("intercept run for the task at hand crashed. The task will remain on the queue.", e);
          //Throw as IOException so this will put the task back on the queue as AeriusException and RuntimExceptions will remove task from queue.
          throw new IOException(e);
        }
      }
    };
    final WorkerClient client = new WorkerClient(factory, workerHandler, workerFactory.getWorkerType());
    client.get(queueName, true, true);
  }

  /**
   * Intercept run command wrapper.
   */
  static class InterceptRunCommand extends InterceptCommand {
    public InterceptRunCommand(final String... optionValues) throws MissingArgumentException {
      super(optionValues);
      if (getOptionValues().length != 2) {
        throw new MissingArgumentException("No valid argument. It should be : <queue> <workerType>");
      }
    }

    @Override
    public void run(final InterceptVisitor visitor) throws Exception {
      visitor.run(this);
    }

    public String getQueueName() {
      return getOptionValues()[IDX_QUEUE_NAME];
    }

    public WorkerType getWorkerType() {
      return WorkerType.valueOf(getOptionValues()[IDX_WORKER_TYPE].toUpperCase(Locale.ENGLISH));
    }
  }
}
