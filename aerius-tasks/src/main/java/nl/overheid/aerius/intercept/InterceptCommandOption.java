/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;

/**
 * Data class for intercept command line option configuration.
 */
abstract class InterceptCommandOption<C extends InterceptCommand> {

  private final String optionName;
  private final Option option;

  public InterceptCommandOption(final String optionName, final Option option) {
    this.optionName = optionName;
    this.option = option;
  }

  protected abstract C create(String... options) throws MissingArgumentException, FileNotFoundException;

  public String getOptionName() {
    return optionName;
  }

  public Option getOption() {
    return option;
  }

  public boolean isOption(final CommandLine cmd) {
    return cmd.hasOption(optionName);
  }
}
