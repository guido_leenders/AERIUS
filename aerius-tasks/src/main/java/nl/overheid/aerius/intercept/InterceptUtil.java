/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import org.apache.commons.cli.UnrecognizedOptionException;

import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Util class for Intercept commands.
 */
final class InterceptUtil {
  public static final String QUEUE_PREFIX = "intercept.";

  private InterceptUtil() {
    // util class
  }

  public static String createInterceptQueueName(final String fromQueue) {
    return fromQueue.startsWith(QUEUE_PREFIX) ? fromQueue : (QUEUE_PREFIX + fromQueue);
  }

  public static WorkerFactory determineWorkerFactory(final WorkerFactory<?>[] workerFactories, final WorkerType workerType)
      throws UnrecognizedOptionException {
    for (final WorkerFactory<?> workerFactory : workerFactories) {
      if (workerType == workerFactory.getWorkerType()) {
        return workerFactory;
      }
    }
    throw new UnrecognizedOptionException("The given worker type " + workerType + " isn't configured for this worker");
  }
}
