/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;

import nl.overheid.aerius.intercept.SaveWorkHandler.SaveWorkCommand;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;

/**
 * Intercepts the queue and removes the task at the head and saves it's result.
 */
class InterceptRemove {
  private static final Logger LOG = LoggerFactory.getLogger(InterceptRemove.class);

  private static final String INTERCEPT_REMOVE = "interceptRemove";
  private static final Option INTERCEPT_REMOVE_OPTION =
      Option.builder(INTERCEPT_REMOVE).argName("queue> <path").numberOfArgs(2)
          .desc("Intercepts the given queue and removes 1 task from the queue. If this is an intercept queue, the queue will be removed.").build();

  public static final InterceptCommandOption<InterceptRemoveCommand> INTERCEPT_REMOVE_OPTIONS =
      new InterceptCommandOption<InterceptRemoveCommand>(INTERCEPT_REMOVE, INTERCEPT_REMOVE_OPTION) {
        @Override
        protected InterceptRemoveCommand create(final String... options) throws MissingArgumentException, FileNotFoundException {
          return new InterceptRemoveCommand(options);
        }
  };


  private final SaveWorkHandler handler;
  private final String queueName;

  public InterceptRemove(final InterceptRemoveCommand options) throws MissingArgumentException, FileNotFoundException {
    queueName = options.getQueueName();
    handler = new SaveWorkHandler(options, true, true, new AeriusException(Reason.INTERNAL_ERROR));
  }

  public void remove(final BrokerConnectionFactory connectionFactory) throws Exception {
    try {
      handler.run(connectionFactory);
    } finally {
      if (queueName.startsWith(InterceptUtil.QUEUE_PREFIX)) {
        deleteQueue(connectionFactory, queueName);
        LOG.warn("Deleted queue '{}'", queueName);
      }
    }
  }

  private void deleteQueue(final BrokerConnectionFactory connectionFactory, final String queueName) throws IOException {
    final Channel channel = connectionFactory.getConnection().createChannel();
    try {
      channel.queueDelete(queueName);
    } finally {
      if (channel.isOpen()) {
        channel.close();
      }
    }
  }

  /**
   * Intercept remove command wrapper.
   */
  static class InterceptRemoveCommand extends SaveWorkCommand {

    public InterceptRemoveCommand(final String... optionValues) throws FileNotFoundException, MissingArgumentException {
      super(optionValues);
    }

    @Override
    public void run(final InterceptVisitor visitor) throws Exception {
      visitor.run(this);
    }
  }

}
