/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.intercept;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.cli.MissingArgumentException;

import nl.overheid.aerius.intercept.InterceptRedirect.InterceptRedirectCommand;
import nl.overheid.aerius.intercept.InterceptRemove.InterceptRemoveCommand;
import nl.overheid.aerius.intercept.InterceptRun.InterceptRunCommand;
import nl.overheid.aerius.intercept.InterceptSave.InterceptSaveCommand;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Class to handle the 'intercept' commands.
 */
public final class WorkerInterceptor implements InterceptVisitor {

  private final BrokerConnectionFactory factory;
  private final WorkerFactory<?>[] workerFactories;
  private final Properties props;

  /**
   * @param factory
   * @param workerFactories
   * @param props
   */
  public WorkerInterceptor(final BrokerConnectionFactory factory, final WorkerFactory<?>[] workerFactories, final Properties props) {
    this.factory = factory;
    this.workerFactories = workerFactories;
    this.props = props;
  }

  /**
   * Main method to handle an intercept command.
   *
   * @param cmdOptions command line arguments
   */
  public void intercept(final InterceptCommandLineOptions cmdOptions) {
    try {
      cmdOptions.getInterceptOptions().run(this);
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void run(final InterceptRedirectCommand options) throws IOException, InterruptedException {
    new InterceptRedirect(options).redirect(factory);
  }

  @Override
  public void run(final InterceptRemoveCommand options) throws MissingArgumentException, FileNotFoundException, Exception {
    new InterceptRemove(options).remove(factory);
  }

  @Override
  public void run(final InterceptRunCommand options) throws Exception {
    new InterceptRun(options, workerFactories).run(factory, props);
  }

  @Override
  public void run(final InterceptSaveCommand options) throws Exception {
    new InterceptSave(options).save(factory);
  }
}
