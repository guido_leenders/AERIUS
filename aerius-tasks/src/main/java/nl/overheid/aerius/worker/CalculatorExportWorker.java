/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.DownloadDirectoryUtils.StorageLocation;

/**
 * Export worker can handle PDF, GML and CSV exports based on worker.
 */
public class CalculatorExportWorker extends AbstractExportWorker<CalculationInputData> {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorExportWorker.class);

  private final ExportWorker worker;
  private final CalculatorBuildDirector calculatorBuildDirector;

  public CalculatorExportWorker(final CalculatorBuildDirector calculatorBuildDirector, final PMF pmf, final BrokerConnectionFactory factory,
      final DBWorkerConfiguration config,
      final ExportWorker worker) {
    // should refactor PDF/GML directories to one single directory - this is weird in this case.
    super(pmf, config.getPAADownloadDirectory(), factory);
    this.worker = worker;
    this.calculatorBuildDirector = calculatorBuildDirector;
  }

  @Override
  protected ImmediateExportData handleExport(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String correlationId) throws Exception {
    try {
      worker.prepare(inputData, backupAttachments);
      if (inputData.isTrackJobProcess()) {
        cleanUpCrashedCalculations(correlationId);
      }
      final ImmediateExportData ied;

      if (inputData.getExportType() != null && inputData.getExportType() != ExportType.GML_SOURCES_ONLY) {
        final CalculationJob calculationJob = calculatorBuildDirector.construct(inputData, correlationId, null).calculate();
        ied = createExportData(inputData, backupAttachments, downloadDirectory, correlationId);
        if (inputData.isRemoveResults()) {
          removeCalculationResults(calculationJob.getCalculatedScenario());
        }
      } else {
        ied = createExportData(inputData, backupAttachments, downloadDirectory, correlationId);
      }

      if (inputData.isTrackJobProcess()) {
        addResultUrlToJob(correlationId, ied.getUrl());
        setEndtimeJob(correlationId);
        setJobStatusToCompleted(correlationId);
      }
      return ied;
    } catch (RuntimeException | AeriusException e) {
      // When a runtime or aerius Exception is thrown the job is cancelled and an endtime should be set
      // Other exceptions mean the job will get back on the queue, and can be picked up again, so no endtime is set.
      if (inputData.isTrackJobProcess()) {
        setEndtimeJob(correlationId);
        if (isNotCancelledJob(e)) {
          final Locale locale = getLocale(inputData);

          setErrorStateAndMessageJob(correlationId, getErrorMessage(e, locale));
        }
      }
      throw e;
    }
  }

  private boolean isNotCancelledJob(final Exception e) {
    return !(e instanceof AeriusException) || ((AeriusException) e).getReason() != Reason.CONNECT_JOB_CANCELLED;
  }

  /**
   * When a calculation crashed and was set back on the queue it could mean there are still calculation results left in the database.
   * This call removes these results if any are present before the calculation starts.
   * @param correlationId job key to remove results for.
   */
  private void cleanUpCrashedCalculations(final String correlationId) {
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.removeJobCalculations(con, correlationId);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set the end time for job {}", correlationId, e);
    }
  }

  private void setEndtimeJob(final String correlationId) {
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.setEndTimeToNow(con, correlationId);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set the end time for job {}", correlationId, e);
    }
  }

  private void setJobStatusToCompleted(final String correlationId) {
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.updateJobStatus(con, correlationId, JobState.COMPLETED);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set the completed status for job {}", correlationId, e);
    }
  }

  private void setErrorStateAndMessageJob(final String correlationId, final String message) {
    try (final Connection con = getPMF().getConnection()) {
      JobRepository.setErrorMessage(con, correlationId, message);
    } catch (final SQLException e) {
      LOGGER.error("Error occurred while trying to set the error message {} for the job {}", correlationId, message, e);
    }
  }

  private ImmediateExportData createExportData(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String downloadDirectory, final String correlationId) throws IOException, AeriusException, SQLException {
    final StorageLocation storageLocation = createStorageLocation(inputData);

    worker.createFileContent(storageLocation.getOutputFile(), inputData, backupAttachments, downloadDirectory, correlationId);
    return worker.postProcess(new ImmediateExportData(storageLocation.getUrl()));
  }

  /**
   * Remove calculations for successful calculations.
   * @param calculatorJob
   * @throws SQLException
   */
  private void removeCalculationResults(final CalculatedScenario scenario) throws SQLException {
    try (Connection connection = getConnection()) {
      for (final Calculation calculation : scenario.getCalculations()) {
        CalculationRepository.removeCalculation(connection, calculation.getCalculationId());
      }
    }
  }

  private void addResultUrlToJob(final String correlationId, final String url) {
    try (final Connection con = getConnection()) {
      JobRepository.setResultUrl(con, correlationId, url);
    } catch (final SQLException e) {
      // Only log as this isn't severe enough to let the export crash. As long as mailing works it should be fine.
      LOGGER.warn("Could not save result URL for job with key '{}'. URL would have been: '{}'", correlationId, url, e);
    }
  }

  @Override
  protected String getFallbackErrorCode(final CalculationInputData inputData) {
    final String errorCode;

    if (inputData == null || inputData.getScenario() == null) {
      errorCode = "NoData";
    } else {
      final ScenarioMetaData metaData = inputData.getScenario().getMetaData();
      if (metaData.getReference() == null) {
        errorCode = metaData.getProjectName();
      } else {
        errorCode = metaData.getReference();
      }
    }

    return errorCode;
  }

  @Override
  protected MessagesEnum getMailSubject(final CalculationInputData inputData) {
    return worker.getMailSubject(inputData);
  }

  @Override
  protected void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    worker.setReplacementTokens(inputData, mailMessageData);
  }

  @Override
  protected MessagesEnum getMailContent(final CalculationInputData inputData) {
    return worker.getMailContent(inputData);
  }

  @Override
  protected String getFileName(final CalculationInputData inputData) {
    return worker.getFileName(inputData);
  }

}
