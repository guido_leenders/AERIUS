/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.CsvOutputHandler;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.ZipFileMaker;
import nl.overheid.aerius.worker.base.AbstractExportWorker.ImmediateExportData;

/**
 * Worker to ZIP and mail the generated CSV files.
 */
public class CsvExportWorker implements ExportWorker {

  private static final Logger LOG = LoggerFactory.getLogger(CsvExportWorker.class);

  private static final String FILENAME_PREFIX = "AERIUS_CSV";

  private int fileCount;

  @Override
  public void prepare(final CalculationInputData inputData, final ArrayList<StringDataSource> backupAttachments)
      throws SQLException, AeriusException {
    // NO-OP
  }

  @Override
  public final void createFileContent(final File outputFile, final CalculationInputData inputData,
      final ArrayList<StringDataSource> backupAttachments, final String downloadDirectory, final String correlationId) throws IOException {
    LOG.info("Generating ZIP for CSV files, reference: {}", inputData.getScenario().getMetaData().getReference());
    final File tempDir = CsvOutputHandler.getFolderForJob(correlationId);
    final File[] exportFiles = tempDir.listFiles();
    ZipFileMaker.files2ZipStream(outputFile, Arrays.asList(exportFiles), tempDir, true);
    fileCount = exportFiles.length;
    if (!tempDir.delete()) {
      LOG.warn("Deleting of temporary directory '{}' failed.", tempDir);
    }
  }

  @Override
  public ImmediateExportData postProcess(final ImmediateExportData immediateExportData) {
    immediateExportData.setFilesInZip(fileCount);
    return immediateExportData;
  }

  @Override
  public MessagesEnum getMailSubject(final CalculationInputData inputData) {
    return inputData.isNameEmpty() ? MessagesEnum.CSV_MAIL_SUBJECT : MessagesEnum.CSV_MAIL_SUBJECT_JOB;
  }

  @Override
  public void setReplacementTokens(final CalculationInputData inputData, final MailMessageData mailMessageData) {
    mailMessageData.setReplacement(ReplacementToken.AERIUS_REFERENCE, inputData.getScenario().getMetaData().getReference());
    mailMessageData.setReplacement(ReplacementToken.JOB, inputData.getName());
  }

  @Override
  public MessagesEnum getMailContent(final CalculationInputData inputData) {
    return MessagesEnum.CSV_MAIL_CONTENT;
  }

  @Override
  public String getFileName(final CalculationInputData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FileFormat.ZIP.getExtension(), null, null);
  }

}
