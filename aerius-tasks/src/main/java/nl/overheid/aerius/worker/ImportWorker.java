/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.common.ContextRepository;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.importer.ImportInput;
import nl.overheid.aerius.shared.domain.importer.ImportOutput;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.base.AbstractDBWorker;

/**
 * Generic worker which can import a (valid AERIUS) file and return the results.
 * This result can be:
 * - CalculatedScenario containing sources
 * - CalculatedScenario containing calculation IDs (results are persisted to the database).
 * - CalculatedScenario containing both sources and calculation Ids
 * - Validation errors.
 */
public class ImportWorker extends AbstractDBWorker<ImportInput, ImportOutput> {

  private static final Logger LOG = LoggerFactory.getLogger(ImportWorker.class);

  public ImportWorker(final PMF pmf) {
    super(pmf);
  }

  @Override
  public ImportOutput run(final ImportInput input, final WorkerIntermediateResultSender resultSender, final String correlationId)
      throws Exception {
    LOG.info("Received input for an import: {}", input);
    final ImportResult importResult = doImport(input);
    final ImportOutput output = convertToOutput(importResult, input);
    LOG.info("Done with ImportInput {}. Returning output.", input);
    return output;
  }

  private ImportResult doImport(final ImportInput input) throws IOException, SQLException, AeriusException {
    ImportResult importResult;
    final Importer importer = new Importer(getPMF(), input.getLocale() == null ? LocaleUtils.getDefaultLocale() : input.getLocale());
    importer.setUseValidMetaData(input.isValidateMetadata());
    importer.setIncludeSources(input.isReturnSources());
    importer.setIncludeResults(input.isPersistResults());
    importer.setValidateAgainstSchema(input.isValidate());

    try (final InputStream inputStream = new ByteArrayInputStream(input.getFileContent())) {
      final CalculatorLimits limits = getLimits(input.isCheckLimits());

      importResult = importer.convertInputStream2ImportResult(input.getFileName(), inputStream, input.getSubstance(), limits);
    } catch (final AeriusException e) {
      LOG.error("Error while importing file {}", input.getFileName(), e);
      importResult = new ImportResult();
      importResult.getExceptions().add(e);
    }

    return importResult;
  }

  private ImportOutput convertToOutput(final ImportResult importResult, final ImportInput input) throws SQLException, AeriusException {
    final ImportOutput output = new ImportOutput();
    // warnings can always be added.
    output.getWarnings().addAll(importResult.getWarnings());
    if (importResult.getExceptions().isEmpty()) {
      LOG.info("Done importing input {}. Converting to scenario", input);
      output.setCalculatedScenario(toCalculatedScenario(importResult, input));
      output.setImportedYear(importResult.getImportedYear());
      output.setImportedTemporaryPeriod(importResult.getImportedTemporaryPeriod());
      output.setVersion(importResult.getVersion());
      output.setDatabaseVersion(importResult.getDatabaseVersion());
      output.setSrid(importResult.getSrid());
      output.setType(importResult.getType());

    } else {
      LOG.info("Exceptions found while importing input {}. Exceptions: {}", input, importResult.getExceptions());
      output.getExceptions().addAll(importResult.getExceptions());
    }
    return output;
  }

  protected CalculatorLimits getLimits(final boolean checkLimits) throws SQLException {
    // If check limits is set to false, return null
    if (!checkLimits) {
      return null;
    }

    try (Connection con = getPMF().getConnection()) {
      return ContextRepository.getCalculatorLimits(con);
    }
  }

  private CalculatedScenario toCalculatedScenario(final ImportResult result, final ImportInput input) throws SQLException {
    final CalculatedScenario calculatedScenario = CalculatedScenarioUtil.toCalculatedScenario(result);
    calculatedScenario.setYear(result.getImportedYear());
    calculatedScenario.setOptions(getCalculationOptions(result, input));

    if (input.isPersistResults() && calculatedScenario.hasResultPoints()) {
      LOG.info("Inserting calculations for input {}", input);
      // inserts the calculation + results
      try (Connection con = getPMF().getConnection()) {
        CalculationRepository.insertCalculationWithResults(con, calculatedScenario);

        // fill set options based on the results in the DB
        for (final Calculation calculation : calculatedScenario.getCalculations()) {
          CalculationRepository.getCalculationSetOptions(con, calculation);
        }
      }
    }

    // clear result points, results are never needed client side. Don't want to send them unnecessarily.
    calculatedScenario.getScenario().clearResultPoints();

    return calculatedScenario;
  }

  private CalculationSetOptions getCalculationOptions(final ImportResult result, final ImportInput input) {
    final CalculationSetOptions options;
    if (input.getOptions() == null) {
      options = new CalculationSetOptions();
    } else {
      options = input.getOptions().copy();
    }
    options.setTemporaryProjectYears(result.getImportedTemporaryPeriod());
    options.setPermitCalculationRadiusType(result.getPermitCalculationRadiusType());
    return options;
  }
}
