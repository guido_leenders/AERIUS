/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.awt.Image;

/**
 * Used in {@link ImageUtils} to merge one or more of these together.
 */
public final class ImageMergeLayer {

    // The X-position of the image. 
    private int posX;

    // The Y-position of the image. 
    private int posY;

    /** Whether to use the width of this layer when calculating the width of the merged image.
     *  This mean that if this layer is wider than the calculated width of the merged image,
     *   a part of this layer will 'fall of' if set to false. By default true. */
    private boolean calculateWidth = true;

    /** Whether to use the height of this layer when calculating the height of the merged image.
     *  This mean that if this layer is higher than the calculated height of the merged image,
     *   a part of this layer will 'fall of' if set to false. By default true. */
    private boolean calculateHeight = true;

    // The image itself. 
    private Image image;

    /**
     * Construct using default positions.
     * @param image The image.
     */
    public ImageMergeLayer(final Image image) {
        this(image, 0, 0);
    }

    /**
     * Construct using self-defined positions.
     * @param image The image.
     * @param posX The X-position to use.
     * @param posY The Y-position to use.
     */
    public ImageMergeLayer(final Image image, final int posX, final int posY) {
        this(image, posX, posY, true, true);
    }

    /**
     * Construct using self-defined positions and setting whether to use the image for calculating the width
     *  and height.
     * @param image The image.
     * @param posX The X-position to use.
     * @param posY The Y-position to use.
     * @param calculateWidth whether to use the width of this layer when calculating the width of the merged image
     * @param calculateHeight whether to use the height of this layer when calculating the height of the merged image
     */
    public ImageMergeLayer(final Image image, final int posX, final int posY, final boolean calculateWidth, final boolean calculateHeight) {
        this.posX = posX;
        this.posY = posY;
        this.image = image;
        this.calculateWidth = calculateWidth;
        this.calculateHeight = calculateHeight;
    }

    /**
     * @see #posX
     * @return posX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * @see #posX
     * @param posX 
     */
    public void setPosX(final int posX) {
        this.posX = posX;
    }

    /**
     * @see #posY
     * @return posY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * @see #posY
     * @param posY 
     */
    public void setPosY(final int posY) {
        this.posY = posY;
    }

    /**
     * @see #image
     * @return image
     */
    public Image getImage() {
        return image;
    }

    /**
     * @see #calculateWidth
     * @return calculateWidth
     */
    public boolean isCalculateWidthForMergedImage() {
        return calculateWidth;
    }

    /**
     * @see #calculateWidth
     * @param calculateWidthForMergedImage 
     */
    public void setCalculateWidthForMergedImage(final boolean calculateWidthForMergedImage) {
        this.calculateWidth = calculateWidthForMergedImage;
    }

    /**
     * @see #calculateHeight
     * @return calculateHeight
     */
    public boolean isCalculateHeightForMergedImage() {
        return calculateHeight;
    }

    /**
     * @see #calculateHeight
     * @param calculateHeightForMergedImage 
     */
    public void setCalculateHeightForMergedImage(final boolean calculateHeightForMergedImage) {
        this.calculateHeight = calculateHeightForMergedImage;
    }

}
