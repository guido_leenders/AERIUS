/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.intercept.InterceptCommandLineOptions;

/**
 * Class to process the command line options for the worker.
 */
class CmdOptions {

  private static final Logger LOG = LoggerFactory.getLogger(CmdOptions.class);

  private static final String HELP = "help";
  private static final String VERSION = "version";
  private static final String CONFIG = "config";
  private static final String LOG4J = "log4j";
  private static final String STORE_PP = "storepp";
  private static final String PP_ORIGIN_ID = "ppOriginId";
  private static final String PP_START_SITE_ID = "ppStartSiteId";
  private static final String PP_START_SOURCE_ID = "ppStartSourceId";
  private static final String FILE_ARG_NAME = "file";
  private static final String NUMBER_ARG_NAME = "number";
  private static final String TYPE_ARG_NAME = "type";
  private static final String EMISSIONS = "emissions";
  private static final String YEAR = "year";
  private static final String TARGET = "target";
  private static final String REQUEST_FILES = "requestFiles";

  private static final Option HELP_OPTION = new Option(HELP, "print this message");
  private static final Option VERSION_OPTION = new Option(VERSION, "print version information");
  private static final Option CONFIG_OPTION =
      Option.builder(CONFIG).argName(FILE_ARG_NAME).hasArg().desc("background running worker options file").build();
  private static final Option LOG4J_OPTION = Option.builder(LOG4J).hasArg().argName(FILE_ARG_NAME).desc("log4 properties file").build();
  private static final Option STORE_PP_OPTION =
      Option.builder(STORE_PP).argName(FILE_ARG_NAME).hasArg()
          .desc("store 'prioritaire projecten' from the given folder").build();
  private static final Option PP_ORIGIN_ID_OPTION =
      Option.builder(PP_ORIGIN_ID).argName(NUMBER_ARG_NAME).type(Number.class).hasArg()
          .desc("The origin ID to use for the 'prioritaire projecten'").build();
  private static final Option PP_START_SITE_ID_OPTION =
      Option.builder(PP_START_SITE_ID).argName(NUMBER_ARG_NAME).type(Number.class).hasArg()
          .desc("The value to start with when handing out site IDs for the 'prioritaire projecten'").build();
  private static final Option PP_START_SOURCE_ID_OPTION =
      Option.builder(PP_START_SOURCE_ID).argName(NUMBER_ARG_NAME).type(Number.class).hasArg()
          .desc("The value to start with when handing out source IDs for the 'prioritaire projecten'").build();
  private static final Option EMISSIONS_OPTION =
      Option.builder(EMISSIONS).argName(FILE_ARG_NAME).hasArg()
          .desc("Write emission files for the given file or directoy to file or files").build();
  private static final Option YEAR_OPTION =
      Option.builder(YEAR).argName(NUMBER_ARG_NAME).type(Number.class).hasArg()
          .desc("The year the emissions must be calculated, required option with -emissions").build();
  private static final Option TARGET_OPTION =
      Option.builder(TARGET).argName(FILE_ARG_NAME).hasArg()
          .desc("The location the files will be written to, required option with -emissions and -requestFiles").build();
  private static final Option REQUEST_FILES_OPTION =
      Option.builder(REQUEST_FILES).argName(TYPE_ARG_NAME).hasArg()
          .desc("Export files for requests (pdf and gmls within the pdf). "
              + "Option should be the type to export (pronouncement or permit, default is pronouncement)").build();

  private static final String OPTION_MISSING = "The required command line argument '{}' is missing.";
  private static final String OPTION_INVALID_ARGUMENT = "The option {} has an invalid argument '{}', it should be a {}.";
  private final Options options;
  private final InterceptCommandLineOptions interceptCmdOptions;
  private final CommandLine cmd;

  public CmdOptions(final String[] args) throws ParseException {
    options = new Options();
    options.addOption(HELP_OPTION);
    options.addOption(VERSION_OPTION);
    options.addOption(CONFIG_OPTION);
    options.addOption(LOG4J_OPTION);
    options.addOption(STORE_PP_OPTION);
    options.addOption(PP_ORIGIN_ID_OPTION);
    options.addOption(PP_START_SITE_ID_OPTION);
    options.addOption(PP_START_SOURCE_ID_OPTION);
    options.addOption(EMISSIONS_OPTION);
    options.addOption(YEAR_OPTION);
    options.addOption(TARGET_OPTION);
    options.addOption(REQUEST_FILES_OPTION);
    options.addOptionGroup(InterceptCommandLineOptions.getOptions());
    cmd = new DefaultParser().parse(options, args);
    interceptCmdOptions = new InterceptCommandLineOptions(cmd);
  }

  public String getLog4JFile() throws FileNotFoundException {
    return getFileOption(LOG4J);
  }

  public String getConfigFile() throws FileNotFoundException {
    return getFileOption(CONFIG);
  }

  public String getStorePPFile() {
    return cmd.getOptionValue(STORE_PP);
  }

  public int getPPOriginId() throws ParseException {
    return parseIntOption(PP_ORIGIN_ID_OPTION);
  }

  public int getPPStartSiteId() throws ParseException {
    return parseIntOption(PP_START_SITE_ID_OPTION);
  }

  public int getPPStartSourceId() throws ParseException {
    return parseIntOption(PP_START_SOURCE_ID_OPTION);
  }

  public String getEmssionsFile() throws FileNotFoundException {
    return getFileOrDirOption(EMISSIONS);
  }

  public InterceptCommandLineOptions getInterceptCmdOptions() {
    return interceptCmdOptions;
  }

  public String getTarget() throws FileNotFoundException {
    return getFileOrDirOption(TARGET);
  }

  public String getRequestFilesType() throws FileNotFoundException {
    return cmd.getOptionValue(REQUEST_FILES);
  }

  public int getYear() throws ParseException {
    return parseIntOption(YEAR_OPTION);
  }

  private int parseIntOption(final Option option) throws ParseException {
    if (cmd.hasOption(option.getOpt())) {
      try {
        final Object result = cmd.getParsedOptionValue(option.getOpt());
        if (!(result instanceof Long)) {
          LOG.error(OPTION_INVALID_ARGUMENT, option.getOpt(), cmd.getOptionValue(option.getOpt()), option.getArgName());
        }
        return ((Long) result).intValue();
      } catch (final ParseException e) {
        LOG.error(OPTION_INVALID_ARGUMENT, option.getOpt(), cmd.getOptionValue(option.getOpt()), option.getArgName());
        throw e;
      }
    } else {
      LOG.error(OPTION_MISSING, option.getOpt());
      throw new MissingArgumentException(option);
    }
  }


  /**
   * Print help or version information if arguments specified. Returns true if the information was printed.
   * @return true if information was printed
   */
  public boolean printIfInfoOption() {
    if (cmd.hasOption(HELP)) {
      printHelp();
    } else if (cmd.hasOption(VERSION)) {
      printVersion();
    } else {
      return false;
    }
    return true;
  }

  private void printHelp() {
    final HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("aerius-worker", options, true);
  }

  private void printVersion() {
    final String implementationVersion = this.getClass().getPackage().getImplementationVersion();
    System.out.println("Version: " + implementationVersion); // NOPMD  - yes, a system.out thingy. It's meant for the user at startup.
  }

  /**
   * Get the file name argument as string and checks is the file exists.
   * @param option option to get file for
   * @return file name
   * @throws FileNotFoundException if file doesn't exist.
   */
  private String getFileOption(final String option) throws FileNotFoundException {
    if (cmd.hasOption(option)) {
      final String sFile = cmd.getOptionValue(option);
      final File file = new File(sFile);

      if (!file.exists() || !file.isFile()) {
        throw new FileNotFoundException(
            "Configuration file (" + sFile + " as supplied by -" + option + ") does not exist. ");
      }
      return sFile;
    }
    return null;
  }

  /**
   * Get the file name argument as string and checks is the file exists.
   * @param option option to get file for
   * @return file name
   * @throws FileNotFoundException if file doesn't exist.
   */
  private String getFileOrDirOption(final String option) throws FileNotFoundException {
    if (cmd.hasOption(option)) {
      final String sFile = cmd.getOptionValue(option);
      final File file = new File(sFile);

      if (!file.exists() || !(file.isFile() || file.isDirectory())) {
        throw new FileNotFoundException(
            "Configuration file (" + sFile + " as supplied by -" + option + ") does not exist. ");
      }
      return sFile;
    }
    return null;
  }
}
