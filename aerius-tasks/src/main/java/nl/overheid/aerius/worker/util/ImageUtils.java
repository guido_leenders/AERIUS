/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import nl.overheid.aerius.geo.shared.BBox;

/**
 * Various image helper methods.
 */
public final class ImageUtils {

  private ImageUtils() {
    /* Not to be extended or constructed. */
  }

  /**
   * Merges the given images. The images are merged in the order they're found
   * in the array.
   * The width and the height of the {@link BufferedImage} are calculated based
   * on the widest image and the highest image.
   * The possible repositioning via the {@link ImageMergeLayer} is calculated
   * also into the width and height of the {@link BufferedImage}.
   *
   * The {@link BufferedImage} can be used in multiple ways, though preferably
   * one of the following:
   * - {@link #writeImageToPNGFile(BufferedImage, java.io.File, Integer)}
   * - {@link javax.imageio.ImageIO#write(java.awt.image.RenderedImage, String, java.io.File)}
   * - {@link javax.imageio.ImageIO#write(java.awt.image.RenderedImage, String, java.io.OutputStream)}
   *
   * @param layers The layers to merge.
   * @return The merged image or null if one of the layers is empty.
   */
  public static BufferedImage mergeImages(final ImageMergeLayer[] layers) {
    BufferedImage image = null;

    int highestPos = 0;
    int widestPos = 0;

    // Calculate the highestPos.
    for (final ImageMergeLayer layer : layers) {
      // Validate whether the image is set.
      if (layer.getImage() == null) {
        // No image found - return null to indicate an error.
        return null;
      }

      // In case we shouldn't use this layer' height in the calculation, ignore it.
      if (!layer.isCalculateHeightForMergedImage()) {
        continue;
      }

      final int currentHeight = layer.getImage().getHeight(null) + layer.getPosY();
      if (currentHeight > highestPos) {
        highestPos = currentHeight;
      }
    }

    // Calculate the widestPos.
    for (final ImageMergeLayer layer : layers) {
      // In case we shouldn't use this layer' width in the calculation, ignore it.
      if (!layer.isCalculateWidthForMergedImage()) {
        continue;
      }

      final int currentWidth = layer.getImage().getWidth(null) + layer.getPosX();
      if (currentWidth > widestPos) {
        widestPos = currentWidth;
      }
    }

    // Create the BufferedImage with the calculated dimensions.
    image = new BufferedImage(widestPos, highestPos, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D graphic = image.createGraphics();

    try {
      // Draw the layers on to the image.
      for (final ImageMergeLayer layer : layers) {
        graphic.drawImage(layer.getImage(), layer.getPosX(), layer.getPosY(), null);
      }
    } finally {
      graphic.dispose();
    }

    return image;
  }

  /**
   * Returns a {@link Color} based on the hexValue given. Hex value should be without a hash tag. i.e.: F2H2D2, 00000, EEEEEE, AB712A etc.
   *
   * @param hexValue The hexValue to get color by.
   * @return color
   */
  public static Color getColorByHexValue(final String hexValue) {
    return Color.decode("0x" + hexValue);
  }


  /**
   * Returns a {@link Color} based on the hexValue given + opacity. Hex value should be without a hash tag. i.e.: F2H2D2, 00000, EEEEEE, AB712A etc.
   *
   * @param hexValue The hexValue to get color by.
   * @param opacity 0-255
   * @return color
   */
  public static Color getColorByHexValue(final String hexValue, final int opacity) {
    final Color color = Color.decode("0x" + hexValue);
    return new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity);
  }

  /**
   * Converts a RGB value (int) to hex value, to be used in HTML for example.
   * @param rgb RGB int value. RGB can be retrieved from a Color object using {@link Color#getRGB()}.
   * @return hex value (example: FFFFFF)
   */
  public static String getRGBToHexValue(final int rgb) {
    return Integer.toHexString(rgb & 0xFFFFFF | 0x1000000).substring(1);
  }

  /**
   * Get a new version of the given image with the given opacity.
   * @param img The image to use
   * @param opacity The opacity to apply
   * @return the new image with the opacity applied
   */
  public static BufferedImage getImageWithOpacity(final BufferedImage img, final float opacity) {
    final BufferedImage target = new BufferedImage(img.getWidth(), img.getHeight(), Transparency.TRANSLUCENT);
    final Graphics2D g = target.createGraphics();
    try {
      g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, opacity));
      g.drawImage(img, null, 0, 0);
    } finally {
      g.dispose();
    }

    return target;
  }

  /**
   * Get a hexagon image with the given size and color.
   * @param height The height of the hexagon in pixels. The width will be calculated.
   * @param color The color string as used by {@link #getColorByHexValue(String)};
   * @return Hexagon image
   */
  public static BufferedImage getHexagonImage(final int height, final String color) {
    final int width = (int) Math.round(height * 1.158);
    final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D graphic = image.createGraphics();

    final double firstFactor = 0.25;
    final double secondFactor = 0.75;

    try {
      graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      graphic.setColor(getColorByHexValue(color));

      final Polygon p = new Polygon();

      p.addPoint((int) Math.round(width * secondFactor), height);
      p.addPoint((int) Math.round(width * firstFactor), height);
      p.addPoint(0, height / 2);
      p.addPoint((int) Math.round(width * firstFactor), 0);
      p.addPoint((int) Math.round(width * secondFactor), 0);
      p.addPoint(width, height / 2);

      graphic.fillPolygon(p);
    } finally {
      graphic.dispose();
    }

    return image;
  }

  /**
   * Get a hexagon image with the given size and color.
   * @param height The height of the hexagon in pixels. The width will be calculated.
   * @param color The color string as used by {@link #getColorByHexValue(String)};
   * @return Hexagon image
   */
  public static BufferedImage getCircleImage(final int height, final String color) {
    final int width = height;
    final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D graphic = image.createGraphics();

    try {
      graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      graphic.setColor(getColorByHexValue(color));
      graphic.fillOval(0, 0, width, height);
    } finally {
      graphic.dispose();
    }

    return image;
  }

  /**
   * Generates an image with a rounded rectangle and a character in the center of it.
   * @param width The total width of the image (and the rectangle in this case).
   * @param height The total height of the image (and the rectangle in this case).
   * @param arcHeight The height of a rounded corner.
   * @param arcWidth The width of a rounded corner.
   * @param border The stroke to use as border. If null, it's not shown. Will have the same color as the text.
   * @param font The font to use when drawing the character on the rectangle.
   * @param rectangleColor The color to use to fill the rectangle with.
   * @param textColor The color to use for the character of text that is drawn over the rectangle.
   * @param text The String to draw.
   * @param widthExtraPadding Extra padding to add (or substract if negative) to the width for the text.
   * @param heightExtraPadding Extra padding to add (or substract if negative) to the height for the text.
   * @return {@link BufferedImage}
   */
  public static BufferedImage getSquareRoundedCornersImageWithCharacter(final int width, final int height, final int arcHeight, final int arcWidth,
      final BasicStroke border, final Font font, final String rectangleColor, final String textColor, final String text, final int widthExtraPadding,
      final int heightExtraPadding) {
    final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D graphic = image.createGraphics();
    final Rectangle2D boundsOfText = FontUtils.getBoundsOfString(font, graphic.getFontRenderContext(), text);

    try {
      graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      graphic.setFont(font);

      graphic.setColor(getColorByHexValue(rectangleColor));
      graphic.fillRoundRect(0, 0, width, height, arcWidth, arcHeight);

      if (border != null) {
        graphic.setColor(getColorByHexValue(textColor));
        graphic.setStroke(border);
        graphic.draw(
            new RoundRectangle2D.Double(
                0 + (border.getLineWidth() / 2), 0 + (border.getLineWidth() / 2),
                width - border.getLineWidth(), height - border.getLineWidth(),
                arcWidth, arcHeight));
        graphic.setStroke(new BasicStroke()); // reset
      }

      graphic.setColor(getColorByHexValue(textColor));
      graphic.drawString(text,
          (width / 2) - (int) (boundsOfText.getWidth() / 2) + widthExtraPadding,
          (height / 2) + (int) (boundsOfText.getHeight() / 2) + heightExtraPadding);
    } finally {
      graphic.dispose();
    }

    return image;
  }

  public static void drawExtent(final Graphics2D graphic, final BBox imageBbox, final BBox bboxToDraw, final int imageWidth, final int imageHeight,
      final int indexNumber, final Font awtFont, final String color) {
    final double textWidth = FontUtils.getWidthOfString(awtFont, graphic.getFontRenderContext(), Integer.toString(indexNumber));
    final double textHeight = FontUtils.getHeightOfString(awtFont, graphic.getFontRenderContext(), Integer.toString(indexNumber));

    final int x = (int) Math.round(imageWidth * ((bboxToDraw.getMinX() - imageBbox.getMinX()) / imageBbox.getWidth()));
    final int y = (int) Math.round(imageHeight * ((imageBbox.getMaxY() - bboxToDraw.getMaxY()) / imageBbox.getHeight()));
    final int width = (int) Math.round(imageWidth * (bboxToDraw.getWidth() / imageBbox.getWidth()));
    final int height = (int) Math.round(imageHeight * (bboxToDraw.getHeight() / imageBbox.getHeight()));

    graphic.setColor(ImageUtils.getColorByHexValue("1D4373"));
    graphic.drawRect(x, y, width, height);
    graphic.setColor(ImageUtils.getColorByHexValue(color));
    final double textHeightFix = 0.6;
    graphic.drawString(
        Integer.toString(indexNumber), x + (width / 2) - (int) (textWidth / 2), y + (height / 2) + (int) (textHeight / 2 * textHeightFix));
  }

  /**
   * Create a line (Texture) paint. The line will be centered in the middle
   * (height) and drawn from the left to the right.
   *
   * @param width
   *          Width of the paint
   * @param height
   *          Height of the paint
   * @param lineColorByHexValue
   *          The hexValue to get line color by. Should be without a hash tag.
   *          i.e.: F2H2D2, 00000, EEEEEE, AB712A etc.
   * @param stroke
   *          The stroke to use for the line.
   * @return {@link TexturePaint}
   */
  public static TexturePaint getLinePaint(final int width, final int height, final String lineColorByHexValue, final BasicStroke stroke) {
    // By using the width/height of the paintbox we'll be sure the line will be painted once and in the center.
    final BufferedImage tile = new BufferedImage(
        width,
        height,
        BufferedImage.TYPE_INT_ARGB);
    final Graphics2D tileGraphics = tile.createGraphics();
    tileGraphics.setPaint(ImageUtils.getColorByHexValue(lineColorByHexValue));

    final int linePosY = (height - (int) stroke.getLineWidth()) / 2;
    final int linePosX = 0;

    // Set the stroke with our settings.
    tileGraphics.setStroke(stroke);

    // Draw the line.
    tileGraphics.drawLine(linePosX, linePosY, width, linePosY);

    // Create the TexturePaint.
    return new TexturePaint(tile, new Rectangle2D.Double(0, 0, width, height));
  }

  /**
   * Rotate given input image.
   * @param input The input image.
   * @param rotation The rotation to apply.
   * @return rotated image.
   */
  public static BufferedImage rotate(final BufferedImage input, final Rotation rotation) {
    final BufferedImage result;

    final double theta;
    switch (rotation) {
    case CLOCKWISE:
      theta = Math.PI / 2;
      break;
    case COUNTERCLOCKWISE:
      theta = -Math.PI / 2;
      break;
    default:
      throw new IllegalArgumentException("Not implemented case in rotate()");
    }

    final AffineTransform xform = new AffineTransform();
    xform.translate(0.5 * input.getHeight(), 0.5 * input.getWidth());
    xform.rotate(theta);
    xform.translate(-0.5 * input.getWidth(), -0.5 * input.getHeight());
    final AffineTransformOp op = new AffineTransformOp(xform, AffineTransformOp.TYPE_BILINEAR);
    result = op.filter(input, null);

    return result;
  }

  /**
   * Used to define rotation in this util.
   */
  public enum Rotation {
    /** Rotate clockwise. */
    CLOCKWISE,
    /** Rotate counterclockwise. */
    COUNTERCLOCKWISE;
  }
}
