/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.Locale;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Various PDF helper methods.
 */
public final class PDFUtils {

  private static final Logger LOG = LoggerFactory.getLogger(PDFUtils.class);

  private static final String EXT_TTF = ".ttf";

  private static boolean fontsRegistered;

  private static final int SCALEBAR_PADDING_BETWEEN_BOTTOM_AND_BAR = 20;
  private static final int SCALEBAR_PADDING_BETWEEN_RIGHT_AND_BAR = 20;

  private static final int SCALEBAR_MIN_SIZE_IN_PX = 30;
  private static final int SCALEBAR_HEIGHT = 16;
  private static final String SCALEBAR_COLOR = "000000";
  private static final int[] SCALEBAR_STEPS_IN_METERS = {
      1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000
  };

  private static final int SQUARE_ROUNDED_CORNERS_IMAGE_WIDTH = 40;
  private static final int SQUARE_ROUNDED_CORNERS_IMAGE_HEIGHT = SQUARE_ROUNDED_CORNERS_IMAGE_WIDTH;
  private static final int SQUARE_ROUNDED_CORNERS_IMAGE_ARCWIDTH = 10;
  private static final int SQUARE_ROUNDED_CORNERS_IMAGE_ARCHEIGHT = SQUARE_ROUNDED_CORNERS_IMAGE_ARCWIDTH;
  private static final int SQUARE_ROUNDED_CORNERS_TEXT_SIZE = 35;
  private static final int SQUARE_ROUNDED_CORNERS_IMAGE_EXTRAPADDING_HEIGHT_TEXT = -7;
  private static final String SQUARE_ROUNDED_CORNERS_BACKGROUNDCOLOR = "06A3D6";
  private static final String SQUARE_ROUNDED_CORNERS_TEXTCOLOR = "FFFFFF";

  private static final int KILO_TO_TON_TRANSITION_POINT = 20000;

  private static final int FONT_SCALEBAR_TEXT_SIZE = 9;
  private static final String FONT_SCALEBAR_PATH = "fonts/RijksoverheidSansText-Regular_2_0.ttf";
  private static java.awt.Font fontScalebarText;

  private PDFUtils() {
    /* Not to be constructed or extended. */
  }

  /**
   * Should only be used once, all PDF fonts (for iText and AWT) will be loaded and registered.
   * @return true if everything is loaded and registered, false if it already was.
   * @throws IOException on I/O error
   */
  public static boolean initFonts() throws IOException {
    synchronized (PDFUtils.class) {
      if (!fontsRegistered) {
        /* iText. */
        readAdditionalPDFFonts();

        /* AWT.  */
        FontUtils.initFonts();

        fontsRegistered = true;
        return true;
      }

      return false;
    }
  }

  /**
   * Get the color for use in the PDF, based on a hexValue of the color.
   * @param hexValue The hex value
   * @return The {@link BaseColor}
   */
  public static Color getColor(final String hexValue) {
    return ImageUtils.getColorByHexValue(hexValue);
  }

  /**
   * The number with one decimal.
   * @param locale The locale to use.
   * @return The numberformatter with right settings.
   */
  public static NumberFormat getNumberFormatOne(final Locale locale) {
    return getNumberFormat(locale, 1);
  }

  /**
   * @param locale The locale to use.
   * @param decimals The number of decimals to use.
   * @return The numberformatter with right settings.
   */
  public static NumberFormat getNumberFormat(final Locale locale, final int decimals) {
    return getNumberFormat(locale, decimals, decimals);
  }

  /**
   * @param locale The locale to use.
   * @param minDecimals The minimum decimals to use.
   * @param maxDecimals The maximum decimals to use.
   * @return The numberformatter with right settings.
   */
  public static NumberFormat getNumberFormat(final Locale locale, final int minDecimals, final int maxDecimals) {
    final NumberFormat format = NumberFormat.getInstance(locale);
    format.setMaximumFractionDigits(maxDecimals);
    format.setMinimumFractionDigits(minDecimals);
    format.setGroupingUsed(true);
    return format;
  }

  /**
   * This allows for a standardizes way to get the emission string.
   * @param locale Locale to use to format number.
   * @param emission The emission to print.
   * @param decimals The number of decimals to use.
   * @param unitTonYear The localized unit to use for ton per year.
   * @param unitKgYear The localized unit to use for kg per year.
   * @return emission string
   */
  public static String getEmissionString(final Locale locale, final double emission, final int decimals,
      final String unitTonYear, final String unitKgYear) {
    String value = "-";
    final String unit = Math.abs(emission) >= KILO_TO_TON_TRANSITION_POINT ? unitTonYear : unitKgYear;
    final double emissionValue = Math.abs(emission) >= KILO_TO_TON_TRANSITION_POINT ? emission / SharedConstants.KILO_TO_TON : emission;
    final NumberFormat format = getNumberFormat(locale, decimals);
    if (emissionValue > 0 && emissionValue < 1) {
      value = "< 1";
    } else if (emissionValue >= 1 || emissionValue < 0) {
      value = format.format(emissionValue);
    }

    return value + (emission == 0 ? "" : " " + unit);
  }

  /**
   * Same as getImage, except null is returned instead of an exception throw if the image does not exists.
   * @see #getImage(String, Locale)
   * @param filename The filename to load
   * @param locale The locale to use (optional, may be null). At this moment only language is used, can be extended later if needed.
   * @throws DocumentException on PDF error
   * @throws IOException on IO error
   * @return Image
   */
  public static Image getImageSafe(final String filename, final Locale locale) throws DocumentException, IOException {
    try {
      return getImage(filename, locale);
    } catch (final IllegalArgumentException e) {
      /* Eat exception that is thrown if the file does not exist. */
    }

    return null;
  }

  /**
   * Get image by filename. The getResource() method called is from the class of {@link PDFUtils} itself. Use the root sign (/) to
   *  add a file in some other funky directory. Examples: 'my_logo.png', '/nl/overheid/aerius/paa/your_logo.png'
   * @param filename The filename to load
   * @param locale The locale to use (optional, may be null). At this moment only language is used, can be extended later if needed.
   * @throws DocumentException on PDF error
   * @throws IOException on IO error
   * @return Image
   */
  public static Image getImage(final String filename, final Locale locale) throws DocumentException, IOException {
    URL resourceToLoad = null;

    /* If there is a locale given, let's check if there are language specific images available. */
    if (locale != null) {
      final int extensionPos = filename.lastIndexOf('.');
      final String filenameWithoutExt;
      final String extension;

      if (extensionPos >= 0) {
        filenameWithoutExt = filename.substring(0, extensionPos);
        extension = filename.substring(extensionPos);
      } else {
        filenameWithoutExt = filename;
        extension = "";
      }

      /* Try to load image with the language string. */
      resourceToLoad = PDFUtils.class.getResource(filenameWithoutExt + "_" + locale.getLanguage() + extension);
    }

    /* If no resource is loaded yet, simply load the filename given. */
    if (resourceToLoad == null) {
      resourceToLoad = PDFUtils.class.getResource(filename);
    }

    /* If it's still not there, throw an exception. */
    if (resourceToLoad == null) {
      throw new IllegalArgumentException("Could not find filename: " + filename + ", locale: " + locale);
    }

    return Image.getInstance(resourceToLoad);
  }

  /**
   * Converts the Point given into a Point as to be used in the image (map). The assumption is made that the ratio between the
   * width and height of the image matches the ratio between the width and height of the BBox.
   *
   * @param bbox The BBox
   * @param point The point as used geographically that will be converted
   * @param width The width (without scale factor)
   * @param height The height (without scale factor)
   * @param scaleFactor The scale factor to use.
   * @return The converted point
   */
  public static Point getPositionInMapImage(final BBox bbox, final Point point, final double width, final double height, final float scaleFactor) {
    /* AWT uses upper-left origin, geometry points use lower-left origin. */
    final double x = (point.getX() - bbox.getMinX()) / bbox.getWidth() * width;
    final double y = (bbox.getHeight() - (point.getY() - bbox.getMinY())) / bbox.getHeight() * height;

    return new Point(Math.round(x * scaleFactor), Math.round(y * scaleFactor));
  }

  /**
   * Returns the delta of the data given. Only the delta of numbers will be calculated, if a column should be excluded from the calculation, supply
   *  excludeColumnsFromDelta with the zero-based index of the column set to true. i.e. <code>new boolean[] {false, false, true, false, true}</code>
   *  will skip the columns 3 and 5.
   *
   * @param first The first data
   * @param second The second data
   * @param excludeColumnsFromDelta Which columns to exclude from delta calculation, non-number columns are excluded by default.
   * @param keepFirstDatasetDefault if true, the default values will be copied from dataset 1, otherwise 2. Probably handy if you want to exclude
   *  some columns from the delta calculation but still want to use the second dataset values.
   * @return new array with the calculated delta where applicable
   * @throws IllegalArgumentException If the datasets don't have the same amount of rows/columns and such.
   */
  public static Object[][] getDeltaOfData(final Object[][] first, final Object[][] second, final boolean[] excludeColumnsFromDelta,
      final boolean keepFirstDatasetDefault) throws IllegalArgumentException {
    if (first == null || second == null) {
      return null;
    }

    if (first.length != second.length) {
      throw new IllegalArgumentException("Cannot calculate delta if the amount of rows are not the same in both datasets");
    }
    if (first.length == 0 && second.length == 0) {
      return first;
    }
    if (first[0].length != second[0].length) {
      throw new IllegalArgumentException("Cannot calculate delta if the amount of columns are not the same in both datasets");
    }

    final Object[][] data = new Object[first.length][first[0].length];

    for (int i = 0; i < first.length; i++) {
      final Object[] row1 = first[i];
      final Object[] row2 = second[i];

      if (row1.length != row2.length) {
        throw new IllegalArgumentException("Row " + (i + 1) + " of the first dataset has " + row1.length + " cells while the second has "
            + row2.length + "cells");
      }

      for (int j = 0; j < row1.length; ++j) {
        final Object cellValue1 = row1[j];
        final Object cellValue2 = row2[j];
        Object newValue;

        if (excludeColumnsFromDelta == null || excludeColumnsFromDelta[j]) {
          newValue = keepFirstDatasetDefault ? cellValue1 : cellValue2;
        } else if (cellValue1 instanceof Number || cellValue2 instanceof Number) {
          if (cellValue1.getClass() != cellValue2.getClass()) {
            throw new IllegalArgumentException("Cell " + (i + 1) + "/" + (j + 1) + " of the first dataset is of type "
                + cellValue1.getClass().getSimpleName() + " while the second is of type " + cellValue2.getClass().getSimpleName()
                + ". They should match!");
          }
          final Number numberValue1 = (Number) cellValue1;
          final Number numberValue2 = (Number) cellValue2;

          if (cellValue1 instanceof Float) {
            newValue = numberValue2.floatValue() - numberValue1.floatValue();
          } else if (cellValue1 instanceof Integer) {
            newValue = numberValue2.intValue() - numberValue1.intValue();
            /* Double and the rest. */
          } else {
            newValue = numberValue2.doubleValue() - numberValue1.doubleValue();
          }

        } else {
          newValue = keepFirstDatasetDefault ? cellValue1 : cellValue2;
        }

        data[i][j] = newValue;
      }
    }

    return data;
  }

  private static void readAdditionalPDFFonts() throws IOException {
    final String fontsPath = PDFUtils.class.getResource("/fonts/").getPath();
    /* If inside a jar, we need to use a little workaround, to make it work.
     * We're going to read the Jar file, find the font files, per font file copy them to a temporary file
     *  and register them one by one with the FontFactory. */
    if (fontsPath.contains(".jar!")) {
      JarFile jarFile = null;
      try {
        jarFile = new JarFile(PDFUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        final Enumeration<JarEntry> jarEntries = jarFile.entries();

        while (jarEntries.hasMoreElements()) {
          final JarEntry entry = jarEntries.nextElement();
          if (entry.getName().endsWith(EXT_TTF)) {
            FileOutputStream fos = null;
            InputStream fis = null;

            try {
              final File tempOutputFile = Files.createTempFile("workerfonts", EXT_TTF).toFile();
              fos = new FileOutputStream(tempOutputFile);
              fis = jarFile.getInputStream(entry);

              final int buffsize = 4096;
              final byte[] b = new byte[buffsize];
              int read;
              while ((read = fis.read(b)) != -1) {
                fos.write(b, 0, read);
              }

              LOG.info("Registering font {} to be used in the PDF export. (from {})", entry.getName(), tempOutputFile.getAbsolutePath());
              FontFactory.register(tempOutputFile.getAbsolutePath());
            } finally {
              if (fos != null) {
                fos.close();
              }
              if (fis != null) {
                fis.close();
              }
            }
          }
        }
      } finally {
        if (jarFile != null) {
          jarFile.close();
        }
      }
    } else {
      LOG.info("{} additional fonts loaded to be used in the PDF exports. (from {})",
          FontFactory.registerDirectory(fontsPath), fontsPath);
    }
  }

  /**
   * Get scaled image. The image is scaled so it will always fit for the given width/height.
   * @param bufImage The image to get.
   * @param width The width to scale to (to fit - max).
   * @param height The height to scale to (to fit - max).
   * @return Scaled PDF image.
   */
  public static Image getImage(final java.awt.Image bufImage, final int width, final int height) {
    try {
      final Image image = Image.getInstance(bufImage, null);
      image.scaleToFit(width, height);

      return image;
    } catch (final BadElementException | IOException e) {
      throw new IllegalArgumentException("Could not convert image to PDF image", e);
    }
  }

  /**
   * Turn Image into an PDF Image.
   * @param bufImage The image to get as an PDF Image.
   * @return PDF Image.
   */
  public static Image getImage(final java.awt.Image bufImage) {
    try {
      return Image.getInstance(bufImage, null);
    } catch (final BadElementException | IOException e) {
      throw new IllegalArgumentException("Could not convert image to PDF image", e);
    }
  }

  /**
   * Turn image into an PDF Image.
   * @param image The image to get as an PDF Image.
   * @return PDF Image.
   */
  public static Image getImage(final byte[] image) {
    try {
      return Image.getInstance(image);
    } catch (final BadElementException | IOException e) {
      throw new IllegalArgumentException("Could not convert image to PDF image", e);
    }
  }

  /**
   * Get scale bar layer.
   * @param bbox The BBox to use.
   * @param width The width of the layer.
   * @param height The height of the layer.
   * @param scaleFactor The scale factor to use.
   * @return ImageMergeLayer
   * @throws AeriusException On error.
   */
  public static ImageMergeLayer getScaleBarLayer(final BBox bbox, final int width, final int height, final float scaleFactor) throws AeriusException {
    final BufferedImage image = getScaleBarImage(bbox, width, scaleFactor);
    return new ImageMergeLayer(image,
        width - SCALEBAR_PADDING_BETWEEN_RIGHT_AND_BAR - image.getWidth(),
        height - SCALEBAR_PADDING_BETWEEN_BOTTOM_AND_BAR - image.getHeight());
  }

  private static BufferedImage getScaleBarImage(final BBox bbox, final int width, final float scaleFactor) throws AeriusException {
    final double onePixelInImageInMeters = bbox.getWidth() / width;
    int widthOfScaleBarToUse = -1;
    int stepToUse = -1;

    /* Find appropriate step that is at least the minimum size. The next step is at most 2.5 times bigger,
     *  so the scalebar will remain between decent sizes. */
    for (final int step : SCALEBAR_STEPS_IN_METERS) {
      final int widthOfScaleBarForStep = (int) Math.round(step / onePixelInImageInMeters);

      if (widthOfScaleBarForStep >= SCALEBAR_MIN_SIZE_IN_PX * scaleFactor) {
        widthOfScaleBarToUse = widthOfScaleBarForStep;
        stepToUse = step;
        break;
      }
    }

    /* No match - PANIC! */
    if (widthOfScaleBarToUse < 0) {
      throw new IllegalArgumentException("Could not find a suitable step for the scalebar. bbox: " + bbox
          + " - width: " + width);
    }

    /* Won't fit in the image - PANIC HARDER! */
    if (widthOfScaleBarToUse > width - SCALEBAR_PADDING_BETWEEN_RIGHT_AND_BAR) {
      throw new IllegalArgumentException("Scalebar will not fit in current image. bbox: " + bbox + " - width: " + width
          + " - width scalebar: " + widthOfScaleBarToUse);
    }

    final BufferedImage image =
        new BufferedImage(widthOfScaleBarToUse + 4, Math.round(SCALEBAR_HEIGHT * scaleFactor) + 4,
            BufferedImage.TYPE_INT_ARGB);
    final int startPos = Math.round((SCALEBAR_HEIGHT - 5) * scaleFactor);
    final int maxHeightBlock = Math.round((SCALEBAR_HEIGHT - 10) * scaleFactor);
    final String scaleText = stepToUse >= SharedConstants.M_TO_KM ? stepToUse / SharedConstants.M_TO_KM + " km"
        : stepToUse + " m";

    final Graphics2D g = image.createGraphics();
    g.setColor(ImageUtils.getColorByHexValue(SCALEBAR_COLOR));
    g.setStroke(new BasicStroke(Math.round(1.3f * scaleFactor)));
    g.setFont(getFontLegend(scaleFactor));

    g.drawLine(2, startPos, 2, startPos + maxHeightBlock);
    g.drawLine(image.getWidth() - 2, startPos, image.getWidth() - 2, startPos + maxHeightBlock);
    g.drawLine(2, startPos + maxHeightBlock, image.getWidth() - 2, startPos + maxHeightBlock);
    g.drawString(
        scaleText,
        (float) (image.getWidth() / 2.0 - FontUtils.getWidthOfString(getFontLegend(scaleFactor),
            g.getFontRenderContext(), scaleText) / 2),
        FONT_SCALEBAR_TEXT_SIZE * scaleFactor + 1);
    g.dispose();

    return image;
  }

  public static BufferedImage getIndexIconImage(final String label, final double scale, final String fontName) {
    return ImageUtils.getSquareRoundedCornersImageWithCharacter(
        (int) (SQUARE_ROUNDED_CORNERS_IMAGE_WIDTH * scale),
        (int) (SQUARE_ROUNDED_CORNERS_IMAGE_HEIGHT * scale),
        (int) (SQUARE_ROUNDED_CORNERS_IMAGE_ARCWIDTH * scale),
        (int) (SQUARE_ROUNDED_CORNERS_IMAGE_ARCHEIGHT * scale),
        null,
        new java.awt.Font(fontName, Font.BOLD, (int) (SQUARE_ROUNDED_CORNERS_TEXT_SIZE * scale)),
        SQUARE_ROUNDED_CORNERS_BACKGROUNDCOLOR,
        SQUARE_ROUNDED_CORNERS_TEXTCOLOR,
        label,
        0,
        (int) (SQUARE_ROUNDED_CORNERS_IMAGE_EXTRAPADDING_HEIGHT_TEXT * scale));
  }

  private static java.awt.Font getFontLegend(final float scaleFactor) throws AeriusException {
    synchronized (PDFUtils.class) {
      if (fontScalebarText == null) {
        try {
          fontScalebarText = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT,
              Thread.currentThread().getContextClassLoader().getResourceAsStream(FONT_SCALEBAR_PATH));
          GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(fontScalebarText);
        } catch (FontFormatException | IOException e) {
          LOG.error("getFontLegend", e);
          throw new AeriusException(Reason.INTERNAL_ERROR);
        }
      }
      return fontScalebarText.deriveFont(FONT_SCALEBAR_TEXT_SIZE * scaleFactor);
    }
  }
}
