/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfiguration;
import nl.overheid.aerius.worker.WorkerConfiguration;

/**
 * Worker queue configuration implementation.
 */
public class BrokerConfiguration extends WorkerConfiguration implements ConnectionConfiguration {

  private static final String BROKER_PREFIX = "broker";
  private static final String BROKER_HOST = "host";
  private static final String BROKER_PORT = "port";
  private static final String BROKER_USERNAME = "username";
  private static final String BROKER_PASSWORD = "password";
  private static final String BROKER_VIRTUALHOST = "virtualhost";
  private static final String BROKER_MANAGEMENT_PORT = "management.port";
  private static final String BROKER_MANAGEMENT_REFRESH_RATE = "management.refresh";

  /**
   * Configuration of broker.
   * @param properties Properties containing the predefined properties.
   */
  public BrokerConfiguration(final Properties properties) {
    super(properties, BROKER_PREFIX);
  }

  @Override
  protected List<String> getValidationErrors() {
    final List<String> reasons = new ArrayList<>();
    validateRequiredProperty(BROKER_HOST, reasons);
    validateRequiredProperty(BROKER_USERNAME, reasons);
    validateRequiredProperty(BROKER_PASSWORD, reasons);
    return reasons;
  }

  @Override
  public String getBrokerHost() {
    return getProperty(BROKER_HOST);
  }

  @Override
  public int getBrokerPort() {
    final Integer port  = getPropertyInt(BROKER_PORT);
    return port == null ? DEFAULT_BROKER_PORT : port;
  }

  @Override
  public String getBrokerUsername() {
    return getProperty(BROKER_USERNAME);
  }

  @Override
  public String getBrokerPassword() {
    return getProperty(BROKER_PASSWORD);
  }

  @Override
  public String getBrokerVirtualHost() {
    final String vhost = getProperty(BROKER_VIRTUALHOST);
    return vhost == null ? DEFAULT_BROKER_VIRTUAL_HOST : vhost;
  }

  @Override
  public int getBrokerManagementPort() {
    final Integer managementPort = getPropertyInt(BROKER_MANAGEMENT_PORT);
    return managementPort == null ? DEFAULT_BROKER_MANAGEMENT_PORT : managementPort;
  }

  @Override
  public int getManagementRefreshRate() {
    final Integer refreshRate = getPropertyInt(BROKER_MANAGEMENT_REFRESH_RATE);
    return refreshRate == null ? DEFAULT_MANAGEMENT_REFRESH_RATE : refreshRate;
  }
}
