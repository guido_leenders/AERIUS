/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailMessageWorker;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 *
 */
public class MessageWorkerFactory extends DatabaseWorkerFactory {

  private static final Logger LOG = LoggerFactory.getLogger(MessageWorkerFactory.class);

  protected MessageWorkerFactory() {
    super(WorkerType.MESSAGE, ProductType.CALCULATOR);
  }

  @Override
  protected WorkerHandler createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf) {
    validateMessages(pmf);
    return new MessageWorkerHandler(pmf);
  }

  private void validateMessages(final PMF pmf) {
    for (final MessagesEnum message : MessagesEnum.values()) {
      for (final Locale locale : LocaleUtils.KNOWN_LOCALES) {
        //just try to get the message for now. If not available, it'll throw an IllegalArgumentException and kill this worker.
        MessageRepository.getString(pmf, message, locale);
      }
    }
  }

  /**
   * Worker for the Message worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class MessageWorkerHandler extends WorkerHandlerImpl<Serializable, Serializable> {

    private final PMF pmf;

    public MessageWorkerHandler(final PMF pmf) {
      this.pmf = pmf;
    }

    @Override
    public Serializable run(final Serializable input, final WorkerIntermediateResultSender resultSender, String correlationId) throws Exception {
      final Serializable output;
      try {
        if (input instanceof MailMessageData) {
          output = handle((MailMessageData) input, resultSender);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(Reason.INTERNAL_ERROR);
      }
      return output;
    }

    private Serializable handle(final MailMessageData input, final WorkerIntermediateResultSender resultSender) throws Exception {
      return new MailMessageWorker(pmf).run(input, resultSender, null);
    }

  }
}
