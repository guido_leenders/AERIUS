/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;

/**
 * Various helper methods concerning methods and such.
 */
public final class SectorIconUtil {

  private static final String RESOURCES_DIR = "/nl/overheid/aerius/main/";

  // Not to be constructed. 
  private SectorIconUtil() { }

  /**
   * Returns an sector specific filename.
   *
   * @param icon icon enum
   * @return full path to sector image
   */
  public static String getIconFilename(final SectorIcon icon) {
    return RESOURCES_DIR + icon.name().toLowerCase().replaceAll("_", "-") + ".png";
  }

  /**
   * Returns an sector specific filename.
   *
   * @param sector the sector
   * @return full path to sector image
   */
  public static String getIconFilename(final Sector sector) {
    return getIconFilename(sector.getProperties().getIcon());
  }
}
