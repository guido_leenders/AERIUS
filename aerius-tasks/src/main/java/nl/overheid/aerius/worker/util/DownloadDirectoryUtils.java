/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * Various helper methods for managing the download directory.
 */
public final class DownloadDirectoryUtils {

  // Not to be constructed or extended.
  private DownloadDirectoryUtils() {
  }

  /**
   * Creates a new subdirectory in the given directory with an unique name, allowing any name for the file.
   * @param webappURL The url of the download base path.
   * @param directory Directory to save file in.
   * @param fileName The name to save the file as.
   * @return  It returns a storage location containing the File object for the given file name and the url to that same file.
   * @throws IOException on IO errors
   */
  public static StorageLocation createStorage(final String webappURL, final String directory, final String fileName)
      throws IOException {
    final String uuid = UUID.randomUUID().toString();
    final File storageDir = new File(directory, uuid + File.separator);

    if (!storageDir.exists() && !storageDir.mkdirs()) {
      throw new IOException("Couldn't create directories needed for path: " + storageDir.getCanonicalPath());
    }

    final File outputFile = new File(storageDir, fileName);

    if (outputFile.exists()) {
      throw new FileNotFoundException("File already exists while we are not allowed to overwrite it (overwriteAllowed flag is set to false): "
          + outputFile.getCanonicalPath());
    }

    final String url = webappURL + uuid + "/" + fileName;

    return new StorageLocation(outputFile, url);
  }

  /**
   * Simple wrapper class to combine a File object with a matching url string.
   */
  public static class StorageLocation {
    private final File outputFile;
    private final String url;

    /**
     * @return The File for the storage location.
     */
    public File getOutputFile() {
      return outputFile;
    }

    /**
     * @return The url for the storage location.
     */
    public String getUrl() {
      return url;
    }

    /**
     * Creates a new instance with the given output file and url.
     * @param outputFile The File object.
     * @param url The url string.
     */
    public StorageLocation(final File outputFile, final String url) {
      this.outputFile = outputFile;
      this.url = url;
    }
  }
}
