/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.util.Locale;
import java.util.ResourceBundle;

import nl.overheid.aerius.util.LocaleUtils;

/**
 * Util to read text given a key from a resource bundle properties file.
 */
public final class TextUtils {

  /** The bundle used by the task worker. */
  private static final String BUNDLE_NAME = "WorkerSharedMessages";

  private TextUtils() {
    //util class
  }

  /**
   * Get text for i18n purposes.
   * @param item The item to retrieve
   * @param locale The locale to use
   * @return The item value found.
   */
  public static String getText(final String item, final Locale locale) {
    final String value = ResourceBundle.getBundle(BUNDLE_NAME, locale).getString(item);
    return value.isEmpty() ? ResourceBundle.getBundle(BUNDLE_NAME, LocaleUtils.getDefaultLocale()).getString(item) : value;
  }

  /**
   * Get text for i18n purposes.
   * @param item The item to retrieve
   * @param variable The variable to use for looking for the item (used as item[variable])
   * @param locale The locale to use
   * @return The item value found.
   */
  public static String getText(final String item, final String variable, final Locale locale) {
    String value = null;
    if (variable != null && !variable.isEmpty()) {
      value = getText(item + "[" + variable + "]", locale);
    }
    return value == null || value.isEmpty() ? getText(item, locale) : value;
  }

  /**
   * Get text for i18n purposes.
   * @param item The item to retrieve
   * @param enumValue The enumValue to use for looking for the item (used as item[enumValue.name()]).
   * @param locale The locale to use
   * @return The item value found.
   */
  public static String getText(final String item, final Enum<?> enumValue, final Locale locale) {
    return getText(item, enumValue == null ? null : enumValue.name(), locale);
  }

}
