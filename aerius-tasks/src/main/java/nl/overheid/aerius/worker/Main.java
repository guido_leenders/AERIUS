/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculator.JobCleanupWorkerFactory;
import nl.overheid.aerius.ops.OPSWorkerFactory;
import nl.overheid.aerius.register.DepositionSpaceWorkerFactory;
import nl.overheid.aerius.register.RequestCalculationWorkerFactory;
import nl.overheid.aerius.register.RequestDecreeGeneratorWorkerFactory;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.srm.worker.SRMWorkerFactory;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.base.ScheduledWorkerFactory;

/**
 * Main class for all workers.
 */
public class Main {
  private static final Logger LOG = LoggerFactory.getLogger(Main.class);

  private static final WorkerFactory<?>[] WORKER_FACTORIES = {
    new OPSWorkerFactory(),
    new SRMWorkerFactory(),
    new CalculatorWorkerFactory(WorkerType.CALCULATOR),
    new CalculatorWorkerFactory(WorkerType.CONNECT),
    // temporarily disable Melding worker - might be removed in next release
//    new MeldingWorkerFactory(),
    new RegisterWorkerFactory(),
    new MessageWorkerFactory(),
    new ImportWorkerFactory(WorkerType.IMPORT_REGISTER),
  };

  private static final ScheduledWorkerFactory<?>[] SCHEDULED_WORKERS = {
    new RequestCalculationWorkerFactory(SegmentType.PROJECTS),
    new RequestCalculationWorkerFactory(SegmentType.PRIORITY_SUBPROJECTS),
    new RequestDecreeGeneratorWorkerFactory(RequestFileType.DECREE, SegmentType.PROJECTS),
    new RequestDecreeGeneratorWorkerFactory(RequestFileType.DECREE, SegmentType.PRIORITY_SUBPROJECTS),
    new RequestDecreeGeneratorWorkerFactory(RequestFileType.DETAIL_DECREE, SegmentType.PROJECTS),
    new RequestDecreeGeneratorWorkerFactory(RequestFileType.DETAIL_DECREE, SegmentType.PRIORITY_SUBPROJECTS),
    new DepositionSpaceWorkerFactory(),
    new JobCleanupWorkerFactory(),
  };


  public static void main(final String[] args) {
    try {
      final WorkerController worker = new WorkerController(WORKER_FACTORIES, SCHEDULED_WORKERS);
      worker.start(args);
    } catch (final Exception e) {
      LOG.error("Exception in main. ", e);
      System.exit(1); // NOPMD - not a JEE application, so we can exit as we like.
    }
  }
}
