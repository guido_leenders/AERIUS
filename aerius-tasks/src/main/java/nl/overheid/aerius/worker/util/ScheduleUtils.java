/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import nl.overheid.aerius.util.LocaleUtils;

/**
 * Various Scheduling helper methods.
 */
public final class ScheduleUtils {

  private static final String START_TIME_FORMAT = "HH:mm";

  // Not to be constructed.
  private ScheduleUtils() { }

  public static Date determineStartTime(final String timeString) throws ParseException {
    final SimpleDateFormat format = new SimpleDateFormat(START_TIME_FORMAT, LocaleUtils.getDefaultLocale());
    final Calendar calendar = Calendar.getInstance();
    final Calendar startTime = Calendar.getInstance();
    startTime.setTime(format.parse(timeString));
    startTime.set(Calendar.DATE, calendar.get(Calendar.DATE));
    startTime.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
    startTime.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
    if (startTime.before(Calendar.getInstance())) {
      startTime.add(Calendar.DATE, 1);
    }
    return startTime.getTime();
  }

}
