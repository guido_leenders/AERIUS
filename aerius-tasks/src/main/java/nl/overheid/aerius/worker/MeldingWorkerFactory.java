/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker;

import java.io.Serializable;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.register.MeldingPayloadData;
import nl.overheid.aerius.register.MeldingPayloadWorker;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * DatabaseWorkerFactory for melding on register database worker.
 */
public class MeldingWorkerFactory extends DatabaseWorkerFactory {
  private static final Logger LOG = LoggerFactory.getLogger(MeldingWorkerFactory.class);

  public MeldingWorkerFactory() {
    super(WorkerType.MELDING, ProductType.REGISTER);
  }

  @Override
  protected WorkerHandler createWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf)
      throws SQLException {
    return new MeldingWorkerHandler(configuration, factory, pmf);
  }

  /**
   * Worker for the Database worker queue. This worker sends the data to the specific
   * database worker.
   */
  private static class MeldingWorkerHandler extends WorkerHandlerImpl<Serializable, Serializable> {

    private final BrokerConnectionFactory factory;
    private final DBWorkerConfiguration configuration;
    private final PMF pmf;
    private final CalculatorBuildDirector calculatorBuildDirector;

    /**
     * @param configuration The database configuration to use for setting up connections with the DB and such
     * @param factory broker connection factory
     * @throws SQLException
     */
    public MeldingWorkerHandler(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final PMF pmf) throws SQLException {
      this.configuration = configuration;
      this.factory = factory;
      this.pmf = pmf;
      calculatorBuildDirector = new CalculatorBuildDirector(pmf, factory);
    }

    @Override
    public Serializable run(final Serializable input, final WorkerIntermediateResultSender resultSender, final String correlationId)
        throws Exception {
      final Serializable output;
      try {
        if (input instanceof MeldingPayloadData) {
          output = handle((MeldingPayloadData) input, resultSender);
        } else {
          throw new UnsupportedOperationException("Worker does not know how to handle the input:"
              + input.getClass().getName());
        }
      } catch (final SQLException e) {
        LOG.error("SQLException for " + input.getClass(), e);
        return new AeriusException(Reason.INTERNAL_ERROR);
      }
      return output;
    }

    private Serializable handle(final MeldingPayloadData input, final WorkerIntermediateResultSender resultSender) throws Exception {
      return new MeldingPayloadWorker(calculatorBuildDirector, pmf, configuration, factory).run(input, resultSender, null);
    }
  }
}
