/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.util;

import nl.overheid.aerius.shared.domain.sector.category.AnimalType;

/**
 * Various helper methods concerning farms and such.
 */
public final class FarmUtil {

  private static final String RESOURCES_DIR = "/nl/overheid/aerius/main/";

  // Not to be constructed.
  private FarmUtil() { }

  /**
   * Returns an animal specific filename. The category should be a RAV-code name.
   *
   * @param category RAV-code
   * @return full path to animal image
   */
  public static String getAnimalImageFilename(final String category) {
    final String filename;

    switch (AnimalType.getByCode(category)) {
      case COW:
        filename = "animal-cow.png";
        break;
      case SHEEP:
        filename = "animal-sheep.png";
        break;
      case GOAT:
        filename = "animal-goat.png";
        break;
      case PIG:
        filename = "animal-pig.png";
        break;
      case CHICKEN:
        filename = "animal-chicken.png";
        break;
      case TURKEY:
        filename = "animal-turkey.png";
        break;
      case HORSE:
        filename = "animal-horse.png";
        break;
      case DUCK:
        filename = "animal-duck.png";
        break;
      case MINK:
        filename = "animal-mink.png";
        break;
      case RABBIT:
        filename = "animal-rabbit.png";
        break;
      case GUINEA_FOWL:
        filename = "animal-guinea-fowl.png";
        break;
      case OSTRICH:
        filename = "animal-ostrich.png";
        break;
      default: // show Mixed icon for other categories...
        filename = "animal-mixed.png";
        break;
    }
    return RESOURCES_DIR + filename;
  }
}
