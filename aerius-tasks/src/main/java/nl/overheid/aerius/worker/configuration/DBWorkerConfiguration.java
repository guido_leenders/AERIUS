/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.worker.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerConfiguration;

/**
 * WorkerConfiguration assures the properties for OPS are always retrieved
 * by the same property name throughout the application.
 */
public class DBWorkerConfiguration extends WorkerConfiguration {

  // The DB username.
  private static final String DATABASE_USERNAME_KEY = "username";
  // The DB password.
  private static final String DATABASE_PASSWORD_KEY = "password";
  private static final String PAA_DOWNLOADS_DIRECTORY = "paa.downloads.directory";
  private static final String GML_DOWNLOADS_DIRECTORY = "gml.downloads.directory";
  private static final String DATABASE_URL_SUFFIX = ".URL";
  private static final String REQUEST_CALCULATION_WORKER = "register.request.calculation.";
  private static final String REQUEST_DECREE_GENERATOR_WORKER = "register.request.generator.";
  private static final String DEPOSITION_SPACE_WORKER = "register.depositionspace.calculation";
  private static final String DEPOSITION_SPACE_WORKER_START_TIME = "register.depositionspace.starttime";
  private static final String JOBCLEANUP_WORKER_ENABLED = "connect.jobcleanup.enabled";
  private static final String JOBCLEANUP_WORKER_AFTER_DAYS = "connect.jobcleanup.after_days";
  private final ProductType productType;
  private final WorkerType workerType;

  /**
   * Configuration for DB workers.
   * @param properties Properties containing the predefined properties.
   * @param productType product database
   * @param workerType
   */
  public DBWorkerConfiguration(final Properties properties, final ProductType productType, final WorkerType workerType) {
    super(properties, "database");
    this.productType = productType;
    this.workerType = workerType;
  }

  @Override
  protected List<String> getValidationErrors() {
    final List<String> reasons = new ArrayList<>();
    if (getProcesses() > 0) {
      validateDirectoryProperty(PAA_DOWNLOADS_DIRECTORY, reasons, true);
      validateDirectoryProperty(GML_DOWNLOADS_DIRECTORY, reasons, true);
      validateRequiredProperty(getDatabaseUrlKey(), reasons);
      validateRequiredProperty(DATABASE_USERNAME_KEY, reasons);
      validateRequiredProperty(DATABASE_PASSWORD_KEY, reasons);
    }
    return reasons;
  }

  public String getDatabaseUrlKey() {
    return workerType.propertyName() + DATABASE_URL_SUFFIX;
  }

  public String getDatabaseUrl() {
    return getProperty(getDatabaseUrlKey());
  }

  public ProductType getProductType() {
    return productType;
  }

  @Override
  public int getProcesses() {
    final String workerTypeName = workerType.propertyName();
    final String dbProcesses = getProperty(workerTypeName + "." + PROCESSES);
    final int processes;

    if (dbProcesses == null) {
      processes = super.getProcesses();
    } else {
      processes = getProcesses(workerTypeName + "." + PROCESSES);
    }
    return processes;
  }

  /**
   * @return The directory where generated PAA files should be written to make them available for downloading.
   */
  public String getPAADownloadDirectory() {
    return getProperty(PAA_DOWNLOADS_DIRECTORY);
  }

  /**
   * @return The directory where generated GML files should be written to make them available for downloading.
   */
  public String getGMLDownloadDirectory() {
    return getProperty(GML_DOWNLOADS_DIRECTORY);
  }

  public String getDatabaseUsername() {
    return getProperty(DATABASE_USERNAME_KEY);
  }

  public String getDatabasePassword() {
    return getProperty(DATABASE_PASSWORD_KEY);
  }

  public boolean isPermitCalculationWorker(final SegmentType segmentType) {
    return getPropertyBooleanSafe(REQUEST_CALCULATION_WORKER + segmentType.name().toLowerCase());
  }

  public boolean isPermitDecreeGeneratorWorker(final SegmentType segmentType, final RequestFileType generateFileType) {
    return getPropertyBooleanSafe(REQUEST_DECREE_GENERATOR_WORKER + segmentType.name().toLowerCase()
        + "." + generateFileType.name().toLowerCase());
  }

  public boolean isDepositionSpaceWorker() {
    return getPropertyBooleanSafe(DEPOSITION_SPACE_WORKER);
  }

  /**
   * @return
   */
  public String getDepositionSpaceStartTime() {
    return getProperty(DEPOSITION_SPACE_WORKER_START_TIME);
  }

  public boolean isJobCleanupEnabled() {
    return getPropertyBooleanSafe(JOBCLEANUP_WORKER_ENABLED);
  }

  public int getJobCleanupAfterDays() {
    return getPropertyIntSafe(JOBCLEANUP_WORKER_AFTER_DAYS);
  }

}
