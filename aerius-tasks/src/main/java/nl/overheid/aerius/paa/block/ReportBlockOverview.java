/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;

import nl.overheid.aerius.paa.common.PAAAnchors;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.table.OverviewTotalEmissionDataTable;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 *
 */
public abstract class ReportBlockOverview<T extends PAAContext> extends PAAReportBlock<T> {

  protected static final String DATE_CALCULATION_FORMAT = "dd MMMM yyyy, HH:mm";

  private static final int HEADER_LOGO_PERCENTAGE_OF_WIDTH_PAGE = 45;

  protected ReportBlockOverview(final PDFDocumentWrapper pdfDocument, final T paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws DocumentException, IOException, AeriusException, SQLException {
    addLogo();

    /* Contact. */
    addContact();

    /* Activity. */
    addActivity();

    /* Total emission. */
    addTotalEmission();

    /* Project contribution. */
    addProjectContribution();

    /* Explanation. */
    addExplanation();
  }

  protected void addLogo() throws DocumentException, IOException {
    pdfDocument.addPadding(PADDING_BETWEEN_TEXT_AND_TABLE);

    pdfDocument.addElement(getLogoTable());

    pdfDocument.addPadding(PADDING_BETWEEN_BLOCKS);
  }

  private PDFSimpleTable getLogoTable() throws DocumentException, IOException {
    final Image headerLogo = PDFUtils.getImage(paaContext.getHeaderLogoFileName(), paaContext.getLocale());
    final PDFSimpleTable logoTable = PAAUtil.getImageTableToFit(headerLogo, HEADER_LOGO_PERCENTAGE_OF_WIDTH_PAGE, 1f);
    logoTable.get().setHorizontalAlignment(Element.ALIGN_LEFT);
    return logoTable;
  }

  protected void addContact() throws DocumentException, IOException {
    /* Contact. */
    final PDFSimpleTable contactTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCell(contactTable, paaContext.getText("base_contact_title"), PAAAnchors.getAnchorReferenceToFeatureBlock());
    contactTable.add(getContactTable(paaContext));
    addTableToDocument(contactTable);
  }

  protected abstract PAABaseTable<?> getContactTable(final T paaContext);

  protected void addActivity() throws DocumentException {
    final PDFSimpleTable activityTableWrapper = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCell(activityTableWrapper, paaContext.getText("base_activity_title"));
    final List<PAABaseTable<?>> activityTables = getActivityTables();
    for (final PAABaseTable<?> activityTable : activityTables) {
      if (activityTables.indexOf(activityTable) != 0) {
        activityTableWrapper.addEmptyCell();
        activityTableWrapper.addEmptyCell(PADDING_BETWEEN_TABLES);
        activityTableWrapper.addEmptyCell();
      }

      activityTableWrapper.add(activityTable);
    }
    addTableToDocument(activityTableWrapper);
  }

  protected abstract List<PAABaseTable<?>> getActivityTables();

  protected void addTotalEmission() throws DocumentException {
    final PDFSimpleTable totalEmissionTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCell(totalEmissionTable, paaContext.getText("base_totalemission_title"));
    totalEmissionTable.add(new OverviewTotalEmissionDataTable(paaContext, set1, set2));
    addTableToDocument(totalEmissionTable);
  }

  protected void addProjectContribution() throws DocumentException, SQLException, IOException {
    new ReportSubBlockProjectContribution(pdfDocument, paaContext).generate();

    pdfDocument.addPadding(PADDING_BETWEEN_TABLES);
  }

  protected void addExplanation() throws DocumentException {
    new ReportSubBlockBaseExplanation(pdfDocument, paaContext).generate();
  }

  private void addTableToDocument(final PDFSimpleTable table) throws DocumentException {
    pdfDocument.addElement(table);

    pdfDocument.addPadding(PADDING_BETWEEN_TABLES);
  }

}
