/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;

import nl.overheid.aerius.paa.base.PAAFrontPage;
import nl.overheid.aerius.paa.common.PAAContext;

/**
 *
 */
public class DetailDecreePAAFrontPage extends PAAFrontPage {

  protected DetailDecreePAAFrontPage(final PAAContext paaContext) {
    super(paaContext);
  }

  @Override
  protected float addIndexBlock(final PdfWriter writer, final float startPosY) throws DocumentException, IOException {
    //no index for this document.
    return startPosY;
  }

}
