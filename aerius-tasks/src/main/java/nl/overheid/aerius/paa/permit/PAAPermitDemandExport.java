/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
//package nl.overheid.aerius.paa.permit;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import nl.overheid.aerius.StringDataSource;
//import nl.overheid.aerius.calculation.domain.CalculationInputData;
//import nl.overheid.aerius.db.PMF;
//import nl.overheid.aerius.paa.base.PAAExport;
//import nl.overheid.aerius.paa.block.PAAReportBlock;
//import nl.overheid.aerius.paa.calculation.CalculationReportBlockOverview;
//import nl.overheid.aerius.paa.common.PAAContext;
//import nl.overheid.aerius.paa.common.PAAContextUtil;
//import nl.overheid.aerius.paa.common.PAAReportType;
//import nl.overheid.aerius.shared.domain.register.SegmentType;
//import nl.overheid.aerius.shared.exception.AeriusException;
//import nl.overheid.aerius.shared.exception.AeriusException.Reason;
//
///**
// * A 'Vergunningsbijlage - Demand' export.
// */
//public class PAAPermitDemandExport extends PAAExport<PAAContext> {
//
//  private static final Logger LOG = LoggerFactory.getLogger(PAAPermitDemandExport.class);
//
//  /**
//   * @param pmf The PMF to use.
//   * @param httpClient The HTTP client to use when using stuff like WMS images.
//   * @param data The data that should be used when constructing this PAA.
//   * @param gmlStrings The GML's to be used for metadata (import reasons).
//   * @throws SQLException In case of database errors.
//   * @throws AeriusException
//   */
//  public PAAPermitDemandExport(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data,
//      final List<StringDataSource> gmlStrings) throws SQLException, AeriusException {
//    super(createContext(pmf, httpClient, data), gmlStrings);
//
//    // This export cannot be done for comparisons. Fail with an internal error.
//    if (getContext().isComparison()) {
//      LOG.error("Some weirdo tried to generate a PERMIT_DEMAND for a comparison.. which is not possible. Failing with an INTERNAL_ERROR.");
//      throw new AeriusException(Reason.INTERNAL_ERROR);
//    }
//  }
//
//  private static PAAContext createContext(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data)
//      throws SQLException, AeriusException {
//    return PAAContextUtil.createContext(pmf, httpClient, data, SegmentType.PROJECTS, true, PAAReportType.PERMIT_DEMAND);
//  }
//
//  @Override
//  protected List<PAAReportBlock<?>> getReportBlocks() {
//    final List<PAAReportBlock<?>> reportBlocks = new ArrayList<>();
//
//    reportBlocks.add(new CalculationReportBlockOverview(pdfDocument, getContext()));
//
//    addSourceRecapBlocks(reportBlocks);
//    addDepositionBlocks(reportBlocks, true, true, false, false);
//    addSourceBlocks(reportBlocks);
//    addDisclaimerBlocks(reportBlocks);
//
//    return reportBlocks;
//  }
//
//}
