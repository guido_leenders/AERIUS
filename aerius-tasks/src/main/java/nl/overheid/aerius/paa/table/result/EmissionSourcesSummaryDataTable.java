/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.awt.image.BufferedImage;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;

import com.lowagie.text.Image;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFontsAwt;
import nl.overheid.aerius.paa.common.PAASourcesUtil;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.marker.MarkerImagesFactory;
import nl.overheid.aerius.pdf.table.EmissionPDFColumn;
import nl.overheid.aerius.pdf.table.ImagePDFColumn;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.util.PDFUtils;
import nl.overheid.aerius.worker.util.SectorIconUtil;

/**
 *
 */
public class EmissionSourcesSummaryDataTable extends PAABaseTable<EmissionSource> {

  private static final int IMAGE_SOURCEMARKER_SCALE_TO_PERCENT = 30;
  private static final int IMAGE_SECTOR_SCALE_TO_PERCENT = 75;

  enum Column implements PDFColumnProperties {
    SOURCEMARKER("sourcemarker", 8),
    SECTOR("sector", 7),
    SOURCE("source", 49),
    EMISSION_NH3("emission_nh3", 18),
    EMISSION_NOX("emission_nox", 18);

    private final String headerKey;
    private final float relativeWidth;

    private Column(final String headerKey, final float relativeWidth) {
      this.headerKey = "emissionsources_summary_datatable_" + headerKey;
      this.relativeWidth = relativeWidth;
    }

    @Override
    public String getHeaderI18nKey() {
      return headerKey;
    }

    @Override
    public float getRelativeWidth() {
      return relativeWidth;
    }
  }

  private final int year;

  public EmissionSourcesSummaryDataTable(final PAAContext paaContext, final List<EmissionSource> sources, final int year) {
    super(paaContext.getLocale());
    this.year = year;
    setData(sources);

    addColumns(getLocale());
  }

  private void addColumns(final Locale locale) {
    addSourceMarker();
    addSectorColumn();
    addSourceColumn(locale);
    addEmissionColumn(locale, Column.EMISSION_NH3, new EmissionValueKey(year, Substance.NH3));
    addEmissionColumn(locale, Column.EMISSION_NOX, new EmissionValueKey(year, Substance.NOX));
  }

  private void addSourceMarker() {
    addColumn(new ImagePDFColumn<EmissionSource>(Column.SOURCEMARKER) {

      @Override
      protected Image getImage(final EmissionSource data) {
        return getSourceMarkerImage(data);
      }

    });
  }

  private void addSectorColumn() {
    addColumn(new ImagePDFColumn<EmissionSource>(Column.SECTOR) {

      @Override
      protected Image getImage(final EmissionSource data) {
        return getSectorIcon(data.getSector());
      }
    });
  }

  private void addSourceColumn(final Locale locale) {
    addColumn(new TextPDFColumn<EmissionSource>(locale, Column.SOURCE) {

      @Override
      protected String getText(final EmissionSource data) {
        return MessageFormat.format(
            getText("emissionsources_summary_datatable_source_cell"),
            data.getLabel(),
            getText("sectorGroup", data.getSector().getSectorGroup()),
            data.getSector().getName());
      }

    });
  }

  private void addEmissionColumn(final Locale locale, final Column column, final EmissionValueKey evk) {
    addColumnRightAligned(new EmissionPDFColumn<EmissionSource>(locale, column) {

      @Override
      protected double getEmission(final EmissionSource data) {
        return data.getEmission(evk);
      }

    });
  }

  private Image getSourceMarkerImage(final EmissionSource source) {
    try {
      final BufferedImage markerImage =
        MarkerImagesFactory.createSourceMarkerImage(PAASourcesUtil.getMarker(source), PAASharedFontsAwt.getFontMarkers(), 2).getMarkersImage();

      final Image image = PDFUtils.getImage(markerImage);
      image.scalePercent(IMAGE_SOURCEMARKER_SCALE_TO_PERCENT);
      return image;
    } catch (final AeriusException e) {
      throw new IllegalArgumentException("Error loading shared AWT fonts", e);
    }
  }

  private Image getSectorIcon(final Sector sector) {
    return getImage(SectorIconUtil.getIconFilename(sector), IMAGE_SECTOR_SCALE_TO_PERCENT);
  }

}
