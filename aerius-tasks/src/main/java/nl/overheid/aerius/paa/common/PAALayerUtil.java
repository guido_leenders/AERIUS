/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Polygon;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.pdf.PDFLayerStack;
import nl.overheid.aerius.pdf.marker.Marker;
import nl.overheid.aerius.pdf.marker.Marker.MarkerDecoration;
import nl.overheid.aerius.pdf.marker.MarkerImagesFactory;
import nl.overheid.aerius.shared.domain.geo.ClusteredReceptorPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.util.ClusterUtil;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 *
 */
public final class PAALayerUtil {

  private static final double MINIMUM_CLUSTER_THRESHOLD = 30;

  private PAALayerUtil() {
    //util class
  }

  /**
   * Get a layer with all the sources given. emissionSourceList2 is optional, only provide if we're comparing both sourceLists.
   * @param bbox The bounding box.
   * @param width The width (without scale factor).
   * @param height The height (without scale factor).
   * @param emissionSourceList1 The first sourceList.
   * @param emissionSourceList2 The (optional) second sourceList.
   * @param singleSourceMode In singleSourceMode polygons will also be drawn, otherwise they're ignored.
   * @param markerClusterthreshold Threshold to use when clustering markers.
   * @return List containing all the image layers.
   * @throws IOException On I/O errors.
   * @throws AeriusException On PAA errors.
   */
  public static List<PDFImageLayer> getLayersForSources(final BBox bbox, final int width, final int height,
      final ArrayList<EmissionSource> emissionSourceList1, final ArrayList<EmissionSource> emissionSourceList2,
      final boolean singleSourceMode, final int markerClusterthreshold) throws AeriusException {
    final List<PDFImageLayer> layers = new ArrayList<>();

    final BBox fixedBBox = GeometryUtil.getFixedBBox(bbox, Math.round(width * PAASharedConstants.QUALITY_SETTING),
        Math.round(height * PAASharedConstants.QUALITY_SETTING));

    for (final EmissionSource source : emissionSourceList1) {
      layers.addAll(getLayersForSource(fixedBBox, width, height, source, singleSourceMode));
    }

    layers.add(getMarkerLayer(fixedBBox, width, height, emissionSourceList1, emissionSourceList2, markerClusterthreshold));

    return layers;
  }

  private static List<PDFImageLayer> getLayersForSource(final BBox fixedBBox, final int width, final int height, final EmissionSource source,
      final boolean singleSourceMode) {
    final List<PDFImageLayer> layers = new ArrayList<>();
    PDFImageLayer layer = null;
    switch (source.getGeometry().getType()) {
    case POINT:
      // Nothing to do, markers will be added separately.
      break;
    case POLYGON:
      if (singleSourceMode) {
        layer = PAAImageLayerUtil.getLayerForPolygon(fixedBBox, width, height,
            (Polygon) GeometryUtil.getGeometryUnsafe(source.getGeometry().getWKT()));
      }
      break;
    case LINE:
      layer = PAAImageLayerUtil.getLayerForLine(fixedBBox, width, height,
          (LineString) GeometryUtil.getGeometryUnsafe(source.getGeometry().getWKT()));
      break;
    default:
      throw new IllegalArgumentException("Not yet implemented!");
    }

    if (layer != null) {
      layers.add(layer);
    }

    if (source instanceof MaritimeMooringEmissionSource) {
      layers.addAll(getLayersForMaritimeSource(fixedBBox, width, height, (MaritimeMooringEmissionSource) source));
    } else if (source instanceof InlandMooringEmissionSource) {
      layers.addAll(getLayersForInlandSource(fixedBBox, width, height, (InlandMooringEmissionSource) source));
    }
    return layers;
  }

  private static List<PDFImageLayer> getLayersForMaritimeSource(final BBox fixedBBox, final int width, final int height,
      final MaritimeMooringEmissionSource source) {
    final List<PDFImageLayer> layers = new ArrayList<>();
    for (final MooringMaritimeVesselGroup vesselGroup : source.getEmissionSubSources()) {
      if (vesselGroup.getInlandRoute() != null) {
        layers.add(PAAImageLayerUtil.getLayerForTransferRoute(fixedBBox, width, height, vesselGroup.getInlandRoute()));
      }
      for (final MaritimeRoute mtr : vesselGroup.getMaritimeRoutes()) {
        layers.add(PAAImageLayerUtil.getLayerForTransferRoute(fixedBBox, width, height, mtr.getRoute()));
      }
    }
    return layers;
  }

  private static List<PDFImageLayer> getLayersForInlandSource(final BBox fixedBBox, final int width, final int height,
      final InlandMooringEmissionSource source) {
    final List<PDFImageLayer> layers = new ArrayList<>();
    for (final InlandMooringVesselGroup vesselGroup : source.getEmissionSubSources()) {
      for (final InlandMooringRoute imr : vesselGroup.getRoutes()) {
        layers.add(PAAImageLayerUtil.getLayerForTransferRoute(fixedBBox, width, height, imr.getRoute()));
      }
    }
    return layers;
  }

  private static PDFImageLayer getMarkerLayer(final BBox bbox, final int width, final int height,
      final ArrayList<EmissionSource> emissionSourceList1, final ArrayList<EmissionSource> emissionSourceList2, final int threshold)
      throws AeriusException {
    final int widthWithScaleFactor = Math.round(width * PAASharedConstants.QUALITY_SETTING);
    final int heightWithScaleFactor = Math.round(height * PAASharedConstants.QUALITY_SETTING);

    final BufferedImage image = new BufferedImage(widthWithScaleFactor, heightWithScaleFactor,
        BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g = image.createGraphics();

    try {
      final List<EmissionSource> sources = getAllSources(emissionSourceList1, emissionSourceList2);

      final Set<ClusteredReceptorPoint<EmissionSource>> clusteredPoints = clusterSources(sources, bbox, threshold);

      for (final ClusteredReceptorPoint<EmissionSource> clusterPoint : clusteredPoints) {
        drawSourceCluster(g, clusterPoint, bbox, width, height, emissionSourceList1, emissionSourceList2);
      }

      final Set<ShippingRoute> routes = getShippingRoutes(sources);
      for (final ShippingRoute route : routes) {
        drawShippingRoute(g, route, bbox, width, height);
      }

    } finally {
      g.dispose();
    }

    return new PDFImageLayer(image);
  }

  private static Set<ClusteredReceptorPoint<EmissionSource>> clusterSources(final List<EmissionSource> sources, final BBox bbox,
      final int threshold) {
    double clusterThreshold = bbox.getWidth() / threshold;
    if (clusterThreshold < MINIMUM_CLUSTER_THRESHOLD) {
      clusterThreshold = 0;
    }
    return ClusterUtil.clusterGenericPoints(sources, clusterThreshold);
  }

  private static List<EmissionSource> getAllSources(final ArrayList<EmissionSource> emissionSourceList1,
      final ArrayList<EmissionSource> emissionSourceList2) {
    final ArrayList<EmissionSource> sources = new ArrayList<>(emissionSourceList1);
    if (emissionSourceList2 != null) {
      for (final EmissionSource source : emissionSourceList2) {
        if (!emissionSourceList1.contains(source)) {
          sources.add(source);
        }
      }
    }
    return sources;
  }

  private static void drawSourceCluster(final Graphics2D g, final ClusteredReceptorPoint<EmissionSource> clusterPoint, final BBox bbox,
      final int width, final int height, final List<EmissionSource> emissionSourceList1, final List<EmissionSource> emissionSourceList2)
      throws AeriusException {
    final Point positionInMap = PDFUtils.getPositionInMapImage(bbox, clusterPoint, width, height, PAASharedConstants.QUALITY_SETTING);

    final List<Marker> markers = new ArrayList<>();
    for (final EmissionSource source : clusterPoint.getPoints()) {
      final Marker marker = PAASourcesUtil.getMarker(source);
      markers.add(marker);

      if (emissionSourceList2 != null) {
        for (final EmissionSource source1 : emissionSourceList1) {
          if (source1.equals(source)) {
            marker.addDecoration(MarkerDecoration.UNDERLINE);
          }
        }
        for (final EmissionSource source2 : emissionSourceList2) {
          if (source2.equals(source)) {
            marker.addDecoration(MarkerDecoration.DOTTED_UNDERLINE);
          }
        }
      }

    }

    final BufferedImage markerImage = MarkerImagesFactory.createSourceMarkerImage(markers, PAASharedFontsAwt.getFontMarkers(), 2)
        .getMarkersWithTriangleImage();
    g.drawImage(markerImage, (int) positionInMap.getX(), (int) positionInMap.getY(), null);
  }

  private static Set<ShippingRoute> getShippingRoutes(final List<EmissionSource> sources) {
    final Set<ShippingRoute> routes = new HashSet<>();
    for (final EmissionSource source : sources) {
      if (source instanceof MaritimeMooringEmissionSource) {
        final MaritimeMooringEmissionSource mmev = (MaritimeMooringEmissionSource) source;
        for (final MooringMaritimeVesselGroup vg : mmev.getEmissionSubSources()) {
          routes.add(vg.getInlandRoute());
          for (final MaritimeRoute maritimeRoute : vg.getMaritimeRoutes()) {
            routes.add(maritimeRoute.getRoute());
          }
        }
      } else if (source instanceof InlandMooringEmissionSource) {
        final InlandMooringEmissionSource imev = (InlandMooringEmissionSource) source;
        for (final InlandMooringVesselGroup vg : imev.getEmissionSubSources()) {
          for (final InlandMooringRoute inlandRoute : vg.getRoutes()) {
            routes.add(inlandRoute.getRoute());
          }
        }
      }
    }
    return routes;
  }

  private static void drawShippingRoute(final Graphics2D g, final ShippingRoute route, final BBox bbox,
      final int width, final int height) throws AeriusException {
    final Point positionInMap = PDFUtils.getPositionInMapImage(bbox, route, width, height, PAASharedConstants.QUALITY_SETTING);
    final Marker routeMarker = new Marker(route,
        nl.overheid.aerius.shared.util.FormatUtil.formatAlphabeticalExact(route.getId()).toUpperCase(),
        "084594");
    final BufferedImage routeMarkerImage =
        MarkerImagesFactory.createRouteMarkerImage(routeMarker, PAASharedFontsAwt.getFontMarkers(), 1).getMarkersImage();
    g.drawImage(routeMarkerImage, (int) positionInMap.getX(), (int) positionInMap.getY(), null);
  }

  /**
   * Get single image containing TMS under all the given layers and also add scalebar on top of the layers.
   * @param bboxAllResults The bbox to use in the image.
   * @param width The width of the image.
   * @param height The height of the image.
   * @param layers The image layers to add.
   * @return {@link BufferedImage}
   * @throws AeriusException On errors.
   */
  public static BufferedImage getFeaturedImage(final PAAContext paaContext, final BBox bboxAllResults, final int width, final int height,
      final PDFImageLayer... layers) throws AeriusException {
    final PDFLayerStack layerStack = getLayers(paaContext, layers);
    layerStack.setDetermineOrientation(false);

    return layerStack.getFeaturedImage(bboxAllResults, width, height, PAASharedConstants.QUALITY_SETTING);
  }

  /**
   * Get a image that can cover a full page, including a TMS layer under all supplied images and a scalebar on top.
   * Image is automagically rotated to the right orientation for an A4.
   * @throws AeriusException
   */
  public static BufferedImage generateMapImage(final PAAContext paaContext, final BBox bbox, final int imageWidth, final int imageHeight,
      final PDFImageLayer... layers) throws AeriusException {
    final PDFLayerStack layerStack = getLayers(paaContext, layers);

    return layerStack.getFeaturedImage(bbox, imageWidth, imageHeight, 1);
  }

  private static PDFLayerStack getLayers(final PAAContext paaContext, final PDFImageLayer... layers) {
    final PDFLayerStack layerStack = new PDFLayerStack(paaContext.getTiledImageCache());

    //add all other layers
    for (final PDFImageLayer layer : layers) {
      layerStack.addLayer(layer);
    }

    return layerStack;
  }

}
