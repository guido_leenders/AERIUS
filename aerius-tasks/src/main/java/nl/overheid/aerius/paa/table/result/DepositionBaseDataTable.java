/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.lowagie.text.Chunk;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.format.PDFFormatUtil;
import nl.overheid.aerius.pdf.table.AbstractPDFColumn;
import nl.overheid.aerius.pdf.table.DepositionComparisonPDFColumn;
import nl.overheid.aerius.pdf.table.DepositionPDFColumn;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.pdf.table.Padding;
import nl.overheid.aerius.pdf.table.TablePDFColumn;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Base data table as used in the PAA export containing deposition data.
 */
abstract class DepositionBaseDataTable<T extends DepositionBaseDataTableRowData> extends PAABaseTable<T> {

  private static final int DELTA_CALCULATION_SUBTABLE_COLSPAN = 4;

  enum Column {
    NAME(20) {

      @Override
      public PDFColumnProperties getProperties(final Collection<Column> columns) {
        //The name column 'soaks' up all the remaining space. With a minimum...
        float actualRelativeWidth = 100;
        for (final Column column : columns) {
          if (column != this) {
            actualRelativeWidth -= column.getRelativeWidth();
          }
        }
        return new PDFColumnPropertiesImpl(null, Math.max(getRelativeWidth(), actualRelativeWidth));
      }

    },
    CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND(12),
    PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND(12),
    MAX_DELTA_DEMAND(12),
    MAX_DEMAND_ONLY_EXCEEDING(12),
    MAX_PROPOSED_DEMAND(18),
    RULES(13);

    private final float relativeWidth;

    private Column(final float relativeWidth) {
      this.relativeWidth = relativeWidth;
    }

    public PDFColumnProperties getProperties(final Collection<Column> columns) {
      return new PDFColumnPropertiesImpl(null, relativeWidth);
    }

    float getRelativeWidth() {
      return relativeWidth;
    }

  }

  private static final String FILENAME_CHECK_EXCEEDING_SPACE = "check_exceeding_space.wmf";
  private static final String FILENAME_CHECK_NOT_EXCEEDING_SPACE = "check_not_exceeding_space.wmf";
  private static final String FILENAME_CHECK_INAPPLICABLE = "check_inapplicable.wmf";

  private static final int SCALE_ICONS_TO_PERCENT = 15;

  protected final PAAContext paaContext;

  protected final Map<Column, AbstractPDFColumn<T>> columns = new LinkedHashMap<>();

  /**
   * Constructor to fill headers and such.
   * @param locale The locale to use.
   * @param paaContext The PAA context.
   * @param pronouncementThreshold The pronouncement threshold value.
   */
  public DepositionBaseDataTable(final Locale locale, final List<Column> columns, final PAAContext paaContext) {
    super(locale);

    this.paaContext = paaContext;
    for (final Column column : columns) {
      this.columns.put(column, null);
    }

    addColumns(locale);
  }

  private void addColumns(final Locale locale) {
    for (final Column column : columns.keySet()) {
      switch (column) {
      case NAME:
        addNameColumn(locale);
        break;
      case CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND:
        addCurrentDemandForMaxDeltaDemandColumn(locale);
        break;
      case PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND:
        addProposedDemandForMaxDeltaDemandColumn(locale);
        break;
      case MAX_DELTA_DEMAND:
        addMaxDeltaDemandColumn(locale);
        break;
      case MAX_DEMAND_ONLY_EXCEEDING:
        addMaxDemandOnlyExceedingColumn(locale);
        break;
      case MAX_PROPOSED_DEMAND:
        addMaxProposedDemandColumn(locale);
        break;
      case RULES:
        addRulesColumn();
        break;
      default:
        break;
      }
    }
  }

  private void addNameColumn(final Locale locale) {
    addColumn(new TextPDFColumn<T>(locale, Column.NAME.getProperties(columns.keySet())) {

      @Override
      protected String getText(final T data) {
        return data.getName();
      }

    }, Column.NAME);
  }

  private void addCurrentDemandForMaxDeltaDemandColumn(final Locale locale) {
    addColumn(new DepositionPDFColumn<T>(locale, Column.CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND.getProperties(columns.keySet())) {

      @Override
      protected double getDeposition(final T data) {
        return data.getDepositionInfo().getCurrentDemandMaxDelta();
      }

    }, Column.CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND);
  }

  private void addProposedDemandForMaxDeltaDemandColumn(final Locale locale) {
    addColumn(new DepositionPDFColumn<T>(locale, Column.PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND.getProperties(columns.keySet())) {

      @Override
      protected double getDeposition(final T data) {
        return data.getDepositionInfo().getProposedDemandMaxDelta();
      }

    }, Column.PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND);
  }

  private void addMaxDeltaDemandColumn(final Locale locale) {
    addColumn(new DepositionComparisonPDFColumn<T>(locale, Column.MAX_DELTA_DEMAND.getProperties(columns.keySet())) {

      @Override
      protected double getDepositionDelta(final T data) {
        return data.getDepositionInfo().getMaxDeltaDemand();
      }
    }, Column.MAX_DELTA_DEMAND);
  }

  private void addMaxDemandOnlyExceedingColumn(final Locale locale) {
    addColumn(new DepositionExceedingPDFColumn<T>(locale, Column.MAX_DEMAND_ONLY_EXCEEDING.getProperties(columns.keySet()), false) {

      @Override
      protected double getDeposition(final T data) {
        return data.getDepositionInfo().getMaxDeltaDemand();
      }

      @Override
      protected double getExceedingDeposition(final T data) {
        return data.getDepositionInfo().getMaxDeltaDemandOnlyExceeding();
      }
    }, Column.MAX_DEMAND_ONLY_EXCEEDING);
  }

  private void addMaxProposedDemandColumn(final Locale locale) {
    addColumn(new DepositionPDFColumn<T>(locale, Column.MAX_PROPOSED_DEMAND.getProperties(columns.keySet())) {

      @Override
      protected double getDeposition(final T data) {
        // the max delta demand also works for 1 situation
        return data.getDepositionInfo().getMaxDeltaDemand();
      }
    }, Column.MAX_PROPOSED_DEMAND);
  }

  private void addRulesColumn() {
    addColumn(new TablePDFColumn<T>(Column.RULES.getProperties(columns.keySet())) {

      @Override
      protected PDFSimpleTable getTable(final T data) {
        final PDFSimpleTable table = new PDFSimpleTable(2);
        final Set<DevelopmentRule> displayRules = getDisplayRules(data);
        for (final DevelopmentRule rule : displayRules) {
          final String imageFilename;
          switch (rule) {
          case EXCEEDING_SPACE_CHECK:
            imageFilename = FILENAME_CHECK_EXCEEDING_SPACE;
            break;
          case NOT_EXCEEDING_SPACE_CHECK:
            imageFilename = FILENAME_CHECK_NOT_EXCEEDING_SPACE;
            break;
          /* The other entries are not to be used in the PDF at this point. */
          default:
            throw new IllegalArgumentException("Unhandled enum type DevelopmentRule " + rule + " in DepositionSummaryDataTable");
          }

          addImageToTable(table, imageFilename);
        }

        if (displayRules.isEmpty()) {
          addImageToTable(table, FILENAME_CHECK_INAPPLICABLE);
        }

        if (displayRules.size() % 2 != 0) {
          table.addEmptyCell();
        } else if (displayRules.size() == 0) {
          table.addEmptyCell();
          table.addEmptyCell();
        }
        return table;
      }

    }, Column.RULES);
  }

  private void addColumn(final AbstractPDFColumn<T> tableColumn, final Column column) {
    addColumn(tableColumn);
    columns.put(column, tableColumn);
  }

  private Set<DevelopmentRule> getDisplayRules(final T data) {
    final Set<DevelopmentRule> rules = new HashSet<>();
    for (final DevelopmentRule rule : data.getRules()) {
      switch (rule) {
      case EXCEEDING_SPACE_CHECK:
      case NOT_EXCEEDING_SPACE_CHECK:
        rules.add(rule);
        break;
      default:
        break;
      }
    }
    return rules;
  }

  private void addImageToTable(final PDFSimpleTable table, final String imageFilename) {
    try {
      final Image image = PDFUtils.getImage(PAASharedConstants.RESOURCES_DIR + imageFilename, getLocale());
      image.scalePercent(SCALE_ICONS_TO_PERCENT);
      table.add(new Phrase(new Chunk(image, 0, 0, true)));
    } catch (DocumentException | IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  protected void addHeaders(final PDFSimpleTable table) {
    // Will make sure the header is repeated on every page if the table is continued on multiple pages.
    table.setHeaderRows(1);

    for (final Entry<Column, AbstractPDFColumn<T>> column : columns.entrySet()) {
      final PdfPCell headerCell = getHeader(column);
      if (headerCell != null) {
        table.add(headerCell);
      }
    }

    applyHeaderCellStyleManually(table);
  }

  protected PdfPCell getHeader(final Entry<Column, AbstractPDFColumn<T>> entry) {
    PdfPCell cell = null;
    switch (entry.getKey()) {
    case NAME:
      cell = createAndGetHeaderCell(entry.getValue(), getFirstHeaderTitle());
      break;
    //assume we get all 4 (max_delta_demand, max_delta_demand_only_exceeding and the 2 for those two) at this point. guess we could check, but meh..
    case CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND:
      cell = getComparisonHeader().getAsCell();
      cell.setColspan(DELTA_CALCULATION_SUBTABLE_COLSPAN);
      break;
    case MAX_PROPOSED_DEMAND:
      cell = createAndGetHeaderCell(entry.getValue(), getText(paaContext.isComparison() ? "deposition_datatable_max_proposed_comparison"
          : "deposition_datatable_max_proposed"));
      break;
    case MAX_DEMAND_ONLY_EXCEEDING:
      if (!paaContext.isComparison()) {
        cell = createAndGetHeaderCell(entry.getValue(), getText("deposition_datatable_only_exceeding"));
      }
      break;
    case RULES:
      cell = createAndGetHeaderCell(entry.getValue(), getText("deposition_datatable_rules"));
      break;
    default:
      break;
    }

    return cell;
  }

  private PDFSimpleTable getComparisonHeader() {
    final PDFSimpleTable subTable = new PDFSimpleTable(getComparisonHeaderRelativeWidths());
    final PdfPCell subTableTitle = PDFSimpleTable.getNewCellWithDefaultSettings();
    subTableTitle.setPhrase(
        new Phrase(getText("deposition_datatable_highest_delta_projectcontribution"), getHeaderFont()));
    subTableTitle.setColspan(DELTA_CALCULATION_SUBTABLE_COLSPAN);
    subTable.add(subTableTitle, new Padding(0, Padding.DEFAULT_PADDING, 0, 0));
    subTable.add(createAndGetHeaderCell(columns.get(Column.CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND),
        getText("deposition_datatable_situation1")), Padding.NO_PADDING);
    subTable.add(createAndGetHeaderCell(columns.get(Column.PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND),
        getText("deposition_datatable_situation2")), Padding.NO_PADDING);
    subTable.add(createAndGetHeaderCell(columns.get(Column.MAX_DELTA_DEMAND),
        getText("deposition_datatable_difference")), Padding.NO_PADDING);
    subTable.add(createAndGetHeaderCell(columns.get(Column.MAX_DEMAND_ONLY_EXCEEDING),
        getText("deposition_datatable_comparison_only_exceeding")), Padding.NO_PADDING);
    return subTable;
  }

  private PDFRelativeWidths getComparisonHeaderRelativeWidths() {
    return new PDFRelativeWidths(
        Column.CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND.getRelativeWidth(),
        Column.PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND.getRelativeWidth(),
        Column.MAX_DELTA_DEMAND.getRelativeWidth(),
        Column.MAX_DEMAND_ONLY_EXCEEDING.getRelativeWidth());
  }

  abstract String getFirstHeaderTitle();

  private abstract class DepositionExceedingPDFColumn<Z> extends TextPDFColumn<Z> {

    private final boolean delta;

    public DepositionExceedingPDFColumn(final Locale locale, final PDFColumnProperties columnProperties, final boolean delta) {
      super(locale, columnProperties);
      this.delta = delta;
    }

    @Override
    protected String getText(final Z data) {
      return PDFFormatUtil.formatAndFilterExceedingDeposition(locale, getDeposition(data), getExceedingDeposition(data), delta);
    }

    protected abstract double getDeposition(Z data);

    protected abstract double getExceedingDeposition(Z data);
  }
}
