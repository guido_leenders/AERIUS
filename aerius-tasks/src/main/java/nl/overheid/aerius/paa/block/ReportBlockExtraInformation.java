/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.sql.SQLException;
import java.text.MessageFormat;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;

/**
 * PAA Report Block: Extra Information.
 */
public class ReportBlockExtraInformation extends PAAReportBlock<PAAContext> {

  private static final PDFRelativeWidths CONTENT_RELATIVE_WIDTH = new PDFRelativeWidths(20, 150);

  public ReportBlockExtraInformation(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws DocumentException, SQLException {
    final PDFSimpleTable extraInformationTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCell(extraInformationTable, paaContext.getText("extrainformation_title"));

    final PDFSimpleTable valueTable = new PDFSimpleTable(CONTENT_RELATIVE_WIDTH);

    addFullWidthCell(valueTable, paaContext.getText("extrainformation_leading"));

    // Add app version.
    valueTable.add(paaContext.getText("app_name"), PAASharedFonts.DEFAULT_TEXT);
    final String appValue = MessageFormat.format(paaContext.getText("extrainformation_app_value"), AeriusVersion.getVersionNumber());
    valueTable.add(appValue, PAASharedFonts.DEFAULT_TEXT);

    // Add database version.
    valueTable.add(paaContext.getText("extrainformation_database"), PAASharedFonts.DEFAULT_TEXT);
    final String dbValue = MessageFormat.format(paaContext.getText("extrainformation_database_value"), getPmf().getDatabaseVersion());
    valueTable.add(dbValue, PAASharedFonts.DEFAULT_TEXT_UNDERLINE);

    // Add link to landing page.
    addFullWidthCell(valueTable, paaContext.getText("extrainformation_trailing"));
    addFullWidthCell(valueTable, MessageRepository.getString(getPmf(), MessagesEnum.LANDING_PAGE_LINK, paaContext.getLocale()));

    extraInformationTable.add(valueTable);
    pdfDocument.addElement(extraInformationTable);
  }

  private void addFullWidthCell(final PDFSimpleTable table, final String text) {
    final PdfPCell leadingCell = new PdfPCell(new Phrase(text, PAASharedFonts.DEFAULT_TEXT.get()));
    leadingCell.setColspan(2);
    leadingCell.setBorder(0);
    table.add(leadingCell);
  }

}
