/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.paa.table.source.AbstractRouteTable.RouteTableRowData;
import nl.overheid.aerius.shared.domain.sector.category.ShippingCategory;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.util.FormatUtil;

/**
 * Data table as used in the PAA export containing emission data for a route.
 */
abstract class AbstractRouteTable<T extends RouteTableRowData<?>> extends PAABaseTable<T> {

  /**
   * @param headerTexts @see {@link PAABaseTable}
   * @param relativeWidths @see {@link PAABaseTable}
   * @param locale @see {@link PAABaseTable}
   */
  AbstractRouteTable(final Locale locale) {
    super(locale);
  }


  protected List<T> toOrderedTableRows(final Map<ShippingRoute, List<T>> tempRowDataMap) {
    final List<T> rowDatas = new ArrayList<>();
    //ensure the rows are alphabetically ordered by routelabel first.
    final ArrayList<ShippingRoute> shippingRoutes = new ArrayList<>(tempRowDataMap.keySet());
    Collections.sort(shippingRoutes, new Comparator<ShippingRoute>() {

      @Override
      public int compare(final ShippingRoute route1, final ShippingRoute route2) {
        return route1.getId() - route2.getId();
      }
    });
    int currentId = -1;
    for (final ShippingRoute route : shippingRoutes) {
      //ensure the rows are sorted within a routegroup.
      Collections.sort(tempRowDataMap.get(route));
      for (final T rowData : tempRowDataMap.get(route)) {
        //In case more rows have the same route label, ensure only the first actually shows it.
        String routeLabel = "";
        if (currentId != route.getId()) {
          routeLabel =  FormatUtil.formatAlphabeticalExact(route.getId()).toUpperCase();
          currentId = route.getId();
        }
        rowData.setLabel(routeLabel);
        rowDatas.add(rowData);
      }
    }
    return rowDatas;
  }

  /**
   * @param <S> The shipping category for this rowdata.
   */
  static class RouteTableRowData<S extends ShippingCategory> implements Comparable<RouteTableRowData<S>> {

    private final S category;
    private String label;

    RouteTableRowData(final S category) {
      this.category = category;
    }

    public S getCategory() {
      return category;
    }

    public String getLabel() {
      return label;
    }

    public void setLabel(final String label) {
      this.label = label;
    }

    @Override
    public int compareTo(final RouteTableRowData<S> other) {
      return category == null || other.category == null ? 0 : category.getName().compareTo(other.category.getName());
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((category == null) ? 0 : category.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      boolean equal = false;
      if (obj != null && this.getClass() == obj.getClass()) {
        final RouteTableRowData<?> other = (RouteTableRowData<?>) obj;
        equal = (category != null && category.equals(other.getCategory())) || (category == null && other.category == null);
      }
      return equal;
    }

  }

}
