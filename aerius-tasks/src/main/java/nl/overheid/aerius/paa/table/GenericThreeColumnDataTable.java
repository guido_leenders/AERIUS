/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import java.util.Locale;

import nl.overheid.aerius.paa.table.GenericThreeColumnDataTable.Column;

/**
 * Datatable containing generic three column data.
 */
public abstract class GenericThreeColumnDataTable extends GenericColumnDataTable<Column> {

  protected enum Column {
    COLUMN_1,
    COLUMN_2,
    COLUMN_3
  }

  /**
   * Default constructor setting header and such.
   * @param locale The locale to use.
   * @param headerRow Headers to set. If a value for a column is null, the column will be 'added' to the one before it via colspan.
   */
  public GenericThreeColumnDataTable(final Locale locale, final GenericThreeColumnDataRow headerRow) {
    super(locale, headerRow);
  }

  public static class GenericThreeColumnDataRow extends GenericColumnDataRow<Column> {

    public GenericThreeColumnDataRow() {
      super(Column.class);
    }

    public GenericThreeColumnDataRow(final String first, final String second, final String third) {
      this();
      set(Column.COLUMN_1, first);
      set(Column.COLUMN_2, second);
      set(Column.COLUMN_3, third);
    }

  }

}
