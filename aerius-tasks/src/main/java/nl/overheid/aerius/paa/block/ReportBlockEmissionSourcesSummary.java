/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.paa.common.PAAAnchors;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.table.result.EmissionSourcesSummaryDataTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;

/**
 * PAA Report Block: Emission Sources summary.
 */
public class ReportBlockEmissionSourcesSummary extends PAAReportBlock<PAAContext> {

  private final EmissionSourceList sources;

  public ReportBlockEmissionSourcesSummary(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext, final EmissionSourceList sources) {
    super(pdfDocument, paaContext);
    this.sources = sources;
  }

  @Override
  public void generate() throws DocumentException {
    final PDFSimpleTable summaryTable = PAAUtil.getDefaultBlockEmptyTable(false);
    PAAUtil.addBlockHeaderCellWithSubHeader(
        summaryTable, paaContext.getText("emissionsources_summary_title"), sources.getName(), PAAAnchors.getAnchorReferenceToEmissionRecapBlock());
    summaryTable.allowSplittingRows();

    summaryTable.add(new EmissionSourcesSummaryDataTable(paaContext, sources, set1.getYear()));
    pdfDocument.addElement(summaryTable);
  }

}
