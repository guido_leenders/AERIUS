/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.paa.block.ReportBlockOverview;
import nl.overheid.aerius.paa.table.GenericThreeColumnDataTable;
import nl.overheid.aerius.paa.table.OverviewContactDataTable;
import nl.overheid.aerius.paa.table.OverviewPermitCalculationRadiusTypeDataTable;
import nl.overheid.aerius.paa.table.OverviewTemporaryProjectDataTable;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;

/**
 * PAA Report Block: Overview for decrees.
 */
public class DecreeReportBlockOverview extends ReportBlockOverview<DecreePAAContext> {

  protected DecreeReportBlockOverview(final PDFDocumentWrapper pdfDocument, final DecreePAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  protected PAABaseTable<?> getContactTable(final DecreePAAContext paaContext) {
    return new OverviewContactDataTable(paaContext);
  }

  @Override
  protected List<PAABaseTable<?>> getActivityTables() {
    final List<PAABaseTable<?>> tables = new ArrayList<>();
    tables.add(new ActivityFirstRowDataTable());
    tables.add(new ActivitySecondRowDataTable());
    tables.add(new ActivityThirdRowDataTable());
    if (paaContext.getCalculatedScenario().getOptions().isTemporaryProjectImpact()) {
      tables.add(new OverviewTemporaryProjectDataTable(paaContext, set1.getYear()));
    }
    if (paaContext.getCalculatedScenario().getOptions().isPermitCalculationRadiusImpact()) {
      tables.add(new OverviewPermitCalculationRadiusTypeDataTable(paaContext));
    }
    return tables;
  }

  class ActivityFirstRowDataTable extends GenericThreeColumnDataTable {

    ActivityFirstRowDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_description", "base_activity_reference", "base_contact_authority"));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, normalize(paaContext.getMetaData().getProjectName()));
      row.set(Column.COLUMN_2, paaContext.getMetaData().getReference());
      row.set(Column.COLUMN_3,
          normalize(paaContext.getDecreeInfo().getAuthority() == null ? null : paaContext.getDecreeInfo().getAuthority().getDescription()));
      addRow(row);
    }

  }

  class ActivitySecondRowDataTable extends GenericThreeColumnDataTable {

    ActivitySecondRowDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow("base_activity_date_calculation", "base_activity_calculation_year", null));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, paaContext.getFormattedCreationData(DATE_CALCULATION_FORMAT));
      row.set(Column.COLUMN_2, set1.getYear());
      addRow(row);
    }

  }

  private static boolean hideSector(final DecreePAAContext context) {
    final String sectorGroupText = context.getText("sectorGroup", context.getDecreeInfo().getSector().getSectorGroup());

    return sectorGroupText.equals(context.getDecreeInfo().getSector().getDescription());
  }

  class ActivityThirdRowDataTable extends GenericThreeColumnDataTable {

    ActivityThirdRowDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_sectorgroup",
          hideSector(paaContext) ? null : "base_activity_sector",
          paaContext.getDecreeInfo().getParentProjectName() == null ? null : "base_activity_parent_project"));

      final String sectorGroupText = getText("sectorGroup", paaContext.getDecreeInfo().getSector().getSectorGroup(), paaContext.getLocale());

      final boolean hideSector = hideSector(paaContext);

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, sectorGroupText);
      row.set(Column.COLUMN_2, hideSector ? null : paaContext.getDecreeInfo().getSector().getDescription());
      row.set(Column.COLUMN_3, paaContext.getDecreeInfo().getParentProjectName());
      addRow(row);
    }

  }

}
