/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.melding;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.paa.block.ReportBlockOverview;
import nl.overheid.aerius.paa.table.GenericThreeColumnDataTable;
import nl.overheid.aerius.paa.table.OverviewPermitCalculationRadiusTypeDataTable;
import nl.overheid.aerius.paa.table.OverviewTemporaryProjectDataTable;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;

/**
 * PAA Report Block: Overview for Melding.
 */
public class MeldingReportBlockOverview extends ReportBlockOverview<MeldingPAAContext> {

  protected MeldingReportBlockOverview(final PDFDocumentWrapper pdfDocument, final MeldingPAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  protected PAABaseTable<?> getContactTable(final MeldingPAAContext paaContext) {
    return new MeldingOverviewContactDataTable(paaContext);
  }

  @Override
  protected List<PAABaseTable<?>> getActivityTables() {
    final List<PAABaseTable<?>> tables = new ArrayList<>();
    tables.add(new ActivityInformationDataTable());
    tables.add(new PreviousReferencesDataTable());
    tables.add(new CalculationInformationDataTable());
    if (paaContext.getCalculatedScenario().getOptions().isTemporaryProjectImpact()) {
      tables.add(new OverviewTemporaryProjectDataTable(paaContext, set1.getYear()));
    }
    if (paaContext.getCalculatedScenario().getOptions().isPermitCalculationRadiusImpact()) {
      tables.add(new OverviewPermitCalculationRadiusTypeDataTable(paaContext));
    }
    return tables;
  }

  class ActivityInformationDataTable extends GenericThreeColumnDataTable {

    ActivityInformationDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_description", "base_activity_reference", "base_activity_reference_situtation"));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, normalize(paaContext.getMetaData().getProjectName()));
      row.set(Column.COLUMN_2, normalize(paaContext.getMetaData().getReference()));
      row.set(Column.COLUMN_3, normalize(set1.getSources().getName()));
      addRow(row);
    }

  }

  class PreviousReferencesDataTable extends GenericThreeColumnDataTable {

    PreviousReferencesDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_nbwet_reference", "base_activity_previous_melding", null));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, paaContext.getMeldingInfo().isExistingPermitPresent()
          ? paaContext.getMeldingInfo().getExistingPermitReference()
          : getNonePresent());
      row.set(Column.COLUMN_2, paaContext.getMeldingInfo().isPreviousMeldingPresent()
          ? paaContext.getMeldingInfo().getPreviousMeldingReference()
          : getNonePresent());
      addRow(row);
    }

    private String getNonePresent() {
      return paaContext.getText("none");
    }

  }

  class CalculationInformationDataTable extends GenericThreeColumnDataTable {

    CalculationInformationDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_date_calculation", "base_activity_calculation_year", null));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, paaContext.getFormattedCreationData(DATE_CALCULATION_FORMAT));
      row.set(Column.COLUMN_2, set1.getYear());
      addRow(row);
    }
  }

}
