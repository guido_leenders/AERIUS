/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.Locale;

import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.shared.domain.source.EmissionSourceCollection;
import nl.overheid.aerius.shared.domain.source.EmissionSubSource;

/**
 * Data table as used in the PAA export containing emission data for a source with subsources.
 */
public abstract class SubSourceDataTable<V extends EmissionSubSource> extends PAABaseTable<V> {

  enum SubSourceColumn {
    SUBSTANCE("substance"),
    EMISSION("emission");

    private final String headerKey;

    private SubSourceColumn(final String headerKey) {
      this.headerKey = getHeaderKey(headerKey);
    }

    public String getHeaderI18nKey() {
      return headerKey;
    }

    public static String getHeaderKey(final String headerKey) {
      return "emission_datatable_" + headerKey;
    }

  }

  /**
   * @param year The year to use.
   * @param emissionSource The emissionSource to make table for.
   * @param locale The locale to use.
   */
  public SubSourceDataTable(final int year, final EmissionSourceCollection<V> emissionSource, final Locale locale) {
    super(locale);

    setData(emissionSource.getEmissionSubSources());

    addColumns(locale, emissionSource, year);
  }

  private void addColumns(final Locale locale, final EmissionSourceCollection<V> emissionSource, final int year) {
    addDescriptionColumns(locale);

    addSpecificColumns(locale);

    addSubstanceColumn(locale, getSubstanceColumnProperties(), emissionSource, year);
    addEmissionColumn(locale, getEmissionColumnProperties(), emissionSource, year);
  }

  protected abstract void addDescriptionColumns(final Locale locale);

  protected abstract void addSpecificColumns(final Locale locale);

  protected abstract PDFColumnProperties getSubstanceColumnProperties();

  protected abstract PDFColumnProperties getEmissionColumnProperties();


  private void addSubstanceColumn(final Locale locale, final PDFColumnProperties column, final EmissionSourceCollection<V> emissionSource,
      final int year) {
    addColumn(new SubstancePDFColumn<V>(locale, column, emissionSource, year));
  }

  private void addEmissionColumn(final Locale locale, final PDFColumnProperties column, final EmissionSourceCollection<V> emissionSource,
      final int year) {
    addColumnRightAligned(new EmissionPDFColumn<V>(locale, column, emissionSource, year));
  }

}
