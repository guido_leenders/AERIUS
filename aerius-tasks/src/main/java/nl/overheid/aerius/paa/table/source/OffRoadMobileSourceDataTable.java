/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.Locale;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;

/**
 * Data table as used in the PAA export containing emission data for a mobile source.
 */
public class OffRoadMobileSourceDataTable extends SubSourceDataTable<OffRoadVehicleEmissionSubSource> {

  enum OffRoadColumn {
    MOBILE_CODE(SubSourceColumn.getHeaderKey("mobile_code"), 8),
    DESCRIPTION(SubSourceColumn.getHeaderKey("description"), 10),
    FUEL_USE(SubSourceColumn.getHeaderKey("fuel_use"), 4),
    CUSTOM_HEIGHT(SubSourceColumn.getHeaderKey("height"), 4),
    CUSTOM_SPREAD(SubSourceColumn.getHeaderKey("spread"), 4),
    CUSTOM_HEAT_CONTENT(SubSourceColumn.getHeaderKey("heatContent"), 4),
    SUBSTANCE(SubSourceColumn.SUBSTANCE.getHeaderI18nKey(), 3),
    EMISSION(SubSourceColumn.EMISSION.getHeaderI18nKey(), 5);

    private final PDFColumnProperties columnProperties;

    private OffRoadColumn(final String headerKey, final float relativeWidth) {
      this.columnProperties = new PDFColumnPropertiesImpl(headerKey, relativeWidth);
    }

    public PDFColumnProperties getColumnProperties() {
      return columnProperties;
    }
  }

  /**
   * Constructor to set header + widths.
   * @param year The year to use.
   * @param emissionSource The MobileEmissionValues to make table for.
   * @param locale The locale to use.
   */
  public OffRoadMobileSourceDataTable(final int year, final OffRoadMobileEmissionSource emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addDescriptionColumns(final Locale locale) {
    addMobileCodeColumn(locale);
    addDescriptionColumn(locale);
  }

  @Override
  protected void addSpecificColumns(final Locale locale) {
    addFuelUseColumn(locale);
    addCustomColumns(locale);
  }

  private void addMobileCodeColumn(final Locale locale) {
    addColumn(new TextPDFColumn<OffRoadVehicleEmissionSubSource>(locale, OffRoadColumn.MOBILE_CODE.getColumnProperties()) {

      @Override
      protected String getText(final OffRoadVehicleEmissionSubSource data) {
        return data.isCustomCategory() ? getText("emission_datatable_custom_code") : data.getStageCategory().getName();
      }
    });
  }

  private void addDescriptionColumn(final Locale locale) {
    addColumn(new TextPDFColumn<OffRoadVehicleEmissionSubSource>(locale, OffRoadColumn.DESCRIPTION.getColumnProperties()) {

      @Override
      protected String getText(final OffRoadVehicleEmissionSubSource data) {
        return data.getName();
      }
    });
  }

  private void addFuelUseColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<OffRoadVehicleEmissionSubSource>(locale, OffRoadColumn.FUEL_USE.getColumnProperties()) {

      @Override
      protected String getText(final OffRoadVehicleEmissionSubSource data) {
        return data.isCustomCategory() ? "" : formatInt(data.getFuelage());
      }
    });
  }

  private void addCustomColumns(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<OffRoadVehicleEmissionSubSource>(locale, OffRoadColumn.CUSTOM_HEIGHT.getColumnProperties()) {

      @Override
      protected String getText(final OffRoadVehicleEmissionSubSource data) {
        return data.isCustomCategory() ? formatDouble(data.getEmissionHeight()) : "";
      }
    });
    addColumnRightAligned(new TextPDFColumn<OffRoadVehicleEmissionSubSource>(locale, OffRoadColumn.CUSTOM_SPREAD.getColumnProperties()) {

      @Override
      protected String getText(final OffRoadVehicleEmissionSubSource data) {
        return data.isCustomCategory() ? formatDouble(data.getSpread()) : "";
      }
    });
    addColumnRightAligned(new TextPDFColumn<OffRoadVehicleEmissionSubSource>(locale, OffRoadColumn.CUSTOM_HEAT_CONTENT.getColumnProperties()) {

      @Override
      protected String getText(final OffRoadVehicleEmissionSubSource data) {
        return data.isCustomCategory() ? formatDouble(data.getHeatContent()) : "";
      }
    });
  }

  @Override
  protected PDFColumnProperties getSubstanceColumnProperties() {
    return OffRoadColumn.SUBSTANCE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getEmissionColumnProperties() {
    return OffRoadColumn.EMISSION.getColumnProperties();
  }

}
