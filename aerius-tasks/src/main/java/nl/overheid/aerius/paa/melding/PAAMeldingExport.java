/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.melding;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.paa.base.PAAExport;
import nl.overheid.aerius.paa.block.PAAReportBlock;
import nl.overheid.aerius.paa.common.PAAContextUtil;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.export.ExportData;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.melding.MeldingInformation;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * The document generated when a user makes a Melding (Pronouncement).
 */
public class PAAMeldingExport extends PAAExport<MeldingPAAContext> {

  /**
   * @param pmf The PMF to use.
   * @param httpClient The http client to use when using stuff like Geoserver images.
   * @param data The data that should be used when constructing this PAA.
   * @param gmlStrings The GML's to be used for metadata (import reasons).
   * @param meldingInfo Melding specific input data.
   * @throws SQLException In case of database errors.
   * @throws AeriusException
   */
  public PAAMeldingExport(final PMF pmf, final CloseableHttpClient httpClient, final CalculatedScenario scenario, final ExportData data,
      final List<StringDataSource> gmlStrings, final MeldingInformation meldingInfo) throws SQLException, AeriusException {
    super(createContext(pmf, httpClient, scenario, data, meldingInfo), gmlStrings);
  }

  private static MeldingPAAContext createContext(final PMF pmf, final CloseableHttpClient httpClient, final CalculatedScenario scenario,
      final ExportData data, final MeldingInformation meldingInfo) throws SQLException, AeriusException {
    final Locale locale = PAAContextUtil.getLocale(data);
    final SectorCategories categories = PAAContextUtil.getSectorCategories(pmf, locale);
    final List<PDFExportAssessmentAreaInfo> exportInfo = PAAContextUtil.getExportInfo(pmf, scenario, SegmentType.PERMIT_THRESHOLD, true);
    final int srid = ReceptorGridSettingsRepository.getSrid(pmf);
    return new MeldingPAAContext(pmf, httpClient, locale, PAAContextUtil.getEpsg(srid), PAAReportType.MELDING, categories,
        scenario, exportInfo, meldingInfo);
  }

  @Override
  protected List<PAAReportBlock<?>> getReportBlocks() {
    final List<PAAReportBlock<?>> reportBlocks = new ArrayList<>();

    reportBlocks.add(new MeldingReportBlockOverview(pdfDocument, getContext()));

    addSourceRecapBlocks(reportBlocks);
    addDepositionBlocks(reportBlocks, true, true, false, false);
    addSourceBlocks(reportBlocks);
    addDisclaimerBlocks(reportBlocks);

    return reportBlocks;
  }

}
