/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.base;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfShading;
import com.lowagie.text.pdf.PdfShadingPattern;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.ShadingColor;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.paa.block.PAAReportBlock;
import nl.overheid.aerius.paa.block.ReportBlockDepositionAreasTables;
import nl.overheid.aerius.paa.block.ReportBlockDepositionHabitatsTables;
import nl.overheid.aerius.paa.block.ReportBlockDisclaimer;
import nl.overheid.aerius.paa.block.ReportBlockEmission;
import nl.overheid.aerius.paa.block.ReportBlockEmissionSourcesSummary;
import nl.overheid.aerius.paa.block.ReportBlockExtraInformation;
import nl.overheid.aerius.paa.block.ReportBlockLocation;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAFlowState;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.WatermarkType;
import nl.overheid.aerius.pdf.PDFDocumentBase;
import nl.overheid.aerius.pdf.PDFMetaData;
import nl.overheid.aerius.pdf.PDFWriterUtil;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.HashUtil;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Base class for PAA's.
 */
public abstract class PAAExport<T extends PAAContext> extends PDFDocumentBase<T> {

  private static final Logger LOG = LoggerFactory.getLogger(PAAExport.class);

  private static final int FOOTER_PADDING_FROM_RIGHT = 40;
  private static final int FOOTER_PADDING_FROM_BOTTOM = 30;
  private static final float FOOTER_POSX = PAASharedConstants.PAGESIZE.getWidth() - FOOTER_PADDING_FROM_RIGHT;
  private static final float FOOTER_POSY = FOOTER_PADDING_FROM_BOTTOM; // in PDF's the bottom of a page would be Y-position 0
  private static final float FOOTER_SCALE = 1.5f;

  private static final int PAGE_MARGIN_TOP = 110;
  private static final int PAGE_MARGIN_LEFT = 20;

  private final List<StringDataSource> gmlStrings;

  private final PAAFlowState flowState = new PAAFlowState();

  /**
   * @param paaContext Context data.
   * @param gmlStrings The GML's to be used for metadata (import reasons).
   */
  public PAAExport(final T paaContext, final List<StringDataSource> gmlStrings) {
    super(paaContext);
    if (gmlStrings == null || gmlStrings.isEmpty() || StringUtils.isEmpty(gmlStrings.get(0).getData())) {
      throw new IllegalArgumentException("gmlStrings may not be empty...");
    }
    if (gmlStrings.size() > 2) {
      throw new IllegalArgumentException("gmlStrings should contain either 1 or 2 elements...");
    }

    this.gmlStrings = gmlStrings;
  }

  protected PAAPageEventHelper createHeaderFooterHelper() {
    return new PAAPageEventHelper(flowState, getContext());
  }

  @Override
  public byte[] generatePDF() throws IOException, SQLException, AeriusException {
    setPageSize(PAASharedConstants.PAGESIZE);
    setPageMarginTop(PAGE_MARGIN_TOP);
    setPageMarginLeft(PAGE_MARGIN_LEFT);

    ensureHeaderFooterHelper();

    return super.generatePDF();
  }

  private void ensureHeaderFooterHelper() {
    final PAAPageEventHelper headerFooterHelper = createHeaderFooterHelper();
    headerFooterHelper.setWaterMark(getContext().getWatermark());
    setHeaderFooterHelper(headerFooterHelper);
  }

  /**
   * Adds a footer to the PDF. This will be called separately as iText doesn't support a good way to add
   *  total page numbers without it being very, very ugly.
   * @param pdfContent The PDF content.
   * @return the new PDF content with the footer.
   * @throws IOException
   * @throws DocumentException
   */
  @Override
  protected void postProcess(final PdfStamper stamper) throws DocumentException, IOException {
    PDFWriterUtil.addPageNumber(stamper, 1, getContext(), PAASharedFonts.FOOTER_FRONTPAGE_PAGENUMBER,
        FOOTER_POSX, FOOTER_POSY - PAASharedFonts.FOOTER_REFERENCE.getSize() * FOOTER_SCALE);
    PDFWriterUtil.addPageNumbers(stamper, 2, getContext(), PAASharedFonts.FOOTER_PAGENUMBER,
        FOOTER_POSX, FOOTER_POSY - PAASharedFonts.FOOTER_REFERENCE.getSize() * FOOTER_SCALE);
  }

  @Override
  protected void addMetaData(final PDFMetaData metaData) {
    addBaseMetaData(getContext().getLocale());

    metaData.addMetadataCustomField(PAAConstants.CALC_PDF_METADATA_GML_KEY, gmlStrings.get(0).getData());
    if (gmlStrings.size() > 1) {
      metaData.addMetadataCustomField(PAAConstants.CALC_PDF_METADATA_2ND_GML_KEY, gmlStrings.get(1).getData());
    }
    final ArrayList<String> gmlStr = new ArrayList<>();
    for (final StringDataSource sds : gmlStrings) {
      gmlStr.add(sds.getData());
    }
    final String metadataHash = HashUtil.generateSaltedHash(PAAConstants.CALC_PDF_METADATA_HASH_SALT, gmlStr);
    if (StringUtils.isEmpty(metadataHash)) {
      throw new IllegalArgumentException("Could not generate metadata hash");
    }
    metaData.addMetadataCustomField(PAAConstants.CALC_PDF_METADATA_HASH_KEY, metadataHash);
  }

  @Override
  protected void addContentPages() throws DocumentException, IOException, SQLException, AeriusException {
    final String reference = getContext().getCalculatedScenario().getMetaData().getReference();

    flowState.setAddProductTitle(true);

    for (final PAAReportBlock<?> reportBlock : getReportBlocks()) {
      LOG.trace("Generating {} [{}]", reportBlock.getClass().getSimpleName(), reference);
      reportBlock.generate();
      if (reportBlock.isNewPageAfterBlock()) {
        newPage();
      }
      flowState.setAddMiniLogoHeader(true);
    }
    flowState.setAddProductTitle(false);
    flowState.setAddMiniLogoHeader(false);
  }

  protected abstract List<PAAReportBlock<?>> getReportBlocks();

  protected void addSourceRecapBlocks(final List<PAAReportBlock<?>> reportBlocks) {
    final PAAReportBlock<?> locationBlock1 = new ReportBlockLocation(pdfDocument, getContext(), getSourcesOne());
    locationBlock1.setNewPageAfterBlock(false);
    reportBlocks.add(locationBlock1);
    reportBlocks.add(new ReportBlockEmissionSourcesSummary(pdfDocument, getContext(), getSourcesOne()));

    if (getContext().isComparison()) {
      final PAAReportBlock<?> locationBlock2 = new ReportBlockLocation(pdfDocument, getContext(), getSourcesTwo());
      locationBlock2.setNewPageAfterBlock(false);
      reportBlocks.add(locationBlock2);
      reportBlocks.add(new ReportBlockEmissionSourcesSummary(pdfDocument, getContext(), getSourcesTwo()));
    }
  }

  protected void addDepositionBlocks(final List<PAAReportBlock<?>> reportBlocks, final boolean foreignAreas, final boolean addNonPasAreas,
      final boolean addNonPasHabitats, final boolean showRulesColumn) {
    // PAS only block - shouldn't have foreign areas
    reportBlocks.add(new ReportBlockDepositionAreasTables(pdfDocument, getContext(), true, false, true, showRulesColumn));
    reportBlocks.add(new ReportBlockDepositionHabitatsTables(pdfDocument, getContext(), true, false, showRulesColumn));

    // non-PAS block - optionally with foreign areas
//    if (addNonPasAreas) {
//      reportBlocks.add(new ReportBlockDepositionAreasTables(pdfDocument, getContext(), false, foreignAreas, false, showRulesColumn));
//    }
//    if (addNonPasHabitats) {
//      reportBlocks.add(new ReportBlockDepositionHabitatsTables(pdfDocument, getContext(), false, foreignAreas, showRulesColumn));
//    }
  }

  protected void addSourceBlocks(final List<PAAReportBlock<?>> reportBlocks) {
    addSourceBlocks(reportBlocks, getSourcesOne());
    if (getSourcesTwo() != null) {
      addSourceBlocks(reportBlocks, getSourcesTwo());
    }
  }

  private void addSourceBlocks(final List<PAAReportBlock<?>> reportBlocks, final EmissionSourceList sources) {
    reportBlocks.add(new ReportBlockEmission(pdfDocument, getContext(), sources));
    newPage();
  }

  protected EmissionSourceList getSourcesOne() {
    return getContext().isComparison() ? ((CalculatedComparison) getContext().getCalculatedScenario()).getCalculationOne().getSources()
        : ((CalculatedSingle) getContext().getCalculatedScenario()).getCalculation().getSources();
  }

  protected EmissionSourceList getSourcesTwo() {
    return getContext().isComparison() ? ((CalculatedComparison) getContext().getCalculatedScenario()).getCalculationTwo().getSources() : null;
  }

  protected void addDisclaimerBlocks(final List<PAAReportBlock<?>> reportBlocks) {
    final PAAReportBlock<?> disclaimerBlock = new ReportBlockDisclaimer(pdfDocument, getContext());
    disclaimerBlock.setNewPageAfterBlock(false);
    reportBlocks.add(disclaimerBlock);
    reportBlocks.add(new ReportBlockExtraInformation(pdfDocument, getContext()));
  }

  @Override
  protected void addFrontPage() {
    final Rectangle fillRectangle = new Rectangle(
        PAASharedConstants.PAGESIZE.getWidth() / 2, 0, PAASharedConstants.PAGESIZE.getWidth(), PAASharedConstants.PAGESIZE.getHeight());

    final PdfShading gradient = PdfShading.simpleAxial(
        getPdfWriter(),
        fillRectangle.getLeft(), fillRectangle.getBottom(), fillRectangle.getRight(), fillRectangle.getTop(),
        PDFUtils.getColor(getContext().getReportType().getProductType() == ProductType.REGISTER ? "aa2165" : "139fd6"),
        PDFUtils.getColor(getContext().getReportType().getProductType() == ProductType.REGISTER ? "412461" : "1a3c6e"));

    PDFWriterUtil.addColorRectangle(
        getPdfWriter(), false, new ShadingColor(new PdfShadingPattern(gradient)),
        fillRectangle.getLeft(), fillRectangle.getBottom(), fillRectangle.getWidth(), fillRectangle.getHeight());
  }

  /**
   * Whether to add watermark. Will be determined based on the release.
   */
  public void setWatermark(final ReleaseType release) {
    final WatermarkType watermark;

    switch (release) {
    case CONCEPT:
      watermark = WatermarkType.CONCEPT;
      break;
    case DEPRECATED:
      watermark = WatermarkType.DEPRECATED;
      break;
    case PRODUCTION:
    default:
      watermark = null;
    }

    getContext().setWatermark(watermark);
  }

  protected PAAFlowState getFlowState() {
    return flowState;
  }

}
