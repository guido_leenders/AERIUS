/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.layer;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.info.Country;
import nl.overheid.aerius.shared.domain.info.DepositionMarker;
import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * The PAA deposition markers in an image layer.
 */
public class DepositionMarkerLayers extends PDFImageLayer {

  private static final String MARKER_FILENAME_NATURE = "marker-high-projectcontribution-nature.png";
  private static final String MARKER_FILENAME_MAX = "marker-high-projectcontribution.png";

  private final PAAContext paaContext;

  private List<DepositionMarker> markers;
  private AssessmentArea maxMarkerArea;
  private final Country[] filterOnCountries;

  public DepositionMarkerLayers(final PAAContext paaContext, final Country ... filterOnCountries) {
    this.paaContext = paaContext;

    this.filterOnCountries = filterOnCountries;

    initData();
  }

  private void initData() {
    DepositionMarker maxMarker = null;

    final DepositionMarkerType defaultMarkerType = DepositionMarkerType.HIGHEST_CALCULATED_DEPOSITION;
    final PDFExportAssessmentAreaInfo areaWithMax = paaContext.getAreaWithHighestProjectContribution();

    markers = new ArrayList<DepositionMarker>();
    for (final PDFExportAssessmentAreaInfo item : paaContext.getExportInfo()) {
      if (item.getDepositionInfo().getMaxDeltaDemand() <= 0) {
        continue;
      }

      // Do a country filter, if there is at least one specified
      if (skipCountry(item.getCountry())) {
        continue;
      }

      final DepositionMarker marker  = new DepositionMarker();
      marker.setX(item.getMaxDeltaPoint().getX());
      marker.setY(item.getMaxDeltaPoint().getY());
      marker.combine(defaultMarkerType, item.getDepositionInfo().getMaxDeltaDemand());

      if (item == areaWithMax) {
        maxMarker = marker;
        maxMarkerArea = item;
      }

      markers.add(marker);
    }

    if (maxMarker != null) {
      maxMarker.setMarkerType(DepositionMarkerType.HIGHEST_CALCULATED_AND_TOTAL);
    }
  }

  private boolean skipCountry(final Country country) {
    boolean skipCountry = false;

    if (filterOnCountries != null && filterOnCountries.length > 0) {
      for (final Country item : filterOnCountries) {
        if (item == country) {
          skipCountry = true;
          break;
        }
      }
    }

    return skipCountry;
  }

  @Override
  public BufferedImage getImage(final BBox bbox, final int width, final int height) {
    final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

    final Graphics2D g = image.createGraphics();
    try {
      for (final DepositionMarker marker : markers) {
        String markerFilename = null;
        switch (marker.getMarkerType()) {
        case HIGHEST_CALCULATED_DEPOSITION: // per nature area
          markerFilename = MARKER_FILENAME_NATURE;
          break;
        case HIGHEST_CALCULATED_AND_TOTAL: // the max max max max one
          markerFilename = MARKER_FILENAME_MAX;
          break;
        default:
          return null;
        }
        if (markerFilename != null) {
          drawMarker(g, marker, markerFilename, bbox, width, height);
        }
      }
    } finally {
      g.dispose();
    }

    return image;
  }

  private void drawMarker(final Graphics2D g, final DepositionMarker marker, final String filename, final BBox bbox,
      final int width, final int height) {
    // Scale factor is already applied if getImage is called, use 1.
    final Point position = PDFUtils.getPositionInMapImage(bbox, marker, width, height, 1);

    final BufferedImage markerImage;
    try {
      markerImage = ImageIO.read(PDFUtils.class.getResource(PAASharedConstants.RESOURCES_DIR + filename));
    } catch (final IOException ioe) {
      throw new RuntimeException(ioe);
    }

    g.drawImage(markerImage, (int) position.getX() - (markerImage.getWidth() / 2), (int) position.getY() - markerImage.getHeight(), null);
  }

  public AssessmentArea getMaxMarkerArea() {
    return maxMarkerArea;
  }

}
