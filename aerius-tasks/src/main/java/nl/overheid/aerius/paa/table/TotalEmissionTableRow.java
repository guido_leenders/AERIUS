/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import nl.overheid.aerius.shared.domain.Substance;

class TotalEmissionTableRow {

  private final Substance substance;
  private final double emissionSituation1;
  private final Double emissionSituation2;

  TotalEmissionTableRow(final Substance substance, final double emissionSituation1, final Double emissionSituation2) {
    this.substance = substance;
    this.emissionSituation1 = emissionSituation1;
    this.emissionSituation2 = emissionSituation2;
  }

  public Substance getSubstance() {
    return substance;
  }

  public double getEmissionSituation1() {
    return emissionSituation1;
  }

  public Double getEmissionSituation2() {
    return emissionSituation2;
  }

}