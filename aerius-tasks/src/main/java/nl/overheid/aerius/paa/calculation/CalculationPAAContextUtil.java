/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.calculation;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.paa.common.PAAContextUtil;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public final class CalculationPAAContextUtil {

  private CalculationPAAContextUtil() {
    //util class
  }

  public static CalculationPAAContext createContext(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data,
      final SegmentType segmentType, final boolean includeDemand, final PAAReportType reportType) throws SQLException, AeriusException {
    final Locale locale = PAAContextUtil.getLocale(data);
    final SectorCategories categories = PAAContextUtil.getSectorCategories(pmf, locale);
    final List<PDFExportAssessmentAreaInfo> exportInfo = PAAContextUtil.getExportInfo(pmf, data.getScenario(), segmentType, includeDemand);
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);
    return new CalculationPAAContext(pmf, httpClient, locale, rgs.getEpsg(), reportType, categories, data.getScenario(), exportInfo);
  }

}
