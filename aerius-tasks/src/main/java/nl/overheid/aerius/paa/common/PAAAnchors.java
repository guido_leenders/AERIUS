/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

/**
 *
 */
public final class PAAAnchors {

  private static final String ANCHORREF_INDEX = "INDEX_";
  private static final String ANCHORREF_CHILD_FEATURE = "FEATURE";
  private static final String ANCHORREF_CHILD_EMISSION_RECAP = "EMISSION_RECAP";
  private static final String ANCHORREF_CHILD_DEPOSITION_MAP = "DEPOSITION_MAP";
  private static final String ANCHORREF_CHILD_DEPOSITION_RESULTS = "DEPOSITION_RESULTS";
  private static final String ANCHORREF_CHILD_EMISSION_DETAILS = "EMISSION_DETAILS";

  private PAAAnchors() {
    //util class
  }

  public static String getAnchorReferenceToFeatureBlock() {
    return ANCHORREF_INDEX + ANCHORREF_CHILD_FEATURE;
  }

  public static String getAnchorReferenceToEmissionRecapBlock() {
    return ANCHORREF_INDEX + ANCHORREF_CHILD_EMISSION_RECAP;
  }

  public static String getAnchorReferenceToDepositionMapBlock() {
    return ANCHORREF_INDEX + ANCHORREF_CHILD_DEPOSITION_MAP;
  }

  public static String getAnchorReferenceToDepositionResultsBlock() {
    return ANCHORREF_INDEX + ANCHORREF_CHILD_DEPOSITION_RESULTS;
  }

  public static String getAnchorReferenceToEmissionDetailsBlock() {
    return ANCHORREF_INDEX + ANCHORREF_CHILD_EMISSION_DETAILS;
  }

}
