/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.text.MessageFormat;
import java.util.Locale;

import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;

/**
 * Data table as used in the PAA export containing emission data for a shipping source.
 */
public class MaritimeRouteSourceDataTable extends ShippingSourceDataTable<RouteMaritimeVesselGroup> {

  enum MaritimeRouteColumn {
    SHIP_CODE(ShippingSourceColumn.SHIP_CODE.getHeaderI18nKey(), 8),
    DESCRIPTION(ShippingSourceColumn.DESCRIPTION.getHeaderI18nKey(), 12),
    VISITS(SubSourceColumn.getHeaderKey("ship_visits"), 6),
    SUBSTANCE(SubSourceColumn.SUBSTANCE.getHeaderI18nKey(), 3),
    EMISSION(SubSourceColumn.EMISSION.getHeaderI18nKey(), 6);

    private final PDFColumnProperties columnProperties;

    private MaritimeRouteColumn(final String headerKey, final float relativeWidth) {
      this.columnProperties = new PDFColumnPropertiesImpl(headerKey, relativeWidth);
    }

    public PDFColumnProperties getColumnProperties() {
      return columnProperties;
    }
  }

  /**
   * Constructor to set header + widths.
   * @param year The year to use.
   * @param emissionSource The ShipEmissionValues to make table for.
   * @param locale The locale to use.
   */
  public MaritimeRouteSourceDataTable(final int year, final MaritimeRouteEmissionSource emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addSpecificColumns(final Locale locale) {
    addVisitsColumn(locale);
  }

  private void addVisitsColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<RouteMaritimeVesselGroup>(locale, MaritimeRouteColumn.VISITS.getColumnProperties()) {

      @Override
      protected String getText(final RouteMaritimeVesselGroup data) {
        return MessageFormat.format(getText("value_per_time_unit", data.getTimeUnit()), formatInt(data.getShipMovementsPerTimeUnit()));
      }
    });
  }

  @Override
  protected PDFColumnProperties getShipCodeColumnProperties() {
    return MaritimeRouteColumn.SHIP_CODE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getDescriptionColumnProperties() {
    return MaritimeRouteColumn.DESCRIPTION.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getSubstanceColumnProperties() {
    return MaritimeRouteColumn.SUBSTANCE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getEmissionColumnProperties() {
    return MaritimeRouteColumn.EMISSION.getColumnProperties();
  }

}
