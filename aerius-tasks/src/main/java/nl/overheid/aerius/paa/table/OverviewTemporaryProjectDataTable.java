/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import nl.overheid.aerius.paa.common.PAAContext;

/**
 *
 */
public class OverviewTemporaryProjectDataTable extends GenericThreeColumnDataTable {

  public OverviewTemporaryProjectDataTable(final PAAContext context, final int startYear) {
    super(context.getLocale(), new GenericThreeColumnDataRow(
        "base_activity_temp_project_startyear",
        "base_activity_temp_project_duration",
        null));

    final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
    row.set(Column.COLUMN_1, startYear);
    row.set(Column.COLUMN_2, context.getCalculatedScenario().getOptions().getTemporaryProjectYears());
    addRow(row);
  }

}
