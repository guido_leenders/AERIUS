/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.pdf.PDFConstants;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSourceCollection;
import nl.overheid.aerius.shared.domain.source.EmissionSubSource;

/**
 *
 */
public class SubstancePDFColumn<T extends EmissionSubSource> extends TextPDFColumn<T> {

  private static final List<Substance> SUBSTANCES = getSubstances();
  private final EmissionSourceCollection<T> source;
  private final int year;

  public SubstancePDFColumn(final Locale locale, final PDFColumnProperties columnProperties, final EmissionSourceCollection<T> source, final int year) {
    super(locale, columnProperties);
    this.source = source;
    this.year = year;
  }

  @Override
  protected String getText(final T data) {
    final List<String> labels = new ArrayList<>();
    for (final Substance substance : SUBSTANCES) {
      if (Double.doubleToLongBits(source.getEmission(data, new EmissionValueKey(year, substance))) != 0) {
        labels.add(getText("emission_datatable_" + substance.name()));
      }
    }
    return StringUtils.join(labels, PDFConstants.NEWLINE);
  }

  private static List<Substance> getSubstances() {
    // don't iterate over Substance.values(). Don't want all emissions (probably won't have the right text) and ordering matters.
    final List<Substance> substances = new ArrayList<>();
    substances.add(Substance.NOX);
    substances.add(Substance.NH3);
    return substances;
  }
}
