/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.text.MessageFormat;
import java.util.Locale;

import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PDFColumnPropertiesImpl;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;

/**
 * Data table as used in the PAA export containing emission data for a road source.
 */
public class RoadSourceDataTable extends SubSourceDataTable<VehicleEmissions> {

  enum RoadColumn {
    EMISSION_TYPE(SubSourceColumn.getHeaderKey("road_type"), 6),
    DESCRIPTION(SubSourceColumn.getHeaderKey("road_description"), 6),
    VEHICLES_PER_TIMEUNIT(SubSourceColumn.getHeaderKey("vehicles_per_timeunit"), 6),
    SUBSTANCE(SubSourceColumn.SUBSTANCE.getHeaderI18nKey(), 3),
    EMISSION(SubSourceColumn.EMISSION.getHeaderI18nKey(), 6);

    private final PDFColumnProperties columnProperties;

    private RoadColumn(final String headerKey, final float relativeWidth) {
      this.columnProperties = new PDFColumnPropertiesImpl(headerKey, relativeWidth);
    }

    public PDFColumnProperties getColumnProperties() {
      return columnProperties;
    }
  }

  /**
   * Constructor to set header + widths.
   *
   * @param year
   *          The year to use.
   * @param emissionSource
   *          The SRM2EmissionSource to make table for.
   * @param locale
   *          The locale to use.
   */
  public RoadSourceDataTable(final int year, final SRM2EmissionSource emissionSource, final Locale locale) {
    super(year, emissionSource, locale);
  }

  @Override
  protected void addDescriptionColumns(final Locale locale) {
    addEmissionTypeColumn(locale);
    addDescriptionColumn(locale);
  }

  @Override
  protected void addSpecificColumns(final Locale locale) {
    addVehiclesPerTimeUnitColumn(locale);
  }

  private void addEmissionTypeColumn(final Locale locale) {
    addColumn(new TextPDFColumn<VehicleEmissions>(locale, RoadColumn.EMISSION_TYPE.getColumnProperties()) {

      @Override
      protected String getText(final VehicleEmissions data) {
        final String returnValue;
        if (data instanceof VehicleSpecificEmissions) {
          returnValue = getText("emission_datatable_road_type_specific");
        } else if (data instanceof VehicleCustomEmissions) {
          returnValue = getText("emission_datatable_road_type_custom");
        } else if (data instanceof VehicleStandardEmissions) {
          returnValue = getText("emission_datatable_road_type_standard");
        } else {
          returnValue = "";
        }
        return returnValue;
      }
    });
  }

  private void addDescriptionColumn(final Locale locale) {
    addColumn(new TextPDFColumn<VehicleEmissions>(locale, RoadColumn.DESCRIPTION.getColumnProperties()) {

      @Override
      protected String getText(final VehicleEmissions data) {
        final String returnValue;
        if (data instanceof VehicleSpecificEmissions) {
          returnValue = ((VehicleSpecificEmissions) data).getCategory().getName();
        } else if (data instanceof VehicleCustomEmissions) {
          returnValue = ((VehicleCustomEmissions) data).getDescription();
        } else if (data instanceof VehicleStandardEmissions) {
          returnValue = PAABaseTable.getText("emission_datatable_vehicle_type",
              ((VehicleStandardEmissions) data).getEmissionCategory().getVehicleType(), getLocale());
        } else {
          returnValue = "";
        }
        return returnValue;
      }
    });
  }

  private void addVehiclesPerTimeUnitColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<VehicleEmissions>(locale, RoadColumn.VEHICLES_PER_TIMEUNIT.getColumnProperties()) {

      @Override
      protected String getText(final VehicleEmissions data) {
        return MessageFormat.format(getText("value_per_time_unit", data.getTimeUnit()), formatDouble(data.getVehiclesPerTimeUnit()));
      }
    });
  }

  @Override
  protected PDFColumnProperties getSubstanceColumnProperties() {
    return RoadColumn.SUBSTANCE.getColumnProperties();
  }

  @Override
  protected PDFColumnProperties getEmissionColumnProperties() {
    return RoadColumn.EMISSION.getColumnProperties();
  }
}
