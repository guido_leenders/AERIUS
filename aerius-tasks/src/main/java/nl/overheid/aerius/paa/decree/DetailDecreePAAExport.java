/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.paa.base.PAAExport;
import nl.overheid.aerius.paa.base.PAAPageEventHelper;
import nl.overheid.aerius.paa.block.PAAReportBlock;
import nl.overheid.aerius.paa.block.ReportBlockDepositionDetail;
import nl.overheid.aerius.paa.common.PAAReportType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
public class DetailDecreePAAExport extends PAAExport<DecreePAAContext> {

  public DetailDecreePAAExport(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data,
      final List<StringDataSource> gmlStrings, final DecreeInformation decreeInfo) throws SQLException, AeriusException {
    super(createContext(pmf, httpClient, data, decreeInfo), gmlStrings);
  }

  private static DecreePAAContext createContext(final PMF pmf, final CloseableHttpClient httpClient, final CalculationInputData data,
      final DecreeInformation decreeInfo) throws SQLException, AeriusException {
    return DecreePAAContextUtil.createContext(pmf, httpClient, data, decreeInfo, PAAReportType.DETAIL_DECREE);
  }

  @Override
  protected List<PAAReportBlock<?>> getReportBlocks() {
    final List<PAAReportBlock<?>> reportBlocks = new ArrayList<>();
    //Detail bit. Shows overview first, then detail pages.
    reportBlocks.add(new ReportBlockDepositionDetail(pdfDocument, getContext()));

    addDisclaimerBlocks(reportBlocks);
    return reportBlocks;
  }

  @Override
  protected PAAPageEventHelper createHeaderFooterHelper() {
    return new DetailDecreePAAPageEventHelper(getFlowState(), getContext());
  }

}
