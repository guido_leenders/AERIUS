/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import java.text.MessageFormat;

import nl.overheid.aerius.paa.base.PAAFrontPage;
import nl.overheid.aerius.shared.domain.register.SegmentType;

/**
 *
 */
public class DecreePAAFrontPage extends PAAFrontPage {

  private final SegmentType segment;

  protected DecreePAAFrontPage(final DecreePAAContext paaContext) {
    super(paaContext);
    this.segment = paaContext.getDecreeInfo().getSegment();
  }

  @Override
  protected String getIndexBlockHeader() {
    return MessageFormat.format(paaContext.getText("frontpage_index_decree"), paaContext.getText("segment", segment));
  }

}
