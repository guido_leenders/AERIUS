/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.db.common.PAARepository;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.table.result.CalculationPointsDataTable;
import nl.overheid.aerius.paa.table.result.CalculationPointsDeltaDataTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.shared.domain.deposition.PAACalculationPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * PAA Report Block: Calculation points.
 */
public class ReportBlockCalculationPoints extends PAAReportBlock<PAAContext> {

  public ReportBlockCalculationPoints(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws SQLException, AeriusException, DocumentException {
    final PDFSimpleTable calculationPointTable = PAAUtil.getDefaultBlockEmptyTable(true);
    calculationPointTable.setKeepTogether(true);

    PAAUtil.addBlockHeaderCell(calculationPointTable, paaContext.getText("calculationpoint_title"));

    final List<PAACalculationPoint> cPoints1;
    final List<PAACalculationPoint> cPoints2;
    try (final Connection con = getPmf().getConnection()) {
      cPoints1 = PAARepository.getCalculationPointDepositions(con, set1.getCalculationId(), set1.getSources());
      if (set2 == null) {
        cPoints2 = null;
      } else {
        cPoints2 = PAARepository.getCalculationPointDepositions(con, set2.getCalculationId(), set2.getSources());
      }
    }

    if (cPoints1 != null && !cPoints1.isEmpty()) {
      final CalculationPointsDataTable cpData;
      if (cPoints2 == null) {
        cpData = new CalculationPointsDataTable(paaContext.getLocale(), cPoints1);
      } else {
        cpData = new CalculationPointsDeltaDataTable(paaContext.getLocale(), cPoints1, cPoints2);
      }
      calculationPointTable.add(cpData);

      pdfDocument.addElement(calculationPointTable);
    }
  }

}
