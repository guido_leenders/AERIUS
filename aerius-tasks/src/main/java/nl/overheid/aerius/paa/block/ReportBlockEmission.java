/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.paa.common.PAAAnchors;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAALayerUtil;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.paa.table.source.FarmSourceDataTable;
import nl.overheid.aerius.paa.table.source.InlandMooringRouteTable;
import nl.overheid.aerius.paa.table.source.InlandMooringSourceDataTable;
import nl.overheid.aerius.paa.table.source.InlandRouteSourceDataTable;
import nl.overheid.aerius.paa.table.source.MaritimeMooringRouteTable;
import nl.overheid.aerius.paa.table.source.MaritimeMooringSourceDataTable;
import nl.overheid.aerius.paa.table.source.MaritimeRouteSourceDataTable;
import nl.overheid.aerius.paa.table.source.OffRoadMobileSourceDataTable;
import nl.overheid.aerius.paa.table.source.PlanSourceDataTable;
import nl.overheid.aerius.paa.table.source.RoadSourceDataTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.PDFImageLayer;
import nl.overheid.aerius.pdf.table.IsPDFTable;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.pdf.table.Padding;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * PAA Report Block: Emission.
 */
public class ReportBlockEmission extends PAAReportBlock<PAAContext> implements EmissionSourceVisitor<List<IsPDFTable>> {

  // There is always some padding added for the empty cell, increase if more padding is needed.
  private static final float PADDING_BETWEEN_SOURCES = 15;
  private static final int SOURCE_IMAGE_WIDTH = 160;
  private static final int SOURCE_IMAGE_HEIGHT = 100;
  private static final float PADDING_SECTOR_SPECIFIC_TABLE = 15;

  private static final int MINIMUM_WIDTH_BBOX = 250;
  private static final int MINIMUM_HEIGHT_BBOX = 250;
  private static final double USE_PADDING_FOR_BBOX_POLYGON_FACTOR = 0.25d;

  private static final int LABELLED_VALUE_PADDING_TOP = 5;

  private static final PDFRelativeWidths CONTENT_RELATIVE_WIDTHS = new PDFRelativeWidths(40, 60);
  private static final PDFRelativeWidths SOURCE_INFO_RELATIVE_WIDTHS = new PDFRelativeWidths(30, 68);
  private static final String ACTUAL_USED_SIGN = " ";
  private final EmissionSourceList sources;

  public ReportBlockEmission(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext, final EmissionSourceList sources) {
    super(pdfDocument, paaContext);
    this.sources = sources;
  }

  @Override
  public void generate() throws DocumentException, AeriusException, IOException {
    boolean firstSource = true;

    for (final EmissionSource source : sources) {
      final PDFSimpleTable emissionTable = PAAUtil.getDefaultBlockEmptyTable(false);
      emissionTable.setKeepTogether(true);

      // Only the first source should have the block header as first column, the rest should start with an empty cell.
      if (firstSource) {
        firstSource = false;
        PAAUtil.addBlockHeaderCellWithSubHeader(emissionTable, paaContext.getText("emission_title"), sources.getName(),
            PAAAnchors.getAnchorReferenceToEmissionDetailsBlock());
      } else {
        pdfDocument.addPadding(PADDING_BETWEEN_SOURCES);
        emissionTable.addEmptyCell();
      }

      final PDFSimpleTable contentTable = new PDFSimpleTable(CONTENT_RELATIVE_WIDTHS);

      //On the left the image containing the map.
      addSourceLocationImage(contentTable, source);

      //On the right the information about the source.
      addSourceInformationTable(contentTable, source);

      for (final IsPDFTable sectorSpecificTable : source.accept(this)) {
        contentTable.add(getSectorSpecificTableCell(sectorSpecificTable));
      }

      emissionTable.add(contentTable);
      pdfDocument.addElement(emissionTable);
    }
  }

  private void addSourceLocationImage(final PDFSimpleTable tableToAddTo, final EmissionSource source) throws AeriusException {
    BBox imageBBox;
    switch (source.getGeometry().getType()) {
    case POLYGON:
      imageBBox = GeometryUtil.determineBBoxForGeometry(source.getGeometry());
      imageBBox = GeometryUtil.addPadding(imageBBox, USE_PADDING_FOR_BBOX_POLYGON_FACTOR);
      break;
    case POINT:
      imageBBox = new BBox();
      imageBBox.setMinX(source.getX());
      imageBBox.setMaxX(source.getX());
      imageBBox.setMinY(source.getY());
      imageBBox.setMaxY(source.getY());
      break;
    case LINE:
      imageBBox = GeometryUtil.determineBBoxForGeometry(source.getGeometry());
      imageBBox = GeometryUtil.addPadding(imageBBox, USE_PADDING_FOR_BBOX_POLYGON_FACTOR);
      break;
    default:
      throw new IllegalArgumentException("Not yet supported type of geometry encountered: "
          + source.getGeometry().getType().toString());
    }
    imageBBox = GeometryUtil.ensureMinimumWidthHeight(imageBBox,
        MINIMUM_WIDTH_BBOX, MINIMUM_HEIGHT_BBOX);

    final ArrayList<EmissionSource> sources = new ArrayList<>();
    sources.add(source);
    final List<PDFImageLayer> imageLayers =
        PAALayerUtil.getLayersForSources(imageBBox, SOURCE_IMAGE_WIDTH, SOURCE_IMAGE_HEIGHT, sources, null, true, 0);

    //add the shipping network wms layer if needed (showing part of the ship network that fits the image)
    if (source instanceof MaritimeMooringEmissionSource) {
      imageLayers.add(0, getWMSLayer(ConstantRepository.findConstantByKey(getPmf(), SharedConstantsEnum.LAYER_SHIP_NETWORK.name(), true), null));
    }

    final BufferedImage tmsImage = PAALayerUtil.getFeaturedImage(paaContext, imageBBox, SOURCE_IMAGE_WIDTH, SOURCE_IMAGE_HEIGHT,
        imageLayers.toArray(new PDFImageLayer[imageLayers.size()]));
    tableToAddTo.addImage(tmsImage, SOURCE_IMAGE_WIDTH, SOURCE_IMAGE_HEIGHT);
  }

  private void addSourceInformationTable(final PDFSimpleTable tableToAddTo, final EmissionSource source) {
    final List<LabelledValue> infoData = new ArrayList<>();
    infoData.add(new LabelledValue(paaContext.getText("emission_name"), source.getLabel()));
    infoData.add(new LabelledValue(paaContext.getText("emission_location"), source.getRoundedX() + ", " + source.getRoundedY()));

    addCharacteristicValues(source, infoData);

    addSectorSpecificValues(source, infoData);

    addEmissionValues(source, infoData);

    final PDFSimpleTable infoTable = new PDFSimpleTable(SOURCE_INFO_RELATIVE_WIDTHS);
    infoTable.setDefaultPadding(new Padding(0, Padding.DEFAULT_PADDING, Padding.DEFAULT_PADDING, Padding.DEFAULT_PADDING));
    //ensure the table gets filled for all supplied label-value pairs.
    final int rows = infoData.size();
    for (final LabelledValue row : infoData) {
      addLabelledValueToTable(infoTable, row);
    }

    /* Add extra padding so the infoTable kinda matches with the emission image,
     * so there won't be any overlap with the content that is going to follow. */
    final int rowHeight = 16;
    final int emptyRowHeight = 5;
    for (int j = 0; j < ((SOURCE_IMAGE_HEIGHT - (rows * rowHeight)) / emptyRowHeight); ++j) {
      infoTable.addEmptyCell();
      infoTable.addEmptyCell();
    }
    tableToAddTo.add(infoTable);
  }

  private void addCharacteristicValues(final EmissionSource source, final List<LabelledValue> infoData) {

    // building influence

    if (source.getSourceCharacteristics().isBuildingInfluence()) {
      final String buildingString =
          "" + getOneDecimalFormat().format(source.getSourceCharacteristics().getBuildingLength()) + " x "
              + getOneDecimalFormat().format(source.getSourceCharacteristics().getBuildingWidth()) + " x "
              + getOneDecimalFormat().format(source.getSourceCharacteristics().getBuildingHeight()) + " m "
              + PDFUtils.getNumberFormat(paaContext.getLocale(), 0).format(source.getSourceCharacteristics().getBuildingOrientation()) + "\u00B0";
      final String defaultBuildingString =
          "" + getOneDecimalFormat().format(OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM) + " x "
              + getOneDecimalFormat().format(OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM) + " x "
              + getOneDecimalFormat().format(OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM) + " m "
              + PDFUtils.getNumberFormat(paaContext.getLocale(), 0).format(0D) + "\u00B0";
      if (isBuildingOutsideLimits(source.getSourceCharacteristics())) {
        final String actualBuildingString = actualBuildingSettings(source.getSourceCharacteristics());
        infoData.add(new LabelledValue(paaContext.getText("emission_building_dimention"), buildingString));
        infoData.add(new LabelledValue("", actualBuildingString));
      } else {
        addValueAndUnderlineIfDefaultString(infoData, paaContext.getText("emission_building_dimention"), buildingString,
            buildingString, defaultBuildingString);
      }
    }

    if (source instanceof HasOPSSourceCharacteristics && !(source instanceof SRM2EmissionSource)) {
      final OPSSourceCharacteristics defaultSourceCharacteristics =
          paaContext.getSectorCategories().getSectorById(source.getSector().getSectorId()).getDefaultCharacteristics();

      //emission height if needed show used calculated value
      final double height = source.getSourceCharacteristics().getEmissionHeight();
      final double actualHeight = determineActualUsedValue(height, OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM,
          OPSLimits.SOURCE_IS_BUILDING_EMISSION_HEIGHT_MAXIMUM);
      if (source.getSourceCharacteristics().isBuildingInfluence() && Double.compare(height, actualHeight) != 0) {
        infoData.add(new LabelledValue(paaContext.getText("emission_height"), getOneDecimalFormat().format(height) + " m"
            + ACTUAL_USED_SIGN + "(" + getOneDecimalFormat().format(actualHeight) + " m)"));
      } else {
        addValueAndUnderlineIfDefault(infoData, paaContext.getText("emission_height"), getOneDecimalFormat().format(height) + " m",
            height, defaultSourceCharacteristics.getEmissionHeight());
      }

      // Special part based on geometry of the source.
      switch (source.getGeometry().getType()) {
      //surface and spread if polygon.
      case POLYGON:
        infoData.add(new LabelledValue(paaContext.getText("emission_surface"),
            getOneDecimalFormat().format(source.getGeometry().getMeasure() / SharedConstants.M2_TO_HA) + " ha"));

        final double spread = source.getSourceCharacteristics().getSpread();
        addValueAndUnderlineIfDefault(infoData, paaContext.getText("emission_spread"), getOneDecimalFormat().format(spread) + " m",
            spread, defaultSourceCharacteristics.getSpread());
        break;
      case LINE:
        break;
      case POINT:
        break;
      default:
        throw new IllegalArgumentException("Not yet supported type of geometry encountered : "
            + source.getGeometry().getType().toString());
      }

      // custom headContent if applicable
      if (source.getSourceCharacteristics().isUserdefinedHeatContent()) {
        final double heatContent = source.getSourceCharacteristics().getHeatContent();
        addValueAndUnderlineIfDefault(infoData, paaContext.getText("emission_heatContent"),
            PDFUtils.getNumberFormat(paaContext.getLocale(), OPSLimits.SOURCE_HEAT_CONTENT_DIGITS_PRECISION).format(heatContent) + " MW", heatContent,
            defaultSourceCharacteristics.getHeatContent());
      } else {
        addValueAndUnderlineIfDefault(infoData, paaContext.getText("emission_outflow_temperature"),
            PDFUtils.getNumberFormat(paaContext.getLocale(), OPSLimits.SOURCE_TEMPERATURE_DIGITS_PRECISION).format(source.getSourceCharacteristics()
                .getEmissionTemperature()) + " \u00B0C", source.getSourceCharacteristics().getEmissionTemperature(),
            OPSSourceCharacteristics.AVERAGE_SURROUNDING_TEMPERATURE);

        //outflowDiameter show the used calculated value
        final double outflowDiameter = source.getSourceCharacteristics().getOutflowDiameter();
        final double actualoutflowDiameter = determineActualUsedValue(outflowDiameter, OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM,
            OPSLimits.SOURCE_IS_BUILDING_OUTFLOW_DIAMETER_MAXIMUM);
        if (source.getSourceCharacteristics().isBuildingInfluence() && Double.compare(outflowDiameter, actualoutflowDiameter) != 0) {
          infoData.add(new LabelledValue(
              paaContext.getText("emission_outflow_diameter"),
              PDFUtils.getNumberFormat(paaContext.getLocale(), OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION).format(outflowDiameter) + " m"
              + ACTUAL_USED_SIGN + "(" + PDFUtils.getNumberFormat(paaContext.getLocale(), OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION)
              .format(actualoutflowDiameter) + " m" + ")"));
        } else {
          addValueAndUnderlineIfDefault(infoData, paaContext.getText("emission_outflow_diameter"),
              PDFUtils.getNumberFormat(paaContext.getLocale(), OPSLimits.SOURCE_OUTFLOW_DIAMETER_DIGITS_PRECISION).format(outflowDiameter) + " m",
              outflowDiameter, OPSLimits.SOURCE_UI_OUTFLOW_DIAMETER_MINIMUM);
        }

        addValueAndUnderlineIfDefaultString(infoData, paaContext.getText("emission_outflow_direction"),
            context.getText("emission_outflow_direction_type", source.getSourceCharacteristics().getOutflowDirection()),
            source.getSourceCharacteristics().getOutflowDirection().toString(), OutflowDirectionType.VERTICAL.toString());

        // outflowVelocity show the used calculated value
        final double outflowVelocity = source.getSourceCharacteristics().getOutflowVelocity();
        final double actualOutflowVelocity = determineActualUsedValue(outflowVelocity, OPSLimits.SOURCE_UI_OUTFLOW_VELOCITY_MINIMUM,
            OPSLimits.SOURCE_IS_BUILDING_OUTFLOW_VELOCITY_MAXIMUM);
        if (source.getSourceCharacteristics().isBuildingInfluence() && Double.compare(outflowVelocity, actualOutflowVelocity) != 0) {
          infoData.add(new LabelledValue(paaContext.getText("emission_outflow_velocity"), getOneDecimalFormat().format(outflowVelocity) + " m/s"
              + ACTUAL_USED_SIGN + "(" + getOneDecimalFormat().format(actualOutflowVelocity) + " m/s" + ")"));
        } else {
          addValueAndUnderlineIfDefault(infoData, paaContext.getText("emission_outflow_velocity"),
              getOneDecimalFormat().format(outflowVelocity) + " m/s", outflowVelocity, 0D);
        }
      }

      //diurnal variation if that could be chosen.
      if (source instanceof GenericEmissionSource) {
        infoData.add(new LabelledValue(paaContext.getText("emission_diurnal_variation_label"),
            source.getSourceCharacteristics().getDiurnalVariationSpecification().getName()));
      }
    }
  }

  private String actualBuildingSettings(final OPSSourceCharacteristics source) {
    return "(" + getOneDecimalFormat().format(
        determineActualUsedValue(source.getBuildingLength(), OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM,
            OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM))
        + " x " + getOneDecimalFormat().format(determineActualUsedValue(source.getBuildingWidth(), OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM,
            OPSLimits.SCOPE_BUILDING_WIDTH_MAXIMUM))
        + " x " + getOneDecimalFormat().format(
            determineActualUsedValue(source.getBuildingHeight(), OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM,
            OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM))
        + " m " + PDFUtils.getNumberFormat(paaContext.getLocale(), 0).format(source.getBuildingOrientation()) + "\u00B0" + ")";
  }

  private double determineActualUsedValue(final double value, final double minimumValue, final double maximumValue) {
    return Math.max(minimumValue, Math.min(maximumValue, value));
  }

  private boolean isBuildingOutsideLimits(final OPSSourceCharacteristics source) {
    return source.getBuildingWidth() < OPSLimits.SCOPE_BUILDING_WIDTH_MINIMUM
        || source.getBuildingWidth() > OPSLimits.SCOPE_BUILDING_WIDTH_MAXIMUM
        || source.getBuildingLength() < OPSLimits.SCOPE_BUILDING_LENGTH_MINIMUM
        || source.getBuildingLength() > OPSLimits.SCOPE_BUILDING_LENGTH_MAXIMUM
        || source.getBuildingHeight() < OPSLimits.SCOPE_BUILDING_HEIGHT_MINIMUM
        || source.getBuildingHeight() > OPSLimits.SCOPE_BUILDING_HEIGHT_MAXIMUM;
  }

  private void addEmissionValues(final EmissionSource source, final List<LabelledValue> infoData) {
    //and last but not least, the emissions.
    final double emissionNOX = source.getEmission(new EmissionValueKey(set1.getYear(), Substance.NOX));
    if (Double.doubleToLongBits(emissionNOX) != 0) {
      final String emissionNOXString = paaContext.getEmissionString(emissionNOX);
      infoData.add(new LabelledValue(paaContext.getText("emission_NOX"), emissionNOXString));
    }

    final double emissionNH3 = source.getEmission(new EmissionValueKey(set1.getYear(), Substance.NH3));
    if (Double.doubleToLongBits(emissionNH3) != 0) {
      final String emissionNH3String = paaContext.getEmissionString(emissionNH3);
      infoData.add(new LabelledValue(paaContext.getText("emission_NH3"), emissionNH3String));
    }
  }

  private void addSectorSpecificValues(final EmissionSource source, final List<LabelledValue> infoData) {
    if (source instanceof InlandRouteEmissionSource) {
      final InlandRouteEmissionSource inlandRoute = (InlandRouteEmissionSource) source;
      final InlandWaterwayCategory category = inlandRoute.getWaterwayCategory();
      if (category != null) {
        infoData.add(new LabelledValue(paaContext.getText("emission_waterway_type"), category.getName()));
        final WaterwayDirection direction = inlandRoute.getWaterwayDirection();
        if ((direction != null) && category.isDirectionRelevant()) {
          infoData.add(new LabelledValue(paaContext.getText("emission_waterway_direction"),
              paaContext.getText("emission_waterway_direction", direction)));
        }
      }
    }
  }

  public NumberFormat getOneDecimalFormat() {
    return PDFUtils.getNumberFormat(paaContext.getLocale(), 1);
  }

  private void addValueAndUnderlineIfDefault(final List<LabelledValue> infoData, final String description,
      final String valueString, final double value, final double defaultValue) {
    if (Double.doubleToLongBits(value) == Double.doubleToLongBits(defaultValue)) {
      infoData.add(new LabelledValue(description,
          new Phrase(valueString, PAASharedFonts.DEFAULT_TEXT_BIG_UNDERLINE.get())));
    } else {
      infoData.add(new LabelledValue(description, valueString));
    }
  }

  private void addValueAndUnderlineIfDefaultString(final List<LabelledValue> infoData, final String description,
      final String valueString, final String value, final String defaultValue) {
    if (value.equals(defaultValue)) {
      infoData.add(new LabelledValue(description,
          new Phrase(valueString, PAASharedFonts.DEFAULT_TEXT_BIG_UNDERLINE.get())));
    } else {
      infoData.add(new LabelledValue(description, valueString));
    }
  }

  private void addLabelledValueToTable(final PDFSimpleTable infoTable, final LabelledValue labelledValue) {
    infoTable.add(new Phrase(labelledValue.getLabel(), PAASharedFonts.EMISSION_KEY.get()),
        new Padding(LABELLED_VALUE_PADDING_TOP, Padding.DEFAULT_PADDING, Padding.DEFAULT_PADDING, Padding.DEFAULT_PADDING));
    if (labelledValue.getValuePhrase() != null) {
      infoTable.add(labelledValue.getValuePhrase());
    } else {
      infoTable.add(labelledValue.getValue() == null ? " " : labelledValue.getValue(), PAASharedFonts.DEFAULT_TEXT_BIG);
    }
  }

  private PdfPCell getSectorSpecificTableCell(final IsPDFTable tableToAdd) {
    final PdfPCell emissionTableCell = tableToAdd.getAsCell();
    emissionTableCell.setColspan(2);
    emissionTableCell.setPaddingTop(PADDING_SECTOR_SPECIFIC_TABLE);
    return emissionTableCell;
  }

  @Override
  public List<IsPDFTable> visit(final FarmEmissionSource emissionSource) throws AeriusException {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new FarmSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    boolean footNote = false;
    // determine to add a footnote
    for (final FarmLodgingEmissions lodging : emissionSource.getEmissionSubSources()) {
      if ((lodging instanceof FarmLodgingStandardEmissions) && ((FarmLodgingStandardEmissions) lodging).isEmissionFactorConstrained()) {
        footNote = true;
        break;
      }
    }
    if (footNote) {
      final PDFSimpleTable descriptionTable = new PDFSimpleTable(1);
      descriptionTable.add(
          paaContext.getText("emission_datatable_Animal_FootNoteRef") + paaContext.getText("emission_datatable_Animal_FootNote"),
          PAASharedFonts.DEFAULT_TEXT);
      sectorSpecificTables.add(descriptionTable);
    }
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final GenericEmissionSource emissionSource) {
    //no need for anything sector specific...
    return new ArrayList<>();
  }

  @Override
  public List<IsPDFTable> visit(final InlandMooringEmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new InlandMooringSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    sectorSpecificTables.add(new InlandMooringRouteTable(emissionSource, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final InlandRouteEmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new InlandRouteSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final MaritimeMooringEmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new MaritimeMooringSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    sectorSpecificTables.add(new MaritimeMooringRouteTable(emissionSource, false, paaContext.getLocale()));
    sectorSpecificTables.add(new MaritimeMooringRouteTable(emissionSource, true, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final MaritimeRouteEmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new MaritimeRouteSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final OffRoadMobileEmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new OffRoadMobileSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final PlanEmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new PlanSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final SRM2EmissionSource emissionSource) {
    final List<IsPDFTable> sectorSpecificTables = new ArrayList<>();
    sectorSpecificTables.add(new RoadSourceDataTable(set1.getYear(), emissionSource, paaContext.getLocale()));
    return sectorSpecificTables;
  }

  @Override
  public List<IsPDFTable> visit(final SRM2NetworkEmissionSource emissionSource) throws AeriusException {
    //nothing specific (yet...)
    return new ArrayList<>();
  }

  private class LabelledValue {

    private final String label;
    private final String value;
    private final Phrase valuePhrase;

    LabelledValue(final String label, final String value) {
      this.label = label;
      this.value = value;
      this.valuePhrase = null;
    }

    LabelledValue(final String label, final Phrase valuePhrase) {
      this.label = label;
      this.value = valuePhrase.getContent();
      this.valuePhrase = valuePhrase;
    }

    public String getLabel() {
      return label;
    }

    public String getValue() {
      return value;
    }

    public Phrase getValuePhrase() {
      return valuePhrase;
    }

  }

}
