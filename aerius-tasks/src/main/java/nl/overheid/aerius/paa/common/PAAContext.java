/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.pdf.PDFContext;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportDepositionInfo;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Data class containing PAA specific variables.
 */
public abstract class PAAContext extends PDFContext {

  private static final String HEADER_LOGO_FILENAME = "logo_{0}.wmf";


  private final SectorCategories categories;
  private final CalculatedScenario calculatedSenario;
  private final List<PDFExportAssessmentAreaInfo> exportInfo;
  private final Date creationDate;
  private final PAAReportType reportType;
  private WatermarkType watermark;

  /**
   * @throws AeriusException
   * @throws SQLException
   *
   */
  public PAAContext(final PMF pmf, final CloseableHttpClient httpClient, final Locale locale, final EPSG epsg, final PAAReportType reportType,
      final SectorCategories categories, final CalculatedScenario calculatedSenario, final List<PDFExportAssessmentAreaInfo> exportInfo)
          throws SQLException, AeriusException {
    super(pmf, httpClient, locale, epsg);
    this.reportType = reportType;
    this.categories = categories;
    this.calculatedSenario = calculatedSenario;
    this.exportInfo = exportInfo;
    creationDate = new Date();
  }

  public List<PDFExportAssessmentAreaInfo> getAreaInfoPasOnly() {
    final Predicate<PDFExportAssessmentAreaInfo> predicate = new Predicate<PDFExportAssessmentAreaInfo>() {

      @Override
      public boolean evaluate(final PDFExportAssessmentAreaInfo areaInfo) {
        return areaInfo.isPasArea();
      }
    };
    return getFilteredAreaInfo(predicate);
  }

  public List<PDFExportAssessmentAreaInfo> getHabitatedAreaInfoPasOnly() {
    return getAreaInfoPasOnly();
    }

  public String getDepositionHabitatsSubheaderText() {
    return getText("deposition_habitats_subheader");
  }

  public List<PDFExportAssessmentAreaInfo> getAreaInfoNonPasOnly(final boolean includingForeign) {
    final Predicate<PDFExportAssessmentAreaInfo> predicate = new Predicate<PDFExportAssessmentAreaInfo>() {

      @Override
      public boolean evaluate(final PDFExportAssessmentAreaInfo areaInfo) {
        return !areaInfo.isPasArea() && (includingForeign || !areaInfo.isForeignAuthority());
      }
    };
    return getFilteredAreaInfo(predicate);
  }

  public List<PDFExportAssessmentAreaInfo> getAreaInfoForeignOnly() {
    final Predicate<PDFExportAssessmentAreaInfo> predicate = new Predicate<PDFExportAssessmentAreaInfo>() {

      @Override
      public boolean evaluate(final PDFExportAssessmentAreaInfo areaInfo) {
        return areaInfo.isForeignAuthority();
      }
    };
    return getFilteredAreaInfo(predicate);
  }

  private List<PDFExportAssessmentAreaInfo> getFilteredAreaInfo(final Predicate<PDFExportAssessmentAreaInfo> predicate) {
    final List<PDFExportAssessmentAreaInfo> filtered = new ArrayList<>(exportInfo);
    CollectionUtils.filter(filtered, predicate);
    return filtered;
  }

  /**
   * Return the area with the highest project contribution (or delta if comparison).
   * @return null, if there is no contribution anywhere at all.
   */
  public PDFExportAssessmentAreaInfo getAreaWithHighestProjectContribution() {
    PDFExportAssessmentAreaInfo result = null;

    for (final PDFExportAssessmentAreaInfo areaInfo : getExportInfo()) {
      final PDFExportDepositionInfo depositionInfo = areaInfo.getDepositionInfo();

      if (!areaInfo.isPasArea() || areaInfo.isForeignAuthority()) {
        continue;
      }

      if (depositionInfo.getMaxDeltaDemand() > 0
          && (result == null || depositionInfo.getMaxDeltaDemand() > result.getDepositionInfo().getMaxDeltaDemand())) {
        result = areaInfo;
      }
    }

    return result;
  }

  public boolean isComparison() {
    return calculatedSenario instanceof CalculatedComparison;
  }

  public boolean isCustomPointsCalculation() {
   return CalculationType.CUSTOM_POINTS == getCalculatedScenario().getOptions().getCalculationType();
  }

  public CalculatedScenario getCalculatedScenario() {
    return calculatedSenario;
  }

  public String getFormattedCreationData(final String format) {
    return new SimpleDateFormat(format, getLocale()).format(creationDate);
  }

  public ScenarioMetaData getMetaData() {
    return calculatedSenario.getMetaData();
  }

  public SectorCategories getSectorCategories() {
    return categories;
  }

  public PAAReportType getReportType() {
    return reportType;
  }

  public List<PDFExportAssessmentAreaInfo> getExportInfo() {
    return exportInfo;
  }

  @Override
  public String getResourcesDir() {
    return PAASharedConstants.RESOURCES_DIR;
  }

  /**
   * Get formatted emission String.
   * @param emission The emission to format.
   * @param mainBlock Whether it's on the mainblock if not.
   * @return Formatted emission String.
   */
  public String getEmissionString(final double emission) {
    return PAAUtil.getEmissionString(getLocale(), emission);
  }

  public String getHeaderLogoFileName() {
    return getResourcesDir() + MessageFormat.format(HEADER_LOGO_FILENAME, getPmf().getProductType().name().toLowerCase());
  }

  public WatermarkType getWatermark() {
    return watermark;
  }

  public void setWatermark(final WatermarkType watermark) {
    this.watermark = watermark;
  }

}
