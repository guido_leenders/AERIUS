/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.lowagie.text.Anchor;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.pdf.font.PDFFonts;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.pdf.table.Padding;
import nl.overheid.aerius.worker.util.PDFUtils;
import nl.overheid.aerius.worker.util.TextUtils;

/**
 *
 */
public final class PAAUtil {

  private static final int DEFAULT_DECIMALS_EMISSIONS = 2;

  private static final int LEFT_CELL_PADDING_RIGHT = 10;
  private static final int LEFT_CELL_PADDING_BOTTOM = 3;

  private PAAUtil() {
    //util class
  }

  /**
   * Add block header cell with given text to given table. The text will contain an anchorReference.
   * @param table The table to add it to.
   * @param text The text to add.
   * @param anchorReference The anchorReference to add the text.
   */
  public static void addBlockHeaderCell(final PDFSimpleTable table, final String text, final String anchorReference) {
    table.add(getBlockHeaderCell(text, anchorReference));
  }

  /**
   * Add block header cell with given text to given table.
   * @param table The table to add it to.
   * @param text The text to add.
   */
  public static void addBlockHeaderCell(final PDFSimpleTable table, final String text) {
    addBlockHeaderCell(table, text, null);
  }

  /**
   * Add a block header cell with text and subtext to the given table.
   * @param table The table to add the header to.
   * @param headerText The text to add as header.
   * @param subHeaderText The text to add as a sub-header.
   * @param anchorReference Anchor reference to use for the header.
   */
  public static void addBlockHeaderCellWithSubHeader(final PDFSimpleTable table, final String headerText, final String subHeaderText,
      final String anchorReference) {
    final PDFSimpleTable headerTable = new PDFSimpleTable(1);
    headerTable.add(getBlockHeaderCell(headerText, anchorReference));
    headerTable.add(getBlockSubHeaderCell(subHeaderText));
    table.add(headerTable);
  }

  /**
   * Add a block header cell with text and subtext to the given table.
   * @param table The table to add the header to.
   * @param headerText The text to add as header.
   * @param subHeaderText The text to add as a sub-header.
   */
  public static void addBlockHeaderCellWithSubHeader(final PDFSimpleTable table, final String headerText, final String subHeaderText) {
    addBlockHeaderCellWithSubHeader(table, headerText, subHeaderText, null);
  }

  /**
   * @param text The text to use in the cell.
   * @param anchorReference The anchorReference to add to the cell (can be null)
   * @return The cell to use as a block header.
   */
  public static PdfPCell getBlockHeaderCell(final String text, final String anchorReference) {
    final Phrase phrase = getPhrase(text, anchorReference, PAASharedFonts.BLOCK_HEADER);
    return getCell(phrase);
  }

  private static PdfPCell getBlockSubHeaderCell(final String text) {
    final Phrase phrase = getPhrase(text, null, PAASharedFonts.BLOCK_SUB_HEADER);
    return getCell(phrase);
  }

  private static Phrase getPhrase(final String text, final String anchorReference, final PDFFonts font) {
    Phrase phrase = new Phrase(text, font.get());
    if (!StringUtils.isEmpty(anchorReference)) {
      final Anchor anchor = new Anchor(phrase);
      phrase = anchor;
      anchor.setName(anchorReference);
    }
    return phrase;
  }

  private static PdfPCell getCell(final Phrase phrase) {
    final PdfPCell cell = new PdfPCell(phrase);
    applyDefaultBlockCellSettingsLeft(cell);

    return cell;
  }

  /**
   * Apply default settings as used for the left block cells.
   * @param cell The cell to apply it to.
   */
  private static void applyDefaultBlockCellSettingsLeft(final PdfPCell cell) {
    cell.setPaddingRight(LEFT_CELL_PADDING_RIGHT);
    cell.setPaddingTop(0);
    cell.setPaddingBottom(LEFT_CELL_PADDING_BOTTOM);
    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    cell.setBorder(0);
  }

  /**
   * Apply default settings as used for the right block cells.
   * @param cell The cell to apply it to.
   */
  private static void applyDefaultBlockCellSettingsRight(final PdfPCell cell) {
    cell.setPaddingLeft(PAASharedConstants.DEFAULT_CELL_PADDING_LEFT);
    cell.setPaddingTop(PAASharedConstants.DEFAULT_CELL_PADDING_TOP);
    cell.setPaddingBottom(0);
    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    cell.setBorder(0);
    cell.setLeading(cell.getLeading() + 2, 1);
  }

  /**
   * Create a generic empty table with given relative widths to be used in the reports.
   * @param relativeWidths The relative widths to use when creating the empty table.
   * @param rightCellIsTextBlock Add extra padding to the top if true to align text properly.
   * @return {@link PDFSimpleTable}
   */
  private static PDFSimpleTable getDefaultBlockEmptyTable(final PDFRelativeWidths relativeWidths, final boolean rightCellIsTextBlock) {
    final PDFSimpleTable table = new PDFSimpleTable(relativeWidths);
    applyDefaultBlockCellSettingsRight(table.get().getDefaultCell());
    if (rightCellIsTextBlock) {
      table.get().getDefaultCell().setPaddingTop(2);
    }

    return table;
  }

  /**
   * Create a generic empty table with default relative widths to be used in the reports.
   * @param rightCellIsTextBlock Add extra padding to the top if true to align text properly.
   * @return {@link PDFSimpleTable}
   */
  public static PDFSimpleTable getDefaultBlockEmptyTable(final boolean rightCellIsTextBlock) {
    return getDefaultBlockEmptyTable(PAASharedConstants.DEFAULT_RELATIVE_WIDTH_TWOCELL_EMPTY_BLOCK, rightCellIsTextBlock);
  }

  /**
   * Get an image table where the image is fitted according to the width percentage given.
   * @param filename The image filename to put.
   * @param widthPercentage The percentage of the content part to use (page - margins and such already applied to the page).
   * @param paddingLeft Extra padding left to add to the image.
   * @return {@link PDFSimpleTable}
   */
  public static PDFSimpleTable getImageTableToFit(final Image image, final float widthPercentage, final float paddingLeft) {
    final PDFSimpleTable table = new PDFSimpleTable(1);

    table.setDefaultPadding(new Padding(Padding.DEFAULT_PADDING, Padding.DEFAULT_PADDING, paddingLeft, Padding.DEFAULT_PADDING));
    table.get().setWidthPercentage(widthPercentage);
    table.addImage(image);

    return table;
  }

  /**
   * Get formatted emission String.
   * @param locale The locale to use.
   * @param emission The emission to format.
   * @return Formatted emission String.
   */
  public static String getEmissionString(final Locale locale, final double emission) {
    return PDFUtils.getEmissionString(locale, emission, DEFAULT_DECIMALS_EMISSIONS,
        getText("paautils_ton_year", locale), getText("paautils_kg_year", locale));
  }

  public static String getText(final String key, final Locale locale) {
    return TextUtils.getText(key, locale);
  }
}
