/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.table.result.DepositionNatureDataTable.PDFExportAssessmentAreaInfoRowData;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportDepositionInfo;

/**
 * Data table as used in the PAA export containing deposition on nature areas.
 */
public class DepositionNatureDataTable extends DepositionBaseDataTable<PDFExportAssessmentAreaInfoRowData> {

  public DepositionNatureDataTable(final PAAContext paaContext, final List<PDFExportAssessmentAreaInfo> areas, final List<Column> columns) {
    super(paaContext.getLocale(), columns, paaContext);

    setData(getRowData(areas));
  }

  private List<PDFExportAssessmentAreaInfoRowData> getRowData(final List<PDFExportAssessmentAreaInfo> areas) {
    final List<PDFExportAssessmentAreaInfoRowData> rowDatas = new ArrayList<>();
    for (final PDFExportAssessmentAreaInfo info : areas) {
      rowDatas.add(new PDFExportAssessmentAreaInfoRowData(info));
    }
    return rowDatas;
  }

  @Override
  String getFirstHeaderTitle() {
    return getText("deposition_datatable_area");
  }

  class PDFExportAssessmentAreaInfoRowData implements DepositionBaseDataTableRowData {

    private final PDFExportAssessmentAreaInfo info;

    PDFExportAssessmentAreaInfoRowData(final PDFExportAssessmentAreaInfo info) {
      this.info = info;
    }

    @Override
    public String getName() {
      return info.getName();
    }

    @Override
    public PDFExportDepositionInfo getDepositionInfo() {
      return info.getDepositionInfo();
    }

    @Override
    public Set<DevelopmentRule> getRules() {
      return info.getShowRules();
    }

  }

}
