/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.pdf.marker.Marker;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 *
 */
public final class PAASourcesUtil {

  private static final Logger LOG = LoggerFactory.getLogger(PAASourcesUtil.class);

  private static final double BBOX_ALL_RESULTS_EXTRA_PADDING_FACTOR = 0.15d;

  private static final int MAINPAGE_MINIMUM_WIDTH_BBOX = 2500;
  private static final int MAINPAGE_MINIMUM_HEIGHT_BBOX = 2500;

  private PAASourcesUtil() {
    //util class
  }

  /**
   * Calculate the minimum BBox needed to fit all the sources.
   * @param sources The source to use.
   * @return BBox.
   * @throws AeriusException On validation errors.
   */
  public static BBox getBBoxAllSources(final List<EmissionSource> sources) throws AeriusException {
    BBox bboxAllSources = determineBBox(sources);

    if (bboxAllSources == null) {
      LOG.error("Invalid bbox, must be valid for variant with index");
      throw new AeriusException(Reason.INTERNAL_ERROR);
    } else {
      // Add extra padding to the sides.
      bboxAllSources = GeometryUtil.addPadding(bboxAllSources, BBOX_ALL_RESULTS_EXTRA_PADDING_FACTOR);
      bboxAllSources = GeometryUtil.ensureMinimumWidthHeight(bboxAllSources,
          MAINPAGE_MINIMUM_WIDTH_BBOX, MAINPAGE_MINIMUM_HEIGHT_BBOX);
    }
    return bboxAllSources;
  }

  /**
   * Return a source marker for given source.
   * @param source The source to return a marker for.
   * @return Source marker
   */
  public static Marker getMarker(final EmissionSource source) {
    return new Marker(source, Integer.toString(source.getId()), source.getSector().getProperties().getColor());
  }

  private static BBox determineBBox(final List<EmissionSource> sources) throws AeriusException {
    // Calculate the minimum BBox needed to fit all the sources.
    final List<WKTGeometry> geoms = new ArrayList<>();
    for (final EmissionSource source : sources) {
      geoms.add(source.getGeometry());
      //add routes to the bounding box.
      if (source instanceof MaritimeMooringEmissionSource) {
        for (final MooringMaritimeVesselGroup vesselGroup : ((MaritimeMooringEmissionSource) source).getEmissionSubSources()) {
          if (vesselGroup.getInlandRoute() != null) {
            geoms.add(vesselGroup.getInlandRoute().getGeometry());
          }
          for (final MaritimeRoute mtr : vesselGroup.getMaritimeRoutes()) {
            geoms.add(mtr.getRoute().getGeometry());
          }
        }
      } else if (source instanceof InlandMooringEmissionSource) {
        for (final InlandMooringVesselGroup vesselGroup : ((InlandMooringEmissionSource) source).getEmissionSubSources()) {
          for (final InlandMooringRoute imr : vesselGroup.getRoutes()) {
            geoms.add(imr.getRoute().getGeometry());
          }
        }
      }
    }
    return GeometryUtil.determineBBoxForGeometries(geoms);
  }

}
