/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.layer;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;
import nl.overheid.aerius.pdf.table.Padding;
import nl.overheid.aerius.shared.domain.info.NatureAreaDirective;
import nl.overheid.aerius.worker.util.ImageUtils;

/**
 * Deposition location legend table to be used with different deposition location blocks.
 */
public class DepositionLocationLegendTable {

  private static final String MARKER_HIGH_PROJECTCONTRIBUTION = "marker-high-projectcontribution.wmf";
  private static final String MARKER_HIGH_PROJECTCONTRIBUTION_NATURE = "marker-high-projectcontribution-nature.wmf";

  private static final int LEGEND_ICON_CELL_PADDING = 4;
  private static final int LEGEND_ICON_HEIGHT = 10;
  private static final int LEGEND_ICON_WIDTH = LEGEND_ICON_HEIGHT;

  private static final PDFRelativeWidths MARKER_RELATIVE_WIDTHS = new PDFRelativeWidths(1, 6, 1, 6, 1, 7, 3);
  private static final PDFRelativeWidths DIRECTIVE_RELATIVE_WIDTHS = new PDFRelativeWidths(2, 15);

  public static PDFSimpleTable get(final PAAContext context, final boolean comparison, final String maxNatureArea, final boolean includeDirectiveTypes)
      throws DocumentException, IOException {
    final PDFSimpleTable markerTable = new PDFSimpleTable(MARKER_RELATIVE_WIDTHS);
    markerTable.addImage(context.getImage(MARKER_HIGH_PROJECTCONTRIBUTION));
    final String highProjectContributionTextKey;
    if (maxNatureArea == null) {
      highProjectContributionTextKey =
          comparison ? "deposition_marker_highest_delta_projectcontribution_noarea" : "deposition_marker_highest_projectcontribution_noarea";
    } else {
      highProjectContributionTextKey =
          comparison ? "deposition_marker_highest_delta_projectcontribution" : "deposition_marker_highest_projectcontribution";
    }
    markerTable.add(MessageFormat.format(context.getText(highProjectContributionTextKey), maxNatureArea),
        PAASharedFonts.DEPOSITION_LEGEND_SMALLER);
    markerTable.addImage(context.getImage(MARKER_HIGH_PROJECTCONTRIBUTION_NATURE));
    markerTable.add(context.getText(
        comparison ? "deposition_marker_highest_delta_projectcontribution_nature" : "deposition_marker_highest_projectcontribution_nature"),
        PAASharedFonts.DEPOSITION_LEGEND_SMALLER);

    markerTable.addEmptyCell();

    if (includeDirectiveTypes) {
      markerTable.add(getDirectiveTypesLegendTable(context));
      markerTable.addEmptyCell();
    } else {
      markerTable.addEmptyCell();
      markerTable.addEmptyCell();
    }

    return markerTable;
  }

  private static PDFSimpleTable getDirectiveTypesLegendTable(final PAAContext context) {
    final PDFSimpleTable table = new PDFSimpleTable(DIRECTIVE_RELATIVE_WIDTHS);
    table.setDefaultPadding(new Padding(0, LEGEND_ICON_CELL_PADDING + 2, LEGEND_ICON_CELL_PADDING, LEGEND_ICON_CELL_PADDING));

    // color, legend text
    final Map<String, String> legendItems = new LinkedHashMap<>();
    legendItems.put("E0E030", getDirectiveLabel(context, NatureAreaDirective.HABITAT));
    legendItems.put("C3E1F5", getDirectiveLabel(context, NatureAreaDirective.BIRD));
    legendItems.put("CDD863", getDirectiveLabel(context, NatureAreaDirective.BIRD_AND_HABITAT));

    for (final Entry<String, String> legendItem : legendItems.entrySet()) {
      table.addImage(
          ImageUtils.getCircleImage(Math.round(LEGEND_ICON_HEIGHT * PAASharedConstants.QUALITY_SETTING), legendItem.getKey()),
          LEGEND_ICON_WIDTH,
          LEGEND_ICON_HEIGHT);
      table.add(legendItem.getValue(), PAASharedFonts.DEPOSITION_LEGEND);
    }

    return table;
  }

  private static String getDirectiveLabel(final PAAContext context, final NatureAreaDirective directive) {
    return context.getText("nature_area_directive", directive);
  }

}
