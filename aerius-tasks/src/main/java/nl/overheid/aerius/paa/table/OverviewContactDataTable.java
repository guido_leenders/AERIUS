/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;

/**
 * Datatable containing contact data.
 */
public class OverviewContactDataTable extends GenericThreeColumnDataTable {

  /**
   * Default constructor setting header and such.
   * @param paaContext The PAA context.
   */
  public OverviewContactDataTable(final PAAContext paaContext) {
    super(paaContext.getLocale(), new GenericThreeColumnDataRow(
        "base_contact_legal_entity",
        "base_contact_location",
        null));

    final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
    row.set(Column.COLUMN_1, normalize(paaContext.getMetaData().getCorporation()));
    row.set(Column.COLUMN_2, normalize(getAddress(paaContext.getMetaData())));
    addRow(row);
  }

  /**
   * Returns a readable string representation of the address.
   * @param metaData
   * @return
   */
  private String getAddress(final ScenarioMetaData metaData) {
    return (metaData.getStreetAddress() == null ? "" : (metaData.getStreetAddress() + ", "))
        + (metaData.getPostcode() == null ? "" : metaData.getPostcode())
        + (metaData.getCity() == null ? "" : (" " + metaData.getCity()));
  }
}
