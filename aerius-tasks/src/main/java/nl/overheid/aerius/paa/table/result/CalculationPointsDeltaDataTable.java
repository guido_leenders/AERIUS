/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.pdf.format.PDFFormatUtil;
import nl.overheid.aerius.pdf.table.DepositionComparisonPDFColumn;
import nl.overheid.aerius.pdf.table.DepositionPDFColumn;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.domain.deposition.PAACalculationPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Data table as used in the PAA export containing calculation point delta data.
 */
public class CalculationPointsDeltaDataTable extends CalculationPointsDataTable {

  private final List<PAACalculationPoint> secondData;

  /**
   * Constructor to fill headers and such.
   * @param locale The locale to use.
   * @param firstData Calculation points data of first situation.
   * @param secondData Calculation points data of second situation.
   */
  public CalculationPointsDeltaDataTable(final Locale locale, final List<PAACalculationPoint> firstData,
      final List<PAACalculationPoint> secondData) {
    super(locale, firstData);

    this.secondData = secondData;
  }

  @Override
  protected void addDepositionColumns(final Locale locale) {
    addColumn(new DepositionPDFColumn<PAACalculationPoint>(locale, Column.CURRENT_DEPOSITION) {

      @Override
      protected double getDeposition(final PAACalculationPoint data) {
        return  data.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION);
      }

    });
    addColumn(new DepositionPDFColumn<PAACalculationPoint>(locale, Column.PROPOSED_DEPOSITION) {

      @Override
      protected double getDeposition(final PAACalculationPoint data) {
        final PAACalculationPoint secondData = getMatchInSecond(data);
        return  secondData == null ? 0D : secondData.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION);
      }

    });
    addColumn(new DepositionComparisonPDFColumn<PAACalculationPoint>(locale, Column.COMPARISON) {

      @Override
      protected double getDepositionDelta(final PAACalculationPoint data) {
        final PAACalculationPoint secondData = getMatchInSecond(data);
        double result = -data.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION);
        if (secondData != null) {
          result += secondData.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION);
        }
        return result;
      }

    });
  }

  @Override
  protected void addDistanceColumn(final Locale locale) {
    addColumn(new TextPDFColumn<PAACalculationPoint>(locale, Column.DISTANCE_TO_CLOSEST_SOURCE) {

      @Override
      protected String getText(final PAACalculationPoint data) {
        final double dcs = data.getDistanceToClosestSource();
        final PAACalculationPoint matchInSecond = getMatchInSecond(data);
        final double distance = matchInSecond == null ? dcs : Math.min(dcs, matchInSecond.getDistanceToClosestSource());
        return PDFFormatUtil.formatDistance(getLocale(), distance);
      }

    });
  }

  private PAACalculationPoint getMatchInSecond(final PAACalculationPoint data) {
    PAACalculationPoint calculationPoint = null;
    for (final PAACalculationPoint secondItem : secondData) {
      if (data.getId() == secondItem.getId() && data.getX() == secondItem.getX()
          && data.getY() == secondItem.getY()) {
        calculationPoint = secondItem;
      }
    }
    return calculationPoint;
  }

}
