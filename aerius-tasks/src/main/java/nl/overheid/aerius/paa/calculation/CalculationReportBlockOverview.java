/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.calculation;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.paa.block.ReportBlockOverview;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.table.GenericThreeColumnDataTable;
import nl.overheid.aerius.paa.table.OverviewContactDataTable;
import nl.overheid.aerius.paa.table.OverviewPermitCalculationRadiusTypeDataTable;
import nl.overheid.aerius.paa.table.OverviewTemporaryProjectDataTable;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * PAA Report Block: Overview for calculations.
 */
public class CalculationReportBlockOverview extends ReportBlockOverview<PAAContext> {

  public CalculationReportBlockOverview(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  protected PAABaseTable<?> getContactTable(final PAAContext paaContext) {
    return new OverviewContactDataTable(paaContext);
  }

  @Override
  protected List<PAABaseTable<?>> getActivityTables() {
    final List<PAABaseTable<?>> tables = new ArrayList<>();
    tables.add(new ActivityFirstRowDataTable());
    tables.add(new ActivitySecondRowDataTable());
    if (paaContext.getCalculatedScenario().getOptions().isTemporaryProjectImpact()) {
      tables.add(new OverviewTemporaryProjectDataTable(paaContext, set1.getYear()));
    }
    if (paaContext.getCalculatedScenario().getOptions().isPermitCalculationRadiusImpact()) {
      tables.add(new OverviewPermitCalculationRadiusTypeDataTable(paaContext));
    }
    return tables;
  }

  class ActivityFirstRowDataTable extends GenericThreeColumnDataTable {

    ActivityFirstRowDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_description", "base_activity_reference", null));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, normalize(paaContext.getMetaData().getProjectName()));
      row.set(Column.COLUMN_2, normalize(paaContext.getMetaData().getReference()));
      row.set(Column.COLUMN_3, normalize(set1.getSources().getName()));
      addRow(row);
    }

  }

  class ActivitySecondRowDataTable extends GenericThreeColumnDataTable {

    ActivitySecondRowDataTable() {
      super(paaContext.getLocale(), new GenericThreeColumnDataRow(
          "base_activity_date_calculation", "base_activity_calculation_year", "base_activity_calculation_options"));

      final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
      row.set(Column.COLUMN_1, paaContext.getFormattedCreationData(DATE_CALCULATION_FORMAT));
      row.set(Column.COLUMN_2, set1.getYear());

      final CalculationType calculationType = paaContext.getCalculatedScenario().getOptions().getCalculationType();
      final String calculationText;
      switch (calculationType) {
      case PAS:
        calculationText = paaContext.getText("base_activity_calculation_permit_text");
        break;
      case RADIUS:
        calculationText = MessageFormat.format(paaContext.getText("base_activity_calculation_options_text"),
            PDFUtils.getNumberFormatOne(paaContext.getLocale()).format(
                paaContext.getCalculatedScenario().getOptions().getCalculateMaximumRange() / SharedConstants.M_TO_KM) + "km");
        break;
      case CUSTOM_POINTS:
        calculationText = paaContext.getText("base_activity_calculation_custom_points_text");
        break;
      default:
        throw new IllegalArgumentException("Unsupported calculationType encountered: " + calculationType);
      }
      row.set(Column.COLUMN_3, calculationText);
      addRow(row);
    }

  }

}
