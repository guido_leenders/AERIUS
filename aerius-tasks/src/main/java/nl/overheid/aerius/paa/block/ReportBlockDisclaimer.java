/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.block;

import com.lowagie.text.DocumentException;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.pdf.PDFDocumentWrapper;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;

/**
 * PAA Report Block: Disclaimer.
 */
public class ReportBlockDisclaimer extends PAAReportBlock<PAAContext> {

  public ReportBlockDisclaimer(final PDFDocumentWrapper pdfDocument, final PAAContext paaContext) {
    super(pdfDocument, paaContext);
  }

  @Override
  public void generate() throws DocumentException {
    final PDFSimpleTable disclaimerTable = PAAUtil.getDefaultBlockEmptyTable(true);
    PAAUtil.addBlockHeaderCell(disclaimerTable, paaContext.getText("disclaimer_title"));
    final String disclaimerText = getDisclaimerText();
    disclaimerTable.add(disclaimerText, PAASharedFonts.DEFAULT_TEXT);
    pdfDocument.addElement(disclaimerTable);

    pdfDocument.addPadding(PADDING_BETWEEN_BLOCKS);
  }

  private String getDisclaimerText() {
    String disclaimer;
    switch (paaContext.getReportType()) {
    case DECREE:
      disclaimer = paaContext.getText("disclaimer_text_decree");
      break;
    case DETAIL_DECREE:
      disclaimer = paaContext.getText("disclaimer_text_detail_decree");
      break;
    default:
      disclaimer = paaContext.getText("disclaimer_text");
      break;
    }
    return disclaimer;
  }

}
