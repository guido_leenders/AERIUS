/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import nl.overheid.aerius.paa.base.PAAFrontPage;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAAFlowState;


/**
 *
 */
public class DetailDecreePAAPageEventHelper extends DecreePAAPageEventHelper {

  protected DetailDecreePAAPageEventHelper(final PAAFlowState flowState, final DecreePAAContext paaContext) {
    super(flowState, paaContext);
  }

  @Override
  protected PAAFrontPage getFrontPage(final PAAContext paaContext) {
    return new DetailDecreePAAFrontPage(paaContext);
  }

}
