/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.layer;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.pdf.font.PDFFonts;
import nl.overheid.aerius.pdf.table.PDFRelativeWidths;
import nl.overheid.aerius.pdf.table.PDFSimpleTable;

/**
 * Deposition legend table to be used with different deposition tables.
 */
public final class DepositionLegendTable {

  private static final String ICON_CHECK_NOT_EXCEEDING_SPACE = "check_not_exceeding_space.wmf";
  private static final String ICON_CHECK_EXCEEDING_SPACE = "check_exceeding_space.wmf";

  private static final int PADDING_BETWEEN_LEGEND_AND_FULLTEXT = 5;

  private static final PDFRelativeWidths LEGEND_RELATIVE_WIDTHS = new PDFRelativeWidths(3.5F, 42F, 15F, 39.5F);

  private DepositionLegendTable() {
    // Not to be constructed
  }

  public static PDFSimpleTable get(final PAAContext context, final boolean showRules) throws DocumentException, IOException {
    final PDFSimpleTable table = new PDFSimpleTable(LEGEND_RELATIVE_WIDTHS);

    if (showRules) {
      addRules(table, context);
    }
    addFootNote(table, context);

    return table;
  }

  private static Map<String, String> getItemMap(final PAAContext context) {
    final Map<String, String> items = new LinkedHashMap<>();

    items.put(ICON_CHECK_NOT_EXCEEDING_SPACE, context.getText("deposition_legend_check_not_exceeding_space"));
    items.put(ICON_CHECK_EXCEEDING_SPACE, context.getText("deposition_legend_check_exceeding_space"));

    return items;
  }

  private static void addRules(final PDFSimpleTable table, final PAAContext context) throws DocumentException, IOException {
    final PDFFonts legendFont = PAASharedFonts.DEPOSITION_LEGEND;

    final Map<String, String> items = getItemMap(context);

    for (final Entry<String, String> legendItem : items.entrySet()) {
      table.addImage(context.getImage(legendItem.getKey()));

      final PdfPCell explainTextCell = PDFSimpleTable.getNewCellWithDefaultSettings();
      explainTextCell.setColspan(2);
      explainTextCell.setPhrase(new Phrase(legendItem.getValue(), legendFont.get()));
      table.add(explainTextCell);
      table.addEmptyCell();
    }
  }

  private static void addFootNote(final PDFSimpleTable table, final PAAContext context) {
      table.addEmptyCell(PADDING_BETWEEN_LEGEND_AND_FULLTEXT);
      table.addEmptyCell();
      table.addEmptyCell();
      table.addEmptyCell();

      final PDFFonts legendFont = PAASharedFonts.DEPOSITION_LEGEND;

      final PdfPCell exceedingExplanationTextCell = PDFSimpleTable.getNewCellWithDefaultSettings();
      exceedingExplanationTextCell.setColspan(2);
      exceedingExplanationTextCell.setPhrase(new Phrase(context.getText("deposition_legend_exceeding_explanation"), legendFont.get()));

      table.add("*", legendFont);
      table.add(exceedingExplanationTextCell);
      table.addEmptyCell();
  }

}
