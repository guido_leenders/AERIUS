/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.common;

import nl.overheid.aerius.shared.domain.ProductType;

/** Supported report types. */
public enum PAAReportType {

  /** 'Eigen berekening'. */
  CALCULATION(ProductType.CALCULATOR),

//  /** 'Vergunningsbijlage - Benodigde ontwikkelingsruimte'. */
//  PERMIT_DEVELOPMENT_SPACES(ProductType.CALCULATOR),
//
//  /** 'Vergunningsbijlage - Bepaling projecteffect'. */
//  PERMIT_DEMAND(ProductType.CALCULATOR),

  /** Melding! No quotes needed. */
  MELDING(ProductType.REGISTER),

  /** 'Bijlage bij Besluit'. */
  DECREE(ProductType.REGISTER),

  /** Detailed report for 'Bijlage bij Besluit'. */
  DETAIL_DECREE(ProductType.REGISTER);

  private final ProductType productType;

  private PAAReportType(final ProductType productType) {
    this.productType = productType;
  }

  public ProductType getProductType() {
    return productType;
  }

}
