/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.source;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;

import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.table.PAABaseTable;
import nl.overheid.aerius.pdf.table.ImagePDFColumn;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.PhrasePDFColumn;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.AbstractCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmFodderMeasure;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.worker.util.FarmUtil;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Data table as used in the PAA export containing emission data for a farm source.
 */
public class FarmSourceDataTable extends PAABaseTable<FarmSourceDataTable.FarmLodgingRowData> {

  enum Column implements PDFColumnProperties {
    ANIMAL("animal", 2.5F),
    RAV_CODE("RAVcode", 4),
    DESCRIPTION("description", 16),
    AMOUNT("amount", 4),
    SUBSTANCE("substance", 3),
    EMISSION_FACTOR("emissionFactor", 5),
    EMISSION("emission", 6);

    private final String headerKey;
    private final float relativeWidth;

    private Column(final String headerKey, final float relativeWidth) {
      this.headerKey = "emission_datatable_" + headerKey;
      this.relativeWidth = relativeWidth;
    }

    @Override
    public String getHeaderI18nKey() {
      return headerKey;
    }

    @Override
    public float getRelativeWidth() {
      return relativeWidth;
    }
  }

  private static final int SCALE_ICONS_TO_PERCENT = 50;
  private static final int EMISSION_FACTOR_DECIMALS = 3;

  private static final Substance FARM_SUBSTANCE = Substance.NH3;
  private static final EmissionValueKey EVK_WITHOUT_YEAR = new EmissionValueKey(FARM_SUBSTANCE);

  /**
   * Constructor to set header + widths.
   * @param year The year to use.
   * @param farmEmissionValues The FarmEmissionValues to make table for.
   * @param locale The locale to use.
   */
  public FarmSourceDataTable(final int year, final FarmEmissionSource farmEmissionValues, final Locale locale) {
    super(locale);

    setData(getRowData(year, farmEmissionValues));

    addColumns(locale);
  }

  private void addColumns(final Locale locale) {
    addAnimalColumn();
    addRavCodeColumn(locale);
    addDescriptionColumn(locale);
    addAmountColumn(locale);
    addSubstanceColumn(locale);
    addEmissionFactorColumn();
    addEmissionColumn();
  }

  private void addAnimalColumn() {
    addColumn(new ImagePDFColumn<FarmLodgingRowData>(Column.ANIMAL) {

      @Override
      protected Image getImage(final FarmLodgingRowData data) {
        return data.getIcon();
      }
    });
  }

  private void addRavCodeColumn(final Locale locale) {
    addColumn(new TextPDFColumn<FarmLodgingRowData>(locale, Column.RAV_CODE) {

      @Override
      protected String getText(final FarmLodgingRowData data) {
        return data.getRavCode();
      }
    });
  }

  private void addDescriptionColumn(final Locale locale) {
    addColumn(new TextPDFColumn<FarmLodgingRowData>(locale, Column.DESCRIPTION) {

      @Override
      protected String getText(final FarmLodgingRowData data) {
        return data.getDescription();
      }
    });
  }

  private void addAmountColumn(final Locale locale) {
    addColumnRightAligned(new TextPDFColumn<FarmLodgingRowData>(locale, Column.AMOUNT) {

      @Override
      protected String getText(final FarmLodgingRowData data) {
        return data.getAmount();
      }
    });
  }

  private void addSubstanceColumn(final Locale locale) {
    addColumn(new TextPDFColumn<FarmLodgingRowData>(locale, Column.SUBSTANCE) {

      @Override
      protected String getText(final FarmLodgingRowData data) {
        return data.getSubstance();
      }
    });
  }

  private void addEmissionFactorColumn() {
    addColumnRightAligned(new PhrasePDFColumn<FarmLodgingRowData>(Column.EMISSION_FACTOR) {

      @Override
      protected Phrase getPhrase(final FarmLodgingRowData data) {
        final Font font = data.isConstrained() ? PAASharedFonts.TABLE_CELL_STRIKETHRU.get() : PAASharedFonts.TABLE_CELL.get();
        return new Phrase(data.getEmissionFactor(), font);
      }
    });
  }

  private void addEmissionColumn() {
    addColumnRightAligned(new PhrasePDFColumn<FarmLodgingRowData>(Column.EMISSION) {

      @Override
      protected Phrase getPhrase(final FarmLodgingRowData data) {
        final Font font = data.isFinalEmission() && !data.isConstrained()
            ? PAASharedFonts.TABLE_CELL.get()
            : PAASharedFonts.TABLE_CELL_STRIKETHRU.get();
        final String emission = data.getEmission() == null ? null : getEmissionString(data.getEmission());
        return new Phrase(emission, font);
      }
    });
  }

  private Image getLodgingIcon(final String name) {
    return getImage(FarmUtil.getAnimalImageFilename(name), SCALE_ICONS_TO_PERCENT);
  }

  private List<FarmLodgingRowData> getRowData(final int year, final FarmEmissionSource farmEmissionValues) {
    final List<FarmLodgingRowData> rowDatas = new ArrayList<>();
    final EmissionValueKey evk = new EmissionValueKey(year, FARM_SUBSTANCE);

    for (final FarmLodgingEmissions lodging : farmEmissionValues.getEmissionSubSources()) {
      rowDatas.addAll(getRowData(evk, lodging));
    }

    return rowDatas;
  }

  private List<FarmLodgingRowData> getRowData(final EmissionValueKey evk, final FarmLodgingEmissions lodging) {
    final List<FarmLodgingRowData> rowDatas = new ArrayList<>();

    //ensure base row is the farm itself, with basic emission values.
    double emission = lodging.getEmission(evk);

    if (lodging instanceof FarmLodgingStandardEmissions) {
      final FarmLodgingStandardEmissions standardEmissions = (FarmLodgingStandardEmissions) lodging;
      emission = standardEmissions.isEmissionFactorConstrained()
          ? standardEmissions.getUnconstrainedFlatEmission(evk)
          : standardEmissions.getFlatEmission(evk);
      rowDatas.add(getStandardData(standardEmissions, emission));

      //add constrained
      if (standardEmissions.isEmissionFactorConstrained()) {
        rowDatas.add(getConstrainedData(standardEmissions));
      }

      //next keep on adding additionals and reductive so emission is updated accordingly.
      for (final FarmAdditionalLodgingSystem additionalSystem : standardEmissions.getLodgingAdditional()) {
        emission = standardEmissions.getEmissionUpToAdditional(evk, additionalSystem);
        rowDatas.add(getAdditionalSystemData(additionalSystem, emission));
      }
      for (final FarmReductiveLodgingSystem reductiveSystem : standardEmissions.getLodgingReductive()) {
        emission = standardEmissions.getEmissionUpToReductive(evk, reductiveSystem);
        rowDatas.add(getReductiveSystemData(reductiveSystem, emission));
      }

      for (int i = 0; i < standardEmissions.getFodderMeasures().size(); ++i) {
        final FarmFodderMeasure fodderMeasure = standardEmissions.getFodderMeasures().get(i);
        emission = standardEmissions.getEmission(evk); // contains the total, which is okay as we show it only if it's the final row
        final double reductionFactorCombined = standardEmissions.getReductionFactorCombinedFodderMeasures(evk);
        rowDatas.add(getFodderMeasureData(fodderMeasure, reductionFactorCombined, emission, standardEmissions.getFodderMeasures().size() > 1));
      }
    } else if (lodging instanceof FarmLodgingCustomEmissions) {
      rowDatas.add(getCustomData((FarmLodgingCustomEmissions) lodging, emission));
    }
    //ensure last line is designated as final.
    if (!rowDatas.isEmpty()) {
      rowDatas.get(rowDatas.size() - 1).setFinalEmission(true);
    }
    return rowDatas;
  }

  protected String formatEmissionFactor(final double number) {
    return PDFUtils.getNumberFormat(getLocale(), EMISSION_FACTOR_DECIMALS).format(number);
  }

  private FarmLodgingRowData getDefaultData(final double emission) {
    final FarmLodgingRowData data = new FarmLodgingRowData(emission);
    data.setSubstance(getText("emission_datatable_NH3"));
    return data;
  }

  private FarmLodgingRowData getStandardData(final FarmLodgingStandardEmissions lodging, final double emission) {
    final FarmLodgingRowData data = getDefaultData(emission);
    data.setIcon(getLodgingIcon(lodging.getCategory().getName()));
    setRavAndDescription(data, lodging.getCategory(), lodging.getSystemDefinition());
    data.setAmount(lodging.getAmount());
    data.setConstrained(lodging.isEmissionFactorConstrained());
    final String emissionFactor;
    if (lodging.isEmissionFactorConstrained()) {
      emissionFactor = formatEmissionFactor(lodging.getUnconstrainedEmissionFactor(EVK_WITHOUT_YEAR))
          + getText("emission_datatable_Animal_FootNoteRef");
    } else {
      emissionFactor = formatEmissionFactor(lodging.getCategory().getEmissionFactor());
    }
    data.setEmissionFactor(emissionFactor);
    return data;
  }

  private FarmLodgingRowData getCustomData(final FarmLodgingCustomEmissions lodging, final double emission) {
    final FarmLodgingRowData data = getDefaultData(emission);
    data.setIcon(getLodgingIcon(null));
    data.setRavCode(getText("emission_datatable_custom_code"));
    data.setDescription(lodging.getDescription());
    data.setAmount(lodging.getAmount());
    data.setEmissionFactor(lodging.getEmissionFactors().getEmission(EVK_WITHOUT_YEAR));
    return data;
  }

  private FarmLodgingRowData getConstrainedData(final FarmLodgingStandardEmissions lodging) {
    final FarmLodgingRowData data = getDefaultData(lodging.getFlatEmission(EVK_WITHOUT_YEAR));
    data.setEmissionFactor(lodging.getEmissionFactor(EVK_WITHOUT_YEAR));
    return data;
  }

  private FarmLodgingRowData getAdditionalSystemData(final FarmAdditionalLodgingSystem additionalSystem, final double emission) {
    final FarmLodgingRowData data = getDefaultData(emission);
    setRavAndDescription(data, additionalSystem.getCategory(), additionalSystem.getSystemDefinition());
    data.setAmount(additionalSystem.getAmount());
    data.setEmissionFactor(additionalSystem.getCategory().getEmissionFactor());
    return data;
  }

  private FarmLodgingRowData getReductiveSystemData(final FarmReductiveLodgingSystem reductiveSystem, final double emission) {
    final FarmLodgingRowData data = getDefaultData(emission);
    setRavAndDescription(data, reductiveSystem.getCategory(), reductiveSystem.getSystemDefinition());
    return data;
  }

  private FarmLodgingRowData getFodderMeasureData(final FarmFodderMeasure fodderMeasure, final double reductionFactorCombined,
      final double emission, final boolean hasMultipleFodderMeasures) {
    final FarmFodderMeasureLodgingSystemRowData data = new FarmFodderMeasureLodgingSystemRowData(emission);
    data.setSubstance(getText("emission_datatable_NH3"));
    data.setRavCode(fodderMeasure.getCategory().getName());
    data.setDescription(fodderMeasure.getCategory().getDescription());
    final String additionalString =
        getText(hasMultipleFodderMeasures ? "emission_datatable_emissionreduction_combined" : "emission_datatable_emissionreduction");
    data.setAdditionalDescription(MessageFormat.format(additionalString, reductionFactorCombined * SharedConstants.PERCENTAGE_TO_FRACTION));
    return data;
  }

  private void setRavAndDescription(final FarmLodgingRowData data, final AbstractCategory category, final FarmLodgingSystemDefinition systemDefinition) {
    data.setRavCode(category.getName());
    final StringBuilder description = new StringBuilder(category.getDescription());
    if (systemDefinition != null) {
      description.append(" (");
      description.append(systemDefinition.getDescription());
      description.append(')');
    }
    data.setDescription(description.toString());
  }

  class FarmLodgingRowData {

    private Double emission;
    private boolean finalEmission;
    private Image icon;
    private String ravCode;
    private String description;
    private String amount;
    private String substance;
    private String emissionFactor;
    private boolean constrained;

    FarmLodgingRowData(final double emission) {
      this.emission = emission;
    }

    public Double getEmission() {
      return emission;
    }

    public void setEmission(final Double emission) {
      this.emission = emission;
    }

    public boolean isFinalEmission() {
      return finalEmission;
    }

    public void setFinalEmission(final boolean finalEmission) {
      this.finalEmission = finalEmission;
    }

    public Image getIcon() {
      return icon;
    }

    public void setIcon(final Image icon) {
      this.icon = icon;
    }

    public String getRavCode() {
      return ravCode;
    }

    public void setRavCode(final String ravCode) {
      this.ravCode = ravCode;
    }

    public String getAmount() {
      return amount;
    }

    public void setAmount(final String amount) {
      this.amount = amount;
    }

    public void setAmount(final int amount) {
      this.amount = formatInt(amount);
    }

    public String getSubstance() {
      return substance;
    }

    public void setSubstance(final String substance) {
      this.substance = substance;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(final String description) {
      this.description = description;
    }

    public String getEmissionFactor() {
      return emissionFactor;
    }

    public void setEmissionFactor(final String emissionFactor) {
      this.emissionFactor = emissionFactor;
    }

    public void setEmissionFactor(final double emissionFactor) {
      this.emissionFactor = formatEmissionFactor(emissionFactor);
    }

    public boolean isConstrained() {
      return constrained;
    }

    public void setConstrained(final boolean constrained) {
      this.constrained = constrained;
    }

  }

  class FarmFodderMeasureLodgingSystemRowData extends FarmLodgingRowData {

    private String additionalDescription;

    FarmFodderMeasureLodgingSystemRowData(final double emission) {
      super(emission);
    }

    @Override
    public Double getEmission() {
      return isFinalEmission() ? super.getEmission() : null;
    }

    @Override
    public String getSubstance() {
      return isFinalEmission() ? super.getSubstance() : null;
    }

    public String getAdditionalDescription() {
      return additionalDescription;
    }

    public void setAdditionalDescription(final String additionalDescription) {
      this.additionalDescription = additionalDescription;
    }

    @Override
    public String getDescription() {
      String description = super.getDescription();
      if (isFinalEmission()) {
        description += additionalDescription;
      }
      return description;
    }

  }
}
