/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table.result;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.table.result.DepositionHabitatsDataTable.PDFExportHabitatInfoRowData;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportAssessmentAreaInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportDepositionInfo;
import nl.overheid.aerius.shared.domain.info.pdf.PDFExportHabitatInfo;

/**
 * Data table as used in the PAA export containing deposition on habitat types.
 */
public class DepositionHabitatsDataTable extends DepositionBaseDataTable<PDFExportHabitatInfoRowData> {

  public DepositionHabitatsDataTable(final Locale locale, final PAAContext paaContext, final PDFExportAssessmentAreaInfo areaInfo,
      final boolean showRulesColumn) {
    super(locale, getColumns(paaContext, showRulesColumn), paaContext);

    setData(getRowData(areaInfo));
  }

  private static List<Column> getColumns(final PAAContext paaContext, final boolean showRulesColumn) {
    final List<Column> columns = new ArrayList<>();
    columns.add(Column.NAME);
    if (paaContext.isComparison()) {
      columns.add(Column.CURRENT_DEMAND_FOR_MAX_DELTA_DEMAND);
      columns.add(Column.PROPOSED_DEMAND_FOR_MAX_DELTA_DEMAND);
      columns.add(Column.MAX_DELTA_DEMAND);
    } else {
      columns.add(Column.MAX_PROPOSED_DEMAND);
    }
    columns.add(Column.MAX_DEMAND_ONLY_EXCEEDING);
    if (showRulesColumn) {
      columns.add(Column.RULES);
    }

    return columns;
  }

  private List<PDFExportHabitatInfoRowData> getRowData(final PDFExportAssessmentAreaInfo areaInfo) {
    final List<PDFExportHabitatInfoRowData> rowDatas = new ArrayList<>();
    for (final PDFExportHabitatInfo habitatInfo : areaInfo.getHabitatInfos()) {
      rowDatas.add(new PDFExportHabitatInfoRowData(habitatInfo));
    }
    return rowDatas;
  }

  @Override
  String getFirstHeaderTitle() {
    return getText("deposition_datatable_habitat");
  }

  class PDFExportHabitatInfoRowData implements DepositionBaseDataTableRowData {

    private final PDFExportHabitatInfo habitatInfo;

    PDFExportHabitatInfoRowData(final PDFExportHabitatInfo habitatInfo) {
      this.habitatInfo = habitatInfo;
    }

    @Override
    public String getName() {
      return habitatInfo.getCode() + " " + habitatInfo.getName();
    }

    @Override
    public PDFExportDepositionInfo getDepositionInfo() {
      return habitatInfo.getDepositionInfo();
    }

    @Override
    public Set<DevelopmentRule> getRules() {
      return habitatInfo.getShowRules();
    }

  }

}
