/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.decree;

import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.user.Authority;

/**
 *
 */
public class DecreeInformation {

  private Authority authority;
  private Sector sector;
  private RequestState requestState;
  private SegmentType segment;
  private DossierAuthorityKey projectKey;
  private String parentProjectName;

  public Authority getAuthority() {
    return authority;
  }

  public void setAuthority(final Authority authority) {
    this.authority = authority;
  }

  public Sector getSector() {
    return sector;
  }

  public void setSector(final Sector sector) {
    this.sector = sector;
  }

  public RequestState getRequestState() {
    return requestState;
  }

  public void setRequestState(final RequestState requestState) {
    this.requestState = requestState;
  }

  public SegmentType getSegment() {
    return segment;
  }

  public void setSegment(final SegmentType segment) {
    this.segment = segment;
  }

  public DossierAuthorityKey getProjectKey() {
    return projectKey;
  }

  public void setProjectKey(final DossierAuthorityKey projectKey) {
    this.projectKey = projectKey;
  }

  public String getParentProjectName() {
    return parentProjectName;
  }

  public void setParentProjectName(final String parentProjectName) {
    this.parentProjectName = parentProjectName;
  }

}
