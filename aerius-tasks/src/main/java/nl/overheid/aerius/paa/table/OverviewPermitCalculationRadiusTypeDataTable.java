/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import nl.overheid.aerius.paa.common.PAAContext;

/**
 * Rows and columns to show Priority project radius type.
 */
public class OverviewPermitCalculationRadiusTypeDataTable extends GenericThreeColumnDataTable {

  public OverviewPermitCalculationRadiusTypeDataTable(final PAAContext paaContext) {
    super(paaContext.getLocale(), new GenericThreeColumnDataRow(
        "base_activity_calculation_radius",
        null,
        null));

    final GenericThreeColumnDataRow row = new GenericThreeColumnDataRow();
    // get translations
    row.set(Column.COLUMN_1, paaContext.getCalculatedScenario().getOptions().getPermitCalculationRadiusType().getName());
    addRow(row);
  }

}
