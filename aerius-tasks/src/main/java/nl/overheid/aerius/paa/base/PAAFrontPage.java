/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.base;

import java.io.IOException;
import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import com.lowagie.text.Anchor;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfWriter;

import nl.overheid.aerius.paa.common.PAAAnchors;
import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.paa.common.PAASharedConstants;
import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.pdf.PDFConstants;
import nl.overheid.aerius.pdf.PDFWriterUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 *
 */
public class PAAFrontPage {

  private static final String LOGO_FILENAME = "logo_frontpage_{0}.wmf";
  private static final int LOGO_HEIGHT = 210;
  private static final float LOGO_PADDINGTOP = 65;
  private static final float LOGO_PADDINGLEFT_FROM_CENTER = 30;
  private static final float LOGO_POSX = PAASharedConstants.PAGESIZE.getWidth() / 2 + LOGO_PADDINGLEFT_FROM_CENTER;
  private static final float LOGO_POSY = PAASharedConstants.PAGESIZE.getHeight() - LOGO_PADDINGTOP;
  private static final String LOGO_FIRSTCHAR_FILENAME = "logo_frontpage_firstchar.wmf";
  private static final int LOGO_FIRSTCHAR_HEIGHT = 310;
  private static final float LOGO_FIRSTCHAR_POSX = 0;
  private static final float LOGO_FIRSTCHAR_POSY_OFFSET = 148;
  private static final float LOGO_FIRSTCHAR_POSY = PAASharedConstants.PAGESIZE.getHeight() / 2 - LOGO_FIRSTCHAR_POSY_OFFSET;

  private static final int FULLTEXT_PADDINGLEFT_FROM_CENTER = 25;
  private static final int FULLTEXT_PADDINGRIGHT = 40;
  private static final int FULLTEXT_PADDINGTOP = 100;
  private static final int FULLTEXT_PADDINGBOTTOM = 110;
  private static final float FULLTEXT_LLX = PAASharedConstants.PAGESIZE.getWidth() / 2
      + FULLTEXT_PADDINGLEFT_FROM_CENTER;
  private static final float FULLTEXT_LLY = FULLTEXT_PADDINGBOTTOM;
  private static final float FULLTEXT_URX = PAASharedConstants.PAGESIZE.getWidth() - FULLTEXT_PADDINGRIGHT;
  private static final float FULLTEXT_URY = PAASharedConstants.PAGESIZE.getHeight() - FULLTEXT_PADDINGTOP;

  private static final float EXTRATEXT_LLX = FULLTEXT_LLX;
  private static final float EXTRATEXT_LLY = 20;
  private static final float EXTRATEXT_URX = FULLTEXT_URX;
  private static final float EXTRATEXT_URY = FULLTEXT_LLY;

  private static final String INDEX_BULLET_FILENAME = "frontpage_index_bullet.png";
  private static final float INDEX_BULLET_IMAGE_SCALE = 8;
  private static final float INDEX_STARTPOS_X = 50;
  private static final float INDEX_STARTPOS_Y_OFFSET = 70;
  private static final float INDEX_STARTPOS_Y = PAASharedConstants.PAGESIZE.getHeight() / 2 - LOGO_HEIGHT / 2 - INDEX_STARTPOS_Y_OFFSET;
  private static final float INDEX_PADDING_BETWEEN_LINES = 16;

  private static final int WATERMARK_HEIGHT = 316;
  private static final int WATERMARK_WIDTH = 153;
  private static final float WATERMARK_RELATIVE_POSX = 0.5f;
  private static final float WATERMARK_POSX = PAASharedConstants.PAGESIZE.getWidth() * WATERMARK_RELATIVE_POSX - WATERMARK_WIDTH / 2 + 1;
  private static final float WATERMARK_RELATIVE_POSY = 0.25f;
  private static final float WATERMARK_POSY = PAASharedConstants.PAGESIZE.getHeight() * WATERMARK_RELATIVE_POSY - WATERMARK_HEIGHT / 2;

  protected final PAAContext paaContext;

  /**
   *
   */
  protected PAAFrontPage(final PAAContext paaContext) {
    this.paaContext = paaContext;
  }

  void addFrontPageContent(final PdfWriter writer) throws DocumentException, IOException {
    // Add AERIUS logo on top.
    addLogoTopRight(writer);

    // Add the first character of AERIUS image to the left.
    addLogoMiddleLeft(writer);

    // Add a specific concept sticker for the frontpage if needed.
    addWaterMark(writer);

    // Main frontpage information on the right side.
    addMainText(writer);

    // Some extra information to the bottom right side.
    addExtraText(writer);

    // Add the index.
    addIndexBlock(writer, INDEX_STARTPOS_Y);
  }

  private void addLogoTopRight(final PdfWriter writer) throws DocumentException, IOException {
    final Image logoImage =
        PDFUtils.getImage(
            paaContext.getResourcesDir() + MessageFormat.format(LOGO_FILENAME, paaContext.getPmf().getProductType().name().toLowerCase()),
            paaContext.getLocale());
    logoImage.scaleToFit(LOGO_HEIGHT, PAASharedConstants.PAGESIZE.getWidth());
    PDFWriterUtil.addImage(writer, logoImage, LOGO_POSX, LOGO_POSY);
  }

  private void addLogoMiddleLeft(final PdfWriter writer) throws DocumentException, IOException {
    final Image logoFirstcharImage = PDFUtils.getImage(paaContext.getResourcesDir() + LOGO_FIRSTCHAR_FILENAME, paaContext.getLocale());
    logoFirstcharImage.scaleToFit(LOGO_FIRSTCHAR_HEIGHT, PAASharedConstants.PAGESIZE.getWidth());
    PDFWriterUtil.addImage(writer, logoFirstcharImage, LOGO_FIRSTCHAR_POSX, LOGO_FIRSTCHAR_POSY);
  }

  private void addWaterMark(final PdfWriter writer) throws DocumentException, IOException {
    if (paaContext.getWatermark() != null) {
      final Image conceptStickerImage =
          PDFUtils.getImage(paaContext.getResourcesDir() + paaContext.getWatermark().getFrontpageFilename(), paaContext.getLocale());
      conceptStickerImage.scaleToFit(WATERMARK_HEIGHT, WATERMARK_WIDTH);
      PDFWriterUtil.addImage(writer, conceptStickerImage, WATERMARK_POSX, WATERMARK_POSY);
    }
  }

  private void addMainText(final PdfWriter writer) throws DocumentException {
    final String fullMessage =
      paaContext.getText(paaContext.isCustomPointsCalculation() ? "frontpage_fulltext_calculationpoints" : "frontpage_fulltext",
        paaContext.getReportType());
    if (fullMessage == null) {
      throw new IllegalArgumentException("Unhandled reportType in PAAFrontPage. Missing implementation.");
    }

    final ColumnText fullText = new ColumnText(writer.getDirectContent());
    fullText.addText(new Phrase(fullMessage, PAASharedFonts.FRONTPAGE_FULL.get()));
    fullText.setSimpleColumn(FULLTEXT_LLX, FULLTEXT_LLY, FULLTEXT_URX,
        FULLTEXT_URY);
    fullText.go();
  }

  private void addExtraText(final PdfWriter writer) throws DocumentException {
    // Some extra information to the bottom right side.
    final ColumnText extraText = new ColumnText(writer.getDirectContent());
    extraText.addText(new Phrase(paaContext.getText("frontpage_extrainfo"), PAASharedFonts.FRONTPAGE_EXTRA.get()));
    extraText.setSimpleColumn(EXTRATEXT_LLX, EXTRATEXT_LLY, EXTRATEXT_URX,
        EXTRATEXT_URY);
    extraText.go();
  }

  protected float addIndexBlock(final PdfWriter writer, final float startPosY)
      throws DocumentException, IOException {
    float nextYPosIndex = startPosY;

    nextYPosIndex = addIndexBlockLine(writer, null, getIndexBlockHeader(), nextYPosIndex, false);
    nextYPosIndex = addIndexBlockLine(writer, PAAAnchors.getAnchorReferenceToFeatureBlock(),
        paaContext.getText("frontpage_index_feature"),
        nextYPosIndex, true);
    nextYPosIndex = addIndexBlockLine(writer, PAAAnchors.getAnchorReferenceToEmissionRecapBlock(),
        paaContext.getText("frontpage_index_emission_recap"),
        nextYPosIndex, true);

//    if (PAAReportType.CALCULATION == paaContext.getReportType()) {
//      nextYPosIndex = addIndexBlockLine(writer, PAAAnchors.getAnchorReferenceToDepositionMapBlock(),
//          paaContext.getText("frontpage_index_deposition_map"),
//          nextYPosIndex, true);
//    }

    nextYPosIndex = addIndexBlockLine(writer, PAAAnchors.getAnchorReferenceToDepositionResultsBlock(),
        paaContext.getText("frontpage_index_deposition_results"),
        nextYPosIndex, true);
    nextYPosIndex = addIndexBlockLine(writer, PAAAnchors.getAnchorReferenceToEmissionDetailsBlock(),
        paaContext.getText("frontpage_index_emission_details"),
        nextYPosIndex, true);
    return nextYPosIndex;
  }

  protected String getIndexBlockHeader() {
    return paaContext.isComparison()
        ? MessageFormat.format(paaContext.getText("frontpage_index_comparison_calculation"),
            getFirstSituationName(), getSecondSituationName())
        : MessageFormat.format(paaContext.getText("frontpage_index_calculation"), getFirstSituationName());
  }

  private String getFirstSituationName() {
    return paaContext.getCalculatedScenario() instanceof CalculatedComparison
        ? ((CalculatedComparison) paaContext.getCalculatedScenario()).getSourcesOne().getName()
        : ((CalculatedSingle) paaContext.getCalculatedScenario()).getSources().getName();
  }

  private String getSecondSituationName() {
    return paaContext.getCalculatedScenario() instanceof CalculatedComparison
        ? ((CalculatedComparison) paaContext.getCalculatedScenario()).getSourcesTwo().getName()
        : null;
  }

  private float addIndexBlockLine(final PdfWriter writer, final String reference, final String label, final float posY, final boolean addBullet)
      throws DocumentException, IOException {
    float posX = INDEX_STARTPOS_X;

    if (addBullet) {
      final Image bulletImage = PDFUtils.getImage(paaContext.getResourcesDir() + INDEX_BULLET_FILENAME, paaContext.getLocale());
      bulletImage.scaleToFit(INDEX_BULLET_IMAGE_SCALE, INDEX_BULLET_IMAGE_SCALE);
      PDFWriterUtil.addImage(writer, bulletImage, posX, posY);
      posX += 10;
    }

    final Anchor anchor = new Anchor(new Phrase(label, PAASharedFonts.FRONTPAGE_INDEX.get()));
    if (!StringUtils.isEmpty(reference)) {
      anchor.setReference(PDFConstants.ANCHOR_TAG + reference);
    }
    PDFWriterUtil.addPhrase(writer, anchor, posX, posY);

    return posY - INDEX_PADDING_BETWEEN_LINES;
  }

}
