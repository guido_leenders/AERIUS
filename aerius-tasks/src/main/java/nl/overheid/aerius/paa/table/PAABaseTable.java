/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import java.io.IOException;
import java.util.Locale;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;

import nl.overheid.aerius.paa.common.PAASharedFonts;
import nl.overheid.aerius.paa.common.PAAUtil;
import nl.overheid.aerius.pdf.table.PDFColumnTable;
import nl.overheid.aerius.worker.util.PDFUtils;

/**
 * Base used to set some default table settings for a table used in the PAA export.
 */
public abstract class PAABaseTable<T> extends PDFColumnTable<T> {

  /**
   * @see PDFColumnTable#PDFTable(Locale)
   */
  public PAABaseTable(final Locale locale) {
    super(locale);
  }

  @Override
  protected void init() {
    super.init();

    setHeaderFont(PAASharedFonts.TABLE_HEADER.get());
  }

  @Override
  protected void beforeGenerating() {
    setCellFont(PAASharedFonts.TABLE_CELL.get());
  }

  protected Image getImage(final String name, final float scaleToPercent) {
    Image image = null;
    try {
      image = PDFUtils.getImage(name, getLocale());
      image.scalePercent(scaleToPercent);
    } catch (IOException | DocumentException e) {
      throw new IllegalArgumentException(e);
    }
    return image;
  }

  protected String formatDouble(final double number) {
    return PDFUtils.getNumberFormat(getLocale(), 1).format(number);
  }

  protected String formatInt(final int number) {
    return PDFUtils.getNumberFormat(getLocale(), 0).format(number);
  }

  protected String getEmissionString(final double emission) {
    return PAAUtil.getEmissionString(getLocale(), emission);
  }

}
