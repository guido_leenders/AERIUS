/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa.table;

import nl.overheid.aerius.paa.common.PAAContext;
import nl.overheid.aerius.pdf.table.PDFColumnProperties;
import nl.overheid.aerius.pdf.table.TextPDFColumn;
import nl.overheid.aerius.pdf.table.cell.ColspanCellProcessor;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Calculation;

/**
 * Datatable containing total emission data.
 */
public class OverviewTotalEmissionDataTable extends PAABaseTable<TotalEmissionTableRow> {

  private enum Column implements PDFColumnProperties {
    SUBSTANCE("", 0.5f),
    SITUATION_1("base_totalemission_situation1", 1f),
    SITUATION_2("base_totalemission_situation2", 1f),
    DIFFERENCE("base_totalemission_difference", 1f);

    private final String headerKey;
    private final float relativeWidth;

    private Column(final String headerKey, final float relativeWidth) {
      this.headerKey = headerKey;
      this.relativeWidth = relativeWidth;
    }

    @Override
    public String getHeaderI18nKey() {
      return headerKey;
    }

    @Override
    public float getRelativeWidth() {
      return relativeWidth;
    }
  }

  /**
   * Default constructor setting header and such.
   * @param locale The locale to use.
   * @param r The report.
   * @param set1 Calculation set 1.
   * @param set2 Calculation set 2.
   */
  public OverviewTotalEmissionDataTable(final PAAContext paaContext, final Calculation set1, final Calculation set2) {
    super(paaContext.getLocale());

    final double nh3TotalEmission1 = set1.getSources().getTotalEmission(new EmissionValueKey(set1.getYear(), Substance.NH3));
    final double noxTotalEmission1 = set1.getSources().getTotalEmission(new EmissionValueKey(set1.getYear(), Substance.NOX));
    final boolean isSingleCalculation = set2 == null;
    final Double nh3TotalEmission2 = isSingleCalculation ? null : set2.getSources().getTotalEmission(
        new EmissionValueKey(set2.getYear(), Substance.NH3));
    final Double noxTotalEmission2 = isSingleCalculation ? null : set2.getSources().getTotalEmission(
        new EmissionValueKey(set2.getYear(), Substance.NOX));

    getData().add(new TotalEmissionTableRow(Substance.NOX, noxTotalEmission1, noxTotalEmission2));
    getData().add(new TotalEmissionTableRow(Substance.NH3, nh3TotalEmission1, nh3TotalEmission2));
    addColumns(paaContext, isSingleCalculation);
  }

  private void addColumns(final PAAContext paaContext, final boolean isSingleCalculation) {
    addColumn(new TextPDFColumn<TotalEmissionTableRow>(paaContext.getLocale(), Column.SUBSTANCE) {

      @Override
      protected String getText(final TotalEmissionTableRow data) {
        return getText("emission_" + data.getSubstance().name());
      }
    });
    addEmissionColumns(paaContext, isSingleCalculation);
  }

  private void addEmissionColumns(final PAAContext paaContext, final boolean isSingleCalculation) {
    final TextPDFColumn<TotalEmissionTableRow> situation1Column = new TextPDFColumn<TotalEmissionTableRow>(paaContext.getLocale(), Column.SITUATION_1) {

      @Override
      protected String getText(final TotalEmissionTableRow data) {
        return paaContext.getEmissionString(data.getEmissionSituation1());
      }
    };
    final TextPDFColumn<TotalEmissionTableRow> situation2Column = new TextPDFColumn<TotalEmissionTableRow>(paaContext.getLocale(), Column.SITUATION_2) {

      @Override
      protected String getText(final TotalEmissionTableRow data) {
        return data.getEmissionSituation2() == null ? null : paaContext.getEmissionString(data.getEmissionSituation2());
      }
    };
    final TextPDFColumn<TotalEmissionTableRow> differenceColumn = new TextPDFColumn<TotalEmissionTableRow>(paaContext.getLocale(), Column.DIFFERENCE) {

      @Override
      protected String getText(final TotalEmissionTableRow data) {
        return data.getEmissionSituation2() == null ? null
            : paaContext.getEmissionString(data.getEmissionSituation2() - data.getEmissionSituation1());
      }
    };
    if (isSingleCalculation) {
      situation1Column.addProcessor(new ColspanCellProcessor(3));
      situation2Column.addProcessor(new ColspanCellProcessor(0));
      differenceColumn.addProcessor(new ColspanCellProcessor(0));
    }
    addColumn(situation1Column);
    addColumn(situation2Column);
    addColumn(differenceColumn);
  }

}
