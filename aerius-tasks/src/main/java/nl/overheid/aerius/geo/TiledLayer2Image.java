/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import javax.imageio.ImageIO;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.worker.util.FallBackImageUtil;
import nl.overheid.aerius.worker.util.ImageMergeLayer;
import nl.overheid.aerius.worker.util.ImageUtils;

/**
 * Base class to stitch tiled layers to a single image. Examples are TMS layers or Tiled WMS layers.
 */
public abstract class TiledLayer2Image {

  private static final Logger LOGGER = LoggerFactory.getLogger(TiledLayer2Image.class);

  private final EPSG epsg;
  private final CloseableHttpClient client;
  private final int tileSize;
  private final String layerName;
  private final URI uri;
  private final double[] resolutions;
  private final double epsgXMin;
  private final double epsgYMin;

  protected TiledLayer2Image(final EPSG epsg, final URL url, final String layerName, final CloseableHttpClient client, final int tileSize)
      throws URISyntaxException {
    this.epsg = epsg;
    this.uri = url.toURI();
    this.layerName = layerName;
    this.client = client;
    this.tileSize = tileSize;
    resolutions = epsg.getResolutions();
    epsgXMin = epsg.getBounds().getMinX();
    epsgYMin = epsg.getBounds().getMinY();
  }

  /**
   * Returns the stitches image for the given bounding box and image width and
   * height. The caller make sure the ratio between width and height is the
   * same as the ratio of the bounding box.
   *
   * @param epsg EPSG to be used for the tiles.
   * @param bounds Bounding box to get tiles for
   * @param width width of the image to create
   * @param height height of the image to create
   * @return stitches image of all tiles
   * @throws URISyntaxException
   * @throws HttpException
   * @throws IOException
   * @throws ClientProtocolException
   * @throws Exception throws exceptions in case errors occur. IOException specifically if the tiles couldn't be fetched.
   */
  public BufferedImage getImage(final BBox bounds, final int width, final int height)
      throws ClientProtocolException, IOException, HttpException, URISyntaxException {
    final int z = getZoomLevel(resolutions, width, bounds);
    final double mapTileSize = mapTileSize(resolutions, z);
    final int xTileOffset = toTile(mapTileSize, (int) bounds.getMinX(), epsgXMin);
    final int yTileOffset = toTile(mapTileSize, (int) bounds.getMinY(), epsgYMin);
    //Scale image to match bounding box.
    final int realWidth = (int) ((bounds.getMaxX() - bounds.getMinX()) / resolutions[z]);
    final int realHeight = (int) ((bounds.getMaxY() - bounds.getMinY()) / resolutions[z]);
    final int xTileCoordinate = tileCoordinate(mapTileSize, xTileOffset, epsgXMin);
    final int xShift = (int) ((bounds.getMinX() - xTileCoordinate) / resolutions[z]);
    final int yTileCoordinate = tileCoordinate(mapTileSize, yTileOffset, epsgYMin);
    final int yShift = (int) ((bounds.getMinY() - yTileCoordinate) / resolutions[z]);
    final int xWidth = countTiles(realWidth);
    final int yHeight = countTiles(realHeight);
    final ImageMergeLayer[] tiles = new ImageMergeLayer[xWidth * yHeight + 1];

    int i = 0;
    tiles[i] = new ImageMergeLayer(new BufferedImage(realWidth, realHeight, BufferedImage.TYPE_INT_ARGB), 0, 0);
    for (int xOffset = 0; xOffset < xWidth; xOffset++) {
      final int xPos = xOffset * tileSize - xShift;
      final int xMin = (int) (xTileCoordinate + (xOffset * mapTileSize));
      final int xMax = (int) (xMin + mapTileSize);
      for (int yOffset = 0; yOffset < yHeight; yOffset++) {
        final int yMin = (int) (yTileCoordinate + (yOffset * mapTileSize));
        final int yMax = (int) (yMin + mapTileSize);
        final Image tileImage = getTileImage(xOffset + xTileOffset, yOffset + yTileOffset, z, xMin, yMin, xMax, yMax);

        if (tileImage == null) {
          throw new IOException("Fetching tile failed for url:" + getURL(xOffset + xTileOffset, yOffset + yTileOffset, z, xMin, yMin, xMax, yMax));
        }
        i++;
        final int yPos = realHeight - ((yOffset + 1) * tileSize - yShift);
        tiles[i] = new ImageMergeLayer(tileImage, xPos, yPos, false, false);
      }
    }
    return stitchImages(width, height, realWidth, realHeight, tiles);
  }

  /**
   * Returns the TMS tile from the TMS server.
   *
   * @param x X-Coordinate
   * @param y Y-Coordinate
   * @param z Zoom level
   * @return returns the tile image
   * @throws HttpException
   * @throws IOException
   * @throws ClientProtocolException
   */
  protected Image getTileImage(final int x, final int y, final int z, final int xMin, final int yMin, final int xMax, final int yMax)
      throws IOException, HttpException, URISyntaxException {
    return getTileImageFromUri(getURL(x, y, z, xMin, yMin, xMax, yMax));
  }

  protected URI getURL(final int x, final int y, final int z, final int xMin, final int yMin, final int xMax, final int yMax)
      throws URISyntaxException {
    return new URI(uri.getScheme(), null, uri.getHost(), uri.getPort(), getPath(uri, x, y, z), getQuery(xMin, yMin, xMax, yMax), null);
  }

  protected Image getTileImageFromUri(final URI uri) {
    final HttpGet request = new HttpGet(uri);
    try (final CloseableHttpResponse response = client.execute(request)) {
      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        final Header header = response.getFirstHeader("Content-Encoding");
        final boolean isZip = header != null && "gzip".equals(header.getValue());

        try (InputStream imageStream = isZip ? new GZIPInputStream(response.getEntity().getContent()) : response.getEntity().getContent()) {
          return ImageIO.read(imageStream);
        }
      } else {
        LOGGER.error("Response status not 200 using fallback image: {} {}", response.getStatusLine().toString(),
            EntityUtils.toString(response.getEntity()));
      }
    } catch (final IOException e) {
      LOGGER.error("Error retrieving url '{}' data using fallback image.", uri, e);
    } finally {
      request.reset();
    }
    return FallBackImageUtil.getFallBackImage();
  }

  /**
   * Calculates zoom level based on the width of the image and the bounding box.
   * The calculation is:
   * <pre>
   * number of tiles = width / tilesize + 1
   * resolution of image = (x max - x min) / number of tile * pixels per tile
   * Loop through the list of resolutions of the map and find the resolution
   * that is the first below the found resolution.
   * </pre>
   * @param resolutions resolutions related to the tile
   * @param width Width of the image to create
   * @param bounds Bounding box of the area to show
   * @return Returns zoom level index for tiles
   */
  int getZoomLevel(final double[] resolutions, final int width, final BBox bounds) {
    final int numberOfTiles = width / tileSize + 1;
    final double resolution = (bounds.getMaxX() - bounds.getMinX()) / (numberOfTiles * tileSize);

    for (int i = 0; i < resolutions.length; i++) {
      if (resolution > resolutions[i]) {
        return i - 1;
      }
    }
    return resolutions.length - 1;
  }

  /**
   * Returns the number of a tile given an coordinate x or y. The tile is
   * calculated:
   * <pre>(coordinate - offset of map) / (resolution of zoomlevel * size of tile)</pre>
   *
   * @param resolutions resolutions related to the tile
   * @param coordinate coordinate to calculate tile for
   * @param zoomLevel zoom level for the given tile
   * @param offset offset of the map
   * @return the number of the tile
   */
  int toTile(final double mapTileSize, final int coordinate, final double offset) {
    return (int) ((coordinate - offset) / mapTileSize);
  }

  /**
   * Returns the coordinate x or y of the tile. This is calculated by
   * <pre>tile number * resolution of zoom level * size of tiles + offset of map</pre>.
   *
   * @param resolutions resolutions related to the tile
   * @param tile value of the tile
   * @param zoomLevel zoom level for the given tile
   * @param offset offset of the map
   * @return coordinate of the tile
   */
  int tileCoordinate(final double mapTileSize, final int tile, final double offset) {
    return (int) (tile * mapTileSize + offset);
  }

  double mapTileSize(final double[] resolutions, final int zoomLevel) {
    return resolutions[zoomLevel] * tileSize;
  }

  /**
   * Calculates the number of tiles there are in a dimension (width or height).
   * @param dimensionSize The size of the dimension
   * @return The number of tiles there will be in that dimension.
   */
  int countTiles(final int dimensionSize) {
    return (int) (Math.ceil(dimensionSize / (double) tileSize) + 1);
  }

  protected abstract String getPath(URI uri, final int x, final int y, final int z);

  protected abstract String getQuery(final int xMin, final int yMin, final int xMax, final int yMax);

  protected BufferedImage stitchImages(final int width, final int height, final int realWidth, final int realHeight, final ImageMergeLayer... tiles) {
    final BufferedImage image = ImageUtils.mergeImages(tiles);
    final BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g = scaledImage.createGraphics();

    final int subImageX = 0;
    final int subImageY = Math.min(0, image.getHeight() - realHeight);
    final int subImageWidth = Math.min(image.getWidth(), realWidth);
    final int subImageHeight = Math.min(image.getHeight(), realHeight);
    try {
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g.drawImage(
          image.getSubimage(subImageX, subImageY, subImageWidth, subImageHeight), 0, 0, width, height, null);
    } catch (final RasterFormatException e) {
      LOGGER.error("Failed to get subimage with variables, subImageX: {} - subImageY: {} - subImageWidth: {} - subImageHeight: {} - width: {} - "
          + "height: {} - realWidth: {} - realHeight: {}", subImageX, subImageY, subImageWidth, subImageHeight, width, height, realWidth, realHeight);
      throw e;
    } finally {
      g.dispose();
    }
    return scaledImage;
  }

  protected String getLayerName() {
    return layerName;
  }

  protected int getTileSize() {
    return tileSize;
  }

  protected EPSG getEpsg() {
    return epsg;
  }
}
