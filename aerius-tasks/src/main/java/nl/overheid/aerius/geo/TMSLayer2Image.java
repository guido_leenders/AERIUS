/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.geo.shared.EPSG;

/**
 * Creates one large image of a bounding box and image width and height of a
 * Tile Map Service (TMS) layer by stitching all individual tiles images to one
 * large image.
 *
 * Default tile size is 256 x 256 pixels.
 */
public class TMSLayer2Image extends TiledLayer2Image {

  private static final int DEFAULT_TILE_SIZE = 256;
  private static final String SERVICE_VERSION = "1.0.0";
  private static final String TYPE = "png";

  /**
   * Initializes the class with TMS path and layer.
   *
   * @param tmsUrl URL to the TMS service
   * @param layerName Name of the layer
   * @param client the HTTP client to use
   * @throws URISyntaxException throws exception if tmsUrl could not be parsed
   */
  public TMSLayer2Image(final EPSG epsg, final URL tmsUrl, final String layerName, final CloseableHttpClient client) throws URISyntaxException {
    this(epsg, tmsUrl, layerName, DEFAULT_TILE_SIZE, client);
  }

  /**
   * Initializes the class with TMS path and layer.
   *
   * @param epsg URL to the TMS service
   * @param tmsUrl URL to the TMS service
   * @param layerName Name of the layer
   * @param tileSize set the size of a tile
   * @param client the HTTP client to use
   * @throws URISyntaxException throws exception if tmsUrl could not be parsed
   */
  public TMSLayer2Image(final EPSG epsg, final URL tmsUrl, final String layerName, final int tileSize, final CloseableHttpClient client)
      throws URISyntaxException {
    super(epsg, tmsUrl, layerName, client, tileSize);
  }

  @Override
  protected String getPath(final URI tmsUri, final int x, final int y, final int z) {
    final String path = tmsUri.getPath();
    return path + (path.endsWith("/") ? "" : "/") + SERVICE_VERSION + "/" + getLayerName() + "/" + z + "/" + x + "/" + y + "." + TYPE;
  }

  @Override
  protected String getQuery(final int xMin, final int yMin, final int xMax, final int yMax) {
    return null;
  }
}
