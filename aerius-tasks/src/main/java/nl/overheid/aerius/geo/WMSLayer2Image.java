/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.CloseableHttpClient;

import nl.overheid.aerius.geo.shared.EPSG;

/**
 * Generates an image (png) of a tiled WMS layer.
 */
public class WMSLayer2Image extends TiledLayer2Image {

  private static final String FORMAT = "FORMAT=image/png";
  private static final String TRANSPARENT = "TRANSPARENT=TRUE";
  private static final String SERVICE = "SERVICE=WMS";
  private static final String VERSION = "VERSION=1.1.1";
  private static final String REQUEST = "REQUEST=GetMap";
  private static final String STATIC_PART = FORMAT + '&' + VERSION + '&' + TRANSPARENT + '&' + SERVICE + '&'  + REQUEST;
  private static final String LAYERS_PRE = "LAYERS=";
  private static final String SRS_PRE = "SRS=";
  private static final String BBOX_PRE = "BBOX=";
  private static final String WIDTH_PRE = "WIDTH=";
  private static final String HEIGHT_PRE = "HEIGHT=";

  public WMSLayer2Image(final EPSG epsg, final URL wmsUrl, final String layerName, final CloseableHttpClient client, final int tileSize)
      throws URISyntaxException {
    super(epsg, wmsUrl, layerName, client, tileSize);
  }

  /**
   * Returns the path part for the given uri, x, y and z coordinates.
   *
   * @param uri uri to the service.
   * @param x X-Coordinate
   * @param y Y-Coordinate
   * @param z Zoom level
   * @return path string
   */
  @Override
  protected String getPath(final URI uri, final int x, final int y, final int z) {
    return uri.getPath();
  }

  /**
   * Returns the query part for the given boundary coordinates.
   *
   * @param xMin the x min boundary
   * @param yMin the y min boundary
   * @param xMax the x max boundary
   * @param yMax the y max boundary
   * @return query string
   */
  @Override
  protected String getQuery(final int xMin, final int yMin, final int xMax, final int yMax) {
    final List<String> parts = new ArrayList<>();
    parts.add(STATIC_PART);
    parts.add(LAYERS_PRE + getLayerName());
    parts.add(SRS_PRE + getEpsg().getEpsgCode());
    parts.add(BBOX_PRE + xMin + ',' + yMin + ',' + xMax + ',' + yMax);
    parts.add(WIDTH_PRE + getTileSize());
    parts.add(HEIGHT_PRE + getTileSize());
    return StringUtils.join(parts.toArray(), '&');
  }
}
