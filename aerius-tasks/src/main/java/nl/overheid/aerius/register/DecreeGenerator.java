/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.register.RequestModifyRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.paa.base.PAAExport;
import nl.overheid.aerius.paa.decree.DecreeInformation;
import nl.overheid.aerius.paa.decree.DecreePAAContext;
import nl.overheid.aerius.paa.decree.DecreePAAExport;
import nl.overheid.aerius.paa.decree.DetailDecreePAAExport;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.ReleaseType;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.InsertRequestFile;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.HttpClientManager;
import nl.overheid.aerius.util.LocaleUtils;

/**
 *
 */
abstract class DecreeGenerator<T extends Request> {

  private static final Logger LOG = LoggerFactory.getLogger(DecreeGenerator.class);

  protected final PMF pmf;
  protected final RequestFileType generateFileType;

  DecreeGenerator(final PMF pmf, final RequestFileType generateFileType) {
    this.pmf = pmf;
    this.generateFileType = generateFileType;
  }

  protected abstract T getOldestWithStatusButNoFile() throws SQLException;

  protected abstract SegmentType getSegmentType();

  protected abstract DossierAuthorityKey getKey(final T request);

  protected abstract void addSpecificInformation(final DecreeInformation decreeInfo, final T request) throws SQLException ;

  protected boolean process() throws SQLException, IOException, AeriusException {
    boolean processed = false;
    final T request = getOldestWithStatusButNoFile();
    if (request != null) {
      LOG.info("Found request with reference {} to generate a {} file for.", request.getReference(), generateFileType);
      process(request);
      LOG.info("Done processing {} PDF for request with reference {}.", generateFileType, request.getReference());
      processed = true;
    }
    return processed;
  }

  /**
   * Process the request.
   * @param request the Request
   * @throws IOException any io exception
   * @throws SQLException any database
   * @throws AeriusException exception
   */
  public void process(final T request) throws IOException, SQLException, AeriusException {
    //get everything required to generate the PDF (shouldn't be changeable data).
    final List<StringDataSource> gmls = new ArrayList<>();
    final CalculationInputData paaExportData = getPAAExportData(request, gmls);
    final DecreeInformation decreeInfo = getDecreeInfo(request);

    //generate the PDF and save to the database
    generatePDF(paaExportData, decreeInfo, request.getReference(), request.getId(), gmls);
  }

  private CalculationInputData getPAAExportData(final T request, final List<StringDataSource> gmls) throws SQLException,
      IOException, AeriusException {
    final CalculationInputData exportData = new CalculationInputData();
    //OPS queue, export type, email and creation date matter not.
    exportData.setLocale(LocaleUtils.getDefaultLocale(pmf).getLanguage());
    // fake scenario based on existing application file and the actual calculations for the request.
    final CalculatedScenario calculatedScenario;
    try (final Connection con = pmf.getConnection()) {
      final RequestFile applicationFile = getApplicationRequestFile(con, request);
      final byte[] content = RequestRepository.getRequestFileContent(con, request.getReference(), RequestFileType.APPLICATION);
      if (applicationFile == null || content == null) {
        LOG.error("Could not find the application content for request with reference {}", request.getReference());
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
      //We have to add datasource to the GML else the PDF won't be generated.
      //Decree's don't have to be imported however, and it would increase filesize by a bit (especially if GML contains results), so skip that.
      gmls.add(new StringDataSource("NO_GML_AVAILABLE", null, GMLWriter.GML_MIMETYPE));
      final Map<SituationType, Integer> calculationMap = RequestRepository.getCalculationIds(con, request.getReference());
      calculatedScenario = extractScenario(content, applicationFile, request, calculationMap);
    }
    exportData.setScenario(calculatedScenario);
    return exportData;
  }

  private RequestFile getApplicationRequestFile(final Connection con, final Request request) throws SQLException {
    RequestFile foundRequestFile = null;
    for (final RequestFile requestFile : RequestRepository.getExistingRequestFiles(con, request.getReference())) {
      if (requestFile.getRequestFileType() == RequestFileType.APPLICATION) {
        foundRequestFile = requestFile;
      }
    }
    return foundRequestFile;
  }

  private CalculatedScenario extractScenario(final byte[] content, final RequestFile requestFile, final T request,
      final Map<SituationType, Integer> calculationMap) throws SQLException, IOException, AeriusException {
    final ImportResult importResult;
    try (final InputStream inputStream = new ByteArrayInputStream(content)) {
      final Importer importer = new Importer(pmf);
      final String dummyFilename = "someFile." + requestFile.getFileFormat().getExtension();
      importResult = importer.convertInputStream2ImportResult(dummyFilename, inputStream);
    }
    final int situationId2 = importResult.getSourceLists().size() == 2 ? 1 : -1;
    final CalculatedScenario scenario = CalculatedScenarioUtil.toCalculatedScenario(importResult, 0, situationId2);
    scenario.setYear(request.getStartYear());
    if (scenario instanceof CalculatedSingle) {
      ((CalculatedSingle) scenario).setCalculationId(calculationMap.get(SituationType.PROPOSED));
      ((CalculatedSingle) scenario).getCalculation().getOptions().setTemporaryProjectYears(request.getTemporaryPeriod());
    } else if (scenario instanceof CalculatedComparison) {
      ((CalculatedComparison) scenario).setCalculationIdOne(calculationMap.get(SituationType.CURRENT));
      ((CalculatedComparison) scenario).setCalculationIdTwo(calculationMap.get(SituationType.PROPOSED));
      ((CalculatedComparison) scenario).getCalculationOne().getOptions().setTemporaryProjectYears(request.getTemporaryPeriod());
      ((CalculatedComparison) scenario).getCalculationTwo().getOptions().setTemporaryProjectYears(request.getTemporaryPeriod());
    }
    return scenario;
  }

  private DecreeInformation getDecreeInfo(final T request) throws SQLException {
    final DecreeInformation decreeInfo = new DecreeInformation();
    decreeInfo.setAuthority(request.getAuthority());
    decreeInfo.setSector(request.getSector());
    decreeInfo.setRequestState(request.getRequestState());
    decreeInfo.setSegment(getSegmentType());
    decreeInfo.setProjectKey(getKey(request));
    addSpecificInformation(decreeInfo, request);
    return decreeInfo;
  }

  private void generatePDF(final CalculationInputData inputData, final DecreeInformation decreeInfo, final String reference,
      final int requestId, final List<StringDataSource> gmls) throws IOException, SQLException, AeriusException {
    LOG.info("Generating {} PDF for: {}", generateFileType, reference);
    byte[] fileContent;
    try (CloseableHttpClient client = createHttpClient()) {
      final PAAExport<DecreePAAContext> export = getProperExport(client, inputData, decreeInfo, gmls);
      if (export == null) {
        return;
      }
      export.setWatermark(ConstantRepository.getEnum(pmf, ConstantsEnum.RELEASE, ReleaseType.class));
      fileContent = export.generatePDF();
    }
    LOG.info("{} PDF generated, inserting in DB: {}", generateFileType, reference);
    //Save to DB as the proper file type.
    persistToDB(requestId, fileContent, generateFileType);
  }

  private PAAExport<DecreePAAContext> getProperExport(final CloseableHttpClient client, final CalculationInputData inputData,
      final DecreeInformation decreeInfo, final List<StringDataSource> gmls) throws SQLException, AeriusException {
    PAAExport<DecreePAAContext> export;
    switch (generateFileType) {
    case DECREE:
      export = new DecreePAAExport(pmf, client, inputData, gmls, decreeInfo);
      break;
    case DETAIL_DECREE:
      export = new DetailDecreePAAExport(pmf, client, inputData, gmls, decreeInfo);
      break;
    default:
      LOG.error("Don't know how to handle {} for generating decree files.", generateFileType);
      export = null;
    }
    return export;
  }

  private void persistToDB(final int requestId, final byte[] fileContent, final RequestFileType requestFileType)
      throws SQLException, AeriusException {
    try (final Connection con = pmf.getConnection()) {
      final InsertRequestFile insertRequestFile = new InsertRequestFile();
      insertRequestFile.setFileFormat(FileFormat.PDF);
      insertRequestFile.setRequestFileType(requestFileType);
      insertRequestFile.setFileContent(fileContent);
      RequestModifyRepository.insertFileContent(con, requestId, insertRequestFile);
    }
  }

  protected CloseableHttpClient createHttpClient() {
    return HttpClientManager.getRetryingHttpClient(-1);
  }

}
