/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register.export;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.AeriusVersion;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.db.register.PrioritySubProjectRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.shared.domain.FileFormat;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData.PriorityProjectExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PriorityProjectKey;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProjectKey;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestState;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class RequestResultFileExporter extends Exporter {

  private static final Logger LOG = LoggerFactory.getLogger(RequestResultFileExporter.class);

  private static final String PROJECT_RESERVATION_FILENAME = "Project_Reservation_0";
  private static final String PROJECT_ASSIGNED_TOTAL_FILENAME = "Project_Assigned_1";
  private static final String RESERVATION_FILENAME = "Reservation";
  private static final String ASSIGNED_FILENAME = "Subproject_assigned";
  private static final String PROJECT_RESERVATION_SITUATIONNAME = "Reservering";
  private static final String PROJECT_ASSIGNED_SITUATIONNAME = "Benutting";
  private static final String SUB_PROJECT_ASSIGNED_UTILISATION_FILENAME = "SubProject_assigned_utilisation";

  private final PMF pmf;

  /**
   * @param pmf The persistence manager factory to use.
   */
  public RequestResultFileExporter(final PMF pmf) {
    this.pmf = pmf;
  }

  /**
   * Add Prioritysubproject GML results to output stream.
   * @param out OutputStream to write content to.
   * @param con database connection.
   * @param prioritySubProjectKey the key.
   * @return int file count.
   * @throws AeriusException project not found exception.
   * @throws IOException writing files exception.
   * @throws SQLException database exception.
   */
  public int generateSubPriorityAssignOverview(final OutputStream out, final Connection con, final PrioritySubProjectKey prioritySubProjectKey)
      throws AeriusException, IOException, SQLException {
    if (!PrioritySubProjectRepository.prioritySubProjectExists(con, prioritySubProjectKey)) {
      LOG.error("Unknown priority sub project key {}", prioritySubProjectKey);
      throw new AeriusException(Reason.PRIORITY_SUBPROJECT_UNKNOWN, prioritySubProjectKey.getReference());
    }
    final List<FileWrapper> generatedFiles = generatePrioritySubProjectFiles(con, prioritySubProjectKey);

    generateZip(out, generatedFiles);

    return generatedFiles.size();
  }

  /**
   * Add priority project GML results to output stream.
   * @param out OutputStream to write content to.
   * @param con database connection.
   * @param exportData @PriorityProjectExportData export data.
   * @return int file count.
   * @throws AeriusException project not found exception.
   * @throws IOException writing files exception.
   * @throws SQLException database exception.
   */
  public int generatePriorityProjectExport(final OutputStream out, final Connection con, final PriorityProjectExportData exportData)
      throws AeriusException, SQLException, IOException {
    if (!PriorityProjectRepository.priorityProjectExists(con, exportData.getPriorityProjectKey())) {
      LOG.error("Unknown priority project key {}", exportData.getPriorityProjectKey());
      throw new AeriusException(Reason.PRIORITY_PROJECT_UNKNOWN, exportData.getPriorityProjectReference());
    }
    final List<FileWrapper> generatedFiles = generateFiles(con, exportData);

    generateZip(out, generatedFiles);

    return generatedFiles.size();
  }


  protected List<FileWrapper> generatePrioritySubProjectFiles(final Connection con, final PrioritySubProjectKey prioritySubProjectKey)
      throws AeriusException, IOException, SQLException {
    final List<FileWrapper> generatedFiles = new ArrayList<>();
    final PrioritySubProject prioritySubProject = PrioritySubProjectRepository.getSkinnedPrioritySubProjectByKey(con, prioritySubProjectKey);
    if (prioritySubProject != null) {
      final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(con);
      generatedFiles.add(generateReservationForPriorityProject(con, prioritySubProject, rgs));
      generatedFiles.add(generateAssignedForPriorityProject(con, prioritySubProject, rgs));
    }
    return generatedFiles;
  }

  private FileWrapper generateReservationForPriorityProject(final Connection con, final PrioritySubProject prioritySubProject,
      final ReceptorGridSettings rgs) throws SQLException, IOException, AeriusException {
    final int priorityProjectId = PrioritySubProjectRepository.getParentProjectIdForSubProject(con, prioritySubProject.getId());
    final List<AeriusPoint> reservedResults = new ArrayList<>(RequestRepository.getResultsForExport(con, priorityProjectId));
    return generateGML(reservedResults, RESERVATION_FILENAME, prioritySubProject.getReference(), rgs, false);
  }

  private FileWrapper generateReservationForPriorityProject(final Connection con, final PriorityProject priorityProject,
      final ReceptorGridSettings rgs) throws SQLException, IOException, AeriusException {
    final List<AeriusPoint> reservedResults = new ArrayList<>(RequestRepository.getResultsForExport(con, priorityProject.getId()));
    final MetaDataInput meta = createMetaData(priorityProject, PROJECT_RESERVATION_SITUATIONNAME);
    return generateGML(reservedResults, PROJECT_RESERVATION_FILENAME, priorityProject.getReference(), rgs, false, meta);
  }

  private FileWrapper generateAssignedTotalForPriorityProject(final Connection con, final PriorityProject priorityProject,
      final ReceptorGridSettings rgs) throws SQLException, IOException, AeriusException {
    final List<AeriusPoint> assignedResults = new ArrayList<>(PriorityProjectRepository.getDemandAssignedResultsForExport(con,
        priorityProject.getId()));
    return generateGML(assignedResults, PROJECT_ASSIGNED_TOTAL_FILENAME, priorityProject.getReference(), rgs, false,
        createMetaData(priorityProject, PROJECT_ASSIGNED_SITUATIONNAME));
  }

  private FileWrapper generateAssignedForPriorityProject(final Connection con, final PrioritySubProject prioritySubProject,
      final ReceptorGridSettings rgs) throws SQLException, IOException, AeriusException {
    final List<AeriusPoint> assignedResults = new ArrayList<>(
        PrioritySubProjectRepository.getAssignedResultsInclSubProject(con, prioritySubProject.getId()));
    return generateGML(assignedResults, ASSIGNED_FILENAME, generateAssignedForPriorityProjectFilename(prioritySubProject), rgs, false,
        createMetaData(prioritySubProject, PROJECT_ASSIGNED_SITUATIONNAME));
  }

  private String generateAssignedForPriorityProjectFilename(final PrioritySubProject prioritySubProject) {
    return prioritySubProject.getReference() + "_"
        + prioritySubProject.getScenarioMetaData().getProjectName() + "_"
        + (prioritySubProject.getLabel() == null ? "" : ("_" + prioritySubProject.getLabel())) + "_"
        + prioritySubProject.getReceivedDate();
  }

  private MetaDataInput createMetaData(final PriorityProject priorityProject, final String name) throws SQLException {
    return returnMetaData(priorityProject, name);
  }

  private MetaDataInput createMetaData(final PrioritySubProject prioritySubProject, final String name)
      throws SQLException {
    return returnMetaData(prioritySubProject, name);
  }

  private MetaDataInput returnMetaData(final Request request, final String name) throws SQLException {
    final MetaDataInput meta = new MetaDataInput();
    meta.setName(name);
    meta.setScenarioMetaData(request.getScenarioMetaData());
    meta.setVersion(AeriusVersion.getVersionNumber());
    meta.setYear(request.getStartYear());
    meta.setDatabaseVersion(pmf.getDatabaseVersion());
    return meta;
  }

  private FileWrapper generateGML(final List<AeriusPoint> results, final String fileName, final String projectReference,
      final ReceptorGridSettings rgs, final boolean useDirectory) throws IOException, AeriusException {
    return generateGML(results, fileName, projectReference, rgs, useDirectory, null);
  }

  private FileWrapper generateGML(final List<AeriusPoint> results, final String fileName,
      final String projectReference,
      final ReceptorGridSettings rgs, final boolean useDirectory, final MetaDataInput metaDataInput)
      throws IOException, AeriusException {
    final GMLWriter builder = new GMLWriter(rgs);

    final File file = File.createTempFile(fileName, FileFormat.GML.getDottedExtension());
    try (final FileOutputStream outputStream = new FileOutputStream(file)) {
      builder.writeAeriusPoints(outputStream, results, metaDataInput);
    }
    final String exportFileName = getFileName(fileName, projectReference, FileFormat.GML.getExtension(), useDirectory);
    return new FileWrapper(file, exportFileName);
  }

  protected List<FileWrapper> generateFiles(final Connection con, final PriorityProjectExportData exportData)
      throws AeriusException, SQLException, IOException {
    final List<FileWrapper> generatedFiles = new ArrayList<>();
    final PriorityProjectKey key = exportData.getPriorityProjectKey() == null
        ? PriorityProjectRepository.getPriorityProjectKey(con, exportData.getPriorityProjectReference()) : exportData.getPriorityProjectKey();
    final PriorityProject priorityProject = PriorityProjectRepository.getPriorityProjectByKey(con, key);

    if (priorityProject != null) {
      for (final PriorityProjectExportType exportType : exportData.getExportTypes()) {
        switch (exportType) {
        case PDF_FILES:
          generatedFiles.addAll(retrieveFilesForPriorityProject(con, priorityProject, true));
          break;
        case GML_FILES:
          generatedFiles.addAll(retrieveFilesForPriorityProject(con, priorityProject, false));
          break;
        case UTILISATION_FILES:
          generatedFiles.addAll(retrieveUtilisationFilesForPriorityProject(con, priorityProject));
          break;
        default:
          LOG.error("Unknown export type {}", exportType);
          throw new AeriusException(Reason.INTERNAL_ERROR);
        }
      }
    }
    return generatedFiles;
  }

  private List<FileWrapper> retrieveFilesForPriorityProject(final Connection con, final PriorityProject priorityProject,
      final boolean exportPdfInsteadOfGml) throws SQLException, IOException {
    final List<FileWrapper> generatedFiles = new ArrayList<>();
    addFromDB(generatedFiles, con, priorityProject, exportPdfInsteadOfGml);
    for (final PrioritySubProject subProject : priorityProject.getSubProjects()) {
      addFromDB(generatedFiles, con, subProject, exportPdfInsteadOfGml);
    }
    return generatedFiles;
  }

  private List<FileWrapper> retrieveUtilisationFilesForPriorityProject(final Connection con, final PriorityProject priorityProject)
      throws SQLException, IOException, AeriusException {
    final List<FileWrapper> generatedFiles = new ArrayList<>();
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(con);
    final FileWrapper reservationForPriorityProject = generateReservationForPriorityProject(con, priorityProject, rgs);
    generatedFiles.add(reservationForPriorityProject);
    generatedFiles.add(generateAssignedTotalForPriorityProject(con, priorityProject, rgs));
    for (final PrioritySubProject subProject : priorityProject.getSubProjects()) {
      if (ArrayUtils.contains(RequestState.PRIORITY_PROJECT_UTILISATION_EXPORT_VALUES, subProject.getRequestState())) {
        final FileWrapper assignedForSubProjectFile = generateAssignedForPriorityProject(con, subProject, rgs);
        final List<FileWrapper> generatedSubFiles = new ArrayList<>();
        generatedSubFiles.add(reservationForPriorityProject);
        generatedSubFiles.add(assignedForSubProjectFile);
        generatedFiles.add(generateSubProjectzipContent(generatedSubFiles, subProject));
      }
    }
    return generatedFiles;
  }

  private FileWrapper generateSubProjectzipContent(final List<FileWrapper> generatedSubFiles, final PrioritySubProject subProject)
      throws IOException {
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      generateZip(outputStream, generatedSubFiles, false);
      final String fileName = getFileName(SUB_PROJECT_ASSIGNED_UTILISATION_FILENAME, generateAssignedForPriorityProjectFilename(subProject),
          FileFormat.ZIP.getExtension(), false);
      final File file = File.createTempFile(fileName, FileFormat.ZIP.getDottedExtension());
      try (final FileOutputStream zipOutputStream = new FileOutputStream(file)) {
        zipOutputStream.write(outputStream.toByteArray());
      }
      return new FileWrapper(file, fileName);
    }
  }

  private void addFromDB(final List<FileWrapper> generatedFiles, final Connection con, final Request request, final boolean exportPdfInsteadOfGml)
      throws SQLException, IOException {
    final String reference = request.getReference();
    final List<RequestFile> requestFiles = RequestRepository.getExistingRequestFiles(con, reference);
    for (final RequestFile requestFile : requestFiles) {
      final boolean export;
      if (exportPdfInsteadOfGml) {
        export = requestFile.getFileFormat() == FileFormat.PDF;
      } else {
        export = requestFile.getFileFormat() == FileFormat.GML || requestFile.getFileFormat() == FileFormat.ZIP;
      }
      if (export) {
        final String fileName = getFileName(requestFile.getRequestFileType().name().toLowerCase(Locale.ENGLISH), reference,
            requestFile.getFileFormat().getExtension(), true);
        final File tempFile = writeToTempFile(con, reference, requestFile);
        generatedFiles.add(new FileWrapper(tempFile, fileName));
      }
    }
  }

  private File writeToTempFile(final Connection con, final String reference, final RequestFile requestFile) throws IOException, SQLException {
    final File file = File.createTempFile(reference, '.' + requestFile.getFileFormat().getExtension());
    FileUtils.writeByteArrayToFile(file, RequestRepository.getRequestFileContent(con, reference, requestFile.getRequestFileType()));
    return file;
  }

  private String getFileName(final String baseName, final String projectReference, final String extension, final boolean useDirectory) {
    final StringBuilder exportFileName = new StringBuilder();
    if (useDirectory) {
      exportFileName.append(projectReference);
      exportFileName.append('/');
    }
    exportFileName.append(getFileName(baseName, projectReference, extension));
    return exportFileName.toString();
  }
}
