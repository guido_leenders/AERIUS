/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.register.export.RequestResultFileExporter;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.base.AbstractExportWorker;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * TODO refactor code to combine with {@link PrioritySubProjectExportWorker}.
 */
public class PriorityProjectExportWorker extends AbstractExportWorker<PriorityProjectExportData> {

  private static final Logger LOGGER = LoggerFactory.getLogger(PriorityProjectExportWorker.class);

  private static final String FILENAME_PREFIX = "AERIUS_prioritair_project";
  private static final String FILENAME_EXTENSION = ".zip";

  public PriorityProjectExportWorker(final PMF pmf, final DBWorkerConfiguration config, final BrokerConnectionFactory factory) {
    super(pmf, config.getGMLDownloadDirectory(), factory);
  }

  @Override
  protected ImmediateExportData handleExport(final PriorityProjectExportData inputData, final ArrayList<StringDataSource> backupAttachments,
      final String correlationId) throws Exception {
    // check key if not available retrieve from projectReference
    if (inputData.getPriorityProjectKey() == null) {
      inputData.setPriorityProjectKey(PriorityProjectRepository.getPriorityProjectKey(getPMF().getConnection(),
          inputData.getPriorityProjectReference()));
    }
    return generateZip(inputData);
  }

  protected ImmediateExportData generateZip(final PriorityProjectExportData inputData) throws SQLException, AeriusException, IOException {
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final Connection con = getConnection()) {
      final RequestResultFileExporter exporter = new RequestResultFileExporter(getPMF());
      final int filesInZip = exporter.generatePriorityProjectExport(outputStream, con, inputData);
      final ImmediateExportData immediateExportData;
      if (inputData.isEmailUser()) {
        immediateExportData = new ImmediateExportData(saveFile(inputData, outputStream.toByteArray()));
      } else if (inputData.isReturnFile()) {
        immediateExportData = new ImmediateExportData(outputStream.toByteArray());
      } else {
        LOGGER.error("PriorityProjectExportWorker data not sent to user or returned to the user. Don't know what to do with: {}", inputData);
        immediateExportData = new ImmediateExportData((String) null);
      }
      immediateExportData.setFilesInZip(filesInZip);
      return immediateExportData;
    }
  }

  @Override
  protected String getFileName(final PriorityProjectExportData inputData) {
    return FileUtil.getFileName(FILENAME_PREFIX, FILENAME_EXTENSION, inputData.getPriorityProjectKey().getDossierId(),
        inputData.getCreationDate());
  }

  @Override
  protected void setReplacementTokens(final PriorityProjectExportData inputData, final MailMessageData mailMessageData) {
    // NO-OP (not using mails)
  }

}
