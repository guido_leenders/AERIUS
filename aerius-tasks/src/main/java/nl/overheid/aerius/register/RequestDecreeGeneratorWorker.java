/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.register.PermitRepository;
import nl.overheid.aerius.db.register.PriorityProjectRepository;
import nl.overheid.aerius.db.register.PrioritySubProjectRepository;
import nl.overheid.aerius.paa.decree.DecreeInformation;
import nl.overheid.aerius.shared.domain.register.DossierAuthorityKey;
import nl.overheid.aerius.shared.domain.register.Permit;
import nl.overheid.aerius.shared.domain.register.PriorityProject;
import nl.overheid.aerius.shared.domain.register.PrioritySubProject;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 *
 */
public class RequestDecreeGeneratorWorker extends AbstractContinuousRegisterWorker implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(RequestDecreeGeneratorWorker.class);

  private static final int WAIT_TIME_IN_MINUTES = 1;

  private final RequestFileType generateFileType;
  private final SegmentType segmentType;

  /**
   * Constructor.
   * @param config database worker configuration
   * @throws AeriusException exception
   **/
  public RequestDecreeGeneratorWorker(final DBWorkerConfiguration config, final RequestFileType generateFileType, final SegmentType segmentType)
      throws AeriusException {
    super(config, WAIT_TIME_IN_MINUTES);
    this.generateFileType = generateFileType;
    this.segmentType = segmentType;
  }

  @Override
  protected String getWorkerName() {
    return getClass().getName() + "[" + segmentType + ", " + generateFileType + "]";
  }

  @Override
  public boolean process() {
    boolean processed = false;
    try {
      final DecreeGenerator<?> generator = determineGenerator();
      processed = generator.process();
    } catch (final SQLException | IOException | AeriusException e) {
      LOG.error("Error processing request for decree", e);
    }
    return processed;
  }

  protected DecreeGenerator<?> determineGenerator() {
    final DecreeGenerator<?> generator;
    switch (segmentType) {
    case PROJECTS:
      generator = new PermitDecreeGenerator(registerPMF, generateFileType);
      break;
    case PRIORITY_SUBPROJECTS:
      generator = new PrioritySubProjectDecreeGenerator(registerPMF, generateFileType);
      break;
    default:
      throw new IllegalArgumentException("Unclear how to handle segment: " + segmentType);
    }
    return generator;
  }

  static class PermitDecreeGenerator extends DecreeGenerator<Permit> {

    PermitDecreeGenerator(final PMF pmf, final RequestFileType generateFileType) {
      super(pmf, generateFileType);
    }

    @Override
    protected Permit getOldestWithStatusButNoFile() throws SQLException {
      try (Connection connection = pmf.getConnection()) {
        return PermitRepository.getOldestWithoutDecrees(connection, generateFileType);
      }
    }

    @Override
    protected SegmentType getSegmentType() {
      return SegmentType.PROJECTS;
    }

    @Override
    protected DossierAuthorityKey getKey(final Permit request) {
      return request.getPermitKey();
    }

    @Override
    protected void addSpecificInformation(final DecreeInformation decreeInfo, final Permit request) {
      // NO-OP
    }

  }

  static class PrioritySubProjectDecreeGenerator extends DecreeGenerator<PrioritySubProject> {

    PrioritySubProjectDecreeGenerator(final PMF pmf, final RequestFileType generateFileType) {
      super(pmf, generateFileType);
    }

    @Override
    protected PrioritySubProject getOldestWithStatusButNoFile() throws SQLException {
      try (Connection connection = pmf.getConnection()) {
        return PrioritySubProjectRepository.getOldestWithoutDecrees(connection, generateFileType);
      }
    }

    @Override
    protected SegmentType getSegmentType() {
      return SegmentType.PRIORITY_PROJECTS;
    }

    @Override
    protected DossierAuthorityKey getKey(final PrioritySubProject request) {
      return request.getKey();
    }

    @Override
    protected void addSpecificInformation(final DecreeInformation decreeInfo, final PrioritySubProject request) throws SQLException {
      try (final Connection con = pmf.getConnection()) {
        final PriorityProject parentProject = PriorityProjectRepository.getPriorityProjectByKey(con, request.getParentKey());
        decreeInfo.setParentProjectName(parentProject.getScenarioMetaData().getProjectName());
      }
    }

  }

}
