/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.CalculatorBuildDirector;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.profile.pas.PASUtil;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.register.RequestModifyRepository;
import nl.overheid.aerius.db.register.RequestRepository;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.register.Request;
import nl.overheid.aerius.shared.domain.register.RequestFile;
import nl.overheid.aerius.shared.domain.register.RequestFileType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * Request Calculation Worker. Calculates the results for each request of a certain segment stored in the database.
 * This process runs indefinitely and checks each minute if there are new requests stored in the database
 * and if so calculates the results for that request.
 */
class RequestCalculationWorker extends AbstractContinuousRegisterWorker {

  private static final Logger LOG = LoggerFactory.getLogger(RequestCalculationWorker.class);

  private static final int WAIT_TIME_IN_MINUTES = 1;

  private final CalculatorBuildDirector calculatorBuildDirector;
  private final SegmentType segmentType;
  private final EnumSet<SegmentType> priorityProjectSet = EnumSet.of(SegmentType.PRIORITY_PROJECTS, SegmentType.PRIORITY_SUBPROJECTS);

  /**
   * Constructor.
   * @param configuration The database configuration
   * @param factory The broker factory
   * @throws AeriusException
   * @throws SQLException
   */
  public RequestCalculationWorker(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory, final SegmentType segmentType)
      throws AeriusException, SQLException {
    super(configuration, WAIT_TIME_IN_MINUTES);
    this.segmentType = segmentType;
    calculatorBuildDirector = createCalculatorBuildDirector(factory);
  }

  protected CalculatorBuildDirector createCalculatorBuildDirector(final BrokerConnectionFactory factory) throws SQLException {
    return new CalculatorBuildDirector(registerPMF, factory);
  }

  @Override
  protected String getWorkerName() {
    return getClass().getName() + "[" + segmentType + "]";
  }

  @Override
  public boolean process() {
    boolean processed = false;
    try {
      final Request request = getOldestRequestWithoutCalculations();
      if (request != null) {
        processRequest(request);
        processed = true;
      }
    } catch (final SQLException e) {
      LOG.error("Error processing request", e);
    }
    return processed;
  }

  /**
   * Returns the oldest request without calculations.
   * @return null or request without calculations.
   * @throws SQLException
   *
   */
  Request getOldestRequestWithoutCalculations() throws SQLException {
    try (Connection connection = registerPMF.getConnection()) {
      return RequestRepository.getOldestRequestWithoutCalculations(connection, segmentType);
    }
  }

  /**
   * Calculate the results for the given request.
   * @param request The request to calculate
   */
  void processRequest(final Request request) {
    CalculatedScenario calculatedScenario = null;
    try {
      LOG.info("Processing request calculation(s) for request_id {}, segment {}", request.getId(), segmentType);
      calculatedScenario = initScenario(request);
      final CalculationInputData inputData = new CalculationInputData();
      inputData.setScenario(calculatedScenario);
      inputData.setQueueName(QueueEnum.PERMIT.getQueueName());

      finish(request, calculatorBuildDirector.construct(inputData, request.getReference(), null).calculate());
      LOG.info("Done processing request calculation(s) for request_id {}, segment {}", request.getId(), segmentType);
    } catch (final Exception e) {
      try {
        cleanUpOnError(request.getId(), calculatedScenario, e);
      } catch (final SQLException e1) {
        LOG.error("cleanUpOnError failed", e1);
      }
    }
  }

  /**
   * Initializes the calculated scenario object.
   * @param request request to get calculated scenario object for
   * @return new scenario object.
   * @throws SQLException
   * @throws AeriusException
   * @throws IOException
   */
  CalculatedScenario initScenario(final Request request) throws SQLException, AeriusException, IOException {
    final ImportResult importResult = getScenario(request);
    final int situationId2 = importResult.getSourceLists().size() == 2 ? 1 : -1;
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(importResult, 0, situationId2);
    initRegisterCalculationOptions(cs, request.getStartYear(), request.getTemporaryPeriod(), null);
    if (priorityProjectSet.contains(request.getSegment())) {
      cs.getOptions().setPermitCalculationRadiusType(importResult.getPermitCalculationRadiusType());
    }

    return cs;
  }

  /**
   * Get the ImportResult as scenario from the database
   * @param request The request to get the scenario object for.
   * @return scenario object
   * @throws SQLException
   * @throws IOException
   * @throws AeriusException
   */
  ImportResult getScenario(final Request request) throws SQLException, IOException, AeriusException {
    final byte[] fileContent;
    RequestFile applicationFile = null;
    try (final Connection con = registerPMF.getConnection()) {
      for (final RequestFile requestFile : RequestRepository.getExistingRequestFiles(con, request.getReference())) {
        if (requestFile.getRequestFileType() == RequestFileType.APPLICATION) {
          applicationFile = requestFile;
        }
      }
      fileContent = RequestRepository.getRequestFileContent(con, request.getReference(), RequestFileType.APPLICATION);
    }
    if (fileContent == null || applicationFile == null) {
      LOG.error("Could not find file for segment {} for request with reference {}", segmentType, request.getReference());
      throw new AeriusException(Reason.INTERNAL_ERROR, request.getReference());
    }
    return getScenario(fileContent, applicationFile);
  }

  private ImportResult getScenario(final byte[] fileContent, final RequestFile applicationFile) throws IOException, SQLException, AeriusException {
    try (final InputStream inputStream = new ByteArrayInputStream(fileContent)) {
      final Importer importer = new Importer(registerPMF);
      //do not import calculation points.
      importer.setIncludeCalculationPoints(false);
      final String dummyFilename = "dummy." + applicationFile.getFileFormat().getExtension();
      return importer.convertInputStream2ImportResult(dummyFilename, inputStream);
    }
  }

  /**
   * Force specific Register request calculation options.
   * @param scenario scenario to force
   * @param year year to set as calculation year
   */
  void initRegisterCalculationOptions(final CalculatedScenario scenario, final int year, final Integer temporaryPeriod,
      final PermitCalculationRadiusType permitCalculationRadiusType) {
    scenario.setOptions(PASUtil.getCalculationSetOptions(temporaryPeriod, permitCalculationRadiusType));
    scenario.setYear(year);
  }

  /**
   * Inserts specific calculation situation data into the database
   * @param connection the database connection
   * @param request the request
   * @param cs the calculuated scenario
   * @throws SQLException
   */
  void insertSituation(final Connection connection, final Request request, final CalculatedScenario cs) throws SQLException {
    if (cs instanceof CalculatedComparison) {
      RequestModifyRepository.insertSituationCalculation(connection, SituationType.CURRENT,
          ((CalculatedComparison) cs).getCalculationIdOne(), request);
      RequestModifyRepository.insertSituationCalculation(connection, SituationType.PROPOSED,
          ((CalculatedComparison) cs).getCalculationIdTwo(), request);
    } else if (cs instanceof CalculatedSingle) {
      RequestModifyRepository.insertSituationCalculation(connection, SituationType.PROPOSED,
          ((CalculatedSingle) cs).getCalculationId(), request);
    }
  }

  /**
   * Changes the state of the request to 'queued'.
   * The request is retrieved again, so that we get the latest changes and last-modified time. Otherwise if the user has
   * changed some properties, the updateState call will fail.
   * @param request the request to change.
   * @param calculationJob
   * @throws SQLException
   * @throws AeriusException
   */
  void finish(final Request request, final CalculationJob calculationJob) throws SQLException, AeriusException {
    try (Connection connection = registerPMF.getConnection()) {
      insertSituation(connection, request, calculationJob.getCalculatedScenario());
      final UserProfile up = getUserProfile(connection);
      RequestModifyRepository.updateRequestToQueued(connection, request, up);
    }
  }

  /**
   * In case of calculation errors remove the results from the database.
   * @param requestId id to remove the results for.
   * @param e error generated.
   * @throws SQLException
   */
  void cleanUpOnError(final int requestId, final CalculatedScenario calculatedScenario, final Exception e) throws SQLException {
    LOG.error("Processing request with id {} failed", requestId, e);
    if (calculatedScenario != null) {
      try (Connection connection = registerPMF.getConnection()) {
        for (final Calculation c : calculatedScenario.getCalculations()) {
          RequestModifyRepository.removeCalculationReference(connection, c.getCalculationId());
          CalculationRepository.removeCalculation(connection, c.getCalculationId());
        }
      }
    }
  }
}
