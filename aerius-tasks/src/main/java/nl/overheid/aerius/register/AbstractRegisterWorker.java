/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.sql.Connection;
import java.sql.SQLException;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * Base class for register workers. Contains a specific username for register system user and a Register PMF.
 */
abstract class AbstractRegisterWorker {

  /**
   * User name of the register worker.
   */
  private static final String REGISTER_WORKER_USERNAME = "register_worker";

  protected final PMF registerPMF;

  /**
   * Constructor.
   * @param configuration The database configuration
   * @throws AeriusException
   */
  public AbstractRegisterWorker(final DBWorkerConfiguration configuration) throws AeriusException {
    registerPMF = WorkerPMFFactory.createPMF(configuration);
  }

  protected UserProfile getUserProfile(final Connection connection) throws SQLException, AeriusException {
    return UserRepository.getUserProfileByName(connection, REGISTER_WORKER_USERNAME);
  }
}
