/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.register.SegmentType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.base.ScheduledWorkerFactory;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * Factory to start a {@link RequestCalculationWorker}.
 */
public class RequestCalculationWorkerFactory implements ScheduledWorkerFactory<DBWorkerConfiguration> {

  private final SegmentType segmentType;

  public RequestCalculationWorkerFactory(final SegmentType segmentType) {
    this.segmentType = segmentType;
  }

  @Override
  public DBWorkerConfiguration createConfiguration(final Properties properties) {
    return new DBWorkerConfiguration(properties, ProductType.REGISTER, WorkerType.REGISTER);
  }

  @Override
  public Runnable createScheduledWorker(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory)
      throws IOException, AeriusException, SQLException {
    return new RequestCalculationWorker(configuration, factory, segmentType);
  }

  @Override
  public boolean isEnabled(final DBWorkerConfiguration configuration) {
    return configuration.isPermitCalculationWorker(segmentType);
  }
}
