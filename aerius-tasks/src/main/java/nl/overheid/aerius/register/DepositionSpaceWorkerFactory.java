/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.IOException;
import java.util.Properties;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.base.ScheduledWorkerFactory;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;

/**
 * Factory to start a {@link DepositionSpaceWorker}.
 */
public class DepositionSpaceWorkerFactory implements ScheduledWorkerFactory<DBWorkerConfiguration> {

  private DBWorkerConfiguration calculatorConfiguration;

  @Override
  public DBWorkerConfiguration createConfiguration(final Properties properties) {
    calculatorConfiguration = new DBWorkerConfiguration(properties, ProductType.CALCULATOR, WorkerType.CALCULATOR);
    return new DBWorkerConfiguration(properties, ProductType.REGISTER, WorkerType.REGISTER);
  }

  @Override
  public Runnable createScheduledWorker(final DBWorkerConfiguration configuration, final BrokerConnectionFactory factory)
      throws IOException, AeriusException {
    return new DepositionSpaceWorker(configuration, calculatorConfiguration);
  }

  @Override
  public boolean isEnabled(final DBWorkerConfiguration configuration) {
    return configuration.isDepositionSpaceWorker();
  }
}
