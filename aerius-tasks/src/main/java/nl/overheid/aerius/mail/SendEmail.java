/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataSource;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.postgresql.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.StringDataSource;
import nl.overheid.aerius.util.EnvUtils;

/**
 *
 */
public final class SendEmail {

  // The logger.
  private static final Logger LOG = LoggerFactory.getLogger(SendEmail.class);

  private static final String ENV_MAIL_HOST = "mail.host";
  private static final String DEFAULT_MAIL_HOST = "localhost";


  private static final Pattern REGEXP_HTML_LINK = Pattern.compile("\\<a href=\\\"(\\S*)\\\".*\\>(.*)\\<\\/a\\>");
  private static final Pattern REGEXP_HTML_TAGS = Pattern.compile("\\<.*?\\>");
  private static final String REGEXP_BASE64_IMAGES = " src=\"data:(image/[\\w]+);base64,([^\"]+)\"";

  private SendEmail() {
    // Don't allow to instantiate.
  }

  /**
   * @param from Email address from.
   * @param toAddresses Email addresses to
   * @param ccAddresses CC addresses to.
   * @param bccAddresses BCC addresses to.
   * @param subject Subject of the email.
   * @param body Body of the email.
   * @param attachments Any datasources that need to be attached to the email.
   * The string value is used for the filename header field.
   * @return True if email was send.
   * @throws IOException
   */
  public static boolean send(final String from, final List<String> toAddresses, final List<String> ccAddresses, final List<String> bccAddresses,
      final String subject, final String body, final List<StringDataSource> attachments) throws IOException {
    try {
      final HtmlEmail email = new HtmlEmail();
      email.setHostName(EnvUtils.getEnvProperty(ENV_MAIL_HOST, DEFAULT_MAIL_HOST));
      email.setFrom(from);
      setToAddresses(email, toAddresses, ccAddresses, bccAddresses);
      email.setSubject(subject);

      setBody(email, body);

      attach(email, attachments);

      email.send();
    } catch (final EmailException e) {
      LOG.error("Could not send email:", e);
      return false;
    }
    return true;
  }

  private static void setToAddresses(final HtmlEmail email, final List<String> toAddresses, final List<String> ccAddresses,
      final List<String> bccAddresses) throws EmailException {
    if (toAddresses != null) {
      for (final String to : toAddresses) {
        email.addTo(to);
      }
    }
    if (ccAddresses != null) {
      for (final String cc : ccAddresses) {
        email.addCc(cc);
      }
    }
    if (bccAddresses != null) {
      for (final String bcc : bccAddresses) {
        email.addBcc(bcc);
      }
    }
  }

  private static void setBody(final HtmlEmail email, final String body) throws EmailException {
    final String actualBody = embedBase64Images(email, body);

    email.setHtmlMsg(actualBody);
    // Add a poor mans no-HTML version of the mail - just in case someone uses an iPhone/dumbphone and such.
    email.setTextMsg(getPlainTextMsg(actualBody));
  }

  private static String embedBase64Images(final HtmlEmail email, final String body) throws EmailException {
    // Check if there are base64 encoded images in the body, if so, add them as attachments and replace them with the cid's.
    final Pattern pattern = Pattern.compile(REGEXP_BASE64_IMAGES);
    final Matcher matcher = pattern.matcher(body);
    final StringBuffer parsedBody = new StringBuffer();
    int imageCounter = 0;

    while (matcher.find()) {
      imageCounter++;
      final String mimetype = matcher.group(1);
      final String base64EncodedImage = matcher.group(2);

      String contentId;
      try {
        contentId = email.embed(createDataSource(Base64.decode(base64EncodedImage), mimetype), "image_" + imageCounter);
      } catch (final IOException e) {
        matcher.appendReplacement(parsedBody, Matcher.quoteReplacement(matcher.group()));

        // Skip image.
        continue;
      }

      matcher.appendReplacement(parsedBody, Matcher.quoteReplacement(" src=\"cid:" + contentId + "\""));
    }
    matcher.appendTail(parsedBody);

    return parsedBody.toString();
  }

  private static String getPlainTextMsg(final String body) {
    String returnValue = body;
    //for plain text mails we're stripping all HTML tags, including links.
    //download button is a link, and since it's the first link in the mail, we replace it via this next bit.
    //will work the same for mails without a download link, there's some link in signature that probably gets replaced.
    //looks a bit weird in that case, but it's only the backup version.
    final Matcher linkMatcher = REGEXP_HTML_LINK.matcher(body);
    if (linkMatcher.find()) {
      returnValue = linkMatcher.replaceFirst(linkMatcher.group(2) + " (link: " + linkMatcher.group(1) + ")");
    }
    final Matcher htmlTagMatcher = REGEXP_HTML_TAGS.matcher(returnValue);
    returnValue = htmlTagMatcher.replaceAll("");
    return returnValue;
  }

  private static void attach(final HtmlEmail email, final List<StringDataSource> attachments) throws EmailException, IOException {
    if (attachments != null) {
      for (final StringDataSource source : attachments) {
        email.attach(createDataSource(source.getBytes(), source.getMimeType()), source.getFileName(), source.getFileName());
      }
    }
  }

  /**
   * Helper method to create a datasource to use as attachment.
   * No reason you can't create your own datasource, this one is just there to help.
   * (Hides apache commons ByteArrayDataSource.)
   * @param file The byte[] of the file to use as datasource.
   * @param fileMIMEType The type of the file.
   * @return The datasource to be used as attachment.
   * @throws IOException When an error occurs creating the datasource.
   */
  public static DataSource createDataSource(final byte[] file, final String fileMIMEType) throws IOException {
    return new ByteArrayDataSource(file, fileMIMEType);
  }

}
