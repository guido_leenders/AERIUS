/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.UserGeoLayerRepository;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.worker.configuration.DBWorkerConfiguration;
import nl.overheid.aerius.worker.util.WorkerPMFFactory;

/**
 * Job cleanup Worker.
 */
public class JobCleanupWorker implements Runnable {

  private static final Logger LOG = LoggerFactory.getLogger(JobCleanupWorker.class);

  // This time will be used as the wait time to start running after the worker is started up and also between runs.
  // Between runs means after a run has finished it will be rescheduled again so it cannot run multiple times if a run takes to long.
  private static final int WAIT_TIME_IN_MINUTES = 60;

  private final PMF calculatorPMF;
  private final int cleanupAfterAmountOfDays;
  private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

  /**
   * @param calculatorConfiguration The configuration containing DB config and start time for this worker.
   * @throws AeriusException
   */
  public JobCleanupWorker(final DBWorkerConfiguration calculatorConfiguration) throws AeriusException {
    this.calculatorPMF = WorkerPMFFactory.createPMF(calculatorConfiguration);

    cleanupAfterAmountOfDays = calculatorConfiguration.getJobCleanupAfterDays();

    if (cleanupAfterAmountOfDays <= 0) {
      throw new IllegalArgumentException("The amount of days to clean jobs after isn't configured properly for the job cleanup worker, "
          + "even though the worker is enabled. Worker disabled for this run.");
    }
  }

  @Override
  public void run() {
    LOG.info(
        "Scheduling job cleanup worker to run every {} minute(s). Will delete jobs older than {} days.",
        WAIT_TIME_IN_MINUTES,
        cleanupAfterAmountOfDays);
    scheduler.scheduleWithFixedDelay(new JobCleanupScheduledTask(), WAIT_TIME_IN_MINUTES, WAIT_TIME_IN_MINUTES, TimeUnit.MINUTES);
  }

  private class JobCleanupScheduledTask implements Runnable {

    @Override
    public void run() {
      LOG.info("Starting to clean up jobs.");
      try (final Connection con = calculatorPMF.getConnection()) {
        final int amountRemoved = JobRepository.removeJobsWithMinAge(con, cleanupAfterAmountOfDays);
        final int userGeolayerRemoved = UserGeoLayerRepository.removeImportsWithMinAge(con, cleanupAfterAmountOfDays);
        LOG.info("Done cleaning up jobs. Amount removed: Connect Jobs : {}, UserGeoLayers : {}", amountRemoved, userGeolayerRemoved);
      } catch (final SQLException e) {
        LOG.error("Error while cleaning up jobs.", e);
      }
    }
  }

}
