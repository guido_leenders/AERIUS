{import_common 'system/sld.sql'}

--Product specific SLD's,
INSERT INTO system.wms_zoom_levels (wms_zoom_level_id,name) VALUES (301,'Bijlage bij besluit - standaard');

INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (301,0,30000,1);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (301,30000,50000,2);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (301,50000,75000,3);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (301,75000,150000,4);
INSERT INTO system.wms_zoom_level_properties (wms_zoom_level_id,min_scale,max_scale,zoom_level) VALUES (301,150000,3000000,5);

--Register, ranged between 3001-3999
INSERT INTO system.sld (sld_id, description) VALUES (3001,'Beschikbare depositieruimte');
INSERT INTO system.sld (sld_id, description) VALUES (3002,'Projectbehoefte');
--REMEMBER: update when new SLDs are added: next sld_id = 3003


INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space >= 0 && available_development_space <= 30','FFEBDA','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space > 30 && available_development_space <= 60','FFD6B6','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space > 60 && available_development_space <= 90','FFC292','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space > 90 && available_development_space <= 120','FFAE6D','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space > 120 && available_development_space <= 150','FF9A49','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space > 150 && available_development_space <= 200','FF8524','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3001,'available_development_space > 200','FF7100','FFFFFF',null,null,null);

INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 0 && development_space_demand < 0.05','FFFDB3','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 0.05 && development_space_demand < 1','FDE76A','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 1 && development_space_demand < 3','FEB66E','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 3 && development_space_demand < 5','A5CC46','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 5 && development_space_demand < 7','23A870','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 7 && development_space_demand < 10','5A7A32','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 10 && development_space_demand < 15','0093BD','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 15 && development_space_demand < 20','0D75B5','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 20 && development_space_demand < 30','6A70B1','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 30 && development_space_demand < 50','304594','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 50 && development_space_demand < 75','7F3B17','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 75 && development_space_demand < 100','5E2C8F','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 100 && development_space_demand < 150','3F2A84','FFFFFF',null,null,null);
INSERT INTO system.sld_rules (sld_id,condition,fill_color,stroke_color,custom_draw_sld,custom_condition_sld,image_url) VALUES (3002,'development_space_demand >= 150','2A1612','FFFFFF',null,null,null);

--Product specific WMS layers. Can refer to sld_id 1-999 (common) and 3001-3999 (register).
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2,2,'register:wms_habitat_areas_sensitivity_view','Stikstofgevoelige habitattypen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (3,2,'register:wms_nature_areas_view','Natuurgebieden');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (4,2,'register:wms_nature_areas_no_names_view','Natuurgebieden (PDF, zonder labels)','register:wms_nature_areas_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (5,2,'register:wms_nature_areas_names_only_view','Natuurgebieden (PDF, alleen labels)','register:wms_nature_areas_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (11,301,'register_report:wms_exceeding_calculation_deposition_results_view','Berekeningsresultaten (alleen op plekken van overschrijding)');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (12,301,'register_report:wms_calculations_deposition_results_difference_view','Verschil projectbijdrage');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (11,301,'register_report:wms_country_exceeding_calculation_deposition_results_view','Berekeningsresultaten per land(alleen op plekken van overschrijding)');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (12,301,'register_report:wms_country_calculations_deposition_results_difference_view','Verschil projectbijdrage per land');

INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (13,2,'register_report:wms_shipping_maritime_network_view','Scheepvaart netwerk');

INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (3001,1,'register:wms_available_development_spaces_view','Beschikbare ruimte');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (3002,1,'register:wms_permit_demands_view','Projectbehoefte');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (14,5,'register:wms_province_areas_view', 'Provincegrenzen');
