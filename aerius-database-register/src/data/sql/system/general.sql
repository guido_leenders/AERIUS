{import_common 'system/general.sql'}

{import_common 'system/sectors.sql'}
{import_common 'system/sectors_sectorgroup.sql'}

{import_common 'system/plans.sql'}

{import_common 'system/gml_conversions.sql'}


INSERT INTO system.constants (key, value) VALUES ('INTERNAL_WMS_PROXY_URL', 'https://test.aerius.nl/register/aerius-geo-wms?');
INSERT INTO system.constants (key, value) VALUES ('SHARED_WMS_PROXY_URL', 'https://test.aerius.nl/register/aerius-geo-wms?');

--URLs where geoserver can access the servlet for SLD
INSERT INTO system.constants (key, value) VALUES ('SHARED_WMS_URL', 'https://test.aerius.nl/geoserver-register/wms?');
INSERT INTO system.constants (key, value) VALUES ('GEOSERVER_CAPIBILITIES_ORIGINAL_URL', 'https://test.aerius.nl:80/geoserver-register/wms?');
INSERT INTO system.constants (key, value) VALUES ('SHARED_SLD_URL', 'http://localhost:8080/register/aerius/');

-- Salts! The application maintainer should generate his own on production.
INSERT INTO system.constants (key, value) VALUES ('SALT_REQUEST_FILE_DOWNLOAD', '');

-- Register config for the UI
INSERT INTO system.constants (key, value) VALUES ('NUMBER_OF_TASKCLIENT_THREADS', '40');

-- Register constants (temporary)
INSERT INTO system.constants (key, value) VALUES ('MANUAL_URL', 'http://www.aerius.nl/nl/manuals/register');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_FETCH_URL', 'https://test.aerius.nl/calculator/');

INSERT INTO system.constants (key, value) VALUES ('MELDING_AUTHORITIES_CENTRAL_MAIL_ADDRESS', 'aeriusmail+CentraalGezag@gmail.com');

INSERT INTO system.constants (key, value) VALUES ('CONVERT_LINE_TO_POINTS_SEGMENT_SIZE', '25');
INSERT INTO system.constants (key, value) VALUES ('CONVERT_POLYGON_TO_POINTS_GRID_SIZE', '100');
INSERT INTO system.constants (key, value) VALUES ('SHIPPING_INLAND_LOCK_HEAT_CONTENT_FACTOR', '0.15');

INSERT INTO system.constants (key, value) VALUES ('CURRENT_PAS_PERIOD_END_YEAR', '2019');

INSERT INTO system.constants (key, value) VALUES ('MAX_RADIUS_FOR_REQUESTS', '10000');

INSERT INTO system.constants (key, value) VALUES ('LAYER_SHIP_NETWORK', 'register_report:wms_shipping_maritime_network_view');


INSERT INTO system.options (option_type, value, label, default_value) VALUES ('filter_min_fraction_used', '0.50', '50%', FALSE);
INSERT INTO system.options (option_type, value, label, default_value) VALUES ('filter_min_fraction_used', '0.60', '60%', TRUE);
INSERT INTO system.options (option_type, value, label, default_value) VALUES ('filter_min_fraction_used', '0.70', '70%', FALSE);
INSERT INTO system.options (option_type, value, label, default_value) VALUES ('filter_min_fraction_used', '0.80', '80%', FALSE);
INSERT INTO system.options (option_type, value, label, default_value) VALUES ('filter_min_fraction_used', '0.90', '90%', FALSE);
INSERT INTO system.options (option_type, value, label, default_value) VALUES ('filter_min_fraction_used', '0.95', '95%', FALSE);
