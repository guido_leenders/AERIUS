{import_common 'system/layers.sql'}

--update the layer capabilities URL to be register specific.
UPDATE system.layer_capabilities SET url = 'https://test.aerius.nl/register/aerius-geo-wms?' WHERE layer_capabilities_id = 1;


--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (11, 25, 'wms', 'register:wms_permit_demands_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name) 
	VALUES (12, 28, 'wms', 'register:wms_province_areas_view');


-- default layers
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (1, 0, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (12, 1, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (3, 2, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer) 
	VALUES (4, 3, false);
