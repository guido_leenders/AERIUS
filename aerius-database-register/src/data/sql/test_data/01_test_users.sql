/*
 * Temporary inserts of example users.
 */
 -- Test users: old ones, left for testing purposes
SELECT ae_add_user('rivm',                   '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+rivm@gmail.com',       'van', 'MinIenW', 'RIVM-tst',                15, 'register_admin');
SELECT ae_add_user('ts',                     '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+ts@gmail.com',         'van', 'MinIenW', 'Toeschouwer-tst',         15, 'register_viewer');
SELECT ae_add_user('edit',                   '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+edit@gmail.com',       'van', 'MinIenW', 'Editor-tst',              15, 'register_editor');
SELECT ae_add_user('super',                  '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+superuser@gmail.com',  'van', 'MinIenW', 'Superuser-tst',           15, 'register_superuser');
SELECT ae_add_user('bij12',                  '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+bij12@gmail.com',      'van', 'MinIenW', 'BIJ12-tst',               15, 'register_special');

-- Test users: each role one and AERIUS authority
SELECT ae_add_user('test_ad_ienw',    '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+1@gmail.com',               'A.', 'AERIUS1', 'Test1',                15, 'register_admin');
SELECT ae_add_user('test_vi_ienw',    '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+2@gmail.com',               'A.', 'AERIUS2', 'Test2',                15, 'register_viewer');
SELECT ae_add_user('test_ed_ienw',    '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+3@gmail.com',               'A.', 'AERIUS3', 'Test3',                15, 'register_editor');
SELECT ae_add_user('test_su_ienw',    '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+4@gmail.com',               'A.', 'AERIUS4', 'Test4',                15, 'register_superuser');

-- Test users: super users and Drenthe/Utrecht authority
SELECT ae_add_user('test_su_drenthe',    '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+5@gmail.com',               'A.', 'AERIUS5', 'Test5',                2, 'register_superuser');
SELECT ae_add_user('test_su_utrecht',    '$shiro1$SHA-256$500000$gXBHk4Cvfhw9xWZJJaSDeg==$EbxbRqKxKZJw1DyT4n2BOH/0nJBc32PwQEwfHT+kGnM=', 'aeriusmail+6@gmail.com',               'A.', 'AERIUS6', 'Test6',                11,'register_superuser');

-- Users for register and melding workers. Needed to get audit trail when worker changes status because calculation results added.
-- Ensure this has superuser permissions with at least UPDATE_PERMIT_STATE + UPDATE_PERMIT_STATE_ENQUEUE.
SELECT ae_add_user('register_worker',           NULL,                                                                                           'register_worker@aerius.nl',            'R.', 'Register', 'Worker',              1, NULL);
SELECT ae_add_user('melding_worker',            NULL,                                                                                           'melding_worker@aerius.nl',             'M.', 'Melding', 'Worker',               1, NULL);

/*
 * Temporary inserts of the authority settings.
 */
INSERT INTO authority_settings (authority_id, setting_key, setting_value) VALUES (1,  'NO_PERMIT_NOTICE_CONFIRMATION', 'BATCH_CONFIRM');
INSERT INTO authority_settings (authority_id, setting_key, setting_value) VALUES (2,  'NO_PERMIT_NOTICE_CONFIRMATION', 'ONE_BY_ONE_CONFIRM');
INSERT INTO authority_settings (authority_id, setting_key, setting_value) VALUES (11, 'NO_PERMIT_NOTICE_CONFIRMATION', 'BATCH_CONFIRM');
