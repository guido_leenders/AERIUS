/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.postgis.PGbox2d;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.util.ILikeWhereClause;
import nl.overheid.aerius.db.util.PGisUtils;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Search database for assessment area, provinces given part of the name and return full name and geometric information.
 */
public class SearchRepository {

  private static final Logger LOG = LoggerFactory.getLogger(SearchRepository.class);

  private static final int MAX_SEARCH_DISTANCE = 10000;

  private static final String QUERY_GET_SUGGESTIONS =
      "SELECT id, display_name, type, boundingbox, "
          + " assessment_area_id, assessment_area_name, assessment_area_boundingbox, distance "
          + " FROM search_widget_results_view "
          + " WHERE search_term ILIKE ? "
          + " ORDER BY type, display_name, id, distance, assessment_area_name ";

  private static final String QUERY_GET_N2K_ID_SUGGESTION =
      "SELECT natura2000_area_id, name, Box2D(geometry) AS boundingbox FROM natura2000_areas WHERE natura2000_area_id = ? ";

  private static final String QUERY_GET_POINT_SUBSUGGESTIONS =
      "SELECT "
          + " assessment_area_id, "
          + " name AS assessment_area_name, "
          + " Box2D(geometry) AS assessment_area_boundingbox, "
          + " ST_Distance(geometry, ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()))::real AS distance "
          + " FROM assessment_areas "
          + " WHERE type = 'natura2000_area'"
          + "   AND ST_DWithin(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), geometry, " + MAX_SEARCH_DISTANCE + ") "
          + " ORDER BY distance, name ";

  private static final String QUERY_GET_AREA_SUBSUGGESTIONS =
      "SELECT "
          + " assessment_area_id, "
          + " assessment_area_name, "
          + " assessment_area_boundingbox, "
          + " distance "
          + " FROM search_widget_sub_results_view "
          + " WHERE assessment_area_type = 'natura2000_area'"
          + "   AND area_type = ?::search_area_type"
          + "   AND distance <= (CASE WHEN area_type = 'province_area' THEN 0 ELSE " + MAX_SEARCH_DISTANCE + " END) "
          + "   AND ST_Intersects(ST_SetSRID(ST_MakePoint(?, ?), ae_get_srid()), geometry) "
          + " ORDER BY distance, assessment_area_name ";

  /**
   * Returns the search suggestions from the database given a search string.
   * @param pmf database connection
   * @param searchString string to search
   * @return list of search results
   * @throws AeriusException
   */
  public static ArrayList<MapSearchSuggestion> getSuggestions(final PMF pmf, final String searchString) throws AeriusException {
    final ArrayList<MapSearchSuggestion> output = new ArrayList<>();

    try (final Connection con = pmf.getConnection();
        final PreparedStatement stmt = con.prepareStatement(QUERY_GET_SUGGESTIONS)) {
      //stmt.setQueryTimeout((int) Math.ceil(TIMEOUT_QUERY_SUGGESTIONS / 1000.0));
      stmt.setString(1, ILikeWhereClause.getSearchString(searchString));
      final ResultSet rst = stmt.executeQuery();

      MapSearchSuggestion currentItem = null;
      while (rst.next()) {
        final long id = rst.getLong("id");
        final String name = rst.getString("display_name");
        final AreaType dbAreaEnum = AreaType.fromDatabaseObjectName(rst.getString("type"));
        final MapSearchSuggestionType type = MapSearchSuggestionType.fromAreaType(dbAreaEnum);
        final BBox boundingBox = PGisUtils.getBox((PGbox2d) rst.getObject("boundingbox"));
        final float distance = rst.getFloat("distance");

        if (currentItem == null || currentItem.getId() != id) {
          currentItem = new MapSearchSuggestion(id, name, type);
          currentItem.setZoomBox(boundingBox);
          currentItem.setDistance(distance);
          output.add(currentItem);
        }

        if (type != MapSearchSuggestionType.NATURE_AREA) {
          final long natureAreaId = rst.getLong("assessment_area_id");
          final String natureName = rst.getString("assessment_area_name");
          final BBox natureBbox = PGisUtils.getBox((PGbox2d) rst.getObject("assessment_area_boundingbox"));

          final MapSearchSuggestion subItem = new MapSearchSuggestion(natureAreaId, natureName, MapSearchSuggestionType.NATURE_AREA);
          subItem.setZoomBox(natureBbox);
          subItem.setDistance(distance);

          currentItem.getSubItems().add(subItem);
        }
      }
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
    return output;
  }

  /**
   * Returns the nature areas at a distance from the given suggestion point.
   * @param pmf database connection
   * @param parentSuggestion suggestion to find nature areas for.
   * @return list of new search suggestions containing nature areas.
   * @throws AeriusException throws exception on sql error or with incorrect suggestion type.
   */
  public static ArrayList<MapSearchSuggestion> getSubSuggestions(final PMF pmf, final MapSearchSuggestion parentSuggestion) throws AeriusException {
    final ArrayList<MapSearchSuggestion> output = new ArrayList<>();

    try (final Connection con = pmf.getConnection();
        final PreparedStatement stmt = con.prepareStatement(parentSuggestion.getType().isPoint()
            ? QUERY_GET_POINT_SUBSUGGESTIONS : QUERY_GET_AREA_SUBSUGGESTIONS)) {
      final Point point = parentSuggestion.getPoint();
      if (parentSuggestion.getType().isPoint()) {
        QueryUtil.setValues(stmt, point.getX(), point.getY(), point.getX(), point.getY());
      } else {
        final AreaType areaType = parentSuggestion.getType().toAreaType();
        if (areaType == null || parentSuggestion.getType() == MapSearchSuggestionType.NATURE_AREA) {
          LOG.error("Subsuggestions for {} can't be queried.", parentSuggestion.getType());
          throw new AeriusException(Reason.INTERNAL_ERROR);
        } else {
          QueryUtil.setValues(stmt, areaType.toDatabaseObjectName(), point.getX(), point.getY());
        }
      }

      final ResultSet rst = stmt.executeQuery();
      while (rst.next()) {
        final long id = rst.getLong("assessment_area_id");
        final String name = rst.getString("assessment_area_name");
        final BBox boundingBox = PGisUtils.getBox((PGbox2d) rst.getObject("assessment_area_boundingbox"));
        final float distance = rst.getFloat("distance");

        final MapSearchSuggestion suggestion = new MapSearchSuggestion(id, name, MapSearchSuggestionType.NATURE_AREA);
        suggestion.setZoomBox(boundingBox);
        suggestion.setDistance(distance);
        output.add(suggestion);
      }
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
    return output;
  }

  /**
   * Returns the Natura 2000 area with the given id from the database as a search suggestion.
   * @param pmf database connection
   * @param n2000Id The id of the N2000 area to return
   * @return MapSearchSuggestion of the N2000 or null if nothing with that id was found
   * @throws AeriusException
   */
  public static MapSearchSuggestion getN2000IdSuggestion(final PMF pmf, final int n2000Id) throws AeriusException {
    MapSearchSuggestion MapSearchSuggestion = null;
    try (final Connection con = pmf.getConnection();
        final PreparedStatement stmt = con.prepareStatement(QUERY_GET_N2K_ID_SUGGESTION)) {
      stmt.setInt(1, n2000Id);
      final ResultSet rst = stmt.executeQuery();
      if (rst.next()) {
        final String name = rst.getString("name");
        final BBox boundingBox = PGisUtils.getBox((PGbox2d) rst.getObject("boundingbox"));
        MapSearchSuggestion = new MapSearchSuggestion(n2000Id, name + " (" + String.valueOf(n2000Id) + ")", MapSearchSuggestionType.NATURE_AREA);
        MapSearchSuggestion.setZoomBox(boundingBox);
      }
    } catch (final SQLException e) {
      LOG.error(e.getMessage(), e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
    return MapSearchSuggestion;
  }

}
