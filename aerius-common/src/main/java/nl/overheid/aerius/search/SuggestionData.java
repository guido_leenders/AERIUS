/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;

/**
 * Encapsulation used to reduce complexity of method.
 */
public class SuggestionData {

  MapSearchSuggestionType suggestionType;

  String name;

  /**
   * @return the suggestionType
   */
  MapSearchSuggestionType getSuggestionType() {
    return suggestionType;
  }

  /**
   * @param suggestionType the suggestionType to set
   */
  void setSuggestionType(final MapSearchSuggestionType suggestionType) {
    this.suggestionType = suggestionType;
  }

  boolean hasSuggestionType() {
    return suggestionType != null;
  }

  /**
   * @return the name
   */
  String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  void setName(final String name) {
    this.name = name;
  }

  boolean hasName() {
    return name != null && !name.isEmpty();
  }
  }

