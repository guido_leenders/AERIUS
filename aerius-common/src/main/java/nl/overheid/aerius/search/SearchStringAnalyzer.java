/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains various analyses of the SearchString for use
 * in Query composition and Result processing.
 */
public final class SearchStringAnalyzer {

  public static final String REGEX_PROBLEM = "There is a problem with the regular expression \"{}" + " description: {}, message: {}, index: {}";

  public static final String TEXT_DEBUG_MESSAGE = "Text \"{}\" found starting at index {} and ending at index {}.";

  private final boolean postcodeMatch;

  private final boolean shortIntegerMatch;

  private final boolean singleWordMatch;

  /**
   *
   */
  public SearchStringAnalyzer(final String searchString) {
    postcodeMatch = PostcodeMatcher.isPostcode(searchString);
    shortIntegerMatch = ShortIntegerMatcher.isShortInteger(searchString);
    singleWordMatch = SingleWordMatcher.isSingleWord(searchString);
  }

  /**
   * @return the isPostcode
   */
  boolean isPostcodeMatch() {
    return postcodeMatch;
  }

  /**
   * @return the isShortInteger
   */
  boolean isShortIntegerMatch() {
    return shortIntegerMatch;
  }

  /**
   * @return the isSingleWord
   */
  boolean isSingleWordMatch() {
    return singleWordMatch;
  }

  /**
   * Contains the logic that recognizes a Postcode.
   */
  private static final class PostcodeMatcher {

    private static final Logger LOG = LoggerFactory.getLogger(SearchStringAnalyzer.PostcodeMatcher.class);

    private static final String POSTCODE_PATTERN_STRING = "^[1-9][0-9]{3}\\s?([a-zA-Z]{2}?|)$";

    private static final Pattern POSTCODE_PATTERN = Pattern.compile(POSTCODE_PATTERN_STRING);

    private PostcodeMatcher() {
      super();
    }

    /**
     * Determines whether the start of the inputString contains at least a PC4 postcode and
     * returns the matched part.
     *
     * @param inputString
     */
    public static boolean isPostcode(final String inputString) {
      try {
        final Matcher matcher = POSTCODE_PATTERN.matcher(inputString.trim());
        return matcher.find();
      } catch (final PatternSyntaxException pse) {
        LOG.error(REGEX_PROBLEM, pse.getPattern(), pse.getDescription(), pse.getMessage(), pse.getIndex(), pse);
      }
      return false;
    }
  }

  /**
   * Contains the logic that recognizes a short Integer String.
   */
  private static final class ShortIntegerMatcher {

    private static final Logger LOG = LoggerFactory.getLogger(SearchStringAnalyzer.ShortIntegerMatcher.class);

    private static final String SHORT_INTEGER_PATTERN_STRING = "^[0-9]{1,3}$";

    private static final Pattern SHORT_INTEGER_PATTERN = Pattern.compile(SHORT_INTEGER_PATTERN_STRING);

    private ShortIntegerMatcher() {
    }

    /**
     * Determines whether the start of the inputString contains exactly a short integer.
     *
     * @param   inputString
     * @returns Whether the String is a short Integer.
     */
    public static boolean isShortInteger(final String inputString) {
      try {
        final Matcher matcher = SHORT_INTEGER_PATTERN.matcher(inputString);
        return matcher.find();
      } catch (final PatternSyntaxException pse) {
        LOG.error(REGEX_PROBLEM,
            pse.getPattern(), pse.getDescription(), pse.getMessage(), pse.getIndex(), pse);
      }
      return false;
    }
  }

  /**
   * Contains the logic that recognizes a single word.
   */
  private static final class SingleWordMatcher {

    private static final Logger LOG = LoggerFactory.getLogger(SearchStringAnalyzer.SingleWordMatcher.class);

    private static final String SINGLE_WORD_PATTERN_STRING = "^[a-zA-Z][a-zA-Z0-9]*$";

    private static final Pattern SINGLE_WORD_PATTERN = Pattern.compile(SINGLE_WORD_PATTERN_STRING);

    private SingleWordMatcher() {
    }

    /**
     * Determines whether the inputString contains a single word.
     *
     * @param inputString
     * @return true when the inputString is considered to be a single word.
     */
    static boolean isSingleWord(final String inputString) {
      final boolean result;
      try {
        final Matcher matcher = SINGLE_WORD_PATTERN.matcher(inputString);
        return matcher.find();
      } catch (final PatternSyntaxException pse) {
        result = false;
        LOG.error(REGEX_PROBLEM,
            pse.getPattern(), pse.getDescription(), pse.getMessage(), pse.getIndex(), pse);
      }
      return result;
    }
  }
}
