/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a single result as constructed by GSON.
 */
public class ResultDoc {

  private String type;
  private String postcode;
  private String straatnaam;
  private String gemeentenaam;
  private String woonplaatsnaam;
  @SerializedName("centroide_rd") private String centroideRd;
  private String weergavenaam;

  /**
   *
   */
  public ResultDoc() {
    // For GSON binding.
  }

  /**
   * @param type the type to set
   */
  void setType(final String type) {
    this.type = type;
  }

  /**
   * @param postcode the postcode to set
   */
  void setPostcode(final String postcode) {
    this.postcode = postcode;
  }

  /**
   * @param straatnaam the straatnaam to set
   */
  void setStraatnaam(final String straatnaam) {
    this.straatnaam = straatnaam;
  }

  /**
   * @param gemeentenaam the gemeentenaam to set
   */
  void setGemeentenaam(final String gemeentenaam) {
    this.gemeentenaam = gemeentenaam;
  }

  /**
   * @param woonplaatsnaam the woonplaatsnaam to set
   */
  void setWoonplaatsnaam(final String woonplaatsnaam) {
    this.woonplaatsnaam = woonplaatsnaam;
  }

  /**
   * @param centroideRd the centroide_rd to set
   */
  void setCentroideRd(final String centroideRd) {
    this.centroideRd = centroideRd;
  }

  /**
   * @return the type
   */
  String getType() {
    return type;
  }

  /**
   * @return the postcode
   */
  String getPostcode() {
    return postcode;
  }

  /**
   * @return the straatnaam
   */
  String getStraatnaam() {
    return straatnaam;
  }

  /**
   * @return the gemeentenaam
   */
  String getGemeentenaam() {
    return gemeentenaam;
  }

  /**
   * @return the woonplaatsnaam
   */
  String getWoonplaatsnaam() {
    return woonplaatsnaam;
  }

  /**
   * @return the centroide_rd
   */
  String getCentroideRd() {
    return centroideRd;
  }

  /**
   * @return the weergavenaam
   */
  String getWeergavenaam() {
    return weergavenaam;
  }

  /**
   * @param weergavenaam the weergavenaam to set
   */
  void setWeergavenaam(final String weergavenaam) {
    this.weergavenaam = weergavenaam;
  }
}
