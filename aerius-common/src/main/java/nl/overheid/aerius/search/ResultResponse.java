/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import java.util.ArrayList;

/**
 * Object containing the complete response from the locatieserver as constructed by GSON.
 */
public class ResultResponse {

  String numFound;

  String start;

  String maxScore;

  ArrayList<ResultDoc> docs;

  public ResultResponse() {
    // For GSON binding.
  }

  /**
   * @return the numFound
   */
  String getNumFound() {
    return numFound;
  }

  /**
   * @param numFound the numFound to set
   */
  void setNumFound(final String numFound) {
    this.numFound = numFound;
  }

  /**
   * @return the start
   */
  String getStart() {
    return start;
  }

  /**
   * @param start the start to set
   */
  void setStart(final String start) {
    this.start = start;
  }

  /**
   * @return the maxScore
   */
  String getMaxScore() {
    return maxScore;
  }

  /**
   * @param maxScore the maxScore to set
   */
  void setMaxScore(final String maxScore) {
    this.maxScore = maxScore;
  }

  /**
   * @return the docs
   */
  ArrayList<ResultDoc> getDocs() {
    return docs;
  }

  /**
   * @param docs the docs to set
   */
  void setDocs(final ArrayList<ResultDoc> docs) {
    this.docs = docs;
  }

}
