/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

/**
 *
 */
public final class SearchQuery {
  private static final String URL_OR = " OR ";
  private static final String QUERY_PROX1_AND = "\"%1$s\"~1 AND ";
  private static final String MUNICIPALITY_CLAUSE = "(gemeentenaam:" + QUERY_PROX1_AND + "type:gemeente)";
  private static final String PLACE_CLAUSE = "(woonplaatsnaam:" + QUERY_PROX1_AND + "type:woonplaats)";
  private static final String QUERY_EXACT_AND = "\"%1$s\" AND ";
  private static final String POSTCODE_CLAUSE = "(postcode:" + QUERY_EXACT_AND + "type:postcode)";
  private static final String ADDRESS_CLAUSE = "(" + QUERY_EXACT_AND + "type:adres)";
  private static final String STREET_CLAUSE = "(straatnaam:" + QUERY_EXACT_AND + "type:weg)";

  /**
   *
   */
  private SearchQuery() {
  }

  public static String constructQuery(final SearchStringAnalyzer searchStringAnalyzer) {
    final StringBuilder sb = new StringBuilder();
    sb.append(MUNICIPALITY_CLAUSE);
    sb.append(URL_OR);
    sb.append(PLACE_CLAUSE);
    if (searchStringAnalyzer.isPostcodeMatch()) {
      sb.append(URL_OR);
      sb.append(POSTCODE_CLAUSE);
    }
    if (!searchStringAnalyzer.isSingleWordMatch()) {
      sb.append(URL_OR);
      sb.append(ADDRESS_CLAUSE);
    }
    sb.append(URL_OR);
    sb.append(STREET_CLAUSE);
    return sb.toString();
  }
}
