/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;

/**
 * Uses PDOK locatieserver to search addresses.
 */
public class SearchLocationServer {

  private static final Logger LOG = LoggerFactory.getLogger(SearchLocationServer.class);

  private static final int TIMEOUT_LOCATIESERVER_CONNECT = 2000;
  private static final int TIMEOUT_LOCATIESERVER_READ = 3000;
  public static final int TIMEOUT_LOCATIESERVER = TIMEOUT_LOCATIESERVER_CONNECT + TIMEOUT_LOCATIESERVER_READ;
  private static final int MAX_NUMBER_RESULT_ROWS = 40;
  private static final int ONE_HUNDRED = 100;

  private static final Gson GSON = new Gson();

  /**
   * Assembles and formats the query, calls the "locatieserver" and constructs MapSearchSuggestion
   * Objects from the server response.
   */
  public ArrayList<MapSearchSuggestion> callLocatieServer(final String epsgCode, final String urlString, final String searchString) {
    final ArrayList<MapSearchSuggestion> noResults = new ArrayList<MapSearchSuggestion>();
    final SearchStringAnalyzer searchStringAnalyzer = new SearchStringAnalyzer(searchString);
    try {
      final String formattedUrlString = constructURLString(urlString, searchString, searchStringAnalyzer);
      final ResultWrapper wrapper = getResultWrapper(formattedUrlString);
      if (wrapper != null) {
        final ResultResponse response = wrapper.getResponse();
        final Iterable<ResultDoc> docsArray = response.getDocs();
        if (docsArray != null) {
          return generateMapSearchSuggestions(docsArray, searchStringAnalyzer);
        }
      }
      return noResults;
    } catch (IOException | JsonIOException | JsonSyntaxException e) {
      LOG.error("No search results because of Exception-al event.", e);
      return noResults;
    }
  }

  /**
   * @param urlString
   * @param searchString
   * @param searchStringAnalyzer
   * @return
   * @throws UnsupportedEncodingException
   */
  private String constructURLString(final String urlString, final String searchString, final SearchStringAnalyzer searchStringAnalyzer)
      throws UnsupportedEncodingException {
    final String queryString = SearchQuery.constructQuery(searchStringAnalyzer);
    final String encodedFilledQueryString = URLEncoder.encode(String.format(queryString, searchString.trim()), StandardCharsets.UTF_8.toString());
    return String.format(urlString, encodedFilledQueryString, MAX_NUMBER_RESULT_ROWS);
  }

  /**
   * @param urlString
   * @return resultWrapper
   * @throws IOException
   */
  private ResultWrapper getResultWrapper(final String urlString) throws IOException {
    final URL url = new URL(urlString);
    final HttpURLConnection connection = openAndConnect(url);
    final Charset charset = JSON2SuggestionUtil.getCharsetFromContentType(connection.getContentType());
    try {
      try (final InputStream inputStream = (InputStream) connection.getContent();
          final InputStreamReader sr = new InputStreamReader(inputStream, charset);
          final BufferedReader reader = new BufferedReader(sr)) {
        return GSON.fromJson(reader, ResultWrapper.class);
      }
    } catch (final IOException e) {
      handleError(connection, charset);
      throw e;
    }
  }

  /**
   * @param url
   * @return
   * @throws IOException
   */
  private HttpURLConnection openAndConnect(final URL url) throws IOException {
    final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setConnectTimeout(TIMEOUT_LOCATIESERVER_CONNECT);
    connection.setReadTimeout(TIMEOUT_LOCATIESERVER_READ);
    connection.connect();
    return connection;
  }

  private void handleError(final HttpURLConnection connection, final Charset charset) throws IOException {
    final int respCode = connection.getResponseCode();
    final StringBuilder sb = new StringBuilder();
    try (final InputStream errorStream = connection.getErrorStream();
        final InputStreamReader sr = new InputStreamReader(errorStream, charset);
        final BufferedReader errorReader = new BufferedReader(sr)) {
      final char[] charArray = new char[ONE_HUNDRED];
      int charsRead = errorReader.read(charArray);
      if (charsRead > 0) {
        do {
          sb.append(charArray, 0, charsRead);
          charsRead = errorReader.read(charArray);
        } while (charsRead > 0);
      }
    }
    LOG.error("HTTP-error - Responsecode: {}, message: {}.", respCode, sb.toString());
  }

  /**
   * @param docsArray
   * @param searchSuggestionList
   */
  private ArrayList<MapSearchSuggestion> generateMapSearchSuggestions(final Iterable<ResultDoc> docsArray,
      final SearchStringAnalyzer searchStringAnalyzer) {
    final ArrayList<MapSearchSuggestion> searchSuggestionList = new ArrayList<>();
    int suggestionId = 1;
    for (final ResultDoc doc : docsArray) {
      final MapSearchSuggestion searchSuggestion =
          JSON2SuggestionFactory.createMapSearchSuggestion(doc, searchStringAnalyzer);
      if (searchSuggestion != null) {
        searchSuggestion.setId(suggestionId);
        searchSuggestionList.add(searchSuggestion);
        suggestionId++;
      }
    }
    return searchSuggestionList;
  }
}
