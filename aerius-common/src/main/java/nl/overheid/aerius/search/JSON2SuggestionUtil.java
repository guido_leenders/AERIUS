/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.Point;

/**
 *
 */
final class JSON2SuggestionUtil {

  private static final Logger LOG = LoggerFactory.getLogger(JSON2SuggestionUtil.class);

  private static final Pattern CHARSET_PATTERN = Pattern.compile("(?i)\\bcharset=\\s*\"?([^\\s;\"]*)");

  private static final Pattern POINT_PATTERN = Pattern.compile("[(\\s)]");

  private static final int EXPECTED_NUM_PARTS = 3;

  private static final int X_INDEX = 1;

  private static final int Y_INDEX = 2;

  /**
   *
   */
  private JSON2SuggestionUtil() {
  }

  /**
  * Parse out a charset from a content type header.
  *
  * @param contentType
  *            e.g. "text/html; charset=EUC-JP"
  * @return Charset constructed from supported contentType, UTF-8 when
  *                 other contentType, null otherwise.
  */
  public static Charset getCharsetFromContentType(final String contentType) {
    if (contentType == null) {
      return null;
    }
    Charset foundCharset = null;
    final Matcher m = CHARSET_PATTERN.matcher(contentType);
    if (m.find()) {
      final String charsetName = m.group(1).trim().toUpperCase(Locale.ENGLISH);
      try {
        foundCharset = Charset.forName(charsetName);
      } catch (final Exception e) {
        // eat the exception, so the fallback kicks in
        LOG.warn("Unsupported charset (" + charsetName + ")found - falling back to default (UTF-8)", e);
      }
    }
    if (foundCharset == null) {
      foundCharset = Charset.forName("UTF-8");
    }
    return foundCharset;
  }

  /**
   * Constructs a Point Object for use in the MapSearchSuggestion Object
   * from the point String received from the new "locatieserver".
   *
   * @param point Locatieserver result coordinate String.
   * @return AERIUS Point Object
   */
  static Point constructPoint(final String point) {
    if (point == null) {
      return null;
    }
    Point midPoint = null;
    final String[] bits = POINT_PATTERN.split(point);
    if (bits.length == EXPECTED_NUM_PARTS) {
      midPoint = createMidPoint(bits);
    }
    return midPoint;
  }

  /**
   * Creates an AERIUS Point Object from the Strings in the parameter array.
   *
   * @param bits
   * @return Point Object, null when something exceptional happened.
   */
  private static Point createMidPoint(final String... bits) {
    final Double xCoordinate = coordinateStringToDouble(bits[X_INDEX]);
    final Double yCoordinate = coordinateStringToDouble(bits[Y_INDEX]);
    if (xCoordinate == null || yCoordinate == null) {
      return null;
    }
    return new Point(xCoordinate, yCoordinate);
  }

  /**
   * Handles the conversion from String to double.
   *
   * @param xy coordinate String
   * @return coordinate as double, 0.0 when an Exception occurs internally.
   */
  private static Double coordinateStringToDouble(final String xy) {
    try {
      return (double) Math.round(Double.parseDouble(xy));
    } catch (final NumberFormatException n) {
      LOG.warn("Unexpected centroide_rd coordinate {} from location server.", xy, n);
      return null;
    }
  }
}
