/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.user;

import java.security.SecureRandom;

/**
 * Helper class to generate a new password.
 */
public final class PasswordUtil {
    private static final int ASCII_LETTER_START = 65;
    private static final int ASCII_NUMBER_START = 49;
    private static SecureRandom secureRandom;

    private PasswordUtil() { }

    public static String createPassword() {
        final char[] builder = {
            getRandomLetter(),
            getRandomLetter(),
            getRandomLetter(),
            getRandomNumber(),
            getRandomNumber(),
            getRandomLetter(),
            getRandomLetter(),
            getRandomLetter()
        };
        return new String(builder);
    }


    private static SecureRandom getSecureRandom() {
        if (secureRandom == null) {
            secureRandom = new SecureRandom();
        }
        return secureRandom;
    }

    private static char getRandomNumber() {
        final SecureRandom sran = getSecureRandom();
        return (char) (sran.nextInt(9) + ASCII_NUMBER_START);
    }

    private static char getRandomLetter() {
        final SecureRandom sran = getSecureRandom();
        return (char) (sran.nextInt(26) + ASCII_LETTER_START);
    }
}
