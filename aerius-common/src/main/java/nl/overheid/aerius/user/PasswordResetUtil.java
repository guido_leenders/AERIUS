/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.user;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

import nl.overheid.aerius.util.SaltDigestUtil;

/**
 *
 */
public final class PasswordResetUtil {
  private static final String SALT = "kgds85mnsdazohdfdgsvpmjytr65e867sg";

  /**
   * Portion of the password that will be used to create a hash.
   */
  private static final int SECURITY_LENGTH = 8;
  private static final int SECURITY_OFFSET = 73;

  private PasswordResetUtil() {
    // NO-OP
  }

  /**
   * Generates a hash based on an email address and the (hashed) password.
   *
   * @param email
   *          The email address of the account.
   * @param userPasswordHashed
   *          The current password of the account.
   * @return The hash.
   * @throws NoSuchAlgorithmException
   *           Thrown if essential encryption algorithm could not be found.
   */
  public static String generateHash(final String email, final String userPasswordHashed) throws NoSuchAlgorithmException {
    final String hashBase = email + SALT + userPasswordHashed;

    final byte[] digestBytes = SaltDigestUtil.digestBytes(hashBase.getBytes(StandardCharsets.UTF_8));
    final String hash = Hex.encodeHexString(digestBytes).substring(SECURITY_OFFSET, SECURITY_OFFSET + SECURITY_LENGTH);

    return SaltDigestUtil.combineSecret(hash, email);
  }
}
