/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base.source.ship;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.base.geo.GML2Geometry;
import nl.overheid.aerius.gml.base.geo.GmlLineString;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 *
 */
public final class GML2Route {

  private static final Logger LOG = LoggerFactory.getLogger(GML2Route.class);

  private final GML2Geometry gml2geometry;

  public GML2Route(final int srid) {
    gml2geometry = new GML2Geometry(srid);
  }

  public ShippingRoute findRoute(final GmlLineString gmlRoute, final List<ShippingRoute> addToList, final String sourceId)
      throws AeriusException {
    try {
      final ShippingRoute route = toRoute(gmlRoute);
      for (final ShippingRoute eRoute : addToList) {
        if (eRoute.getGeometry().getWKT().equals(route.getGeometry().getWKT())) {
          return eRoute;
        }
      }
      addToList.add(route);
      return route;
    } catch (final AeriusException e) {
      LOG.error("Invalid route geometry", e);
      throw new AeriusException(Reason.GML_GEOMETRY_INVALID, sourceId);
    }
  }

  public ShippingRoute toRoute(final GmlLineString gmlRoute) throws AeriusException {
    final WKTGeometry geometry = gml2geometry.fromXMLLineString(gmlRoute);
    final ShippingRoute route = new ShippingRoute();
    route.setGeometry(geometry);
    final Point middleOfGeometry = GeometryUtil.middleOfGeometry(geometry);
    route.setX(middleOfGeometry.getX());
    route.setY(middleOfGeometry.getY());
    return route;
  }

}
