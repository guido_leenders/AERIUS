/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.source.lodging;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.v2_2.source.Emission;
import nl.overheid.aerius.gml.v2_2.source.EmissionProperty;
import nl.overheid.aerius.shared.domain.sector.category.AbstractEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmAdditionalLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingFodderMeasureCategory;
import nl.overheid.aerius.shared.domain.sector.category.FarmLodgingSystemDefinition;
import nl.overheid.aerius.shared.domain.sector.category.FarmReductiveLodgingSystemCategory;
import nl.overheid.aerius.shared.domain.source.FarmAdditionalLodgingSystem;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmFodderMeasure;
import nl.overheid.aerius.shared.domain.source.FarmLodgingCustomEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.shared.domain.source.FarmReductiveLodgingSystem;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class GML2Farm extends AbstractGML2Specific<FarmLodgingEmissionSource, FarmEmissionSource> {

  private static final Logger LOG = LoggerFactory.getLogger(GML2Farm.class);

  public GML2Farm(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public FarmEmissionSource convert(final FarmLodgingEmissionSource source) throws AeriusException {
    final FarmEmissionSource emissionValues = new FarmEmissionSource();
    for (final FarmLodgingProperty lodging : source.getFarmLodgings()) {
      emissionValues.getEmissionSubSources().add(getFarmLodging(lodging.getProperty(), source.getId()));
    }
    return emissionValues;
  }

  private FarmLodgingEmissions getFarmLodging(final AbstractFarmLodging lodging, final String sourceId) throws AeriusException {
    final FarmLodgingEmissions returnLodging;
    if (lodging instanceof CustomFarmLodging) {
      returnLodging = convertCustom((CustomFarmLodging) lodging);
    } else if (lodging instanceof FarmLodging) {
      returnLodging = convertStandard((FarmLodging) lodging, sourceId);
    } else {
      LOG.error("Don't know how to treat lodging type: {}", lodging.getClass());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    returnLodging.setAmount(lodging.getNumberOfAnimals());
    return returnLodging;
  }

  private FarmLodgingCustomEmissions convertCustom(final CustomFarmLodging customLodging) {
    final FarmLodgingCustomEmissions customEmissions = new FarmLodgingCustomEmissions();
    customEmissions.setDescription(customLodging.getDescription());
    for (final EmissionProperty emissionProperty : customLodging.getEmissionFactors()) {
      final Emission emission = emissionProperty.getProperty();
      customEmissions.getEmissionFactors().setEmission(emission.getSubstance(), emission.getValue());
    }
    return customEmissions;
  }

  private FarmLodgingStandardEmissions convertStandard(final FarmLodging standardLodging, final String sourceId) throws AeriusException {
    final FarmLodgingStandardEmissions standardEmissions = new FarmLodgingStandardEmissions();
    final String categoryCode = standardLodging.getCode();
    final FarmLodgingCategory category = getCategories().determineFarmLodgingCategoryByCode(categoryCode);
    validateCategory(category, categoryCode, sourceId);

    standardEmissions.setCategory(category);
    handleLodgingSystems(standardLodging, sourceId, standardEmissions);
    handleFodderMeasures(standardLodging, sourceId, standardEmissions);

    return standardEmissions;
  }

  private void handleLodgingSystems(final FarmLodging lodging, final String sourceId,
      final FarmLodgingStandardEmissions standardEmissions) throws AeriusException {
    for (final LodgingSystemProperty lodgingSystemProperty : lodging.getLodgingSystems()) {
      final AbstractLodgingSystem lodgingSystem = lodgingSystemProperty.getProperty();
      if (lodgingSystem instanceof AdditionalLodgingSystem) {
        final FarmAdditionalLodgingSystem resultSystem = getAdditionalSystem((AdditionalLodgingSystem) lodgingSystem, sourceId);
        standardEmissions.getLodgingAdditional().add(resultSystem);
      } else if (lodgingSystem instanceof ReductiveLodgingSystem) {
        final FarmReductiveLodgingSystem resultSystem = getReductiveSystem((ReductiveLodgingSystem) lodgingSystem, sourceId);
        standardEmissions.getLodgingReductive().add(resultSystem);
      }
    }

    //handle BWL code bit. Ït's OK if it's null.
    standardEmissions.setSystemDefinition(getSystemDefinition(lodging.getLodgingSystemDefinitionCode(),
        standardEmissions.getCategory().getFarmLodgingSystemDefinitions()));
  }

  private void handleFodderMeasures(final FarmLodging lodging, final String sourceId,
      final FarmLodgingStandardEmissions standardEmissions) throws AeriusException {
    for (final LodgingFodderMeasureProperty fodderMeasure : lodging.getFodderMeasures()) {
      final FarmFodderMeasure resultSystem = getFodderMeasure(fodderMeasure.getProperty(), sourceId, standardEmissions.getCategory());
      standardEmissions.getFodderMeasures().add(resultSystem);
    }

    //handle BWL code bit. Ït's OK if it's null.
    standardEmissions.setSystemDefinition(getSystemDefinition(lodging.getLodgingSystemDefinitionCode(),
        standardEmissions.getCategory().getFarmLodgingSystemDefinitions()));
  }

  private FarmAdditionalLodgingSystem getAdditionalSystem(final AdditionalLodgingSystem additionalSystem, final String sourceId)
      throws AeriusException {
    final FarmAdditionalLodgingSystem resultSystem = new FarmAdditionalLodgingSystem();
    resultSystem.setAmount(additionalSystem.getNumberOfAnimals());
    final FarmAdditionalLodgingSystemCategory category =
        getCategories().getFarmLodgingCategories().determineAdditionalLodgingSystemByCode(additionalSystem.getCode());
    validateCategory(category, additionalSystem.getCode(), sourceId);

    resultSystem.setCategory(category);
    resultSystem.setSystemDefinition(getSystemDefinition(additionalSystem.getLodgingSystemDefinitionCode(),
        category.getFarmLodgingSystemDefinitions()));
    return resultSystem;
  }

  private FarmReductiveLodgingSystem getReductiveSystem(final ReductiveLodgingSystem reductiveSystem, final String sourceId)
      throws AeriusException {
    final FarmReductiveLodgingSystem resultSystem = new FarmReductiveLodgingSystem();
    final FarmReductiveLodgingSystemCategory category =
        getCategories().getFarmLodgingCategories().determineReductiveLodgingSystemByCode(reductiveSystem.getCode());
    validateCategory(category, reductiveSystem.getCode(), sourceId);

    resultSystem.setCategory(category);
    resultSystem.setSystemDefinition(getSystemDefinition(reductiveSystem.getLodgingSystemDefinitionCode(),
        category.getFarmLodgingSystemDefinitions()));
    return resultSystem;
  }

  private FarmFodderMeasure getFodderMeasure(final LodgingFodderMeasure fodderMeasure, final String sourceId,
      final FarmLodgingCategory farmLodgingCategory) throws AeriusException {
    final FarmFodderMeasure resultMeasure = new FarmFodderMeasure();
    final FarmLodgingFodderMeasureCategory category =
        getCategories().getFarmLodgingCategories().determineLodgingFodderMeasureByCode(fodderMeasure.getCode());
    validateFodderMeasureCategory(category, fodderMeasure.getCode(), sourceId, farmLodgingCategory);

    resultMeasure.setCategory(category);
    return resultMeasure;
  }

  private void validateCategory(final AbstractEmissionCategory category, final String code, final String sourceId) throws AeriusException {
    if (category == null) {
      //If we can't find the code, throw exception.
      //If it is a custom lodging type, it should have been specified as being one.
      throw new AeriusException(Reason.GML_UNKNOWN_RAV_CODE, sourceId, code);
    }
  }

  private void validateFodderMeasureCategory(final FarmLodgingFodderMeasureCategory category, final String code, final String sourceId,
      final FarmLodgingCategory farmLodgingCategory) throws AeriusException {
    if (category == null) {
      throw new AeriusException(Reason.GML_UNKNOWN_PAS_MEASURE_CODE, sourceId, code);
    }
    if (!category.canApplyToFarmLodgingCategory(farmLodgingCategory)) {
      throw new AeriusException(Reason.GML_INVALID_PAS_MEASURE_CATEGORY, sourceId, code, farmLodgingCategory.getCode());
    }
  }

  private FarmLodgingSystemDefinition getSystemDefinition(final String code, final List<FarmLodgingSystemDefinition> systemDefinitions) {
    FarmLodgingSystemDefinition rightDefinition = null;
    if (code != null) {
      for (final FarmLodgingSystemDefinition systemDefinition : systemDefinitions) {
        if (code.equals(systemDefinition.getCode())) {
          rightDefinition = systemDefinition;
          break;
        }
      }
      //TODO: if rightDefinition is null, a.k.a code not found, we could give a warning instead of ignoring it.
      //      an exception is probably to much, because it's not a value necessary for calculation.
    }
    return rightDefinition;
  }

}
