/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_0.togml;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.v2_0.source.ship.MaritimeShippingProperty;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;

/**
 *
 */
class MaritimeRoute2GML extends SpecificSource2GML<MaritimeRouteEmissionSource> {

  @Override
  public nl.overheid.aerius.gml.v2_0.source.EmissionSource convert(final MaritimeRouteEmissionSource emissionSource) {
    final nl.overheid.aerius.gml.v2_0.source.ship.MaritimeShippingEmissionSource returnSource =
        new nl.overheid.aerius.gml.v2_0.source.ship.MaritimeShippingEmissionSource();
    final List<MaritimeShippingProperty> maritimeShippings = new ArrayList<>();

    for (final RouteMaritimeVesselGroup vesselGroupEmissionValues : emissionSource.getEmissionSubSources()) {
      final nl.overheid.aerius.gml.v2_0.source.ship.MaritimeShipping gmlShip =
          new nl.overheid.aerius.gml.v2_0.source.ship.MaritimeShipping();
      gmlShip.setCode(vesselGroupEmissionValues.getCategory().getCode());
      gmlShip.setShipsPerYear(vesselGroupEmissionValues.getShipMovementsPerYear());
      gmlShip.setDescription(vesselGroupEmissionValues.getName());
      maritimeShippings.add(new MaritimeShippingProperty(gmlShip));
    }
    returnSource.setMovementType(emissionSource.getMovementType());
    returnSource.setMaritimeShippings(maritimeShippings);
    return returnSource;
  }

}
