/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v0_5.source.plan;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class GML2Plan extends AbstractGML2Specific<PlanEmissionSource, nl.overheid.aerius.shared.domain.source.PlanEmissionSource> {

  /**
   * @param conversionData The conversion data to use.
   */
  public GML2Plan(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public nl.overheid.aerius.shared.domain.source.PlanEmissionSource convert(final PlanEmissionSource source) throws AeriusException {
    final nl.overheid.aerius.shared.domain.source.PlanEmissionSource emissionSource =
        new nl.overheid.aerius.shared.domain.source.PlanEmissionSource();
    int i = 0;
    for (final PlanProperty planProperty : source.getPlans()) {
      final Plan plan = planProperty.getProperty();
      final PlanEmission planEmission = new PlanEmission();
      planEmission.setId(i++);
      final PlanCategory category = getCategories().determinePlanEmissionCategoryByCode(plan.getCode());
      if (category == null) {
        //If we can't find the code, throw exception.
        throw new AeriusException(Reason.GML_UNKNOWN_PLAN_CODE, source.getId(), plan.getCode());
      }
      planEmission.setCategory(category);
      planEmission.setAmount(plan.getAmount());
      planEmission.setName(plan.getDescription());
      emissionSource.getEmissionSubSources().add(planEmission);
    }
    return emissionSource;
  }

}
