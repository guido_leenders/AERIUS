/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v0_5.source.ship;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import nl.overheid.aerius.gml.v0_5.base.CalculatorSchema;
import nl.overheid.aerius.gml.v0_5.geo.LineString;

/**
 *
 */
@XmlType(name = "MooringMaritimeShippingType", namespace = CalculatorSchema.NAMESPACE,
    propOrder = { "shipsPerYear", "averageResidenceTime", "inlandRoute", "maritimeRoutes" })
public class MooringMaritimeShipping extends AbstractShipping {

  private int shipsPerYear;
  private int averageResidenceTime;
  private LineString route;
  private List<MaritimeShippingRouteProperty> maritimeRoutes = new ArrayList<>();

  @XmlElement(name = "shipsPerYear", namespace = CalculatorSchema.NAMESPACE)
  public int getShipsPerYear() {
    return shipsPerYear;
  }

  public void setShipsPerYear(final int shipsPerYear) {
    this.shipsPerYear = shipsPerYear;
  }

  @XmlElement(name = "averageResidenceTime", namespace = CalculatorSchema.NAMESPACE)
  public int getAverageResidenceTime() {
    return averageResidenceTime;
  }

  public void setAverageResidenceTime(final int averageResidenceTime) {
    this.averageResidenceTime = averageResidenceTime;
  }

  @XmlElement(name = "inlandRoute", namespace = CalculatorSchema.NAMESPACE)
  public LineString getInlandRoute() {
    return route;
  }

  public void setInlandRoute(final LineString route) {
    this.route = route;
  }

  @XmlElement(name = "maritimeRoute", namespace = CalculatorSchema.NAMESPACE)
  public List<MaritimeShippingRouteProperty> getMaritimeRoutes() {
    return maritimeRoutes;
  }

  public void setMaritimeRoutes(final List<MaritimeShippingRouteProperty> maritimeRoutes) {
    this.maritimeRoutes = maritimeRoutes;
  }

}
