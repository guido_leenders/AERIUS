/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v0_5.source.road;

import javax.xml.bind.annotation.XmlElementRef;

import nl.overheid.aerius.gml.base.AbstractProperty;

/**
 *
 */
public class PartialChangeProperty extends AbstractProperty<AbstractLinearReference> {

  /**
   * Default constructor, needed for JAXB.
   */
  public PartialChangeProperty() {
    super(null);
  }

  /**
   * Convenience constructor.
   * @param linearReference The property to use.
   */
  public PartialChangeProperty(final AbstractLinearReference linearReference) {
    super(linearReference);
  }

  @XmlElementRef
  @Override
  public AbstractLinearReference getProperty() {
    return super.getProperty();
  }

  @Override
  public void setProperty(final AbstractLinearReference property) {
    super.setProperty(property);
  }
}
