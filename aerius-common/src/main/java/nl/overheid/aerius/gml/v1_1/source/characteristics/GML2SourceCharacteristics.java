/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_1.source.characteristics;

import java.util.Map;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Utility class to convert to and from GML objects (specific for source characteristics).
 */
public final class GML2SourceCharacteristics {

  private GML2SourceCharacteristics() {
    // Util class.
  }

  public static OPSSourceCharacteristics fromGML(final EmissionSourceCharacteristics characteristics, final Sector sector,
      final Map<Integer, DiurnalVariationSpecification> specs) {
    final OPSSourceCharacteristics defaultCharacteristics = sector == null ? new OPSSourceCharacteristics()
        : sector.getDefaultCharacteristics();
    final OPSSourceCharacteristics returnCharacteristics = new OPSSourceCharacteristics();
    if (characteristics.getDiurnalVariation() == null) {
      returnCharacteristics.setDiurnalVariationSpecification(defaultCharacteristics.getDiurnalVariationSpecification());
    } else {
      returnCharacteristics.setDiurnalVariationSpecification(specs.get(characteristics.getDiurnalVariation().getOption()));
    }
    returnCharacteristics.setHeatContent(characteristics.getHeatContent());
    returnCharacteristics.setEmissionHeight(characteristics.getEmissionHeight());
    if ((sector != null) && sector.getProperties().isBuildingPossible()) {
      returnCharacteristics.setBuildingHeight(getOrDefault(characteristics.getBuildingHeight(), defaultCharacteristics.getBuildingHeight()));
    }
    returnCharacteristics.setSpread(getOrDefault(characteristics.getSpread(), defaultCharacteristics.getSpread()));
    returnCharacteristics.setParticleSizeDistribution(defaultCharacteristics.getParticleSizeDistribution());
    final HeatContentSpecification heatContentSpecification = characteristics.getHeatContentSpecification();
    if (heatContentSpecification != null) {
      returnCharacteristics.setEmissionTemperature(heatContentSpecification.getEmissionTemperature());
      returnCharacteristics.setOutflowVelocity(heatContentSpecification.getOutflowVelocity());
      // Not copying the outflow surface:
      // we'd have to do our own conversion from surface to radius, better to leave that up to the user.
    }
    return returnCharacteristics;
  }

  private static <T extends Number> T getOrDefault(final T value, final T defaultValue) {
    return value == null ? defaultValue : value;
  }
}
