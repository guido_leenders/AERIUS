/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_1.togml;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.base.geo.Geometry2GML;
import nl.overheid.aerius.gml.v2_1.base.CalculatorSchema;
import nl.overheid.aerius.gml.v2_1.geo.LineString;
import nl.overheid.aerius.gml.v2_1.source.TimeUnit;
import nl.overheid.aerius.gml.v2_1.source.ship.MaritimeShippingRoute;
import nl.overheid.aerius.gml.v2_1.source.ship.MaritimeShippingRouteProperty;
import nl.overheid.aerius.gml.v2_1.source.ship.MooringMaritimeShippingProperty;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
class MaritimeMooring2GML extends SpecificSource2GML<MaritimeMooringEmissionSource> {

  private final Geometry2GML geometry2gml;

  public MaritimeMooring2GML(final Geometry2GML geometry2gmL) {
    this.geometry2gml = geometry2gmL;
  }

  @Override
  public nl.overheid.aerius.gml.v2_1.source.EmissionSource convert(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    final nl.overheid.aerius.gml.v2_1.source.ship.MooringMaritimeShippingEmissionSource returnSource =
        new nl.overheid.aerius.gml.v2_1.source.ship.MooringMaritimeShippingEmissionSource();
    final List<MooringMaritimeShippingProperty> mooringMaritimeShippings = new ArrayList<>();

    for (final MooringMaritimeVesselGroup vesselGroupEmissionValues : emissionSource.getEmissionSubSources()) {
      final nl.overheid.aerius.gml.v2_1.source.ship.MooringMaritimeShipping gmlShip =
          new nl.overheid.aerius.gml.v2_1.source.ship.MooringMaritimeShipping();
      gmlShip.setCode(vesselGroupEmissionValues.getCategory().getCode());
      gmlShip.setShipsPerTimeUnit(vesselGroupEmissionValues.getNumberOfShipsPerTimeUnit());
      gmlShip.setTimeUnit(TimeUnit.from(vesselGroupEmissionValues.getTimeUnit()));
      gmlShip.setDescription(vesselGroupEmissionValues.getName());
      gmlShip.setAverageResidenceTime(vesselGroupEmissionValues.getResidenceTime());
      gmlShip.setInlandRoute(toInlandRoute(vesselGroupEmissionValues.getInlandRoute(), emissionSource.getId(), vesselGroupEmissionValues.getId()));
      gmlShip.setMaritimeRoutes(
          toShippingRouteProperties(vesselGroupEmissionValues.getMaritimeRoutes(), emissionSource.getId(), vesselGroupEmissionValues.getId()));
      //we're not adding emissionfactor/description to avoid impression that it will be used on import.
      mooringMaritimeShippings.add(new MooringMaritimeShippingProperty(gmlShip));
    }
    returnSource.setShips(mooringMaritimeShippings);
    return returnSource;
  }

  private LineString toInlandRoute(final ShippingRoute shippingRoute, final int sourceId, final int vesselGroupId) throws AeriusException {
    LineString lineString = null;
    if (shippingRoute != null && shippingRoute.getGeometry() != null) {
      lineString = geometry2gml.toXMLLineString(shippingRoute.getGeometry(), new LineString());
      lineString.getGMLLineString().setId(CalculatorSchema.GML_ID_NAMESPACE + ".InlandRoute." + sourceId
          + ID_SEPARATOR + vesselGroupId + ID_SEPARATOR + shippingRoute.getId());
    }
    return lineString;
  }

  private List<MaritimeShippingRouteProperty> toShippingRouteProperties(final List<MaritimeRoute> originalRoutes, final int sourceId,
      final int vesselGroupId) throws AeriusException {
    final List<MaritimeShippingRouteProperty> maritimeRoutes = new ArrayList<>();
    //keep track of an ID so we won't violate unique ID constraints.
    //still depends on the id of vesselGroupEmissionValues being set right in UI however.
    int currentId = 0;
    for (final MaritimeRoute mr : originalRoutes) {
      final MaritimeShippingRoute maritimeRoute = new MaritimeShippingRoute();
      final LineString route = geometry2gml.toXMLLineString(mr.getRoute().getGeometry(), new LineString());
      route.getGMLLineString().setId(CalculatorSchema.GML_ID_NAMESPACE
          + ".MaritimeRoute." + sourceId + ID_SEPARATOR + vesselGroupId + ID_SEPARATOR + currentId);
      maritimeRoute.setRoute(route);
      maritimeRoute.setShippingMovementsPerTimeUnit(mr.getShipMovementsPerTimeUnit());
      maritimeRoute.setTimeUnit(TimeUnit.from(mr.getTimeUnit()));
      maritimeRoutes.add(new MaritimeShippingRouteProperty(maritimeRoute));
      currentId++;
    }
    return maritimeRoutes;
  }

}
