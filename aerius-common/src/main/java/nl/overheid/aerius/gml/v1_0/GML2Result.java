/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_0;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.gml.base.geo.GML2Geometry;
import nl.overheid.aerius.gml.v1_0.result.AbstractCalculationPoint;
import nl.overheid.aerius.gml.v1_0.result.CustomCalculationPoint;
import nl.overheid.aerius.gml.v1_0.result.ReceptorPoint;
import nl.overheid.aerius.gml.v1_0.result.Result;
import nl.overheid.aerius.gml.v1_0.result.ResultProperty;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Utility class to convert GML objects to result objects.
 */
final class GML2Result {

  private static final Logger LOG = LoggerFactory.getLogger(GML2Result.class);

  private final GML2Geometry gml2geometry;
  private final ReceptorUtil receptorUtil;

  /**
   * @param srid expected srid
   */
  public GML2Result(final int srid, final ReceptorUtil receptorUtil) {
    gml2geometry = new GML2Geometry(srid);
    this.receptorUtil = receptorUtil;
  }

  /**
   * Convert GML Calculation/Result points to AeriusPoints (custom or not).
   * @param calculationPoint The GML-representation of a calculation point.
   * @param includeResults if true get the results from the GML point
   * @return The receptor point.
   * @throws AeriusException When an exception occurs parsing.
   */
  public AeriusPoint fromGML(final AbstractCalculationPoint calculationPoint, final boolean includeResults) throws AeriusException {
    final AeriusPoint returnPoint = includeResults ? new AeriusResultPoint() : new AeriusPoint();
    if (calculationPoint instanceof CustomCalculationPoint) {
      fillAeriusPoint((CustomCalculationPoint) calculationPoint, returnPoint);
    } else if (calculationPoint instanceof ReceptorPoint) {
      fillAeriusPoint((ReceptorPoint) calculationPoint, returnPoint);
    } else {
      throw new AeriusException(Reason.INTERNAL_ERROR, "Could not handle calculation point: " + calculationPoint.getClass());
    }
    if (returnPoint instanceof AeriusResultPoint) {
      includeResults((AeriusResultPoint) returnPoint, calculationPoint);
    }
    return returnPoint;
  }

  private void fillAeriusPoint(final CustomCalculationPoint origin, final AeriusPoint target) throws AeriusException {
    final Point point;
    try {
      point = GeometryUtil.getPoint(gml2geometry.getGeometry(origin).getWKT());
    } catch (final AeriusException e) {
      LOG.trace("Geometry exception, rethrown as AeriusException", e);
      throw new AeriusException(Reason.GML_GEOMETRY_INVALID, origin.getId());
    }
    target.setX(Math.round(point.getX()));
    target.setY(Math.round(point.getY()));
    target.setLabel(origin.getLabel());
    target.setPointType(AeriusPointType.POINT);
  }

  private void fillAeriusPoint(final ReceptorPoint origin, final AeriusPoint target) {
    target.setId(origin.getReceptorPointId());
    receptorUtil.setAeriusPointFromId(target);
  }

  /**
   * Get results from GML object.
   * @param resultPoint
   * @param calculationPoint
   */
  private static void includeResults(final AeriusResultPoint resultPoint, final AbstractCalculationPoint calculationPoint) {
    for (final ResultProperty result : calculationPoint.getResults()) {
      final Result p = result.getProperty();
      resultPoint.getEmissionResults().put(EmissionResultKey.safeValueOf(p.getSubstance(), p.getResultType()), p.getValue());
    }
  }
}
