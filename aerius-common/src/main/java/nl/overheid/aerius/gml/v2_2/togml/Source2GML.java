/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.togml;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.gml.base.geo.Geometry2GML;
import nl.overheid.aerius.gml.v2_2.base.ReferenceType;
import nl.overheid.aerius.gml.v2_2.source.Emission;
import nl.overheid.aerius.gml.v2_2.source.EmissionProperty;
import nl.overheid.aerius.gml.v2_2.source.road.RoadNetwork;
import nl.overheid.aerius.gml.v2_2.source.road.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Util class to convert {@link EmissionSource} to GML object.
 */
final class Source2GML implements EmissionSourceVisitor<nl.overheid.aerius.gml.v2_2.source.EmissionSource> {

  private static final Logger LOG = LoggerFactory.getLogger(Source2GML.class);

  private static final String ID_SEPARATOR = "_";

  private final Geometry2GML geometry2gml;

  public Source2GML(final Geometry2GML geometry2gml) {
    this.geometry2gml = geometry2gml;
  }

  /**
   * Convert a emissionsource to a GML-object.
   * Based on the actual emissionvalues object of the emissionSource, a specific GML-object will be chosen.
   * @param source The source to convert.
   * @param substances The substances to use when determining emission values.
   * @param year The year to use when determining emission values.
   * @return The converted GML-object (can be a subclass)
   * @throws AeriusException when objects could not be converted to GML.
   */
  public List<FeatureMember> toGML(final EmissionSource source,
      final Substance[] substances, final Integer year) throws AeriusException {
    if (source.getGeometry() == null) {
      throw new IllegalArgumentException("Emissionsource without geometry not allowed: " + source);
    }
    final List<FeatureMember> members = new ArrayList<>();
    if (source instanceof SRM2NetworkEmissionSource) {
      members.addAll(toGMLNetwork(source, substances, year));
    } else {
      members.add(toGMLDefault(source, substances, year));
    }
    return members;
  }

  private nl.overheid.aerius.gml.v2_2.source.EmissionSource toGMLDefault(final EmissionSource source,
      final Substance[] substances, final Integer year) throws AeriusException {
    final nl.overheid.aerius.gml.v2_2.source.EmissionSource returnSource = source.accept(this);
    //set the generic properties.
    returnSource.setGeometry(geometry2gml, source.getGeometry());
    //use a specific prefix for ID to achieve unique IDs
    returnSource.setId("ES." + source.getId());
    returnSource.setLabel(source.getLabel());
    //getSector always returns a sector, even if it was null.
    returnSource.setSectorId(source.getSector().getSectorId());
    if (source instanceof HasOPSSourceCharacteristics) {
      returnSource.setCharacteristics(SourceCharacteristics2GML.toGML(source.getSourceCharacteristics(), true));
      //ensure spread isn't exported for pointsources.
      if (source.getGeometry().getType() == TYPE.POINT) {
        returnSource.getCharacteristics().setSpread(null);
      }
      //ensure diurnal variation isn't exported for sources other then generic ones.
      if (!(source instanceof GenericEmissionSource)) { //
        returnSource.getCharacteristics().setDiurnalVariation(null);
      }
    }
    //always set emissionvalues (even for things like farm/road where we won't use them when importing)
    returnSource.setEmissionValues(getEmissions(source, substances, year));
    if (returnSource.getEmissionValues() == null) {
      throw new IllegalArgumentException("Emissionsource without emission not allowed: " + source);
    }
    return returnSource;
  }

  private List<FeatureMember<?>> toGMLNetwork(final EmissionSource source,
      final Substance[] substances, final Integer year) throws AeriusException {
    final List<FeatureMember<?>> networkMembers = new ArrayList<>();

    if (source instanceof SRM2NetworkEmissionSource) {
      final RoadNetwork network = new RoadNetwork();
      network.setId("NW." + source.getId());
      network.setLabel(source.getLabel());

      final ArrayList<ReferenceType> roadSources = new ArrayList<>();
      //list to ensure each road has a unique ID.
      final EmissionSourceList roadList = new EmissionSourceList();

      for (final EmissionSource roadSource : ((SRM2NetworkEmissionSource) source).getEmissionSources()) {
        if (roadSource.hasAnyEmissions()) {
          //ensure a proper ID.
          roadList.add(roadSource);
          //use the normal toGML to convert each road to a GML object.
          final SRM2RoadEmissionSource gmlRoad = (SRM2RoadEmissionSource) toGMLDefault(roadSource, substances, year);
          gmlRoad.setId("NWR." + source.getId() + ID_SEPARATOR + roadSource.getId());
          //ensure the right references are used.
          gmlRoad.setNetwork(new ReferenceType(network));
          roadSources.add(new ReferenceType(gmlRoad));
          //and add to the list of members to return.
          networkMembers.add(gmlRoad);
        }
      }
      network.setReferences(roadSources);

      networkMembers.add(network);
    }

    return networkMembers;
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final GenericEmissionSource emissionSource) {
    return new nl.overheid.aerius.gml.v2_2.source.EmissionSource();
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    return new MaritimeMooring2GML(geometry2gml).convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final MaritimeRouteEmissionSource emissionSource) {
    return new MaritimeRoute2GML().convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    return new InlandRoute2GML().convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    return new InlandMooring2GML(geometry2gml).convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final OffRoadMobileEmissionSource emissionSource) {
    return new OffRoad2GML().convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final PlanEmissionSource emissionSource) {
    return new Plan2GML().convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final FarmEmissionSource emissionSource) {
    return new Farm2GML().convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final SRM2EmissionSource emissionSource) {
    return new SRM22Road().convert(emissionSource);
  }

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource visit(final SRM2NetworkEmissionSource emissionSource)
      throws AeriusException {
    LOG.error("#visit method should not be called for sourc2gml conversion on SRM2NetworkEmissionSource.");
    throw new AeriusException(Reason.INTERNAL_ERROR);
  }

  private List<EmissionProperty> getEmissions(
      final EmissionSource source, final Substance[] substances, final Integer year) {
    final List<EmissionProperty> emissions = new ArrayList<>(substances.length);
    for (final Substance substance : substances) {
      final EmissionValueKey evk;
      if (year == null) {
        evk = new EmissionValueKey(substance);
      } else {
        evk = new EmissionValueKey(year, substance);
      }
      //always export for each substance found
      //it'll result in a bit more elements, but it's clearer to user
      //if all emissions would be 0 it'd result in emissions being empty as well, which conflicts with XSD.
      final Emission emission = new Emission();
      emission.setSubstance(substance);
      emission.setValue(source.getEmission(evk));
      emissions.add(new EmissionProperty(emission));

    }
    return emissions;
  }

}
