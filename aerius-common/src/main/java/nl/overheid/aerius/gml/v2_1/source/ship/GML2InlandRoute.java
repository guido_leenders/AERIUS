/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_1.source.ship;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.base.source.ship.InlandShippingUtil;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Conversion of GML inland route object to data object.
 */
public class GML2InlandRoute extends AbstractGML2Specific<InlandShippingEmissionSource, InlandRouteEmissionSource> {

  /**
   * @param conversionData The conversion data to use.
   */
  public GML2InlandRoute(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public InlandRouteEmissionSource convert(final InlandShippingEmissionSource source) throws AeriusException {
    final InlandRouteEmissionSource emissionValues = new InlandRouteEmissionSource();
    GML2InlandUtil.setWaterwayType(getCategories(), source.getInlandWaterwayProperty(), source.getId(), emissionValues.getInlandWaterwayType());

    for (final InlandShippingProperty shippingProperty : source.getInlandShippings()) {
      addVesselGroup(emissionValues, shippingProperty.getProperty(), source.getId());
    }
    return emissionValues;
  }

  private void addVesselGroup(final InlandRouteEmissionSource emissionValues, final InlandShipping shipping, final String sourceId)
      throws AeriusException {
    final InlandShippingCategory category = InlandShippingUtil.getShipCategory(getCategories(), shipping.getCode(), sourceId);
    InlandShippingUtil.validateWaterwayForShip(getCategories(), emissionValues.getInlandWaterwayType(), category, sourceId);

    final RouteInlandVesselGroup vesselGroupEmissionValues = new RouteInlandVesselGroup();
    vesselGroupEmissionValues.setId(emissionValues.getEmissionSubSources().size());
    vesselGroupEmissionValues.setName(shipping.getDescription());
    vesselGroupEmissionValues.setCategory(category);
    vesselGroupEmissionValues.setNumberOfShipsAtoBperTimeUnit(shipping.getNumberOfShipsAtoBperTimeUnit());
    vesselGroupEmissionValues.setNumberOfShipsBtoAperTimeUnit(shipping.getNumberOfShipsBtoAperTimeUnit());
    vesselGroupEmissionValues.setTimeUnitShipsAtoB(TimeUnit.valueOf(shipping.getTimeUnitShipsAtoB()));
    vesselGroupEmissionValues.setTimeUnitShipsBtoA(TimeUnit.valueOf(shipping.getTimeUnitShipsBtoA()));
    vesselGroupEmissionValues.setPercentageLadenAtoB(shipping.getPercentageLadenAtoB());
    vesselGroupEmissionValues.setPercentageLadenBtoA(shipping.getPercentageLadenBtoA());
    emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues);
  }
}
