/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.base.geo.GML2Geometry;
import nl.overheid.aerius.gml.v2_1.base.FeatureMemberImpl;
import nl.overheid.aerius.gml.v2_1.base.ReferenceType;
import nl.overheid.aerius.gml.v2_1.source.characteristics.GML2SourceCharacteristics;
import nl.overheid.aerius.gml.v2_1.source.road.RoadEmissionSource;
import nl.overheid.aerius.gml.v2_1.source.road.RoadNetwork;
import nl.overheid.aerius.gml.v2_1.source.road.SRM2RoadEmissionSource;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.HasOPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Utility class to convert to and from GML objects (specific for emission sources).
 */
class GML2Source {

  private static final Logger LOG = LoggerFactory.getLogger(GML2Source.class);
  // If important, the ID of a source can be obtained with the following pattern. ID is always the last number in the ID.
  private static final Pattern SOURCE_ID_PATTERN = Pattern.compile("\\d+");

  private final GMLConversionData conversionData;
  private final GML2Geometry gml2geometry;
  private final GML2SourceVisitor visitor;

  /**
   * @param conversionData The data to use when converting. Should be filled.
   */
  public GML2Source(final GMLConversionData conversionData) {
    this.conversionData = conversionData;
    this.gml2geometry = new GML2Geometry(conversionData.getSrid());
    this.visitor = new GML2SourceVisitor(conversionData);
  }

  ArrayList<EmissionSource> fromGML(final List<FeatureMember> members) throws AeriusException {
    final ArrayList<EmissionSource> sources = new ArrayList<>();

    final List<SRM2RoadEmissionSource> networkMembers = new ArrayList<>();
    final Map<ReferenceType, EmissionSource> networkMap = new HashMap<>();
    for (final FeatureMember member : members) {
      try {
        if (member instanceof SRM2RoadEmissionSource && ((SRM2RoadEmissionSource) member).getNetwork() != null) {
          networkMembers.add((SRM2RoadEmissionSource) member);
        } else if (member instanceof nl.overheid.aerius.gml.v2_1.source.EmissionSource) {
          sources.add(fromGML((nl.overheid.aerius.gml.v2_1.source.EmissionSource) member));
        } else if (member instanceof RoadNetwork) {
          final EmissionSource es = fromGML((RoadNetwork) member);
          sources.add(es);
          networkMap.put(new ReferenceType((RoadNetwork) member), es);
        }
      } catch (final AeriusException e) {
        conversionData.getErrors().add(e);
      }
    }
    // now link all found network members to their respective networks.
    linkNetworks(networkMap, networkMembers);
    return sources;
  }

  /**
   * Convert from a GML-object to an EmissionSource. Based on the exact type of input, a specific EmissionValues will be used.
   *
   * @param source The GML-object to convert.
   * @return The EmissionSource represented by the GML-object.
   * @throws AeriusException In case of errors converting.
   */
  EmissionSource fromGML(final nl.overheid.aerius.gml.v2_1.source.EmissionSource source) throws AeriusException {
    final EmissionSource returnSource = visitor.visit(source);
    fromGenericEmissionSource(source, returnSource);
    conversionData.putAndTrack(getNetworkId(source), returnSource.getId(), source.getId());
    return returnSource;
  }

  /**
   * Convert from a GML-network object to an EmissionSource.
   *
   * @param network The GML-object to convert.
   * @return The EmissionSource represented by the GML-object.
   * @throws AeriusException In case of errors converting.
   */
  EmissionSource fromGML(final RoadNetwork network) throws AeriusException {
    final SRM2NetworkEmissionSource returnSource = new SRM2NetworkEmissionSource();
    returnSource.setId(getMemberId(network));
    returnSource.setLabel(network.getLabel());
    return returnSource;
  }

  private static String getNetworkId(final nl.overheid.aerius.gml.v2_1.source.EmissionSource source) {
    final String networkId;
    if (source instanceof RoadEmissionSource) {
      final RoadEmissionSource res = (RoadEmissionSource) source;
      networkId = res.getNetwork() == null ? null : res.getNetwork().getHref();
    } else {
      networkId = null;
    }
    return networkId;
  }

  private void linkNetworks(final Map<ReferenceType, EmissionSource> networkMap,
      final List<SRM2RoadEmissionSource> networkMembers) throws AeriusException {
    for (final SRM2RoadEmissionSource networkMember : networkMembers) {
      if (networkMap.get(networkMember.getNetwork()) instanceof SRM2NetworkEmissionSource) {
        final SRM2NetworkEmissionSource networkEmissionValues = (SRM2NetworkEmissionSource) networkMap.get(networkMember.getNetwork());
        networkEmissionValues.getEmissionSources().add(fromGML(networkMember));
      } else {
        throw new AeriusException(Reason.SRM2_ROAD_NOT_IN_NETWORK, networkMember.getId());
      }
    }
    for (final EmissionSource network : networkMap.values()) {
      final SRM2NetworkEmissionSource networkEmissionValues = (SRM2NetworkEmissionSource) network;
      if (!networkEmissionValues.getEmissionSources().isEmpty()) {
        final BBox boundingBox = GeometryUtil.determineBBox(networkEmissionValues.getEmissionSources());
        network.setX(boundingBox.getMidX());
        network.setY(boundingBox.getMidY());
        network.setGeometry(new WKTGeometry(network.toWKT()));
        //ensure the network has a sector (even if there could be more than one)
        network.setSector(networkEmissionValues.getEmissionSources().get(0).getSector());
      }
    }
  }

  private void fromGenericEmissionSource(final nl.overheid.aerius.gml.v2_1.source.EmissionSource source, final EmissionSource returnSource)
      throws AeriusException {
    final WKTGeometry gmlGeometry = gml2geometry.getGeometry(source);
    final Point midPoint;
    try {
      midPoint = GeometryUtil.middleOfGeometry(gmlGeometry);
    } catch (final AeriusException e) {
      LOG.error("Error parsing WKT", e);
      throw new AeriusException(Reason.GML_GEOMETRY_INVALID, source.getId());
    }
    returnSource.setId(getMemberId(source));
    returnSource.setX(midPoint.getX());
    returnSource.setY(midPoint.getY());
    returnSource.setGeometry(gmlGeometry);
    returnSource.setLabel(source.getLabel());
    returnSource.setSector(conversionData.getSectorCategories().determineSectorById(source.getSectorId()));
    final Map<String, DiurnalVariationSpecification> diurnalVariationMap = createDiurnalVariationMap(
        conversionData.getSectorCategories().getDiurnalVariations());
    if (returnSource instanceof HasOPSSourceCharacteristics) {
      if (source.getCharacteristics() == null) {
        // if characteristics weren't supplied in GML, use the sector default.
        returnSource.setSourceCharacteristics(returnSource.getSector().getDefaultCharacteristics());
      } else {
        returnSource
            .setSourceCharacteristics(GML2SourceCharacteristics.fromGML(source.getCharacteristics(), returnSource.getSector(), diurnalVariationMap));
      }
    }
  }

  private Map<String, DiurnalVariationSpecification> createDiurnalVariationMap(final List<DiurnalVariationSpecification> diurnalVariations) {
    final Map<String, DiurnalVariationSpecification> map = new HashMap<>();

    for (final DiurnalVariationSpecification spec : diurnalVariations) {
      map.put(spec.getCode(), spec);
    }

    return map;
  }

  private int getMemberId(final FeatureMemberImpl member) {
    final Matcher matcher = SOURCE_ID_PATTERN.matcher(member.getId());
    int id = 0;
    while (matcher.find()) {
      id = Integer.parseInt(matcher.group());
    }
    return id;
  }

}
