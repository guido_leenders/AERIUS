/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.togml;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.v2_2.source.plan.PlanProperty;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource.PlanEmission;

/**
 *
 */
class Plan2GML extends SpecificSource2GML<PlanEmissionSource> {

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource convert(final PlanEmissionSource emissionSource) {
    final nl.overheid.aerius.gml.v2_2.source.plan.PlanEmissionSource returnSource =
        new nl.overheid.aerius.gml.v2_2.source.plan.PlanEmissionSource();
    final List<PlanEmission> plans = emissionSource.getEmissionSubSources();
    final List<PlanProperty> gmlPlans = new ArrayList<>(plans.size());

    for (final PlanEmission plan : plans) {
      final nl.overheid.aerius.gml.v2_2.source.plan.Plan gmlPlanSource =
          new nl.overheid.aerius.gml.v2_2.source.plan.Plan();
      gmlPlanSource.setDescription(plan.getName());
      gmlPlanSource.setCode(plan.getCategory().getCode());
      gmlPlanSource.setAmount(plan.getAmount());
      //we're not adding emissionfactor/description to avoid impression that it will be used on import.
      gmlPlans.add(new PlanProperty(gmlPlanSource));
    }

    returnSource.setPlans(gmlPlans);
    return returnSource;
  }

}
