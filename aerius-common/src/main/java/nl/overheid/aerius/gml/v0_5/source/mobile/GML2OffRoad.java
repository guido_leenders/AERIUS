/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v0_5.source.mobile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.v0_5.source.Emission;
import nl.overheid.aerius.gml.v0_5.source.EmissionProperty;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource.SourceCategoryType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class GML2OffRoad extends
    AbstractGML2Specific<OffRoadMobileEmissionSource, nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource> {

  private static final Logger LOG = LoggerFactory.getLogger(GML2OffRoad.class);

  /**
   * @param conversionData The conversionData to use.
   */
  public GML2OffRoad(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource convert(final OffRoadMobileEmissionSource source)
      throws AeriusException {
    final nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource emissionSource =
        new nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource();

    for (final OffRoadMobileSourceProperty offRoadMobileSourceProperty : source.getOffRoadMobileSources()) {
      final AbstractOffRoadMobileSource offRoadMobileSource = offRoadMobileSourceProperty.getProperty();
      if (offRoadMobileSource instanceof OffRoadMobileSource) {
        emissionSource.getEmissionSubSources().add(convert((OffRoadMobileSource) offRoadMobileSource,
            emissionSource.getEmissionSubSources().size(), source.getId()));
      } else if (offRoadMobileSource instanceof CustomOffRoadMobileSource) {
        emissionSource.getEmissionSubSources().add(convert((CustomOffRoadMobileSource) offRoadMobileSource,
            emissionSource.getEmissionSubSources().size()));
      } else {
        LOG.error("Don't know how to treat offroad mobile source type: " + offRoadMobileSource.getClass());
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }

    return emissionSource;
  }

  private OffRoadVehicleEmissionSubSource convert(
      final OffRoadMobileSource mobileSource, final int proposedId, final String sourceId) throws AeriusException {
    final OffRoadVehicleEmissionSubSource vehicleEmissionValues = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValues.setId(proposedId);
    vehicleEmissionValues.setCategoryType(SourceCategoryType.STAGE_CLASS);
    vehicleEmissionValues.setName(mobileSource.getDescription());
    vehicleEmissionValues.setFuelage(mobileSource.getLiterFuelPerYear());
    final OffRoadMobileSourceCategory category = getCategories().determineOffRoadMobileSourceCategoryByCode(mobileSource.getCode());
    if (category == null) {
      //If we can't find the code, throw exception.
      throw new AeriusException(Reason.GML_UNKNOWN_MOBILE_SOURCE_CODE, sourceId, mobileSource.getCode());
    }
    vehicleEmissionValues.setStageCategory(category);
    return vehicleEmissionValues;
  }

  private OffRoadVehicleEmissionSubSource convert(final CustomOffRoadMobileSource customMobileSource, final int proposedId) {
    final OffRoadVehicleEmissionSubSource vehicleEmissionValues = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValues.setId(proposedId);
    vehicleEmissionValues.setCategoryType(SourceCategoryType.DEVIATING_CLASS);
    vehicleEmissionValues.setName(customMobileSource.getDescription());
    vehicleEmissionValues.setEmissionHeight(customMobileSource.getCharacteristics().getEmissionHeight());
    vehicleEmissionValues.setSpread(customMobileSource.getCharacteristics().getSpread());
    vehicleEmissionValues.setHeatContent(customMobileSource.getCharacteristics().getHeatContent());
    for (final EmissionProperty emissionProperty : customMobileSource.getEmissions()) {
      final Emission emission = emissionProperty.getProperty();
      if (emission.getSubstance() == Substance.NOX) {
        vehicleEmissionValues.setEmissionNOX(emission.getValue());
      }
    }
    return vehicleEmissionValues;
  }

}
