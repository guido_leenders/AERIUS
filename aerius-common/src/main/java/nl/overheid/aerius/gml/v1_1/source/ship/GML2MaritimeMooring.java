/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_1.source.ship;

import java.util.ArrayList;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.base.source.ship.GML2Route;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Conversion of GML maritime mooring object to data object.
 */
public class GML2MaritimeMooring extends AbstractGML2Specific<MooringMaritimeShippingEmissionSource, MaritimeMooringEmissionSource> {

  private final GML2Route gml2route;

  /**
   * @param conversionData The conversion data to use.
   */
  public GML2MaritimeMooring(final GMLConversionData conversionData) {
    super(conversionData);
    gml2route = new GML2Route(conversionData.getSrid());
  }

  @Override
  public MaritimeMooringEmissionSource convert(final MooringMaritimeShippingEmissionSource source) throws AeriusException {
    final MaritimeMooringEmissionSource emissionValues = new MaritimeMooringEmissionSource();
    for (final MooringMaritimeShippingProperty mooringMaritimeShippingProperty : source.getShips()) {
      final MooringMaritimeShipping mooringMaritimeShipping = mooringMaritimeShippingProperty.getProperty();
      final MaritimeShippingCategory category = getCategories().determineMaritimeShippingCategoryByCode(mooringMaritimeShipping.getCode());
      if (category == null) {
        //If we can't find the code, throw exception.
        throw new AeriusException(Reason.GML_UNKNOWN_SHIP_CODE, source.getId(), mooringMaritimeShipping.getCode());
      }
      final MooringMaritimeVesselGroup vesselGroupEmissionValues = new MooringMaritimeVesselGroup();
      vesselGroupEmissionValues.setId(emissionValues.getEmissionSubSources().size());
      vesselGroupEmissionValues.setName(mooringMaritimeShipping.getDescription());
      vesselGroupEmissionValues.setCategory(category);
      vesselGroupEmissionValues.setNumberOfShips(mooringMaritimeShipping.getShipsPerYear(), TimeUnit.YEAR);
      vesselGroupEmissionValues.setResidenceTime(mooringMaritimeShipping.getAverageResidenceTime());
      if (mooringMaritimeShipping.getInlandRoute() != null) {
        final ShippingRoute route = handleInlandRoute(mooringMaritimeShipping);
        emissionValues.getInlandRoutes().add(route);
        vesselGroupEmissionValues.setInlandRoute(route);

      }
      vesselGroupEmissionValues.getMaritimeRoutes().addAll(handleMaritimeRoutes(mooringMaritimeShipping, source.getId()));
      emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues);
    }
    return emissionValues;
  }

  private ShippingRoute handleInlandRoute(final MooringMaritimeShipping mooringMaritimeShipping) throws AeriusException {
    final ShippingRoute route = gml2route.toRoute(mooringMaritimeShipping.getInlandRoute());
    route.setEndPoint(GeometryUtil.lastPointFromWKT(route.getGeometry().getWKT()));
    getConversionData().getInlandRoutes().add(route);
    return route;
  }

  private ArrayList<MaritimeRoute> handleMaritimeRoutes(final MooringMaritimeShipping mooringMaritimeShipping, final String sourceId)
      throws AeriusException {
    final ArrayList<MaritimeRoute> routes = new ArrayList<>();
    if (mooringMaritimeShipping.getMaritimeRoutes() != null) {
      for (final MaritimeShippingRouteProperty msrp : mooringMaritimeShipping.getMaritimeRoutes()) {
        final ShippingRoute route = gml2route.findRoute(msrp.getProperty().getRoute(), getConversionData().getMaritimeRoutes(), sourceId);
        final String wholeId = msrp.getProperty().getRoute().getGMLLineString().getId();
        route.setId(Integer.valueOf(wholeId.substring(wholeId.lastIndexOf('_') + 1)));
        final MaritimeRoute mr = new MaritimeRoute();
        mr.setShipMovements(msrp.getProperty().getShippingMovements(), TimeUnit.YEAR);
        mr.setRoute(route);
        routes.add(mr);
      }
    }
    return routes;
  }

}
