/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_0.source.ship;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.base.source.ship.GML2Route;
import nl.overheid.aerius.gml.base.source.ship.InlandShippingUtil;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.ShippingRoute;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Conversion of GML inland mooring object to data object.
 */
public class GML2InlandMooring extends AbstractGML2Specific<MooringInlandShippingEmissionSource, InlandMooringEmissionSource> {

  private final GML2Route gml2route;
  /**
   * @param conversionData The conversion data to use.
   */
  public GML2InlandMooring(final GMLConversionData conversionData) {
    super(conversionData);
    gml2route = new GML2Route(conversionData.getSrid());
  }

  @Override
  public InlandMooringEmissionSource convert(final MooringInlandShippingEmissionSource source) throws AeriusException {
    final InlandMooringEmissionSource emissionSource = new InlandMooringEmissionSource();
    for (final MooringInlandShippingProperty mooringInlandShippingProperty : source.getShips()) {
      addVesselGroup(mooringInlandShippingProperty.getProperty(), emissionSource, source.getId());
    }
    return emissionSource;
  }

  private void addVesselGroup(final MooringInlandShipping mooringInlandShipping, final InlandMooringEmissionSource emissionSource,
      final String sourceId) throws AeriusException {
    final InlandShippingCategory category = InlandShippingUtil.getShipCategory(getCategories(), mooringInlandShipping.getCode(), sourceId);
    final InlandMooringVesselGroup vesselGroupEmissionValues = new InlandMooringVesselGroup();
    vesselGroupEmissionValues.setId(emissionSource.getEmissionSubSources().size());
    vesselGroupEmissionValues.setName(mooringInlandShipping.getDescription());
    vesselGroupEmissionValues.setCategory(category);
    vesselGroupEmissionValues.setResidenceTime(mooringInlandShipping.getAverageResidenceTime());
    if (mooringInlandShipping.getRoutes() != null) {
      for (final InlandShippingRouteProperty isrp : mooringInlandShipping.getRoutes()) {
        addRoute(isrp.getProperty(), emissionSource, vesselGroupEmissionValues, sourceId);
      }
    }
    emissionSource.getEmissionSubSources().add(vesselGroupEmissionValues);
  }

  private void addRoute(final InlandShippingRoute inlandShippingRoute, final InlandMooringEmissionSource emissionSource,
      final InlandMooringVesselGroup vesselGroupEmissionValues, final String sourceId) throws AeriusException {
    final ShippingRoute route = gml2route.findRoute(inlandShippingRoute.getRoute(), emissionSource.getInlandRoutes(), sourceId);
    final String wholeId = inlandShippingRoute.getRoute().getGMLLineString().getId();
    route.setId(Integer.valueOf(wholeId.substring(wholeId.lastIndexOf('_') + 1)));
    final InlandMooringRoute imr = new InlandMooringRoute();
    imr.setShipMovements(inlandShippingRoute.getShippingMovements(), TimeUnit.YEAR);
    imr.setPercentageLaden(inlandShippingRoute.getPercentageLaden());
    imr.setDirection(inlandShippingRoute.getDirection());
    imr.setRoute(route);
    vesselGroupEmissionValues.getRoutes().add(imr);
    getConversionData().setOrForceWaterwayType(emissionSource.getLabel(), route, null);
  }

}
