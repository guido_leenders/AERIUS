/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base.source.ship;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.HasShippingRoute;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayTypeUtil;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public class InlandShippingUtil {
  private static final Logger LOG = LoggerFactory.getLogger(InlandShippingUtil.class);

  private final PMF pmf;
  private final List<AeriusException> warnings;
  private final Map<WKTGeometry, InlandWaterwayType> geometryWaterwayCache = new HashMap<>();

  public InlandShippingUtil(final PMF pmf, final List<AeriusException> warnings) {
    this.pmf = pmf;
    this.warnings = warnings;
  }

  /**
   * Since IMAER version 2.0 the waterway type if required. Not having a water type throws an AeriusException.
   *
   * @param categories
   * @param waterwayType
   * @param direction
   * @param sourceId
   * @return
   * @throws AeriusException
   */
  public static InlandWaterwayCategory getWaterwayCategory(final SectorCategories categories, final String waterwayType,
      final WaterwayDirection direction, final String sourceId) throws AeriusException {
    final InlandWaterwayCategory category = categories.getInlandShippingCategories().getWaterwayCategoryByCode(waterwayType);
    if (category == null || (category.isDirectionRelevant() && !category.getDirections().contains(direction))) {
      //If we can't find the category, or it doesn't allow the direction, throw exception.
      throw new AeriusException(Reason.GML_UNKNOWN_WATERWAY_CODE, sourceId, waterwayType);
    }
    return category;
  }

  public static void validateWaterwayForShip(final SectorCategories categories, final InlandWaterwayType waterwayType,
      final InlandShippingCategory shipCategory, final String sourceId) throws AeriusException {
    if (waterwayType != null && !categories.getInlandShippingCategories().isValidCombination(shipCategory, waterwayType.getWaterwayCategory())) {
      throw new AeriusException(Reason.GML_INVALID_SHIP_FOR_WATERWAY, sourceId, shipCategory.getCode(), waterwayType.getWaterwayCategory().getCode());
    }
  }

  public static InlandShippingCategory getShipCategory(final SectorCategories categories, final String code, final String sourceId)
      throws AeriusException {
    final InlandShippingCategory category = categories.getInlandShippingCategories().getShipCategoryByCode(code);
    if (category == null) {
      //If we can't find the code, throw exception.
      throw new AeriusException(Reason.GML_UNKNOWN_SHIP_CODE, sourceId, code);
    }
    return category;
  }

  /**
   * Forces a waterway type based on the location of the given route. This will always return a waterway type even if the geometry is now where
   * near a water type.
   * <p>This method is only relevant for IMAER < 2.0 because since that version waterway type is not determined automatically but required to be set
   * in the GML.
   *
   * @param categories
   * @param sourceId
   * @param route
   * @param inlandWaterwayType
   * @throws AeriusException
   */
  public void setOrForceWaterwayType(final SectorCategories categories, final String sourceId, final HasShippingRoute route,
      final InlandWaterwayType inlandWaterwayType) throws AeriusException {
    if (inlandWaterwayType == null || inlandWaterwayType.getWaterwayCategory() == null) {
      final WKTGeometry routeGeometry = route.getGeometry();
      final InlandWaterwayType way = determineInlandWaterWayType(sourceId, routeGeometry);
      route.setWaterwayCategory(categories.getInlandShippingCategories().getWaterwayCategoryByCode(way.getWaterwayCategory().getCode()));
      route.setWaterwayDirection(way.getWaterwayDirection());
    } else {
      route.setWaterwayCategory(inlandWaterwayType.getWaterwayCategory());
      route.setWaterwayDirection(inlandWaterwayType.getWaterwayDirection());
    }
  }

  /**
   * @param sourceId
   * @param routeGeometry
   * @throws AeriusException
   */
  private InlandWaterwayType determineInlandWaterWayType(final String sourceId, final WKTGeometry routeGeometry) throws AeriusException {
    final InlandWaterwayType way;
    if (geometryWaterwayCache.containsKey(routeGeometry)) {
      way = geometryWaterwayCache.get(routeGeometry);
    } else {
      final List<InlandWaterwayType> ways = suggestInlandShippingWaterway(routeGeometry);
      final AeriusException ae = InlandWaterwayTypeUtil.detectInconclusiveRoute(sourceId, ways);
      if (ae != null) {
        warnings.add(ae);
      }
      // Should never happen.
      if (ways.isEmpty()) {
        LOG.error("No type of waterway could be established for: {}.", routeGeometry.toString());
        throw new AeriusException(Reason.INTERNAL_ERROR);
      } else {
        way = ways.get(0);
        geometryWaterwayCache.put(routeGeometry, way);
        LOG.debug("Keys in cache: {}", geometryWaterwayCache.keySet().size());
      }
    }
    return way;
  }

  private List<InlandWaterwayType> suggestInlandShippingWaterway(final WKTGeometry geometry) throws AeriusException {
    try (Connection con = pmf.getConnection()) {
      return ShippingRepository.suggestInlandShippingWaterway(con, geometry);
    } catch (final SQLException e) {
      LOG.error("SuggestInlandShippingWaterway sql exception:", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }
}
