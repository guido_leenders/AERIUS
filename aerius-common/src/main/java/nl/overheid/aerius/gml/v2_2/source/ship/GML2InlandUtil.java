/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.source.ship;

import nl.overheid.aerius.gml.base.source.ship.InlandShippingUtil;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
final class GML2InlandUtil {

  private GML2InlandUtil() {
    //util class.
  }

  static void setWaterwayType(final SectorCategories categories, final InlandWaterwayProperty waterwayProperty,
      final String sourceId, final InlandWaterwayType inlandWaterwayType) throws AeriusException {
    final InlandWaterway inlandWaterway = waterwayProperty.getProperty();
    final InlandWaterwayCategory waterwayCategory =
        InlandShippingUtil.getWaterwayCategory(categories, inlandWaterway.getType(), inlandWaterway.getDirection(), sourceId);
    inlandWaterwayType.setWaterwayCategory(waterwayCategory);
    inlandWaterwayType.setWaterwayDirection(inlandWaterway.getDirection());
  }
}
