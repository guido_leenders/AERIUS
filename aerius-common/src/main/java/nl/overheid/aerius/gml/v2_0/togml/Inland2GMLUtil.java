/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_0.togml;

import nl.overheid.aerius.gml.v2_0.source.ship.InlandWaterway;
import nl.overheid.aerius.gml.v2_0.source.ship.InlandWaterwayProperty;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
final class Inland2GMLUtil {

  private Inland2GMLUtil() {
    //util class.
  }

  static InlandWaterwayProperty getWaterway(final InlandWaterwayType waterwayType, final int sourceId) throws AeriusException {
    if (waterwayType == null || waterwayType.getWaterwayCategory() == null) {
      throw new AeriusException(Reason.GML_INLAND_WATERWAY_NOT_SET, String.valueOf(sourceId));
    } else {
      final InlandWaterway waterway = new InlandWaterway();
      waterway.setType(waterwayType.getWaterwayCategory().getCode());
      waterway.setDirection(waterwayType.getWaterwayDirection() == WaterwayDirection.IRRELEVANT ? null : waterwayType.getWaterwayDirection());
      return new InlandWaterwayProperty(waterway);
    }
  }
}
