/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.source.ship;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Conversion of GML maritime route object to data object.
 */
public class GML2MaritimeRoute extends AbstractGML2Specific<MaritimeShippingEmissionSource, MaritimeRouteEmissionSource> {

  /**
   * @param conversionData The conversion data to use.
   */
  public GML2MaritimeRoute(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public MaritimeRouteEmissionSource convert(final MaritimeShippingEmissionSource source) throws AeriusException {
    final MaritimeRouteEmissionSource emissionValues = new MaritimeRouteEmissionSource();
    final ShippingMovementType movementType = source.getMovementType();
    emissionValues.setMovementType(movementType);
    for (final MaritimeShippingProperty maritimeShippingProperty : source.getMaritimeShippings()) {
      final MaritimeShipping maritimeShipping = maritimeShippingProperty.getProperty();
      final MaritimeShippingCategory category = getCategories().determineMaritimeShippingCategoryByCode(maritimeShipping.getCode());
      if (category == null) {
        //If we can't find the code, throw exception.
        throw new AeriusException(Reason.GML_UNKNOWN_SHIP_CODE, source.getId(), maritimeShipping.getCode());
      }
      final RouteMaritimeVesselGroup vesselGroupEmissionValues = new RouteMaritimeVesselGroup(movementType);
      vesselGroupEmissionValues.setId(emissionValues.getEmissionSubSources().size());
      vesselGroupEmissionValues.setName(maritimeShipping.getDescription());
      vesselGroupEmissionValues.setCategory(category);
      vesselGroupEmissionValues.setShipMovementsPerTimeUnit(maritimeShipping.getShipsPerTimeUnit());
      vesselGroupEmissionValues.setTimeUnit(TimeUnit.valueOf(maritimeShipping.getTimeUnit()));
      emissionValues.getEmissionSubSources().add(vesselGroupEmissionValues);
    }
    return emissionValues;
  }

}
