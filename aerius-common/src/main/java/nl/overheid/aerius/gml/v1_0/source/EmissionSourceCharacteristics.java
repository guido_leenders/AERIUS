/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_0.source;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import nl.overheid.aerius.gml.v1_0.base.CalculatorSchema;

/**
 *
 */
@XmlType(name = "EmissionSourceCharacteristicsType", namespace = CalculatorSchema.NAMESPACE,
    propOrder = { "heatContent", "emissionHeight", "buildingHeight", "spread", "diurnalVariation" })
public class EmissionSourceCharacteristics {
  private double heatContent;
  private double emissionHeight;
  private Double buildingHeight;
  private Double spread;
  private DiurnalVariationEnum diurnalVariation;

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public double getHeatContent() {
    return heatContent;
  }

  public void setHeatContent(final double heatContent) {
    this.heatContent = heatContent;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public double getEmissionHeight() {
    return emissionHeight;
  }

  public void setEmissionHeight(final double emissionHeight) {
    this.emissionHeight = emissionHeight;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public Double getBuildingHeight() {
    return buildingHeight;
  }

  public void setBuildingHeight(final Double buildingHeight) {
    this.buildingHeight = buildingHeight;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public Double getSpread() {
    return spread;
  }

  public void setSpread(final Double spread) {
    this.spread = spread;
  }

  @XmlElement(namespace = CalculatorSchema.NAMESPACE)
  public DiurnalVariationEnum getDiurnalVariation() {
    return diurnalVariation;
  }

  public void setDiurnalVariation(final DiurnalVariationEnum diurnalVariation) {
    this.diurnalVariation = diurnalVariation;
  }

}
