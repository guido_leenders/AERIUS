/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.conversion.EmissionsCalculator;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.FeatureCollection;
import nl.overheid.aerius.gml.base.FeatureMember;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.base.GMLVersionReader;
import nl.overheid.aerius.gml.base.GMLVersionReaderFactory;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Class to read data from a feature collection that was created from an IMAER GML.
 * This class maintains the state of a IMAER GML read and is initialized with that data.
 */
public final class GMLReader {

  private static final Logger LOG = LoggerFactory.getLogger(GMLReader.class);

  private final PMF pmf;
  private final GMLVersionReaderFactory factory;
  private final FeatureCollection featureCollection;
  private final GMLMetaDataReader metaDataReader;
  private final GMLVersionReader versionReader;
  private final GMLConversionData conversionData;

  /**
   * Constructor.
   *
   * @param pmf database connection factory
   * @param rgs the receptor grid settings
   * @param categories
   * @param factory specific version factory
   * @param featureCollection the feature collection with parsed IMAER GML data
   * @param errors list to add errors on
   * @param warnings list to add warnings on
   */
  GMLReader(final PMF pmf, final ReceptorGridSettings rgs, final SectorCategories categories, final GMLVersionReaderFactory factory,
      final FeatureCollection featureCollection,
      final List<AeriusException> errors, final List<AeriusException> warnings) {
    this.pmf = pmf;
    this.factory = factory;
    this.featureCollection = featureCollection;
    metaDataReader = new GMLMetaDataReader(featureCollection);
    conversionData = new GMLConversionData(pmf, factory.getLegacyCodeConverter(), categories, rgs, errors, warnings);
    versionReader = factory.createReader(conversionData);
  }

  public AeriusGMLVersion getVersion() {
    return factory.getVersion();
  }

  public GMLMetaDataReader metaDataReader() {
    return metaDataReader;
  }

  /**
   * Returns the {@link EmissionSourceList} from the feature collection.
   * @param categories source categories.
   * @param keys emission keys used to set the emissions values.
   * @return List of sources
   * @throws AeriusException error
   */
  public EmissionSourceList readEmissionSourceList(final List<EmissionValueKey> keys) throws AeriusException {
    final EmissionSourceList emissionSourceList = toEmissionSources();
    //ensure emissions are set (where needed)
    enforceEmissions(keys, emissionSourceList);
    return emissionSourceList;
  }

  /**
   * Sets the name on emission source list from the feature collection.
   * @param emissionSourceList list to set name on
   */
  public void setName(final EmissionSourceList emissionSourceList) {
    emissionSourceList.setName(featureCollection == null ? "" : featureCollection.getName());
  }

  private void enforceEmissions(final List<EmissionValueKey> keys, final EmissionSourceList emissionSourceList) throws AeriusException {
    try (Connection con = pmf.getConnection()) {
      EmissionsCalculator.setEmissions(con, emissionSourceList, keys);
    } catch (final SQLException e) {
      LOG.error("SQL Exception:", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private EmissionSourceList toEmissionSources() throws AeriusException {
    final EmissionSourceList emissionSourceList = conversionData.getEmissionSourceList();
    if ((featureCollection != null) && (featureCollection.getFeatureMembers() != null)) {
      emissionSourceList.setAll(versionReader.fromGML(featureCollection.getFeatureMembers()));
    }
    setName(emissionSourceList);
    return emissionSourceList;
  }

  /**
  * Retrieve all receptor points (domain objects) from a list of FeatureMembers.
  * Only features extending AbstractCalculationPoint will be handled (AeriusPoint, CustomCalculationPoint, etc).
  * @param includeReceptorPoint if true also read ReceptorPoints from GML
  * @return List of all receptor points defined in the GML
  * @throws AeriusException in case of errors
  */
  public List<AeriusPoint> getAeriusPoints(final boolean includeReceptorPoint) throws AeriusException {
    final List<AeriusPoint> points = new ArrayList<>();
    if ((featureCollection != null) && (featureCollection.getFeatureMembers() != null)) {
      for (final FeatureMember member : featureCollection.getFeatureMembers()) {
        addAeriusPoint(includeReceptorPoint, points, member);
      }
    }
    return points;
  }

  private void addAeriusPoint(final boolean includeResults, final List<AeriusPoint> points, final FeatureMember member) throws AeriusException {
    final AeriusPoint rp = versionReader.fromGML(member, includeResults);
    if (rp != null) {
      points.add(rp);
    }
  }
}
