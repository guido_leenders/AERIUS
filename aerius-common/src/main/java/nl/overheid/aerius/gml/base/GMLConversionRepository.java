/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.db.util.Attribute;
import nl.overheid.aerius.db.util.Query;
import nl.overheid.aerius.db.util.QueryBuilder;
import nl.overheid.aerius.db.util.QueryUtil;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;

/**
 *
 */
final class GMLConversionRepository {

  private enum RepositoryAttribute implements Attribute {
    TYPE,
    OLD_VALUE,
    NEW_VALUE,
    ISSUE_WARNING,

    GML_VERSION;

    @Override
    public String attribute() {
      return name().toLowerCase();
    }

  }

  private static final Query SELECT_GML_LEGACY_CODES =
      QueryBuilder.from("system.gml_conversions")
          .select(RepositoryAttribute.TYPE, RepositoryAttribute.OLD_VALUE, RepositoryAttribute.NEW_VALUE, RepositoryAttribute.ISSUE_WARNING)
          .where(RepositoryAttribute.GML_VERSION).getQuery();

  private GMLConversionRepository() {
    //do not instantiate.
  }

  /**
   * Get a GMLLegacyCodeConverter specific for the given AERIUS GML version.
   * @param con database connection
   * @param version AERIUS GML version
   * @return map containing the proper conversions.
   * @throws SQLException in case something went wrong with the database
   */
  static Map<GMLLegacyCodeType, Map<String, Conversion>> getLegacyCodes(final Connection con, final AeriusGMLVersion version)
      throws SQLException {
    final Map<GMLLegacyCodeType, Map<String, Conversion>> codeMaps = new HashMap<>();
    try (final PreparedStatement statement = con.prepareStatement(SELECT_GML_LEGACY_CODES.get())) {
      SELECT_GML_LEGACY_CODES.setParameter(statement, RepositoryAttribute.GML_VERSION, version.name());

      final ResultSet rs = statement.executeQuery();
      while (rs.next()) {
        final GMLLegacyCodeType codeType = QueryUtil.getEnum(rs, RepositoryAttribute.TYPE, GMLLegacyCodeType.class);
        if (!codeMaps.containsKey(codeType)) {
          codeMaps.put(codeType, new HashMap<String, Conversion>());
        }
        codeMaps.get(codeType).put(QueryUtil.getString(rs, RepositoryAttribute.OLD_VALUE),
            new Conversion(QueryUtil.getString(rs, RepositoryAttribute.NEW_VALUE), QueryUtil.getBoolean(rs, RepositoryAttribute.ISSUE_WARNING)));
      }
    }
    return codeMaps;
  }
}
