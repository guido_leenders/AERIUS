/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.source.characteristics;

import java.util.Map;

import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Utility class to convert to and from GML objects (specific for source characteristics).
 */
public final class GML2SourceCharacteristics {

  private GML2SourceCharacteristics() {
    // Util class.
  }

  public static OPSSourceCharacteristics fromGML(final EmissionSourceCharacteristics characteristics, final Sector sector,
      final Map<String, DiurnalVariationSpecification> specs) {
    final OPSSourceCharacteristics defaultCharacteristics = sector == null ? new OPSSourceCharacteristics() : sector.getDefaultCharacteristics();
    final OPSSourceCharacteristics returnCharacteristics = new OPSSourceCharacteristics();
    returnCharacteristics.setEmissionHeight(characteristics.getEmissionHeight());
    returnCharacteristics.setSpread(getOrDefault(characteristics.getSpread(), defaultCharacteristics.getSpread()));
    returnCharacteristics.setParticleSizeDistribution(defaultCharacteristics.getParticleSizeDistribution());
    if (characteristics.getDiurnalVariation() == null) {
      returnCharacteristics.setDiurnalVariationSpecification(defaultCharacteristics.getDiurnalVariationSpecification());
    } else {
      returnCharacteristics.setDiurnalVariationSpecification(specs.get(characteristics.getDiurnalVariation()));
    }

    fromGMLToBuildingProperties(characteristics, returnCharacteristics);

    fromGMLToHeatContent(characteristics, returnCharacteristics);

    return returnCharacteristics;
  }

  private static void fromGMLToBuildingProperties(final EmissionSourceCharacteristics characteristics,
      final OPSSourceCharacteristics returnCharacteristics) {
    final Building gmlBuilding = characteristics.getBuilding();
    if (gmlBuilding != null) {
      returnCharacteristics.setBuildingInfluence(true);
      returnCharacteristics.setBuildingHeight(gmlBuilding.getHeight());
      returnCharacteristics.setBuildingWidth(gmlBuilding.getWidth());
      returnCharacteristics.setBuildingLength(gmlBuilding.getLength());
      returnCharacteristics.setBuildingOrientation(gmlBuilding.getOrientation());
    }
  }

  private static void fromGMLToHeatContent(final EmissionSourceCharacteristics characteristics,
      final OPSSourceCharacteristics returnCharacteristics) {
    final AbstractHeatContent gmlHeatContent = characteristics.getHeatContent();
    if (gmlHeatContent instanceof SpecifiedHeatContent) {
      returnCharacteristics.setHeatContentType(HeatContentType.NOT_FORCED);
      returnCharacteristics.setHeatContent(((SpecifiedHeatContent) gmlHeatContent).getValue());
    } else if (gmlHeatContent instanceof CalculatedHeatContent) {
      returnCharacteristics.setHeatContentType(HeatContentType.FORCED);
      final CalculatedHeatContent calculatedHeatContent = (CalculatedHeatContent) gmlHeatContent;
      returnCharacteristics.setEmissionTemperature(calculatedHeatContent.getEmissionTemperature());
      returnCharacteristics.setOutflowDiameter(calculatedHeatContent.getOutflowDiameter());
      returnCharacteristics.setOutflowVelocity(calculatedHeatContent.getOutflowVelocity());
      returnCharacteristics.setOutflowDirection(calculatedHeatContent.getOutflowDirection());
    }
  }

  private static <T extends Number> T getOrDefault(final T value, final T defaultValue) {
    return value == null ? defaultValue : value;
  }
}
