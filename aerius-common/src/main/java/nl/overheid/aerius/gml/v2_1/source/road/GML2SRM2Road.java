/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_1.source.road;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.v2_1.source.Emission;
import nl.overheid.aerius.gml.v2_1.source.EmissionProperty;
import nl.overheid.aerius.shared.domain.sector.category.OnRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSourceLinearReference;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class GML2SRM2Road extends AbstractGML2Specific<SRM2RoadEmissionSource, SRM2EmissionSource> {

  /**
   * @param conversionData
   *          The conversion data to use.
   */
  public GML2SRM2Road(final GMLConversionData conversionData) {
    super(conversionData);
  }

  @Override
  public SRM2EmissionSource convert(final SRM2RoadEmissionSource source) throws AeriusException {
    final SRM2EmissionSource emissionSource = new SRM2EmissionSource();
    final RoadType roadType = RoadType.valueFromSectorId(source.getSectorId());
    for (final VehiclesProperty vp : source.getVehicles()) {
      emissionSource.getEmissionSubSources().add(getVehicleEmissions(source, roadType, vp));
    }
    emissionSource.setFreeway(source.isFreeway());

    setOptionalSRM2Variables(source, emissionSource);

    return emissionSource;
  }

  private VehicleEmissions getVehicleEmissions(final SRM2RoadEmissionSource source, final RoadType roadType,
      final VehiclesProperty vp) throws AeriusException {
    final AbstractVehicle av = vp.getProperty();
    final VehicleEmissions vehicleEmissions;
    if (av instanceof StandardVehicle) {
      vehicleEmissions = getEmissionValues(source, (StandardVehicle) av);
    } else if (av instanceof SpecificVehicle) {
      vehicleEmissions = getEmissionValues(source, (SpecificVehicle) av, roadType);
    } else if (av instanceof CustomVehicle) {
      vehicleEmissions = getEmissionValues((CustomVehicle) av);
    } else {
      throw new IllegalArgumentException("Instance not supported:" + av.getClass().getCanonicalName());
    }
    vehicleEmissions.setVehiclesPerTimeUnit(av.getVehiclesPerTimeUnit());
    vehicleEmissions.setTimeUnit(TimeUnit.valueOf(av.getTimeUnit()));
    return vehicleEmissions;
  }

  private VehicleEmissions getEmissionValues(final SRM2RoadEmissionSource source, final StandardVehicle sv) throws AeriusException {
    final VehicleStandardEmissions vse = new VehicleStandardEmissions();
    vse.setStagnationFraction(sv.getStagnationFactor());
    final RoadEmissionCategory category = getCategories().getRoadEmissionCategories().findClosestCategory(
        RoadType.valueFromSectorId(source.getSectorId()), sv.getVehicleType(), sv.isStrictEnforcement(), sv.getMaximumSpeed(), null);
    if (category == null) {
      throw new AeriusException(Reason.GML_UNKNOWN_ROAD_CATEGORY, source.getId(), String.valueOf(source.getSectorId()),
          String.valueOf(sv.getMaximumSpeed()), String.valueOf(sv.isStrictEnforcement()), String.valueOf(sv.getVehicleType()));
    }
    vse.setEmissionCategory(category);
    return vse;
  }

  private VehicleEmissions getEmissionValues(final RoadEmissionSource source, final SpecificVehicle sv, final RoadType roadType)
      throws AeriusException {
    final OnRoadMobileSourceCategory category = getCategories().determineOnRoadMobileSourceCategoryByCode(
        getConversionData().getCode(GMLLegacyCodeType.ON_ROAD_MOBILE_SOURCE, sv.getCode(), source.getLabel()));
    if (category == null) {
      // If we can't find the code, throw exception.
      throw new AeriusException(Reason.GML_UNKNOWN_MOBILE_SOURCE_CODE, source.getId(), sv.getCode());
    }
    final VehicleSpecificEmissions vse = new VehicleSpecificEmissions();
    vse.setCategory(category);
    vse.setRoadType(roadType);
    return vse;
  }

  private VehicleEmissions getEmissionValues(final CustomVehicle cv) {
    final VehicleCustomEmissions vce = new VehicleCustomEmissions();
    vce.setDescription(cv.getDescription());
    for (final EmissionProperty e : cv.getEmissions()) {
      final Emission emission = e.getProperty();
      vce.setEmission(emission.getSubstance(), emission.getValue());
    }
    return vce;
  }

  private void setOptionalSRM2Variables(final SRM2RoadEmissionSource source, final SRM2EmissionSource emissionSource) throws AeriusException {
    // set the optional stuff.
    if (source.getTunnelFactor() != null) {
      emissionSource.setTunnelFactor(source.getTunnelFactor());
    }
    if (source.getElevation() != null) {
      emissionSource.setElevation(source.getElevation());
    }
    if (source.getElevationHeight() != null) {
      emissionSource.setElevationHeight(source.getElevationHeight());
    }

    if (source.getBarrierLeft() != null) {
      emissionSource.setBarrierLeft(getRoadSideBarrier(source.getBarrierLeft()));
    }
    if (source.getBarrierRight() != null) {
      emissionSource.setBarrierRight(getRoadSideBarrier(source.getBarrierRight()));
    }
    if (!source.getDynamicSegments().isEmpty()) {
      setDynamicSegments(source, emissionSource);
    }
  }

  private void setDynamicSegments(final SRM2RoadEmissionSource source, final SRM2EmissionSource emissionSource) throws AeriusException {
    final ArrayList<SRM2EmissionSourceLinearReference> dynamicSegments = new ArrayList<>();

    for (final PartialChangeProperty partialChangeProperty : source.getDynamicSegments()) {
      final AbstractLinearReference partialChange = partialChangeProperty.getProperty();
      if (partialChange instanceof SRM2RoadLinearReference) {
        handleSegment((SRM2RoadLinearReference) partialChange, dynamicSegments);
      }
    }

    emissionSource.setDynamicSegments(dynamicSegments);
  }

  private void handleSegment(final SRM2RoadLinearReference dynamicSegmentGML, final List<SRM2EmissionSourceLinearReference> dynamicSegments)
      throws AeriusException {
    final double fromPosition = dynamicSegmentGML.getFromPosition();
    validatePositionFraction(fromPosition);

    final double toPosition = dynamicSegmentGML.getToPosition();
    validatePositionFraction(toPosition);

    final SRM2EmissionSourceLinearReference dynamicSegment = new SRM2EmissionSourceLinearReference();
    dynamicSegment.setStart(fromPosition);
    dynamicSegment.setEnd(toPosition);
    dynamicSegment.setFreeway(dynamicSegmentGML.isFreeway());
    dynamicSegment.setTunnelFactor(dynamicSegmentGML.getTunnelFactor());
    dynamicSegment.setElevation(dynamicSegmentGML.getElevation());
    dynamicSegment.setElevationHeight(dynamicSegmentGML.getElevationHeight());

    if (dynamicSegmentGML.getBarrierLeft() != null) {
      dynamicSegment.setBarrierLeft(getRoadSideBarrier(dynamicSegmentGML.getBarrierLeft()));
    }

    if (dynamicSegmentGML.getBarrierRight() != null) {
      dynamicSegment.setBarrierRight(getRoadSideBarrier(dynamicSegmentGML.getBarrierRight()));
    }

    dynamicSegments.add(dynamicSegment);
  }

  private void validatePositionFraction(final double position) throws AeriusException {
    if (position < 0 || position > 1) {
      throw new AeriusException(Reason.GML_ROAD_SEGMENT_POSITION_NOT_FRACTION, String.valueOf(position));
    }
  }

  private SRM2EmissionSource.RoadSideBarrier getRoadSideBarrier(
      final RoadSideBarrierProperty barrierProperty) {
    final RoadSideBarrier gmlBarrier = barrierProperty.getProperty();
    final SRM2EmissionSource.RoadSideBarrier barrier = new SRM2EmissionSource.RoadSideBarrier();
    barrier.setBarrierType(gmlBarrier.getBarrierType());
    barrier.setHeight(gmlBarrier.getHeight());
    barrier.setDistance(gmlBarrier.getDistance());
    return barrier;
  }
}
