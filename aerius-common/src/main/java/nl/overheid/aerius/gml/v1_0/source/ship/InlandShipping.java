/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_0.source.ship;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import nl.overheid.aerius.gml.v1_0.base.CalculatorSchema;

/**
 *
 */
@XmlType(name = "InlandShipType", namespace = CalculatorSchema.NAMESPACE,
    propOrder = { "numberOfShipsAtoB", "numberOfShipsBtoA", "percentageLadenAtoB", "percentageLadenBtoA" })
public class InlandShipping extends AbstractShipping {

  private int numberOfShipsAtoB;
  private int numberOfShipsBtoA;
  private int percentageLadenAtoB;
  private int percentageLadenBtoA;

  @XmlElement(name = "numberOfShipsAtoB", namespace = CalculatorSchema.NAMESPACE)
  public int getNumberOfShipsAtoB() {
    return numberOfShipsAtoB;
  }

  @XmlElement(name = "numberOfShipsBtoA", namespace = CalculatorSchema.NAMESPACE)
  public int getNumberOfShipsBtoA() {
    return numberOfShipsBtoA;
  }

  public void setNumberOfShipsAtoB(final int numberOfShipsAtoB) {
    this.numberOfShipsAtoB = numberOfShipsAtoB;
  }

  public void setNumberOfShipsBtoA(final int numberOfShipsBtoA) {
    this.numberOfShipsBtoA = numberOfShipsBtoA;
  }

  @XmlElement(name = "percentageLadenAtoB", namespace = CalculatorSchema.NAMESPACE)
  public int getPercentageLadenAtoB() {
    return percentageLadenAtoB;
  }

  @XmlElement(name = "percentageLadenBtoA", namespace = CalculatorSchema.NAMESPACE)
  public int getPercentageLadenBtoA() {
    return percentageLadenBtoA;
  }

  public void setPercentageLadenAtoB(final int percentageLadenAtoB) {
    this.percentageLadenAtoB = percentageLadenAtoB;
  }

  public void setPercentageLadenBtoA(final int percentageLadenBtoA) {
    this.percentageLadenBtoA = percentageLadenBtoA;
  }

}
