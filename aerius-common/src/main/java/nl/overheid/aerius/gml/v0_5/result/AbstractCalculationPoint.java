/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v0_5.result;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import nl.overheid.aerius.geo.shared.WKTGeometry.TYPE;
import nl.overheid.aerius.gml.v0_5.base.CalculatorSchema;
import nl.overheid.aerius.gml.v0_5.base.FeatureMemberImpl;
import nl.overheid.aerius.gml.v0_5.geo.Point;
import nl.overheid.aerius.gml.v0_5.geo.Polygon;

/**
 *
 */
@XmlType(propOrder = { "point", "representation", "results" })
public abstract class AbstractCalculationPoint extends FeatureMemberImpl {

  private List<ResultProperty> results = new ArrayList<>();
  private Polygon representation;

  @XmlElement(name = "result", namespace = CalculatorSchema.NAMESPACE)
  public List<ResultProperty> getResults() {
    return results;
  }

  public void setResults(final List<ResultProperty> results) {
    this.results = results;
  }

  @XmlElement(name = "representation", namespace = CalculatorSchema.NAMESPACE)
  public Polygon getRepresentation() {
    return representation;
  }

  public void setRepresentation(final Polygon representation) {
    this.representation = representation;
  }

  @Override
  public boolean isValidGeometry(final TYPE type) {
    return type == TYPE.POINT;
  }

  @XmlElement(name = "GM_Point", namespace = CalculatorSchema.NAMESPACE)
  public Point getPoint() {
    return super.getEmissionSourceGeometry().getPoint();
  }

  /**
   * @param point The point to set.
   */
  public void setPoint(final Point point) {
    super.getEmissionSourceGeometry().setPoint(point);
  }
}
