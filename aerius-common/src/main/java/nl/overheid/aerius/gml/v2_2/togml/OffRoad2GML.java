/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.togml;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.v2_2.source.mobile.AbstractOffRoadVehicleSpecification;
import nl.overheid.aerius.gml.v2_2.source.mobile.ConsumptionOffRoadVehicleSpecification;
import nl.overheid.aerius.gml.v2_2.source.mobile.CustomOffRoadMobileSource;
import nl.overheid.aerius.gml.v2_2.source.mobile.OffRoadMobileSource;
import nl.overheid.aerius.gml.v2_2.source.mobile.OffRoadMobileSourceProperty;
import nl.overheid.aerius.gml.v2_2.source.mobile.OperatingHoursOffRoadVehicleSpecification;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleConsumptionSpecification;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleOperatingHoursSpecification;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleSpecification;

/**
 *
 */
class OffRoad2GML extends SpecificSource2GML<OffRoadMobileEmissionSource> {

  @Override
  public nl.overheid.aerius.gml.v2_2.source.EmissionSource convert(final OffRoadMobileEmissionSource emissionSource) {
    final nl.overheid.aerius.gml.v2_2.source.mobile.OffRoadMobileEmissionSource returnSource =
        new nl.overheid.aerius.gml.v2_2.source.mobile.OffRoadMobileEmissionSource();
    final List<OffRoadMobileSourceProperty> mobileSources = new ArrayList<>();

    for (final OffRoadVehicleEmissionSubSource vehicleEmissionValues : emissionSource.getEmissionSubSources()) {
      if (vehicleEmissionValues.isCustomCategory()) {
        mobileSources.add(toCustom(vehicleEmissionValues));
      } else {
        mobileSources.add(toStandard(vehicleEmissionValues));
      }
    }

    returnSource.setOffRoadMobileSources(mobileSources);
    return returnSource;
  }

  private OffRoadMobileSourceProperty toCustom(final OffRoadVehicleEmissionSubSource vehicleEmissionValues) {
    final CustomOffRoadMobileSource gmlMobileSource = new CustomOffRoadMobileSource();
    gmlMobileSource.setCharacteristics(SourceCharacteristics2GML.toGML(vehicleEmissionValues.getCharacteristics(), false));
    gmlMobileSource.setEmissions(getEmissions(vehicleEmissionValues.getEmissionValues(), Substance.NOX));
    gmlMobileSource.setDescription(vehicleEmissionValues.getName());
    setAdditionalSpecifications(vehicleEmissionValues.getVehicleSpecification(), gmlMobileSource);
    return new OffRoadMobileSourceProperty(gmlMobileSource);
  }

  private void setAdditionalSpecifications(final OffRoadVehicleSpecification vehicleSpecification, final CustomOffRoadMobileSource gmlMobileSource) {
    // if no fuelType don't store anything.
    if (vehicleSpecification != null && vehicleSpecification.getFuelType() != null) {
      if (vehicleSpecification instanceof OffRoadVehicleConsumptionSpecification) {
        gmlMobileSource.setOffRoadVehicleSpecification(toConsuming((OffRoadVehicleConsumptionSpecification) vehicleSpecification));
      } else if (vehicleSpecification instanceof OffRoadVehicleOperatingHoursSpecification) {
        gmlMobileSource.setOffRoadVehicleSpecification(toOperatingHours((OffRoadVehicleOperatingHoursSpecification) vehicleSpecification));
      }
    }
  }

  private AbstractOffRoadVehicleSpecification toConsuming(final OffRoadVehicleConsumptionSpecification specification) {
    final ConsumptionOffRoadVehicleSpecification gmlSpecification = new ConsumptionOffRoadVehicleSpecification();
    gmlSpecification.setEmissionFactor(specification.getEmissionFactor());
    gmlSpecification.setEnergyEfficiency(specification.getEnergyEfficiency());
    gmlSpecification.setConsumption(specification.getUsage());
    setCategories(specification, gmlSpecification);
    return gmlSpecification;
  }

  private AbstractOffRoadVehicleSpecification toOperatingHours(final OffRoadVehicleOperatingHoursSpecification specification) {
    final OperatingHoursOffRoadVehicleSpecification gmlSpecification = new OperatingHoursOffRoadVehicleSpecification();
    gmlSpecification.setEmissionFactor(specification.getEmissionFactor());
    gmlSpecification.setLoad(specification.getLoad());
    gmlSpecification.setPower(specification.getPower());
    gmlSpecification.setOperatingHours(specification.getUsage());
    setCategories(specification, gmlSpecification);
    return gmlSpecification;
  }

  private void setCategories(final OffRoadVehicleSpecification specification, final AbstractOffRoadVehicleSpecification gmlSpecification) {
    gmlSpecification.setFuelCode(specification.getFuelType().getCode());
    //No need to do anything with the machinery type.
  }

  private OffRoadMobileSourceProperty toStandard(final OffRoadVehicleEmissionSubSource vehicleEmissionValues) {
    final OffRoadMobileSource gmlMobileSource = new OffRoadMobileSource();
    gmlMobileSource.setCode(vehicleEmissionValues.getStageCategory().getCode());
    gmlMobileSource.setLiterFuelPerYear(vehicleEmissionValues.getFuelage());
    gmlMobileSource.setDescription(vehicleEmissionValues.getName());
    return new OffRoadMobileSourceProperty(gmlMobileSource);
  }

}
