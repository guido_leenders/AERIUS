/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.togml;

import nl.overheid.aerius.gml.v2_2.source.characteristics.AbstractHeatContent;
import nl.overheid.aerius.gml.v2_2.source.characteristics.Building;
import nl.overheid.aerius.gml.v2_2.source.characteristics.CalculatedHeatContent;
import nl.overheid.aerius.gml.v2_2.source.characteristics.EmissionSourceCharacteristics;
import nl.overheid.aerius.gml.v2_2.source.characteristics.SpecifiedHeatContent;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;

/**
 *
 */
final class SourceCharacteristics2GML {

  private SourceCharacteristics2GML() {
    //util class.
  }

  static EmissionSourceCharacteristics toGML(
      final OPSSourceCharacteristics characteristics, final boolean includeOptionals) {
    final EmissionSourceCharacteristics returnCharacteristics = new EmissionSourceCharacteristics();
    returnCharacteristics.setHeatContent(determineHeatContent(characteristics));
    returnCharacteristics.setEmissionHeight(characteristics.getEmissionHeight());
    returnCharacteristics.setSpread(characteristics.getSpread());
    if (includeOptionals) {
      returnCharacteristics.setDiurnalVariation(characteristics.getDiurnalVariationSpecification().getCode());
    }
    returnCharacteristics.setBuilding(determineBuilding(characteristics));
    return returnCharacteristics;
  }

  private static AbstractHeatContent determineHeatContent(final OPSSourceCharacteristics characteristics) {
    if (characteristics.isUserdefinedHeatContent()) {
      final SpecifiedHeatContent heatContent = new SpecifiedHeatContent();
      heatContent.setValue(characteristics.getHeatContent());
      return heatContent;
    } else {
      final CalculatedHeatContent heatContent = new CalculatedHeatContent();
      heatContent.setEmissionTemperature(characteristics.getEmissionTemperature());
      heatContent.setOutflowDiameter(characteristics.getOutflowDiameter());
      heatContent.setOutflowVelocity(characteristics.getOutflowVelocity());
      heatContent.setOutflowDirection(characteristics.getOutflowDirection());
      return heatContent;

    }
  }

  private static Building determineBuilding(final OPSSourceCharacteristics characteristics) {
    final Building building;
    if (characteristics.isBuildingInfluence()) {
      building = new Building();
      building.setHeight(characteristics.getBuildingHeight());
      building.setWidth(characteristics.getBuildingWidth());
      building.setLength(characteristics.getBuildingLength());
      building.setOrientation(characteristics.getBuildingOrientation());
    } else {
      building = null;
    }
    return building;
  }

}
