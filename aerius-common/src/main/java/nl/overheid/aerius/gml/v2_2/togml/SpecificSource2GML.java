/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_2.togml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.gml.v2_2.source.Emission;
import nl.overheid.aerius.gml.v2_2.source.EmissionProperty;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Base class for {@link EmissionSource} objects conversion to GML.
 * @param <T> specific {@link EmissionSource}
 */
abstract class SpecificSource2GML<T extends EmissionSource> {

  protected static final String ID_SEPARATOR = "_";

  abstract nl.overheid.aerius.gml.v2_2.source.EmissionSource convert(T emissionSource) throws AeriusException;

  protected List<EmissionProperty> getEmissions(final EmissionValues emissionValues, final Substance defaultSubstance) {
    final List<EmissionProperty> emissions = new ArrayList<>(emissionValues.entrySet().size() + 1);

    for (final Entry<EmissionValueKey, Double> e : emissionValues.entrySet()) {
      final Emission emission = new Emission();
      emission.setSubstance(e.getKey().getSubstance());
      emission.setValue(e.getValue());
      emissions.add(new EmissionProperty(emission));
    }
    if (emissions.isEmpty()) {
      final Emission emission = new Emission();
      emission.setSubstance(defaultSubstance);
      emission.setValue(0.0);
      emissions.add(new EmissionProperty(emission));
    }
    return emissions;
  }

}
