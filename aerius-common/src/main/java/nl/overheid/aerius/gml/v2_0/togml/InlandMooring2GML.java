/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_0.togml;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.base.geo.Geometry2GML;
import nl.overheid.aerius.gml.v2_0.base.CalculatorSchema;
import nl.overheid.aerius.gml.v2_0.geo.LineString;
import nl.overheid.aerius.gml.v2_0.source.ship.InlandShippingRoute;
import nl.overheid.aerius.gml.v2_0.source.ship.InlandShippingRouteProperty;
import nl.overheid.aerius.gml.v2_0.source.ship.MooringInlandShippingProperty;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 *
 */
class InlandMooring2GML extends SpecificSource2GML<InlandMooringEmissionSource> {

  private final Geometry2GML geometry2gmL;

  public InlandMooring2GML(final Geometry2GML geometry2gmL) {
    this.geometry2gmL = geometry2gmL;
  }

  @Override
  public nl.overheid.aerius.gml.v2_0.source.EmissionSource convert(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    final nl.overheid.aerius.gml.v2_0.source.ship.MooringInlandShippingEmissionSource returnSource =
        new nl.overheid.aerius.gml.v2_0.source.ship.MooringInlandShippingEmissionSource();
    final List<MooringInlandShippingProperty> mooringMaritimeShippings = new ArrayList<>();

    for (final InlandMooringVesselGroup vesselGroupEmissionValues : emissionSource.getEmissionSubSources()) {
      final nl.overheid.aerius.gml.v2_0.source.ship.MooringInlandShipping gmlShip =
          new nl.overheid.aerius.gml.v2_0.source.ship.MooringInlandShipping();
      gmlShip.setCode(vesselGroupEmissionValues.getCategory().getCode());
      gmlShip.setDescription(vesselGroupEmissionValues.getName());
      gmlShip.setAverageResidenceTime(vesselGroupEmissionValues.getResidenceTime());
      gmlShip.setRoutes(
          toShippingRouteProperties(vesselGroupEmissionValues.getRoutes(), emissionSource.getId(), vesselGroupEmissionValues.getId()));
      //we're not adding emissionfactor/description to avoid impression that it will be used on import.
      mooringMaritimeShippings.add(new MooringInlandShippingProperty(gmlShip));
    }
    returnSource.setShips(mooringMaritimeShippings);
    return returnSource;
  }

  private List<InlandShippingRouteProperty> toShippingRouteProperties(final List<InlandMooringRoute> originalRoutes, final int sourceId,
      final int vesselGroupId) throws AeriusException {
    final List<InlandShippingRouteProperty> routes = new ArrayList<>();
    //keep track of an ID so we won't violate unique ID constraints.
    //still depends on the id of vesselGroupEmissionValues being set right in UI however.
    int currentId = 0;
    for (final InlandMooringRoute imr : originalRoutes) {
      final InlandShippingRoute route = new InlandShippingRoute();
      final LineString routeGeometry = geometry2gmL.toXMLLineString(imr.getRoute().getGeometry(), new LineString());
      routeGeometry.getGMLLineString().setId(CalculatorSchema.GML_ID_NAMESPACE
          + ".InlandMooringRoute." + sourceId + ID_SEPARATOR + vesselGroupId + ID_SEPARATOR + currentId);
      route.setRoute(routeGeometry);
      route.setShippingMovements(imr.getTimeUnit().getPerYear(imr.getShipMovementsPerTimeUnit()));
      route.setPercentageLaden(imr.getPercentageLaden());
      route.setDirection(imr.getDirection());
      route.setInlandWaterwayProperty(Inland2GMLUtil.getWaterway(imr.getRoute().getInlandWaterwayType(), sourceId));
      routes.add(new InlandShippingRouteProperty(route));
      currentId++;
    }
    return routes;
  }

}
