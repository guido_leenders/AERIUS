/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v2_0.togml;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.gml.v2_0.source.road.AbstractVehicle;
import nl.overheid.aerius.gml.v2_0.source.road.CustomVehicle;
import nl.overheid.aerius.gml.v2_0.source.road.PartialChangeProperty;
import nl.overheid.aerius.gml.v2_0.source.road.RoadSideBarrierProperty;
import nl.overheid.aerius.gml.v2_0.source.road.SRM2RoadEmissionSource;
import nl.overheid.aerius.gml.v2_0.source.road.SRM2RoadLinearReference;
import nl.overheid.aerius.gml.v2_0.source.road.SpecificVehicle;
import nl.overheid.aerius.gml.v2_0.source.road.StandardVehicle;
import nl.overheid.aerius.gml.v2_0.source.road.VehiclesProperty;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSourceLinearReference;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleCustomEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleSpecificEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;

/**
 * Converts {@link SRM2EmissionSource} to GML data object.
 */
class SRM22Road extends SpecificSource2GML<SRM2EmissionSource> {

  @Override
  public nl.overheid.aerius.gml.v2_0.source.EmissionSource convert(final SRM2EmissionSource emissionSource) {
    final SRM2RoadEmissionSource returnSource = new SRM2RoadEmissionSource();

    returnSource.setVehicles(toVehicleProperties(emissionSource.getEmissionSubSources(), emissionSource.isFreeway()));

    handleFreeWay(emissionSource, returnSource);
    handleTunnel(emissionSource, returnSource);
    handleElevation(emissionSource, returnSource);
    handleBarriers(emissionSource, returnSource);

    returnSource.setDynamicSegments(getDynamicSegments(emissionSource));

    return returnSource;
  }

  private List<VehiclesProperty> toVehicleProperties(final List<VehicleEmissions> vehicleEmissions, final boolean isFreeway) {
    final List<VehiclesProperty> vehiclesList = new ArrayList<>();

    for (final VehicleEmissions vem : vehicleEmissions) {
      final AbstractVehicle vehicle;
      if (vem instanceof VehicleStandardEmissions) {
        vehicle = toVehicleEmissionSource((VehicleStandardEmissions) vem, isFreeway);
      } else if (vem instanceof VehicleSpecificEmissions) {
        vehicle = toVehicleEmissionSource((VehicleSpecificEmissions) vem);
      } else if (vem instanceof VehicleCustomEmissions) {
        vehicle = toVehicleEmissionSource((VehicleCustomEmissions) vem);
      } else {
        throw new IllegalArgumentException("EmissionCategory for traffic not allowed to be null: " + vem);
      }
      vehicle.setVehiclesPerDay(vem.getTimeUnit().toUnit(vem.getVehiclesPerTimeUnit(), TimeUnit.DAY));
      vehiclesList.add(new VehiclesProperty(vehicle));
    }
    return vehiclesList;
  }

  private void handleFreeWay(final SRM2EmissionSource emissionSource, final SRM2RoadEmissionSource returnSource) {
    returnSource.setFreeway(emissionSource.isFreeway());
  }

  private void handleTunnel(final SRM2EmissionSource emissionSource, final SRM2RoadEmissionSource returnSource) {
    //don't return tunnel factor if it's the default value.
    if (Double.doubleToLongBits(emissionSource.getTunnelFactor()) != Double.doubleToLongBits(1.0)) {
      returnSource.setTunnelFactor(emissionSource.getTunnelFactor());
    }
  }

  private void handleElevation(final SRM2EmissionSource emissionSource, final SRM2RoadEmissionSource returnSource) {
    returnSource.setElevation(emissionSource.getElevation());
    //don't set the elevation height if it's normal elevation.
    if (returnSource.getElevation() != RoadElevation.NORMAL) {
      returnSource.setElevationHeight(emissionSource.getElevationHeight());
    }
  }

  private void handleBarriers(final SRM2EmissionSource emissionSource, final SRM2RoadEmissionSource returnSource) {
    if (emissionSource.getBarrierLeft() != null) {
      returnSource.setBarrierLeft(toGMLRoadSideBarrier(emissionSource.getBarrierLeft()));
    }
    if (emissionSource.getBarrierRight() != null) {
      returnSource.setBarrierRight(toGMLRoadSideBarrier(emissionSource.getBarrierRight()));
    }
  }

  private RoadSideBarrierProperty toGMLRoadSideBarrier(final RoadSideBarrier barrier) {
    final nl.overheid.aerius.gml.v2_0.source.road.RoadSideBarrier gmlBarrier = new nl.overheid.aerius.gml.v2_0.source.road.RoadSideBarrier();
    gmlBarrier.setBarrierType(barrier.getBarrierType());
    gmlBarrier.setHeight(barrier.getHeight());
    gmlBarrier.setDistance(barrier.getDistance());
    return new RoadSideBarrierProperty(gmlBarrier);
  }

  private AbstractVehicle toVehicleEmissionSource(final VehicleStandardEmissions vse, final boolean isFreeway) {
    final StandardVehicle sv = new StandardVehicle();

    sv.setStagnationFactor(vse.getStagnationFraction());
    if (vse.getEmissionCategory() != null) {
      sv.setVehicleType(vse.getEmissionCategory().getVehicleType());
      if (isFreeway) {
        sv.setMaximumSpeed(vse.getEmissionCategory().getMaximumSpeed());
        sv.setStrictEnforcement(vse.getEmissionCategory().isStrictEnforcement());
      }
    }
    return sv;
  }

  private AbstractVehicle toVehicleEmissionSource(final VehicleSpecificEmissions vse) {
    final SpecificVehicle sv = new SpecificVehicle();

    sv.setCode(vse.getCategory() == null ? null : vse.getCategory().getCode());
    return sv;
  }

  private AbstractVehicle toVehicleEmissionSource(final VehicleCustomEmissions vce) {
    final CustomVehicle cv = new CustomVehicle();
    cv.setDescription(vce.getDescription());
    cv.setEmissions(getEmissions(vce.getEmissionValues(), Substance.NOX));
    return cv;
  }

  private List<PartialChangeProperty> getDynamicSegments(final SRM2EmissionSource emissionSource) {
    final List<PartialChangeProperty> partialChangeProperties = new ArrayList<>();
    if (!emissionSource.getDynamicSegments().isEmpty()) {
      for (final SRM2EmissionSourceLinearReference dynamicSegment : emissionSource.getDynamicSegments()) {
        addPartialChanges(dynamicSegment, partialChangeProperties);
      }

    }
    return partialChangeProperties;
  }

  private void addPartialChanges(final SRM2EmissionSourceLinearReference dynamicSegment,
      final List<PartialChangeProperty> partialChangeProperties) {
    final SRM2RoadLinearReference dynamicSegmentGML = new SRM2RoadLinearReference();

    dynamicSegmentGML.setFromPosition(dynamicSegment.getStart());
    dynamicSegmentGML.setToPosition(dynamicSegment.getEnd());
    dynamicSegmentGML.setFreeway(dynamicSegment.getFreeway());
    dynamicSegmentGML.setTunnelFactor(dynamicSegment.getTunnelFactor());
    dynamicSegmentGML.setElevation(dynamicSegment.getElevation());
    dynamicSegmentGML.setElevationHeight(dynamicSegment.getElevationHeight());

    if (dynamicSegment.getBarrierLeft() != null) {
      dynamicSegmentGML.setBarrierLeft(toGMLRoadSideBarrier(dynamicSegment.getBarrierLeft()));
    }

    if (dynamicSegment.getBarrierRight() != null) {
      dynamicSegmentGML.setBarrierRight(toGMLRoadSideBarrier(dynamicSegment.getBarrierRight()));
    }

    partialChangeProperties.add(new PartialChangeProperty(dynamicSegmentGML));
  }

}
