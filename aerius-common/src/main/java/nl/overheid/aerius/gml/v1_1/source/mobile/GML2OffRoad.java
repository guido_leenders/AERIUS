/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.v1_1.source.mobile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.gml.base.AbstractGML2Specific;
import nl.overheid.aerius.gml.base.GMLConversionData;
import nl.overheid.aerius.gml.v1_1.source.Emission;
import nl.overheid.aerius.gml.v1_1.source.EmissionProperty;
import nl.overheid.aerius.gml.v1_1.source.characteristics.GML2SourceCharacteristics;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.FuelTypeProperties;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMachineryType.MachineryFuelType;
import nl.overheid.aerius.shared.domain.sector.category.OffRoadMobileSourceCategory;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource.OffRoadVehicleEmissionSubSource.SourceCategoryType;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleConsumptionSpecification;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleOperatingHoursSpecification;
import nl.overheid.aerius.shared.domain.source.OffRoadVehicleSpecification;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *
 */
public class GML2OffRoad extends
    AbstractGML2Specific<OffRoadMobileEmissionSource, nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource> {

  private static final Logger LOG = LoggerFactory.getLogger(GML2OffRoad.class);
  private final Map<Integer, DiurnalVariationSpecification> diurnalVariationMap;

  /**
   * @param conversionData The conversionData to use.
   */
  public GML2OffRoad(final GMLConversionData conversionData) {
    super(conversionData);

    diurnalVariationMap = createDiurnalVariationMap(conversionData.getSectorCategories().getDiurnalVariations());
  }

  @Override
  public nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource convert(final OffRoadMobileEmissionSource source)
      throws AeriusException {
    final nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource emissionSource =
        new nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource();

    for (final OffRoadMobileSourceProperty offRoadMobileSourceProperty : source.getOffRoadMobileSources()) {
      final AbstractOffRoadMobileSource offRoadMobileSource = offRoadMobileSourceProperty.getProperty();
      if (offRoadMobileSource instanceof OffRoadMobileSource) {
        emissionSource.getEmissionSubSources().add(convert((OffRoadMobileSource) offRoadMobileSource,
            emissionSource.getEmissionSubSources().size(), source.getId()));
      } else if (offRoadMobileSource instanceof CustomOffRoadMobileSource) {
        emissionSource.getEmissionSubSources().add(convert((CustomOffRoadMobileSource) offRoadMobileSource,
            emissionSource.getEmissionSubSources().size()));
      } else {
        LOG.error("Don't know how to treat offroad mobile source type: " + offRoadMobileSource.getClass());
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }

    return emissionSource;
  }

  private OffRoadVehicleEmissionSubSource convert(
      final OffRoadMobileSource mobileSource, final int proposedId, final String sourceId) throws AeriusException {
    final OffRoadVehicleEmissionSubSource vehicleEmissionValues = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValues.setId(proposedId);
    vehicleEmissionValues.setCategoryType(SourceCategoryType.STAGE_CLASS);
    vehicleEmissionValues.setName(mobileSource.getDescription());
    vehicleEmissionValues.setFuelage(mobileSource.getLiterFuelPerYear());
    final OffRoadMobileSourceCategory category = getCategories().determineOffRoadMobileSourceCategoryByCode(mobileSource.getCode());
    if (category == null) {
      //If we can't find the code, throw exception.
      throw new AeriusException(Reason.GML_UNKNOWN_MOBILE_SOURCE_CODE, sourceId, mobileSource.getCode());
    }
    vehicleEmissionValues.setStageCategory(category);
    return vehicleEmissionValues;
  }

  private OffRoadVehicleEmissionSubSource convert(final CustomOffRoadMobileSource customMobileSource, final int proposedId) {
    final OffRoadVehicleEmissionSubSource vehicleEmissionValues = new OffRoadVehicleEmissionSubSource();
    vehicleEmissionValues.setId(proposedId);
    vehicleEmissionValues.setCategoryType(SourceCategoryType.DEVIATING_CLASS);
    vehicleEmissionValues.setName(customMobileSource.getDescription());
    vehicleEmissionValues.setCharacteristics(GML2SourceCharacteristics.fromGML(customMobileSource.getCharacteristics(), null, diurnalVariationMap));
    for (final EmissionProperty emissionProperty : customMobileSource.getEmissions()) {
      final Emission emission = emissionProperty.getProperty();
      if (emission.getSubstance() == Substance.NOX) {
        vehicleEmissionValues.setEmissionNOX(emission.getValue());
      }
    }
    final OffRoadVehicleSpecification specification = convert(customMobileSource.getOffRoadVehicleSpecification());
    if (specification != null) {
      vehicleEmissionValues.setVehicleSpecification(specification);
      vehicleEmissionValues.setEmissionNOX(specification.getEmission());
    }
    return vehicleEmissionValues;
  }

  private OffRoadVehicleSpecification convert(final AbstractOffRoadVehicleSpecification gmlSpecification) {
    OffRoadVehicleSpecification specification = null;
    if (gmlSpecification instanceof ConsumptionOffRoadVehicleSpecification) {
      specification = convertSpecific((ConsumptionOffRoadVehicleSpecification) gmlSpecification);
    } else if (gmlSpecification instanceof OperatingHoursOffRoadVehicleSpecification) {
      specification = convertSpecific((OperatingHoursOffRoadVehicleSpecification) gmlSpecification);
    }
    return specification;
  }

  private OffRoadVehicleConsumptionSpecification convertSpecific(final ConsumptionOffRoadVehicleSpecification gmlSpecification) {
    final OffRoadVehicleConsumptionSpecification specification = new OffRoadVehicleConsumptionSpecification();
    specification.setEmissionFactor(gmlSpecification.getEmissionFactor());
    specification.setEnergyEfficiency(gmlSpecification.getEnergyEfficiency());
    specification.setUsage(gmlSpecification.getConsumption());
    setCategories(specification, gmlSpecification);
    return specification;
  }

  private OffRoadVehicleOperatingHoursSpecification convertSpecific(final OperatingHoursOffRoadVehicleSpecification gmlSpecification) {
    final OffRoadVehicleOperatingHoursSpecification specification = new OffRoadVehicleOperatingHoursSpecification();
    specification.setEmissionFactor(gmlSpecification.getEmissionFactor());
    specification.setLoad(gmlSpecification.getLoad());
    specification.setPower(gmlSpecification.getPower());
    specification.setUsage(gmlSpecification.getOperatingHours());
    setCategories(specification, gmlSpecification);
    return specification;
  }

  private Map<Integer, DiurnalVariationSpecification> createDiurnalVariationMap(final List<DiurnalVariationSpecification> diurnalVariations) {
    final Map<Integer, DiurnalVariationSpecification> map = new HashMap<>();

    for (final DiurnalVariationSpecification spec : diurnalVariations) {
      map.put(spec.getId(), spec);
    }

    return map;
  }

  private void setCategories(final OffRoadVehicleSpecification specification, final AbstractOffRoadVehicleSpecification gmlSpecification) {
    //GML doesn't contain a machinery type code, so always use the OTHER version.
    final OffRoadMachineryType machineryType = getCategories().determineMachineryTypeCategoryByCode(OffRoadMachineryType.OTHER_CODE);
    MachineryFuelType fuelType = null;
    for (final FuelTypeProperties categoryFuelType : machineryType.getFuelTypes()) {
      if (categoryFuelType.getFuelType().getCode().equalsIgnoreCase(gmlSpecification.getFuelCode())) {
        fuelType = categoryFuelType.getFuelType();
      }
    }
    specification.setMachineryType(machineryType);
    specification.setFuelType(fuelType);
  }

}
