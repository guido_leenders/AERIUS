/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.paa;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.pdf.PdfReader;

import nl.overheid.aerius.PAAConstants;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.HashUtil;

/**
 * Util class to read a PAA import data from a stream and convert them to internal
 * data structure objects.
 */
public final class PAAImport {

  private static final Logger LOG = LoggerFactory.getLogger(PAAImport.class);

  private PAAImport() {
    // util class
  }

  /**
   * Retrieve all emission sources from the pdf calculation metadata field.
   * @param paaInputStream The inputstream containing the PAA.
   * @return List of all emission sources defined in the pdf calculation metadata field.
   * @throws AeriusException throws Aerius exception
   */
  public static ScenarioGMLs importPAAFromStream(final InputStream paaInputStream) throws AeriusException {
    final ArrayList<String> gmlStrings;
    final String metadataHash;
    try {
      // Open pdf
      final PdfReader reader = new PdfReader(paaInputStream);

      gmlStrings = readGMLStrings(reader);

      // Get metadata
      metadataHash = readMetadataHash(reader);
    } catch (final IOException e) {
      LOG.error("IOException while importing PAA", e);
      throw new AeriusException(Reason.IMPORT_FILE_COULD_NOT_BE_READ);
    }

    // Validate calculation
    validate(gmlStrings, metadataHash);

    return toScenarioGMLs(gmlStrings);
  }

  private static ArrayList<String> readGMLStrings(final PdfReader reader) throws AeriusException {
    final ArrayList<String> gmlStrings = new ArrayList<>();
    // Open pdf

    // Try to get calculation
    if (!reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_GML_KEY)) {
      throw new AeriusException(Reason.PAA_VALIDATION_FAILED);
    }
    //first situation should always be there.
    gmlStrings.add((String) reader.getInfo().get(PAAConstants.CALC_PDF_METADATA_GML_KEY));
    //second situation might not be
    if (reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_2ND_GML_KEY)) {
      gmlStrings.add((String) reader.getInfo().get(PAAConstants.CALC_PDF_METADATA_2ND_GML_KEY));
    }
    return gmlStrings;
  }

  private static String readMetadataHash(final PdfReader reader) throws AeriusException {
    // Try to get validation key
    if (!reader.getInfo().containsKey(PAAConstants.CALC_PDF_METADATA_HASH_KEY)) {
      throw new AeriusException(Reason.PAA_VALIDATION_FAILED);
    }
    return (String) reader.getInfo().get(PAAConstants.CALC_PDF_METADATA_HASH_KEY);
  }

  private static void validate(final ArrayList<String> gmlStrings, final String metadataHash) throws AeriusException {
    final String hash = HashUtil.generateSaltedHash(PAAConstants.CALC_PDF_METADATA_HASH_SALT, gmlStrings);
    if (!hash.equals(metadataHash)) {
      throw new AeriusException(Reason.PAA_VALIDATION_FAILED);
    }
  }

  private static ScenarioGMLs toScenarioGMLs(final ArrayList<String> gmlStrings) {
    final ScenarioGMLs scenarioGMLs = new ScenarioGMLs();
    if (gmlStrings.size() > 1) {
      //proposed situation is the last GML in the list, current is the first.
      scenarioGMLs.setCurrentGML(gmlStrings.get(0));
      scenarioGMLs.setProposedGML(gmlStrings.get(gmlStrings.size() - 1));
    } else if (gmlStrings.size() == 1) {
      scenarioGMLs.setProposedGML(gmlStrings.get(0));
    }
    return scenarioGMLs;
  }
}
