/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import nl.overheid.aerius.taskmanager.client.NoopCallback;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Utility class to send message tasks to the workers.
 */
public final class MessageTaskClient {

  private static final String TOKEN_CALC_CREATION_DATE_FORMAT = "EEEE dd MMMM yyyy";
  private static final String TOKEN_CALC_CREATION_TIME_FORMAT = "HH:mm";
  private static final String TOKEN_CALC_CREATION_DATE_TIME_FORMAT = TOKEN_CALC_CREATION_DATE_FORMAT + " " + TOKEN_CALC_CREATION_TIME_FORMAT;

  private MessageTaskClient() {
    // utility class
  }

  /**
   * @param client The client to use to send the task.
   * @param messageData The object containing the data for the message
   * @throws IOException In case putting task on queue failed.
   */
  public static void startMessageTask(final TaskManagerClient client, final MailMessageData messageData) throws IOException {
    client.sendTask(messageData, new NoopCallback(), WorkerType.MESSAGE, QueueEnum.MESSAGE);
  }

  /**
   * @param date The date to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted date. (only date information, no time).
   */
  public static String getDefaultDateFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CALC_CREATION_DATE_FORMAT, locale).format(date);
  }

  /**
   * @param date The time to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted time. (only time information, no date).
   */
  public static String getDefaultTimeFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CALC_CREATION_TIME_FORMAT, locale).format(date);
  }

  /**
   * @param date The date time to format according to default format for mail text.
   * @param locale The locale to use to format it.
   * @return The formatted date and time.
   */
  public static String getDefaultDateTimeFormatted(final Date date, final Locale locale) {
    return new SimpleDateFormat(TOKEN_CALC_CREATION_DATE_TIME_FORMAT, locale).format(date);
  }

}
