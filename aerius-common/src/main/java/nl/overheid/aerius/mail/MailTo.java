/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 */
public class MailTo implements Serializable {

  private static final long serialVersionUID = 2463260408096437518L;

  private final ArrayList<String> toAddresses = new ArrayList<>();
  private final ArrayList<String> ccAddresses = new ArrayList<>();
  private final ArrayList<String> bccAddresses = new ArrayList<>();

  public MailTo() {
    // no op
  }

  public MailTo(final String emailTo) {
    toAddresses.add(emailTo);
  }

  public void addTo(final String to) {
    toAddresses.add(to);
  }

  public void addCc(final String cc) {
    ccAddresses.add(cc);
  }

  public void addBcc(final String bcc) {
    bccAddresses.add(bcc);
  }

  public ArrayList<String> getToAddresses() {
    return toAddresses;
  }

  public ArrayList<String> getCcAddresses() {
    return ccAddresses;
  }

  public ArrayList<String> getBccAddresses() {
    return bccAddresses;
  }

  @Override
  public String toString() {
    return "MailTo [" + toString("to", toAddresses) + toString("cc", ccAddresses) + toString("bcc", bccAddresses) + ']';
  }

  private String toString(final String type, final List<String> addresses) {
      return addresses.isEmpty() ? "" : '(' + type + "=" + addresses.size() + ':' + ArrayUtils.toString(addresses) + ')';
  }
}
