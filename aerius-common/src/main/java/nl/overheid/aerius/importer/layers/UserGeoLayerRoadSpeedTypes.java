/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.layers;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;

public class UserGeoLayerRoadSpeedTypes extends MultiValueUserGeoLayerHandler<SRM2EmissionSource> {

  private static final String TYPE = "ROAD_SPEED_TYPES";

  UserGeoLayerRoadSpeedTypes() {
    super(TYPE);
  }

  @Override
  String[][] getValues(final SRM2EmissionSource source) {

    final String strictEnforcement = getStream(source)
        .map(i -> i.getEmissionCategory().isStrictEnforcement())
        .distinct()
        .map(Object::toString)
        .reduce((a, b) -> "") // One or non values
        .get();

    final String maximumSpeed = getStream(source)
        .map(i -> i.getEmissionCategory().getMaximumSpeed())
        .distinct()
        .sorted()
        .map(Object::toString)
        .collect(Collectors.joining("-"));

    return new String[][] {{maximumSpeed, strictEnforcement}};
  }

  @Override
  boolean processSource(final SRM2EmissionSource source) {
    return getStream(source).count() > 0;
  }

  private Stream<VehicleStandardEmissions> getStream(final SRM2EmissionSource source) {
    return source.getEmissionSubSources().stream()
        .filter(i -> i instanceof VehicleStandardEmissions)
        .map(VehicleStandardEmissions.class::cast)
        .filter(i -> VehicleType.LIGHT_TRAFFIC == i.getEmissionCategory().getVehicleType());
  }
}
