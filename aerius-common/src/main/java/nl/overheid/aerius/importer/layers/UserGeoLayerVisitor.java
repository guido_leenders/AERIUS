/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.layers;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceVisitor;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

public class UserGeoLayerVisitor implements EmissionSourceVisitor<Integer> {

  private static final List<UserGeoLayerHandler<SRM2EmissionSource>> SRM2_HANDLERS = Arrays.asList(
      new UserGeoLayerRoadTotalVerhiclesPerDay(),
      new UserGeoLayerRoadTotalVerhiclesPerDayLightTraffic(),
      new UserGeoLayerSourceLabels(), // currently only used for SRM2
      new UserGeoLayerRoadSpeedTypes());

  private final Connection con;
  private final int importedImaerFileId;
  private final GMLWriter writer;
  private final int year;

  public UserGeoLayerVisitor(final Connection con, final GMLWriter writer, final int year, final int importedImaerFileId) {
    this.con = con;
    this.writer = writer;
    this.year = year;
    this.importedImaerFileId = importedImaerFileId;
  }

  @Override
  public Integer visit(final FarmEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final GenericEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final InlandMooringEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final InlandRouteEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final MaritimeMooringEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final MaritimeRouteEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final OffRoadMobileEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final PlanEmissionSource emissionSource) throws AeriusException {
    return 0;
  }

  @Override
  public Integer visit(final SRM2EmissionSource emissionSource) throws AeriusException {
    int amount = 0;

    for (final UserGeoLayerHandler<SRM2EmissionSource> handler : SRM2_HANDLERS) {
      if (handler.handle(con, writer, importedImaerFileId, emissionSource, year)) {
        amount++;
      }
    }

    return amount;
  }

  @Override
  public Integer visit(final SRM2NetworkEmissionSource network) throws AeriusException {
    int amount = 0;

    for (final EmissionSource source : network.getEmissionSources()) {
      amount += source.accept(this);
    }

    return amount;
  }

}
