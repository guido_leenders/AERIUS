/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.layers;

import nl.overheid.aerius.shared.domain.source.EmissionSource;

abstract class MultiValueUserGeoLayerHandler<T extends EmissionSource> extends UserGeoLayerHandler<T> {

  protected static final String DELIMITER_FIELD = ";";
  protected static final String DELIMITER_ROW = "|";

  private static final String DELIMITER_REGEX = "[\\|;]"; // matches both field and row delimiter

  MultiValueUserGeoLayerHandler(final String type) {
    super(type);
  }

  abstract String[][] getValues(T source);

  @Override
  final String getValue(final T source) {
    final String[][] values = getValues(source);

    final StringBuilder builder = new StringBuilder();
    for (int i = 0; i < values.length; ++i) {
      final String[] rows = values[i];
      if (i > 0) {
        builder.append(DELIMITER_ROW);
      }

      for (int j = 0; j < rows.length; ++j) {
        final String column = rows[j];
        if (j > 0) {
          builder.append(DELIMITER_FIELD);
        }

        builder.append(sanitiseValue(column));
      }
    }
    return builder.toString();
  }

  /**
   * Replaces field and row delimiters from the value as they're not allowed to be used.
   * @param value The value to sanitise.
   * @return
   */
  protected String sanitiseValue(final String value) {
    return value == null ? null : value.replaceAll(DELIMITER_REGEX, "");
  }

}
