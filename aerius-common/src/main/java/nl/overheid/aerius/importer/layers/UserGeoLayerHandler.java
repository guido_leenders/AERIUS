/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer.layers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.UserGeoLayerRepository;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

abstract class UserGeoLayerHandler<T extends EmissionSource> {

  private static final Logger LOG = LoggerFactory.getLogger(UserGeoLayerHandler.class);

  private final String type;

  UserGeoLayerHandler(final String type) {
    this.type = type;
  }

  /**
   * Whether to process this source.
   * @return true if it's needs to be inserted or not.
   */
  boolean processSource(final T source) {
    // process all sources by default - override if this is not the case.
    return true;
  }

  abstract String getValue(T source);

  boolean handle(final Connection con, final GMLWriter writer, final int importedImaerFileId, final T source, final int year) throws AeriusException {
    final boolean doInsert = processSource(source);

    if (doInsert) {
      try {
        insertFeature(con, writer, importedImaerFileId, source, year, getValue(source));
      } catch (final SQLException e) {
        LOG.error("SQL Exception while inserting feature", e);
        throw new AeriusException(Reason.SQL_ERROR);
      }
    }

    return doInsert;
  }

  private void insertFeature(final Connection con, final GMLWriter writer, final int importedImaerFileId, final T source, final int year,
      final String value) throws SQLException, AeriusException {
    Integer userGeoLayerId = UserGeoLayerRepository.getUserGeoLayerId(con, importedImaerFileId, type);
    if (userGeoLayerId == null) {
      userGeoLayerId = UserGeoLayerRepository.insertUserGeoLayer(con, importedImaerFileId, type);
    }

    final String sourceId = Integer.toString(source.getId());
    Integer importedImaerFeatureId = UserGeoLayerRepository.getImaerFeatureId(con, importedImaerFileId, sourceId);
    if (importedImaerFeatureId == null) {
      importedImaerFeatureId = UserGeoLayerRepository.insertImaerFeature(
        con, importedImaerFileId, sourceToFeature(writer, source, year), sourceId, source.getGeometry());
    }

    UserGeoLayerRepository.insertUserGeoLayerFeature(con, userGeoLayerId, importedImaerFeatureId, value);
  }

  private String sourceToFeature(final GMLWriter writer, final T source, final int year) throws AeriusException {
    try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      writer.writeEmissionSource(outputStream, source, year);

      return outputStream.toString(StandardCharsets.UTF_8.name());
    } catch (final IOException e) {
      LOG.error("Error converting emission source to feature", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

}
