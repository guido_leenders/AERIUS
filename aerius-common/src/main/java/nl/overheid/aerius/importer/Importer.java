/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.PermitCalculationRadiusTypesRepository;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.GMLMetaDataReader;
import nl.overheid.aerius.gml.GMLReader;
import nl.overheid.aerius.gml.GMLReaderFactory;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.ops.importer.BrnImportReader;
import nl.overheid.aerius.ops.importer.RcpImportReader;
import nl.overheid.aerius.paa.PAAImport;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.ScenarioGMLs;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceCheckLimits;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.util.GeometryUtil;
import nl.overheid.aerius.util.LocaleUtils;
import nl.overheid.aerius.validation.ValidationVisitor;

/**
 * Util class to convert an input stream to AERIUS data structures. The Importer should not be reused to read multiple data objects, since the
 * importer has state data.
 */
public class Importer {

  private static final Logger LOG = LoggerFactory.getLogger(Importer.class);
  private static final int MAX_FILES_TO_IMPORT = 2;
  private final SectorCategories categories;
  private final List<PermitCalculationRadiusType> permitCalculationRadiusTypes;

  private final ImportResult importResult = new ImportResult();
  private boolean useValidSectors = true;
  private boolean useImportedLanduses;
  private boolean useValidMetaData;

  private boolean includeCalculationPoints = true;
  private boolean includeSources = true;
  private boolean includeResults;
  private boolean validateAgainstSchema = true;
  private boolean validateStrict;
  private final GMLReaderFactory factory;

  public Importer(final PMF pmf) throws SQLException, AeriusException {
    this(pmf, LocaleUtils.getDefaultLocale());
  }

  public Importer(final PMF pmf, final Locale locale) throws SQLException, AeriusException {
    this(pmf, SectorRepository.getSectorCategories(pmf, locale), PermitCalculationRadiusTypesRepository.getTypes(pmf, locale));
  }

  private Importer(final PMF pmf, final SectorCategories categories, final List<PermitCalculationRadiusType> radiusTypes)
      throws SQLException, AeriusException {
    this.categories = categories;
    this.permitCalculationRadiusTypes = radiusTypes;
    factory = GMLReaderFactory.getFactory(pmf);
  }

  /**
   * If set to true, the importer will use valid AERIUS sectors when importing .BRN files. If not found, the SECTOR_DEFAULT is used. If set to false,
   * the importer will use a 'fake' sector with the ID supplied in the .BRN file. Default is true.
   */
  public void setUseValidSectors(final boolean useValidSectors) {
    this.useValidSectors = useValidSectors;
  }

  /**
   * If set to true, the importer will use the landuse columns supplied in the .RCP file when constructing AeriusPoints. If set to false, the importer
   * will not use the landuse, and points will be only x,y,label. Default is false.
   * <p>This is specific for RCP files. <b>When this value is set to true it also will use the name column as id if it contains an integer</b>.
   */
  public void setUseImportedLanduses(final boolean useImportedLanduses) {
    this.useImportedLanduses = useImportedLanduses;
  }

  /**
   * If set to true, metadata in GML files will be checked for required fields (for Register). Any missing field will result in an AeriusException
   * added to the errors list. If set to false, the check will not be done. Default is false;
   */
  public void setUseValidMetaData(final boolean useValidMetaData) {
    this.useValidMetaData = useValidMetaData;
  }

  /**
   * If set to true, calculation points will be imported from GML files. If set to false, calculation points will be ignored. Default is true.
   */
  public void setIncludeCalculationPoints(final boolean includeCalculationPoints) {
    this.includeCalculationPoints = includeCalculationPoints;
  }

  /**
   * If set to true, sources will be imported from GML files. If set to false, sources will be ignored. Default is true.
   */
  public void setIncludeSources(final boolean includeSources) {
    this.includeSources = includeSources;
  }

  /**
   * If set to true, results for points will be imported from GML files. If set to false, results will be ignored. Default is false.
   */
  public void setIncludeResults(final boolean includeResults) {
    this.includeResults = includeResults;
  }

  /**
   * If set to true, the GML file(s) will be validated against the XSD schema before importing.. If set to false, this check will not be done. Default
   * is true.
   */
  public void setValidateAgainstSchema(final boolean validate) {
    this.validateAgainstSchema = validate;
  }

  /**
   * If set to true, the GML file(s) warnings will be forced as exceptions. If set to false, this check will do nothing and returns warnings and
   * exceptions. Default is false.
   */
  public void setValidateStrict(final boolean strict) {
    this.validateStrict = strict;
  }

  public boolean isValidateStrict() {
    return validateStrict;
  }

  /**
   * Read the data from the input stream and return a ImportResult containing useable data.
   *
   * @param filename name of the input stream to import, used to determine type
   * @param inputStream Read data from this stream
   * @return {@link ImportResult}
   * @throws AeriusException incase of an internal AERIUS specific error
   * @throws IOException on IO error
   */
  public ImportResult convertInputStream2ImportResult(final String filename, final InputStream inputStream) throws IOException, AeriusException {
    return convertInputStream2ImportResult(filename, inputStream, null);
  }

  /**
   * Same as {@link #convertInputStream2ImportResult(InputStream, ImportType, Substance)} but with limit check.
   *
   * @param filename filename
   * @param inputStream Read data from this stream
   * @param sub The substance
   * @param limits data object with limits to check against
   * @return {@link ImportResult}
   * @throws AeriusException incase of an internal AERIUS specific error
   * @throws IOException on IO error
   * @throws SQLException on DB error
   */
  public ImportResult convertInputStream2ImportResult(final String filename, final InputStream inputStream, final Substance sub)
      throws AeriusException, IOException {
    return convertInputStream2ImportResult(filename, inputStream, sub, null);
  }

  /**
   * Read the data from the input stream and return a ImportResult containing useable data.
   *
   * @param filename name of file to import
   * @param inputStream Read data from this stream
   * @param sub The substance
   * @return {@link ImportResult}
   * @throws AeriusException in case of an internal AERIUS specific error
   * @throws IOException on IO error
   */
  public ImportResult convertInputStream2ImportResult(final String filename, final InputStream inputStream, final Substance sub,
      final CalculatorLimits limits) throws IOException, AeriusException {
    final ImportType type = checkFileType(filename);

    switch (type) {
    case BRN:
      final BrnImportReader brnImportReader = new BrnImportReader(useValidSectors);
      brnImportReader.read(filename, inputStream, categories, sub, importResult);
      break;
    case RCP:
      final RcpImportReader rcpImportReader = new RcpImportReader(useImportedLanduses);
      rcpImportReader.read(filename, inputStream, categories, sub, importResult);
      break;
    case SRM2:
      final LegacyNSLImportReader srm2Importer = new LegacyNSLImportReader();
      srm2Importer.read(filename, inputStream, categories, sub, importResult);
      break;
    case GML:
      convertGML2ImportResult(inputStream, importResult);
      break;
    case PAA:
      convertPAA2ImportResult(inputStream, importResult);
      break;
    case ZIP:
      convertZIP2ImportResult(inputStream, sub);
      break;
    default:
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    importResult.setType(type);
    importResult.setSrid(factory.getSrid());
    validateImportSources(importResult);
    checkIntersections(importResult.getSourceLists());
    validateStrict(importResult, isValidateStrict());
    checkResultLimits(importResult, limits);
    return importResult;
  }

  private void validateImportSources(final ImportResult importResult) {
    final ValidationVisitor visitor = new ValidationVisitor(importResult.getExceptions(), importResult.getWarnings());
    for (final EmissionSourceList emissionSourceList : importResult.getSourceLists()) {
      visitor.visitSources(emissionSourceList);
    }
  }

  private ImportResult convertPAA2ImportResult(final InputStream inputStream, final ImportResult result) throws IOException, AeriusException {
    // Need context to set default values when needed during conversion.
    // retrieve GML strings from paa.
    final ScenarioGMLs scenarioGMLs = PAAImport.importPAAFromStream(inputStream);
    final boolean originalIncludeCalculationPoints = includeCalculationPoints;
    includeCalculationPoints = false;
    // current situation first (if available).
    if (scenarioGMLs.getCurrentGML() != null) {
      try (final InputStream gmlInputStream = new ByteArrayInputStream(scenarioGMLs.getCurrentGML().getBytes(StandardCharsets.UTF_8))) {
        importGML(gmlInputStream, result);
      }
    }
    includeCalculationPoints = originalIncludeCalculationPoints;
    // proposed situation.
    try (final InputStream gmlInputStream = new ByteArrayInputStream(scenarioGMLs.getProposedGML().getBytes(StandardCharsets.UTF_8))) {
      importGML(gmlInputStream, result);
    }
    return result;
  }

  private void convertZIP2ImportResult(final InputStream inputStream, final Substance sub) throws IOException, AeriusException {
    boolean foundFiles = false;
    try (ZipInputStream zipStream = new ZipInputStream(inputStream)) {
      ZipEntry zipEntry;
      while (((zipEntry = zipStream.getNextEntry()) != null) && (importResult.getSourceLists().size() < MAX_FILES_TO_IMPORT)) {
        final String name = zipEntry.getName();
        final ImportType entryMatchType = ImportType.determineByFilename(name);
        if (entryMatchType != null) {
          convertInputStream2ImportResult(name, IOUtils.toBufferedInputStream(zipStream), sub);
          foundFiles = true;
        }
        // else skip this file, not supported.
      }
    } catch (final EOFException e) {
      LOG.debug("Zip looks corrupt", e);
      throw new AeriusException(Reason.ZIP_WITHOUT_USABLE_FILES);
    }
    if (!foundFiles) {
      throw new AeriusException(Reason.ZIP_WITHOUT_USABLE_FILES);
    }
  }

  private void convertGML2ImportResult(final InputStream inputStream, final ImportResult importResult) throws AeriusException {
    // Need context to set default values when needed during conversion.
    importGML(inputStream, importResult);
    if (includeSources) {
      validateSources(importResult);
    }
  }

  private void importGML(final InputStream inputStream, final ImportResult result) throws AeriusException {
    final GMLReader reader = createGMLReader(inputStream, result);
    setImportResultMetaData(result, reader);
    validateMetaData(result.getMetaData(), result.getExceptions(), useValidMetaData);
    validateGMLVersion(reader.getVersion(), result.getWarnings());

    if (includeSources) {
      result.addSources(reader.readEmissionSourceList(getKeys(result.getImportedYear())));
    } else {
      final EmissionSourceList esl = new EmissionSourceList();
      result.addSources(esl);
      reader.setName(esl);
    }
    if (includeCalculationPoints || includeResults) {
      final List<AeriusPoint> points = reader.getAeriusPoints(includeResults);
      if (includeCalculationPoints) {
        result.getCalculationPoints().merge(filterCalculationPoints(points));
      }
      if (includeResults) {
        result.addResultPoints(filterResultPoints(points));
      }
    }
    LOG.info("GML imported with GML version: {}", reader.getVersion());
  }

  protected GMLReader createGMLReader(final InputStream inputStream, final ImportResult result) throws AeriusException {
    return factory.createReader(FilterImportUtil.filterResults(inputStream, includeResults), categories, validateAgainstSchema, 
        result.getExceptions(), result.getWarnings());
  }

  private void validateGMLVersion(final AeriusGMLVersion version, final ArrayList<AeriusException> warnings) throws AeriusException {
    if (GMLWriter.LATEST_WRITER_VERSION != version) {
      warnings.add(new AeriusException(Reason.GML_VERSION_NOT_LATEST, version.toString(), GMLWriter.LATEST_WRITER_VERSION.toString()));
    }
  }

  private static void validateSources(final ImportResult result) {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    final Validator validator = factory.getValidator();
    final Set<ConstraintViolation<ImportResult>> resultValidations = validator.validate(result);
    for (final ConstraintViolation<ImportResult> cv : resultValidations) {
      result.getExceptions().add(new AeriusException(Reason.GML_VALIDATION_FAILED, cv.getMessage()));
    }
    for (final EmissionSourceList esl : result.getSourceLists()) {
      for (final EmissionSource emissionSource : esl) {
        final Set<ConstraintViolation<EmissionSource>> constraintViolations = validator.validate(emissionSource);
        if (!constraintViolations.isEmpty()) {
          for (final ConstraintViolation<EmissionSource> cv : constraintViolations) {
            result.getExceptions().add(new AeriusException(Reason.SOURCE_VALIDATION_FAILED, emissionSource.getLabel(), cv.getMessage(),
                cv.getInvalidValue() == null ? "" : String.valueOf(cv.getInvalidValue())));
          }
        }
        if (!emissionSource.hasAnyEmissions()) {
          result.getWarnings().add(new AeriusException(Reason.GML_SOURCE_NO_EMISSION, emissionSource.getLabel()));
        }
      }
    }
  }

  /**
   * Return list of calculation points from the GML data.
   *
   * @param pmf database connection
   * @param points imported points
   * @param includeResults include results (receptors) when true
   * @return list of calculation points from gml
   * @throws AeriusException
   */
  private static CalculationPointList filterCalculationPoints(final List<AeriusPoint> points) throws AeriusException {
    final CalculationPointList list = new CalculationPointList();

    points.stream().filter(p -> p.getPointType() == AeriusPointType.POINT).forEach(list::add);
    return list;
  }

  /**
   * Returns list of result points. This can be both point and receptor results.
   *
   * @param points list of points to filter
   * @param b
   * @return
   */
  private static ArrayList<AeriusResultPoint> filterResultPoints(final List<AeriusPoint> points) {
    final ArrayList<AeriusResultPoint> list = new ArrayList<>();

    for (final AeriusPoint point : points) {
      if (point instanceof AeriusResultPoint) {
        list.add((AeriusResultPoint) point);
      }
    }
    return list;
  }

  /**
   * Returns the year - substances key list for all substances.
   *
   * @param year
   * @return
   */
  private static List<EmissionValueKey> getKeys(final Integer year) {
    return EmissionValueKey.getEmissionValueKeys(year, Substance.EMISSION_SUBSTANCES);
  }

  private void setImportResultMetaData(final ImportResult result, final GMLReader reader) {
    final GMLMetaDataReader metaDataReader = reader.metaDataReader();
    result.setMetaData(metaDataReader.readMetaData());
    result.setImportedYear(metaDataReader.readYear());
    result.setImportedTemporaryPeriod(metaDataReader.readTemporaryProjectPeriodYear());
    result.setPermitCalculationRadiusType(toPermitCalculationRadiusType(metaDataReader.readPermitCalculationRadiusType()));
    result.setVersion(metaDataReader.readAeriusVersion());
    result.setDatabaseVersion(metaDataReader.readDatabaseVersion());
  }

  private PermitCalculationRadiusType toPermitCalculationRadiusType(final String type) {
    for (final PermitCalculationRadiusType pcrt : permitCalculationRadiusTypes) {
      if (pcrt.getCode().equals(type)) {
        return pcrt;
      }
    }
    return null;
  }

  /**
   * Validates all meta data elements. Adds any violation to the list of exceptions.
   *
   * @param metaData
   * @param exceptions
   */
  private static void validateMetaData(final ScenarioMetaData metaData, final List<AeriusException> exceptions, final boolean useValidMetaData) {
    if (!useValidMetaData) {
      return;
    }
    validateMetaDataElement(metaData.getProjectName(), "projectName", exceptions);
    validateMetaDataElement(metaData.getCorporation(), "corporation", exceptions);
    validateMetaDataElement(metaData.getReference(), "reference", exceptions);
    validateMetaDataElement(metaData.getDescription(), "description", exceptions);
    validateMetaDataElement(metaData.getStreetAddress(), "streetAddress", exceptions);
    validateMetaDataElement(metaData.getPostcode(), "postcode", exceptions);
    validateMetaDataElement(metaData.getCity(), "city", exceptions);
  }

  private static void validateMetaDataElement(final String metaDataElement, final String elementName, final List<AeriusException> exceptions) {
    if (StringUtils.isEmpty(metaDataElement)) {
      exceptions.add(new AeriusException(Reason.GML_METADATA_EMPTY, elementName));
    }
  }

  /**
   * Checks if all geometries are not self intersecting.
   *
   * @param arrayList Emission Source List to check
   * @throws AeriusException throws exception if a geometry intersects
   */
  private static void checkIntersections(final ArrayList<EmissionSourceList> sourceLists) throws AeriusException {
    for (final EmissionSourceList esl : sourceLists) {
      checkEmissionSourcesIntersections(esl);
    }
  }

  private static void checkEmissionSourcesIntersections(final ArrayList<EmissionSource> esl) throws AeriusException {
    for (final EmissionSource emissionSource : esl) {
      if (emissionSource instanceof SRM2NetworkEmissionSource) {
        checkEmissionSourcesIntersections(((SRM2NetworkEmissionSource) emissionSource).getEmissionSources());
      } else {
        checkIntersections(emissionSource);
        validateGeoZero(emissionSource);
      }
    }
  }

  /**
   * Checks if all geometries are not self intersecting.
   *
   * @param arrayList Emission Source List to check
   * @throws AeriusException throws exception if a geometry intersects
   */
  private static void checkIntersections(final EmissionSource emissionSource) throws AeriusException {
    final String wkt = emissionSource.getGeometry() == null ? null : emissionSource.getGeometry().getWKT();
    if (!GeometryUtil.validWKT(wkt)) {
      throw new AeriusException(Reason.GML_GEOMETRY_INVALID, emissionSource.getLabel());
    }
    if (GeometryUtil.hasIntersections(wkt)) {
      throw new AeriusException(Reason.GML_GEOMETRY_INTERSECTS, emissionSource.getLabel(), emissionSource.getGeometry().toString());
    }

  }

  /**
   * Checks if all geometries length or surface > 0
   *
   * @param arrayList Emission Source List to check
   * @throws AeriusException throws exception if a geometry intersects
   */
  private static void validateGeoZero(final EmissionSource emissionSource) throws AeriusException {
    final WKTGeometry geo = emissionSource.getGeometry();
    if (geo != null) {
      switch (geo.getType()) {
      case LINE:
        if (geo.getMeasure() <= 0) {
          throw new AeriusException(Reason.LIMIT_LINE_LENGTH_ZERO, emissionSource.getLabel(), String.valueOf(geo.getMeasure()));
        }
        break;
      case POLYGON:
        if (MathUtil.round(geo.getMeasure()) <= 0) {
          throw new AeriusException(Reason.LIMIT_POLYGON_SURFACE_ZERO, emissionSource.getLabel(), String.valueOf(geo.getMeasure()));
        }
        break;
      default:
        // no checks for other geometries.
      }
    }
  }

  private static ImportType checkFileType(final String filename) throws AeriusException {
    final ImportType type = ImportType.determineByFilename(filename);
    if (type == null) {
      throw new AeriusException(Reason.IMPORT_FILE_UNSUPPORTED);
    }
    return type;
  }

  private static void checkResultLimits(final ImportResult importResult, final CalculatorLimits limits) throws AeriusException {
    if (limits == null) {
      return;
    }

    for (final EmissionSourceList esl : importResult.getSourceLists()) {
      EmissionSourceCheckLimits.check(esl, limits);
    }
  }

  /**
   * Determine if we use strict validation
   *
   * @param importResult @ImportResult.
   */
  private static void validateStrict(final ImportResult importResult, final boolean validateStrict) {
    if (!validateStrict) {
      return;
    }

    // move warnings to exceptions list
    importResult.getExceptions().addAll(importResult.getWarnings());
    importResult.getWarnings().clear();
  }
}
