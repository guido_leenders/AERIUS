/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

import org.apache.commons.io.input.ReaderInputStream;

import com.github.rwitzel.streamflyer.core.ModifyingReader;
import com.github.rwitzel.streamflyer.regex.RegexModifier;

import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util to allow text base filtering on the @Inputstream is used to filter out content before data is converted to a XML.
 */
final class FilterImportUtil {

  private FilterImportUtil() {
    // Util class.
  }

  /**
   * filter out result block as text based on a regular expression.
   * 
   * @param inputStream stream to filter.
   * @param includeResults determine if we want to filer out.
   * @return
   * @throws AeriusException
   */
  static InputStream filterResults(final InputStream inputStream, final boolean includeResults) {
    if (includeResults) {
      return inputStream;
    } else {
      final RegexModifier myModifier = new RegexModifier("<imaer:featureMember>[\n].+<imaer:Receptor.*>((.|\n)*?)<\\/imaer:featureMember>",
          Pattern.CASE_INSENSITIVE, "");
      final Reader reader = new ModifyingReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8), myModifier);
      return new ReaderInputStream(reader, StandardCharsets.UTF_8);
    }
  }
}
