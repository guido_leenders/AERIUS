/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import java.io.Serializable;

/**
 * Object to store zip content.
 */
public class MeldingPayloadData implements Serializable  {

  private static final long serialVersionUID = -5348404841919034834L;

  public static final String MELDING_JSON = "MeldingInputData.json";

  public static final String PROPOSED_GML = "proposedGML.gml";

  public static final String CURRENT_GML = "currentGML.gml";

  public static final String ATTACHMENTS = "attachments.zip";

  private byte[] payload;

  public byte[] getPayload() {
    return payload;
  }

  public void setPayload(final byte[] payload) {
    this.payload = payload;
  }

}
