/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.http;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * Wrapper to get remote content and return it as String.
 */
public final class HttpClientProxy {

  private HttpClientProxy() {}

  public static byte[] getRemoteContent(final CloseableHttpClient client, final String url)
      throws URISyntaxException, IOException, ParseException, HttpException {
    final HttpGet getRequest = new HttpGet(url);
    try {
      try (final CloseableHttpResponse httpResponse = client.execute(getRequest)) {
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          return EntityUtils.toByteArray(httpResponse.getEntity());
        } else {
          throw new HttpException(httpResponse.getStatusLine().getStatusCode(), EntityUtils.toString(httpResponse.getEntity()));
        }
      }
    } finally {
      // Abort in the outer try so the inner try handles the normal try-with case, then abort. Resetting here really does nothing
      // because these are http requests that are immediate from the client's perspective, but it solves a sonar issue, so...
      getRequest.reset();
    }
  }

  public static String postJsonRemoteContent(final CloseableHttpClient client, final String url, final String postData)
      throws URISyntaxException, IOException, ParseException, HttpException {
    final HttpPost postRequest = new HttpPost(url);
    postRequest.addHeader(HTTP.CONTENT_TYPE, "application/json");
    postRequest.addHeader("Accept", "application/json");
    postRequest.setEntity(new StringEntity(postData, StandardCharsets.UTF_8));
    try {
      try (final CloseableHttpResponse httpResponse = client.execute(postRequest)) {
        final String content = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          return content;
        } else {
          throw new HttpException(httpResponse.getStatusLine().getStatusCode(), content);
        }
      }
    } finally {
      // Abort in the outer try so the inner try handles the normal try-with case, then abort. Resetting here really does nothing
      // because these are http requests that are immediate from the client's perspective, but it solves a sonar issue, so...
      postRequest.reset();
    }
  }
  
  public static String deleteRemoteContent(final CloseableHttpClient client, final String url)
      throws URISyntaxException, IOException, ParseException, HttpException {
    final HttpDelete deleteRequest = new HttpDelete(url);
    try {
      try (final CloseableHttpResponse httpResponse = client.execute(deleteRequest)) {
        final String content = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
          return content;
        } else {
          throw new HttpException(httpResponse.getStatusLine().getStatusCode(), content);
        }
      }
    } finally {
      // Abort in the outer try so the inner try handles the normal try-with case, then abort. Resetting here really does nothing
      // because these are http requests that are immediate from the client's perspective, but it solves a sonar issue, so...
      deleteRequest.reset();
    }
  }

}
