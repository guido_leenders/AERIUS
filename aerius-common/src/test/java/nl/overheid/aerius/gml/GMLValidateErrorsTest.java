/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class to check how the GML importer handles invalid input.
 */
public class GMLValidateErrorsTest extends BaseDBTest {

  private static final String LATEST_VALIDATE = "latest/validate/errors/";

  private static final double EPSILON = 0.0001;

  private Importer importer;

  @Override
  @Before
  public void setUp() throws SQLException, AeriusException {
    importer = new Importer(getCalcPMF());
  }

  @Test(expected = AeriusException.class)
  public void testGMLValidationFailed() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5201_projectiesysteem.gml", "GML Invalid projectiesystem", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertFalse("Should show list of possible options", e.getArgs()[0].isEmpty());
      assertTrue("Invalid content was found",
          e.getArgs()[0].contains("Invalid content was found starting with element 'gml:Pointy'"));
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLGeometryInvalid() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5202_projectiesysteem.gml", "GML Invalid geometry", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertFalse("Should show list of possible options", e.getArgs()[0].isEmpty());
      assertTrue("Invalid content was found",
          e.getArgs()[0].contains("Invalid content was found starting with element 'imaer:GM_SQUARE'"));
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLEncodingIncorrect() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5203_unsupported_character.gml", "GML Incorrect encoding", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertFalse("Should show list of possible options", e.getArgs()[0].isEmpty());
      assertTrue("Invalid content was found",
          e.getArgs()[0].contains("Invalid content was found starting with element 'imaer:GM_SURFACE'"));
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLGeometryIntersects() throws IOException, AeriusException, SQLException {
    assertResult("fout_5204_intersecting_geometry.gml", "GML Geometry intersects", Reason.GML_GEOMETRY_INTERSECTS);
  }

  @Test(expected = AeriusException.class)
  public void testGMLGeometryNotPermitted() throws IOException, AeriusException, SQLException {
    assertResult("fout_5205_unallowed_source_geometry.gml", "GML Geometry not permitted", Reason.SHIPPING_ROUTE_GEOMETRY_NOT_ALLOWED);
  }

  @Test(expected = AeriusException.class)
  public void testGMLGeometryUnknown() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5206_unsupported_geometry.gml", "GML Geometry unkown", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertFalse("Should show list of possible options", e.getArgs()[0].isEmpty());
      assertTrue("Invalid content was found",
          e.getArgs()[0].contains("The value of {abstract} in the element declaration for 'gml:AbstractRing' must be false"));
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnknownRav() throws IOException, AeriusException, SQLException {
    assertResult("fout_5207_unknown_rav.gml", "GML Unknown rav", Reason.GML_UNKNOWN_RAV_CODE);
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnknownMobileSource() throws IOException, AeriusException, SQLException {
    assertResult("fout_5208_unknown_mobile_source.gml", "GML Unknown mobile source", Reason.GML_UNKNOWN_MOBILE_SOURCE_CODE);
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnknownShipCode() throws IOException, AeriusException, SQLException {
    assertResult("fout_5209_unknown_ship.gml", "GML Unknown ship code", Reason.GML_UNKNOWN_SHIP_CODE);
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnknownPlancode() throws IOException, AeriusException, SQLException {
    assertResult("fout_5210_unknown_plan.gml", "GML Unknown plan code", Reason.GML_UNKNOWN_PLAN_CODE);
  }

  @Test(expected = AeriusException.class)
  public void testGMLGenericParseError() throws IOException, AeriusException, SQLException {
    assertResult("fout_5211_gml_content.gml", "GML Generic parse error", Reason.GML_GENERIC_PARSE_ERROR);
  }

  @Test(expected = AeriusException.class)
  public void testGMLParseError() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5212_gml_content.gml", "GML parse error", Reason.GML_PARSE_ERROR);
    } catch (final AeriusException e) {
      assertEquals("Row", "30", e.getArgs()[0]);
      assertEquals("Column", "27", e.getArgs()[1]);
      assertEquals("Tag in error", "gml:Pointy", e.getArgs()[2]);
      assertEquals("Tag expected", "</gml:Pointy>", e.getArgs()[3]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLVersionNotSupported() throws IOException, AeriusException, SQLException {
    assertResult("fout_5213_unsupported_version.gml", "GML Version not supported", Reason.GML_VERSION_NOT_SUPPORTED);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGMLGeometryInvalid_5215() throws IOException, AeriusException, SQLException {
    assertResult("fout_5215_invalid_geometry.gml", "GML Geometry invalid", Reason.GEOMETRY_INVALID);
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnknownPasMeasure() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5216_unknown_pas_measure.gml", "GML Unknown PAS measure", Reason.GML_UNKNOWN_PAS_MEASURE_CODE);
    } catch (final AeriusException e) {
      assertEquals("Id", "ES.1", e.getArgs()[0]);
      assertEquals("code", "PAS2013.09-01", e.getArgs()[1]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnsupportedLodingMeasure() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5217_unsupported_lodging_measure.gml", "GML Unsupported loding measure", Reason.GML_INVALID_PAS_MEASURE_CATEGORY);
    } catch (final AeriusException e) {
      assertEquals("Label", "ES.1", e.getArgs()[0]);
      assertEquals("Offending category code", "PAS2015.04-01", e.getArgs()[1]);
      assertEquals("Category", "A1.1", e.getArgs()[2]);
      throw e;
    }
  }

  @Test()
  public void testGMLOldObjectConversion() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5218_old_object_conversion.gml", "GML old object conversion", Reason.GML_INVALID_CATEGORY_MATCH);
    } catch (final AeriusException e) {
      assertEquals("Label", "ES.1", e.getArgs()[0]);
      assertEquals("Offending category code", "PAS2015.04-01", e.getArgs()[1]);
      assertEquals("Category", "A1.1", e.getArgs()[2]);
      throw e;
    }
  }

  @Test()
  public void testGMLInvalidRoadCategoryMatch() throws IOException, AeriusException, SQLException {
    assertResult("fout_5219_obsolete_roadsource.gml", "GML Invalid road category match", Reason.GML_INVALID_ROAD_CATEGORY_MATCH);
  }

  @Test(expected = AeriusException.class)
  public void testGMLIdNotUnique() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5220_conflicting_id.gml", "GML Id not unique", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertTrue("Contains error", e.getArgs()[0].contains("There are multiple occurrences of ID value 'ES.1'"));
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLUnknownRoadCategory() throws IOException, AeriusException, SQLException {
    assertResult("fout_5221_unknown_road.gml", "GML Unknown road category", Reason.GML_UNKNOWN_ROAD_CATEGORY);
  }

  @Test(expected = AeriusException.class)
  public void testGMLMetaDataEmpty() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5222_missing_metadata.gml", "GML Metadata empty", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertTrue("Contains error", e.getArgs()[0].contains("Invalid content was found starting with element 'imaer:version'"));
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLSourceNegativeVehicles() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5230_gml_source_negative_vehicles.gml", "GML vehicles less than zero", Reason.SRM2_SOURCE_NEGATIVE_VEHICLES);
    } catch (final AeriusException e) {
      assertEquals("Expected label to be filled", "Bron 1", e.getArgs()[0]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLRoadSegmentNotAFraction() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_5231_gml_road_segment_position_not_fraction.gml", "GML road segment not a fraction",
          Reason.GML_ROAD_SEGMENT_POSITION_NOT_FRACTION);
    } catch (final AeriusException e) {
      assertEquals("Expected road segment position to be 50", 50D, Double.parseDouble(e.getArgs()[0]), EPSILON);
      throw e;
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGMLUnknownError() throws IOException, AeriusException, SQLException {
    assertResult("fout_666_unknown_error.gml", "GML Unknown error", Reason.INTERNAL_ERROR);
  }

  @Test(expected = AeriusException.class)
  public void testGMLYear2100() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_year_over_2100.gml", "GML year greater than", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertEquals("Year invalid", "year must be less than 2100", e.getArgs()[0]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGMLYear1900() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_year_under_1900.gml", "GML year under than", Reason.GML_VALIDATION_FAILED);
    } catch (final AeriusException e) {
      assertEquals("Year invalid", "year must be greater than 1900", e.getArgs()[0]);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testError6210() throws IOException, AeriusException, SQLException {
    try {
      assertResult("fout_6210_srm2_road_not_in_network.gml", "road not in network", Reason.SRM2_ROAD_NOT_IN_NETWORK);
    } catch (final AeriusException e) {
      assertEquals("Road not in network", "NWR.1_1", e.getArgs()[0]);
      throw e;
    }
  }

  private void assertResult(final String fileName, final String expectedReasonTxt, final Reason expectedReason)
      throws IOException, SQLException, AeriusException {
    try {
      final ImportResult result = getImportResult(LATEST_VALIDATE, fileName);
      if (!result.getExceptions().isEmpty()) {
        throw result.getExceptions().get(0);
      }
    } catch (final AeriusException e) {
      assertSame(expectedReasonTxt, expectedReason, e.getReason());
      throw e;
    }
  }

  private File getFile(final String relativePath, final String fileName) throws FileNotFoundException {
    final URL url = getClass().getResource(relativePath + fileName);
    File file = null;
    if (url != null) {
      file = new File(url.getFile());
      if (!file.exists()) {
        file = null;
      }
    }
    return file;
  }

  private ImportResult getImportResult(final String relativePath, final String fileName)
      throws IOException, AeriusException, SQLException {
    ImportResult result = null;
    final File file = getFile(relativePath, fileName);
    if (file == null) {
      throw new FileNotFoundException(fileName);
    } else {
      try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
        result = importer.convertInputStream2ImportResult(fileName, inputStream);
      }
    }
    return result;
  }
}
