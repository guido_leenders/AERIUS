/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base.source.ship;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.TestPMF;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandWaterwayType;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.LocaleUtils;

public class InlandShippingUtilTest extends BaseDBTest {

  private TestPMF pmf;

  final private List<AeriusException> warnings = new ArrayList<>();
  private InlandShippingUtil inlandRouteUtil;
  final private String sourceId = "M0";
  final private InlandRouteEmissionSource ship = createNew();
  final private InlandWaterwayType inlandWaterwayType = null;

  @Before
  public void before() throws SQLException {
    pmf = getPMF(ProductType.CALCULATOR);
    inlandRouteUtil = new InlandShippingUtil(pmf, warnings);
  }

  @Test
  public void testSetOrForceWaterwayType() throws AeriusException, SQLException {
    ship.setGeometry(new WKTGeometry("LINESTRING(68340.22 443427.6,64308.22 445201.68)"));
    final SectorCategories sectors = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
    inlandRouteUtil.setOrForceWaterwayType(sectors, sourceId, ship, inlandWaterwayType);
    final InlandWaterwayCategory expectCategorie = sectors.getInlandShippingCategories().getWaterwayCategoryByCode(
        ship.getWaterwayCategory().getCode());
    Assert.assertFalse("Should have warnings ", warnings.isEmpty());
    Assert.assertTrue("Expect Reason.INLAND_SHIPPING_WATERWAY_INCONCLUSIVE",
        warnings.get(0).getReason() == Reason.INLAND_SHIPPING_WATERWAY_INCONCLUSIVE);
    Assert.assertEquals("getWaterwayCategory should be the same ", expectCategorie, ship.getWaterwayCategory());
  }

  /**
   * Test with Bogus route on the Veluwe where no feasable waterways are to be found to make sure
   * at least one waterway is always found.
   *
   * @throws AeriusException
   * @throws SQLException
   */
  @Test
  public void testSetOrForceWaterwayType_NeverNone() throws AeriusException, SQLException {
    ship.setGeometry(new WKTGeometry("LINESTRING(188735.74 458991.12,185510.14 465442.32)"));
    final SectorCategories sectors = SectorRepository.getSectorCategories(pmf, LocaleUtils.getDefaultLocale());
    inlandRouteUtil.setOrForceWaterwayType(sectors, sourceId, ship, inlandWaterwayType);
    Assert.assertNotNull("At least one WaterWayType should be found for any line", ship.getWaterwayCategory());
  }

  private InlandRouteEmissionSource createNew() {
    final InlandRouteEmissionSource sev = new InlandRouteEmissionSource();
    sev.getEmissionSubSources().add(createNewGroup(1));
    sev.getEmissionSubSources().add(createNewGroup(1));
    sev.getEmissionSubSources().add(createNewGroup(2));
    sev.getEmissionSubSources().add(createNewGroup(2));
    sev.getEmissionSubSources().add(createNewGroup(3));
    sev.getEmissionSubSources().add(createNewGroup(3));
    sev.getEmissionSubSources().add(createNewGroup(3));
    return sev;
  }

  private InlandShippingCategory createCategory(final int id) {
    final InlandShippingCategory c = new InlandShippingCategory();
    c.setName("ship name" + id);

    return c;
  }
  private RouteInlandVesselGroup createNewGroup(final int id) {
    final RouteInlandVesselGroup v = new RouteInlandVesselGroup();

    v.setId(id);
    v.setCategory(createCategory(id));
    v.setName("name" + id);
    v.setNumberOfShipsAtoB(id * 13 + 5, TimeUnit.YEAR);
    return v;
  }

}
