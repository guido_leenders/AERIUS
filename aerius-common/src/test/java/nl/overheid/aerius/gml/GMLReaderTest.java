/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;
import nl.overheid.aerius.gml.base.GMLVersionReaderFactory;
import nl.overheid.aerius.gml.base.MetaData;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.gml.v0_5.GMLReaderFactoryV05;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;

/**
 * Tests the basic cases for GML which will (most likely) hardly change.
 */
public class GMLReaderTest extends BaseDBTest {

  private static final Logger LOG = Logger.getLogger(GMLReaderTest.class);

  private static final String USED_EXTENSION = ".gml";
  private static final String SOURCES_ONLY_FILE = "test_sources_only";
  private static final String EMPTY_METADATA_FILE = "test_sources_empty_metadata";
  private static final String RECEPTORS_FILE = "test_receptors";
  private static final String MIXED_FEATURES_FILE = "test_mixed_features";
  private static final String OLD_CODES_FILE = "test_old_codes";
  private static final String UNROUNDED_CALCPOINT_FILE = "test_calcpoint_rounding";
  private static final String METADATA_FILE = "test_metadata";

  private static final String PATH_ALL = "src/test/resources/nl/overheid/aerius/gml/";
  private static final String PATH_CURRENT_VERSION = GMLWriter.LATEST_WRITER_VERSION.name().toLowerCase(Locale.ENGLISH) + "/";

  private static final int GML_YEAR = 2013;
  private static final String VERSION = "V1.1";
  private static final String DATABASE_VERSION = "SomeDBVersion";
  private static final String SITUATION_NAME = "Situatie 1";
  private static final Integer TEMPORARY_PERIOD = 3;

  private static TestDomain testDomain;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
  }

  @Test
  public void testConvertToEmissionSources() throws IOException, AeriusException, SQLException {
    final File file = getFile(PATH_CURRENT_VERSION, SOURCES_ONLY_FILE);
    final List<EmissionSource> sources = getEmissionSourcesFromFile(file);
    assertNotNull("Emission sources", sources);
    assertEquals("Number of sources", 2, sources.size());
  }

  @Test
  public void testConvertToEmissionSourcesWithoutMetadata() throws IOException, AeriusException, SQLException {
    final File file = getFile(PATH_CURRENT_VERSION, EMPTY_METADATA_FILE);
    final List<EmissionSource> sources = getEmissionSourcesFromFile(file);
    assertNotNull("Emission sources without meta data", sources);
    assertEquals("Number of sources", 2, sources.size());
  }

  @Test
  public void testConvertToAeriusPoints() throws IOException, AeriusException, SQLException {
    final File file = getFile(PATH_CURRENT_VERSION, RECEPTORS_FILE);
    final List<AeriusPoint> points = getAeriusPointsFromFile(file, true);
    assertNotNull("Receptor points", points);
    assertEquals("Number of receptor points", 3, points.size());
  }

  @Test
  public void testCalcPointToRoundedAeriusPoint() throws IOException, AeriusException, SQLException {
    final List<File> files = getFiles(PATH_ALL, UNROUNDED_CALCPOINT_FILE);
    for (final File file : files) {
      final List<AeriusPoint> points = getAeriusPointsFromFile(file, false);
      assertNotNull("Calculation points", points);
      assertEquals("Number of calculation points", 1, points.size());
      assertAllCoordinatesRounded(points);
    }
  }

  @Test
  public void testConvertFromMultipleFeatures() throws IOException, AeriusException, SQLException {
    final File file = getFile(PATH_CURRENT_VERSION, MIXED_FEATURES_FILE);
    final List<AeriusPoint> points = getAeriusPointsFromFile(file, true);
    assertNotNull("Receptor points", points);
    assertEquals("Number of receptor points", 3, points.size());

    final List<EmissionSource> sources = getEmissionSourcesFromFile(file);
    assertNotNull("Emission sources", sources);
    assertEquals("Number of sources", 2, sources.size());
  }

  @Test
  public void testConvertMetaData() throws AeriusException, IOException, SQLException {
    final File file = getFile(PATH_CURRENT_VERSION, MIXED_FEATURES_FILE);
    final GMLReader reader = getGMLReaderFromFile(file);
    final MetaData expectedMetaData = getMetaData();
    final GMLMetaDataReader metaDataReader = reader.metaDataReader();
    assertEquals("Metadata year", expectedMetaData.getYear().intValue(), metaDataReader.readYear());
    assertEquals("Metadata version", expectedMetaData.getVersion(), metaDataReader.readAeriusVersion());
    assertEquals("Metadata DB version", expectedMetaData.getDatabaseVersion(), metaDataReader.readDatabaseVersion());
    assertEquals("Metadata temporary period", expectedMetaData.getTemporaryPeriod(), metaDataReader.readTemporaryProjectPeriodYear());
    final ScenarioMetaData MetaData = metaDataReader.readMetaData();
    assertEquals("Metadata name", expectedMetaData.getProjectName(), MetaData.getProjectName());
    assertEquals("Metadata reference", expectedMetaData.getReference(), MetaData.getReference());
    assertEquals("Metadata corporation", expectedMetaData.getCorporation(), MetaData.getCorporation());
    assertEquals("Metadata description", expectedMetaData.getDescription(), MetaData.getDescription());
  }

  @Test
  public void testMetaData() throws IOException, AeriusException, SQLException {
    final File file = getFile(PATH_CURRENT_VERSION, METADATA_FILE);
    final GMLReader reader = getGMLReaderFromFile(file);
    final GMLMetaDataReader metaDataReader = reader.metaDataReader();
    assertNotNull("Should have meta data reader", metaDataReader);
  }

  @Test
  public void testConvertFromOldCodes() throws IOException, AeriusException, SQLException {
    final File file = getFile("v0_5/", OLD_CODES_FILE);
    final Map<GMLLegacyCodeType, Map<String, Conversion>> codeMaps = new HashMap<>();

    final Map<String, Conversion> offRoadMobileSourceMap = new HashMap<>();
    offRoadMobileSourceMap.put("101", new Conversion("BA-B-E3", true));
    codeMaps.put(GMLLegacyCodeType.ON_ROAD_MOBILE_SOURCE, offRoadMobileSourceMap);
    final GMLReaderFactoryV05 gmlReaderFactoryV05 = new GMLReaderFactoryV05(getCalcConnection()) {
      @Override
      protected Map<GMLLegacyCodeType, Map<String, Conversion>> getLegacyCodes(final Connection con, final AeriusGMLVersion version)
          throws SQLException {
        return codeMaps;
      }
    };
    final GMLReaderFactory factory = new GMLReaderFactory(getCalcPMF(), new GMLReaderProxy(getCalcConnection()) {
      @Override
      public GMLVersionReaderFactory determineReaderFactory(final NamespaceContext nameSpaceContext) throws AeriusException {
        return gmlReaderFactoryV05;
      }
    }, RECEPTOR_GRID_SETTINGS);
    final EmissionSourceList sources;
    final ArrayList<AeriusException> warnings = new ArrayList<>();
    try (InputStream inputStream = new FileInputStream(file)) {
      final GMLReader reader = factory.createReader(inputStream, testDomain.getCategories(), true, warnings, warnings);
      sources = reader.readEmissionSourceList(new EmissionValueKey(Substance.NOXNH3).hatch());
    } catch (final AeriusException e) {
      LOG.error("File in error during convertToCollection: " + file);
      throw e;
    }

    assertNotNull("Emission sources", sources);
    //4 conversions, but 2 shouldn't issue a warning.
    assertEquals("Expected warnings", 1, warnings.size());
    for (final EmissionSource source : sources) {
      if ("IncorrectSector".equals(source.getLabel())) {
        assertEquals("Sector ID", 1400, source.getSector().getSectorId());
      }
    }
  }

  private MetaData getMetaData() {
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setReference("SomeReference001");
    metaData.setCorporation("Big Corp");
    metaData.setProjectName("SomeProject");
    metaData.setDescription("SomeFunkyDescription");
    final MetaDataInput metaDataInput = new MetaDataInput();
    metaDataInput.setScenarioMetaData(metaData);
    metaDataInput.setYear(GML_YEAR);
    metaDataInput.setName(SITUATION_NAME);
    metaDataInput.setVersion(VERSION);
    metaDataInput.setDatabaseVersion(DATABASE_VERSION);
    metaDataInput.getOptions().setTemporaryProjectYears(TEMPORARY_PERIOD);
    metaDataInput.getOptions().setCalculationType(CalculationType.PAS);
    final GMLWriter writer = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    return writer.getWriter().metaData2GML(metaDataInput);
  }

  @Test
  public void testConvertAllVersions() throws IOException, AeriusException, SQLException {
    //Test mainly checks if a file can be imported, and some small checks on contents of the import.
    //To be flexible with adding new (complex) cases, this is quite minimal.
    List<File> files = getFiles(PATH_ALL, SOURCES_ONLY_FILE);
    for (final File file : files) {
      final List<EmissionSource> sources = getEmissionSourcesFromFile(file);
      assertNotNull("Emission sources for " + file, sources);
      assertEquals("Number of sources for " + file, 2, sources.size());
    }
    files = getFiles(PATH_ALL, EMPTY_METADATA_FILE);
    for (final File file : files) {
      final List<EmissionSource> sources = getEmissionSourcesFromFile(file);
      assertNotNull("Emission sources for " + file, sources);
      assertEquals("Number of sources for " + file, 2, sources.size());
    }
    files = getFiles(PATH_ALL, RECEPTORS_FILE);
    for (final File file : files) {
      final List<AeriusPoint> points = getAeriusPointsFromFile(file, true);
      assertNotNull("Receptor points", points);
      assertEquals("Number of receptor points", 3, points.size());

    }
    files = getFiles(PATH_ALL, MIXED_FEATURES_FILE);
    for (final File file : files) {
      final List<AeriusPoint> points = getAeriusPointsFromFile(file, true);
      assertNotNull("Receptor points", points);
      assertEquals("Number of receptor points", 3, points.size());
      assertFalse("Results included", ((AeriusResultPoint) points.get(0)).getEmissionResults().isEmpty());
      final List<EmissionSource> sources = getEmissionSourcesFromFile(file);
      assertNotNull("Emission sources for " + file, sources);
      assertEquals("Number of sources for " + file, 2, sources.size());
    }
  }

  private File getFile(final String relativePath, final String fileName) throws FileNotFoundException {
    final URL url = getClass().getResource(relativePath + fileName + USED_EXTENSION);
    if (url == null) {
      throw new FileNotFoundException(relativePath + fileName + USED_EXTENSION);
    }
    final File file = new File(url.getFile());
    if (!file.exists()) {
      throw new FileNotFoundException(file.toString());
    }
    return file;
  }

  /**
   * Finds all files with fileName in the directory, recursively.
   */
  private List<File> getFiles(final String directory, final String fileName) throws FileNotFoundException {
    final String actualFileName = fileName + USED_EXTENSION;
    return FileUtil.getFiles(directory, actualFileName);
  }

  private EmissionSourceList getEmissionSourcesFromFile(final File file) throws IOException, AeriusException, SQLException {
    final GMLReader reader = getGMLReaderFromFile(file);
    EmissionSourceList sources = null;
    final List<AeriusException> errors = new ArrayList<>();
    final List<AeriusException> warnings = new ArrayList<>();
    try (InputStream inputStream = new FileInputStream(file)) {
      sources = reader.readEmissionSourceList(new EmissionValueKey(Substance.NOXNH3).hatch());

    } catch (final AeriusException e) {
      LOG.error("File in error during convertToEmissionSources: " + file);
      throw e;
    }
    assertTrue("Expected no exceptions/warnings: " + warnings, warnings.isEmpty());
    return sources;
  }

  private List<AeriusPoint> getAeriusPointsFromFile(final File file, final boolean includeReceptors) throws IOException, AeriusException, SQLException {
    final GMLReader reader = getGMLReaderFromFile(file);
    return reader.getAeriusPoints(includeReceptors);
  }

  private GMLReader getGMLReaderFromFile(final File file) throws IOException, AeriusException, SQLException {
    final GMLReaderFactory factory = GMLReaderFactory.getFactory(getCalcPMF());
    try (InputStream inputStream = new FileInputStream(file)) {
      return factory.createReader(inputStream, testDomain.getCategories(), true, new ArrayList<AeriusException>(), new ArrayList<AeriusException>());
    } catch (final AeriusException e) {
      LOG.error("File in error during convertToCollection: " + file);
      throw e;
    }
  }

  /**
   * Asserts whether both coordinates of all points have correctly been rounded.
   *
   *
   * @param calculationPoints
   */
  private void assertAllCoordinatesRounded(final List<AeriusPoint> calculationPoints) {
    for (final AeriusPoint point : calculationPoints) {
      final double theX = point.getX();
      assertEquals("The x coordinate should have been rounded before", Math.round(theX), theX, 0d);
      final double theY = point.getY();
      assertEquals("The y coordinate should have been rounded before", Math.round(theY), theY, 0d);
    }
  }
}
