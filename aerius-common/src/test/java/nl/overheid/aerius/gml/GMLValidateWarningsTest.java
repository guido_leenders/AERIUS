/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class to check how the GML importer handles invalid input report these as warnings.
 */
@RunWith(Parameterized.class)
public class GMLValidateWarningsTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(GMLValidateWarningsTest.class);

  private enum TestFile {
    WARNING_OLD_GML_VERSION;

    private final List<AeriusGMLVersion> warningsIn;

    private TestFile(final AeriusGMLVersion... warningsIn) {
      this.warningsIn = Arrays.asList(warningsIn);
    }

    public String getFileName() {
      return name().toLowerCase() + ".gml";
    }

    public boolean expectWarning(final AeriusGMLVersion version) {
      return warningsIn.contains(version);
    }

  }

  private final AeriusGMLVersion version;

  private final TestFile testFile;

  /**
   * Initialize test with version and file to test.
   */
  public GMLValidateWarningsTest(final AeriusGMLVersion version, final TestFile file) {
    this.version = version;
    this.testFile = file;
  }

  @Parameters(name = "{0}: {1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> files = new ArrayList<>();
    for (final AeriusGMLVersion version : AeriusGMLVersion.values()) {
      for (final TestFile file : TestFile.values()) {
        final Object[] f = new Object[2];
        f[0] = version;
        f[1] = file;
        files.add(f);
      }
    }
    return files;
  }

  @Test
  public void testVersionGML() throws SQLException, IOException, AeriusException {
    final ImportResult oldResult = getImportResult(version.name().toLowerCase(), testFile);
    if (testFile.expectWarning(version)) {
      assertFalse("Expected warnings, but got none", oldResult.getWarnings().isEmpty());
    } else {
      // warnings test on allowed
      for (final AeriusException warning : oldResult.getWarnings()) {
        assertSame("Not expected warning, got " + warning.getReason() + " " + warning.getMessage(),
            warning.getReason(), Reason.GML_VERSION_NOT_LATEST);
      }
    }
  }

  private File getFile(final String relativePath, final String fileName) throws FileNotFoundException {
    final URL url = getClass().getResource(relativePath + '/' + fileName);
    File file = null;
    if (url != null) {
      file = new File(url.getFile());
      if (!file.exists()) {
        file = null;
      }
    }
    return file;
  }

  private ImportResult getImportResult(final String relativePath, final TestFile testFile)
      throws IOException, AeriusException, SQLException {
    ImportResult result = null;
    final File file = getFile(relativePath, testFile.getFileName());
    if (file == null) {
      LOG.info("Could not find file for test. relativePath: {}, filename: {}", relativePath, testFile);
    } else {
      final Importer importer = new Importer(getCalcPMF());
      try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
        result = importer.convertInputStream2ImportResult(testFile.getFileName(), inputStream);
      }
    }
    return result;
  }

}
