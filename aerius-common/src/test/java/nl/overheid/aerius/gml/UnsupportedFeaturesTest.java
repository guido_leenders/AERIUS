/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.FileUtil;

/**
 * Verifies that validation exceptions are thrown when unsupported features are supplied in an gml file. All gml files
 * in src/test/resources/nl/overheid/aerius/gml/latest/validate/unsupportedFeatures are tested.
 */
@RunWith(Parameterized.class)
public class UnsupportedFeaturesTest extends BaseDBTest {
  private static final String RESOURCE_PATH = "latest/validate/unsupportedFeatures/";

  private static final FilenameFilter GML_FILENAME_FILTER = new FilenameFilter() {
    @Override
    public boolean accept(final File dir, final String name) {
      return (name != null) && (ImportType.determineByFilename(name) == ImportType.GML);
    }
  };

  private final File file;
  private final Importer importer;

  public UnsupportedFeaturesTest(final String fileName, final File file) throws SQLException, AeriusException {
    this.file = file;
    importer = new Importer(getCalcPMF());
  }

  private static List<File> getGMLFiles() throws FileNotFoundException {
    final File basePath = new File(UnsupportedFeaturesTest.class.getResource(".").getPath(), RESOURCE_PATH);

    return FileUtil.getFilesWithExtension(basePath, GML_FILENAME_FILTER);
  }

  @Parameters(name = "{0}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> parameters = new ArrayList<>();

    for (final File file : getGMLFiles()) {
      parameters.add(new Object[] {file.getName(), file});
    }

    return parameters;
  }

  /**
   * Asserts that either an AeriusException is thrown or that the import result contains at least one AeriusException
   * and that the exception has {@link Reason#GML_VALIDATION_FAILED} as reason.
   *
   * @throws FileNotFoundException
   *           Thrown if the expected resource path cannot be found.
   * @throws IOException
   *           Thrown if an error occured while reasing the test file.
   */
  @Test
  public void testValidationFailure() throws FileNotFoundException, IOException {
    try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
      final ImportResult result = importer.convertInputStream2ImportResult(file.getName(), inputStream);

      assertNotNull("Import result cannot be null.", result);
      assertFalse("No validation errors found.", result.getExceptions().isEmpty());

      final AeriusException exception = result.getExceptions().get(0);
      assertNotNull("Exception cannot be null.", exception);
      assertEquals("Invalid reason.", Reason.GML_VALIDATION_FAILED, exception.getReason());
    } catch (final AeriusException e) {
      assertEquals("Invalid reason.", Reason.GML_VALIDATION_FAILED, e.getReason());
    }
  }
}
