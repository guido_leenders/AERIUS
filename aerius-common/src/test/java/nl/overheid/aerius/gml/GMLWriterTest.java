/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link GMLWriter}.
 */
public class GMLWriterTest extends BaseDBTest {

  private static final String SOURCES_ONLY_FILE = "test_sources_only";
  private static final String RECEPTORS_DEPOSITION_ONLY_FILE = "test_receptors_deposition_only";
  private static final String RECEPTORS_CONCENTRATION_ONLY_FILE = "test_receptors_concentration_only";
  private static final String RECEPTORS_ALL_FILE = "test_receptors";
  private static final String MIXED_FEATURES_FILE = "test_mixed_features";
  private static final String PATH_CURRENT_VERSION = GMLWriter.LATEST_WRITER_VERSION.name().toLowerCase() + "/";

  private static final int XCOORD_1 = TestDomain.XCOORD_1;
  private static final int YCOORD_1 = TestDomain.YCOORD_1;
  private static final int XCOORD_2 = TestDomain.XCOORD_2;
  private static final int YCOORD_2 = TestDomain.YCOORD_2;

  private static final int GML_YEAR = 2013;
  private static final String VERSION = "V1.1";
  private static final String DATABASE_VERSION = "SomeDBVersion";
  private static final String SITUATION_NAME = "Situatie 1";

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
  }

  @Test
  public void testConvertSources() throws IOException, AeriusException {
    final GMLWriter builder = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    final ArrayList<EmissionSource> sources = getExampleEmissionSources();
    final String result = getConversionResult(builder, sources);
    assertFalse("Result shouldn't be empty", result.isEmpty());
    AssertGML.assertEqualsGML(AssertGML.getFileContent(PATH_CURRENT_VERSION, SOURCES_ONLY_FILE), result);
  }

  private String getConversionResult(final GMLWriter builder, final ArrayList<EmissionSource> sources) throws IOException, AeriusException {
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
      builder.writeEmissionSources(bos, sources, getMetaDataInput(getScenarioMetaData()));
      return bos.toString(StandardCharsets.UTF_8.name());
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConvertInvalidSources() throws IOException, AeriusException {
    final GMLWriter converter = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    ArrayList<EmissionSource> sources = getExampleEmissionSources();
    sources.get(0).setGeometry(null);
    try {
      getConversionResult(converter, sources);
      fail("Emissionsource not allowed to have no geometry.");
    } catch (final IllegalArgumentException e) {
      sources = getExampleEmissionSources();
      ((GenericEmissionSource) sources.get(0)).setEmissionValues(new EmissionValues());
      //source has no emission, it'll just be exported with 0.0 emissions for all substances.
      //this used to be an error situation, now it's treated as a work-in-progress export.
      getConversionResult(converter, sources);
      throw e;
    }
  }

  @Test
  public void testConvertMetaData() throws IOException, AeriusException {
    final GMLWriter writer = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    final ScenarioMetaData metaData = getScenarioMetaData();
    final String originalReference = metaData.getReference();
    final EmissionSourceList sourceList = new EmissionSourceList();
    sourceList.setName(SITUATION_NAME);
    String result;
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
      final MetaDataInput metaDataInput = getMetaDataInput(metaData);
      metaDataInput.getOptions().setTemporaryProjectYears(null);
      metaDataInput.getOptions().setPermitCalculationRadiusType(null);
      writer.write(bos, sourceList, new ArrayList<AeriusPoint>(), metaDataInput);
      result = bos.toString(StandardCharsets.UTF_8.name());
    }
    validateDefaultMetaDataFields(result, metaData);
    assertFalse("Shouldn't contain temporaryPeriod", result.contains("<imaer:temporaryPeriod>"));
    assertFalse("Shouldn't contain PermitCalculationRadiusType", result.contains("<imaer:permitCalculationRadiusType>"));
    assertNotEquals("Reference should be overwritten", originalReference, metaData.getReference());
    assertTrue("Should contain new generated reference", result.contains(getExpectedElement("reference", metaData.getReference())));

    writer.setOverrideReference(false);
    final Integer temporaryPeriod = 6;
    final PermitCalculationRadiusType permitCalculationRadiusType = new PermitCalculationRadiusType();
    permitCalculationRadiusType.setCode("PRIORITY_PROJECT_MAIN_ROADS");
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
      final MetaDataInput metaDataInput = getMetaDataInput(metaData);
      metaDataInput.getOptions().setTemporaryProjectYears(temporaryPeriod);
      metaDataInput.getOptions().setPermitCalculationRadiusType(permitCalculationRadiusType);
      writer.write(bos, sourceList, new ArrayList<AeriusPoint>(), metaDataInput);
      result = bos.toString(StandardCharsets.UTF_8.name());
    }
    validateDefaultMetaDataFields(result, metaData);
    assertTrue("Should contain supplied reference", result.contains(getExpectedElement("reference", metaData.getReference())));
    assertFalse("Should still not contain temporaryPeriod", result.contains(getExpectedElement("temporaryPeriod")));
    assertFalse("Should still not contain permitCalculationRadiusType",
        result.contains(getExpectedElement("permitCalculationRadiusType")));
  }

  private void validateDefaultMetaDataFields(final String result, final ScenarioMetaData metaData) {
    assertTrue("Should contain version", result.contains(getExpectedElement("aeriusVersion", VERSION)));
    assertTrue("Should contain databaseVersion", result.contains(getExpectedElement("databaseVersion", DATABASE_VERSION)));
    assertTrue("Should contain year", result.contains(getExpectedElement("year", String.valueOf(GML_YEAR))));
    assertTrue("Should contain situationName", result.contains(getExpectedElement("name", SITUATION_NAME)));
    assertTrue("Should contain corporation", result.contains(getExpectedElement("corporation", metaData.getCorporation())));
    assertTrue("Should contain projectName", result.contains(getExpectedElement("name", metaData.getProjectName())));
    assertTrue("Should contain description", result.contains(getExpectedElement("description", metaData.getDescription())));
    assertTrue("Should contain streetAddress", result.contains(getExpectedElement("streetAddress", metaData.getStreetAddress())));
    assertTrue("Should contain postcode", result.contains(getExpectedElement("postcode", metaData.getPostcode())));
    assertTrue("Should contain city", result.contains(getExpectedElement("city", metaData.getCity())));
    assertTrue("Should contain reference tag", result.contains("<imaer:reference>"));
  }

  private String getExpectedElement(final String element, final String value) {
    return "<imaer:" + element + ">" + value + "</imaer:" + element + ">";
  }

  private String getExpectedElement(final String element) {
    return "<imaer:" + element + ">";
  }

  private MetaDataInput getMetaDataInput(final ScenarioMetaData scenarioMetaData) {
    final MetaDataInput metaDataInput = new MetaDataInput();
    metaDataInput.setScenarioMetaData(scenarioMetaData);
    metaDataInput.setYear(GML_YEAR);
    metaDataInput.setName(SITUATION_NAME);
    metaDataInput.setVersion(VERSION);
    metaDataInput.setDatabaseVersion(DATABASE_VERSION);
    metaDataInput.setOptions(getCalculationOptions());
    return metaDataInput;
  }

  private CalculationSetOptions getCalculationOptions() {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setTemporaryProjectYears(2);
    options.setCalculationType(CalculationType.NATURE_AREA);
    options.setCalculateMaximumRange(3);
    final ArrayList<EmissionValueKey> keys = EmissionValueKey.getEmissionValueKeys(0, new Substance[] {Substance.NOXNH3});
    for (final EmissionValueKey key : keys) {
      options.getSubstances().add(key.getSubstance());
    }
    for (final Substance substance : options.getSubstances()) {
      options.getEmissionResultKeys().add(EmissionResultKey.safeValueOf(substance, EmissionResultType.CONCENTRATION));
      options.getEmissionResultKeys().add(EmissionResultKey.safeValueOf(substance, EmissionResultType.DEPOSITION));
    }
    return options;
  }

  private EmissionSourceList getExampleEmissionSources() {
    final EmissionSourceList sources = new EmissionSourceList();

    final WKTGeometry geometrySource1 = new WKTGeometry("POINT(" + XCOORD_1 + " " + YCOORD_1 + ")", 1);
    final GenericEmissionSource emissionSource = new GenericEmissionSource();
    emissionSource.setEmission(Substance.NH3, 40020.300);
    emissionSource.setEmission(Substance.NOX, 10098.3242);
    final GenericEmissionSource source1 = TestDomain.getSource(1, geometrySource1, "SomeSource", emissionSource);
    source1.setSourceCharacteristics(TestDomain.getNonDefaultCharacteristics());
    sources.add(source1);

    final WKTGeometry geometrySource2 = new WKTGeometry("POINT(" + XCOORD_2 + " " + YCOORD_2 + ")", 1);
    final GenericEmissionSource emissionSource2 = new GenericEmissionSource();
    emissionSource2.setEmission(Substance.NH3, 7123.11);
    final GenericEmissionSource source2 = TestDomain.getSource(2, geometrySource2, "ëa?e:(é", emissionSource2);
    sources.add(source2);

    return sources;
  }

  @Test
  public void testConvertReceptorsDepositionOnly() throws IOException, AeriusException {
    assertConvertReceptors(true, false, RECEPTORS_DEPOSITION_ONLY_FILE);
  }

  @Test
  public void testConvertReceptorsConcentrationOnly() throws IOException, AeriusException {
    assertConvertReceptors(false, true, RECEPTORS_CONCENTRATION_ONLY_FILE);
  }

  @Test
  public void testConvertReceptorsAll() throws IOException, AeriusException {
    assertConvertReceptors(true, true, RECEPTORS_ALL_FILE);
  }

  public void assertConvertReceptors(final boolean includeDeposition, final boolean includeConcentration, final String receptorFile)
      throws IOException, AeriusException {
    final ArrayList<AeriusPoint> receptors = getExampleAeriusPoints(includeDeposition, includeConcentration);
    final MetaDataInput metaDataInput = getMetaDataInput(new ScenarioMetaData());
    metaDataInput.setName(null);
    metaDataInput.getOptions().setTemporaryProjectYears(null);
    metaDataInput.setResultsIncluded(true);
    final GMLWriter writer = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    String result;
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
      writer.writeAeriusPoints(bos, receptors, metaDataInput);
      result = bos.toString(StandardCharsets.UTF_8.name());
    }
    assertFalse("Result shouldn't be empty for " + receptorFile, result.isEmpty());
    AssertGML.assertEqualsGML(AssertGML.getFileContent(PATH_CURRENT_VERSION, receptorFile), result);
  }

  private ArrayList<AeriusPoint> getExampleAeriusPoints(final boolean includeDeposition, final boolean includeConcentration) {
    final ArrayList<AeriusPoint> receptors = new ArrayList<>();

    final int xCoord1 = XCOORD_1 + 1000;
    final int yCoord1 = YCOORD_1 + 1000;
    final AeriusResultPoint receptor1 = new AeriusResultPoint(xCoord1, yCoord1);
    receptor1.setId(1);
    if (includeDeposition) {
      receptor1.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 8546.77);
      receptor1.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 968.3);
    }
    if (includeConcentration) {
      receptor1.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, 95.8);
      receptor1.setEmissionResult(EmissionResultKey.NOX_CONCENTRATION, 3.001);
    }
    receptors.add(receptor1);

    final int xCoord2 = XCOORD_2 - 1000;
    final int yCoord2 = YCOORD_2 + 1000;
    final AeriusResultPoint receptor2 = new AeriusResultPoint(xCoord2, yCoord2);
    receptor2.setId(2);
    receptor2.setPointType(AeriusPointType.POINT);
    receptor2.setLabel("DB-team 1e depositie");
    if (includeConcentration && includeDeposition) {
      receptor2.setEmissionResult(EmissionResultKey.NOX_CONCENTRATION, 98.4);
      receptor2.setEmissionResult(EmissionResultKey.NO2_CONCENTRATION, 12595.2);
      receptor2.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 49.2);
      receptor2.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, 1574.4);
      receptor2.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 787.2);
      receptor2.setEmissionResult(EmissionResultKey.PM10_CONCENTRATION, 50380.8);
      receptor2.setEmissionResult(EmissionResultKey.PM25_CONCENTRATION, 201523.2);
    } else if (includeDeposition) {
      receptor2.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, 110.66);
      receptor2.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, 89.11);
    } else if (includeConcentration) {
      receptor2.setEmissionResult(EmissionResultKey.NH3_CONCENTRATION, 80.0);
      receptor2.setEmissionResult(EmissionResultKey.NOX_CONCENTRATION, 0.001);
    }

    receptors.add(receptor2);

    final int xCoord3 = xCoord2 - 10;
    final int yCoord3 = yCoord2 - 60;
    final AeriusResultPoint receptor3 = new AeriusResultPoint(xCoord3, yCoord3);
    receptor3.setId(3);
    receptor3.setPointType(AeriusPointType.POINT);
    receptor3.setLabel("DB-team 2e depositie");
    receptors.add(receptor3);

    return receptors;
  }

  @Test
  public void testConvertMixedFeatures() throws IOException, AeriusException {
    final GMLWriter builder = new GMLWriter(RECEPTOR_GRID_SETTINGS);
    builder.setOverrideReference(false);
    final EmissionSourceList emissionSources = getExampleEmissionSources();
    final ArrayList<AeriusPoint> receptors = getExampleAeriusPoints(true, false);
    String result;
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
      emissionSources.setName(SITUATION_NAME);
      final MetaDataInput metaDataInput = getMetaDataInput(getScenarioMetaData());
      metaDataInput.getOptions().setTemporaryProjectYears(3);
      metaDataInput.setResultsIncluded(true);
      metaDataInput.setResearchArea(true);
      builder.write(bos, emissionSources, receptors, metaDataInput);
      result = bos.toString(StandardCharsets.UTF_8.name());
    }
    assertFalse("Result shouldn't be empty", result.isEmpty());
    AssertGML.assertEqualsGML(AssertGML.getFileContent(PATH_CURRENT_VERSION, MIXED_FEATURES_FILE), result);
  }

  private ScenarioMetaData getScenarioMetaData() {
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setReference("SomeReference001");
    metaData.setCorporation("Big Corp");
    metaData.setProjectName("SomeProject");
    metaData.setDescription("SomeFunkyDescription");
    metaData.setStreetAddress("SomeStreet");
    metaData.setCity("SomeCity");
    metaData.setPostcode("somePostalCode");
    return metaData;
  }

}
