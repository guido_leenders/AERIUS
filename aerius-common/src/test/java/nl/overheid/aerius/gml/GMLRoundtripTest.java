/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.gml.base.AeriusGMLVersion;
import nl.overheid.aerius.gml.base.MetaDataInput;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class that reads an gml, converts to the data structures and then converts
 * it back to a gml file. It should not throw an exception. Expected warnings - if any - must be specified.
 *
 * <p>There are 2 ways to work around older version compatibility test problems here:
 *
 * <p>a) Make a copy of the expected file as found in the latest version and modify
 * the content to match the output as expected given the older version and give the
 * filename: [test]_v[version].gml
 * <p>b) Add the test to {@link #SKIPPED_TESTS}. This should preferably only be done
 * for tests that contain features not available in older versions.
 */
@RunWith(Parameterized.class)
public class GMLRoundtripTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(GMLRoundtripTest.class);

  private static final Object[][] FILES = {
    { "farm", },
    { "farm_with_systems", },
    { "farm_with_systems_and_fodder_measures", },
    { "industry", },
    { "inlandshipping", },
    { "inlandshipping_with_waterway", },
    { "maritimeship", },
    { "mooringinland", },
    { "mooringmaritime", },
    { "offroad", },
    { "offroad_with_specifications", },
    { "plan", },
    { "road", },
    { "road_non_urban", },
    { "road_dynamic_segmentation", },
    { "road_empty", EnumSet.of(Reason.SRM2_SOURCE_NO_VEHICLES)},
    { "metadata", },
    { "two_networks", },
    { "industry_with_calculated_heat_content", },
    { "industry_with_building", },
  };

  /**
   * This is a list of file/version combinations that should be skipped, because the older version doesn't support newer version features
   * and thus there is no older version file to test.
   */
  private static final Object[][] SKIPPED_TESTS = {
    {AeriusGMLVersion.V0_5, "farm_with_systems_and_fodder_measures" },
    {AeriusGMLVersion.V0_5, "offroad_with_specifications" },
    {AeriusGMLVersion.V0_5, "metadata" },
    {AeriusGMLVersion.V0_5, "inlandshipping_with_waterway" }, // not supported in v0.5
    {AeriusGMLVersion.V0_5, "two_networks" },
    {AeriusGMLVersion.V0_5, "industry_with_calculated_heat_content" },
    {AeriusGMLVersion.V0_5, "industry_with_building" },
    {AeriusGMLVersion.V1_0, "offroad_with_specifications" },
    {AeriusGMLVersion.V1_0, "metadata" },
    {AeriusGMLVersion.V1_0, "two_networks" },
    {AeriusGMLVersion.V1_0, "industry_with_calculated_heat_content" },
    {AeriusGMLVersion.V1_0, "industry_with_building" },
    {AeriusGMLVersion.V1_1, "industry_with_calculated_heat_content" },
    {AeriusGMLVersion.V1_1, "industry_with_building" },
    {AeriusGMLVersion.V2_0, "industry_with_calculated_heat_content" },
    {AeriusGMLVersion.V2_0, "industry_with_building" },
    {AeriusGMLVersion.V2_1, "industry_with_calculated_heat_content" },
    {AeriusGMLVersion.V2_1, "industry_with_building" },
  };

  private static final String LATEST_VERSION = "latest";
  private static final String TEST_FOLDER = "/roundtrip/";
  private static final AeriusGMLVersion CURRENT_GML_VERSION = GMLWriter.LATEST_WRITER_VERSION;
  private static final String CURRENT_VERSION = CURRENT_GML_VERSION.name().toLowerCase();

  private final String file;
  private final AeriusGMLVersion version;
  private final EnumSet<Reason> expectedWarnings;

  /**
   * Initialize test with version and file to test.
   */
  public GMLRoundtripTest(final AeriusGMLVersion version, final String file, final EnumSet<Reason> warnings) {
    this.version = version;
    this.file = file;
    this.expectedWarnings = warnings;
  }

  @Parameters(name = "{0}: {1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> files = new ArrayList<>();
    for (final AeriusGMLVersion version : AeriusGMLVersion.values()) {
      for (final Object[] object : FILES) {
        if (!skipThisTest(version, (String) object[0])) {
          final Object[] f = new Object[3];
          f[0] = version;
          f[1] = object[0];
          f[2] = getWarningReasons(object, version);
          files.add(f);
        } else {
          LOG.debug("Skipping test '{}' for '{}'", object[0], version);
        }
      }
    }
    return files;
  }

  private static boolean skipThisTest(final AeriusGMLVersion version, final String file) {
    boolean skipThisTest = false;
    for (final Object[] st : SKIPPED_TESTS) {
      if (st[0] == version && st[1].equals(file)) {
        skipThisTest = true;
      };
    }
    return skipThisTest;
  }

  private static EnumSet<Reason> getWarningReasons(final Object[] object, final AeriusGMLVersion version) {
    final EnumSet<Reason> warnings = object.length == 2
        ? EnumSet.copyOf(((EnumSet<Reason>) object[1])) : EnumSet.noneOf(Reason.class);
    if (version != CURRENT_GML_VERSION) {
      warnings.add(Reason.GML_VERSION_NOT_LATEST);
    }
    return warnings;
  }

  @Test
  public void testRoundTripGML() {
    final String versionString = version.name().toLowerCase();
    final String fileVersion = ": " + file + ':' + versionString;
    final ImportResult result = importAndCompare(versionString, fileVersion);

    assertTrue("No Exceptions: " + ArrayUtils.toString(result.getExceptions()) + fileVersion, result.getExceptions().isEmpty());
    assertExpectedWarnings(result.getWarnings());
    assertUnExpectedWarnings(result.getWarnings());
  }

  private ImportResult importAndCompare(final String versionString, final String fileVersion) {
    try {
      final ImportResult result = getImportResult(versionString, TEST_FOLDER);
      final List<AeriusPoint> customCalculationPoints = new ArrayList<>(result.getCalculationPoints());
      final GMLWriter gmlc = new GMLWriter(RECEPTOR_GRID_SETTINGS);
      final String gml;
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
        gmlc.write(bos, result.getSources(0), customCalculationPoints, getMetaData(result));
        gml = bos.toString(StandardCharsets.UTF_8.name());
        assertFalse("Generated GML is empty", gml.isEmpty());
      }
      assertGMLs(gml, versionString);
      return result;
    } catch (final Exception e) {
      throw new AssertionError(fileVersion, e);
    }
  }

  /**
   * Expected warnings check.
   */
  private void assertExpectedWarnings(final List<AeriusException> warnings) {
    for (final Reason warning: expectedWarnings) {
      boolean contains = false;
      for (final AeriusException ae: warnings) {
        if (ae.getReason() == warning) {
          contains = true;
        }
      }
      assertTrue("Expected warning " + warning + " not found", contains);
    }
  }

  /**
   * Test unexpected warnings.
   */
  public void assertUnExpectedWarnings(final List<AeriusException> warnings) {
    for (final AeriusException warning : warnings) {
      if (!expectedWarnings.contains(warning.getReason())) {
        fail("Not expected warning, got " + warning.getReason() + " " + warning.getMessage());
      }
    }
  }

  private MetaDataInput getMetaData(final ImportResult result) {
    final MetaDataInput input = new MetaDataInput();
    input.setScenarioMetaData(result.getMetaData());
    input.setYear(result.getImportedYear());
    input.setVersion("DEV");
    input.setDatabaseVersion(result.getDatabaseVersion());
    input.getOptions().setTemporaryProjectYears(result.getImportedTemporaryPeriod());
    return input;
  }

  private void assertGMLs(final String generatedGML, final String version) throws IOException {
    final String actual = replaceIncomparables(generatedGML);
    assertFalse("Result shouldn't be empty", actual.isEmpty());
    final String expected = replaceIncomparables(getFileContent(version));
    AssertGML.assertEqualsGML(expected, actual);
  }

  private String getFileContent(final String version) throws IOException {
    final String relativePathCompatibleOld = LATEST_VERSION + TEST_FOLDER;
    final String fileNameCompatibleOld = file + '_' + version;
    final String relativePathCurrent = CURRENT_VERSION + TEST_FOLDER;
    final String fileNameCurrent = file;

    final String compatibleOld = AssertGML.getFileContent(relativePathCompatibleOld, fileNameCompatibleOld);
    if (compatibleOld.isEmpty()) {
      LOG.debug("Getting current file content with path '{}' and filename '{}'. Not an old compatible version found.",
          relativePathCurrent, fileNameCurrent);
    } else {
      LOG.debug("Getting current file content with path '{}' and filename '{}'. Old compatible version found.",
          relativePathCompatibleOld, fileNameCompatibleOld);
    }
    return compatibleOld.isEmpty() ? AssertGML.getFileContent(relativePathCurrent, fileNameCurrent) : compatibleOld;
  }

  private ImportResult getImportResult(final String versionString, final String testFolder)
      throws IOException, AeriusException, SQLException {
    final String relativePath = versionString + testFolder;
    final AtomicReference<String> readVersion = new AtomicReference<>();
    final Importer importer = new Importer(getCalcPMF()) {
      @Override
      protected GMLReader createGMLReader(final InputStream inputStream, final ImportResult result) throws AeriusException {
        final GMLReader reader = super.createGMLReader(inputStream, result);
        readVersion.set(reader.getVersion().name().toLowerCase());
        return reader;
      }
    };
    final ImportResult result;
    final String gmlFileName = file + ".gml";
    final String fullPath = relativePath + gmlFileName;
    LOG.debug("Importing file '{}", fullPath);
    try (final InputStream inputStream = getFileInputStream(fullPath)) {
      result = importer.convertInputStream2ImportResult(gmlFileName, inputStream);
    }
    assertEquals("GML imported is not of expected version", versionString, readVersion.get());
    return result;
  }

  private String replaceIncomparables(final String string) {
    return AssertGML.USE_ORIGINAL_GML ? string
        : string.replaceAll("<imaer:version>[^<]+</imaer:version>", "").replaceAll("\\s*<imaer:reference>[^<]+</imaer:reference>", "");
  }
}
