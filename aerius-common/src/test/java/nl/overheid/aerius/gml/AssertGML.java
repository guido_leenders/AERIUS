/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Convenience class for tests in this package.
 */
public final class AssertGML {

  /**
   * Due to linebreak interpretation differences between linux/windows and a randomness in attribute order for the collection,
   * tests can fail on either.
   * Following property should fix that. Set it to false to get the unittests to work on both,
   * set it to true to obtain prettier GML, which in turn can be used to update test files.
   */
  static final boolean USE_ORIGINAL_GML = false;

  private static final String USED_EXTENSION = ".gml";

  private static final String REGEX_FEATURE_COLLECTION_ELEMENT = "<imaer:FeatureCollectionCalculator[^>]*>";

  private AssertGML() {
    //util convenience class.
  }

  static void assertEqualsGML(final String expected, final String actual) {
    assertEquals("GML contented compared not equal", getCleanGML(expected), getCleanGML(actual));
  }

  private static String getCleanGML(final String gml) {
    //clean out the feature collection element, the order of the xmlns attributes isn't guaranteed.
    return USE_ORIGINAL_GML ? gml : replaceNewLines(gml.replaceAll(REGEX_FEATURE_COLLECTION_ELEMENT, ""));
  }


  static String getFileContent(final String relativePath, final String fileName) throws IOException {
    final StringBuilder result = new StringBuilder();
    final URL resource = AssertGML.class.getResource(relativePath + fileName + USED_EXTENSION);
    try {
      if (resource != null) {
        final Path path = Paths.get(resource.toURI());
        try (final BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
          String line;

          while ((line = br.readLine()) != null) {
            result.append(line);
            if (USE_ORIGINAL_GML) {
              result.append(System.getProperty("line.separator"));
            }
          }
        }
      }
      return result.toString();
    } catch (final URISyntaxException e) {
      throw new IOException(e);
    }
  }


  private static String replaceNewLines(final String string) {
    return USE_ORIGINAL_GML ? string : string.replaceAll("[\n\r]", "");
  }
}
