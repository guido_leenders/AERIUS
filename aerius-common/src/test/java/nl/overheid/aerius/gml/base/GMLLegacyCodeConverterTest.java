/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml.base;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.Conversion;
import nl.overheid.aerius.gml.base.GMLLegacyCodeConverter.GMLLegacyCodeType;

/**
 * Test class for {@link GMLLegacyCodeConverter}.
 */
public class GMLLegacyCodeConverterTest extends BaseDBTest {

  @Test
  public void testGetLegacyCodes() throws SQLException {
    try (final Connection connection = getCalcConnection()) {
      //only testing if query works, nothing more.
      final Map<GMLLegacyCodeType, Map<String, Conversion>> converter =
          GMLConversionRepository.getLegacyCodes(connection, AeriusGMLVersion.V0_5);
      assertNotNull("GMLLegacyCodeConverter", converter);
    }
  }
}
