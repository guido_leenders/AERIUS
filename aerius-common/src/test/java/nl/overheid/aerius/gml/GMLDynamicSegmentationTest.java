/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.gml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

public class GMLDynamicSegmentationTest extends BaseDBTest {
  private static final Logger LOG = Logger.getLogger(GMLDynamicSegmentationTest.class);
  private static final String DYNAMIC_SEGMENTATION_FILE = "v1_1/test_dynamic_segmentation_road.gml";

  private static TestDomain testDomain;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
  }

  @Test
  public void testConvertToEmissionSources() throws IOException, AeriusException, SQLException {
    final GMLReaderFactory factory = GMLReaderFactory.getFactory(getCalcPMF());
    EmissionSourceList sources = null;
    final List<AeriusException> errors = new ArrayList<>();
    final List<AeriusException> warnings = new ArrayList<>();

    try (InputStream inputStream = getClass().getResourceAsStream(DYNAMIC_SEGMENTATION_FILE)) {
      final GMLReader reader = factory.createReader(inputStream, testDomain.getCategories(), true, errors, warnings);
      sources = reader.readEmissionSourceList(new EmissionValueKey(Substance.NOXNH3).hatch());

    } catch (final AeriusException e) {
      LOG.error("File in error during convertToEmissionSources: " + DYNAMIC_SEGMENTATION_FILE);
      throw e;
    }

    assertTrue("Expected no exceptions/warnings: " + warnings, warnings.isEmpty());
    assertEquals("Unexpected number of sources.", 1, sources.size());
    final EmissionSource source = sources.get(0);
    assertEquals("Invalid source type.", SRM2EmissionSource.class, source.getClass());

    final SRM2EmissionSource srm2Source = (SRM2EmissionSource) source;

    assertEquals("Segment start position should be 0.5", srm2Source.getDynamicSegments().get(0).getStart(), 0.5d, 0.00001d);
    assertEquals("Invalid number of dynamic segments.", 2, srm2Source.getDynamicSegments().size());
  }
}
