/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.verification.srm2;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.srm2.conversion.EmissionSourceSRM2Converter;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.util.FileUtil;

/**
 * Unit test to verify the conversion of AERIUS road sources to SRM2RoadSegments.
 */
@RunWith(Parameterized.class)
public class SourceConversionVerificationTest extends BaseDBTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(SourceConversionVerificationTest.class);

  private static final FilenameFilter FILENAME_FILTER = new FilenameFilter() {
    @Override
    public boolean accept(final File dir, final String name) {
      return name != null && (ImportType.determineByFilename(name) == ImportType.GML
          || ImportType.determineByFilename(name) == ImportType.SRM2);
    }
  };

  private static final int YEAR = 2014;
  private static final List<EmissionValueKey> KEYS =
      Arrays.asList(
          new EmissionValueKey(YEAR, Substance.NOX),
          new EmissionValueKey(YEAR, Substance.NO2),
          new EmissionValueKey(YEAR, Substance.NH3));

  private Importer importer;

  private final String testInput;
  private final String testReference;
  private final Integer warnings;

  public SourceConversionVerificationTest(final String testInput, final String testReference, final Integer warnings) {
    this.testInput = testInput;
    this.testReference = testReference;
    this.warnings = warnings;
  }

  @Parameters(name = "{0} - {1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> parameters = new ArrayList<>();

    for (final File file : getFiles()) {
      parameters.add(new Object[] {
          file.getName(),
          FileUtil.getFileWithoutExtension(file) + ".txt",
          file.getName().endsWith(ImportType.SRM2.getExtension()) ? 1 : 0
      });
    }
    return parameters;
  }

  private static List<File> getFiles() throws FileNotFoundException {
    return FileUtil.getFilesWithExtension(new File(SourceConversionVerificationTest.class.getResource(".").getPath()), FILENAME_FILTER);
  }
  @Before
  public void before() throws SQLException, AeriusException {
    importer = new Importer(getCalcPMF());
  }

  @Test
  public void testConversionValidation() throws SQLException, IOException, AeriusException {
    final AbstractLineColumnReaderExtension refResultReader = new AbstractLineColumnReaderExtension();
    final List<SRM2RoadSegment> referenceResults = refResultReader.readObjects().getObjects();

    final ImportResult result;
    try (final InputStream fis = getFileInputStream(testInput)) {
      result = importer.convertInputStream2ImportResult(testInput, fis);
    }

    assertEquals("Exceptions during import expected no exceptions: " + result.getExceptions().toString(), 0, result.getExceptions().size());
    assertEquals("Exceptions during import expected warnings: ", warnings.intValue(), result.getWarnings().size());
    if (warnings.intValue() > 0) {
      assertEquals("Exceptions should be warning " + result.getWarnings().toString(), Reason.SRM2_FILESUPPORT_WILL_BE_REMOVED,
          result.getWarnings().get(0).getReason());
    }

    final EmissionSourceSRM2Converter converter = new EmissionSourceSRM2Converter();
    final List<SRM2RoadSegment> convertedResults = converter.convert(result.getSourceLists().get(0), KEYS);
    convertedResults.forEach(e -> {
        // Clear segment id and srm2 because it's not in the reference data
        e.setSRM2(false);
        e.setSegmentId(0);
        LOGGER.debug("{};{};{};{};{};{};{};{};{}",
            e.getStartX(), e.getStartY(), e.getEndX(), e.getEndY(), e.getElevationHeight(), e.getSigma0(),
            e.getEmission(Substance.NOX),e.getEmission(Substance.NO2),e.getEmission(Substance.NH3));
    });
    assertEquals("Expect same amount of results", referenceResults.size(), convertedResults.size());
    for (int i = 0; i < convertedResults.size(); i++) {
      assertEquals("Result for row " + (i + 1) + ", message:", referenceResults.get(i), convertedResults.get(i));
    }
  }

  /**
   * Reader for reference results.
   */
  private final class AbstractLineColumnReaderExtension extends AbstractLineColumnReader<SRM2RoadSegment> {
    public AbstractLineColumnReaderExtension() {
      super(";");
    }

    public LineReaderResult<SRM2RoadSegment> readObjects() throws IOException {
      return readObjects(getFileInputStream(testReference));
    }

    @Override
    public LineReaderResult<SRM2RoadSegment> readObjects(final InputStream inputStream) throws IOException {
      try (final InputStreamReader reader = new InputStreamReader(inputStream)) {
        return super.readObjects(reader, 1);
      }
    }

    @Override
    protected SRM2RoadSegment parseLine(final String line, final List<AeriusException> warnings) {
      final SRM2RoadSegment segment = new SRM2RoadSegment();
      segment.setStartX(getDouble("startX", 0));
      segment.setStartY(getDouble("endY", 1));
      segment.setEndX(getDouble("endX", 2));
      segment.setEndY(getDouble("endY", 3));
      segment.setElevationHeight(getInt("elevationHeight", 4));
      segment.setSigma0(getDouble("sigma0", 5));
      segment.setEmission(Substance.NOX, getDouble("NOx", 6));
      segment.setEmission(Substance.NO2, getDouble("NO2", 7));
      segment.setEmission(Substance.NH3, getDouble("NH3", 8));
      return segment;
    }
  }
}
