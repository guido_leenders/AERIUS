/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.verification.ops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.EngineSourceExpander;
import nl.overheid.aerius.calculation.conversion.OPSSourceConverter;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnFileReader;
import nl.overheid.aerius.ops.io.BrnFileWriter;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Unit test to verify the conversion of AERIUS sources to OPS sources. This unit test reads a set of gml input files
 * and converts them to OPS source files. Those files are tested against reference OPS source files.
 * Each gml file verification is run as a single test.
 */
@RunWith(Parameterized.class)
public class SourceConversionVerificationTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(SourceConversionVerificationTest.class);
  private static SourceConverter geometryExpander;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    geometryExpander = new OPSSourceConverter(getCalcPMF().getConnection());
  }

  /**
   * Constant used to check if all available verification files have been handled. This is just a check if a file
   * might be missed because somewhere in the test something happened, causing the file to be skipped.
   */
  private static final int EXPECTED_COUNT = 11;

  private static final FilenameFilter GML_FILENAME_FILTER = new FilenameFilter() {
    @Override
    public boolean accept(final File dir, final String name) {
      return (name != null) && (ImportType.determineByFilename(name) == ImportType.GML);
    }
  };

  /**
   * Comparator for sorting ops source objects so they can be compared item by item.
   */
  private static final Comparator<OPSSource> OPS_SORT_COMPARATOR = new Comparator<OPSSource>() {
    @Override
    public int compare(final OPSSource o1, final OPSSource o2) {
      final int cx = Double.compare(o1.getPoint().getX(), o2.getPoint().getX());
      final int cy = cx == 0 ? Double.compare(o1.getPoint().getY(), o2.getPoint().getY()) : cx;
      final int d = cy == 0 ? Integer.compare(o1.getDiameter(), o2.getDiameter()) : cy;
      final int dv = d == 0 ? Integer.compare(o1.getDiurnalVariation(), o2.getDiurnalVariation()) : d;
      final int hc = dv == 0 ? Double.compare(o1.getHeatContent(), o2.getHeatContent()) : dv;
      final int s = hc == 0 ? Double.compare(o1.getSpread(), o2.getSpread()) : hc;
      final int psd = s == 0 ? Integer.compare(o1.getParticleSizeDistribution(), o2.getParticleSizeDistribution()) : s;
      return psd == 0 ? Double.compare(o1.getEmissionHeight(), o2.getEmissionHeight()) : psd;
    }
  };

  private final int testId;
  private final File file;

  /**
   * Initialize test with a specific file. testId is used to check if number of GML files found is as expected.
   */
  public SourceConversionVerificationTest(final Integer testId, final String filename) {
    this.testId = testId;
    file = new File(SourceConversionVerificationTest.class.getResource(filename).getPath());
  }

  private static List<File> getGMLFiles() throws FileNotFoundException {
    return FileUtil.getFilesWithExtension(new File(SourceConversionVerificationTest.class.getResource(".").getPath()), GML_FILENAME_FILTER);
  }

  @Parameters(name = "{1}")
  public static List<Object[]> data() throws FileNotFoundException {
    final List<Object[]> fileNames = new ArrayList<>();
    int i = 0;
    for (final File file : getGMLFiles()) {
      final Object[] f = new Object[2];
      f[0] = i++;
      f[1] = file.getName();
      fileNames.add(f);
    }
    return fileNames;
  }

  @Test
  public void testNumberofGMLFiles() throws FileNotFoundException {
    // only run test once, so check on testId
    if (testId == 0) {
      assertEquals("Number of gml files tested", EXPECTED_COUNT, getGMLFiles().size());
    }
  }

  @Test
  public void testConversionValidation() throws SQLException, IOException, AeriusException {
    final Importer importer = new Importer(getCalcPMF(), LocaleUtils.getDefaultLocale());
    final String fileWithoutExt = file.getPath().substring(0, file.getPath().lastIndexOf("."));
    final ImportResult result;
    try (final InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
      result = importer.convertInputStream2ImportResult(file.getName(), inputStream);
    }
    final List<OPSSource> opsSources = new ArrayList<>();
    try (final Connection con = getCalcConnection()) {
      final List<EmissionValueKey> keys = new ArrayList<>();

      for (final Substance substance : Substance.RESULT_SUBSTANCES) {
        keys.add(new EmissionValueKey(result.getImportedYear(), substance));
      }
      // Convert sources to ops points
      conver2OpsSources(result, opsSources, con, keys);
    }
    assertNotNull("sources may never be null. GML file: " + file.getName(), opsSources);
    for (final Substance substance : Substance.RESULT_SUBSTANCES) {
      final EmissionValueKey key = new EmissionValueKey(result.getImportedYear(), substance);
      LOG.trace("Calculate emission for key: {}", key.hatch());
      final OPSInputData input = new OPSInputData(OPSVersion.VERSION, RECEPTOR_GRID_SETTINGS.getZoomLevel1().getSurfaceLevel1());
      input.setSubstances(key.getSubstance().hatch());
      input.setYear(result.getImportedYear());
      input.setEmissionResultKeys(EnumSet.copyOf(Stream.of(EmissionResultKey.values()).filter(erk -> erk.getSubstance() == substance).collect(Collectors.toSet())));
      input.setEmissionSources(opsSources);
      final File opsFile = new File(fileWithoutExt + "_" + substance.getName().toLowerCase() + ".brn");
      // get reference file, if exists for substance check results
      if (opsFile.exists()) {
        LOG.info("Verify {}", opsFile);
        //Write computed ops sources to file to make sure it matches exactly the expected ops sources, including roundings due to file format.
        final Path createTempDirectory = Files.createTempDirectory(null);
        final File computedTempPath = createTempDirectory.toFile();
        BrnFileWriter.writeFile(computedTempPath, input.getEmissionSources(), substance);
        final File computedTempFile = new File(computedTempPath, BrnFileWriter.getFileName());
        boolean testSuccess = false;
        try (InputStream cs = new FileInputStream(computedTempFile);
            InputStream is = new FileInputStream(opsFile)) {
          // read reference file
          final BrnFileReader edfr = new BrnFileReader(substance);
          final LineReaderResult<OPSSource> expectedReader = edfr.readObjects(is);
          assertTrue("Check if no errors in ops reference file. GML file: " + file.getName() + ". Exceptions: "
              + Arrays.toString(expectedReader.getExceptions().toArray()), expectedReader.getExceptions().isEmpty());
          final List<OPSSource> expectedObjects = expectedReader.getObjects();
          // sort sources and reference sources for easy comparing.
          Collections.sort(expectedObjects, OPS_SORT_COMPARATOR);
          // Read computed sources from file again.
          final BrnFileReader comfr = new BrnFileReader(substance);
          final LineReaderResult<OPSSource> computedReader = comfr.readObjects(cs);
          final List<OPSSource> actualObjects = computedReader.getObjects();
          Collections.sort(actualObjects, OPS_SORT_COMPARATOR);
          assertFalse("Check if sources list is not empty. GML file: " + file.getName(), expectedObjects.isEmpty());
          assertEquals("Check if same number of ops sources in reference file. GML file: " + file.getName(),
              expectedObjects.size(), actualObjects.size());
          for (int i = 0; i < expectedObjects.size(); i++) {
            actualObjects.get(i).setId(0); //reset id to avoid checking it.
            final OPSSource opsSource = expectedObjects.get(i);
            final Point point = opsSource.getPoint();
            final Point opssPoint = actualObjects.get(i).getPoint();
            assertEquals("Check if object (" + i + ") equal. GML file: " + file.getName(), opsSource, actualObjects.get(i));
            assertEquals("Check if x & y equal (" + opsSource + "). GML file: " + file.getName(),
                point.getRoundedX() + "," + point.getRoundedY(),
                opssPoint.getRoundedX() + "," + opssPoint.getRoundedY());
            assertEquals("Check emission equals (" + opsSource + "). GML file: " + file.getName(),
                opsSource.getEmission(substance), actualObjects.get(i).getEmission(substance), 0.001);
            assertEquals("Check if object ops characteristics (x:" + opssPoint.getX() + ",y:" + opssPoint.getY() + ") equal. GML file: "
                + file.getName(), opsSource, actualObjects.get(i));
          }
          testSuccess = true;
        } finally {
          //Delete files if test successful, or if trace not enabled.
          if (testSuccess || !LOG.isTraceEnabled()) {
            Files.delete(computedTempFile.toPath());
            Files.delete(createTempDirectory);
          }
        }
      } else if ((input.getEmissionSources() != null) && !input.getEmissionSources().isEmpty()) {
        // if not exists, but we found results it's an error.
        LOG.warn("No ops reference file found while source file has emissions for substance {} ({}), missing file: {}.",
            substance, input.getEmissionSources().size(), opsFile.getName());
      } // else no reference file and no results for substance, so nothing to check just ignore substance
    }
  }

  private void conver2OpsSources(final ImportResult result, final List<OPSSource> opsSources, final Connection con, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    for (final EngineSource es : EngineSourceExpander.toEngineSources(con, geometryExpander, result.getSources(0), keys)) {
      if (es instanceof OPSSource) {
        opsSources.add((OPSSource) es);
      }
    }
  }
}
