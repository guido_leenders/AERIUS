/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mock;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * Mocks CloseableHttpClient calls to services that return images (like tiles or wms layers).
 */
public final class MockCloseableHttpClientUtil {

  /**
   * Util class.
   */
  private  MockCloseableHttpClientUtil() {
  }

  public static CloseableHttpClient create() {
    try {
      final CloseableHttpClient client = Mockito.mock(CloseableHttpClient.class);
      final CloseableHttpResponse response = Mockito.mock(CloseableHttpResponse.class);
      final HttpEntity httpEntity = Mockito.mock(HttpEntity.class);
      final StatusLine statusLine = Mockito.mock(StatusLine.class);
      Mockito.when(statusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
      Mockito.when(response.getStatusLine()).thenReturn(statusLine);
      Mockito.when(response.getEntity()).thenReturn(httpEntity);
      final RenderedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
      final ByteArrayOutputStream os = new ByteArrayOutputStream();
      ImageIO.write(image, "png", os);
      Mockito.when(httpEntity.getContent()).thenAnswer(new Answer<InputStream>() {
        @Override
        public InputStream answer(final InvocationOnMock invocation) throws Throwable {
          return new ByteArrayInputStream(os.toByteArray());
        }
      });
      Mockito.when(client.execute((HttpUriRequest) Mockito.any())).thenReturn(response);
      return client;
    } catch (IllegalStateException | IOException e) {
      throw new RuntimeException(e);
    }
  }
}
