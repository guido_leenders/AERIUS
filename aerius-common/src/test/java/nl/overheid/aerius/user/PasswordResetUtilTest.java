/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.user;

import static org.junit.Assert.assertNotNull;

import java.security.NoSuchAlgorithmException;

import org.junit.Test;

public class PasswordResetUtilTest {
  @Test
  public void testHash() throws NoSuchAlgorithmException {
    final String hash = PasswordResetUtil.generateHash("no.one@nowhere.nl", "$6576hsgdsfgugf");

    assertNotNull("Hash is null.", hash);
  }

  @Test
  public void testHashNullPassword() throws NoSuchAlgorithmException {
    final String hash = PasswordResetUtil.generateHash("no.one@nowhere.nl", null);

    assertNotNull("Hash is null.", hash);
  }
}
