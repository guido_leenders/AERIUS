/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.register;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.HashMap;

import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.register.RequestSituation;
import nl.overheid.aerius.shared.domain.register.SituationType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RegisterImportUtil}.
 */
public class RegisterImportUtilTest {

  private static final String REQUIRED_FIELD = "Req";

  @Test
  public void testConvertToSituations() {
    final CalculatedSingle single = new CalculatedSingle();
    final int currentYear = Calendar.getInstance().get(Calendar.YEAR);

    final EmissionSourceList list1 = TestDomain.getExampleSourceList();
    list1.setName("Test list 1");
    final GenericEmissionSource source1 = (GenericEmissionSource) list1.get(0);
    source1.setYearDependent(true);
    source1.setEmission(new EmissionValueKey(currentYear, Substance.NH3), 80.8);
    single.setSources(list1);

    HashMap<SituationType, RequestSituation> situationMap = RegisterImportUtil.convertToSituations(single, currentYear);
    assertEquals("Situation map size", 1, situationMap.size());
    assertNotNull("PROPOSED situation with 1 sourcelist", situationMap.get(SituationType.PROPOSED));
    assertEquals("PROPOSED situation with 1 sourcelist, name", list1.getName(), situationMap.get(SituationType.PROPOSED).getName());
    for (final Substance substance : Substance.RESULT_SUBSTANCES) {
      if (substance != Substance.NOXNH3) {
        assertEquals("PROPOSED situation with 1 sourcelist, emission " + substance.getName(), list1.getTotalEmission(new EmissionValueKey(currentYear, substance)),
            situationMap.get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(new EmissionValueKey(substance)), 1E-3);
      }
    }

    final CalculatedComparison comparison = new CalculatedComparison();
    final EmissionSourceList list2 = TestDomain.getExampleSourceList();
    list2.setName("Test list 2");
    //when adding another, result should be 2 entries in map.
    comparison.setSourcesOne(list1);
    comparison.setSourcesTwo(list2);

    situationMap = RegisterImportUtil.convertToSituations(comparison, currentYear);
    assertEquals("Situation map size", 2, situationMap.size());
    assertNotNull("PROPOSED situation with 2 sourcelists", situationMap.get(SituationType.PROPOSED));
    assertNotNull("CURRENT situation with 2 sourcelists", situationMap.get(SituationType.CURRENT));
    assertEquals("PROPOSED situation with 2 sourcelist, name", list2.getName(), situationMap.get(SituationType.PROPOSED).getName());
    assertEquals("CURRENT situation with 2 sourcelist, name", list1.getName(), situationMap.get(SituationType.CURRENT).getName());
    for (final Substance substance : Substance.RESULT_SUBSTANCES) {
      if (substance != Substance.NOXNH3) {
        //if added later, it's the proposed situation.
        assertEquals("PROPOSED situation with 2 sourcelists, emission " + substance.getName(), list2.getTotalEmission(new EmissionValueKey(currentYear, substance)),
            situationMap.get(SituationType.PROPOSED).getTotalEmissionValues().getEmission(new EmissionValueKey(substance)), 1E-3);
        assertEquals("CURRENT situation with 2 sourcelists, emission " + substance.getName(), list1.getTotalEmission(new EmissionValueKey(currentYear, substance)),
            situationMap.get(SituationType.CURRENT).getTotalEmissionValues().getEmission(new EmissionValueKey(substance)), 1E-3);
      }
    }
  }

  @Test
  public void testValidateMetadata() throws AeriusException {
    final ScenarioMetaData metaData = new ScenarioMetaData();
    metaData.setProjectName(REQUIRED_FIELD);
    metaData.setDescription(REQUIRED_FIELD);
    metaData.setCorporation(REQUIRED_FIELD);
    metaData.setStreetAddress(REQUIRED_FIELD);
    metaData.setPostcode(REQUIRED_FIELD);
    metaData.setCity(REQUIRED_FIELD);
    metaData.setReference(REQUIRED_FIELD);
    RegisterImportUtil.validateMetaData(metaData);
  }

  @Test(expected = AeriusException.class)
  public void testValidateEmptyMetadata() throws AeriusException {
    final ScenarioMetaData metaData = new ScenarioMetaData();
    RegisterImportUtil.validateMetaData(metaData);
  }
}
