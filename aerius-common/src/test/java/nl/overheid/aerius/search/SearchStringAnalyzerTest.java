/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Test class for {@link SearchStringAnalyzer}.
 */
public class SearchStringAnalyzerTest {

  @Test
  public void testPostcode4Match() {
    final String testString = "3333";
    final boolean yes = matchPostcode(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a postcode.", yes);
  }

  @Test
  public void testNoSpacePostcode6Match() {
    final String testString = "3333AA";
    final boolean yes = matchPostcode(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a postcode.", yes);
  }

  @Test
  public void testNoSpacePostcode6MisMatch() {
    final String testString = "3333AA 1234567";
    final boolean yes = matchPostcode(testString);
    assertFalse("String \"" + testString + "\" should not recognised as a postcode.", yes);
  }

  @Test
  public void testSpacePostcode6Match() {
    final String testString = "3333 AA";
    final boolean yes = matchPostcode(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a postcode.", yes);
  }

  @Test
  public void testSpacePostcode6MisMatch() {
    final String testString = "3333 AA Blabla";
    final boolean yes = matchPostcode(testString);
    assertFalse("String \"" + testString + "\" should not recognised as a postcode.", yes);
  }

  @Test
  public void testPostcodeMisMatch() {
    final String testString = "3ABC";
    final boolean yes = matchPostcode(testString);
    assertFalse("String \"" + testString + "\" should not be recognised as a postcode.", yes);
  }

  @Test
  public void testShortIntegerMatch1() {
    final String testString = "3";
    final boolean yes = matchShortInteger(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a short integer.", yes);
  }

  @Test
  public void testShortIntegerMatch2() {
    final String testString = "33";
    final boolean yes = matchShortInteger(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a short integer.", yes);
  }

  @Test
  public void testShortIntegerMatch3() {
    final String testString = "333";
    final boolean yes = matchShortInteger(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a short integer.", yes);
  }

  @Test
  public void testShortIntegerMatch4() {
    final String testString = "3333";
    final boolean yes = matchShortInteger(testString);
    assertFalse("String \"" + testString + "\" should not be recognised as a short integer.", yes);
  }

  @Test
  public void testSingleWordMatch1() {
    final String testString = "AA";
    final boolean yes = matchSingleWord(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a single word.", yes);
  }

  @Test
  public void testSingleWordMatch2() {
    final String testString = "ABCDEFGHIJKLMNOPQ";
    final boolean yes = matchSingleWord(testString);
    assertTrue("String \"" + testString + "\" should be recognised as a single word.", yes);
  }

  @Test
  public void testSingleWordMatch3() {
    final String testString = "3ABC";
    final boolean yes = matchSingleWord(testString);
    assertFalse("String \"" + testString + "\" should not be recognised as a single word.", yes);
  }

  @Test
  public void testSingleWordMatch4() {
    final String testString = "Bla bla";
    final boolean yes = matchShortInteger(testString);
    assertFalse("String \"" + testString + "\" should not be recognised as a single word.", yes);
  }

  /**
   * @param testString
   * @return
   */
  private boolean matchPostcode(final String testString) {
    final SearchStringAnalyzer ssa = new SearchStringAnalyzer(testString);
    final boolean yes = ssa.isPostcodeMatch();
    return yes;
  }

  /**
   * @param testString
   * @return
   */
  private boolean matchShortInteger(final String testString) {
    final SearchStringAnalyzer ssa = new SearchStringAnalyzer(testString);
    final boolean yes = ssa.isShortIntegerMatch();
    return yes;
  }

  /**
   * @param testString
   * @return
   */
  private boolean matchSingleWord(final String testString) {
    final SearchStringAnalyzer ssa = new SearchStringAnalyzer(testString);
    final boolean yes = ssa.isSingleWordMatch();
    return yes;
  }
}
