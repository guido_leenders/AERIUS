/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;

/**
 * Test class for {@link SearchGeocoder}.
 */
public class SearchLocationServerTest extends BaseDBTest {

  /**
   * String forming the base on which the actual query String is built.
   * Should be kept synchronized with the String stored in the database Constants table.
   */
  private static String baseUrlString;
  private static final String SEARCH_STR_UTRECHT = "utrecht";
  private static final String SEARCH_STR_CROESELAAN = "croeselaan 15";
  private static final String SEARCH_STR_CROESELAAN_POSTCODE = "3521 BJ";
  private static final String EPSG_CODE = EPSGProxy.getEPSG(RDNew.SRID).getEpsgCode();
  private static final String SEARCH_STR_VIJFHEERENLANDEN = "vijfheerenlanden";

  @BeforeClass
  public static void setupBeforeClass() {
    baseUrlString = ConstantRepository.getString(getCalcPMF(), SharedConstantsEnum.PDOK_LOCATIESERVER_URL, false);
  }

  @Test
  public void testCallLocatieServerMunicipality() {
    final Collection<MapSearchSuggestion> results = wrappedSearch(SEARCH_STR_UTRECHT);
    assertEquals("Callgeocoder results count", 8, results.size());
    final Iterator<MapSearchSuggestion> resultIterator = results.iterator();
    final MapSearchSuggestion firstSuggestion = resultIterator.next();
    assertEquals("Locatieserver results idx at 0 point x", 133587, (int) firstSuggestion.getPoint().getX());
    assertEquals("Locatieserver results idx at 0 point y", 455922, (int) firstSuggestion.getPoint().getY());
    assertTrue("Locatieserver results idx at 0 name", SEARCH_STR_UTRECHT.equalsIgnoreCase(firstSuggestion.getName()));
    assertTypes(results);
  }

  @Test
  public void testCallLocatieServerGSONUtreg() {
    final Collection<MapSearchSuggestion> results = wrappedSearch(SEARCH_STR_UTRECHT);
    assertEquals("Locatieserver results count", 8, results.size());
    final Iterator<MapSearchSuggestion> resultIterator = results.iterator();
    final MapSearchSuggestion firstSuggestion = resultIterator.next();
    assertEquals("Locatieserver results idx at 0 point x", 133587, (int) firstSuggestion.getPoint().getX());
    assertEquals("Locatieserver results idx at 0 point y", 455922, (int) firstSuggestion.getPoint().getY());
    assertTrue("Locatieserver results idx at 0 name", SEARCH_STR_UTRECHT.equalsIgnoreCase(firstSuggestion.getName()));
    assertTypes(results);
  }

  @Test
  public void testCallLocatieServerGSONCroeselaan15() {
    final Collection<MapSearchSuggestion> results = wrappedSearch(SEARCH_STR_CROESELAAN);
    assertEquals("Locatieserver results count", 1, results.size());
    final Iterator<MapSearchSuggestion> resultIterator = results.iterator();
    final MapSearchSuggestion firstSuggestion = resultIterator.next();
    assertEquals("Locatieserver results idx at 0 point x", 135650, (int) firstSuggestion.getPoint().getX());
    assertEquals("Locatieserver results idx at 0 point y", 455603, (int) firstSuggestion.getPoint().getY());
    assertTrue("Locatieserver results idx at 0 name", firstSuggestion.getName().toLowerCase().contains(SEARCH_STR_UTRECHT.toLowerCase()));
    assertEquals("Locatieserver result should contain ADDRESS", MapSearchSuggestionType.ADDRESS, firstSuggestion.getType());
  }

  @Test
  public void testCallLocatieServerGSONCroeselaan15Postcode() {
    final Collection<MapSearchSuggestion> results = wrappedSearch(SEARCH_STR_CROESELAAN_POSTCODE);
    assertEquals("Locatieserver results count", 4, results.size());
    final Iterator<MapSearchSuggestion> resultIterator = results.iterator();
    final MapSearchSuggestion firstSuggestion = resultIterator.next();
    assertEquals("Locatieserver results idx at 0 point x", 135621, (int) firstSuggestion.getPoint().getX());
    assertEquals("Locatieserver results idx at 0 point y", 455661, (int) firstSuggestion.getPoint().getY());
    assertTrue("Locatieserver results idx at 0 name", firstSuggestion.getName().toLowerCase().contains(SEARCH_STR_UTRECHT.toLowerCase()));
    assertEquals("Locatieserver result should contain ADDRESS", MapSearchSuggestionType.ADDRESS, firstSuggestion.getType());
  }

  @Test
  public void testCallLocatieServerMunicipality_Vijfheerenlanden() {
    final Collection<MapSearchSuggestion> results = wrappedSearch(SEARCH_STR_VIJFHEERENLANDEN);
    assertEquals("Callgeocoder results count", 4, results.size());
    final Iterator<MapSearchSuggestion> resultIterator = results.iterator();
    final MapSearchSuggestion firstSuggestion = resultIterator.next();
    assertEquals("Locatieserver results idx at 0 point x", 132275, (int) firstSuggestion.getPoint().getX());
    assertEquals("Locatieserver results idx at 0 point y", 438665, (int) firstSuggestion.getPoint().getY());
    assertTrue("Locatieserver results idx at 0 name", SEARCH_STR_VIJFHEERENLANDEN.equalsIgnoreCase(firstSuggestion.getName()));
  }

  /**
   * @param results
   */
  private void assertTypes(final Collection<MapSearchSuggestion> results) {
    final Set<MapSearchSuggestionType> typetjes = new HashSet<>();
    for (final MapSearchSuggestion result : results) {
      typetjes.add(result.getType());
    }
    assertTrue("Locatieserver results should contain at least one TOWN_AREA", typetjes.contains(MapSearchSuggestionType.TOWN_AREA));
  }

  /**
   * @return
   */
  private Collection<MapSearchSuggestion> wrappedSearch(final String searchString) {
    final SearchLocationServer gcSearch = new SearchLocationServer();
    final Collection<MapSearchSuggestion> results = gcSearch.callLocatieServer(EPSG_CODE, baseUrlString, searchString);
    return results;
  }
}
