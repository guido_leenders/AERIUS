/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link SearchRepository}.
 */
public class SearchRepositoryTest extends BaseDBTest {
  private static final String SEARCH_STR_UTRECHT = "utrecht";
  private static final String SEARCH_STR_BINNENVELD = "binnenveld";

  @Test
  public void testGetDatabaseSuggestionsProvince() throws AeriusException {
    final ArrayList<MapSearchSuggestion> results = SearchRepository.getSuggestions(getCalcPMF(), SEARCH_STR_UTRECHT);
    assertEquals("SearchRepository Province results count", 4, results.size());
    assertFalse("SearchRepository Province subitems results not empty", results.get(0).getSubItems().isEmpty());
  }

  @Test
  public void testGetDatabaseSuggestionsNatureArea() throws AeriusException {
    final ArrayList<MapSearchSuggestion> results = SearchRepository.getSuggestions(getCalcPMF(), SEARCH_STR_BINNENVELD);
    assertEquals("SearchRepository Nature Area results count", 1, results.size());
    assertTrue("SearchRepository Nature Area subitems results", results.get(0).getSubItems().isEmpty());
  }

  @Test
  public void testGetSubSuggestionsXY() throws AeriusException {
    final MapSearchSuggestion ss = new MapSearchSuggestion(1, "x: 142822 y: 459168", MapSearchSuggestionType.XY_COORDINATE);
    ss.setPoint(new Point(142822, 459168));
    final ArrayList<MapSearchSuggestion> results = SearchRepository.getSubSuggestions(getCalcPMF(), ss);
    assertFalse("SearchRepository X-Y results not empty", results.isEmpty());
  }

  @Test(expected = AeriusException.class)
  public void testGetSubSuggestionsNatureArea() throws AeriusException {
    final MapSearchSuggestion ss = new MapSearchSuggestion(1, SEARCH_STR_BINNENVELD, MapSearchSuggestionType.NATURE_AREA);
    ss.setPoint(new Point(168042, 447079));
    SearchRepository.getSubSuggestions(getCalcPMF(), ss);
  }

  @Test
  public void testGetSubSuggestionsProvince() throws AeriusException {
    final MapSearchSuggestion ss = new MapSearchSuggestion(1, SEARCH_STR_UTRECHT, MapSearchSuggestionType.PROVINCE_AREA);
    ss.setPoint(new Point(142822, 459168));
    final ArrayList<MapSearchSuggestion> results = SearchRepository.getSubSuggestions(getCalcPMF(), ss);
    assertFalse("SearchRepository province results not empty", results.isEmpty());
  }
}
