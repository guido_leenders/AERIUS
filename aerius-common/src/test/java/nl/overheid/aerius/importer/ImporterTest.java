/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.importer;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.CalculatorLimits;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.LandUse;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link Importer}.
 */
public class ImporterTest extends BaseDBTest {

  private Importer importer;

  @Before
  public void before() throws SQLException, AeriusException {
    importer = new Importer(getCalcPMF());
  }

  @Test
  public void testExceedMaxSources() throws AeriusException, IOException, SQLException {
    final CalculatorLimits limits = new CalculatorLimits();
    final String filename = "max_sources_exceeded.brn";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1);
      importer.convertInputStream2ImportResult(filename, inputStream, Substance.NH3, limits);
      fail("Exceed of max sources not detected");
    } catch (final AeriusException e) {
      assertEquals("MaxSources", Reason.LIMIT_SOURCES_EXCEEDED, e.getReason());
    }
  }

  @Test
  public void testExceedMaxLine() throws AeriusException, IOException, SQLException {
    final CalculatorLimits limits = new CalculatorLimits();
    final String filename = "max_line_exceeded.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1000);
      limits.setMaxLineLength(10);
      importer.convertInputStream2ImportResult(filename, inputStream, null, limits);
      fail("Exceed of max line length not detected");
    } catch (final AeriusException e) {
      assertEquals("MaxLineLength", Reason.LIMIT_LINE_LENGTH_EXCEEDED, e.getReason());
    }
  }

  @Test
  public void testZeroLineLength() throws AeriusException, IOException, SQLException {
    final CalculatorLimits limits = new CalculatorLimits();
    final String filename = "zero_line_length.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1000);
      limits.setMaxLineLength(10);
      importer.convertInputStream2ImportResult(filename, inputStream, null, limits);
      fail("Should throw exception.");
    } catch (final AeriusException e) {
      assertEquals("Exception " + Reason.LIMIT_LINE_LENGTH_ZERO, Reason.LIMIT_LINE_LENGTH_ZERO, e.getReason());
    }
  }

  @Test
  public void testSMR2ZeroLineLength() throws AeriusException, IOException, SQLException {
    final CalculatorLimits limits = new CalculatorLimits();
    final String filename = "zero_line_length.csv";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1000);
      limits.setMaxLineLength(10);
      importer.convertInputStream2ImportResult(filename, inputStream, null, limits);
      fail("Should throw exception.");
    } catch (final AeriusException e) {
      assertEquals("Exception " + Reason.LIMIT_LINE_LENGTH_ZERO, Reason.LIMIT_LINE_LENGTH_ZERO, e.getReason());
    }
  }

  @Test
  public void testExceedMaxSurface() throws AeriusException, IOException, SQLException {
    final CalculatorLimits limits = new CalculatorLimits();
    final String filename = "max_surface_exceeded.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      limits.setMaxSources(1000);
      limits.setMaxPolygonSurface(10);
      importer.convertInputStream2ImportResult(filename, inputStream, null, limits);
      fail("Exceed of max surface size not detected");
    } catch (final AeriusException e) {
      assertEquals("MaxPolygonSurface", Reason.LIMIT_POLYGON_SURFACE_EXCEEDED, e.getReason());
    }
  }

  @Test
  public void testLineIntersects() throws AeriusException, IOException, SQLException {
    final String filename = "line_intersects.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      importer.convertInputStream2ImportResult(filename, inputStream);
    } catch (final AeriusException e) {
      assertEquals("LineIntersects error code", Reason.GML_GEOMETRY_INTERSECTS, e.getReason());
      assertEquals("LineIntersects name check", "source 2", e.getArgs()[0]);
    }
  }

  @Test
  public void testPolygonIntersects() throws AeriusException, IOException, SQLException {
    final String filename = "geometry_intersects.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      importer.convertInputStream2ImportResult(filename, inputStream);
      fail("Polygon intersects not detected");
    } catch (final AeriusException e) {
      assertEquals("PolygonIntersects error code", Reason.GML_GEOMETRY_INTERSECTS, e.getReason());
      assertEquals("PolygonIntersects name check", "source 2", e.getArgs()[0]);
    }
  }

  @Test
  public void testGMLImportWithCustomPointsWithoutResults() throws IOException, AeriusException, SQLException {
    final String filename = "custom_calculation_points_with_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      final ImportResult result = importer.convertInputStream2ImportResult(filename, inputStream);

      assertNoExceptions(result);
      assertEquals("Count nr. of calculation points", 15, result.getCalculationPoints().size());
      final AeriusPoint ap = result.getCalculationPoints().get(0);
      assertNotSame("Should not be a AeriusResultPoint", AeriusResultPoint.class, ap.getClass());
    }
  }

  @Test
  public void testGMLImportWithCustomPointsWithResults() throws IOException, AeriusException, SQLException {
    final String filename = "custom_calculation_points_with_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      importer.setIncludeResults(true);
      final ImportResult result = importer.convertInputStream2ImportResult(filename, inputStream, null);

      assertNoExceptions(result);
      final AeriusPoint ap = result.getCalculationPoints().get(0);
      assertSame("Should be a AeriusResultPoint", AeriusResultPoint.class, ap.getClass());
      assertTrue("Should have results", result.hasAnyResultPoints());
      assertFalse("AeriusResultPoint should have results", ((AeriusResultPoint) ap).getEmissionResults().isEmpty());
    }
  }

  @Test
  public void testGMLImportWithCustomPointsWithResultsFilterResults() throws IOException, AeriusException, SQLException {
    final String filename = "custom_calculation_points_with_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      importer.setIncludeResults(false);
      final ImportResult result = importer.convertInputStream2ImportResult(filename, inputStream, null);

      assertNoExceptions(result);
      final AeriusPoint ap = result.getCalculationPoints().get(0);
      assertNotSame("Should not be AeriusResultPoint", AeriusResultPoint.class, ap.getClass());
      assertFalse("Should not have results", result.hasAnyResultPoints());
    }
  }

  @Test
  public void testGMLImportWithCalculationResults() throws IOException, AeriusException, SQLException {
    final String filename = "with_calculation_results.gml";
    try (final InputStream inputStream = getFileInputStream(filename)) {
      final ImportResult result =
          importer.convertInputStream2ImportResult(filename, inputStream);

      assertNoExceptions(result);
      assertTrue("Count nr. of calculation points", result.getCalculationPoints().isEmpty());
    }
  }

  @Test
  public void testPDFImport() throws AeriusException, IOException, SQLException {
    final String fileName = "AERIUS_bijlage_test.pdf";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result =
          importer.convertInputStream2ImportResult(fileName, inputStream);
      assertNoExceptions(result);
      assertEquals("Number of sources", 1, result.getSources(0).size());
      assertEquals("Name of source", "AERIUS", result.getSources(0).get(0).getLabel());
      assertEquals("Number of sources in variant", 1, result.getSources(1).size());
      assertEquals("Number of custom calculation points", 0, result.getCalculationPoints().size());
      assertEquals("Imported year", 2014, result.getImportedYear());
    }
  }

  @Test
  public void testZIPImportComparison() throws AeriusException, IOException, SQLException {
    final String fileName = "AERIUS_gml_test_comparison.zip";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result =
          importer.convertInputStream2ImportResult(fileName, inputStream);
      assertNoExceptions(result);
      assertEquals("Number of sources", 1, result.getSources(0).size());
      assertEquals("Name of original list", "voor verhuizing", result.getSources(0).getName());
      assertEquals("Number of sources in variant", 1, result.getSources(1).size());
      assertEquals("Name of variant list", "na verhuizing", result.getSources(1).getName());
      assertEquals("Number of custom calculation points", 0, result.getCalculationPoints().size());
      assertEquals("Imported year", 2013, result.getImportedYear());
    }
  }

  @Test
  public void testZIPImportSingle() throws AeriusException, IOException, SQLException {
    final String fileName = "AERIUS_gml_test_single.zip";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result =
          importer.convertInputStream2ImportResult(fileName, inputStream);
      assertNoExceptions(result);
      assertEquals("Number of sources", 1, result.getSources(0).size());
      assertEquals("Name of variant list", "na verhuizing", result.getSources(0).getName());
      assertNull("Variant", result.getSources(1));
      assertEquals("Number of custom calculation points", 0, result.getCalculationPoints().size());
      assertEquals("Imported year", 2013, result.getImportedYear());
    }
  }

  /**
   * case 1: empty zip file
   */
  @Test
  public void testZIPImportIncorrectEmptyFile() throws AeriusException, IOException, SQLException {
    final String fileName1 = "AERIUS_incorrect_import_1.zip";
    try (final InputStream inputStream = getFileInputStream(fileName1)) {
      importer.convertInputStream2ImportResult(fileName1, inputStream);
      fail("Should get an exception on empty zip file");
    } catch (final AeriusException e) {
      assertEquals("Error code for empty zip exception", Reason.ZIP_WITHOUT_USABLE_FILES, e.getReason());
    }
  }

  /**
   * case 2: zip file containing no aerius import-able files (by extension).
   */
  @Test
  public void testZIPImportIncorrectNoValidFiles() throws IOException {
    final String fileName2 = "AERIUS_incorrect_import_2.zip";
    try (final InputStream inputStream = getFileInputStream(fileName2)) {
      importer.convertInputStream2ImportResult(fileName2, inputStream);
      fail("Should get an exception on zip file with no import-able files");
    } catch (final AeriusException e) {
      assertEquals("Error code for unusable files in zip exception", Reason.ZIP_WITHOUT_USABLE_FILES, e.getReason());
    }
  }

  /**
   * case 3: zip file containing aerius importable files (by extension) but not an actual aerius file.
   */
  @Test
  public void testZIPImportIncorrectNoAeriusFile() throws IOException {
    final String fileName3 = "AERIUS_incorrect_import_3.zip";
    try (final InputStream inputStream = getFileInputStream(fileName3)) {
      importer.convertInputStream2ImportResult(fileName3, inputStream);
      fail("Should get an exception with valid file extension but file content incorrect");
    } catch (final AeriusException e) {
      assertEquals("Error code for invalid files exception", Reason.PAA_VALIDATION_FAILED, e.getReason());
    }
  }

  /**
   * case 4: zip file containing 3 GML files.
   */
  @Test
  public void testZIPImportIncorrect3GML() throws IOException, AeriusException {
    final String fileName = "AERIUS_incorrect_import_4.zip";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result = importer.convertInputStream2ImportResult(fileName, inputStream);
      assertEquals("Should only import 2 gml files", 2, result.getSourceLists().size());
    }
  }

  @Test
  public void testRCPImportWithLandUses() throws AeriusException, IOException, SQLException {
    final String fileName = "receptors_with_landuse.rcp";
    importer.setUseImportedLanduses(true);
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result =
          importer.convertInputStream2ImportResult(fileName, inputStream);
      assertNoExceptions(result);
      assertEquals("Number of receptors", 239, result.getCalculationPoints().size());
      for (final AeriusPoint point : result.getCalculationPoints()) {
        assertTrue("Point should be OPS receptor", point instanceof OPSReceptor);
        assertAeriusPoint(point);
        final OPSReceptor opsPoint = (OPSReceptor) point;
        assertNotNull("Average roughness for point " + opsPoint.getId(), opsPoint.getAverageRoughness());
        assertNotNull("LandUse for point " + opsPoint.getId(), opsPoint.getLandUse());
        assertNotNull("LandUse array for point " + opsPoint.getId(), opsPoint.getLandUses());
      }
      assertSpecificPoint(result.getCalculationPoints().get(0), 4245393);
    }
  }

  @Test
  public void testRCPImportWithoutLandUses() throws AeriusException, IOException, SQLException {
    final String fileName = "receptors_with_landuse.rcp";
    importer.setUseImportedLanduses(false);
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result =
          importer.convertInputStream2ImportResult(fileName, inputStream);
      assertNoExceptions(result);
      assertEquals("Number of receptors", 239, result.getCalculationPoints().size());
      for (final AeriusPoint point : result.getCalculationPoints()) {
        assertSame("Point class should be AeriusPoint (without landuses and such)", AeriusPoint.class, point.getClass());
        assertAeriusPoint(point);
      }
      assertSpecificPoint(result.getCalculationPoints().get(0), 1);
    }
  }

  @Test
  public void testSRM2ConvertExceptions() throws SQLException, IOException, AeriusException {
    final String fileName = "srm2_roads_example_exeception.csv";
    try (final InputStream inputStream = getFileInputStream(fileName)) {
      final ImportResult result = importer.convertInputStream2ImportResult(fileName, inputStream);

      assertFalse("Warning list should not be empty", result.getWarnings().isEmpty());
      assertEquals("Warning list length should be 1", 1, result.getWarnings().size());
      assertFalse("Exception list should not be empty", result.getExceptions().isEmpty());
      assertEquals("Exception list length should be 6", 6, result.getExceptions().size());

      for (final AeriusException warning : result.getWarnings()) {
        assertEquals("Warning should be " + Reason.SRM2_FILESUPPORT_WILL_BE_REMOVED, Reason.SRM2_FILESUPPORT_WILL_BE_REMOVED, warning.getReason());
      }

      for (final AeriusException exception : result.getExceptions()) {
        assertEquals("Exception should be " + Reason.SRM2_SOURCE_NEGATIVE_VEHICLES, Reason.SRM2_SOURCE_NEGATIVE_VEHICLES, exception.getReason());
      }
    }
  }

  private void assertNoExceptions(final ImportResult result) {
    assertEquals("No exceptions:" + ArrayUtils.toString(result.getExceptions()), 0, result.getExceptions().size());
  }

  private void assertAeriusPoint(final AeriusPoint point) {
    assertNotEquals("ID of point", 0, point.getId());
    assertNotEquals("X-coord of point " + point.getId(), 0, point.getX(), 1E-5);
    assertNotEquals("y-coord of point " + point.getId(), 0, point.getY(), 1E-5);
    assertNotNull("Label for point " + point.getId(), point.getLabel());
  }

  private void assertSpecificPoint(final AeriusPoint point, final int id) {
    assertEquals("ID of specific point", id, point.getId());
    assertEquals("X-coord of specific point", 168879, point.getX(), 1E-5);
    assertEquals("y-coord of specific point", 445950, point.getY(), 1E-5);
    assertEquals("Label for specific point", "4245393", point.getLabel());
    if (point instanceof OPSReceptor) {
      final OPSReceptor opsPoint = (OPSReceptor) point;
      assertEquals("Average roughness for specific point", 0.028959, opsPoint.getAverageRoughness(), 1E-5);
      assertEquals("LandUse for specific point", LandUse.GRASS, opsPoint.getLandUse());
      assertArrayEquals(new int[] {98, 0, 0, 0, 0, 2, 0, 0, 0}, opsPoint.getLandUses());
    }
  }
}
