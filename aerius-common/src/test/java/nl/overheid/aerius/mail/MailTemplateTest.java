/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.mail;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.i18n.MessageRepository;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Testing templates in the database: If they contain replacement tokens, are they correctly spelled and expected?
 */
public class MailTemplateTest extends BaseDBTest {

  private static final Pattern REPLACEMENT_TOKEN_PATTERN = Pattern.compile("\\[(\\w*)\\]");

  @Test
  public void testTemplates() throws SQLException {
    final Map<MessagesEnum, EnumSet<ReplacementToken>> templateMap = getTemplateMap();
    for (final Entry<MessagesEnum, EnumSet<ReplacementToken>> entry : templateMap.entrySet()) {
      final String template = MessageRepository.getString(getCalcConnection(), entry.getKey(), LocaleUtils.getDefaultLocale());
      assertEquals("Replacement tokens in template for " + entry.getKey(), entry.getValue(), getTokensInTemplate(template));
    }
  }

  private EnumSet<ReplacementToken> getTokensInTemplate(final String template) {
    final Set<ReplacementToken> tokensFound = new HashSet<>();
    final Matcher matcher = REPLACEMENT_TOKEN_PATTERN.matcher(template);
    while (matcher.find()) {
      tokensFound.add(ReplacementToken.valueOf(matcher.group(1)));
    }
    return tokensFound.isEmpty() ? EnumSet.noneOf(ReplacementToken.class) : EnumSet.copyOf(tokensFound);
  }

  private Map<MessagesEnum, EnumSet<ReplacementToken>> getTemplateMap() {
    final Map<MessagesEnum, EnumSet<ReplacementToken>> templateMap = new HashMap<>();
    //ensure all message enums are in the map
    for (final MessagesEnum messagesEnum : MessagesEnum.values()) {
      templateMap.put(messagesEnum, EnumSet.noneOf(ReplacementToken.class));
    }
    //set every expected replacement token
    templateMap.put(MessagesEnum.MAIL_SUBJECT_TEMPLATE, EnumSet.of(ReplacementToken.MAIL_SUBJECT));
    templateMap.put(MessagesEnum.MAIL_CONTENT_TEMPLATE, EnumSet.of(ReplacementToken.MAIL_CONTENT, ReplacementToken.MAIL_SIGNATURE));

    templateMap.put(MessagesEnum.DEFAULT_FILE_MAIL_CONTENT,
        EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.ERROR_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.ERROR_CODE, ReplacementToken.ERROR_MESSAGE, ReplacementToken.ERROR_SOLUTION));

    templateMap.put(MessagesEnum.PASSWORD_RESET_REQUEST_BODY, EnumSet.of(ReplacementToken.PASSWORD_RESET_URL));
    templateMap.put(MessagesEnum.PASSWORD_RESET_CONFIRM_BODY, EnumSet.of(ReplacementToken.PASSWORD_RESET_PLAIN_TEXT_NEW));

    templateMap.put(MessagesEnum.PAA_DEVELOPMENT_SPACES_MAIL_SUBJECT, EnumSet.of(ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.PAA_DEMAND_MAIL_SUBJECT, EnumSet.of(ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.PAA_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.PAA_OWN_USE_MAIL_SUBJECT, EnumSet.of(ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.PAA_OWN_USE_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.GML_MAIL_SUBJECT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE));
    templateMap.put(MessagesEnum.GML_MAIL_SUBJECT_JOB, EnumSet.of(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.JOB));
    templateMap.put(MessagesEnum.GML_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.CSV_MAIL_SUBJECT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE));
    templateMap.put(MessagesEnum.CSV_MAIL_SUBJECT_JOB, EnumSet.of(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.JOB));
    templateMap.put(MessagesEnum.CSV_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME,
        ReplacementToken.DOWNLOAD_LINK));

    templateMap.put(MessagesEnum.MELDING_REGISTERED_USER_MAIL_SUBJECT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE, ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.MELDING_REGISTERED_USER_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.DOWNLOAD_LINK,
        ReplacementToken.AERIUS_REFERENCE));
    templateMap.put(MessagesEnum.MELDING_REGISTERED_USER_MAIL_WITH_ATTACHMENTS_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, ReplacementToken.DOWNLOAD_LINK,
        ReplacementToken.AERIUS_REFERENCE));

    templateMap.put(MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.AERIUS_REFERENCE,
        ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING));
    templateMap.put(MessagesEnum.MELDING_NOT_REGISTERED_USER_MAIL_ATTACHMENTSLIST_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, ReplacementToken.AERIUS_REFERENCE,
        ReplacementToken.TECHNICAL_REASON_NOT_REGISTERED_MELDING));

    templateMap.put(MessagesEnum.MELDING_NOT_REGISTERED_TECHNICAL_REASON, EnumSet.of(ReplacementToken.REASON_NOT_REGISTERED_MELDING));

    templateMap.put(MessagesEnum.MELDING_AUTHORITY_MAIL_SUBJECT,
        EnumSet.of(ReplacementToken.AUTHORITY, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.MELDING_AUTHORITY_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.DOWNLOAD_LINK, ReplacementToken.AUTHORITY,
        ReplacementToken.AERIUS_REFERENCE));

    templateMap.put(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBJECT,
        EnumSet.of(ReplacementToken.AUTHORITY, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.PROJECT_NAME, ReplacementToken.DOCUMENT_MAIL_NR,
            ReplacementToken.MAX_DOCUMENT_MAIL_NR));
    templateMap.put(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.MAIL_SUBSTANTIATION, ReplacementToken.AERIUS_REFERENCE));

    templateMap.put(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_MAIL_SUBSTANTIATION, EnumSet.of(ReplacementToken.MELDING_SUBSTANTIATION));

    templateMap.put(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_SUBJECT,
        EnumSet.of(ReplacementToken.AUTHORITY, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.MELDING_DOCUMENTS_AUTHORITY_STATUS_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.MELDING_ATTACHMENTS_RESULT_LIST, ReplacementToken.AERIUS_REFERENCE));

    templateMap.put(MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_SUBJECT,
        EnumSet.of(ReplacementToken.AUTHORITY, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.MELDING_NOT_REGISTERED_AUTHORITY_MAIL_CONTENT, EnumSet.of(ReplacementToken.CALC_CREATION_DATE,
        ReplacementToken.CALC_CREATION_TIME, ReplacementToken.REASON_NOT_REGISTERED_MELDING, ReplacementToken.AUTHORITY));

    templateMap.put(MessagesEnum.MELDING_DELETE_AUTHORITY_MAIL_SUBJECT,
        EnumSet.of(ReplacementToken.AUTHORITY, ReplacementToken.AERIUS_REFERENCE, ReplacementToken.PROJECT_NAME));
    templateMap.put(MessagesEnum.MELDING_DELETE_AUTHORITY_MAIL_CONTENT, EnumSet.of(ReplacementToken.AERIUS_REFERENCE,
        ReplacementToken.MELDING_CREATE_DATE, ReplacementToken.MELDING_REMOVE_DATE, ReplacementToken.MELDING_REMOVED_BY));

    templateMap.put(MessagesEnum.CONNECT_APIKEY_CONFIRM_BODY,
        EnumSet.of(ReplacementToken.CALC_CREATION_DATE, ReplacementToken.CALC_CREATION_TIME, ReplacementToken.CONNECT_APIKEY));

    return templateMap;
  }
}
