<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:imaer="http://imaer.aerius.nl/1.0" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/1.0 http://imaer.aerius.nl/1.0/IMAER.xsd">
    <imaer:aeriusCalculatorMetaData>
        <imaer:year>2014</imaer:year>
        <imaer:version>1.0-SNAPSHOT_20150624_6392a6c8ab</imaer:version>
        <imaer:databaseVersion>1.0-SNAPSHOT_20150624_f92d2a3375</imaer:databaseVersion>
        <imaer:situationName>Situatie 1</imaer:situationName>
    </imaer:aeriusCalculatorMetaData>
    <imaer:featureMembers>
        <imaer:EmissionSource sectorId="1800" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Bron 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>0.0</imaer:heatContent>
                    <imaer:emissionHeight>4.5</imaer:emissionHeight>
                    <imaer:diurnalVariation>INDUSTRIAL_ACTIVITY</imaer:diurnalVariation>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Point>
                        <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.POINT">
                            <gml:pos>168638.36094246 446718.74820774</gml:pos>
                        </gml:Point>
                    </imaer:GM_Point>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>10000.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
        </imaer:EmissionSource>
        <imaer:CalculationPoint gml:id="CP.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1.POINT">
                    <gml:pos>170168.0 440674.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>25.4659</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.128</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>18.85328482672544</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn H6510A (6 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.2.POINT">
                    <gml:pos>174593.0 447730.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>36.892700000000005</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.199</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>25.749515018480963</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Veluwe H9120 (6 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.3">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.3</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.3.POINT">
                    <gml:pos>168399.0 446785.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>29.55</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>34.8</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>21.38334966</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Binnenveld H7140A</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.4">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4.POINT">
                    <gml:pos>165947.0 450966.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>33.3728</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.274</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>23.68576853186816</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>MEEUWENKAMPJE (5 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.5">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.5</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.5.POINT">
                    <gml:pos>171648.0 439932.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>24.1451</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.106</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>18.019526572274238</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn H3150baz (7 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.6">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.6</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.6.POINT">
                    <gml:pos>165736.0 442102.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>23.1802</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.207</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>17.40563196284096</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn (5 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.7">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.7</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.7.POINT">
                    <gml:pos>168460.0 446834.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>30.41</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>44.0</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>21.9068600744</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Binnenveld</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.8">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.8</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.8.POINT">
                    <gml:pos>165552.0 442218.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>23.184</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.219</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>17.408057569344003</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn H91E0A (5 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.9">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.9</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.9.POINT">
                    <gml:pos>169041.0 440305.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>23.9725</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.123</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>17.9100115094</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn H91F0 (6 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.10">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.10</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.10.POINT">
                    <gml:pos>170000.0 440084.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>0.0248</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.118</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>0.0248</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn H3270 (7 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.11">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.11</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.11.POINT">
                    <gml:pos>174629.0 447205.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>36.895500000000006</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.209</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>25.751135218536003</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Veluwe (6 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.12">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.12</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.12.POINT">
                    <gml:pos>170704.0 440096.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>25.4686</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.111</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>18.85498143255104</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Uiterwaarden Neder-Rijn H6430A (7 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.13">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.13</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.13.POINT">
                    <gml:pos>175833.0 449100.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>23.864</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.187</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>17.841101664704</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Veluwe H4030 (8 km)</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.14">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.14</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.14.POINT">
                    <gml:pos>168210.0 446723.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>27.04</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>12.9</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>19.8370235984</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Binnenveld H6410</imaer:label>
        </imaer:CalculationPoint>
        <imaer:CalculationPoint gml:id="CP.15">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.15</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.15.POINT">
                    <gml:pos>168148.0 446894.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NOX">
                    <imaer:value>26.35</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>9.01</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="CONCENTRATION" substance="NO2">
                    <imaer:value>19.40713334</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:label>Binnenveld H7140B (1 km)</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMembers>
</imaer:FeatureCollectionCalculator>
