/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.UserRepository;
import nl.overheid.aerius.db.register.FilterRepository;
import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.exception.AuthorizationException;
import nl.overheid.aerius.shared.service.AdminService;
import nl.overheid.aerius.user.PasswordUtil;

/**
 * FIXME Error messages are concatenated in a way they remain in the string buffers, use string format or rethink handleSQLException()
 */
public class AdminServiceImpl implements AdminService {
  private static final Logger LOG = LoggerFactory.getLogger(AdminServiceImpl.class);

  private final Permission administratorPermission;
  private final PMF pmf;

  public AdminServiceImpl(final Permission administratorPermission, final PMF pmf) {
    this.administratorPermission = administratorPermission;
    this.pmf = pmf;
  }

  @Override
  public void updateUser(final AdminUserProfile profile) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkPermission(connection);
      checkEdit(connection, profile);
      UserRepository.updateUser(connection, profile);
    } catch (final SQLException e) {
      handleSQLException(e, "updating user: " + profile.getName());
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public ArrayList<AdminUserProfile> getUsers(final UserManagementFilter filter) throws AeriusException {

    ArrayList<AdminUserProfile> userProfiles = null;
    try (final Connection connection = pmf.getConnection()) {
      checkPermission(connection);
      userProfiles = UserRepository.getUserProfiles(connection, 0, 0, filter);

      FilterRepository.insertOrUpdateFilter(connection, filter, UserService.getCurrentUserProfileStrict(connection));
    } catch (final SQLException e) {
      handleSQLException(e, "retrieving users");
    }

    return userProfiles;
  }

  @Override
  public void resetPassword(final AdminUserProfile profile) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkPermission(connection);
    } catch (final SQLException e) {
      handleSQLException(e, "reset password");
    }
    // TODO This.
  }

  @Override
  public AdminUserProfile createUser(final AdminUserProfile profile) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkPermission(connection);
      final int id = UserRepository.createUser(connection, profile, PasswordUtil.createPassword());

      profile.setId(id);

      return profile;
    } catch (final SQLException e) {
      handleSQLException(e, "creating user: " + profile.getName());
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public void removeUser(final AdminUserProfile profile) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkPermission(connection);
      checkEdit(connection, profile);
      UserRepository.deleteUser(connection, profile);
    } catch (final SQLException e) {
      handleSQLException(e, "removing user: " + profile.getName());
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private void checkPermission(final Connection connection) throws AeriusException {
    if (!UserService.getCurrentUserProfileStrict(connection).hasPermission(administratorPermission)) {
      throw new AuthorizationException();
    }
  }

  private void checkEdit(final Connection connection, final UserProfile userProfile) throws AeriusException {
    if (UserService.getCurrentUserProfileStrict(connection).getId() == userProfile.getId()) {
      throw new AuthorizationException();
    }
  }

  private void handleSQLException(final SQLException e, final String action) throws AeriusException {
    LOG.error("Error while {}", action, e);
    throw new AeriusException(Reason.SQL_ERROR);
  }

  @Override
  public AdminUserRole createRole(final AdminUserRole role) throws AeriusException {
    return null;
  }

  @Override
  public void updateRole(final AdminUserRole role) throws AeriusException {
    try (final Connection connection = pmf.getConnection()) {
      checkPermission(connection);
      UserRepository.updateRole(connection, role);
    } catch (final SQLException e) {
      handleSQLException(e, "updating role " + role);
    }
  }
}
