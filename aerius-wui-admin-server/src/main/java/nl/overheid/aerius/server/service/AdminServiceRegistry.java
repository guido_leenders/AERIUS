/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.server.service;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.ServiceURLConstants;
import nl.overheid.aerius.shared.domain.auth.AdminPermission;

/**
 * ServiceRegistry implementation for Admin.
 */
public class AdminServiceRegistry extends AbstractServiceRegistry {
  /**
   * Creates a new instance, initializing all admin services.
   * @param session
   */
  public AdminServiceRegistry(final PMF pmf, final AdminSession session) {
    super(pmf, session);

    addService(ServiceURLConstants.ADMIN_SERVICE_RELATIVE_PATH, new AdminServiceImpl(AdminPermission.ADMINISTER_USERS, pmf));
  }
}
