BEGIN; SELECT setup.ae_load_table('sectors', '{data_folder}/public/sectors_20160617.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('gcn_sectors', '{data_folder}/public/gcn_sectors_20190222.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('sectors_main_gcn_sector', '{data_folder}/public/sectors_main_gcn_sector_20190314.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('emission_diurnal_variations', '{data_folder}/public/emission_diurnal_variations_20170119.txt'); COMMIT;

BEGIN; SELECT setup.ae_load_table('sector_default_source_characteristics', '{data_folder}/public/sector_default_source_characteristics_20190314.txt'); COMMIT;
BEGIN; SELECT setup.ae_load_table('gcn_sector_source_characteristics', '{data_folder}/public/gcn_sector_source_characteristics_20190222.txt'); COMMIT;
