# Intercepting tasks on the queue

Most activities for various AERIUS tasks are handled via the message bus.
On some occasions this way of handling tasks can come to a (near) halt.
The two common issues causing this are listed below.

### Long running tasks
It can happen that a tasks keeps running forever, or at least it seems like it
is when it's running for days. This could be due to configuration settings or a
bug. In such a case it can be necessary to stop the task and or remove it or
run it in a more controlled way.

A worker may take a long while to finish a certain task. If this happens
multiple times these workers will stay busy and cannot handle any new tasks
causing DDOS like behaviour, as the system will get more new tasks over a time
period than it can handle in the same time frame. This is mostly caused by bugs
causing a worker to go into a infinite loop, trigger locking issues or simply
code that doesn't scale well for big jobs. This could also be caused by various
configuration issues, i.e. having a worker of type A that makes use of workers
of type B, but there aren't any of those available forcing the worker to wait
indefinitely. In these cases it could prove useful to intercept this task to
debug what the problem is and if needed remove or even for important messages
temporarily move (redirect) it to another queue to be put back after the bug
is fixed.

### Worker exhaustion (The sneaky little bug that could)

The workers are written with the notion that environmental issues shouldn't cause
a task to fail. Environmental issues can vary from not having enough disk space,
to not having enough memory or even OS-specific problems. If processing a
specific task causes an exception we don't expect to be thrown, the worker will
shut itself down. The task itself will be put back on the queue as it wasn't
processed fully so any other available worker can pick it up, minimizing
environmental issues causing some tasks to be randomly failing.
A side effect of this approach is that a genuine bug in the code could be
seen as a environmental issue and cause the worker to shut itself down.
But as it is a genuine bug all workers will try processing the problematic task one by
one until there are no workers left - exhausting them all and making the
system unresponsive to new tasks.
While in theory it's a good thing that the system doesn't allow such bugs to
potentially corrupt anything important - the other side is that the system will be
unusable. In these cases it could prove useful to intercept this task to debug what
the problem is and if needed remove or even for important messages temporarily
move (redirect) it to another queue to be put back after the bug is fixed.

## The intercept command line tool

To manage the queue there is a command line tool available. This tool is
present in each worker and can be run by starting the java jar with the
appropriate command line options. The following options are available:

`-interceptSave <queueName> <path>`
This command saves the task as json at the tip of the queue on the given path.

`-interceptRemove <queueName> <path>`
This command removes the task at the tip of the queue and saves it on the given
path. If the queueName starts with `intercept.` the queue is also removed.
This command also will send an AeriusException with internal error reason back
to the process that made the call to insure proper handling of the case. This
will mean the calling process will stop with a failure.
This command can also be used to remove an empty intercept queue created by the
redirect command. 

`-interceptRedirect <inQueueName> <outQueueName>`
This command moves the task at the top of the inQueueName and places it in the
outQueueName. The outQueueName is always prefixed with `intercept.`. If that
queue doesn't exist it is created.

`-interceptRun <queueName> <worker type>`
This command executes the task at the tip of the queue. It must be specified
the type of work that must be performed. The worker type corresponds with the
available types of workers, e.g. calculator.
If an RuntimeException or AeriusException occurs during the run this is
converted to a IOException. This will cause the command to shutdown and put
the task on the queue again. If such an exception would occur in a worker
the task would be canceled and an error message is send to the sender.
Depending on the worker type all configuration normally configured for the
worker must also be present in the passed config properties file.

All above commands also require the standard config properties file and log
arguments.    

## Workflow

When the intercept tool is to be used. Use the following workflow.

First when a problem task is still running see if it's possible to wait till
all other tasks on the queue can be handled. Sometimes this requires to wait a
bit till other tasks are handled. Then if not all tasks are already killed,
stop all workers that can handle the task and or run the task. This will put
the task back on the queue. This is most likely a task on the worker queue,
but if it were a task not yet handled by the taskmanager, the taskmanager
should be stopped.

Now we can save the task with the `-interceptSave` command. This way we can
inspect the task and see if something is askew or if we have the task we're
looking for.

Then it's probably best to redirect the task to a different queue with the
`-interceptRedirect` command. When the task is moved to a different queue
it would be possible to restart all the workers so normal operation continues.
Depending on how busy the system is this can be the preferred action. Always
use only one queue per redirected task.

With the task on a different queue it is possible to run this tasks with the
command `-interceptRun`. This way we can run the tasks in a more controlled
way. It would even be possible to add profilers or debuggers. Or one could
simply let the task run in this separate process, while the system operates
normally.

If the task remains on the queue because it can't be handled the task should be
removed with the `-interceptRemove` command. This will make sure the calling
process will handle it correctly. In case of a redirected task this will also
clear the redirected queue.
 
  