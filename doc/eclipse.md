# Working with Eclipse
 
## Introduction

AERIUS is a Java project. The used development environment is Eclipse. To work
effectively with Eclipse several configuration files can be found in the map 
[aerius-tools/src/main/resources](/aerius-tools/src/main/resources). It is
recommended to first configure Eclipse with these files.

## Importing the AERIUS project in Eclipse.

AERIUS is build with maven. To get the AERIUS project into Eclipse it can 
simply be imported with the maven import. 

Make sure to import the project with the profile `aerius-ide`.

When the projects are imported Eclipse starts building them automatically.
However, in the `aerius-common` and `aerius-shared` project
additional source files are generated. Unfortunately Eclipse doesn't correctly
added these new source paths to the build path. So you have fix this manually.
To fix: Right click `aerius-shared`, `Build Path` > `Configure Build Path`, 
scroll to `target/generated-sources/gwt,` select `Excluded: **` and click 
button `Remove`. Now it says `Excluded: (None)`. This process should be
repeated in a similar way for `aerius-common`. 

## Debugging AERIUS GWT web application
To run and debug the AERIUS web application 2 applications must be started. The
GWT Super Dev Mode (SDM) and the Tomcat Server. The SDM dynamically compiles the
Java code to JavaScript for debugging. The following steps must be done to 
configure the applications.

First, create a SDM Code Server launch configuration:

1. Create a new Java Run Configuration for aerius-[*product*]-client
2. In [*Program Arguments*] add the source directories this code server must use 
to compile sources. We'll use the `-src` flag for this:

> -src "${workspace_loc}\aerius-wui-[*product*]-client\src\main\java"
> -src "${workspace_loc}\aerius-wui-client\src\main\java"
> -src "${workspace_loc}\aerius-wui-scenario-base-client\src\main\java"

For admin modules (Register) ad:
> -src "${workspace_loc}\aerius-wui-admin-client\src\main\java"

Furthermore add the Module to compile: `nl.overheid.aerius.wui.[*prodcut*]Debug`

3. Find your GWT SDK in Eclipse Plugin directory, or download it.
4. Under classpath add `gwt-codeserver.jar` as an external JAR dependency.
5. Under main, insert `com.google.gwt.dev.codeserver.CodeServer` to run as a Main class
6. Run the codeserver and keep it running for the duration of the debugging session. 

Second, Tomcat Server config must be created.

1. In the `Servers` view in Eclipse add a new server.
2. Add the specific AERIUS server `aerius-wui-[*product*]-server` product you want to ebug.
3. Go to the `Debug configuration` and open the properties of the newly created launch configuration
4. Under Arguments / VM Arguments, add `-Dgwt.codeserver.port=9876`, or whichever SDM code server port you're running at.
5. Tomcat also requires a `context.xml`. By default this file is not present
in the AERIUS projects, but must be manually created. In the server `webapps` directory
create a `META-INF` directory.
6. In this directory create a `context.xml`. In 
[the aerius-wui-server/src/main/config/context.xml](/aerius-wui-server/src/main/config/context.xml)
an template of this file can be found.

By starting both launch configurations you are ready to run the web application.
Start them and go to `localhost:8080/[*product*]` in your browser. The 
page should load and showing it's copmpiling.
Bonus, you're ready to start debugging GWT in SDM, but to finish up you could do this:

Oh, source maps! They're likely already configured correctly for you it's a 
default config now, but here's how to do it in Chrome (v38 ):

1. F12
2. Hit the settings button in the top right corner (older versions = bottom right corner)
3. Under sources, enable 'Enable JavaScript source maps' (in older versions, this would be under 'Scripts')

### How to fix if debug doesn't seem to work

#### You may be getting a blank page when you test the web application in the browser

Make sure the server is running. A blank page indicates the server files are 
not correctly deployed to the server, or the server crashed during startup.

Make sure you have your database setup correctly.

Check the error log.

##### The splash screen indicates an error has occurred.

This occurs most often due to failed serialization of a domain object (Contexts 
most likely). It happens when domain objects have been changed and the server 
still has old files, or the first time the server starts up after a clean 
without client code deployed. 

- Refresh common/shared
- Refresh wui-server
- Refresh wui-[*product*]-server.

At this point the server dialog should indicate [*started, restart*] or 
[*stopped, republish*]. If so, restart/start the server.

If not, you need to either keep refreshing until it does, or you're dealing 
with some other problem not commonly encountered.

##### No matter what I do, I still get a Serialization error (in the Context)

Compare your SDM Run Configurations and also your (Apache Tomcat) Server Run 
Configurations. For example I lacked a parameter in one of the latter which was 
the reason for the Serialization error. It was: `-Dgwt.codeserver.port=9876`

See [aerius-tools/src/main/resources/eclipse-workspace](/aerius-tools/src/main/resources/eclipse-workspace) for some
 sample run configurations (check paths).

Other (futile) things to try:
- Remove all modules from server, Clean Tomcat Work Directory, Clean, re-add correct module to server.
- Clear browser cache (files).
- Delete as much as you can from the workDir that SDM tells you when it starts up 
(`%TEMP%`) especially all those g`wt-codeserver-\*` folders. 

##### Something to do with Ginjector

This error is utterly absurd, it shows up out of nowhere, for no reason, and can ruin an entire day and your mood. They're not unlike parking tickets in this sense. It will appear as though you caused it by a code change, but nothing could be further from the truth: you are not at fault, the error is only trying to trick you into thinking that you are like the deceiving little pest it is. To solve it you need to do a full maven rebuild of the project, refresh everything (shared first, then clients, then servers), clean the webserver, redeploy to webserver, restart the code server and webserver, pray to jesus and all his apostles, start the application, and all should be well.

##### In general

Make sure the server is fresh and up-to-date. The server is not up to date when it indicates you need to restart:

> [*Started, Restart*] or [*Stopped, Republish*]

After refreshing (web application) projects, changes are compiled into target dir(s), the server listens for changes on this dir and whenever it encounters something that cannot be dynamically reloaded, it will indicate you need to issue a cold restart using the above indicators. Usually, only servlets and classes that are _not_ Serializable can be  dynamically reloaded.

Note this: Server projects (eg. wui-server) depend on projects like -common and -shared. Refreshing those last 2 does not automatically cause the server (EE container) at which the server _project_ is deployed to refresh/restart. Only after explicitly refreshing the server project will it receive the changed files in its /webapps (or similar) directory. Because of this, the order in which you refresh projects also matters.

When in doubt, refresh everything 3+ times and you should be fine. ;)

