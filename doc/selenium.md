# Selenium Tests


## Prerequisites
These are the minimum requirements for running the selenium tests in this repository:
- Java IDE (Eclipse)
- Recent Chrome (driver included in repository)
- Test Environment

An optional but highly recommended set of [`Natural`](http://marketplace.eclipse.org/content/natural) plugins is available from the Eclipse marketplace. This provides Cucumber syntax highlighting which helps writing well formatted feature files.

The [pom.xml](/aerius-wui-test/pom.xml) should take care of retrieving essential libraries for the project like Selenium, Cucumber and JUnit. Working with an editor lacking pom support would require these libraries to be installed manually, which is beyond the scope of this document.


## Basic Overview
All selenium related files are stored in the test root folder [aerius-wui-test](/aerius-wui-test). From here all relevant test and support classes can be found in [test/src/test/java/nl/overheid/aerius/wui/cucumber](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber) with feature and resource files residing at [test/src/test/resources](/aerius-wui-test/src/test/resources).

#### Test and Support Classes
Please note that product specific classes are organized in their own separate folder. Containing at least one runnable test script following naming convention `<product>CucumberTest.java` and a `stepdef` sub folder with corresponding step definitions. 
The only exceptions to this rule are the [shared](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/shared) and [base](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/base) folders, which contain more generic classes and/or step definitions that are (re-)used across all products alike (ie: notification panel, search box etc.).

#### Feature and Resource Files
The actual test scenario's and test data are organized in a similar way and mostly self explanatory content wise. One important thing to mention here is that import file locations are determined based on file extension. Just make sure any new  test files are added to the appropriate (sub) folder.


## Configuration
There are three possible ways of setting up test configuration options. In order of lowest to highest precedence; property files, command line parameters and the selenium driver wrapper.

#### Property Files
The default [selenium_base.properties](/aerius-wui-test/src/test/resources/nl/overheid/aerius/wui/cucumber/base/selenium_base.properties) in this repository contains the (mostly documented) option values including file paths, wait polling, timeouts etc. Specific options requiring different values in a Linux environment are automatically overridden with values from the additional [selenium_linux.properties](/aerius-wui-test/src/test/resources/nl/overheid/aerius/wui/cucumber/base/selenium_linux.properties).

#### Command Line
This is the preferred method of passing in sensitive data like a base URL containing user credentials for basic authentication: `-Dcalculator.app.path=https://<user>:<pass>@calculator.aerius.nl/calculator/?locale=nl`

Or for providing the path to the property file containing the user credentials for register user roles, which could differ between test environments: `-Dregister.users=/home/test/register-acceptance.users.properties`

#### Driver Wrapper
The [SeleniumDriverWrapper](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/base/SeleniumDriverWrapper.java) class is primarily meant to take care of instantiating WebDriver instances. It contains all recognized configuration options and convenience methods to work with these values on runtime, as well as overriding them. Despite not currently being in use as such, it's good to have this on record in case the need arises in the future. 


## Test Classes, Tags and Features
The complete set of automated UI tests is usually scheduled to run after a nightly build completes and executed using a single maven command from jenkins. However, this a bit too much for developing and maintaining specific tests. So, each product's test set can also be executed individually (conveniently from within eclipse) by running one of the following test classes as a JUnit test:

#### Classes
These classes extend [CucumberBase](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/base/CucumberBase.java) class to take care of starting up and shutting down selenium driver instances:
* [Calculator](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/calculator/CalculatorCucumberTest.java)
* [Register](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/register/RegisterCucumberTest.java)
* [Scenario](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/scenario/ScenarioCucumberTest.java)
* ~~[Monitor](/aerius-wui-test/src/test/java/nl/overheid/aerius/wui/cucumber/monitor/MonitorCucumberTest.java)~~

#### Tags
By default all of the product specific scripts mentioned above are set up to run the complete set of tests identified by the `@nightly` tag. Some of these scripts also contain a list of relevant tags to facilitate more fine grained testing of specific features like `@comparison`, `@shipping` and `@import`. If so required test execution can be limited to a single test scenario by applying a unique tag to a specific scenario in the cucumber feature file.

#### Features
All test scenario's are organized per product and specified in Cucumber feature files using the [Gherkin language](https://cucumber.io/docs/gherkin). The optional Cucumber plugin enables comfortable editing of these files in Eclipse and helps spotting any blatant (grammar) mistakes before running the test.

## Reports
Alongside the default HTML report the test runners are set up to generate JSON files with test results. The HTML reports are primarily useful for running tests locally and capture screenshots in case a test fails. The JSON files are used to gather the same results per product and feed the [Cucumber Reports](https://plugins.jenkins.io/cucumber-reports) Jenkins plugin. These JSON files contain the same test results (including screenshots) but is more powerful since Jenkins can keeps track of test results over time which allows for trend analysis.
