# Hot fix branch

This document outlines a way to set up and manage a branch tracking upstream/master rather than origin/master, which you can use to quickly push hotfixes into upstream if the need may arise. This is an optional sandboxed branch for upstream - in most cases, working out of master will suffice and is much simpler. See bottom of this document for details.

## Set-up the upstream

1) Create a branch tracking remote

````
git checkout -b remotemaster
git branch -u upstream/master remotemaster
````

Done.

You can do you your thing and push changes/hot fixes here as usual.

The same thing applies to all other remote branches, simply s/master/[any-other-branch].

## Cherry picking hot fixes out of feature branch [using a dedicated remotemaster branch]

Sometimes you'll already have created a fix in a feature you're working on locally. Rather than push the entire feature just to fix upstream/master, it's generally better to cherry-pick the hot fix out of your feature branch and push that into upstream/master. To do this, follow this general outline:

1) Find and copy your hotfix's commit hash into the clipboard, use any of:

````
gitk
git log --pretty=oneline
[your favorite git history explorer]
````

2) Stash all changes in your feature branch

````
git stash -u
````

(This isn't really required, but stashing away any untracked is something I find will remove any complications)

3) Checkout the branch that's tracking upstream/master (or any upstream branch, see set-up outline)

````
git checkout remotemaster
````

4) Cherry-pick the hotfix

````
git cherry-pick [commit_hash]
````

5) Push

````
git pull --rebase
git push upstream HEAD:master
````

It's not entirely like normal because you have to explicitly specify where to push to.

6) Go back to your feature, and apply the stash (containing untracked changes)

````
git checkout [feature]
git stash apply
````

## Pushing hot fixes out of origin/master

If you've just committed a hot fix in  your origin/master branch and you'd like to push that to upstream/master, and that's all you've done (no features, and such), which should be the case if you're developing features out of a feature branch rather than master. Then you can just 

````
[optional]
git fetch upstream
git rebase upstream/master
[/optional]
git push upstream HEAD:master
````

and be done with it.
