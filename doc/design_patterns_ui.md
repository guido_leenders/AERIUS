# UI Design Patterns and goals

This document describes various (loosely enforced) design patterns and goals that are being employed in all UI projects.

## Satellite overview

We're using GWT's Activity pattern to handle history and places in web applications.

We're using GIN dependency injection.

We're using event binders and event handlers to communicate across goal-agnostic components.
 
We're using UI binders to draw/render components.

We're using Context/UserContext/AppContext to retain application state.

We're using the Editor framework to safely manipulate objects.

## GIN dependency injection

GIN stands for GWT INjection. It's like Guice, except for GWT.

Pretty simple.

Use @Inject for constructors and fields, use @Singleton for singletons. What you'd expect basically.

Have a look at a ClientModule (ie. [CalculatorClientModule](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/calculator/CalculatorClientModule.java) to see how mappings between interfaces and concrete implementations work.

## Context/UserContext/AppContext

We're using various contexts to store application state. These interfaces kind of evolved in a way that isn't exactly what you'd expect anymore, but it's easy to get used to. This will probably be refactored in the short-to-medium term to follow the intended design more closely. Of note: the intention is logical, the implementation, however, has diverged.

So.

Context should contain general application configuration and state. This is independent of a user. Think settings, constants and layers.
UserContext should contain user specific or user-related local application state. Think user-defined scenario's, stored filters, stored layers, permissions.
AppContext should contain application-specific configuration and state that are independent of user, and are used internally. Think global singletons, service interfaces and agents.

The Context and UserContext objects are retrieved from the server - the AppContext is constructed and maintained locally.

AppContext is the newest to the bunch - we discovered we started abusing (primarily) UserContext to retain application-specific state, so we fashioned AppContext out of dust to alleviate that. 

All of these contexts have their application-specific extensions. Here's the latest:

- Context
  - AERIUS2Context
    - ScenarioBaseContext
      - ScenarioContext
      - CalculatorContext
    - AdminContext
      - RegisterContext
  - MeldingContext
  - MonitorContext
  
- UserContext
  - ScenarioBaseUserContext
    - ScenarioUserContext
    - CalculatorUserContext
  - AdminUserContext
    - RegisterUserContext
  - MeldingUserContext
  - MonitorUserContext
  
- AppContext
  - ScenarioBaseAppContext<C extends ScenarioBaseContext, UC extends ScenarioBaseUserContext>
    - ScenarioAppContext
    - CalculatorAppContext
  - RegisterAppContext
  
Ideally, we want the application to retrieve both the UserContext and Context out of the AppContext wherever possible. You can inject the UserContext and Context objects as you'd expect, but we'd like to move away from that. Inject AppContext instead, and fetch the relevant context using its getters. This way AppContext, rather than the injection framework, has control over which context to pass (even though this is just a getter).

## Activities and places

We've adopted the Activity/Places pattern throughout the application to keep track of history and application location state. This allows us to navigate back and forth through places, without leaving the web application - and still retain the browser's native history support.

To read up on this pattern, see this:

http://www.gwtproject.org/doc/latest/DevGuideMvpActivitiesAndPlaces.html

It's pretty straightforward. In AERIUS, presenters are typically an implementation of Activity (AbstractActivity usually). These are instantiated through an ActivityFactory in the ActivityMapper (ie. CalculatorActivityMapper) when the Place associated with the activity is 'activated'. This is a GIN factory, so use @Inject to get whatever you need in the Presenter. Place navigation happens via the PlaceController.

Navigation via the PlaceController should ideally happen only through Presenters; Views shouldn't have any knowledge of the application's navigational topology - only its bordered off responsibilities of displaying components. 

## MVP - Model View Presenter

In general, we try to follow the MVP design directive. In a sentence, this means there's a Model that contains some data, a Presenter that knows what to do with that data, and a View that knows how to display it.

Activities and Presenters are typically one and the same. Views are UiBinders. Models are POJOs.

## EventBus and EventBinders

The display components / Views part of the MVP design paradigm follow a top-down hierarchy. Something like this is possible and logical from a display perspective:

Main
  - Menu
    - Ton of buttons
  - Form
    - Ton of inputs
  - Main information panel
    - Update information button
  - Notification center
  - Map

From a communication perspective, this is terrible. For example, if you want to update the main information displayed on the page, let's say you would click the 'update information button'. You would, somehow, spin up a fetching agent that retrieves new information and updates the panel. When this new information comes in, you want to display a notification in the notification center. In the above model you would need to have a dependency to the notification panel in the main information panel. Yuck.

It would be great if the main information panel could just say "Hey, everyone, I've just updated this here information. Do with that whatever you want.", the notification center could listen to something saying that, and show an appropriate notification. Neither knows about the other - they have their own responsibilities and don't much care who or what told them to do something.

This is what the EventBus model brings to the table. It allows application-wide and component-agnostic communication. It breaks dependencies that would otherwise exist, and it allows us to uncouple independent components from one another, making for a much more maintainable application.

Use an Event (GenericEvent), an EventBinder and the EventBus whenever some random component needs to communicate to some other random component (or, really, nothing in particular) it doesn't *need* to know about.

For each *Activity*, an EventBus is passed into the start(panel, eventBus) method. This is a ResettableEventBus and will be cleaned out when the Activity ends. Use it for attaching disposable event handlers.

If you're injecting an EventBus in the constructor, it's the application-global event bus. You would typically only use this in these 2 cases:

- Firing events (it doesn't matter which one you take)
- Listening to events for components that are always available.

For Views part of a Presenter, always use the EventBus made available to you in the Activity's start() method, rather than the one you fetch from the constructor (except if you're only firing events)

We're using this library to do our binding:

https://github.com/google/gwteventbinder

Some sample code:

The sender:

````java
public class SomePresenterThing {

  @Inject
  public SomePresenterThing(final EventBus eventBus) {
    button.addClickHandler(final ClickEvent e) {
      eventBus.fireEvent(new NotificationEvent("We all live in a yellow submarine"));
    }
  }
}

````

The handler:

````java
public class SomeTopLevelViewImpl {
  interface SomeTopLevelViewImplEventBinder extends EventBinder<SomeTopLevelViewImpl> {}

  private static final SomeTopLevelViewImplEventBinder EVENT_BINDER = GWT.create(SomeTopLevelViewImplEventBinder.class);
    
  @Inject
  public SomeViewImplEventBinder(final EventBus eventBus) {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
  }
  
  @EventHandler
  public onNotificationEvent(final NotificationEvent e) {
    displayNotification(e.getMessage());
  }
  
}
````

## UiBinders

UI Binders are GWT's way to model view components without touching any code. They're like JSP, but not.

This write-up explains it better than I ever can:

http://www.gwtproject.org/doc/latest/DevGuideUiBinder.html

## Editor Framework

The Editor Framework allows us to, put simply, throw an object into a View component, map its values to inputs, and fetch an edited object out of it when we want it.

It removes a shitton of plumbing code, simplifies the application topology (if used efficiently), makes for easier testing, and is less vulnerable to bugs.

It also has built-in support for validation (well - AERIUS added validated value boxes), which allows us to define arbitrary conditionals that an input must abide by.

It might be a bit confusing to first get familiar with, but it makes everyone's lives - specifically a maintainer that delves into your code as a result of the necessity to fix an unavoidable bug due to your not using the Editor framework - easier.

Here's the GWT Project guide to the Editor Framework:

http://www.gwtproject.org/doc/latest/DevGuideUiEditors.html

## Validation

Validation is loosely based on javax validation. Javax validation uses annotations, which are hard to internationalize and are not context sensitive. Meaning it's hard to bind an error to
the widget that contains the error. All AERIUS validations extend the [Validator](/aerius-wui-client/src/main/java/nl/overheid/aerius/wui/main/ui/validation/Validator.java) interface. Validation is used in combination with the GWT Editor. In the flush methods
the validator is called and records errors with the editor.

Error reporting to the user is done via the [ErrorPopupController](/aerius-wui-client/src/main/java/nl/overheid/aerius/wui/main/widget/validation/ErrorPopupController.java) This class shows a popup with an error messages on only one widget at a time.

## Tooltip and context specific help

The application has an option to show context sensitive help and provide context sensitive
links to a manual page with specific information.

The context sensitive information works by extending the interface [HelpToolTipInfo](/aerius-wui-client/src/main/java/nl/overheid/aerius/shared/i18n/HelpToolTipInfo.java). 
Via the [HelpPopupController](/aerius-wui-client/src/main/java/nl/overheid/aerius/wui/main/widget/HelpPopupController.java) tool tips are bound to widgets.

## Flexbox
UI development using CSS3 Flexboxes has been described here:

[flexbox.md](flexbox.md)

