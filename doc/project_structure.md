# Project structure

The project structure describes the content and relations of the specific
projects. Below is a graphical view of the dependencies of the projects.
![project structure](aerius_project_structure.png)

Here is a descriptions of each individual project:

### [aerius-common](/aerius-common)
A project with server and worker common functionality.

### [aerius-connect-api](/aerius-connect-api)
The api interfaces of the connect server. This project can be used to create
a java client for connect.

### [aerius-connect-server](/aerius-connect-server)
The connect server.

### [aerius-core-calculation](/aerius-core-calculation)
The library handling AERIUS calculations.

### [aerius-core-repository](/aerius-core-repository)
The library handling wrapping the database.
 
### [aerius-database](/aerius-database)
The database sql structure, as well as the default loaded data and configuration
of the statically loaded data.

### [aerius-geoserver](/aerius-geoserver)
The AERIUS geoserver configuration. This project contains only the data
structure. But the maven build creates a war file containing a complete
geoserver. Thus data and geoserver. This war file can simply be deployed. 

### [aerius-shared](/aerius-shared)
The library containing all data objects that are both shared between the
wui-client projects and all the worker, server projects. The files in this
class should comply with the limits imposed by GWT for serializing files.
It also can't contain references to java code that can't be compiled to
JavaScript.

### [aerius-taskmanager](/aerius-taskmanager)
The AERIUS taskmanager.

### [aerius-taskmanager-client](/aerius-taskmanager-client)
The AERIUS taskmanager api. Used by applications that need to send tasks to
the queue, or read tasks from the queue.

### [aerius-tasks](/aerius-tasks)
The main worker project. Contains the the database related workers, and the
worker main.

### [aerius-tools](/aerius-tools)
All configuration files for the development environment and scripts used on
the severs.

### [aerius-worker-base](/aerius-worker-base)
The base class for all workers. Contains generic code for running a worker.

### [aerius-worker-ops](/aerius-worker-ops)
The AERIUS OPS worker that wraps the OPS executable. Handles creating OPS input
files, running OPS and reading the OPS output results. The worker listens to the
queue for new tasks, and sends the results back via the reply queue.

### [aerius-worker-srm](/aerius-worker-srm)
The AERIUS SRM2 worker that implements the SRM2 model. This worker runs on OpenCL
and requires the OpenCL environment to be installed. The worker project also
contains the Pascal application that generates the preprocessed data from the
PreSRM library.

### [aerius-wui-admin-client](/aerius-wui-admin-client)
The client side of the user interface of the all administrative activities.
This contains user and role management. The admin module is referenced from
the client side applications having administrative tasks (e.g. register).
It's a compile time binding, meaning it doesn't run a separate application.
 
### [aerius-wui-admin-server](/aerius-wui-admin-server)
The server side, the servlets for the admin module.

### [aerius-wui-calculator-client](/aerius-wui-calculator-client)
The client side of the user interface of AERIUS Calculator.

### [aerius-wui-calculator-server](/aerius-wui-calculator-server)
The server side of the user interface of AERIUS Calculator.

### [aerius-wui-client](/aerius-wui-client)
The client side shared code project containing all generic and shared user 
interface code.

### [aerius-wui-melding-client](/aerius-wui-melding-client)
The client side of the user interface of AERIUS Melding.

### [aerius-wui-melding-server](/aerius-wui-melding-server)
The server side of the user interface of AERIUS Melding.

### [aerius-wui-register-client](/aerius-wui-register-client)
The client side of the user interface of AERIUS Register.

### [aerius-wui-register-server](/aerius-wui-register-server)
The server side of the user interface of AERIUS Register.

### [aerius-wui-scenario-base-client](/aerius-wui-scenario-base-client)
The client side shared user interface code project for the Calculator and 
Scenario products.

### [aerius-wui-scenario-base-server](/aerius-wui-scenario-base-server)
The server side shared user interface code project for the Calculator and 
Scenario products.

### [aerius-wui-scenario-client](/aerius-wui-scenario-client)
The client side of the user interface of AERIUS Calculator.

### [aerius-wui-scenario-server](/aerius-wui-scenario-server)
The server side of the user interface of AERIUS Calculator.

### [aerius-wui-server](/aerius-wui-server)
The server side shared code project containing all generic and shared user 
interface code.

### [aerius-wui-test](/aerius-wui-test)
The project containing all Selenium/Cucumber tests and generation of the images
for the manual. 
