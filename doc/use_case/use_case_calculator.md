# Use Cases Calculator
This page describes the main use cases for the Calculator product.
The use cases give a global overview of what happens internally. 
For specific details see the source code.


## Context

When the web application is started quite some information is retrieved from the server in a Context object. 
This is done through [ContextRepository](/aerius-core-repository/src/main/java/nl/overheid/aerius/db/common/ContextRepository.java). 
This object contains, for instance, the sector category data of all sectors. 
This can be seen as a static information required for the application to work, and shouldn't change within the client.
Any user specific information should be kept in a UserContext object. 


## Editing an emission source
A main feature is adding, removing, editing and copying emission sources.
Editing is technically a 3 phase step process, while the user walks through 2 pages.

Page 1: [LocationPresenter](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/calculator/ui/LocationPresenter.java)

1. The user selects the location on the map or enters the location manually.

Page 2: [SourceDetailPresenter](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/calculator/ui/SourceDetailPresenter.java)

2. The user selects the sector. 
At that point the specific domain object is created and location data is copied into the object. 
This is all done in [EmissionSourceEditorVisitor](/aerius-wui-scenario-base-client/src/main/java/nl/overheid/aerius/wui/scenario/base/source/EmissionSourceEditorVisitor.java).
Via [SectorChanger](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/source/SectorChanger.java) the specific [EmissionSource](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/source/EmissionSource.java) object will be created.
This is done based on the [EmissionCalculationMethod](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/sector/EmissionCalculationMethod.java) configured in the database in [SectorProperties](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/sector/SectorProperties.java).
If the user selects another sector all sector specific data is reset.

3. Based on the actual EmissionSource object and possibly the children of that object a specific editor is loaded. 
The user can edit the content. 
When the user saves, he will be redirected to the overview page [CalculatorEmissionSourcesPresenter](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/calculator/ui/CalculatorEmissionSourcesPresenter.java). 
The source is then stored in the [EmissionSourceList](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/source/EmissionSourceList.java), which represents a situation.


## Calculation

When the user starts a calculation a [CalculatedScenario](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/calculation/CalculatedScenario.java) object is created. 
This is done with [ScenarioUtil](/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/calculation/CalculatedScenarioUtil.java)

This contains all sources and calculation options. 
It also calculates a hash for the state of the sources at that point. 
If the user would change the sources during or after calculation this hash won't match and the calculation results will be invalidated.

The CalculatedScenario object is send to the server [CalculateServlet](/aerius-wui-scenario-base-server/src/main/java/nl/overheid/aerius/server/servlet/CalculateServlet.java) 
and the web application starts polling for initialization results. 
In the servlet the job is tracked with [ServerCalculationTracker](/aerius-wui-scenario-base-server/src/main/java/nl/overheid/aerius/server/servlet/ServerCalculationTracker.java).
At the server the CalculatedScenario object is send to the queue *CALCULATOR.CALCULATOR_UI*

The taskmanager will ensure the task is retrieved from this queue and send to the workers when there is enough capacity (through another queue).
This happens for all tasks send to the workers: each task goes through the taskmanager first.

The calculator worker will first initialize the calculation(s) needed in the database and send the calculation ID(s) back to the web server. 
The web client will then (via initial polling) get the calculation id and will then start polling for actual calculation results.

The calculation worker after initialization will start the [Calculator](/aerius-core-calculation/src/main/java/nl/overheid/aerius/calculation/Calculator.java). 
The calculator will chunk the calculation in sets of all sources by sector with a number of receptors to calculate.
These sets or chunks are send to the worker that will do the actual calculation. 
The calculator can have several chunks going in parallel.
When results arrive from these chunks they are stored in the database and are send back via the queue to the webserver. 
The webclient will then pick up these results grouped by kilometer block [CalculationResultUtil](/aerius-core-calculation/src/main/java/nl/overheid/aerius/calculation/CalculationResultUtil.java) via polling.

The calculator waits for all results to be in, and will send a final signal back when everything is done.
When this signal arrives, the web client will request the webserver to provide the summary of the results. 
The webserver queries this summary from the database.

When a new calculation is started the previous calculation is deleted from the database.


## Exporting a file

When the user requests a export the web client sends all required information like sources and calculation options to the webserver. 
The webserver [ExportServlet](/aerius-wui-calculator-server/src/main/java/nl/overheid/aerius/server/servlet/ExportServlet.java) passes these values in a task to the proper export queue and starts polling for a result.

The correct worker, based on the export task, will construct a file or document.
When this file or document is created it will be stored in a location that is also accessible via the webserver.

Finally an e-mail containing the URL to the file is send to the user. 

The url is send back to the webserver as well. 
The web client retrieves the url (or error) via polling and gives this information to the user via the notification panel.

### Exporting a PDF

The task data is contained in [PAAExportData] (/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/export/PAAExportData.java).
These tasks are send to the *PAA_EXPORT* queue.

The task will be handled by the [PAAWorker] (/aerius-tasks/src/main/java/nl/overheid/aerius/paa/PAAWorker.java). 
This worker will create an IMAER GML of the sources for each situation. 
Then it will proceed to start a calculation, similar to the way CalculationWorker does. 
When the calculation is finished the PDF document will be created.
The IMAER GML(s) will be added to this PDF as meta data so it can be imported to recreate the situation when needed. 

### Exporting a GML

The task data is contained in [OPSExportData] (/aerius-shared/src/main/java/nl/overheid/aerius/shared/domain/export/OPSExportData.java).
These tasks are send to the  *GIS_** queue.

The worker will create an IMAER GML of the sources.
If it should be a GML with results, these results will be queried from the database and added to the IMAER GML. 
When the GML document is successfully created it will be stored in a location that is also accessible via the webserver.


## Mailing the user

When a mail has to be send to the user, a specific task can be created and send to the queue *MESSAGE.MESSAGE*.
This is done by collecting relevant data, like the url to a document or a reference, and using [MessageTaskClient] (/aerius-common/src/main/java/nl/overheid/aerius/mail/MessageTaskClient.java).
This will ensure the data is send to the [MailMessageWorker](/aerius-tasks/src/main/java/nl/overheid/aerius/mail/MailMessageWorker.java). 
The e-mail worker will query the matching e-mail template and construct the e-mail and send it to the mail server. 


## Make a 'Melding'

A 'Melding' is an action a user can do to formally report about the users initiative if certain conditions are met. 
This is the (legally) lighter version of a permit request. 
A permit request must be mailed to the authorities outside AERIUS for legal reasons. 
A 'Melding' however can be done completely within AERIUS.
This process starts in the Calculator web-application, goes through the Melding web-application and ends up in a worker that uses a Register database.

When the user wants to do a 'Melding' first a calculation must be done. 
When a calculation is completed the button to start the 'Melding' process is activated [
MeldingActivity](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/calculator/ui/MeldingActivity.java).
When the user clicks on the button all sources are send to the server and converted to an IMAER GML [
MeldingExportControllerImpl](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/melding/MeldingExportControllerImpl.java). 
This IMAER GML is send back to the web client as String and put in a html form [
MeldingForwardPanel](/aerius-wui-calculator-client/src/main/java/nl/overheid/aerius/wui/melding/MeldingForwardPanel.java).
When the user continues, the form redirects the user to the Melding web-application init page.
This start page is a servlet [MeldingInitServlet](/aerius-wui-melding-server/nl/overheid/aerius/server/servlet/MeldingInitServlet.java) that reads the form data (i.e. the IMAER GML) and persists it in a session on the server. 
The user is redirected to the web client with a unique key referencing the data via the url param 'relayState'.

The web client itself requires authentication with eHerkenning.
Accessing the page will cause Shiro to redirect to the eHerkenning application (third party). See [EHerkenningFilter](/aerius-wui-melding-server/src/main/java/nl/overheid/aerius/server/eherkenning/EHerkenningFilter.java).
If the user successfully authenticates he or she will be redirected back with the relayState param. 
The user can then proceed to enter the required information and possibly upload additional files. 
When the user finalizes the process a json object with all data, including the GML originally created in Calculator, is created and sent to the melding queue for further processing.

The melding queue is read by the [MeldingPayloadWorker](/aerius-tasks/src/main/java/nl/overheid/aerius/register/MeldingPayloadWorker.java). 
There is only one instance running so it guarantees First-In-First-Out. 
The MeldingPayloadWorker will read the the data and use the GML to construct the situation(s). 
This is passed to the [MeldingWorker](/aerius-tasks/src/main/java/nl/overheid/aerius/register/MeldingWorker.java) that will calculate the effects. When the calculation is finished the melding is inserted into the data, as well as  checks are performed to determine wether or not this is a valid Melding, see [ae_assign_pronouncement](/aerius-database/src/main/sql/common/requests/03-functions.sql).
These checks are only rudimentary since not all conditions that classify a valid Melding can be checked. 
If the checks are passed the Melding PDF is created. 
Along with all other data required for the melding to be a proper Register Request, the pdf is persisted to the database.

When all is done the submitter and authority are also informed by e-mail.
This is done through the mailing worker [MailMessageWorker](/aerius-tasks/src/main/java/nl/overheid/aerius/mail/MailMessageWorker.java), and multiple e-mails are send.
The authority that is responsible (based on location of the sources) will be informed the results by one e-mail.
If the user has uploaded any files, another e-mail will be send to the central mail box of participating authorities containing those files. These files are not persisted within AERIUS.
The user will receive an e-mail with the result, and in case of success a link to the generated PDF as well.
