# How to setup the database-build-environment on Windows

## Prepare PostgreSQL and PostGIS
Download the latest PostgreSQL 9.4 installer. The current version is 'postgresql-9.4.14-1-windows-x64.exe'.

Run the installer and use the default configuration. Use Stack Builder to install the latest (Spacial extention) PostGIS 2.3.2 bundle.

Open pgAdmin and connect to the database as user 'postgres'. Create a new Login Role called 'aerius' and make sure all Role privileges are checked. Reconnect to the database as user 'aerius'. 

## Prepare Ruby
Download the latest RubyInstaller from https://rubyinstaller.org/downloads/. The current version is 'rubyinstaller-2.4.2-2-x64.exe'.

Run the installer and make sure to run it as Administrator. Check 'Add Ruby executables to you PATH' during the install.

Install the following gems using `gem install GEMNAME` and make sure to run it as Administrator.
Gems:
- net-ssh
- net-sftp
- clbustos-rtf

## Prepare Git
Download the latest Git from https://git-scm.com/download/win, run the installer and Checkout the AERIUS-II git repository.

## Prepare database build script
Define your own settings in `\AERIUS-II\aerius-database-build\config\AeriusSettings.User.rb` and create the db-data folder on your machine.

## Test database build script
Test the build script by running the following commands from one of the AERIUS databases root folders (e.g. aerius-database-melding):
- db-data sync: `ruby ..\aerius-database-build\bin\SyncDBData.rb settings.rb --from-sftp --to-local`
- test-structure: `ruby ..\aerius-database-build\bin\Build.rb test_structure.rb settings.rb -v structure`
- build database: `ruby ..\aerius-database-build\bin\Build.rb default settings.rb -v #`

After testing the bare scripts you can use [Maven](/doc/development_build_test.md) for building your databases.