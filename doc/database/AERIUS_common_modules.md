# AERIUS Common modules

## essentials
Bevat onze standaard vastgestelde database schemas, de basis typen en de alle generieke (helper) functies en moet aan elke AERIUS database toegevoegd worden. Een deel van de functionaliteit uit *essentials* is ter ondersteuning van het buildsysteem.

## general
Bevat losse componenten die niet door alle, maar wel door het gros van de applicaties gebruikt worden.

## constants
Bevat de (generieke) (public) constanten. Deze zijn soms al bij het bouwen van de tabelstructuur nodig.

## areas_hexagons_and_receptors
Bevat basis functionaliteit van de gebieden, hexagonen en receptoren. De gebieden omvatten onder andere landen, provincies, en verschillende soorten toetsgebieden en habitatgebieden. Ook zijn de natuurdoelstellingen hier te vinden en zijn er tussentabellen met de mapping tussen sommige van deze objecten, zoals de tabel met KDW-gebieden.

## sectors
Bevat sectoren, GCN-sectoren, de koppeling hiertussen en de OPS karakteristieken

## sources
Bevat de bronnen en prioritaire projecten voor Monitor.

## emission_factors
Bevat voor alle broncategorieën de emissiefactoren of andere data om tot de emissie te komen.

## shipping
Bevat voor de broncategorieën binnenvaart en zeescheepvaart, de binnenvaart vaarwegen en het zeescheepvaart netwerk. Deze netwerken worden gebruikt bij het bepalen van de emissie.

## deposition_jurisdiction_policies
Bevat de “Basisscenario + aanvullend rijks- en provinciaal beleid”-depositie en aanvullende functionaliteit om dit samen met KDW en projectbijdrage te presenteren.

## search_widget
Bevat de functionaliteit om gebieden te zoeken in de zoekbalk, welke niet in andere modules zijn opgenomen. Natuurgebieden zijn in deze module gemapped aan gemeenten, plaatsen en postcodes. De 
mapping met provincies is bijvoorbeeld al opgenomen in de module *areas_hexagons_and_receptors*.

## calculations
Bevat voor de rekenmodule de functionaliteit voor het opslaan van de rekenresultaten. Tevens bevat de module functionaliteit voor het (geaggregeerd) ophalen van deze resultaten 

## development_spaces_and_rules
Bevat de functionaliteit voor het beheren en toetsen van de totale ontwikkelingsruimte in de verschillende segmenten.

## requests
Bevat de functionaliteit voor het beheren van meldingen, aanvragen en prioritaire (deel)projecten, welke in de basis allemaal een request zijn.

## pdf_export
Bevat de PDF specifieke functionaliteit. Het gaat hierbij voornamelijk om het presenteren van gebied- en depositiedata.

## system
Bevat alle functionaliteit voor het system schema.

## users
Bevat de functionaliteit voor het beheren van de gebruikers, rollen en rechten.

## users_api
Bevat de functionaliteit voor de Connect API users.

## geometric_utils
Bevat de generieke geometrie (helper) functies zoals die voor receptor en hexagonen het opdelen van vlak en lijnbronnen in puntbronnen.

## opendata
Bevat alle base-, WFS- en WMS-views voor de opendata services die aangeboden worden.

## validations
Bevat de basisfunctionaliteit voor de data validaties en de gedeelde (common) validaties.

## calculator
Bevat de specifieke AERIUS Calculator functionaliteit.

## register
Bevat de specifieke AERIUS Register functionaliteit.

## assertions
***Deze module wordt niet meer gebruikt*** en bevat de basisfunctionalieit voor het maken van, en een aantal database unit tests.