# DeploySystem gebruikers instructies

## Features

* Bouwen van volledige databases inclusief databasedump bestand
* Testen van de SQL met de structuur (DDL)
* Multi-module: hergebruiken van gedeelde modules en SQL
* Management van data en databronnen; kant-en-klaar of gegenereerd tijdens build
* Documenteren à la Javadoc
* Afdwingen van conventies
* Allerhande geautomatiseerde database utilities


## Bouwen van databases

### Wat je moet weten

* Je bouwt altijd een ***module***. Er is tenminste één module maar er kunnen er meerdere zijn, bijvoorbeeld voor verschillende product databases binnen hetzelfde project.
* Iedere module heeft een set ***DDL SQL scripts***: de database object definities (Data Definition Language). Hiermee bouw je enkel de database structuur, dus een lege database.
* Iedere module heeft een set ***data SQL scripts***: hierin wordt via SQL de data geïmporteerd en gegenereerd. Hierna is je lege database gevuld. Dit wordt aangestuurd via het ***loadscript*** van de module.
* Er is een ***runscript***. Hierin zijn *alle* stappen van de build benoemd. Eén daarvan is het aanroepen van bovengenoemde loadscript.
* Er is een algemene folder met ***databronnen***. Dit zijn *plain text table-dumps*, welke de loadscripts nodig hebben.

### Batchbestanden

Het buildsysteem kan om verscheidene manieren worden ingezet. Om de aanroep het buildsysteem en de juiste parameters te vereenvoudigen, zijn er batchbestanden beschikbaar. Deze zijn te vinden in `aerius-database\src\build\scripts`. Te weten:

* **build_dev.bat _[module]_** bouwt een complete database inclusief data, documentatie, validatie en aanmaken van de databasedump. Geschikt voor release. Buildconfiguratie `DEV`.
* **build_quick.bat _[module]_** doet hetzelfde, maar slaat enkele langdurende stappen over, zoals VACUUMing, validatie en de databasedump. Bedoeld voor het snel bouwen van lokale databases voor ontwikkel- en testdoeleinden. Buildconfiguratie `QUICK`.
* **test_build.bat _[module]_** bouwt een database zonder data. Zeer vlot, dus geschikt om wijzigingen in de DDL scripts en de aanwezigheid van databronnen te testen vóór een commit. Of automatisch na een commit (Jenkins).
* **test_structure.bat _[module]_** doet hetzelfde, maar checkt niet de aanwezigheid van databronnen en test ook de documentatie niet. Te gebruiken in de uitzonderlijke gevallen dat `test_build` faalt omdat databronnen nog niet op orde zijn.
* **generate_doc.bat _[module]_** genereert enkel documentatie, voor de gegeven module.
* **run_validations.bat _[module] -n [dbnaam]_** doet enkel de data validatie, voor de gegeven module en op de reeds bestaande database.

Er zijn wellicht enige [settings](#settings) die je moet instellen voordat de batchbestanden succesvol kunnen draaien. Zie hiervoor de gelijknamige paragraaf verderop.

De batchbestanden ondersteunen verder dezelfde [commandline parameters](#commandline) die verderop in dit document worden beschreven. In sommige gevallen kan *[module*] worden weggelaten om ALLE modules te bouwen. Daarnaast kun je in alle gevallen de `-n` parameter gebruiken om de standaard database naam te overschrijven. Het `#` teken wordt daarbij vervangen door de huidige git hash.

Er zijn meer batchbestanden, maar dat zijn utilities die [later](#meer-utilities) worden besproken.


## Configuratie

Het hart van het buildsysteem bevindt zich in `aerius-database\src\build\scripts\DeploySystem`. Deze scripts zijn geschreven in Ruby. Het "entry point" is `Build.rb`. Men kan veel invloed uitoefenen op de werking van het buildscript, door middel van een combinatie van:

* Het runscript
* Settings in een Ruby bestand (`UserSettings.rb`)
* Commandline parameters

### Runscripts

Een build kan alleen draaien indien er een runscript is dat gevolgd kan worden. Indien geen runscript wordt opgegeven, wordt `default.rb` gebruikt. Dat is een complete database build, die verschillende "build configurations" ondersteunt.

De eerder genoemde batchbestanden roepen vaak het gelijknamige runscript aan. Deze staan ook in `aerius-database\src\build\scripts`.

Voorbeeld van een runscript om een volledige database te bouwen:

```ruby
clear_log
default_build_config :DEV
check_datasources
create_database # error if exists!

begin
  import_database_structure
  cluster_tables
  update_comments

  load_data

  reindex_database
  analyze_vacuum_database :analyze

  generate_rtf_documentation "#{get_database_name()} SQL Comments.rtf"

  validate_contents unless $build_config == :QUICK
  dump_database :overwrite_existing unless $build_config == :QUICK
ensure
  drop_database_if_exists :aggressive if $on_build_server
end
```

Voorbeeld van een veel korter runscript welke slechts één taak doet:

```ruby
clear_log
validate_contents
```

Je kunt eenvoudig je eigen runscript maken en deze aanroepen via een batchbestand. Of aanpassingen doen in het runscript zodat deze op bepaalde instellingen (bijv. build flags) kan reageren. De beschikbare commando's hiervoor worden in [bijlage 1](#bijlage-1-run--en-loadscript-commandos) toegelicht.

### Settings

Het bestand `build/scripts/DeploySystem/Settings.rb` bevat alle default settings specifiek voor het project. Voor het grootste deel zijn dit folderlocaties. Het bestand staat in versiebeheer en het is daarom niet handig als individuele gebruikers deze continu moeten aanpassen aan hun eigen situatie.

Daar is een alternatief voor: Iedere setting kan lokaal overschreven worden door gebruikers, middels het bestand `build/scripts/UserSettings.rb` (dus één directory hoger). Dit bestand is optioneel en mag niet in het versiebeheer; voeg het toe aan de ignore-instellingen. Iedere setting in `Settings.rb` die bij een gebruiker afwijkt, kan worden overschreven in `UserSettings.rb`.

Alle settings zijn Ruby globale variabelen. Vanwege de veiligheid staan wachtwoorden in `Settings.rb` standaard op `REDACTED`. Die moeten dus sowieso in `UserSettings.rb` opnieuw worden opgegeven. Minimaal nodig zal zijn om de PostgreSQL-gebruiker op te geven:

```ruby
$pg_username = 'postgres'
$pg_password = 'postgres'
```

Verder wellicht nodig zijn **$dbdata_path**, **$pg_bin_path** en **$git_bin_path**. Een toelichting op alle beschikbare settings is terug te vinden in [bijlage 3](#bijlage-3-settings).

Sommige settings kunnen worden overschreven via commandline parameters of in het runscript.

### Commandline

Het is zelden nodig om het buildscript rechtstreeks — dus zonder batchbestanden — aan te roepen. Desalniettemin, de syntax is:

```bat
ruby Build.rb [runscript] [module] [parameters]
```

Hierbij is:

* `runscript` de bestandsnaam van het runscript. Indien niet opgegeven, wordt `default.rb` aangehouden.
* `module` de naam van de module (database) die gebouwd moet worden. Er kunnen meerdere worden opgegeven gescheiden met komma's en zonder spaties. Indien niet opgegeven, wordt `all` aangehouden, oftewel alle modules worden dan gebouwd.

Parameters kunnen zijn:

* `--path` (`-p`): Pad met de DDL-scripts die worden verwerkt voor de structuur. Default iets als `src/main/sql`.
* `--db-data-path` (`-d`): Pad met databronnen (gebruikt door loadscripts). Default hetgeen wat in de `Settings.rb`  wordt opgegeven.
* `--database-name` (`-n`): Doel naam voor de database die gebouwd wordt. Het `#` teken wordt daarbij vervangen door de huidige git hash.
* `--version` (`-v`): Versiestring welke gebruikt wordt voor de doel database naam. Je kunt dit gebruiken in plaats van `--database-name`. De databasenaam wordt dan de waarde van de **$database_name_prefix** setting, gevolgd door de huidige module, gevolgd door de gegeven versiestring; gescheiden door streepjes.
* `--dump-filename` (`-f`): Bestandsnaam zonder pad voor het database dump bestand. Indien niet opgegeven, wordt de database naam gebruikt.
* `--flags` (`-l`): Kommagescheiden lijst van build flags. Bijv. het opgegeven van `NO_TESTDATA,DEL_SETUP` is vanuit het run- en loadscript te benaderen via de **$build_flags** array als `[:NO_TESTDATA, :DEL_SETUP]`.
* `--help` (`-h`): Toelichting parameters.


## Organisatie en conventies

Bij het ontwerpen van je database is het goed om de volgende conventies aan te houden. Dan kan het buildsysteem er het beste mee omgaan.

### Database schema's

* `public`: De data waar het allemaal om gaat. Hierin zijn de gebruikers van de applicatie veelal geïnteresseerd.
* `system`: Alle functionaliteit benodigd om de (web)applicatie te laten werken. Denk aan (WMS-) servergegevens, SLD's, website teksten, rekenhart instellingen, kleurdefinities en andere constanten. Voor een groot deel is dit dus data niet rechtstreeks aan een gebruiker van de applicatie getoond wordt.
* `i18n`: Vertalingen.
* `setup`: Alle functionaliteit nodig voor het buildsysteem en views/functions om de data te genereren. Hier staan ook tussentabellen die tijdens de build worden gemaakt maar niet meer interessant zijn terwijl de applicatie draait. De webapplicatie mag dus nooit naar dit schema verwijzen. 

### Mappenstructuur

Houd de volgende mappenstructuur aan zodat het buildsysteem automatisch alles kan vinden:

>* `src`
  * `build`
     * `scripts`
       * `DeploySystem`
     * `log`
       * *`module1`*
       * *`module2` ...*
  * `main/sql`
     * `common`
     * *`module1`*
     * *`module2` ...*
  * `data/sql`
     * `common`
     * *`module1`*
     * *`module2` ...*
* `target`
  * `build`
    * *`module1`*
    * *`module2` ...*

De hoofdmap bevat een `src` folder en een `target` folder. Beide mappen bevatten een `build` folder. In `target/build` komt de uitvoer van de build: de database-dump en de documentatie. De scripts om daartoe te komen staan in `src/build`. Deze map bevat `log` met de uitvoerlogbestanden per module, en `scripts` voor de scripts van het buildsysteem zelf. Dit is een deel projectspecifieke instellingen/scripts, en het generieke buildsysteem `DeploySystem` welke ook in andere projecten kan worden gebruikt.

De database structuur is vastgelegd in de DDL scripts in `src/main/sql`. Per module er is een map. De structuur in de map `common` kan vanuit andere modules worden aangeroepen.

Idem voor `src/data/sql`. Het gaat hier echter niet om DDL scripts, maar om SQL scripts welke tabellen vullen door ofwel databronbestanden in te lezen, ofwel queries uit te voeren.

De databronbestanden zelf moeten ook een plekje hebben, maar dit kan buiten deze structuur (bijvoorbeeld op een snelle SSD). Wil je ze wel hier bewaren, gebruik dan `src/data/db-data` —  ignore deze folder dan in git of svn.

### Organisatie van DDL SQL scripts

De organisatie van de DDL scripts binnen een module staat vrij; maar enige conventie is wel noodzakelijk om het overzicht te behouden. Normaliter bevat deze map dus meerdere niveaus aan subfolders.

Het buildsysteem gaat alfabetisch en recursief door de mappen en bestanden heen (hierbij worden mappen en bestanden door elkaar verwerkt). Als de volgorde van verwerking belangrijk is, moeten de folder- en bestandsnamen daarom beginnen met een nummer.

Op het hoogste niveau staat vaak een `00-common.sql` welke door de naam als eerst zal worden uitgevoerd. Hier kunnen de imports van de common module geplaatst worden (zie hoofdstuk [Common](#common)).

Daarna staat er voor ieder database schema een folder, bijvoorbeeld:

>* 10-public
>* 20-system
>* 21-i18n
>* 99-setup

Binnen de schema's *kan* eventueel eerst een opdeling gemaakt worden op onderwerp m.b.v. folders. Daarbinnen *kunnen* bestanden worden opgesplitst zoals dit:

>* 01-types
>* 02-tables
>* 03-functions
>* 04-views
>* 05-triggers
>* 06-casts
>* i18n

Afhankelijk van de hoeveelheid statements kunnen dit SQL bestanden zijn, of folders met daarin weer meerdere bestanden. De `i18n` folder is bedoeld om tabellen met vertalingen te scheiden van de gewone tabellen.

Het schema `setup` en dus de map `99-setup` bevat veel functionaliteit die met de build te maken heeft. Deze functionaliteit kun je groeperen in folders. Een voorzetje:

* `01-build` Alles wat wordt aangeroepen tijdens de "data" fase van de build om de inhoud van tabellen te bepalen. Zoals de zogenaamde *build views*. Bijvoorbeeld, de view `setup.receptors_to_background_cells_view` zou dan de inhoud van de tabel `receptors_to_background_cells` bepalen.
* `02-validation` De validatie functies; zie paragraaf [Data validatie](#data-validatie).
* `03-summary` De rapportage functies; zie paragraaf [Rapportages](#rapportages).

### Organisatie van data SQL scripts

Maak logische blokken die vanuit het loadscript kunnen worden aangeroepen.

De eerder genoemde *build views* kunnen hier worden aangeroepen.

```sql
BEGIN;
	INSERT INTO receptors_to_background_cells
	SELECT receptor_id, background_cell_id FROM setup.receptors_to_background_cells_view;
COMMIT;
```


## Omgaan met data

### Het loadscript

Het loadscript is altijd genaamd `load.rb`. Iedere module in de map `data/sql` moet zo'n loadscript bevatten. Hierin staan alle stappen die met data te maken hebben. Ook worden de SQL scripts of folders aangeroepen waarin de databronnen worden geïmporteerd en/of de data gegenereerd.

De reden dat dit een apart bestand is, is zodat er eenvoudig een database zonder data gebouwd kan worden.

Voorbeeld van een loadscript:

```ruby
add_build_constants

run_sql "general.sql"

unless $build_flags.include?(:NO_TESTDATA) then
  run_sql "test_users.sql"
  run_sql "test_data.sql"
end

run_sql_folder "system"
run_sql_folder "i18n"

synchronize_serials

if $build_config == :RC || $build_config == :TEST then
  validate_contents
  create_summary
end
```

Alle mogelijke commando's zijn terug te vinden in Bijlage 1.

### Databronnen

Een ***databron*** is een tekstbestand met daarin een dump van de tabeldata, gegenereerd volgens een vaste conventie.

Het buildsysteem kan alleen een gevulde database maken als er een *lokale* folder is waarin alle databronnen te vinden zijn, bijvoorbeeld `src/data/db-data`. Het buildsysteem kent de locatie van deze map (configureerbaar via **$db_data_dir** of **$db_data_path**). Vervolgens kan er vanuit de SQL scripts verwezen worden naar dit pad via de `{data_folder}` variabele in plaats van een absoluut pad.

Het is verstandig om een centrale (gedeelde) versie van deze map te maken die leidend is, bijvoorbeeld op een NAS of FTP. Aangezien het buildsysteem alleen werkt met lokale mappen, moet er dan wel af en toe een sync-script gedraaid worden. Deze is bijgeleverd. Verder controleert de build eerst of alle databronnen wel aanwezig zijn.

> De insteek is dat databronnen *niet* het standaard versiebeheer systeem ingaan. Je wil liever niet bestanden van honderden MB's in git of svn hangen. In plaats daarvan dient `db-data` dus als een zeer eenvoudige versiebeheer; zorg daarom voor goede backups.

Naast de gedeelde map `db-data` moet er ook een map zijn waarin de totstandkoming van de databronnen terug te vinden is, bijvoorbeeld `org`.

### Databron inladen

Vanuit de SQL scripts in `src/data/sql` kan de utility functie `ae_load_table` worden gebruikt:

```sql
BEGIN; SELECT setup.ae_load_table('habitat_types', '{data_folder}/public/habitat_types_20151207.txt'); COMMIT;
```

### Databron maken

> **Let op:** Het wordt aangeraden om niet-afgeleide data zoveel mogelijk in databron bestanden te beheren. Grote SQL bestanden vol `INSERT INTO` commando's in de `data/sql` map plaatsen kan wel, maar is moeilijk te onderhouden. Een uitzondering zijn sommige tabellen in het `system` schema, zoals constanten en wms-gegevens, hierbij is het soms wel makkelijk om dit "inline" te kunnen aanpassen.

Databronnen genereer je lokaal. Dit kan zijn door shapefiles of andere bestanden te importeren. Of door simpelweg queries los te laten op andere PostgreSQL tabellen. Hoe dan ook, uiteindelijk heb je een PostgreSQL tabel of query klaarstaan welke als databron moet dienen. Die schrijf je dan als volgt naar een databron bestand:

```sql
SELECT setup.ae_store_table('setup.uncalculated_receptors', 'd:/Database/db-data/org/setup.uncalculated_receptors_calculator_20151019/{tablename}_{datesuffix}.txt', TRUE);
```

of

```sql
SELECT setup.ae_store_query('depositions_jurisdiction_policies',
	$$ SELECT depositions_jurisdiction_policies.* FROM depositions_jurisdiction_policies INNER JOIN receptors USING (receptor_id) ORDER BY year, receptor_id $$,
	'd:\Database\db-data\org\depositions_jurisdiction_policies_20150916\{queryname}_{datesuffix}.txt');
```

Je werkt hier wel met absolute paden, maar de tabel/query naam en datum kun je automatisch laten invullen. De datum in de naam is belangrijk voor het versiebeheer. Door op deze manier te werken kunnen alle oude versies (eerdere data releases) in `db-data` bewaard blijven en teruggehaald worden. Je kunt dan zelfs een oude git-revisie die naar oude databronnen verwijst, opnieuw builden.

Het gebruik van bovengenoemde functies zorgt voor een standaard formaat wat altijd werkt. Het dwingt de maker van de data ook af om alvast goed te testen op een echte database. Dit vermindert het risico op bijvoorbeeld key violations wanneer de data weer wordt ingeladen.

### Databron herleidbaar houden

Gebruik voor het maken van een databron zo veel mogelijk (SQL) scripts en documenteer al je stappen. Verzamel dit allemaal in één map. Idealiter zou je deze map aan iemand anders moeten kunnen geven die alleen maar de genummerde scripts op volgorde hoeft uit te voeren om tot hetzelfde resultaat te komen.

Gebruik een foldernaam met daarin de datum en zo mogelijk tabelnaam, bijvoorbeeld `depositions_jurisdiction_policies_20150916`.

Plaats de map in de eerder genoemde centrale `org` map. 

Zet in de map ook een kopie van het databronbestand, zodat de `org` map makkelijk doorzocht kan worden op de herkomst van een bepaalde databron.

## Common

Zowel de DDL SQL scripts in `main/sql` als de data SQL scripts in `data/sql` kunnen andere SQL scripts importeren. Deze andere scripts moeten dan geplaatst worden in respectievelijk `main/sql/common` en `data/sql/common`.

### SQL script importeren

> De SQL scripts mogen commando's en variabelen bevatten welke het buildsysteem speciale acties laat uitvoeren. Deze staan tussen `{deze haken}`. Deze ben je in de loop van dit document reeds tegengekomen. Let op: PostgreSQL kent deze commando's niet.

Je kunt een SQL-bestand invoegen op de volgende manier:

```ruby
{import_common 'system/constants/constants.sql'}
```

Indien deze regel bijvoorbeeld wordt geplaatst in een bestand ergens in `main/sql`, dan wordt de inhoud van het bestand `main/sql/common/system/constants/constants.sql` op die locatie ingevoegd.

Vanwege de organisatie binnen `common` is het gebruikelijker om een hele common-folder in één keer te importeren. Ook deze folder wordt dan alfabetisch doorlopen, en mag weer subfolders bevatten.

```ruby
{import_common 'development_spaces_and_rules/'}
{import_common 'validations/core/'}
```

### Organisatie van common

De functionaliteit in common moet opgedeeld zijn in logische blokken die in één keer geïmporteerd kunnen worden. Er mag best afhankelijkheid zijn tussen de blokken, maar hoe minder hoe beter.

## Meer features

### Documentatie

De DDL statements van verschillende soorten database objecten kunnen van commentaar worden voorzien op een soortgelijke wijze als Javadoc. Het buildsysteem verzameld deze dan automatisch en doet het volgende:

* Koppelt het aan de database objecten middels het `COMMENT ON` statement.
* Genereert een RTF bestand in de `target` folder met een overzicht van alle database objecten en databronnen.

Voorbeeldnotatie:  

```sql
/*
 * permit_assessment_area_review_info_view
 * ---------------------------------------
 * Geeft per vergunning en N2000-gebied o.a. aan hoeveel ruimte al toegekend is.
 * Deze view wordt gebruikt in Register in de Toetsing-tab van een aanvraag.
 *
 * @column available_inclusive Beschikbare ontwikkelingsruimte indien de aanvraag wordt goedgekeurd.
 * @column available_exclusive Huidig beschikbare ontwikkelingsruimte in het gebied (zonder aanvraag).
 *
 * @see ae_development_rule_exceeding_space_check()
 * @todo Performance moet worden verbeterd 
 */
CREATE OR REPLACE VIEW permit_assessment_area_review_info_view AS
SELECT
	request_id,
	available_inclusive,
	...
```

Bij functies zijn andere @tags mogelijk:

```sql
/*
 * ae_determine_nearest_province
 * -----------------------------
 * Functie welke teruggeeft in welke provincie een punt ligt, of anders wat de dichtsbijzijnde provincie is.
 *
 * @param v_point Het punt waarbij de provincie gevonden moet worden. In het geval van een aanvraag wordt hier het middelpunt van de bronnen voor gebruikt.
 * @returns Id van provincie (province_area_id).
 */
CREATE OR REPLACE FUNCTION ae_determine_nearest_province(v_point geometry)
	RETURNS integer AS
	...
```

### Multithreaded data inladen

Dit is mogelijk vanuit de SQL-scripts in `data/sql` en kan een behoorlijke performance boost opleveren. Voorbeeld:

```sql
{multithread on: SELECT unnest(enum_range(NULL::setup.sectorgroup)) AS sector_group ORDER BY sector_group}

	SELECT setup.ae_build_sector_economic_growths('{sector_group}');

{/multithread}
```

De query na de `on:` wordt uitgevoerd, en het aantal teruggegeven records bepaalt het aantal threads dat wordt gemaakt. Dus voor ieder resultaat wordt een thread gestart waarin de query binnen het `{multithread}` blok wordt uitgevoerd; hierbij worden gegeven variabelen gesubstitueerd met de uitkomsten van de velden in dat record (in dit voorbeeld `{sector_group}`). De variabele naam moet dus overeenkomen met de veldnaam uit de query, vandaar de expliciete `AS`.

Er zullen maximaal `$max_threads` tegelijk draaien (zie [Settings](#settings)), het overschot blijft in de wacht staan tot er weer threads klaar zijn.

> **Let op:** De `on` query wordt uitgevoerd terwijl het SQL bestand wordt geparsed. De query moet dus niet afhankelijk zijn van data welke in datzelfde SQL bestand wordt geïmporteerd; immers het bestand is dan nog niet uitgevoerd en de tabellen zullen nog leeg zijn. In dit geval kun je de SQL-bestanden opsplitsen, zodat de tabellen al tijdens een eerder `run_sql` commando worden gevuld.

## Oplossingen

### Data validatie

* Het loadscript-commando `validate_contents` roept de functie `setup.ae_validate_all()` aan.

* Deze functie moet je zelf toevoegen in je module DDL en het buildsysteem doet verder niets met de uitkomst.

* `main/sql/common/validations/core` biedt je echter wel utilities en handvatten:
  * Tabelstructuur om de validatieresultaten in op te slaan (`01-results.sql`)
  * Functies met veelgebruikte validaties en hulpmiddelen (`02-utils.sql`)

*  Een leeg voorbeeld van `setup.ae_validate_all()` staat in de `essentials` module.

Zo kun je een `setup.ae_validate_all()` maken welke individuele validatie functies (common of je eigen) aanroept middels `setup.ae_perform_and_report_validation(…)`. Zo lang die functies een  `SETOF setup.validation_result` returnen, wordt dit resultaat automatisch verwerkt (opgeslagen in de tabel).


### Rapportages

* Het loadscript-commando `create_summary` roept de functie `setup.ae_output_summary_table(filespec text)` aan.

* Deze functie moet je zelf toevoegen in je module DDL en het buildsysteem doet verder niets met de uitkomst.

* De functie moet één of meer rapportage bestanden genereren en deze wegschrijven volgens de meegegeven bestandsspecificatie (bijvoorbeeld als CSV via `COPY TO`).

* Het buildsysteem roept de functie aan met als bestandsspecificatie de `target` folder van de huidige module gevolgd door `{title}_{datesuffix}.csv`. Het is aan de functie om deze twee variabelen te vervangen met de naam van het rapport en de huidige datum (YYYYMMDD).

## Meer utilities

### Databronnen lokaal synchroniseren

De eerste stap van het buildsysteem is om alle gebruikte databronnen op te zoeken. Op het moment dat er in data SQL scripts verwezen wordt naar een databron dat het buildsysteem niet kan vinden, dan verschijnt er direct een foutmelding. Dit is bijvoorbeeld zo als er een nieuwe versie van een databron is gemaakt.

In dat geval moet je je lokale `db-data` map synchroniseren met de centrale map. Indien dit een SFTP is, dan kun je het volgende sync-script gebruiken.

```bat
sync_dbdata_to_local.bat [module] [parameters]
```

Parameters zijn:

* `module`: De module die gesynced moet worden. Indien niet opgegeven worden alle modules verwerkt.
* `--continue` (`-c`): Niet stoppen bij ontbrekende bestanden.
* `--help` (`-h`): Toelichting parameters en toont ook alle default gebruikte paden.

Voor de gebruikte paden worden de [Settings](#settings) aangehouden.

### Overige

* **unused_datasources.bat** maakt een batchbestand aan genaamd `delete_unused_datasources.bat` in de `target` map, met daarin DEL-commando's voor iedere lokale databron waar niet meer naar verwezen wordt vanuit de data SQL scripts. Vaak zijn dat databronnen die reeds vervangen zijn door een nieuwere versie. Het aangemaakte batchbestand kan dan gebruikt worden om schijfruimte vrij te maken.

  *Dit is alleen bedoeld voor de lokale `db-data` map, nooit voor de server waar de databronnen centraal zijn opgeslagen. Daar moet het versiebeheer immers in stand blijven.*

* **missing_org.bat** verzameld lokaal de databronnen waar naar verwezen wordt vanuit de data SQL scripts, en doorloopt vervolgens de `org` folder op de SFTP om te controleren of al deze databronnen daarin terug te vinden zijn. De databronnen die niet terug te vinden zijn, zijn dus niet herleidbaar. Of er is geen kopie van die databron in de bijbehorende `org` map gezet.

Voor de gebruikte paden in bovenstaande batchbestanden worden de [Settings](#settings) aangehouden.


## Bijlage 1: Run- en loadscript commando's

Dit is een overzicht van alle commando's die in beide soorten scripts gebruikt mogen worden. Deze zijn terug te vinden in `ScriptCommands.rb`; het gaat dan alleen om de public functies.

Het vinkje geeft aan in welk script het commando normaliter geplaatst zou moeten worden. Je kunt ze in beide scripts gebruiken maar dat is dan vaak niet logisch i.v.m. het verschil tussen het bouwen van een lege database en een gevulde.

Commando's welke een custom PostgreSQL functie aanroepen zijn er afhankelijk van dat deze functie daadwerkelijk is gedefinieerd; daarom kunnen deze het beste in het loadscript worden geplaatst.

Commando 			| Beschrijving | Run | Load
:--- 				| :--- | :---: | :---:
||
**clear_log**			| Gooit de `log` folder leeg. | ✔ | |
**check_datasources**		| Controleert of alle databron bestanden waarnaar verwezen wordt vanuit de data SQL scripts bestaan. | ✔ | |
**create_database**		| Maakt een nieuwe database aan met de aan het buildsysteem opgegeven naam of versie-string.<br> Met parameter `:overwrite_existing` wordt er géén foutmelding gegeven indien de database al bestaat.<br> Met `:dont_update_comment` wordt er geen `COMMENT` op de database gezet. | ✔ | |
**drop_database_if_exists**	| Verwijdert de database met de aan het buildsysteem opgegeven naam of versie-string.<br> Met parameter `:aggressive` worden alle verbindingen op de database eerst afgebroken. | ✔ | |
**import_database_structure**	| Voert alle DDL SQL scripts uit voor de module.<br> Met parameter `:sql_as_is` worden de bestanden op geen enkele manier door het buildsysteem verwerkt/geparsed. | ✔ | |
**update_comments**		| Zet comments op alle database objecten aan de hand van de documentatie in de DDL SQL scripts. Zie [Documentatie](#documentatie). | ✔ | |
**cluster_tables**		| Voert de functie `setup.ae_cluster_all_tables()` uit welke alle tabellen clustert volgens de primary key. | ✔ | |
**load_data**			| Voert het loadscript (`load.rb`) van deze module uit | ✔ | |
**run_sql _file_**		| Voert het SQL bestand uit (relatief ten opzichte van `load.rb`).<br> Met parameter `:sql_as_is` worden de bestanden op geen enkele manier door het buildsysteem verwerkt/geparsed. | | ✔ |
**run_sql_folder _folder_**	| Voert alle SQL bestanden in de opgegeven folder uit (relatief ten opzichte van `load.rb`). | | ✔ |
**add_constant _key, value_**	| Voegt een constante *key* met waarde *value* toe aan de tabel `system.constants` (welke dus moet bestaan) | | ✔ |
**add_build_constants**		| Voegt de constanten `CURRENT_DATABASE_NAME`, `CURRENT_DATABASE_VERSION` en `CURRENT_GIT_REVISION` toe met de juiste waarden. Hierdoor kan in een database altijd opgezocht worden met welke source revisie deze is gebouwd. | | ✔ |
**synchronize_serials**		| Voert de functie `setup.ae_synchronize_all_serials()` uit. Deze zorgt ervoor dat alle serials in de database weer "goed" staan. Dat wil zeggen voor iedere tabel op de MAX(id)+1. Door het gebruik van `COPY FROM` statements tijdens het inladen van de data, kunnen serials namelijk uit de pas gaan lopen. Bij juiste naamgeving, kan de functie zelf de relevante tabellen en velden vinden. | | ✔ |
**validate_contents**		| Voert de functie `setup.ae_validate_all()` uit, zie [Data validatie](#data-validatie). | | ✔ |
**create_summary**		| Voert de functie `setup.ae_output_summary_table(…)` uit, zie [Rapportages](#rapportages). | | ✔ |
**analyze_vacuum_database**	| Voert de `ANALYZE` en/of `VACUUM` statements uit. Hierdoor kunnen stappen zoals de validatie sneller verlopen. Het is met name handig indien de gemaakte database rechtstreeks wordt gebruikt; als alleen de database dump wordt gebruikt dan valt hiermee geen winst te behalen.<br> Parameters kunnen zijn `:analyze`, `:vacuum`, `:full`, of een combinatie daarvan. Dit volgt dus het SQL statement. | | ✔ |
**generate_rtf_documentation _[filename]_** | Genereert een RTF bestand met de opgegeven naam in de `target` module folder, aan de hand van de documentatie in de DDL SQL scripts. Zie [Documentatie](#documentatie). | ✔ | |
**dump_database _[path]_**	| Dumpt de database naar een binair database dump bestand met extensie `.backup`. Het pad is de `target` module folder tenzij anders opgegeven in de eerste parameter.<br> De bestandsnaam kan worden opgegeven via de commandline **--dump-filename** of met de **$dump_filetitle** setting, zo niet dan wordt de database naam gebruikt.<br> De parameter `:overwrite_existing` zorgt ervoor dat een reeds bestaand dump bestand zonder foutmelding wordt overschreven.<br> De `COMMENT` van de database wordt uitgebreid met de bestandsnaam van de dump, tenzij `:dont_update_comment` wordt meegegeven. | ✔ | |
||
**has_build_flag _flag_**	| Geef een String of Symbol op om te testen of deze is gezet als build flag. Indien een Array wordt meegegeven moeten alle build flags die hierin staan gezet zijn. | ✔ | ✔ |
**get_database_name**		| Geeft de database naam terug (ingesteld via commandline of andere wijze). | ✔ | ✔ |
**set_database_name _name_**	| Om de database naam via het script in te stellen. | ✔ | |
**default_database_name _name_** | Gebruik om een database naam in te stellen, indien deze niet al meegegeven wordt via de commandline of op een andere wijze. | ✔ | |
**get_version**			| Geeft de versie-string terug (ingesteld via commandline of andere wijze). Als alleen een database naam opgegeven werd, kan het buildsysteem soms de versie-string hier uit bepalen.  | ✔ | ✔ |
**set_version _version_**	| Om de versie-string via het script in te stellen. | ✔ | |
**set_db_path _path_**		| Gebruik om het pad naar de databronnen via het script in te stellen. Normaliter gebeurd dit via settings (**$dbdata_dir**, **$dbdata_path**) of commandline. | ✔ | |
||
**execute_sql _sql_**		| Voert het gegeven SQL statement uit. | ✔ | ✔ |
**execute_ext_sql _sql_**	| Voert het gegeven SQL statement uit buiten de database om. Dit is nodig voor bijvoorbeeld `DROP DATABASE` statements. | ✔ | ✔ |
**terminate_connections**	| Breekt alle verbindingen naar de database af. | ✔ | |


## Bijlage 2: PostgreSQL functies

Er zijn enkele "vereiste" PostgreSQL functies die aanwezig moeten zijn om bepaalde run- en loadscript commando's te kunnen uitvoeren. Deze zijn deels al genoemd in [bijlage 1](#bijlage-1-run--en-loadscript-commandos).

Om deze functies bij te voegen in je DDL script is het voldoende om de `essentials` module te importeren. Voor gebruik van constanten is ook `common\system\constants` nodig.

Hier volgt een opsomming van de interessante functies; bekijk de documentatie in de broncode (of gegenereerde RTF) voor een toelichting.

* `setup.ae_load_table(tablename regclass, filespec text)`
* `setup.ae_store_table(tablename regclass, filespec text, ordered bool = FALSE, use_pretty_csv_output boolean = FALSE)`
* `setup.ae_store_query(queryname text, sql_in text, filespec text, use_pretty_csv_output boolean = FALSE)`
* `setup.ae_synchronize_all_serials()`
* `setup.ae_cluster_all_tables()`
* `setup.ae_validate_all()` (lege functie, overschrijf deze in je module)


## Bijlage 3: Settings

Variabele 			| Beschrijving | Default
:--- 				| :--- | :---
||
**$pg_hostname**		| PostgreSQL server host | *niet opgegeven*
**$pg_port**			| PostgreSQL server poort | *niet opgegeven*
**$pg_username**		| PostgreSQL gebruikersnaam | 'postgres'
**$pg_password**		| PostgreSQL wachtwoord | 'postgres'
**$database_tablespace** 	| PostgreSQL tablespace welke gebruikt moet worden bij aanmaken databases | *leeg*
||
**$pg_bin_path**		| Pad naar PostgreSQL bin folder. <br>Kan overruled worden met `POSTGRESQL_BIN` environment variabele. Anders standaard "Program Files" folder op Windows, of shell path op Linux. |
**$git_bin_path**		| Pad naar git folder met de git.exe. <br>Kan overruled worden met `GIT_BIN` environment variabele. Anders standaard "Program Files" folder op Windows, of shell path op Linux. | 
||
**$on_build_server**		| Kan aangezet worden op de build server om het runscript andere dingen te laten doen | false
||
**$database_name_prefix**	| De prefix voor het genereren van een database naam als alleen de version wordt opgegeven: `prefix-module-version` | 'AERIUSII'
||
**$dbdata_dir**			| Locatie van de databronnen folder relatief op de lokale data folder en de SFTP (**$sftp_data_path**) | 'db-data/aeriusII/'
**$org_dir**			| Locatie van de databronnen org-folder relatief op de SFTP (**$sftp_data_path**) | 'org/'
**$dbdata_path**		| Volledige (absolute) pad naar lokale databronnen folder. <br>Indien niet opgegeven dan wordt het opgebouwd m.b.v. scriptlocatie en **$dbdata_dir**. | *niet opgegeven* 
||
**$max_threads**		| Maximaal aantal simultane threads voor {multithread} blokken in de data SQL | 10
||
**$sftp_data_path**		| SFTP url voor syncen van databronnen; **$dbdata_dir** moet hieraan toegevoegd kunnen worden | 'sftp://sftp.rivm.nl/'
**$sftp_data_username**		| Gebruikersnaam voor bovenstaande SFTP | 'REDACTED'
**$sftp_data_password**		| Wachtwoord voor bovenstaande gebruiker | 'REDACTED'
**$sftp_data_readonly_username**| Readonly gebruiker voor bovenstaande SFTP | 'REDACTED'
**$sftp_data_readonly_password**| Wachtwoord voor bovenstaande gebruiker | 'REDACTED'
||
**$module**			| Module om te bouwen als Symbol. Gebruiker liever de [commandline](#commandline) om de module op te geven. | :all


