# AERIUS application

This page describes the technical architecture of AERIUS.

AERIUS is a system consisting of several applications that communicate via a
message bus. The user facing applications are GWT applications running in the
browser and running on a Tomcat webserver. All tasks requested by the user are
send over the messagebus to the worker applications. These workers can be seen
as dedicated services. All applications are written in Java code. The
applications all use centralized product specific databases. Meaning for each
specific product one database is available. The database runs on PostgreSQL
with the geo extension Postgis. Due to the data structures, i.e. operations on
lots of geo points part of the business logic is build in sql stored procedures.

## Webservers
The webservers are the user facing applications. The webservers host the GWT
applications. The GWT applications can be seen as fat clients running the the
browser. They mainly communicate with the server by only sending data back and
to the browser, also called ajax. The webservers itself are Apache Tomcat
applications. There are 5 different GWT/tomcat applications in the AERIUS
system:

### Calculator
The Calculator webserver application is a GWT application with which a user can
add emission sources, perform calculations, and create pdf documents. The
calculator has a strong map component based on the OpenLayers JavaScript library.
The library handles all geo information, like showing map layers, map interaction.
The calculator runs on top of the calculator database. The calculator doesn't
persist user data. But the user can import and export the data as IMAER data
(GML or PDF). Calculation results are temporary persisted during the session
of the user.

### Melding
The Melding webserver application is a small GWT application with which a user
can make a 'melding' (notice). The melding is initiated from the calculator.
It opens the melding application with a POST call with the source data
submitted in post. Then the user is asked to login with eHerkenning. The
authentication is done with Apache Shiro with a custom adapter that wraps
the Connectics eHerkenning library. When the user finalizes the process all
data is send to the message queue, were it is handled on a FIFO basis by a
melding worker. The melding worker processes the melding and informs the user
via e-mail about the status of the melding. The melding application runs on
top of the melding database, while the melding workers runs on top of the 
register database.

### Register
The Register webserver application is a GWT application used to manage and
register the permit applications. Different permits types are managed and 
also the meldings are inserted by the worker in the register database.
The register application has user access and role management. Roles are
feature driven. So a role gives access to a specific feature. The role
management is done within the application. The user management UI is
technically a separate module that can be reused for other applications.
Authentication control is managed by Apache Shiro. There is a specific module
for the external authentication service used.
All long duration tasks, like calculation a permit, generating documents and 
aggregated data are handled by workers. The UI application only reads data
and triggers tasks.
The register works on top of the register database. This is a relatively large
database because all permit calculation data is stored and persisted. It's the
most important database of the product ranges since all the important data of
the permits are stored in this database; both permits and the calculation results
as well as the state of the development space.

### Scenario
The Scenario webserver application is a GWT application that is very similar
to the Calculator. While the Calculator is targeted at making calculations for
permit applications, the Scenario is targeted for large data sets and more 
complex calculations. Some different features are: Importing of AERIUS IMAER 
GML files with results, and showing the results. Importing larges source sets
as the Calculator.

The Scenario application is the most recent application so the feature set
might change in the future. For example it's currently not possible to make
calculations.

### Connect
The Connect webserver application is web services application based on
websockets. It uses the JSON-RPC 2.0 prototol for communications. The
Connect application is intended for machine to machine interaction, but
can also be used to perform large calculations, that are not possible with
the calculator due to configured limits.

### Geoserver
All AERIUS maps are provided by the Geoserver map server. The map
configurations are stored in text based files, which are packaged into
a single deploy file together with the Geoserver binary during build.
This way map configurations are managed in the SCM repository.
For some maps a custom style is provided through a SLD. The SLD's are
served via a servlet in the product specific webapplication. Maps are
accessed via a proxy servlet in the product webapplication, were if
available, the SLD url is appended to the map url and sending that
url to Geoserver.

## Workers
Workers are Java applications. Some listen to the RabbitMQ queue for
new tasks, or run on an internal loop, or are run as command-line
applications.

### Calculation Workers
Calculation workers perform run the scientific models on the given input
sources and output at the given result points. Result points are always
returned even if there would be no result. The principle of a calculaton
workers is that it calculates the effect of the input for a specific point
location, a result point. This allows the calculation to be parallized.
A set of result points can be split over multiple calculation workers, were
each calculation worker get the same input and a sub set of the result points.
The calculation workers get as input the data specific for the model used.
The input data therefor is only converted once in the preparation phase before
calculation starts. Calculation workers therefor don't have a connection to
a AERIUS database, because all information is in the input.
One Calculation worker can run multiple processes. This can be configured and
the number of processes run on a single machine depends on how the specific
calculation workers operates.

#### OPS Worker
The OPS worker is the worker that calculates results with the Operationele
Prioritaire Stoffen (OPS) model. The application is a native application
developed and maintained by the RIVM. The executable is wrapped by the OPS
Worker. The worker generates the required input files based, run the OPS
executable, parses the result files, and send the results back.
The number of concurrent OPS exectubles that can be run on 1 machine is
theoretically limited by the number of cores. Since OPS takes 100% CPU when
it runs, it doens't make much sense to configure the number of concurrent
worker processes on 1 machine to be more than the available cores.

#### SRM2 Worker
The SRM2 worker is the workers that calculates results with the
Standaardrekenmethode 2 (SRM2) from the "Regeling beoordeling luchtkwaliteit
2007". It a model specific for road traffic. The model itself is implemented
in OpenCL and via Java JOCL library integrated in the SRM2 Worker. The model
uses static location specific input paramters that are installed with the
worker. There are 3 sources of this data:
1) The data map of PreSRM.
2) Preprocessed data with a small Pascal application that is part of AERIUS.
This application runs PreSRM on 1x1km areas covering the whole Netherlands
and generates an static input file for each year needed. 
3) A file containing the deposition velocity covering the whole Netherlands.
This file is prepared by the maintainer of AERIUS.
When a new version of PreSRM is to be used this data must be updated
and regenerated.
When a process is run OpenCL takes up all availabe cores. But because of the
calculation distance between sources and result points in many cases no
calculations need to be done for the input. This is handled before the 
OpenCL calculation is done, so the number of processes can be set higher
than the available cores. A value between 10 and 20 is recommended.

### Database Workers
Database workers are basically workers that use a database. For these workers
the database is seen as the limiting factor and therefor they are refered to
as database workers. Most workers listen to a queue for tasks, and some workers
run on a internal loop. There are also command line workers, which are
technically also called database workers. These workers get their input via the
command line.

#### Calculation Worker
The calculation worker handles tasks from the Calculator web application. These
tasks can be perform a calculation, generate a pdf calculation report, or create
an IMAER gml file. The calculation worker uses the calculator database.

A separate worker is the Import worker. That reads an IMAER and converts it to
the internal datastructure. This worker has a separate queue and is both
available for Calculator and Register.

#### Melding Worker
The Melding worker handles "melding" requests done by users via the Melding web
application. Because this process first-come, first-served only 1 process is
running this worker. The worker calculates the "melding" and generates a report.
If the "melding" was successful it stored in the register database, else it is
rejected. reports are send back to the user via the message worker.

#### Register Worker

#### Monitor Worker

#### Message Worker
The message worker delivers input (files or messages) to the user via e-mail.
E-mails are created using templates. In the input variables can be given that
are used to customize the e-mail message from the templates. The worker uses
the calculator database.

### Scheduled Workers
Scheduled workers run on their own schedule. Or they query the database or run
at a specific time. Opposite to all other workers they don't listen to a queue.
The following workers are available:

#### RequestCalculationWorker
The request calculation worker queries the database every minute to check if
there are permits added that need to be calculated. It takes the oldest permit
first. This workers runs against the register database, and only 1 process
should be running, because it uses the state of the permit in the database to
determine if it should be running.

#### RequestDecreeGeneratorWorker
The request decree generator worker queries the database every minute to check
if there are permits that are changed to state accepted or rejected and have no
pdf yet. This workers runs against the register database, and only 1 process
should be running, because it uses the state of the permit in the database to
determine if it should be running.

#### DepositionSpaceWorker
The deposition space worker recalculates the permit threshold values for the
nature areas and recalculates for all permits queued if there is deposition
space available. This process should run at least once a day, preferable at 
night when nobody is working, because when running this process the database is
locked. This workers runs against the register database, and only 1 process
should be running, because it uses the state of the permit in the database to
determine if it should be running.

## Task Manager
