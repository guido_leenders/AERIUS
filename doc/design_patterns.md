# Design Patterns and Goals

This document describes various (loosely enforced) design patterns and goals
that are being employed in all Java code. In no particular order.
 
## Exception handling
To present the user with user friendly messages a special exception is 
available: [AeriusException](/aerius-shared/src/main/java/nl/overheid/aerius/shared/exception/AeriusException.java)
This exception should be used when an errors is thrown that must be contains
information that should be shown to the user. 
An AeriusException is created with a `Reason`. This refers to a code in the 
AeriusExceptionMessage properties file. This file contains the language specific user
friendly textual representation of the error message.

## User text, Internationalization
Any text the user reads may not be put directly in source code. There are 2
places to put texts. Firstly in properties files, secondly in the database.
Most messages should be put in the properties files. Texts in the database
are used for e-mail template messages, because this allows for easy change
in production, while properties file can only be changed at compile time. 
 
## Database access
Database access from Java code is mainly done via the aerius-database project.
Database access is implemented directly JDBC, without the use of an ORM. This
design was chosen because the database uses a lot of views and Postgis extended
functionality that mostly doesn't work well with ORM implementations. 

The pattern to access the database is via Repository classes. These classes 
provide static methods that get the connection object passes as argument.

## GeoServer
Creating a new geoserver layer:
