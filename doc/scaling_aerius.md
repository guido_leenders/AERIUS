# Scaling AERIUS

The engine of AERIUS is to make air quality calculations using scientific model implementations, like OPS and SRM2.
These calculations are CPU intensive and can take a long time to run. On the other hand these calculations can easily
be parallelized. The challenge here is to keep the system operational while handling multiple users. It is very easy to
create a system that becomes unusable in a heartbeat because the calculation of a single user blocks all other users.

To manage such a system there are a number of [non-functional requirements](non_functional_requirements.md)
specified. The system should adhere to these requirements. 

## Task manager
The task manager is the central application through which all jobs are passed and the task manager manages the priority 
and workload on the workers. Jobs are organized by the type of work that is performed. For example OPS calculations
are grouped. Each type of work has it's own scheduler in the taskmanager and work independently from other work types.
In the figure below an overview is given of how the clients, workers, queues and scheduler are connected.
![Taskmanager scheduler](taskmanager_scheduler.png) 

The flow of an job is as follows.
1. A client puts a job on the task type queue within the work group. The queue to put the job on depends on the type of
task. For example a web interface calculation for Calculator is put on the queue `aerius.calculator.calculator_ui`.
2. The taskmanagers listens to all worker type queues and takes a message from each queue if one is present.
3. Somewhere else in the universe when a worker starts listing to the worker queue it opens a RabbbitMQ channel for each process
in the worker that can handle jobs. The queue is named `aerius.worker.calculator`. Where `.worker` indicates that it's
a worker queue.
4. The taskmanager counts the number of channels and then knows the capacity available. This check is done with a
fixed interval and the capacity is updated accordingly.
5. When a worker is available the taskmanager scheduler determines which job should be run. This is based on priority
and maximum load for a given worker type.
6. The job is then send to the worker and the taskmanager keeps track of this job.
7. The scheduler unlocks the client queue related to the job type and picks up a single job from the queue again if
available, or just continues listing on the queue till a job becomes available. 
7. If the job is finished the worker sends a reply via the work reply queue, e.g. `aerius.worker.calculator.reply`.
8. The scheduler marks the job as finished and marks the worker as free. A new job can now be scheduled.

### Miscellaneous
The task manager/scheduler is simpler then other schedulers in the sense that it doesn't know which worker will
process which task, as workers just pick up tasks when available. The keeps management simpler as we don't need to track
workers by the taskmanager. Also when a worker crashes the job is put back on the queue and a new worker will pick it
up, instead of having the taskmanager to reschedule the task. This working is based on the assumption that all jobs for
a specific worker type are in general similar in resources required and the resources needed for a worker determine the
system the worker should run on, not the job itself.
