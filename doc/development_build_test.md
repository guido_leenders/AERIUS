# Developing

The AERIUS application is written in JAVA and PostgreSQL. The project is build
with Maven 3.To develop a JAVA development environment, like Eclipse, 
recommended. This page describes the installation of the development
environment, how to build the project and how to test and debug the project.

## Installing a development environment
To start developing several tools need to be installed. This is the list of
tools required to run a development environment: 
[Java 8+ SDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

[Maven 3](http://maven.apache.org/download.cgi)

[Eclipse for Java EE Developers](http://www.eclipse.org/downloads/)

[Google plugin for Eclipse](https://developers.google.com/eclipse/docs/download)

[Ruby 2.4+](https://www.ruby-lang.org/en/downloads/) Only required for building the databases.

To develop with Eclipse read the [Eclipse page](eclipse.md) to import the project
and configuration of Eclipse.  

## Building
The AERIUS project is build with Maven 3. The project can be simply build by
calling `maven install`. This will also run the unit tests, that requires the
test databases. Since some project generate test jar files to build without
running the tests run: `maven install -DskipTests`. This will create the test
jars, but skips running the tests.

It is also possible to build specific products. This can be done by running
maven with the profile option. For example `maven install -Pcalculator`.
See the parent [pom.xml](/pom.xml) for the available profiles. 

## Testing

To test and debug the application the runtime tooling needs to be installed on
the development machine. This requires the same products to be installed as in a 
production environment. Therefore for specific details see the
[production](production.md] page for the installation of those products.

### Unit tests
All products have unit tests. These test can be run with maven. Some tests
require a database. These databases have the convention `unittest_{product name}`.
specific details can be found in the properties files in the test/resoures maps.

### Selenium tests
The web application ui is tested with Selenium/Cucumber. These tests can be 
found in the project [aerius-wui-test](/aerius-wui-test). Selenium
is also used to create the images for the user manual and all tests can be executed 
with maven. More detailed information about the ins and outs of the test set is 
available in [Selenium](/doc/selenium.md).
To run Selenium a complete running AERIUS environment must be present as it 
tests the whole system. This is also the reason the project is not part of the default 
profile in the main maven project.

## Debugging
To test and debug thee application the required AERIUS products must be run. A
complete Calculator test environment would require thw following applications
to run:
- SDM of Calculator
- Tomcat with Calculator
- AERIUS taskmanager
- AERIUS worker

SDM as well as Tomcat configuration is described on the [eclipse](eclipse.md) page.
A taskmanager or worker requires 2 configuration files. A properties file with 
product specific configrations and a log configuration file. For both products a
template/example configuration can be found in the `src/main/config` directory
of the their projects. Test specific properties file can be put into the
`src/test/config` as this directory is put in the gitignore list.

Testing a development deployment on mobile/remote devices has been described on
the [remote testing](remote_testing.md) page.

## Software quality
Several coding styling standards and uniform checkstyle rules, which we 
encourage be adopted to the letter, can be found in the drectory
[resources](/aerius-tools/src/main/resources). There are configuration files for checkstyle,
pmd and sonarqube. The sonarqube is the most up-to-date configuration.
