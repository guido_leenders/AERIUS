/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.road;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.runner.OPSSingleSubstanceRunner;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * <p>Calculate road related sources with OPS complementary with SRM2.</p>
 *
 * <p>An SRM2 calculation is done on receptors that have at least 1 source within 5km of that point. For that point all sources within 5km are taken
 * into the calculation. With OPS the complementary results are calculated here. This means for each receptor if no sources are present within 5km of
 * the receptor all sources are calculated and if at least 1 source is available only the sources > 5km are calculated.</p>
 *
 * <p>For a single calculation chunk in the OpsRoadWorkHandler the sources are split into 2 categories:
 * <ul>
 * <li>All sources that have that have no receptors in the chunk that have sources within <= 5km. These sources are stored in the standard set of
 * sources of the calculation input object.</li>
 * <li>All other sources are stored in the nearby sources list in the calculation input object.</li>
 * </ul>
 *
 * <p>Within the ops runner the following calculations are done:
 * <ul>
 * <li>1 calculation with all sources with no receptors <= 5km and all receptors in the chunk.</li>
 * <li>Next for all nearby sources that have at least 1 receptor within 5km calculate for each receptor the following:
 *    <ul>
 *    <li>Are there any sources within 5km?</li>
 *    <li>If yes do an OPS calculation with all nearby sources > 5km.</li>
 *    <li>If no do an OPS calculation with all nearby sources > 5km.</li>
 *    </ul></li>
 * </ul>
 * <p>When all these calculations are finished add the results of the source with no receptors <= 5km + the nearby sources and this makes up the total
 * of a receptor.</p>
 */
public class OPSRoadRunner {

  private static final Logger LOGGER = LoggerFactory.getLogger(OPSRoadRunner.class);

  /**
   * The single receptor calculations are performed in separate threads. Because each calculation will be short there is a lot of overhead in OPS.
   * Therefore the calculations are threaded to create some kind of pipeline effect.
   */
  private static final int MAX_THREADS = 10;
  private static final int ROAD_RELEVANT_DISTANCE = 5000;
  private static final int ROAD_INCLUDED_DISTANCE = 5000;

  private final OPSSingleSubstanceRunner runner;
  private final ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);

  public OPSRoadRunner(final OPSSingleSubstanceRunner runner) {
    this.runner = runner;
  }

  public void run(final Collection<OPSSource> nearbySources, final Collection<OPSReceptor> receptors,
      final List<AeriusResultPoint> results, final List<Substance> substances, final int year,
      final Map<Substance, EnumSet<EmissionResultKey>> erkMap) {
    final Map<Integer, AeriusResultPoint> resultsMap = new HashMap<>();
    results.forEach(r -> resultsMap.put(r.getId(), r));

    final Map<Integer, SourcesReceptors> sourcesReceptorMap = new HashMap<>();
    receptors.forEach(r -> collectSets(sourcesReceptorMap, nearbySources, r));
    final List<Future<?>> tasks = new ArrayList<>();

    sourcesReceptorMap.forEach((k, v) -> tasks.add(executor.submit(() -> runForReceptor(v, resultsMap, substances, year, erkMap))));
    tasks.forEach(t -> {
      try {
        t.get();
      } catch (InterruptedException | ExecutionException e) {
        Thread.currentThread().interrupt();
        LOGGER.error("OPSRoadRunner run aborted", e);
        executor.shutdown();
      }
    });
  }

  private void collectSets(final Map<Integer, SourcesReceptors> map, final Collection<OPSSource> sources, final OPSReceptor receptor) {
    final int range = sources.stream().anyMatch(s -> isInsideRange(receptor, s)) ? ROAD_INCLUDED_DISTANCE : ROAD_RELEVANT_DISTANCE;
    final List<OPSSource> filteredSources = sources.stream().filter(s -> isOutsideRange(receptor, s, range)).collect(Collectors.toList());
    if (filteredSources.isEmpty()) {
      return;
    }
    final int hash = filteredSources.hashCode();

    if (!map.containsKey(hash)) {
      map.put(hash, new SourcesReceptors(filteredSources));
    }
    map.get(hash).addOPSReceptor(receptor);
  }

  private boolean isInsideRange(final OPSReceptor receptor, final OPSSource s) {
    return receptor.distance(s.getPoint()) <= ROAD_RELEVANT_DISTANCE;
  }

  private boolean isOutsideRange(final OPSReceptor receptor, final OPSSource s, final int range) {
    return receptor.distance(s.getPoint()) > range;
  }

  private void runForReceptor(final SourcesReceptors sr, final Map<Integer, AeriusResultPoint> resultsMap, final List<Substance> substances,
      final int year, final Map<Substance, EnumSet<EmissionResultKey>> erkMap) {
    final Collection<OPSSource> sources = sr.getSources();

    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Run ops for #{} sources on receptor: #{}.", sources.size(), sr.getReceptors().size());
    }
    substances.forEach(s -> runSingleSubstance(sources, sr.getReceptors(), s, year, erkMap.get(s), resultsMap));
  }

  private void runSingleSubstance(final Collection<OPSSource> nearbySources, final Collection<OPSReceptor> receptors, final Substance substance,
      final int year, final Set<EmissionResultKey> erks, final Map<Integer, AeriusResultPoint> resultsMap) {
    try {
      final List<AeriusResultPoint> results = runner.runSingleSubstance(nearbySources, receptors, substance, year, erks);

      if (!results.isEmpty()) {
        results.forEach(r ->
        { final AeriusResultPoint orgPoint = resultsMap.get(r.getId());
        r.getEmissionResults().getHashMap().forEach(
            (k, v) -> orgPoint.setEmissionResult(k, v + orgPoint.getEmissionResult(k)));
        });
      }
    } catch (IOException | AeriusException e) {
      LOGGER.error("Single run of nearby point failed", e);
      throw new RuntimeException(e);
    }
  }

  private static class SourcesReceptors {
    private final Collection<OPSSource> sources;
    private final Collection<OPSReceptor> receptors = new ArrayList<>();

    public SourcesReceptors(final Collection<OPSSource> sources) {
      this.sources = sources;
    }

    public void addOPSReceptor(final OPSReceptor receptor) {
      receptors.add(receptor);
    }

    public Collection<OPSSource> getSources() {
      return sources;
    }

    public Collection<OPSReceptor> getReceptors() {
      return receptors;
    }
  }
}
