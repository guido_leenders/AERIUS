/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.ops.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class to read the V1 OPS brn sources file.
 */
public class BrnFileReaderV1 extends BrnFileReaderV0 {

  private static final int SOURCE_ID_OFFSET = 0;
  private static final int X_OFFSET = 1;
  private static final int Y_OFFSET = 2;
  private static final int EMISSION_OFFSET = 3;
  private static final int HEAT_CONTENT_OFFSET = 4;
  private static final int HEIGHT_OFFSET = 5;
  private static final int DIAMETER_OFFSET = 6;
  private static final int SPREAD_OFFSET = 7;
  private static final int DIURNAL_VARIATION_OFFSET = 8;
  private static final int CATEGORY_OFFSET = 9;
  private static final int AREA_OFFSET = 10;
  private static final int PARTICLE_SIZE_DISTRIBUTION_OFFSET = 11;
  private static final int COMP_OFFSET = 12;
  private static final int NOT_RELEVANT = 0;

  private static final Map<Column, ColumnPosition> COLUMNS_MAP;
  static {
    final Map<Column, ColumnPosition> map = new EnumMap<>(Column.class);
    map.put(Column.SOURCE, new ColumnPosition(SOURCE_ID_OFFSET, NOT_RELEVANT));
    map.put(Column.X, new ColumnPosition(X_OFFSET, NOT_RELEVANT));
    map.put(Column.Y, new ColumnPosition(Y_OFFSET, NOT_RELEVANT));
    map.put(Column.EMISSION, new ColumnPosition(EMISSION_OFFSET, NOT_RELEVANT));
    map.put(Column.HEAT, new ColumnPosition(HEAT_CONTENT_OFFSET, NOT_RELEVANT));
    map.put(Column.HEIGHT, new ColumnPosition(HEIGHT_OFFSET, NOT_RELEVANT));
    map.put(Column.DIAMETER, new ColumnPosition(DIAMETER_OFFSET, NOT_RELEVANT));
    map.put(Column.SPREAD, new ColumnPosition(SPREAD_OFFSET, NOT_RELEVANT));
    map.put(Column.DIURNAL_VARIATION, new ColumnPosition(DIURNAL_VARIATION_OFFSET, NOT_RELEVANT));
    map.put(Column.CATEGORY, new ColumnPosition(CATEGORY_OFFSET, NOT_RELEVANT));
    map.put(Column.AREA, new ColumnPosition(AREA_OFFSET, NOT_RELEVANT));
    map.put(Column.PARTICLE_SIZE_DISTRIBUTION, new ColumnPosition(PARTICLE_SIZE_DISTRIBUTION_OFFSET, NOT_RELEVANT));
    map.put(Column.COMP, new ColumnPosition(COMP_OFFSET, NOT_RELEVANT));
    COLUMNS_MAP = Collections.unmodifiableMap(map);
  }

  private String[] splitCurrentLine;

  public BrnFileReaderV1(final Substance substance) {
    this(COLUMNS_MAP, substance);
  }

  protected BrnFileReaderV1(final Map<Column, ColumnPosition> columns, final Substance substance) {
    super(columns, substance);
  }

  @Override
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    final StringBuilder headerBuffer = new StringBuilder();

    reader.mark(1);
    while (reader.read() == '!') {
      incrementCurrentLineNumber();
      headerBuffer.append('!');
      headerBuffer.append(reader.readLine());
      headerBuffer.append(OSUtils.NL);
      reader.mark(1);
    }
    reader.reset();
    return headerBuffer.toString();
  }

  @Override
  protected OPSSource parseLine(final String line, final List<AeriusException> warnings) {
    splitCurrentLine = line.trim().split("\\s+");
    return super.parseLine(line, warnings);
  }

  @Override
  protected String processColumn(final int index) {
    return index < splitCurrentLine.length ? splitCurrentLine[index] : "";
  }

  @Override
  protected String processColumn(final int index, final int size) {
    return processColumn(index);
  }
}
