/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.ops.domain.OPSConfiguration;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.OPSFileWriter;
import nl.overheid.aerius.ops.road.OPSRoadRunner;
import nl.overheid.aerius.ops.runner.OPSSingleSubstanceRunner;
import nl.overheid.aerius.ops.subreceptor.OPSSubReceptorCalculator;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Handles a full runs of OPS. Includes writing configuration files,
 * instrumenting OPS and reading output from OPS output files. This class can
 * be reused to run multiple runs. In the constructor the location of OPS is
 * initialized and each run is started via {@link #run(int, OPSInputData)}.
 * This class is thread safe.
 */
class RunOPS {

  private static final String OPS_BIN = "/bin";

  private static final OPSTimeLogger OPS_LOGGER = new OPSTimeLogger();
  private static final OPSResultCollector COLLECTOR = new OPSResultCollector();

  private final Validator validator;
  private final OPSSingleSubstanceRunner runner;
  private final OPSRoadRunner roadRunner;
  /**
   * Initializes the class with OPS location information.
   *
   * @param configuration OPS configuration
   * @param keepGeneratedFiles Boolean to determine if any generated files should remain or should be deleted once done.
   */
  public RunOPS(final OPSConfiguration configuration) {
    final File opsRoot = configuration.getOPSRoot();
    if (opsRoot == null || configuration.getRunFilesDirectory() == null) {
      throw new IllegalArgumentException("No argument allowed to be null or empty");
    }
    runner = new OPSSingleSubstanceRunner(new OPSFileWriter(configuration), new File(opsRoot, OPS_BIN), configuration.isKeepGeneratedFiles());
    roadRunner = new OPSRoadRunner(runner);
    final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
    validator = validatorFactory.getValidator();
  }

  /**
   * Perform a OPS run for each substance present in the input data.
   * @param inputData all input values used in a OPS calculation
   * @return the result of an OPS calculation
   * @throws IOException IOException
   * @throws AeriusException AeriusException
   * @throws OPSInvalidVersionException
   */
  public CalculationResult run(final OPSInputData inputData) throws IOException, AeriusException, OPSInvalidVersionException {
    final long startTime = System.nanoTime();

    validateInput(inputData);
    final CalculationResult result = new CalculationResult();
    final List<Substance> supportedSubstances = SubstanceUtil.supportedSubstances(inputData.getSubstances());
    final Map<Substance, EnumSet<EmissionResultKey>> erkMap =
        SubstanceUtil.emissionResultKeys(supportedSubstances, inputData.getEmissionResultKeys());

    result.setResults(COLLECTOR.mergeResults(runAll(inputData, supportedSubstances, erkMap)));
    runRoad(inputData, result, supportedSubstances, erkMap);
    OPS_LOGGER.logDuration(startTime, inputData);
    return result;
  }


  private void runRoad(final OPSInputData inputData, final CalculationResult result, final List<Substance> supportedSubstances,
      final Map<Substance, EnumSet<EmissionResultKey>> erkMap) {
    final Collection<OPSSource> nearbySources = inputData.getNearbySources();

    if (nearbySources == null || nearbySources.isEmpty()) {
      return;
    }
    roadRunner.run(nearbySources, inputData.getReceptors(), result.getResults(), supportedSubstances, inputData.getYear(), erkMap);
  }

  private Map<Substance, List<AeriusResultPoint>> runAll(final OPSInputData inputData, final List<Substance> supportedSubstances,
      final Map<Substance, EnumSet<EmissionResultKey>> erkMap) throws IOException, AeriusException {
    final Map<Substance, List<AeriusResultPoint>> results = new EnumMap<>(Substance.class);

    for (final Substance substance : supportedSubstances) {
      final EnumSet<EmissionResultKey> erks = erkMap.get(substance);

      if (SubstanceUtil.hasEmissionForSubstance(inputData.getEmissionSources(), substance)) {
        final List<AeriusResultPoint> singleSubstanceResults =
            runner.runSingleSubstance(inputData.getEmissionSources(), inputData.getReceptors(), substance, inputData.getYear(), erks);
        final OPSSubReceptorCalculator subReceptorCalculator = createSubReceptorCalculator(inputData, substance, inputData.getYear(), erks);
        subReceptorCalculator.recalculateInHexagonSources(inputData.getEmissionSources(), singleSubstanceResults, substance);
        results.put(substance, singleSubstanceResults);
      } else {
        results.put(substance, emptyResultsForSubstance(inputData.getReceptors(), erks));
      }
    }
    return results;
  }

  private OPSSubReceptorCalculator createSubReceptorCalculator(final OPSInputData inputData, final Substance substance, final int year,
      final Set<EmissionResultKey> erks) {
    return new OPSSubReceptorCalculator(inputData) {

      @Override
      protected List<AeriusResultPoint> recalculate(final List<OPSSource> sources, final ArrayList<OPSReceptor> receptors)
          throws IOException, AeriusException {
        return runner.runSingleSubstance(sources, receptors, substance, year, erks);
      }

      @Override
      protected boolean isValidSource(final OPSSource source, final Substance substance) {
        return SubstanceUtil.hasEmissionForSubstance(source, substance);
      }

    };
  }

  private List<AeriusResultPoint> emptyResultsForSubstance(final Collection<OPSReceptor> receptors, final Set<EmissionResultKey> erks) {
    final List<AeriusResultPoint> emptyResults = new ArrayList<>();
    for (final OPSReceptor opsr : receptors) {
      final AeriusResultPoint rp = new AeriusResultPoint(opsr.getId(), opsr.getPointType(), Math.round(opsr.getX()), Math.round(opsr.getY()));
      for (final EmissionResultKey erk : erks) {
        rp.setEmissionResult(erk, 0.0);
      }
      emptyResults.add(rp);
    }
    return emptyResults;
  }

  private void validateInput(final OPSInputData data) throws AeriusException, OPSInvalidVersionException {
    final List<String> errorMessages = new ArrayList<>();
    OPSVersion.validateVersion(data.getOpsVersion());
    aggregateError(validator.validate(data), errorMessages);
    for (final OPSSource input : data.getEmissionSources()) {
      for (int i = 0; i < input.getSubstances().size(); i++) {
        aggregateError(validator.validate(input), errorMessages);
      }
    }
    if (!errorMessages.isEmpty()) {
      throw new AeriusException(Reason.OPS_INPUT_VALIDATION, StringUtils.join(errorMessages, ','));
    }
  }

  private <C> void aggregateError(final Set<ConstraintViolation<C>> sViolations, final List<String> errorMessages) {
    if (!sViolations.isEmpty()) {
      for (final ConstraintViolation<C> violation : sViolations) {
        errorMessages.add(violation.getPropertyPath() + ": " + violation.getMessage());
      }
    }
  }
}
