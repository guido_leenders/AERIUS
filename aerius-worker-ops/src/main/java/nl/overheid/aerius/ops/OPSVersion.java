/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;

public final class OPSVersion {
  public static final String VERSION = "4.6.2.5";

  private OPSVersion() {
    // Util class.
  }

  /**
   * Validates if the given OPS version matches the expected version by this product. If it doesn't match it throws an
   * {@link OPSInvalidVersionException}.
   *
   * @param opsVersion version to check
   * @throws OPSInvalidVersionException in case OPS version doesn't match
   */
  public static void validateVersion(final String opsVersion) throws OPSInvalidVersionException {
    if (!VERSION.equals(opsVersion)) {
      throw new OPSInvalidVersionException("Data received with version number '" + opsVersion +"', expecting: '" + VERSION + "'.");
    }
  }
}
