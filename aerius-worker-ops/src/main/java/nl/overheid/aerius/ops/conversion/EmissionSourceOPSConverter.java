/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.conversion;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.HasEmission;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.EmissionCalculationMethod;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.shared.domain.sector.SectorProperties;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;

/**
 * Converter class to convert EmissionSource related objects to OPSSource related objects.
 */
public final class EmissionSourceOPSConverter {
  static final String OPS_ID_PREFIX = " (";
  static final String OPS_ID_POSTFIX = ")";

  /**
   * Copies the {@link EmissionSource} object to an {@link OPSSource} object for
   * the given {@link EmissionValueKey} key emission. It only looks at the
   * x, y coordinates and not at the geometry in {@link EmissionSource}. To
   * pass {@link EmissionSource} with lines and such, these must be converted
   * to point sources first to be used here.
   *
   * @param source {@link EmissionSource} object to convert to {@link OPSSource}
   * @param point the x-y location of the point
   * @param emissions the emissions of the point
   * @param keys keys to get the emission values for from the source
   * @return new {@link OPSSource} object
   */
  public List<OPSSource> convert(final EmissionSource source, final Point point, final HasEmission emissions, final List<EmissionValueKey> keys) {
    final OPSSource ops = new OPSSource(source.getId(), point.getSrid(), point.getX(), point.getY());

    OPSCopyFactory.toOpsSource(source.getSourceCharacteristics(), ops);
    for (final EmissionValueKey key : keys) {
      final double emission = emissions.getEmission(key);
      if (emission > 0) {
        ops.setEmission(key.getSubstance(), emission);
      }
    }
    final List<OPSSource> list = new ArrayList<>();
    if (ops.hasEmission()) {
      list.add(ops);
    }
    return list;
  }

  /**
   * Copies the {@link OPSSource} object to an {@link EmissionSource} object with the
   * the given {@link substance} as the EmissionValue.
   * The list of sectors is used to set a sector based on the category number of the OPSsource.
   *
   * @param opsSource The OPS Source object.
   * @param substance The substance to use for the emission value.
   * @param categories The object to use for default values. For instance, the list of sectors is used to pick a sector for the source
   *  (based on category number of the opsSource).
   *  @param useValidSectors if true use the sectors from the database, if false fake the sector by  setting the category as sector id.
   * @return The EmissionSource representation of the OPSSource.
   */
  public static EmissionSource convert(final OPSSource opsSource, final Substance substance, final SectorCategories categories,
      final boolean useValidSectors) {
    final GenericEmissionSource source = new GenericEmissionSource(0, opsSource.getPoint());

    //OPS sources are always point sources.
    source.setGeometry(new WKTGeometry(source.toWKT()));

    source.setEmission(substance, opsSource.getEmission(substance));
    source.setSourceCharacteristics(OPSCopyFactory.toSourceCharacteristics(categories.getDiurnalVariations(), opsSource));
    // based on the category number, try to select a sector for this source.
    source.setSector(getSector(opsSource.getCat(), categories, useValidSectors));

    // As we don't use the OPSSource's ID in an EmissionSource, we retain its information by moving it to the source's label.
    // Loss of information is impossible - as it would violate the laws of quantum dynamics. Thus if we do not do this,
    //  we would effectively destroy the universe.
    source.setLabel(opsSource.getComp() + OPS_ID_PREFIX + opsSource.getId() + OPS_ID_POSTFIX);
    return source;
  }

  private static Sector getSector(final int category, final SectorCategories categories, final boolean useValidSectors) {
    final Sector sector;
    if (useValidSectors) {
      sector = categories == null ? Sector.SECTOR_DEFAULT : categories.determineSectorById(category);
    } else {
      sector = new Sector(category, SectorGroup.OTHER, "",
          new SectorProperties("000000", SectorIcon.OTHER, EmissionCalculationMethod.GENERIC, CalculationEngine.OPS, false));
    }
    return sector;
  }
}
