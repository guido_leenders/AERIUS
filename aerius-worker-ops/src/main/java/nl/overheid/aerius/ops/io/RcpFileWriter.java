/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.util.OSUtils;

/**
 * Class to write OPS receptors to a receptors.rcp file.
 */
final class RcpFileWriter {

  /**
   * @see OPSRunner#createSourceInputFile
   */
  private static final String FILENAME = "receptors.rcp";
  private static final int AVERAGE_ROUGHNESS_RESOLUTION = 6;
  private static final String HEADER = "Nr Name X-coor Y-coor";
  private static final String HEADER_WITH_TERRAIN = HEADER + " AVG-z0 DOM-LU LU1 LU2 LU3 LU4 LU5 LU6 LU7 LU8 LU9";
  // Format: 0 ID Xcoord Ycoord
  private static final String STRING_FORMAT = "%d %s %d %d";
  //Format: 0 ID Xcoord Ycoord z0 Landuse Landuses
  private static final String STRING_FORMAT_WITH_TERRAIN = STRING_FORMAT + " %s %d %s";

  private RcpFileWriter() {
  }

  public static String getFileName() {
    return FILENAME;
  }

  /**
   * @param path File containing the path to create the receptor file in.
   * @param opsReceptors The receptors to write in the file (according to OPS-format).
   * @throws IOException When an error occurs writing to the file.
   */
  public static void writeFile(final File path, final Collection<OPSReceptor> opsReceptors) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(new File(path, FILENAME).toPath(), StandardCharsets.UTF_8)) {
      if (opsReceptors.isEmpty()) {
        writer.write(HEADER_WITH_TERRAIN + OSUtils.NL);
        return;
      }

      if (opsReceptors.iterator().next().hasNoTerrainData()) {
        writer.write(HEADER + OSUtils.NL);
        for (final OPSReceptor opsReceptor : opsReceptors) {
          writer.write(String.format(Locale.US, STRING_FORMAT + OSUtils.NL, 0,
              OPSReceptorCodeUtil.getCodeFromAeriusPoint(opsReceptor),
              opsReceptor.getRoundedX(), opsReceptor.getRoundedY()));
        }
      } else {
        writer.write(HEADER_WITH_TERRAIN + OSUtils.NL);
        for (final OPSReceptor opsReceptor : opsReceptors) {
          writer.write(String.format(Locale.US, STRING_FORMAT_WITH_TERRAIN + OSUtils.NL, 0,
              OPSReceptorCodeUtil.getCodeFromAeriusPoint(opsReceptor),
              opsReceptor.getRoundedX(), opsReceptor.getRoundedY(),
              getAverageRoughnessAsString(opsReceptor),
              opsReceptor.getLandUse().getOption(),
              getLandUsesAsString(opsReceptor)));
        }
      }
    }
  }

  private static String getAverageRoughnessAsString(final OPSReceptor opsReceptor) {
    return BigDecimal.valueOf(opsReceptor.getAverageRoughness()).setScale(AVERAGE_ROUGHNESS_RESOLUTION, RoundingMode.HALF_UP).toString();
  }

  private static String getLandUsesAsString(final OPSReceptor opsReceptor) {
    int[] landUses = opsReceptor.getLandUses();
    if (landUses == null) {
      // Creates an array with 9 occurrences of 0.
      landUses = new int[SharedConstants.OPS_LAND_USE_CLASSES];
      //set the correct column to represent 100% of the landuse. Landuse starts at 1 however.
      landUses[opsReceptor.getLandUse().getOption() - 1] = SharedConstants.PERCENTAGE_TO_FRACTION;
    }

    final List<String> percentages = new ArrayList<>(landUses.length);
    for (final int landUse : landUses) {
      percentages.add(String.valueOf(landUse));
    }

    return StringUtils.join(percentages, ' ');
  }
}
