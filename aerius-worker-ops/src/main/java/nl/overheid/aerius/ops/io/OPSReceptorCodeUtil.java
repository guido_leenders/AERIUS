/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;

/**
 * Util class to identify receptor via a code in OPS receptor files.
 */
final class OPSReceptorCodeUtil {

  private static final char RECEPTOR_PREFIX = 'R';
  private static final char POINT_PREFIX = 'P';

  /**
   * Util class.
   */
  private OPSReceptorCodeUtil() {
    // util class.
  }

  public static void setAeriusPointFromCode(final AeriusPoint ap, final String idCode) {
    if (!StringUtils.isEmpty(idCode)) {
      final boolean pp = idCode.charAt(0) == POINT_PREFIX;
      final boolean rp = idCode.charAt(0) == RECEPTOR_PREFIX;
      ap.setPointType(pp ? AeriusPointType.POINT : AeriusPointType.RECEPTOR);
      ap.setId(Integer.valueOf(pp || rp ? (idCode.length() > 1 ? idCode.substring(1) : "0") : idCode));
    }
  }

  public static String getCodeFromAeriusPoint(final AeriusPoint point) {
    final char prefix;
    if (point.getPointType() == AeriusPointType.POINT) {
      prefix = POINT_PREFIX;
    } else {
      prefix = RECEPTOR_PREFIX;
    }
    return prefix + String.valueOf(point.getId());
  }
}
