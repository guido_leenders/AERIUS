/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.domain;

import java.util.Locale;

/**
 * Enum with OPS supported grid sizes.
 */
public enum GridSize {
  /**
   * 250 meter.
   */
  SIZE_250("250", 250),
  /**
   * 500 meter.
   */
  SIZE_500("500", 500),
  /**
   * 1000 meter.
   */
  SIZE_1000("1000", 1000),
  /**
   * 2000 meter.
   */
  SIZE_2000("2000", 2000),
  /**
   * 5000 meter.
   */
  SIZE_5000("5000", 5000),
  /**
   * None.
   */
  NONE("none", 0);

  private final int gridSizeTypeValue;
  private final String gridSizeTypeString;

  private GridSize(final String gridSizeTypeString, final int gridSizeTypeValue) {
    this.gridSizeTypeString = gridSizeTypeString;
    this.gridSizeTypeValue = gridSizeTypeValue;
  }

  public static GridSize safeValueOf(final String value) {
    try {
      return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
    } catch (final IllegalArgumentException e) {
      return null;
    }
  }

  public int getGridSizeTypeValue() {
    return gridSizeTypeValue;
  }

  public String getGridSizeTypeString() {
    return gridSizeTypeString;
  }

  @Override
  public String toString() {
    return gridSizeTypeString;
  }
}
