/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.runner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.ops.OPSResultCollector;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSRetryException;
import nl.overheid.aerius.ops.exception.OPSRunDirectoryExistsException;
import nl.overheid.aerius.ops.io.OPSFileWriter;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.FileUtil;
import nl.overheid.aerius.worker.ExecuteParameters;
import nl.overheid.aerius.worker.ExecuteWorker;

public class OPSSingleSubstanceRunner {

  private static final Logger LOGGER = LoggerFactory.getLogger(OPSSingleSubstanceRunner.class);

  private static final int MAX_RUN_RETRIES = 5;
  private static final int RETRY_WAIT_MILLISECONDS = 10;

  private static final String OPS_RUN = "/";
  private static final String EXECUTABLE_NAME = "ops";
  private static final String VERBOSE_FLAG = "-v";
  private static final String CONTROL_FILE_FLAG = "-i";
  private static final String TERRAIN_DATA_FLAG = "-perc";

  private static final OPSResultCollector COLLECTOR = new OPSResultCollector();

  private final OPSFileWriter opsFileWriter;
  private final File opsBinDir;
  private final boolean keepGeneratedFiles;

  public OPSSingleSubstanceRunner(final OPSFileWriter opsFileWriter, final File opsBinDir, final boolean keepGeneratedFiles) {
    this.opsFileWriter = opsFileWriter;
    this.opsBinDir = opsBinDir;
    this.keepGeneratedFiles = keepGeneratedFiles;
  }

  /**
   * Performs the actual run of OPS. Including creating a directory for the
   * configuration and output files, generating the configuration files,
   * reading the OPS output and cleaning up the files if execution was
   * successful.
   *
   * @param sources the sources to run
   * @param receptors the receptors to run
   * @param substance the substance to run
   * @param year the year to run
   * @param erks the emission result keys to get the results for
   * @return output data of this run
   * @throws IOException when reading/writing files failed
   * @throws OPSException when OPS internally failed
   * @throws AeriusException
   */
  public List<AeriusResultPoint> runSingleSubstance(final Collection<OPSSource> sources, final Collection<OPSReceptor> receptors,
      final Substance substance, final int year, final Set<EmissionResultKey> erks) throws IOException, AeriusException {
    //Run ID is an unique id referencing to this specific run
    final String runId = UUID.randomUUID().toString();
    final String runIdFilesDirectory = OPS_RUN + runId + OPS_RUN;
    final File physicalRunFilesDirectory = opsFileWriter.getRunDirectory(runIdFilesDirectory);
    // check if directory exists
    if (physicalRunFilesDirectory.exists()) {
      // Don't delete the folder if it exists, but throw an exception.
      // This should prevent folders from being deleted while a previous run is still happening.
      // It should leave folders for 'error-generating-runs' intact as well,
      // which would allow for better examination of errors, but might also result in running out of disk-space.
      // decided to leave existing folders around forever (until manual deletion/automatic deletion by system)
      throw new OPSRunDirectoryExistsException(physicalRunFilesDirectory.toString());
    }
    // create run folder.
    if (!physicalRunFilesDirectory.mkdirs()) {
      throw new NoSuchFileException("Directory '" + physicalRunFilesDirectory + "' could not be created.");
    }

    // write files to run folder
    opsFileWriter.writeFiles(runId, sources, receptors, substance, year);
    // construct the execute worker.
    final ExecuteWorker executeWorker = constructExecuteWorker(runId, receptors);

    // perform the actual ops run.
    performRun(runId, physicalRunFilesDirectory, executeWorker);

    // Collect the ops results.
    final List<AeriusResultPoint> results;
    try {
      results = COLLECTOR.collectResults(physicalRunFilesDirectory, substance, erks);
    } catch (final FileNotFoundException e) {
      LOGGER.error("OPS result file not found, probably not generated? location: {}", physicalRunFilesDirectory, e);
      throw new AeriusException(Reason.OPS_INTERNAL_EXCEPTION, "OPS didn't finish correctly.");
    }
    // remove the ops results
    if (!keepGeneratedFiles) {
      FileUtil.removeDirectoryRecursively(physicalRunFilesDirectory.toPath());
    }
    return results;
  }

  private ExecuteWorker constructExecuteWorker(final String runId, final Collection<OPSReceptor> receptors) {
    final List<String> parameters = new ArrayList<>();
    //Order here is important: OPS expects the arguments in a certain order (switching terrain flag with verbose flag for instance won't work).
    parameters.add(VERBOSE_FLAG);
    parameters.add(CONTROL_FILE_FLAG);
    parameters.add(opsFileWriter.getSettingsLocation(runId));
    if (!receptors.isEmpty() && receptors.stream().findFirst().get().hasTerrainData()) {
      parameters.add(TERRAIN_DATA_FLAG);
    }
    final ExecuteParameters executeParameters = new ExecuteParameters(EXECUTABLE_NAME, parameters.toArray(new String[parameters.size()]));
    return new ExecuteWorker(executeParameters, opsBinDir.getAbsolutePath());
  }

  private void performRun(final String runId, final File physicalRunFilesDirectory, final ExecuteWorker executeWorker)
      throws IOException, AeriusException {
    int runTries = 1;
    try {
      //When run concurrently, OPS can run into troubles reading it's own files.
      //To avoid crashing the calculation, we're retrying a couple of times.
      while (runTries <= MAX_RUN_RETRIES) {
        try {
          executeWorker.run(runId, physicalRunFilesDirectory);
          //check if there are any error files.
          ResultDataFileReader.checkForErrors(physicalRunFilesDirectory);
          break;
        } catch (final OPSRetryException e) {
          LOGGER.error("Error while running OPS. This was try: {}. Errormsg: {}", runTries, e.getArgs());
          if (runTries >= MAX_RUN_RETRIES) {
            LOGGER.error("Finished trying, throwing an IOException so this worker dies.");
            //throwing an IOException instead so the worker dies instead of the calculation. Another worker can probably handle it.
            throw new IOException(e);
          } else {
            ResultDataFileReader.clearErrorFile(physicalRunFilesDirectory);
            Thread.sleep(TimeUnit.MILLISECONDS.toMillis(RETRY_WAIT_MILLISECONDS));
            runTries++;
          }
        }
      }
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
      LOGGER.error("RunOPS#performRun", e);
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }
}
