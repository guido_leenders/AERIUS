/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.overheid.aerius.io.AbstractLineReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.exception.OPSRetryException;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Reader for the OPS results file.
 */
public class ResultDataFileReader extends AbstractLineReader<AeriusResultPoint> {

  /**
   * Default name of the result file for OPS.
   */
  public static final String FILE_NAME = "result.tab";
  /**
   * Default name of the error file for OPS.
   * If present, it indicates errors when running OPS.
   */
  public static final String ERR_FILE_NAME = "settings.err";

  protected static final int DEFAULT_NUM_HEADER_LINES = 3;

  private static final String NAME_COLUMN_NAME = "Name";
  private static final String X_COLUMN_NAME = "X-coordinate";
  private static final String Y_COLUMN_NAME = "Y-coordinate";
  private static final String EMISSION_RESULT_COLUMN_NAME = "EmissionResult";

  private static final int NAME_OFFSET = 0;
  private static final int NAME_SIZE = 12;
  private static final int X_OFFSET = 12;
  private static final int X_SIZE = 8;
  private static final int Y_OFFSET = 20;
  private static final int Y_SIZE = 8;

  private static final Pattern RETRY_ERROR_REGEX = Pattern.compile("Io-status += +30");

  protected File inputFile;
  private Set<EmissionResultKey> keys;

  @Override
  public LineReaderResult<AeriusResultPoint> readObjects(final InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException("You can use this method, use readObjects with path, substance and EmissionResultKey");
  }

  public LineReaderResult<AeriusResultPoint> readObjects(final File path, final Substance substance, final EmissionResultKey erk)
      throws IOException {
    return readObjects(path, substance, EnumSet.of(erk));
  }

  /**
   * Reads the OPS results from the default filename at the given location and returns
   * a list of {@link OPSResult}s. Each item represents the result of one receptor.
   *
   * @param path Path to the location of the OPS results file
   * @param keys The emission result types to read.
   * @return List of results
   * @throws IOException in case of errors while reading the result file.
   */
  public LineReaderResult<AeriusResultPoint> readObjects(final File path, final Substance substance, final Set<EmissionResultKey> keys)
      throws IOException {
    this.inputFile = new File(path, FILE_NAME);
    this.keys = keys;
    try (final InputStream fis = new FileInputStream(inputFile);
        final InputStreamReader reader = new InputStreamReader(fis, StandardCharsets.UTF_8.name())) {
      return readObjects(reader, getNumHeaderLines(), getMaxLineNumber(), false);
    }
  }

  protected int getNumHeaderLines() throws IOException {
    return DEFAULT_NUM_HEADER_LINES;
  }

  protected int getMaxLineNumber() throws IOException {
    return Integer.MAX_VALUE;
  }

  /**
   * Reads one line in the OPS results file and returns the result of one receptor.
   */
  @Override
  protected AeriusResultPoint parseLine(final String line, final List<AeriusException> warnings) {
    final AeriusResultPoint arp = new AeriusResultPoint();
    OPSReceptorCodeUtil.setAeriusPointFromCode(arp, getIdAndCode());
    arp.setX(getX());
    arp.setY(getY());
    for (final EmissionResultKey key: keys) {
      arp.setEmissionResult(key, getEmissionResult(key));
    }
    return arp;
  }

  private String getIdAndCode() {
    return getString(NAME_COLUMN_NAME, NAME_OFFSET, NAME_SIZE);
  }

  protected int getX() {
    return getInt(X_COLUMN_NAME, X_OFFSET, X_SIZE);
  }

  protected int getY() {
    return getInt(Y_COLUMN_NAME, Y_OFFSET, Y_SIZE);
  }

  private Double getEmissionResult(final EmissionResultKey key) {
    // OPS can return values below 0; these values are rounded to 0.
    final ResultColumn rc = ResultColumn.getResultColumnFromResultType(key);
    return Math.max(0, getDouble(EMISSION_RESULT_COLUMN_NAME, rc.getOffset(), rc.getLength()));
  }

  /**
   * Checks if the OPS generated error file (settings.err) exists on the path
   * and throws an OPSException with the content of that error file.
   *
   * @param path Path to the location of the OPS results file
   * @throws IOException when the file exists but reading the error file failed
   * @throws AeriusException when the file exists, contains information contained in the file if it could be read.
   */
  public static void checkForErrors(final File path) throws IOException, AeriusException {
    final File errFile = new File(path, ERR_FILE_NAME);
    boolean retryError = false;

    if (errFile.exists()) {
      try (final FileReader fr = new FileReader(errFile)) {
        try (final BufferedReader br = new BufferedReader(fr)) {
          final StringBuilder sb = new StringBuilder();

          String line;
          while ((line = br.readLine()) != null) {
            sb.append(line);
            final Matcher matcher = RETRY_ERROR_REGEX.matcher(line);
            if (matcher.find()) {
              retryError = true;
            }
          }
          if (retryError) {
            throw new OPSRetryException(Reason.OPS_INTERNAL_EXCEPTION, sb.toString());
          } else {
            throw new AeriusException(Reason.OPS_INTERNAL_EXCEPTION, sb.toString());
          }
        }
      }
    }
  }

  /**
   * @param path The path to clear the error file for.
   */
  public static void clearErrorFile(final File path) {
    final File errFile = new File(path, ERR_FILE_NAME);
    if (errFile.exists()) {
      errFile.delete();
    }
  }
}
