/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.worker.WorkerConfiguration;

/**
 * OPS worker configuration options.
 */
class OPSWorkerConfiguration extends WorkerConfiguration {

  private static final String OPS_WORKER_PREFIX = "ops";
  private static final String OPS_ROOT = "root";
  private static final String OPS_RUNFILES_DIRECTORY = "runfiles.directory";
  private static final String OPS_KEEPGENERATEDFILES = "keepgeneratedfiles";
  private static final String OPS_SETTINGS_METEOFILE = "settings.meteofile";
  private static final String OPS_SETTINGS_LANDUSE = "settings.landuse";

  /**
   * Configuration for OPS-related workers.
   * @param properties Properties containing the predefined properties.
   */
  public OPSWorkerConfiguration(final Properties properties) {
    super(properties, OPS_WORKER_PREFIX);
  }

  @Override
  protected List<String> getValidationErrors() {
    final List<String> reasons = new ArrayList<>();
    if (getProcesses() > 0) {
      validateDirectoryProperty(OPS_ROOT, getOPSRoot(), reasons, false);
      validateDirectoryProperty(OPS_RUNFILES_DIRECTORY, reasons, true);
    }
    return reasons;
  }

  /**
   * @return The directory path containing the OPS installation.
   */
  public File getOPSRoot() {
    final String root = getProperty(OPS_ROOT);
    if (root == null) {
      return null;
    }
    return new File(root, OPSVersion.VERSION);
  }

  /**
   * @return The directory where the files used by OPS can be (temporarily) stored.
   */
  public File getOPSRunFilesDirectory() {
    final String directory = getProperty(OPS_RUNFILES_DIRECTORY);
    return directory == null ? null : new File(directory);
  }

  /**
   * @return If true doesn't delete the generated input and output files after an OPS run.
   */
  public boolean isOPSKeepGeneratedFiles() {
    return getPropertyBooleanSafe(OPS_KEEPGENERATEDFILES);
  }

  public String getSettingsMeteoFile() {
    return getProperty(OPS_SETTINGS_METEOFILE);
  }

  public String getSettingsLandUse() {
    return getProperty(OPS_SETTINGS_LANDUSE);
  }
}
