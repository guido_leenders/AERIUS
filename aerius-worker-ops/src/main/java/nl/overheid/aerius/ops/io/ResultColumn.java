/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * The different columns in the OPS result file.
 */
enum ResultColumn {

  /**
   * The (main) concentration column.
   */
  CONCENTRATION(EmissionResultType.CONCENTRATION, 28, 12),
  /**
   * The dry deposition column. DO NOT USE WITH PM10.
   */
  DRY_DEPOSITION(EmissionResultType.DRY_DEPOSITION, 40, 12),
  /**
   * The wet deposition column. DO NOT USE WITH PM10.
   */
  WET_DEPOSITION(EmissionResultType.WET_DEPOSITION, 52, 12),
  /**
   * The total deposition column. DO NOT USE WITH PM10.
   */
  TOTAL_DEPOSITION(EmissionResultType.DEPOSITION, 64, 12);

  private final int offset;
  private final int length;
  private final EmissionResultType emissionResultType;

  private ResultColumn(final EmissionResultType emissionResultType, final int offset, final int length) {
    this.emissionResultType = emissionResultType;
    this.offset = offset;
    this.length = length;
  }

  /**
   * Returns the {@link ResultColumn} from the {@link EmissionResultType}.
   * @return the ResultColumn
   */
  public static ResultColumn getResultColumnFromResultType(final EmissionResultKey erk) {
    for (final ResultColumn rc : values()) {
      if (erk != null && rc.emissionResultType == erk.getEmissionResultType()) {
        return rc;
      }
    }
    return null;
  }

  public int getOffset() {
    return offset;
  }

  public int getLength() {
    return length;
  }

}
