/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Import reader for OPS receptor files.
 */
public class RcpImportReader implements ImportReader {

  private static final String RCP_FILE_EXTENSION = ".rcp";

  private final boolean useImportedLandUses;

  public RcpImportReader(final boolean useImportedLandUses) {
    this.useImportedLandUses = useImportedLandUses;
  }

  @Override
  public boolean detect(final String filename) {
    return filename.toLowerCase(Locale.ENGLISH).endsWith(RCP_FILE_EXTENSION);
  }

  @Override
  public void read(final String filename, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportResult importResult) throws IOException, AeriusException {
    final RcpFileReader reader = new RcpFileReader();
    reader.setReadTerrainData(useImportedLandUses);

    final LineReaderResult<OPSReceptor> results = reader.readObjects(inputStream);
    final List<OPSReceptor> receptors = results.getObjects();
    final CalculationPointList calculationPoints = new CalculationPointList();

    if (useImportedLandUses) {
      // By using setAll the read id's on the receptors are maintained. When the name was an integer value it will be used as int.
      calculationPoints.setAll(receptors);
    } else {
      for (final OPSReceptor r : receptors) {
        final AeriusPoint ap = new AeriusPoint(r.getId(), AeriusPointType.POINT, r.getX(), r.getY());
        ap.setLabel(r.getLabel());
        calculationPoints.add(ap);
      }
    }
    importResult.setCalculationPoints(calculationPoints);
    importResult.getExceptions().addAll(results.getExceptions());
  }
}
