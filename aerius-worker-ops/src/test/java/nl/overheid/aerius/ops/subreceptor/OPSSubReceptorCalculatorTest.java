/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.subreceptor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.ops.OPSVersion;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link OPSSubReceptorCalculator}.
 */
public class OPSSubReceptorCalculatorTest {

  @Test
  public void testShouldRecalculate() {
    final TestOPSSubReceptorCalculator subReceptorCalculator = createTestOPSSubReceptorCalculator();
    final OPSSource source = new OPSSource(1, RDNew.SRID, 1000, 1000);
    final AeriusResultPoint receptor1 = new AeriusResultPoint(0, 0);
    final AeriusResultPoint receptor2 = new AeriusResultPoint(999, 1001);
    final AeriusResultPoint receptor3 = new AeriusResultPoint(1000, 1000);
    final AeriusResultPoint receptor4 = new AeriusResultPoint(1020, 1000);
    final AeriusResultPoint receptor5 = new AeriusResultPoint(-1000, 1000);
    assertFalse("Way off receptor", subReceptorCalculator.shouldRecalculate(source, receptor1));
    assertTrue("Near enough receptor", subReceptorCalculator.shouldRecalculate(source, receptor2));
    assertTrue("Same location receptor", subReceptorCalculator.shouldRecalculate(source, receptor3));
    assertFalse("Boundary case receptor", subReceptorCalculator.shouldRecalculate(source, receptor4));
    assertFalse("Random negative coordinate receptor", subReceptorCalculator.shouldRecalculate(source, receptor5));
  }

  @Test
  public void testRecalculate() throws IOException, AeriusException {
    final TestOPSSubReceptorCalculator subReceptorCalculator = createTestOPSSubReceptorCalculator();
    final OPSSource nearbySource = new OPSSource();
    final ArrayList<OPSReceptor> subPoints = new ArrayList<>();
    subReceptorCalculator.recalculate(nearbySource, subPoints);
    assertTrue("The recalculate method should be called", subReceptorCalculator.recalculated);
  }

  private TestOPSSubReceptorCalculator createTestOPSSubReceptorCalculator() {
    return new TestOPSSubReceptorCalculator(new OPSInputData(OPSVersion.VERSION, OPSTestUtil.ZOOM_LEVEL_1_SURFACE));
  }

  @Test
  public void testRecalculateSubReceptorSources() throws IOException, AeriusException {
    final OPSInputData inputData = new OPSInputData(OPSVersion.VERSION, OPSTestUtil.ZOOM_LEVEL_1_SURFACE);
    final TestOPSSubReceptorCalculator subReceptorCalculator = new TestOPSSubReceptorCalculator(inputData);
    final ArrayList<OPSSource> nearbySources = new ArrayList<>();
    final OPSSource source = new OPSSource(1, RDNew.SRID, 1000, 1000);
    nearbySources.add(source);

    final ArrayList<AeriusResultPoint> singleSubstanceResults = new ArrayList<>();
    final AeriusResultPoint receptor = new AeriusResultPoint(999, 1001);
    singleSubstanceResults.add(receptor);

    final ArrayList<OPSReceptor> opsReceptors = new ArrayList<>();
    opsReceptors.add(new OPSReceptor(receptor));

    inputData.setEmissionSources(nearbySources);
    inputData.setReceptors(opsReceptors);

    subReceptorCalculator.recalculateInHexagonSources(nearbySources, singleSubstanceResults, Substance.NH3);
    assertTrue("The recalculate method should be called", subReceptorCalculator.recalculated);
  }

  private static class TestOPSSubReceptorCalculator extends OPSSubReceptorCalculator {

    private boolean recalculated;

    public TestOPSSubReceptorCalculator(final OPSInputData inputData) {
      super(inputData);
    }

    @Override
    protected boolean isValidSource(final OPSSource sources, final Substance substance) {
      return true;
    }

    @Override
    protected List<AeriusResultPoint> recalculate(final List<OPSSource> sources, final ArrayList<OPSReceptor> receptors)
        throws IOException, AeriusException {
      recalculated = true;
      return new ArrayList<>();
    }

  }

}
