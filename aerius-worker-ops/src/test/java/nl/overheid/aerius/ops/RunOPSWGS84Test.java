/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static nl.overheid.aerius.ops.util.ResultDataFileReaderHelper.assertResults;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;

@RunWith(Parameterized.class)
public class RunOPSWGS84Test extends BaseRunOPSTest {
  private static final String RESOURCES_DIRECTORY = "runops_wgs84/";
  private static final String RECEPTOR_FILE = "receptors.rcp";
  private static final String EMISSION_SOURCE_FILE = "emissions.brn";
  private static final String EXPECTED_RESULT_FILE = ResultDataFileReader.FILE_NAME;

  @Parameter(value = 0)
  public String testFolder;
  @Parameter(value = 1)
  public Substance substance;

  @Parameters(name = "emission file: {0}")
  public static List<Object[]> data() {
    return Arrays.asList(new Object[] {"testcase_road_11/", Substance.NOX}, new Object[] {"testcase_farm_17/", Substance.NH3});
  }

  @Test
  public void testRunWGS84Points() throws FileNotFoundException, IOException, AeriusException, OPSInvalidVersionException {
    final String testResourcesPath = RESOURCES_DIRECTORY + testFolder;
    final String sourcesFile = testResourcesPath + EMISSION_SOURCE_FILE;
    final String receptorFile = testResourcesPath + RECEPTOR_FILE;
    final EmissionResultKey erk = EmissionResultKey.valueOf(substance);
    final OPSInputData inputData = OPSTestUtil.getInputTestData(erk, YEAR, RunOPSWGS84Test.class, sourcesFile, receptorFile);

    setSources(inputData, sourcesFile, substance);
    setReceptors(inputData, receptorFile);
    final CalculationResult result = runOPS.run(inputData);
    final String message = "EPSG";
    final String resultFile = testResourcesPath + EXPECTED_RESULT_FILE;
    assertResults(RunOPSWGS84Test.class, message, new ResultDataFileReader(), resultFile, erk, result.getResults(), true);
  }

}
