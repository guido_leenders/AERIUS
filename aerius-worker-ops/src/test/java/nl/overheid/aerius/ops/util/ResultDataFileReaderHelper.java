/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.util;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Helper class for reading OPS reference result files and comparing results.
 */
public final class ResultDataFileReaderHelper {
  private static final Logger LOG = LoggerFactory.getLogger(ResultDataFileReaderHelper.class);

  // 4 significant digits
  private static final double OPS_PRECISION = 1e3;

  // Not to be constructed.
  private ResultDataFileReaderHelper() {}

  /**
   * Reads the results and asserts if the results are the same as the reference file.
   *
   * @param clazz Reference class to use as base path for reference file location.
   * @param message Assert message prefix
   * @param reader The result reader that can read the substance specific results file
   * @param referenceFile the name of the reference file
   * @param erk the substance key to for which the results are
   * @param results The results from the ops run.
   * @param failOnResultDiff TODO
   * @return
   * @throws IOException
   */
  public static List<AssertionError> assertResults(final Class<?> clazz, final String message, final ResultDataFileReader reader,
      final String referenceFile, final EmissionResultKey erk, final List<AeriusResultPoint> results, final boolean failOnResultDiff)
          throws IOException {
    final LineReaderResult<AeriusResultPoint> references = reader.readObjects(new File(clazz.getResource(referenceFile).getFile()).getParentFile(),
        erk.getSubstance(), erk);
    if (!references.getExceptions().isEmpty()) {
      references.getExceptions().forEach(e -> LOG.error("Error reading reference input file:{}", references, e));
      fail("Errors reading reference input file: " + references);
    }
    return testResults(message, references.getObjects(), results, erk, failOnResultDiff);
  }

  private static List<AssertionError> testResults(final String message, final List<AeriusResultPoint> expected, final List<AeriusResultPoint> results,
      final EmissionResultKey key, final boolean failOnResultDiff) {
    Assert.assertEquals(message + "#readResults:size", expected.size(), results.size());
    final List<AssertionError> errors = new ArrayList<>();

    for (int i = 0; i < expected.size(); i++) {
      final AeriusResultPoint res = results.get(i);
      final AeriusResultPoint exp = expected.get(i);

      Assert.assertEquals(message + "#readResults:id", exp.getId(), res.getId());
      Assert.assertEquals(message + "#readResults:x", exp.getRoundedX(), res.getRoundedX());
      Assert.assertEquals(message + "#readResults:y", exp.getRoundedY(), res.getRoundedY());

      try {
        Assert.assertEquals(message + "#readResults:totalNxx", exp.getEmissionResult(key), res.getEmissionResult(key),
            exp.getEmissionResult(key) / OPS_PRECISION);
      } catch (final AssertionError e) {
        if (failOnResultDiff) {
          throw e;
        } else {
          errors.add(e);
          LOG.error("Output differs: {}", e.getMessage());
        }
      }
    }
    return errors;
  }
}
