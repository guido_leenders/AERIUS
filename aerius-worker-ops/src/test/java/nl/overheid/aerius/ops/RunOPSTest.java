/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.exception.OPSInvalidVersionException;
import nl.overheid.aerius.ops.io.RcpFileReader;
import nl.overheid.aerius.ops.io.ResultDataFileReader;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.ops.OPSLimits;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *  Test class for RunOPS.
 *  All tests will still work without OPS installed in the right location
 *  (see worker.properties file in src/test/resources), they are just won't test anything.
 */
public class RunOPSTest extends BaseRunOPSTest {

  private static final String EXPECTED_NOX = "runopsNOx/" + ResultDataFileReader.FILE_NAME;

  @Ignore("Since ops 4.5.2 this error isn't triggered. For now ignore this test.")
  @Test(expected = IOException.class)
  public void testRunOPSLockingFile() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    try (final Writer writer = Files.newBufferedWriter(
        new File(getConfig().getOPSRoot(), "meteo/m095104c.001").toPath(), StandardCharsets.UTF_8, StandardOpenOption.WRITE)) {
      final ResultDataFileReader reader = new ResultDataFileReader();

      assertRunOPS(reader, EmissionResultKey.NOX_DEPOSITION, EXPECTED_NOX, false, true);
    }
  }

  @Test(expected = AeriusException.class)
  public void testRunFailedOPS() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    final OPSInputData data = getInputTestData(EmissionResultKey.NOX_DEPOSITION);
    // Diurnal variation 9 does not work (arbitrarily chosen value).
    ((ArrayList<OPSSource>) data.getEmissionSources()).get(0).setDiurnalVariation(9);
    runOPS.run(data);
  }

  /**
   * Test if OPS triggers an error if to many input objects are passed to OPS. This test only runs when a new version
   * of OPS is used, because the test can take up-to 10 minutes and only tests the limits of OPS.
   *
   * If a new version of OPS is installed this test should be run at least once before updating the OPS version number here
   * to be sure the test still runs as expected or possible if any input can be handled this test can be removed.
   * @throws OPSInvalidVersionException
   */
  @Test
  public void runStackOverflowOPSTest() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    if (!config.getOPSRoot().toString().contains("4.6.2.2")) {
      final OPSInputData data = getInputTestData(EmissionResultKey.NH3_DEPOSITION);
      try (InputStream inputStream = getClass().getResourceAsStream("stackoverflow/" + "receptors.rcp")) {
        final LineReaderResult<OPSReceptor> receptors = new RcpFileReader().readObjects(inputStream);
        data.setReceptors(receptors.getObjects());
      }
      try {
        runOPS.run(data);
        fail("The OPS stackoverflow unit test didn't throw an exception, so this new version seems improved!");
      } catch (final AeriusException e) {
        assertSame("Reason should be ops internal exception", Reason.OPS_INTERNAL_EXCEPTION, e.getReason());
      }
    }
  }

  @Test
  public void testFileRemainsAfterFailedOPS() throws IOException, InterruptedException, OPSInvalidVersionException {
    final File resultDirectory = config.getRunFilesDirectory();
    final int numberOfFilesBeforeRun = resultDirectory.listFiles().length;
    try {
      testRunFailedOPS();
    } catch (final AeriusException e) {
      //expected exception
    }
    Assert.assertEquals("Number of files/directories in" + resultDirectory, numberOfFilesBeforeRun + 1, resultDirectory.listFiles().length);
  }

  @Test
  public void testRunOPSMultipleTimesAtSameTime() throws Throwable {
    final EmissionResultKey key = EmissionResultKey.NH3_DEPOSITION;
    final OPSInputData data = getInputTestData(key);
    final Set<Callable<Boolean>> tasks = new HashSet<>();
    final int numberOfTasks = 10;
    for (int i = 0; i < numberOfTasks; i++) {
      tasks.add(new OPSRunCallable(runOPS, data));
    }
    final ExecutorService service = Executors.newFixedThreadPool(10);
    final List<Future<Boolean>> results = service.invokeAll(tasks);
    int validResults = 0;
    try {
      for (final Future<Boolean> result : results) {

        // wait for all results to be in.
        if (result.get()) {
          validResults++;
        }
      }
      Assert.assertEquals("Right results", numberOfTasks, validResults);
    } catch (final ExecutionException e) {
      throw e.getCause();
    } finally {
      service.shutdownNow();
    }
  }

  @Test
  public void testRunOPSInvalidDataNH3() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSInvalidData(EmissionResultKey.NH3_DEPOSITION);
  }

  @Test
  public void testRunOPSInvalidDataNOX() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSInvalidData(EmissionResultKey.NOX_DEPOSITION);
  }

  @Ignore("Disable pm10 calculations because the results are not representative.")
  @Test
  public void testRunOPSInvalidDataPM10() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSInvalidData(EmissionResultKey.PM10_CONCENTRATION);
  }



  private void assertOPSInvalidData(final EmissionResultKey key)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    OPSInputData data = new OPSInputData(OPSVersion.VERSION, OPSTestUtil.ZOOM_LEVEL_1_SURFACE);

    // Empty list emission sources does not fail, just returns receptors with 0.0 deposition.

    data = getInputTestData(key);
    adjustAndTestValue(data, "setReceptors", null,
        Collection.class, data, true, "Null receptors");

    data = getInputTestData(key);
    adjustAndTestValue(data, "setReceptors", new ArrayList<OPSReceptor>(),
        Collection.class, data, true, "Empty list receptors");
  }

  @Test
  public void testRunOPSReceptorData() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    final EmissionResultKey nh3ConKey = EmissionResultKey.NH3_CONCENTRATION;

    OPSInputData data = getInputTestData(nh3ConKey);
    // ID can be higher then the maximum value of integer, so no point testing min - 1, max + 1
    OPSReceptor receptor = data.getReceptors().iterator().next();
    ArrayList<AeriusResultPoint> results = adjustAndTestValue(receptor, "setId", OPSLimits.RECEPTOR_ID_MINIMUM,
        Integer.TYPE, data, false, "Receptor id minimum");
    Assert.assertNotNull("Should be a result object", results);
    Assert.assertEquals("Returned list size", data.getReceptors().size(), results.size());
    Assert.assertEquals("Returned ID", OPSLimits.RECEPTOR_ID_MINIMUM, results.get(0).getId());

    data = getInputTestData(nh3ConKey);
    receptor = data.getReceptors().iterator().next();
    results = adjustAndTestValue(receptor, "setId", OPSLimits.RECEPTOR_ID_MAXIMUM,
        Integer.TYPE, data, false, "Receptor id maximum");
    Assert.assertNotNull("Should be a result object", results);
    Assert.assertEquals("Returned list size", data.getReceptors().size(), results.size());
    Assert.assertEquals("Returned ID", OPSLimits.RECEPTOR_ID_MAXIMUM, results.get(0).getId());

    data = getInputTestData(nh3ConKey);
    receptor = data.getReceptors().iterator().next();
    assertMinAndMax(receptor, "setX",
        SharedConstants.X_COORDINATE_MINIMUM, SharedConstants.X_COORDINATE_MAXIMUM, Double.TYPE, data,
        "X-coordinate");

    data = getInputTestData(nh3ConKey);
    receptor = data.getReceptors().iterator().next();
    assertMinAndMax(receptor, "setY",
        SharedConstants.Y_COORDINATE_MINIMUM, SharedConstants.Y_COORDINATE_MAXIMUM, Double.TYPE, data,
        "Y-coordinate");
  }

  @Test
  public void testRunOPSSourceNH3Data() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSTestSourceData(EmissionResultKey.NH3_DEPOSITION);
  }

  @Test
  public void testRunOPSSourceNOxData() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSTestSourceData(EmissionResultKey.NOX_DEPOSITION);
  }

  @Ignore("Disable pm10 calculations because the results are not representative.")
  @Test
  public void testRunOPSSourcePM10Data() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertOPSTestSourceData(EmissionResultKey.PM10_CONCENTRATION);
  }

  private void assertOPSTestSourceData(final EmissionResultKey key)
      throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    OPSInputData data = null;

    //id has no limit: default used is 0 in OPS.

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().iterator().next().getPoint(), "setX",
        SharedConstants.X_COORDINATE_MINIMUM, SharedConstants.X_COORDINATE_MAXIMUM, Double.TYPE, data, "X-coordinate");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().iterator().next().getPoint(), "setY",
        SharedConstants.Y_COORDINATE_MINIMUM, SharedConstants.Y_COORDINATE_MAXIMUM, Double.TYPE, data, "Y-coordinate");

    data = getInputTestData(key);
    // only test maximum for setHeatContent
    assertMax(data.getEmissionSources().iterator().next(), "setHeatContent", OPSLimits.SOURCE_HEAT_CONTENT_MAXIMUM, Double.TYPE, data,
        "Heat content");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().iterator().next(), "setEmissionHeight",
        (int) OPSLimits.SOURCE_EMISSION_HEIGHT_MINIMUM, (int) OPSLimits.SOURCE_EMISSION_HEIGHT_MAXIMUM, Double.TYPE, data,
        "Height");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().iterator().next(), "setDiameter",
        OPSLimits.SOURCE_DIAMETER_MINIMUM, OPSLimits.SOURCE_DIAMETER_MAXIMUM, Integer.TYPE, data,
        "Diameter");

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().iterator().next(), "setSpread",
        OPSLimits.SOURCE_SPREAD_MINIMUM, OPSLimits.SOURCE_SPREAD_MAXIMUM, Double.TYPE, data,
        "Spread");

    data = getInputTestData(key);
    //difference between NH3 and others due to 'hack' in OPS.
    if (key == EmissionResultKey.NH3_DEPOSITION) {
      assertMinAndMax(data.getEmissionSources().iterator().next(), "setDiurnalVariation",
          OPSLimits.SOURCE_DIURNAL_VARIATION_MINIMUM,
          OPSLimits.SOURCE_DIURNAL_VARIATION_MAXIMUM,
          Integer.TYPE, data,
          "Diurnal Variation");
    } else {
      //Can't really test maximum if it's not NH3. It is 3 instead of 5 as 4 and 5 are NH3-only. Using 4 or 5 results in AeriusException.
      assertMin(data.getEmissionSources().iterator().next(), "setDiurnalVariation",
          OPSLimits.SOURCE_DIURNAL_VARIATION_MINIMUM,
          Integer.TYPE, data,
          "Diurnal Variation");
    }

    data = getInputTestData(key);
    assertMinAndMax(data.getEmissionSources().iterator().next(), "setArea",
        OPSLimits.SOURCE_AREA_MINIMUM, OPSLimits.SOURCE_AREA_MAXIMUM, Integer.TYPE, data,
        "Area");

    // Test not done for substance PM10 because minimum value 0 is invalid for PM10.
    if (key != EmissionResultKey.PM10_CONCENTRATION) {
      data = getInputTestData(key);
      assertMinAndMax(data.getEmissionSources().iterator().next(), "setParticleSizeDistribution",
          OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MINIMUM + 0, OPSLimits.SOURCE_PARTICLE_SIZE_DISTRIBUTION_MAXIMUM,
          Integer.TYPE, data,
          "Particle Size Distribution");
    }
  }

  @Test
  public void testRunOPSExpectedResults() throws IOException, AeriusException, OPSInvalidVersionException {
    final OPSInputData data = OPSTestUtil.getInputTestData(EmissionResultKey.NOX_DEPOSITION, 2020);

    final EnumSet<EmissionResultKey> resultKeysToReturn = EnumSet.of(EmissionResultKey.NOX_CONCENTRATION);
    data.setEmissionResultKeys(resultKeysToReturn);
    CalculationResult result = runOPS.run(data);
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION));

    resultKeysToReturn.add(EmissionResultKey.NOX_DEPOSITION);
    result = runOPS.run(data);
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION,
        EmissionResultKey.NOXNH3_DEPOSITION));

    resultKeysToReturn.add(EmissionResultKey.NH3_DEPOSITION);
    result = runOPS.run(data);
    //no NH3 stuff, we aren't calculating that one...
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION,
        EmissionResultKey.NOXNH3_DEPOSITION));

    data.getSubstances().add(Substance.NH3);
    result = runOPS.run(data);
    //now we are calculating NH3 as well, so the run should return results (even though no sources actually have emissions for it)
    assertValidateResults(result, EnumSet.of(EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NOX_DEPOSITION,
        EmissionResultKey.NOXNH3_DEPOSITION, EmissionResultKey.NH3_DEPOSITION));

    //    Disable pm10 calculations because the results are not representative.
    //    data.setSubstances(Substance.PM10.hatch());
    //    resultKeysToReturn.clear();
    //    resultKeysToReturn.add(EmissionResultKey.PM25_CONCENTRATION);
    //    result = runOPS.run(data);
    //    //no sources with the substance, but there will be results (even though it's all 0.0)...
    //    assertValidateResults(result, EnumSet.of(EmissionResultKey.PM25_CONCENTRATION));
  }

  private void assertValidateResults(final CalculationResult result, final Set<EmissionResultKey> resultKeysExpected) {
    for (final AeriusResultPoint resultPoint : result.getResults()) {
      for (final EmissionResultKey key : EmissionResultKey.values()) {
        if (resultKeysExpected.contains(key)) {
          Assert.assertTrue("Should have some results for " + key, resultPoint.getEmissionResults().hasResult(key));
        } else {
          Assert.assertFalse("Shouldn't have any results for " + key, resultPoint.getEmissionResults().hasResult(key));
        }
      }
    }
  }

  private void assertMinAndMax(final Object adjustValueOn, final String methodNameToAdjustValue,
      final int minimumValue, final int maximumValue, final Class<?> valueType, final OPSInputData data, final String extraInfo)
          throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    assertMin(adjustValueOn, methodNameToAdjustValue, minimumValue, valueType, data, extraInfo);

    assertMax(adjustValueOn, methodNameToAdjustValue, maximumValue, valueType, data, extraInfo);
  }

  private void assertMin(final Object adjustValueOn, final String methodNameToAdjustValue,
      final int minimumValue, final Class<?> valueType, final OPSInputData data, final String extraInfo)
          throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, minimumValue - 1, valueType, data, true, extraInfo + " minimum");
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, minimumValue, valueType, data, false, extraInfo + " minimum");
  }

  private void assertMax(final Object adjustValueOn, final String methodNameToAdjustValue,
      final int maximumValue, final Class<?> valueType, final OPSInputData data, final String extraInfo)
          throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, maximumValue, valueType, data, false, extraInfo + " maximum");
    adjustAndTestValue(adjustValueOn, methodNameToAdjustValue, maximumValue + 1, valueType, data, true, extraInfo + " maximum");
  }

  private void adjustValue(final Object adjustValueOn, final String methodNameToAdjustValue, final Object value, final Class<?> valueType) {
    final Class<? extends Object> cls = adjustValueOn.getClass();
    try {
      final Method method = cls.getMethod(methodNameToAdjustValue, valueType);
      method.invoke(adjustValueOn, value);
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      Assert.fail("Error while executing method. " + e.getMessage());
    }
  }

  private ArrayList<AeriusResultPoint> adjustAndTestValue(final Object adjustValueOn, final String methodNameToAdjustValue, final Object value,
      final Class<?> valueType, final OPSInputData data, final boolean shouldFail, final String extraInfo)
          throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
    adjustValue(adjustValueOn, methodNameToAdjustValue, value, valueType);

    ArrayList<AeriusResultPoint> results = null;
    if (shouldFail) {
      runOPSExpectInvalidInputException(data, extraInfo);
    } else {
      results = runOPS.run(data).getResults();
      Assert.assertFalse("Returned result shouldn't be empty. Test: " + extraInfo, results.isEmpty());
    }
    return results;
  }

  private void runOPSExpectInvalidInputException(final OPSInputData data, final String testDescription) throws IOException,
      InterruptedException, AeriusException, OPSInvalidVersionException {
    try {
      runOPS.run(data);
      Assert.fail("Input should have been qualified as invalid. Test was: " + testDescription);
    } catch (final AeriusException e) {
      Assert.assertSame("Check on correct reason", Reason.OPS_INPUT_VALIDATION, e.getReason());
      Assert.assertFalse("Violations in exception shouldn't be empty.", e.getArgs()[0].isEmpty());
    }
  }

  class OPSRunCallable implements Callable<Boolean> {

    private final RunOPS ops;
    private final OPSInputData data;

    public OPSRunCallable(final RunOPS ops, final OPSInputData data) {
      this.ops = ops;
      this.data = data;
    }

    @Override
    public Boolean call() throws IOException, InterruptedException, AeriusException, OPSInvalidVersionException {
      return !ops.run(data).getResults().isEmpty();
    }
  }
}
