/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link RcpImportReader}.
 */
public class RcpImportReaderTest {

  private static final String FILENAME = "RcpImportReader.rcp";
  private InputStream inputStream;

  @Before
  public void before() {
    inputStream = getClass().getResourceAsStream(FILENAME);
  }

  @After
  public void after() throws IOException {
    inputStream.close();
  }

  @Test
  public void detectTest() {
    final ImportReader ir = new RcpImportReader(false);
    assertTrue("Detect extension correcty", ir.detect("receptors.rcp"));
    assertTrue("Detect extension case insane", ir.detect("RECEPTORS.RCP"));
    assertFalse("Not detect other extensions", ir.detect("receptors"));
  }

  @Test
  public void testRCPWithErrors() throws IOException, AeriusException {
    final ImportReader ir = new RcpImportReader(false);
    final ImportResult result = new ImportResult();
    ir.read(FILENAME, inputStream, null, null, result);
    assertEquals("Should have read # rows correctly", 6, result.getCalculationPoints().size());
    assertEquals("Should have read # errors", 1,  result.getExceptions().size());
    assertEquals("Should have autonumbered id's", 2, result.getCalculationPoints().get(1).getId());
  }

  @Test
  public void testRCPWithNameAsId() throws IOException, AeriusException {
    final ImportReader ir = new RcpImportReader(true);
    final ImportResult result = new ImportResult();
    ir.read(FILENAME, inputStream, null, null, result);
    assertEquals("Should have read name as id", 3324315, result.getCalculationPoints().get(1).getId());
  }

}
