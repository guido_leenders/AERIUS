/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.ops.util.OPSTestUtil;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test for {@link RcpFileReader} class.
 */
public class RcpFileReaderTest {

  private static final String RESOURCES_DIRECTORY = "receptor/";
  private static final String USED_EXTENSION = ".rcp";
  private static final String RECEPTOR_FILE = "receptor";
  private static final String RECEPTOR_FILE_FREEFORMAT = "receptor_freeformat";
  private static final String RECEPTOR_FAIL_FILE = "receptor_fail";

  @Test
  public void testParseRegularOPSFile() throws IOException {
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FILE);

    assertNotNull("Check if has receptors.", result);
    testExpectedReceptors(result);
  }

  @Test
  public void testParseFreeFormatOPSFile() throws IOException {
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FILE_FREEFORMAT);

    assertNotNull("Check if has receptors.", result);
    testExpectedReceptors(result);
  }

  private void testExpectedReceptors(final LineReaderResult<OPSReceptor> input) {
    final List<OPSReceptor> receptors = input.getObjects();
    assertEquals("Receptors parsed", 7, receptors.size());
    // ID's are descending from 7
    for (final OPSReceptor receptor : receptors) {
      assertNotEquals("receptor ID", 0, receptor.getId());
    }
    // header    : snr    x(m)    y(m)    q(g/s) hc(MW)  h(m)   d(m)  s(m)  dv cat area sd  comp
    // first line:   7  139123  434456 2.500E-01  2.100   2.5      3   0.2   4   2   2   2  17
    assertEquals("X coord", 106206, receptors.get(0).getX(), 0.00001);
    assertEquals("Y coord", 419024, receptors.get(0).getY(), 0.00001);
//    assertEquals("Name", "words", receptors.get(0).getId());
    assertTrue("Check if no exceptions present", input.getExceptions().isEmpty());
  }

  @Test
  public void testParseRegularOPSFileFail1() throws IOException {
    // first fail file is a file where a line has been cut in half.
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FAIL_FILE + 1);

    assertEquals("Number of exceptions", 2, result.getExceptions().size());
    // Fail line: "   7 3310760       "
    final AeriusException e1 = result.getExceptions().get(0);
    assertEquals("Error reason", AeriusException.Reason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, e1.getReason());
    assertEquals("Error line number", "2", e1.getArgs()[0]);
    // Fail line: "   106206  419024"
    final AeriusException e2 = result.getExceptions().get(1);
    assertEquals("Error reason", AeriusException.Reason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, e2.getReason());
    assertEquals("Error line number", "3", e2.getArgs()[0]);
  }

  @Test
  public void testParseRegularOPSFileFail2() throws IOException {
    // second fail file is a file where a line contains a word as ID (should be a number).
    final LineReaderResult<OPSReceptor> result = readRcpFile(RECEPTOR_FAIL_FILE + 2);

    assertEquals("Number of exceptions", 1, result.getExceptions().size());
    // Fail line: " one 3310760       106206  419024"
    final AeriusException e = result.getExceptions().get(0);
    assertEquals("Error reason", AeriusException.Reason.IO_EXCEPTION_NUMBER_FORMAT, e.getReason());
    assertEquals("Error line number", "2", e.getArgs()[0]);
    assertEquals("Error column name", "Nr", e.getArgs()[1]);
    assertEquals("Error column content", "one", e.getArgs()[2]);
  }

  private LineReaderResult<OPSReceptor> readRcpFile(final String fileName) throws IOException {
    return OPSTestUtil.readRcpFile(getClass(), RESOURCES_DIRECTORY + fileName + USED_EXTENSION);
  }
}
