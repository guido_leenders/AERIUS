/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.ops.importer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.ops.DiurnalVariationSpecification;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link BrnImportReader}.
 */
public class BrnImportReaderTest {

  private static final String FILENAME = "BrnImportReader.brn";
  private final ImportReader ir = new BrnImportReader(true);

  private InputStream inputStream;
  private final SectorCategories categories = new SectorCategories();

  @Before
  public void before() {
    inputStream = getClass().getResourceAsStream(FILENAME);
    final ArrayList<DiurnalVariationSpecification> diurnalVariations = new ArrayList<>();
    categories.setDiurnalVariations(diurnalVariations);
  }

  @After
  public void after() throws IOException {
    inputStream.close();
  }

  @Test
  public void detectTest() {
    assertTrue("Detect extension correcty", ir.detect("sources.brn"));
    assertTrue("Detect extension case insane", ir.detect("sources.BRN"));
    assertFalse("Not detect other extensions", ir.detect("sources"));
  }

  @Test(expected = AeriusException.class)
  public void testBRNWithoutSubstance() throws AeriusException, IOException, SQLException {
    try {
      ir.read(FILENAME, inputStream, categories, null, new ImportResult());
      fail("Absence of substance not detected");
    } catch (final AeriusException e) {
      assertEquals("Exception reason BRN without substance", Reason.BRN_WITHOUT_SUBSTANCE, e.getReason());
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testBRNUnsupportedSubstance() throws AeriusException, IOException, SQLException {
    try {
      ir.read(FILENAME, inputStream, categories, Substance.NOXNH3, new ImportResult());
      fail("Absence of substance not detected");
    } catch (final AeriusException e) {
      assertEquals("Exception reason BRN unsupported substance", Reason.BRN_SUBSTANCE_NOT_SUPPORTED, e.getReason());
      throw e;
    }
  }

  @Test
  public void testBRNWithErrors() throws IOException, AeriusException {
    final ImportResult result = new ImportResult();
    ir.read(FILENAME, inputStream, categories, Substance.NH3, result);
    assertEquals("Should have read # rows correctly", 6, result.getSourceLists().get(0).size());
    assertEquals("Should have read # errors", 1,  result.getExceptions().size());
  }
}
