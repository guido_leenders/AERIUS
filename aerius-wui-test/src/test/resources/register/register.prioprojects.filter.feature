@register-filter @nightly @single
Feature: For priority projects a range of filters can be applied to filter items on province, sector, status, authority date etc.

  Background:
    Given AERIUS REGISTER is open

  @project-filter @AER-1247 
  Scenario: Priority project 'Voortgang' filter should contain specified options  
    Given the user is logged in as 'ADMIN'
    And the menu item 'prioritary projects' is selected
    When the priority project filter panel is toggled
    Then the priority project filter 'Voortgang' contains options
    | Alle                  |
    | Nog geen reservering  |
    | Niet benut            |
    | Deels benut (≤50%)    |
    | Deels benut (>50%)    |
    | Volledig benut        |
    | Voltooid              |

  @project-filter @project-filter-authorities
  Scenario: Priority project 'Bevoegd gezag' filter should contain specified options
    Given the user is logged in as 'ADMIN'
    And the menu item 'prioritary projects' is selected
    When the priority project filter panel is toggled
    Then the priority project filter 'Bevoegd gezag' contains options
    | Alle                                                |
    | Onbekend                                            |
    | Provincie Drenthe                                   |
    | Provincie Flevoland                                 |
    | Provincie Fryslân                                   |
    | Provincie Gelderland                                |
    | Provincie Groningen                                 |
    | Provincie Limburg                                   |
    | Provincie Noord-Brabant                             |
    | Provincie Noord-Holland                             |
    | Provincie Overijssel                                |
    | Provincie Utrecht                                   |
    | Provincie Zeeland                                   |
    | Provincie Zuid-Holland                              |
    | Ministerie van Defensie                             |
    | Ministerie van Landbouw, Natuur en Voedselkwaliteit |
    | Ministerie van Infrastructuur en Waterstaat         |

  @project-filter @project-filter-sector
  Scenario: Priority project 'Sector' filter should contain specified options
    Given the user is logged in as 'ADMIN'
    And the menu item 'prioritary projects' is selected
    When the priority project filter panel is toggled
    Then the priority project filter 'Sector' contains options
    | Alle sectoren                     |
    | Energie                           |
    | Stalemissies                      |
    | Mestopslag                        |
    | Beweiding                         |
    | Mestaanwending                    |
    | Glastuinbouw                      |
    | Vuurhaarden, overig               |
    | Woningen                          |
    | Recreatie                         |
    | Kantoren en winkels               |
    | Afvalverwerking                   |
    | Voedings- en genotmiddelen        |
    | Chemische industrie               |
    | Bouwmaterialen                    |
    | Basismetaal                       |
    | Metaalbewerkingsindustrie         |
    | Overig                            |
    | Landbouw                          |
    | Bouw en Industrie                 |
    | Delfstoffenwinning                |
    | Consumenten mobiele werktuigen    |
    | Emplacement                       |
    | Spoorweg                          |
    | Stijgen                           |
    | Landen                            |
    | Taxiën                            |
    | Bronnen luchthaventerrein         |
    | Snelwegen                         |
    | Buitenwegen                       |
    | Binnen bebouwde kom               |
    | Zeescheepvaart: Aanlegplaats      |
    | Zeescheepvaart: Binnengaats route |
    | Zeescheepvaart: Zeeroute          |
    | Binnenvaart: Aanlegplaats         |
    | Binnenvaart: Vaarroute            |
    | Anders...                         |
    | Plan                              |

  @project-filter @project-filter-area @AER-977 @AER-1710
  Scenario: Priority project 'Gebied' filter should contain specified options
    Given the user is logged in as 'ADMIN'
    And the menu item 'prioritary projects' is selected
    When the priority project filter panel is toggled
    Then the priority project filter 'Gebied ' contains options
    | Alle gebieden                                  |
    | Aamsveen                                       |
    | Abdij Lilbosch & voormalig Klooster Mariahoop  |
    | Abtskolk & De Putten                           |
    | Achter de Voort, Agelerbroek & Voltherbroek    |
    | Alde Feanen                                    |
    | Arkemheen                                      |
    | Bakkeveense Duinen                             |
    | Bargerveen                                     |
    | Bekendelle                                     |
    | Bemelerberg & Schiepersberg                    |
    | Bergvennen & Brecklenkampse Veld               |
    | Biesbosch                                      |
    | Binnenveld                                     |
    | Boetelerveld                                   |
    | Boezems Kinderdijk                             |
    | Borkeld                                        |
    | Boschhuizerbergen                              |
    | Botshol                                        |
    | Brabantse Wal                                  |
    | Broekvelden, Vettenbroek & Polder Stein        |
    | Brunssummerheide                               |
    | Bunder- en Elslooërbos                         |
    | Buurserzand & Haaksbergerveen                  |
    | Canisvliet                                     |
    | Coepelduynen                                   |
    | De Bruuk                                       |
    | Deelen                                         |
    | Deurnsche Peel & Mariapeel                     |
    | De Wieden                                      |
    | De Wilck                                       |
    | Dinkelland                                     |
    | Doggersbank                                    |
    | Donkse Laagten                                 |
    | Drentsche Aa-gebied                            |
    | Drents-Friese Wold & Leggelderveld             |
    | Drouwenerzand                                  |
    | Duinen Ameland                                 |
    | Duinen Den Helder-Callantsoog                  |
    | Duinen en Lage Land Texel                      |
    | Duinen Goeree & Kwade Hoek                     |
    | Duinen Schiermonnikoog                         |
    | Duinen Terschelling                            |
    | Duinen Vlieland                                |
    | Dwingelderveld                                 |
    | Eemmeer & Gooimeer Zuidoever                   |
    | Eilandspolder                                  |
    | Elperstroomgebied                              |
    | Engbertsdijksvenen                             |
    | Fochteloërveen                                 |
    | Friese Front                                   |
    | Geleenbeekdal                                  |
    | Geuldal                                        |
    | Grensmaas                                      |
    | Grevelingen                                    |
    | Groote Gat                                     |
    | Groote Peel                                    |
    | Groote Wielen                                  |
    | Haringvliet                                    |
    | Hollands Diep                                  |
    | Holtingerveld                                  |
    | IJsselmeer                                     |
    | Ilperveld, Varkensland, Oostzanerveld & Twiske |
    | Kampina & Oisterwijkse Vennen                  |
    | Kempenland-West                                |
    | Kennemerland-Zuid                              |
    | Ketelmeer & Vossemeer                          |
    | Klaverbank                                     |
    | Kolland & Overlangbroek                        |
    | Kop van Schouwen                               |
    | Korenburgerveen                                |
    | Krammer-Volkerak                               |
    | Kunderberg                                     |
    | Landgoederen Brummen                           |
    | Landgoederen Oldenzaal                         |
    | Langstraat                                     |
    | Lauwersmeer                                    |
    | Leekstermeergebied                             |
    | Leenderbos, Groote Heide & De Plateaux         |
    | Lemselermaten                                  |
    | Lepelaarplassen                                |
    | Leudal                                         |
    | Lieftinghsbroek                                |
    | Lingegebied & Diefdijk-Zuid                    |
    | Loevestein, Pompveld & Kornsche Boezem         |
    | Lonnekermeer                                   |
    | Loonse en Drunense Duinen & Leemkuilen         |
    | Maas bij Eijsden                               |
    | Maasduinen                                     |
    | Manteling van Walcheren                        |
    | Mantingerbos                                   |
    | Mantingerzand                                  |
    | Markermeer & IJmeer                            |
    | Markiezaat                                     |
    | Meijendel & Berkheide                          |
    | Meinweg                                        |
    | Naardermeer                                    |
    | Nieuwkoopse Plassen & De Haeck                 |
    | Noorbeemden & Hoogbos                          |
    | Noordhollands Duinreservaat                    |
    | Noordzeekustzone                               |
    | Norgerholt                                     |
    | Oeffelter Meent                                |
    | Olde Maten & Veerslootslanden                  |
    | Oostelijke Vechtplassen                        |
    | Oosterschelde                                  |
    | Oostvaardersplassen                            |
    | Oudegaasterbrekken, Fluessen en omgeving       |
    | Oudeland van Strijen                           |
    | Oude Maas                                      |
    | Polder Westzaan                                |
    | Polder Zeevang                                 |
    | Regte Heide & Riels Laag                       |
    | Rijntakken                                     |
    | Roerdal                                        |
    | Rottige Meenthe & Brandemeer                   |
    | Sallandse Heuvelrug                            |
    | Sarsven en De Banen                            |
    | Savelsbos                                      |
    | Schoorlse Duinen                               |
    | Sint Jansberg                                  |
    | Sint Pietersberg & Jekerdal                    |
    | Sneekermeergebied                              |
    | Solleveld & Kapittelduinen                     |
    | Springendal & Dal van de Mosbeek               |
    | Stelkampsveld                                  |
    | Strabrechtse Heide & Beuven                    |
    | Swalmdal                                       |
    | Uiterwaarden Lek                               |
    | Uiterwaarden Zwarte Water en Vecht             |
    | Ulvenhoutse Bos                                |
    | Van Oordt's Mersken                            |
    | Vecht- en Beneden-Reggegebied                  |
    | Veerse Meer                                    |
    | Veluwe                                         |
    | Veluwerandmeren                                |
    | Vlakte van de Raan                             |
    | Vlijmens Ven, Moerputten & Bossche Broek       |
    | Vogelkreek                                     |
    | Voordelta                                      |
    | Voornes Duin                                   |
    | Waddenzee                                      |
    | Weerribben                                     |
    | Weerter- en Budelerbergen & Ringselven         |
    | Westduinpark & Wapendal                        |
    | Westerschelde & Saeftinghe                     |
    | Wierdense Veld                                 |
    | Wijnjeterper Schar                             |
    | Willinks Weust                                 |
    | Witte en Zwarte Brekken                        |
    | Witterveld                                     |
    | Witte Veen                                     |
    | Wooldse Veen                                   |
    | Wormer- en Jisperveld & Kalverpolder           |
    | Yerseke en Kapelse Moer                        |
    | Zeldersche Driessen                            |
    | Zoommeer                                       |
    | Zouweboezem                                    |
    | Zuidlaardermeergebied                          |
    | Zwanenwater & Pettemerduinen                   |
    | Zwarte Meer                                    |
    | Zwin & Kievittepolder                          |
