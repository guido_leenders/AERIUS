@register-manual-capture
Feature: Capture screenshots for the AERIUS REGISTER manual

  Background: 
    Given AERIUS REGISTER is open sized 1191 x 911

  @register-manual-basisfuncties
  Scenario: Setup for basic functionality screenshots (2325, 2326, 2327, 2590)
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '2325_Basisfuncties-Menu-Stap_1'
    And the menu item 'applications' is selected
    And the application with filename 'AERIUS_register_manual.pdf' is imported with dossierID '48-ZPH2-NH'
    And wait until the application dossier status is 'In de wachtrij'
    And wait for 1 seconds
    And the application dossier status is set to 'PENDING_WITH_SPACE'
    And the notifications panel is opened
    And wait for 1 seconds
    And take a screenshot for the 'register' manual named '2326_Basisfuncties-Berichtencentrum-Stap_1'
    And the notifications panel is closed
    And wait for 1 seconds
    And the application menu item -development space- is selected
    And wait for 2 seconds
    And wait until the page is loaded
    And the user profile is opened
    And wait for 1 seconds
    And take a screenshot for the 'register' manual named '2327_Basisfuncties-Accountinformatie-Stap_3'
    And the user profile is closed
    And the search for '48-' is started
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '2590_Basisfuncties-Zoekfunctie-Stap_4'

  @register-manual-deleted
  Scenario: Basicfunctionality - delete application
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the menu item 'applications' is selected
    And the application with description '48-ZPH2-NH' is deleted

  @register-manual-unfiltered
  Scenario: Basicfunctionality - reset filters and grab dashboard overview (2337)
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    Given the menu item 'dashboard' is selected
    And wait until the page is loaded
    When the user filters permit dashboard on province 'Nederland'
    And the user filters notice dashboard on province 'Nederland'
    And wait until the page is loaded
    Then take a screenshot for the 'register' manual named '2337_Basisfuncties-Dashboard-Stap_1'

  @register-manual-filtered
  Scenario: Basicfunctionality - apply filters and capture dashboard overview (2335,2336, 2367) 
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the user filters permit dashboard on province 'Noord-Brabant'
    And the user filters notice dashboard on province 'Noord-Brabant'
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '2335_Basisfuncties-Dashboard-PrioritaireProjecten-Stap_1'
    And take a screenshot for the 'register' manual named '2336_Basisfuncties-Dashboard-Ontwikkelingsruimte-Stap_1'
    And take a screenshot for the 'register' manual named '2367_Basisfuncties-Dashboard-Grenswaardereservering-Stap_1'

  @register-manual-applications
  Scenario: Basicfuctionality - permit applications (2340)
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the menu item 'applications' is selected
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2340_Basisfuncties-Aanvragen-Stap_1'

  @register-manual-filteredapplications
  Scenario: Basicfuctionality - filtered permit applications (2342)
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the menu item 'applications' is selected
    And wait for 3 seconds
    And the overview filter panel is toggled
    And the overview filter STATUS options are shown
    And take a screenshot for the 'register' manual named '2342_Basisfuncties-Aanvragen-Filter-Stap_1'

  @register-manual-importapplication
  Scenario: Basicfuctionality - import new permit application (2344, 2346, 2600, 2348, 2350, 2368, 2354, 2357, 2593, 2594)
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the menu item 'applications' is selected
    And the application import window is opened
    And the application import dossier id is set to '1243'
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2344_Basisfuncties-Aanvragen-Import-Stap_1'
    And the application import file 'AERIUS_register_manual.pdf' is selected
    And the application import date is set to '14-08-2018 12:00'
    And the application import window is confirmed
    And wait until the application dossier status is 'In de wachtrij'
    And wait for 2 seconds
    And take a screenshot for the 'register' manual named '2346_Basisfuncties-Aanvragen-Gegevensoverzicht-Stap_1'
    And take a screenshot for the 'register' manual named '2600_Basisfuncties-Aanvragen-Verwijderen-Stap_1'
    And take a screenshot for the 'register' manual named '2348_Basisfuncties-Aanvragen-DossierInformatie-Stap_1'
    And the application menu item -details- is selected
    And take a screenshot for the 'register' manual named '2350_Basisfuncties-Aanvragen-Details-Stap_1'
    And the application menu item -permit rules- is selected
    And wait for 10 seconds
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '2368_Basisfuncties-Aanvragen-Toetsing-Stap_1'
    And the application menu item -development space- is selected
    And take a screenshot for the 'register' manual named '2354_Basisfuncties-Aanvragen-Ontwikkelingsruimte-Stap_1'
    And the application dossier status edit window is opened
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2357_Basisfuncties-Aanvragen-StatusOvergang-Stap_1'
    And the application dossier status edit window is cancelled
    And the application menu item -dossier- is selected
    And the mouse hovers on button 'Download'
    And take a screenshot for the 'register' manual named '2593_Basisfuncties-Aanvragen-BijlageBijBesluit-Stap_2'
    And the application dossier status is set to 'PENDING_WITHOUT_SPACE'
    And the notifications are all deleted
    And wait for 3 seconds
    And the application dossier status is set to 'REJECTED_WITHOUT_SPACE'
    And wait for 3 seconds
    And the notifications are all deleted
    And the application dossier status is changed to 'REJECTED_FINAL'
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2594_Basisfuncties-Aanvragen-DefinitieAfgewezen-Stap_3'
    And the application dossier status change is confirmed'

  @register-manual-priorityprojects
  Scenario: Basicfuctionality - priority projects overview (2998, 2975, 2981, 2995, 2983, 2994, 2985, 2986)
    And the user is logged in as 'SUPERUSER'
    And wait until the page is loaded
    And the menu item 'prioritary projects' is selected
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2998_Basisfuncties-Nieuw-PrioProject-Stap_1'
    And the overview filter panel is toggled
    And the overview filter SECTOR options are shown
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2975_Basisfuncties-Filter-PrioProject-Stap_1'
    And the user opens priority project with dossierID 'Test PP 3530 voor Groningen'
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '2981_Basisfuncties-Dossier-PrioProject-Stap_1'
    And take a screenshot for the 'register' manual named '2995_Basisfuncties-Export-PrioProject-Stap_1'
    And the user switches to the 'Project' tab of the priority project
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2983_Basisfuncties-Project-PrioProject-Stap_1'
    And the user opens priority sub project with dossierID 'Test PDP 4130 voor Noord-Holland'
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2994_Basisfuncties-Status-SubProject-Stap_1'
    And take a screenshot for the 'register' manual named '2985_Basisfuncties-Aanvraag-SubProject-Stap_1'
    And the user switches to the 'Toetsing' details page
    And wait for 10 seconds
    And wait until the page is loaded
    And the assessment explanation is toggled
    And take a screenshot for the 'register' manual named '2986_Basisfuncties-Toetsing-SubProject-Stap_1'

  @register-manual-applicationfilter
  Scenario: Basicfuctionality - filter permit application (2825, 2827, 2826, 2829, 2990, 2828)
    And the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the menu item 'applications' is selected
    And the overview filter panel is toggled
    And the overview filter STATUS options are shown
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2825_Basisfuncties-Filter-Aanvragen-Stap_1'
    And the overview filter is applied
    And the overview filter panel is toggled
    And the overview filter STATUS is set to 'In behandeling (wel OR)'
    And the overview filter is applied
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2827_Basisfuncties-Filter-Aanvragen-Stap_2'
    And the overview filter panel is toggled
    And the overview filter STATUS is set to 'In de wachtrij'
    And the overview filter is applied
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2826_Basisfuncties-Filter-Aanvragen-Stap_3'
    And the overview filter panel is toggled
    And the overview filter STATUS is set to 'Definitief besluit'
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2829_Basisfuncties-Filter-Aanvragen-Stap_4'
    And the overview filter STATUS is set to 'In behandeling (geen OR)'
    And the overview filter is applied
    And the overview filter panel is toggled
    And the overview filter STATUS options are shown
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2990_Basisfuncties-Filter-Aanvragen-Stap_5'
    And the overview filter is applied
    And the overview filter panel is toggled
    And the overview filter STATUS is set to 'OR afgeboekt'
    And the overview filter STATUS is set to 'Afgewezen (geen OR)'
    And the overview filter is applied
    And the overview filter panel is toggled
    And the overview filter STATUS options are shown    
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2828_Basisfuncties-Filter-Aanvragen-Stap_6'

  @register-manual-addition
  Scenario: Additional images2016 (2975, 2979)
    Given the user is logged in as 'SUPERUSER'
    And wait until the page is loaded
    And the menu item 'dashboard' is selected
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '2975_Basisfuncties-Dashboard-PrioProject-Stap_1'
    And the menu item 'prioritary projects' is selected
    And the overview filter panel is toggled
    And the overview filter SECTOR options are shown
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2979_Basisfuncties-Filter-PrioProject-Stap_1'

  @register-manual-addition
  Scenario: Additional images2016 (2994, 2988, 3044)
    Given the user is logged in as 'ADMIN'
    And wait until the page is loaded
    And the menu item 'prioritary projects' is selected
    And the user can import 'AERIUS_SET01_000 - PP02 - RESERVERING.zip' with dossierID 'PrioProject' as a new priority project 
    And the user switches to the 'Project' tab of the priority project
    And the partial project file 'AERIUS_SET01_02 - PDP01.02 - Aansluiting N371.gml.zip' is imported
    And the application dossier status edit window is opened
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2994_Basisfuncties-Status-PrioProject-Stap_1'
    And the application dossier status edit window is cancelled
    And the menu item 'role management' is selected
    And wait for 3 seconds
    And take a screenshot for the 'register' manual named '2988_Basisfuncties-Rollen-Stap_1'
    And the menu item 'applications' is selected
    And the application with filename 'AERIUS_pending_without_space.pdf' is imported with dossierID 'PendingWithoutSpace1'
    And wait until the application dossier status is 'In de wachtrij'
    And the applications tab -assessment- is selected
    And wait until the page is loaded
    And take a screenshot for the 'register' manual named '3044_Basisfuncties-Aanvraag_Toetsing-Stap_1'

  @register-manual-actualization
  Scenario: Additional image new actualization tab (2999)
    Given the user is logged in as 'SUPERUSER'
    And wait until the page is loaded
    And the menu item 'prioritary projects' is selected
    And the search for 'PrioProject' is started
    And the search suggestion 'PrioProject' is selected
    And the user switches to the 'Actualisatie' tab of the priority project
    And take a screenshot for the 'register' manual named '2999_Basisfuncties-Actualisatie-PrioProject-Stap_1'
