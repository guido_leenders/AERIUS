@user-authorization @nightly @ignore-auth
Feature: Authentication for user roles and functionalities to process submitted applications
  A viewer should only be allowed to view submitted applications
  An editor should be able to view and edit applications, except status transition and deleting
  A superuser is allowed to do almost anything. Manage applications and priority projects including status transition and deleting
  An administrator should be able to do everything including user / role management

  Background:
    Given AERIUS REGISTER is open

  ################################################
  # Checking the accessibility of test roles
  ################################################ 
  @user-access
  Scenario: As a user i can log in with the role of viewer
    Given the user is logged in as 'VIEWER'
    Then the users full name is shown as 'Test2, AERIUS2'
    And the menu item -prioritary projects- is available
    And the menu item -user management- is not available
    And the menu item -role management- is not available

  @user-access
  Scenario: As a user i can log in with the role of editor
    Given the user is logged in as 'EDITOR'
    Then the users full name is shown as 'Test3, AERIUS3'
    And the menu item -prioritary projects- is available
    And the menu item -user management- is not available
    And the menu item -role management- is not available

  @user-access
  Scenario: As a user i can log in with the role of superuser
    Given the user is logged in as 'SUPERUSER'
    Then the users full name is shown as 'Test4, AERIUS4'
    And the menu item -prioritary projects- is available
    And the menu item -user management- is not available
    And the menu item -role management- is not available

  @user-access
  Scenario: As a user i can log in with the role of administrator
    Given the user is logged in as 'ADMIN'
    Then the users full name is shown as 'Test1, AERIUS1'
    And the menu item -prioritary projects- is available
    And the menu item -user management- is available
    And the menu item -role management- is available

  ##############################################
  # Checking the user roles import permissions 
  ##############################################
  @user-permissions @viewer @editor @superuser @admin
  Scenario Outline: The permit application import button should not be shown on the dashboard page anymore
    Given the user is logged in as '<user_role>'
    When the menu item 'dashboard' is selected
    Then the page does not contain button 'newApplication'

    Examples: Making sure the obsolete import button doesn't reappear on anyone's dashboard page
      | user_role |
      | VIEWER    |
      | EDITOR    |
      | SUPERUSER |
      | ADMIN     |

  @user-permissions @viewer
  Scenario: As a viewer i can not import new permit applications from the application page
    Given the user is logged in as 'VIEWER'
    When the menu item 'applications' is selected
    Then the page does not contain button 'newApplication'

  @user-permissions @viewer @editor @superuser
  Scenario Outline: As a viewer, editor or superuser i can not import new priority project reservation from the priority projects page
    Given the user is logged in as '<user_role>'
    When the menu item 'prioritary projects' is selected
    And wait for 3 seconds
    Then the page does not contain button 'priorityProjectNewButton'

    Examples: User roles that can not import reservation files from the priority project page
      | user_role |
      | VIEWER    |
      | EDITOR    |
      | SUPERUSER |

  @user-permissions @viewer @editor
  Scenario Outline: As a viewer or editor i can not import new actualization projects from the priority projects page
    Given the user is logged in as '<user_role>'
    When the menu item 'prioritary projects' is selected
    And wait for 3 seconds
    Then the page does not contain button 'actualisationNewButton'

    Examples: User roles that can not import actualization files from the priority project page
      | user_role |
      | VIEWER    |
      | EDITOR    |


  ################################################
  # Checking admin's user management permissions
  ################################################   
  @user-management @admin
  Scenario Outline: As an administrator i can add new users
    Given the user is logged in as 'ADMIN'
    Given the menu item 'user management' is selected
    When the add new user form is opened
    And the new user form field 'name' is set to '<name>'
    And the new user form field 'initials' is set to '<initials>'
    And the new user form field 'firstname' is set to '<firstname>'
    And the new user form field 'lastname' is set to '<lastname>'
    And the new user form field 'organisation' is set to '<organisation>'
    And the new user form field 'email' is set to '<email>'
    And the new user is 'enabled'
    And the new user form field 'roles' is set to '<roles>'
    And the new user form is submitted
    And wait for 5 seconds
    And the notification panel contains the message 'Gebruiker "<name>" is succesvol aangemaakt.'
    And the notification panel contains no errors

    Examples: User accounts, details and roles to be added by the admin user
      | name      | initials | firstname | lastname       | organisation           | email                        | roles              |
      | Seleen1   | S.V.     | Seleen    | Cuke_Viewer    | Provincie Utrecht      | selenium+viewer@aerius.nl    | register_viewer    |
      | Seleen2   | S.E.     | Seleen    | Cuke_Editor    | Provincie Limburg      | selenium+editor@aerius.nl    | register_editor    |
      | Seleen3   | S.S.     | Seleen    | Cuke_Superuser | Provincie Drenthe      | selenium+superuser@aerius.nl | register_superuser |
      | Seleen4   | S.A.     | Seleen    | Cuke_Admin     | Provincie Zeeland      | selenium+admin@aerius.nl     | register_admin     |
      | Seleen6   | S.S.     | Seleen    | Cuke_Special   | Provincie Fryslân      | selenium+special@aerius.nl   | register_special   |

  @user-management @user-delete @admin
  Scenario Outline: As an administrator i can delete users
    Given the user is logged in as 'ADMIN'
    Given the menu item 'user management' is selected
    When the user '<username>' is deleted
    Then the notification panel contains the message 'Deze gebruiker is succesvol verwijderd.'
    And the notification panel contains no errors

    Examples: User accounts to delete
      | username             |
      | Cuke_Viewer, S.V.    |
      | Cuke_Editor, S.E.    |
      | Cuke_Superuser, S.S. |
      | Cuke_Admin, S.A.     |
      | Cuke_Special, S.S.   |

  @user-profile
  Scenario: As a user i can inspect my user profile
    Given the user is logged in as 'SUPERUSER'
    When the user profile is opened
    Then the user profile contains 'AERIUS4 A. Test4'
    And the user profile contains 'Ministerie van Infrastructuur en Waterstaat'
    And the user profile contains 'register_superuser'
    And the user profile is closed
