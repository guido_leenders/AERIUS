@register-filter @nightly @ignore-dashboard
Feature: The dashboard, prioritary projects, permits and notices overview offer a filter to narrow down the listed items
  A range of filters can be applied to filter items on province, sector, status, authority date etc.

  Background:
    Given AERIUS REGISTER is open

  @fire
  Scenario: Check the total amount of expected nature areas shown on the dashboard
    Given the user is logged in as 'VIEWER'
    And wait until the page is loaded
    And the notification panel contains no errors
    When the user filters permit dashboard on province 'Nederland'
    And the user filters notice dashboard on province 'Limburg'
    And wait until the page is loaded
    Then the permit dashboard contains '122' nature areas
    And the notice dashboard contains '21' nature areas

  @register-filter-dashboard
  Scenario: As a viewer i can filter the dashboard prioritary projects table on zuid holland and check the habitats
    Given the user is logged in as 'VIEWER'
    And wait until the page is loaded
    And the notification panel contains no errors
    When the user filters permit dashboard on province 'Zuid-Holland'
    And the user filters notice dashboard on province 'Limburg'
    And wait until the page is loaded
    # check presence of all relevant habitats for this area 
    Then the overview contains area 'Solleveld & Kapittelduinen'
    And the overview contains area 'Kennemerland-Zuid'
    And the overview contains area 'Duinen Goeree & Kwade Hoek'
    And the overview contains area 'Nieuwkoopse Plassen & De Haeck'
    And the overview contains area 'Voornes Duin'
    And the overview contains area 'Grevelingen'
    And the overview contains area 'Lingegebied & Diefdijk-Zuid'
    And the overview contains area 'Meijendel & Berkheide'
    And the overview contains area 'Biesbosch'
    And the overview contains area 'Westduinpark & Wapendal'
    And the overview contains area 'Coepelduynen'
    And the overview contains area 'Krammer-Volkerak'
    And the overview contains area 'Uiterwaarden Lek'
    # reset filters 
    And the user filters permit dashboard on province 'Nederland'
    And the user filters notice dashboard on province 'Nederland'

  @register-filter-dashboard
  Scenario: As an editor i can filter the permit dashboard table on zuid holland and check the habitats
    Given the user is logged in as 'EDITOR'
    And wait until the page is loaded
    And the notification panel contains no errors
    When the user filters permit dashboard on province 'Zuid-Holland'
    And the user filters notice dashboard on province 'Limburg'
    And wait until the page is loaded
    # check presence of all relevant habitats for this area 
    Then the overview contains area 'Solleveld & Kapittelduinen'
    And the overview contains area 'Kennemerland-Zuid'
    And the overview contains area 'Duinen Goeree & Kwade Hoek'
    And the overview contains area 'Nieuwkoopse Plassen & De Haeck'
    And the overview contains area 'Voornes Duin'
    And the overview contains area 'Grevelingen'
    And the overview contains area 'Lingegebied & Diefdijk-Zuid'
    And the overview contains area 'Meijendel & Berkheide'
    And the overview contains area 'Biesbosch'
    And the overview contains area 'Westduinpark & Wapendal'
    And the overview contains area 'Coepelduynen'
    And the overview contains area 'Krammer-Volkerak'
    And the overview contains area 'Uiterwaarden Lek'
    # reset filters 
    And the user filters permit dashboard on province 'Nederland'
    And the user filters notice dashboard on province 'Nederland'

  @register-filter-dashboard
  Scenario: As a superuser i can filter the notice dashboard on zuid holland and check the habitats
    Given the user is logged in as 'SUPERUSER'
    And wait until the page is loaded
    And the notification panel contains no errors
    When the user filters permit dashboard on province 'Zuid-Holland'
    And the user filters notice dashboard on province 'Limburg'
    And wait until the page is loaded
    # check presence of all relevant habitats for this area 
    Then the overview contains area 'Solleveld & Kapittelduinen'
    And the overview contains area 'Kennemerland-Zuid'
    And the overview contains area 'Duinen Goeree & Kwade Hoek'
    And the overview contains area 'Nieuwkoopse Plassen & De Haeck'
    And the overview contains area 'Voornes Duin'
    And the overview contains area 'Grevelingen'
    And the overview contains area 'Lingegebied & Diefdijk-Zuid'
    And the overview contains area 'Meijendel & Berkheide'
    And the overview contains area 'Biesbosch'
    And the overview contains area 'Westduinpark & Wapendal'
    And the overview contains area 'Coepelduynen'
    And the overview contains area 'Krammer-Volkerak'
    And the overview contains area 'Uiterwaarden Lek'
    # reset filters 
    And the user filters permit dashboard on province 'Nederland'
    And the user filters notice dashboard on province 'Nederland'

  @register-filter-application
  Scenario: As a viewer i can filter permit applications to narrow down the amount shown in the overview
    Given the user is logged in as 'VIEWER'
    And the menu item 'applications' is selected
    When the overview filter panel is toggled
    And the overview filter LOCATION is set to 'Groningen'
    And the overview filter AUTHORITY is set to 'Ministerie van Infrastructuur en Waterstaat'
    Then the application with description 'Test permit3530 voor Groningen, Provincie Groningen' is opened
    And wait for 3 seconds

  @register-filter-application
  Scenario: As a viewer i can reset all filter options with the filter reset button 
    Given the user is logged in as 'VIEWER'
    And the menu item 'applications' is selected
    And the overview filter panel is toggled
    # 
    And the overview filter STATUS is set to 'OR afgeboekt'
    And the overview filter STATUS is set to 'In de wachtrij'
    #
    And the overview filter LOCATION is set to 'Zeeland'
    And the overview filter LOCATION is set to 'Utrecht'
    And the overview filter LOCATION is set to 'Groningen'
    #
    And the overview filter AUTHORITY is set to 'Provincie Drenthe'
    And the overview filter AUTHORITY is set to 'Ministerie van Infrastructuur en Waterstaat'
    #
    And the overview filter SECTOR is set to 'Energie'
    And the overview filter SECTOR is set to 'Stalemissies'
    And the overview filter SECTOR is set to 'Basismetaal'
    #
    And the overview filter FROM DATE is set to '14-03-2014'
    #
    And the overview filter TILL DATE is set to '15-09-2015'
    #
    And the overview filter is applied
    And wait for 3 seconds
    And the overview filter panel is toggled
    And the overview filter is reset
    And the overview filter is applied
