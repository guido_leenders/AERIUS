@nightly @scenarios @scenarios-calculate
Feature: A user can set up a new scenario by clicking the 'new' button from the scenarios overview page 
  The scenarios set up page allows the user to import scenarios files and set calculation options  

  Background: Open scenario calculator to login and open new calculation setup window
    Given AERIUS SCENARIO is open
    And the menu item -scenarios- is selected
    And the user authenticates with api key '7a60054572184f40b5e01436547be09b'
    And the user opens a new connect scenario page

  @scenarios-setup @AER-1561
  Scenario: A user can set up a new Connect scenario by clicking the New button  
    When the scenarios set up page is shown
    And the calculation options panel is open
    Then the notification panel contains no errors

  @scenarios-import
  Scenario: After uploading a new file it is shown in the list
    When the user imports scenario file 'AERIUS_comparison_situation1.gml'
    Then the scenario file 'AERIUS_comparison_situation1.gml' is shown in the list
    And the notification panel contains no errors

## Functionality postponed until next development cycle
#  Scenario: After uploading a new file it's meta data is shown in project information
#
#  @scenarios-options
#  Scenario: With calculation option set to PDF column 'situation' is visible while column 'research area' is hidden
#    When the user sets calculation option 'form' to 'PDF'
#    Then the table column 'Situatie' is shown
#    And the table column 'Onderzoeksgebied' is hidden
#
#  @scenarios-options
#  Scenario: With calculation option set to GML column 'research area' is visible while column 'situation' is hidden
#    When the user sets calculation option 'form' to 'GML'
#    Then the table column 'Onderzoeksgebied' is shown
#    And the table column 'Situatie' is hidden

  @scenarios-delete
  Scenario: A user can delete a file to remove it from the list
    When the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user deletes scenario file 'AERIUS_comparison_situation1.gml'
    Then the scenario file 'AERIUS_comparison_situation1.gml' is not shown in the list

  @scenarios-delete
  Scenario: The calculate button is disabled when there are no uploaded files present
    When the scenario file list is empty
    Then the calculate button is disabled
    And the notification panel contains no errors

  @scenarios-delete
  Scenario: The calculate button is disabled when all uploaded files are deleted
    When the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user deletes scenario file 'AERIUS_comparison_situation1.gml'
    Then the calculate button is disabled
    And the notification panel contains no errors

  @scenarios-cancel
  Scenario: Cancelling the scenario setup without any uploaded files takes the user back to the overview page immediately
    When the user cancels the connect scenario setup
    Then the scenarios overview page is shown
    And the notification panel contains no errors

  @scenarios-cancel @AER-1561 @AER-1733
  Scenario: After cancelling a Connect scenario any new scenario should start from a default set up page 
    Given the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user cancels the connect scenario setup
    And the user accepts the confirmation dialog
    And the scenarios overview page is shown
    When the user opens a new connect scenario page
    Then the scenario file 'AERIUS_comparison_situation1.gml' is not shown in the list
    And the calculation option 'form' is set to 'GML'
    And the calculation option 'type' is set to 'voor natuurgebieden'
    And the user sets calculation option 'year' to '2025'
    And the calculation option 'year' is set to '2025'
    And the notification panel contains no errors

  @scenarios-cancel
  Scenario: Cancelling the scenario setup with uploaded files present asks for confirmation before returning to the overview page
    Given the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user cancels the connect scenario setup
    When the user accepts the confirmation dialog
    Then the scenarios overview page is shown
    And the notification panel contains no errors

  @scenarios-cancel
  Scenario: After cancelling the cancel confirmation the user stays on the scenario setup page 
    Given the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user cancels the connect scenario setup
    When the user cancels the confirmation dialog
    Then the scenarios set up page is shown
    And the notification panel contains no errors

  @scenarios-calculations-comparison @AER-1561 @comparison @AER-1727 @ignorepdf
  Scenario: A user can setup and calculate a comparison scenario PDF
    Given the user sets calculation option 'form' to 'PDF - benodigde ontwikkelingsruimte'
    And the user sets calculation job id to 'ComparisonPDF'
    And the user sets calculation option 'year' to '2030'
    And the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user imports scenario file 'AERIUS_comparison_situation2.gml'
    And the scenario imported file '1' is marked as situation 'huidig'
    And the scenario imported file '2' is marked as situation 'beoogd'
    When the scenarios calculation is started
    Then the scenarios overview page is shown
    And the scenarios overview contains job id 'ComparisonPDF' with status
      | Wachtrij |
      | ha berekend |
    And the notification panel contains no errors
    And the notification panel contains the message 'Uw berekening is succesvol gestart.'

  # TODO: Obsolete step priority permit radius for item @AER-1718.
  @scenarios-calculations @AER-1561 @AER-1718 @cal19ignore
  Scenario: A user can setup and calculate a research area scenario GML
    Given the user sets calculation option 'form' to 'GML'
    And the user sets calculation job id to 'ResearchAreaGML'
    And the user sets calculation option 'year' to '2030'
    And the user enables calculation option 'priority permit'
    And the user sets calculation option 'priority permit radius' to 'Prioritair project Hoofdwegennet'
    And the user imports scenario file 'AERIUS_research_plan.gml'
    And the user imports scenario file 'AERIUS_research_area.gml'
    And the user sets scenario file at row '1' as research area
    When the scenarios calculation is started
    Then the scenarios overview page is shown
    And the scenarios overview contains job id 'ResearchAreaGML' with status 'Wachtrij'
    And the notification panel contains no errors
    And the notification panel contains the message 'Uw berekening is succesvol gestart.'

  @scenarios-calculations @AER-1604 @#1414 @cal19ignore
  Scenario: The user receives and error message when a research area calculation is started without enabling radius option
    Given the user sets calculation option 'form' to 'GML'
    And the user imports scenario file 'AERIUS_research_plan.gml'
    And the user imports scenario file 'AERIUS_research_area.gml'
    And the user sets scenario file at row '1' as research area
    When the scenarios calculation is started
    Then the notification panel contains the message 'Uw berekening kon niet gestart worden door fouten: 1017'

  @scenarios-calculations @AER-1733
  Scenario: After starting a Connect scenario calculation a new scenario should start from a fresh set up page without any files and the default year  
    Given the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the scenarios calculation is started
    When the scenarios overview page is shown
    And the user opens a new connect scenario page
    Then the scenario file 'AERIUS_comparison_situation1.gml' is not shown in the list
    And the calculation option 'form' is set to 'GML'
    And the calculation option 'type' is set to 'voor natuurgebieden'
    And the calculation option year is set to current year
    And the notification panel contains no errors

  @scenarios-options @AER-1561 @AER-1717 @cal19ignore
  Scenario: As a user i can specify a Temporary Project and duration using the calculation options
    Given the user imports scenario file 'AERIUS_comparison_situation1.gml'
    # check default option values
    When the user enables calculation option 'temporary project'
    Then the calculation option 'temporary project years' is set to '1'
    # change option and check if set correctly
    When the user sets calculation option 'temporary project years' to '3'
    Then the calculation option 'temporary project years' is set to '3'
    # start calculation and check for errors #
    When the scenarios calculation is started
    Then the scenarios overview page is shown
    And the notification panel contains no errors
    And the notification panel contains the message 'Uw berekening is succesvol gestart.'

  # TODO: Obsolete step priority permit radius for item @AER-1718.
  @scenarios-options @AER-1561 @AER-1718 @distancelimitignore
  Scenario: As a user i can specify a Priority Project' and radius for roads or shipping using the calculation options
    Given the user imports scenario file 'AERIUS_comparison_situation1.gml'
    # check default option values
    When the user enables calculation option 'priority permit'
    Then the calculation option 'priority permit radius' is set to 'Prioritair project Hoofdvaarwegennet'
    # change option and check if set correctly
    When the user sets calculation option 'priority permit radius' to 'Prioritair project Hoofdwegennet'
    Then the calculation option 'priority permit radius' is set to 'Prioritair project Hoofdwegennet'
    # start calculation and check for errors
    When the scenarios calculation is started
    Then the scenarios overview page is shown
    And the notification panel contains no errors
    And the notification panel contains the message 'Uw berekening is succesvol gestart.'

  @scenarios-options @bugfix @AER-1561 @AER-1717 @cal19ignore
  Scenario: The start year of a temporary project should match the value set for the year calculation option
    Given the user sets calculation option 'year' to '2021'
    And the user enables calculation option 'temporary project'
    Then the temporary project start year should be '2021'
    And the notification panel contains no errors

  # TODO: Temporarily ignored until performance issues are fixed
  @scenarios-filter @AER-1314 @cal19ignore
  Scenario: The filtered overview should only show active jobs or all jobs when filter option is disabled  
    Given the user cancels the connect scenario setup
    And the scenarios overview page is shown
    And the scenarios overview filter is toggled
    When the scenarios overview filter is 'enabled'
    Then the scenarios overview page shows active jobs only
    ###
    When the menu item -utils- is selected
    And the menu item -scenarios- is selected
    Then the scenarios overview filter is 'disabled'
    And the scenarios overview shows all jobs
