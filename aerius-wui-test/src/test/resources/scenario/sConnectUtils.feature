@nightly @scenario-utils
Feature: Various useful utilities provided by AERIUS Connect are made available using the scenario user interface 
  This includes validation and conversion for single files as well as utils to determine delta, highest or summed values from multiple GML result files
  Any erroneous or invalid input should be detected and brought to the attention of the user with validation and/or error-messages

#  V A L I D A T E 

  @scenario-validate-success
  Scenario: A user can submit a single valid file and validation will report success without warning or error messages
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file 'IMAER2.2_Valid.gml'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label is empty
    And the notification panel contains no errors

  @Scenario-validate-source-properties
  Scenario Outline: A user can submit a single valid file with adjusted source properties and validation will report success without warning or error messages
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file '<gml_name>'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label is empty
    And the notification panel contains no errors

    Examples: Different combinations of source properties
      | gml_name                          |
      | IMAER2.2_ForcedNoBuilding.gml     |
      | IMAER2.2_ForcedWithBuilding.gml   |
      | IMAER2.2_UnforcedNoBuilding.gml   |
      | IMAER2.2_UnforcedWithBuilding.gml |

  @scenario-validate-warning
  Scenario: A user can submit an old but valid file and validation will report success and a warning message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file 'AERIUS_comparison_situation1.gml'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn waarschuwingen gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label contains the message 'Uw AERIUS bestand bevat de volgende waarschuwing(en): 5223 - The GML file is valid but is not the latest IMAER version, provided version "V1_1" expected version "V2_2"'
    And the error label is empty
    And the notification panel contains no errors

  @scenario-validate-error
  Scenario: A user can submit an invalid file and validation will report failure and an error message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file 'hattemerbroek.csv'
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): The file was not recognized and could not be imported.'
    And the notification panel contains no errors

#  C O N V E R T 

  @scenario-convert-success
  Scenario: A user can submit a single valid file and conversion will report success without warning or error messages
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user converts file 'IMAER2.2_Valid.GML'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS bestand(en), zie browser download dialoogscherm voor het resultaat. (Let op, uw browser moet pop-ups op deze pagina toestaan om het bestand te ontvangen)'
    And the warning label is empty
    And the error label is empty
    And the notification panel contains no errors

  @scenario-convert-warning
  Scenario: A user can submit an SRM2 file format and conversion will report success and a warning message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user converts file 'hattemerbroek.csv'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn waarschuwingen gevonden in uw aangeleverde AERIUS bestand(en), zie browser download dialoogscherm voor het resultaat. (Let op, uw browser moet pop-ups op deze pagina toestaan om het bestand te ontvangen)'
    And the warning label contains the message 'Uw AERIUS bestand bevat de volgende waarschuwing(en): 6207 - The SRM2 csv-file file format not supported will be removed in near future.'
    And the error label is empty
    And the notification panel contains no errors

#  D E L T A 

  @scenario-delta-success
  Scenario: Submitting multiple files to determine delta values should be possible only when the user sets the current and proposed files properly      
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option 'DELTA_VALUE' is selected
    And the user continues to next step
    And the file 'AERIUS_RS.zip' is imported
    And the file 'AERIUS_RA.zip' is imported
    And the file 'AERIUS_RS.zip' is shown in the overview
    And the file 'AERIUS_RA.zip' is shown in the overview
    When the user continues to next step
    Then the notification panel contains no errors
    And the util message label contains the message 'Voor verschil bepaling moet een huidige en beoogde situatie geselecteerd worden.'
    When the scenario imported file '1' is marked as situation 'huidig'
    And the scenario imported file '2' is marked as situation 'beoogd'
    And the user continues to next step
    And wait until the page is loaded
    Then the notification panel contains no errors
    And the success label contains the message 'Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS bestand(en), zie browser download dialoogscherm voor het resultaat. (Let op, uw browser moet pop-ups op deze pagina toestaan om het bestand te ontvangen)'
    And the warning label is empty
    And the error label is empty

#  H I G H E S T  V A L U E

  @scenario-highest-success
  Scenario: Submitting multiple files to determine highest values should be possible only when the user sets the current and proposed files properly
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option 'HIGHEST_VALUE' is selected
    And the user continues to next step
    And the file 'AERIUS_RS.zip' is imported
    And the file 'AERIUS_RA.zip' is imported
    And the file 'AERIUS_RS.zip' is shown in the overview
    And the file 'AERIUS_RA.zip' is shown in the overview
    When the user continues to next step
    And wait until the page is loaded
    Then the notification panel contains no errors
    And the success label contains the message 'Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS bestand(en), zie browser download dialoogscherm voor het resultaat. (Let op, uw browser moet pop-ups op deze pagina toestaan om het bestand te ontvangen)'
    And the warning label is empty
    And the error label is empty

#  T O T A L  V A L U E

  @scenario-total-success
  Scenario: Submitting multiple files to determine highest values should be possible only when the user sets the current and proposed files properly
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option 'TOTAL_VALUE' is selected
    And the user continues to next step
    And the file 'AERIUS_RS.zip' is imported
    And the file 'AERIUS_RA.zip' is imported
    And the file 'AERIUS_RS.zip' is shown in the overview
    And the file 'AERIUS_RA.zip' is shown in the overview
    When the user continues to next step
    And wait until the page is loaded
    Then the notification panel contains no errors
    And the success label contains the message 'Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS bestand(en), zie browser download dialoogscherm voor het resultaat. (Let op, uw browser moet pop-ups op deze pagina toestaan om het bestand te ontvangen)'
    And the warning label is empty
    And the error label is empty

  @scenario-multifile-error @rainy
  Scenario Outline: Submitting multiple files to a single file operation should result in a message notifying the user that only 1 file can be processed at a time
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option '<util_option>' is selected
    And the user continues to next step
    And the file '<file_name_1>' is imported
    And the file '<file_name_2>' is imported
    And the file '<file_name_1>' is shown in the overview
    And the file '<file_name_2>' is shown in the overview
    When the user continues to next step
    Then the notification panel contains no errors
    And the util message label contains the message 'Maximaal aantal bestand(en) toegestaan 1'

    Examples: Util options requiring a single file and two files to test requirement notifications
      | util_option | file_name_1   | file_name_2   |
      | VALIDATE    | AERIUS_RS.zip | AERIUS_RA.zip |
      | CONVERT     | AERIUS_RS.zip | AERIUS_RA.zip |

  @scenario-multifile-error @rainy
  Scenario Outline: Submitting multiple files to a single file operation should be possible to correct
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option '<util_option>' is selected
    And the user continues to next step
    And the file '<file_name_1>' is imported
    And the file '<file_name_2>' is imported
    And the file '<file_name_1>' is shown in the overview
    And the file '<file_name_2>' is shown in the overview
    When the user continues to next step
    Then the notification panel contains no errors
    And the util message label contains the message 'Maximaal aantal bestand(en) toegestaan 1'
    And the user deletes file '<file_name_2>' from the overview
    And the user continues to next step
    And the success label contains the message '<message>'

    Examples: Util options requiring a single file and two files to test requirement notifications
      | util_option | file_name_1        | file_name_2   | message                                                                                                                                                                                                                                                   |
      | VALIDATE    | AERIUS_IMAER22.zip | AERIUS_RA.zip | Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS GML bestand(en).                                                                                                                                          |
      | CONVERT     | AERIUS_IMAER22.zip | AERIUS_RA.zip | Uw taak is uitgevoerd, er zijn geen waarschuwingen of fouten gevonden in uw aangeleverde AERIUS bestand(en), zie browser download dialoogscherm voor het resultaat. (Let op, uw browser moet pop-ups op deze pagina toestaan om het bestand te ontvangen) |     

  @scenario-singlefile-error @rainy
  Scenario Outline: Submitting a single file to a multi file operation should result in a message notifying the user that at least 2 files are required for processing
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option '<util_option>' is selected
    And the user continues to next step
    And the file '<file_name_1>' is imported
    And the file '<file_name_1>' is shown in the overview
    When the user continues to next step
    Then the notification panel contains no errors
    And the util message label contains the message 'Minimaal vereiste aantal bestanden 2'

    Examples: Util options requiring multiple files and one file to test requirement notifications
      | util_option   | file_name_1   |
      | DELTA_VALUE   | AERIUS_RA.zip |
      | HIGHEST_VALUE | AERIUS_RA.zip |
      | TOTAL_VALUE   | AERIUS_RA.zip |

  @scenario-validate-error @rainy
  Scenario: A user can submit a single invalid [empty] file and validation will report failure with an appropriate error message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file 'AERIUS_empty.gml'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): [errorCode=IMPORTED_FILE_NOT_FOUND,'
    And the notification panel contains no errors

  @scenario-validate-error @rainy
  Scenario: A user can submit a single invalid [header only] file and validation will report failure with an appropriate error message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file 'AERIUS_xml_header_only.gml'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): 5211 - The GML file contains an error. This information is known about the error: ParseError at [row,col]:[1,56] Message: Premature end of file..'
    And the notification panel contains no errors

  @scenario-convert-error @rainy
  Scenario: A user can submit a single invalid [empty] file and implicit validation on conversion will report failure with an appropriate error message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user converts file 'AERIUS_empty.gml'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): [errorCode=IMPORTED_FILE_NOT_FOUND,'
    And the notification panel contains no errors

  @scenario-convert-error @rainy
  Scenario: A user can submit a single invalid [header only] file and implicit validation on conversion will report failure with an appropriate error message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user converts file 'AERIUS_xml_header_only.gml'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): 5211 - The GML file contains an error. This information is known about the error: ParseError at [row,col]:[1,56] Message: Premature end of file..'
    And the notification panel contains no errors

  @scenario-validate-PP @AER-1375
  Scenario: VALIDATING a project file including the FORBIDDEN SECTOR 9000 will result in ERROR
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option 'VALIDATE' is selected
    And the user continues to next step
    And the file 'AERIUS_sector9000.gml' is imported
    And the file 'AERIUS_sector9000.gml' is shown in the overview
    When the user toggles validate as PP
    And the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): 20025 - A source with sector 9000 "Plan" has been found. This is not allowed in a priority project.'
    And the notification panel contains no errors

  @scenario-validate-PP @AER-1375
  Scenario: VALIDATING a project file including the FORBIDDEN SECTOR 9999 will result in ERROR
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option 'VALIDATE' is selected
    And the user continues to next step
    And the file 'AERIUS_sector9999.gml' is imported
    And the file 'AERIUS_sector9999.gml' is shown in the overview
    When the user toggles validate as PP
    And the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is niet geslaagd, er zijn fouten gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label is empty
    And the error label contains the message 'Uw AERIUS bestand bevat de volgende fout(en): 20025 - A source with sector 9999 "Anders..." has been found. This is not allowed in a priority project.'
    And the notification panel contains no errors

  @scenario-validate-PP @AER-1375
  Scenario: VALIDATING a project file including the FORBIDDEN SECTOR 9999 as regular GML will NOT result in ERROR
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the utils option 'VALIDATE' is selected
    And the user continues to next step
    And the file 'AERIUS_sector9999.gml' is imported
    And the file 'AERIUS_sector9999.gml' is shown in the overview
    And the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn waarschuwingen gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label contains the message 'Uw AERIUS bestand bevat de volgende waarschuwing(en): 5223 - The GML file is valid but is not the latest IMAER version, provided version "V1_1" expected version "V2_2"'
    And the notification panel contains no errors

    @scenario-validate-backwards-compatibility @AER-1777 @AER-1778
  Scenario Outline: A user can submit an old but valid gml files and validation will report success and a warning message
    Given AERIUS SCENARIO is open
    And the menu item -utils- is selected
    And the user validates file '<gml_name>'
    And the notification panel contains no errors
    When the user continues to next step
    And wait until the page is loaded
    Then the success label contains the message 'Uw taak is uitgevoerd, er zijn waarschuwingen gevonden in uw aangeleverde AERIUS GML bestand(en).'
    And the warning label contains the message '<message>'
    And the error label is empty
    And the notification panel contains no errors

    Examples: Submiting different IMAER versions to validate
      | gml_name           | message                                                                                                                                                                 |
      | IMAER2.1_Valid.gml | Uw AERIUS bestand bevat de volgende waarschuwing(en): 5223 - The GML file is valid but is not the latest IMAER version, provided version "V2_1" expected version "V2_2" |
      | IMAER2.0_Valid.gml | Uw AERIUS bestand bevat de volgende waarschuwing(en): 5223 - The GML file is valid but is not the latest IMAER version, provided version "V2_0" expected version "V2_2" |
