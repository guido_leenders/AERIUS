@scenario-manual-capture
Feature: Capture screenshots for the AERIUS SCENARIO manual

  Background: 
    Given AERIUS SCENARIO is open sized 1191 x 911

  Scenario: Capture screenshots of basic functionality for the manual
    # 3022 Werkwijze handleiding (No Image)
    # 3025 Helpdesk (No Image)
    # 3030 Startscherm
    And wait for 3 seconds
    And take a screenshot for the 'scenario' manual named '3030_Basisfuncties-Start-Scenario-Stap_1'

    # 3082 Berekening importeren (zelfde basisscherm als #3030)
    And take a screenshot for the 'scenario' manual named '3082_Basisfuncties-Import-Scenario-Stap_1'

    # 3079 Toepassing scenario
    And the startup is set to import a file
    And the ZIP file 'scenario_reservering.zip' is imported
#    And the calculation ends
    And wait for 3 seconds
    And take a screenshot for the 'scenario' manual named '3079_Basisfuncties-Toepassing-Scenario-Stap_1'

    # 3077 Aflezen resultaten
    And the search for 'Veluwe' is started
    And the search suggestion 'Veluwe' is selected
    And wait for 3 seconds
    And take a screenshot for the 'scenario' manual named '3077_Basisfuncties-Resultaten-Scenario-Stap_1'

    # 3033 Resultaten in tabel
    And take a screenshot for the 'scenario' manual named '3033_Basisfuncties-ResultaatTabel-Scenario-Stap_2'

    # 3084 Resultaten in grafiek
    ### Grafiek weergave resultaten niet beschikbaar in scenario
    And the sub menu item -results graph- is selected
    And wait for 3 seconds
    And take a screenshot for the 'scenario' manual named '3084_Basisfuncties-ResultaatGrafiek-Scenario-Stap_3'

    # 3085 Resultaten filteren
    #Step skipped because of removed filter option
    #And the sub menu item -results filter- is selected
    And wait for 3 seconds
    And take a screenshot for the 'scenario' manual named '3085_Basisfuncties-ResultaatFilter-Scenario-Stap_4'

  Scenario: Capture screenshot of traffic network visualisation for the manual
    Given the startup is set to import a file
    When the GML file 'hattemerbroek.gml' is imported
    And the scenario import ends
    And the menu item -emission sources- is selected
    And the source with id tag '1' is clicked
    And the map is zoomed out 3 times
    When the notifications are all deleted
    Then take a screenshot for the 'scenario' manual named '3090_Verkeersnetwerk-Eigenschappen'
    When the taskbar layer-panel is opened
    Then take a screenshot for the 'scenario' manual named '3091_Verkeersnetwerk-Kaartlagen'

  Scenario: Capture screenshots of Connect utilities for the manual
    When the menu item -utils- is selected
    Then take a screenshot for the 'scenario' manual named '3100_Utilfuncties-Overzicht'

    When the menu item -utils- is selected
    And the utils option 'VALIDATE' is selected
    And the user continues to next step
    Then take a screenshot for the 'scenario' manual named '3101_Utilfuncties-Valideren'

    When the user returns to previous step
    And the utils option 'CONVERT' is selected
    And the user continues to next step
    Then take a screenshot for the 'scenario' manual named '3102_Utilfuncties-Converteren'

    When the user returns to previous step
    And the utils option 'DELTA_VALUE' is selected
    And the user continues to next step
    Then take a screenshot for the 'scenario' manual named '3103_Utilfuncties-Verschil-Stap_1'

    When the file 'AERIUS_RS.zip' is imported
    And the file 'AERIUS_RA.zip' is imported
    And the scenario imported file '1' is marked as situation 'huidig'
    And the scenario imported file '2' is marked as situation 'beoogd'
    Then take a screenshot for the 'scenario' manual named '3104_Utilfuncties-Verschil-Stap_2'

    When the user returns to previous step
    And the utils option 'HIGHEST_VALUE' is selected
    And the user continues to next step
    Then take a screenshot for the 'scenario' manual named '3105_Utilfuncties-Hoogste'

    When the user returns to previous step
    And the utils option 'TOTAL_VALUE' is selected
    And the user continues to next step
    Then take a screenshot for the 'scenario' manual named '3106_Utilfuncties-Totaal'

  @scenario-calculations @cal19ignore
  Scenario: Capture screenshots of Connect Calculations for the manual
    When the menu item -scenarios- is selected
    Then take a screenshot for the 'scenario' manual named '3200_Rekenfuncties-Login'

    When the user authenticates with api key '7a60054572184f40b5e01436547be09b'
    And the scenarios overview page is shown
    Then take a screenshot for the 'scenario' manual named '3201_Rekenfuncties-JobOverzicht'

    When the user opens a new connect scenario page
    Then take a screenshot for the 'scenario' manual named '3202_Rekenfuncties-InvoerBerekening'

    # Changed steps with reference to (temporarily?) removed PDF to use GML output option 
    When the user sets calculation job id to 'Vergelijking GML'
    # And the user sets calculation option 'form' to 'PDF'
    And the user imports scenario file 'AERIUS_comparison_situation1.gml'
    And the user imports scenario file 'AERIUS_comparison_situation2.gml'
    And the scenario imported file '1' is marked as situation 'huidig'
    And the scenario imported file '2' is marked as situation 'beoogd'
    Then take a screenshot for the 'scenario' manual named '3203_Rekenfuncties-InvoerBerekening-Stap_2'
    And the scenarios calculation is started

    When the scenarios overview page is shown
    And the scenarios overview contains job id 'Vergelijking GML' with status 'Wachtrij'
    And the notification panel contains no errors
    And the notifications panel is closed
    Then take a screenshot for the 'scenario' manual named '3204_Rekenfuncties-JobOverzicht-Actief'

    When the scenarios overview filter is toggled
    And the scenarios overview filter is 'enabled'
    Then take a screenshot for the 'scenario' manual named '3205_Rekenfuncties-JobOverzicht-Filter'
