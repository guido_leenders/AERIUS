@nightly @scenarios-analysis
Feature: Analytical map layers for traffic networks as requested by the Dutch Ministry of Infrastructure and Water Management

  Background: Open scenario for importing traffic network files
    Given AERIUS SCENARIO is open

  @scenarios-csv
  Scenario: Checking the presence of analytical map layers for CSV traffic network sources
    Given the startup is set to import a file
    When the CSV file 'hattemerbroek.csv' is imported
    And the scenario import ends
    And the menu item -emission sources- is selected
    And the source with id tag '1' is clicked
    And the map is zoomed out 3 times
    Then the notification panel contains no errors
    And the notification panel contains the message 'Uw bestand is geïmporteerd.Er is 1 bron toegevoegd.'
    And the notification panel contains the message 'Het SRM2 csv-bestandsformaat wordt officeel niet ondersteund en mogelijk op termijn verwijderd.'
    And the following layers are present
        |Wegverkeer totale intensiteit|
        |Wegverkeer totale intensiteit licht verkeer|
        |Wegverkeer snelheidstypering|
        |Bron labels|

  @scenarios-gml
  Scenario: Checking the presence of analytical map layers for GML traffic network sources
    Given the startup is set to import a file
    When the GML file 'hattemerbroek.gml' is imported
    And the scenario import ends
    And the menu item -emission sources- is selected
    And the source with id tag '1' is clicked
    And the map is zoomed out 3 times
    Then the notification panel contains no errors
    And the notification panel contains the message 'Uw bestand is geïmporteerd.Er is 1 bron toegevoegd.'
    And the following layers are present
        |Wegverkeer totale intensiteit|
        |Wegverkeer totale intensiteit licht verkeer|
        |Wegverkeer snelheidstypering|
        |Bron labels|

  @scenarios-nonetwork
  Scenario: Checking the absence of analytical map layers for non traffic network sources
    Given the startup is set to import a file
    When the GML file 'AERIUS_comparison_situation1.gml' is imported
    When the scenario import ends
    Then the notification panel contains no errors
    And the following layers are absent
        |Wegverkeer totale intensiteit|
        |Wegverkeer totale intensiteit licht verkeer|
        |Wegverkeer snelheidstypering|
        |Bron labels|
