<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:imaer="http://imaer.aerius.nl/2.0" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/2.0 http://imaer.aerius.nl/2.0/IMAER.xsd">
    <imaer:metadata>
        <imaer:AeriusCalculatorMetadata>
            <imaer:project>
                <imaer:ProjectMetadata>
                    <imaer:year>2017</imaer:year>
                    <imaer:permitCalculationRadiusType>PRIORITY_PROJECT_MAIN_SHIPPING_LANES</imaer:permitCalculationRadiusType>
                </imaer:ProjectMetadata>
            </imaer:project>
            <imaer:situation>
                <imaer:SituationMetadata>
                    <imaer:name>Binnenvaart Route &amp; Aanlegplaats</imaer:name>
                    <imaer:reference>RSwM2vvpReov</imaer:reference>
                </imaer:SituationMetadata>
            </imaer:situation>
            <imaer:calculation>
                <imaer:CalculationMetadata>
                    <imaer:type>PERMIT</imaer:type>
                    <imaer:substance>NOX</imaer:substance>
                    <imaer:substance>NO2</imaer:substance>
                    <imaer:substance>NH3</imaer:substance>
                    <imaer:resultType>DEPOSITION</imaer:resultType>
                </imaer:CalculationMetadata>
            </imaer:calculation>
            <imaer:version>
                <imaer:VersionMetadata>
                    <imaer:aeriusVersion>1.0-SNAPSHOT_20170224_7d2d695817</imaer:aeriusVersion>
                    <imaer:databaseVersion>1.0-SNAPSHOT_20170224_0cfe229b75</imaer:databaseVersion>
                </imaer:VersionMetadata>
            </imaer:version>
        </imaer:AeriusCalculatorMetadata>
    </imaer:metadata>
    <imaer:featureMember>
        <imaer:MooringInlandShippingEmissionSource sectorId="7610" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>BB-Haven-Noord</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.CURVE">
                            <gml:posList>119998.54 425149.2 120171.58 424950.96 119731.42 424542.72</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>32734.86649572159</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>8035.409217464449</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:mooringInlandShipping>
                <imaer:MooringInlandShipping shipType="M9">
                    <imaer:description>Hringhorni</imaer:description>
                    <imaer:averageResidenceTime>712</imaer:averageResidenceTime>
                    <imaer:route>
                        <imaer:InlandShippingRoute>
                            <imaer:direction>ARRIVE</imaer:direction>
                            <imaer:percentageLaden>85</imaer:percentageLaden>
                            <imaer:route>
								<gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.InlandMooringRoute.1_0_0">
									<gml:posList>120047.67054386655 424836.0363441205 120069.94 424813.62</gml:posList>
								</gml:LineString>
                            </imaer:route>
                            <imaer:shippingMovements>356</imaer:shippingMovements>
                            <imaer:waterway>
								<imaer:InlandWaterway>
									<imaer:type>CEMT_Va</imaer:type>
								</imaer:InlandWaterway>
                            </imaer:waterway>
                        </imaer:InlandShippingRoute>
                    </imaer:route>
                    <imaer:route>
                        <imaer:InlandShippingRoute>
                            <imaer:direction>DEPART</imaer:direction>
                            <imaer:percentageLaden>15</imaer:percentageLaden>
                            <imaer:route>
								<gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.InlandMooringRoute.1_0_1">
									<gml:posList>120047.67054386655 424836.0363441205 120069.94 424813.62</gml:posList>
								</gml:LineString>
                            </imaer:route>
                            <imaer:shippingMovements>356</imaer:shippingMovements>
                            <imaer:waterway>
								<imaer:InlandWaterway>
									<imaer:type>CEMT_Va</imaer:type>
								</imaer:InlandWaterway>
                            </imaer:waterway>
                        </imaer:InlandShippingRoute>
                    </imaer:route>
                </imaer:MooringInlandShipping>
            </imaer:mooringInlandShipping>
        </imaer:MooringInlandShippingEmissionSource>
    </imaer:featureMember>
    <imaer:featureMember>
        <imaer:InlandShippingEmissionSource sectorId="7620" gml:id="ES.2">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.2</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>BB-Route-Noord</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.2.CURVE">
                            <gml:posList>119998.54 425147.1 118652.02 423905.58 118041.34000000001 423544.38 117323.14 423371.34 116594.01999999999 423240.3 115891.78 423431.82 115270.18000000001 423640.14 114541.06 423606.54 113260.9 423253.74 112195.78000000001 422524.62 111530.5 421765.26 111026.50000000001 420898.38 110609.86 419413.26 109843.78000000003 418375.01999999996 108835.78 417471.18 106860.1 416073.42 104830.66 414971.34</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>5972.935077681283</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>175.60283350600596</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:inlandShipping>
                <imaer:InlandShipping shipType="M9">
                    <imaer:description>Hringhorni</imaer:description>
                    <imaer:numberOfShipsAtoB>1</imaer:numberOfShipsAtoB>
                    <imaer:numberOfShipsBtoA>1</imaer:numberOfShipsBtoA>
                    <imaer:percentageLadenAtoB>85</imaer:percentageLadenAtoB>
                    <imaer:percentageLadenBtoA>15</imaer:percentageLadenBtoA>
                </imaer:InlandShipping>
            </imaer:inlandShipping>
            <imaer:waterway>
                <imaer:InlandWaterway>
                    <imaer:type>CEMT_Va</imaer:type>
                </imaer:InlandWaterway>
            </imaer:waterway>
        </imaer:InlandShippingEmissionSource>
    </imaer:featureMember>
</imaer:FeatureCollectionCalculator>
