<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:imaer="http://imaer.aerius.nl/0.5" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/0.5 http://imaer.aerius.nl/0.5/IMAER.xsd">
    <imaer:aeriusCalculatorMetaData>
        <imaer:year>2015</imaer:year>
        <imaer:version>BETA10_20150115_97a81f0d53</imaer:version>
        <imaer:databaseVersion>BETA10_20150115_dedd72141d</imaer:databaseVersion>
        <imaer:situationName>Situatie 1</imaer:situationName>
    </imaer:aeriusCalculatorMetaData>
    <imaer:featureMembers>
        <imaer:OffRoadMobileSourceEmissionSource sectorId="3230" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Zandwinning</imaer:label>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Surface>
                        <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.SURFACE">
                            <gml:exterior>
<gml:LinearRing>
    <gml:posList>180191.26 326909.52 180003.1 326973.36 179599.9 326993.52 179848.54 327218.64 179945.98 327386.64 180268.54 328176.24 180325.66 328639.92 180439.9 328727.28 180701.98 328858.32 180937.18 328922.16 180974.14 328878.48 180866.62 328781.04 180691.9 328552.56 180510.46 328001.52 180191.26 326909.52</gml:posList>
</gml:LinearRing>
                            </gml:exterior>
                        </gml:Polygon>
                    </imaer:GM_Surface>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>2218.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>0.0</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:offRoadMobileSource>
                <imaer:StandardOffRoadMobileSource offRoadMobileSourceType="S3BL">
                    <imaer:description>Dumper 1</imaer:description>
                    <imaer:literFuelPerYear>100000</imaer:literFuelPerYear>
                </imaer:StandardOffRoadMobileSource>
            </imaer:offRoadMobileSource>
            <imaer:offRoadMobileSource>
                <imaer:CustomOffRoadMobileSource>
                    <imaer:description>Dumper 2</imaer:description>
                    <imaer:emission>
                        <imaer:Emission substance="NOX">
                            <imaer:value>1109.0</imaer:value>
                        </imaer:Emission>
                    </imaer:emission>
                    <imaer:emissionSourceCharacteristics>
                        <imaer:EmissionSourceCharacteristics>
                            <imaer:heatContent>0.0</imaer:heatContent>
                            <imaer:emissionHeight>4.0</imaer:emissionHeight>
                            <imaer:spread>4.0</imaer:spread>
                        </imaer:EmissionSourceCharacteristics>
                    </imaer:emissionSourceCharacteristics>
                </imaer:CustomOffRoadMobileSource>
            </imaer:offRoadMobileSource>
        </imaer:OffRoadMobileSourceEmissionSource>
    </imaer:featureMembers>
</imaer:FeatureCollectionCalculator>
