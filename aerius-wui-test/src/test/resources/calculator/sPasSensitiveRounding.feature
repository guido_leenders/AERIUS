@nightly @pas-rounding @opsfix @cal19ignore
Feature: Check PAS sensitive rounding

  # When a deposition result is PAS sensitive, i.e., when rounding could give a wrong impression 
  # due to 0,053 being rounded to 0,05, then it appears to be on the threshold rather than over. 
  # In this case the deposition result should be prefixed with '>'. The same goes for values between 1,00  and 1,005.

  @comparison
  Scenario: Upload comparison pdf, calculate and check PAS sensitive rounding
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the PDF file 'rounding_pas_sensitive.pdf' is imported
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    And the menu item -results- is selected
    And the sub menu item -results table- is selected

    When the results table is set to display as Total Deposition
    And situation 1 is selected
    And the results table is set to show the area 'Vecht- en Beneden-Reggegebied' 
    Then the result for Total Deposition in HabitatArea 'H2310' is '0,82'
    And the result for Total Deposition in HabitatArea 'H2330' is '0,66'

    When the results table is set to display as Maximum deposition
    Then the result for Maximum in HabitatArea 'H4010A' is '>0,05'
    And the result for Maximum in HabitatArea 'H2310' is '0,06'
    And the result for Maximum in HabitatArea 'H9190' is '>0,05'

    When the results table is set to display as Average
    Then the result for Average in HabitatArea 'H6120' is '0,06'
    And the result for Average in HabitatArea 'ZGH9120' is '0,06'
    And the result for Average in HabitatArea 'H5130' is '>0,05'
    And the result for Average in HabitatArea 'H4030' is '0,04'

    When the comparison tab is selected
    Then the result for Total Deposition in HabitatArea 'ZGH2310' is '+0,06'
    And the result for Total Deposition in HabitatArea 'H7110B' is '+>0,05'
    And the result for Total Deposition in HabitatArea 'H7150' is '+>0,05'
    And the result for Total Deposition in HabitatArea 'ZGH2330' is '+0,06'

    When the results table is set to display as Total Deposition
    Then the result for Total Deposition in HabitatArea 'H4010A' is '+0,79'

    When the results table is set to display as Maximum increase
    Then the result for Maximum in HabitatArea 'H7110B' is '+0,09'
    And the result for Maximum in HabitatArea 'ZGH9120' is '+0,10'
    And the result for Maximum in HabitatArea 'H6230vka' is '+0,07'
