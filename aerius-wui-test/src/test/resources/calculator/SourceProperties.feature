@source-properties @nightly
Feature: Testing calculations with source properties combinations of building influence

  Background:
    Given AERIUS CALCULATOR is open

  @source-properties @AER-1770
  Scenario: Test with properties not forced without building influence
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 129918 and Y 419196
    And the source label is set to 'Farmer Pan Fluit'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'F 1.7' with bwl-code 'BWL 2010.13.V4' and an amount of 10000
    And the source sub item is saved
    And the source characteristics is toggled
    And the source characteristics are set to height 6, heat 0.15
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -results- is selected
    And the results table is set to show the area 'Loevestein, Pompveld & Kornsche Boezem'
    Then the result for Total Deposition in HabitatArea 'H91E0C' is '0,18'

  @source-properties @AER-1770
  Scenario: Test with properties not forced with building influence
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 129918 and Y 419196
    And the source label is set to 'Farmer Pan Fluit'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'F 1.7' with bwl-code 'BWL 2010.13.V4' and an amount of 10000
    And the source sub item is saved
    And the source characteristics is toggled
    When the source property emission height is set to 6
    And building influence is selected
    And the source heat content is set with building influence using lenght 12,0, width 3,0, height 4,0, orientation 100
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -results- is selected
    And the results table is set to show the area 'Loevestein, Pompveld & Kornsche Boezem'
    Then the result for Total Deposition in HabitatArea 'H91E0C' is '0,23'

  @source-properties @AER-1770
  Scenario: Test with properties forced without building influence
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 129918 and Y 419196
    And the source label is set to 'Farmer Pan Fluit'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'F 1.7' with bwl-code 'BWL 2010.13.V4' and an amount of 10000
    And the source sub item is saved
    And the source characteristics is toggled
    And the properties are toggled to forced
    When the source properties emission temperature is set to 15, emission height to 6, outflow diameter to 2, outflow velocity to HORIZONTAL with a speed of 1
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -results- is selected
    And the results table is set to show the area 'Loevestein, Pompveld & Kornsche Boezem'
    Then the result for Total Deposition in HabitatArea 'H91E0C' is '0,19'

  @source-properties @AER-1770
  Scenario: Test with properties forced with building influence
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 129918 and Y 419196
    And the source label is set to 'Farmer Pan Fluit'
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the farming emission is added a rav-code 'F 1.7' with bwl-code 'BWL 2010.13.V4' and an amount of 10000
    And the source sub item is saved
    And the source characteristics is toggled
    And the properties are toggled to forced
    When the source properties emission height is set to 6, outflow diameter to 2, outflow velocity to HORIZONTAL with a speed of 1
    And building influence is selected
    And the source heat content is set with building influence using lenght 12, width 3, height 4, orientation 100
    And the source is saved
    Then the calculation is started
    And the calculation ends
    And the menu item -results- is selected
    And the results table is set to show the area 'Loevestein, Pompveld & Kornsche Boezem'
    Then the result for Total Deposition in HabitatArea 'H91E0C' is '0,23'
