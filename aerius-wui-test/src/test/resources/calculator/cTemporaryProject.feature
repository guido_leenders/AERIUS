@composite @temporary @nightly
Feature: Calculation for temporary project

  Background: 
    Given AERIUS CALCULATOR is open

  @AER-1717 @AER-1732
  Scenario: Upload a GML with the option Temporary project, check the warning, continue to calculate without the option
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'AERIUS_TemporaryProjectOption.gml' with an old calculation option is imported
    Then the import dialog panel contains the message 'tijdelijk project'
    And the option to continue the import is selected
    # Calculate and check for errors
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # Check habitats for expected deposition values
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Naardermeer' 
    Then the results table for area 'Naardermeer' contains an exact match for values
      | H3150baz     | 14,13  |
      | H91D0        | 14,13  |
      | Lg05         | 14,13  |
      | H3140lv      |  9,22  |
      | H7140B       |  9,22  |
      | H7140A       |  3,81  |
      | H9999:94     |  2,99  |
      | ZGH3150baz   |  1,41  |
      | H6410        |  1,29  |
      | H3130        |  1,02  |
      | ZGH7140B     |  0,65  |
      | H4010B       |  0,26  |

  @AER-1713 @AER-1727 @AER-1717 @cal19ignore
  Scenario: Add source and calculate as temporary project
    Given the startup is set to input sources
    And the source location is set to a 'polygon' with coordinates 191847.46120001 569017.4203,192317.86120001 568674.7003,191854.18120001 568499.9803,191847.46120001 569017.4203
    And the source label is set to 'Festivalterrein'
    And the source sector is set to sectorgroup PLAN with no sector
    And the plan emission item is given name 'Flatlands', category '9' and amount 420
    And the source sub item is saved
    And the source is saved
    # Calculate and check for errors
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    And the notifications are all deleted
    # Check habitats for expected deposition values
    When the menu item -results- is selected
    And the sub menu item -results table- is selected
    And the results table is set to display as Total Deposition
    And the results table is set to show the area 'Alde Feanen'
    Then the result for Total Deposition in HabitatArea 'H7140B' is '0,99'
    And the result for Total Deposition in HabitatArea 'H6410' is '0,39'
    And the result for Total Deposition in HabitatArea 'H91D0' is '0,21'
    And the result for Total Deposition in HabitatArea 'H4010B' is '0,01'
    # Enable temporary project option
    When the taskbar options-panel is opened
    And the calculation project type is set to temporary
    And the calculation project duration is set to 4 years
    And the taskbar panel is closed
    # Recalculate and check for errors
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # Check habitats for expected deposition values
    When the menu item -results- is selected
    And the sub menu item -results table- is selected
    And the results table is set to display as Total Deposition
    And the results table is set to show the area 'Alde Feanen'
    Then the result for Total Deposition in HabitatArea 'H7140B' is '0,66'
    And the result for Total Deposition in HabitatArea 'H6410' is '0,26'
    And the result for Total Deposition in HabitatArea 'H91D0' is '0,14'
    And the notification panel contains no errors
