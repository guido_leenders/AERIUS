@single @layers @nightly
Feature: Open and modify layers in the layerpanel and click on the map

  Background:
    Given AERIUS CALCULATOR is open
    And the taskbar layer-panel is opened

  Scenario Outline: Select and modify maplayer in the layerpanel <maplayer>
    Given the layer with the name '<maplayer>' is present
    And the search for coordinates X <xcoordinate> and Y <ycoordinate> is started
    And the map is zoomed out 2 times
    And the layer with the name '<maplayer>' is toggled on or off
    And the layer with the name '<maplayer>' is toggled collapse
    And the layer with the name '<maplayer>' has its opacity set to 85
    And the map is clicked in the center
    And the taskbar info-panel is opened
    And the taskbar panel is closed
    And the notification panel contains no errors

    Examples: Maplayers to use
      | maplayer                       | xcoordinate | ycoordinate |
      | Binnenvaart                    | 67613       | 442963      |
      | BAG                            | 80340       | 454086      |
      | Scheepvaart netwerk            | 78566       | 458542      |
      | Zeescheepvaart netwerk         | 53671       | 463358      |
      | Natuurgebieden                 | 99353       | 496495      |
      | Natuurgebieden                 | 205659      | 552823      |
      | Stikstofgevoelige habitattypen | 186930      | 582370      |
      | Stikstofgevoelige habitattypen | 188329      | 488451      |
      | Stikstofgevoelige habitattypen | 28959       | 409129      |
      | Totale depositie               | 132407      | 430294      |
      | Luchtfoto (PDOK)               | 132407      | 430294      |
      | Geografische rekengrondslag    | 12468       | 412698      |
