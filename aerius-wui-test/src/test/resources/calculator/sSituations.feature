@single @situations @nightly
Feature: Creating, renaming, deleting situations

  Scenario: Create source in situation 1
    Given AERIUS CALCULATOR is open
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 222059 and Y 559090
    And the source label is set to 'Source 1'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 149, NOX 3000, PM10 0
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Source 1' with emission row label 'NH3' should be 149,0
    Then the source emission of source 'Source 1' with emission row label 'NOx' should be 3000,0

  @comparison
  Scenario: Creating and renaming situations
    When situation 1 is renamed to 'Old <#$#$#>'
    When situation 1 is renamed to 'Old Situation'
    When the alternate situation is created
    When situation 2 is renamed to 'New ():###'
    When situation 2 is renamed to 'New Situation'
    When the situations are switched
    When the situations are switched
    When situation 2 is deleted
    When the alternate situation is created
    When situation 2 is renamed to 'Old Situation'
    When situation 1 is renamed to 'AA'
    When situation 1 is deleted
    When the alternate situation is created
    When situation 2 is renamed to 'New B'
    When the situations are switched
    When the situations are switched
    When the comparison tab is selected
    And the notification panel contains no errors
