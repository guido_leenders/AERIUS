@composite @calcpoints @nightly @AER-1713 @AER-1737
Feature: Correct behaviour, calculation and export when calculationpoints are modified mutliple times

  Scenario: Set up a basic emission source and check if ui shows the correct values
    Given AERIUS CALCULATOR is open
    And the startup is set to input sources
    When the direct search for 'alphen aan den rijn' is started
    And the source location is set to a point with coordinates X 179058 and Y 332120
    And the source label is set to 'Bouwterrein 1'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 1399, NOX 12500, PM10 0
    And the source is saved
    Then the source emission of source 'Bouwterrein 1' with emission row label 'NH3' should be 1399,0
    And the source emission of source 'Bouwterrein 1' with emission row label 'NOx' should be 12500,0

  Scenario: Create, edit and delete calculation points
    Given the menu item -calculationpoints- is selected
    And the calculationpoint location is set to coordinates X 95892 and Y 456813
    And the calculationpoint label is set to 'CalcPoint AA'
    Then the calculationpoint is saved
    # Add another calculation point

    When a new calculation point 'CalcPoint AA1' is added at coordinates X 95892 Y 456813
    # Delete all calculation points

    Then all calculationpoints are deleted
    # Add and delete a calculation point

    When a new calculation point 'CalcPoint AA' is added at coordinates X 95892 Y 456813
    Then the calculationpoint with label 'CalcPoint AA' is deleted
    # Add and edit calculation point

    When a new calculation point 'CalcPoint AA' is added at coordinates X 95892 Y 456813
    When the calculationpoint with label 'CalcPoint AA' is edited
    And the calculationpoint label is set to 'CalcPoint A'
    Then the calculationpoint is saved
    # Add more calculation points

    When a new calculation point 'CalcPoint B' is added at coordinates X 109009 Y 456276
    And a new calculation point 'CalcPoint C' is added at coordinates X 118256 Y 448749
    And a new calculation point 'CalcPoint D' is added at coordinates X 116105 Y 438212
    # Delete a couple of points

    Then the calculationpoint with label 'CalcPoint B' is deleted
    And the calculationpoint with label 'CalcPoint C' is deleted
    # Copy calculation points

    When the calculation point 'CalcPoint D' is copied to 'CalcPoint B'
    And the calculation point 'CalcPoint D' is copied to 'CalcPoint C'
    # Add calculation point

    When a new calculation point 'CalcPoint E' is added at coordinates X 80247 Y 451813
    # Delete calculation point

    When the calculationpoint with label 'CalcPoint C' is deleted
    # Add calculation point

    When a new calculation point 'CalcPoint C' is added at coordinates X 118256 Y 448749

  Scenario: Edit calculation points
    #Edit calculation Point
    When the calculationpoint with label 'CalcPoint A' is selected
    And the selected calculationpoint is edited
    And the calculationpoint location is set to coordinates X 95992 and Y 456813
    And the calculationpoint label is set to 'CalcPoint AA'
    And the calculationpoint is saved
    #Edit calculation Point
    When the calculationpoint with label 'CalcPoint B' is selected
    And the selected calculationpoint is edited
    And the calculationpoint location is set to coordinates X 109109 and Y 456276
    And the calculationpoint label is set to 'CalcPoint BB'
    And the calculationpoint is saved
    #Edit calculation Point
    When the calculationpoint with label 'CalcPoint C' is selected
    And the selected calculationpoint is edited
    And the calculationpoint location is set to coordinates X 118356 and Y 448749
    And the calculationpoint label is set to 'CalcPoint CC'
    And the calculationpoint is saved
    #Edit calculation Point
    When the calculationpoint with label 'CalcPoint D' is selected
    And the selected calculationpoint is edited
    And the calculationpoint label is set to 'CalcPoint DD'
    And the calculationpoint is saved
    #Edit calculation Point
    When the calculationpoint with label 'CalcPoint E' is selected
    And the selected calculationpoint is edited
    And the calculationpoint label is set to 'CalcPoint EE'
    And the calculationpoint is saved
    #Delete calculation point
    When the calculationpoint with label 'CalcPoint AA' is deleted
    # Copy and rename calculation point

    When the calculationpoint with label 'CalcPoint EE' is selected
    And the selected calculationpoint is edited
    And the calculationpoint location is set to coordinates X 95892 and Y 456813
    And the calculationpoint label is set to 'CalcPoint AA'
    And the calculationpoint is saved
    When the calculationpoint with label 'CalcPoint AA' is deleted
    Then the notification panel contains no errors

  Scenario: Determine calculation points automatically and delete some
    When a new calculation point is added
    And the calculationpoints are automatically determined within a range of 15 km
    Then the notification panel contains no errors
#    And the calculationpoint with label 'Hamonterheide, Hageven, Buitenheide, Stamprooierbroek en Mariaho (9 km)' is deleted
#    And the calculationpoint with label 'Mangelbeek en heide- en vengebieden tussen Houthalen en Gruitrod (11 km)' is deleted
#    And the calculationpoint with label 'Itterbeek met Brand, Jagersborg en Schootsheide en Bergerven (9 km)' is deleted
#    And the calculationpoint with label 'Mechelse Heide en vallei van de Ziepbeek (4 km)' is deleted
#    And the calculationpoint with label 'De Mechelse Heide en de Vallei van de Ziepbeek (4 km)' is deleted
#    And the calculationpoint with label 'Geleenbeekdal (9 km)' is deleted
    And the calculationpoint with label 'CalcPoint CC' is deleted
    And the calculationpoint with label 'CalcPoint DD' is deleted

  @AER-1737
  Scenario: Perform calculation and check results
    When the taskbar options-panel is opened
    And the calculation scope is set to Calculation Points
    And the taskbar panel is closed
    And the taskbar year is set to 2025
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # Check deposition on calculation points
    When the menu item -results- is selected
    And the sub menu item -results table- is selected
    Then the result for calculationpoint with label 'CalcPoint BB' is '0,00'
    And the result for calculationpoint with label 'Bunder- en Elslooërbos (4 km)' is '0,21'
    And the result for calculationpoint with label 'Geuldal (11 km)' is '0,08'
    And the result for calculationpoint with label 'Bemelerberg & Schiepersberg (14 km)' is '0,06'
    And the result for calculationpoint with label 'Grensmaas' is '4,80'

  @AER-1727
  Scenario: Export the situation
    When an export to GML with sources only is started
    Then the notification panel contains no errors
#    And an export to PAA is started, with coorporation 'Food Inc.', project 'New Products', location 'Kleine Staat 1, 6211 ED, Maastricht', description 'Do something, then something else ... i dont know'
#    Then the notification panel contains no errors
    And an export to PAA OWN USE is started, with coorporation 'Food Inc. Own Use', project 'New Products Own Use', location 'Kleine Staat 19, 6211 ED, Maastricht', description 'Do do do do, da, da, da'
    Then the notification panel contains no errors
    And the notifications are all deleted

  @comparison @AER-1732
  Scenario: Create alternate situation for comparison, calculate and check results
    Given the menu item -sources- is selected
    When the alternate situation is created
    And situation 2 is selected
    And a new source is added
    And the source location is set to a point with coordinates X 179058 and Y 334120
    And the source label is set to 'New'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 95000, NOX 1000, PM10 0
    And the source is saved
    And the calculation is started
    And the calculation ends
    # Check
    When the menu item -results- is selected
    And the sub menu item -results table- is selected
    And the comparison tab is selected
    And the situations are switched
    And the situations are switched
    Then the result for calculationpoint with label 'CalcPoint BB' is '+0,04'
    And the result for calculationpoint with label 'Bunder- en Elslooërbos (4 km)' is '+6,40'
    And the result for calculationpoint with label 'Geuldal (11 km)' is '+2,69'
    And the result for calculationpoint with label 'Bemelerberg & Schiepersberg (14 km)' is '+2,15'
    And the result for calculationpoint with label 'Grensmaas' is '+12,05'
    And the notification panel contains no errors
