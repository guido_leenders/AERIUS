@nightly @foreignareas
Feature: Tests surrounding foreign nature areas and calculation points

  Background:
    Given AERIUS CALCULATOR is open

  @AER-1769
  Scenario: Only Dutch Nature areas should have a calculation result
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 212444 and Y 376228
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 1000, NOX 1000, PM10 0
    And the source is saved
    Then the calculation is started
    Then the calculation ends
    And the notification panel contains no errors
    And the results panel only contains '18' label IDs
    And the results table is set to show the area 'Maasduinen'
    Then the result for Total Deposition in HabitatArea 'H91E0C' is '0,14'

  @AER-1769
  Scenario: Placing automatic calculation points should also place points in foreign areas
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 212444 and Y 376228
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 1000, NOX 1000, PM10 0
    And the source is saved
    Given the menu item -calculationpoints- is selected
    And the calculationpoints are automatically determined within a range of 6 km
    When the taskbar options-panel is opened
    And the calculation scope is set to Calculation Points
    And the taskbar panel is closed
    Then the calculation is started
    Then the calculation ends
    And the notification panel contains no errors
    And the results panel only contains '3' label IDs
    And the result for calculationpoint with label 'Hangmoor Damerbruch (4 km)' is '0,36'
