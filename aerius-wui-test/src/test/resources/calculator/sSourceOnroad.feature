@single @road @onroad @nightly @opsfixed
Feature: Add, modify and calculate sources of category ONROAD

  Scenario: Add source and check validations
    Given AERIUS CALCULATOR is open
    When the search for 'weer' is started
    And the search suggestion 'Weerribben' is selected
    ##
    # Add source
    ##
    And the taskbar substance is set to 'NOx'
    When the startup is set to input sources
    And the source location is set to a 'line' with coordinates 196953 532378 197880 531195
    And the source label is set to 'Onroad 1'
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3112
    # Add item / check validation
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto LPG - Euro 3' and the vehicle amount of 0
    And the source sub item is saved
    Then a validation message is showing with the text 'Aantal voertuigen moet groter dan 0 zijn'
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto LPG - Euro 3' and the vehicle amount of 1000
    And the source sub item is saved    
    # edit subsource
    And the source sub item with label 'Bestelauto LPG - Euro 3' is selected to edit
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto LPG - Euro 3' and the vehicle amount of 100
    And the source sub item is saved
    And the source sub item with label 'Bestelauto LPG - Euro 3' emission should be '16,4'
    # Add items
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Trekker' and the vehicle amount of 100
    And the source sub item is saved
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Vrachtauto diesel 3,5-10 ton GVW - Euro 3' and the vehicle amount of 100
    And the source sub item is saved
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Trekker diesel zwaar' and the vehicle amount of 100
    And the source sub item is saved
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Vrachtauto diesel 10-20 ton GVW - Euro 3' and the vehicle amount of 100
    And the source sub item is saved
    # Edit items
    And the source sub item with label 'Trekker diesel zwaar (gemiddeld 43 ton GVW) - Euro 3' is selected to edit
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto benzine - Euro 6' and the vehicle amount of 333
    And the source sub item is saved
    And the source sub item with label 'Bestelauto LPG - Euro 3' is selected to edit
    When the road emission is added a new EUROCLASS specification with category '43 ton gvw)- euro 5' and the vehicle amount of 111
    And the source sub item is saved
    And the source sub item with label 'Vrachtauto diesel 10-20 ton GVW - Euro 3' is selected to edit
    When the road emission is added a new EUROCLASS specification with category '43 ton gvw)- euro 6' and the vehicle amount of 666
    And the source sub item is saved
    # Delete items
    When the source sub item with label 'Bestelauto benzine - Euro 6' is selected and deleted
    When the source sub item with label 'Trekker diesel zwaar (gemiddeld 43 ton GVW) - Euro 5' is selected and deleted
    # Add item
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto diesel < 2,0 ton GVW - Euro 3' and the vehicle amount of 5
    And the source sub item is saved
    # Save
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Onroad 1' with emission row label 'Trekker diesel licht (gemiddeld 19 ton GVW) - Euro 3' should be 522,3
    Then the source emission of source 'Onroad 1' with emission row label 'Vrachtauto diesel 3,5-10 ton GVW - Euro 3' should be 216,0
    Then the source emission of source 'Onroad 1' with emission row label 'Trekker diesel zwaar (gemiddeld 43 ton GVW) - Euro 6' should be 713,5
    Then the source emission of source 'Onroad 1' with emission row label 'Bestelauto diesel < 2,0 ton GVW - Euro 3' should be 2,0

  Scenario: Copy and delete sources
    ##
    # Copy sources
    ##
    When the source with label 'Onroad 1' is selected
    And the selected source is copied
    And the source label is set to 'Onroad 1a'
    And the source is saved
    When the source with label 'Onroad 1' is selected
    And the selected source is copied
    And the source label is set to 'Onroad 1b'
    And the source is saved
    ##
    # Delete source
    ##
    When the source with label 'Onroad 1a' is selected
    And the selected source is deleted
    ##
    # Edit source
    ##
    When the source with label 'Onroad 1b' is selected
    And the selected source is edited
    And the source sub item with label 'Vrachtauto diesel 3,5-10 ton GVW - Euro 3' is selected and deleted
    And the source sub item with label 'Trekker diesel zwaar (gemiddeld 43 ton GVW) - Euro 6' is selected and deleted
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Onroad 1b' with emission row label 'Trekker diesel licht (gemiddeld 19 ton GVW) - Euro 3' should be 522,3
    Then the source emission of source 'Onroad 1b' with emission row label 'Bestelauto diesel < 2,0 ton GVW - Euro 3' should be 2,0

  Scenario: Add point source onroad
    # Add source
    When a new source is added
    And the source location is set to a 'LINE' with coordinates 193448 532571, 193443 532594
    And the source label is set to 'Onroad 2'
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3112
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto LPG - Euro 3' and the vehicle amount of 100
    And the source sub item is saved
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Trekker' and the vehicle amount of 100
    And the source sub item is saved
    And the source is added a new sub item
    When the road emission is added a new EUROCLASS specification with category 'Bestelauto LPG - Euro 3' and the vehicle amount of 5
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Onroad 2' with emission row label 'Bestelauto LPG - Euro 3' should be 0,0
    Then the source emission of source 'Onroad 2' with emission row label 'Trekker diesel licht (gemiddeld 19 ton GVW) - Euro 3' should be 8,2
    Then the source emission of source 'Onroad 2' with emission row label 'Bestelauto LPG - Euro 3' should be 0,0

  Scenario: Add point source onroad
    # Add source
    When a new source is added
    And the taskbar year is set to 2025
    And the source location is set to a 'LINE' with coordinates 193448 532571, 193443 532594
    And the source label is set to 'Onroad 3'
    And the source sector is set to sectorgroup ROAD_TRANSPORTATION with sector 3111
    And the road emission is added a new STANDARD specification with category 'LIGHT_TRAFFIC' and the vehicle amount of 10000 and trafficjam 25
    And the source sub item is saved
    And a validation message is showing with the text 'De maximum snelheid moet worden ingevuld'
    And the road emission max speed is set to 100
    And the source sub item is saved
    #
    And the source is added a new sub item
    And the road emission is added a new STANDARD specification with category 'NORMAL_FREIGHT' and the vehicle amount of 10000 and trafficjam 0
    And the source sub item is saved
    #
    And the source is added a new sub item
    And the road emission is added a new STANDARD specification with category 'LIGHT_TRAFFIC' and the vehicle amount of 10000 and trafficjam 33
    And the source sub item is saved
    # Delete source
    And the source sub item with label 'Licht verkeer' is selected and deleted
    #
    And the source is added a new sub item
    And the road emission is added a new STANDARD specification with category 'LIGHT_TRAFFIC' and the vehicle amount of 10000 and trafficjam 33
    And the source sub item is saved
    #
    And the source sub item with label 'Licht verkeer' is selected to edit
    And the road emission is added a new STANDARD specification with category 'AUTO_BUS' and the vehicle amount of 1000 and trafficjam 10
    And the source sub item is saved
    #
    And the source sub item with label 'Busverkeer' is selected to copy
    And the source sub item is saved
    #
    And the source is saved  
    #
    When the source with label 'Onroad 3' is selected
    And the selected source is copied
    And the source label is set to 'Onroad 4'
    And the source is saved
    #
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    #
    Then the source emission of source 'Onroad 3' with emission row label 'Licht verkeer' should be 18,2
    And the source emission of source 'Onroad 3' with emission row label 'Middelzwaar vrachtverkeer' should be 110,6
    And the source emission of source 'Onroad 3' with emission row label 'Busverkeer' should be 14,2
    #
    And the source emission of source 'Onroad 4' with emission row label 'Licht verkeer' should be 18,2
    And the source emission of source 'Onroad 4' with emission row label 'Middelzwaar vrachtverkeer' should be 110,6
    And the source emission of source 'Onroad 4' with emission row label 'Busverkeer' should be 14,2


#  Scenario: Export and Calculation of Onroad sector
#    # Export
#    Given an export to PAA is started, with coorporation 'RoadCompany', project 'Roadkill - Onroad', location 'Highway, 7777VW, Lowtown', description 'Speeding kills rodents and birds'
#    Then the notification panel contains no errors
#    # Calculate
#    When the taskbar options-panel is opend
#    And the calculation scope is set to Nature Areas
#    And the calculation range is set to 3 km
#    And the taskbar panel is closed
#    And the calculation is started
#    And wait for 3 seconds
#    And the calculation ends
#    Then the notification panel contains no errors
#
#  Scenario: Check switching results
#    When the menu item -results- is selected
#    And the sub menu item -results filter- is selected
#    And the taskbar substance is set to 'NH3'
#    And the taskbar substance is set to 'NOx'
##    And the menu themeswitch is set to 'Luchtkwaliteit'
##    And the taskbar substance is set to 'PM10'
##    And the taskbar resulttype is set to DAYS
##    And the menu themeswitch is set to 'Natura 2000'
#    And the taskbar substance is set to 'NOx+NH3'
#    Then the notification panel contains no errors
