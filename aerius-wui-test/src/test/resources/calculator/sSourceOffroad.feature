@single @road @offroad @nightly
Feature: Add, modify and calculate sources of category OFFROAD

  @AER-1727
  Scenario: Category OFFROAD with STAGE classes
    Given AERIUS CALCULATOR is open
    When the search for 'drents' is started
    And the search suggestion 'Drents-Friese Wold & Leggelderveld' is selected
    ##
    # Add source
    ##
    When the startup is set to input sources
    And the source location is set to a point with coordinates X 223062 and Y 546417
    And the source label is set to 'Offroad Zero'
    And the source sector is set to sectorgroup MOBILE_EQUIPMENT with sector 3220
    When the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 0
    # Switching focus and saving with fuel amount = 0 should show validation
    Then a validation message is showing with the text 'Brandstofverbruik moet groter dan 0 zijn'
    When the source sub item is saved
    Then a validation message is showing with the text 'Brandstofverbruik moet groter dan 0 zijn'
    # Continue with valid input
    When the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 30
    And the source sub item is saved
    And the taskbar substance is set to 'NOx'
    And the source sub item with label 'Mobile 1' emission should be '1,7'
    # Add item
    When the source is added a new sub item
    And the offroad emission item is set to CUSTOM class with name 'Mobile Custom', height 1, heat 10 and NOX 1300
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Offroad Zero' with emission row label 'Mobile 1' should be 1,7
    Then the source emission of source 'Offroad Zero' with emission row label 'Mobile Custom' should be 1300,0
    ##
    # Add source
    ##
    When a new source is added
    And the source location is set to a point with coordinates X 225038 and Y 545173
    And the source label is set to 'Offroad One'
    And the source sector is set to sectorgroup MOBILE_EQUIPMENT with sector 3210
    And the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 30
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Offroad One' with emission row label 'Mobile 1' should be 1,7
    # Edit source / subsource
    When the source with label 'Offroad One' is selected
    And the selected source is edited
    And the source sub item with label 'Mobile 1' is selected to edit
    And the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 100
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Offroad One' with emission row label 'Mobile 1' should be 5,5
    # Edit items
    When the source with label 'Offroad One' is selected
    And the selected source is edited
    And the source is added a new sub item
    And the offroad emission item is set to STAGE class with name 'Mobile 4', category 4 and fuel amount 130
    And the source sub item is saved
    And the source is added a new sub item
    And the offroad emission item is set to STAGE class with name 'Mobile 18', category 18 and fuel amount 230
    And the source sub item is saved
    # Delete items
    And the source sub item with label 'Mobile 1' is selected and deleted
    And the source sub item with label 'Mobile 18' is selected and deleted
    And the source sub item with label 'Mobile 4' is selected and deleted
    # Add new items
    And the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 30
    And the source sub item is saved
    And the source is added a new sub item
    And the offroad emission item is set to STAGE class with name 'Mobile 4', category 4 and fuel amount 130
    And the source sub item is saved
    And the source is added a new sub item
    And the offroad emission item is set to STAGE class with name 'Mobile 18', category 18 and fuel amount 2300
    And the source sub item is saved
    And the source is added a new sub item
    And the offroad emission item is set to STAGE class with name 'Mobile 9', category 9 and fuel amount 1000
    And the source sub item is saved
    And the source sub item with label 'Mobile 4' is selected and deleted
    And the source is added a new sub item
    And the offroad emission item is set to STAGE class with name 'Mobile 4', category 4 and fuel amount 150
    And the source sub item is saved
    And the source is saved
    Then the source emission of source 'Offroad One' with emission row label 'Mobile 1' should be 1,7
    Then the source emission of source 'Offroad One' with emission row label 'Mobile 18' should be 2,8
    Then the source emission of source 'Offroad One' with emission row label 'Mobile 9' should be 17,8
    Then the source emission of source 'Offroad One' with emission row label 'Mobile 4' should be 3,8
    #
    When the source with label 'Offroad One' is selected
    And the selected source is copied
    And the source label is set to 'Offroad Two'
    And the source sub item with label 'Mobile 18' is selected and deleted
    And the source sub item with label 'Mobile 9' is selected and deleted
    And the source is saved
    Then the source emission of source 'Offroad Two' with emission row label 'Mobile 1' should be 1,7
    Then the source emission of source 'Offroad Two' with emission row label 'Mobile 4' should be 3,8
    # Export
    Given an export to PAA OWN USE is started, with coorporation 'RoadCompany', project 'Roadkill - Offroad', location 'Wegisweg, 4444 AB, Cliffbay', description 'Speeding kills rodents and birds'
    Then the notification panel contains no errors
    # Calculate
    # removed nature areas option (temporarily?)
#    When the taskbar options-panel is opend
#    And the calculation scope is set to Nature Areas
#    And the calculation range is set to 3 km
#    And the taskbar panel is closed
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors

  @offroad_dev
  Scenario: Category OFFROAD with DEVIATING classes
    Given AERIUS CALCULATOR is open
    When the search for 'oeffel' is started
    And the search suggestion 'Oeffelter Meent' is selected
    ##
    # Add source
    ##
    When the startup is set to input sources
    And the source location is set to a point with coordinates X 192571 and Y 413684
    And the source label is set to 'Offroad Deviating'
    And the source sector is set to sectorgroup MOBILE_EQUIPMENT with sector 3220
    And the offroad emission item is set to CUSTOM class with name 'Mobile Devi 1', height 3, heat 0 and NOX 0
    And the offroad emission item calculator is opened
    And the offroad emission item calculator is set for LOAD-USAGE with machinery 'bulldozers 60 kW, bouwjaar vanaf 2003', fueltype 'Diesel', power 100, load 60, usage 50, emissionfactor 7,9
    And the offroad emission item calculator is closed, saving the calculated emission
    And the offroad emission item calculator saved value should be '23,7'
    And the source sub item is saved
    #
    And the source is added a new sub item
    And the offroad emission item is set to CUSTOM class with name 'Mobile Devi 2', height 3, heat 0 and NOX 0
    And the offroad emission item calculator is opened
    And the offroad emission item calculator is set for FUEL-USAGE with machinery 'laadschoppen 200 kW, bouwjaar vanaf 2015', fueltype 'Diesel', fuelusage 1000, efficiency 200, emissionfactor 5
    And the offroad emission item calculator is closed, saving the calculated emission
    And the offroad emission item calculator saved value should be '21'
    And the source sub item is saved
    And the source is saved
    #
    And an export to GML with sources only is started
    Then the notification panel contains no errors
    #
    And the calculation is started
    And the notification panel contains the message 'De rekenstof is automatisch aangepast naar NOx zodat de rekeninstelling overeenkomt met de ingevoerde bronnen.'
    And the calculation ends
    Then the notification panel contains no errors
