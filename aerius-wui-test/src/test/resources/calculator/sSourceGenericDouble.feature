@single @nightly @generic @genericdouble
Feature: Sectors Test at different locations - diff number of receptors - and different emission

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario Outline: Add two generic sources and start a calculation <sectorgroup>
    When the startup is set to input sources
    And the source location is set to a point with coordinates X <xcoordinate_1> and Y <ycoordinate_1>
    And the source label is set to 'Source A <sectornumber>'
    And the source sector is set to sectorgroup <sectorgroup> with sector <sectornumber>
    And the source emission is set to NH3 <nh3>, NOX <nox>, PM10 <pm10>
    And the source is saved
    And a new source is added
    And the source location is set to a point with coordinates X <xcoordinate_2> and Y <ycoordinate_2>
    And the source label is set to 'Source B <sectornumber>'
    And the source sector is set to sectorgroup <sectorgroup> with sector <sectornumber>
    And the source emission is set to NH3 <nh3>, NOX <nox>, PM10 <pm10>
    And the source is saved
    And the namelabel switch for sources is toggled
    And the menu themeswitch is set to '<theme>'
    # removed nature areas option  (temporarily?)
#    And the taskbar options-panel is opend
#    And the calculation scope is set to Nature Areas
#    And the calculation range is set to <calc_range> km
#    And the taskbar panel is closed
    And the calculation is started
    Then the calculation ends
    And the notification panel contains no errors

    Examples: 
      | xcoordinate_1 | ycoordinate_1 | xcoordinate_2 | ycoordinate_2 | sectorgroup         | sectornumber | nh3  | nox | pm10 | calc_range | theme          |
      | 80290         | 449287        | 80590         | 449587        | ENERGY              | -            | 300  | 250 | 0    | 4          | Natura 2000    |
      | 176532        | 394452        | 176132        | 394952        | ENERGY              | -            | 300  | 250 | 100  | 4          | Natura 2000    |
      | 176532        | 394452        | 176132        | 394952        | ENERGY              | -            | 0    | 250 | 100  | 4          | Natura 2000    |
      | 271794        | 586106        | 271294        | 586906        | LIVE_AND_WORK       | 8200         | 250  | 600 | 100  | 3          | Natura 2000    |
      | 271794        | 586106        | 271294        | 586906        | LIVE_AND_WORK       | 8210         | 250  | 600 | 100  | 3          | Natura 2000    |
      | 271794        | 586106        | 271294        | 586906        | LIVE_AND_WORK       | 8640         | 250  | 600 | 100  | 3          | Natura 2000    |
      | 188574        | 452082        | 189574        | 451082        | INDUSTRY            | 1050         | 0    | 600 | 100  | 3          | Natura 2000    |
      | 188574        | 452082        | 189574        | 451082        | INDUSTRY            | 1100         | 250  | 0   | 100  | 2          | Natura 2000    |
      | 188574        | 452082        | 189574        | 451082        | INDUSTRY            | 1300         | 250  | 600 | 0    | 2          | Natura 2000    |
      | 188574        | 452082        | 189574        | 451082        | INDUSTRY            | 1400         | 250  | 600 | 100  | 1          | Natura 2000    |
      | 188574        | 452082        | 190939        | 465415        | INDUSTRY            | 1500         | 1250 | 600 | 100  | 1          | Natura 2000    |
      | 188574        | 452082        | 134169        | 411225        | INDUSTRY            | 1700         | 250  | 600 | 100  | 1          | Natura 2000    |
      | 188574        | 452082        | 189574        | 451082        | INDUSTRY            | 1800         | 0    | 600 | 100  | 3          | Natura 2000    |
      | 91290         | 449287        | 91690         | 449487        | RAIL_TRANSPORTATION | 3710         | 250  | 600 | 100  | 1          | Natura 2000    |
      | 91290         | 449287        | 91690         | 449487        | RAIL_TRANSPORTATION | 3720         | 250  | 600 | 100  | 1          | Natura 2000    |
      | 108794        | 515519        | 108294        | 515219        | OTHER               | -            | 300  | 250 | 100  | 4          | Natura 2000    |

  Scenario Outline: Add two generic sources, different sectorgroups, and start a calculation <sectorgroup1> and <sectorgroup2>
    When the startup is set to input sources
    And the source location is set to a point with coordinates X <xcoordinate_1> and Y <ycoordinate_1>
    And the source label is set to 'Source A <sectornumber1>'
    And the source sector is set to sectorgroup <sectorgroup1> with sector <sectornumber1>
    And the source emission is set to NH3 <nh3>, NOX <nox>, PM10 <pm10>
    And the source is saved
    And a new source is added
    And the source location is set to a point with coordinates X <xcoordinate_2> and Y <ycoordinate_2>
    And the source label is set to 'Source B <sectornumber2>'
    And the source sector is set to sectorgroup <sectorgroup2> with sector <sectornumber2>
    And the source emission is set to NH3 <nh3>, NOX <nox>, PM10 <pm10>
    And the source is saved
    And the namelabel switch for sources is toggled
    And the menu themeswitch is set to '<theme>'
    And the calculation is started
    Then the calculation ends
    And the notification panel contains no errors

   Examples: 
      | xcoordinate_1 | ycoordinate_1 | xcoordinate_2 | ycoordinate_2 | sectorgroup1        | sectorgroup2 | sectornumber1  | sectornumber2 | nh3  | nox | pm10 | calc_range | theme          |
      | 80290         | 449287        | 80590         | 449587        | LIVE_AND_WORK       | INDUSTRY     | 8200           | 1050          | 300  | 250 | 0    | 4          | Natura 2000    |
