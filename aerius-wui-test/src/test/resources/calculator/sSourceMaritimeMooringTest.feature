@nautical @nautical-maritime @single
Feature: Maritime Mooring Test

  Background: 
    Given AERIUS CALCULATOR is open

  @maritime-mooring
  Scenario: Test Maritime Mooring
    When the startup is set to input sources
    And the source location is set to a 'line' with coordinates 62561 439180 64523 439879
    And the source label is set to 'Mask B.V.'
    And the source sector is set to sectorgroup SHIPPING with sector 7510
    And the maritime mooring ship is a 'ship 1' of category 10 with 1000 ships staying 24 hours
    And the maritime mooring inland route is drawn with coordinates 65000 440000 65800 443800
    # test placeholder correct
    Then the maritime mooring sea route at row 0 must be empty
    # test if maritime routes are optional by saving before entering sea routes
    When the source sub item is saved
    # open to continue
    And the source sub item with label 'ship 1' is selected
    And the maritime mooring sea route at row 0 is drawn with coordinates 35800 433800
    # test diff correctly calculated
    Then the maritime mooring sea route at row 0 must be 2000 ships
    # test new row added when route and value added
    And the maritime mooring sea route row 1 must be 'visible'
    When the maritime mooring sea route at row 0 is set to 500 ships
    # test value not updated when ships set to lower than required value
    Then the maritime mooring sea route at row 1 must be empty
    When the maritime mooring sea route at row 1 is drawn with coordinates 69000 460000
    # test movements auto filled when route selected
    Then the maritime mooring sea route at row 1 must be 1500 ships
    # test new empty row added when auto fill movements
    And the maritime mooring sea route row 2 must be 'visible'
    When the maritime mooring sea route at row 0 is set to 100 ships
    # test if movements set to value less then matching then new row added
    Then the maritime mooring sea route row 2 must be 'visible'
    And the maritime mooring sea route at row 2 must be empty
    When the source sub item is saved
    # test save not allowed when no matching movements and ships
    Then an error message for not matching maritime mooring visits must be given
    # error message given
    When the maritime mooring sea route at row 0 is set to 200 ships
    # test no new row added when already new row present
    Then the maritime mooring sea route row 3 must be 'invisible'
    When the maritime mooring sea route at row 1 is set to 1500 ships
    # test no new row added when already new row present
    Then the maritime mooring sea route row 3 must be 'invisible'
    When the source sub item is saved
    # test save not allowed when no matching movements and ships
    Then an error message for not matching maritime mooring visits must be given
    When the maritime mooring sea route at row 0 is set to 500 ships
    Then the maritime mooring sea route row 3 must be 'invisible'
    When the source sub item is saved
    And the source sub item with label 'ship 1' is selected
    Then there must be a maritime mooring ship 'ship 1' of category 10 with 1000 ships staying 24 hours
    #   And inland route
    And the maritime mooring sea route at 0 must be 'A'
    And the maritime mooring sea route at row 0 must be 500 ships
    And the maritime mooring sea route at 1 must be 'B'
    And the maritime mooring sea route at row 1 must be 1500 ships
    And the maritime mooring sea route at row 2 must be empty
    And the maritime mooring sea route row 3 must be 'invisible'

  @maritime-mooring
  Scenario: Calculate maritime mooring source
    When the search for 'Solleveld' is started
    And the search suggestion 'Solleveld & Kapittelduinen' is selected
    And the startup is set to input sources
    And the source location is set to a 'line' with coordinates 58545 447855 58438 447613
    And the source label is set to 'MariMooring'
    And the source sector is set to sectorgroup SHIPPING with sector 7510
    And the maritime mooring ship is a 'TowingBoat' of category 10 with 100 ships staying 12 hours
    And the maritime mooring inland route is drawn with coordinates 59713 447683
    And the maritime mooring sea route at row 0 is drawn with coordinates 60483 447300
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NOx'
    Then the total emission of the source with label 'MariMooring' should be 711,5 kg/j
    And the map is zoomed out 3 times
    And the calculation is started
    And the calculation ends
    And the notification panel contains no errors

