@composite @shipping @nightly
Feature: Shipping Emission

  Background:
    Given AERIUS CALCULATOR is open
    Given the startup is set to import a file

  # TODO: Obsolete step priority project for item @AER-1718.
  @maritime-mooring-import-v05 @AER-1713 @AER-1718
  Scenario: Import an ancient maritime mooring source from an IMAER v0.5 GML, check values and calculate to check for errors
    When the GML file 'maritime_mooring_v0.5.gml' is imported
    And the notification panel contains no errors
    Then the source emission of source 'Aanlegplaatsbedrijf' with emission row label 'Tankers' should be 22438,7
    # limit calculation time by applying priority project radius
    When the taskbar options-panel is opened
    Then the calculation option priority project radius is enabled
    And the calculation option priority project type is set to 'Prioritair project Hoofdvaarwegennet'
    And the taskbar panel is closed
    # start calculation
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # check calculation results
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Solleveld & Kapittelduinen'
    Then the result for Total Deposition in HabitatArea 'H2160' is '0,67'
    And the result for Total Deposition in HabitatArea 'Lg12' is '0,56'
    And the result for Total Deposition in HabitatArea 'H2120' is '0,32'

  @inland-mooring-import-v05 @AER-1759
  Scenario: Import an ancient inland mooring source from a v0.5 IMAER GML, check values and calculate to check for errors
    When the GML file 'inland_mooring_v0.5.gml' is imported
    And the notification panel contains no errors
    Then the source emission of source 'Aanlegplaats industrie' with emission row label 'Zandschepen' should be 727,8
    And the calculation is started
    And the calculation ends
    And the notification panel contains no errors

  @inland-route-import-v05 @AER-1759
  Scenario: Test import shipping inland route
    When the GML file 'inland_route_v0.5.gml' is imported
    And the notification panel contains no errors
    Then the source emission of source 'Vaarroute' with emission row label 'Zandschepen' should be 17782,1
    # start calculation
    When the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    # check calculation result
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Biesbosch'
    Then the result for Total Deposition in HabitatArea 'H6510B' is '0,08'

  @inland-mooring-import-v10
  Scenario: Test shipping inland mooring
    When the GML file 'inland_mooring_v1.0.gml' is imported
    And the notification panel contains no errors
    And the source with label 'Bron 1' is selected
    And the selected source is edited
    And the source sub item with label 'Binnenvaartschepen' is selected to edit
    #
    Then the inland mooring load at 'ARRIVE' row 0 must be 50
    And the inland mooring load at 'ARRIVE' row 0 is set to 80
    And the inland mooring load at 'ARRIVE' row 0 must be 80
    #
    And the source sub item is saved
    And the source is saved

  @inland-mooring-import-v11 @AER-1727 @AER-1718 @AER-1759
  Scenario: Import an inland mooring source from a v1.1 IMAER GML, check values, calculate results and export permit application 
    When the GML file 'AERIUS_inland_mooring.gml' is imported
    And the notification panel contains the message 'Uw bestand is geïmporteerd.Er is 1 bron toegevoegd.'
    And the notification panel contains no errors
    Then the source emission of source 'Nautical_Heavy' with emission row label 'BulkM9' should be 895,6
    # copy and edit the copied source sub item
    When the source with label 'Nautical_Heavy' is edited
    And the source sub item with label 'BulkM9' is selected to copy
    And the inland mooring ship is a 'EditM7' of category 7610 staying 1 hours
    And the inland route ship type is set to 'M7'
    And the inland mooring ship movements at 'ARRIVE' row 0 is set to 356
    And the inland mooring load at 'ARRIVE' row 0 is set to 99
    Then the source sub item is saved
    And there are no validation errors
    # check if the copied source values have been set correctly
    When the source sub item with label 'EditM7' is selected to edit
    Then the inland route ship type must be 'M7'
    And the inland mooring ship movements at 'ARRIVE' row 0 must be 356
    And the inland mooring load at 'ARRIVE' row 0 must be 99
    And the inland waterway classification must be 'CEMT_Vb'
    And the source sub item is saved
    # edit the source and sub item to check values 
    When the source sub item with label 'BulkM9' is selected to edit
    Then the inland mooring ship is a 'BulkM9' of category M9 staying 3 hours
    And the inland route ship type must be 'M9'
    And the inland mooring ship movements at 'ARRIVE' row 0 must be 304
    And the inland mooring load at 'ARRIVE' row 0 must be 90
    And the inland waterway classification must be 'CEMT_Vb'
    # try mismatching waterway class and check notifications 
    When the inland waterway classification is set to 'CEMT_I'
    And the source sub item is saved
    Then a validation message is showing with the text 'Vaartuig "M9" is niet toegestaan op vaarwater "CEMT_I"'
    # change all inputs to valid values, save and check for errors
    When the inland waterway classification is set to 'CEMT_Va'
    And the inland route ship type is set to 'M8'
    And the inland mooring ship movements at 'ARRIVE' row 0 is set to 246
    And the inland mooring load at 'ARRIVE' row 0 is set to 95
    And the source sub item is saved
    And the source is saved
    Then there are no validation errors
    # limit the calculation range by enabling the priority project calculation option
    When the taskbar options-panel is opened
    And the calculation option priority project radius is enabled
    And the calculation option priority project type is set to 'Prioritair project Hoofdvaarwegennet' 
    Then the taskbar panel is closed
    # start a calculation, export results and check for errors once more
#    When the calculation is started
#    And the calculation ends
#    And the notification panel contains no errors
    Then an export to PAA OWN USE is started, with coorporation 'Inland Mooring (7610-BulkM9)', project 'Docker', location 'Havenbedrijf, 1234AB, Rotterdam', description 'Dock and cover...'
    And the notification panel contains no errors

  @inland-mooring-validator @rainy-day
  Scenario: Import an inland mooring source and check validation errors on all compulsory input fields when left empty  
    When the GML file 'AERIUS_inland_mooring.gml' is imported
    Then the notification panel contains no errors
    # open source, add a new sub source and trigger validation errors for each input field
    When the source with label 'Nautical_Heavy' is edited
    And the source is added a new sub item
    And the source sub item is saved
    Then a validation message is showing with the text 'Scheepvaart categorie '' niet geldig'
    # set a correct value for ship type and check next field 
    When the inland route ship type is set to 'M8'
    And the source sub item is saved
    Then a validation message is showing with the text 'Verblijftijd moet groter dan 0 zijn'
    # set correct value for residence time and check next field
    When the inland mooring residence time is set to 3
    And the source sub item is saved
    Then a validation message is showing with the text 'Naam veld mag niet leeg zijn'
    # set correct value for ship name and check next field
    When the inland mooring ship is a 'Baggerschuit' of category M8 staying 3 hours
    And the source sub item is saved
    Then a validation message is showing with the text 'Geen route aangegeven'
    # set correct value for ship route and check next field
    When the inland mooring route at 'ARRIVE' row 0 is set to 'A' 
    And the source sub item is saved
    Then a validation message is showing with the text 'Vaarbewegingen moet groter dan 0 zijn als een route is ingevuld'
    # set correct value for ship movements and check next field
    When the inland mooring ship movements at 'ARRIVE' row 0 is set to 150
    And the source sub item is saved
    Then there are no validation errors
    When the inland waterway classification is set to 'CEMT_Va'
    And the source is saved
    Then there are no validation errors
    And the notification panel contains no errors
