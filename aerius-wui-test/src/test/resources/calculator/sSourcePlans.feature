@single @plan @nightly @AER-1727
Feature: Add, modify and calculate sources of category PLAN

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario: Add, modify items sources of category PLAN
    And the taskbar substance is set to 'NOx'
    When the search for 'meinweg' is started
    And the search suggestion 'Meinweg' is selected
    # Add source
    When the startup is set to input sources
#    And the source location is set to a 'polygon' with coordinates 204860 354254 204860 354066 205015 354160 205068 354375 204860 354254
    And the source location is set to a 'polygon' with coordinates 205154 354057 205176 354014 205254 354089 205231 354126 205154 354058
    And the source label is set to 'Area 1'
    And the source sector is set to sectorgroup PLAN with no sector
    # Check validation
    And the source sub item is saved
    Then a validation message is showing with the text 'Naam veld mag niet leeg zijn'
    # Enter data
    And the plan emission item is given name 'Plan A', category '1' and amount 54
    And the source sub item is saved
    And the source sub item with label 'Plan A' emission should be '59,9'
    # Copy item
    And the source sub item with label 'Plan A' is selected to copy
    And the plan emission item is given the name 'Plan B'
    And the source sub item is saved
    # Copy item
    And the source sub item with label 'Plan A' is selected to copy
    And the plan emission item is given name 'Plan C' and category '13'
    And the source sub item is saved
    # Copy item
    And the source sub item with label 'Plan C' is selected to copy
    And the plan emission item is given name 'Plan D', category '7' and amount 2
    And the source sub item is saved
    And the source sub item with label 'Plan D' emission should be '2008,0'
    # Copy item
    And the source sub item with label 'Plan B' is selected to copy
    And the plan emission item is given name 'Plan D1', category '3' and amount 230
    And the source sub item is saved
    # Delete items
    And the source sub item with label 'Plan B' is selected and deleted
    And the source sub item with label 'Plan C' is selected and deleted
    # Edit item
    And the source sub item with label 'Plan D1' is selected to edit
    And the plan emission item is given the name 'Plan C'
    And the source sub item is saved
    # Add item
    And the source is added a new sub item
    And the plan emission item is given name 'Plan B' and category '1'
    And the source sub item is saved
    # Edit item
    And the source sub item with label 'Plan A' is selected to edit
    And the plan emission item is given name 'Plan C1' and category '7'
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Area 1' with emission row label 'NOx' should be 2429,4
    # emission of NH3 for the plan category was discarded   
    # Then the source emission of source 'Area 1' with emission row label 'NH3' should be 230,0
    ##
    # Copy the Source
    ##
    When the source with label 'Area 1' is selected
    And the selected source is copied
    And the source label is set to 'Area 2'
    And the source is saved
    #
    When the source with label 'Area 1' is selected
    And the selected source is copied
    And the source label is set to 'Area 3'
    And the source is saved
    # Delete source
    When the source with label 'Area 2' is selected
    And the selected source is deleted
    # Edit source
    When the source with label 'Area 3' is selected
    And the selected source is edited
    And the source sub item with label 'Plan B' is selected and deleted
    And the source sub item with label 'Plan C1' is selected and deleted
    #
    And the source sub item with label 'Plan C' is selected to edit
    And the plan emission item is given name 'Plan A', category '9' and amount 100
    And the source sub item is saved
    #
    And the source sub item with label 'Plan D' is selected to edit
    And the plan emission item is given name 'Plan B', category '4' and amount 100
    And the source sub item is saved
    # Save source
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Area 3' with emission row label 'NOx' should be 284,2
    # emission of NH3 for the plan category was discarded   
    # Then the source emission of source 'Area 3' with emission row label 'NH3' should be 100,0
    ##
    # Add point
    ##
    When a new source is added
    And the source location is set to a point with coordinates X 204699 and Y 353713
    And the source label is set to 'Area 51'
    And the source sector is set to sectorgroup Plan with no sector
    And the plan emission item is given name 'Home', category '1' and amount 150
    And the source sub item is saved
    And the source is saved
    And the taskbar substance is set to 'NH3'
    And the taskbar substance is set to 'NOx'
    Then the source emission of source 'Area 51' with emission row label 'NOx' should be 166,5
    # emission of NH3 for the plan category was discarded   
    # Then the source emission of source 'Area 51' with emission row label 'NH3' should be 150,0
    # Export
    Given an export to PAA OWN USE is started, with coorporation 'M.I.B.', project 'Roswell - Area 51', location 'Texasstreet, 9999 PO, Citybay', description 'I love it when a plan comes together'
    Then the notification panel contains no errors
    # Calculate
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
