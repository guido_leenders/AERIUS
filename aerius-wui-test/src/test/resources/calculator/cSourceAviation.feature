@composite @aviation @nightly
Feature: Aviation Emission

  Background:
    Given AERIUS CALCULATOR is open

  @AER-1713 @AER-1727 @AER-1743 @AER-1788
  Scenario: Make a pdf for aviation permit
    Given the startup is set to input sources
    And the taskbar year is set to 2025
    And the source location is set to a point with coordinates X 89246 and Y 440605
    And the source label is set to 'Zestienhoven'
    And the source sector is set to sectorgroup AVIATION with sector 3640
    And the source emission is set to NH3 1800, NOX 2100, PM10 0
    And the source is saved
    And an export to PAA OWN USE is started, with coorporation 'Aviation', project 'Aviation 1', location 'Aviation 1, 6211 ED, Aviation', description 'Aviation'
    # check

    When the calculation is started
    And the calculation ends
    And the menu item -results- is selected
    And the results table is set to show the area 'Meijendel & Berkheide'
    Then the result for Total Deposition in HabitatArea 'H2160' is '0,14'

