@single @import @nightly
Feature: ImportFile.feature: Add and calculate various sources by importing BRN and GML files into CALCULATOR

  Background: 
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file

  @import-brn @AER-1732
  Scenario Outline: Calculate imported BRN sources and check for automatic substance switch and errors 
    Given the BRN file '<brn_filename>' is imported
    When the BRN input substance is set to 'NOx'
    And the taskbar substance is 'NOx+NH3'
    Then the notification panel contains no errors
    #
    When the calculation is started
    And the calculation ends
    Then the taskbar substance is 'NOx'
    And the notification panel contains no errors

    Examples: Different brn files that need to pass with no errors
    | brn_filename              |
    | ImportOPSSourcesSmall.brn |
    | ImportSmallVersion4.brn   |

  @import-brn
  Scenario: Import BRN sources from a corrupted file and choose to continue importing while diregarding any invalid records 
    Given the BRN file 'ImportOPSSourcesBroken.brn' is imported
    When the BRN input substance is set to 'NOx'
    And the BRN file is imported despite invalid records
    And the taskbar substance is 'NOx+NH3'
    Then the notification panel contains no errors
    #
    When the calculation is started
    And the calculation ends
    Then the taskbar substance is 'NOx'
    And the notification panel contains no errors

  @import-gml
  Scenario: Calculate imported GML source and check for automatic substance switch and errors 
    Given the GML file 'case_1.gml' is imported
    When the taskbar substance is set to 'NOx+NH3'
    Then the notification panel contains no errors
    And the source emission of source 'Bron 1' with emission row label 'E 5.100' should be 68,0
    #
    When the calculation is started
    And the calculation ends
    Then the taskbar substance is 'NH3'
    And the notification panel contains no errors

  # TODO: Obsolete step priority project for item @AER-1718.
  @import-gml @fire @AER-1718 @AER-1759
  Scenario: Calculate imported GML with various sources and check for errors 
    Given  the GML file 'various_sources.gml' is imported
    When the taskbar substance is 'NOx'
    Then the notification panel contains no errors
    And the source emission of source 'Bron 1' with emission row label 'Boot AA' should be 8933,3
    And the source emission of source 'Bron 1' with emission row label 'Boot BB' should be 207,9
    And the source emission of source 'Bron 2' with emission row label 'NOx' should be 606,1
    And the source emission of source 'Bron 3' with emission row label 'Duikboot' should be 22614,8
    And the source emission of source 'Bron 3' with emission row label 'Sleep' should be 52814,6
    And the source emission of source 'Bron 4' with emission row label 'Middelzwaar vrachtverkeer' should be 274,7
    And the source emission of source 'Bron 4' with emission row label 'Busverkeer' should be 260,7
    And the source emission of source 'Bron 5' with emission row label 'Dieplader' should be 25,5
    And the source emission of source 'Bron 5' with emission row label 'Graafmachine' should be 17,8
    And the source emission of source 'Bron 5' with emission row label 'Afwijkende klasse' should be 2000,0
    And the source emission of source 'Bron 6' with emission row label 'D 1.1.1' should be 20,0
    And the source emission of source 'Bron 6' with emission row label 'E 5.14' should be 35,0
    And the source emission of source 'Bron 6' with emission row label 'A 1.1' should be 5700,0
    And the source emission of source 'Bron 6' with emission row label 'L 2.100' should be 150,0
    And the source emission of source 'Bron 7' with emission row label 'NOx' should be 1500,0
    And the source emission of source 'Bron 7' with emission row label 'NH3' should be 200,0
    And the source emission of source 'Bron 8' with emission row label 'NOx' should be 1000,0
    And the source emission of source 'Bron 8' with emission row label 'NH3' should be 200,0
    # limit calculation time by enabling priority radius option
    When the taskbar options-panel is opened
    And the calculation option priority project radius is enabled
    And the calculation option priority project type is set to 'Prioritair project Hoofdwegennet'
    And the taskbar panel is closed
#    And the calculation is started
#    And the calculation ends
    Then the taskbar substance is 'NOx+NH3'
    And the notification panel contains no errors
