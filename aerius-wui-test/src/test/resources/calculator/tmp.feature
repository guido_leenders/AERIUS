@tmpfix
Feature: tmp

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario: Set up the first situation
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 179058 and Y 332120
    And the source label is set to 'Bouwterrein 1'
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 1399, NOX 12500, PM10 0
    And the source is saved
    #
    And the calculation is started
    And the calculation ends
    And the sub menu item -results table- is selected
    And the results table is set to show the area 'Geleenbeekdal' 
    And the result for Total Deposition in HabitatArea 'H91E0C' is '0,246'

