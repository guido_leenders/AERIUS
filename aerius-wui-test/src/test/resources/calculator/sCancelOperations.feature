@nightly @cancel
Feature: Test the cancellation of editing operations

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario: Cancellation of first emission source for situation 1
    Given the startup is set to input sources
    When the operation is cancelled
    Then the notification panel contains no errors

    Given the startup is set to input sources
    When the source location is set to a point with coordinates X 141911 and Y 474017
    And the operation is cancelled
    Then the notification panel contains no errors

    # cancel after Y coordinate has been filled
    Given a new source is added
    When the source location coordinate Y is set to 5
    And the operation is cancelled
    Then the notification panel contains no errors

    # cancel after X coordinate has been filled
    Given a new source is added
    When the source location coordinate X is set to 5
    And the operation is cancelled
    Then the notification panel contains no errors

    Given a new source is added
    When the source location is set to a point with coordinates X 141911 and Y 474017
    And the source sector is set to sectorgroup ENERGY with no sector
    And the operation is cancelled
    Then the notification panel contains no errors

    Given a new source is added
    When the source location is set to a point with coordinates X 141911 and Y 474017
    And the source sector is set to sectorgroup AGRICULTURE with sector 4110
    And the operation is on subsource is cancelled
    And the operation is cancelled
    Then the notification panel contains no errors

    Given a new source is added
    And the source location is set to a point with coordinates X 141911 and Y 474017
    And the source sector is set to sectorgroup MOBILE_EQUIPMENT with sector 3220
    And the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 10
    When the operation is on subsource is cancelled
    And the operation is cancelled
    Then the notification panel contains no errors

    Given a new source is added
    And the source location is set to a point with coordinates X 141911 and Y 474017
    And the source sector is set to sectorgroup MOBILE_EQUIPMENT with sector 3220
    And the offroad emission item is set to STAGE class with name 'Mobile 1', category 1 and fuel amount 10
    When the source sub item is saved
    And the operation is cancelled
    Then the notification panel contains no errors

  Scenario: Cancel editing of existing source and check if source is unchanged
    Given the startup is set to input sources
    And the source location is set to a point with coordinates X 141911 and Y 474017
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 100, NOX 250, PM10 0
    And the source is saved
    And the source with label 'Bron 1' is selected
    And the selected source is edited
    And the source label is set to 'BRON EDIT'
    And the source emission is set to NH3 250, NOX 100, PM10 0
    When the operation is cancelled
    Then the notification panel contains no errors
    And the source emission of source 'Bron 1' with emission row label 'NOx' should be 250,0
    And the source emission of source 'Bron 1' with emission row label 'NH3' should be 100,0
