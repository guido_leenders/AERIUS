@nightly @distance-limit @ignore
Feature: Check calculation results for various source categories with and without optional distance treshold

  # Calculation results under 0.05 should be retained for:
  # - Road source calculations with distance limit
  # - Shipping source calculations with distance limit
  # - Generic source calculations with distance limit
  # Calculation results under 0.05 should be discarded for:
  # - All calculations without distance limit

#TODO: Check if the scenarios in this feature are still viable and remove obsolete GML's.
  @AER-1118 @roads-distance-limit @AER-1670 @AER-1713 @AER-1737 @AER-1718 @AER-1743
  Scenario: Upload a GML with the option Distance limit, check the warning, continue to calculate without the option
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'RoadSource-DistanceLimit.gml' with an old calculation option is imported
    Then the import dialog panel contains the message 'Afstandsgrenswaarde'
    And the option to continue the import is selected
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Meijendel & Berkheide' 
    Then the results table for area 'Meijendel & Berkheide' contains an exact match for values
      | H2130A     | 0,40  |
      | H2160      | 0,40  |
      | H2180Ao    | 0,21  |
      | H2130B     | 0,16  |
      | Lg12       | 0,10  |

  @AER-1118 @roads-distance-limit @AER-1670 @AER-1713 @AER-1737 @distancelimitignore
  Scenario: Upload GML with road source, calculate and check calculation results without distance limit
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'RoadSource-NoDistanceLimit.gml' is imported
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Meijendel & Berkheide' 
    Then the results table for area 'Meijendel & Berkheide' contains an exact match for values
      | H2130A     | 0,40  |
      | H2160      | 0,40  |
      | H2180Ao    | 0,21  |
      | H2130B     | 0,16  |
      | Lg12       | 0,10  |
      | H2180C     | 0,03  |
      | H2120      | 0,03  |
      | ZGH2180Abe | 0,01  |
      | H2180B     | 0,01  |
      | ZGH2130A   | 0,00  |
      | ZGH2160    | 0,00  |
      | H2190B     | 0,00  |
      | ZGH2130B   | 0,00  |
      | ZGH2180Ao  | 0,00  |
      | ZGH2180C   | 0,00  |
      | H2110      | 0,00  |
      | H2180Abe   | 0,00  |
      | ZGH2180B   | 0,00  |
      | H2190Ae    | 0,00  |
      | H2190Aom   | 0,00  |
      | H2190C     | 0,00  |
      | H3140      | 0,00  |

  @AER-1118 @shipping-distance-limit @AER-1713 @AER-1718 @distancelimitignore
  Scenario: Upload GML with shgipping source, calculate and check calculation results with distance limit
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ShippingSource-DistanceLimit.gml' is imported
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Solleveld & Kapittelduinen' 
    Then the results table for area 'Solleveld & Kapittelduinen' contains an exact match for values 
      | H2130A   | 0,36 |
      | H2180C   | 0,36 |
      | H2190Ae  | 0,36 |
      | H2160    | 0,35 |
      | Lg12     | 0,30 |
      | H2190B   | 0,27 |
      | ZGH2130A | 0,21 |
      | H2120    | 0,15 |
      | ZGH2190B | 0,06 |
      | H2110    | 0,04 |
      | H2180Ao  | 0,04 |
      | H2190Aom | 0,03 |
      | ZGH2120  | 0,00 |
      | H2130B   | 0,00 |
      | ZGH2130B | 0,00 |
      | H2150    | 0,00 |
      | H2180Abe | 0,00 |

  @AER-1118 @shipping-distance-limit @AER-1713 @distancelimitignore
  Scenario: Upload GML with shipping source, calculate and check calculation results without distance limit
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'ShippingSource-NoDistanceLimit.gml' is imported
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Solleveld & Kapittelduinen' 
    Then the results table for area 'Solleveld & Kapittelduinen' contains an exact match for values 
      | H2130A   | 0,36 |
      | H2180C   | 0,36 |
      | H2190Ae  | 0,36 |
      | H2160    | 0,35 |
      | Lg12     | 0,30 |
      | H2190B   | 0,27 |
      | ZGH2130A | 0,21 |
      | H2120    | 0,15 |
      | ZGH2190B | 0,06 |
      | H2110    | 0,04 |
      | H2180Ao  | 0,04 |
      | H2190Aom | 0,03 |
      | ZGH2120  | 0,02 |
      | H2180Abe | 0,02 |
      | H2130B   | 0,01 |
      | H2150    | 0,01 |
      | ZGH2130B | 0,01 |

  @AER-1118 @generic-distance-limit @AER-1713 @AER-1718 @distancelimitignore
  Scenario: Upload GML with generic sources, calculate with distance limit for roads enabled and check calculation results  
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GenericSource-DistanceLimitRoads.gml' is imported
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Solleveld & Kapittelduinen' 
    Then the results table for area 'Solleveld & Kapittelduinen' contains an exact match for values
      | H2180C   | 0,66 |
      | H2160    | 0,59 |
      | H2190B   | 0,48 |
      | H2130A   | 0,44 |
      | ZGH2130A | 0,44 |
      | H2190Ae  | 0,42 |
      | Lg12     | 0,36 |
      | H2120    | 0,30 |
      | H2180Ao  | 0,12 |
      | ZGH2190B | 0,09 |
      | H2110    | 0,04 |
      | ZGH2120  | 0,00 |
      | H2130B   | 0,00 |
      | ZGH2130B | 0,00 |
      | H2150    | 0,00 |
      | H2180Abe | 0,00 |
      | H2190Aom | 0,00 |

  @AER-1118 @generic-distance-limit @AER-1713 @AER-1718 @distancelimitignore
  Scenario: Upload GML with generic sources, calculate with distance limit for shipping enabled and check calculation results  
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GenericSource-DistanceLimitShipping.gml' is imported
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Solleveld & Kapittelduinen' 
    Then the results table for area 'Solleveld & Kapittelduinen' contains an exact match for values 
      | H2180C   | 0,66 |
      | H2160    | 0,59 |
      | H2190B   | 0,48 |
      | H2130A   | 0,44 |
      | ZGH2130A | 0,44 |
      | H2190Ae  | 0,42 |
      | Lg12     | 0,36 |
      | H2120    | 0,30 |
      | H2180Ao  | 0,12 |
      | ZGH2190B | 0,09 |
      | H2190Aom | 0,06 |
      | H2110    | 0,04 |
      | ZGH2120  | 0,00 |
      | H2130B   | 0,00 |
      | ZGH2130B | 0,00 |
      | H2150    | 0,00 |
      | H2180Abe | 0,00 |

  @AER-1118 @generic-distance-limit @AER-1713 @distancelimitignore
  Scenario: Upload GML with generic sources, calculate without distance limit and check calculation results
    Given AERIUS CALCULATOR is open
    And the startup is set to import a file
    And the GML file 'GenericSource-NoDistanceLimit.gml' is imported
    And the calculation is started
    And the calculation ends
    Then the notification panel contains no errors
    #
    Given the menu item -results- is selected
    And the sub menu item -results table- is selected
    When the results table is set to display as Maximum deposition
    And the results table is set to show the area 'Solleveld & Kapittelduinen' 
    Then the results table for area 'Solleveld & Kapittelduinen' contains an exact match for values 
      | H2180C   | 0,66 |
      | H2160    | 0,59 |
      | H2190B   | 0,48 |
      | H2130A   | 0,44 |
      | ZGH2130A | 0,44 |
      | H2190Ae  | 0,42 |
      | Lg12     | 0,36 |
      | H2120    | 0,30 |
      | H2180Ao  | 0,12 |
      | ZGH2190B | 0,09 |
      | H2190Aom | 0,06 |
      | H2110    | 0,04 |
      | H2130B   | 0,03 |
      | H2180Abe | 0,03 |
      | ZGH2130B | 0,03 |
      | ZGH2120  | 0,02 |
      | H2150    | 0,02 |
