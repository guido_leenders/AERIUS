@random-sources
Feature: Generate a lot of sources

  Scenario: Set up the first situation
    ##
    Given AERIUS CALCULATOR is open
    And the startup is set to input sources
    And the source location is set to a point with coordinates X 179058 and Y 332120
    And the source sector is set to sectorgroup ENERGY with no sector
    And the source emission is set to NH3 1399, NOX 12500, PM10 0
    And the source is saved
    #
    And a nice collection of 250 sources is added at a random location
    #
    And the taskbar options-panel is opened
    And the calculation scope is set to Nature Areas
    And the calculation range is set to 2 km
    And the taskbar panel is closed
    And the calculation is started
    And the calculation ends
    #
    And an export to GML with results is started
