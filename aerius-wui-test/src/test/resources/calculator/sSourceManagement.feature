@single @source-focus @nightly @aer-1205
Feature: The last added or edited source should always stay highlighted and scrolled into view 

  Background: 
    Given AERIUS CALCULATOR is open

  Scenario: After adding or editing an emission source the selection should be maintained and visible
    Given the startup is set to input sources
    And the menu item -sources- is selected

    # The last source entry is visible and highlighted 

    When a nice collection of 18 sources is added at a random location
    Then the source with label 'Random Source 18' is scrolled into view
    And the source with label 'Random Source 18' is highlighted

    # The last edited source is visible and highlighted

    When the source with label 'Random Source 12' is selected
    And the source is selected to edit
    And the source is saved
    Then the source with label 'Random Source 12' is scrolled into view
    And the source with label 'Random Source 12' is highlighted

    # New source entry replaces previously deleted source(s)

    When the source with label 'Random Source 1' is selected
    And the selected source is deleted
    And a nice collection of 1 sources is added at a random location
    Then the source with label 'Random Source 1' is scrolled into view
    And the source with label 'Random Source 1' is highlighted
