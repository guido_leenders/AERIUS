@single @nightly @search
Feature: Using the searchbox to search for string and selecting results

  Background: Open AERIUS CALCULATOR
    Given AERIUS CALCULATOR is open

  Scenario Outline: Search for <label>
    When the search for '<term>' is started
    Then the search suggestion '<suggestion>' is selected
    And the notification panel contains no errors

  Examples:
    | label                           | term             | suggestion                                 | 
    | province                        | gelder           | Gelderland (63)                           |
    | receptor-ID                     | 4734398          | x: 117696 y: 463143                        |
    | city                            | ter aar          | Ter Aar - 2 km (18)                        |
    | municipality                    | kaag en braassem | Kaag en Braassem - 4 km (18)               |
    | postcode4                       | 2401             | 2401 (Alphen aan den Rijn) - 5 km (14)     |
    | postcode6                       | 2401VC           | Vliestroom, 2401VC Alphen aan den Rijn     |
    | postcode6 + number              | 2401VC 115       | Vliestroom 115, 2401VC Alphen aan den Rijn |
    | street                          | vliestroom       | Vliestroom, Alphen aan den Rijn            |
    | street + number                 | vliestroom 115   | Vliestroom 115, 2401VC Alphen aan den Rijn |
    | nature 2000 area                | nieuwkoop        | Nieuwkoopse Plassen & De Haeck             |
    | special characters              | fochteloërveen   | Fochteloërveen                             |
    | nature area number single digit | 6                | Duinen Schiermonnikoog (6)                 |
    | nature area number multi digit  | 133              | Kampina & Oisterwijkse Vennen (133)        |
