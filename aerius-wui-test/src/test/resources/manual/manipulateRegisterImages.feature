@register-manual-edit
Feature: Image manipulation steps adding labels for the AERIUS REGISTER manual

  Background: 
    Given process images for product 'register'

  @register-manipulate-basisfuncties
  Scenario: 2325 Basisfuncties
    Given manipulate image '2325_Basisfuncties-Menu-Stap_1'
    And add image 'A' at position 147, 65
    And add image 'B' at position 147, 110
    And add image 'C' at position 147, 155
    And add image 'D' at position 147, 200
    And add image 'E' at position 147, 350
    And add image 'F' at position 147, 395
    And add image 'G' at position 147, 440

  Scenario: 2326 Berichtencentrum
    Given manipulate image '2326_Basisfuncties-Berichtencentrum-Stap_1'
    And add image 'A' at position 1007, 50

  @register-manipulate-accountinfo
  Scenario: 2327 Accountinformatie
    Given manipulate image '2327_Basisfuncties-Accountinformatie-Stap_3'
    And add image 'A' at position 900, 21
    And add image 'B' at position 1090, 44
    And add image 'Muis' at position 1140, 44

  Scenario: 2590 Zelfde scherm als bij 2327 met A bij zoekveld
    Given manipulate image '2590_Basisfuncties-Zoekfunctie-Stap_4'
    And add image 'A' at position 438, 44

    # 2364 Geen screenshot (Helpdesk plaatje zoals voor calculator) 
    # 2602 Plaatje met browseropties (Niet van de applicatie)
  Scenario: 2337
    Given manipulate image '2337_Basisfuncties-Dashboard-Stap_1'
    And add image 'A' at position 140, 77
    And add image 'B' at position 140, 315
    And add image 'C' at position 140, 560

  Scenario: 2335 Overzicht Dashboard Prioritaire Projecten (geen tags)
    Given manipulate image '2335_Basisfuncties-Dashboard-PrioritaireProjecten-Stap_1'

  Scenario: 2336 Overzicht Dashboard Ontwikkelingsruimte (geen tags)
    Given manipulate image '2336_Basisfuncties-Dashboard-Ontwikkelingsruimte-Stap_1'
#    And add image 'A' at position 429, 92
#    And add image 'B' at position 548, 92
#    And add image 'C' at position 613, 92
#    And add image 'D' at position 678, 92

  Scenario: 2367 Overzicht Dashboard Grenswaardereservering
    Given manipulate image '2367_Basisfuncties-Dashboard-Grenswaardereservering-Stap_1'
    And add image 'A' at position 1050, 540
    And add image 'B' at position 829, 540
    And add image 'C' at position 1079, 630

  Scenario: 2340 overzicht van aanvragen
    Given manipulate image '2340_Basisfuncties-Aanvragen-Stap_1'
    And add image 'A' at position 190, 75
    And add image 'B' at position 252, 75
    And add image 'C' at position 315, 75
    And add image 'D' at position 442, 25

  Scenario: 2342 aanvragen selecteren met een filter (geen tags)
    Given manipulate image '2342_Basisfuncties-Aanvragen-Filter-Stap_1'

  Scenario: 2344 invoeren nieuwe aanvraag (geen tags)
    Given manipulate image '2344_Basisfuncties-Aanvragen-Import-Stap_1'

  Scenario: 2346 overzicht van gegevens
    Given manipulate image '2346_Basisfuncties-Aanvragen-Gegevensoverzicht-Stap_1'
    And add image 'A' at position 265, 39
    And add image 'B' at position 342, 39
    And add image 'C' at position 422, 39
    And add image 'D' at position 547, 39

  Scenario: 2600 aanvraag verwijderen
    Given manipulate image '2600_Basisfuncties-Aanvragen-Verwijderen-Stap_1'
    And add image 'A' at position 1113, 101

  Scenario: 2348 dossier informatie
    Given manipulate image '2348_Basisfuncties-Aanvragen-DossierInformatie-Stap_1'
    And add image 'Muis' at position 270, 102

  Scenario: 2350 tab dossier
    Given manipulate image '2350_Basisfuncties-Aanvragen-Details-Stap_1'
    And add image 'Muis' at position 351, 102
    # eerst even bugje fixen (FIXME schermprint met verkeerde tab)

  Scenario: 2368 tab toetsing (FIXME nieuw screenshot met wachten op turbo widget)
    Given manipulate image '2368_Basisfuncties-Aanvragen-Toetsing-Stap_1'
    And add image 'Muis' at position 509, 102
    And add image 'A' at position 426, 505

  Scenario: 2354 tab ontwikkelingsruimte
    Given manipulate image '2354_Basisfuncties-Aanvragen-Ontwikkelingsruimte-Stap_1'
    And add image 'Muis' at position 546, 102

  Scenario: 2357 status aanpassen
    Given manipulate image '2357_Basisfuncties-Aanvragen-StatusOvergang-Stap_1'
    And add image 'Muis' at position 1076, 102

  Scenario: 2593 bijlage bij besluit
    Given manipulate image '2593_Basisfuncties-Aanvragen-BijlageBijBesluit-Stap_2'
    And add image 'A' at position 369, 480

  Scenario: 2594 aanvraag verwijderen (dmv definitief afwijzen) link naar handleiding bestaat niet
    Given manipulate image '2594_Basisfuncties-Aanvragen-DefinitieAfgewezen-Stap_3'

  Scenario: 2998 nieuw prioritair project
    Given manipulate image '2998_Basisfuncties-Nieuw-PrioProject-Stap_1'
#    And add image 'A' at position 160, 70

  Scenario: 2975 overzicht projecten met filter geopend
    Given manipulate image '2975_Basisfuncties-Filter-PrioProject-Stap_1'
    And add image 'A' at position 409, 39
    And add image 'B' at position 573, 103
    And add image 'C' at position 827, 172

  Scenario: 2981 prioritair project dossier info
    Given manipulate image '2981_Basisfuncties-Dossier-PrioProject-Stap_1'
#    And add image 'A' at position 150, 133
#    And add image 'B' at position 857, 139
#    And add image 'C' at position 396, 396

  Scenario: 2995 prioritair project dossier export
    Given manipulate image '2995_Basisfuncties-Export-PrioProject-Stap_1'
#    And add image 'A' at position 1065, 100
#    And add image 'B' at position 1115, 100

  Scenario: 2983 prioritair project detail info
    Given manipulate image '2983_Basisfuncties-Project-PrioProject-Stap_1'
#    And add image 'A' at position 280, 220
#    And add image 'B' at position 280, 259
#    And add image 'C' at position 280, 300
#    And add image 'D' at position 280, 345
#    And add image 'E' at position 660, 172
#    And add image 'F' at position 660, 220

  Scenario: 2994 prioritair deelproject status info (geen tags)
    Given manipulate image '2994_Basisfuncties-Status-SubProject-Stap_1'

  Scenario: 2985 prioritair deelproject aanvraag opties
    Given manipulate image '2985_Basisfuncties-Aanvraag-SubProject-Stap_1'
#    And add image 'A' at position 1062, 159
#    And add image 'B' at position 1115, 159

  Scenario: 2986 prioritair deelproject toetsing
    Given manipulate image '2986_Basisfuncties-Toetsing-SubProject-Stap_1'
#    And add image 'A' at position 1062, 159
#    And add image 'B' at position 152, 755

  Scenario: 2825 overzicht projecten met filter geopend (geen tags)
    Given manipulate image '2825_Basisfuncties-Filter-Aanvragen-Stap_1'

  Scenario: 2827 statusfilter op in behandeling (geen tags)
    Given manipulate image '2827_Basisfuncties-Filter-Aanvragen-Stap_2'

  Scenario: 2826 statusfilter op in de wachtrij (geen tags)
    Given manipulate image '2826_Basisfuncties-Filter-Aanvragen-Stap_3'

  Scenario: 2829 statusfilter op in behandeling (geen tags)
    Given manipulate image '2829_Basisfuncties-Filter-Aanvragen-Stap_4'

  Scenario: 2990 statusfilter op in behandeling (geen tags)
    Given manipulate image '2990_Basisfuncties-Filter-Aanvragen-Stap_5'

  Scenario: 2828 statusfilter op in behandeling (geen tags)
    Given manipulate image '2828_Basisfuncties-Filter-Aanvragen-Stap_6'

  Scenario: 2975 overzicht prioritaire projecten 
    Given manipulate image '2975_Basisfuncties-Dashboard-PrioProject-Stap_1'

  Scenario: 2979 overzicht projecten met filter geopend
    Given manipulate image '2979_Basisfuncties-Filter-PrioProject-Stap_1'
#    And add image 'A' at position 405, 34
#    And add image 'B' at position 571, 94
#    And add image 'C' at position 827, 172
#    And add image 'Muis' at position 1000, 188

  Scenario: 2994 statussen van prioritair project (Geen Tags)
    Given manipulate image '2994_Basisfuncties-Status-PrioProject-Stap_1'

  Scenario: 2988 rollen en rechten
    Given manipulate image '2988_Basisfuncties-Rollen-Stap_1'
    And add image 'Muis' at position 75, 290

  Scenario: 2999 nieuwe tab actualisatie
    Given manipulate image '2999_Basisfuncties-Actualisatie-PrioProject-Stap_1'

  Scenario: 3044 aanvraag tab toetsing
    Given manipulate image '3044_Basisfuncties-Aanvraag_Toetsing-Stap_1'
