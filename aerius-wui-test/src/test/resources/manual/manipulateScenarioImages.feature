@scenario-manual-edit
Feature: Image manipulation steps adding labels for the AERIUS SCENARIO manual

  Background: 
    Given process images for product 'scenario'

  Scenario: 3030 Startscherm (No Labels)
    Given manipulate image '3030_Basisfuncties-Start-Scenario-Stap_1'
    And add image 'A' at position 200, 31
    And add image 'B' at position 297, 31
    And add image 'C' at position 372, 31
    And add image 'D' at position 424, 31
    And add image 'E' at position 476, 31
    And add image 'F' at position 164, 252

  Scenario: 3082 Berekening importeren (zelfde basisscherm als #3030)
    Given manipulate image '3082_Basisfuncties-Import-Scenario-Stap_1'
    And add image 'A' at position 165, 219
    And add image 'Muis' at position 334, 240

  Scenario: 3079 Toepassing scenario (No Labels)
    Given manipulate image '3079_Basisfuncties-Toepassing-Scenario-Stap_1'

  Scenario: 3077 Aflezen resultaten (No Labels)
    Given manipulate image '3077_Basisfuncties-Resultaten-Scenario-Stap_1'
    And add image 'A' at position 222, 124
    And add image 'B' at position 337, 124
    And add image 'C' at position 828, 444
    And add image 'D' at position 1125, 18

  Scenario: 3033 Resultaten in tabel (No Labels)
    Given manipulate image '3033_Basisfuncties-ResultaatTabel-Scenario-Stap_2'
    And add image 'A' at position 360, 355

  Scenario: 3084 Resultaten in grafiek
    ### Grafiek weergave resultaten niet beschikbaar in scenario
    Given manipulate image '3084_Basisfuncties-ResultaatGrafiek-Scenario-Stap_3'
#    And add image 'A' at position 165, 219

  Scenario: 3085 Resultaten filteren
    Given manipulate image '3085_Basisfuncties-ResultaatFilter-Scenario-Stap_4'
    And add image 'A' at position 348, 220
    And add image 'B' at position 348, 264
    And add image 'C' at position 185, 427
    And add image 'D' at position 478, 32
    And add image 'D' at position 848, 418

  Scenario: 3090 Verkeersnetwerk visualisatie
    Given manipulate image '3090_Verkeersnetwerk-Eigenschappen'

  Scenario: 3091 Verkeersnetwerk visualisatie kaartlagen
    Given manipulate image '3091_Verkeersnetwerk-Kaartlagen'

  Scenario: 3100 Utilities overzicht
    Given manipulate image '3100_Utilfuncties-Overzicht'
#    And add image 'A' at position 380, 222
#    And add image 'B' at position 380, 254
#    And add image 'C' at position 380, 286
#    And add image 'D' at position 380, 318
#    And add image 'E' at position 380, 350

  Scenario: 3101 Utilities valideren en opties
    Given manipulate image '3101_Utilfuncties-Valideren'
    And add image 'A' at position 130, 400
    And add image 'B' at position 130, 432

  Scenario: 3102 Utilities converteren
    Given manipulate image '3102_Utilfuncties-Converteren'

  Scenario: 3103 Utilities verschilberekening leeg
    Given manipulate image '3103_Utilfuncties-Verschil-Stap_1'

  Scenario: 3104 Utilities verschilberekening ingevoerd
    Given manipulate image '3104_Utilfuncties-Verschil-Stap_2'

  Scenario: 3105 Utilities hoogste waarde
    Given manipulate image '3105_Utilfuncties-Hoogste'

  Scenario: 3106 Utilities totale waarde
    Given manipulate image '3106_Utilfuncties-Totaal'

  @cal19ignore
  Scenario: 3200 Rekenfuncties login
    Given manipulate image '3200_Rekenfuncties-Login'

  @cal19ignore
  Scenario: 3201 Rekenfuncties joboverzicht
    Given manipulate image '3201_Rekenfuncties-JobOverzicht'

  @cal19ignore
  Scenario: 3202 Rekenfuncties invoer berekening
    Given manipulate image '3202_Rekenfuncties-InvoerBerekening'

  @cal19ignore
  Scenario: 3203 Rekenfuncties invoer berekening voltooid
    Given manipulate image '3203_Rekenfuncties-InvoerBerekening-Stap_2'

  @cal19ignore
  Scenario: 3204 Rekenfuncties joboverzicht actief
    Given manipulate image '3204_Rekenfuncties-JobOverzicht-Actief'

  @cal19ignore
  Scenario: 3205 Rekenfuncties joboverzicht filter
    Given manipulate image '3205_Rekenfuncties-JobOverzicht-Filter'
