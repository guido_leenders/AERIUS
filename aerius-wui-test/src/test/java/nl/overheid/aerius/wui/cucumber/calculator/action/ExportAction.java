/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for generating an export using the export dialog.
 */
public class ExportAction {
  private static final Logger LOG = LoggerFactory.getLogger(ExportAction.class);

  private final FunctionalTestBase base;

  public ExportAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Clicks the export button to start an export.
   */
  public void clickExportButton() {
    base.buttonClick(TestID.BUTTON_EXPORT);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Clicks the confirm button in the export dialog, to confirm the export.
   */
  public void clickConfirmExportButton() {
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
    base.browserWaitForDrawAnimation();
  }

  /**
   * Selects the export type in the export dialog.
   * @param exportType {@link ExportType} export type/method to use.
   * TODO Commented out PDF export for AER-1727.
   */
  public void selectExportType(final ExportType exportType) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    switch (exportType) {
      case GML_SOURCES_ONLY: base.inputSelectRadioButton(TestID.RADIO_EXPORT_GISONLY); break;
      case GML_WITH_RESULTS: base.inputSelectRadioButton(TestID.RADIO_EXPORT_GISALL); break;
//      case PAA_DEVELOPMENT_SPACES: base.inputSelectRadioButton(TestID.RADIO_EXPORT_PAA_DEVELOPMENT_SPACES); break;
//      case PAA_DEMAND: base.inputSelectRadioButton(TestID.RADIO_EXPORT_PAA_DEMAND); break;
      case PAA_OWN_USE: base.inputSelectRadioButton(TestID.RADIO_EXPORT_PAA_OWNUSE); break;
      default: base.inputSelectRadioButton(TestID.RADIO_EXPORT_GISONLY); break;
    }
  }

  /**
   * Sets the field email address in the export dialog.
   * @param emailAddress String the email address to send the export to
   */
  public void setEmailAddress(final String emailAddress) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    if (!emailAddress.equals(base.waitForElementVisible(base.id(TestID.BUTTON_DIALOG_CONFIRM)).getText())) {
      base.inputSetValue(TestID.INPUT_EXPORT_EMAILADRESS, emailAddress);
    } else {
      LOG.info("Email address already filled in: " + emailAddress);
    }
  }

  /**
   * Sets the field corporation in the export dialog.
   * @param corporation String corporation
   */
  public void setCorporation(final String corporation) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    base.inputSetValue(TestID.INPUT_EXPORT_COORPORATION, corporation);
  }

  /**
   * Sets the field project name in the export dialog.
   * @param projectName String project name
   */
  public void setProjectName(final String projectName) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    base.inputSetValue(TestID.INPUT_EXPORT_PROJECTNAME, projectName);
  }

  /**
   * Sets the field street address in the export dialog.
   * @param location String location
   */
  public void setStreetAddress(final String streetAddress) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    base.inputSetValue(TestID.INPUT_EXPORT_STREET_ADDRESS, streetAddress);
  }

  /**
   * Sets the field postal code in the export dialog.
   * @param location String location
   */
  public void setPostcode(final String postcode) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    base.inputSetValue(TestID.INPUT_EXPORT_POSTCODE, postcode);
  }

  /**
   * Sets the field city in the export dialog.
   * @param location String location
   */
  public void setCity(final String city) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    base.inputSetValue(TestID.INPUT_EXPORT_CITY, city);
  }

  /**
   * Sets the field description in the export dialog.
   * @param description String description
   */
  public void setDescription(final String description) {
    base.buttonHover(TestID.BUTTON_DIALOG_CONFIRM);
    base.inputSetValue(TestID.INPUT_EXPORT_DESCRIPTION, description);
  }

  /**
   * Exports to GML witch sources only to the given email address.
   * @param emailAddress
   */
  public void exportGmlOnly(final String emailAddress) {
    clickExportButton();
    selectExportType(ExportType.GML_SOURCES_ONLY);
    setEmailAddress(emailAddress);
    clickConfirmExportButton();
  }

  /**
   * Exports to GML witch sources and results to the given email address.
   * @param emailAddress
   */
  public void exportGmlAll(final String emailAddress) {
    clickExportButton();
    selectExportType(ExportType.GML_WITH_RESULTS);
    setEmailAddress(emailAddress);
    clickConfirmExportButton();
  }

  /**
   * Sets multiple fields and exports to PAA (PDF) to the given email address.
   * @param mail
   * @param corporation
   * @param project
   * @param location
   * @param description
   * TODO Commented out PDF export method for AER-1727.
   */
//  public void exportPaa(final String mail, final String corporation, final String project, final String location, final String description) {
//
//    final String[] locationElements = location.split(",");
//
//    if (locationElements.length < 3) {
//      Assert.fail("PAA Export, location '" + location + "' not correctly specified (street, postcode, city)");
//    }
//
//    final String locationStreet = locationElements[0].trim();
//    final String locationPostcode = locationElements[1].trim();
//    final String locationCity = locationElements[2].trim();
//
//    clickExportButton();
//    selectExportType(ExportType.PAA_DEVELOPMENT_SPACES);
//    setEmailAddress(mail);
//    setCorporation(corporation);
//    setProjectName(project);
//    setStreetAddress(locationStreet);
//    setPostcode(locationPostcode);
//    setCity(locationCity);
//    setDescription(description);
//    clickConfirmExportButton();
//  }

  /**
   * Sets multiple fields and exports to PAA (PDF) to the given email address.
   * @param mail
   * @param corporation
   * @param project
   * @param location
   * @param description
   * TODO Commented out PDF export for AER-1727.
   */
  public void exportPaaOwnUse(final String mail, final String corporation, final String project, final String location, final String description) {

    final String[] locationElements = location.split(",");

    if (locationElements.length < 3) {
      Assert.fail("PAA Export Own User, location '" + location + "' not correctly specified (street, postcode, city)");
    }

    final String locationStreet = locationElements[0].trim();
    final String locationPostcode = locationElements[1].trim();
    final String locationCity = locationElements[2].trim();

    clickExportButton();
    selectExportType(ExportType.PAA_OWN_USE);
    setEmailAddress(mail);
    setCorporation(corporation);
    setProjectName(project);
    setStreetAddress(locationStreet);
    setPostcode(locationPostcode);
    setCity(locationCity);
    setDescription(description);
    clickConfirmExportButton();
  }

}
