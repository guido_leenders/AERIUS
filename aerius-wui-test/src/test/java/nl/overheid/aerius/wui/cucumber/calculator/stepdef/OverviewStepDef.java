/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;
import java.util.Random;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.CoordinateAttribute;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.SectorGroups;
import nl.overheid.aerius.wui.cucumber.base.Randomizer;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.CalculationPointAction;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionEditorGeneric;
import nl.overheid.aerius.wui.cucumber.calculator.action.LocationAction;
import nl.overheid.aerius.wui.cucumber.calculator.action.OverviewAction;
import nl.overheid.aerius.wui.cucumber.calculator.action.SourceAction;

public class OverviewStepDef {

  private final FunctionalTestBase base;
  private final OverviewAction overviewPage;
  private final LocationAction locationPage;
  private final SourceAction sourceDetails;
  private final EmissionEditorGeneric sourceEmission;
  private final CalculationPointAction calcpointDetails;

  public OverviewStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    overviewPage = new OverviewAction(base);
    locationPage = new LocationAction(base);
    sourceDetails = new SourceAction(base);
    sourceEmission = new EmissionEditorGeneric(base);
    calcpointDetails = new CalculationPointAction(base);
  }

  @When("^a new source is added$")
  public void when_overview_add_source() {
    overviewPage.clickAddSourceButton();
  }

  @When("^a nice collection of (.+) sources is added at a random location$")
  public void when_overview_add_sources_random(final Integer amount) {
    final int randomIntNH3 = 500;
    final int randomIntNOX = 1200;

    for (int i = 1; i <= amount; i++) {

      final String randomSector = Randomizer.get("ENERGY", "LIVE_AND_WORK", "RAIL_TRANSPORTATION", "AGRICULTURE", "INDUSTRY", "OTHER");
      final Random emissionRandom = new Random();

      overviewPage.clickAddSourceButton();
      locationPage.setCoordinatesXY(base.getRandomCoordinate(CoordinateAttribute.x), base.getRandomCoordinate(CoordinateAttribute.y));
      locationPage.clickButtonNext();
      sourceDetails.setLabel("Random Source " + Integer.toString(i));
      sourceDetails.setSectorGroup(SectorGroups.getUnsafe(randomSector));
      sourceDetails.setSector(base.getSectorGroupDefaultSector(SectorGroups.getUnsafe(randomSector)));
      sourceEmission.setGenericEmissions(Integer.toString(emissionRandom.nextInt(randomIntNH3)), 
          Integer.toString(emissionRandom.nextInt(randomIntNOX)), "0");
      sourceDetails.clickSaveNextStep();
    }
  }

  public String getRandom(final String... entries) {
    return entries[new Random().nextInt(entries.length)];
  }

  @When("^all sources are deleted$")
  public void when_overview_delete_all_sources() {
    overviewPage.clickDeleteAllSourcesButton();
    overviewPage.confirmDeleteAllSources();
  }

  @When("^the user clicks on delete all sources button$")
  public void user_clicks_delete_all_sources() {
    overviewPage.clickDeleteAllSourcesButton();
  }

  @When("^the user confirms the delete all sources dialog$")
  public void user_confirms_delete_all_sources() {
    overviewPage.confirmDeleteAllSources();
  }

  @When("^the user cancels the delete all sources dialog$")
  public void user_cancels_delete_all_sources() {
    overviewPage.cancelDeleteAllSources();
  }

  @When("^the source with label '(.+)' is selected$")
  public void when_overview_select_source(final String label) {
    overviewPage.selectSourceByLabel(label);
  }

  @When("^the source with label '(.+)' is edited$")
  public void when_overview_edit_source(final String label) {
    overviewPage.selectSourceByLabel(label);
    overviewPage.clickEditSourceButton();
    base.browserWaitForDrawAnimation();
  }

  @When("^the source with label '(.+)' is clicked on ID-tag$")
  public void when_overview_click_source_tag(final String label) {
    overviewPage.clickSourceIdTag(label);
  }

  @When("^the source with label '(.+)' is unavailable$")
  public void when_overview_source_unavailable(final String label) {
    Assert.assertFalse("Source should be unavailable", overviewPage.checkSourceAvailableByLabel(label));
  }

  @When("^the selected source is copied$")
  public void when_overview_copy_source() {
    overviewPage.clickCopySourceButton();
  }

  @When("^the selected source is edited$")
  public void when_overview_edit_source() {
    overviewPage.clickEditSourceButton();
    base.browserWaitForDrawAnimation();
  }

  @When("^the selected source is deleted$")
  public void when_overview_delete_source() {
    overviewPage.clickDeleteSourceButton();
  }

  @When("^a new calculation point is added$")
  public void when_overview_add_calcpoint() {
    overviewPage.clickAddCalcPointButton();
  }

  @When("^a new calculation point '(.+)' is added at coordinates X (.+) Y (.+)$")
  public void when_new_calculation_at_coords(final String label, final String xCoordinate, final String yCoordinate) {
    overviewPage.clickAddCalcPointButton();
    locationPage.setCoordinatesXY(xCoordinate, yCoordinate);
    calcpointDetails.setLabel(label);
    calcpointDetails.clickSaveButton();
  }

  @When("^all calculationpoints are deleted$")
  public void when_overview_delete_all_calcpoints() {
    overviewPage.clickDeleteAllCalcPointsButton();
  }

  @When("^the calculationpoint with label '(.+)' is selected$")
  public void when_overview_select_calcpoint(final String label) {
    overviewPage.selectCalculationPoint(label);
  }

  @When("^the calculationpoint with label '(.+)' is deleted$")
  public void when_calculationpoint_deleted(final String label) {
    overviewPage.selectCalculationPoint(label);
    overviewPage.clickDeleteCalcPointButton();
  }

  @When("^the calculationpoint with label '(.+)' is edited$")
  public void when_calculationpoint_edited(final String label) {
    overviewPage.selectCalculationPoint(label);
    overviewPage.clickEditCalcPointButton();
  }

  @When("^the selected calculationpoint is copied$")
  public void when_overview_copy_calcpoint() {
    overviewPage.clickCopyCalcPointButton();
  }

  @When("^the calculation point '(.+)' is copied to '(.+)'$")
  public void when_calculation_point_copied_to(final String sourceLabel, final String targetLabel) {
    overviewPage.selectCalculationPoint(sourceLabel);
    overviewPage.clickCopyCalcPointButton();
    calcpointDetails.setLabel(targetLabel);
    calcpointDetails.clickSaveButton();
  }

  @When("^the selected calculationpoint is edited$")
  public void when_overview_edit_calcpoint() {
    overviewPage.clickEditCalcPointButton();
  }

  @When("^the selected calculationpoint is deleted$")
  public void when_overview_delete_calcpoint() {
    overviewPage.clickDeleteCalcPointButton();
  }

  @When("^the namelabel switch for sources is toggled$")
  public void when_overview_toggle_name_labels_source() {
    base.buttonClick(TestID.TOGGLE_SOURCE_LABELS + "-label");
  }

  @When("^the namelabel switch for calculation points is toggled$")
  public void when_overview_toggle_name_labels_calcpoint() {
    base.buttonClick(TestID.TOGGLE_CALCPOINT_LABELS + "-label");
  }

  @When("^the overview edit button is disabled$")
  public void when_overview_check_edit_button_is_disabled() {
    base.objectIsEnabled(TestID.BUTTON_SOURCE_EDIT, false);
  }

  @Then("^the source emission of source '(.+)' with emission row label '(.+)' should be (.+)$")
  public void when_overview_check_source_emission_row(final String sourceLabel, final String emissionRowLabel, final String expectedEmission) {
    overviewPage.sourceDetailViewerCheckEmission(sourceLabel, emissionRowLabel, expectedEmission);
  }

  @And("^the total emission label shows '(.+)'$")
  public void checkTotalEmissionText(final String emissionText) {
    overviewPage.totalEmissionText(emissionText);
  }
}
