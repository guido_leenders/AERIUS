/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;

public class GenericStepDef {
  private static final Logger LOG = LoggerFactory.getLogger(GenericStepDef.class);

  private final FunctionalTestBase base;
  private final SeleniumDriverWrapper sdw;

  public GenericStepDef() throws IOException {
    sdw = SeleniumDriverWrapper.getInstance();
    base = new FunctionalTestBase(sdw);
  }

  @Given("^AERIUS (.+) is open$")
  public void aerius_application_is_open(final AeriusApplication application) {
    switch (application) {
    case CALCULATOR:
      openApplicationUrl(SeleniumDriverWrapper.CALCULATOR_APP_PATH);
      break;
    case REGISTER:
      openApplicationUrlAtLogin();
      break;
    case SCENARIO:
      openApplicationUrl(SeleniumDriverWrapper.SCENARIO_APP_PATH);
      break;
    default:
      throw new IllegalArgumentException("Invalid application path provided: " + application);
    }
  }

  private void openApplicationUrlAtLogin() {
    final String xpath = "//div[contains(@class, 'gwt-PopupPanelGlass')]";
    final boolean clicksBlocked = base.getDriver().findElements(base.xpath(xpath)).size() > 0;

    // Handle any click blocking dialog pop ups first
    if (clicksBlocked) {
      LOG.debug("Clicks appear to be blocked by a pop up dialog. Attempting to cancel.");
      base.buttonClick(base.waitForElementVisible(base.xpath("//*[contains(@id, 'Cancel') or contains(@id, 'cancel')]")));
      base.browserWaitForPopUpPanel();
    }

    // Logout in case the log out button is still shown, indicating an active session
    if (base.isElementDisplayed(TestIDRegister.BUTTON_LOGOUT)) {
      base.buttonClick(TestIDRegister.BUTTON_LOGOUT);
      // Slow down a bit or face the occasional error throwing soot in the food
      base.browserWaitForDrawAnimation();
      base.browserAlertAccept();
      base.getDriver().navigate().refresh();
    } else {
      openApplicationUrl(SeleniumDriverWrapper.REGISTER_APP_PATH);
    }
  }

  private void openApplicationUrl(final String applicationPath) {
    base.browserReload(base.getRunParameterAsString(applicationPath));
  }

  @Given("^AERIUS (.+) is open sized (\\d+) x (\\d+)$")
  public void aerius_application_is_open_sized(final AeriusApplication application, final int width, final int height) {
    base.getDriver().manage().window().setPosition(new Point(0, 0));
    base.getDriver().manage().window().setSize(new Dimension(width, height));
    aerius_application_is_open(application);
  }

  @Then("^wait until the page is loaded$")
  public void wait_until_page_ready() {
    // Triple the maximum wait time for loading pages due to diminished performance on test 
    final int timeOutMillis = sdw.getSettingPageLoadTimeout() * 3;
    LOG.debug("Waiting {} milliseconds for loading page", timeOutMillis);
    base.browserWaitForPageLoaded(Duration.ofMillis(timeOutMillis));
  }

  @Given("^wait for (\\d+) seconds$")
  public void when_aerius_wait(final int seconds) {
    LOG.debug("Slacking for {} seconds...", seconds);
    base.browserWait(Duration.ofSeconds(seconds));
  }

  @When("^the operation is cancelled$")
  public void when_cancel_operation() {
    base.buttonClick(TestID.BUTTON_CANCEL);
  }

  @When("^the operation is on subsource is cancelled$")
  public void when_cancel_operation_subsource() {
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_CANCEL);
  }

  @Then("^take a screenshot for the '(.+)' manual named '(.+)'$")
  public void take_a_screenshot_named(final String product, final String name) throws InterruptedException, IOException {
    final File path = new File(GenericStepDef.this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath(), "/manual/" + product);
    LOG.debug("Storing Manual screenshot: {}", path);
    path.mkdirs();
    base.takeScreenshot(path.getPath(), name);
  }

  @Then("^a validation message is showing with the text '(.+)'$")
  public void then_validation_message_is_showing(final String validationMessage) {
    base.checkOnValidationMessage(true, validationMessage);
    // Remove focus to trigger the pop-up's disappearance act and leave a clean state for the following step
    base.switchFocus(); 
  }

  @Then("^no validation message is showing$")
  public void then_validation_message_not_showing() {
    base.checkNoValidation();
  }

  enum AeriusApplication {
    CALCULATOR,
    REGISTER,
    SCENARIO
  }

}
