/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.SectorGroups;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.SourceAction;

public class SourceStepDef {

  private final FunctionalTestBase base;
  private final SourceAction sourceDetails;

  public SourceStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    sourceDetails = new SourceAction(base);
  }

  @When("^the source label is set to '(.+)'$")
  public void when_source_label_is_set(final String label) {
    sourceDetails.setLabel(label);
  }

  @When("^the source sector is set to sectorgroup (.+) with no sector$")
  public void when_source_sectorgroup_is_set(final String sectorgroup) {
    sourceDetails.setSectorGroup(SectorGroups.getUnsafe(sectorgroup));
  }

  @When("^the source sector is set to sectorgroup (.+) with sector (.+)$")
  public void when_source_sectorgroup_and_sector_is_set(final String sectorgroup, final String sector) {
    sourceDetails.setSectorGroup(SectorGroups.getUnsafe(sectorgroup));
    if (!"-".equals(sector)) {
      sourceDetails.setSector(sector);
    }
  }

  @When("^the source characteristics is toggled$")
  public void when_toggle_source_characteristics() {
    sourceDetails.toggleCharacteristicsCollapsePanel();
  }

  @When("^the source emissions is toggled$")
  public void toggleSourceEmissions() {
    sourceDetails.toggleSourceEmissionPanel();
  }

  @When("^the properties are toggled to forced$")
  public void toggleForced() {
    sourceDetails.toggleHeatContentForced();
  }

  @When("^the properties are toggled to not forced$")
  public void toggleNotForced() {
    sourceDetails.toggleHeatContentNotForced();
  }

  @When("^building influence is selected$")
  public void toggleBuildingInfluence() {
    sourceDetails.toggleBuildingInfluence();
  }

  @When("^the source characteristics are set to height (.*), heat (.*)$")
  public void when_source_characteristics_set(final String emissionHeight, final String heat) {
    sourceDetails.setSourceCharacteristics(emissionHeight, heat, "");
  }

  @When("^the source property heat content is set to (.*)$")
  public void set_input_heat_content(final String heatContent) {
    sourceDetails.setSourceCharacteristics("", heatContent, "");
  }

  @When("^the source property emission height is set to (.*)$")
  public void when_source_properties_set_height(final String emissionHeight) {
    sourceDetails.setSourceCharacteristics(emissionHeight, "", "");
  }

  @When("^the source property building length is set to (.*)$")
  public void set_input_building_length(final String buildingLength) {
    sourceDetails.setBuildingLength(buildingLength);
  }

  @When("^the source property building width is set to (.*)$")
  public void set_input_building_width(final String buildingWidth) {
    sourceDetails.setBuildingWidth(buildingWidth);
  }

  @When("^the source property building height is set to (.*)$")
  public void set_input_building_height(final String buildingHeight) {
    sourceDetails.setBuildingHeight(buildingHeight);
  }

  @When("^the source property building orientation is set to (.*)$")
  public void set_input_building_orientation(final String buildingOrientation) {
    sourceDetails.setBuildingOrientation(buildingOrientation);
  }

  @When("^the source property emission temperature is set to (.*)$")
  public void setEmissionTemperature(final String emissionTemperature) {
    sourceDetails.setSourceCharacteristicsForced(emissionTemperature, "", "", "");
  }

  @When("^the source property outflow diameter is set to (.*)$")
  public void setOutflowDiameter(final String outflowDiameter) {
    sourceDetails.setSourceCharacteristicsForced("", outflowDiameter, "", "");
  }

  @When("^the source property outflow velocity is set to (.*)$")
  public void setOutflowVelocity(final String outflowVelocity) {
    sourceDetails.setSourceCharacteristicsForced("", "", "", outflowVelocity);
  }

  @When("^the source properties emission temperature is set to (.*), emission height to (.*), outflow diameter to (.*), outflow velocity to (.+) with a speed of (.*)$")
  public void setSourcePropertiesForced(final String emissionTemperature, final String emissionHeight, final String outflowDiameter, final String outflowExitDiameter,
    final String outflowVelocity) {
    sourceDetails.setSourceCharacteristics(emissionHeight, "", "");
    sourceDetails.setSourceCharacteristicsForced(emissionTemperature, outflowDiameter, outflowExitDiameter, outflowVelocity);
  }

  @When("^the source properties emission height is set to (.*), outflow diameter to (.*), outflow velocity to (.+) with a speed of (.*)$")
  public void setSourcePropertiesForcedBuilding(final String emissionHeight, final String outflowDiameter, final String outflowExitDiameter,
    final String outflowVelocity) {
    sourceDetails.setSourceCharacteristics(emissionHeight, "", "");
    sourceDetails.setSourceCharacteristicsForced("", outflowDiameter, outflowExitDiameter, outflowVelocity);
  }

  @When("^the source heat content is set with building influence using lenght (.+), width (.+), height (.+), orientation (.+)$")
  public void when_source_building_influence(final String lenght, final String width, final String height, final String orientation) {
    sourceDetails.setBuildingInfluence(lenght, width, height, orientation);
  }

  @When("^the source location is edited$")
  public void when_source_edit_location() {
    base.buttonClick(TestID.BUTTON_EDIT_LOCATION);
  }

  @When("^the source is saved$")
  public void when_source_is_saved() {
    sourceDetails.clickSaveNextStep();
  }

  @Then("^the source with label '(.+)' is scrolled into view$")
  public void source_with_label_is_visible(final String label) {
    sourceDetails.isSourceScrolledIntoView(label);
  }

  @Then("^the source with label '(.+)' is highlighted$")
  public void source_with_label_is_highlighted(final String label) {
    sourceDetails.isSourceHighlighted(label);
  }

  @Then("^check that source property field '(.+)' is (.*)$")
  public void sourcePropertyFieldCheck(final String fieldName, final String fieldValue) {
    switch(fieldName) {
      case "Building height":
        sourceDetails.checkBuildingHeight(fieldValue, fieldName);
        break;
      case "Building length":
        sourceDetails.checkBuildingLength(fieldValue, fieldName);
        break;
      case "Building orientation":
        sourceDetails.checkBuildingOrientation(fieldValue, fieldName);
        break;
      case "Building width":
        sourceDetails.checkBuildingWidth(fieldValue, fieldName);
        break;
      case "Emission height":
        sourceDetails.checkEmissionHeight(fieldValue, fieldName);
        break;
      case "Emission temperature":
        sourceDetails.checkEmissionTemperature(fieldValue, fieldName);
        break;
      case "Heat content":
        sourceDetails.checkHeatContent(fieldValue, fieldName);
        break;
      case "Outflow diameter":
        sourceDetails.checkOutflowDiamter(fieldValue, fieldName);
        break;
      case "Outflow exit direction":
        sourceDetails.checkOutflowExitDirection(fieldValue, fieldName);
        break;
      case "Outflow velocity":
        sourceDetails.checkOutflowVelocity(fieldValue, fieldName);
        break;
      default:
        break;
      }
  }

  @Then("^the warning message for building influence is shown$")
  public void checkForValidationWarningMessage() {
    sourceDetails.checkForValidationWarningMessage();
  }

  @Then("^the warning message for building influence is not shown$") 
  public void checkForValidationWarningMessageAbsense() {
    Assert.assertFalse("Validation warning message is shown", sourceDetails.checkForValidationWarningMessageAbsense());
  }

  @Then("^the popup error message shows '(.+)'$")
  public void sourcePropertyPopUpErrorMessage(final String validationMessage) {
    sourceDetails.checkPopUpValidationMessage(validationMessage);
  }  
}
