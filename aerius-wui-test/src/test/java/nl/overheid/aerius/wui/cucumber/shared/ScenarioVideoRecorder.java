/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared;

import cucumber.api.Scenario;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.base.VideoRecorder;

/**
 * Capture a video for each test scenario using {@link VideoRecorder}
 * Recording can be enabled/disabled in the selenium.properties file
 */
public class ScenarioVideoRecorder implements ScenarioHandler {

  private SeleniumDriverWrapper sdw;
  private VideoRecorder videoRecorder;

  public ScenarioVideoRecorder() {
    sdw = SeleniumDriverWrapper.getInstance();
    if (sdw.getSettingVideoCapture()) {
      videoRecorder = VideoRecorder.getInstance();
    }
  }

  @Override
  public void beforeScenario(final Scenario result) {
    if (sdw.getSettingVideoCapture()) {
      videoRecorder.startRecording();
    }
  }

  @Override
  public void afterScenario(final Scenario result) {
    if (sdw.getSettingVideoCapture()) {
      videoRecorder.stopRecording();
      result.write("Video written to disk as: " + videoRecorder.getVideoFilePath());
    }
  }

}
