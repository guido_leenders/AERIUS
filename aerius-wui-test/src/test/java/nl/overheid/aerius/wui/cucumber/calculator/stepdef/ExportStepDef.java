/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.ExportAction;

import cucumber.api.java.en.When;

public class ExportStepDef {

  private final FunctionalTestBase base;
  private final ExportAction export;

  public ExportStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    export = new ExportAction(base);
  }

  @When("^the export button is clicked$")
  public void when_click_export_button() {
    export.clickExportButton();
  }

  @When("^the export option PAA OWN USE is selected$")
  public void when_select_export_option_paa_own_use() {
    export.selectExportType(ExportType.PAA_OWN_USE);
  }

  @When("^an export to GML with sources only is started$")
  public void when_export_gml_only() {
    export.exportGmlOnly(base.getRunParameterAsString(SeleniumDriverWrapper.CALCULATOR_USER_EMAIL));
    base.browserWaitForDrawAnimation();
  }

  @When("^an export to GML with results is started$")
  public void when_export_gml_all() {
    export.exportGmlAll(base.getRunParameterAsString(SeleniumDriverWrapper.CALCULATOR_USER_EMAIL));
    base.browserWaitForDrawAnimation();
  }

//  TODO Commented out PDF export stepdef for AER-1727
//  @When("^an export to PAA is started, with coorporation '(.+)', project '(.+)', location '(.+)', description '(.+)'$")
//  public void when_export_paa(final String coorporation, final String project, final String location, final String description) {
//    export.exportPaa(base.getRunParameterAsString(SeleniumDriverWrapper.CALCULATOR_USER_EMAIL), coorporation, project, location, description);
//    base.browserWaitForDrawAnimation();
//  }

//  TODO Commented out PDF export stepdef for AER-1727
  @When("^an export to PAA OWN USE is started, with coorporation '(.+)', project '(.+)', location '(.+)', description '(.+)'$")
  public void when_export_paa_own_use(final String coorporation, final String project, final String location, final String description) {
    export.exportPaaOwnUse(base.getRunParameterAsString(SeleniumDriverWrapper.CALCULATOR_USER_EMAIL), coorporation, project, location, description);
    base.browserWaitForDrawAnimation();
  }

}
