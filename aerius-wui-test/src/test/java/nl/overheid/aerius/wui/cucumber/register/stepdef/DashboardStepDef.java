/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.io.IOException;
import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.DashboardAction;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;
import nl.overheid.aerius.wui.cucumber.shared.action.NotificationAction;

import cucumber.api.java.en.When;

public class DashboardStepDef {
  private static final Logger LOG = LoggerFactory.getLogger(DashboardStepDef.class);

  private final FunctionalTestBase base;
  private final ImportAction imports;
  private final DashboardAction dashboard;
  private final NotificationAction notificationCenter;

  public DashboardStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    imports = new ImportAction(base);
    dashboard = new DashboardAction(base);
    notificationCenter = new NotificationAction(base);
  }

  @When("^the application with filename '(.+)' is imported with dossierID '(.+)'$")
  public void when_import_application(final String fileName, final String dossierId) {
    dashboard.buttonClickImport();
    imports.importSetApplicationId(dossierId);
    imports.importSetApplicationDate("21-12-2112 21:12");
    imports.importSelectFile(fileName, ImportType.PAA);
    imports.importConfirm(true);
    LOG.debug("Imported file '{}' with dossier id '{}'", fileName, dossierId);
    notificationCenter.checkNotificationForErrors();
  }

  @When("^the application import window is opened$")
  public void when_import_application_is_opened() {
    dashboard.buttonClickImport();
    LOG.debug("Opened the import application window");
  }

  @When("^the application import dossier id is set to '(.+)'$")
  public void when_import_application_is_opened(final String dossierId) {
    imports.importSetApplicationId(dossierId);
    LOG.debug("Set the import application dossier id to: " + dossierId);
  }

  @When("^the application import date is set to '(.+)'$")
  public void when_import_application_date_is_set(final String date) {
    imports.importSetApplicationDate(date);
    LOG.debug("Set the import application date to: " + date);
  }

  @When("^the application import file '(.+)' is selected$")
  public void when_application_import_file_is_selected(final String importFile) {
    imports.importSelectFile(importFile, ImportType.PAA);
    LOG.debug("Selected import file: " + importFile);
  }

  @When("^the application import window is confirmed$")
  public void when_import_application_is_confirmed() {
    imports.importConfirm();
    base.waitForElementVisible(base.id(TestIDRegister.APPLICATION_DOSSIER_ID_VIEWER), Duration.ofSeconds(5));
    LOG.debug("Confirmed and closed the application import window");
  }

  @When("^the application import window is confirmed to check for errors$")
  public void import_application_is_confirmed_for_error_check() {
    imports.importConfirm();
    LOG.debug("Confirmed application import window and ready to test for errors");
  }

  @When("^the application import window is cancelled$")
  public void when_import_application_is_cancelled() {
    imports.importCancel();
    base.waitForElementVisible(base.id(TestIDRegister.APPLICATION_DOSSIER_ID_VIEWER), Duration.ofSeconds(3));
    LOG.debug("Cancelled and closed the application import window");
  }

  @When("^the application import window contains error message '(.+)'$")
  public void the_import_window_contains_error_message(final String expectedErrorMessage) {
    imports.windowContainsError(expectedErrorMessage.replace("\"", "''"));
    imports.importCancel(true);
    LOG.debug("Confirmed presence of error message '{}' in the application import window", expectedErrorMessage);
  }

  @When("^the user filters permit dashboard on province '(.+)'$")
  public void when_user_filters_permit_province(final String province) {
    base.inputSelectListItemByText(base.id(TestIDRegister.INPUT_SELECT_PROVINCE_FILTER), province);
  }

  @When("^the user filters notice dashboard on province '(.+)'$")
  public void when_user_filters_notice_province(final String province) {
    base.inputSelectListItemByText(base.id(TestIDRegister.LIST_PROVINCES_NOTICES), province);
  }

  /**
   * Look up a specific filter checkbox and toggle if necessary
   * Fails if filter option is not found
   */
  @When("^the display of permit provinces with '(.+)' is '(.+)'$")
  public void hide_permit_provinces_from_view_by_filter_option(final String devRoomFilter, final String enabled) {

    WebElement checkBoxShortageFilter = null;
    final Boolean activateCheckBox = enabled.contentEquals("enabled") ? true : false;

    base.browserWaitForDrawAnimation();

    switch (devRoomFilter) {

    case "shortage":
      checkBoxShortageFilter = base.buttonHover(TestIDRegister.INPUT_CHECKBOX_PROVINCE_SHORTAGE);
      break;
    case "near shortage":
      checkBoxShortageFilter = base.buttonHover(TestIDRegister.INPUT_CHECKBOX_PROVINCE_NEAR_SHORTAGE);
      break;
    case "no shortage":
      checkBoxShortageFilter = base.buttonHover(TestIDRegister.INPUT_CHECKBOX_PROVINCE_NO_SHORTAGE);
      break;
    default:
      Assert.assertTrue("Unknown development room filter option: " + devRoomFilter, false);
      break;
    }
    if (null != checkBoxShortageFilter) {
      dashboard.checkBoxKeyToggle(checkBoxShortageFilter, activateCheckBox);
    }
  }

  @When("^the overview contains areas with status indication '(.+)'$")
  public void the_overview_contains_areas_with_status_indication(final String areaStatus) {
    dashboard.eCheckStatusPresence(areaStatus);
  }

  @When("^the overview contains area '(.+)'$")
  public void the_overview_contains_area(final String areaName) {
    dashboard.eCheckAreaPresence(areaName);
  }

  @When("^the page does not contain button '(.+)'$")
  public void the_page_not_contains(final String element) {
    final String xpath = "//*[contains(@id, '" + element + "') and not(contains(@style, 'display: none'))]";
    base.waitForElementInvisible(base.xpath(xpath), Duration.ofSeconds(1));
  }

  @When("^the permit dashboard contains '(.+)' nature areas$")
  public void the_permit_dashboard_contains_areas(final int areaCount) {
    final String xpath = "//div[contains(@title,'Afgeboekt')]/preceding-sibling::div[@class='gwt-Label']";
    final List<WebElement> areas = base.waitForElementList(base.xpath(xpath), Duration.ofSeconds(1), true);
    LOG.trace("Development room dashboard contains {} areas.", areas.size());
    Assert.assertTrue(areas.size() == areaCount);
  }

  @When("^the notice dashboard contains '(.+)' nature areas$")
  public void the_notice_dashboard_contains_areas(final int areaCount) {
    final String xpath = "//div[contains(@title,'Benut')]/preceding-sibling::div[@class='gwt-Label']";
    final List<WebElement> areas = base.waitForElementList(base.xpath(xpath), Duration.ofSeconds(1), true);
    LOG.trace("Treshold reservation dashboard contains {} areas.", areas.size());
    Assert.assertTrue(areas.size() == areaCount);
  }

}
