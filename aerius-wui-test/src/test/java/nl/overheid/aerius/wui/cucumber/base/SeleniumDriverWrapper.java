/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.base;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract helper class providing a convenient wrapper around Selenium @WebDriver
 * and access to system properties set for the current test run. (defaults are
 * set in <code>selenium[_base].properties</code>)
 */
public final class SeleniumDriverWrapper {

  private static final Logger LOG = LoggerFactory.getLogger(SeleniumDriverWrapper.class);
  private static SeleniumDriverWrapper INSTANCE;

  public static void initInstance() {
    getInstance();
  }

  public static void destroyInstance() {
    if (INSTANCE != null) {
      INSTANCE.getDriver().quit();
      INSTANCE = null;
    }
  }

  public static SeleniumDriverWrapper getInstance() {
    if (INSTANCE == null) {
      try {
        INSTANCE = new SeleniumDriverWrapper();
      } catch (final Exception e) {
        throw new RuntimeException(e);
      }
    }
    return INSTANCE;
  }

  private static final String SELENIUM_BASE_PROPERTIES = "selenium_base.properties";
  private static final String SELENIUM_LINUX_PROPERTIES = "selenium_linux.properties";

  // Not a real property, the driver will be loaded from the resources directory based on OS.
  // Get the latest chrome driver from http://chromedriver.storage.googleapis.com/index.html if the need arises.
  // At this time the drivers bundled are: Windows 32-bit / Linux 64-bit.
  private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
  private static final String WEBDRIVER_GECKO_DRIVER = "webdriver.gecko.driver";
  private static final String WEBDRIVER_IE_DRIVER = "webdriver.ie.driver";
  private static final String WEBDRIVER_FIREFOX_PROFILE = "webdriver.firefox.profile";

  private static final String CHROMEDRIVER_BINARY = "../../src/test/resources/chromedriver";
  private static final String GECKODRIVER_BINARY = "../../src/test/resources/geckodriver";
  private static final String EXPLORERDRIVER_BINARY = "../../src/test/resources/IEDriverServer";

  /*
   * ################################################
   * Testrun configuration from selenium.properties
   * ################################################
   */
  public static final String SETTINGS_USEBROWSER = "settings.usebrowser";
  public static final String SETTINGS_WINDOW_MAXIMIZE = "settings.window.maximize";
  public static final String SETTINGS_DRIVER_HEADLESS = "settings.driver.headless";
  public static final String SELENIUM_SERVER_URL = "selenium.server.url";
  public static final String CALCULATOR_USER_EMAIL = "calculator.user.email";
  public static final String CALCULATOR_APP_PATH = "calculator.app.path";
  public static final String REGISTER_APP_PATH = "register.app.path";
  public static final String SCENARIO_APP_PATH = "scenario.app.path";
  public static final String SETTINGS_PAGELOAD_WAIT = "settings.pageload.wait";
  public static final String SETTINGS_SYNC_LOG_MSG = "settings.sync.verbose";
  public static final String SETTINGS_SYNC_TIMEOUT = "settings.sync.timeout";
  public static final String SETTINGS_SYNC_SLEEP = "settings.sync.sleep";
  public static final String SETTINGS_CALCULATION_TIMEOUT = "settings.calculation.timeout";
  public static final String SETTINGS_NOTIFICATION_TIMEOUT = "settings.notification.timeout";
  public static final String SETTINGS_LOG_VERBOSE = "settings.log.verbose";
  public static final String SETTINGS_VALIDATION_TIMEOUT = "settings.validation.timeout";
  public static final String SETTINGS_OBJECTS_HIGHLIGHT = "settings.objects.highlight";
  public static final String SETTINGS_PATH_SCREENSHOTS = "settings.output.screenshots.path";
  public static final String SETTINGS_VIDEO_CAPTURE = "settings.output.video.capture";
  public static final String SETTINGS_VIDEO_MOUSE = "settings.output.video.mouse";
  public static final String SETTINGS_VIDEO_PATH = "settings.output.video.path";
  public static final String SETTINGS_VIDEO_BITDEPTH = "settings.output.video.bitdepth";
  public static final String SETTINGS_VIDEO_FRAMERATE = "settings.output.video.framerate";
  public static final String SETTINGS_AUDIO_CAPTURE = "settings.output.audio.capture";
  public static final String SETTINGS_AUDIO_BITDEPTH = "settings.output.audio.bitdepth";
  public static final String SETTINGS_AUDIO_FREQUENCY = "settings.output.audio.frequency";
  public static final String UNITS_SOURCE_EMISSION = "units.emission.source";
  public static final String UNITS_DEPOSITION_MOLHAYEAR = "units.deposition.molhayear";
  public static final String UNITS_DEPOSITION_MOLYEAR = "units.deposition.molyear";
  public static final String UNITS_DEPOSITION_PERCENTAGE = "units.deposition.percentage";

  // # End of configuration

  private WebDriver driver;
  private final String browser;

  public Properties props = new Properties();

  private SeleniumDriverWrapper() throws IOException {
    props.load(SeleniumDriverWrapper.class.getResourceAsStream(SELENIUM_BASE_PROPERTIES));
    // If not Windows, overwrite some keys with the Linux variant.
    if (!FunctionalTestBase.isWindows()) {
      props.load(SeleniumDriverWrapper.class.getResourceAsStream(SELENIUM_LINUX_PROPERTIES));
    }
    browser = getSettingAsString(SETTINGS_USEBROWSER, "chrome");
    driver = startDriver();
  }

  public WebDriver startDriver() {
    switch (browser) {
    case "firefox":
      createFireFoxDriver();
      break;
    case "chrome":
      createChromeDriver();
      break;
    case "explorer":
      createExplorerDriver();
      break;
    case "htmlunit":
      createHtmlUnitDriver();
      break;
    default:
      throw new IllegalArgumentException("Unknown browser in '" + SETTINGS_USEBROWSER + "': " + browser);
    }
    if (startDriverFullScreen()) {
      driver.manage().window().maximize();
    }

    return driver;
  }

  private void createFireFoxDriver() {
    final FirefoxOptions options = new FirefoxOptions();
    final DesiredCapabilities capabilities = DesiredCapabilities.firefox().merge(options);
    driver = remoteUrlAvailable() ? createRemoteWebDriver(capabilities) : createLocalFirefoxDriver(options);
  }

  private void createChromeDriver() {
    final ChromeOptions options = new ChromeOptions();

    if (startDriverFullScreen()) {
      options.addArguments("--start-maximized");
    }
    if (startDriverHeadless()) {
      options.addArguments("--headless");
    }

    final DesiredCapabilities capabilities = DesiredCapabilities.chrome().merge(options);
    driver = remoteUrlAvailable() ? createRemoteWebDriver(capabilities) : createLocalChromeDriver(options);
  }

  private void createExplorerDriver() {
    final DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
    driver = remoteUrlAvailable() ? createRemoteWebDriver(capabilities) : createLocalExplorerDriver();
  }

  private void createHtmlUnitDriver() {
    final DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
    capabilities.setJavascriptEnabled(true);
    driver = remoteUrlAvailable() ? createRemoteWebDriver(capabilities) : createLocalHeadlessDriver(capabilities);
  }

  private WebDriver createRemoteWebDriver(final DesiredCapabilities dc) {
    try {
      final RemoteWebDriver remoteWebDriver = new RemoteWebDriver(new URL(getRemoteUrl()), dc);
      // Set file detector to facilitate uploading of local files to a remote webdriver
      remoteWebDriver.setFileDetector(new LocalFileDetector());
      return remoteWebDriver;
    } catch (final MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  private boolean startDriverFullScreen() {
    return getRunParameterAsBoolean(SETTINGS_WINDOW_MAXIMIZE);
  }

  private boolean startDriverHeadless() {
    return getRunParameterAsBoolean(SETTINGS_DRIVER_HEADLESS);
  }

  private boolean remoteUrlAvailable() {
    return getRunParameterAsString(SELENIUM_SERVER_URL) != null;
  }

  private String getRemoteUrl() {
    return getRunParameterAsString(SELENIUM_SERVER_URL);
  }

  /**
   * Start a local ChromeDriver instance
   * @param options
   * @return ChromeDriver
   */
  private WebDriver createLocalChromeDriver(final ChromeOptions options) {
    final String driverPath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
        + CHROMEDRIVER_BINARY + (FunctionalTestBase.isWindows() ? ".exe" : "");

    System.setProperty(WEBDRIVER_CHROME_DRIVER, driverPath);

    return new ChromeDriver(options);
  }

  private WebDriver createLocalFirefoxDriver(final FirefoxOptions options) {
    LOG.info("Starting local FireFoxDriver");
    final String profilePath = props.getProperty(WEBDRIVER_FIREFOX_PROFILE);

    final String driverPath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
        + GECKODRIVER_BINARY + (FunctionalTestBase.isWindows() ? ".exe" : "");

    System.setProperty(WEBDRIVER_GECKO_DRIVER, driverPath);

    if (profilePath != null) {
      options.setProfile(new FirefoxProfile(new File(profilePath)));
    } else {
      options.setProfile(new FirefoxProfile());
    }

    return new FirefoxDriver(options);
  }

  private WebDriver createLocalExplorerDriver() {
    if (FunctionalTestBase.isWindows()) {
      LOG.info("Starting local Internet Explorer Driver");
    } else {
      LOG.warn("Attempting to start a local Internet Explorer on Linux!");
    }

    final String driverPath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + EXPLORERDRIVER_BINARY + ".exe";

    System.setProperty(WEBDRIVER_IE_DRIVER, driverPath);

    return new InternetExplorerDriver();
  }

  private WebDriver createLocalHeadlessDriver(final DesiredCapabilities capabilities) {
    LOG.info("Starting local headless (HTMLUnit) Driver");

    return new HtmlUnitDriver(capabilities);
  }

  public WebDriver getDriver() {
    return driver;
  }

  /**
   * @return true if video capture is enabled
   */
  public boolean getSettingVideoCapture() {
    return getRunParameterAsBoolean(SETTINGS_VIDEO_CAPTURE);
  }

  /**
   * @return true if mouse capture is enabled
   */
  public boolean getSettingVideoMouse() {
    return getRunParameterAsBoolean(SETTINGS_VIDEO_MOUSE);
  }

  /**
   * @return preferred output path for video files
   */
  public String getSettingVideoOutputPath() {
    return getRunParameterAsString(SETTINGS_VIDEO_PATH);
  }

  /**
   * @return preferred bit depth for video recording
   */
  public int getSettingVideoBitDepth() {
    return getRunParameterAsInt(SETTINGS_VIDEO_BITDEPTH);
  }

  /**
   * @return preferred frame rate for video recording
   */
  public int getSettingVideoFrameRate() {
    return getRunParameterAsInt(SETTINGS_VIDEO_FRAMERATE);
  }

  /**
   * @return true if video capture is enabled
   */
  public boolean getSettingAudioCapture() {
    return getRunParameterAsBoolean(SETTINGS_AUDIO_CAPTURE);
  }

  /**
   * @return the preferred bit depth for audio recording
   */
  public int getSettingAudioBitDepth() {
    return getRunParameterAsInt(SETTINGS_AUDIO_BITDEPTH);
  }

  /**
   * @return the preferred sample frequency for audio recording
   */
  public int getSettingAudioFrequency() {
    return getRunParameterAsInt(SETTINGS_AUDIO_FREQUENCY);
  }

  /**
   * @return the preferred time out period for loading pages
   */
  public int getSettingPageLoadTimeout() {
    return getRunParameterAsInt(SETTINGS_PAGELOAD_WAIT);
  }

  /**
   * @return the preferred time out period for calculations
   */
  public int getSettingCalculationTimeout() {
    return getRunParameterAsInt(SETTINGS_CALCULATION_TIMEOUT);
  }

  /**
   * @return the preferred time out period for notifications
   */
  public int getSettingNotificationTimeout() {
    return getRunParameterAsInt(SETTINGS_NOTIFICATION_TIMEOUT);
  }

  /**
   * @return the preferred time out period for element sync
   */
  public Duration getSettingSyncTimeout() {
    return Duration.ofSeconds(getRunParameterAsInt(SETTINGS_SYNC_TIMEOUT));
  }

  /**
   * @return the preferred sleep (polling) interval for element sync
   */
  public Duration getSettingSyncSleep() {
    return Duration.ofMillis(getRunParameterAsInt(SETTINGS_SYNC_SLEEP));
  }

  /**
   * @return boolean indicating whether verbose logging is enabled
   */
  public boolean getSettingLogVerbose() {
    return getRunParameterAsBoolean(SETTINGS_LOG_VERBOSE);
  }


  /**
   * Return the value from selenium.properties as a String or null if not found
   *
   * @param property name as string
   * @return value as @String
   */
  public String getRunParameterAsString(final String property) {
    return getSettingAsString(property, null);
  }

  /**
   * Returns the property value from selenium.properties as a String or the
   * given default value if the property is not set.
   *
   * @param property name as string
   * @param defaultValue the value to return if property is not set.
   * @return value as @String
   */
  public String getSettingAsString(final String property, final String defaultValue) {
    final String systemProperty = System.getProperty(property);
    final String result = systemProperty == null ? props.getProperty(property) : systemProperty;

    return result == null ? defaultValue : result;
  }

  /**
   * Return property value from selenium.properties as an Integer
   *
   * @param property string
   * @return value as @Integer
   */
  public int getRunParameterAsInt(final String property) {
    return Integer.parseInt(getRunParameterAsString(property));
  }

  /**
   * Gets run parameters from selenium.properties as a Boolean
   *
   * @param property string
   * @return value as @Boolean
   */
  public boolean getRunParameterAsBoolean(final String property) {
    return Boolean.parseBoolean(getRunParameterAsString(property));
  }

}
