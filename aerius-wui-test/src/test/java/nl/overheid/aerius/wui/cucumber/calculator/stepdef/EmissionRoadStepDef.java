/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionValuesEditorRoadAction;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionValuesEditorRoadAction.OnRoadSourceType;

import cucumber.api.java.en.When;

public class EmissionRoadStepDef {

  private final FunctionalTestBase base;
  private final EmissionValuesEditorRoadAction sourceEmissionRoad;

  public EmissionRoadStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    sourceEmissionRoad = new EmissionValuesEditorRoadAction(base);
  }

  @When("^the road speed typing panel is toggled$")
  public void when_source_road_toggle_speed_panel() {
    base.objectClickChild("buttonPlusMinus-Snelheidstypering", "div", TestID.DIV_COLLAPSEPANEL_EMISSION_SOURCE_DETAILS);
  }

  @When("^the road emission panel is toggled$")
  public void when_source_road_toggle_emission_panel() {
    base.objectClickChild("buttonPlusMinus-Verkeersemissies (per 24 uur)", "div", TestID.DIV_COLLAPSEPANEL_EMISSION_SOURCE_DETAILS);
  }

  @When("^the road emission max speed is set to (\\d+)$")
  public void when_source_road_max_speed_set(final String maxSpeed) {
    base.inputSelectListItem(TestID.LIST_MAXSPEED, maxSpeed, true);
  }

  @When("^the road emission strict enforcement is toggled$")
  public void when_source_road_strict_enforcement_toggled() {
    base.buttonClick(TestID.CHECKBOX_STRICT_ENFORCEMENT);
  }

  @When("^the road emission is added a new STANDARD specification with category '(.+)' and the vehicle amount of (\\d+) and trafficjam (\\d+)$")
  public void when_source_onroad_standard_item_is_set(final String sourceCategory, final String vehiclesPerDay, final String trafficJam) {
    sourceEmissionRoad.setOnRoadSpecificationType(OnRoadSourceType.STANDARD);
    sourceEmissionRoad.setOnRoadStandardCategory(sourceCategory);
    sourceEmissionRoad.setOnRoadVehiclesPerDay(vehiclesPerDay);
    sourceEmissionRoad.setOnRoadStagnationFactor(trafficJam);
  }

  @When("^the road emission is added a new EUROCLASS specification with category '(.+)' and the vehicle amount of (\\d+)$")
  public void when_source_onroad_euro_item_is_set(final String sourceCategory, final String vehiclesPerDay) {
    sourceEmissionRoad.setOnRoadSpecificationType(OnRoadSourceType.EURO);
    sourceEmissionRoad.setOnRoadEuroCategory(sourceCategory);
    sourceEmissionRoad.setOnRoadVehiclesPerDay(vehiclesPerDay);
    base.browserWaitForDrawAnimation();
  }

  @When("^the offroad emission item is set to STAGE class with name '(.+)', category (\\d+) and fuel amount (\\d+)$")
  public void when_source_offroad_item_stage_is_set(final String mobileSourceName, final String mobileSourceCategory, final String fuelAmount) {
    sourceEmissionRoad.setOffRoadStageItem(mobileSourceName, mobileSourceCategory, fuelAmount);
  }

  @When("^the offroad emission item is set to CUSTOM class with name '(.+)', height (\\d+), spread (\\d+), heat (\\d+) and NOX (\\d+)$")
  public void when_source_offroad_item_custom_is_set(final String mobileSourceName, final String height, final String spread, 
      final String heat, final String emission) {
    sourceEmissionRoad.setOffRoadCustomItem(mobileSourceName, height, spread, heat, emission);
  }

  @When("^the offroad emission item is set to CUSTOM class with name '(.+)', height (\\d+), heat (\\d+) and NOX (\\d+)$")
  public void when_source_offroad_item_custom_is_set(final String mobileSourceName, final String height, final String heat,
      final String emission) {
    sourceEmissionRoad.setOffRoadCustomItem(mobileSourceName, height, "", heat, emission);
  }

  @When("^the offroad emission item name is set to '(.+)'$")
  public void when_source_offroad_item_set_name(final String mobileSourceName) {
    sourceEmissionRoad.setOffRoadCustomItemName(mobileSourceName);
  }

  @When("^the offroad emission item calculator is opened$")
  public void when_source_offroad_item_calculator_opened() {
    base.buttonClick(TestID.BUTTON_OPEN_CALCULATOR_MOBILE);
    base.browserWaitForDrawAnimation();
  }

  @When("^the offroad emission item calculator is set for LOAD-USAGE with machinery '(.+)', fueltype '(.+)', power (\\d+), "
      + "load (\\d+), usage (\\d+), emissionfactor (.+)$")
  public void when_source_offroad_item_calculator_opened(final String machinery, final String fueltype, final String power, 
      final String load, final String usage, final String emissionfactor) {
    final String idPrefix = TestID.OFF_ROAD_CALCULATOR_RUNNING_HOURS;
    base.inputSelectListItemByText(base.id(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_MACHINERY_LIST)), machinery);
    base.inputSelectListItemByText(base.id(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_FUEL_LIST)), fueltype);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_POWER), power);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_LOAD), load);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_USAGE), usage);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_EF_MACHINERY), emissionfactor);
  }

  @When("^the offroad emission item calculator is set for FUEL-USAGE with machinery '(.+)', fueltype '(.+)', fuelusage (\\d+), "
      + "efficiency (\\d+), emissionfactor (.+)$")
  public void when_source_offroad_item_calculator_opened(final String machinery, final String fuelType, final String fuelUsage, 
      final String efficiency, final String emissionfactor) {
    final String idPrefix = TestID.OFF_ROAD_CALCULATOR_CONSUMPTION;
    base.buttonClick(TestID.OFF_ROAD_CALCULATOR + "-" + TestID.OFF_ROAD_CALCULATOR_TOGGLE_BUTTON + "-CONSUMPTION");
    base.inputSelectListItemByText(base.id(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_MACHINERY_LIST)), machinery);
    base.inputSelectListItemByText(base.id(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_FUEL_LIST)), fuelType);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_FUEL_USAGE), fuelUsage);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_EFFICIENCY), efficiency);
    base.inputSetValue(TestID.concat(idPrefix, TestID.OFF_ROAD_CALCULATOR_EF_FUEL), emissionfactor);
  }

  @When("^the offroad emission item calculator is closed, saving the calculated emission$")
  public void when_source_offroad_item_calculator_closed() {
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
    base.browserWaitForDrawAnimation();
  }

  @When("^the offroad emission item calculator saved value should be '(.+)'$")
  public void when_source_offroad_item_calculator_check_saved_value(final String savedValue) {
    base.inputCheckValue(base.idPrefix(TestID.INPUT_MOBILESOURCE_EMISSION), savedValue);
  }

  @When("^the road network tabel is selected at row '(.+)'$")
  public void when_source_road_network_select_row(final int roadNetworkRow) {
    base.tableClickElementInCell(TestID.CELLTABLE_NETWORK, roadNetworkRow, 1, "div");
  }

}
