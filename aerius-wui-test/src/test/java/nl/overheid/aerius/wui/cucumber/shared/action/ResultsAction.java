/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.DataTable;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.CalculationResultTableTypes;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;

/**
 * This class contains all the methods that apply for checking calculation results in the results-page.
 */
public class ResultsAction {
  private static final Logger LOG = LoggerFactory.getLogger(ResultsAction.class);

  private final FunctionalTestBase base;

  public ResultsAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Selects a type from the calculation resultstype-selector at the resultstable.
   * Precondition: Application displays calculation grid.
   * Postcondition: Application displays calculation grid.
   * @param resultTableType CalculationResultTableTypes result-type to display
   */
  public void selectResultsTableType(final CalculationResultTableTypes resultTableType) {
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
    base.waitForElementVisible(base.id(TestID.INPUT_DEPOSITION_COMPARISON_TYPE), Duration.ofSeconds(5));
    base.inputSelectListItem(TestID.INPUT_DEPOSITION_COMPARISON_TYPE, resultTableType.toString(), true);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  /**
   * Selects an area from the deposition results table.
   * Precondition: Application displays calculation grid.
   * Postcondition: Application selected area and its details.
   * @param areaName nature area to select from the results list
   */
  public void selectResultsTableArea(final String areaNameToSelect) {
    boolean natureAreaSelected = false;
    final WebElement resultsTable = base.buttonHover(TestID.DIV_TABLE_CALC_RES_DEPOSITION_COMPARISON);

    final List<WebElement> natureAreas = resultsTable.findElements(By.xpath(".//div[contains(@id, '" + TestID.TABLE + "-')]"));

    for (final WebElement natureArea : natureAreas) {

      final WebElement buttonPlusMinus = natureArea.findElement(By.xpath(".//div[contains(@id, '" + TestID.BUTTON_COLLAPSEPANEL_PLUSMINUS + "')]"));
      final WebElement natureAreaName = natureArea.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));
      if (areaNameToSelect.equals(natureAreaName.getText().trim())) {
        buttonPlusMinus.click();
        natureAreaSelected = true;
        LOG.info("Nature area toggeled collapse '" + areaNameToSelect + "'");
        break;
      }
    }
    if (!natureAreaSelected) {
      Assert.fail("Nature area NOT available: " + areaNameToSelect);
    }
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  /**
   * Checks the deposition-results from the resultstable from the row corresponding to the given habitat-area.
   * Precondition: Application displays the HT-details of the selected area.
   * Postcondition: Application displays the HT-details of the selected area, values have been checked.
   * @param habitatAreaCode habitat-area code to check
   * @param expectedValue expected value
   * @param depositionUnit expected unit of deposition
   */
  public void checkDepositionInResultsTableByHT(final String habitatToCheck, final String expectedValue, final String depositionUnit) {
    boolean habitatAreaChecked = false;
    base.waitForElementClickable(base.waitForElementVisible(base.id(TestID.DIV_TABLE_CALC_RES_DEPOSITION_COMPARISON)), true);

    // TODO: debug-id to pinpoint this element (innerTable) directly using the habitat code
    final List<WebElement> habitatAreas = base.waitForElementList(base.xpath("//div[@title='" + habitatToCheck + "']/parent::div"));
    Assert.assertTrue("Unexpected amount of habitat areas found: " + habitatAreas.size(), habitatAreas.size() > 0);
    for (final WebElement habitatArea : habitatAreas) {
      final String habitatAreaCode = habitatArea.findElement(By.xpath("./div[contains(@id, '" + TestID.CODE + "')]")).getText().trim();
      final String habitatAreaValue = habitatArea.findElement(By.xpath("./div[contains(@id, '" + TestID.VALUE + "')]")).getText().trim();
      if (habitatToCheck.equals(habitatAreaCode)) {
        // Replace newlines with spaces and log found habitat/deposition results on one line
        LOG.debug("Found deposition result for habitat area: '{}'", habitatArea.getText().replaceAll("\n", " "));
        habitatAreaChecked = true;
        Assert.assertEquals(expectedValue + depositionUnit, habitatAreaValue);
        break;
      }
    }
    if (!habitatAreaChecked) {
      Assert.fail("Habitat area not found: " + habitatToCheck);
    }
  }

  /**
   * Checks the amount of deposition of given calculation point.
   * Precondition: Application displays calculation point overview.
   * Postcondition: Application displays calculation point overview.
   * @param calculationPointLabel label of calculation point to check
   * @param deposition total amount of deposition of calculation point
   */
  public void checkCalcPointDepositionInOverview(final String calculationPointToCheck, final String expectedDepositionValue) {
    boolean calculationPointChecked = false;
    final WebElement resultsTable = base.buttonHover(TestID.DIV_TABLE_CALC_RES_CALCPOINT_COMPARISON);

    final List<WebElement> calculationPoints = resultsTable.findElements(By.xpath(".//div[contains(@id, '" + TestID.TABLE + "-')]"));

    for (final WebElement calcPoint : calculationPoints) {

      final WebElement calcPointLabelHolder =  calcPoint.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));
      final WebElement calcPointDepositionHolder =  calcPoint.findElement(By.xpath(".//div[contains(@id, '" + TestID.DEPOSITION + "')]"));

      final String calcPointLabel = calcPointLabelHolder.getText().trim();
      final String calcPointDeposition = calcPointDepositionHolder.getText().trim();
      final String depositionUnit = base.getRunParameterAsString(SeleniumDriverWrapper.UNITS_DEPOSITION_MOLHAYEAR);

      if (calculationPointToCheck.equals(calcPointLabel)) {
        calculationPointChecked = true;
        Assert.assertEquals("Calculationpoint result checked: '" + calculationPointToCheck + "'", expectedDepositionValue
            + depositionUnit, calcPointDeposition);
        LOG.info("Calculationpoint results checked: '" + calculationPointToCheck + "', value '" + calcPointDeposition + "'");
        break;
      }
    }
    if (!calculationPointChecked) {
      Assert.fail("Calculationpoint NOT available: " + calculationPointToCheck);
    }
  }

  /**
   * Moves the sliderbarknob up the given number of bars
   * @param filterKnob filterknob ID
   * @param numberOfBarsToMove number of bars to move up
   */
  public void moveFilterSliderBarUpInBars(final String filterKnob, final int numberOfBarsToMove) {
    final int yOffsetToMoveUp = numberOfBarsToMove * -20;
    moveFilterSliderBar(filterKnob, yOffsetToMoveUp);
  }

  /**
   * Moves the sliderbarknob down the given number of bars
   * @param filterKnob filterknob ID
   * @param numberOfBarsToMove number of bars to move down
   */
  public void moveFilterSliderBarDownInBars(final String filterKnob, final int numberOfBarsToMove) {
    final int yOffsetToMoveDown = numberOfBarsToMove * 20;
    moveFilterSliderBar(filterKnob, yOffsetToMoveDown);
  }

  /**
   * Moves the filter sliderbarknob with given offset
   * @param filterKnob filterknob ID
   * @param yOffset yOffset
   */
  public void moveFilterSliderBar(final String filterKnob, final int yOffset) {
    base.mouseMoveToElement(filterKnob);
    base.mouseClickAndHoldElement(filterKnob);
    base.mouseMoveByOffset(0, yOffset);
    base.mouseRelease();
  }

  /**
   * Checks if the expected data in the provided {@link DataTable} matches the actual
   * calculation results found in the application once calculation is completed   
   * @param expectedData 
   */
  public void resultsTableContains(final String area, final DataTable expectedData) {
    final List<WebElement> actualResultElements = 
        base.getElementListByXPath("//div[text()='" + area + "']//ancestor::div[starts-with(@id, 'gwt-debug-divtable-')]"
            + "//div[starts-with(@id, 'gwt-debug-innerTable-')]");
    ArrayList<ArrayList<String>> actualResultData = new ArrayList<ArrayList<String>>();

    // Gather actual data from all elements found in the results table
    for (final WebElement el : actualResultElements) {
      ArrayList<String> entry = new ArrayList<String>();
      entry.add(el.findElement(base.id(TestID.CODE)).getText().trim());
      entry.add(el.findElement(base.id(TestID.VALUE)).getText().trim());
      actualResultData.add(entry);
    }

    final DataTable actualResults = DataTable.create(actualResultData);
    LOG.trace("Expected data:\n{}", expectedData.toString());
    LOG.trace("Actual data:\n{}", actualResults.toString());

    // Compare expected list of results with actual results
    expectedData.diff(actualResults);
  }

  public void resultsTableMessage(final String message) {
    final String xPath = "//div[contains(@class, 'ResultTableView')]";
    assertEquals("Result table message is incorrect.", message, base.getElementByXPath(xPath).getText());
  }
  
  /**
   * Checks the exact amount of nature and habitat areas in de result list.
   * @param labelIDAmount
   */
  public void checkLabelIDAmount(final int labelIDAmount) {
	final List<WebElement> actualLabelIDAmount = 
	  base.waitForElementList(base.id(TestID.LABEL));
	assertEquals("The amount of Nature Areas is incorrect", labelIDAmount, actualLabelIDAmount.size());
  }
}
