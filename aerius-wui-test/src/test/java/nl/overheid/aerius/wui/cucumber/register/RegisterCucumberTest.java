/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import nl.overheid.aerius.wui.cucumber.base.CucumberBase;

/**
 * This is the runner class for cucumber test for the AERIUS REGISTER
 * Main Feature Testing:
 * @nightly               - lock, stock and barrel
 * @user-authorization    - user availability, role management and general access verification
 * @priority-projects     - all priority project related features
 * - @project-import      - only import projects, easy setup for manual test
 * - @project-cycle       - the full cycle of handling priority projects
 * @permit-applications   - all permit applications related features
 * - @applications-import - only import functionality, easy setup for manual test
 * - @applications-cycle  - the full cycle state transition
 *
 * Role Specific Testing:
 * @viewer, @editor, @superuser, @admin
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:target/cucumber/nightly/register", "json:target/cucumber/nightly/register/nightly_report.json"},
    features = "src/test/resources/register",
    glue = {
        "nl.overheid.aerius.wui.cucumber.register.stepdef",
        "nl.overheid.aerius.wui.cucumber.shared.action",
        "nl.overheid.aerius.wui.cucumber.shared.stepdef"
        },
    tags = {"@nightly", "~@cal19ignore", "~@ignore-auth", "~@ignore-dashboard", "~@ignore-applications", "~@ignore-permit"}
    )
public class RegisterCucumberTest extends CucumberBase {

}
