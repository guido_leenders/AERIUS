/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.SectorGroups;

/**
 * This class contains all the methods that apply for editing a source in the "Source Details" page.
 */
public class SourceAction {

  protected final FunctionalTestBase base;

  public SourceAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Selects a source's subitem from the source-details-overview and clicks the copy button.
   * Used with some sectors which can have subitems, like mobile sources, plans, shipping.
   * Precondition: Application displays sources-details overview with subitems.
   * Postcondition: Application selected a subitem, copied it and displays item details.
   * @param label subitem label
   * @throws InterruptedException
   */
  public void clickCopyEmissionItemByLabel(final String label) {
    base.sourceDetailsSelectSubItemByName(label);
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_COPY);
  }

  /**
   * Selects a source's subitem from the source-details-overview and clicks the delete button.
   * Used with some sectors which can have subitems, like mobile sources, plans, shipping.
   * Precondition: Application displays sources-details overview with subitems.
   * Postcondition: Application selected a subitem, deleted it
   * @param label subitem label
   */
  public void clickDeleteEmissionItemByLabel(final String label) {
    base.sourceDetailsSelectSubItemByName(label);
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_DELETE);
  }

  /**
   * Selects a source's subitem from the source-details-overview and clicks the edit button.
   * Used with some sectors which can have subitems, like mobile sources, plans, shipping.
   * Precondition: Application displays sources-details overview with subitems.
   * Postcondition: Application selected a subitem and displays subitem details.
   * @param label subitem label
   */
  public void clickEditEmissionItemByLabel(final String label) {
    base.sourceDetailsSelectSubItemByName(label);
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_EDIT);
  }

  /**
   * Selects a source's subitem from the source-details-overview a
   * Precondition: Application displays sources-details overview with subitems.
   * Postcondition: Application selected a subitem
   * @param label subitem label
   */
  public void clickSelectEmissionItemByLabel(final String label) {
    base.sourceDetailsSelectSubItemByName(label);
  }

  /**
   * Saves a sub item.
   * Precondition: Application displays a sub item and displays sub item details.
   * Postcondition: Application displays sources-details overview with sub items.
   */
  public void clickSaveSubItem() {
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  /**
   * Toggles source characteristics panel.
   */
  public void toggleCharacteristicsCollapsePanel() {
    base.objectClickChild("buttonPlusMinus", "div", TestID.DIV_COLLAPSEPANEL_SOURCECHARACTERISTICS);
  }

  /**
   * Toggles source characteristics panel.
   */
  public void toggleSourceEmissionPanel() {
    base.objectClickChild("buttonPlusMinus", "div", TestID.DIV_COLLAPSEPANEL_EMISSION_SOURCE_DETAILS);
  }

  /**
   * Toggles Building influence options
   */
  public void toggleBuildingInfluence() {
	  base.buttonClick(TestID.INPUT_OPS_BUILDING_INFLUENCE_CHECKBOX);
  }

  /**
   * Toggles to Heat Content Forced
   */
  public void toggleHeatContentForced() {
    base.buttonClick(TestID.INPUT_OPS_HEAT_TOGGLE_BUTTON_FORCED);
  }

  /**
   * Toggles to Heat Content Not Forced
   */
  public void toggleHeatContentNotForced() {
    base.buttonClick(TestID.INPUT_OPS_HEAT_TOGGLE_BUTTON_NOT_FORCED);
  }
  
/**
   * Sets the label in source details.
   * @param label String label
   */
  public void setLabel(final String label) {
    base.inputSetValue(base.id(TestID.INPUT_LABEL), label);
  }

  /**
  * Sets the sector group in source details.
  * @param sectorGroup {@link SectorGroups} sectorgroup
  */
  public void setSectorGroup(final SectorGroups sectorGroup) {
    final String sectorGroupListBox = base.returnSectorGroepValue(sectorGroup);
    base.inputSelectListItem(TestID.LIST_SECTORGROUP, sectorGroupListBox, false);
  }

  /**
  * Sets the sector in source details
  * @param sectorNumber String sectorNumber
  */
  public void setSector(final String sectorNumber) {
    if (!"".equals(sectorNumber)) {
      base.inputSelectListItem(TestID.LIST_SECTOR, sectorNumber, true);
    }
  }

  /**
   * Deprecated
   * Sets the source characteristic 'spread'.
   * @param spread the emission spread
   */
  @Deprecated
  public void setSpread(final String spread) {
    base.inputSetValue(TestID.INPUT_OPS_SPREAD, spread);
  }

  public void setBuildingHeight(final String height) {
    base.inputSetValue(TestID.INPUT_OPS_BUILDING_HEIGHT, height, false, false);
  }

  public void checkBuildingHeight(final String height, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OPS_BUILDING_HEIGHT);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(height, actualValue, fieldName);
  }

  public void setBuildingLength(final String length) {
    base.inputSetValue(TestID.INPUT_OPS_BUILDING_LENGTH, length, false, false);
  }

  public void checkBuildingLength(final String length, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OPS_BUILDING_LENGTH);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(length, actualValue, fieldName);
  }

  public void setBuildingOrientation(final String orientation) {
    base.inputSetValue(TestID.INPUT_OPS_BUILDING_ORIENTATION, orientation, false, false);
  }

  public void checkBuildingOrientation(final String orientation, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OPS_BUILDING_ORIENTATION);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(orientation, actualValue, fieldName);
  }

  public void setBuildingWidth(final String width) {
    base.inputSetValue(TestID.INPUT_OPS_BUILDING_WIDTH, width, false, false);
  }

  public void checkBuildingWidth(final String width, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OPS_BUILDING_WIDTH);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(width, actualValue, fieldName);
  }

  public void setEmissionHeight(final String emissionHeight) {
    base.inputSetValue(TestID.INPUT_OPS_HEIGHT, emissionHeight, false, false);
  }

  public void checkEmissionHeight(final String emissionHeight, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OPS_HEIGHT);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(emissionHeight, actualValue, fieldName);
  }

  public void setEmissionTemperature(final String emissionTemperature) {
    base.inputSetValue(TestID.INPUT_EMISSION_TEMPERATURE, emissionTemperature, false, false);
  }

  public void checkEmissionTemperature(final String emissionTemperature, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_EMISSION_TEMPERATURE);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(emissionTemperature, actualValue, fieldName);
  }

  public void setHeatContent(final String heatContent) {
    base.inputSetValue(TestID.INPUT_OPS_HEATCONTENT, heatContent, false, false);
  }

  public void checkHeatContent(final String heatContent, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OPS_HEATCONTENT);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(heatContent, actualValue, fieldName);
  }

  public void setOutflowDiameter(final String outflowDiameter) {
    base.inputSetValue(TestID.INPUT_OUTFLOW_DIAMETER, outflowDiameter, false, false);
  }

  public void checkOutflowDiamter(final String outflowDiameter, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OUTFLOW_DIAMETER);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(outflowDiameter, actualValue, fieldName);
  }
 
  public void setOutflowExitDirection(final String outflowExitDirection) {
    base.inputSelectListItem(TestID.INPUT_OPS_OUTFLOW_DIRECTION, outflowExitDirection, true);
  }

  public void checkOutflowExitDirection(final String outflowExitDirection, final String fieldName) {
    assertEquals("Base value for " + fieldName + " is incorrect.", outflowExitDirection, 
        base.waitForElementVisible(base.id(TestID.INPUT_OPS_OUTFLOW_DIRECTION)).getAttribute("title"));
  }

  public void setOutflowVelocity(final String outflowVelocity) {
    base.inputSetValue(TestID.INPUT_OUTFLOW_VELOCITY, outflowVelocity, false, false);
  }

  public void checkOutflowVelocity(final String outflowVelocity, final String fieldName) {
    final String valueID = base.idPrefix(TestID.INPUT_OUTFLOW_VELOCITY);
    final String actualValue = actualValueSourceEmission(valueID);
    assertBaseValue(outflowVelocity, actualValue, fieldName);
  }

  public String actualValueSourceEmission(final String fieldID) {
    return base.inputGetValue(fieldID);
  }

  public void assertBaseValue(final String expectedValue, final String actualValue, final String fieldName) {
    assertEquals("Base value for " + fieldName + " is incorrect.", expectedValue, actualValue);
  }

  /**
   * Sets multiple source characteristics.
   * Precondition: Application displays source details.
   * Postcondition: Application entered source characteristics/
   * @param height emission height
   * @param heat emission heatcontent
   * @param diurnalVariation emission diurnalVariation
   */
  public void setSourceCharacteristics(final String emissionHeight, final String heat, final String diurnalVariation) {
    if (!emissionHeight.isEmpty()) {
      setEmissionHeight(emissionHeight);
    }
    if (!heat.isEmpty()) {
      setHeatContent(heat);
    }
  }

  /**
   * Sets multiple source properties when forced is chosen
   * Precondition: Application displays forced source properties.
   * Postcondition: Application entered source characteristics.
   * @param emissionTemperature
   * @param outflowDiameter
   * @param outflowExitDirection
   * @param outflowVelocity
   */
  public void setSourceCharacteristicsForced(final String emissionTemperature, final String outflowDiameter, final String outflowExitDirection, final String outflowVelocity) {
    if (!emissionTemperature.isEmpty()) {
      setEmissionTemperature(emissionTemperature);
    }

    if (!outflowDiameter.isEmpty()) {
      setOutflowDiameter(outflowDiameter);
    }

    if (!outflowExitDirection.isEmpty()) {
      setOutflowExitDirection(outflowExitDirection);
    }

    if (!outflowVelocity.isEmpty()) {
      setOutflowVelocity(outflowVelocity);
    }
  }

  /**
   * Sets multiple building influence properties.
   * Precondition: Application displays source properties and building influence.
   * Postcondition:
   * @param length building length
   * @param width building width
   * @param height building height
   * @param orientation building orientation
   */
  public void setBuildingInfluence(final String length, final String width, final String height, final String orientation) {
    if (!length.isEmpty()) {
      setBuildingLength(length);
    }

    if (!width.isEmpty()) {
      setBuildingWidth(width);
    }

    if (!height.isEmpty()) {
      setBuildingHeight(height);
    }

    if (!orientation.isEmpty()) {
      setBuildingOrientation(orientation);
    }
  }

   /**
   * Checks the emission value of source sub items in the source detail emission values editor
   */
  public void checkEmissionItemEmissionValue(final String emissionItemLabel, final String expectedValue) {
    base.sourceDetailsSubItemCheckEmission(emissionItemLabel, expectedValue);
  }

  /**
   * Enters a new record within a sources editable cell table for subitems.
   * Precondition: Application displays source details.
   * Postcondition: Application shows new editable row for subitems.
   */
  public void clickAddNewEmissionItem() {
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_ADD);
  }

  /**
   * Saves the source data by clicking the Save/Next step button.
   * Precondition: Application displays source details.
   * Postcondition: Application entered source-data and shows sources-overview
   */
  public void clickSaveNextStep() {
    base.buttonClick(TestID.BUTTON_NEXTSTEP);
  }

  public void isSourceScrolledIntoView(final String label) {
    final String xpath = "//div[@id='" + base.idPrefix(TestID.LABEL) + "' and contains(.,'" + label + "')]";
    Assert.assertTrue("Source is not scrolled into view: " + label, base.isElementScrolledIntoView(base.waitForElementVisible(base.xpath(xpath))));
  }

  public void isSourceHighlighted(final String label) {
    final String xpath = "//div[contains(@class, 'selectedRow')]/child::div[@id='gwt-debug-label' and .='" + label + "']";
    base.waitForElementVisible(base.xpath(xpath));
  }

  public void checkPopUpValidationMessage(final String validationMessage) {
    base.checkOnValidationMessage(true, validationMessage);
  }

  public void checkForValidationWarningMessage() {
    base.inputCheckValue(base.idPrefix(TestID.INPUT_OPS_BUILDING_WARNING_LABEL), 
      "Een of meer ingevulde getallen valt buiten de standaardwaarden waarmee AERIUS Calculator de invloed van het gebouw bepaalt, "
      + "zie factsheet. Er wordt gerekend met de dichtstbijzijnde waarde die momenteel beschikbaar is.");
  }

  public boolean checkForValidationWarningMessageAbsense() {
    return base.isElementVisible(TestID.INPUT_OPS_BUILDING_WARNING_LABEL);
  }
}
