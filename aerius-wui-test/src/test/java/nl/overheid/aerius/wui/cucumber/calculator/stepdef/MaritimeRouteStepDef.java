/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import org.openqa.selenium.Keys;

import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.SourceAction;

/**
 * Set Definition class for Maritime Mooring detail screen
 */
public class MaritimeRouteStepDef {

  private final FunctionalTestBase base;
  private final SourceAction source;

  public MaritimeRouteStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    source = new SourceAction(base);
  }

  @When("^the maritime route ship is a '(.+)' of category '(.+)' with (\\d+) ships$")
  public void when_the_maritime_route_ship_is_entered(final String name, final String category, final int amount) {
    base.inputSetValue(TestID.INPUT_SHIPPING_NAME, name);
    base.inputSetValue(TestID.INPUT_SHIPPING_CATEGORY, category, false, true);
    base.inputSetValue(TestID.INPUT_SHIPPING_AMOUNT, String.valueOf(amount));
    source.clickSaveSubItem();
  }

  @When("^the maritime route ship name is set to '(.+)'$")
  public void when_the_maritime_route_ship_name(final String name) {
    setInputValue(TestID.INPUT_SHIPPING_NAME, name);
  }

  @When("^the maritime route ship category is set to '(.+)'$")
  public void when_the_maritime_route_ship_category(final String category) {
    setInputValue(TestID.INPUT_SHIPPING_CATEGORY, category);
  }

  @When("^the maritime route ship amount  is set to '(.+)'$")
  public void when_the_maritime_route_ship_amount(final String amount) {
    setInputValue(TestID.INPUT_SHIPPING_AMOUNT, amount);
  }

  private void setInputValue(final String id, final String value) {
    base.buttonHover(id).sendKeys(Keys.chord(Keys.CONTROL, "a"), value);
  }
}
