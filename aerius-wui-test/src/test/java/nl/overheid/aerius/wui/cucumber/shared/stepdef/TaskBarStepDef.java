/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;
import java.util.List;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.TaskBarAction;
import nl.overheid.aerius.wui.cucumber.shared.action.TaskBarAction.ResultType;
import nl.overheid.aerius.wui.cucumber.shared.action.TaskBarAction.TaskBarPanel;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TaskBarStepDef {

  private final FunctionalTestBase base;
  private final TaskBarAction taskbar;

  public TaskBarStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    taskbar = new TaskBarAction(base);
  }

  @When("^the taskbar layer-panel is opened$")
  public void when_taskbar_panel_layers_is_opened() {
    taskbar.openPanel(TaskBarPanel.LAYERS);
  }

  @When("^the taskbar legend-panel is opened$")
  public void when_taskbar_panel_legend_is_opened() {
    taskbar.openPanel(TaskBarPanel.LEGEND);
  }

  @When("^the taskbar info-panel is opened$")
  public void when_taskbar_panel_info_is_opened() {
    taskbar.openPanel(TaskBarPanel.INFO);
  }

  @When("^the taskbar layer-panel is clicked$")
  public void when_taskbar_layer_panelbutton_clicked() {
    base.buttonClick(TestID.BUTTON_LAYERPANEL);
  }

  @When("^the taskbar options-panel is opened$")
  public void when_taskbar_panel_options_is_opened() {
    taskbar.openPanel(TaskBarPanel.OPTIONS);
  }

  @When("^the layer with the name '(.+)' is present$")
  public void when_taskbar_layer_present(final String mapLayerName) {
    if (!taskbar.isLayerPresent(mapLayerName)) {
      SeleniumDriverWrapper.getInstance().getDriver().navigate().refresh();
    }
  }

  @When("^the layer with the name '(.+)' is toggled on or off$")
  public void when_taskbar_layer_toggle_on(final String mapLayerName) {
    taskbar.layerToggleOnOff(mapLayerName);
  }

  @When("^the layer with the name '(.+)' is toggled collapse$")
  public void when_taskbar_layer_toggle_collapse(final String mapLayerName) {
    taskbar.layerToggleCollapse(mapLayerName);
  }

  @When("^the layer with the name '(.+)' has its opacity set to (\\d+)$")
  public void when_taskbar_layer_set_opacity(final String mapLayerName, final String opacity) {
    taskbar.layerSetOpacity(mapLayerName, opacity);
  }

  @When("^the taskbar panel is closed$")
  public void when_taskbar_panel_is_closed() {
    taskbar.closePanel();
  }

  @When("^the taskbar year is set to (\\d+)$")
  public void when_taskbar_set_year(final String year) {
    taskbar.setYear(year);
  }

  @When("^the taskbar year is (\\d+)$")
  public void then_taskbar_year_is(final String year) {
    taskbar.setYear(year);
  }

  @When("^the taskbar year list is opened$")
  public void when_taskbar_open_year() {
    taskbar.openYearList();
  }

  @When("^the taskbar substance is set to '(.+)'$")
  public void when_taskbar_set_substance(final String substance) {
    taskbar.setSubstance(substance);
  }

  @When("^the taskbar substance is '(.+)'$")
  public void then_taskbar_substance_is(final String substance) {
    taskbar.getSubstance(substance);
  }

  @When("^the taskbar substance list is opened$")
  public void when_taskbar_open_substance() {
    taskbar.openSubstanceList();
  }

  @When("^the taskbar resulttype is set to CONCENTRATION$")
  public void when_taskbar_set_resulttype_concentration() {
    taskbar.setResultType(ResultType.CONCENTRATION);
  }

  @When("^the taskbar resulttype is set to DEPOSITION$")
  public void when_taskbar_set_resulttype_depostion() {
    taskbar.setResultType(ResultType.DEPOSTION);
  }

  @When("^the taskbar resulttype is set to DAYS$")
  public void when_taskbar_set_resulttype_days() {
    taskbar.setResultType(ResultType.DAYS);
  }

  @When("^the habitat with code '(.+)' is selected from the filter$")
  public void when_taskbar_select_habitat_from_filter(final String habitatCode) {
    taskbar.selectHabitatFromFilter(habitatCode);
  }

  @When("^the habitat filter list is opened$")
  public void when_taskbar_open_habitat_filter_list() {
    taskbar.openHabitatFilterList();
  }

  @When("^the taskbar info-panel is toggeled for Habitatype$")
  public void when_taskbar_infopanel_toggle_habitat() {
    taskbar.infoToggleHabitats();
  }

  @When("^the taskbar info-panel is toggeled for Depositie$")
  public void when_taskbar_infopanel_toggle_deposition() {
    taskbar.infoToggleDeposition();
  }

  @When("^the taskbar info-panel is toggeled for area '(.+)'$")
  public void when_taskbar_infopanel_toggle_area(final String areaName) {
    taskbar.infoToggleAreaName(areaName);
  }

  @When("^the taskbar info-panel is toggeled for habitat '(.+)'$")
  public void when_taskbar_infopanel_select_ht_area(final String htCode) {
    taskbar.infoToggleHabitatTypeByCode(htCode);
  }

  @When("^the taskbar layer Total Deposition list is opend$")
  public void when_taskbar_total_deposition_list_open() {
    taskbar.openTotalDepositionLayerList();
  }

  @When("^the taskbar layer Total Deposition layer '(.+)' is selected$")
  public void when_taskbar_total_deposition_select_layer(final String totalDepositionLayerName) {
    taskbar.selectLayerFromTotalDepositionPerSector(totalDepositionLayerName);
  }

  @When("^the following layers are present$")
  public void layers_are_present(final List<String> layers) {
    when_taskbar_panel_layers_is_opened();
    checkLayerAvailability(layers, true);
  }

  @When("^the following layers are absent$")
  public void layers_are_absent(final List<String> layers) {
    when_taskbar_panel_layers_is_opened();
    checkLayerAvailability(layers, false);
  }

  @Then("^the taskbar year is set to year and the background deposition is '(.+)'$")
  public void check_background_deposition(final String backgroundDeposition, final List<String> years) {
    for (final String year : years) {
      taskbar.setYear(year);
      taskbar.backgroundDepositionCheck(backgroundDeposition);
    }
  }

  @And("^the cursor pointer for '(.+)' is visible with image '(.+)'$")
  public void cursorPointerInfoPanel(final String cursorType, final String cursorImage) {
  final String xPath = "//table[contains(@class, 'markerTable')]";
    taskbar.checkCursorPointerAmount();
    taskbar.checkCursorPointer(cursorType, cursorImage, xPath);
  }

  @And("^the cursor pointer for '(.+)' is visible on the map with image '(.+)'$")
  public void cursorPointerMapLayer(final String cursorType, final String cursorImage) {
    final String xPath = "//div[contains(@class, 'infoMarkerContainer')]";
    taskbar.checkCursorPointer(cursorType, cursorImage, xPath);
  }

  private void checkLayerAvailability(final List<String> layers, final Boolean expected) {
    for (final String layer : layers) {
      final String debugId = TestID.DIV_LAYERPANELITEM + "-" + layer.replace(" ", "_");
      if (expected) {
        base.waitForElementVisible(base.id(debugId));
      } else {
        base.waitForElementInvisible(base.id(debugId));
      }
    }
  }

}
