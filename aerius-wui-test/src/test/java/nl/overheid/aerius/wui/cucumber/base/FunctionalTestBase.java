/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.base;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDRegister;

/**
 * This class contains all basic methods for handling the browser, mouse,
 * inputs (field, list box, radio buttons), tables
 */
public class FunctionalTestBase {

  private static final Logger LOG = LoggerFactory.getLogger(FunctionalTestBase.class);
  private final SeleniumDriverWrapper seleniumDriverWrapper;
  private WebDriver driver;
  private final String driverWindowHandle;

  public FunctionalTestBase(final SeleniumDriverWrapper seleniumDriverWrapper) {
    this.seleniumDriverWrapper = seleniumDriverWrapper;
    driver = seleniumDriverWrapper.getDriver();
    driverWindowHandle = driver.getWindowHandle();
  }

  /***
   * Switch focus from active element by sending a click to the application header/title text
   * Conveniently remove focus from pop up elements to ensure they disappear before continuing
   */
  public void switchFocus() {
    try {
      buttonClick(xpath("//h1"));
      browserWaitForPopUpPanel(seleniumDriverWrapper.getSettingSyncTimeout());
    } catch (final Exception e) {
      LOG.warn("Exception caught while switching focus: ", e);
    }
  }

  /**
   * Returns true if the current OS is windows.
   */
  public static boolean isWindows() {
    return System.getProperty("os.name").startsWith("Windows");
  }

  /**
   * Returns true if current browser is Chrome
   */
  public boolean isChrome() {
    return ((RemoteWebDriver) driver).getCapabilities().getBrowserName().equals("chrome");
  }

  public ProductType getProductType() {
    final Pattern p = Pattern.compile("(calculator|register|scenario|melding)");
    final Matcher m = p.matcher(driver.getCurrentUrl());
    String product = null;
    if (m.find()) {
      product = m.group().toUpperCase();
    }

    // Make the current scenario fail if appName is still null at this point
    Assert.assertNotNull("No applicable application name found in URL!", product);

    return ProductType.valueOf(product);
  }

  /**
   * @return the driver
   */
  public WebDriver getDriver() {
    return driver;
  }

  /**
   * @return the initial window handle
   */
  public String getInitialWindowHandle() {
    return driverWindowHandle;
  }

  /**
   * Return a {@link By} object for the given id with <code>gwt-debug-</code> prefix
   * @param id the 'raw' id as specified in the {@link TestID} class
   * @return a prefixed {@link By} locator to the desired {@link WebElement}
   */
  public By id(final String id) {
    return By.id(idPrefix(id));
  }

  public By xpath(final String xpath) {
    return By.xpath(xpath);
  }

  /**
   * Add the default GWT debug id prefix to given string
   * @param id string without prefix
   * @return prefixed debug id
   */
  public String idPrefix(final String id) {
    return "gwt-debug-" + normalizeDebugIdPart(id);
  }

  /**
   * Returns a test id post fix with baseId prefixed to the id.
   * @param baseId  prefix base id
   * @param id      actual id
   * @return concatenated id
   */
  public String withBaseId(final String baseId, final String id) {
    return normalizeDebugIdPart(baseId) + "-" + normalizeDebugIdPart(id);
  }

  public void browserSwitchToWindow(final String windowName) {
    driver.switchTo().window(windowName);
    LOG.debug("Switched to window: '{}'", windowName);
  }

  public void browserSwitchToLastWindow() {
    browserWait(Duration.ofSeconds(1));
    for (final String winHandle : driver.getWindowHandles()) {
      driver.switchTo().window(winHandle);
      LOG.debug("Switched to window handle: '{}' with title: '{}'", winHandle, driver.getTitle());
    }
  }

  /**
   * Make the browser sleep for the given duration of time
   * @param {@link Duration}
   */
  public void browserWait(final Duration duration) {
    try {
      synchronized (driver) {
        LOG.trace("Slacking for {} milliseconds!", duration.toMillis());
        Thread.sleep(duration.toMillis());
      }
    } catch (InterruptedException e) {
      LOG.error("Caught Exception while idling {} milliseconds", duration.toMillis(), e);
    }
  }

  /**
   * Default wait for animated elements to be drawn like the import pop up dialogue (500 ms) 
   */
  public void browserWaitForDrawAnimation() {
    browserWait(Duration.ofMillis(500));
  }

  /**
   * Sync to the animated loading widget and wait for it to disappear.
   * Ensures a smooth transition in to and out of time consuming steps
   * TODO: make AERIUS spit out a proper id with every widget
   * @param milliseconds
   */
  public void browserWaitForPageLoaded(final Duration duration) {
    final By locator = xpath("//*[contains(@class, 'ProgressStatusWidget') or contains(@id, 'gwt-debug-turboturbo')]");
    try {
      LOG.trace("Locating element using: '{}'", locator);
      waitForElementVisible(locator);
      LOG.debug("Waiting for loading widget to become invisible...");
      waitForElementInvisible(locator, duration);
    } catch (final Exception e) {
      LOG.trace("Caught exception: '{}'", e);
    }
  }

  /**
   * Sync to the pop up panel and wait for it to disappear.
   * Ensures a smooth transition to and from click blocking dialogs.
   */
  public void browserWaitForPopUpPanel(final Duration timeOutDuration) {
    final String xpath = "//div[contains(@class, 'gwt-PopupPanel')]";
    LOG.debug("Waiting {} seconds for popup panel to become invisible...", timeOutDuration.getSeconds());
    waitForElementInvisible(xpath(xpath), timeOutDuration);
  }

  /**
   * Sync to the glass panel and wait for it to disappear.
   * Ensures a smooth transition to and from click blocking dialogs.
   */
  public void browserWaitForPopUpPanel() {
    // Wait for pop up to disappear twice as long as default page load timeout 
    browserWaitForPopUpPanel(getSettingPageLoadTimeout().multipliedBy(2));
  }

  /**
   * Navigate to the given URL, accept any alerts and wait for any loading widgets
   * @param url to navigate to
   */
  public void browserReload(final String url) {
    driver.get(url);
    browserAlertAccept();
    browserWaitForPageLoaded(getSettingPageLoadTimeout());
  }

  /**
   * Accept any alert present and log exceptions if verbose logging is enabled
   */
  public void browserAlertAccept() {
    try {
      LOG.info("Accepting alert containing text: '{}'", getAlert().getText());
      getAlert().accept();
    } catch (final TimeoutException e) {
      LOG.trace("Caught TimoutException while accepting alert: ", e);
    }
  }

  /**
   * Dismiss any alert present and log exceptions if verbose logging is enabled
   */
  public void browserAlertDismiss() {
    try {
      LOG.info("Dismissing alert containing text: '{}'", getAlert().getText());
      getAlert().dismiss();
    } catch (final TimeoutException e) {
      LOG.trace("Caught TimoutException while dismissing alert: ", e);
    }
  }

  /**
   * Wait for and return any browser alert popping up in the next second
   * @return {@link Alert} if present or
   */
  private Alert getAlert() {
    return getWaitDefault().withTimeout(Duration.ofSeconds(1)).until(ExpectedConditions.alertIsPresent());
  }

  /**
   * Returns a handy wait object with default settings (selenium.properties)
   * @return {@link WebDriverWait} to use for
   */
  public WebDriverWait getWaitDefault() {
    return new WebDriverWait(driver, seleniumDriverWrapper.getSettingSyncTimeout().getSeconds(), seleniumDriverWrapper.getSettingSyncSleep().toMillis());
  }

  /**
   * Scan entire page source for presence of given text. Keep in mind this is
   * the least efficient method of finding elements on the page!
   * @param searchText
   * @param expected true if searchText should be displayed
   */
  public void browserCheckForText(final String searchText, final boolean expected) {
    LOG.debug("Checking page for text '{}' presence to be '{}'", searchText, String.valueOf(expected));
    final boolean textFoundOnPage = driver.getPageSource().contains(searchText);
    Assert.assertEquals("Check for text failed '" + searchText + "'", expected, textFoundOnPage);
  }

  /**
   * Close the browser and switch focus to another windows (if found present)
   */
  public void browserClose() {
    final String currentWindow = driver.getWindowHandle();
    driver.close();
    // return to the initial browser window if available
    if (currentWindow != getInitialWindowHandle()) {
      driver.switchTo().window(getInitialWindowHandle());
    }
  }

  /**
   * Get the id of a {@link WebElement} object, and returns the logical id as string.
   * @param {@link WebElement} object
   * @return id of element or null if not found
   */
  public String getElementId(final WebElement webElement) {
    try {
      return waitForElementClickable(webElement, false).getAttribute("id");
    } catch (final Exception e) {
      LOG.error("Failed to get atrribute id from WebElement", e);
      return null;
    }
  }

  /**
   * Gets the CheckBox element to click on.
   */
  public String getBrowserCheckboxSetStyle() {
    return "label";
  }

  /**
   * Gets the CheckBox element to click on.
   */
  public String getBrowserRadioSetStyle() {
    return "label";
  }

  /**
   * Takes a screenshot of the active window and stores in at the location defined in SETTINGS_PATH_SCREENSHOTS.
   * @return path to screenshot taken
   */
  public String takeScreenshot(final String path, final String imageName) throws IOException {
    final File file = new File(path, imageName + ".png");
    final File scrFile = takeScreenshot(OutputType.FILE);
    // store file
    FileHandler.copy(scrFile, file);

    return file.getAbsolutePath();
  }

  public <E> E takeScreenshot(final OutputType<E> outputType) {
    // RemoteWebDriver does not implement the TakesScreenshot class
    // if the driver does have the Capabilities to take a screenshot
    // then Augmenter will add the TakesScreenshot methods to the instance
    return ((TakesScreenshot) new Augmenter().augment(driver)).getScreenshotAs(outputType);
  }

  /**
   * Checking for a specific text in a button
   * @param id a unique identifier used to locate the button
   * @param text the string expected for this button
   */
  public void buttonCheckText(final String id, final String text) {
    Assert.assertTrue(buttonTextContains(id, text));
  }

  public WebElement waitForElementVisible(final By locator) {
    return waitForElementVisible(locator, seleniumDriverWrapper.getSettingSyncTimeout());
  }

  public WebElement waitForElementVisible(final By locator, final Duration timeOutDuration) {
    LOG.debug("Waiting {} second(s) for element to become visible using locator: '{}'", timeOutDuration.getSeconds(), locator.toString());
    return new WebDriverWait(driver, timeOutDuration.getSeconds(), seleniumDriverWrapper.getSettingSyncSleep().toMillis())
        .ignoring(StaleElementReferenceException.class)
        .until(ExpectedConditions.visibilityOfElementLocated(locator));
  }

  public void waitForElementInvisible(final By locator) {
    waitForElementInvisible(locator, seleniumDriverWrapper.getSettingSyncTimeout());
  }

  public void waitForElementInvisible(final By locator, final Duration timeOutDuration) {
    LOG.debug("Waiting {} second(s) for element to become invisible using locator: '{}'", timeOutDuration.getSeconds(), locator.toString());
    new WebDriverWait(driver, timeOutDuration.getSeconds(), seleniumDriverWrapper.getSettingSyncSleep().toMillis())
        .ignoring(StaleElementReferenceException.class)
        .until(ExpectedConditions.invisibilityOfAllElements(waitForElementList(locator)));
  }

  public WebElement waitForElementClickable(final WebElement webElement, final boolean failOnError) {
    WebElement result = null;
    final FluentWait<WebDriver> wait = new FluentWait<>(driver)
        .withTimeout(seleniumDriverWrapper.getSettingSyncTimeout())
        .pollingEvery(seleniumDriverWrapper.getSettingSyncSleep());
    try {
      result = wait.until(ExpectedConditions.elementToBeClickable(webElement));
      dumpElementDetails(result);
    } catch (final StaleElementReferenceException stale) {
      throw stale;
    } catch (final Exception e) {
      if (failOnError) {
        Assert.fail("An unexpected exception occurred while waiting for clickable element: " + e);
      } else {
        LOG.trace("Intentionally ignoring exception while waiting for clickable element: ", e);
      }
    }
    return result;
  }

  public List<WebElement> waitForElementList(final By locator, final Duration timeOutDuration, final boolean failOnError) {
    try {
      final WebDriverWait wait = new WebDriverWait(driver, timeOutDuration.getSeconds());
      wait.until(driver -> driver.findElements(locator) != null);
    } catch (final TimeoutException e) {
      if (failOnError) {
        Assert.fail("Timeout occurred while waiting for element list: " + locator.toString());
      } else {
        LOG.trace("Intentionally ignoring exception while waiting for element list: ", e);
      }
    }
    final List<WebElement> elementsFound = driver.findElements(locator);
    LOG.debug("Found {} elements using locator: '{}'", elementsFound.size(), locator);
    return elementsFound;
  }

  public List<WebElement> waitForElementList(final By locator, final boolean failOnError) {
    return waitForElementList(locator, getSettingPageLoadTimeout(), failOnError);
  }

  public List<WebElement> waitForElementList(final By locator) {
    return waitForElementList(locator, true);
  }

  /**
   * @return {@link Integer} timeout in duration as set in selenium.properties (default: 9000)
   */
  public Duration getSettingPageLoadTimeout() {
    return Duration.ofMillis(getRunParameterAsInt(SeleniumDriverWrapper.SETTINGS_PAGELOAD_WAIT));
  }

  public void dumpElementDetails(final WebElement webElement) {
    if (LOG.isTraceEnabled()) {
      LOG.trace("Element details:\n--> id: '{}'\n--> name: '{}'\n--> style: '{}'\n--> class: '{}'\n--> text: '{}'",
          webElement.getAttribute("id"),
          webElement.getTagName(),
          webElement.getAttribute("style"),
          webElement.getAttribute("class"),
          webElement.getText());
    }
  }

  /*
   * Compare window height and element Y position to determine whether the element is scrolled into view
   * If an element's position + height is greater than the height of the screen dimension then it's not
   * currently visible from the user perspective and the method returns false. Otherwise this element is
   * scrolled into view and the method returns true   
   */
  public boolean isElementScrolledIntoView(final WebElement webElement) {
    final Dimension screen = SeleniumDriverWrapper.getInstance().getDriver().manage().window().getSize();
    final int elementHeight = webElement.getRect().getHeight();
    final int elementYpos = webElement.getRect().getY();
    return screen.height > elementHeight + elementYpos;
  }

  /**
   * @return {@link Boolean} determine if verbose logging is enabled in selenium.properties
   */
  public boolean getSettingHighlight() {
    return getRunParameterAsBoolean(SeleniumDriverWrapper.SETTINGS_OBJECTS_HIGHLIGHT);
  }

  /**
   * Sets the value on the input-element with the given id.
   * TODO: get rid of implicit prefixes and test which scenarios fail
   * @param id    id of element to set
   * @param value value to set
   */
  public WebElement inputSetValue(final String id, final String value) {
    if (id.startsWith(idPrefix(""))) {
      return inputSetValue(By.id(id), value);
    } else {
      return inputSetValue(id(id), value);
    }
  }

  /**
   * Sets the value on the input-element with the given {@link By} locator.
   * @param locator {@link By} needed to locate a {@link WebElement}
   * @param value the value to set if element is found
   */
  public WebElement inputSetValue(final By locator, final String value) {
    return inputSetValue(waitForElementVisible(locator), value, true, false);
  }

  /**
   * Sets the value on the input-element with the given id.
   * @param id          id of element to set
   * @param value       value to set
   * @param checkInput  boolean to check input after set
   */
  public WebElement inputSetValue(final String id, final String value, final boolean checkInput, final boolean submitWithEnter) {
    return inputSetValue(waitForElementVisible(id(id)), value, checkInput, submitWithEnter);
  }

  /**
   * Set the given value on the input element with the given id.
   * @param {@link WebElement} to set value on
   * @param value to set
   * @param verify true if value of input must be verified after setting
   * @param submitUseEnter whether or not to use enter to submit
   */
  public WebElement inputSetValue(final WebElement webElement, final String value, final boolean verify, final boolean submitUseEnter) {
    // send a click to get focus
    buttonClick(webElement);
    try {
      // make sure the value entered overwrites data which might already be present
      webElement.sendKeys(Keys.chord(Keys.CONTROL, "a"));
      if (submitUseEnter) {
        webElement.sendKeys(value, Keys.ENTER, Keys.TAB);
      } else {
        webElement.sendKeys(value, Keys.TAB);
      }
      LOG.debug("Value of element '{}' is '{}'. Text is '{}'", getElementId(webElement), webElement.getAttribute("value"), webElement.getText());
    } catch (final Exception e) {
      LOG.warn("Caught exception while setting value on element '{}': {}", webElement.toString(), e.toString());
    }
    if (verify) {
      Assert.assertEquals(value, inputGetValue(getElementId(webElement)).trim());
    }
    return webElement;
  }

  /**
   * Checks on absence of validation pop up
   */
  public void checkNoValidation() {
    checkOnValidationMessage(false, "");
  }

  /**
   * Check for the presence of a validation pop up with given text
   * Replacing double quotes in the expected text by single quotes
   * to allow exact matching of text
   * @param validationErrorExpected boolean
   * @param validationMessage       expected validation message
   */
  public void checkOnValidationMessage(final boolean validationErrorExpected, final String validationMessage) {
    // TODO: remove the need for implicit quote replacement
    browserWaitForDrawAnimation();
    final String expectedMessage = validationMessage.replace("\"", "'");
    boolean validationErrorIsDisplayed = false;
    String actualMessage;
    if (validationErrorExpected) {
      LOG.debug("Checking for presence of validation message: '{}'", validationMessage);
    } else {
      LOG.debug("Checking for absence of validation message: '{}'", validationMessage);
    }
    validationErrorIsDisplayed = isValidationPopupShown();

    if (!validationErrorExpected && validationErrorIsDisplayed) {
      actualMessage = driver.findElement(id(TestID.DIV_VALIDATION_POPUP)).getText();
      Assert.fail("Unexpected validation message: " + actualMessage);
    } else if (validationErrorExpected && !validationErrorIsDisplayed) {
      Assert.fail("Expected validation message not present: " + expectedMessage);
    } else if (validationErrorExpected && validationErrorIsDisplayed) {
      actualMessage = driver.findElement(id(TestID.DIV_VALIDATION_POPUP)).getText();
      driver.findElement(id(TestID.DIV_VALIDATION_POPUP)).click();
      Assert.assertEquals(expectedMessage, actualMessage);
    }
    LOG.debug("Validations are fine");
  }

  /**
   * Detect any present validation error pop up and make it go away
   */
  public void cleanValidationErrorPopUps() {
    final By byXpath = xpath("//div[contains(@class, 'errorInput')]");
    buttonClick(waitForElementVisible(byXpath));
    waitForElementInvisible(byXpath);
  }

  public boolean isValidationPopupShown() {
    final Duration timeOutSeconds = Duration.ofSeconds(seleniumDriverWrapper.getRunParameterAsInt(SeleniumDriverWrapper.SETTINGS_VALIDATION_TIMEOUT));
    return waitForElementList(id(TestID.DIV_VALIDATION_POPUP), timeOutSeconds, false).size() > 0;
  }

  /**
   * Gets value of element with given id (or text for non-form elements)
   * @param id of the element to get value from
   * @return string with value or text of element
   */
  public String inputGetValue(final String id) {
    if (!id.startsWith(idPrefix(""))) {
      LOG.warn("This id will probably fail to deliver a WebElement: '{}'", id);
    }
    return inputGetValue(waitForElementVisible(By.id(id)));
  }

  /**
   * Gets value of given element (or text for non-form elements)
   * @param webElement of the element to get value from
   * @return string with value or text of element
   */
  public String inputGetValue(final WebElement webElement) {
    String value = webElement.getAttribute("value");
    if (value == null) {
      value = webElement.getText();
    }
    return value;
  }

  /**
   * Selects the value in a list box by id and visible text.
   * @param id of the list box element to select option from
   * @param text as expected
   */
  public void inputSelectListItemByText(final By id, final String text) {
    final WebElement webElement = buttonHover(waitForElementVisible(id));
    final Select selectItem = new Select(webElement);
    LOG.info("Clearing selection using element id: '{}'", id.toString());
    // clear whole selection in case of multiple options
    if (selectItem.isMultiple()) {
      selectItem.deselectAll();
    }
    // select item by text (not by value!)
    selectItem.selectByVisibleText(text);

  }

  /**
   * Selects given value in a list box {@link WebElement}.
   * @param id of the list box
   * @param value of option to select
   * @param verify if selection was successful
   */
  public void inputSelectListItem(final String id, final String value, final Boolean verify) {
    inputSelectListItem(waitForElementVisible(id(id)), value, verify);
  }

  /**
   * Selects given value in a list box @WebElement
   * @param webElement reference to list box
   * @param value of option to select
   * @param verify if selection was successful
   */
  public void inputSelectListItem(final WebElement webElement, final String value, final Boolean verify) {
    buttonHover(webElement);
    final String id = getElementId(webElement);
    final Select selectItem = new Select(webElement);
    // clear any active selection to prepare for new selection
    if (selectItem.isMultiple()) {
      selectItem.deselectAll();
      LOG.debug("Selection(s) cleared for element with id: '{}'", id);
    }
    selectItem.selectByValue(value);

    // check if selected value is correct
    if (verify) {
      if (inputGetValue(id).equals(value)) {
        LOG.info("Selection correctly set to '{}' for element with id: '{}'", value, id);
      } else {
        Assert.fail("Selection was not set to " + value + " for element with id: " + id);
      }
    }
  }

  /**
   * Hovers on given @WebElement and returns the element for chaining actions
   * @param id of the element to hover on
   * @return the @WebElement after hovering
   */
  public WebElement buttonHover(final String id) {
    return buttonHover(waitForElementVisible(id(id)));
  }

  /**
   * Hovers on given @WebElement and returns the element for chaining actions
   * @param webElement to hover on
   * @return the @WebElement after hovering
   */
  public WebElement buttonHover(final WebElement webElement) {
    objectHighlight(webElement);
    new Actions(driver).moveToElement(webElement).perform();
    return webElement;
  }

  /**
   * Clicks on a button with the given id.
   * @param id of the element to click
   * @return the @WebElement after clicking
   */
  public WebElement buttonClick(final String id) {
    return buttonClick(waitForElementVisible(id(id)));
  }

  /**
   * Clicks on a button using the given @By locator.
   * @param locator of the element to click
   * @return the @WebElement after clicking
   */
  public WebElement buttonClick(final By locator) {
    return buttonClick(waitForElementVisible(locator));
  }

  /**
   * Clicks on a @WebElement if it's ready to receive a click before time runs out
   * Returns the resulting element for chaining actions
   * @param webElement {@link WebElement}
   * @return the @WebElement after clicking
   */
  public WebElement buttonClick(final WebElement webElement) {
    final WebElement result = waitForElementClickable(buttonHover(webElement), true);
    result.click();
    return result;
  }

  /**
   * Click on a child-{@link WebElement} if found in the given parent-element.
   * @param childId the child id to search for
   * @param childTag the child tag to target (input/span etc)
   * @param id the parent id to search
   */
  public void objectClickChild(final String childId, final String childTag, final String id) {
    final WebElement parentElement = waitForElementVisible(id(id)); // getElement(parentId);
    final WebElement childElement = parentElement.findElement(By.xpath(".//" + childTag + "[contains(@id, '" + childId + "')]"));
    buttonClick(childElement);
  }

  /**
   * Selects a radio button {@link WebElement} with the given id.
   * @param id of radio-option element to select
   */
  public void inputSelectRadioButton(final String id) {
    buttonClick(id + "-" + getBrowserRadioSetStyle());
  }

  /**
   * Clicks an element of given type, in given table at given row and column.
   * @param id      tableId of table to search
   * @param row     row to click at
   * @param column  column to click at
   * @param tagName type of element (tag) to click on
   */
  public void tableClickElementInCell(final String tableId, final int row, final int column, final String tagName) {

    // get table rows and columns
    final WebElement webTable = waitForElementVisible(id(tableId));
    final List<WebElement> webTableRows = webTable.findElements(By.tagName("tr"));
    final List<WebElement> webTableColumns = webTableRows.get(row - 1).findElements(By.tagName("td"));
    final List<WebElement> webTableElements = webTableColumns.get(column - 1).findElements(By.tagName(tagName));

    // get first element of given type
    final WebElement webElementToClick = webTableElements.get(0);
    webElementToClick.click();
  }

  /**
   * Selects a sub source in the the source details emission editor overview
   * @param text  text to search for
   */
  public void sourceDetailsSelectSubItemByName(final String subSourceLabel) {

    boolean subSourceSelected = false;

    // get all sources from the overview and find given source by text
    final List<WebElement> overviewSubSources = driver.findElements(By.xpath("//div[contains(@id, '" + TestID.DIVTABLE_EDITABLE + "-"
        + TestID.TABLE + "')]"));

    for (final WebElement subSource : overviewSubSources) {

      final WebElement subSourceDescription = subSource.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));

      if (subSourceLabel.equals(subSourceDescription.getText().trim())) {
        subSourceDescription.click();
        subSourceSelected = true;
        LOG.info("Sub Source selected: '{}'", subSourceLabel);
        browserWaitForDrawAnimation();

        if (!waitForElementVisible(id(TestID.BUTTON_EDITABLETABLE_EDIT)).isEnabled()) {
          subSourceDescription.click();
        }
        break;
      }
    }
    Assert.assertTrue("Sub Source NOT available: " + subSourceLabel, subSourceSelected);
  }

  /**
   * Checks the emission of a sub source in the the source details emission editor overview
   * @param text  text to search for
   */
  public void sourceDetailsSubItemCheckEmission(final String subSourceLabel, final String subSourceExpectedEmission) {

    boolean subSourceChecked = false;

    // get all sources from the overview and find given source by text
    final List<WebElement> overviewSubSources = driver.findElements(By.xpath("//div[contains(@id, '" + TestID.DIVTABLE_EDITABLE + "-"
        + TestID.TABLE + "')]"));

    for (final WebElement subSource : overviewSubSources) {

      final WebElement subSourceDescription = subSource.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));
      final WebElement subSourceEmissionContainer = subSource.findElement(By.xpath(".//div[contains(@id, '" + TestID.EMISSION + "')]"));
      final String subSourceEmission = subSourceEmissionContainer.getText();
      final String expectedValue = subSourceExpectedEmission + getRunParameterAsString(SeleniumDriverWrapper.UNITS_SOURCE_EMISSION);

      if (subSourceLabel.equals(subSourceDescription.getText().trim())) {
        if (subSourceEmission.equals(expectedValue)) {
          Assert.assertEquals("Check sub source emission:'" + subSourceLabel + "'", expectedValue, subSourceEmission);
          LOG.info("Sub Source emission OK:'" + subSourceLabel + "' [" + subSourceEmission + "]");
        } else {
          Assert.assertEquals("Check sub source emission:'" + subSourceLabel + "'", expectedValue, subSourceEmission);
          LOG.info("Sub Source emission NOK:'" + subSourceLabel + "' [" + subSourceEmission + "]");
        }
        subSourceChecked = true;
        break;
      }
    }
    Assert.assertTrue("Sub Source NOT available: " + subSourceLabel, subSourceChecked);
  }

  /**
   * Selects a source in the source overview table
   * @param text  text to search for
   */
  public void overviewSelectSourceByName(final String sourceLabel) {

    boolean sourceSelected = false;
    waitForElementVisible(id(TestID.BUTTON_SOURCE_EDIT));

    // get all sources from the overview and find given source by text
    final List<WebElement> overviewSources = driver.findElements(By.xpath("//div[contains(@id, '" + TestID.DIVTABLE_SOURCE + "-"
        + TestID.TABLE + "')]"));

    for (final WebElement source : overviewSources) {

      final WebElement sourceDescription = source.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));

      if (sourceLabel.equals(sourceDescription.getText().trim())) {
        mouseMoveToElement(sourceDescription);
        sourceDescription.click();
        sourceSelected = true;
        LOG.info("Source selected: '{}'", sourceLabel);
        browserWaitForDrawAnimation();

        if (!waitForElementVisible(id(TestID.BUTTON_SOURCE_EDIT)).isEnabled()) {
          sourceDescription.click();
          mouseMoveToElement(sourceDescription);
        }

        break;
      }
    }
    Assert.assertTrue("Source NOT available: " + sourceLabel, sourceSelected);
  }

  /**
   * Clicks ID-tag of source in the source overview table
   * @param text  text to search for
   */
  public void overviewClickIDSourceByName(final String sourceLabel) {

    boolean sourceSelected = false;
    waitForElementVisible(id(TestID.BUTTON_SOURCE_EDIT));

    // get all sources from the overview and find given source by text
    final List<WebElement> overviewSources = driver.findElements(By.xpath("//div[contains(@id, '" + TestID.DIVTABLE_SOURCE + "-"
        + TestID.TABLE + "')]"));

    for (final WebElement source : overviewSources) {

      final WebElement sourceDescription = source.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));
      final WebElement sourceIDtag = source.findElement(By.xpath(".//div[contains(@id, '" + TestID.ID + "')]"));

      if (sourceLabel.equals(sourceDescription.getText().trim())) {
        sourceIDtag.click();
        sourceSelected = true;
        LOG.info("Source clicked on ID-tag: '{}'", sourceLabel);
        browserWaitForDrawAnimation();

        if (!waitForElementVisible(id(TestID.BUTTON_SOURCE_EDIT)).isEnabled()) {
          sourceIDtag.click();
        }

        break;
      }
    }
    Assert.assertTrue("Source NOT available: " + sourceLabel, sourceSelected);
  }

  /**
   * Selects a calculation point in the overview table
   * @param text  text to search for
   */
  public void overviewSelectCalcpointByName(final String calcPointLabel) {

    boolean calcPointSelected = false;
    waitForElementVisible(id(TestID.BUTTON_CP_EDIT));

    // get list of all calculation point elements from overview
    final String xpathCalcPoints = "//div[contains(@id, '" + TestID.DIVTABLE_CALCULATION_POINT + "-" + TestID.TABLE + "')]";
    final List<WebElement> overviewCalcpoints = driver.findElements(By.xpath(xpathCalcPoints));

    // test each calculation point for matching label and fail if not found
    for (final WebElement calcPoint : overviewCalcpoints) {
      final WebElement calcPointDescription = calcPoint.findElement(By.xpath(".//div[contains(@id, '" + TestID.LABEL + "')]"));
      LOG.debug("Calculation point found with label: '{}'", calcPointDescription.getText());
      if (calcPointLabel.equals(calcPointDescription.getText())) {
        calcPointDescription.click();
        calcPointSelected = true;
        LOG.info("Calculation point selected: '{}'", calcPointLabel);
        if (!waitForElementVisible(id(TestID.BUTTON_CP_EDIT)).isEnabled()) {
          calcPointDescription.click();
        }
        break;
      }
    }
    Assert.assertTrue("Calculation point NOT available: " + calcPointLabel, calcPointSelected);
  }

  /**
   * Clicks and hold an element
   * @param id  id of the element to click
   */
  public void mouseClickAndHoldElement(final String id) {
    final WebElement webElement = waitForElementVisible(id(id)); // getElement(id);
    new Actions(driver).clickAndHold(webElement).perform();
  }

  /**
   * Move mouse by offset
   * @param xOffset
   * @param yOffset
   */
  public void mouseMoveByOffset(final int xOffset, final int yOffset) {
    new Actions(driver).moveByOffset(xOffset, yOffset).perform();
  }

  /**
   * Release the mousebutton
   * @param xOffset
   * @param yOffset
   */
  public void mouseRelease() {
    new Actions(driver).release().perform();
  }

  /**
   * Moves MousePointer to a given element.
   * @param id  id of the element to move to
   */
  public void mouseMoveToElement(final String id) {
    final WebElement webElement = waitForElementVisible(id(id));
    new Actions(driver).moveToElement(webElement).perform();
  }

  /**
   * Moves MousePointer to a given element {@link WebElement}.
   * @param {@link WebElement} element to move to
   */
  public void mouseMoveToElement(final WebElement webElement) {
    new Actions(driver).moveToElement(webElement).perform();
  }

  /**
   * Moves MousePointer to a given element {@link WebElement}.
   * @param {@link WebElement} element to move to
   * @param xOffset
   *          Offset from the top-left corner. A negative value means
   *          coordinates right from the element
   * @param yOffset
   *          Offset from the top-left corner. A negative value means
   *          coordinates above the element
   */
  public void mouseMoveToElement(final WebElement webElement, final int xOffset, final int yOffset) {
    new Actions(driver).moveToElement(webElement, xOffset, yOffset).perform();
  }

  /**
   * Click Mouse on a given element.
   * @param {@link WebElement} to click on
   */
  public void mouseClickOnElement(final WebElement webElement) {
    new Actions(driver).click(webElement).perform();
  }

  /**
   * Click Mouse at the current position.
   */
  public void mouseClickCurrentPosition() {
    new Actions(driver).click().perform();
  }

  /**
   * DoubleClick Mouse on a given element.
   * @param {@link WebElement} double click on
   */
  public void mouseDoubleClickOnElement(final WebElement webElement) {
    final Actions action = new Actions(driver);

    // Chrome browser requires a slightly different click sequence to make this work
    if (isChrome()) {
      action.click(webElement);
    }
    action.doubleClick(webElement);
    action.perform();
  }

  /**
   * Enumeration of coordinate x, y
   */
  public enum CoordinateAttribute {
    x, y;
  }

  /**
   * Enumeration of all valid SectorGroups. Also used in
   * {@link FunctionalTestBase#getRandomSectorGroup()}
   */
  public enum SectorGroups {
    ENERGY, AGRICULTURE, LIVE_AND_WORK, INDUSTRY, MOBILE_EQUIPMENT, RAIL_TRANSPORTATION, AVIATION, ROAD_TRANSPORTATION, OTHER,
    // those below are not used in randomizer, if this changes: also change
    // index in {@link FunctionalTestBase#getRandomSectorGroup()}.
    SHIPPING, PLAN, EMPTY;

    public static SectorGroups getUnsafe(final String value) {
      for (final SectorGroups item : values()) {
        if (item.name().equalsIgnoreCase(value)) {
          return item;
        }
      }

      throw new IllegalArgumentException("SectorGroups with  value '" + value + "' does not exist.");
    }
  }

  /**
   * Enumeration of all valid situation tabs.
   */
  public enum SituationTabs {
    TAB_1, TAB_2, COMPARE;
  }

  /**
   * Enumeration of all valid mouse clicks.
   */
  public enum MapMouseClicks {
    SINGLE,
    DOUBLE; // Issues circumvented by workaround 
  }

  /**
   * Enumeration of calculation result types.
   */
  public enum CalculationResultTableTypes {
    TOTAL_DEPOSITION, MAXIMUM_INCREASE, MAXIMUM_DEPOSITION, AVERAGE_DEPOSITION, PERCENTAGE_KDW, DEPOSITION_SPACE;
  }

  /**
   * Returns sector option-value used to select list items.
   * @param sectorGroep {@link SectorGroups} to get value for
   * @return sector option value (to use with select boxes)
   */
  public String returnSectorGroepValue(final SectorGroups sectorGroep) {
    return sectorGroep.name();
  }

  /**
   * Returns random sector group.
   */
  public SectorGroups getRandomSectorGroup() {
    final int randomNextInt = 6;
    final SectorGroups randomValue = SectorGroups.values()[new Random().nextInt(randomNextInt)];
    return randomValue;
  }

  /**
   * Returns default sector in sector group, used for randomization of sectors.
   * @param sectorgroup {@link SectorGroups} to get default sector
   */
  public String getSectorGroupDefaultSector(final SectorGroups sectorgroup) {

    final HashMap<SectorGroups, String> sectorsInSectorGroup = new HashMap<>();
    sectorsInSectorGroup.clear();

    sectorsInSectorGroup.put(SectorGroups.LIVE_AND_WORK, "8640");
    sectorsInSectorGroup.put(SectorGroups.RAIL_TRANSPORTATION, "3720");
    sectorsInSectorGroup.put(SectorGroups.ENERGY, "");
    sectorsInSectorGroup.put(SectorGroups.AGRICULTURE, "4130");
    sectorsInSectorGroup.put(SectorGroups.INDUSTRY, "1500");
    sectorsInSectorGroup.put(SectorGroups.OTHER, "");

    final String defaultSector = sectorsInSectorGroup.get(sectorgroup);
    return defaultSector;
  }

  /**
   * Returns random (valid) number for given coordinate (x or y).
   * @param attribute {@link coordinateAttributes} to get "x" or "y"
   */
  public String getRandomCoordinate(final CoordinateAttribute attribute) {

    final int xMinBound = 104300;
    final int xMaxBound = 216000;
    final int yMinBound = 392000;
    final int yMaxBound = 579000;
    int randomCoordinate = 0;

    final Random randomNumber = new Random();

    switch (attribute) {
    case x:
      randomCoordinate = randomNumber.nextInt(xMaxBound - xMinBound) + xMinBound;
      break;
    case y:
      randomCoordinate = randomNumber.nextInt(yMaxBound - yMinBound) + yMinBound;
      break;
    default:
      randomCoordinate = randomNumber.nextInt(xMaxBound - xMinBound) + xMinBound;
      break;
    }

    return String.valueOf(randomCoordinate);
  }

  /**
   * Expands the expected search suggestion in the suggestions pop up
   * @param expectedSuggestion
   */
  public void expandSearchSuggestion(final String expectedSuggestion) {
    boolean suggestionExpanded = false;

    for (final WebElement suggestion : getSearchSuggestions()) {
      if (getSearchSuggestionItem(suggestion).getText().trim().equals(expectedSuggestion)) {
        LOG.info("Expanding search suggestion: '{}'", expectedSuggestion);
        buttonClick(suggestion.findElement(By.xpath(".//div[contains(@id, 'buttonPlusMinus')]")));
        suggestionExpanded = true;
        break;
      }
    }
    if (!suggestionExpanded) {
      Assert.fail("Search suggestion NOT available: " + expectedSuggestion);
    }
  }

  /**
   * Get a {@link List} of {@link WebElement}s with all search suggestions
   * @return a list of all available suggestion elements
   */
  private List<WebElement> getSearchSuggestions() {
    final String xpathSuggestions = ".//div[contains(@id, '-" + TestID.DIV_SEARCHSUGGESTION + "-')]";
    return waitForElementList(By.xpath(xpathSuggestions));
  }

  /**
   * Find a specific suggestion element from the given {@link WebElement}
   * The returned element contains the text as shown in the UI
   * @param element
   * @return the {@link WebElement} containing suggestion text
   */
  private WebElement getSearchSuggestionItem(final WebElement element) {
    return element.findElement(By.xpath(".//div[contains(@class, 'Label')]"));
  }

  /**
   * Compare the value of given {@link TestID} to expected value
   * @param id of the element to check
   * @param expectedValue to compare with actual value
   */
  public void inputCheckValue(final String id, final String expectedValue) {
    String actualValue = inputGetValue(id);
    LOG.debug("Checking for expected value '{}' on element id: '{}'", expectedValue, id);
    if (actualValue == null) {
      // only form-elements have a value property, settle for visible text otherwise
      actualValue = waitForElementVisible(id(id)).getText();
    }
    Assert.assertEquals("Unexpected value found on id: " + id, expectedValue, actualValue);
  }

  /**
   * Check whether an object with the given id is displayed
   * @param id of the object to check
   * @return boolean true if displayed or false on invisible
   */
  public boolean isElementDisplayed(final String id) {
    try {
      driver.findElement(id(id)).getLocation();
      LOG.info("check is displayed: " + driver.findElement(id(id)).getText());
    } catch (final Exception e) {
      return false;
    }
    return true;
  }

  /*
   * Check whether an object with the given id is visible
   * @return boolean true if visible or false on invisible
   */
  public boolean isElementVisible(final String id) {
    return driver.findElement(id(id)).isDisplayed();
  }

  /**
   * Check whether an object with the given id and expected value is enabled
   * @param id of the object to check
   * @param expectedValue expected value: object is enabled (true) or not (false)
   */
  public void objectIsEnabled(final String id, final boolean expectedValue) {
    synchronized (driver) {
      browserWaitForDrawAnimation();
    }

    boolean objectIsEnabled = true;

    try {
      final WebElement webElement = waitForElementVisible(id(id)); // getElement(id);
      objectIsEnabled = webElement.isEnabled();
    } catch (final Exception e) {
      objectIsEnabled = false;
    }

    LOG.debug("checked value for object '" + id + "' is enabled. Expected: '" + expectedValue + "', Actual: '"
        + objectIsEnabled + "'");

    if (expectedValue != objectIsEnabled) {
      Assert.assertEquals("checked value for object is enabled: " + id, expectedValue, objectIsEnabled);
    }

  }

  /**
   * Highlights the given {@link WebElement}-object in the GUI.
   * @param id  {@link WebElement} to highlight
   */
  public void objectHighlight(final WebElement element) {
    final String cssTransitionJs = "transition: transform 0.1s;";
    final String cssTransformJs = "transform: scale(1.2,1.2); border: solid 3px yellow;";
    final String cssResetJs = "transform: scale(1,1);";
    if (getSettingHighlight()) {
      executeJsStyle(element, cssTransitionJs);
      executeJsStyle(element, cssTransformJs);
      browserWait(seleniumDriverWrapper.getSettingSyncSleep());
      executeJsStyle(element, cssResetJs);
    }
  }

  private void executeJsStyle(final WebElement element, final String script) {
    final JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, script);
  }

  /**
   * Finds an element by Xpath returns it as a {@link WebElement} object.
   * Returns NULL if element-id is not set or element can not be found
   * @param xpath   xpath of the element
   * @return {@link WebElement}
   */
  public WebElement getElementByXPath(final String xpath) {
    try {
      return driver.findElement(By.xpath(xpath));
    } catch (final Exception e) {
      LOG.info("Element not found by xpath: '{}'", xpath);
      return null;
    }
  }

  /**
   * Finds a List of elements {@link WebElement} by Xpath returns it as a {@link WebElement} object.
   * Returns NULL if element-id is not set or element can not be found
   * @param xpath xpath of the element
   * @return List of WebElements
   */
  public List<WebElement> getElementListByXPath(final String xpath) {
    try {
      LOG.info("Locating element by xpath: '{}'", xpath);
      return driver.findElements(By.xpath(xpath));
    } catch (final Exception e) {
      LOG.info("Caught exception: {}", e.toString());
      return null;
    }
  }

  /**
   * Gets run parameters from selenium.properties as a String
   * @param runParameter string run parameter to get
   * @return value from selenium.properties as a String
   */
  public String getRunParameterAsString(final String runParameter) {
    return seleniumDriverWrapper.getRunParameterAsString(runParameter);
  }

  /**
   * Gets run parameters from selenium.properties as an Integer
   * @param runParameter string run parameter to get
   * @return value from selenium.properties as an Integer
   */
  public int getRunParameterAsInt(final String runParameter) {
    return seleniumDriverWrapper.getRunParameterAsInt(runParameter);
  }

  /**
   * Gets run parameters from selenium.properties as a Boolean
   * @param runParameter string run parameter to get
   * @return value from selenium.properties as a Boolean
   */
  public boolean getRunParameterAsBoolean(final String runParameter) {
    return seleniumDriverWrapper.getRunParameterAsBoolean(runParameter);
  }

  /**
   * Checks a specific button for specific content
   * @param id unique identifier
   * @param text value to match
   * @return boolean
   */
  public boolean buttonTextContains(final String id, final String text) {
    return buttonHover(id).getText().contains(text);
  }

  /**
   * Prevent an unexpected notification panel to block clicks on underlying elements
   */
  public void checkAndHandleUnexpectedNotifications() {
    try {
      // check if delete all button is shown (notifications panel is opened)
      waitForElementInvisible(id(TestIDRegister.BUTTON_DELETE_ALL_NOTIFICATIONS), Duration.ofSeconds(3));
    } catch (final Exception e) {
      // visible panel detected, click button and make sure it's gone
      LOG.debug("Notification panel is open and blocking elements: {}", e);
      buttonClick(TestIDRegister.BUTTON_DELETE_ALL_NOTIFICATIONS);
      try {
        waitForElementInvisible(id(TestIDRegister.BUTTON_DELETE_ALL_NOTIFICATIONS), Duration.ofSeconds(3));
      } catch (final Exception f) {
        Assert.fail("Notification panel messed up the test flow: " + f);
      }
    }
  }

  /**
   * Where Selenium fails to click an element {@link JavascriptExecutor} is our last resort.
   * @param webElement
   */
  public void buttonClickJs(final WebElement webElement) {
    final JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].click();", webElement);
  }

  /**
   * Replace whitespace by underscores to return valid debug id's
   * @param debugIdPart
   * @return String valid debug id 
   */
  private String normalizeDebugIdPart(final String debugIdPart) {
    return debugIdPart.replaceAll(" ", "_");
  }

}
