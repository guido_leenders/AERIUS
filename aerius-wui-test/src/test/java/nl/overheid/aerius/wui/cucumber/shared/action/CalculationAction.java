/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for executing a calculation
 * from the "Calculation Options" page.
 */
public class CalculationAction {

  /**
   * Enumeration of all usable types of calculation with a certain scope.
   */
  public enum CalculationScope {
    // This type of calculation only calculates within Nature Areas
    NATURE_AREAS,
    // This type of calculation only calculates within a radius around sources
    RADIUS,
    // This type of calculation only calculates custom defined calculation points
    CALCULATION_POINTS;

    public static CalculationScope getUnsafe(final String value) {
      for (final CalculationScope item : values()) {
        if (item.name().equalsIgnoreCase(value)) {
          return item;
        }
      }

      throw new IllegalArgumentException("Calculation Scope with  value '" + value + "' does not exist.");
    }

  }

  private final FunctionalTestBase base;

  public CalculationAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Sets the calculation scope.
   * @param calculationScope {@link CalculationScope} calculation scope to use
   */
  public void setCalculationScope(final CalculationScope calculationScope) {
    switch (calculationScope) {

    case NATURE_AREAS:
      base.inputSelectRadioButton(TestID.RADIO_CALC_OPT_N2000);
      break;
    case RADIUS:
      base.inputSelectRadioButton(TestID.RADIO_CALC_OPT_RADIUS);
      break;
    case CALCULATION_POINTS:
      base.inputSelectRadioButton(TestID.RADIO_CALC_OPT_CALCPOINTS);
      break;
    default:
      base.inputSelectRadioButton(TestID.RADIO_CALC_OPT_N2000);
      break;
    }
  }

  /**
   * Set the calculation option max range (the radius of the calculation)
   * @param maxRange the range to calculate for
   */
  public void setCalculationMaxRange(final String maxRange) {
    base.inputSetValue(TestID.INPUT_CALC_OPT_MAXRANGE, maxRange);
  }

  /**
   * Starts the calculation by clicking the "Calculate" button.
   */
  public void clickStartCalculationButton() {
    base.buttonClick(TestID.BUTTON_CALCULATE);
  }

  /**
   * Clicks the stop calculation button.
   */
  public void clickStopCalculationButton() {
    base.buttonClick(TestID.BUTTON_CALCULATE);
  }

  /**
   * Enable the priority project option to calculate with limited radius
   * TODO: Obsolete calculation option method for @AER-1718.
   */
  public void setCalculationOptionEnabled() {
    final WebElement checkBox = base.buttonHover(TestID.CHECKBOX_PERMIT_CALCULATION_RADIUS);
    if (checkBox.isSelected()) {
      Assert.fail("Priority radius checkbox is already ticked!");
    }
    checkBox.click();
    base.switchFocus();
  }

  /**
   * Select the priority project calculation type
   * @param optionType
   * TODO: Obsolete Calculation option method for AER-1718
   */
  public void setCalculationOptionType(String optionType) {
    final String xpath = "//select[contains(@title, 'Prioritair project')]";
    final WebElement webElement = base.waitForElementVisible(By.xpath(xpath));
    new Select(webElement).selectByVisibleText(optionType);
  }

}
