/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.StartUpAction;

import cucumber.api.java.en.When;

public class StartupStepDef {

  private final FunctionalTestBase base;
  private final StartUpAction startupPage;

  public StartupStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    startupPage = new StartUpAction(base);
  }

  @When("^the startup is set to import a file$")
  public void when_startup_import() {
    startupPage.startImportFile();
  }

  @When("^the startup is set to input sources$")
  public void when_startup_input_sources() {
    startupPage.startInputSource();
  }

}
