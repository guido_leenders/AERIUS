/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import java.time.Duration;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for determining calculation points automatically
 * from the "Determine Calculation points Automatically" pop up.
 */
public class CalculationPointDetermineAction {

  private final FunctionalTestBase base;

  public CalculationPointDetermineAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Opens the pop up for determining points automatically.
   */
  public void clickDetermineCalcPointsButton() {
    base.buttonClick(TestID.BUTTON_DETERMINE_CALCPOINTS);
  }

  /**
   * Clicks the calculate button in the pop up for determining points automatically.
   */
   public void clickCalculateCalcPointsButton() {
     base.buttonClick(TestID.DETERMINE_CALCPOINTS_NOTSOUSELESSCALCBUTTON);
   }

   /**
   * Clicks the confirm button in the pop up for determining points automatically.
   */
  public void clickConfirmButton() {
    base.objectIsEnabled(TestID.BUTTON_DIALOG_CONFIRM, true);
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
    base.waitForElementVisible(base.id(TestID.BUTTON_CP_ADD));
  }

  /**
  * Clicks the cancel button in the pop up for determining points automatically.
  */
  public void clickCancelButton() {
    base.buttonClick(TestID.BUTTON_DIALOG_CANCEL);
  }

  /**
  * Sets the boundary input field in the pop up for determining points automatically.
  * @param determineCalcpointsBoundary String boundary/distance
  */
  public void setBoundary(final String determineCalcpointsBoundary) {
    base.waitForElementVisible(base.id(TestID.DETERMINE_CALCPOINTS_NOTSOUSELESSCALCBUTTON));
    base.inputSetValue(TestID.DETERMINE_CALCPOINTS_BOUNDARY, determineCalcpointsBoundary);
  }

   /**
   * Adds calculation points using the functionality for determining and adding calculation points automatically
   * Precondition: Application displays form to enter new calculation point.
   * Postcondition: Application added calculation points and displays source overview
   * @param determineCalcpointsBoundary String boundary/distance
   */
  public void determineCalculationPointsAutomatically(final String determineCalcpointsBoundary) {
    //open pop up, sync and fill
    clickDetermineCalcPointsButton();
    base.objectIsEnabled(TestID.BUTTON_DIALOG_CONFIRM, false);
    setBoundary(determineCalcpointsBoundary);
    clickCalculateCalcPointsButton();
    clickConfirmButton();
    base.browserWait(Duration.ofSeconds(2));
  }

}
