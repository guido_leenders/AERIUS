/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import java.time.Duration;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.MapMouseClicks;

/**
 * This class contains all the methods that apply for handling the actions on the map.
 */
public class MapAction {
  private static final Logger LOG = LoggerFactory.getLogger(MapAction.class);

  private final FunctionalTestBase base;

  public MapAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Enumeration of possible actions to perform on the map.
   */
  public enum MapPanAction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    CLICK;
  }

  /**
   * Clicks on the map at the current location of the MousePointer
   */
  public void clickOnMap() {
    base.buttonClick(base.xpath("//div[contains(@class, 'olMap')]"));
  }

  /**
   * Clicks on the map at the current location of the MousePointer
   */
  public void selectPointMapCenter() {
    base.buttonClick(base.xpath("//div[contains(@id, 'Map_8_events')]"));
  }

  /**
   * Clicks on the map center
   */
  public void clickOnMapCenter() {
    clickOnMapCenter(MapMouseClicks.SINGLE);
  }

  /**
   * Clicks on the map center
   * @param mousClickType type of mouseclick {@link MapMouseClicks}
   */
  public void clickOnMapCenter(final MapMouseClicks mousClickType) {

    switch (mousClickType) {
    case SINGLE:
      base.mouseClickOnElement(base.getElementByXPath("//body"));
      base.buttonClick(base.getElementByXPath("//div[@id='gwt-debug-map']"));
      break;
    case DOUBLE:
      base.mouseClickOnElement(base.getElementByXPath("//body"));
      base.mouseDoubleClickOnElement(base.getElementByXPath("//div[@id='gwt-debug-map']"));
      break;
    default:
      LOG.error("Clicking on the map should never reach the default switch case!");
      break;
    }
  }

  /**
   * Draws a line on the map given a String of x-y coordinates separated by spaces.
   * Precondition: map must be in draw line mode.
   * PostCondition: line is drawn on the map.
   *
   * @param coordinates list of x-y coordinates
   */
  public void clickLineOnMap(final String coordinates) {
    final WebElement mapLayer = base.getElementByXPath("//div[contains(@class, 'olMap')]");
    base.mouseMoveToElement(mapLayer);
    final String[] coArray = coordinates.replaceAll("  +", " ").split(" ");
    for (int i = 0; i < coArray.length; i += 2) {
      (new SearchAction(base)).searchCoordinatesXy(coArray[i], coArray[i + 1]);
      base.browserWait(Duration.ofSeconds(5));

      if (i == coArray.length - 2) {
        clickOnMapCenter(MapMouseClicks.DOUBLE);
        LOG.info("Double click mouse on map at x:" +  coArray[i] + ",y:" + coArray[i + 1]);
      } else {
        clickOnMapCenter(MapMouseClicks.SINGLE);
        LOG.info("Single click mouse on map at x:" +  coArray[i] + ",y:" + coArray[i + 1]);
      }
    }
  }

  /**
   * Clicks on the center of map while panning in between.
   * Can be used for drawing line-sources of surface-sources.
   * @param panClickSequence {@link MapPanAction} array of actions on map (up, down, left, right, click)
   */
  public void panClickSequence(final MapPanAction[] panActions) {

    final WebElement mapLayer = base.getElementByXPath("//div[contains(@class, 'olMap')]");

    //loop array with panActions
    for (final MapPanAction action : panActions) {

      switch (action) {
      case UP: panUp(); break;
      case DOWN: panDown(); break;
      case LEFT: panLeft(); break;
      case RIGHT: panRight(); break;
      case CLICK: clickOnMapCenter(); break;
      default: clickOnMapCenter(); break;
      }

    }
    base.mouseDoubleClickOnElement(mapLayer);
  }

  /**
   * Pushes zoom-out button.
   */
  public void clickZoomOut() {
    base.buttonClick(TestID.BUTTON_ZOOM_OUT);
  }

  /**
   * Pushes zoom-in button.
   */
  public void clickZoomIn() {
    base.buttonClick(TestID.BUTTON_ZOOM_IN);
  }

  /**
   * Pans the map up.
   */
  public void panUp() {
    final WebElement panButton = base.buttonHover(TestID.BUTTON_PAN);
    base.mouseMoveToElement(panButton, 36, 10);
    base.mouseClickCurrentPosition();
  }

  /**
   * Pans the map down.
   */
  public void panDown() {
    final WebElement panButton = base.buttonHover(TestID.BUTTON_PAN);
    base.mouseMoveToElement(panButton, 36, 63);
    base.mouseClickCurrentPosition();
  }

  /**
   * Pans the map left.
   */
  public void panLeft() {
    final WebElement panButton = base.buttonHover(TestID.BUTTON_PAN);
    base.mouseMoveToElement(panButton, 10, 36);
    base.mouseClickCurrentPosition();
  }

  /**
   * Pans the map right.
   */
  public void panRight() {
    final WebElement panButton = base.buttonHover(TestID.BUTTON_PAN);
    base.mouseMoveToElement(panButton, 63, 36);
    base.mouseClickCurrentPosition();
  }

}
