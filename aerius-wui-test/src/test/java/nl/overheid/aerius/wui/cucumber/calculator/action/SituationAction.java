/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.SituationTabs;

/**
 * This class contains all the methods that apply for handling the situation-tabs.
 */
public class SituationAction {

  private final FunctionalTestBase base;

  public SituationAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Create a new situation tab (make alternate situation).
   * Precondition: Application displays source overview of situation 1.
   * Postcondition: Application created alternative (situation 2).
   */
  public void createAlternateSituation() {
    base.buttonClick(TestID.TAB_NEW);
  }

  /**
   * Deletes a situation tab.
   * Precondition: Application displays source overview.
   * Postcondition: Application deleted situation.
   * @param situationTab {@link situationTab} tab to delete
   */
  public void deleteSituation(final SituationTabs situationTab) {
    switchToSituation(situationTab);
    clickSituationTab(situationTab);
    base.buttonClick(TestID.TAB_SIT_DELETE);
  }

  /**
   * Renames a situation tab.
   * Precondition: Application displays source overview.
   * Postcondition: Application renamed situation.
   * @param situationTab {@link situationTab} tab to rename
   * @param value value of the new name
   */
  public void renameSituation(final SituationTabs situationTab, final String value) {
    switchToSituation(situationTab);
    clickSituationTab(situationTab);
    base.inputSetValue(TestID.TAB_SIT_NAMEFIELD, value);
    base.buttonClick(TestID.TAB_SIT_OK);
  }

  public void clickSituationTab(final SituationTabs situationTab) {
    switch (situationTab) {
    case TAB_1: base.buttonClick(TestID.TAB_SIT_1); break;
    case TAB_2: base.buttonClick(TestID.TAB_SIT_2); break;
    case COMPARE: base.buttonClick(TestID.TAB_COMPARE); break;
    default: base.buttonClick(TestID.TAB_SIT_1); break;
    }
  }

  /**
   * Switches to the given situation.
   * Precondition: Application displays situation.
   * Postcondition: Application displays situation.
   * @param situationTab {@link situationTab} tab to switch to
   */
  public void switchToSituation(final SituationTabs situationTab) {
    clickSituationTab(situationTab);
    //close possible tab-popup
    if (base.isElementDisplayed(TestID.TAB_SIT_OK)) {
      base.buttonClick(TestID.TAB_SIT_OK);
    }
  }

  /**
   * Switches the situation tabs in position.
   * Precondition: Application displays source overview.
   * Postcondition: Application switch situations.
   */
  public void switchSituations() {
    base.buttonClick(TestID.TAB_SWITCH);
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
  }

  /**
   * Checks if all the tabs are visible
   * Precondition: Menu item Emission sources or Results must be selected
   */
  public void checkIfAllTabItemsVisible() {
	  base.isElementDisplayed(TestID.TAB_SIT_1);
	  base.isElementDisplayed(TestID.TAB_SIT_2);
	  base.isElementDisplayed(TestID.TAB_COMPARE);
  }

}
