/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.CalculationResultTableTypes;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.ResultsAction;

public class ResultsStepDef {

  private final FunctionalTestBase base;
  private final ResultsAction results;

  public ResultsStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    results = new ResultsAction(base);
  }

  @When("^the results table is set to display as Total Deposition$")
  public void when_results_table_show_total() {
    results.selectResultsTableType(CalculationResultTableTypes.TOTAL_DEPOSITION);
  }

  @When("^the results table is set to display as Maximum deposition$")
  public void when_results_table_show_maximum() {
    results.selectResultsTableType(CalculationResultTableTypes.MAXIMUM_DEPOSITION);
  }

  @When("^the results table is set to display as Deposition space$")
  public void when_results_table_show_deposition_space() {
    results.selectResultsTableType(CalculationResultTableTypes.DEPOSITION_SPACE);
  }

  @When("^the results table is set to display as Maximum increase$")
  public void when_results_table_show_maximum_increase() {
    results.selectResultsTableType(CalculationResultTableTypes.MAXIMUM_INCREASE);
  }

  @When("^the results table is set to display as Average$")
  public void when_results_table_show_average() {
    results.selectResultsTableType(CalculationResultTableTypes.AVERAGE_DEPOSITION);
  }

  @When("^the results table is set to display as Percentage$")
  public void when_results_table_show_percentage() {
    results.selectResultsTableType(CalculationResultTableTypes.PERCENTAGE_KDW);
  }

  @When("^the results table is set to show the area '(.+)'$")
  public void when_results_table_show_area(final String areaName) {
    results.selectResultsTableArea(areaName);
  }

  @When("^the results filter is set to show the area '(.+)'$")
  public void when_results_filter_show_area(final String areaName) {
    base.inputSelectListItemByText(base.id(TestID.LIST_NATURA), areaName);
  }

  @When("^the results filter is set to show the habitat '(.+)'$")
  public void when_results_filter_show_habitat(final String habitName) {
    base.inputSelectListItemByText(base.id(TestID.LIST_HABITAT), habitName);
  }

  @When("^the results filter lower sliderknob is moved down (\\d+) bars$")
  public void when_results_filter_lowerknob_down(final int numberOfBarsToMove) {
    results.moveFilterSliderBarDownInBars(TestID.FILTER_UPPER_KNOB, numberOfBarsToMove);
  }

  @When("^the results filter lower sliderknob is moved up (\\d+) bars$")
  public void when_results_filter_lowerknob_up(final int numberOfBarsToMove) {
    results.moveFilterSliderBarUpInBars(TestID.FILTER_UPPER_KNOB, numberOfBarsToMove);
  }

  @When("^the results filter upper sliderknob is moved down (\\d+) bars$")
  public void when_results_filter_upperknob_down(final int numberOfBarsToMove) {
    results.moveFilterSliderBarDownInBars(TestID.FILTER_LOWER_KNOB, numberOfBarsToMove);
  }

  @When("^the results filter upper sliderknob is moved up (\\d+) bars$")
  public void when_results_filter_upperknob_up(final int numberOfBarsToMove) {
    results.moveFilterSliderBarUpInBars(TestID.FILTER_LOWER_KNOB, numberOfBarsToMove);
  }

  @Then("^the result for Total Deposition in HabitatArea '(.+)' is '(.+)'$")
  public void then_results_total_deposition_is(final String habitatAreaCode, final String expectedValue) {
    results.checkDepositionInResultsTableByHT(habitatAreaCode, expectedValue, "");
  }

  @Then("^the result for Maximum in HabitatArea '(.+)' is '(.+)'$")
  public void then_results_maximum_deposition_is(final String habitatAreaCode, final String expectedValue) {
    results.checkDepositionInResultsTableByHT(habitatAreaCode, expectedValue, "");
  }

  @Then("^the result for Average in HabitatArea '(.+)' is '(.+)'$")
  public void then_results_average_deposition_is(final String habitatAreaCode, final String expectedValue) {
    results.checkDepositionInResultsTableByHT(habitatAreaCode, expectedValue, "");
  }

  @Then("^the result for Percentage in HabitatArea '(.+)' is '(.+)'$")
  public void then_results_percentage_deposition_is(final String habitatAreaCode, final String expectedValue) {
    results.checkDepositionInResultsTableByHT(habitatAreaCode, expectedValue, "");
  }

  @Then("^the result for calculationpoint with label '(.+)' is '(.+)'$")
  public void then_results_calcpoint_deposition_is(final String calculationPointLabel, final String expectedValue) {
    results.checkCalcPointDepositionInOverview(calculationPointLabel, expectedValue);
  }

  @Then("^the results table for area '(.+)' contains an exact match for values$")
  public void results_exact_match_for_values(final String area, final DataTable expectedData) {
    results.resultsTableContains(area, expectedData);
  }

  @Then("^the results table shows error message: '(.+)'$")
  public void results_table_message(final String message) {
    results.resultsTableMessage(message);
  }
  
  @And("^the results panel only contains '(.+)' label IDs$")
  public void label_id_amount(final int labelIDAmount) {
	results.checkLabelIDAmount(labelIDAmount);
  }
}
