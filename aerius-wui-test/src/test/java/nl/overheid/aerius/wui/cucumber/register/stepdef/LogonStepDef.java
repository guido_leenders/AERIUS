/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.io.IOException;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.LogonAction;
import nl.overheid.aerius.wui.cucumber.register.action.LogonAction.UserRoles;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LogonStepDef {

  private final FunctionalTestBase base;
  private final LogonAction logonAction;

  public LogonStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    logonAction = new LogonAction(base);
  }

  @When("^the user is logged in as '(.+)'$")
  public void the_user_is_logged_in_as(final UserRoles role) {
    logonAction.loginUserAsRole(role);
  }

  @Then("^the users full name is shown as '(.+)'$")
  public void the_users_fullname_is_shown_as(final String fullName) {
    logonAction.userFullNameShownAs(fullName);
  }

  @When("^the user logs off$")
  public void when_user_logs_out() {
    logonAction.userLogOff();
  }

  @When("^the user closes the browser$")
  public void and_closes_the_browser() {
    base.browserClose();
  }

}
