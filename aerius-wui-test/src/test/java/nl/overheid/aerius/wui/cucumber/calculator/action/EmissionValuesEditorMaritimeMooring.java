/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * Action class for Maritime Mooring editor.
 */
public class EmissionValuesEditorMaritimeMooring extends SourceAction {

  public EmissionValuesEditorMaritimeMooring(final FunctionalTestBase testRun) {
    super(testRun);
  }

  /**
   * Fills in a record for a ship (but not the route)
   * Precondition: Application displays source details, of source category 'maritime mooring', with an empty new row form
   * Postcondition: Application entered new row data
   *
   * @param shipName name (description) of ship
   * @param shipCategory category of the ship
   * @param shipAmount amount visits/ships
   * @param shipResidenceTime durations of visits
   */
  public void setSubItemShippingData(final String shipName, final String shipCategory, final String shipAmount, final String shipResidenceTime) {
    if (!shipName.isEmpty()) {
      base.inputSetValue(TestID.INPUT_SHIPPING_NAME, shipName);
    }
    if (!shipCategory.isEmpty()) {
      base.inputSetValue(TestID.INPUT_SHIPPING_CATEGORY, shipCategory, false, true);
    }
    if (!shipAmount.isEmpty()) {
      base.inputSetValue(TestID.INPUT_SHIPPING_AMOUNT, shipAmount);
    }
    if (!shipResidenceTime.isEmpty()) {
      base.inputSetValue(TestID.INPUT_SHIPPING_RESIDENCETIME, shipResidenceTime);
    }
  }

  /**
   * Gets the name of the ship group in the maritime mooring overview.
   * Precondition: Application displays overview in maritime mooring editor
   * @return name of the ship group in input field
   */
  public String getSubItemName() {
    return base.inputGetValue(base.idPrefix(TestID.INPUT_SHIPPING_NAME));
  }

  /**
   * Gets the number of ships in the ship group in the maritime mooring detail view.
   * Precondition: Application displays detail view of a maritime mooring editor.
   * @return returns number of ships in input field
   */
  public String getSubItemAmount() {
    return base.inputGetValue(base.idPrefix(TestID.INPUT_SHIPPING_AMOUNT));
  }

  /**
   * Gets the residence time in the ship group in the maritime mooring detail view.
   * Precondition: Application displays detail view of a maritime mooring editor.
   * @return returns residence time in input field
   */
  public String getSubItemResidenceTime() {
    return base.inputGetValue(base.idPrefix(TestID.INPUT_SHIPPING_RESIDENCETIME));
  }

  /**
   * Sets the application in modes to draw a new inland route for a maritime mooring ship
   * Precondition: Application displays form for entering new maritime mooring ship record
   * Postcondition: Application in state that the user should draw a new route
   */
  public void setSubItemNewInlandRoute() {
    base.buttonClick(TestID.BUTTON_SHIPPING_ROUTES);
    base.browserWaitForDrawAnimation();
    base.buttonClick(TestID.BUTTON_SHIPPING_NEWROUTE);
  }

  /**
   * Sets the application in modes to draw a new maritime mooring route for the given row.
   * Precondition: Application displays form for entering new maritime mooring route
   * Postcondition: Application in state that the user should draw a new route
   * @param row index of the row to draw the route for
   */
  public void setSubItemNewMaritimeRoute(final int row) {
    base.buttonClick(base.withBaseId(String.valueOf(row), TestID.BUTTON_SHIPPING_MARITIME_ROUTES));
    base.browserWaitForDrawAnimation();
    base.buttonClick(TestID.BUTTON_SHIPPING_NEWROUTE);
  }

  /**
   * Gets the sea route number for the given row
   * Precondition: Application displays form for entering new maritime mooring route and row must be visible
   * @param row index of the row to get the row number of
   * @return character of the row
   */
  public String getMaritimeMooringRouteNumberAtRow(final String row) {
    final WebElement element = base.waitForElementList(base.id(base.withBaseId(row, TestID.BUTTON_SHIPPING_MARITIME_ROUTES))).get(0);
    return element.getText();
  }

  /**
   * Sets the number of movements for a maritime mooring at the given sea route row.
   * Precondition: Application displays maritime mooring detail page and has the row visible at which the movements are to be set
   * @param row index of the row to set the movements for
   * @param movements movements to set
   */
  public void setMaritimeMovements(final int row, final String movements) {
    base.inputSetValue(base.withBaseId(String.valueOf(row), TestID.INPUT_SHIPPING_MOVEMENTS), movements);
  }

  /**
   * Gets the number of movements for a maritime mooring at the given sea route row.
   * Precondition: Application displays maritime mooring detail page and has the row visible from which the movements are read
   * @param row index of the row to get the movements for
   * @return number of movements
   */
  public String getShipMovementsAtRow(final int row) {
    return base.inputGetValue(base.withBaseId(base.idPrefix(String.valueOf(row)), TestID.INPUT_SHIPPING_MOVEMENTS));
  }

  /**
   * Returns if the given row for maritime mooring at the given sea route row is visible
   * Precondition: Application displays maritime mooring detail page
   * @param row index of the row to see if it's visible
   * @return true if row visible else false
   */
  public boolean isMaritimeMooringRowVisible(final int row) {
    return !base.waitForElementList(base.id(base.withBaseId(String.valueOf(row), TestID.PANEL_MARITIME_ROUTES))).isEmpty();
  }

  /**
   * Returns true if error pop up is visible.
   */
  public boolean checkErrorMessageNoMatchingNumbers() {
    return base.isValidationPopupShown();
  }
}
