/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import java.time.Duration;

import org.openqa.selenium.Keys;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for handling the searchbox.
 */
public class SearchAction {

  private final FunctionalTestBase base;

  public SearchAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Enters given string in the search box.
   * @param searchString string to enter in the search box
   */
  public void setSearchInput(final String searchString) {
    base.buttonClick(TestID.INPUT_SEARCHBOX).sendKeys(Keys.chord(Keys.CONTROL, "a"), searchString);
  }

  /**
   * Submit search query by clicking the search button and wait for suggestions
   */
  public void submitSearchClick() {
    base.buttonClick(TestID.BUTTON_SEARCHIMAGE);
    base.waitForElementVisible(base.id(TestID.DIV_SEARCHSUGGESTION));
  }

  /**
   * Submit search query by sending {@link Keys}.ENTER and wait for suggestions
   */
  public void submitSearchEnter() {
    base.waitForElementVisible(base.id(TestID.INPUT_SEARCHBOX)).sendKeys(Keys.ENTER);
    base.waitForElementVisible(base.id(TestID.DIV_SEARCHSUGGESTION), Duration.ofSeconds(5));
  }

  /**
   * Clear the search box to ensure a proper search
   */
  public void clearSearch() {
    base.waitForElementVisible(base.id(TestID.INPUT_SEARCHBOX)).sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.BACK_SPACE);
 }

  /**
   * Wait for presence of suggestions pop up and select the desired suggestion
   * from the list. Fails if not found before time out occurs
   * @param expectedSuggestion the expected suggestion text
   */
  public void selectSearchSuggestion(final String expectedSuggestion) {
    final String xpath = "//div[.='" + expectedSuggestion + "']";
    base.buttonClick(base.xpath(xpath));
    // Ensure the suggestion pop up is gone or it might block the following test step
    base.switchFocus();
    base.browserWaitForDrawAnimation();
  }

  /**
   * Expands the search suggestion with given text in the suggestions pop up
   * @param expectedSuggestion to select from suggestions (visible text)
   */
  public void expandSearchSuggestion(final String expectedSuggestion) {
    base.browserWaitForPageLoaded(base.getSettingPageLoadTimeout());
    base.waitForElementVisible(base.id(TestID.DIV_SEARCHSUGGESTION));
    base.expandSearchSuggestion(expectedSuggestion);
  }

  /**
   * Enter a search query and start search by clicking the search button
   * @param searchString to enter in the search box
   */
  public void searchFor(final String searchString) {
    setSearchInput(searchString);
    submitSearchClick();
  }

  /**
   * Enter a search query and start search by enter
   * @param searchString to enter in the search box
   */
  public void searchDirectlyFor(final String searchString) {
    setSearchInput(searchString);
    base.browserWaitForDrawAnimation();
    submitSearchEnter();
  }

  /**
  * Center the map on a given XY-coordinate by searching for coordinates
  * @param xCoordinate
  * @param yCoordinate
  */
  public void searchCoordinatesXy(final String xCoordinate, final String yCoordinate) {
    final String searchString = "x: " + xCoordinate + " y: " + yCoordinate;

    base.buttonClick(TestID.BUTTON_ZOOM_OUT);
    setSearchInput(searchString);
    submitSearchClick();
    base.browserWaitForDrawAnimation();
    selectSearchSuggestion(searchString);
  }

}
