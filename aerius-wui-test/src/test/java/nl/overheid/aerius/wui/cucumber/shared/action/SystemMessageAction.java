/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for checking calculation results in the results-page.
 */
public class SystemMessageAction {
  private static final Logger LOG = LoggerFactory.getLogger(SystemMessageAction.class);

  private final FunctionalTestBase base;

  public SystemMessageAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Sanitize current URL and navigate to system message management page.
   * Fails if destination verification is unsuccessful
   * @throws MalformedURLException
   */
  public void openSystemMessageForm() throws MalformedURLException {
    final URL url = new URL(base.getDriver().getCurrentUrl());
    final String systemMessagePath = url.getProtocol() + "://" + url.getHost() + url.getPath() + "aerius-system-info-update";
    LOG.info("Navigating to system message management at: " + systemMessagePath);
    base.browserReload(systemMessagePath);
    // Ensure that the expected page is shown
    final String xpath = "//h2[contains(.,'Systeem info bericht beheren.')]";
    Assert.assertNotNull("Element not found by xpath: " + xpath, base.getElementByXPath(xpath));
  }

  /**
   * Fill the system message text field with a string of text or html
   * @param messageText
   */
  public void enterMessageText(final String messageText) {
    base.waitForElementVisible(base.id(TestID.SYSTEM_MESSAGE_TXT)).sendKeys(messageText);
  }

  /**
   * Set up the system message for a specific language. Currently only nl and en supported
   * Anything else but nl and en will be assumed meant for nl
   * @param messageLocale
   */
  public void enterMessageLocale(final String messageLocale) {
    base.waitForElementVisible(base.id(TestID.SYSTEM_MESSAGE_LOCALE)).sendKeys(messageLocale);
  }

  /**
   * Fill the system message UUID field with a string of text
   * @param messageUUID
   */
  public void enterMessageUUID(final String messageUUID) {
    base.waitForElementVisible(base.id(TestID.SYSTEM_MESSAGE_UUID)).sendKeys(messageUUID);
  }

  /**
   * Tick the delete-all check box.
   */
  public void deleteAllMessages() {
    final WebElement deleteAllCheckBox = base.waitForElementVisible(base.id(TestID.SYSTEM_MESSAGE_DELETE));
    if (!deleteAllCheckBox.isSelected()) {
      deleteAllCheckBox.click();
    } else {
      LOG.info("Delete-all check box is selected already.");
    }
  }

  /**
   * Submit the system message form
   */
  public void submitSystemMessage() {
    base.buttonClick(TestID.SYSTEM_MESSAGE_SUBMIT);
  }

  /**
   * Make sure the confirmation page contains an error message
   */
  public void checkConfirmationError() {
    final String xpath = "//h2[.='Fout bij verwerken']";
    Assert.assertNotNull("Element not found by xpath: " + xpath, base.waitForElementVisible(base.xpath(xpath)));
  }

  /**
   * Make sure the confirmation page confirms a successful operation
   */
  public void checkConfirmationSuccess() {
    final String xpath = "//h2[.='Bericht verwerkt.']";
    Assert.assertNotNull("Element not found by xpath: " + xpath, base.waitForElementVisible(base.xpath(xpath)));
  }

  /**
   * Ensure that a system message is shown as expected.
   * @param message
   */
  public void checkMessageTextPresence(final String message) {
    final WebElement systemMessage = base.waitForElementVisible(base.id(TestID.SYSTEM_MESSAGE_BANNER));
    Assert.assertEquals("System message not shown as expected!", message, systemMessage.getText());
  }

  /**
   * Ensure that a system message is not shown.
   */
  public void checkMessageTextAbsence() {
    final WebElement pageRoot = base.waitForElementVisible(base.xpath("//html"));
    final WebElement systemMessage = pageRoot.findElement(base.id(TestID.SYSTEM_MESSAGE_BANNER));
    Assert.assertEquals("System message shown unexpectedly!", "", systemMessage.getText());
  }

  /**
   * Ensure that both the textual representation and anchor link are shown as expected
   * @param linkText
   * @param linkUrl
   */
  public void checkLinkTextAndUrl(final String linkText, final String linkUrl) {
    final WebElement systemMessage = base.waitForElementVisible(base.id(TestID.SYSTEM_MESSAGE_BANNER));
    final WebElement systemMessageLink = systemMessage.findElement(By.linkText(linkText));
    Assert.assertNotNull("System message missing link: " + linkText, systemMessageLink);
    Assert.assertEquals("System message link text not as expected!", linkText, systemMessageLink.getText());
    Assert.assertEquals("System message link url not as expected!", linkUrl, systemMessageLink.getAttribute("href"));
  }

}
