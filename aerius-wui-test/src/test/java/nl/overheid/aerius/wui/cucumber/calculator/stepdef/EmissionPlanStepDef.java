/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionEditorPlanAction;

import cucumber.api.java.en.When;

public class EmissionPlanStepDef {

  private final FunctionalTestBase base;
  private final EmissionEditorPlanAction sourceEmissionPlan;

  public EmissionPlanStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    sourceEmissionPlan = new EmissionEditorPlanAction(base);
  }

  @When("^the plan emission item is given the name '(.+)'$")
  public void when_source_plan_emission_name_set(final String planName) throws InterruptedException {
    sourceEmissionPlan.setName(planName);
  }

  @When("^the plan emission item is given name '(.+)' and category '(.+)'$")
  public void when_source_plan_emission_is_set(final String planName, final String planCategory) throws InterruptedException {
    sourceEmissionPlan.setName(planName);
    sourceEmissionPlan.setCategory(planCategory);
  }

  @When("^the plan emission item is given name '(.+)', category '(.+)' and amount (\\d+)$")
  public void when_source_plan_emission_with_amount_is_set(final String planName, final String planCategory, final String planAmount)
      throws InterruptedException {
    sourceEmissionPlan.setName(planName);
    sourceEmissionPlan.setCategory(planCategory);
    sourceEmissionPlan.setAmount(planAmount);
  }

}
