/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.scenario.stepdef;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.WebElement;

import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.scenario.action.ScenariosAction;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class ScenariosStepDef {

  private final FunctionalTestBase base;
  private final ScenariosAction scenarios;

  public ScenariosStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    scenarios = new ScenariosAction(base);
  }

  /*
   * Login / Overview Steps
   */

  @When("^the user requests an api key for e-mail address '(.+)'$")
  public void user_requests_apikey_with_email(final String emailAddress) {
    scenarios.requestApiKey(emailAddress);
  }

  @When("^the user authenticates with api key '(.+)'$")
  public void user_authenticates_with_apikey(final String apiKey) {
    scenarios.authenticateApiKey(apiKey);
  }

  @When("^the scenarios overview page is shown$")
  public void scenarios_overview_page_shown() {
    scenarios.checkOverviewPageShown();
  }

  @When("^the scenarios overview contains job id '(.+)' with status '(.+)'$")
  public void overview_contains_job_status(final String jobId, final String jobStatus) {
    scenarios.checkOverviewContainsJobStatus(jobId, jobStatus);
  }

  @When("^the scenarios overview contains job id '(.+)' with status$")
  public void overview_contains_job_status(final String jobId, final List<String> jobStatus) {
    scenarios.checkOverviewContainsJobStatus(jobId, jobStatus);
  }

  @When("^the user enters project key '(.+)'$")
  public void user_enters_project_key(final String projectKey) {
    base.inputSetValue(base.id(TestIDScenario.CONNECT_OVERVIEW_PROJECT_KEY), projectKey);
  }

  @When("^the user confirms project import from register$")
  public void user_confirms_project_import() {
    base.buttonClick(TestIDScenario.CONNECT_OVERVIEW_REQUEST_EXPORT);
  }

  @When("^the scenarios overview filter is '(.+)'$")
  public void scenarios_overview_page_filter(final String state) {
    scenarios.checkOverviewFilterStatus(state);
  }

  @When("^the scenarios overview page shows active jobs only$")
  public void scenarios_overview_shows_active_jobs_only() {
    scenarios.checkOverviewContainsActiveJobsOnly();
  }

  @When("^the scenarios overview filter is toggled$")
  public void scenarios_overview_filter_toggle() {
    base.buttonClick(TestIDScenario.CONNECT_OVERVIEW_TOGGLE_STATUS + "-label");
  }

  @When("^the scenarios overview shows all jobs$")
  public void scenarios_overview_shows_all_jobs() {
    scenarios.checkOverviewContainsAllJobs();
  }

  /*
   * Scenario Setup Steps
   */

  @When("^the user opens a new connect scenario page$")
  public void user_opens_new_scenario() {
    scenarios.openNewScenarioPage();
  }

  @When("^the scenarios set up page is shown$")
  public void scenarios_setup_page_shown() {
    scenarios.checkSetupPageShown();
  }

  @And("^the calculation options panel is open$")
  public void check_calculation_options_panel() {
    scenarios.checkIfCalculationOptionsPanelIsOpen();
  }

  @When("^the user imports scenario file '(.+)'")
  public void user_imports_scenario_file(final String fileName) {
    scenarios.importScenarioFile(fileName);
  }

  @When("^the user sets scenario file at row '(.+)' as research area")
  public void user_sets_scenario_file_as_research(final String rowNumber) {
    scenarios.setResearchArea(rowNumber, true);
  }

  @When("^the scenario file '(.+)' is shown in the list$")
  public void scenario_file_shown_in_list(final String fileName) {
    scenarios.checkFileInList(fileName, true);
  }

  @When("^the scenario file '(.+)' is not shown in the list$")
  public void scenario_file_not_in_list(final String fileName) {
    scenarios.checkFileInList(fileName, false);
  }

  @When("^the scenario file list is empty$")
  public void scenario_file_list_empty() {
    scenarios.checkFileListEmpty();
  }

  @When("the scenario imported file '(.+)' is marked as situation '(.+)'")
  public void scenario_file_marked_as_situation(final int row, final String situation) {
    final String xpath = "//div[@id='gwt-debug-calculation-detail-table-situation']/span[contains(.,'" + situation + "')]";
    List<WebElement> radios = base.waitForElementList(base.xpath(xpath));
    base.buttonClick(radios.get(row - 1)).isSelected();
  }

  @When("^the table column '(.+)' is shown$")
  public void table_column_shown(final String columnName) {
    scenarios.checkTableColumnShown(columnName);
  }

  @When("^the table column '(.+)' is hidden$")
  public void table_column_hidden(final String columnName) {
    scenarios.checkTableColumnHidden(columnName);
  }

  @When("^the user deletes scenario file '(.+)'$")
  public void user_deletes_scenario_file(final String fileName) {
    scenarios.deleteScenarioFile(fileName);
  }

  @When("^the calculate button is disabled$")
  public void calculate_button_disabled() {
    scenarios.checkCalculateDisabled();
  }

  @When("^the user cancels the connect scenario setup$")
  public void the_user_cancels_the_connect_scenario_setup() {
    base.buttonClick(TestIDScenario.CONNECT_CONFIGURE_CANCEL);
  }

  @When("^the user accepts the confirmation dialog$")
  public void the_user_accepts_the_confirmation_dialog() {
    base.browserAlertAccept();
  }

  @When("^the user cancels the confirmation dialog$")
  public void the_user_cancels_the_confirmation_dialog() {
    base.browserAlertDismiss();
  }

  @When("^the user sets calculation job id to '(.+)'$")
  public void the_user_sets_option(final String jobId) {
    scenarios.setCalculationJobId(jobId);
  }

  @When("^the user sets calculation option '(.+)' to '(.+)'$")
  public void the_user_sets_option(final String optionName, final String optionValue) {
    scenarios.setCalculationOption(optionName, optionValue);
  }

  @When("^the calculation option '(.+)' is set to '(.+)'$")
  public void the_calculation_option_is_set_to(final String optionName, final String optionValue) {
    scenarios.checkCalculationOption(optionName, optionValue);
  }

  @When("^the calculation option year is set to current year$")
  public void the_calculation_year_is_set_to_current() {
    final int year = Calendar.getInstance().get(Calendar.YEAR);
    scenarios.checkCalculationOption("year", String.valueOf(year));
  }

  //TODO: Commented out stepdef option for @AER-1718.
  @When("^the user enables calculation option '(.+)'$")
  public void the_user_enables_option(final String optionName) {
//    scenarios.setCalculationOptionEnabled(optionName);
  }

  @When("^the temporary project start year should be '(.+)'$")
  public void the_temp_start_year_should_be(final String startYear) {
    scenarios.checkTemporaryStartYear(startYear);
  }

  @When("^the scenarios calculation is started$")
  public void the_scenarios_calculation_started() {
    base.buttonClick(TestIDScenario.CONNECT_CONFIGURE_CALCULATE);
    // Grant some extra wait time for slow clients to return to the overview page
    base.waitForElementInvisible(
        base.id(TestIDScenario.CONNECT_CONFIGURE_CALCULATE),
        SeleniumDriverWrapper.getInstance().getSettingSyncTimeout().multipliedBy(3)
        );
  }

  /*
   * Combined Steps
   */

  @When("^the user is logged in using api key '(.+)'$")
  public void user_login_using_key(final String apiKey) {
    user_authenticates_with_apikey(apiKey);
    scenarios_overview_page_shown();
  }

  @When("^the user requests results for project key '(.+)'$")
  public void request_results_for_project_key(final String projectKey) {
    user_enters_project_key(projectKey);
    user_confirms_project_import();
  }

  @When("^the source with id tag '(.+)' is clicked$")
  public void when_overview_click_source_tag(final String id) {
    final String xpath = "//div[@id='gwt-debug-id' and contains(.,'" + id + "')]";
    base.buttonClick(base.waitForElementVisible(base.xpath(xpath)));
  }

}
