/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for generating an export using
 * the export dialog.
 */
public class ApplicationAction {

  private final FunctionalTestBase base;

  public ApplicationAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Enumeration of all valid application statuses
   */
  public enum ApplicationStatus {
    QUEUED, PENDING_WITH_SPACE, PENDING_WITHOUT_SPACE, REJECTED_WITHOUT_SPACE,
    ASSIGNED, ASSIGNED_FINAL, REJECTED, REJECTED_FINAL;

    public static ApplicationStatus getUnsafe(final String value) {
      for (final ApplicationStatus item : values()) {
        if (item.name().equalsIgnoreCase(value)) {
          return item;
        }
      }

      throw new IllegalArgumentException("Invalid ApplicationStatus supplied: " + value);
    }
  }

}
