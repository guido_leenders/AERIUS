/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator;

import org.junit.runner.RunWith;

import nl.overheid.aerius.wui.cucumber.base.CucumberBase;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This is the runner class for cucumber tests for the AERIUS CALCULATOR
 *
 * # general
 * @run: run own tests
 * @single: run regression-test on single features
 * @composite: run composite regression-tests
 * @bug-retest: run bug retest
 *
 * # specific sub tags
 * @maritime: run tests for shipping sector
 * @shipping run test for inland shipping
 * @road: run tests for road sector
 * @plan: run tests for plan sector
 * @farmlodging: run tests for farming sector
 * @comparison: run tests for comparison
 * @calcpoints: run tests for calculation points
 * @manual: run test to make screenshots for the user manual
 */
@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:target/cucumber/nightly/calculator6", "json:target/cucumber/nightly/calculator6/nightly_report.json"},
    features = {"src/test/resources/calculator/sSourceOnroad.feature",
        "src/test/resources/calculator/sSourcePlans.feature",
        "src/test/resources/calculator/sSystemMessage.feature"},
    glue = {
        "nl.overheid.aerius.wui.cucumber.calculator.stepdef", 
        "nl.overheid.aerius.wui.cucumber.shared.action",
        "nl.overheid.aerius.wui.cucumber.shared.stepdef"
        },
    tags = {"@nightly"}
    )
public class CalculatorCucumberSuite6Test extends CucumberBase {

}
