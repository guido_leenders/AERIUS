/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.shared.test.TestIDMelding;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;

import cucumber.api.java.en.When;

public class MeldingInformationStepDef {

  private final FunctionalTestBase base;
  private final ImportAction importFile;

  public MeldingInformationStepDef() throws IOException {

    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    importFile = new ImportAction(base);
  }

  /*
   * BENEFICIARY (1st contact details)
   */
  @When("^the melding beneficiary organisation is set to '(.+)'$")
  public void when_melding_beneficiary_organisation_set(final String beneficiaryOrgName) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.ORG_NAME;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryOrgName);
  }

  @When("^the melding beneficiary contact is set to '(.+)'$")
  public void when_melding_beneficiary_contact_set(final String beneficiaryContact) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.ORG_CONTACT;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryContact);
  }

  @When("^the melding beneficiary address is set to '(.+)'$")
  public void when_melding_beneficiary_address_set(final String beneficiaryAddress) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.ORG_ADDR;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryAddress);
  }

  @When("^the melding beneficiary postcode is set to '(.+)'$")
  public void when_melding_beneficiary_postcode_set(final String beneficiaryPostcode) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.ORG_POST;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryPostcode);
  }

  @When("^the melding beneficiary city is set to '(.+)'$")
  public void when_melding_beneficiary_city_set(final String beneficiaryCity) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.ORG_CITY;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryCity);
  }

  @When("^the melding beneficiary email is set to '(.+)'$")
  public void when_melding_beneficiary_email_set(final String beneficiaryEmail) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.ORG_EMAIL;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryEmail);
  }

  @When("^the melding beneficiary correspondence details is set to other$")
  public void when_melding_benefactor_correspondenceaddress_set_other() {
    base.buttonClick(TestIDMelding.BENEFICIARY + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.CUSTOM
        + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding beneficiary correspondence address is set to '(.+)'$")
  public void when_melding_beneficiary_correspondenceaddress_set(final String beneficiaryCorrespondenceAddress) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.ORG_ADDR;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryCorrespondenceAddress);
  }

  @When("^the melding beneficiary correspondence postcode is set to '(.+)'$")
  public void when_melding_beneficiary_correspondencepostcode_set(final String beneficiaryCorrespondencePostcode) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.ORG_POST;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryCorrespondencePostcode);
  }

  @When("^the melding beneficiary correspondence city is set to '(.+)'$")
  public void when_melding_beneficiary_correspondencecity_set(final String beneficiaryCorrespondenceCity) {
    final String inputField = TestIDMelding.BENEFICIARY + TestIDMelding.BENEFICIARY_CORRESPONDENCE + TestIDMelding.ORG_CITY;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), beneficiaryCorrespondenceCity);
  }

  /*
   * BENEFACTOR (2nd contact details)
   */
  @When("^the melding benefactor is set to other$")
  public void when_melding_benefactor_set_other() {
    base.buttonClick(TestIDMelding.BENEFACTOR + TestIDMelding.CUSTOM + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding benefactor is set to same$")
  public void when_melding_benefactor_set_same() {
    base.buttonClick(TestIDMelding.BENEFACTOR + TestIDMelding.SAME + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding benefactor organisation is set to '(.+)'$")
  public void when_melding_benefactor_organisation_set(final String benefactorOrgName) {
    final String inputField = TestIDMelding.BENEFACTOR + TestIDMelding.ORG_NAME;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), benefactorOrgName);
  }

  @When("^the melding benefactor contact is set to '(.+)'$")
  public void when_melding_benefactor_contact_set(final String benefactorContact) {
    final String inputField = TestIDMelding.BENEFACTOR + TestIDMelding.ORG_CONTACT;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), benefactorContact);
  }

  @When("^the melding benefactor address is set to '(.+)'$")
  public void when_melding_benefactor_address_set(final String benefactorAddress) {
    final String inputField = TestIDMelding.BENEFACTOR + TestIDMelding.ORG_ADDR;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), benefactorAddress);
  }

  @When("^the melding benefactor postcode is set to '(.+)'$")
  public void when_melding_benefactor_postcode_set(final String benefactorPostcode) {
    final String inputField = TestIDMelding.BENEFACTOR + TestIDMelding.ORG_POST;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), benefactorPostcode);
  }

  @When("^the melding benefactor city is set to '(.+)'$")
  public void when_melding_benefactor_city_set(final String benefactorCity) {
    final String inputField = TestIDMelding.BENEFACTOR + TestIDMelding.ORG_CITY;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), benefactorCity);
  }

  @When("^the melding benefactor email is set to '(.+)'$")
  public void when_melding_benefactor_email_set(final String benefactorEmail) {
    final String inputField = TestIDMelding.BENEFACTOR + TestIDMelding.ORG_EMAIL;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), benefactorEmail);
  }

  @When("^the melding benefactor email confirmation is toggled$")
  public void when_melding_benefactor_email_confirmation_toggle() {
    final String checkBox = TestIDMelding.BENEFACTOR + TestIDMelding.SEND_CONFIRMATION + "-" + base.getBrowserRadioSetStyle();
    base.buttonClick(checkBox);
  }

  @When("^the melding benefactor PDF-file '(.+)' is added$")
  public void when_melding_benefactor_add_file(final String fileName) {
    importFile.importSelectFile(fileName, ImportType.PAA);
    base.buttonHover(TestID.INPUT_IMPORTFILE);
  }

  /*
   * EXECUTOR (3rd contact details)
   */
  @When("^the melding executor is set to other$")
  public void when_melding_executor_set_other() {
    base.buttonClick(TestIDMelding.EXECUTOR + TestIDMelding.CUSTOM + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding executor is set to same$")
  public void when_melding_executor_set_same() {
    base.buttonClick(TestIDMelding.EXECUTOR + TestIDMelding.SAME + "-" + base.getBrowserRadioSetStyle());
  }

  @When("^the melding executor organisation is set to '(.+)'$")
  public void when_melding_executor_organisation_set(final String executorOrgName) {
    final String inputField = TestIDMelding.EXECUTOR + TestIDMelding.ORG_NAME;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), executorOrgName);
  }

  @When("^the melding executor contact is set to '(.+)'$")
  public void when_melding_executor_contact_set(final String executorContact) {
    final String inputField = TestIDMelding.EXECUTOR + TestIDMelding.ORG_CONTACT;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), executorContact);
  }

  @When("^the melding executor address is set to '(.+)'$")
  public void when_melding_executor_address_set(final String executorAddress) {
    final String inputField = TestIDMelding.EXECUTOR + TestIDMelding.ORG_ADDR;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), executorAddress);
  }

  @When("^the melding executor postcode is set to '(.+)'$")
  public void when_melding_executor_postcode_set(final String executorPostcode) {
    final String inputField = TestIDMelding.EXECUTOR + TestIDMelding.ORG_POST;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), executorPostcode);
  }

  @When("^the melding executor city is set to '(.+)'$")
  public void when_melding_executor_city_set(final String executorCity) {
    final String inputField = TestIDMelding.EXECUTOR + TestIDMelding.ORG_CITY;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), executorCity);
  }

  @When("^the melding executor email is set to '(.+)'$")
  public void when_melding_executor_email_set(final String executorEmail) {
    final String inputField = TestIDMelding.EXECUTOR + TestIDMelding.ORG_EMAIL;
    base.waitForElementVisible(base.id(inputField));
    base.inputSetValue(base.id(inputField), executorEmail);
  }

  @When("^the melding executor email confirmation is toggled$")
  public void when_melding_executor_email_confirmation_toggle() {
    final String checkBox = TestIDMelding.EXECUTOR + TestIDMelding.SEND_CONFIRMATION + "-" + base.getBrowserRadioSetStyle();
    base.buttonClick(base.waitForElementVisible(base.id(checkBox)));
  }

}
