/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import org.apache.http.annotation.Obsolete;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionEditorFarmLodging;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionEditorFarmLodging.LodgingStackType;
import nl.overheid.aerius.wui.cucumber.shared.action.NotificationAction;
import cucumber.api.java.en.When;

public class EmissionFarmingLodgingStepDef {

  private final FunctionalTestBase base;
  private final EmissionEditorFarmLodging sourceEmissionFarmingLodging;
  private final NotificationAction notification;

  public EmissionFarmingLodgingStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    sourceEmissionFarmingLodging = new EmissionEditorFarmLodging(base);
    notification = new NotificationAction(base);
  }

  @When("^the farming emission is added a rav-code '(.+)' with bwl-code '(.+)' and an amount of (\\d+)$")
  public void when_source_farming_emission_rav_is_set(final String ravCode, final String bwlCode, final String ravAmount) {
    sourceEmissionFarmingLodging.setRavCode(ravCode, bwlCode, ravAmount);
  }

  @When("^the farming additional is added '(.+)' and amount of (\\d+)$")
  public void when_source_farming_emission_additional_is_set(final String addCode, final String addAmount) {
    sourceEmissionFarmingLodging.setAdditionalCode(addCode, addAmount);
  }

  @When("^the farming additional code is set to '(.+)' to open the list$")
  public void when_source_farming_emission_additional_set_code(final String addCode) {
    sourceEmissionFarmingLodging.setMeasureSelect(LodgingStackType.ADDITIONAL_MEASURE);
    sourceEmissionFarmingLodging.enterAdditionalCode(addCode);
  }

  @When("^the farming reductive code '(.+)' to open te list$")
  public void when_source_farming_emission_reductive_set_code(final String redCode) {
    sourceEmissionFarmingLodging.setMeasureSelect(LodgingStackType.REDUCING_MEASURE);
    sourceEmissionFarmingLodging.enterReductiveCode(redCode);
  }

  @When("^the farming reductive is added '(.+)'$")
  public void when_source_farming_emission_reductive_is_set(final String redCode) {
    sourceEmissionFarmingLodging.setReductiveCode(redCode);
  }

  @When("^the farming fodder measure is added '(.+)'$")
  public void when_source_farming_fodder_is_set(final String fodderCode) {
    sourceEmissionFarmingLodging.setMeasureSelect(LodgingStackType.FODDER_MEASURE);
    sourceEmissionFarmingLodging.enterFodderCode(fodderCode);
  }

  @When("^the farming emission is added a custom lodge '(.+)' with an emissionfactor of (.+) and an amount of (\\d+)$")
  public void when_source_farming_emission_customlodge_is_set(final String ravDescription, final String emissionfactor, final String ravAmount) {
    sourceEmissionFarmingLodging.setCustomLodge(ravDescription, emissionfactor, ravAmount);
    notification.checkForValidationErrors();
  }

  @When("^the farming emission additional at row (\\d+) is deleted$")
  public void when_source_farming_emission_additional_is_deleted(final String row) {
    sourceEmissionFarmingLodging.clickDeleteAdditional(row);
  }

  @When("^the farming emission reductive at row (\\d+) is deleted$")
  public void when_source_farming_emission_reductive_is_deleted(final String row) {
    sourceEmissionFarmingLodging.clickDeleteReductive(row);
  }

  @When("^the farming emission rav-code at row (\\d+) is deleted$")
  public void when_source_farming_emission_rav_is_deleted(final String row) {
    sourceEmissionFarmingLodging.clickDeleteRavRow(row);
  }

}
