/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for editing a calculation point
 * in the "Calculation point Details" page.
 * TODO move all related code to this class
 */
public class CalculationPointAction {

  private final FunctionalTestBase base;

  public CalculationPointAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Clicks Save / Next step button.
   * Precondition: calculation point entry details are shown
   * Postcondition: Application displays calculation point overview.
   */
  public void clickSaveButton() {
    base.buttonClick(TestID.BUTTON_NEXTSTEP);
  }

  /**
   * Set the label for calculation point 
   * Precondition: calculation point entry details are shown
   * Postcondition: Application entered calculation point label
   *
   * @param label String calculation point label
   */
  public void setLabel(final String label) {
    base.inputSetValue(TestID.INPUT_LABEL, label);
  }

}
