/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * Action class for Inland Mooring editor.
 */
public class EmissionEditorInlandMooring {

  private final FunctionalTestBase base;

  public EmissionEditorInlandMooring(final FunctionalTestBase testrun) {
    this.base = testrun;
  }

  /**
   * Add a new row to an inland mooring source using the given parameters
   * Skips setting a value when an empty string is provided 
   * @param name identify this specific ship
   * @param category the class of vessel 
   * @param residenceTime how long the ship will stay docked
   */
  public void setSubItemShippingData(final String name, final String category, final String residenceTime) {
    if (!name.isEmpty()) {
      setSubItemShipName(name);
    }
    if (!category.isEmpty()) {
      setSubItemShipType(category);
    }
    if (!residenceTime.isEmpty()) {
      setSubItemResidenceTime(residenceTime);
    }
  }

  /**
   * Add a new row to an inland route source using the given parameters
   * Skips setting a value when an empty string is provided 
   * @param name identify this specific ship
   * @param category the class of vessel 
   * @param roundTrips how often ship travels back and forth
   */
  public void setSubItemInlandRouteData(final String name, final String category, final String roundTrips) {
    if (!name.isEmpty()) {
      base.inputSetValue(TestID.INPUT_SHIPPING_NAME, name);
    }
    if (!category.isEmpty()) {
      base.inputSetValue(TestID.INPUT_SHIPPING_CATEGORY, category, false, true);
    }
    if (!roundTrips.isEmpty()) {
      base.inputSetValue(TestID.INPUT_INLAND_SHIPPING_AMOUNT_ATOB, roundTrips);
      base.inputSetValue(TestID.INPUT_INLAND_SHIPPING_AMOUNT_BTOA, roundTrips);
    }
  }

  public String getSubItemShipType() {
    return base.inputGetValue(base.buttonHover(TestID.INPUT_SHIPPING_CATEGORY));
  }

  public String getSubItemMovementAtoB() {
    return base.inputGetValue(base.buttonHover(TestID.INPUT_INLAND_SHIPPING_AMOUNT_ATOB));
  }

  public String getSubItemMovementBtoA() {
    return base.inputGetValue(base.buttonHover(TestID.INPUT_INLAND_SHIPPING_AMOUNT_BTOA));
  }

  public void getSubItemLoadPercentAtoB(final String loadPercentage) {
    base.inputSetValue(TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_ATOB, loadPercentage);
  }

  public String getSubItemLoadPercentAtoB() {
    return base.inputGetValue(base.buttonHover(TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_ATOB));
  }

  public void setSubItemLoadPercentBtoA(final String loadPercentage) {
    base.inputSetValue(TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_BTOA, loadPercentage);
  }

  public String getSubItemLoadPercentBtoA() {
    return base.inputGetValue(base.buttonHover(TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN_BTOA));
  }

  public String getSubItemResidenceTime() {
    return base.inputGetValue(base.buttonHover(TestID.INPUT_SHIPPING_RESIDENCETIME));
  }

  public void subItemInlandRouteSetNew(final NavigationDirection navDir, final int row) {
    base.buttonClick(navDir.name() + "-" + base.withBaseId(String.valueOf(row), TestID.BUTTON_SHIPPING_INLAND_ROUTES));
    base.browserWaitForDrawAnimation();
    base.buttonClick(TestID.BUTTON_SHIPPING_NEWROUTE);
  }

  public boolean isInlandMooringRowVisible(final int row) {
    return !base.waitForElementList(base.id(base.withBaseId(String.valueOf(row), TestID.PANEL_INLAND_MOORING_ROUTES))).isEmpty();
  }

  public void setSubItemRouteDirection(final int row, final NavigationDirection dir) {
    base.buttonClick(base.withBaseId(String.valueOf(row), TestID.BUTTON_SHIPPING_INLAND_NAVIGATION_DIRECTION));
    if (dir == NavigationDirection.ARRIVE) {
      base.buttonClick(TestID.BUTTON_SHIPPING_INLAND_NAVIGATION_ARRIVE);
    } else if (dir == NavigationDirection.DEPART) {
      base.buttonClick(TestID.BUTTON_SHIPPING_INLAND_NAVIGATION_DEPART);
    }
  }

  public void setSubItemRouteMovements(final NavigationDirection navDir, final int row, final String movements) {
    base.inputSetValue(base.withBaseId(navDir + "-" + String.valueOf(row), TestID.INPUT_SHIPPING_MOVEMENTS), movements, true, true);
  }

  public void setSubItemRouteLoad(final NavigationDirection navDir, final int row, final String ladenPercentage) {
    base.inputSetValue(navDir + "-" + base.withBaseId(String.valueOf(row), TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN), 
        ladenPercentage, true, true);
  }

  public char getInlandMooringRouteNumberAtRow(final int row) {
    final WebElement element = base.waitForElementList(base.id(base.withBaseId(String.valueOf(row), TestID.BUTTON_SHIPPING_INLAND_ROUTES))).get(0);
    final String text = element.getText();
    final String[] split = text.split(" ");
    return split.length > 1 ? split[1].trim().charAt(0) : 0;
  }

  public NavigationDirection getInlandMooringDirectionAtRow(final int row) {
    final WebElement element = base.waitForElementList(base.id(base.withBaseId(String.valueOf(row),
        TestID.BUTTON_SHIPPING_INLAND_NAVIGATION_DIRECTION))).get(0);
    final String text = element.getText();
    final char dir = text.isEmpty() ? 0 : text.charAt(0);
    return dir == 'A' ? NavigationDirection.ARRIVE : (dir == 'V' ? NavigationDirection.DEPART : null);
  }

  public String getInlandMooringMovementsAtRow(final NavigationDirection navDir, final int row) {
    return base.inputGetValue(base.buttonClick(navDir + "-" + base.withBaseId(String.valueOf(row), TestID.INPUT_SHIPPING_MOVEMENTS)));
  }

  public String getInlandMooringLoadAtRow(final NavigationDirection navDir, final String row) {
    final String id = navDir + "-" + base.withBaseId(row, TestID.INPUT_INLAND_SHIPPING_PERCENTAGE_LADEN);
    return base.inputGetValue(base.buttonClick(id));
  }

  public void setSubItemShipName(final String shipName) {
    final WebElement shipNameInput = base.waitForElementVisible(base.id(TestID.INPUT_SHIPPING_NAME));
    shipNameInput.sendKeys(Keys.chord(Keys.CONTROL, "a"), shipName, Keys.ENTER);
  }

  public void setSubItemShipType(final String shipType) {
    // Grab input element and clear before sending the actual input
    final WebElement shipTypeInput = base.waitForElementVisible(base.id(TestID.INPUT_SHIPPING_CATEGORY));
    shipTypeInput.sendKeys(Keys.chord(Keys.CONTROL, "a"), shipType, Keys.ENTER);
  }

  public void setSubItemResidenceTime(final String shipResidenceTime) {
    base.inputSetValue(TestID.INPUT_SHIPPING_RESIDENCETIME, shipResidenceTime);
  }

  public void setInlandMooringRouteNumberAtRow(final NavigationDirection navDir, final int row, final String route) {
    final String routeButtonId = navDir.name() + "-" + String.valueOf(row) + "-" + TestID.BUTTON_SHIPPING_INLAND_ROUTES;
    final String optionXpath = "//div[contains(@class,'EditableListButtonItem') and contains(.,'" + route + "')]";

    // remove any blocking validation pop up present
    base.cleanValidationErrorPopUps();
    base.buttonClick(routeButtonId);

    final WebElement webElement = base.waitForElementVisible(base.xpath(optionXpath));
    // send click using JavaScript (selenium refuses because other elements in the way)
    base.buttonClickJs(webElement);
    base.switchFocus();
    base.waitForElementInvisible(base.xpath(optionXpath));
  }

}
