/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.scenario.stepdef;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.shared.action.MenuAction.MenuHasTestId;

/**
 * Enumeration of all calculator usable menu options.
 */
public enum ScenarioMenuOption implements MenuHasTestId {
  START(TestID.MENU_ITEM_START),
  EMISSIONSOURCES(TestID.MENU_ITEM_EMISSIONSOURCES_OVERVIEW),
  RESULTS(TestID.MENU_ITEM_RESULTS),
  SCENARIOS(TestID.MENU_ITEM_SCENARIOS),
  UTILS(TestID.MENU_ITEM_UTILS);

  private final String testId;

  private ScenarioMenuOption(final String testId) {
    this.testId = TestID.MENU_ITEMS + "-" + testId;
  }

  @Override
  public String getTestId() {
    return testId;
  }

  public enum ScenarioSubMenuOption implements MenuHasTestId {
    RESULTS_OVERVIEW(TestID.RESULT_GROUPBUTTONS + "-overview"),
    RESULTS_GRAPH(TestID.RESULT_GROUPBUTTONS + "-graphics"),
    RESULTS_TABLE(TestID.RESULT_GROUPBUTTONS +  "-table"),
    RESULTS_FILTER(TestID.RESULT_GROUPBUTTONS + "-filter");

    private final String testId;

    private ScenarioSubMenuOption(final String testId) {
      this.testId = testId;
    }

    @Override
    public String getTestId() {
      return testId;
    }

  }
}
