/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared.action;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.calculator.action.CalculatorMenuOption.CalculatorSubMenuOption;
import nl.overheid.aerius.wui.cucumber.register.action.RegisterMenuOption.RegisterSubMenuOption;
import nl.overheid.aerius.wui.cucumber.scenario.stepdef.ScenarioMenuOption.ScenarioSubMenuOption;

/**
 * This class contains all the methods that apply for selecting menu items from the side menu
 */
public class MenuAction {
  private static final Logger LOG = LoggerFactory.getLogger(MenuAction.class);

  private final FunctionalTestBase base;

  public MenuAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

  /**
   * Selects the given menu options in any of the AERIUS applications.
   * @param menuOption menu option to select
   */
  public void selectMenuOption(final MenuHasTestId menuOption) {
    base.buttonClick(menuOption.getTestId());
  }

  /**
   * Selects the given menu from a sub menu (button).
   * @param menuOption menu option to select
   */
  public void selectSubMenuOptionCalculator(final CalculatorSubMenuOption menuOption) {
    base.buttonHover(menuOption.getTestId());
    base.buttonClick(base.buttonHover(menuOption.getTestId()).findElement(By.tagName("label")));
    base.buttonHover(menuOption.getTestId());
  }

  /**
   * Selects the given sub menu from within the AERIUS SCENARIO application (button).
   * @param menuOption menu option to select
   */
  public void selectSubMenuOptionScenario(final ScenarioSubMenuOption menuOption) {
    base.buttonHover(menuOption.getTestId());
    base.buttonClick(base.buttonHover(menuOption.getTestId()).findElement(By.tagName("label")));
    base.buttonHover(menuOption.getTestId());
  }

  /**
   * Selects the given menu from a sub menu (button).
   * @param menuOption menu option to select
   */
  public void selectSubMenuOptionRegister(final RegisterSubMenuOption menuOption) {
    base.buttonClick(menuOption.getTestId());
  }

  /**
   * Assert that the given menu item is present.
   * @param menuOption menu option to select
   */
  public void testAvailableMenuOptionRegister(final MenuHasTestId menuOption, final String expected) {
    final Boolean shouldBeAvailable = "is".equals(expected); // String "is" or "is not"
    final boolean isAvailable = base.getDriver().findElements(base.id(menuOption.getTestId())).size() > 0;
    assertTrue("Menu option with ID '" + menuOption + "' " + expected + " available", shouldBeAvailable == isAvailable);
  }

  /**
   * Selects the given option from a theme switch in the Calculator
   * @param menuOption menu option to select
   */
  public void selectCalculatorThemeSwitchOption(final String themeSwitchOption) {
    selectThemeSwitchOption(themeSwitchOption, TestID.MENU_ITEMS + "-" + TestID.MENU_ITEM_CALCULATOR_THEME);
  }

  private void selectThemeSwitchOption(final String themeSwitchOption, final String themeSwitchId) {
    base.buttonHover(themeSwitchId);

    int counter = 0;

    while (counter < 3) {
      final WebElement webElement = base.buttonHover(themeSwitchId).findElement(By.tagName("div"));
      LOG.debug("current theme: " + webElement.getText());

      if (webElement.getText().equals(themeSwitchOption)) {
        break;
      } else {
        base.buttonClick(themeSwitchId);
        base.browserWaitForDrawAnimation();
      }
      counter++;
    }
  }

  /**
   * Enum product specific interfaces should implement this class.
   */
  public interface MenuHasTestId {
    String getTestId();
  }
}
