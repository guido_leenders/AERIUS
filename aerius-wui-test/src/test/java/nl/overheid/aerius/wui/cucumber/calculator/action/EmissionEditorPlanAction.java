/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for the emission values editor for "plans".
 */
public class EmissionEditorPlanAction extends SourceAction {

  public EmissionEditorPlanAction(final FunctionalTestBase testRun) {
    super(testRun);
  }

  /**
   * Sets the name of the plan.
   * @param planName name of the plan
   */
  public void setName(final String planName) {
    base.inputSetValue(TestID.INPUT_PLAN_NAME, planName);
  }

  /**
  * Sets the category of the plan.
  * @param planCategory category of the plan
  */
  public void setCategory(final String planCategory) {
    base.inputSelectListItem(TestID.INPUT_PLAN_CATEGORY, planCategory, true);
  }

   /**
   * Sets the amount of the plan.
   * @param planAmount amount of the plan
   */
  public void setAmount(final String planAmount) {
    base.inputSetValue(TestID.INPUT_PLAN_AMOUNT, planAmount);
  }

  /**
  * Saves the plan item by clicking the Save-button.
  */
  public void clickSaveItem() {
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  /**
   *  Sets the plan data by setting multiple fields.
   *  @param planName name of the plan
   *  @param planCategory category of the plan
   *  @param planAmount amount of the plan
   */
  public void setPlanData(final String planName, final String planCategory, final String planAmount) {
    if (!planName.isEmpty()) {
      setName(planName);
    }
    if (!planCategory.isEmpty()) {
      setCategory(planCategory);
    }
    if (!planAmount.isEmpty()) {
      setAmount(planAmount);
    }
    clickSaveItem();
  }

}
