/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.SearchAction;

import cucumber.api.java.en.When;

/**
 * Shared Step definition class for Search box.
 */
public class SearchStepDef {

  private final FunctionalTestBase base;
  private final SearchAction search;

  public SearchStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    search = new SearchAction(base);
  }

  @When("^the direct search for '(.+)' is started$")
  public void when_direct_search_for(final String searchString) {
    search.searchDirectlyFor(searchString);
  }

  @When("^the search box is cleared'$")
  public void when_search_box_is_cleared() {
    search.clearSearch();
    search.submitSearchClick();
  }

  @When("^the search is started$")
  public void when_search_button_clicked() {
    search.submitSearchClick();
  }

  @When("^the search box is entered '(.+)'$")
  public void when_search_enter_string(final String searchString) {
    search.setSearchInput(searchString);
  }

  @When("^the search suggestion '(.+)' is expanded$")
  public void when_search_expand_suggestion(final String searchSuggestion) {
    search.expandSearchSuggestion(searchSuggestion);
  }

  @When("^the search for '(.+)' is started$")
  public void when_search_for(final String searchString) {
    search.searchFor(searchString);
  }

  @When("^the search for coordinates X (.+) and Y (.+) is started$")
  public void when_search_for(final String xCoordinate, final String yCoordinate) {
    search.searchCoordinatesXy(xCoordinate, yCoordinate);
  }

  @When("^the search suggestion '(.+)' is selected$")
  public void when_search_select_suggestion(final String searchSuggestion) {
    search.selectSearchSuggestion(searchSuggestion);
  }
}
