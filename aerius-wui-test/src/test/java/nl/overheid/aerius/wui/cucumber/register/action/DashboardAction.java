/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.action;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains methods specifically for actions on the Register dashboard page
 */
public class DashboardAction {
  private static final Logger LOG = LoggerFactory.getLogger(DashboardAction.class);

  private final FunctionalTestBase base;

  public DashboardAction(final FunctionalTestBase testRun) {
    this.base = testRun;
  }

  /**
   * Click the import button on the dashboard to open the file import dialog
   */
  public void buttonClickImport() {
    base.buttonClick(TestIDRegister.BUTTON_NEW_APPLICATION);
    base.waitForElementClickable(base.waitForElementVisible(base.id(TestIDRegister.INPUT_DOSSIER_ID)), true);
  }

  /**
   * Toggles check box in dashboard by sending a mouse click event
   */
  public void checkBoxClickToggle(final WebElement checkBox, final Boolean enable) {
    if ((checkBox.isSelected() && !enable) || (!checkBox.isSelected() && enable)) {
      checkBox.click();
    }
  }

  /**
   * Toggles check box in dashboard by sending a space key input
   */
  public void checkBoxKeyToggle(final WebElement e, final Boolean enable) {
    if ((e.isSelected() && !enable) || (!e.isSelected() && enable)) {
      e.sendKeys(Keys.SPACE);
    }
  }

  /**
   * Checks presence of nature area in overview table
   */
  public void eCheckAreaPresence(final String areaName) {
    LOG.debug("Checking presence of specific Permit Province: " + areaName);
    final String xpath = "//div[contains(@class, 'gwt-Label') and contains(.,'" + areaName + "')]";
    final List<WebElement> areaList = base.waitForElementList(base.xpath(xpath));
    Assert.assertTrue("Overview contains less than 1 Permit Province!", areaList.size() > 0);
  }

  public void eCheckStatusPresence(final String areaStatus) {
    LOG.debug("Checking presence of area status indication: " + areaStatus);
    final List<WebElement> areaList = base.waitForElementList(base.xpath("//div[contains(text(),'" + areaStatus + "')]"));
    LOG.debug("Found {} areas with status indication: '{}'", areaList.size(), areaStatus);
  }

}
