/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;
import java.time.Duration;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.shared.domain.importer.ImportType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.ImportAction;

public class ImportStepDef {

  private final FunctionalTestBase base;
  private final ImportAction importFile;

  public ImportStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    importFile = new ImportAction(base);
  }

  @When("^the user imports file '(.+)'$")
  public void the_user_imports_file(final String filename) {
    importFile.importFile(filename, ImportType.determineByFilename(filename));
  }

  @When("^the GML file '(.+)' is imported$")
  public void when_file_import_gml(final String filename) {
    importFile.importFile(filename, ImportType.GML);
  }

  @When("^the GML file '(.+)' with an old calculation option is imported$")
  public void when_file_import_wrong_gml(final String filename) {
    importFile.importFileWrong(filename, ImportType.GML);
  }

  @When("^the PDF file '(.+)' is imported$")
  public void when_file_import_paa(final String filename) {
    importFile.importFile(filename, ImportType.PAA);
  }

  @When("^the ZIP file '(.+)' is imported$")
  public void when_file_import_zip(final String filename) {
    importFile.importFile(filename, ImportType.ZIP);
  }

  @When("^the CSV file '(.+)' is imported$")
  public void when_file_import_srm2(final String filename) {
    importFile.importFile(filename, ImportType.SRM2);
  }

  @When("^the BRN file '(.+)' is imported$")
  public void when_file_import_brn(final String filename) {
    importFile.importSelectFile(filename, ImportType.BRN);
  }

  @When("^the BRN input substance is set to '(.+)'$")
  public void when_file_import_brn_set_substance(final String substance) {
    base.buttonClick(TestID.RADIO_IMPORT_SUBSTANCE + "-" + substance.toLowerCase() + "-label");
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
    base.browserWait(Duration.ofSeconds(5));
  }

  @When("^the BRN file is imported despite invalid records$")
  public void file_import_brn_despite_invalid_records() {
    base.waitForElementVisible(base.xpath("//div[contains(.,'Op regel 4 van het bestand bevat kolom')]"));
    base.waitForElementVisible(base.xpath("//div[contains(.,'De ongeldige waarde is: ')]"));

    base.buttonClick(TestID.CHECKBOX_IMPORT_ERRORCONTINUE);
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);

    base.browserWaitForPopUpPanel();
  }

  @When("^an import is started$")
  public void when_file_start_import() {
    base.buttonClick(TestID.TOOLBOX_SOURCE_PREFIX + "-" + TestID.TOOLBOX + "-" + TestID.BUTTON_TOOLBOX_CUSTOM);
  }

  @Then ("^the import dialog panel contains the message '(.+)'$")
  public void dialog_panel_warning_message(final String message) {
    base.waitForElementVisible(base.xpath("//div[contains(., '" + message + "')]"));
  }

  @And ("^the option to continue the import is selected$")
  public void continue_import() {
    base.buttonClick(TestID.CHECKBOX_IMPORT_ERRORCONTINUE);
    base.buttonClick(TestID.BUTTON_DIALOG_CONFIRM);
  }
}
