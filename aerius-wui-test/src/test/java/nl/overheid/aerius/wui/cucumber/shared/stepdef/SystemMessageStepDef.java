/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import java.io.IOException;
import java.net.MalformedURLException;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.SystemMessageAction;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SystemMessageStepDef {

  private final FunctionalTestBase base;
  private final SystemMessageAction systemMessage;

  public SystemMessageStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    systemMessage = new SystemMessageAction(base);
  }

  @Given("^the system message entry page is opened$")
  public void the_system_message_entry_page_is_opened() throws InterruptedException, MalformedURLException {
    systemMessage.openSystemMessageForm();
  }

  @Given("^the administrator enters message text '(.+)'$")
  public void the_administrator_enters_message_text(final String messageText) {
    systemMessage.enterMessageText(messageText);
  }

  @Given("^the administrator enters locale '(.+)'$")
  public void the_administrator_enters_locale(final String messageLocale) {
    systemMessage.enterMessageLocale(messageLocale);
  }

  @When("^the administrator enters UUID '(.+)'$")
  public void the_administrator_enters_UUID(final String messageUUID) {
    systemMessage.enterMessageUUID(messageUUID);
  }

  @When("^the administrator ticks the delete all messages check box$")
  public void the_administrator_ticks_delete_all() {
    systemMessage.deleteAllMessages();
  }

  @When("^the system message is submitted$")
  public void the_system_message_is_submitted() {
    systemMessage.submitSystemMessage();
  }

  @Then("^the system message confirmation contains an error$")
  public void the_system_message_confirmation_contains_error() {
    systemMessage.checkConfirmationError();
  }

  @Then("^the system message confirmation is shown$")
  public void the_system_message_confirmation_is_shown() {
    systemMessage.checkConfirmationSuccess();
  }

  @Then("^the system message is shown with text '(.+)'$")
  public void the_system_message_is_shown_with_text(final String message) {
    systemMessage.checkMessageTextPresence(message);
  }

  @Then("^the system message is not shown$")
  public void the_system_message_is_not_shown() {
    systemMessage.checkMessageTextAbsence();
  }

  @Then("^the system message is shown with link text '(.+)' pointing to url '(.+)'$")
  public void the_system_message_is_shown_with_link(final String linkText, final String linkUrl) {
    systemMessage.checkLinkTextAndUrl(linkText, linkUrl);
  }

}
