/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.shared.stepdef;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.shared.action.NotificationAction;

public class NotificationStepDef {

  private final FunctionalTestBase base;
  private final NotificationAction notificationPanel;

  public NotificationStepDef() {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    notificationPanel = new NotificationAction(base);
  }

  @When("^the notifications panel is opened$")
  public void then_notification_panel_is_opened() {
    notificationPanel.open();
  }

  @When("^the notifications panel is closed$")
  public void then_notification_panel_is_closed() {
    notificationPanel.close();
  }

  @When("^the notifications are all deleted$")
  public void then_notification_panel_delete_all() {
    notificationPanel.deleteAllNotifications();
  }

  @Then("^the notification panel contains no errors$")
  public void then_notification_contains_no_erros() {
    notificationPanel.checkNotificationForErrors();
  }

  @Then("^the notification panel contains the message '(.+)'$")
  public void then_notification_contains_message(final String message) {
    // TODO: some notification messages contain two single quotes which should have already been replaced
    // replacing here allows double instead of single quotes in the feature files without parsing errors
    notificationPanel.waitForNotificationMessage(message.replace("\"", "'"));
  }

  @Then("^there are no validation errors$")
  public void there_are_no_validation_errors() {
    notificationPanel.checkForValidationErrors();
  }

}
