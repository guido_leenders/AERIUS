/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import org.openqa.selenium.By;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for the emission values editor for road-sectors.
 */
public class EmissionValuesEditorRoadAction extends SourceAction {

  public EmissionValuesEditorRoadAction(final FunctionalTestBase testRun) {
    super(testRun);
  }

  /**
   * Enumeration of all usable types of OffRoad Mobile Sources.
   */
  public enum OffRoadMobileSourceType {
    // Stage-class vehicles
    STAGE_CLASS,
    // Custom class vehicles
    DEVIATING_CLASS;
  }

  /**
   * Enumeration of all usable types of OffRoad Mobile Sources.
   */
  public enum OnRoadSourceType {
    // Standard vehicles
    STANDARD,
    // Euro class vehicles
    EURO;
  }

  /**
   * Sets the Max Speed for traffic.
   * @param maxSpeed the maximum allowed speed
   *
   */
  public void setOnRoadMaxSpeed(final String maxSpeed) {
    base.inputSelectListItem(TestID.LIST_MAXSPEED, maxSpeed, true);
  }

  /**
   * Sets the Stagnation Factor traffic.
   * @param stagnationFactor the stagnation factor
   */
  public void setOnRoadStagnationFactor(final String stagnationFactor) {
    base.inputSetValue(TestID.INPUT_STAGNATION, stagnationFactor);
  }

  /**
   * Sets the name for an offroad item stage_class.
   * @param mobileSourceName name/label of the source
   */
  public void setOffRoadStageItemName(final String mobileSourceName) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_NAME, mobileSourceName);
  }

  /**
   * Sets the category for an offroad item stage_class.
   * @param mobileSourceCategory category of the source
   */
  public void setOffRoadStageItemCategory(final String mobileSourceCategory) {
    base.inputSelectListItem(TestID.INPUT_MOBILESOURCE_CATEGORY, mobileSourceCategory, true);
  }

  /**
   * Sets the fuel amount for an offroad item stage_class.
   * @param fuelAmount fuel of the source
   */
  public void setOffRoadStageItemFuelAmount(final String fuelAmount) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_FUELAMOUNT, fuelAmount);
  }

  /**
   * Sets multiple fields in a record for a mobile source OffRoad.
   * Precondition: Application displays source details.
   * Postcondition: Application entered source-data and shows sources-overview
   * @param mobileSourceName name of the mobile source
   * @param mobileSourceCategory category of mobile source
   * @param fuelAmount fuelAmount amount of fuel used by given type
   */
  public void setOffRoadStageItem(final String mobileSourceName, final String mobileSourceCategory, final String fuelAmount) {

    selectOffRoadItemType(OffRoadMobileSourceType.STAGE_CLASS);

    if (!mobileSourceName.isEmpty()) {
      setOffRoadStageItemName(mobileSourceName);
    }
    if (!mobileSourceCategory.isEmpty()) {
      setOffRoadStageItemCategory(mobileSourceCategory);
    }
    if (!fuelAmount.isEmpty()) {
      setOffRoadStageItemFuelAmount(fuelAmount);
    }
  }

  /**
   * Sets the name for an offroad custom class item.
   * @param mobileSourceName name/label
   */
  public void setOffRoadCustomItemName(final String mobileSourceName) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_NAME, mobileSourceName);
  }

  /**
   * Sets the height for an offroad custom class item.
   * @param height ops height value
   */
  public void setOffRoadCustomItemHeight(final String height) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_HEIGHT, height);
  }

  /**
   * Sets the spread for an offroad custom class item.
   * @param spread ops spread value
   */
  public void setOffRoadCustomItemSpread(final String spread) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_SPREAD, spread);
  }

  /**
   * Sets the heat for an offroad custom class item.
   * @param heat ops heat value
   */
  public void setOffRoadCustomItemHeat(final String heat) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_HEAT, heat);
  }

  /**
   * Sets the emission for an offroad custom class item.
   * @param emission ops emission value
   */
  public void setOffRoadCustomItemEmission(final String emission) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_EMISSION, emission);
  }

  /**
   * Selects the type/class for an offroad item.
   * @param sourceType {@link OffRoadMobileSourceType}
   */
  public void selectOffRoadItemType(final OffRoadMobileSourceType sourceType) {
    switch (sourceType) {
      case STAGE_CLASS:
        base.waitForElementVisible(base.id(TestID.INPUT_MOBILESOURCE_TYPE_TOGGLE + "-STAGE_CLASS")).findElement(By.tagName("label")).click();
        break;
      case DEVIATING_CLASS:
        base.waitForElementVisible(base.id(TestID.INPUT_MOBILESOURCE_TYPE_TOGGLE + "-DEVIATING_CLASS")).findElement(By.tagName("label")).click();
        break;
      default:
        base.waitForElementVisible(base.id(TestID.INPUT_MOBILESOURCE_TYPE_TOGGLE + "-STAGE_CLASS")).findElement(By.tagName("label")).click();
        break;
    }
  }

  /**
   * Clicks the Save/Next step button to save an item.
   */
  public void clickSaveItem() {
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  /**
   * Sets multiple fields in a record for a CUSTOM mobile source.
   * Precondition: Application displays source details.
   * Postcondition: Application entered source-data and shows sources-overview.
   * @param sourceName name of the mobile source
   * @param height OPS emission height of mobile source
   * @param spread OPS spread of mobile source
   * @param heat OPS heat of mobile source
   * @param emission emission of mobile source
   */
  public void setOffRoadCustomItem(final String sourceName, final String height, final String spread, final String heat, final String emission) {

    selectOffRoadItemType(OffRoadMobileSourceType.DEVIATING_CLASS);
    if (!sourceName.isEmpty()) {
      setOffRoadCustomItemName(sourceName);
    }
    if (!height.isEmpty()) {
      setOffRoadCustomItemHeight(height);
    }
    if (!spread.isEmpty()) {
      setOffRoadCustomItemSpread(spread);
    }
    if (!heat.isEmpty()) {
     setOffRoadCustomItemHeat(heat);
    }
    if (!emission.isEmpty()) {
     setOffRoadCustomItemEmission(emission);
    }
  }

  /**
   * Sets the type of specification
   * @param type type of vehicles
   */
  public void setOnRoadSpecificationType(final OnRoadSourceType specificationType) {
    switch (specificationType) {
      case STANDARD:
        base.waitForElementVisible(base.id(TestID.ROAD_TOGGLE_BUTTON_STANDARD)).findElement(By.tagName("label")).click();
        break;
      case EURO:
        base.waitForElementVisible(base.id(TestID.ROAD_TOGGLE_BUTTON_EURO)).findElement(By.tagName("label")).click();
        break;
      default:
        base.buttonClick(TestID.ROAD_TOGGLE_BUTTON_STANDARD);
        break;
    }
  }

  /**
   * Sets the category for standard specification
   * @param vehicleType String number-value indication the category from the dropdownlist
   */
  public void setOnRoadStandardCategory(final String vehicleType) {
    base.inputSelectListItem(TestID.LIST_VEHICLE_TYPE, vehicleType, true);
  }

  /**
   * Sets the category for EURO specification
   * @param sourceCategory category of euroclass
   */
  public void setOnRoadEuroCategory(final String sourceCategory) {
    base.inputSetValue(TestID.INPUT_MOBILESOURCE_CATEGORY, sourceCategory, false, true);
  }

  /**
   * Sets the amount of vehicles per day for an OnRoad item.
   * @param vehiclesPerDay String the value for the amount of fuel
   */
  public void setOnRoadVehiclesPerDay(final String vehiclesPerDay) {
    base.inputSetValue(TestID.INPUT_VEHICLES_PER_TIMEUNIT, vehiclesPerDay, false, true);
  }

}
