/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.CalculationPointAction;
import nl.overheid.aerius.wui.cucumber.calculator.action.CalculationPointDetermineAction;

import cucumber.api.java.en.When;

public class CalculationPointStepDef {

  private final FunctionalTestBase base;
  private final CalculationPointAction calcpointDetails;
  private final CalculationPointDetermineAction calcpointDetermine;

  public CalculationPointStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    calcpointDetails = new CalculationPointAction(base);
    calcpointDetermine = new CalculationPointDetermineAction(base);
  }

  @When("^the calculationpoint label is set to '(.+)'$")
  public void when_calcpoint_label_is_set(final String label) throws InterruptedException {
    calcpointDetails.setLabel(label);
  }

  @When("^the calculationpoint is saved$")
  public void when_calcpoint_is_saved() throws InterruptedException {
    calcpointDetails.clickSaveButton();
  }

  @When("^the calculationpoints are automatically determined within a range of (\\d+) km$")
  public void when_calcpoints_are_determined(final String determineCalcpointsBoundary) throws InterruptedException {
    calcpointDetermine.determineCalculationPointsAutomatically(determineCalcpointsBoundary);
  }

  @When("^the calculationpoint calculator is opened$")
  public void when_calcpoints_open_calculator() throws InterruptedException {
    calcpointDetermine.clickDetermineCalcPointsButton();
  }

  @When("^the calculationpoint calculator boundary is set to '(.+)'$")
  public void when_calcpoints_calculator_set_boundary(final String determineCalcpointsBoundary) throws InterruptedException {
    calcpointDetermine.setBoundary(determineCalcpointsBoundary);
  }

  @When("^the calculationpoint calculator starts calculation$")
  public void when_calcpoints_calculator_start_calculation() throws InterruptedException {
    calcpointDetermine.clickCalculateCalcPointsButton();
  }

  @When("^the calculationpoint calculator is closed$")
  public void when_calcpoints_calculator_close() throws InterruptedException {
    calcpointDetermine.clickConfirmButton();
  }

}
