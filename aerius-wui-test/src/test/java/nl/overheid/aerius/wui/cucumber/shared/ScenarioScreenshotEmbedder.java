/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.shared;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import cucumber.api.Scenario;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * Analyze the results after every scenario and capture a screenshot when status is failed.
 */
public class ScenarioScreenshotEmbedder implements ScenarioHandler {
  private FunctionalTestBase base;

  public ScenarioScreenshotEmbedder(final FunctionalTestBase testRun) {
    base = testRun;
  }

  @Override
  public void beforeScenario(final Scenario result) {
  }

  @Override
  public void afterScenario(final Scenario result) {
    if (result.isFailed()) {
      final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
      final byte[] screenshot = base.takeScreenshot(OutputType.BYTES);
      // Log the current URL and date/time but take out basic authentication credentials if present
      result.write("[" + timeStamp + "] Capture URL: " + base.getDriver().getCurrentUrl().replaceFirst("//.+@", "//"));
      result.embed(screenshot, "image/png");
    }
  }

}
