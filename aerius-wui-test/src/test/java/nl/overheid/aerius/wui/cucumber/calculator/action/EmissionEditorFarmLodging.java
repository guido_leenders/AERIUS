/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.action;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for the emission values editor for farming.
 * TODO: finish cleaning up this class (like the setCode() for RAV and BWL, which are already taken care of)
 */
public class EmissionEditorFarmLodging extends SourceAction {
  private static final Logger LOG = LoggerFactory.getLogger(EmissionEditorFarmLodging.class);

  public EmissionEditorFarmLodging(final FunctionalTestBase testRun) {
    super(testRun);
  }

  /**
   * Enumeration of all usable types of OffRoad Mobile Sources.
   */
  public enum FarmLodgingSourceType {
    // RAV-class RAV code
    STANDARD,
    // Custom class farm
    CUSTOM;
  }

  /**
   * {@link LodgingStackType} containing the different types of measure.
   */
  public enum LodgingStackType {
    ADDITIONAL_MEASURE,
    REDUCING_MEASURE,
    FODDER_MEASURE
  }

  /**
   * Sets an entire record for a RAV-code (code and amount).
   * Precondition: Application displays source details.
   * @param ravCode type of animals
   * @param bwlCode type of animal housing
   * @param ravAmount amount of animals
   */
  public void setRavCode(final String ravCode, final String bwlCode, final String ravAmount) {
    LOG.debug("Setting animal housing details RAV: '{}', BWL: '{}', amount: '{}'", ravCode, bwlCode, ravAmount);
    setCodeRAV(ravCode);
    setCodeBWL(bwlCode);
    setRavAmount(ravAmount);
  }

  /**
   * Sets an entire record for a Additional measures (code and amount).
   * Precondition: Application displays source details.
   * @param addCode additional measure, indicating type of additional measure
   * @param addAmount amount of animals
   */
  public void setAdditionalCode(final String addCode, final String addAmount) {
    LOG.debug("Setting additional measures code to '{}' and amount '{}'", addCode, addAmount);
    setMeasureSelect(LodgingStackType.ADDITIONAL_MEASURE);
    setAdditionalCode(addCode);
    setAdditionalAmount(addAmount);
  }

  /**
   * Sets an entire record for a Additional measures (code and amount).
   * Precondition: Application displays source details.
   * @param addCode additional measure, indicating type of additional measure
   * @param addAmount amount of animals
   */
  public void setReductiveCode(final String redCode) {
    LOG.debug("Setting reduction measures code to '{}'", redCode);
    base.waitForElementVisible(base.id(TestID.BUTTON_EDITABLETABLE_SUBMIT));
    setMeasureSelect(LodgingStackType.REDUCING_MEASURE);
    setReductiveCodeRow(redCode);
    LOG.debug("farming reductive code set; code: '" + redCode + "'");
  }

  /**
   * Sets an entire record for a custom lodge (factor and amount).
   * Precondition: Application displays source details.
   * @param ravDescription RAV-code,indicating type of animals
   * @param ravFactor emission factor
   * @param ravAmount amount of animals
   */
  public void setCustomLodge(final String ravDescription, final String emissionfactor, final String ravAmount) {
    LOG.debug("Setting custom lodge '{}' to factor '{}' and amount '{}'", ravDescription, emissionfactor, ravAmount);
    base.waitForElementVisible(base.id(TestID.BUTTON_EDITABLETABLE_SUBMIT));
    setLodgingSelect(TestID.FARM_TOGGLE_BUTTON, FarmLodgingSourceType.CUSTOM);
    setCustomLodgeCode(ravDescription);
    setCustomLodgeFactor(emissionfactor);
    setCustomLodgeAmount(ravAmount);
  }

  /**
   * Deletes a Additional-code row from Additional-list (lodging).
   * Precondition: Application displays source details.
   * @param row String row to delete
   */
  public void clickDeleteAdditional(final String row) {
    final List<WebElement> addRecordsCount = base.waitForElementList(base.xpath("//input[contains(@id, '-amount')]"));
    Assert.assertTrue("Expecting at least 1 input element, found 0!", addRecordsCount.size() >= 1);
    final WebElement webElementCount = addRecordsCount.get(Integer.valueOf(row) - 1);
    webElementCount.click();
    base.mouseMoveToElement(webElementCount);
    base.browserWait(Duration.ofSeconds(1));

    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//button[contains(@id, '-delete')]"));
    Assert.assertTrue("Expecting at least 1 button element, found 0!", ravRecords.size() >= 1);
    final WebElement webElement = ravRecords.get(Integer.valueOf(row) - 1);

    base.buttonClick(webElement);
    base.browserWait(Duration.ofSeconds(1));
  }

  /**
   * Deletes a reduction-code row from Reductive-list (lodging).
   * Precondition: Application displays source details.
   * @param row String row to delete
   */
  public void clickDeleteReductive(final String row) {
    final List<WebElement> addRecordsCount = base.waitForElementList(base.xpath("//input[contains(@id, '-description')]"));
    final WebElement webElementCount = addRecordsCount.get(Integer.valueOf(row) - 1);
    webElementCount.click();
    base.mouseMoveToElement(webElementCount);

    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//button[contains(@id, '-delete')]"));
    final WebElement webElement = ravRecords.get(Integer.valueOf(row) - 1);

    base.buttonClick(webElement);
  }

  /**
   * Select given code from the RAV list box
   * @param code to set
   */
  public void setCodeRAV(final String code) {
    LOG.info("Selecting RAV code: '{}'", code);
    setCodeFakeSelect(base.buttonClick(TestID.LODGING + "-" + TestID.RAV_SUGGEST_BOX), code);
  }

  /**
   * Select given code from the BWL list box
   * @param code to set
   */
  public void setCodeBWL(final String code) {
    LOG.info("Selecting BWL code: '{}'", code);
    setCode(base.buttonClick(TestID.LODGING + "-" + TestID.BWL_SUGGEST_BOX), code);
  }

  /**
   * Sets the given value on the fake list box (non-native browser drop down selection) 
   * @param webElement
   * @param value
   */
  private void setCodeFakeSelect(final WebElement webElement, final String value) {
    // Select code string (if already present) and overwrite with new value
    webElement.sendKeys(Keys.chord(Keys.CONTROL, "a"), value);
    // Wait for fake select options pop up and click the value to be selected
    base.buttonClick(base.xpath("//div[starts-with(@title,'" + value + "')]"));
  }

  /**
   * Sets the given value on the provided {@link WebElement} 
   * @param webElement
   * @param value
   */
  private void setCode(final WebElement webElement, final String value) {
    base.inputSetValue(webElement, value, false, true);
    // send Keys to confirm suggestion for the best chance of success
    webElement.sendKeys(Keys.ARROW_DOWN);
    webElement.sendKeys(Keys.ARROW_UP);
    webElement.sendKeys(Keys.TAB);
  }

  /**
   * Set given value as RAV amount
   * @param value to set
   */
  public void setRavAmount(final String value) {
    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'lodging-amount')]"));
    final WebElement webElement = ravRecords.get(ravRecords.size() - 1);
    base.inputSetValue(webElement, value, false, true);
  }

  /**
   * Selects the additional measure type to select.
   * @param id id of element to set
   */
  public void setMeasureSelect(final LodgingStackType measureType) {
    switch (measureType) {
    case ADDITIONAL_MEASURE:
      base.inputSelectListItem(TestID.LODGING_STACK_TYPE, LodgingStackType.ADDITIONAL_MEASURE.name(), false);
      break;
    case REDUCING_MEASURE:
      base.inputSelectListItem(TestID.LODGING_STACK_TYPE, LodgingStackType.REDUCING_MEASURE.name(), false);
      break;
    case FODDER_MEASURE:
      base.inputSelectListItem(TestID.LODGING_STACK_TYPE, LodgingStackType.FODDER_MEASURE.name(), false);
      break;
    default:
      base.inputSelectListItem(TestID.LODGING_STACK_TYPE, LodgingStackType.ADDITIONAL_MEASURE.name(), false);
      break;
    }
  }

  /**
   * Selects the farm lodging type to select.
   * @param id id of element to set
   */
  public void setLodgingSelect(final String sourceType, final FarmLodgingSourceType farmLodingSourceType) {
    final String source = sourceType + "-" + FarmLodgingSourceType.CUSTOM.name().toLowerCase();
    final WebElement select = base.getElementByXPath("//*[contains(@id, '" + source + "')]/label[1]");
    select.click();
  }

  /**
   * Selects given value from the additional measure list box
   * @param value to set
   */
  public void setAdditionalCode(final String value) {
    final List<WebElement> addRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'description')]"));
    final WebElement webElement = addRecords.get(addRecords.size() - 1);
    base.inputSetValue(webElement, value, false, true);
    //send Keys to confirm suggestion
    webElement.sendKeys(Keys.ARROW_DOWN);
    webElement.sendKeys(Keys.ARROW_UP);
    webElement.sendKeys(Keys.TAB);
  }

  /**
   * Fills the value on Additional-suggestBox on the new row, without confirming it
   * @param value value to set
   */
  public void enterAdditionalCode(final String value) {
    final List<WebElement> addRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'description')]"));
    final WebElement webElement = addRecords.get(addRecords.size() - 1);
    webElement.sendKeys(value);
  }

  /**
   * Set given value as RAV amount.
   * @param value value to set
   */
  public void setAdditionalAmount(final String value) {
    final List<WebElement> addRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'amount')]"));
    final WebElement webElement = addRecords.get(addRecords.size() - 1);
    base.inputSetValue(webElement, value, false, true);
  }

  /**
   * Sets the value on Reductive-suggestBox on the new row.
   * @param id id of element to set
   * @param value value to set
   */
  public void setReductiveCodeRow(final String value) {
    final List<WebElement> redRecords = base.waitForElementList(base.xpath("//input[contains(@id, '-description')]"));
    final WebElement webElement = redRecords.get(redRecords.size() - 1);
    base.inputSetValue(webElement, value, false, true);
    webElement.sendKeys(Keys.ARROW_DOWN);
    webElement.sendKeys(Keys.ARROW_UP);
    webElement.sendKeys(Keys.TAB);
  }

  /**
   * Fills the value on reduction measure list box without confirming
   * @param value to set
   */
  public void enterReductiveCode(final String value) {
    final List<WebElement> redRecords = base.waitForElementList(base.xpath("//input[contains(@id, '-description')]"));
    final WebElement webElement = redRecords.get(redRecords.size() - 1);
    webElement.sendKeys(value);
  }

  /**
   * Fills the value on Fodder-suggestBox on the new row, without confirming it
   * @param value to set
   */
  public void enterFodderCode(final String value) {
    final List<WebElement> redRecords = base.waitForElementList(base.xpath("//input[contains(@id, '-description')]"));
    final WebElement webElement = redRecords.get(redRecords.size() - 1);
    webElement.sendKeys(value);
  }

  /**
   * Sets the value for 'description' on a record for a custom lodge.
   * @param value value to set on new row
   */
  public void setCustomLodgeCode(final String value) {
    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'custom-description')]"));
    final WebElement webElement = ravRecords.get(0);
    base.inputSetValue(webElement, value, false, true);
  }

  /**
   * Sets the value for 'factor' on a record for a custom lodge.
   * @param value value to set on new row
   */
  public void setCustomLodgeFactor(final String value) {
    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'custom-factor')]"));
    final WebElement webElement = ravRecords.get(0);
    base.inputSetValue(webElement, value, false, true);
  }

  /**
   * Sets the value for 'amount' on a record for a custom lodge.
   * @param value value to set on new row
   */
  public void setCustomLodgeAmount(final String value) {
    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//input[contains(@id, 'custom-amount')]"));
    final WebElement webElement = ravRecords.get(0);
    base.inputSetValue(webElement, value, false, true);
  }

  /**
   * Deletes a RAV-code row from RAV-list (lodging).
   * Precondition: Application displays source details.
   * @param row String row to delete
   */
  public void clickDeleteRavRow(final String row) {
    final List<WebElement> ravRecordsCount = base.waitForElementList(base.xpath("//input[contains(@id, 'lodging-amount')]"));
    final WebElement webElementCount = ravRecordsCount.get(Integer.valueOf(row) - 1);
    webElementCount.click();
    base.mouseMoveToElement(webElementCount);

    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//button[contains(@id, 'lodging-delete')]"));
    final WebElement webElement = ravRecords.get(Integer.valueOf(row) - 1);
    base.buttonClick(webElement);
  }

  /**
   * Deletes custom lodge row from custom list.
   * Precondition: Application displays source details.
   * @param row String row to delete
   */
  public void clickDeleteCustomLodgeRow(final String row) {
    final List<WebElement> ravRecordsFocus = base.waitForElementList(base.xpath("//input[contains(@id, 'custom-factor')]"));
    final WebElement webElementFocus = ravRecordsFocus.get(Integer.valueOf(row) - 1);
    webElementFocus.click();
    base.mouseMoveToElement(webElementFocus);

    final List<WebElement> ravRecords = base.waitForElementList(base.xpath("//button[contains(@id, 'custom-delete')]"));
    final WebElement webElement = ravRecords.get(Integer.valueOf(row) - 1);
    base.buttonClick(webElement);
  }

}
