/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;

/**
 * This class contains all the methods that apply for the applications startup-page.
 */
public class StartUpAction {

  protected final FunctionalTestBase base;

  public StartUpAction(final FunctionalTestBase testRun) {
    base = testRun;
  }

/**
 * Starts source input from startup view.
 * Precondition: Application displays startup view.
 * Postcondition: Application displays overview.
 */
  public void startInputSource() {
    base.buttonClick(TestID.BUTTON_INPUT_SOURCES);
  }

 /**
 * Starts import file from startup view.
 * Precondition: Application displays startup view.
 * Postcondition: Application displays import dialog.
 */
  public void startImportFile() {
    base.buttonClick(TestID.BUTTON_IMPORT);
  }

}
