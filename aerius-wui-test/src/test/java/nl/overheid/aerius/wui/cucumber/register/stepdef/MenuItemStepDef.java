/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.io.IOException;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.RegisterMenuOption;
import nl.overheid.aerius.wui.cucumber.register.action.RegisterMenuOption.RegisterSubMenuOption;
import nl.overheid.aerius.wui.cucumber.shared.action.MenuAction;

import cucumber.api.java.en.When;

public class MenuItemStepDef {

  private final FunctionalTestBase base;
  private final MenuAction menuItem;

  public MenuItemStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    menuItem = new MenuAction(base);
  }

  /*
   * Introducing one menu item action step to rule them all (or most)
   * Spaces are converted to underscores for convenience & readability
   */
  @When("^the menu item '(.+)' is selected$")
  public void when_menu_item_selected(final String item) throws InterruptedException {
    String itemName = item.replace(" ", "_");
    menuItem.selectMenuOption(RegisterMenuOption.valueOf(itemName.toUpperCase()));
  }

  /*
   * Selects the menu tab assessment in the applications window 
   */
  @When("^the applications tab -assessment- is selected$")
  public void when_applications_tab_assessment_selected() throws InterruptedException {
    menuItem.selectMenuOption(RegisterSubMenuOption.APPLICATION_PERMIT_RULES);
  }

  @When("^the menu item -prioritary projects- (.+) available$")
  public void menu_item_prio_projects_available(final String expected) throws InterruptedException {
    menuItem.testAvailableMenuOptionRegister(RegisterMenuOption.PRIORITARY_PROJECTS, expected);
  }

  @When("^the menu item -user management- (.+) available$")
  public void menu_item_user_management_available(final String expected) throws InterruptedException {
    menuItem.testAvailableMenuOptionRegister(RegisterMenuOption.USER_MANAGEMENT, expected);
  }

  @When("^the menu item -role management- (.+) available$")
  public void menu_item_role_management_available(final String expected) throws InterruptedException {
    menuItem.testAvailableMenuOptionRegister(RegisterMenuOption.ROLE_MANAGEMENT, expected);
  }

  @When("^the application menu item -development space- is selected$")
  public void register_application_select_submenu_development_space() throws InterruptedException {
    menuItem.selectSubMenuOptionRegister(RegisterSubMenuOption.APPLICATION_DEVELOPMENT_SPACE);
  }

  @When("^the application menu item -details- is selected$")
  public void register_application_select_submenu_dossier_details() throws InterruptedException {
    menuItem.selectSubMenuOptionRegister(RegisterSubMenuOption.APPLICATION_DETAILS);
  }

  @When("^the application menu item -permit rules- is selected$")
  public void register_application_select_submenu_permit_rules() throws InterruptedException {
    menuItem.selectSubMenuOptionRegister(RegisterSubMenuOption.APPLICATION_PERMIT_RULES);
  }

  @When("^the application menu item -dossier- is selected$")
  public void register_application_select_submenu_dossier_information() throws InterruptedException {
    menuItem.selectSubMenuOptionRegister(RegisterSubMenuOption.APPLICATION_DOSSIER);
  }

  @When("^the mouse hovers on button '(.+)'$")
  public void mouse_hover_on_button(final String button) throws InterruptedException {
    if ("Download".equals(button)) {
      base.buttonHover(TestIDRegister.BUTTON_DOWNLOAD_ANNEX_APPLICATION);
    }
  }

}
