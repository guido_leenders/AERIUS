/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.calculator.stepdef;

import java.io.IOException;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.calculator.action.EmissionEditorGeneric;

import cucumber.api.java.en.When;

public class EmissionGenericStepDef {

  private final FunctionalTestBase base;
  private final EmissionEditorGeneric sourceEmission;

  public EmissionGenericStepDef() throws IOException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    sourceEmission = new EmissionEditorGeneric(base);
  }

  @When("^the source emission is set to NH3 (.+), NOX (.+), PM10 (.+)$")
  public void when_source_emission_is_set(final String nh3, final String nox, final String pm10) {
    sourceEmission.setGenericEmissions(nh3, nox, pm10);
  }

  @When("^the source is added a new sub item$")
  public void when_source_subitem_is_added() {
    sourceEmission.clickAddNewEmissionItem();
  }

  @When("^the source is selected to edit")
  public void when_the_source_is_selected_to_edit() {
    sourceEmission.clickEditSource();
  }

  @When("^the source with label '(.+)' is selected to edit$")
  public void the_source_with_label_is_edited(final String label) {
    sourceEmission.clickEditEmissionItemByLabel(label);
  }

  @When("^the source sub item with label '(.+)' is selected$")
  public void the_source_sub_item_with_label_is_selected(final String label) {
    sourceEmission.clickEditEmissionItemByLabel(label);
  }

  @When("^the source sub item with label '(.+)' is only selected$")
  public void the_source_sub_item_with_label_is_selected_only(final String label) {
    sourceEmission.clickSelectEmissionItemByLabel(label);
  }

  @When("^the source sub item with label '(.+)' is selected to edit$")
  public void the_source_sub_item_with_label_is_edited(final String label) {
    sourceEmission.clickEditEmissionItemByLabel(label);
  }

  @When("^the source sub item with label '(.+)' is selected to copy$")
  public void the_source_sub_item_with_label_is_copied(final String label) {
    sourceEmission.clickCopyEmissionItemByLabel(label);
  }

  @When("^the source sub item with label '(.+)' is selected and deleted$")
  public void the_source_sub_item_with_label_is_deleted(final String label) {
    sourceEmission.clickDeleteEmissionItemByLabel(label);
  }

  @When("^the source sub item is saved$")
  public void when_the_sub_item_is_saved() {
    sourceEmission.clickSaveSubItem();
    base.browserWaitForDrawAnimation();
  }

  @When("^the source sub item with label '(.+)' emission should be '(.+)'$")
  public void the_source_sub_item_check_emission(final String label, final String expectedValue) {
    sourceEmission.checkEmissionItemEmissionValue(label, expectedValue);
  }

  @When("^the source input field for OPS spread is absent$")
  public void the_source_input_field_spread_absent() {
    base.waitForElementInvisible(base.id(TestID.INPUT_MOBILESOURCE_SPREAD));
  }

  @When("^the source input field for OPS spread is present$")
  public void the_source_input_field_spread_present() {
    base.waitForElementVisible(base.id(TestID.INPUT_MOBILESOURCE_SPREAD));
  }

}
