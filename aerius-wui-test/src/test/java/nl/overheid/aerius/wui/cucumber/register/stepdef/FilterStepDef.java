/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package nl.overheid.aerius.wui.cucumber.register.stepdef;

import java.io.IOException;

import nl.overheid.aerius.shared.test.TestIDRegister;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.SeleniumDriverWrapper;
import nl.overheid.aerius.wui.cucumber.register.action.FilterAction;

import cucumber.api.java.en.When;

public class FilterStepDef {

  private final FunctionalTestBase base;
  private final FilterAction filter;

  public FilterStepDef() throws IOException, InterruptedException {
    base = new FunctionalTestBase(SeleniumDriverWrapper.getInstance());
    filter = new FilterAction(base);
  }

  /*
   * WHEN
   */

  @When("^the overview filter panel is toggled$")
  public void when_filter_panel_toggel_collapse() {
    filter.toggleFilterPanelCollapse();
  }

  @When("^the overview filter STATUS options are shown$")
  public void status_filter_options_shown() {
    filter.showStatusFilterOptionsApplicationStatus();
    base.browserWaitForDrawAnimation();
  }

  @When("^the overview filter SECTOR options are shown$")
  public void status_filter_sector_options_shown() {
    filter.showStatusFilterOptionsApplicationSector();
    base.browserWaitForDrawAnimation();
  }

  @When("^the overview filter STATUS is set to '(.+)'$")
  public void when_filter_set_status(final String applicationStatus) {
    filter.setFilterApplicationStatus(applicationStatus);
  }

  @When("^the overview filter LOCATION is set to '(.+)'$")
  public void when_filter_set_location(final String applicationLocation) {
    filter.setFilterApplicationLocation(applicationLocation);
  }

  @When("^the overview filter AUTHORITY is set to '(.+)'$")
  public void when_filter_set_authority(final String applicationAuthority) {
    filter.setFilterApplicationAuthority(applicationAuthority);
  }

  @When("^the overview filter AREA is set to '(.+)'$")
  public void when_filter_set_area(final String applicationArea) {
    filter.setFilterApplicationArea(applicationArea);
  }

  @When("^the overview filter SECTOR is set to '(.+)'$")
  public void when_filter_set_sector(final String applicationSector) {
    filter.setFilterApplicationSector(applicationSector);
  }

  @When("^the overview filter FROM DATE is set to '(.+)'$")
  public void when_filter_set_from_date(final String applicationFromDate) {
    filter.setFilterApplicationFromDate(applicationFromDate);
  }

  @When("^the overview filter TILL DATE is set to '(.+)'$")
  public void when_filter_set_till_date(final String applicationTillDate) {
    filter.setFilterApplicationTillDate(applicationTillDate);
  }

  @When("^the overview filter is applied$")
  public void when_filter_apply() {
    base.buttonClick(TestIDRegister.BUTTON_FILTER_APPLY);
  }

  @When("^the overview filter is reset$")
  public void when_filter_reset() {
    base.buttonClick(TestIDRegister.BUTTON_FILTER_RESET);
  }

}
