/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator.action;

import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase;
import nl.overheid.aerius.wui.cucumber.base.FunctionalTestBase.MapMouseClicks;
import nl.overheid.aerius.wui.cucumber.shared.action.MapAction;

/**
 * This class contains all the methods that apply for the emission values editor for shipping-sectors.
 */
public class EmissionValuesEditorShippingAction extends SourceAction {
  private static final Logger LOG = LoggerFactory.getLogger(EmissionValuesEditorShippingAction.class);

  public EmissionValuesEditorShippingAction(final FunctionalTestBase testRun) {
    super(testRun);
  }

  /**
   * Set the label/name for a new ship record.
   * @param shipName label/name of the ship
   */
  public void setShipName(final String shipName) {
    base.inputSetValue(TestID.INPUT_SHIPPING_NAME, shipName);
  }

  /**
   * Set the category for a new ship record.
   * @param shipCategory category of the ship
   */
  public void setShipCategory(final String shipCategory) {
    base.inputSetValue(TestID.INPUT_SHIPPING_CATEGORY, shipCategory, false, true);
  }

  /**
   * Set the amount for a new ship record.
   * @param shipAmount amount of visits
   */
  public void setShipAmount(final String shipAmount) {
    base.inputSetValue(TestID.INPUT_SHIPPING_AMOUNT, shipAmount);
  }

  /**
   * Set the residence time for a new ship record.
   * @param shipResidenceTime residence time of ships
   */
  public void setShipResidenceTime(final String shipResidenceTime) {
    base.inputSetValue(TestID.INPUT_SHIPPING_RESIDENCETIME, shipResidenceTime);
  }

  /**
   * Set multiple fields in a record for a ship (except the route).
   * Precondition: Application displays source details, of source category 'shipping', with an empty new row form.
   * @param shipName name (description) of ship
   * @param shipCategory category of the ship
   * @param shipAmount amount visits/ships
   * @param shipResidenceTime durations of visits
   */
  public void setShippingData(final String shipName, final String shipCategory, final String shipAmount, final String shipResidenceTime) {
    if (!shipName.isEmpty()) {
      setShipName(shipName);
    }
    if (!shipCategory.isEmpty()) {
      setShipCategory(shipCategory);
    }
    if (!shipAmount.isEmpty()) {
      setShipAmount(shipAmount);
    }
    if (!shipResidenceTime.isEmpty()) {
      setShipResidenceTime(shipResidenceTime);
    }
    clickSaveItem();
  }

  /**
   * Submit a new row with shipping data.
   * Precondition: Application displays sub item details.
   */
  public void clickSaveItem() {
    base.buttonClick(TestID.BUTTON_EDITABLETABLE_SUBMIT);
  }

  /**
   * Start entering a new route for a ship by clicking the 'new route' button.
   * Precondition: Application displays form for new shipping entry.
   */
  public void setNewInlandRoute() {
    base.buttonClick(TestID.BUTTON_SHIPPING_ROUTES);
    base.buttonClick(TestID.BUTTON_SHIPPING_NEWROUTE);
    new MapAction(base).clickOnMapCenter(MapMouseClicks.DOUBLE);
  }

  /**
   * Set a simple maritime route for a ship by clicking on the map.
   * Precondition: Application displays form for entering new ship record.
   */
  public void setNewMaritimeRoute() {
    base.buttonClick(TestID.BUTTON_SHIPPING_MARITIME_ROUTES);
    base.buttonClick(TestID.BUTTON_SHIPPING_NEWROUTE);
    new MapAction(base).clickOnMapCenter(MapMouseClicks.DOUBLE);
  }

  /**
   * Select an existing inland route from the list.
   * Precondition: Application displays form for entering new ship record.
   * @param routeLabel the label identifying the inland route entry
   */
  public void selectExistingInlandRoute(final String routeLabel) {
    base.buttonClick(TestID.BUTTON_SHIPPING_ROUTES);
    base.buttonClick(TestID.BUTTON_SHIPPING_EXISTINGROUTE + "-" + routeLabel);
  }

  /**
   * Select an existing maritime route from the list.
   * Precondition: Application displays form for entering new ship record.
   * @param routeLabel the label identifying the maritime route entry
   */
  public void selectExistingMaritimeRoute(final String routeLabel) {
    base.buttonClick(TestID.BUTTON_SHIPPING_MARITIME_ROUTES);
    base.buttonClick(TestID.BUTTON_SHIPPING_EXISTINGROUTE + "-" + routeLabel);
  }

  /**
   * Delete an existing shipping route.
   * Precondition: Application displays form for entering new ship record.
   * @param routeLabel label of the route to select
   */
  public void clickDeleteShippingItem(final String routeLabel) {
    base.buttonClick(TestID.BUTTON_SHIPPING_ROUTES);
    base.mouseMoveToElement(TestID.BUTTON_SHIPPING_EXISTINGROUTE + "-" + routeLabel);
    base.buttonClick(TestID.BUTTON_SHIPPING_DELETEROUTE + "-" + routeLabel);
    base.isElementDisplayed(TestID.BUTTON_SHIPPING_EXISTINGROUTE + "-" + routeLabel);
    base.buttonClick(TestID.BUTTON_SHIPPING_ROUTES);
  }

  public void setWaterwayClass(final String waterwayClass) {
    base.inputSelectListItemByText(base.id(TestID.INPUT_WATERWAY), waterwayClass);
  }

  public String getWaterwayClass() {
    final Select comboBox = new Select(base.waitForElementVisible(base.id(TestID.INPUT_WATERWAY)));
    final String selectedComboValue = comboBox.getFirstSelectedOption().getText();
    LOG.debug("Retrieved selected comboBox option: '{}'", selectedComboValue);
    return selectedComboValue;
  }

}
