/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.cucumber.calculator;

import org.junit.Ignore;
import org.junit.runner.RunWith;

import nl.overheid.aerius.wui.cucumber.base.CucumberBase;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * This is the runner class for NIGHTLY cucumber tests for the AERIUS MELDINGEN
 */
@Ignore
@RunWith (Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:target/cucumber/nightly/melding", "json:target/cucumber/nightly/melding/nightly_report.json"},
    features = "src/test/resources/melding",
    glue = {
        "nl.overheid.aerius.wui.cucumber.calculator.stepdef", 
        "nl.overheid.aerius.wui.cucumber.shared.action",
        "nl.overheid.aerius.wui.cucumber.shared.stepdef"
        },
    tags = {"@nightly"}
    )
public class MeldingCucumberTest extends CucumberBase {

}
