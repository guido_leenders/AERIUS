/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonUtil;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;

/**
 * A VectorFeature displaying a receptor as a hexagon.
 */
public class ReceptorFeature extends VectorFeature {
  private final AeriusPoint receptor;

  public ReceptorFeature(final AeriusPoint receptor, final HexagonZoomLevel zoomLevel) {
    super(Geometry.fromWKT(HexagonUtil.createHexagonWkt(receptor, zoomLevel)));
    this.receptor = receptor;
  }

  @Override
  public boolean equals(final Object obj) {
    return obj != null && this.getClass() == obj.getClass() ? receptor.getId() == ((ReceptorFeature) obj).receptor.getId() : false;
  }

  public AeriusPoint getReceptor() {
    return receptor;
  }

  @Override
  public int hashCode() {
    return receptor.getId();
  }
}
