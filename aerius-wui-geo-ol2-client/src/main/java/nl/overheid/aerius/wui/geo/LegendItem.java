/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.geo.shared.LayerProps.ImageLegend;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.LayerProps.TextLegend;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.wui.main.resources.R;

public class LegendItem extends Composite {
  private static final int NUM_COLUMNS = 3;
  private static LegendItemUiBinder uiBinder = GWT.create(LegendItemUiBinder.class);

  interface LegendItemUiBinder extends UiBinder<Widget, LegendItem> {}

  interface CustomStyle extends CssResource {
    String colorRangesTable();

    String colorRangesTextBlock();
  }

  @UiField CustomStyle style;

  @UiField FlowPanel container;
  @UiField Label title;
  @UiField SimplePanel item;

  private final MapLayer layer;

  public LegendItem(final MapLayer layer) {
    this.layer = layer;

    initWidget(uiBinder.createAndBindUi(this));

    final Legend legend = layer.getLegend();

    container.setVisible(legend != null);
    if (legend instanceof ColorRangesLegend) {
      item.setWidget(drawFull((ColorRangesLegend) legend));
    } else if (legend instanceof TextLegend) {
      item.setWidget(drawText((TextLegend) legend));
    } else if (legend instanceof ComparisonLegend) {
      item.setWidget(drawCompact((ComparisonLegend) legend));
    } else if (legend instanceof ReceptorLegend) {
      item.setWidget(drawCompact((ReceptorLegend) legend));
    } else if (legend instanceof ImagesLegend) {
      item.setWidget(((ImagesLegend) legend).getPanel());
    } else if (legend instanceof ImageLegend) {
      item.setWidget(drawImage((ImageLegend) legend));
    }
  }

  private Widget drawImage(final ImageLegend legend) {
    // Return wrapped img element
    return new HTMLPanel("<img src=\"" + legend.getImageUrl() + "\" />");
  }

  private Widget drawText(final TextLegend legend) {
    final Label label = new Label(legend.getText());
    label.setStyleName(R.css().textLegend());
    return label;
  }

  private Grid drawFull(final ColorRangesLegend legend) {
    final int rows = legend.getLegendNames().length;
    final Grid g = new Grid(rows, 2);
    g.addStyleName(style.colorRangesTable());

    for (int i = 0; i < rows; i++) {
      drawCell(g, legend, i, i, 0, 1);
    }
    return g;
  }

  private Grid drawCompact(final ColorRangesLegend legend) {
    final int rows = (int) Math.ceil(legend.size() / (double) NUM_COLUMNS);

    final Grid g = new Grid(rows, NUM_COLUMNS * 2);
    g.addStyleName(style.colorRangesTable());

    for (int i = 0; i < NUM_COLUMNS; i++) {
      for (int j = 0; j < rows; j++) {
        final int idx = rows * i + j;

        if (idx >= legend.size()) {
          break;
        }
        final int col1 = i * 2;
        drawCell(g, legend, idx, j, col1, col1 + 1);
      }
    }

    return g;
  }

  /**
   * Draw a single legend item
   *
   * @param g
   * @param legend
   * @param idx
   * @param row
   * @param col1 column of color
   * @param col2 column of text
   */
  private void drawCell(final Grid g, final ColorRangesLegend legend, final int idx, final int row, final int col1, final int col2) {
    g.setHTML(row, col1, SharedConstants.NBSP);
    if (legend.isHexagon()) {
      g.getCellFormatter().getElement(row, col1).addClassName(R.css().colorRangesColorHexagon());
    } else {
      g.getCellFormatter().getElement(row, col1).setClassName(R.css().colorRangesColorCircle());
    }
    g.getCellFormatter().getElement(row, col2).setClassName(style.colorRangesTextBlock());

    final Style s = g.getCellFormatter().getElement(row, col1).getStyle();
    s.setBackgroundColor(legend.getColors()[idx]);
    g.setHTML(row, col2, legend.getLegendNames()[idx]);
  }

  public MapLayer getLayer() {
    return layer;
  }

  public void refreshName() {
    item.setTitle(layer.getTitle());
  }

  public void setTitleText(final String legendTitle) {
    title.setText(legendTitle);
  }
}
