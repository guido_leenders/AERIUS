/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo.search;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.search.SearchAction;
import nl.overheid.aerius.wui.main.ui.search.SearchSuggestionTable;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

public class MapSearchSuggestionTable extends SearchSuggestionTable<MapSearchSuggestionType, MapSearchSuggestion> {
  interface MapSearchSuggestionTableUiBinder extends UiBinder<Widget, MapSearchSuggestionTable> {}

  private static final MapSearchSuggestionTableUiBinder UI_BINDER = GWT.create(MapSearchSuggestionTableUiBinder.class);

  public MapSearchSuggestionTable(final HelpPopupController hpC, final SearchAction<MapSearchSuggestionType, MapSearchSuggestion> searchAction) {
    super(hpC, searchAction);

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @Override
  protected SearchSuggestionTable<MapSearchSuggestionType, MapSearchSuggestion> createSuggestionTable(final HelpPopupController hpC, final SearchAction<MapSearchSuggestionType, MapSearchSuggestion> searchAction) {
    return new MapSearchSuggestionTable(hpC, searchAction);
  }

  @Override
  protected String getName(final MapSearchSuggestion object) {
    final StringBuilder bldr = new StringBuilder(object.getName());
    if (object.getDistance() != 0f) { // Province distance would always be 0, so don't add to label
      bldr.append(" - ");
      bldr.append(M.messages().unitKm(FormatUtil.toWhole(object.getDistance() / SharedConstants.M_TO_KM)));
    }

    if (!object.getSubItems().isEmpty()) {
      bldr.append(" (");
      bldr.append(object.getSubItems().size());
      bldr.append(")");
    }

    return bldr.toString();
  }
}
