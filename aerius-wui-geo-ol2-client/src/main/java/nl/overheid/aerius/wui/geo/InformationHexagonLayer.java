/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;

/**
 * Simple vector layer that draws the hexagon that represents a {@link AeriusPoint}.
 */
class InformationHexagonLayer extends Vector {
  // Style to give the hexagon
  private static final String INFO_BACKGROUND_COLOR = "#33F";
  private static final double INFO_OPACITY = 0.6d;
  private static final String INFO_STROKE_COLOR = "#FFF";
  private static final Style INFO_STYLE = new Style();
  static {
    INFO_STYLE.setFillColor(INFO_BACKGROUND_COLOR);
    INFO_STYLE.setFillOpacity(INFO_OPACITY);
    INFO_STYLE.setStrokeColor(INFO_STROKE_COLOR);
  }

  private final HexagonZoomLevel zoomLevel1;
  private final ReceptorUtil receptorUtil;

  /**
   * Create the information hexagon layer with the given name.
   *
   * @param name Name to give this layer (appended to a fixed value)
   */
  public InformationHexagonLayer(final String name, final HexagonZoomLevel zoomLevel1, final ReceptorUtil receptorUtil) {
    super("vector_" + name);
    this.zoomLevel1 = zoomLevel1;
    this.receptorUtil = receptorUtil;
  }

  /**
   * Set the marker layer to the given AeriusPoint.
   *
   * @param receptor the AeriusPoint to draw
   */
  public void drawReceptor(final AeriusPoint receptor) {
    // Attach the receptor to the grid (to make sure it's got a proper location)
    final AeriusPoint copy = receptor.copy();
    receptorUtil.attachReceptorToGrid(copy);

    // Destroy the previous hexagon, if any
    destroyFeatures();

    // Draw the hexagon vector
    final ReceptorFeature feature = new ReceptorFeature(copy, zoomLevel1);
    feature.setStyle(INFO_STYLE);
    addFeature(feature);

    // Redraw the layer, showing the hexagon
    redraw();
  }
}
