/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo.search;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AreaType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.geo.WKTGeometryWithBox;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.shared.service.MapSearchServiceAsync;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.search.SearchAction;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceCall;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceHandle;

/**
 * Search actions. Call the server for finding results and perform the action when a user selects a suggestion.
 */
@Singleton
public abstract class MapSearchAction implements SearchAction<MapSearchSuggestionType, MapSearchSuggestion> {

  // Simplify geometries we fetch for highlighting. We don't need more detail than 25m.
  private static final float HIGHLIGHT_GEOMETRY_RESOLUTION = 25.0f;

  // Start searching after at least this amount of characters are entered.
  private static final int SEARCH_BOX_AMOUNT_OF_CHARS_NEEDED = 3;

  // Regex used to identify whether a N2000 ID was entered as search term.
  private static final RegExp SEARCH_TERM_N2000_ID_REGEX = RegExp.compile("^[1-9][0-9]?[0-9]?$");

  // Regex used to identify whether a Receptor ID was entered as search term.
  private static final RegExp SEARCH_TERM_RECEPTOR_ID_REGEX = RegExp.compile("^[1-9][0-9]{0,7}$");

  // Regex used to identify whether an Rijksdriehoekstelsel XY coordinate was entered as search term.
  private static final RegExp SEARCH_TERM_COORDINATE_REGEX = RegExp
      .compile("^\\s*(x\\:)?\\s*([1-2]?\\d{5})\\s*[,;\\s]\\s*(y\\:)?\\s*([3-6]\\d{5})\\s*$", "i");
  // The relevant groups in the above regular expression that identify the X and Y coordinates respectively.
  private static final int SEARCH_TERM_COORDINATE_REGEX_GROUP_X = 2;
  private static final int SEARCH_TERM_COORDINATE_REGEX_GROUP_Y = 4;

  private static final String SEARCH_THREAD = "service_search";

  private final EventBus eventBus;
  private final ReceptorGridSettings gridSettings;
  private final ReceptorUtil receptorUtil;
  private final MapSearchServiceAsync searchService;
  private final FetchGeometryServiceAsync fetchGeometryService;

  private MapLayoutPanel map;

  private final HashMap<String, ConcurrentAsyncServiceHandle<?>> searchConcurrencyMap = new HashMap<>();

  public MapSearchAction(final EventBus eventBus, final ReceptorGridSettings gridSettings, final MapSearchServiceAsync searchService,
      final FetchGeometryServiceAsync fetchGeometryService) {
    this.eventBus = eventBus;
    this.gridSettings = gridSettings;
    this.searchService = searchService;
    this.fetchGeometryService = fetchGeometryService;
    receptorUtil = new ReceptorUtil(gridSettings);
  }

  public void setMap(final MapLayoutPanel map) {
    this.map = map;
  }

  /**
   * Returns whether the given search string is a meaningful search term, that should trigger some action. This is the case when there are at least 3
   * characters, or when a number (for N2000-id) is entered.
   *
   * @param searchText
   * @return True or false; when false the search box will not show the suggestions popup nor the search animation.
   */
  @Override
  public boolean isSearchable(final String searchText) {
    return (searchText != null) && !searchText.isEmpty()
        && ((searchText.length() >= SEARCH_BOX_AMOUNT_OF_CHARS_NEEDED) || SEARCH_TERM_N2000_ID_REGEX.test(searchText));
  }

  /**
   * Search for the given search string and return results via the callback.
   *
   * @param searchText text to search
   * @param callback
   */
  @Override
  public void search(final String searchText, final AsyncCallback<ArrayList<MapSearchSuggestion>> callback) {
    final ArrayList<MapSearchSuggestion> suggestions = new ArrayList<>();
    if (handleCoordinateEntered(searchText, suggestions)) {
      // We won't call the search service, as it would not yield any results.
      callback.onSuccess(suggestions); // Fake it.
    } else {
      handleReceptorIdEntered(searchText, suggestions);
      handleSearchService(searchText, suggestions, callback);
    }
  }

  private boolean handleCoordinateEntered(final String searchText, final ArrayList<MapSearchSuggestion> suggestions) {
    boolean handled = false;
    final MatchResult coordinateMatch = SEARCH_TERM_COORDINATE_REGEX.exec(searchText);
    if (coordinateMatch != null) {
      final int x = Integer.parseInt(coordinateMatch.getGroup(SEARCH_TERM_COORDINATE_REGEX_GROUP_X));
      final int y = Integer.parseInt(coordinateMatch.getGroup(SEARCH_TERM_COORDINATE_REGEX_GROUP_Y));
      suggestions.addAll(getCoordinateSuggestions(x, y, MapSearchSuggestionType.XY_COORDINATE));
      handled = true;
    }
    return handled;
  }

  private boolean handleReceptorIdEntered(final String searchText, final ArrayList<MapSearchSuggestion> suggestions) {
    boolean handled = false;
    final MatchResult receptorIdMatch = SEARCH_TERM_RECEPTOR_ID_REGEX.exec(searchText);
    if (receptorIdMatch != null) {
      final int id = Integer.parseInt(receptorIdMatch.getGroup(0));
      final AeriusPoint receptor = new AeriusPoint(id);
      receptorUtil.setAeriusPointFromId(receptor);
      if (gridSettings.getBoundingBox().isPointWithinBoundingBox(receptor)) {
        final int x = Math.round((float) receptor.getX());
        final int y = Math.round((float) receptor.getY());
        suggestions.addAll(getCoordinateSuggestions(x, y, MapSearchSuggestionType.RECEPTOR_ID));
        handled = true;
      }
    }
    return handled;
  }

  private ArrayList<MapSearchSuggestion> getCoordinateSuggestions(final int x, final int y, final MapSearchSuggestionType type) {
    final ArrayList<MapSearchSuggestion> searchSuggestions = new ArrayList<>();
    final MapSearchSuggestion suggestion = new MapSearchSuggestion(0, M.messages().infoPanelLocation(x, y), type);
    suggestion.setZoomBox(new BBox(x, y, 0));
    suggestion.setPoint(new Point(x, y));
    suggestion.setLazySubItems(true);
    searchSuggestions.add(suggestion);
    return searchSuggestions;
  }

  private void handleSearchService(final String searchText, final ArrayList<MapSearchSuggestion> additionalSuggestions,
      final AsyncCallback<ArrayList<MapSearchSuggestion>> callback) {
    // Wrap the callback so that we can return additional items generated at the client side.
    final AsyncCallback<ArrayList<MapSearchSuggestion>> usedCallback = new AsyncCallback<ArrayList<MapSearchSuggestion>>() {
      @Override
      public void onSuccess(final ArrayList<MapSearchSuggestion> result) {
        if (additionalSuggestions != null) {
          result.addAll(additionalSuggestions);
        }
        callback.onSuccess(result);
      }

      @Override
      public void onFailure(final Throwable caught) {
        callback.onFailure(caught);
      }
    };

    ConcurrentAsyncServiceUtil.register(SEARCH_THREAD, searchConcurrencyMap, usedCallback,
        new ConcurrentAsyncServiceCall<ArrayList<MapSearchSuggestion>>() {
          @Override
          public void execute(final AsyncCallback<ArrayList<MapSearchSuggestion>> callback) {
            searchService.getSuggestions(searchText, callback);
          }
        });
  }

  @Override
  public void selectSuggestion(final MapSearchSuggestion suggestion) {
    final AreaType areaType = suggestion.getType().toAreaType();

    if (areaType != null) {
      registerConcurrent(suggestion, areaType);
    }
    if (suggestion.getZoomBox() != null) {
      // We have a zoombox. Zoom right now while the geometry to draw is being fetched.
      eventBus.fireEvent(new LocationChangeEvent(suggestion.getZoomBox()));

      if ((map != null) && (suggestion.getType() == MapSearchSuggestionType.NATURE_AREA)) {
        map.setVisible(getProductType().getLayerName(SharedConstants.NATURE_AREAS), true);
      }
    }
  }

  private void registerConcurrent(final MapSearchSuggestion suggestion, final AreaType areaType) {
    ConcurrentAsyncServiceUtil.register(SEARCH_THREAD, searchConcurrencyMap, new AppAsyncCallback<WKTGeometryWithBox>() {
      @Override
      public void onSuccess(final WKTGeometryWithBox geometryWithBox) {
        if (geometryWithBox != null) {
          eventBus.fireEvent(new LocationChangeEvent(geometryWithBox.getBoundingBox(), geometryWithBox));
        } else {
          eventBus.fireEvent(new LocationChangeEvent(new BBox(suggestion.getPoint().getX(), suggestion.getPoint().getY(), 0)));
        }
      }
    }, new ConcurrentAsyncServiceCall<WKTGeometryWithBox>() {
      @Override
      public void execute(final AsyncCallback<WKTGeometryWithBox> callback) {
        if (suggestion.getPoint() == null) {
          fetchGeometryService.getSimplifiedGeometry(areaType, suggestion.getId(), HIGHLIGHT_GEOMETRY_RESOLUTION, suggestion.getName(), callback);
        } else {
          fetchGeometryService.getSimplifiedGeometry(areaType, suggestion.getPoint(), HIGHLIGHT_GEOMETRY_RESOLUTION, suggestion.getName(), callback);
        }
      }
    });
  }

  protected abstract ProductType getProductType();

  @Override
  public void getSubSuggestions(final MapSearchSuggestion parentSuggestion, final AsyncCallback<ArrayList<MapSearchSuggestion>> callback) {
    searchService.getSubSuggestions(parentSuggestion, callback);
  }

}
