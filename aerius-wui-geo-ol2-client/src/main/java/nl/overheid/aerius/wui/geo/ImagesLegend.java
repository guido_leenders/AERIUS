/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Base class for legends that have multiple images as legend items.
 */
public abstract class ImagesLegend implements Legend {

  private final String title;

  public ImagesLegend(final String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  /**
   * Returns the legend.
   * @return
   */
  public Widget getPanel() {
    final FlowPanel panel = new FlowPanel();
    panel.add(new Label(getTitle()));
    addImages(panel);
    return panel;
  }

  /**
   * Implementations should add the images of the legend in the implementation. The implementation should use {@link #getImageLegend(Image, String)}
   * to create new entries and add them to the the given panel parameter.
   * @param panel to add the images.
   */
  protected abstract void addImages(FlowPanel panel);

  protected Widget getImageLegend(final Image image, final String text) {
    final FlowPanel marker = new FlowPanel();
    marker.add(image);
    final Label label = new Label(text);
    label.getElement().setClassName(R.css().imagesLegendLabel());
    marker.add(label);
    return marker;
  }
}
