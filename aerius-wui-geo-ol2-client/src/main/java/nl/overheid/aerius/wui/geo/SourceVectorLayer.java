/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gwtopenmaps.openlayers.client.Style;
import org.gwtopenmaps.openlayers.client.StyleMap;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.layer.Layer;
import org.gwtopenmaps.openlayers.client.layer.Markers;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.wui.geo.MarkerLayerWrapper.MarkerContainer;

/**
 * Manages the vector layer with all Features related to source objects. This can be line, polygons, and representing e.g. shipping routes.
 */
class SourceVectorLayer {

  private final Vector vectorLayer;
  private final Markers markerLayer;
  private final List<MarkerContainer<EmissionSource>> containers = new ArrayList<>();

  private final Map<String, VectorFeature> eSVFMap = new HashMap<String, VectorFeature>();

  public SourceVectorLayer(final String name, final SourceMapPanel smp) {
    final MarkerContainer<EmissionSource> routeContainer = smp.getMartimeMooringRouteContainer();
    final MarkerContainer<EmissionSource> inlandShipMarkerContainer = smp.getInlandRouteContainer();
    vectorLayer = new Vector("source_" + name);
    markerLayer = new Markers("marker_" + name);
    vectorLayer.setStyleMap(StyleMap.narrowToOpenLayersStyleMap(DrawSourceStyleOptions.getDefaultStyleMap()));
    if (routeContainer != null) {
      routeContainer.init(vectorLayer, markerLayer);
    }
    if (inlandShipMarkerContainer != null) {
      inlandShipMarkerContainer.init(null, markerLayer);
    }
    containers.add(routeContainer);
    containers.add(inlandShipMarkerContainer);
  }

  public void add(final int eslId, final EmissionSource src) {
    if (src instanceof SRM2NetworkEmissionSource) {
      final Style customStyle = DrawSourceStyleOptions.getRoadSegmentStyle();
      final SRM2NetworkEmissionSource ev = (SRM2NetworkEmissionSource) src;

      remove(eslId, src);
      for (final EmissionSource es : ev.getEmissionSources()) {
        add(eslId, eslId + "_" + src.getId() + "_" + es.getId(), es, false, customStyle);
      }
    } else {
      add(eslId, eslId + "_" + src.getId(), src, true, null);
    }

    for (final MarkerContainer<EmissionSource> container : containers) {
      container.add(src);
    }
  }

  private void add(final int eslId, final String vfId, final EmissionSource src, final boolean remove, final Style customStyle) {
    if (remove) {
      remove(eslId, src);
    }

    if (src.getGeometry() != null && src.getGeometry().getType() != WKTGeometry.TYPE.POINT) {
      final VectorFeature feature = new VectorFeature(Geometry.fromWKT(src.getGeometry().getWKT()));
      if (customStyle != null) {
        feature.setStyle(customStyle);
      }
      vectorLayer.addFeature(feature);
      eSVFMap.put(vfId, feature);
    }
  }

  public Vector asLayer() {
    return vectorLayer;
  }

  public Layer asMarkerLayer() {
    return markerLayer;
  }

  public void clear() {
    vectorLayer.destroyFeatures();
    eSVFMap.clear();
    for (final MarkerContainer<EmissionSource> container : containers) {
      container.clear();
    }
  }

  public void remove(final int eslId, final EmissionSource src) {
    if (src instanceof SRM2NetworkEmissionSource) {
      final SRM2NetworkEmissionSource ev = (SRM2NetworkEmissionSource) src;

      for (final EmissionSource es : ev.getEmissionSources()) {
        remove(eslId + "_" + src.getId() + "_" + es.getId(), es);
      }
    } else {
      remove(eslId + "_" + src.getId(), src);
    }

    for (final MarkerContainer<EmissionSource> container : containers) {
      container.remove(src);
    }
  }

  private void remove(final String vfId, final EmissionSource src) {
    final VectorFeature feature = eSVFMap.remove(vfId);
    if (feature != null) {
      vectorLayer.removeFeature(feature);
    }
  }
}
