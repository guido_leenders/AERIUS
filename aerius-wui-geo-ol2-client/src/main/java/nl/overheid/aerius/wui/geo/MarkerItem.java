/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;

/**
 * A MarkerItem typed to an object that has an ID.
 *
 * This MarkerItem can be typed to a specific {@link TYPE} and contains
 * a reference to an object with an ID. A {@link MarkerItem} lives on a
 * {@link Point}. How it is visually represented is determined by extending
 * implementations.
 *
 * @param <E> {@link HasId} type of the object that is contained inside
 * this {@link MarkerItem}.
 */
public abstract class MarkerItem<E extends HasId> extends Point {
  private static final long serialVersionUID = -3579050020880989765L;

  private final TYPE type;
  private DivElement element;
  private E object;

  /**
   * Represents the type of MarkerItem.
   */
  public enum TYPE {
    /**
     * {@link MarkerItem} that represents an Emission Source.
     */
    EMISSION_SOURCE,

    /**
     * {@link MarkerItem} that represents a selected Emission Source.
     */
    EMISSION_SOURCE_SELECTED,

    /**
     * {@link MarkerItem} that represents a custom calculation point.
     */
    CALCULATION_POINT,

    /**
     * {@link MarkerItem} that represents an information marker.
     */
    INFO,

    /**
     * {@link MarkerItem} that represents an emission result.
     */
    EMISSION_RESULT,

    /**
     * {@link MarkerItem} that represents a shipping route.
     */
    SHIPPING_ROUTE,

    /**
     * {@link MarkerItem} that represents the begin or end markers of a shipping route.
     */
    SHIPPING_ROUTE_BOUNDARY,

    /**
     * {@link MarkerItem} that represents a site.
     */
    SITE,

    /**
     * {@link MarkerItem} that represents a notice.
     */
    NOTICE,

    /**
     * {@link MarkerItem} that represents a development kind.
     */
    DEVELOPMENT_KIND,

    /**
     * {@link MarkerItem} that represents a permit.
     */
    PERMIT;
  }

  /**
   * Create a {@link MarkerItem} on the given point, containing the given {@link HasId} object
   * of the given {@link TYPE}.
   *
   * @param point {@link Point} this {@link MarkerItem} should live on.
   * @param obj Typed object contained in this {@link MarkerItem}.
   * @param type {@link TYPE} of this {@link MarkerItem}
   */
  public MarkerItem(final Point point, final E obj, final TYPE type) {
    super(point.getX(), point.getY());

    this.type = type;
    this.object = obj;
    this.element = Document.get().createDivElement();
  }

  /**
   * @NOTE A ClassCastException will occur when the given Object is not type of Point.
   *
   * Using this constructor is only legal when the object provided is both the object used to
   * uniquely identify the marker and contains information about its location.
   *
   * @param object object that's both the parameterized type and a Point, will be checked at _runtime_, use caution.
   * @param type of this marker
   */
  public MarkerItem(final E object, final TYPE type) {
    this((Point) object, object, type);
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj) && this.getClass() == obj.getClass()
        && type == ((MarkerItem<?>) obj).getType()
        && object.getId() == ((MarkerItem<?>) obj).getId();
  }

  @Override
  public int hashCode() {
    return object.getId() + 31 * super.hashCode();
  }

  protected DivElement getElement() {
    return element;
  }

  public TYPE getType() {
    return type;
  }

  public E getObject() {
    return object;
  }

  public int getId() {
    return object.getId();
  }
}
