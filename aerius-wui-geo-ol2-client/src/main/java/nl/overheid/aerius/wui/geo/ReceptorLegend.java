/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.List;

import nl.overheid.aerius.geo.shared.LayerProps.ColorRangesLegend;
import nl.overheid.aerius.shared.domain.ColorRange;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings;
import nl.overheid.aerius.shared.domain.result.EmissionResultColorRanges;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Legend for receptor layers: hexagon receptor layers and calculation point layers. The colors depend on substance and emission result type.
 */
public class ReceptorLegend extends ColorRangesLegend {
  private static final long serialVersionUID = 123522123859150168L;

  public ReceptorLegend(final boolean hexagon, final EmissionResultType emissionResultType,
      final EmissionResultValueDisplaySettings displaySettings) {
    this(hexagon, EmissionResultColorRanges.valueOf(emissionResultType).getRanges(), emissionResultType, displaySettings);
  }

  public ReceptorLegend(final boolean hexagon, final List<ColorRange> ranges, final EmissionResultType emissionResultType,
      final EmissionResultValueDisplaySettings displaySettings) {
    super(new String[ranges.size()], new String[ranges.size()], hexagon);
    final int size = ranges.size();

    for (int i = 0; i < size; i++) {
      if (i < (size - 1)) {
        getLegendNames()[i] = FormatUtil.formatEmissionResult(emissionResultType, ranges.get(i).getLowerValue(), displaySettings) + " - "
            + FormatUtil.formatEmissionResult(emissionResultType, ranges.get(i + 1).getLowerValue(), displaySettings);
      } else {
        getLegendNames()[i] = " >= " + FormatUtil.formatEmissionResult(emissionResultType, ranges.get(i).getLowerValue(), displaySettings);
      }

      getColors()[i] = ColorUtil.webColor(ranges.get(i).getColor());
    }
  }
}
