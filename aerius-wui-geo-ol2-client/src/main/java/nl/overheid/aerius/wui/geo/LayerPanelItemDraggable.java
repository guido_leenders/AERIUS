/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;

/**
 * A row in the selected layer panel.
 */
class LayerPanelItemDraggable extends LayerPanelItemImpl implements LayerPanelItem, IsCollapsiblePanel {
  private final Label nameField;


  public LayerPanelItemDraggable(final MapLayer layer, final LayerItemChangeHandler changeHandler, final LegendTitleFactory unitFactory,
      final HelpPopupController hpC) {
    super(layer, changeHandler, unitFactory, hpC);

    nameField = new Label();
    itemTitle = new FocusPanel(nameField);

    init();
  }

  @Override
  public final void setLayerName(final String name) {
    nameField.setText(name);
    // set it only once
    if (getMacroLabel() == null) {
      setMacroLabel(name);
    }
  }

  /**
   * Returns the widget presenting the draggable area.
   *
   * @return
   */
  @Override
  public Widget getDragHandler() {
    return itemTitle;
  }

  /**
   * Returns the widget label with macro elements.
   *
   * @return
   */
  @Override
  public String getLayerMacroLabel() {
    return super.getMacroLabel();
  }
}
