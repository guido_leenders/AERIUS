/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import org.gwtopenmaps.openlayers.client.Icon;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.Size;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.Element;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Abstract base class for map markers.
 */
public abstract class SingleMarker<E extends HasId> extends MarkerItem<E> {

  private final Icon defaultIcon = new Icon("", new Size(0, 0));
  private final Marker marker;

  public SingleMarker(final Point point, final E obj, final TYPE type) {
    super(point, obj, type);
    marker = new Marker(new LonLat(getX(), getY()), defaultIcon);
    final Element image = marker.getIcon().getJSObject().getPropertyAsDomElement("imageDiv");

    image.setInnerText(null); // Clear content
    final DivElement topContainer = Document.get().createDivElement();
    topContainer.addClassName(R.css().infoMarkerContainer());
    image.appendChild(topContainer);
    topContainer.appendChild(getElement());
  }

  public Marker getMarker() {
    return marker;
  }
}
