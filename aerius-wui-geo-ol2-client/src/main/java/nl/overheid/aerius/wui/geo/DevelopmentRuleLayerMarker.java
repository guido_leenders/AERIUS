/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Image;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleMarker;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;

/**
 * Calculation marker capable of showing multiple markers on one location.
 */
@SuppressWarnings("serial")
public class DevelopmentRuleLayerMarker extends MarkerItem<DevelopmentRuleMarker> {

  private final DevelopmentRule markerType;

  /**
   * Constructor.
   *
   * @param point The location of this marker.
   * @param type The marker category @TYPE.
   * @param marketType The marker type @DevelopmentRule.
   */
  public DevelopmentRuleLayerMarker(final DevelopmentRuleMarker point, final TYPE type, final DevelopmentRule marketType) {
    super(point, type);
    this.markerType = marketType;
    getElement().getStyle().setPosition(Position.ABSOLUTE);
  }

  public DevelopmentRule getMarkerType() {
    return markerType;
  }

  /**
   * Draws the actual markers.
   */
  public void drawMarker() {
    getElement().setInnerHTML(null);
    final Image i = new Image(DevelopmentSpaceUtil.getDevelopmentRuleIcon(getMarkerType()));
    getElement().appendChild(i.getElement());
    getElement().getStyle().setPosition(Position.ABSOLUTE);
    getElement().getStyle().setTop(-i.getHeight() / 2.0, Unit.PX);
    getElement().getStyle().setLeft(-i.getWidth() / 2.0, Unit.PX);
  }


}
