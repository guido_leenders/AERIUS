/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.DragEndEvent;
import com.allen_sauer.gwt.dnd.client.DragHandler;
import com.allen_sauer.gwt.dnd.client.DragHandlerAdapter;
import com.allen_sauer.gwt.dnd.client.DragStartEvent;
import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.drop.AbstractInsertPanelDropController;
import com.allen_sauer.gwt.dnd.client.util.LocationWidgetComparator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.InsertPanel;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.LabeledMultiMapLayer;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.geo.shared.LabeledMultiLayerProps;
import nl.overheid.aerius.geo.shared.LayerMultiWMSProps;
import nl.overheid.aerius.wui.geo.LayerPanelItemImpl.LayerItemChangeHandler;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.AttachedPopupBase;
import nl.overheid.aerius.wui.main.widget.AttachedPopupContent;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;
import nl.overheid.aerius.wui.main.widget.CollapsibleStackPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.animation.FadeAnimation;
import nl.overheid.aerius.wui.main.widget.animation.FadeAnimation.Fade;
import nl.overheid.aerius.wui.main.widget.dialog.AeriusDialogBox;

/**
 * Panel to display the selected layers. The panel has a header that contains a
 * button to open the layer selection popup and a slidebar that
 */
@Singleton
public class LayerPanel extends Composite implements AttachedPopupContent, RequiresResize {
  interface LayerPanelEventBinder extends EventBinder<LayerPanel> {}

  private final LayerPanelEventBinder eventBinder = GWT.create(LayerPanelEventBinder.class);

  private static final LayerPanelUiBinder UI_BINDER = GWT.create(LayerPanelUiBinder.class);
  private static final int ANIMATION_RUNTIME = 500; // ms

  interface LayerPanelUiBinder extends UiBinder<Widget, LayerPanel> {}

  private final AeriusDialogBox configPopup;

  @UiField CollapsibleStackPanel layers;
  @UiField AttachedPopupBase wrapper;
  @UiField AbsolutePanel boundary;

  private final PickupDragController dragController;
  private final DragHandler dragHandler;
  private final MapLayoutPanel map;

  private final LayerConfigPanel configPanel;

  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  private boolean layoutScheduled;

  /**
   * Custom DropController displaying a dotted placeholder for the Layer
   * widget.
   */
  private static class LayerDropController extends AbstractInsertPanelDropController {

    public LayerDropController(final InsertPanel dropTarget) {
      super(dropTarget);
    }

    @Override
    protected LocationWidgetComparator getLocationWidgetComparator() {
      return LocationWidgetComparator.BOTTOM_RIGHT_COMPARATOR;
    }

    @Override
    protected Widget newPositioner(final DragContext context) {
      final HTML positioner = new HTML(context.draggable.getElement().getInnerHTML());

      positioner.addStyleName(R.css().whileDragging());

      return positioner;
    }
  }

  private final LayerItemChangeHandler changeHandler = new LayerItemChangeHandler() {
    @Override
    public void changeOpacity(final MapLayer mapLayer, final float opacity) {
      map.setOpacity(mapLayer, opacity);
    }

    @Override
    public void changeEnabled(final MapLayer layer, final boolean value) {
      setLayerEnabled(layer, value);
      map.setVisible(layer, value);
    }

    @Override
    public void zoomToZoomLevel(final MapLayer layer) {
      switch (layer.isLayerInZoom()) {
      case EXCEED:
        layer.zoomToMinScale();
        break;
      case INCEED:
        layer.zoomToMaxScale();
        break;
      case IN_ZOOM:
      default:
        break;
      }
    }

    @Override
    public void changeLabelEnabled(final LabeledMultiMapLayer layer, final boolean value) {
      layer.getDefaultLayer().setVisible(value);
      if (value && !layer.getLabelLayer().isVisible()) {
        // make layer visible
        setLayerEnabled(layer, value);
        map.setVisible(layer, value);
      }
    }
  };

  private final CloseHandler<Boolean> closeHandler = new CloseHandler<Boolean>() {
    @Override
    public void onClose(final CloseEvent<Boolean> event) {
      configPopup.hide();
    }
  };

  private final LegendTitleFactory unitFactory;

  private final HelpPopupController hpC;

  private final ResizeHandler resizeHandler = new ResizeHandler() {
    @Override
    public void onResize(final ResizeEvent event) {
      forceLayout();
    }
  };

  private HandlerRegistration resizeRegistration;

  @Inject
  public LayerPanel(final EventBus eventBus, final MapLayoutPanel map, final LayerConfigPanel configPanel,
      final LegendTitleFactory unitFactory, final HelpPopupController hpC) {
    this.map = map;
    this.configPanel = configPanel;
    this.unitFactory = unitFactory;
    this.hpC = hpC;

    initWidget(UI_BINDER.createAndBindUi(this));
    //    layerConfig.setVisible(DevModeUtil.I.isDevMode()); // For now hide button when in compiled mode.
    final LayerDropController ldc = new LayerDropController(layers);

    configPopup = new AeriusDialogBox();
    configPopup.setHTML(M.messages().layerConfigTitle());
    configPopup.setWidget(configPanel);
    configPopup.setGlassEnabled(true);

    configPanel.addCloseHandler(closeHandler);

    eventBinder.bindEventHandlers(this, eventBus);

    dragController = new PickupDragController(boundary, false);
    dragController.setBehaviorMultipleSelection(false);
    dragController.setBehaviorConstrainedToBoundaryPanel(true);
    dragController.registerDropController(ldc);
    dragHandler = new DragHandlerAdapter() {
      private int oldIdx = -1;

      @Override
      public void onDragEnd(final DragEndEvent event) {
        final Object source = event.getSource();

        if (source instanceof LayerPanelItem) {
          final int newIdx = getReversedLayersWidgetIndex((Widget) source);
          final int delta = oldIdx - newIdx;
          map.raiseLayer(((LayerPanelItem) source).getLayer(), delta);
        }
      }

      @Override
      public void onPreviewDragStart(final DragStartEvent event) {
        final Object source = event.getSource();

        if (source instanceof LayerPanelItem) {
          oldIdx = getReversedLayersWidgetIndex((Widget) source);
        }
      }
    };
  }

  @Override
  protected void onAttach() {
    super.onAttach();

    resizeRegistration = Window.addResizeHandler(resizeHandler);

    scheduledLayout();
  }

  @Override
  protected void onDetach() {
    super.onDetach();

    resizeRegistration.removeHandler();
  }

  @EventHandler
  public void onLayerChange(final LayerChangeEvent event) {
    final MapLayer layer = event.getLayer();

    switch (event.getChange()) {
    case ADDED:
      addLayer(layer);
      break;
    case REMOVED:
      removeLayer(layer);
      break;
    case DISABLED:
      setLayerEnabled(layer, false);
      break;
    case ENABLED:
      setLayerEnabled(layer, true);
      break;
    case NAME:
      updateLayerName(layer);
      break;
    case OPACITY:
      updateLayerOpacity(layer);
      break;
    case VISIBLE:
    case HIDDEN:
      final LayerPanelItem lw = findLayerItem(layer);

      if (lw != null) {
        lw.setLayerVisibility();
      }
      break;
    case LEGEND_CHANGED:
      final LayerPanelItem layerItem = findLayerItem(layer);
      if (layerItem != null) {
        layerItem.updateLegend(layer);
      }
      break;
    default:
    }
  }

  @EventHandler
  public void onEmissionResultKeyChanged(final EmissionResultKeyChangeEvent e) {
    for (final Widget lw : layers) {
      if (lw instanceof LayerPanelItem) {
        ((LayerPanelItem) lw).updateTitle();
      }
    }
  }

  private void updateLayerOpacity(final MapLayer layer) {
    final LayerPanelItem findLayerItem = findLayerItem(layer);

    if (findLayerItem == null) {
      return;
    }

    findLayerItem.setLayerOpacity(layer.getOpacity());
  }

  /**
   * Adds the layer to the list of layers, adds the layer to the map and makes
   * the layer draggable. Layers on the panel are added in reversed order. The
   * layer on top is shown at the first position, while it is at the last
   * layer in the layers list of the map.
   *
   * @param lp
   */
  private void addLayer(final MapLayer layer) {
    LayerPanelItem item;
    if (layer.getLayerProps() instanceof LayerMultiWMSProps<?> || layer.getLayerProps() instanceof LabeledMultiLayerProps) {
      item = new LayerPanelItemListBox(layer, changeHandler, unitFactory, hpC);
    } else {
      item = new LayerPanelItemDraggable(layer, changeHandler, unitFactory, hpC);
    }

    dragController.makeDraggable(item.asWidget(), item.getDragHandler());
    layers.insert(item, 0);

    //Only animate when visible
    if (isAttached() && isVisible()) {
      new FadeAnimation(item.asWidget(), Fade.IN).run(ANIMATION_RUNTIME);
    }
  }

  private LayerPanelItem findLayerItem(final MapLayer layer) {
    for (final Widget lw : layers) {
      if ((lw instanceof LayerPanelItem) && ((LayerPanelItem) lw).getLayer().equals(layer)) {
        return (LayerPanelItem) lw;
      }
    }
    return null;
  }

  /**
   * Because layers are shown in reversed order enabled layers, layer on top
   * (last on the list) is shown at the first position in this panel.
   *
   * @param source
   * @return
   */
  private int getReversedLayersWidgetIndex(final Widget widget) {
    return layers.getWidgetCount() - 1 - layers.getWidgetIndex(widget);
  }

  @Override
  protected void onLoad() {
    dragController.addDragHandler(dragHandler);
  }

  @Override
  protected void onUnload() {
    dragController.removeDragHandler(dragHandler);
  }

  /**
   * Removes the layer from the list and map.
   *
   * @param lp
   */
  private void removeLayer(final MapLayer layer) {
    removeLayer(findLayerItem(layer));
  }

  private void removeLayer(final LayerPanelItem lw) {
    if (lw != null) {
      layers.remove(lw);
    }
  }

  /**
   * Sets the state of the layer to enabled if true, disable if false on the Layer panel.
   * If the layer is null, it's not part on the layer panel and no action should be taken.
   *
   * @param layer Layer to change state
   * @param enabled if true layer enabled else layer disabled
   */
  private void setLayerEnabled(final MapLayer layer, final boolean enabled) {
    final LayerPanelItem lw = findLayerItem(layer);

    if (lw != null) {
      lw.setEnabled(enabled);
    } // else layer hidden from layer panel.
  }

  private void updateLayerName(final MapLayer layer) {
    final LayerPanelItem lw = findLayerItem(layer);
    if (lw == null) {
      GWT.log("Layer that should be updated could not be found. [probably, an event binding has not been let go.]");
      return;
    }

    lw.setLayerName(layer.getTitle());
  }

  @Override
  public void setPopup(final AttachedPopupPanel pp) {
    wrapper.setPopup(pp);
  }

  @Override
  public void onResize() {
    scheduledLayout();
  }

  private void scheduledLayout() {
    if (layoutScheduled) {
      return;
    }

    layoutScheduled = true;
    Scheduler.get().scheduleDeferred(layoutCmd);
  }

  private void forceLayout() {
    layers.getElement().getStyle().setProperty("maxHeight", wrapper.calculateMaxHeightBasedOnScreen(), Unit.PX);
  }

}
