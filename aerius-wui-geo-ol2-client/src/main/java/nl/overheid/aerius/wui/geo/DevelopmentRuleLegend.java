/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Marker legend with no implementation whatsoever.
 *
 * Note the double meaning of 'marker'; legend meant for markers, and a marker class
 *
 */
public class DevelopmentRuleLegend extends ImagesLegend {

  public DevelopmentRuleLegend() {
    super(M.messages().legendDepositionMarkerText());
  }

  @Override
  protected void addImages(final FlowPanel panel) {
    panel.add(getImageLegend(new Image(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK)),
        M.messages().calculatorDevelopmentRuleType(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK)));
    panel.add(getImageLegend(new Image(DevelopmentSpaceUtil.getDevelopmentRuleIcon(DevelopmentRule.EXCEEDING_SPACE_CHECK)),
        M.messages().calculatorDevelopmentRuleType(DevelopmentRule.EXCEEDING_SPACE_CHECK)));
  }
}
