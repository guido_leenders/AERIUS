/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;

import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.wui.main.widget.LabelWithValue;

/**
 * Not actually a cell table because cell tables suck. (though conveniently named after it to indicate it's doing a lot of the same things)
 */
public class LayerCellTable extends FlowPanel {
  public interface CustomStyle {
    String activeLayerItem();

    String activeLayerItemSelected();
  }

  private final ArrayList<MapLayer> selection = new ArrayList<MapLayer>();

  private CustomStyle style;

  private final ClickHandler clickHandler = new ClickHandler() {

    @SuppressWarnings("unchecked")
    @Override
    public void onClick(final ClickEvent event) {
      if (event.getSource() instanceof LabelWithValue<?>) {
        final LabelWithValue<MapLayer> item = (LabelWithValue<MapLayer>) event.getSource();

        final boolean selected = selection.contains(item.getValue());
        if (selected) {
          selection.remove(item.getValue());
        } else {
          if (!event.isControlKeyDown()) {
            clearSelection();
          }

          selection.add(item.getValue());
        }

        item.setStyleName(style.activeLayerItemSelected(), !selected);
      }
    }
  };

  public void setStyle(final CustomStyle style) {
    this.style = style;
  }

  public void clearLayers() {
    clear();
  }

  public void addAllLayers(final List<MapLayer> lst) {
    for (final MapLayer layer : lst) {
      addLayer(layer);
    }
  }

  public void addLayer(final MapLayer layer) {
    final LabelWithValue<MapLayer> label = new LabelWithValue<MapLayer>(layer.getTitle(), layer);
    label.setStyleName(style.activeLayerItem());

    label.addClickHandler(clickHandler);

    insert(label, 0);
  }

  public ArrayList<MapLayer> getSelection() {
    return selection;
  }

  public void clearSelection() {
    clearSelection(false);
  }

  @SuppressWarnings("unchecked")
  public void clearSelection(final boolean remove) {
    for (int i = getWidgetCount() - 1; i >= 0; i--) {
      if (getWidget(i) instanceof LabelWithValue<?>) {
        final LabelWithValue<MapLayer> item = (LabelWithValue<MapLayer>) getWidget(i);

        if (!selection.contains(item.getValue())) {
          continue;
        }

        if (remove) {
          remove(item);
        } else {
          item.removeStyleName(style.activeLayerItemSelected());
        }
      }
    }

    selection.clear();
  }
}
