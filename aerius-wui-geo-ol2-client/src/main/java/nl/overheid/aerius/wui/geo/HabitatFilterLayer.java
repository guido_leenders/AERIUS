/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.google.inject.Inject;

import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.WMSMapLayer;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.domain.ColorRange;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.domain.deposition.HabitatFilterRangeType;
import nl.overheid.aerius.shared.domain.info.CriticalDepositionAreaType;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;

/**
 * Layer to filter results for a specific habitat type between a certain range of values for some year.
 */
public class HabitatFilterLayer {

  private static final String ASSESSMENT_AREA_ID = "assessmentAreaId";

  private final Context context;
  private final UserContext userContext;

  private final MapLayoutPanel map;

  private final FetchGeometryServiceAsync service;

  private WMSMapLayer filterLayer;

  private LayerWMSProps habitatFilterLayerItem;

  @Inject
  public HabitatFilterLayer(final Context context, final UserContext userContext, final MapLayoutPanel map, final FetchGeometryServiceAsync service) {
    this.context = context;
    this.userContext = userContext;
    this.map = map;
    this.service = service;
  }

  public void showLayer(final HabitatFilterRangeType filterType, final int assessmentAreaId, final int habitatTypeId,
      final CriticalDepositionAreaType filterRelevance, final int year, final double lowerValue, final double upperValue) {
    habitatFilterLayerItem = getLayerProps(filterType);
    habitatFilterLayerItem.setTitle(M.messages().layerHabitatFilter());
    habitatFilterLayerItem.setParamFilters(formatParameters(assessmentAreaId, habitatTypeId, year, filterRelevance, lowerValue, upperValue));
    habitatFilterLayerItem.setOpacity(1f);

    removeLayer();

    LayerPreparationUtil.prepareLayer(service, habitatFilterLayerItem, new AppAsyncCallback<LayerProps>() {
      @Override
      public void onSuccess(final LayerProps result) {
        removeLayer();
        habitatFilterLayerItem.setEnabled(true);
        filterLayer = (WMSMapLayer) map.addLayer(habitatFilterLayerItem);
      }
    });
  }

  private LayerWMSProps getLayerProps(final HabitatFilterRangeType filterType) {
    final LayerWMSProps wmsProps;
    switch (filterType) {
    case DELTA_DEPOSITION:
      wmsProps = context.getLayer(WMSLayerType.WMS_CRITICAL_DEPOSITION_AREA_RECEPTOR_DELTA_DEPOSITIONS_VIEW);
      break;
    case DEVIATION_FROM_CRITICAL_DEPOSITION:
      wmsProps = context.getLayer(WMSLayerType.WMS_CRITICAL_DEPOSITION_AREA_RECEPTOR_DEVIATIONS_VIEW);
      break;
    case TOTAL_DEPOSITION:
    default:
      wmsProps = context.getLayer(WMSLayerType.WMS_CRITICAL_DEPOSITION_AREA_RECEPTOR_DEPOSITIONS_VIEW);
      break;
    }
    return wmsProps.shallowCopy();
  }

  private HashMap<String, Object> formatParameters(final int assessmentAreaId, final int habitatTypeId, final int year,
      final CriticalDepositionAreaType filterRelevance, final double lowerValue, final double upperValue) {
    final HashMap<String, Object> map = new HashMap<>();

    map.put(ASSESSMENT_AREA_ID, assessmentAreaId);
    map.put("criticalDepositionAreaId", habitatTypeId);
    map.put("year", year);
    map.put("lowerValue", lowerValue);
    map.put("upperValue", upperValue);
    map.put("type", filterRelevance.name().toLowerCase());

    return map;
  }

  public void removeLayer() {
    if (filterLayer != null) {
      map.removeLayer((MapLayer) filterLayer);
      filterLayer = null;
    }
  }

  public void setLegend(final Set<ColorRange> keySet) {
    if (habitatFilterLayerItem != null) {
      habitatFilterLayerItem.setLegend(
          new ReceptorLegend(true, new ArrayList<>(keySet), EmissionResultType.DEPOSITION, userContext.getEmissionResultValueDisplaySettings()));
    }
  }

  public MapLayer getLayer() {
    return filterLayer;
  }
}
