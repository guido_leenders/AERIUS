/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo.search;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestion;
import nl.overheid.aerius.shared.domain.search.MapSearchSuggestionType;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.search.SearchPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * The searchPanel for map based searches.
 */
@Singleton
public class MapSearchPanel extends SearchPanel<MapSearchSuggestionType, MapSearchSuggestion, MapSearchAction> {

  @Inject
  public MapSearchPanel(final EventBus eventBus, final HelpPopupController hpC, final MapSearchTable searchTable, final MapSearchAction searchAction) {
    super(eventBus, hpC, searchTable, searchAction);

    setPlaceHolder(M.messages().searchWidgetPlaceholder());
  }

  public void setMap(final MapLayoutPanel map) {
    searchAction.setMap(map);
  }
}
