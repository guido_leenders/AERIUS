/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.geo.LabeledMultiMapLayer;
import nl.overheid.aerius.geo.LayerInZoom;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.IsCollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.PercentageSliderBar;

/**
 * A row in the selected layer panel.
 */
abstract class LayerPanelItemImpl extends Composite implements LayerPanelItem, IsCollapsiblePanel {
  private static final LayerPanelItemImplUiBinder UI_BINDER = GWT.create(LayerPanelItemImplUiBinder.class);

  interface LayerPanelItemImplUiBinder extends UiBinder<Widget, LayerPanelItemImpl> {}

  protected interface LayerItemChangeHandler {
    void changeOpacity(MapLayer layer, float opacity);

    void changeEnabled(MapLayer layer, boolean value);

    void zoomToZoomLevel(MapLayer layer);

    void changeLabelEnabled(LabeledMultiMapLayer layer, boolean value);
  }

  @UiField protected CollapsiblePanel collapsiblePanel;
  @UiField protected FocusPanel zoomButton;
  @UiField(provided = true) protected Widget itemTitle;
  @UiField protected CheckBox checkBox;
  @UiField protected SimplePanel sliderContainer;
  @UiField protected SimplePanel legendContainer;
  @UiField protected PercentageSliderBar sliderBar;

  protected final MapLayer layer;
  protected final LayerItemChangeHandler changeHandler;
  private final LegendTitleFactory unitFactory;
  private LegendItem legend;
  private String macroLabel;
  private final HelpPopupController hpC;

  protected LayerPanelItemImpl(final MapLayer al, final LayerItemChangeHandler changeHandler, final LegendTitleFactory unitFactory,
      final HelpPopupController hpC) {
    this.layer = al;
    this.changeHandler = changeHandler;
    this.unitFactory = unitFactory;
    this.hpC = hpC;
  }

  protected void init() {
    initWidget(UI_BINDER.createAndBindUi(this));

    sliderBar.setCurrentValue(layer.getOpacity());
    setEnabled(layer.getLayerProps().isEnabled());
    setLayerName(layer.getTitle());
    setLayerVisibility();

    updateLegend(layer);

    itemTitle.ensureDebugId(TestID.DIV_LAYERPANELDRAGGER + "-" + layer.getTitle());
    collapsiblePanel.ensureDebugId(TestID.DIV_LAYERPANELITEM + "-" + layer.getTitle());
    sliderBar.ensureDebugId(TestID.DIV_LAYERPANELDRAGGER + "-" + layer.getTitle());
    checkBox.ensureDebugId(TestID.DIV_LAYERPANELCHECKBOX + "-" + layer.getTitle());
    zoomButton.ensureDebugId(TestID.DIV_LAYERPANELZOOM + "-" + layer.getTitle());

    hpC.addWidget(sliderBar, hpC.tt().ttLayerPanelSlider());

    // Ugh, unfortunately, there's no slidestart event for the sliderbar or similar.
    DOM.sinkEvents(sliderBar.getKnob(), Event.ONMOUSEDOWN);
    DOM.setEventListener(sliderBar.getKnob(), new EventListener() {
      @Override
      public void onBrowserEvent(final Event event) {
        if (Event.ONMOUSEDOWN == event.getTypeInt() && !checkBox.getValue()) {
          checkBox.setValue(true);
          changeHandler.changeEnabled(layer, true);
        }
      }
    });
  }

  @Override
  public void updateTitle() {
    legend.setTitleText(unitFactory.getLegendTitle(layer));
  }

  @Override
  public void updateLegend(final MapLayer layer) {
    legend = new LegendItem(layer);
    updateTitle();
    legendContainer.setWidget(legend);
  }

  /**
   * Returns the layer associated with this widget.
   *
   * @return
   */
  @Override
  public MapLayer getLayer() {
    return layer;
  }

  /**
   * Returns true if the layer check box is set.
   *
   * @return
   */
  public boolean isEnabled() {
    return checkBox.getValue();
  }

  @UiHandler("checkBox")
  void onCheckBoxClick(final ValueChangeEvent<Boolean> e) {
    changeHandler.changeEnabled(layer, checkBox.getValue());
    // Don't call setEnabled because changeEnabled will call map and trigger
    // an event, which eventually will call setEnabled.
  }

  @UiHandler("zoomButton")
  void onZoomClick(final ClickEvent e) {
    changeHandler.zoomToZoomLevel(getLayer());
  }

  /**
   * Update transparency of active layer.
   *
   * @param event
   */
  @UiHandler("sliderBar")
  void onSliderChange(final ValueChangeEvent<Double> event) {
    changeHandler.changeOpacity(getLayer(), event.getValue().floatValue());
  }

  /**
   * Updates the layer state
   */
  @Override
  public final void setLayerVisibility() {
    final String reason = layer.getNotVisibleReason();
    collapsiblePanel.getTitleWidget().setStyleName(R.css().layerNotVisible(), reason != null);
    collapsiblePanel.getTitleWidget().setTitle(reason);
    zoomButton.setTitle(reason);
    zoomButton.setVisible(layer.isLayerInZoom() != LayerInZoom.IN_ZOOM);
  }

  @Override
  public void setLayerOpacity(final float opacity) {
    sliderBar.setCurrentValue(opacity, false);
  }

  /**
   *
   * @param enabled true if enabled
   */
  @Override
  public void setEnabled(final boolean enabled) {
    checkBox.setValue(enabled);
    changeHandler.changeOpacity(layer, sliderBar.getValue().floatValue());
  }

  @Override
  public CollapsiblePanel asCollapsible() {
    return collapsiblePanel;
  }

  public String getMacroLabel() {
    return macroLabel;
  }

  public void setMacroLabel(final String macroLabel) {
    this.macroLabel = macroLabel;
  }
}
