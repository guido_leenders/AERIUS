/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.Icon;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.Size;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.Element;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * A marker representing one or more marker items. Each item is identified
 * by an ID. This marker works like a regular OpenLayers makers layer,
 * but hijacks the marker by replacing the image with its own content.
 */
public class AeriusMarker {
  private static final int MAX_WIDTH = 600;

  /**
   * Correction of position of marker content drawn left from the center. The total position correction is to position the marker content left of the
   * black arrow. The total left position is calculated by taking the total width of the left arrow minus a correction. With
   * {@link #POSITION_OVER_LEFT_ARROW} the correction is added such that the marker content overlaps the left arrow to make the arrow as small as
   * possible. But not too much as if it looks the arrow doesn't align nicely with the bottom of the marker.
   */
  private static final int POSITION_OVER_LEFT_ARROW = 7;

  private final Icon defaultIcon = new Icon("", new Size(0, 0));

  protected final DivElement root = Document.get().createDivElement();

  private DivElement topContainer;
  private DivElement sourceContainer;
  private DivElement sourceItems;
  private DivElement depositionContainer;
  private DivElement depositionItems;

  private final ArrayList<MarkerItem<? extends HasId>> markerItems = new ArrayList<MarkerItem<? extends HasId>>();

  private final Marker marker;

  /**
   * Creates a marker at the location.
   *
   * @param p (Point)location to place the marker
   */
  public AeriusMarker(final Point p) {
    marker = new Marker(new LonLat(p.getX(), p.getY()), defaultIcon);
    final Element image = marker.getIcon().getJSObject().getPropertyAsDomElement("imageDiv");

    // Clear content
    image.setInnerText(null);

    root.getStyle().setWidth(MAX_WIDTH, Unit.PX);
    image.appendChild(root);
  }

  /**
   * Fill the marker with the given MarkerItems.
   *
   * @param items The items to fill the Marker with
   */
  public void setMarkerItems(final ArrayList<? extends MarkerItem<? extends HasId>> items) {
    markerItems.clear();
    for (final MarkerItem<? extends HasId> i : items) {
      addMarkerItem(i);
    }
  }

  /**
   * Add a marker item to the marker.
   * @param i marker item
   */
  public void addMarkerItem(final MarkerItem<? extends HasId> i) {
    markerItems.add(i);
    switch (i.getType()) {
    case EMISSION_SOURCE:
    case EMISSION_SOURCE_SELECTED:
      if (sourceContainer == null) {
        createSourceContainer();
      }
      sourceItems.appendChild(i.getElement());
      break;
    case CALCULATION_POINT:
      if (depositionContainer == null) {
        createDepositionContainer();
      }

      i.getElement().getStyle().setFloat(com.google.gwt.dom.client.Style.Float.RIGHT);
      depositionItems.appendChild(i.getElement());
      break;
    case SHIPPING_ROUTE:
    case SITE:
    case EMISSION_RESULT:
    case DEVELOPMENT_KIND:
    case INFO:
    case SHIPPING_ROUTE_BOUNDARY:
      if (topContainer == null) {
        createTopContainer();
      }

      topContainer.appendChild(i.getElement());
      break;
    default:
      // Do nothing
    }
  }

  private void createTopContainer() {
    topContainer = Document.get().createDivElement();
    topContainer.addClassName(R.css().infoMarkerContainer());
    root.appendChild(topContainer);
  }

  private void createSourceContainer() {
    sourceContainer = Document.get().createDivElement();
    sourceContainer.addClassName(R.css().sourceMarkerContainer());

    sourceItems = Document.get().createDivElement();
    sourceItems.getStyle().setDisplay(Display.INLINE_BLOCK);
    sourceItems.getStyle().setWidth(MAX_WIDTH - R.css().sourceMarkerArrowWidth(), Unit.PX);

    final DivElement sourceArrow = Document.get().createDivElement();
    sourceArrow.setClassName(R.css().sourceMarkerArrow());
    sourceArrow.getStyle().setDisplay(Display.INLINE_BLOCK);

    sourceContainer.appendChild(sourceArrow);
    sourceContainer.appendChild(sourceItems);
    sourceContainer.getStyle().setWidth(MAX_WIDTH, Unit.PX);

    root.appendChild(sourceContainer);
  }

  private void createDepositionContainer() {
    depositionContainer = Document.get().createDivElement();
    depositionContainer.addClassName(R.css().depositionMarkerContainer());
    depositionContainer.getStyle().setTop(0, Unit.PX);
    depositionContainer.getStyle().setRight(0, Unit.PX);

    depositionItems = Document.get().createDivElement();
    depositionItems.setClassName(R.css().depositionMarkerItems());
    depositionItems.getStyle().setDisplay(Display.INLINE_BLOCK);
    depositionItems.getStyle().setPosition(Position.ABSOLUTE);
    depositionItems.getStyle().setTop(0, Unit.PX);
    depositionItems.getStyle().setRight(R.css().sourceMarkerArrowWidth() - POSITION_OVER_LEFT_ARROW, Unit.PX);
    depositionItems.getStyle().setWidth(MAX_WIDTH, Unit.PX);

    final DivElement depositionArrow = Document.get().createDivElement();
    depositionArrow.setClassName(R.css().depositionMarkerArrow());
    depositionArrow.getStyle().setDisplay(Display.INLINE_BLOCK);

    depositionContainer.appendChild(depositionArrow);
    depositionContainer.appendChild(depositionItems);

    root.appendChild(depositionContainer);
  }

  public Marker getMarker() {
    return marker;
  }
}
