/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.Size;
import org.gwtopenmaps.openlayers.client.layer.TransitionEffect;
import org.gwtopenmaps.openlayers.client.layer.WMS;
import org.gwtopenmaps.openlayers.client.layer.WMSOptions;
import org.gwtopenmaps.openlayers.client.layer.WMSParams;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.LayerWMSProps;

/**
 * Wrapper for WMS layer, implementing the {@link MapLayer} interface.
 */
public class WMSMapLayer extends WMS implements MapLayer {
  private static final String KEY_VIEWPARAMS = "viewparams";

  private final LayerWMSProps props;
  private final MapLayoutPanel map;
  private final LayerVisibility visibility;
  private String title;

  protected WMSMapLayer(final String title, final MapLayoutPanel map, final String url, final LayerWMSProps props) {
    this(title, map, url, props, new LayerVisibility());
  }

  protected WMSMapLayer(final String title, final MapLayoutPanel map, final String url, final LayerWMSProps props, final LayerVisibility visibility) {
    super(title, url, defaultWMSParams(props.getName(), props.getSldURL()),
        defaultWMSOptions(props, map.getReceptorGridSettings().getEpsg().getEpsgCode()));
    setTitle(title);
    this.map = map;
    this.props = props;
    this.visibility = visibility;

    redrawFilters();
    visibility.setLayer(this);
  }

  private static WMSOptions defaultWMSOptions(final LayerWMSProps layerProperties, final String epsgCode) {
    final WMSOptions wmsOptions = new WMSOptions();

    wmsOptions.setUntiled();
    wmsOptions.setTransitionEffect(TransitionEffect.RESIZE);
    if (layerProperties.getMaxScale() > 0) {
      wmsOptions.setMaxScale(layerProperties.getMaxScale());
    }
    if (layerProperties.getMinScale() > 0) {
      wmsOptions.setMinScale(layerProperties.getMinScale());
    }
    if (layerProperties.getTileSize() > 0) {
      wmsOptions.setSingleTile(false);
      wmsOptions.setTileSize(new Size(layerProperties.getTileSize(), layerProperties.getTileSize()));
    }
    wmsOptions.setAttribution(layerProperties.getAttribution());
    wmsOptions.setProjection(epsgCode);
    wmsOptions.setRatio(1);
    return wmsOptions;
  }

  private static WMSParams defaultWMSParams(final String layerNames, final String sldUrl) {
    final WMSParams wmsParams = new WMSParams();

    wmsParams.setFormat("image/png");
    wmsParams.setTransparent(true);
    wmsParams.setLayers(layerNames);
    if (sldUrl != null) {
      wmsParams.setParameter("SLD", sldUrl);
    }

    return wmsParams;
  }

  @Override
  public boolean redraw() {
    map.setVisible(this, props.isEnabled());
    redrawFilters();
    return super.redraw();
  }

  public final void redrawFilters() {
    redrawViewParams();
  }

  private void redrawViewParams() {
    if (props.getParamFilters() == null) {
      return;
    }

    // Join the view params to a JS array
    final JsArrayString jsa = JavaScriptObject.createArray().cast();
    for (final Entry<String, Object> entry : props.getParamFilters().entrySet()) {
      jsa.push(entry.getKey() + ":" + entry.getValue());
    }

    setParam(KEY_VIEWPARAMS, jsa.join(";"));
  }

  @Override
  public void attachLayer() {
    map.addLayerToMap(this);
  }

  @Override
  public void destroy() {
    try {
      if (getJSObject() != null) {
        getMap().removeLayer(this);
      }

      super.destroy();
    } catch (final Exception e) {
      Logger.getLogger("WMSMapLayer").log(Level.FINEST, "destroy() threw error", e);
    }
  }

  @Override
  public int getLayerIndex() {
    return getMap().getLayerIndex(this);
  }

  @Override
  public LayerProps getLayerProps() {
    return props;
  }

  @Override
  public Legend getLegend() {
    return props.getLegend();
  }

  @Override
  public Map getMap() {
    return map.getMap();
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public void setTitle(final String title) {
    this.title = title;
  }

  @Override
  public String getNotVisibleReason() {
    return visibility.getNotVisibleReason();
  }

  @Override
  public LayerInZoom isLayerInZoom() {
    return visibility.isLayerInZoom();
  }

  @Override
  public void raiseLayer(final int delta) {
    getMap().raiseLayer(this, delta);
  }

  @Override
  public void refreshLayer() {
    map.setVisible(this, props.isEnabled());
    redrawFilters();
  }

  @Override
  public void setOpacity(final float opacity) {
    props.setOpacity(opacity);
    super.setOpacity(opacity);
  }

  private void setParam(final String param, final String value) {
    setParam(param, value, false);
  }

  private void setParam(final String param, final String value, final boolean refresh) {
    getParams().setParam(param, value);

    if (refresh) {
      refreshLayer();
    }
  }

  @Override
  public boolean setVisible(final boolean isVisible) {
    return visibility.setIsVisible(isVisible);
  }

  @Override
  public void zoomToMinScale() {
    getMap().zoomToScale(props.getMinScale(), true);
  }

  @Override
  public void zoomToMaxScale() {
    getMap().zoomToScale(props.getMaxScale(), true);
  }
}
