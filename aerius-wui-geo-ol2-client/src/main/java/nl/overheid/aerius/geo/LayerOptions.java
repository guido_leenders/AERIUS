/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * Layer options are set in JavaScript due to incomplete implementation of
 * the GWT-OpenLayers library and huge amount to add support.
 */
final class LayerOptions {

  private LayerOptions() {
  }

  /**
   * Layer options are set in JavaScript due to incomplete implementation of
   * the GWT-OpenLayers library and huge amount to add support.
   * 
   * @param styleMap 
   * @return OpenLayers layerOptions JavaScript object
   */
  public static JSObject getLayerOptions(JSObject styleMap) {
    final JSObject lo = JSObject.createJSObject();

    lo.setProperty("styleMap", styleMap);
    return lo;
  }
}
