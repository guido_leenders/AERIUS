/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.layer.Markers;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;

/**
 * Layer to display a sonar on the map.
 */
public class SonarLayer extends Markers {

  interface SonarLayerEventBinder extends EventBinder<SonarLayer> { }

  private static final int SONAR_STAY_TIME = 11000;

  private final SonarLayerEventBinder eventBinder = GWT.create(SonarLayerEventBinder.class);

  private final Timer sonarTimer = new Timer() {
    @Override
    public void run() {
      clearMarkers();
    }
  };

  public SonarLayer(final EventBus eventBus) {
    super("sonarMarker");
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  void onLocationChangeEvent(final LocationChangeEvent event) {
    if (event.getValue() != null) {
      final Point point = new Point(event.getValue().getMidX(), event.getValue().getMidY());
      drawSonar(point);
    }
  }

  private void drawSonar(final Point point) {
    clearMarkers();
    final Sonar sonar = new Sonar(point);
    addMarker(sonar);
    sonarTimer.schedule(SONAR_STAY_TIME);
  }
}
