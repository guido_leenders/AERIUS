/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.event.EventListener;
import org.gwtopenmaps.openlayers.client.event.LayerLoadEndListener;
import org.gwtopenmaps.openlayers.client.event.LayerLoadStartListener;
import org.gwtopenmaps.openlayers.client.layer.Layer;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * The interface for a layer on the {@link MapLayoutPanel}.
 */
public interface MapLayer {

  /**
   *
   */
  class LayerVisibility {
    private boolean isVisible;
    private String notVisibleReason;
    private MapLayer layer;

    public void setLayer(final MapLayer layer) {
      this.layer = layer;
    }

    /**
     * Returns the text the reason a layer is not visible at a specific zoom
     * level or null in case the layer is visible.
     *
     * @return Reason layer is not visible or null if visible
     */
    public String getNotVisibleReason() {
      return notVisibleReason;
    }

    /**
     * Sets the Layer to visible. If isVisible is true and the layer was not
     * already added to the map the layer will be added. Passing isVisible
     * true doesn't mean the layer will be visible directly. Other
     * conditions, like layer not in zoom level could hide the layer.
     *
     * @param isVisible new visibility state
     * @return Returns true if the visibility state of the layer changed
     */
    public final boolean setIsVisible(final boolean isVisible) {
      final boolean change = this.isVisible != isVisible;
      this.isVisible = isVisible;
      layer.getLayerProps().setEnabled(isVisible);
      final String oldNVR = notVisibleReason;

      setNotVisibleReason(null);
      final boolean actualIsVisible = isVisible && notVisibleReason == null;

      if (layer instanceof Layer) {
        ((Layer) layer).setIsVisible(actualIsVisible);
      }
      return change || oldNVR != null && !oldNVR.equals(notVisibleReason) || oldNVR == null && notVisibleReason != null;
    }

    public LayerInZoom isLayerInZoom() {
      return inScale(layer.getMap().getScale());
    }

    private LayerInZoom inScale(final double scale) {
      final LayerInZoom value;

      final double mins = layer.getLayerProps().getMinScale();
      final double maxs = layer.getLayerProps().getMaxScale();

      if (mins > 0 && mins < scale) {
        value = LayerInZoom.EXCEED;
      } else if (maxs > 0 && scale < maxs) {
        value = LayerInZoom.INCEED;
      } else {
        value = LayerInZoom.IN_ZOOM;
      }

      return value;
    }

    /**
     * Sets the reason the layer is not visible. Set to null if the layer is
     * visible and there is so there is no reason.
     *
     * @param notVisibleReason
     */
    protected void setNotVisibleReason(final String notVisibleReason) {
      if (notVisibleReason != null) {
        this.notVisibleReason = notVisibleReason;
      } else {
        switch (isLayerInZoom()) {
        case EXCEED:
          this.notVisibleReason = M.messages().layerZoomExceeded(layer.getTitle());
          break;
        case INCEED:
          this.notVisibleReason = M.messages().layerZoomInceeded(layer.getTitle());
          break;
        case IN_ZOOM:
          this.notVisibleReason = null;
          break;
        default:
          throw new IllegalArgumentException("Unhandled LayerInZoom enum value in setNotVisibleReason(). Missing implementation.");
        }
      }
    }
  }

  /**
   * Adds a {@link LayerLoadEndListener} to the map.
   *
   * @param listener LayerLoadEndListener
   */
  void addLayerLoadEndListener(final LayerLoadEndListener listener);

  /**
   * Adds a {@link LayerLoadStartListener} to the map.
   *
   * @param listener LayerLoadStartListener
   */
  void addLayerLoadStartListener(final LayerLoadStartListener listener);

  /**
   * Adds this layer to the map.
   *
   * <p>Do not call this method directly. It's managed by {@link MapLayoutPanel}.
   */
  void attachLayer();

  /**
   * Destroys the layer. After calling this method the layer can not be reused
   * again and should be recreated.
   *
   * <p>Do not call this method directly. It's managed by {@link MapLayoutPanel}.
   */
  void destroy();

  /**
   * Returns the index of the Layer on the map.
   *
   * @return
   */
  int getLayerIndex();

  /**
   * Returns the layer properties and user settings of that layer.
   * @return layer properties
   */
  LayerProps getLayerProps();

  /**
   * Returns the OpenLayers Map instance this layer is bind to. The implementation of this interface should pass the AeriusMap via the
   * constructor and return the map from the AeriusMap.
   *
   * @return map
   */
  Map getMap();

  /**
   * The display name of the layer.
   *
   * @return display name of the layer
   */
  String getTitle();

  /**
   * Returns the text the reason a layer is not visible at a specific zoom level or null in case the layer is visible.
   *
   * @return text why the layer is not visible
   */
  String getNotVisibleReason();

  /**
   * The opacity value of the layer.
   *
   * @return opacity
   */
  float getOpacity();

  Legend getLegend();

  /**
   * Returns true if this layer is an OpenLayers base layer.
   *
   * @return true if is base layer
   */
  boolean isBaseLayer();

  /**
   * Returns if the layer can be displayed at the current zoom level.
   *
   * @return enum containing the value.
   */
  LayerInZoom isLayerInZoom();

  /**
   * Returns true if this layer is currently visible.
   *
   * @return true if the layer is visible.
   */
  boolean isVisible();

  /**
   * Redraws the layer.
   *
   * @return
   */
  boolean redraw();

  void raiseLayer(int delta);

  /**
   * Redraws the layer.
   */
  void refreshLayer();

  /**
   * Removes the EventListener.
   *
   * @param listener EventListener to remove
   */
  void removeListener(EventListener listener);

  /**
   * Set the display name of the layer.
   *
   * @param name
   */
  void setTitle(String name);

  /**
   * Makes the Layer visible. If the layer was not already added to the make
   * the layer will be added if isVisible is true. The method returns if the
   * visibility was changed.
   *
   * <p>Do not call this method directly. It's managed by {@link MapLayoutPanel}.
   *
   * @param isVisible
   * @return
   */
  boolean setVisible(boolean isVisible);

  /**
   * Set the opacity of the layer.
   *
   * <p>Do not call this method directly. It's managed by {@link MapLayoutPanel}.
   *
   * @param opacity
   */
  void setOpacity(float opacity);

  /**
   * Zooms to the minimal Scale value of this layer.
   */
  void zoomToMinScale();

  /**
   * Zooms to the maximum Scale value of this layer.
   */
  void zoomToMaxScale();
}
