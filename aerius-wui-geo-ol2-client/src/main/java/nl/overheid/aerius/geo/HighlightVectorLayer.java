/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.event.PurgeHighlightedAreaEvent;

/**
 * Layer to highlight a geometric area for a specific amount of time on the map.
 */
class HighlightVectorLayer extends Vector {

  interface HighlightVectorLayerEventBinder extends EventBinder<HighlightVectorLayer> { }

  private static final int SONAR_STAY_TIME = 11000;
  private static final AreaHighlightStyle HIGHLIGHT_VECTOR_STYLE = new AreaHighlightStyle();

  private final HighlightVectorLayerEventBinder eventBinder = GWT.create(HighlightVectorLayerEventBinder.class);
  private final Timer highlightTimer = new Timer() {
    @Override
    public void run() {
      destroyFeatures();
    }
  };

  public HighlightVectorLayer(final EventBus eventBus) {
    super("sonarVector");
    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onAreaHighlightEvent(final PurgeHighlightedAreaEvent e) {
    destroyFeatures();
  }

  @EventHandler
  void onLocationChangeEvent(final LocationChangeEvent event) {
    drawHighlight(event.getGeometry(), true);
  }

  private void drawHighlight(final WKTGeometry geometry, final boolean useTimer) {
    // Destroy current features and cancel running timer if any
    destroyFeatures();
    highlightTimer.cancel();

    // Draw the highlighting area.
    if (geometry != null) {
      addFeature(new VectorFeature(Geometry.fromWKT(geometry.getWKT()), HIGHLIGHT_VECTOR_STYLE));

      if (useTimer) {
        highlightTimer.schedule(SONAR_STAY_TIME);
      }
    }
  }
}
