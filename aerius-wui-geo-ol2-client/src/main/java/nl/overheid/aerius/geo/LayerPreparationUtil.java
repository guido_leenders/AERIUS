/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.geo.shared.MultiLayerProps;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AggregatedAsyncCallback;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.GWTAtomicInteger;
import nl.overheid.aerius.wui.main.util.NotificationUtil;

public final class LayerPreparationUtil {
  private static final HashMap<String, AggregatedAsyncCallback<LayerWMSProps>> CALL_CACHE = new HashMap<String, AggregatedAsyncCallback<LayerWMSProps>>();
  private static final HashMap<String, String> CAPABILITIES_CACHE = new HashMap<String, String>();

  /**
   * Time to wait before broadcasting errors
   */
  private static final int UI_LOAD_DELAY = 5000;

  private LayerPreparationUtil() {}

  /**
   * Prepare the given layer props, guaranteeing they can be added to a map.
   *
   * @param service Properties to prepare.
   * @param props Properties to prepare.
   * @param callback Callback to call when the properties have been prepared.
   */
  public static void prepareLayer(final FetchGeometryServiceAsync service, final LayerProps props, final AsyncCallback<LayerProps> callback) {
    if (props instanceof LayerWMSProps) {
      // WMS layers are really the only type that need preparation
      prepareWMS(service, (LayerWMSProps) props, callback);
    } else if (props instanceof MultiLayerProps) {
      // MultiLayerProps may contain WMS layers, so we must drill down into them and prepare those.
      prepareMulti(service, (MultiLayerProps) props, callback);
    } else {
      // We need not prepare other layer types, call onSuccess to the callback (do NOT defer)
      callback.onSuccess(props);
    }
  }

  /**
   * Fire-and-forget variety of {@link #prepareLayer(FetchGeometryServiceAsync, LayerProps, AsyncCallback)}.
   *
   * @param props props to prepare
   */
  public static void prepareLayer(final FetchGeometryServiceAsync service, final LayerProps props) {
    prepareLayer(service, props, new AsyncCallback<LayerProps>() {
      @Override
      public void onFailure(final Throwable caught) {}

      @Override
      public void onSuccess(final LayerProps result) {}
    });
  }

  public static void addLayerDeferred(final FetchGeometryServiceAsync service, final MapLayoutPanel map, final LayerProps props) {
    prepareLayer(service, props, new AppAsyncCallback<LayerProps>() {
      @Override
      public void onSuccess(final LayerProps result) {
        map.addLayer(result);
      }
    });
  }

  public static void addLayerDeferred(final FetchGeometryServiceAsync service, final MapLayoutPanel map, final LayerProps props,
      final EventBus eventBus) {
    prepareLayer(service, props, new AsyncCallback<LayerProps>() {
      @Override
      public void onFailure(final Throwable caught) {
        new Timer() {
          @Override
          public void run() {
            NotificationUtil.broadcastError(eventBus, M.messages().errorLayerCouldNotBeLoaded(
                props.getTitle() == null ? props.getName() : props.getTitle()));
          }
        }.schedule(UI_LOAD_DELAY);
      }

      @Override
      public void onSuccess(final LayerProps result) {
        map.addLayer(result);
      }
    });
  }

  /**
   * Recursively prepares layers within a {@link MultiLayerProps} layer, calls callback when all layers have been prepared.
   *
   * @param multiProps Layer to prepare
   * @param callback Callback to call when layers have been prepared
   */
  private static void prepareMulti(final FetchGeometryServiceAsync service, final MultiLayerProps multiProps,
      final AsyncCallback<LayerProps> callback) {
    final int size = multiProps.getLayers().size();
    final GWTAtomicInteger counter = new GWTAtomicInteger();

    // For each layer in this MultiLayerProps, prepare it recursively
    for (final LayerProps singleProps : multiProps.getLayers()) {
      prepareLayer(service, singleProps, new AsyncCallback<LayerProps>() {
        @Override
        public void onFailure(final Throwable caught) {
          callOnSuccessWhenCompleted();
        }

        @Override
        public void onSuccess(final LayerProps result) {
          callOnSuccessWhenCompleted();
        }

        private void callOnSuccessWhenCompleted() {
          final int current = counter.incrementAndGet();
          if (size == current) {
            callback.onSuccess(multiProps);
          }
        }
      });
    }
  }

  private static void prepareWMS(final FetchGeometryServiceAsync service, final LayerWMSProps props, final AsyncCallback<LayerProps> callback) {
    // Check if the capabilities for this layer are already set,
    if (props.getCapabilities().getCapabilitiesXML() != null) {
      callback.onSuccess(props);
    } else {
      final String url = props.getCapabilities().getUrl();

      // Check if we have the capabilities in the capability cache, if yes, set it and defer to .onSuccess
      if (CAPABILITIES_CACHE.containsKey(url)) {
        props.getCapabilities().setCapabilitiesXML(CAPABILITIES_CACHE.get(url));
        callback.onSuccess(props);
      } else if (CALL_CACHE.containsKey(url)) {
        // Check if we are already retrieving these specific capabilities, if yes, aggregate into that callback
        attachToAggregatedCallback(CALL_CACHE.get(url), callback, props);
      } else if (!CALL_CACHE.isEmpty()) {
        // Check if another call is already being made, if yes, wait for it to end and recursively proceed (WORK_AROUND_ALERT)

        // The map does not support ordering layers after deferredly retrieving capabilities, which is why we aggregate and chain them, this
        // statement is the chaining part and is the mother of all work-arounds.
        final AggregatedAsyncCallback<LayerWMSProps> next = CALL_CACHE.values().iterator().next();
        next.addLastListener(new AsyncCallback<LayerWMSProps>() {
          @Override
          public void onFailure(final Throwable caught) {
            prepareWMS(service, props, callback);
          }

          @Override
          public void onSuccess(final LayerWMSProps result) {
            prepareWMS(service, props, callback);
          }
        });
      } else {
        // Otherwise, initiate a new call to retrieve capabilities via an aggregationCallback that will be cached and other layers can wait for.
        fetchGetCapabilities(service, props, callback, url);
      }
    }
  }

  private static void fetchGetCapabilities(final FetchGeometryServiceAsync service, final LayerWMSProps props,
      final AsyncCallback<LayerProps> callback, final String url) {
    final AggregatedAsyncCallback<LayerWMSProps> aggregationCallback = new AggregatedAsyncCallback<LayerWMSProps>() {
      @Override
      public void onSuccess(final LayerWMSProps result) {
        CAPABILITIES_CACHE.put(props.getCapabilities().getUrl(), result.getCapabilities().getCapabilitiesXML());
        CALL_CACHE.remove(url);

        super.onSuccess(result);
      }
      @Override
      public void onFailure(final Throwable caught) {
        CAPABILITIES_CACHE.put(props.getCapabilities().getUrl(), "");
        CALL_CACHE.remove(url);
        super.onFailure(caught);
      }
    };
    attachToAggregatedCallback(aggregationCallback, callback, props);
    CALL_CACHE.put(url, aggregationCallback);
    service.fetchGetCapabilities(url, new AsyncCallback<String>() {

      @Override
      public void onSuccess(final String capabilities) {
        props.getCapabilities().setCapabilitiesXML(capabilities);
        aggregationCallback.onSuccess(props);
      }

      @Override
      public void onFailure(final Throwable caught) {
        aggregationCallback.onFailure(caught);
      }
    });
  }

  private static void attachToAggregatedCallback(final AggregatedAsyncCallback<LayerWMSProps> aggregatedAsyncCallback,
      final AsyncCallback<LayerProps> callback, final LayerWMSProps props) {
    aggregatedAsyncCallback.addListener(new AsyncCallback<LayerWMSProps>() {
      @Override
      public void onFailure(final Throwable caught) {
        callback.onFailure(caught);
      }

      @Override
      public void onSuccess(final LayerWMSProps result) {
        // Copy the resulting capabilities into this layer's props
        props.getCapabilities().setCapabilitiesXML(result.getCapabilities().getCapabilitiesXML());
        callback.onSuccess(props);
      }
    });
  }

  public static void prepareLayers(final FetchGeometryServiceAsync service, final ArrayList<LayerProps> layers,
      final AsyncCallback<Integer> asyncCallback) {
    final int size = layers.size();
    final GWTAtomicInteger counter = new GWTAtomicInteger();
    final GWTAtomicInteger succesful = new GWTAtomicInteger();

    for (final LayerProps props : layers) {
      final AsyncCallback<LayerProps> callback = new AsyncCallback<LayerProps>() {
        @Override
        public void onFailure(final Throwable caught) {
          Logger.getLogger("Application").log(Level.SEVERE, "Layer preparation fail in initial layer check.", caught);

          completeCallWhenAllDone(asyncCallback, size, counter, succesful);
        }

        @Override
        public void onSuccess(final LayerProps result) {
          succesful.incrementAndGet();
          completeCallWhenAllDone(asyncCallback, size, counter, succesful);
        }

        private void completeCallWhenAllDone(final AsyncCallback<Integer> asyncCallback, final int size,
            final GWTAtomicInteger counter, final GWTAtomicInteger succesful) {
          if (counter.incrementAndGet() == size) {
            asyncCallback.onSuccess(succesful.get());
          }
        }
      };

      LayerPreparationUtil.prepareLayer(service, props, callback);
    }
  }
}
