/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.ArrayList;
import java.util.HashMap;

import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.MapOptions;
import org.gwtopenmaps.openlayers.client.Projection;
import org.gwtopenmaps.openlayers.client.control.Attribution;
import org.gwtopenmaps.openlayers.client.control.MousePosition;
import org.gwtopenmaps.openlayers.client.control.MousePositionOptions;
import org.gwtopenmaps.openlayers.client.control.MousePositionOutput;
import org.gwtopenmaps.openlayers.client.control.Navigation;
import org.gwtopenmaps.openlayers.client.control.NavigationOptions;
import org.gwtopenmaps.openlayers.client.control.ScaleLine;
import org.gwtopenmaps.openlayers.client.control.ScaleLineOptions;
import org.gwtopenmaps.openlayers.client.event.MapZoomListener;
import org.gwtopenmaps.openlayers.client.layer.Layer;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JObjectArray;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ProvidesResize;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.geo.client.events.LayerChangeEvent.CHANGE;
import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSG;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * {@link LayoutPanel} for displaying a {@link Map}.
 */
@Singleton
public class MapLayoutPanel extends LayoutPanel implements RequiresResize, ProvidesResize {

  interface MapLayoutPanelEventBinder extends EventBinder<MapLayoutPanel> {}

  private final MapLayoutPanelEventBinder eventBinder = GWT.create(MapLayoutPanelEventBinder.class);

  /**
   * Helper interface to create custom layers.
   */
  public interface SimpleLayerFactory {
    /**
     * Creates a MapLayer based on the LayerProps information.
     *
     * @param eventBus EventBus to listen to layer change events
     * @param map Map to which the new layer will be attached.
     * @param li Input data to create layer from
     * @return A new MapLayer
     */
    MapLayer createLayer(EventBus eventBus, MapLayoutPanel map, LayerProps li);
  }

  protected final AbsolutePanel panel = new AbsolutePanel();
  private final ArrayList<MapLayer> layers = new ArrayList<MapLayer>();
  private final SimpleLayerFactory slf;
  private final MapZoomListener zoomListener = new MapZoomListener() {
    @Override
    public void onMapZoom(final MapZoomEvent eventObject) {
      refreshLayers();
    }
  };
  private InteractionLayer<?> interactionLayer;
  private InteractionLayer<?> defaultInteractionLayer;
  private final Map map;
  private Bounds extent;

  private final HighlightVectorLayer highlightVectorLayer;
  private final SonarLayer sonarMarkerLayer;
  private final Vector vectorLayerDivider = new Vector("__vector_divider_layer__");
  protected final EventBus eventBus;

  private final HashMap<LayerProps, Integer> layerOrdering = new HashMap<LayerProps, Integer>();
  private final ReceptorGridSettings receptorGridSettings;

  @Inject
  public MapLayoutPanel(final SimpleLayerFactory slf, final EventBus eventBus, final Context context, final NavigationHandler navHandler) {
    this(slf, eventBus, context.getReceptorGridSettings(), navHandler);
  }

  protected MapLayoutPanel(final SimpleLayerFactory slf, final EventBus eventBus, final ReceptorGridSettings receptorGridSettings) {
    this(slf, eventBus, receptorGridSettings, new NavigationHandler());
  }

  private MapLayoutPanel(final SimpleLayerFactory slf, final EventBus eventBus, final ReceptorGridSettings receptorGridSettings,
      final NavigationHandler navHandler) {
    this.slf = slf;
    this.eventBus = eventBus;
    this.receptorGridSettings = receptorGridSettings;
    add(panel);
    setWidgetTopBottom(panel, 0, Unit.PX, 0, Unit.PX);
    final LayoutPanel mapPanel = new LayoutPanel();

    // Explicitly set width and height, needed for Map to work correctly.
    mapPanel.setWidth("100%");
    mapPanel.setHeight("100%");
    final MapOptions mapOptions = getMapOptions(receptorGridSettings.getEpsg());
    map = new Map(mapPanel.getElement(), mapOptions);
    panel.add(mapPanel);

    // Scale line on map.
    final ScaleLineOptions opties = new ScaleLineOptions();

    opties.setBottomInUnits("");
    final ScaleLine scaleLine = new ScaleLine(opties);
    scaleLine.getJSObject().setProperty("div", DOM.getElementById("scaleline-id"));
    map.addControl(scaleLine);

    // Enable navigation handlers
    final NavigationOptions no = new NavigationOptions();

    final MousePositionOptions mpo = new MousePositionOptions();
    mpo.setFormatOutput(new MousePositionOutput() {
      @Override
      public String format(final LonLat lonLat, final Map map) {
        return "x: " + MathUtil.round(lonLat.lon()) + " y:" + MathUtil.round(lonLat.lat());
      }
    });

    no.setDocumentDrag(true);
    map.addControl(new Navigation(no));
    map.addControl(new Attribution());
    map.addControl(new MousePosition(mpo));

    map.addMapZoomListener(zoomListener);

    // Register navigation with the map
    navHandler.registerMap(map);
    map.addLayer(vectorLayerDivider);
    highlightVectorLayer = new HighlightVectorLayer(eventBus);
    map.addLayer(highlightVectorLayer);
    sonarMarkerLayer = new SonarLayer(eventBus);
    map.addLayer(sonarMarkerLayer);
    defaultInteractionLayer = new VectorInteractionLayer(this);
    defaultInteractionLayer.activate();
    highlightVectorLayer.setZIndex(R.css().zIndexSonarVectorLayer());
    sonarMarkerLayer.setZIndex(R.css().zIndexSonarMarkerLayer());

    ensureDebugId(TestID.MAP);
  }

  /**
   * This has become optional for implementations that do not extend MapLayoutPanel but instead use it directly.
   *
   * (Extending classes may also bind handlers for parent)
   */
  public HandlerRegistration attachEventBinders() {
    return eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onLocationChangeEvent(final LocationChangeEvent event) {
    final BBox bbox = event.getValue() == null ? receptorGridSettings.getBoundingBox() : event.getValue();
    final Bounds bounds = WktUtil.toBounds(bbox);

    if (event.isSoft() && map.getExtent().containsBounds(bounds, false, true)) {
      return;
    }

    map.zoomToExtent(bounds);
  }

  private static MapOptions getMapOptions(final EPSG epsg) {
    final MapOptions defaultMapOptions = new MapOptions();

    defaultMapOptions.setAllOverlays(true);
    defaultMapOptions.setProjection(epsg.getEpsgCode());
    defaultMapOptions.setDisplayProjection(new Projection(epsg.getEpsgCode()));
    defaultMapOptions.setResolutions(epsg.getResolutions());
    defaultMapOptions.setNumZoomLevels(epsg.getZoomLevel());
    if (epsg.getMaxResolution() > 0) {
      defaultMapOptions.setMaxResolution(epsg.getMaxResolution());
    }

    defaultMapOptions.setUnits(epsg.getUnit());
    defaultMapOptions.setControls(new JObjectArray(new JSObject[] {}));
    defaultMapOptions.setMaxExtent(WktUtil.toBounds(epsg.getBounds()));
    return defaultMapOptions;
  }

  /**
   * Add a layer to the map. Use this for special purpose layers, like a
   * marker layer. The layer needs to set its own z-index.
   *
   * @param layer Layer to add
   */
  public void addLayer(final Layer layer) {
    if (map.getLayerIndex(layer) < 0) {
      map.addLayer(layer);
      final InteractionLayer<?> il = interactionLayer == null ? defaultInteractionLayer == null ? null
          : defaultInteractionLayer : interactionLayer;

      //add layer below interaction layer, if no interaction layer available
      //this layer is added as last layer, so no correction needed.
      if (il != null) {
        map.setLayerIndex(layer, map.getLayerIndex(il.asLayer()));
      }
    }
  }

  /**
   * Add a {@link MapLayer} to the map. The MapLayer must be pre-configured.
   * This method of adding a layer to the map skips the usage of a LayerProps and
   * a LayerFactory, allowing the creation of layers created solely on the
   * client. If the layer is added a {@link LayerChangeEvent#ADD} is fired.
   *
   * @param layer MapLayer to add
   */
  public void addLayer(final MapLayer layer) {
    layers.add(layer);
    layer.attachLayer();

    eventBus.fireEvent(new LayerChangeEvent(layer, LayerChangeEvent.CHANGE.ADDED));
  }

  public void setLayerTitle(final MapLayer layer, final String name) {
    layer.setTitle(name);
    eventBus.fireEvent(new LayerChangeEvent(layer, CHANGE.NAME));
  }

  public MapLayer addLayer(final LayerProps lp) {
    final MapLayer layer = slf.createLayer(eventBus, this, lp);
    if (layer != null) {
      layer.setOpacity(lp.getOpacity());
      addLayer(layer);
    }

    return layer;
  }

  /**
   * Returns the default interaction layer.
   *
   * @return default InteractionLayer
   */
  public InteractionLayer<?> getDefaultInteractionLayer() {
    return defaultInteractionLayer;
  }

  /**
   * EventBus of this MapLayoutPanel.
   *
   * @return
   */
  public final EventBus getEventBus() {
    return eventBus;
  }

  /**
   * Returns the layer in order of lowest layer first, layer on top last.
   *
   * @return ArrayList of MapLayers
   */
  public ArrayList<MapLayer> getLayers() {
    return layers;
  }

  /**
   * Finds the {@link MapLayer} object related to the layer with the given name.
   * @param layerName name of the layer to search
   * @return maplayer object or null
   */
  public MapLayer getLayer(final String layerName) {
    for (final MapLayer layer : layers) {
      if ((layer.getLayerProps() != null) && (layerName != null) && layerName.equals(layer.getLayerProps().getName())) {
        return layer;
      }
    }
    return null;
  }

  /**
   * Returns the embedded {@link Map} object.
   *
   * NOTE: Do not directly add layers to this map, but use the methods
   * in this class. Because layer zIndex is managed in this class.
   *
   * @return Open Layers Map of this panel
   */
  public Map getMap() {
    return map;
  }

  /**
   * Returns the {@link SimpleLayerFactory} instance for this map.
   *
   * @return SimpleLayerFactory
   */
  SimpleLayerFactory getSimpleLayerFactory() {
    return slf;
  }

  /**
   * Raises the layer with the delta. Use this method instead of directly
   * calling raiseLayer on {@link Map} because this method takes into account
   * the fact that layers can consist of {@link GroupLayer}'s and this
   * method recalculates the delta based on this information.
   *
   * @param layer Layer to raise
   * @param delta relative delta to raise
   */
  public void raiseLayer(final MapLayer layer, final int delta) {
    final int curIdx = layers.indexOf(layer);
    final int newIdx = Math.min(Math.max(0, curIdx - delta), layers.size() - 1);
    final int actualIdx = layer.getLayerIndex();
    //if newIdx is 0 if will become the lowest layer, so it's set to 0,
    //because getLayerIndex returns the highest layer in ComplexLayers and
    //thus if positioned as last it won't work.
    final int actualNewIdx = newIdx == 0 ? 0 : layers.get(newIdx).getLayerIndex();

    layers.remove(layer);
    layers.add(newIdx, layer);
    layer.raiseLayer(actualNewIdx - actualIdx);
    eventBus.fireEvent(new LayerChangeEvent(layer, LayerChangeEvent.CHANGE.ORDER));
  }

  /**
   * Redraws all attached MapLayers.
   */
  public void refreshLayers() {
    for (final MapLayer layer : layers) {
      layer.refreshLayer();
    }
  }

  /**
   * Removes the layer that matches the {@link LayerProps} object. This can be
   * used to remove layers without a direct pointer to the actual layer
   * instance.
   *
   * @param li LayerProps to remove
   */
  public void removeLayer(final LayerProps li) {
    li.setEnabled(false);
    for (final MapLayer layer : layers) {
      if (layer.getLayerProps().equals(li)) {
        removeLayer(layer);
      }
    }
  }

  /**
   * Removes a {@link MapLayer} from the map.
   *
   * @param layer Layer to remove
   */
  public void removeLayer(final MapLayer layer) {
    layers.remove(layer);
    layerOrdering.remove(layer.getLayerProps());
    layer.destroy();
    eventBus.fireEvent(new LayerChangeEvent(layer, LayerChangeEvent.CHANGE.REMOVED));
  }

  /**
   * Removes a {@link Layer}.
   *
   * NOTE: Only use to remove layers which where added as {@link Layer} object, ie by using .addLayer(Layer)
   *
   * @param layer Layer to remove
   */
  public void removeLayer(final Layer layer) {
    if ((layer.getJSObject() != null) && (map.getLayerIndex(layer) >= 0)) {
      map.removeLayer(layer);
    }
  }

  /**
   * Unsets the current interaction layer and if setDefault is true activates
   * the default interaction layer.
   *
   * @param setDefault
   */
  void resetInteractionLayer(final boolean setDefault) {
    interactionLayer = null;
    if (setDefault && (defaultInteractionLayer != null)) {
      defaultInteractionLayer.activate();
    }
  }

  /**
   * Sets the default interaction layer.
   *
   * @param layer interaction layer to set as default
   */
  public void setDefaultInteractionLayer(final InteractionLayer<?> layer) {
    if (defaultInteractionLayer != null) {
      defaultInteractionLayer.deactivate(false);
    }
    defaultInteractionLayer = layer;
    defaultInteractionLayer.activate();
  }

  public ReceptorGridSettings getReceptorGridSettings() {
    return receptorGridSettings;
  }

  public void setDefaultExtent() {
    setExtent(WktUtil.toBounds(receptorGridSettings.getBoundingBox()));
  }

  /**
   * Set the default zoom extent, which will be zoomed to when
   * {@link #zoomToExtent()} is called.
   *
   * @param extent Extent
   */
  public void setExtent(final Bounds extent) {
    this.extent = extent;
  }

  /**
   * This method is called from the {@link InteractionLayer#activate()} method
   * and should probably never be called otherwise.
   *
   * @param layer layer to set as current interaction layer
   */
  void setInteractionLayer(final InteractionLayer<?> layer) {
    if ((interactionLayer != null) && (layer != interactionLayer)) {
      interactionLayer.deactivate(false);
    }
    interactionLayer = layer;
    map.addLayer(layer.asLayer());
    map.setLayerIndex(interactionLayer.asLayer(), R.css().zIndexInteractionLayer());
  }

  /**
   * Sets the opacity of the layer.
   *
   * @param layer Layer to set opacity
   * @param opacity float opacity value between 0 and 1
   */
  public void setOpacity(final MapLayer layer, final float opacity) {
    layer.setOpacity(opacity);
    eventBus.fireEvent(new LayerChangeEvent(layer, CHANGE.OPACITY));
  }

  /**
   * Set the layer visibility. Due to other conditions the layer might not
   * become visible. For example if zoom conditions exist and the layer is
   * outside its zoom range the layer doesn't become visible until it is in
   * range. When this method is used instead of directly setting visibility on
   * the layer and event is triggered when the layers visibility has changed.
   * This is preferred as it makes it possible for other parts of the
   * application to react on layer visibilities changes. For example update a
   * list of layer status.
   *
   * @param layer layer to set visibility
   * @param visible new visibility value
   */
  public void setVisible(final MapLayer layer, final boolean visible) {
    if (layer.setVisible(visible)) {
      eventBus.fireEvent(new LayerChangeEvent(layer, layer.isVisible() ? LayerChangeEvent.CHANGE.VISIBLE
          : LayerChangeEvent.CHANGE.HIDDEN));
    }
  }

  public void setVisible(final String layerName, final boolean visible) {
    final MapLayer layer = getLayer(layerName);
    if (layer != null) {
      setVisible(layer, visible);
    }
  }

  /**
   * Set the navigation widget above the map.
   *
   * @param navigationWidget
   */
  public void setNavigationWidget(final NavigationWidget navigationWidget) {
    panel.add(navigationWidget, 0, 0);
    navigationWidget.getElement().getStyle().setZIndex(R.css().zIndexNavigationWidget());
  }

  public void addWidget(final Widget w) {
    panel.add(w);
  }

  /**
   * Zooms to Extent using a scheduleDeferred command so it will be zoomed
   * after everything has finished. If bounds is null no action will be taken.
   *
   * @param bounds Bounds to zoom to
   */
  public void zoomDeferred(final Bounds bounds) {
    if (bounds != null) {
      Scheduler.get().scheduleDeferred(new ScheduledCommand() {
        @Override
        public void execute() {
          map.zoomToExtent(bounds);
        }
      });
    }
  }

  /**
   * Zooms to the extent set on the navigation widget, if a navigation widget
   * is present, otherwise nothing happens.
   */
  public void zoomToExtent() {
    if (extent != null) {
      map.zoomToExtent(extent);
    }
  }

  /**
   * This method adds map layer to the open layers map and corrects the zindex
   * such that the layer is positioned under the vector layers.
   *
   * @param layer
   */
  public void addLayerToMap(final Layer layer) {
    if (map.getLayerIndex(layer) < 0) {
      map.addLayer(layer);
      map.setLayerIndex(layer, map.getLayerIndex(vectorLayerDivider));
    }
    if (layer instanceof MapLayer) {
      //explicitly set layer visibility, this will also check if layer is not
      //visible due to specific rules, like not in zoom level.
      setVisible((MapLayer) layer, ((MapLayer) layer).getLayerProps().isEnabled());
    }
  }
}
