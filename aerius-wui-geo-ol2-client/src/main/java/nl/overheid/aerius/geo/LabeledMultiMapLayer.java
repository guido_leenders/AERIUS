/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.LabeledMultiLayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.LayerWMSProps;

public class LabeledMultiMapLayer extends MultiMapLayer {

  public LabeledMultiMapLayer(final EventBus eventBus, final MapLayoutPanel map, final LabeledMultiLayerProps props) {
    super(eventBus, map, props);
  }

  @Override
  public void refreshLayer() {
    map.setVisible(this, props.isEnabled());
  }

  @Override
  public Legend getLegend() {
    return getLabelLayer().getLegend();
  }

  @Override
  public boolean setVisible(final boolean isVisible) {
    props.setEnabled(isVisible);
    boolean changed = false;

    for (final MapLayer layer : layers) {
      changed = layer.setVisible(isVisible) || changed;
    }
    return changed;
  }

  public MapLayer getLabelLayer() {
    return layers.get(0);
  }

  public MapLayer getDefaultLayer() {
    return layers.get(layers.size() - 1);
  }

  public void setParamFilter(final String type, final String value) {
    for (final MapLayer l : getLayers()) { 
      if (l.getLayerProps() instanceof LayerWMSProps) {
        final LayerWMSProps multiproperties = (LayerWMSProps) l.getLayerProps();
        multiproperties.setParamFilter(type, value);
      }
    }
  }
}
