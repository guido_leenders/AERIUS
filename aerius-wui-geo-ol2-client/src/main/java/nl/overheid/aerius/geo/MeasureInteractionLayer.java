/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.control.Measure;
import org.gwtopenmaps.openlayers.client.control.MeasureOptions;
import org.gwtopenmaps.openlayers.client.event.MeasureEvent;
import org.gwtopenmaps.openlayers.client.event.MeasureListener;
import org.gwtopenmaps.openlayers.client.event.MeasurePartialListener;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.geometry.MultiLineString;
import org.gwtopenmaps.openlayers.client.handler.Handler;
import org.gwtopenmaps.openlayers.client.handler.HandlerOptions;
import org.gwtopenmaps.openlayers.client.handler.PathHandlerOptions;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.user.client.DOM;

/**
 * Interaction Layer to measure items on the map. This can be used to draw
 * lines and polygons and measure the length and size.
 */
public class MeasureInteractionLayer extends AbstractInteractionLayer<Vector> implements MeasureListener, MeasurePartialListener {

  /**
   * Listener to use to get notified when a geometry is measured..
   */
  public interface MeasureInteractionLayerListener {

    /**
     * @param event The measure event
     */
    void onMeasure(MeasureInteractionEvent event);

    void onMeasurePartial(MeasureInteractionEvent measureInteractionEvent);
  }

  public class MeasureInteractionEvent {
    private final Geometry geometry;
    private final int measure;
    private final double x;
    private final double y;

    /**
     * @param geometry The geometry that is measured.
     * @param measure The measure of the event
     * (1 if a point, length in m of a line, surface in m^2 of a polygon).
     * @param x The x coordinate of the geometry
     * (x-coordinate of a point, x-coordinate of middlepoint of a line, x-coordinate of centroid of polygon).
     * @param y The y coordinate of the geometry (see x).
     */
    public MeasureInteractionEvent(final Geometry geometry, final int measure, final double x, final double y) {
      this.geometry = geometry;
      this.measure = measure;
      this.x = x;
      this.y = y;
    }

    /**
     * @return The geometry that is measured.
     */
    public Geometry getGeometry() {
      return geometry;
    }

    /**
     * @return The measure of the event
     */
    public int getMeasure() {
      return measure;
    }

    /**
     * @return The x coordinate of the geometry
     */
    public double getX() {
      return x;
    }

    /**
     * @return The y coordinate of the geometry
     */
    public double getY() {
      return y;
    }
  }

  private final Measure measureControl;
  private final MeasureInteractionLayerListener listener;

  /**
   * @param map The map
   * @param handler The handler for the measure control. Use to assure you get a certain geometry as result.
   * @param styleMap The (styling) options for the layer.
   * @param listener The listener to notify on measure.
   */
  public MeasureInteractionLayer(final MapLayoutPanel map, final Handler handler, final JSObject styleMap, final MeasureInteractionLayerListener listener) {
    super(map, new Vector("Measure" + DOM.createUniqueId()));
    final MeasureOptions options = new MeasureOptions();
    final JSObject layerOptions = LayerOptions.getLayerOptions(styleMap);
    options.setPersist(true);
    final HandlerOptions handlerOptions = new PathHandlerOptions();
    handlerOptions.getJSObject().setProperty("layerOptions", layerOptions);

    options.setHandlerOptions(handlerOptions);
    this.listener = listener;
    measureControl = new Measure(handler, options);
    measureControl.addMeasureListener(this);
    measureControl.setImmediate(true);
  }

  @Override
  public void onMeasure(final MeasureEvent event) {
    listener.onMeasure(getMeasureInteractionEvent(event));
  }

  @Override
  public void onMeasurePartial(final MeasureEvent event) {
    listener.onMeasurePartial(getMeasureInteractionEvent(event));
  }

  public void activatePartialListener() {
    measureControl.addMeasurePartialListener(this);
  }

  @Override
  protected void preActivate() {
    getMap().addControl(measureControl);
    measureControl.activate();
  }

  @Override
  protected void preDeactivate() {
    measureControl.deactivate();
    getMap().removeControl(measureControl);
  }

  @Override
  public void destroy() {
    deactivate();
  }

  private MeasureInteractionEvent getMeasureInteractionEvent(final MeasureEvent event) {
    int measureFactor = 0;
    LonLat ll = event.getGeometry().getBounds().getCenterLonLat();
    if (Geometry.LINESTRING_CLASS_NAME.equals(event.getGeometry().getClassName())
        || Geometry.MULTI_LINE_STRING_CLASS_NAME.equals(event.getGeometry().getClassName())) {
      final MultiLineString line = MultiLineString.narrowToMultiLineString(event.getGeometry().getJSObject());
      measureFactor = "km".equals(event.getUnits()) ? 1000 : 1;
      ll = LineUtil.middleOfMultiLineString(line).getBounds().getCenterLonLat();
    } else if (Geometry.POLYGON_CLASS_NAME.equals(event.getGeometry().getClassName())
        || Geometry.MULTI_POLYGON_CLASS_NAME.equals(event.getGeometry().getClassName())) {
      measureFactor = "km".equals(event.getUnits()) ? 1000 * 1000 : 1;
    }

    final int measure = measureFactor == 0 ? 1 : Math.round(measureFactor * event.getMeasure());

    return new MeasureInteractionEvent(event.getGeometry(), measure, ll.lon(), ll.lat());
  }
}
