/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.layer.Markers;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;

/**
 *  Shows marker icons on layers.
 *
 *  @param <M> type of markers implement @MarkerItem
 */
public class MarkerLayer extends Markers implements MapLayer {
  private final LayerProps li = new LayerProps();
  private final MapLayoutPanel map;
  private final Legend legend;

  /**
   * Init map with markers and title.
   *
   * @param map the layer for the markers
   * @param title title for the map
   * @param legend 
   */
  public MarkerLayer(final MapLayoutPanel map, final String title, final Legend legend) {
    super(title);
    this.map = map;
    this.legend = legend;
    li.setEnabled(true);

  }

  @Override
  public void attachLayer() {
    map.addLayerToMap(this);
  }

  @Override
  public int getLayerIndex() {
    return getMap().getLayerIndex(this);
  }

  @Override
  public LayerProps getLayerProps() {
    return li;
  }

  @Override
  public Legend getLegend() {
    return legend;
  }

  @Override
  public Map getMap() {
    return map.getMap();
  }

  @Override
  public String getNotVisibleReason() {
    return null;
  }

  @Override
  public LayerInZoom isLayerInZoom() {
    return LayerInZoom.IN_ZOOM;
  }

  @Override
  public void raiseLayer(final int delta) {
    getMap().raiseLayer(this, delta);
  }

  @Override
  public void refreshLayer() {
    // Do nothing
  }

  @Override
  public boolean setVisible(final boolean isVisible) {
    setIsVisible(isVisible);
    return true;
  }

  @Override
  public void zoomToMinScale() {
    getMap().zoomToScale(1, true);
  }

  @Override
  public void zoomToMaxScale() {
    // No-op
  }

  @Override
  public String getTitle() {
    return getName();
  }

  @Override
  public void setTitle(final String name) {
    setName(name);
  }

}
