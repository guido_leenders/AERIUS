/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.geometry.MultiLineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;

import nl.overheid.aerius.shared.MathUtil;

/**
 * Util class for line geometries.
 */
public final class LineUtil {

  private LineUtil() {
  }

  /**
   * Calculates the point in the middle of a {@link MultiLineString}.
   *
   * @param line {@link MultiLineString} to calculate middle point
   * @return point in the middle or exact point if not found
   */
  public static Point middleOfMultiLineString(final MultiLineString line) {
    final double length = line.getLength();
    final int middle = MathUtil.round(length / 2);
    Point prevPoint = Point.narrowToPoint(line.getComponent(0));
    int curLength = 0;
    for (int i = 1; i < line.getNumberOfComponents(); i++) {
      final Point nextPoint = Point.narrowToPoint(line.getComponent(i));
      final double d = prevPoint.distanceTo(nextPoint);

      if (curLength + d >= middle) {
        final double posOnLine = (middle - curLength) / d;

        return new Point(prevPoint.getX() + (nextPoint.getX() - prevPoint.getX()) * posOnLine, prevPoint.getY()
            + (nextPoint.getY() - prevPoint.getY()) * posOnLine);
      }
      curLength += d;
      prevPoint = nextPoint;
    }
    return prevPoint;
  }
}
