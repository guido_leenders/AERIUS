/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.ArrayList;

import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.StyleMap;
import org.gwtopenmaps.openlayers.client.control.DrawFeature;
import org.gwtopenmaps.openlayers.client.control.DrawFeature.FeatureAddedListener;
import org.gwtopenmaps.openlayers.client.control.DrawFeatureOptions;
import org.gwtopenmaps.openlayers.client.event.EventHandler;
import org.gwtopenmaps.openlayers.client.event.EventObject;
import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.geometry.Geometry;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.MultiLineString;
import org.gwtopenmaps.openlayers.client.geometry.MultiPolygon;
import org.gwtopenmaps.openlayers.client.handler.Handler;
import org.gwtopenmaps.openlayers.client.handler.HandlerOptions;
import org.gwtopenmaps.openlayers.client.handler.PathHandlerOptions;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.user.client.DOM;

import nl.overheid.aerius.geo.shared.Point;

/**
 * Interaction Layer to wrap {@link DrawFeature} object.
 */
public class DrawFeatureInteractionLayer extends AbstractInteractionLayer<Vector> implements FeatureAddedListener {

  /**
   * Listener to use to get notified when a geometry is measured..
   */
  public interface DrawFeatureInteractionLayerListener {

    /**
     * @param geometry The geometry that is measured.
     * @param measure The measure of the geometry
     * (1 if a point, length in m of a line, surface in m^2 of a polygon).
     * @param x The x coordinate of the geometry
     * (x-coordinate of a point, x-coordinate of middlepoint of a line, x-coordinate of centroid of polygon).
     * @param y The y coordinate of the geometry (see x).
     * @param endPoint The point which was chosen as endpoint if geometry was a line and endpoints were supplied.
     * Null if these conditions do not hold.
     */
    void onDrawEnd(final Geometry geometry, final double measure, final double x, final double y, final Point endPoint);

  }

  private final DrawFeature drawFeature;
  private final DrawFeatureInteractionLayerListener listener;
  private VectorFeature lastFeature;
  private ArrayList<Point> endPoints = new ArrayList<Point>();
  private Double endPointSearchDistance;

  /**
   *
   * @param map Map to attach this layer
   * @param handler type of feature, e.g. PointHandler
   * @param styleMap the styling options for the layer.
   * @param listener The listener to notify when drawing is done.
   */
  public DrawFeatureInteractionLayer(final MapLayoutPanel map, final Handler handler, final JSObject styleMap,
      final DrawFeatureInteractionLayerListener listener) {
    super(map, new Vector("DrawFeature" + DOM.createUniqueId()));
    final DrawFeatureOptions drawFeatureOptions = new DrawFeatureOptions();
    final JSObject layerOptions = LayerOptions.getLayerOptions(styleMap);
    final HandlerOptions handlerOptions = new PathHandlerOptions();
    handlerOptions.getJSObject().setProperty("layerOptions", layerOptions);
    drawFeatureOptions.setHandlerOptions(handlerOptions);

    this.listener = listener;
    drawFeatureOptions.onFeatureAdded(this);
    asLayer().setStyleMap(StyleMap.narrowToOpenLayersStyleMap(styleMap));
    drawFeature = new DrawFeature(asLayer(), handler, drawFeatureOptions);
  }

  @Override
  protected void preActivate() {
    getMap().addControl(drawFeature);
    drawFeature.activate();
  }

  @Override
  protected void preDeactivate() {
    getMap().removeControl(drawFeature);
    drawFeature.deactivate();
  }

  @Override
  public void destroy() {
    deactivate();
    asLayer().destroyFeatures();
  }

  /**
   * Set the start point of the geometry (only use for path/polygon).
   * @param x The x-coordinate of the starting point.
   * @param y The y-coordinate of the starting point.
   */
  public void setStartPoint(final double x, final double y) {
    //can't add right away, the mouse has to move over the map to insertXY.
    getMap().getEvents().register("mousemove", getMap(), new EventHandler() {

      @Override
      public void onHandle(final EventObject eventObject) {
        drawFeature.insertXY(x, y);
        getMap().getEvents().unregister("mousemove", getMap(), this);
      }
    });
  }

  /**
   * Ensure when the drawing is finished with a double-click, the route will end at certain points.
   * Which one is chosen to snap to is determined by checking for nearest point.
   *
   * @param points The points to which can be attached.
   */
  public void attachOneToEnd(final ArrayList<Point> points) {
    this.endPoints = points;
  }

  /**
   * Set the maximum distance an end point may be away to attach to it. If no end point is found
   * within this distance, no additional point is added.
   * @param endPointSearchDistance Search range distance; if null then there is no limit.
   */
  public void setEndPointSearchDistance(final Double endPointSearchDistance) {
    this.endPointSearchDistance = endPointSearchDistance;
  }

  /**
   * Called when a feature is added on the layer.
   * @param vf The feature added on the layer.
   */
  @Override
  public void onFeatureAdded(final VectorFeature vf) {
    if (lastFeature != null) {
      asLayer().removeFeature(lastFeature);
    }
    final Point endPoint = determineEnd(vf.getGeometry());
    lastFeature = vf;
    if (listener != null) {
      double measure = 1;
      LonLat ll = vf.getGeometry().getBounds().getCenterLonLat();
      if (Geometry.LINESTRING_CLASS_NAME.equals(vf.getGeometry().getClassName())
          || Geometry.MULTI_LINE_STRING_CLASS_NAME.equals(vf.getGeometry().getClassName())) {
        final MultiLineString line = MultiLineString.narrowToMultiLineString(vf.getGeometry().getJSObject());
        ll = LineUtil.middleOfMultiLineString(line).getBounds().getCenterLonLat();
        measure = line.getLength();
      } else if (Geometry.POLYGON_CLASS_NAME.equals(vf.getGeometry().getClassName())
          || Geometry.MULTI_POLYGON_CLASS_NAME.equals(vf.getGeometry().getClassName())) {
        final MultiPolygon polygon = MultiPolygon.narrowToMultiPolygon(vf.getGeometry().getJSObject());
        measure = polygon.getArea();
      }
      listener.onDrawEnd(vf.getGeometry(), measure, ll.lon(), ll.lat(), endPoint);
    }
  }

  private Point determineEnd(final Geometry geometry) {
    Point returnPoint = null;
    if (Geometry.LINESTRING_CLASS_NAME.equals(geometry.getClassName())) {
      final LineString line = LineString.narrowToLineString(geometry.getJSObject());
      final org.gwtopenmaps.openlayers.client.geometry.Point gwtPoint =
          org.gwtopenmaps.openlayers.client.geometry.Point.narrowToPoint(line.getComponent(line.getNumberOfComponents() - 1));
      final Point lastPoint = new Point(gwtPoint.getX(), gwtPoint.getY());

      final Point nearestEndPoint = determineNearestEndPoint(lastPoint);

      if (nearestEndPoint == null) {
        // No end point found, just return the last point.
        returnPoint = lastPoint;
      } else {
        // An end point was found, add it to the geometry.
        line.addComponent(new org.gwtopenmaps.openlayers.client.geometry.Point(nearestEndPoint.getX(), nearestEndPoint.getY()));
        returnPoint = nearestEndPoint;
      }
    }
    return returnPoint;
  }

  private Point determineNearestEndPoint(final Point lastPoint) {
    Point nearestPoint = null;
    if ((endPoints != null) && !endPoints.isEmpty()) {
      for (final Point endPoint : endPoints) {
        if ((nearestPoint == null) || (endPoint.distance(lastPoint) < nearestPoint.distance(lastPoint))) {
          nearestPoint = endPoint;
        }
      }
    }
    if ((endPointSearchDistance != null) && (nearestPoint != null) && (nearestPoint.distance(lastPoint) > endPointSearchDistance)) {
      // Nearest point found not within the search range, return null.
      nearestPoint = null;
    }
    return nearestPoint;
  }
}
