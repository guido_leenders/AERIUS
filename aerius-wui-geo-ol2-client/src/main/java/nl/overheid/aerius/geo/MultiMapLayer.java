/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.event.EventListener;
import org.gwtopenmaps.openlayers.client.event.LayerLoadEndListener;
import org.gwtopenmaps.openlayers.client.event.LayerLoadStartListener;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.client.events.LayerChangeEvent;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;
import nl.overheid.aerius.geo.shared.MultiLayerProps;

/**
 *
 */
public class MultiMapLayer implements MapLayer {

  interface MultiLayerEventBinder extends EventBinder<MultiMapLayer> {}

  private final MultiLayerEventBinder eventBinder = GWT.create(MultiLayerEventBinder.class);

  protected final MapLayoutPanel map;
  protected final MultiLayerProps props;
  protected final List<MapLayer> layers = new ArrayList<MapLayer>();
  private final EventBus eventBus;

  private final HandlerRegistration eventHandles;

  public MultiMapLayer(final EventBus eventBus, final MapLayoutPanel map, final MultiLayerProps props) {
    this.eventBus = eventBus;
    this.map = map;
    this.props = props;

    drawLayers();

    eventHandles = eventBinder.bindEventHandlers(this, eventBus);
  }

  private void drawLayers() {
    final HashSet<LayerProps> existing = new HashSet<LayerProps>();
    final HashSet<MapLayer> toRemove = new HashSet<MapLayer>();
    for (final MapLayer layer : layers) {
      if (!props.getLayers().contains(layer.getLayerProps())) {
        map.removeLayer(layer);
        toRemove.add(layer);
      } else {
        existing.add(layer.getLayerProps());
        map.setOpacity(layer, layer.getLayerProps().getOpacity());
      }
    }
    layers.removeAll(toRemove);
    for (final LayerProps li : props.getLayers()) {
      if (!existing.contains(li)) {
        final MapLayer layer = map.getSimpleLayerFactory().createLayer(eventBus, map, li);
        if (layer != null) {
          layers.add(layer);
          layer.attachLayer();
          map.setOpacity(layer, li.getOpacity());
        }
      }
    }
  }

  @EventHandler
  public void onLayerChange(final LayerChangeEvent event) {
    switch (event.getChange()) {
    case VISIBLE:
    case HIDDEN:
      for (final MapLayer l : layers) {
        if (l.equals(event.getLayer())) {
          eventBus.fireEvent(new LayerChangeEvent(MultiMapLayer.this, event.getChange()));
          break;
        }
      }
      break;
    default:
      return;
    }
  }

  @Override
  public void addLayerLoadEndListener(final LayerLoadEndListener listener) {
    for (final MapLayer layer : layers) {
      layer.addLayerLoadEndListener(listener);
    }
  }

  @Override
  public void addLayerLoadStartListener(final LayerLoadStartListener listener) {
    for (final MapLayer layer : layers) {
      layer.addLayerLoadStartListener(listener);
    }
  }

  @Override
  public void attachLayer() {
    for (final MapLayer layer : layers) {
      layer.attachLayer();
    }
  }

  @Override
  public void destroy() {
    for (final MapLayer layer : layers) {
      layer.destroy();
    }
    eventHandles.removeHandler();
  }

  /**
   * The layer index for a Multi layer is the layer index of the last child layer.
   */
  @Override
  public int getLayerIndex() {
    return layers.get(0).getLayerIndex();
  }

  @Override
  public Legend getLegend() {
    return props.getLegend();
  }

  @Override
  public LayerProps getLayerProps() {
    return props;
  }

  @Override
  public Map getMap() {
    return map.getMap();
  }

  @Override
  public String getTitle() {
    return layers.isEmpty() ? "" : layers.get(0).getTitle();
  }

  @Override
  public void setTitle(final String title) {
    if (!layers.isEmpty()) {
      layers.get(0).setTitle(title);
    }
  }

  /**
   * Returns the reason for the {@link MultiLayer} not being visible. If there
   * are more layers the {@link MultiLayer} is defined as being visible if at
   * least one is visible. Therefore if one of the layers returns null then
   * the reason is null, if no layer is visible the reason is the reason of
   * the last layer.
   */
  @Override
  public String getNotVisibleReason() {
    String reason = null;
    for (final MapLayer layer : layers) {
      reason = layer.getNotVisibleReason();

      if (reason == null) {
        return null;
      }
    }
    return reason;
  }

  @Override
  public float getOpacity() {
    return layers.isEmpty() ? 1 : layers.get(0).getOpacity();
  }

  /**
   * Returns true if any of the layers is a base layer.
   */
  @Override
  public boolean isBaseLayer() {
    for (final MapLayer layer : layers) {
      if (layer.isBaseLayer()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public LayerInZoom isLayerInZoom() {
    for (final MapLayer layer : layers) {
      if (layer.isLayerInZoom() == LayerInZoom.IN_ZOOM) {
        return LayerInZoom.IN_ZOOM;
      }
    }
    return layers.isEmpty() ? LayerInZoom.IN_ZOOM : layers.get(0).isLayerInZoom();
  }

  @Override
  public boolean isVisible() {
    for (final MapLayer layer : layers) {
      if (layer.isVisible()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void raiseLayer(final int delta) {
    for (final MapLayer layer : layers) {
      layer.raiseLayer(delta);
    }
  }

  @Override
  public boolean redraw() {
    drawLayers();

    boolean rd = false;
    for (final MapLayer layer : layers) {
      //make sure all redraws are called, so redraw left of expression.
      rd = layer.redraw() || rd;
    }
    return rd;
  }

  @Override
  public void removeListener(final EventListener listener) {
    for (final MapLayer layer : layers) {
      layer.removeListener(listener);
    }
  }

  @Override
  public boolean setVisible(final boolean isVisible) {
    props.setEnabled(isVisible);
    boolean changed = false;

    for (final MapLayer layer : layers) {
      changed = layer.setVisible(isVisible) || changed;
    }
    return changed;
  }

  @Override
  public void setOpacity(final float opacity) {
    props.setOpacity(opacity);
    for (final MapLayer layer : layers) {
      layer.setOpacity(opacity);
    }
  }

  @Override
  public void zoomToMinScale() {
    layers.get(0).zoomToMinScale();
  }

  @Override
  public void zoomToMaxScale() {
    // No-op
  }

  public void addLayer(final WMSMapLayer addLayer) {
    layers.add(addLayer);

  }

  public void removeAllLayers() {
    for (final MapLayer layer : layers) {
      layer.setVisible(false);
    }

  }

  public List<MapLayer> getLayers() {
    return layers;
  }

  @Override
  public void refreshLayer() {
    // TODO Auto-generated method stub

  }
}
