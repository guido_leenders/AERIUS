/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import java.util.ArrayList;
import java.util.List;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.event.MapMoveEndListener;
import org.gwtopenmaps.openlayers.client.event.MapZoomListener;

/**
 * Handler to navigate one or more maps with one navigation controller. For each
 * registered map when a map zoom or map move is done, the event is passed to
 * the other registered maps, so they follow the navigation events.
 */
public class NavigationHandler {

    /**
     * The maps we are handling navigation commands for.
     */
    private final List<Map> maps = new ArrayList<Map>();

    /**
     * Whether a change is already fired and is being adjusted in other maps. So
     * we don't get in an infinite loop trying to apply the same changes to the
     * map which causing the change in the first place.
     */
    private boolean mapChangeInProgress;

    /**
     * Register a map to handle navigation commands for.
     *
     * @param map The map to register.
     */
    public void registerMap(final Map map) {
        assert !maps.contains(map) : "The map is already registered";

        maps.add(map);

        // Add the listeners which will let us know when something important changed on a map.
        // On move complete.
        map.addMapMoveEndListener(new MapMoveEndListener() {
            @Override
            public void onMapMoveEnd(final MapMoveEndEvent eventObject) {
                mapChanged(map);
            }
        });

        // On zoom.
        map.addMapZoomListener(new MapZoomListener() {
            @Override
            public void onMapZoom(final MapZoomEvent eventObject) {
                mapChanged(map);
            }
        });
    }

    private void mapChanged(final Map changedMap) {
        // If there is already a change in progress, ignore.
        if (mapChangeInProgress) {
            return;
        }
        // Set the progress flag to true.
        mapChangeInProgress = true;

        for (final Map map : maps) {
            // Skip the map that is initially changed, he doesn't have to update itself.
            if (map == changedMap) {
                continue;
            }

            // Make the changes.
            map.zoomTo(changedMap.getZoom());
            map.zoomToExtent(changedMap.getExtent());
        }

        // Reset the progress flag to allow changes again.
        mapChangeInProgress = false;
    }
}
