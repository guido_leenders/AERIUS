/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Bounds;
import org.gwtopenmaps.openlayers.client.geometry.LineString;
import org.gwtopenmaps.openlayers.client.geometry.Point;
import org.gwtopenmaps.openlayers.client.geometry.Polygon;

import nl.overheid.aerius.geo.shared.BBox;

/**
 * Class with static utility methods with OpenLayers conversion methods from and
 * to domain specific geometry classes.
 */
public final class WktUtil {

  private WktUtil() {
  }

  public static Bounds toBounds(final BBox bbox) {
    return bbox == null ? null : new Bounds(bbox.getMinX(), bbox.getMinY(), bbox.getMaxX(), bbox.getMaxY());
  }

  public static BBox toBBox(final Bounds extent) {
    return extent == null ? null : new BBox(extent.getLowerLeftX(), extent.getUpperRightY(), extent.getUpperRightX(),
        extent.getLowerLeftY());
  }

  /**
   * Converts an AERIUS point to an Open Layers point.
   *
   * @param p AERIUS point
   * @return Open Layers point
   */
  public static Point toPoint(final nl.overheid.aerius.geo.shared.Point p) {
    return new Point(p.getX(), p.getY());
  }

  /**
   * Converts an Open Layers point to an AERIUS  point.
   *
   * @param p Open Layers point
   * @return AERIUS point
   */
  public static nl.overheid.aerius.geo.shared.Point fromPoint(final Point p) {
    return new nl.overheid.aerius.geo.shared.Point(p.getX(), p.getY());
  }

  public static org.gwtopenmaps.openlayers.client.geometry.Geometry string2LineString(final String value) {
    return LineString.fromWKT("LINESTRING(" + space2CommaString(value) + ")");
  }

  public static org.gwtopenmaps.openlayers.client.geometry.Geometry string2Polygon(final String value) {
    return Polygon.fromWKT("POLYGON((" + space2CommaString(value) + "))");
  }

  /**
   * Replaces the spaces between the tuple points with a comma. This is to convert a GML poslist notation to a wkt notation.
   * @param value tuple of points
   * @return replaced points.
   */
  public static String space2CommaString(final String value) {
    if (value != null && value.indexOf(',') == -1) {
      final String stripped = value.trim().replaceAll("  +", " ");
      final char[] valueChars = stripped.toCharArray();
      boolean odd = true;
      for (int fromIndex = 0, i = stripped.indexOf(' '); i != -1; fromIndex = i + 1, i = stripped.indexOf(' ', fromIndex)) {
        if (!odd) {
          valueChars[i] = ',';
        }
        odd = !odd;
      }
      return String.valueOf(valueChars);
    } else {
      return value == null ? "" : value.trim();
    }
  }
}
