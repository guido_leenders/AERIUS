/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Icon;
import org.gwtopenmaps.openlayers.client.LonLat;
import org.gwtopenmaps.openlayers.client.Marker;
import org.gwtopenmaps.openlayers.client.Size;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.wui.main.resources.R;

public class Sonar extends Marker {
  public Sonar(final Point p) {
    super(new LonLat(p.getX(), p.getY()), new Icon("", new Size(0, 0)));

    // Clear initial content
    final Element image = getIcon().getJSObject().getPropertyAsDomElement("imageDiv");
    image.setInnerText(null);

    final DivElement innerSonar = Document.get().createDivElement();
    innerSonar.setClassName(R.css().innerSonar());

    final DivElement sonar = Document.get().createDivElement();
    sonar.setClassName(R.css().sonar());
    sonar.appendChild(innerSonar);

    final DivElement animationSonar = Document.get().createDivElement();
    animationSonar.setClassName(R.css().animationSonar());

    final DivElement outerSonar = Document.get().createDivElement();
    outerSonar.setClassName(R.css().outerSonar());
    outerSonar.appendChild(animationSonar);
    outerSonar.appendChild(sonar);

    image.appendChild(outerSonar);
  }
}
