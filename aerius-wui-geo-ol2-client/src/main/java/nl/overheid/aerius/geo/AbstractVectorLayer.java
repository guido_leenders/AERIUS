/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import org.gwtopenmaps.openlayers.client.Map;
import org.gwtopenmaps.openlayers.client.layer.Vector;

import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerProps.Legend;

/**
 * Abstract layer for showing vectors on a map.
 */
public abstract class AbstractVectorLayer extends Vector implements MapLayer {

  protected final MapLayoutPanel map;

  private final LayerProps layerProps = new LayerProps();

  private LayerVisibility layerVisibility = new LayerVisibility();

  protected AbstractVectorLayer(final String name, final MapLayoutPanel map) {
    super(name);
    this.map = map;
    layerVisibility.setLayer(this);
  }

  @Override
  public void attachLayer() {
    map.addLayerToMap(this);
  }

  @Override
  public void destroy() {
    destroyFeatures();
  }

  @Override
  public int getLayerIndex() {
    return getMap().getLayerIndex(this);
  }

  @Override
  public LayerProps getLayerProps() {
    return layerProps;
  }

  @Override
  public Legend getLegend() {
    return null;
  }

  @Override
  public Map getMap() {
    return map.getMap();
  }

  @Override
  public String getNotVisibleReason() {
    return layerVisibility.getNotVisibleReason();
  }

  @Override
  public LayerInZoom isLayerInZoom() {
    return LayerInZoom.IN_ZOOM;
  }

  @Override
  public void raiseLayer(final int delta) {
    getMap().raiseLayer(this, delta);
  }

  @Override
  public void refreshLayer() {
    //no-op
  }

  @Override
  public boolean setVisible(final boolean isVisible) {
    return layerVisibility.setIsVisible(isVisible);
  }

  @Override
  public void zoomToMinScale() {
    getMap().zoomToScale(1, true);
  }

  @Override
  public void zoomToMaxScale() {
    // No-op
  }

  @Override
  public String getTitle() {
    return getName();
  }

  @Override
  public void setTitle(final String name) {
    setName(name);
  }

  protected String getCurrentVisibilityReason() {
    return null;
  }

  protected void setLayerVisibility(final LayerVisibility layerVisibility) {
    this.layerVisibility = layerVisibility;
  }

}
