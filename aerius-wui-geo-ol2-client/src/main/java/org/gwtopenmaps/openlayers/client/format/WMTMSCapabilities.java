package org.gwtopenmaps.openlayers.client.format;

import org.gwtopenmaps.openlayers.client.util.JSObject;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;

/**
 * Data object for WMS GetCapabilities data. The underlying data model is
 * specific for OpenLayers, which is a re-map of the returned XML which uses
 * at some points different names. It's also version independent.
 */
public class WMTMSCapabilities extends JSObject {

  public static class PersonPrimary extends JSObject {
    protected PersonPrimary() {
    }

    public final native String getPerson() /*-{
          return this.person;
      }-*/;

    public final native String getOrganization() /*-{
          return this.organization;
      }-*/;
  }

  public static class ContactAddress extends JSObject {
    protected ContactAddress() {
    }

    public final native String getType() /*-{
          return this.type;
      }-*/;

    public final native String getAddress() /*-{
          return this.address;
      }-*/;

    public final native String getCity() /*-{
          return this.city;
      }-*/;

    public final native String getStateOrProvince() /*-{
          return this.stateOrProvince;
      }-*/;

    public final native String getPostcode() /*-{
          return this.postcode;
      }-*/;

    public final native String getCountry() /*-{
          return this.country;
      }-*/;
  }

  public static class ContactInformation extends JSObject {
    protected ContactInformation() {
    }

    public final native PersonPrimary getPersonPrimary() /*-{
          return this.personPrimary;
      }-*/;

    public final native String getPosition() /*-{
          return this.position;
      }-*/;

    public final native ContactAddress getContactAddress() /*-{
          return this.contactAddress;
      }-*/;

    public final native String getPhone() /*-{
          return this.phone;
      }-*/;

    public final native  String getFax() /*-{
          return this.fax;
      }-*/;

    public final native String getEmail() /*-{
          return this.email;
      }-*/;
  }

  public static class Service extends JSObject {
    protected Service() {
    }

    public final native String getAbstract() /*-{
          return this['abstract'];
      }-*/;

    public final native String getAccessConstraints() /*-{
          return this.accessConstraints;
      }-*/;

    public final native ContactInformation getContactInformation() /*-{
          return this.contactInformation;
      }-*/;

    public final native String getFees() /*-{
          return this.fees;
      }-*/;

    public final native String getHref() /*-{
          return this.href;
      }-*/;

    public final native JsArrayString getKeywords() /*-{
          return this.keywords;
      }-*/;

    public final native String getName() /*-{
          return this.name;
      }-*/;

    public final native String getTitle() /*-{
          return this.title;
      }-*/;
  }

  public static class Logo extends JSObject {
    protected Logo() {
    }

    public final native String getFormat() /*-{
          return this.format;
      }-*/;

    public final native int getHeight() /*-{
          return this.height;
      }-*/;

    public final native String getHref() /*-{
          return this.href;
      }-*/;

    public final native int getWidth() /*-{
          return this.width;
      }-*/;
  }

  public static class Attribution extends JSObject {
    protected Attribution() {
    }

    public final native String getHref() /*-{
          return this.href;
      }-*/;

    public final native Logo getLogo() /*-{
          return this.logo;
      }-*/;

    public final native String getTitle() /*-{
          return this.title;
      }-*/;
  }

  public static class Layer extends JSObject {
    protected Layer() {
    }

    public final native String getAbstract() /*-{
          return this['abstract'];
      }-*/;

    public final native Attribution getAttribution() /*-{
          return this.attribution;
      }-*/;

    public final native JsArrayString getKeywords() /*-{
          return this.keywords;
      }-*/;

    public final native String getName() /*-{
          return this.name;
      }-*/;

    public final native String getPrefix() /*-{
          return this.prefix;
      }-*/;

    public final native String getTitle() /*-{
          return this.title;
      }-*/;
  }

  public static class Request extends JSObject {
    protected Request() {
    }

    public final native JsArrayString getFormats() /*-{
          return this.formats;
      }-*/;

    public final native String getHref() /*-{
          return this.href;
      }-*/;
  }

  protected WMTMSCapabilities() {
  }

  public final native JsArray<Layer> getLayers() /*-{
        return this.capability ? this.capability.layers : {};
    }-*/;

  public final native Request describeLayer() /*-{
        return this.capability.request.describelayer;
    }-*/;

  public final native Request getCapabilities() /*-{
        return this.capability.request.getcapabilities;
    }-*/;

  public final native Request getFeatureInfo() /*-{
        return this.capability.request.getfeatureinfo;
    }-*/;

  public final native Request getLegendGraphic() /*-{
        return this.capability.request.getlegendgraphic;
    }-*/;

  public final native Request getMap() /*-{
        return this.capability ? this.capability.request.getmap : {};
    }-*/;

  public final native Service getService() /*-{
        return this.service;
    }-*/;

  public final native Request getStyles() /*-{
        return this.capability.request.getstyles;
    }-*/;

  public final native String getVersion() /*-{
        return this.version;
    }-*/;
}
