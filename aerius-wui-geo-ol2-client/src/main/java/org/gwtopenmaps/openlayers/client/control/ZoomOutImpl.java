package org.gwtopenmaps.openlayers.client.control;

import org.gwtopenmaps.openlayers.client.util.JSObject;

/**
 * @author Edwin Commandeur - Atlis EJS
 *
 */
class ZoomOutImpl
{

    public static native JSObject create() /*-{
        return new $wnd.OpenLayers.Control.ZoomOut();
    }-*/;

    public static native void trigger(JSObject zoomOut) /*-{
        zoomOut.trigger();
    }-*/;
}
