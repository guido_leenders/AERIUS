/**
 *
 */
package org.gwtopenmaps.openlayers.client.control;

import org.gwtopenmaps.openlayers.client.feature.VectorFeature;
import org.gwtopenmaps.openlayers.client.layer.Vector;
import org.gwtopenmaps.openlayers.client.util.JSObject;


/**
 * @author Edwin Commandeur - Atlis EJS
 *
 */
public class ModifyFeature extends Control
{
  public static final int RESHAPE = 1;
  public static final int RESIZE = 2;
  public static final int ROTATE = 4;
  public static final int DRAG = 8;

  public static final String CLASS_NAME = "OpenLayers.Control.ModifyFeature";

  protected ModifyFeature(final JSObject modifyFeature)
  {
    super(modifyFeature);
  }

  public ModifyFeature(final Vector vectorLayer)
  {
    this(ModifyFeatureImpl.create(vectorLayer.getJSObject()));
  }

  public ModifyFeature(final Vector vectorLayer,
      final ModifyFeatureOptions modifyFeatureOptions)
  {
    this(ModifyFeatureImpl.create(vectorLayer.getJSObject(),
        modifyFeatureOptions.getJSObject()));
  }

  /**
   * Set the mode of the ModifyFeature. Can accept more than one mode at same
   * time.
   *
   * @param modes
   *            One or more modes to set to this ModifyFeature
   */
  public void setMode(final int... modes)
  {

    int modeValue = 0;

    if (modes != null)
    {
      for (final int mode : modes)
      {
        modeValue |= mode;
      }
    }

    this.getJSObject().setProperty("mode", modeValue);
  }

  /**
   * Property: modified {Boolean} The currently selected feature has been
   * modified.
   */
  public boolean isSelectedFeatureModified()
  {
    return ModifyFeatureImpl.isSelectedFeatureModified(getJSObject());
  }

  public void selectFeature(final VectorFeature feature)
  {
    ModifyFeatureImpl.selectFeature(getJSObject(), feature.getJSObject());
  }

  /**
   *
   */
  public interface OnModificationStartListener
  {
    void onModificationStart(VectorFeature vectorFeature);
  }

  public interface OnModificationListener
  {
    void onModification(VectorFeature vectorFeature);
  }

  public interface OnModificationEndListener
  {
    void onModificationEnd(VectorFeature vectorFeature);
  }

}
