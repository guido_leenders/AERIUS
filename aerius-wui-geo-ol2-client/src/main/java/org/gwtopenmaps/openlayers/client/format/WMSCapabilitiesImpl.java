package org.gwtopenmaps.openlayers.client.format;

import org.gwtopenmaps.openlayers.client.util.JSObject;

class WMSCapabilitiesImpl {

    public static native JSObject create() /*-{
        return new $wnd.OpenLayers.Format.WMSCapabilities();
    }-*/;

    public static native WMTMSCapabilities read(JSObject jSObject, String data) /*-{
        return jSObject.read(data);
    }-*/;
}
