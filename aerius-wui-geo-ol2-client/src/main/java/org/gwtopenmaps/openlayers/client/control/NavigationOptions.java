package org.gwtopenmaps.openlayers.client.control;

/**
 * @author Edwin Commandeur - Atlis EJS
 * 
 */
public class NavigationOptions extends ControlOptions
{

    /**
     * Allow panning of the map by dragging outside map viewport. Default is
     * false.
     * 
     * @param documentDrag
     */
    public void setDocumentDrag(boolean documentDrag)
    {
        getJSObject().setProperty("documentDrag", documentDrag);
    }

    /**
     * Whether or not to handle right clicks. Default is false.
     * 
     * @param handleRightClicks
     */
    public void setHandleRightClicks(boolean handleRightClicks)
    {
        getJSObject().setProperty("handleRightClicks", handleRightClicks);
    }

    /**
     * Whether the user can draw a box to zoom.
     *
     * @param zoomBoxEnabled
     */
    public void setZoomBoxEnabled(boolean zoomBoxEnabled)
    {
        getJSObject().setProperty("zoomBoxEnabled", zoomBoxEnabled);
    }

    /**
     * OpenLayers.Handler key code of the key, which has to be pressed, while
     * drawing the zoom box with the mouse on the screen. You should probably
     * set handleRightClicks to true if you use this with MOD_CTRL, to disable
     * the context menu for machines which use CTRL-Click as a right click.
     * Default: <OpenLayers.Handler.MOD_SHIFT>
     * 
     * @param zoomBoxKeyMask
     *            - by default ... (shift)
     */
    public void setZoomBoxKeyMask(int zoomBoxKeyMask)
    {
        getJSObject().setProperty("zoomBoxKeyMask", zoomBoxKeyMask);
    }

    /**
     * Whether the mouse wheel should zoom the map.
     *
     * @param zoomWheelEnabled
     */
    public void setZoomWheelEnabled(boolean zoomWheelEnabled)
    {
        getJSObject().setProperty("zoomWheelEnabled", zoomWheelEnabled);
    }
}
