/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test class for {@link WktUtil}.
 */
public class WktUtilTest {


  @Test
  public void testSpace2CommaString() {
    assertEquals("Test commas inserted 1 point", "1 2", WktUtil.space2CommaString("1 2"));
    assertEquals("Test commas inserted 1 point, multiple spaces", "1 2", WktUtil.space2CommaString(" 1 2 "));
    assertEquals("Test commas inserted 2 points", "1 2,3 4", WktUtil.space2CommaString("1 2 3 4"));
    assertEquals("Test commas inserted 2 points, multiple spaces", "1 2,3 4", WktUtil.space2CommaString(" 1  2  3 4 "));
  }
}
