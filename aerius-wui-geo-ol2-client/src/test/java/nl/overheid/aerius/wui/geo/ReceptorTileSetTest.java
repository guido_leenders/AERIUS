/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.geo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.EPSGProxy;
import nl.overheid.aerius.shared.domain.geo.HexagonZoomLevel;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.wui.geo.ReceptorTileSet.TiledIterator;

/**
 * TODO This test does not yet fully cover the {@link ReceptorTileSet}.
 */
public class ReceptorTileSetTest {
  @Test
  public void testTiledIterator() {
    final ArrayList<Iterator<String>> arr = new ArrayList<Iterator<String>>();
    final ArrayList<String> arr1 = new ArrayList<String>();
    final ArrayList<String> arr2 = new ArrayList<String>();

    arr1.add("a");
    arr1.add("b");
    arr2.add("c");

    arr.add(arr1.iterator());
    arr.add(arr2.iterator());

    final ArrayList<String> testArr = new ArrayList<String>();
    testArr.add("a");
    testArr.add("b");
    testArr.add("c");

    final BBox receptorBounding = new BBox(1, 1, 10000, 10000);
    final HexagonZoomLevel zoomLevel1 = new HexagonZoomLevel(1, 10000);
    final ArrayList<HexagonZoomLevel> hexagonZoomLevels = new ArrayList<>();
    hexagonZoomLevels.add(zoomLevel1);
    final ReceptorGridSettings rgs = new ReceptorGridSettings(receptorBounding, EPSGProxy.defaultEpsg(), 1529, hexagonZoomLevels);
    final ReceptorUtil ru = new ReceptorUtil(rgs);
    final TiledIterator<String> tiledIterator = new ReceptorTileSet(ru, 1, receptorBounding, zoomLevel1).new TiledIterator<String>(arr);

    int i = 0;
    for (final String s : tiledIterator) {
      assertEquals(s, testArr.get(i++));
    }
  }
}
