/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.taskmanager.db.ConfigurationRepository;
import nl.overheid.aerius.taskmanager.db.PMF;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskManagerConfiguration;

/**
 * Class to help with loading and saving configuration.
 */
final class ConfigurationManager {

  private static final Logger LOG = LoggerFactory.getLogger(TaskManagerConfiguration.class);

  private ConfigurationManager() {
  }

  /**
   * Load the configuration from default properties and a database configured in a property file
   * (either from a "taskmanager.properties" or from classpath).
   * @param propertiesFile name of the properties file
   * @return The configuration file to be used by the taskmanager.
   * @throws IOException When an exception occurred trying to
   * @throws SQLException When an exception occurred retrieving configuration from the database.
   */
  public static TaskManagerConfiguration loadConfigurationFromDatabase(final String propertiesFileName) throws IOException, SQLException {
    return loadConfigurationFromDatabase(getPropertiesFromFile(propertiesFileName));
  }

  public static TaskManagerConfiguration loadConfigurationFromDatabase(final Properties properties) throws IOException, SQLException {
    final TaskManagerConfiguration config;
    try (final Connection connection = getConnection(properties)) {
      config = ConfigurationRepository.getTaskManagerConfig(connection);
      return config;
    } catch (final SQLException e) {
      LOG.error("Error retrieving configuration from database.", e);
      throw e;
    }
  }

  public static Properties getPropertiesFromFile(final String propertiesFileName) throws IOException {
    final File localFile = new File(propertiesFileName);
    final Properties properties = new Properties();

    try (final InputStream inputStream = new FileInputStream(localFile)) {
      properties.load(inputStream);
    }
    return properties;
  }

  private static Connection getConnection(final Properties properties) throws IOException, SQLException {
    return PMF.getConnection((String) properties.get("database.url"),
        (String) properties.get("database.username"),
        (String) properties.get("database.password"));
  }
}
