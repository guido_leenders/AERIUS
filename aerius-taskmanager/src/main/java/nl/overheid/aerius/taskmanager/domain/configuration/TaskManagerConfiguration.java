/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.domain.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.taskmanager.client.configuration.ConnectionConfiguration;

/**
 * configuration for the TaskManager.
 *
 */
public class TaskManagerConfiguration implements Serializable {

  private static final long serialVersionUID = -1304013543385486140L;

  private ConnectionConfiguration brokerConfiguration;
  private List<TaskSchedulerConfiguration> taskSchedulerConfigurations = new ArrayList<>();

  public ConnectionConfiguration getBrokerConfiguration() {
    return brokerConfiguration;
  }

  public void setBrokerConfiguration(final ConnectionConfiguration brokerConfiguration) {
    this.brokerConfiguration = brokerConfiguration;
  }

  public List<TaskSchedulerConfiguration> getTaskSchedulerConfigurations() {
    return taskSchedulerConfigurations;
  }

  public void setTaskSchedulerConfigurations(final List<TaskSchedulerConfiguration> taskSchedulerConfigurations) {
    this.taskSchedulerConfigurations = taskSchedulerConfigurations;
  }

  @Override
  public String toString() {
    return "TaskManagerConfiguration [brokerConfiguration=" + brokerConfiguration + ", taskSchedulerConfigurations="
        + taskSchedulerConfigurations + "]";
  }
}
