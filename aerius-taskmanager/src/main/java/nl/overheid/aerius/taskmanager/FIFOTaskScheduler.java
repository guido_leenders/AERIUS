/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.annotation.ThreadSafe;

import nl.overheid.aerius.taskmanager.domain.MessageMetaData;
import nl.overheid.aerius.taskmanager.domain.configuration.TaskSchedulerConfiguration;


/**
 * FIFO implementation of the task scheduler.
 */
@ThreadSafe
class FIFOTaskScheduler implements TaskScheduler {

  private final BlockingQueue<Task> tasks = new LinkedBlockingQueue<Task>();

  @Override
  public void addTask(final Task task) {
    tasks.add(task);
  }

  @Override
  public Task getNextTask() throws InterruptedException {
    return tasks.take();
  }

  @Override
  public void onTaskFinished(final MessageMetaData task) {
    // Not used
  }

  @Override
  public void onWorkerPoolSizeChange(final int numberOfWorkers) {
    // Not used
  }

  public static class FIFOSchedulerFactory implements SchedulerFactory {
    @Override
    public TaskScheduler createScheduler(final TaskSchedulerConfiguration configuration) {
      return new FIFOTaskScheduler();
    }
  }
}
