/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.mq;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import nl.overheid.aerius.taskmanager.adaptor.TaskMessageHandler;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerResultSender;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * RabbitMQ implementation of a {@link TaskMessageHandler}. RabbitMQ will starts listening to the queue and when messages are received they are send
 * to the {@link MessageReceivedHandler}.
 */
class RabbitMQMessageHandler extends DefaultConsumer implements TaskMessageHandler<RabbitMQMessageMetaData, RabbitMQMessage> {

  private static final Logger LOG = LoggerFactory.getLogger(RabbitMQMessageHandler.class);

  private static final int MAX_RETRIES = 3;
  private final WorkerType workerType;
  private final String queueName;

  private MessageReceivedHandler messageReceivedHandler;
  private boolean isShutdown;

  /**
   *
   * @param channel The rabbitMQ channel to use, see {@link DefaultConsumer}.
   * @param messageHandler The message handler which should take care of incoming messages.
   * @throws IOException
   */
  public RabbitMQMessageHandler(final BrokerConnectionFactory factory, final WorkerType workerType, final String queueName) throws IOException {
    super(factory.getConnection().createChannel());
    this.workerType = workerType;
    this.queueName = queueName;
  }

  @Override
  public void addMessageReceivedHandler(final MessageReceivedHandler messageReceivedHandler) {
    this.messageReceivedHandler = messageReceivedHandler;
  }

  @Override
  public void start() throws IOException {
    final Channel taskChannel = getChannel();
    final String qn = workerType.getTaskQueueName(queueName);
    // ensure a durable channel exists
    taskChannel.queueDeclare(qn, true, false, false, null);
    //ensure only one message gets delivered at a time.
    taskChannel.basicQos(1);

    final String consumerTag = workerType.getConsumerTag(queueName);
    taskChannel.basicConsume(qn, false, consumerTag, this);
  }

  @Override
  public void shutDown() throws IOException {
    isShutdown = true;
    getChannel().basicCancel(workerType.getConsumerTag(queueName));
  }

  @Override
  public void handleDelivery(final String consumerTag, final Envelope envelope,
      final BasicProperties properties, final byte[] body) throws IOException {
    // take care of cases where original client did not supply messageId by making one ourselves.
    BasicProperties actualProperties = properties;
    if (properties.getMessageId() == null) {
      actualProperties = properties.builder().messageId(UUID.randomUUID().toString()).build();
    }
    final RabbitMQMessage message = new RabbitMQMessage(getChannel(), envelope.getDeliveryTag(), actualProperties, body);
    // the queueName as we use it is actually the routingKey used by AMQP.
    if (messageReceivedHandler != null) {
      try {
        messageReceivedHandler.onMessageReceived(message);
      } catch (final RuntimeException e) {
        messageDeliveryToWorkerFailed(message.getMetaData());
      }
    }
  }

  @Override
  public void messageDeliveredToWorker(final RabbitMQMessageMetaData message) throws IOException {
    try {
      getChannel().basicAck(message.getDeliveryTag(), false);
    } catch (final ShutdownSignalException e) {
      if (restartChannel(e)) {
        messageDeliveredToWorker(message);
      }
    }
  }

  @Override
  public void messageDeliveryToWorkerFailed(final RabbitMQMessageMetaData message) throws IOException {
    try {
      getChannel().basicNack(message.getDeliveryTag(), false, true);
    } catch (final ShutdownSignalException e) {
      if (restartChannel(e)) {
        messageDeliveryToWorkerFailed(message);
      }
    }
  }

  @Override
  public void messageDeliveryAborted(final RabbitMQMessage message, final RuntimeException exception) throws IOException {
    final WorkerResultSender sender = new WorkerResultSender(getChannel(), message.getProperties());
    sender.sendIntermediateResult(exception);
    messageDeliveredToWorker(message.getMetaData());
  }

  private boolean restartChannel(final ShutdownSignalException e) {
    for (int i = 0; i < MAX_RETRIES; i++) {
      if (!isShutdown && !e.isHardError()) {
        try {
          start();
          return true;
        } catch (final ShutdownSignalException | IOException e1) {
          LOG.error("Restart channel failed, retry", e1);
          try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(10));
          } catch (final InterruptedException e2) {
            LOG.error("Restart channel sleep interrupted", e2);
            Thread.currentThread().interrupt();
            return false;
          }
        }
      }
    }
    return false;
  }
}
