/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.domain.configuration;

import java.io.Serializable;

/**
 * The configuration of a task, which is used by Task Consumer to retrieve the queue name
 * and used by the scheduler to determine if a message should be handled or not.
 *
 */
public class TaskQueueConfiguration implements Serializable {

  private static final long serialVersionUID = 7719329377305394882L;

  // configured with DB
  private String queueName;
  private String description;
  private int priority;
  private double maxCapacityUse;

  private TaskSchedulerConfiguration schedulerConfiguration;

  /**
   * Empty constructor.
   */
  public TaskQueueConfiguration() {
    // no assignments
  }

  /**
   * @param queueName The name of the queue the task corresponds to.
   * @param description The description of this task queue.
   * @param priority The priority this task should have.
   * @param maxCapacityUse The maximum capacity this task can use of the total workers assigned. Should be a fraction.
   * @param schedulerConfig The scheduler configuration that this task corresponds to.
   */
  public TaskQueueConfiguration(final String queueName, final String description, final int priority, final double maxCapacityUse,
      final TaskSchedulerConfiguration schedulerConfig) {
    this.queueName = queueName;
    this.description = description;
    this.priority = priority;
    this.maxCapacityUse = maxCapacityUse;
    this.schedulerConfiguration = schedulerConfig;
  }

  public String getQueueName() {
    return queueName;
  }

  public void setQueueName(final String queueName) {
    this.queueName = queueName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(final int priority) {
    this.priority = priority;
  }

  public double getMaxCapacityUse() {
    return maxCapacityUse;
  }

  public void setMaxCapacityUse(final double maxCapacityUse) {
    this.maxCapacityUse = maxCapacityUse;
  }

  public TaskSchedulerConfiguration getSchedulerConfiguration() {
    return schedulerConfiguration;
  }

  public void setSchedulerConfiguration(final TaskSchedulerConfiguration schedulerConfiguration) {
    this.schedulerConfiguration = schedulerConfiguration;
  }

  @Override
  public String toString() {
    return "TaskQueueConfiguration [queueName=" + queueName + ", description=" + description + ", priority=" + priority
        + ", maxCapacityUse=" + maxCapacityUse + "]";
  }
}
