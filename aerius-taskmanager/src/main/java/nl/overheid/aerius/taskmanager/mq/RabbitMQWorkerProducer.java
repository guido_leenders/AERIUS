/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.mq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import nl.overheid.aerius.taskmanager.adaptor.WorkerProducer;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.QueueConstants;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.taskmanager.client.mq.QueueUpdateHandler;
import nl.overheid.aerius.taskmanager.client.mq.RabbitMQQueueMonitor;
import nl.overheid.aerius.taskmanager.domain.Message;

/**
 * RabbitMQ implementation of a {@link WorkerProducer}.
 */
class RabbitMQWorkerProducer implements WorkerProducer {

  protected static final String WORKER_REPLY_AFFIX = ".reply";

  private static final Logger LOG = LoggerFactory.getLogger(RabbitMQWorkerProducer.class);

  private RabbitMQQueueMonitor wpManager;
  private final BrokerConnectionFactory factory;
  private final WorkerType workerType;
  private final String workerQueue;

  private WorkerFinishedHandler workerFinishedHandler;

  public RabbitMQWorkerProducer(final BrokerConnectionFactory factory, final WorkerType workerType) {
    this.factory = factory;
    this.workerType = workerType;
    this.workerQueue = workerType.getWorkerQueueName();
  }

  @Override
  public void setWorkerFinishedHandler(final WorkerFinishedHandler workerFinishedHandler) {
    this.workerFinishedHandler = workerFinishedHandler;
  }

  @Override
  public void start(final ExecutorService executorService, final QueueUpdateHandler workerPoolSize) throws IOException {
    wpManager = new RabbitMQQueueMonitor(factory.getConnectionConfiguration());
    wpManager.addQueueUpdateHandler(workerType.getWorkerQueueName(), workerPoolSize);
    executorService.execute(wpManager);
    final Channel replyChannel = factory.getConnection().createChannel();
    // Create an exclusive reply queue with predefined name (so we can set
    // a replyCC header).
    // Queue will be deleted once taskmanager is down.
    // reply queue is not durable because the system will 'reboot' after connection problems anyway.
    // Making it durable would only make sense if we'd keep track of tasks-in-progress during shutdown/startup.
    replyChannel.queueDeclare(getWorkerReplyQueue(), false, true, true, null);
    // ensure the worker queue is around as well (so we can retrieve number of customers later on).
    // Worker queue is durable and non-exclusive with autodelete off.
    replyChannel.queueDeclare(workerQueue, true, false, false, null);
    replyChannel.basicConsume(getWorkerReplyQueue(), true, workerType.getConsumerTag("capacityreply"), new DefaultConsumer(replyChannel) {
      @Override
      public void handleDelivery(final String consumerTag, final Envelope envelope, final BasicProperties properties, final byte[] body) {
        workerFinishedHandler.onWorkerFinished(properties.getMessageId());
      }
    });
    LOG.info("Started worker queue for {}.", workerType);
  }

  @Override
  public void forwardMessage(final Message<?> message) throws IOException {
    final RabbitMQMessage rabbitMQMessage = (RabbitMQMessage) message;
    // Do we set the replyTo to something fake?
    // or do we expect worker to send instead of CC the message?
    final Channel channel = factory.getConnection().createChannel();
    try {
      channel.queueDeclare(workerQueue, true, false, false, null);
      final BasicProperties.Builder forwardBuilder = rabbitMQMessage.getProperties().builder();
      // new header map (even in case of existing headers, original can be a UnmodifiableMap)
      final Map<String, Object> headers = rabbitMQMessage.getProperties().getHeaders() == null ? new HashMap<>()
          : new HashMap<>(rabbitMQMessage.getProperties().getHeaders());

      // we want to be notified when a worker has finished it's job.
      // To do this, we set our own property, replyCC.
      // It's the worker implementation (through taskmanager client) to use this property to return a message.
      // (either through RabbitMQ CC-mechanism or by sending an empty message to the replyQueue)
      headers.put(QueueConstants.TASKMANAGER_REPLY_QUEUE, getWorkerReplyQueue());
      forwardBuilder.headers(headers);
      final BasicProperties forwardProperties = forwardBuilder.deliveryMode(2).build();
      channel.basicPublish("", workerQueue, forwardProperties, rabbitMQMessage.getBody());
    } finally {
      try {
        channel.close();
      } catch (final IOException e) {
        // eat error.
      }
    }
  }

  @Override
  public void shutdown() {
    wpManager.shutdown();
  }

  private String getWorkerReplyQueue() {
    return workerQueue + WORKER_REPLY_AFFIX;
  }
}
