/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager.domain.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * The configuration of a TaskScheduler.
 */
public class TaskSchedulerConfiguration implements Serializable {

  private static final long serialVersionUID = 5748451508914683867L;

  // configured with DB
  private Integer id;
  private String description;

  private TaskManagerConfiguration taskManagerConfiguration;

  private List<TaskQueueConfiguration> taskConfigurations = new ArrayList<>();

  private WorkerType workerType;

  /**
   * @param id The (database) ID.
   * @param description The description of this scheduler.
   * @param managerConfig The taskmanager configuration this schedulerconfiguration belongs to.
   * @param workerType worker type
   */
  public TaskSchedulerConfiguration(final Integer id, final String description,
      final TaskManagerConfiguration managerConfig, final WorkerType workerType) {
    this.id = id;
    this.description = description;
    this.taskManagerConfiguration = managerConfig;
    this.workerType = workerType;
  }

  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public WorkerType getWorkerType() {
    return workerType;
  }

  public void setWorkerType(final WorkerType workerType) {
    this.workerType = workerType;
  }

  public TaskManagerConfiguration getTaskManagerConfiguration() {
    return taskManagerConfiguration;
  }

  public void setTaskManagerConfiguration(final TaskManagerConfiguration taskManagerConfiguration) {
    this.taskManagerConfiguration = taskManagerConfiguration;
  }

  public List<TaskQueueConfiguration> getTaskConfigurations() {
    return taskConfigurations;
  }

  public void setTaskConfigurations(final List<TaskQueueConfiguration> taskConfigurations) {
    this.taskConfigurations = taskConfigurations;
  }

  @Override
  public String toString() {
    return "TaskSchedulerConfiguration [id=" + id + ", description=" + description + ", taskConfigurations=" + taskConfigurations
        + ", workerType=" + workerType + "]";
  }
}
