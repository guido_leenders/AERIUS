/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import org.junit.Test;

import nl.overheid.aerius.taskmanager.domain.configuration.TaskManagerConfiguration;

/**
 * Test for {@link ConfigurationManager}.
 */
public class ConfigurationManagerTest {

  @Test
  public void testLoadConfigurationFromDatabase() throws IOException, SQLException {
    final String propFile = getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "taskmanager_test.properties";
    final Properties properties = ConfigurationManager.getPropertiesFromFile(propFile);

    properties.setProperty("database.username", System.getProperty("database.username"));
    properties.setProperty("database.password", System.getProperty("database.password"));
    final TaskManagerConfiguration tmc = ConfigurationManager.loadConfigurationFromDatabase(properties);
    assertNotNull("No username could be retreived from database", tmc.getBrokerConfiguration().getBrokerUsername());
    assertFalse("No schedulers found in the database", tmc.getTaskSchedulerConfigurations().isEmpty());
  }
}
