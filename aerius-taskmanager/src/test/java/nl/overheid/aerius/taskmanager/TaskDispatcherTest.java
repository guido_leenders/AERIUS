/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.taskmanager;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.taskmanager.TaskDispatcher.State;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Test for {@link TaskDispatcher} class.
 */
public class TaskDispatcherTest {

  private static ExecutorService executor;

  private TaskDispatcher dispatcher;
  private WorkerPool workerPool;
  private TaskConsumer taskConsumer;
  private MockAdaptorFactory factory;

  @Before
  public void setUp() throws IOException, InterruptedException {
    executor = Executors.newCachedThreadPool();
    final TaskScheduler scheduler = new FIFOTaskScheduler();
    workerPool = new WorkerPool(WorkerType.TEST, new MockWorkerProducer(), scheduler);
    dispatcher = new TaskDispatcher(WorkerType.TEST, scheduler, workerPool);
    factory = new MockAdaptorFactory();
    taskConsumer = new TaskConsumer(WorkerType.TEST, "testqueue", dispatcher, factory);
  }

  @After
  public void after() throws InterruptedException {
    dispatcher.shutdown();
    executor.shutdownNow();
    executor.awaitTermination(10, TimeUnit.MILLISECONDS);
  }

  @Test(timeout = 3000)
  public void testNoFreeWorkers() throws InterruptedException {
    final AtomicBoolean unLocked = new AtomicBoolean(false);
    // Add Worker which will unlock
    workerPool.onQueueUpdate("", 1, 0, 0);
    executor.execute(dispatcher);
    await().until(() -> dispatcher.getState() == State.WAIT_FOR_TASK);
    // Remove worker, 1 worker locked but at this point no actual workers available.
    workerPool.onQueueUpdate("", 0, 0, 0);
    // Send task, should get NoFreeWorkersException in dispatcher.
    forwardTask(unLocked);
    // Dispatcher should go back to wait for worker to become available.
    await().until(() -> dispatcher.getState() == State.WAIT_FOR_WORKER);
    assertEquals("WorkerPool should be empty", 0, workerPool.getCurrentWorkerSize());
    workerPool.onQueueUpdate("", 1, 0, 0);
    assertEquals("WorkerPool should have 1 running", 1, workerPool.getCurrentWorkerSize());
  }

  @Test(timeout = 3000)
  public void testForwardTest() throws InterruptedException {
    final AtomicBoolean unLocked = new AtomicBoolean(false);
    dispatcher.forwardTask(createTask());
    executor.execute(dispatcher);
    forwardTask(unLocked);
    assertFalse("Taskconsumer must be locked at this point", unLocked.get());
    workerPool.onQueueUpdate("", 1, 0, 0); //add worker which will unlock
    await().until(() -> dispatcher.getState() == State.WAIT_FOR_WORKER);
    assertTrue("Taskconsumer must be unlocked at this point", unLocked.get());
  }

  @Test(timeout = 3000)
  public void testForwardDuplicateTask() throws InterruptedException {
    final Task task = createTask();
    executor.execute(dispatcher);
    dispatcher.forwardTask(task);
    await().until(() -> dispatcher.isLocked(task));
    await().until(() -> dispatcher.getState() == State.WAIT_FOR_WORKER);
    workerPool.onQueueUpdate("", 2, 0, 0); //add worker which will unlock
    await().until(() -> !dispatcher.isLocked(task));
    // Now force the issue.
    assertSame("Taskdispatcher must be waiting for task", TaskDispatcher.State.WAIT_FOR_TASK, dispatcher.getState());
    // Forwarding same Task object, so same id.
    dispatcher.forwardTask(task);
    await().until(() -> factory.getMockTaskMessageHandler().getAbortedMessage() != null);
    await().until(() -> !dispatcher.isLocked(task));
    // Now test with a non-duplicate Task.
    dispatcher.forwardTask(createTask());
    await().until(() -> dispatcher.getState() == State.WAIT_FOR_WORKER);
  }

  private void forwardTask(final AtomicBoolean unLocked) {
    executor.execute(new Runnable() {
      @Override
      public void run() {
          dispatcher.forwardTask(createTask());
          unLocked.set(true);
      }
    });
  }

  private Task createTask() {
    return new MockTask(taskConsumer);
  }
}
