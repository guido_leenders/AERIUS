/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.util.Arrays;

import nl.overheid.aerius.util.OSUtils;

/**
 * Base implementation of an OpenCL source. Source implementations that require constant values to be set should extend this.
 */
public abstract class AbstractSource implements Source {
  private static final String CONSTANTS_INSERT = "$$CONSTANTS_INSERT$$";

  private final Constant[] constants;

  /**
   * Super constructor for sub classes.
   * @param constants Optional constant values that will be prefixed to the source.
   */
  protected AbstractSource(final Constant[] constants) {
    this.constants = constants;
  }

  /**
   * Subclasses are required to implement this to provide the source code to which the constants will be prefixed as header.
   * @return The OpenCL source code.
   */
  protected abstract String getBody();

  @Override
  public final String getSource() {
    final String body = getBody();
    return body.replace(CONSTANTS_INSERT, getConstants());
  }

  private String getConstants() {
    final StringBuilder builder = new StringBuilder();

    for (final Constant constant : constants) {
      builder.append(constant.getValue()).append(OSUtils.LNL);
    }
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.hashCode(constants);
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    final AbstractSource other = (AbstractSource) obj;

    if (!Arrays.equals(constants, other.constants)) {
      return false;
    }

    return true;
  }
}
