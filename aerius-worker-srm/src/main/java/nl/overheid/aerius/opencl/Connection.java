/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import java.nio.Buffer;
import java.nio.DoubleBuffer;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLKernel;

/**
 * A wrapper around an OpenCL device and its queue.
 */
public interface Connection extends AutoCloseable {
  /**
   * Creates a buffer for use with a kernel. Any buffer created in this way will automatically be released when the connection is closed.
   * @param buffer A direct java nio buffer.
   * @param usage The usage pattern for the buffer by the kernel
   * @param <T> The type of the buffer.
   * @return A jocl buffer.
   */
  <T extends Buffer> CLBuffer<T> createCLBuffer(T buffer, MemoryUsage usage);

  CLBuffer<DoubleBuffer> createDoubleCLBuffer(final int size, final MemoryUsage usage);

  <T extends Buffer> CLBuffer<T> createAndUploadBuffer(final T buffer, final MemoryUsage usage);

  void rewindAndUploadBuffer(final CLBuffer<?> buffer);

  void uploadBuffer(final CLBuffer<?> buffer);

  void downloadBuffer(final CLBuffer<?> buffer);

  void releaseBuffers();

  /**
   * Releases the buffer, allowing the memory to be reclaimed by the garbage collector.
   * @param buffer The buffer to be released
   */
  void releaseBuffer(CLBuffer<? extends Buffer> buffer);

  /**
   * Creates a buffer for use with a kernel. Any buffer created in this way will automatically be released when the connection is closed.
   * @param size The number of elements in the buffer.
   * @param type The class of the wrapped java nio buffer.
   * @param usage The usage pattern for the buffer by the kernel
   * @param <T> The type of the buffer.
   * @return A jocl buffer.
   */
  <T extends Buffer> CLBuffer<T> createBuffer(int size, Class<T> type, MemoryUsage usage);

  /**
   * Returns a kernel with the given name and compiles the program in source if that was not yet done for the device.
   * @param source The source for the program that contains the kernel.
   * @param kernelName The name of the kernel.
   * @return The kernel.
   */
  CLKernel getKernel(Source source, String kernelName);

  /**
   * Queues the upload of a buffer to device memory.
   * @param buffer The buffer to upload.
   * @param block If true the call will block until the upload is finished.
   */
  void queueBufferWrite(CLBuffer<? extends Buffer> buffer, boolean block);

  /**
   * Queues the download of a buffer from device memory.
   * @param buffer The buffer to download.
   * @param block If true the call will block until the download is finished.
   */
  void queueBufferRead(CLBuffer<? extends Buffer> buffer, boolean block);

  /**
   * Queues the execution of a 1D kernel.
   * @param kernel The kernel to be executed.
   * @param worksize The amount of work items for the kernel.
   */
  void queue1DKernelExecution(CLKernel kernel, long worksize);

  /**
   * @return The maximum number of bytes of global memory available for this connection.
   */
  long getMaxGlobalMemSize();

  /**
   * @return The maximum number of bytes of local memory available for this connection.
   */
  long getMaxLocalMemSize();

  /**
   * @return The maximum number of bytes available for a single buffer.
   */
  long getMaxBufferSize();

  /**
   * Closes the connection and releases all buffers created with either createBuffer() method.
   */
  @Override
  void close();
}
