/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.util.OSUtils;

/**
 * Writes emissions and geometry of srm2 objects to file.
 */
public final class SRM2EmissionsWriter {

  private static final String EMISSIONS = "_emissions_";
  private static final String FILE_EXTENSION = ".csv";
  private static final String FORMAT = "%s;%s;%f;%f;%f" + OSUtils.NL;
  private static final String HEADER = "description;geometry;%s;%s;%s" + OSUtils.NL;
  private static final String FOOTER = "total;;%f;%f;%f" + OSUtils.NL;

  private SRM2EmissionsWriter() {
    // util class
  }

  /**
   * Writes the emission values per source to a file.
   * @param file file without extension
   * @param year year to get emissions for
   * @param sources sources
   * @throws IOException io error
   */
  public static void writeEmissions(final File file, final int year, final List<EmissionSource> sources) throws IOException {
    try (final Writer writer = Files.newBufferedWriter(createFileName(file, year) , StandardCharsets.UTF_8)) {
      final List<EmissionValueKey> keys = new ArrayList<>();
      keys.add(new EmissionValueKey(year, Substance.NOX));
      keys.add(new EmissionValueKey(year, Substance.NO2));
      keys.add(new EmissionValueKey(year, Substance.NH3));
      writer.write(String.format(Locale.US, HEADER, keys.get(0).getSubstance().getName(), keys.get(1).getSubstance().getName(),
          keys.get(2).getSubstance().getName()));
      final Map<EmissionValueKey, Double> totalEmissionMap = new HashMap<>();
      for (final EmissionValueKey key : keys) {
        totalEmissionMap.put(key, 0.0);
      }
      for (final EmissionSource es : sources) {
        if (es instanceof SRM2NetworkEmissionSource) {
          for (final EmissionSource nes : ((SRM2NetworkEmissionSource) es).getEmissionSources()) {
            writeSource(writer, keys, totalEmissionMap, nes);
          }
        } else {
          writeSource(writer, keys, totalEmissionMap, es);
        }
      }
      writer.write(String.format(Locale.US, FOOTER, totalEmissionMap.get(keys.get(0)), totalEmissionMap.get(keys.get(1)),
          totalEmissionMap.get(keys.get(2))));
    }
  }

  private static void writeSource(final Writer writer, final List<EmissionValueKey> keys, final Map<EmissionValueKey, Double> totalEmissionMap,
      final EmissionSource es) throws IOException {
    writer.write(String.format(Locale.US, FORMAT, es.getLabel(), es.getGeometry().getWKT(),
        es.getEmission(keys.get(0)), es.getEmission(keys.get(1)), es.getEmission(keys.get(2))));
    for (final EmissionValueKey key : keys) {
      totalEmissionMap.put(key, totalEmissionMap.get(key) + es.getEmission(key));
    }
  }

  private static Path createFileName(final File file, final int year) {
    return new File(file.getAbsoluteFile() + EMISSIONS + year + FILE_EXTENSION).toPath();
  }
}
