/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.calculation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.opencl.Constant;
import nl.overheid.aerius.opencl.Context;
import nl.overheid.aerius.opencl.DefineConstant;
import nl.overheid.aerius.opencl.FileSource;
import nl.overheid.aerius.opencl.Source;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;
import nl.overheid.aerius.srm.backgrounddata.MapData;
import nl.overheid.aerius.srm.backgrounddata.PreSRMData;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * An SRM2 implementation that uses OpenCL.
 */
public class SRM2Calculator {

  private static final String SOURCECODE_CL = "nl/overheid/aerius/srm2/SRM2Algorithm.cl";
  private static final String TEST_ENABLED = "TEST_ENABLED";
  private static final String DEPOSITION_CONVERSION_NH3 = "DEPOSITION_CONVERSION_NH3";
  private static final String DEPOSITION_CONVERSION_NO2 = "DEPOSITION_CONVERSION_NO2";
  private static final String WIND_SECTORS = "WIND_SECTORS";
  private static final String MAX_SEGMENT_LENGTH = "MAX_SEGMENT_LENGTH";
  /**
   * Constant used as define in openCL code. If set it will calculate with the given number of rings.
   * if set it will not calculate sources within a given distance of a receptor
   * If not set it will always calculate and set a minimum distance of source-receptor, if the actual distance is below the minimum.
   */
  private static final String NR_SUB_RECEPTORS = "NR_SUB_RECEPTORS";

  private static final Logger LOG = LoggerFactory.getLogger(SRM2Calculator.class);

  // Lock actual calculation over multiple instances of this class, to make sure only one actual calculation is done.
  private static final Object LOCK = new Object();

  private final RangeUtil rangeUtil;
  private final Context context;
  private final SourceData sourceData;
  private final long connectionTimeout;
  private final SRM2CalculationOptions options;

  /**
   * Create a new instance with the specified OpenCL context.
   *
   * @param context The OpenCL context to use.
   * @param preSRMData  Preloaded pre-srm data
   * @param connectionTimeout timout for connecting to the OpenCL runtime
   * @param options specific profile srm2 options
   */
  public SRM2Calculator(final Context context, final PreSRMData preSRMData, final long connectionTimeout, final SRM2CalculationOptions options) {
    this.context = context;
    this.connectionTimeout = connectionTimeout;
    this.options = options;
    final Source programSource = createSource(preSRMData, false, options);
    sourceData = new SourceData(programSource, preSRMData);
    rangeUtil = new RangeUtil(options.getMaxSourceDistanceMeter());
  }

  /**
   * Loads the OpenCL source file and prefixes it with defines for constants. All pre-srm data should be loaded before
   * calling this.
   *
   * @param preSRMData
   *          The pre-srm data.
   * @param test
   *          True if test kernels should be enabled. Should be false for production.
   * @param options
   * @return The newly created OpenCL source.
   */
  static Source createSource(final PreSRMData preSRMData, final boolean test, final SRM2CalculationOptions options) {
    final ArrayList<Constant> constants = new ArrayList<>();

    constants.add(new DefineConstant(MAX_SEGMENT_LENGTH, SRMConstants.MAX_SEGMENT_LENGTH));
    constants.add(new DefineConstant(WIND_SECTORS, SRMConstants.WIND_SECTORS));
    constants.add(new DefineConstant(DEPOSITION_CONVERSION_NO2, SRMConstants.DEPOSITION_CONVERSION_NO2));
    constants.add(new DefineConstant(DEPOSITION_CONVERSION_NH3, SRMConstants.DEPOSITION_CONVERSION_NH3));

    if (options.isSubReceptorCalculation()) {
      addSubReceptorCalculationConstants(constants);
    }
    addLandUseConstants(constants, preSRMData);

    if (test) {
      constants.add(new DefineConstant(TEST_ENABLED, true));
    }

    return new FileSource(SOURCECODE_CL, constants.toArray(new Constant[constants.size()]));
  }

  private static void addSubReceptorCalculationConstants(final List<Constant> constants) {
    final SubReceptorDisplacementsConstant displacements = new SubReceptorDisplacementsConstant(SRMConstants.SUB_RECEPTOR_RINGS,
        SRMConstants.ZOOM_LEVEL_1);

    constants.add(new DefineConstant(NR_SUB_RECEPTORS, displacements.getNumberOfSubReceptors()));
    constants.add(displacements);
  }

  private static void addLandUseConstants(final List<Constant> constants, final PreSRMData preSRMData) {
    setLandUse(constants, LandUseType.LOW, preSRMData.getLandUse(LandUseType.LOW));
    setLandUse(constants, LandUseType.MED, preSRMData.getLandUse(LandUseType.MED));
    setLandUse(constants, LandUseType.HIGH, preSRMData.getLandUse(LandUseType.HIGH));
  }

  private static void setLandUse(final List<Constant> constants, final LandUseType landUseType, final MapData mapData) {
    final String lut = landUseType.name().toUpperCase(Locale.ENGLISH);
    final String landUse = "LANDUSE_" + lut;

    constants.add(new DefineConstant(landUse + "_WIDTH", mapData.getWidth()));
    constants.add(new DefineConstant(landUse + "_HEIGHT", mapData.getHeight()));
    constants.add(new DefineConstant(landUse + "_OFFSET_X", mapData.getOffsetX()));
    constants.add(new DefineConstant(landUse + "_OFFSET_Y", mapData.getOffsetY()));
    constants.add(new DefineConstant(landUse + "_DIAMETER", mapData.getDiameter()));
    constants.add(new DefineConstant(landUse + "_DEFAULT", mapData.getNoDataValue()));
  }

  /**
   * Calculates the concentration at the receptors caused by the road segments for the given year.
   *
   * @param year
   *          The year for which to calculate the results.
   * @param substances
   *          The substances to calculate for.
   * @param emissionResultKeys
   *          The EmissionResultKeys to get results for.
   * @param sources
   *          The road segments that are the sources.
   * @param receptors
   *          The receptors for which to calculate the results.
   * @return Returns a map containing a list of results for each substance in the same order as the provided calculation
   *         receptors.
   * @throws AeriusException
   */
  public ArrayList<AeriusResultPoint> calculate(final int year, final List<Substance> substances, final EnumSet<EmissionResultKey> emissionResultKeys,
      final Collection<SRM2RoadSegment> sources, final Collection<AeriusPoint> receptors) throws AeriusException {
    LOG.info("Starting calculation for {} sources, {} receptors, {} substances and {} emissionResultKeys.", sources.size(), receptors.size(),
        substances.size(), emissionResultKeys.size());

    final ArrayList<AeriusResultPoint> results = new ArrayList<>(receptors.size());
    final ArrayList<AeriusResultPoint> emptyResults = new ArrayList<>(receptors.size());

    computeEmptyResults(emissionResultKeys, sources, receptors, results, emptyResults);

    if (sources.isEmpty()) {
      LOG.info("Nothing to do, sources list is empty");
    } else if (results.isEmpty()) {
      LOG.info("No receptors to calculate.");
    } else {
      actualCalculate(year, substances, emissionResultKeys, sources, results);
    }
    results.addAll(emptyResults);

    return results;
  }

  private void computeEmptyResults(final Set<EmissionResultKey> emissionResultKeys, final Collection<SRM2RoadSegment> sources,
      final Collection<AeriusPoint> receptors, final ArrayList<AeriusResultPoint> results,
      final ArrayList<AeriusResultPoint> emptyResults) {

    for (final AeriusPoint inputPoint : receptors) {
      final AeriusResultPoint receptor = new AeriusResultPoint(inputPoint.getId(), inputPoint.getPointType(), inputPoint.getX(), inputPoint.getY());

      if (rangeUtil.inAnyRange(sources, receptor)) {
        results.add(receptor);
      } else {
        emissionResultKeys.forEach(key -> receptor.setEmissionResult(key, 0.0));
        emptyResults.add(receptor);
      }
    }
    if (LOG.isInfoEnabled()) {
      LOG.info("Skipping {} receptors that have no source in range.", emptyResults.size());
    }
  }

  private void actualCalculate(final int year, final Collection<Substance> substances, final Set<EmissionResultKey> emissionResultKeys,
      final Collection<SRM2RoadSegment> sources, final ArrayList<AeriusResultPoint> results) throws AeriusException {
    final List<CalculationContext> calculations = new ArrayList<>();
    final boolean cdep = options.isCalculateDeposition();

    synchronized (LOCK) {
      if (substances.contains(Substance.NOX) || substances.contains(Substance.NO2)) {
        calculations.add(
            new CalculationContext(sourceData, year, Substance.NO2, sources, results, new NOX2CalculationContext(emissionResultKeys), cdep));
      }
      if (substances.contains(Substance.NH3)) {
        final EnumSet<EmissionResultKey> erks = emissionResultKeys.stream()
            .filter(erk -> erk == EmissionResultKey.NH3_CONCENTRATION || erk == EmissionResultKey.NH3_DEPOSITION)
            .collect(Collectors.toCollection(() -> EnumSet.noneOf(EmissionResultKey.class)));
        calculations.add(new CalculationContext(sourceData, year, Substance.NH3, sources, results, new NH3CalculationContext(erks), cdep));
      }
      if (substances.contains(Substance.PM10)) {
        final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.PM10_CONCENTRATION);
        calculations.add(new CalculationContext(sourceData, year, Substance.PM10, sources, results, new NH3CalculationContext(erks), cdep));
      }
      if (substances.contains(Substance.PM25)) {
        final Set<EmissionResultKey> erks = EnumSet.of(EmissionResultKey.PM25_CONCENTRATION);
        calculations.add(new CalculationContext(sourceData, year, Substance.PM25, sources, results, new NH3CalculationContext(erks), cdep));
      }

      final long start = System.nanoTime();

      for (final CalculationContext calculationContext : calculations) {
        calculationContext.runCalculation(context, connectionTimeout);
      }
      LOG.info("Computation took {} ms", TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start));
    }
  }
}
