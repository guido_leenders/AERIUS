/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.Objects;

import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

import nl.overheid.aerius.srm.SRMConstants;

/**
 * Data class for windrose concentration O3 and background NH3 information at a x-y coordinate.
 */
public class WindRoseConcentration extends Point implements WindRose {

  private static final long serialVersionUID = 862350448386360647L;

  private final double[] o3;
  private double backgroundO3;
  private double backgroundNH3;
  private double backgroundNO2;
  private double backgroundPM10;
  private double backgroundPM25;

  public WindRoseConcentration(final CoordinateSequence coordinateSequence, final GeometryFactory factory) {
    super(coordinateSequence, factory);
    o3 = new double[SRMConstants.WIND_SECTORS];
  }

  @Override
  public boolean equals(final Object o) {
    return super.equals(o) && Objects.equals(o3, ((WindRoseConcentration) o).o3)
        && Objects.equals(backgroundO3, ((WindRoseConcentration) o).getBackgroundO3())
        && Objects.equals(backgroundNH3, ((WindRoseConcentration) o).getBackgroundNH3())
        && Objects.equals(backgroundNO2, ((WindRoseConcentration) o).getBackgroundNO2())
        && Objects.equals(backgroundPM10, ((WindRoseConcentration) o).getBackgroundPM10())
        && Objects.equals(backgroundPM25, ((WindRoseConcentration) o).getBackgroundPM25());
  }

  @Override
  public int hashCode() {
    return Objects.hash(o3, backgroundO3, backgroundNH3, backgroundNO2, backgroundPM10, backgroundPM10) + 31 * super.hashCode();
  }

  public double getBackgroundNH3() {
    return backgroundNH3;
  }

  public void setBackgroundNH3(final double backgroundNH3) {
    this.backgroundNH3 = backgroundNH3;
  }

  public double getBackgroundNO2() {
    return backgroundNO2;
  }

  public void setBackgroundNO2(final double backgroundNO2) {
    this.backgroundNO2 = backgroundNO2;
  }

  public double getBackgroundO3() {
    return backgroundO3;
  }

  public void setBackgroundO3(final double backgroundO3) {
    this.backgroundO3 = backgroundO3;
  }

  public double getBackgroundPM10() {
    return backgroundPM10;
  }

  public void setBackgroundPM10(final double backgroundPM10) {
    this.backgroundPM10 = backgroundPM10;
  }

  public double getBackgroundPM25() {
    return backgroundPM25;
  }

  public void setBackgroundPM25(final double backgroundPM25) {
    this.backgroundPM25 = backgroundPM25;
  }

  public double getO3(final int sector) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for O3, sector:" + sector);
    }
    return o3[sector];
  }

  public void setO3(final int sector, final double o3Factor) {
    if (sector < 0 || sector >= SRMConstants.WIND_SECTORS) {
      throw new IndexOutOfBoundsException("Invalid wind sector for O3, sector:" + sector);
    }

    o3[sector] = o3Factor;
  }
}
