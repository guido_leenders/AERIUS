/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.srm.backgrounddata.LandUseData.LandUseType;

/**
 * Data object containing the PRE-SRM calculated data.
 *
 * @NotThreadSafe
 */
public class PreSRMData {

  private final int srid;
  private final WindRoseTable<WindRoseSpeed> windRoseSpeedPrognoseTable;
  private final Map<Integer, WindRoseTable<WindRoseConcentration>> windRoseConcentrationTable = new HashMap<>();
  private LandUseData landUseData;
  private Map<Substance, DepositionVelocityMap> depositionVelocityMaps = new EnumMap<>(Substance.class);

  public PreSRMData(final int srid) {
    this.srid = srid;
    windRoseSpeedPrognoseTable = new WindRoseTable<>(srid);
  }

  public WindRoseTable<WindRoseConcentration> getWindRoseConcentrationTable(final int year) {
    if (!windRoseConcentrationTable.containsKey(year)) {
       windRoseConcentrationTable.put(year, new WindRoseTable<WindRoseConcentration>(srid));
    }
    return windRoseConcentrationTable.get(year);
  }

  public WindRoseTable<WindRoseSpeed> getWindRoseSpeedTable() {
    return windRoseSpeedPrognoseTable;
  }

  public MapData getLandUse(final LandUseType type) {
    return landUseData.getMapData(type);
  }

  /**
   * @See {@link LandUseData#getLandUseValueSource(double, double)}.
   */
  public double getLandUseValueSource(final double x, final double y) {
    return landUseData.getLandUseValueSource(x, y);
  }

  void setLandUseData(final LandUseData landUseData) {
    this.landUseData = landUseData ;
  }

  public DepositionVelocityMap getDepositionVelocityMap(final Substance substance) {
    return depositionVelocityMaps.get(substance);
  }

  void setDepositionVelocityMaps(final Map<Substance, DepositionVelocityMap> depositionVelocityMaps) {
    this.depositionVelocityMaps = depositionVelocityMaps;
  }
}
