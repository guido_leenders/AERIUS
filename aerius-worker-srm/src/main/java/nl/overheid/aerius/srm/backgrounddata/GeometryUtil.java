/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;

import nl.overheid.aerius.geo.shared.RDNew;

/**
 * Geometry util class to construct {@link CoordinateSequence} geometric objects.
 */
final class GeometryUtil {

  public static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory(new PrecisionModel(), RDNew.SRID);

  private GeometryUtil() {
    // Util class.
  }

  public static WindRoseSpeed create(final int x, final int y) {
    return new WindRoseSpeed(getCoordinateSequence(x, y), GEOMETRY_FACTORY);
  }

  public static CoordinateSequence getCoordinateSequence(final int x, final int y) {
    return GEOMETRY_FACTORY.getCoordinateSequenceFactory().create(new Coordinate[]{new Coordinate(x, y)});
  }

}
