/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.worker.WorkerConfiguration;

/**
 * SRM Worker configuration options.
 */
public class SRMWorkerConfiguration extends WorkerConfiguration {

  private static final String SRM_WORKER_PREFIX = "srm2";

  public static class SRMProfileConfiguration extends WorkerConfiguration {
    private static final String SRM_WORKER_PRESRM_DIRECTORY = "presrm.directory";

    private static final String SRM2_WORKER_LANDUSE_DIRECTORY = "landuse.directory";
    private static final String SRM2_WORKER_DEPOSITION_VELOCITY_FILE = "depositionvelocity.file";
    private static final String SRM_WORKER_GCN_DATA_DIRECTORY = "gcn.directory";
    private static final String SRM2_WORKER_DEPOSITION_VELOCITY_EXTENSION = ".csv";
    private final SRM2CalculationOptions options;


    public SRMProfileConfiguration(final Properties properties, final SRM2CalculationOptions opt) {
      super(properties, SRM_WORKER_PREFIX);
      this.options = opt;
    }

    @Override
    protected List<String> getValidationErrors() {
      final List<String> reasons = new ArrayList<>();
      validateDirectoryProperty(SRM_WORKER_PRESRM_DIRECTORY, reasons, false);
      if (options.isCalculateSRM1()) {
        validateDirectoryProperty(SRM_WORKER_GCN_DATA_DIRECTORY, reasons, false);
      }
      if (options.isCalculateDeposition()) {
        validateFilepathProperty(SRM2_WORKER_DEPOSITION_VELOCITY_FILE, reasons, SRM2_WORKER_DEPOSITION_VELOCITY_EXTENSION);
      }
      return reasons;
    }

    public SRM2CalculationOptions getOptions() {
      return options;
    }

    public File getPreSRMDirectory() {
      final String preSRMDirectory = getProperty(SRM_WORKER_PRESRM_DIRECTORY);

      return preSRMDirectory == null ? null : new File(preSRMDirectory);
    }

    public File getLandUseDirectory() {
      final String landUseDirectory = getProperty(SRM2_WORKER_LANDUSE_DIRECTORY);
      return landUseDirectory == null ? null : new File(landUseDirectory);
    }

    public File getDepositionVelocityFilepath() {
      final String depostionVelocityFilepath = getProperty(SRM2_WORKER_DEPOSITION_VELOCITY_FILE);
      return depostionVelocityFilepath == null ? null : new File(depostionVelocityFilepath);
    }

    public File getGCNDirectory() {
      final String gcnDirectory = getProperty(SRM_WORKER_GCN_DATA_DIRECTORY);
      return gcnDirectory == null ? null : new File(gcnDirectory);
    }

    public boolean isConfigured() {
      return getPreSRMDirectory() != null;
    }
  }

  private static final String SRM2_WORKER_CONNECTION_TIMEOUT = "connection_timeout";
  private static final String SRM2_WORKER_FORCE_CPU = "force_cpu";

  private final Map<Profile, SRMProfileConfiguration> profileConfig = new EnumMap<>(Profile.class);

  public SRMWorkerConfiguration(final Properties properties, final List<SRM2CalculationOptions> options) {
    super(properties, SRM_WORKER_PREFIX);
    options.forEach(opt -> profileConfig.put(opt.getProfile(), new SRMProfileConfiguration(properties, opt)));
  }

  public SRMProfileConfiguration getProfileConfiguration(final Profile profile) {
    return profileConfig.get(profile);
  }

  public boolean hasProfileConfig(final Profile profile) {
    return profileConfig.containsKey(profile) && profileConfig.get(profile).isConfigured();
  }

  @Override
  protected List<String> getValidationErrors() {
    if (getProcesses() > 0) {
      return profileConfig.entrySet().stream().flatMap(e -> e.getValue().getValidationErrors().stream()).collect(Collectors.toList());
    } else {
      return Collections.emptyList();
    }
  }

  public boolean isForceCpu() {
    return getPropertyBooleanSafe(SRM2_WORKER_FORCE_CPU);
  }

  public long getConnectionTimeout() {
    return getPropertyIntSafe(SRM2_WORKER_CONNECTION_TIMEOUT);
  }
}
