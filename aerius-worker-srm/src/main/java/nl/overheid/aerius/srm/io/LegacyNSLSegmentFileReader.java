/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;

import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.io.AbstractLineColumnReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadSpeedType;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier.RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Reader to read Nationaal Samenwerkingsprogramma Luchtkwaliteit (NSL) legacy csv files.
 */
class LegacyNSLSegmentFileReader extends AbstractLineColumnReader<EmissionSource> {
  private static final Logger LOGGER = LoggerFactory.getLogger(LegacyNSLSegmentFileReader.class);

  //segment_id:nwb_weg_id:nwb_versie:begin_pos:eind_pos:overheidid:overheid      :
  //9187      :261318052 :801       :0        :0.498778:1904      :Stichtse Vecht:

  //straatnaam:straatnr:wegbeheer:hoogte:x        :y        :wegtype:snelheid:
  //Floraweg  :        :G        :0     :130879.00:459350.00:92     :e       :

  //tun_factor:boom_fact:maxsnelh_p:maxs_p_dyn:maxsnelh_v:a_rand_l:a_gevel_l:bebdicht_l:
  //1         :1        :50        :          :50        :9.40    :         :          :

  //a_toepas_l:a_scherm_l:s_hoogte_l:a_rand_r:a_gevel_r:bebdicht_r:a_toepas_r:a_scherm_r:
  //          :          :          :9.50    :42.80    :0.02      :          :          :

  //s_hoogte_r:stagf_lv:int_lv:int_lv_dyn:stagf_mv:int_mv:stagf_zv:int_zv:stagf_bv:int_bv:
  //          :0.000000:8077  :          :0.000000:217   :0.000000:170   :0.000000:46    :

  //opmerking;gewijzigd           ;geomet_wkt                             ;actie
  //          ;28-05-2013 10:22:07;LINESTRING(130835 459390,130923 459309);u

  /**
   * 92 = road on the road network with a broad profile
   * weg van het onderliggende wegennet met een breed profiel
   */
  private static final int ROAD_WIDE_PROFILE = 92;
  /**
   * 93 = freeway on the road network with a broad profile
   * (snel)weg van het hoofdwegennet met een breed profiel
   */
  private static final int FREEWAY_WIDE_PROFILE = 93;
  /**
   * 94 = freeway on the road network with a broad profile and, strict enforcement of the speed
   * (snel)weg van het hoofdwegennet met een breed profiel en toepassing van strikte handhaving op de snelheid
   * */
  private static final int FREEWAY_WIDE_PROFILE_SE = 94;

  // @formatter:off
  private enum Columns {
    SEGMENT_ID,
    NWB_WEG_ID,
    NWB_VERSIE,
    BEGIN_POS,
    EIND_POS,
    OVERHEIDID,
    OVERHEID,
    STRAATNAAM,
    STRAATNR,
    WEGBEHEER,
    HOOGTE,
    X,
    Y,
    WEGTYPE,
    SNELHEID,
    TUN_FACTOR,
    BOOM_FACT,
    /**
     * Maximum speed cars.
     */
    MAXSNELH_P,
    /**
     * Maximum speed cars dynamic. set speed different at certain times during the day.
     */
    MAXS_P_DYN,
    /**
     * Maximum speed trucks
     */
    MAXSNELH_V,
    A_RAND_L,
    A_GEVEL_L,
    BEBDICHT_L,
    A_TOEPAS_L,
    A_SCHERM_L,
    S_HOOGTE_L,
    A_RAND_R,
    A_GEVEL_R,
    BEBDICHT_R,
    A_TOEPAS_R,
    A_SCHERM_R,
    S_HOOGTE_R,
    STAGF_LV,
    INT_LV,
    INT_LV_DYN,
    STAGF_MV,
    INT_MV,
    STAGF_ZV,
    INT_ZV,
    STAGF_BV,
    INT_BV,
    OPMERKING,
    GEWIJZIGD,
    GEOMET_WKT,
    ACTIE;
    // @formatter:on

    static Columns safeValueOf(final String value) {
      try {
        return value == null ? null : valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (final IllegalArgumentException e) {
        return null;
      }
    }
  }

  private static final Columns[] EXPECTED_COLUMNS = new Columns[] {
      Columns.WEGTYPE, Columns.SNELHEID, Columns.NWB_WEG_ID, Columns.SEGMENT_ID, Columns.MAXSNELH_P, Columns.MAXS_P_DYN, Columns.INT_LV,
      Columns.INT_LV_DYN, Columns.STAGF_LV, Columns.MAXSNELH_V, Columns.INT_MV, Columns.STAGF_MV, Columns.MAXSNELH_V, Columns.INT_ZV,
      Columns.STAGF_ZV, Columns.MAXSNELH_V, Columns.INT_BV, Columns.STAGF_BV, Columns.HOOGTE, Columns.TUN_FACTOR, Columns.BOOM_FACT,
      Columns.S_HOOGTE_L, Columns.A_SCHERM_L, Columns.S_HOOGTE_R, Columns.A_SCHERM_R, Columns.GEOMET_WKT,
  };

  private static final String COLUMN_SEPARATOR = ";";

  private final SectorCategories categories;
  private final Map<Columns, Integer> columnIndexes = new EnumMap<>(Columns.class);

  /**
   * @param categories The context to be used for determining road emission categories.
   */
  public LegacyNSLSegmentFileReader(final SectorCategories categories) {
    super(COLUMN_SEPARATOR);
    this.categories = categories;
  }

  /**
   * Reads the file and returns the results.
   * @param reader containing csv file content
   * @return results
   * @throws IOException general read problem
   */
  public LineReaderResult<EmissionSource> readObjects(final BufferedReader reader) throws IOException {
    return super.readObjects(reader, 0, Integer.MAX_VALUE, false);
  }

  @Override
  public LineReaderResult<EmissionSource> readObjects(final InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException("readObjects for inputstream is not supported.");
  }

  /**
   * Try to parse the given header and if successful collects the header column indexes.
   *
   * @param header the string (single line) that should contain the header text
   * @return null of header file could be parsed, else the error.
   * @throws IOException
   */
  public AeriusException parseHeader(final String header) throws IOException {
    return headerToColumnIndexes(header);
  }

  @Override
  protected String skipHeaderRow(final BufferedReader reader, final int headerRowCount) throws IOException, AeriusException {
    // header is handled in {@link #parseHeader}.
    // header is parsed in parseHeader, but to set the current line index passed the header the increment is needed here.
    incrementCurrentLineNumber();
    return null;
  }

  @Override
  protected SRM2EmissionSource parseLine(final String line, final List<AeriusException> warnings) throws AeriusException {
    final SRM2EmissionSource srm2 = new SRM2EmissionSource();

    srm2.setSegmentId(getInt(Columns.SEGMENT_ID));
    srm2.setId(srm2.getSegmentId());
    final int wegType = (int) getDouble(Columns.WEGTYPE); //wegtype is int, but user could store as double.
    final boolean strictEnforcement = wegType == FREEWAY_WIDE_PROFILE_SE;
    final RoadSpeedType speedType = snelheidToSpeedType(getString(Columns.SNELHEID));
    final RoadType roadType = wegBeheerToRoadType(wegType, speedType);
    final WKTGeometry wktGeometry = getGeometry();
    srm2.setGeometry(wktGeometry);
    final Point point = GeometryUtil.middleOfGeometry(wktGeometry);
    srm2.setX(point.getX());
    srm2.setY(point.getY());

    srm2.setLabel(getString(Columns.NWB_WEG_ID) + "-" + getString(Columns.SEGMENT_ID));
    srm2.setSector(categories.getSectorById(roadType.getSectorId()));
    srm2.setRoadType(wegType);
    //Store the sector id as ParticleSizeDistribution, needed for OPS.
    srm2.setSourceCharacteristics(srm2.getSector().getDefaultCharacteristics().copy());
    srm2.getSourceCharacteristics().setParticleSizeDistribution(srm2.getSector().getSectorId());

    final ArrayList<VehicleEmissions> allTraffic = srm2.getEmissionSubSources();
    addTraffic(allTraffic, roadType, VehicleType.LIGHT_TRAFFIC, Columns.MAXSNELH_P, Columns.INT_LV, Columns.STAGF_LV, strictEnforcement, speedType);
    //dynamic does not have a stagnation factor.
    addTraffic(allTraffic, roadType, VehicleType.LIGHT_TRAFFIC, Columns.MAXS_P_DYN, Columns.INT_LV_DYN, null, strictEnforcement, speedType);
    addTraffic(allTraffic, roadType, VehicleType.NORMAL_FREIGHT, Columns.MAXSNELH_V, Columns.INT_MV, Columns.STAGF_MV, strictEnforcement, speedType);
    addTraffic(allTraffic, roadType, VehicleType.HEAVY_FREIGHT, Columns.MAXSNELH_V, Columns.INT_ZV, Columns.STAGF_ZV, strictEnforcement, speedType);
    addTraffic(allTraffic, roadType, VehicleType.AUTO_BUS, Columns.MAXSNELH_V, Columns.INT_BV, Columns.STAGF_BV, strictEnforcement, speedType);

    final int height = (int) Math.round(getDouble(Columns.HOOGTE));
    srm2.setElevationHeight(height);
    srm2.setElevation(getRoadElevation(height));
    srm2.setFreeway(roadType == RoadType.FREEWAY);

    srm2.setTunnelFactor(getDouble(Columns.TUN_FACTOR));
    if (srm2.getTunnelFactor() <= 0) {
      warnings.add(new AeriusException(Reason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO, String.valueOf(srm2.getTunnelFactor()), srm2.getLabel()));
      srm2.setTunnelFactor(0.0);
    }

    final RoadSideBarrier barrierLeft = getRoadSideBarrier(Columns.S_HOOGTE_L, Columns.A_SCHERM_L);
    if (Double.doubleToLongBits(barrierLeft.getHeight()) != 0) {
      srm2.setBarrierLeft(barrierLeft);
    }

    final RoadSideBarrier barrierRight = getRoadSideBarrier(Columns.S_HOOGTE_R, Columns.A_SCHERM_R);
    if (Double.doubleToLongBits(barrierRight.getHeight()) != 0) {
      srm2.setBarrierRight(barrierRight);
    }
    return srm2;
  }

  /**
   * Parses the header and maps column names in the header to colum index values.
   * Return is header validation result.
   *
   * @param header String containing the file header
   *
   * @return null if valid else an {@link AeriusException}
   */
  private AeriusException headerToColumnIndexes(final String header) {
    final List<String> columnsInHeader = Arrays.asList(header.trim().split(COLUMN_SEPARATOR));
    for (final String column : columnsInHeader) {
      final Columns knownValue = Columns.safeValueOf(column);
      if (knownValue != null) {
        columnIndexes.put(knownValue, columnsInHeader.indexOf(column));
      }
    }
    return validateHeader();
  }

  /**
   * Validates the header. If valid null is returned else an {@link AeriusException}.
   *
   * @return null if valid else an {@link AeriusException}
   */
  private AeriusException validateHeader() {
    final List<Columns> notFound = new ArrayList<>();
    for (final Columns expected : EXPECTED_COLUMNS) {
      if (!columnIndexes.containsKey(expected)) {
        notFound.add(expected);
      }
    }

    return notFound.isEmpty() ? null : new AeriusException(Reason.SRM2_MISSING_COLUMN_HEADER, StringUtils.join(notFound, ", "));
  }

  private String getString(final Columns column) throws AeriusException {
    return getString(column.name(), columnIndexes.get(column));
  }

  private int getInt(final Columns column) throws AeriusException {
    return getInt(column.name(), columnIndexes.get(column));
  }

  private double getDouble(final Columns column) throws AeriusException {
    return getDouble(column.name(), columnIndexes.get(column));
  }

  private WKTGeometry getGeometry() throws AeriusException {
    final String lineGeo = getString(Columns.GEOMET_WKT);
    if (StringUtils.isEmpty(lineGeo)) {
      throw new AeriusException(Reason.SRM2_INCORRECT_EXPECTED_VALUE, String.valueOf(getCurrentLineNumber()), Columns.GEOMET_WKT.name());
    }
    final Geometry geometry;
    try {
      geometry = GeometryUtil.getGeometry(lineGeo.replace('"', ' ').trim());
    } catch (final AeriusException e) {
      LOGGER.trace("Invalid srm2 wkt geometery: {}",lineGeo, e);
      throw new AeriusException(Reason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), lineGeo);
    }
    if (!(geometry instanceof LineString)) {
      throw new AeriusException(Reason.SRM2_INCORRECT_WKT_VALUE, String.valueOf(getCurrentLineNumber()), lineGeo);
    }
    //default toText uses 2D, hence it removes any 3D information.
    final String wkt = geometry.toText();
    //At this point we could use the getLength of geometry, however that would leave us with inconsistent length if the Z coordinate isn't 0.
    return new WKTGeometry(wkt, GeometryUtil.determineLength(wkt));
  }

  /**
   * Returns the {@link RoadElevation} enum given a height.
   * @param height actual height
   * @return road elevation enum
   */
  private RoadElevation getRoadElevation(final int height) {
    final RoadElevation re;
    if (height > 0) {
      //In line with NSL Monitoringstool roads with a height are steep dykes.
      re = RoadElevation.STEEP_DYKE;
    } else if (height < 0) {
      re = RoadElevation.TUNNEL;
    } else {
      re = RoadElevation.NORMAL;
    }
    return re;
  }

  private RoadSideBarrier getRoadSideBarrier(final Columns heightColumn, final Columns distanceColumn) throws AeriusException {
    final RoadSideBarrier barrier = new RoadSideBarrier();
    //no way to tell if it's a screen or a wall. Assume the worst.
    barrier.setBarrierType(RoadSideBarrierType.SCREEN);
    barrier.setHeight(getDouble(heightColumn));
    barrier.setDistance(getDouble(distanceColumn));
    return barrier;
  }

  /**
   * Set the source data for specific traffic types.
   * @param vehicles list to add values, but only when not zero
   * @param roadType type of the road
   * @param vehicleType vehicle type
   * @param maxSpeedColumn max speed
   * @param intensityColumn number of vehicles
   * @param stagnationColumn stagnation factor
   * @param strictEnforcement strict enforcement
   * @param speedType non freeway road speed type
   * @throws AeriusException exception when data error
   */
  private void addTraffic(final ArrayList<VehicleEmissions> vehicles, final RoadType roadType, final VehicleType vehicleType,
      final Columns maxSpeedColumn, final Columns intensityColumn, final Columns stagnationColumn, final boolean strictEnforcement,
      final RoadSpeedType speedType) throws AeriusException {
    final int maxSpeed = (int) getDouble(maxSpeedColumn);

    final RoadEmissionCategory roadEmissionCategory = categories.getRoadEmissionCategories().findClosestCategory(roadType, vehicleType,
        strictEnforcement, maxSpeed, speedType);

    if (roadEmissionCategory == null) {
      throw new AeriusException(Reason.SRM2IO_EXCEPTION_NO_ROAD_PROPERTIES, String.valueOf(getCurrentLineNumber()), getCurrentColumnContent(),
          roadType + "," + vehicleType + "," + maxSpeed + (strictEnforcement ? ",SE" : ""));
    }

    final VehicleStandardEmissions traffic = new VehicleStandardEmissions();
    traffic.setEmissionCategory(roadEmissionCategory);
    traffic.setVehicles(getDouble(intensityColumn), TimeUnit.DAY);
    if (stagnationColumn != null) {
      traffic.setStagnationFraction(getDouble(stagnationColumn));
    }

    if (Double.compare(traffic.getVehiclesPerTimeUnit(), 0) != 0) {
      vehicles.add(traffic);
    }
  }

  private RoadType wegBeheerToRoadType(final int wegtype, final RoadSpeedType speedType) throws AeriusException {
    final RoadType roadType;

    switch (wegtype) {
    case ROAD_WIDE_PROFILE:
      roadType = RoadType.NON_URBAN_ROAD;
      break;
    case FREEWAY_WIDE_PROFILE:
    case FREEWAY_WIDE_PROFILE_SE:
      roadType = RoadType.FREEWAY;
      break;
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
      // For SRM1-road valid values are: 0, 1, 2, 3, 4
      // If speed type == B this should be handled as a non urban road.
      roadType = speedType == RoadSpeedType.B ? RoadType.NON_URBAN_ROAD : RoadType.URBAN_ROAD;
      break;
    default:
      throw new AeriusException(Reason.SRM_LEGACY_INVALID_ROAD_TYPE, String.valueOf(getCurrentLineNumber()), String.valueOf(wegtype));
    }
    return roadType;
  }

  private RoadSpeedType snelheidToSpeedType(final String snelheid) throws AeriusException {
    final RoadSpeedType speedType = RoadSpeedType.safeValueOf(snelheid);

    if (speedType == null) {
      throw new AeriusException(Reason.SRM_LEGACY_INVALID_SPEED_TYPE, String.valueOf(getCurrentLineNumber()), snelheid);
    } else {
      return speedType;
    }
  }
}
