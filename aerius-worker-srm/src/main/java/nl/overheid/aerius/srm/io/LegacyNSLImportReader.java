/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.io.ImportReader;
import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Import reader for Nationaal Samenwerkingsprogramma Luchtkwaliteit (NSL) legacy csv files.
 */
public class LegacyNSLImportReader implements ImportReader {

  private static final String SRM2_FILE_EXTENSION = ".csv";

  @Override
  public boolean detect(final String filename) {
    return filename.toLowerCase(Locale.ENGLISH).endsWith(SRM2_FILE_EXTENSION);
  }

  @Override
  public void read(final String name, final InputStream inputStream, final SectorCategories categories, final Substance substance,
      final ImportResult importResult) throws IOException, AeriusException {

    LineReaderResult<?> results;
    try (InputStreamReader isr = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(isr)) {
      final String header = bufferedReader.readLine();

      try {
        results = parseSegmentFile(name, importResult, bufferedReader, categories, header);
      } catch (final AeriusException e) {
        results = null;
        importResult.getExceptions().add(e);
      }
      if (results != null) {
        importResult.getExceptions().addAll(results.getExceptions());
        importResult.getWarnings().addAll(results.getWarnings());
      }
    }
    importResult.getWarnings().add(srm2Warning());
  }

  private LineReaderResult<?> parseSegmentFile(final String name, final ImportResult importResult, final BufferedReader bufferedReader,
      final SectorCategories categories, final String header) throws IOException, AeriusException {
    final LegacyNSLSegmentFileReader reader = new LegacyNSLSegmentFileReader(categories);
    final AeriusException exception = reader.parseHeader(header);

    if (exception == null) {
      return readerSegmentFile(importResult, reader, name, bufferedReader);
    } else {
      throw exception;
    }
  }

  private LineReaderResult<?> readerSegmentFile(final ImportResult importResult, final LegacyNSLSegmentFileReader reader, final String name,
      final BufferedReader bufferedReader) throws IOException {
    final LineReaderResult<EmissionSource> results = reader.readObjects(bufferedReader);

    final EmissionSourceList emissionSources = new EmissionSourceList();
    if (!results.getObjects().isEmpty()) {
      emissionSources.add(createNetwork(name, results));
    }
    importResult.addSources(emissionSources);
    return results;
  }

  private SRM2NetworkEmissionSource createNetwork(final String name, final LineReaderResult<EmissionSource> results) {
    final SRM2NetworkEmissionSource network = new SRM2NetworkEmissionSource();
    final List<EmissionSource> objects = results.getObjects();

    network.getEmissionSources().addAll(objects);
    //Determine an approximation of the center of the network by determining the bounding box of all sources.
    final BBox boundingBox = GeometryUtil.determineBBox(objects);
    network.setX(boundingBox.getMidX());
    network.setY(boundingBox.getMidY());
    network.setGeometry(new WKTGeometry(network.toWKT()));
    //ensure the network has a sector (even if there could be more than one)
    network.setSector(objects.get(0).getSector());
    network.setLabel(name);
    return network;
  }

  private AeriusException srm2Warning() {
    return new AeriusException(Reason.SRM2_FILESUPPORT_WILL_BE_REMOVED);
  }
}
