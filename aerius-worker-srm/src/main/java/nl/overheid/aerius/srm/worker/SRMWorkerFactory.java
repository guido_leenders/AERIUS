/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm2.domain.SRM2CalculationOptions;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.taskmanager.client.WorkerType;
import nl.overheid.aerius.worker.WorkerFactory;

/**
 * Worker Factory for SRM Calculations. Depending on the {@link Profile} passed as input the specific profile calculator is run.
 */
public class SRMWorkerFactory implements WorkerFactory<SRMWorkerConfiguration> {

  private static List<SRM2CalculationOptions> options = Arrays.asList(SRMConstants.PAS_SRM_CALCULATION_OPTIONS);

  @Override
  public SRMWorkerConfiguration createConfiguration(final Properties properties) {
    return new SRMWorkerConfiguration(properties, options);
  }

  @Override
  public WorkerHandler createWorkerHandler(final SRMWorkerConfiguration config, final BrokerConnectionFactory factory) throws IOException {
    return new SRMWorkerHandler(config);
  }

  @Override
  public WorkerType getWorkerType() {
    return WorkerType.ASRM2;
  }
}
