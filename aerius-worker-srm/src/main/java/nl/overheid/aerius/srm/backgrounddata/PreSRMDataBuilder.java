/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NotDirectoryException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.io.LineReaderResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMConstants;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration.SRMProfileConfiguration;

/**
 * Builder class to load all pre-srm related data.
 * This includes:
 * <ul>
 * <li>Pre-processed Wind Rose factors and speeds</li>
 * <li>Pre-processed Wind Rose concentration O3 and NH3 background for specific years</li>
 * <li>Land use</li>
 * <li>Deposition velocity data</li>
 * </ul>
 */
public class PreSRMDataBuilder {
  private static final Logger LOG = LoggerFactory.getLogger(PreSRMDataBuilder.class);

  private static final String WIND_ROSE_FILENAME = "windrose.txt";
  private static final String CONCENTRATION_FILE_PREFIX = "con_";
  private static final Pattern CONCENTRATION_FILE_PATTERN = Pattern.compile(CONCENTRATION_FILE_PREFIX + "(\\d+)\\.txt");

  public PreSRMData build(final SRMProfileConfiguration config) throws IOException {
    final PreSRMData data = new PreSRMData(SRMConstants.SRID);
    final File preSRMDirectory = config.getPreSRMDirectory().getAbsoluteFile();
    loadWindRose(data, preSRMDirectory, config.getOptions().getPreSRMVersion());
    loadWindroseConcentration(data, preSRMDirectory, config.getOptions().getPreSRMVersion());
    loadLandUseData(data, config.getLandUseDirectory().getAbsoluteFile());
    loadDepositionVelocities(data, config.getDepositionVelocityFilepath());
    return data;
  }

  private void loadLandUseData(final PreSRMData data, final File landUseDirectory) throws IOException {
    LOG.info("Loading landUse data from: {}.", landUseDirectory);
    data.setLandUseData(LandUseDataBuilder.loadZ0(landUseDirectory));
  }

  private void loadWindRose(final PreSRMData data, final File directory, final String preSrmVersion) throws FileNotFoundException, IOException {
    LOG.info("Loading wind rose speed/factor from: {}.", directory);
    final WindRoseSpeedReader wrsReader = new WindRoseSpeedReader(preSrmVersion);
    try (final InputStream is = getInputStream(directory, WIND_ROSE_FILENAME)) {
      final LineReaderResult<WindRoseSpeed> results = wrsReader.readObjects(is);
      final List<AeriusException> exceptions = results.getExceptions();
      if (!exceptions.isEmpty()) {
        throw new IOException("Error while reading meteo file.", exceptions.get(0));
      }
      data.getWindRoseSpeedTable().addAll(results.getObjects());
    }
  }

  private void loadWindroseConcentration(final PreSRMData data, final File directory, final String preSrmVersion)
      throws IOException, FileNotFoundException {
    LOG.info("Loading concentration background data");
    LOG.info("Scanning {}", directory.getName());
    final File[] files = directory.listFiles();
    if (!directory.isDirectory() || files == null) {
      throw new NotDirectoryException("Invalid directory specified: " + directory.getPath());
    }
    for (final File file : files) {
      final Matcher matcher = CONCENTRATION_FILE_PATTERN.matcher(file.getName());

      if (matcher.matches()) {
        LOG.info("Loading {}", file.getCanonicalPath());
        final int year = Integer.parseInt(matcher.group(1));
        try (final InputStream is = new FileInputStream(file)) {
          loadWRConcentratonForYear(data, is, preSrmVersion, year);
        }
      }
    }
  }

  private void loadWRConcentratonForYear(final PreSRMData data, final InputStream is, final String preSrmVersion, final int year) throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(preSrmVersion, year);
    final LineReaderResult<WindRoseConcentration> results = reader.readObjects(is);

    final List<AeriusException> exceptions = results.getExceptions();
    if (!exceptions.isEmpty()) {
      throw new IOException("Error while reading meteo file.", exceptions.get(0));
    }
    data.getWindRoseConcentrationTable(year).addAll(results.getObjects());
  }

  public void loadDepositionVelocities(final PreSRMData data, final File depositionvelocityFilepath) throws IOException {
    LOG.info("Loading deposition velocity data from {}.", depositionvelocityFilepath);
    try (final InputStream is = new FileInputStream(depositionvelocityFilepath)) {
      data.setDepositionVelocityMaps(DepositionVelocityReader.read(is, SRMConstants.HEX_HOR, SRMConstants.getReceptorUtil()));
    }
  }

  private InputStream getInputStream(final File directory, final String filename) throws FileNotFoundException {
    return new FileInputStream(new File(directory, filename));
  }
}
