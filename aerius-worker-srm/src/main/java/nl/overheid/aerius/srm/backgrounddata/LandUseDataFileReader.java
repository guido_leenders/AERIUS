/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.DoubleBuffer;
import java.nio.charset.StandardCharsets;

import com.jogamp.common.nio.Buffers;

/**
 * Reads the Z0 data from the data files.
 */
final class LandUseDataFileReader {
  private static final int HEADER_LINES = 6;
  private static final String SPLIT_PATTERN = "\\s+";

  private LandUseDataFileReader() {
  }

  public static MapData read(final InputStream inputStream) throws IOException {
    try (final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8.name()))) {
      // reads the header. The following lines should be read in the following order as they represent the order in the file.
      final int columns = Integer.parseInt(readHeaderValue(reader));
      final int rows = Integer.parseInt(readHeaderValue(reader));
      final int offsetX = Integer.parseInt(readHeaderValue(reader));
      final int offsetY = Integer.parseInt(readHeaderValue(reader));
      final int cellSize = Integer.parseInt(readHeaderValue(reader));
      readHeaderValue(reader); // skip the value for "no valid data" row, we handle no valid data ourselves.
      return new MapData(columns, rows, offsetX, offsetY, cellSize, readData(reader, columns, rows));
    }
  }

  private static DoubleBuffer readData(final BufferedReader reader, final int columns, final int rows) throws IOException {
    final DoubleBuffer data = Buffers.newDirectDoubleBuffer(columns * rows);

    for (int y = 0; y < rows; y++) {
      final String line = reader.readLine();

      if (line == null) {
        throw new IOException("Unexpected end of file. Expected " + rows + " data lines, found " + y);
      }
      final String[] parts = line.split(SPLIT_PATTERN);

      if (parts.length != columns) {
        throw new IOException("Invalid number of columns for row " + (y + HEADER_LINES) + ". Expected " + columns + ", actual " + parts.length);
      }

      for (int x = 0; x < columns; x++) {
        // Reverse order for y values as the highest y coordinate has the lowest line number.
        // Also substract 1 to go to zero based index.
        final int index = (rows - y - 1) * columns + x;
        data.put(index, Double.parseDouble(parts[x]));
      }
    }
    return data;
  }

  private static String readHeaderValue(final BufferedReader reader) throws IOException {
    final String line = reader.readLine();
    if (line == null) {
      throw new IOException("Unexpected end of file. Expected header line.");
    }
    final String[] parts = line.split(SPLIT_PATTERN);

    if (parts.length != 2) {
      throw new IOException("Invalid header line. Split pattern of spaces doesn't return 2 values.  [" + line + "].");
    }

    return parts[1];
  }
}
