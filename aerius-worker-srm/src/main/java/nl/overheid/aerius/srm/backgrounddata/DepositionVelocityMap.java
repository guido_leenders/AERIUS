/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import java.util.HashMap;
import java.util.Map;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.srm.SRMConstants;

/**
 * Utility class that looks up deposition velocities for level 1 hexagons, in a map containing level 2 hexagons,
 * interpolating where necessary.
 */
public final class DepositionVelocityMap {
  private static final int HEXAGON_IN_LEVEL = 4;
  private final Map<Integer, Double> level2 = new HashMap<>();

  private final int hexHor;
  private final ReceptorUtil receptorUtil;

  /**
   * @param hexHor Number of hexagons on a receptor grid row for zoomlevel 1.
   */
  public DepositionVelocityMap(final int hexHor, final ReceptorUtil receptorUtil) {
    this.hexHor = hexHor;
    this.receptorUtil = receptorUtil;
  }

  boolean put(final Integer receptorId, final Double value) {
    return level2.put(receptorId, value) == null;
  }

  int size() {
    return level2.size();
  }

  /**
   * Finds the deposition velocity for the level 1 hexagon nearest the given point.
   *
   * @param point
   *          The point.
   * @return The deposition velocity, or null if they could not be found.
   */
  public Double getDepositionVelocity(final AeriusResultPoint point) {
    if (point.getPointType() == AeriusPointType.RECEPTOR) {
      return getDepositionVelocityById(point.getId());
    } else {
      return getDepositionVelocityByCoordinate(point.getX(), point.getY());
    }
  }

  /**
   * Finds the deposition velocity for the level 1 hexagon nearest the given coordinates.
   *
   * @param x
   *          The x coordinate.
   * @param y
   *          The y coordinate.
   * @return The deposition velocity, or null if they could not be found.
   */
  public Double getDepositionVelocityByCoordinate(final double x, final double y) {
    return getDepositionVelocityById(receptorUtil.getReceptorIdFromCoordinate(x, y, SRMConstants.ZOOM_LEVEL_1));
  }

  /**
   * Finds the deposition velocity for the level 1 hexagon with the given id.
   *
   * @param receptorId
   *          The level 1 receptor id.
   * @return The deposition velocity, or null if they could not be found.
   */
  public Double getDepositionVelocityById(final int receptorId) {
    final int id = receptorId - 1;
    final int dx = id % hexHor;
    final int dy = id / hexHor;

    final Double result;

    final boolean modX = dx % 2 == 0;
    switch (dy % HEXAGON_IN_LEVEL) {
    case 0:
      result = resultCase02(dx, dy, modX);
      break;
    case 1:
      result = resultCase13(dx, dy, modX);
      break;
    case 2:
      result = resultCase02(dx, dy, !modX);
      break;
    default: // 3
      result = resultCase13(dx, dy, !modX);
    }
    return result;
  }

  private Double resultCase02(final int dx, final int dy, final boolean modX) {
    final Double result;
    if (modX) {
      result = getLvl2Velocity(dx, dy);
    } else {
      result = getResult(dx, dy - 2, dx, dy + 2);
    }
    return result;
  }

  private Double resultCase13(final int dx, final int dy, final boolean modX) {
    final Double result;
    if (modX) {
      result = getResult(dx, dy - 1, dx + 1, dy + 1);
    } else {
      result = getResult(dx, dy + 1, dx + 1, dy - 1);
    }
    return result;
  }

  private Double getResult(final int dv1x, final int dv1y, final int dv2x, final int dv2y) {
    return (getLvl2Velocity(dv1x, dv1y) + getLvl2Velocity(dv2x, dv2y)) / 2.0;
  }

  private Double getLvl2Velocity(final int x, final int y) {
    final int id = x + y * hexHor + 1;

    return level2.get(id);
  }
}
