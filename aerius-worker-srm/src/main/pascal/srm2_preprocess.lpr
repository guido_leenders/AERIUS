{
 Copyright the State of the Netherlands

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see http://www.gnu.org/licenses/.
}
{%BuildCommand $TargetOS() $(CompPath) $(EdFile)}
program srm2_preprocess;

{$mode objfpc}{$H+}

uses
  math, SysUtils, eventlog, INIFiles,
  modeldefs, pre_srm;

type
  TOutputOptions = Record
    wind, con      : Boolean;
    outputFilename : ansistring;
  end;

type
  TInputData = Record
    yearPrognose  : integer;
    datapath      : ansistring;
    startX, endX,
    startY, endY  : integer;
    blockSize     : integer;
    startYear,
    endYear       : integer;
    outputOptions : TOutputOptions;
  end;

const
  KM_BLOCK = 1000;
  HALF_KM_BLOCK = 500;
  TAB = char(9);
  NR_WIND = 36;
  PRESRM = 'PRESRM';
  AERIUS = 'AERIUS';
  // project init
  USE_STABILITY = false;
  USER_ROUGHNESS = 0.08; // default srm2
  USTAR_LIMIT = -1;
  MONINOBU_LIMIT = -1;
  MULTI_STATION = False;
  DUBBELTELLINGSCORRECTIE = True;
  USE_COMP = [TComponent.O3, TComponent.NH3];

var
  EventLog1 : TEventLog;

{
  Returns pre_srm version as text string
}
function PreSRM_Version: ansistring;
begin
  PreSRM_Version := format('PreSRM version: %.3f', [PS_Version]);
end;

procedure Init_File(year: integer; outputFilename: ansistring;
    var froos: textfile);
begin
 AssignFile (froos, format(outputFilename, [year]));
 Rewrite(froos);
 {$i+}
end;

{
  Header of the file.
  Writes year in the first row, header of the table in the second row.
}
procedure Write_Header(year: integer; outputOptions: TOutputOptions;
    var froos: textfile);
var
  i : integer;
begin
  writeln(froos, PreSRM_Version);
  writeln(froos, format('year          : %d', [year]));
  write(froos, 'X', TAB, 'Y');
  if (outputOptions.wind) then
  begin
    for i := 1 to NR_WIND do
      write(froos, TAB, format('WF%2.2d', [i]));
    for i := 1 to NR_WIND do
      write(froos, TAB, format('WS%2.2d', [i]));
  end;
  if (outputOptions.con) then
  begin
    for i := 1 to NR_WIND do
      write(froos, TAB, format('C%2.2d', [i]));
    write(froos, TAB, 'Con_NH3');
  end;
  writeln(froos);
end;

{
  Ignore specific blocks that cause presrm to crash, while sea salt did return
  values.
}
function Valid_Block(midX, midY: integer): Boolean;
begin
  Valid_Block := true;
  if ((midX = 37500) and (midY = 437500))
    or ((midX = 87500) and (midY = 362500))
    or ((midX = 12500) and (midY = 412500)) then
    Valid_Block := false;
end;

{
 Checks if a block has valid values for all 1 km2 blocks in the block given.
 If for any 1 km2 block true is returned the total block is considered valid.

 This check is done to work around PreSRM crashing on initializing concentration
 for blocks that don't have any values for the whole block.
}
function Has_Valid_Data(proj: Pointer; midX, midY, width, height: integer): Boolean;
var
  x, y : integer;
  xx, yy : integer;
  startX, startY : integer;
  days : integer;
  conc: longInt;
begin
  Has_Valid_Data := false;
  startX := midX - (width div 2) + HALF_KM_BLOCK;
  startY := midY - (height div 2) + HALF_KM_BLOCK;
  for x := 0 to (width div KM_BLOCK) - 1 do
  begin
    for y := 0 to (height div KM_BLOCK) - 1 do
    begin
      xx := startX + x * KM_BLOCK;
      yy := startY + Y * KM_BLOCK;
      days := 0;
      conc := 0;
      Has_Valid_Data := PS_SeaSaltCorrection(proj, xx, yy, conc, days)
          or Has_Valid_Data;
    end;
  end;
end;

{
  Initializes the pre-srm project structure.
}
procedure Init_Project(year, yearPrognose, midX, midY, width, height: integer;
    datapath: ansistring; var proj, conc: Pointer);
var
  dummy     : Pointer;
  datadir   : ansistring;
  validData : boolean;
begin
  dummy := nil;
  datadir := ExtractFilePath(datapath);
  proj := PS_Proj_Init(year, midX, midY, width, height, pAnsiChar(datadir),
      year > yearPrognose);
  try
    // Check if block has valid data to avoid pre-srm from crashing on init
    // concentration.
    validData := Valid_Block(midX, midY)
      and Has_Valid_Data(proj, midX, midY, width, height);
    if validData then
    begin
     conc := PS_Conc_Init(proj, USE_COMP, -1, -1);
     EventLog1.log('Concentration initialized');
    end
    else
    begin
    EventLog1.log(format('No valid data, ignoring block:(%d, %d)',
        [midX, midY]));
    PS_Cleanup(proj, dummy, dummy, dummy);
    proj := nil;
    conc := nil;
    end;
  except
    PS_Cleanup(proj, dummy, conc, dummy);
    proj := nil;
    conc := nil;
  end;
end;

{
  Returns the wind factor for the given sector.
}
function Get_Wind_Sector(classdata: Pointer; sect: integer) : double;
var
  hpmteo      : array[1..3] of double;
  hpmteoTotal : double;
  wind        : integer;
  freq        : double;
begin
  freq := 0;
  for wind := 1 to 3 do
  begin
    PS_Class_Meteo(classdata, sect, wind, freq);
    hpmteo[wind] := freq;
  end;
  hpmteoTotal := hpmteo[1] + hpmteo[2] + hpmteo[3];
  if hpmteoTotal > 0 then
  begin
    Get_Wind_Sector := hpmteoTotal /
        (hpmteo[1] * 0.8 / 1.45 + hpmteo[2] * 1.0 / 4 + hpmteo[3] * 1.1 / 8);
    end
    else
    Get_Wind_Sector := 0;
end;

{
  Writes the data for a row.
  Columns are per sector: wind factor, wind speed, O3 concentration
  and the last column is the total NH3 concentration.
}
procedure Write_Line(var froos: textfile; proj, conc: Pointer;
    meteoX, meteoY: integer; outputOptions: TOutputOptions);
var
  sect          : integer;
  meteo,
  classdata     : Pointer;
  ws, wf, c_o3  : array[1..36] of double;
  dbc           : single;
  cncNH3, cncO3 : double;
  dummy         : Pointer;
begin
  dummy := nil;
  dbc := 0;
  cncO3 := PS_MeanConc(conc, TComponent.O3, meteoX, meteoY, dbc);
  if cncO3 > 0 then
  begin
    EventLog1.Log('km ' + format('x=%d, y=%d',[meteoX, meteoY]));
    // per 1x1 km vak meteo interpoleren en classdata opvragen
   meteo := PS_Meteo_Init(proj, USE_STABILITY, USER_ROUGHNESS, USTAR_LIMIT,
       MONINOBU_LIMIT, meteoX, meteoY, MULTI_STATION);
   try
     if meteo = nil then
     begin
      EventLog1.Log('meteo is nil!');
     end
     else
     begin
       classdata := PS_ClassData_Init(proj, meteo, conc, NR_WIND, NR_WIND,
           DUBBELTELLINGSCORRECTIE);
       PS_Class_SetCoord(classdata, meteoX, meteoY);
  
       for sect := 1 to NR_WIND do
       begin
         ws[sect] := Get_Wind_Sector(classdata, sect - 1);
         PS_Class_Conc(classdata, sect - 1, TComponent.O3, c_o3[sect], wf[sect]);
       end;
       write(froos, meteoX:0, TAB, meteoY:0, TAB);
       if (outputOptions.wind) then
       begin
         for sect := 1 to NR_WIND do
         begin
           write(froos, wf[sect]:0:5, TAB);
          end;
         for sect := 1 to NR_WIND do
         begin
           write(froos, ws[sect]:0:3, TAB);
         end;
       end;
       if (outputOptions.con) then
       begin
         for sect := 1 to NR_WIND do
         begin
           write(froos, c_o3[sect]:0:3, TAB);
         end;
         dbc := 0;
         cncNH3 :=
             Max(0, PS_MeanConc(conc, TComponent.NH3, meteoX, meteoY, dbc));
         write(froos, cncNH3:0:3);
       end;
       writeln(froos);
     end
     finally
      PS_Cleanup(dummy, meteo, dummy, classdata);
     end;
  end
  else
    EventLog1.Log('No valid gc:km ' + format('x=%d, y=%d',[meteoX, meteoY]));
end;

{
  Writes the lines for a block (for example with a 25 block size this would be
  all values within the 25km area per km area.)
}
procedure Write_Block(var froos: textfile; proj, conc: Pointer;
    startX, endX, startY, endY: integer; outputOptions: TOutputOptions);
var
  x, y : Integer;
begin
  for x := startX to endX - 1 do
  begin
    for y := startY to endY - 1 do
    begin
      Write_Line(froos, proj, conc, x * KM_BLOCK + HALF_KM_BLOCK,
          y * KM_BLOCK + HALF_KM_BLOCK, outputOptions);
    end;
  end;
end;

{
 Writes all blocks for a given year.
}
procedure Write_Blocks(year: integer; outputOptions: TOutputOptions;
    var froos: textfile; id: TInputData);
var
  bX, bY,
  bStartX, bEndX,
  bStartY, bEndY,
  bAStartX, bAEndX,
  bAStartY, bAEndY,
  bWidth, bHeight,
  bCenterX, bCenterY : integer;
  proj, conc         : Pointer;
  dummy              : Pointer;
begin
  dummy := nil;
  bStartX := id.startX div id.blockSize;
  bEndX := (id.endX - 1) div id.blockSize;
  bStartY := id.startY div id.blockSize;
  bEndY := (id.endY - 1) div id.blockSize;
  for bX := bStartX to bEndX do
  begin
    for bY := bStartY to bEndY do
    begin
      bAStartX := Max(bX * id.blockSize, id.startX);
      bAEndX := Min((bX + 1) * id.blockSize, id.endX);
      bAStartY := Max(bY * id.blockSize, id.startY);
      bAEndY := Min((bY + 1) * id.blockSize, id.endY);
      bWidth := (bAEndX - bAStartX) * KM_BLOCK;
      bHeight := (bAEndY - bAStartY) * KM_BLOCK;
      bCenterX := bAStartX * KM_BLOCK + bWidth div 2;
      bCenterY := bAStartY * KM_BLOCK + bHeight div 2;
      EventLog1.Log(
          format('Block:(%d, %d) center:(%d, %d), x:(%d - %d), Y:(%d - %d)',
          [bX, bY, bCenterX, bCenterY, bAStartX, bAEndX, bAStartY, bAEndY]));
      try
        try
          proj := nil;
          conc := nil;
          Init_Project(year, id.yearPrognose, bCenterX, bCenterY, bWidth,
              bHeight, id.datapath, proj, conc);
          if PS_Errorcode <> 0 then
          begin
            EventLog1.Log('Error in proj init, ignoring '
                + format('(%d, %d)', [bX, bY]));
            continue; // year failed
          end;
          if proj <> nil then
            Write_Block(froos, proj, conc, bAStartX, bAEndX, bAStartY, bAEndY,
                outputOptions);
        finally
          if conc <> nil then
            PS_Cleanup(dummy, dummy, conc, dummy);
          if proj <> nil then
            PS_Cleanup(proj, dummy, dummy, dummy);
        end
      except
        on e: Exception do
        EventLog1.Log(
           format('Block error!:(%d, %d) center:(%d, %d), x:(%d - %d), Y:(%d - %d) - %s',
          [bX, bY, bCenterX, bCenterY, bAStartX, bAEndX, bAStartY, bAEndY, e.Message]));
      end;
    end;
  end;
end;

{
 Writes the data for a give year.
}
procedure Write_Year(id: TInputData; year: integer;
    outputOptions: TOutputOptions);
var
  froos : textfile;
begin
  try
    Init_File(year, outputOptions.outputFilename, froos);
    Write_Header(year, outputOptions, froos);
    Write_Blocks(year, outputOptions, froos, id);
  finally
    closefile(froos);
  end;
end;

{
  Generates the files for all years specified in the input data.
}
procedure Run_Years(inputData : TInputData);
var
  year    : integer;
begin
  for year := inputData.startYear to inputData.endYear do
  begin
    EventLog1.Log(format('Year: %d', [year]));
    Write_Year(inputData, year, inputData.outputOptions);
  end;
end;

{
  Reads the custom values from the configuration .ini file.
}
function Read_Ini(iniFilename: ansistring): TInputData;
var
  iniFile   : TINIFile;
  inputData : TInputData;
begin
  iniFile := TINIFile.Create(iniFilename);
  inputData.yearPrognose := iniFile.ReadInteger(PRESRM, 'year_prognose', 0);
  inputData.datapath := iniFile.ReadString(AERIUS, 'DataPath', '');
  inputData.startYear := iniFile.ReadInteger(AERIUS, 'StartYear', -1);
  inputData.endYear := iniFile.ReadInteger(AERIUS, 'EndYear', -1);
  inputData.blockSize := iniFile.ReadInteger(AERIUS, 'BlockSize', 25);
  EventLog1.Log(format('BlockSize: %d', [inputData.blockSize]));
  inputData.startX := iniFile.ReadInteger(AERIUS, 'StartX', 6);
  inputData.endX := iniFile.ReadInteger(AERIUS, 'EndX', 278);
  inputData.startY := iniFile.ReadInteger(AERIUS, 'StartY', 306);
  inputData.endY := iniFile.ReadInteger(AERIUS, 'EndY', 620);

  inputData.outputOptions.outputFilename := iniFile.ReadString(AERIUS,
      'OutputFilename', 'roos_' + tcompnames[TComponent.O3]);
  inputData.outputOptions.wind := iniFile.ReadBool(AERIUS, 'WriteWind', True);
  inputData.outputOptions.con := iniFile.ReadBool(AERIUS, 'WriteCon', True);
  
  Read_Ini := inputData;
end;

{
  Logging.
}
procedure Logging;
begin
  EventLog1 := TEventLog.Create(nil);
  EventLog1.LogType := ltFile;
  EventLog1.FileName := 'srm_pp.log';
  EventLog1.Active := True;
  EventLog1.Log(PreSRM_Version);
end;

{
  Main.
}
procedure Main(iniFile: ansistring);
begin
  PS_HandleExceptions(True);
  Logging;
  Run_Years(Read_Ini(iniFile));
  EventLog1.Log('Finished');
end;

begin
  Main(ParamStr(1));
end.
