unit depac;

{

  Definition unit for the DEPAC DLL
  ECN Petten; Luchtkwaliteit en KlimaatVerandering
  R. Rodink (rodink@ecn.nl)
  A.T. Vermeulen (a.vermeulen@ecn.nl)

  Februari 2010

  Revisons:


}

interface

{
  procedure depac_gcn2010( var comp:integer;
                           var day_of_year,lat,t,ust,glrad,sinphi,rh:single;
                           var nwet,lu,iratns,month:integer;
                           var rc_tot:single);overload;stdcall;
    external 'depac.dll' name'_DEPAC_mp_DEPAC_GCN2010_A@52'


  procedure depac_gcn2010( var comp:integer;
                           var day_of_year,lat,t,ust,glrad,sinphi,rh:single;
                           var nwet,lu,iratns,month:integer;
                           var rc_tot,c_ave_prev, catm, ccomp_tot:single);overload;stdcall;
    external 'depac.dll' name'_DEPAC_mp_DEPAC_GCN2010_B@64'
}

  procedure depac_gcn2010( var comp,day_of_year:integer;
                           var lat,t,ust,glrad,sinphi,rh:single;
                           var nwet,lu,iratns,month:integer;
                           var rc_tot,c_ave_prev, catm, ccomp_tot,ra, rb, rc_eff:single);stdcall;
    external 'depac.dll' name'_DEPAC_mp_DEPAC_GCN2010_C@76'

implementation

end.
