/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

/**
 * Test class for load {@link LandUseData} with {@link LandUseDataFileReader}.
 */
public class LandUseDataFileReaderTest {
  @Test
  public void testReadFile() throws IOException {
    MapData mapData = null;

    try (final InputStream is = LandUseDataFileReaderTest.class.getResourceAsStream("z0nl1000-test.asc")) {
      mapData = LandUseDataFileReader.read(is);
    }

    assertNotNull("Map data should not be null", mapData);
    final int width = mapData.getWidth();
    assertEquals("Invalid datagrid height.", 2, mapData.getHeight());
    assertEquals("Invalid datagrid width.", 291, width);
    assertEquals("Invalid offset x.", 0, mapData.getOffsetX(), 0.1);
    assertEquals("Invalid offset y.", 300000, mapData.getOffsetY(), 0.1);
    assertEquals("Invalid diameter.", 1000, mapData.getDiameter(), 0.1);
    assertEquals("Invalid default value.", -1.0, mapData.getNoDataValue(), 0.0001);
    assertEquals("Invalid value for cell [290, 1].", 0.2231, getCell(mapData, width, 290, 1), 0.0001);
    assertEquals("Invalid value for cell [263, 0].", 0.0082, getCell(mapData, width, 263, 0), 0.0001);
  }

  @Test(expected = IOException.class)
  public void testUnexpectedEOF() throws IOException {
    MapData mapData = null;

    try (final InputStream is = LandUseDataFileReaderTest.class.getResourceAsStream("z0nl1000-test-unexpected-EOF.asc")) {
      mapData = LandUseDataFileReader.read(is);
    }

    assertNull("Expected exception.", mapData);
  }

  @Test(expected = IOException.class)
  public void testInvalidDataLine() throws IOException {
    MapData mapData = null;

    try (final InputStream is = LandUseDataFileReaderTest.class.getResourceAsStream("z0nl1000-test-invalid-data-line.asc")) {
      mapData = LandUseDataFileReader.read(is);
    }

    assertNull("Expected exception.", mapData);
  }

  // x and y are 0 based indices.
  private double getCell(final MapData mapData, final int width, final int x, final int y) {
    return mapData.getData().get(y * width + x);
  }
}
