/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategories;
import nl.overheid.aerius.shared.domain.sector.category.RoadSpeedType;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier.RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Test class for {@link LegacyNSLSegmentFileReader}.
 */
public class LegacyNSLSegmentFileReaderTest extends BaseDBTest {

  private static final String[] TEST_FILES = {"srm2_roads_example.csv", "line_at_first_column.csv", "geometry_3d.csv"};
  private static final int[] NUMBER_OF_RESULTS = {160, 3, 2};
  private static final String SPECIFIC_TEST_FILE = "specific_test_case.csv";
  private final LegacyNSLImportReader reader = new LegacyNSLImportReader();
  private SectorCategories categories;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    try (Connection con = BaseDBTest.getCalcPMF().getConnection()) {
      categories = SectorRepository.getSectorCategories(con, getCalcMessagesKey());
    }
  }

  @Test
  public void testReader() throws Exception {
    final Pattern start2DCoordinatePattern = Pattern.compile("\\(\\d*\\.?\\d* \\d*\\.?\\d*,");
    for (int i = 0; i < TEST_FILES.length; i++) {
      final String fileName = TEST_FILES[i];
      final File file = new File(getClass().getResource(fileName).getFile());
      try (InputStream inputStream = new FileInputStream(file)) {
        final ImportResult results = new ImportResult();
        reader.read(fileName, inputStream, categories, null, results);

        if (!results.getExceptions().isEmpty()) {
          throw results.getExceptions().get(0);
        }
        final List<EmissionSource> sources = ((SRM2NetworkEmissionSource) results.getSourceLists().get(0).get(0)).getEmissionSources();
        assertEquals("Count nr. of roads", NUMBER_OF_RESULTS[i], sources.size());

        for (final EmissionSource source : sources) {
          assertEquals("Road geom is line", WKTGeometry.TYPE.LINE, source.getGeometry().getType());
          assertTrue("Road length > 0", source.getGeometry().getMeasure() > 0);
          //for some reason, the 121th and 129th rows in the csv have no intensities at all...
          if (!"-1312707".equals(source.getLabel()) && !"-1312716".equals(source.getLabel())) {
            assertNotEquals("Emission for source " + source, 0.0, source.getEmission(new EmissionValueKey(2014, Substance.NOXNH3)));
            final String wkt = source.getGeometry().getWKT();
            assertTrue("Geometry should be a linestring but was " + wkt, wkt.contains("LINESTRING"));
            final Matcher matcher = start2DCoordinatePattern.matcher(wkt);
            assertTrue("Geometry should contain 2 coordinates " + wkt, matcher.find());
          }
        }
      }

    }
  }

  @Test
  public void testSpecificCase() throws Exception {
    final String fileName = SPECIFIC_TEST_FILE;
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult results = new ImportResult();
      reader.read(fileName, inputStream, categories, null, results);

      if (!results.getExceptions().isEmpty()) {
        throw results.getExceptions().get(0);
      }
      final List<EmissionSource> esl = ((SRM2NetworkEmissionSource) results.getSourceLists().get(0).get(0)).getEmissionSources();
      assertEquals("Count nr. of roads", 3, esl.size());
      for (final EmissionSource source : esl) {
        assertTrue("Should be instance of SRM2 source", source instanceof SRM2EmissionSource);
      }
      validateFirstSource((SRM2EmissionSource) esl.get(0));
      validateSecondSource((SRM2EmissionSource) esl.get(1));
      validateThirdSource((SRM2EmissionSource) esl.get(2));

    }
  }

  private void validateFirstSource(final SRM2EmissionSource source) {
    assertEquals("Label", "10101-101", source.getLabel());
    assertEquals("Sector ID", 3112, source.getSector().getSectorId());
    assertNull("Shouldn't have a right barrier", source.getBarrierRight());
    assertNotNull("Should have a left barrier", source.getBarrierLeft());
    validateBarrier(source.getBarrierLeft(), 5.0, 3.0);
    assertEquals("Elevation height", 0.0, source.getElevationHeight(), 1E-4);
    assertEquals("Elevation type", RoadElevation.NORMAL, source.getElevation());
    assertEquals("tunnel factor", 1.0, source.getTunnelFactor(), 1E-4);
    assertFalse("Freeway", source.isFreeway());
    assertEquals("Number of subsources", 4, source.getEmissionSubSources().size());
    validateSubSource(source.getEmissionSubSources().get(0), VehicleType.LIGHT_TRAFFIC, 5000, 0.1, 120, false, RoadType.NON_URBAN_ROAD);
    validateSubSource(source.getEmissionSubSources().get(1), VehicleType.NORMAL_FREIGHT, 200, 0.0, 100, false, RoadType.NON_URBAN_ROAD);
    validateSubSource(source.getEmissionSubSources().get(2), VehicleType.HEAVY_FREIGHT, 150, 0.0, 100, false, RoadType.NON_URBAN_ROAD);
    validateSubSource(source.getEmissionSubSources().get(3), VehicleType.AUTO_BUS, 75, 0.3, 100, false, RoadType.NON_URBAN_ROAD);
  }

  private void validateSecondSource(final SRM2EmissionSource source) {
    assertEquals("Label", "20202-202", source.getLabel());
    assertEquals("Sector ID", 3111, source.getSector().getSectorId());
    assertNotNull("Should have a right barrier", source.getBarrierRight());
    assertNull("Shouldn't have a left barrier", source.getBarrierLeft());
    validateBarrier(source.getBarrierRight(), 4.0, 6.0);
    assertEquals("Elevation height", 1.0, source.getElevationHeight(), 1E-4);
    assertEquals("Elevation type", RoadElevation.STEEP_DYKE, source.getElevation());
    assertEquals("tunnel factor", 1.2, source.getTunnelFactor(), 1E-4);
    assertTrue("Freeway", source.isFreeway());
    //5 sources, as the dynamic vehicle column is added as a 2nd light_traffic source with different speed.
    assertEquals("Number of subsources", 5, source.getEmissionSubSources().size());
    validateSubSource(source.getEmissionSubSources().get(0), VehicleType.LIGHT_TRAFFIC, 10000, 0.3, 100, true, RoadType.FREEWAY);
    //there is no strict enforcement for speed = 130. hence the false.
    validateSubSource(source.getEmissionSubSources().get(1), VehicleType.LIGHT_TRAFFIC, 2000, 0.0, 130, false, RoadType.FREEWAY);
    validateSubSource(source.getEmissionSubSources().get(2), VehicleType.NORMAL_FREIGHT, 300, 0.2, 100, true, RoadType.FREEWAY);
    validateSubSource(source.getEmissionSubSources().get(3), VehicleType.HEAVY_FREIGHT, 250, 0.0, 100, true, RoadType.FREEWAY);
    validateSubSource(source.getEmissionSubSources().get(4), VehicleType.AUTO_BUS, 175, 0.0, 100, true, RoadType.FREEWAY);
  }

  private void validateThirdSource(final SRM2EmissionSource source) {
    assertEquals("Label", "30303-303", source.getLabel());
    assertEquals("Sector ID", 3113, source.getSector().getSectorId());
    assertNull("Shouldn't have a right barrier", source.getBarrierRight());
    assertNull("Shouldn't have a left barrier", source.getBarrierLeft());
    assertEquals("Elevation height", -1.0, source.getElevationHeight(), 1E-4);
    assertEquals("Elevation type", RoadElevation.TUNNEL, source.getElevation());
    assertEquals("tunnel factor", 1.0, source.getTunnelFactor(), 1E-4);
    assertFalse("Freeway", source.isFreeway());
    assertEquals("Number of subsources", 4, source.getEmissionSubSources().size());
    validateSubSource(source.getEmissionSubSources().get(0), VehicleType.LIGHT_TRAFFIC, 5000, 0.0, 50, false, RoadType.URBAN_ROAD);
    validateSubSource(source.getEmissionSubSources().get(1), VehicleType.NORMAL_FREIGHT, 400, 0.0, 50, false, RoadType.URBAN_ROAD);
    validateSubSource(source.getEmissionSubSources().get(2), VehicleType.HEAVY_FREIGHT, 350, 0.0, 50, false, RoadType.URBAN_ROAD);
    validateSubSource(source.getEmissionSubSources().get(3), VehicleType.AUTO_BUS, 275, 0.0, 50, false, RoadType.URBAN_ROAD);
  }

  private void validateBarrier(final RoadSideBarrier barrier, final double expectedDistance, final double expectedHeight) {
    assertEquals("Barrier type", RoadSideBarrierType.SCREEN, barrier.getBarrierType());
    assertEquals("Barrier distance", expectedDistance, barrier.getDistance(), 1E-4);
    assertEquals("Barrier height", expectedHeight, barrier.getHeight(), 1E-4);
  }

  private void validateSubSource(final VehicleEmissions vehicleEmissions, final VehicleType vehicleType, final double expectedVehicles,
      final double expectedStagnationFraction, final int expectedMaxSpeed, final boolean expectedStrictEnforcement, final RoadType roadType) {
    assertEquals(vehicleEmissions + ": Nr of vehicles", expectedVehicles,
        vehicleEmissions.getTimeUnit().toUnit(vehicleEmissions.getVehiclesPerTimeUnit(), TimeUnit.DAY), 1E-4);
    assertTrue(vehicleEmissions + ": Should be standard emissions", vehicleEmissions instanceof VehicleStandardEmissions);
    final VehicleStandardEmissions standardEmissions = (VehicleStandardEmissions) vehicleEmissions;
    assertNotNull(vehicleEmissions + ": emission category", standardEmissions.getEmissionCategory());
    assertEquals(vehicleEmissions + ": category road type", roadType, standardEmissions.getEmissionCategory().getRoadType());
    assertEquals(vehicleEmissions + ": vehicle type", vehicleType, standardEmissions.getEmissionCategory().getVehicleType());
    if (roadType == RoadType.FREEWAY) {
      assertEquals(vehicleEmissions + ": max speed",
          expectedMaxSpeed, standardEmissions.getEmissionCategory().getMaximumSpeed());
      assertEquals(vehicleEmissions + ": strict enforcement",
          expectedStrictEnforcement, standardEmissions.getEmissionCategory().isStrictEnforcement());
    } else {
      assertEquals(vehicleEmissions + ": max speed", 0, standardEmissions.getEmissionCategory().getMaximumSpeed());
      assertFalse(vehicleEmissions + ": strict enforcement", standardEmissions.getEmissionCategory().isStrictEnforcement());
    }
    assertEquals(vehicleEmissions + ": stagnation factor", expectedStagnationFraction, standardEmissions.getStagnationFraction(), 1E-4);
  }

  @Test
  public void testIncorrectColumnHeader() throws Exception {
    final String fileName = "incorrect_column_header.csv";
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult results = new ImportResult();
      reader.read(fileName, inputStream, categories, null, results);

      if (results.getExceptions().size() == 1) {
        if (results.getExceptions().get(0) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(0);
          assertEquals("Reason for exception", Reason.SRM2_MISSING_COLUMN_HEADER, ae.getReason());
          assertEquals("Size of arguments", 1, ae.getArgs().length);
          assertEquals("Argument for exception SRM2_MISSING_COLUMN_HEADER", "SEGMENT_ID, GEOMET_WKT", ae.getArgs()[0]);
        } else {
          throw results.getExceptions().get(0);
        }
      } else {
        fail("Expected 1 exception, got " + results.getExceptions().size());
      }
    }
  }

  @Test
  public void testMissingWKT() throws Exception {
    final String fileName = "incorrect_wkt_value.csv";
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult results = new ImportResult();
      reader.read(fileName, inputStream, categories, null, results);

      if (results.getExceptions().size() == 2) {
        if (results.getExceptions().get(0) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(0);
          assertEquals("Reason for exception", Reason.SRM2_INCORRECT_EXPECTED_VALUE, ae.getReason());
          assertEquals("Size of arguments", 2, ae.getArgs().length);
          assertEquals("Argument for exception SRM2_INCORRECT_EXPECTED_VALUE count", "2", ae.getArgs()[0]);
          assertEquals("Argument for exception SRM2_INCORRECT_EXPECTED_VALUE", "GEOMET_WKT", ae.getArgs()[1]);
        } else {
          throw results.getExceptions().get(0);
        }
        if (results.getExceptions().get(1) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(1);
          assertEquals("Reason for exception", Reason.SRM2_INCORRECT_WKT_VALUE, ae.getReason());
          assertEquals("Size of arguments", 2, ae.getArgs().length);
          assertEquals("Argument for exception SRM2_INCORRECT_WKT_VALUE count", "3", ae.getArgs()[0]);
          assertEquals("Argument for exception SRM2_INCORRECT_WKT_VALUE", "POINT(130829 459381)", ae.getArgs()[1]);
        } else {
          throw results.getExceptions().get(1);
        }
      } else {
        fail("Expected 2 exceptions, got " + results.getExceptions().size());
      }
    }
  }

  @Test
  public void testParseError() throws Exception {
    final String fileName = "incorrect_parse_value.csv";
    final File file = new File(getClass().getResource(fileName).getFile());
    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult results = new ImportResult();
      reader.read(fileName, inputStream, categories, null, results);

      if (results.getExceptions().size() == 2) {
        if (results.getExceptions().get(0) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(0);
          assertEquals("Reason for exception", Reason.IO_EXCEPTION_NUMBER_FORMAT, ae.getReason());
          assertEquals("Size of arguments", 3, ae.getArgs().length);
          assertEquals("Argument for exception IO_EXCEPTION_NUMBER_FORMAT linenr", "2", ae.getArgs()[0]);
          assertEquals("Argument for exception IO_EXCEPTION_NUMBER_FORMAT column", "MAXSNELH_V", ae.getArgs()[1]);
          assertEquals("Argument for exception IO_EXCEPTION_NUMBER_FORMAT value", "geen_snelheid", ae.getArgs()[2]);
        } else {
          throw results.getExceptions().get(0);
        }
        if (results.getExceptions().get(1) instanceof AeriusException) {
          final AeriusException ae = results.getExceptions().get(1);
          assertEquals("Reason for exception", Reason.IO_EXCEPTION_NOT_ENOUGH_FIELDS, ae.getReason());
          assertEquals("Size of arguments", 1, ae.getArgs().length);
          assertEquals("Argument for exception IO_EXCEPTION_NOT_ENOUGH_FIELDS linenr", "4", ae.getArgs()[0]);
        } else {
          throw results.getExceptions().get(1);
        }
      } else {
        fail("Expected 3 exception, got " + results.getExceptions().size());
      }
    }
  }

  @Test
  public void testFindRoadCategory() throws SQLException {
    final RoadEmissionCategories cat = categories.getRoadEmissionCategories();
    assertEquals("130, !strict= 130", 130, cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 130, null).getMaximumSpeed());
    assertEquals("130, strict= 130 !strict", 130,
        cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 130, null).getMaximumSpeed());
    assertFalse("130, strict= 130 !strict",
        cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 130, null).isStrictEnforcement());
    assertEquals("90, !strict= 100", 100, cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 90, null).getMaximumSpeed());
    assertEquals("90, strict= 100", 100, cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 90, null).getMaximumSpeed());
    assertTrue("90, strict= 100 strict", cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 90, null).isStrictEnforcement());
    assertEquals("140, !strict= 130", 130, cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 140, null).getMaximumSpeed());
    assertFalse("140, !strict= 130 !strict",
        cat.findClosestCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, true, 140, null).isStrictEnforcement());
    assertEquals("Non urban road: 90 = 0, strict", 0,
        cat.findClosestCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 90, RoadSpeedType.E)
        .getMaximumSpeed());
    assertEquals("Urban road: speed type d", RoadSpeedType.D,
        cat.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 90, RoadSpeedType.D).getSpeedType());
    assertEquals("Urban road: speed type c", RoadSpeedType.C,
        cat.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, false, 90, RoadSpeedType.C).getSpeedType());
    assertEquals("Non urban road: 90 = 0, !strict", 0,
        cat.findClosestCategory(RoadType.NON_URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 90, RoadSpeedType.E)
        .getMaximumSpeed());
    assertEquals("Urban road: 90 = 0, !strict", 0, cat.findClosestCategory(RoadType.URBAN_ROAD, VehicleType.LIGHT_TRAFFIC, true, 90, RoadSpeedType.C)
        .getMaximumSpeed());
  }
}
