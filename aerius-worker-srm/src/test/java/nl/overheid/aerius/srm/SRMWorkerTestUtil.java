/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm;

import java.io.IOException;

import nl.overheid.aerius.shared.exception.InvalidInputException;
import nl.overheid.aerius.srm.config.SRMWorkerConfiguration;
import nl.overheid.aerius.srm.worker.SRMWorkerFactory;
import nl.overheid.aerius.taskmanager.client.WorkerHandler;
import nl.overheid.aerius.worker.PropertiesUtil;

/**
 * Util class to read background data.
 */
public class SRMWorkerTestUtil {

  private static final String UNITTEST_PROPERTIES_FILE = "unittest.properties";

  private SRMWorkerTestUtil() {
    // util class
  }

  public static WorkerHandler createWorkerHandler() throws IOException, InvalidInputException {
    final SRMWorkerFactory factory = new SRMWorkerFactory();
    final SRMWorkerConfiguration config = getTestSRMWorkerConfiguration(factory);

    return factory.createWorkerHandler(config, null);
  }

  public static SRMWorkerConfiguration getTestSRMWorkerConfiguration() throws InvalidInputException, IOException {
    return getTestSRMWorkerConfiguration(new SRMWorkerFactory());
  }

  public static SRMWorkerConfiguration getTestSRMWorkerConfiguration(final SRMWorkerFactory factory) throws InvalidInputException, IOException {
    final String fileName = SRMWorkerTestUtil.class.getResource(UNITTEST_PROPERTIES_FILE).getFile();
    final SRMWorkerConfiguration config = factory.createConfiguration(PropertiesUtil.getFromPropertyFile(fileName));

    config.validate();
    return config;
  }
}
