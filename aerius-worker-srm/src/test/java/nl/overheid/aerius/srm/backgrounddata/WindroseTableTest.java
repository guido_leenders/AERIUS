/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.geo.shared.RDNew;

/**
 * Test class for {@link WindRoseTable}.
 */
public class WindroseTableTest {

  @Test
  public void testGet() {
    final WindRoseTable<WindRoseSpeed> table = new WindRoseTable<WindRoseSpeed>(RDNew.SRID);
    final List<WindRoseSpeed> list = new ArrayList<>();
    list.add(GeometryUtil.create(10500, 10500));
    list.add(GeometryUtil.create(11500, 10500));
    list.add(GeometryUtil.create(12500, 10500));
    list.add(GeometryUtil.create(10500, 11500));
    list.add(GeometryUtil.create(11500, 11500));
    list.add(GeometryUtil.create(12500, 11500));
    list.add(GeometryUtil.create(10500, 12500));
    final WindRoseSpeed windrose = GeometryUtil.create(11500, 12500);
    list.add(windrose);
    windrose.setAvgWindSpeed(3, 3.33f);
    list.add(GeometryUtil.create(12500, 12500));
    table.addAll(list);

    assertSame("Find windrose left-below", windrose, table.get(11200, 12100));
    assertSame("Find windrose left-above", windrose, table.get(11700, 12100));
    assertSame("Find windrose right-below", windrose, table.get(11200, 12700));
    assertSame("Find windrose right-above", windrose, table.get(11700, 12700));
    assertEquals("Is it really the same, check value", 3.33f, table.get(11700, 12700).getAvgWindSpeed(3), 0.0001);
  }
}
