/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.backgrounddata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import nl.overheid.aerius.io.LineReaderResult;

/**
 * Test class for {@link WindRoseConcentrationReader}.
 */
public class WindRoseConcentrationReaderTest {

  private static final int DATA_VERSION = 2020;
  private static final String PRE_SRM_VERSION = "1.901";
  private static final String CON_2020_TXT = "1.603/con_2020.txt";

  @Test
  public void testReadFile() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, DATA_VERSION);
    final LineReaderResult<WindRoseConcentration> results = readResults(reader, CON_2020_TXT);
    assertEquals("# rows", 43570, results.getObjects().size());
    final int row = 0;
    assertEquals("O3 sector 35", 58.530, results.getObjects().get(row).getO3(35), 0.0001);
    assertEquals("NH3 background concentration", 2.637, results.getObjects().get(row).getBackgroundNH3(), 0.0001);
    assertEquals("X", 13500.0, results.getObjects().get(row).getX(), 0.0);
    assertEquals("Y", 370500.0, results.getObjects().get(row).getY(), 0.0);
  }

  @Test(expected = IOException.class)
  public void testReadFileIncorrectPreSrmVersion() throws IOException {
    try {
      final WindRoseConcentrationReader reader = new WindRoseConcentrationReader("0,999", DATA_VERSION);
      readResults(reader, CON_2020_TXT);
    } catch (final IOException e) {
      assertTrue("Expected exception for invalid presrm verion", e.getMessage().contains("version"));
      throw e;
    }
  }

  @Test(expected = IOException.class)
  public void testReadFileIncorrectYear() throws IOException {
    try {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, 2000);
    readResults(reader, CON_2020_TXT);
    } catch (final IOException e) {
      assertTrue("Expected exception for invalid presrm verion", e.getMessage().contains("year"));
      throw e;
    }
  }

  @Test(expected = NumberFormatException.class)
  public void testReadFileInvalidYear() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, DATA_VERSION);
    readResults(reader, "con_2020_invalid_year.txt");
  }

  @Test(expected = IOException.class)
  public void testReadFileInvalidHeader() throws IOException {
    final WindRoseConcentrationReader reader = new WindRoseConcentrationReader(PRE_SRM_VERSION, DATA_VERSION);
    readResults(reader, "con_2020_invalid_header.txt");
  }

  private LineReaderResult<WindRoseConcentration> readResults(final WindRoseConcentrationReader reader, final String filename) throws IOException {
    final LineReaderResult<WindRoseConcentration> results;
    try (final InputStream is = WindRoseSpeedReaderTest.class.getResourceAsStream(filename)) {
      results = reader.readObjects(is);
    }
    return results;
  }

}
