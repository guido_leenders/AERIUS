/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm.worker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.opencl.OpenCLTestUtil;
import nl.overheid.aerius.ops.importer.RcpImportReader;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.SectorCategories;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm.SRMWorkerTestUtil;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.srm2.conversion.EmissionSourceSRM2Converter;
import nl.overheid.aerius.srm2.domain.SRM2InputData;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;

/**
 * Test class for SRM2 calculation.
 */
public class SRM2WorkerTest extends BaseDBTest {

  private static final Logger LOG = LoggerFactory.getLogger(SRM2WorkerTest.class);

  private static final String TEST_SOURCES_RESOURCE = "srm2_reference_test_1_sources.csv";
  private static final String TEST_RECEPTORS_RESOURCE = "srm2_reference_test_1_receptors.rcp";
  private static final String TEST_RESULTS_RESOURCE = "srm2_reference_test_1_results.csv";
  private static final int ROUDING_SCALE = 7;

  private static final int YEAR = 2020;
  private static final Substance[] SUBSTANCES = new Substance[] {Substance.NOX, Substance.NO2, Substance.NH3};
  private static final EnumSet<EmissionResultKey> EMISSION_RESULT_KEYS = EnumSet.of(
      EmissionResultKey.NOX_DEPOSITION, EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NO2_CONCENTRATION,
      EmissionResultKey.NH3_DEPOSITION, EmissionResultKey.NH3_CONCENTRATION);

  private final LegacyNSLImportReader reader = new LegacyNSLImportReader();
  private EmissionSourceSRM2Converter converter;
  private SectorCategories sectorCategories;

  @Override
  @Before
  public void setUp() throws Exception {
    OpenCLTestUtil.assumeOpenCLAvailable();

    try (Connection con = BaseDBTest.getCalcPMF().getConnection()) {
      sectorCategories = SectorRepository.getSectorCategories(con, getCalcMessagesKey());
    }
    converter = new EmissionSourceSRM2Converter();
  }

  private List<SRM2RoadSegment> loadSources(final InputStream sourceInputStream, final String fileName) throws IOException, AeriusException {
    final ImportResult results = new ImportResult();
    reader.read(fileName, sourceInputStream, sectorCategories, null, results);

    final EmissionSourceList emissionSources = results.getSourceLists().get(0);
    final List<EmissionValueKey> emissionValueKeys = new ArrayList<>();

    for (final Substance substance : SUBSTANCES) {
      emissionValueKeys.add(new EmissionValueKey(YEAR, substance));
    }

    LOG.info("Loaded sources: {}", emissionSources.size());
    assertFalse("Sources list is empty for some reason", emissionSources.isEmpty());

    return converter.convert(emissionSources, emissionValueKeys);
  }

  private List<AeriusPoint> loadReceptors(final String filename, final InputStream receptorInputStream) throws IOException, AeriusException {
    final ImportResult receptorResult = new ImportResult();
    new RcpImportReader(false).read(filename, receptorInputStream, sectorCategories, null, receptorResult);
    final CalculationPointList receptors = receptorResult.getCalculationPoints();

    // Fix id's
    final ReceptorUtil ru = new ReceptorUtil(RECEPTOR_GRID_SETTINGS);
    for (final AeriusPoint cp : receptors) {
      ru.setReceptorIdFromPoint(cp);
    }

    LOG.info("Loaded {} receptors.", receptors.size());

    return receptors;
  }

  @Test
  public void testSRM2Worker() throws Throwable {
    List<SRM2RoadSegment> sources;

    try (final InputStream sourceInputStream = getClass().getResourceAsStream(TEST_SOURCES_RESOURCE)) {
      sources = loadSources(sourceInputStream, TEST_SOURCES_RESOURCE);
    }

    List<AeriusPoint> receptors;

    try (final InputStream receptorInputStream = getClass().getResourceAsStream(TEST_RECEPTORS_RESOURCE)) {
      receptors = loadReceptors(TEST_RECEPTORS_RESOURCE, receptorInputStream);
    }

    final SRM2InputData input= new SRM2InputData(Profile.PAS);
    input.setSubstances(Arrays.asList(SUBSTANCES));
    input.setEmissionResultKeys(EMISSION_RESULT_KEYS);
    input.setYear(YEAR);
    input.setEmissionSources(sources);
    input.setReceptors(receptors);

    final Serializable output = SRMWorkerTestUtil.createWorkerHandler().handleWorkLoad(input, null, null);
    assertNotNull("SRM2 worker returned 'null' result", output);
    if (output instanceof Throwable) {
      throw (Throwable) output;
    }
    assertTrue("Result not of expected type", output instanceof CalculationResult);

    final ArrayList<AeriusResultPoint> results = ((CalculationResult) output).getResults();

    assertEquals("Invalid number of results", input.getReceptors().size(), results.size());
    LOG.info("ID at <X, Y>: {} {} {} {} {}", EmissionResultKey.NOX_CONCENTRATION.name(), EmissionResultKey.NO2_CONCENTRATION.name(),
        EmissionResultKey.NOX_DEPOSITION.name(), EmissionResultKey.NH3_CONCENTRATION.name(), EmissionResultKey.NH3_DEPOSITION.name());

    int total = 0;
    int nill = 0;
    int nan = 0;
    int infinite = 0;
    int zero = 0;
    int weirdERK = 0;
    int ok = 0;

    final List<String> errorValues = new ArrayList<>();

    for (final AeriusResultPoint arp : results) {
      LOG.trace("{} at <{}, {}>: {} {} {} {} {}", arp.getId(), arp.getX(), arp.getY(), arp.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION),
          arp.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION), arp.getEmissionResult(EmissionResultKey.NOX_DEPOSITION),
          arp.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION), arp.getEmissionResult(EmissionResultKey.NH3_DEPOSITION));

      for (final Map.Entry<EmissionResultKey, Double> result : arp.getEmissionResults().entrySet()) {
        total++;

        if (result.getValue() == null) {
          nill++;
          addErrorValues(errorValues, arp, result);
        } else if (Double.isNaN(result.getValue())) {
          nan++;
          addErrorValues(errorValues, arp, result);
        } else if (Double.isInfinite(result.getValue())) {
          infinite++;
          addErrorValues(errorValues, arp, result);
        } else if (Math.abs(result.getValue()) < 0.00001) {
          zero++;
          addErrorValues(errorValues, arp, result);
        } else if (!EMISSION_RESULT_KEYS.contains(result.getKey())) {
          weirdERK++;
          addErrorValues(errorValues, arp, result);
        } else {
          ok++;
        }
      }
    }

    for (final String e : errorValues) {
      LOG.error(e);
    }

    final Path resultFile = writeResultsToFile(results);

    assertEquals("Found errors. Null: " + nill + ", NaN: " + nan + ", infinite: " + infinite + ", zero: " + zero + ", weirdERK: " + weirdERK
        + " Total: " + total, total, ok);

    assertEquals("Results (generated in " + resultFile.toString() + ")", getFileContent(TEST_RESULTS_RESOURCE), getFileContent(resultFile));

    Files.deleteIfExists(resultFile);
  }

  private Path writeResultsToFile(final ArrayList<AeriusResultPoint> results) throws IOException {
    final Path resultFile = Files.createTempFile("SRM2_test", ".csv");
    try (final BufferedWriter writer = Files.newBufferedWriter(resultFile, StandardCharsets.UTF_8)) {
      writer.append("id,x,y,WKT,concentration_nox,concentration_no2,deposition_nox,concentration_nh3,deposition_nh3");
      writer.newLine();
      for (final AeriusResultPoint arp : results) {
        final List<String> resultList = new ArrayList<>();
        resultList.add(String.valueOf(arp.getId()));
        resultList.add(String.valueOf(arp.getX()));
        resultList.add(String.valueOf(arp.getY()));
        resultList.add(arp.toWKT());
        resultList.add(getRoundedResult(arp.getEmissionResult(EmissionResultKey.NOX_CONCENTRATION)));
        resultList.add(getRoundedResult(arp.getEmissionResult(EmissionResultKey.NO2_CONCENTRATION)));
        resultList.add(getRoundedResult(arp.getEmissionResult(EmissionResultKey.NOX_DEPOSITION)));
        resultList.add(getRoundedResult(arp.getEmissionResult(EmissionResultKey.NH3_CONCENTRATION)));
        resultList.add(getRoundedResult(arp.getEmissionResult(EmissionResultKey.NH3_DEPOSITION)));
        writer.append(StringUtils.join(resultList, ","));
        writer.newLine();
      }
    }
    return resultFile;
  }

  /**
   * Scale results so it will return with the same precision as in the reference file.
   * @param result to scale
   * @return scaled result
   */
  private String getRoundedResult(final double result) {
    return BigDecimal.valueOf(result).setScale(ROUDING_SCALE + 1, RoundingMode.HALF_DOWN)
        .setScale(ROUDING_SCALE, RoundingMode.HALF_UP).toPlainString();
  }

  private void addErrorValues(final List<String> errorValues, final AeriusResultPoint arp,
      final Map.Entry<EmissionResultKey, Double> result) {
    errorValues.add("Id:" + arp.getId() + ", <" + arp.getX() + ", " + arp.getY() + ">, " + result.getKey() + ", " + result.getValue());
  }

  private String getFileContent(final String fileName) throws IOException {
    final Path file = new File(getClass().getResource(fileName).getFile()).toPath();
    return getFileContent(file);
  }

  private String getFileContent(final Path filePath) throws IOException {
    final List<String> lines = new ArrayList<>();
    try (final BufferedReader br = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)) {
      String line;

      while ((line = br.readLine()) != null) {
        lines.add(line + System.getProperty("line.separator"));
      }
    }
    Collections.sort(lines);
    return Arrays.toString(lines.toArray(new String[lines.size()]));
  }
}
