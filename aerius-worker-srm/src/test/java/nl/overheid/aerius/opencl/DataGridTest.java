/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.nio.DoubleBuffer;

import org.junit.Test;

import com.jogamp.common.nio.Buffers;

public class DataGridTest {
  @Test
  public void testConstructorOk() {
    final DataGrid test = new DataGrid(5, 6, Buffers.newDirectDoubleBuffer(30));

    assertNotNull("DataGrid not created.", test);
    assertEquals("Invalid width.", 5, test.getWidth());
    assertEquals("Invalid height.", 6, test.getHeight());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidWidth() {
    new DataGrid(-1, 6, Buffers.newDirectDoubleBuffer(30));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidHeight() {
    new DataGrid(5, -1, Buffers.newDirectDoubleBuffer(30));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testInvalidBufferSize() {
    new DataGrid(5, 6, Buffers.newDirectDoubleBuffer(25));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testBufferNotDirect() {
    new DataGrid(5, 6, DoubleBuffer.allocate(25));
  }
}
