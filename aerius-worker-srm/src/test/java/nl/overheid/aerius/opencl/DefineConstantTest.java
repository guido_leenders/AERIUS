/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DefineConstantTest {
  // Store the default locale to reset it after running the tests.
  private static Locale defaultLocale;

  @BeforeClass
  public static void setUpBeforeClass() {
    defaultLocale = Locale.getDefault();
    Locale.setDefault(Locale.GERMANY);
  }

  @AfterClass
  public static void tearDownAfterClass() {
    Locale.setDefault(defaultLocale);
  }

  @Test
  public void testConstructors() {
    assertEquals("Invalid boolean constructor.", "#define NAME 0", new DefineConstant("NAME", false).getValue());
    assertEquals("Invalid boolean constructor.", "#define NAME 1", new DefineConstant("NAME", true).getValue());
    assertEquals("Invalid int constructor.", "#define NAME 100", new DefineConstant("NAME", 100).getValue());
    assertEquals("Invalid int constructor.", "#define NAME -999", new DefineConstant("NAME", -999).getValue());
    assertEquals("Invalid int constructor.", "#define NAME 10000", new DefineConstant("NAME", 10000).getValue());
    assertEquals("Invalid int constructor.", "#define NAME -99999", new DefineConstant("NAME", -99999).getValue());
    assertEquals("Invalid double constructor.", "#define NAME 1.1", new DefineConstant("NAME", 1.1).getValue());
    assertEquals("Invalid double constructor.", "#define NAME -1.1", new DefineConstant("NAME", -1.1).getValue());
    assertEquals("Invalid double constructor.", "#define NAME 0.11", new DefineConstant("NAME", 0.11).getValue());
    assertEquals("Invalid double constructor.", "#define NAME -0.11", new DefineConstant("NAME", -0.11).getValue());
    assertEquals("Invalid double constructor.", "#define NAME 1.1E-2", new DefineConstant("NAME", 0.011).getValue());
    assertEquals("Invalid double constructor.", "#define NAME -1.1E-2", new DefineConstant("NAME", -0.011).getValue());
    assertEquals("Invalid double constructor.", "#define NAME 1.1E-7", new DefineConstant("NAME", 0.00000011).getValue());
    assertEquals("Invalid double constructor.", "#define NAME -1.1E-7", new DefineConstant("NAME", -0.00000011).getValue());
    assertEquals("Invalid double constructor.", "#define NAME 1.23456E5", new DefineConstant("NAME", 123456.0).getValue());
    assertEquals("Invalid double constructor.", "#define NAME -1.23456E5", new DefineConstant("NAME", -123456.0).getValue());
  }
}
