/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.opencl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.DoubleBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Before;
import org.junit.Test;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opencl.CLBuffer;

public class DefaultContextTest {
  @Before
  public void setUp() throws Exception {
    OpenCLTestUtil.assumeOpenCLAvailable();
  }

  @Test
  public void testInitClose() {
    final Context context = new DefaultContext(true, false);

    try {
      assertTrue("At least 1 connection should be available.", context.getPoolSize() > 0);
      assertEquals("No connection taken yet.", context.getPoolSize(), context.getCurrentPoolSize());
    } finally {
      context.close();
    }
  }

  @Test
  public void testGetConnection() {
    final Context context = new DefaultContext(true, false);
    Connection connection = null;

    try {
      connection = context.getConnection();
      assertNotNull("No connection received.", connection);
      assertEquals("One connection taken.", context.getPoolSize() - 1, context.getCurrentPoolSize());
    } finally {
      if (connection != null) {
        connection.close();
      }
      context.close();
    }
  }

  @Test
  public void testGetMultipleConnections() {
    final Context context = new DefaultContext(true, false);
    // Make certain there is some contention.
    final int threadCount = context.getPoolSize() * 10;

    final CountDownLatch startSignal = new CountDownLatch(1);

    final ReentrantLock counterLock = new ReentrantLock();
    final Condition counterMod = counterLock.newCondition();

    // Atomic integer used here because it is mutable, unlike the regular Integer class.
    // The object must be final for the anonymous runnable classes to access it.
    final AtomicInteger counter = new AtomicInteger();

    for (int i = 0; i < threadCount; i++) {
      final Thread t = new Thread(new Runnable() {
        @Override
        public void run() {
          try {
            startSignal.await();
          } catch (final InterruptedException e) {
            // Should not happen
          }

          final Connection connection = context.getConnection();

          try {
            Thread.sleep(100);
          } catch (final InterruptedException e) {
            // Should not happen
          }

          connection.close();

          counterLock.lock();

          try {
            counter.incrementAndGet();
            counterMod.signalAll();
          } finally {
            counterLock.unlock();
          }
        }
      });

      startSignal.countDown();

      t.setName("t" + i);
      t.start();
    }

    // 100 ms waiting per thread + 10% margin, converted to nanoseconds
    long timeout = threadCount * 110000000;

    counterLock.lock();

    try {
      while (counter.get() < threadCount) {
        try {
          timeout = counterMod.awaitNanos(timeout);

          if (timeout <= 0) {
            break;
          }
        } catch (final InterruptedException e) {
          // Should not happen
        }
      }

      assertEquals("Not all connections closed.", threadCount, counter.get());
    } finally {
      counterLock.unlock();
      context.close();
    }
  }

  @Test
  public void testCloseConnectionWait() throws InterruptedException {
    final Context context = new DefaultContext(true, false);
    final Connection connection = context.getConnection();

    final AtomicBoolean closeDone = new AtomicBoolean();

    final Thread closeThread = new Thread(new Runnable() {
      @Override
      public void run() {
        context.close();
        closeDone.set(true);
      }
    });

    closeThread.start();

    while (!context.isClosed()) {
      try {
        Thread.sleep(10);
      } catch (final InterruptedException e) {
        e.printStackTrace();
      }
    }

    try {
      assertFalse("Context already closed!", closeDone.get());
    } finally {
      connection.close();

      closeThread.join(1000);
      assertTrue("Context not closed!", closeDone.get());
    }
  }

  @Test(expected = IllegalStateException.class)
  public void testConnectionRefusalAfterClose() {
    final Context context = new DefaultContext(true, false);
    context.close();

    context.getConnection();
  }

  @Test
  public void testBufferReleaseOnConnectionClose() {
    final Context context = new DefaultContext(true, false);

    try {
      final Connection connection = context.getConnection();

      final CLBuffer<DoubleBuffer> buffer1 = connection.createBuffer(1, DoubleBuffer.class, MemoryUsage.READ_ONLY);
      final CLBuffer<DoubleBuffer> buffer2 = connection.createCLBuffer(Buffers.newDirectDoubleBuffer(1), MemoryUsage.READ_ONLY);
      final CLBuffer<DoubleBuffer> buffer3 = connection.createBuffer(1, DoubleBuffer.class, MemoryUsage.READ_ONLY);
      buffer3.release();

      assertTrue("Buffer not released.", buffer3.isReleased());

      connection.close();

      assertTrue("New buffer not released.", buffer1.isReleased());
      assertTrue("Wrapped buffer not released.", buffer2.isReleased());
      assertTrue("Already released buffer not released.", buffer3.isReleased());
    } finally {
      context.close();
    }
  }
}
