/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.srm2.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.common.sector.SectorRepository;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.Profile;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.category.RoadEmissionCategory;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier.RoadSideBarrierType;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSourceLinearReference;
import nl.overheid.aerius.shared.domain.source.TimeUnit;
import nl.overheid.aerius.shared.domain.source.VehicleEmissions;
import nl.overheid.aerius.shared.domain.source.VehicleStandardEmissions;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.srm.io.LegacyNSLImportReader;
import nl.overheid.aerius.srm2.domain.SRM2InputData;
import nl.overheid.aerius.srm2.domain.SRM2RoadSegment;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link EmissionSourceSRM2Converter}.
 */
public class EmissionSourceSRM2ConverterTest extends BaseDBTest {

  private static final String SRM2_ROADS_EXAMPLE_CSV = "../../srm/io/srm2_roads_example.csv";
  private static final String SRM2_ROADS_EXAMPLE_WARNINGS_CSV = "srm2_roads_example_warnings.csv";

  private final EmissionValueKey NOX_KEY = new EmissionValueKey(2020, Substance.NOX);
  private final EmissionValueKey PM10_KEY = new EmissionValueKey(2020, Substance.PM10);
  private final EmissionSourceSRM2Converter converter = new EmissionSourceSRM2Converter();
  private final LegacyNSLImportReader reader = new LegacyNSLImportReader();

  @Test
  public void testConvert1() throws AeriusException {
    final SRM2EmissionSource ev = new SRM2EmissionSource();
    ev.setGeometry(new WKTGeometry(
        "LINESTRING(135363  455540, 135414 455621, 135612 455738, 135904 455231, 135501 454643, 135209 455083, 135118 455144)", 2261));
    addVehicle(ev);

    final List<SRM2RoadSegment> segments = converter.convert(ev, NOX_KEY.hatch());
    assertEquals("#segments check", 6, segments.size());
    assertEquals("Emission", 200 * 3.0 / (24 * 60 * 60 * 1000), segments.get(0).getEmission(Substance.NOX), 0.1E-7);
  }

  @Test
  public void testConvert2() throws AeriusException {
    final SRM2EmissionSource ev = new SRM2EmissionSource();
    ev.setGeometry(new WKTGeometry("LINESTRING(135363 455540,135414 455621)"));
    addVehicle(ev);

    final List<SRM2RoadSegment> segments = converter.convert(ev, NOX_KEY.hatch());
    assertEquals("#segments check", 1, segments.size());
  }

  @Test
  public void testConvert3() throws SQLException, IOException, AeriusException {
    final File file = new File(getClass().getResource(SRM2_ROADS_EXAMPLE_CSV).getFile());

    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult results = new ImportResult();
      reader.read(SRM2_ROADS_EXAMPLE_CSV, inputStream,
          SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale()), null, results);

      assertEquals("Warning list should only contain deprecation warning", 1, results.getWarnings().size());
      final List<SRM2RoadSegment> roadSegments = converter.convert(results.getSourceLists().get(0), NOX_KEY.hatch());

      for (final SRM2RoadSegment segment : roadSegments) {
        assertNotEquals("Segment " + segment, 0.0, segment.getEmission(Substance.NOX));
      }
    }
  }

  @Test
  public void testConvertGenericEmissionSource() throws AeriusException {
    final GenericEmissionSource ev = new GenericEmissionSource();
    ev.setGeometry(new WKTGeometry(
        "LINESTRING(135363  455540, 135414 455621, 135612 455738, 135904 455231, 135501 454643, 135209 455083, 135118 455144)", 2261));
    ev.setEmission(NOX_KEY, 600.0 * 365 / (1000 * 1000));

    final List<SRM2RoadSegment> segments = converter.convert(ev, NOX_KEY.hatch());
    assertEquals("#segments check", 6, segments.size());
    assertEquals("Emission", (200 * 3.0 / (24 * 60 * 60 * 1000)) / 2261, segments.get(0).getEmission(Substance.NOX), 0.1E-7);
  }

  @Test
  public void testConvertWarings() throws SQLException, IOException, AeriusException {
    final File file = new File(getClass().getResource(SRM2_ROADS_EXAMPLE_WARNINGS_CSV).getFile());

    try (InputStream inputStream = new FileInputStream(file)) {
      final ImportResult results = new ImportResult();
      reader.read(SRM2_ROADS_EXAMPLE_CSV, inputStream,
          SectorRepository.getSectorCategories(getCalcPMF(), LocaleUtils.getDefaultLocale()), null, results);

      final List<SRM2RoadSegment> roadSegments = converter.convert(results.getSourceLists().get(0), NOX_KEY.hatch());
      assertFalse("Warning list should not be empty", results.getWarnings().isEmpty());
      assertEquals("Warning list length should be 3", 3, results.getWarnings().size());

      for (final AeriusException warning : results.getWarnings()) {
        if (warning.getReason() != Reason.SRM2_FILESUPPORT_WILL_BE_REMOVED) {
          assertEquals("Warning should be " + Reason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO, Reason.SRM2_SOURCE_TUNNEL_FACTOR_ZERO, warning.getReason());
        }
      }
      for (final SRM2RoadSegment segment : roadSegments) {
        assertEquals("Segment " + segment, Double.doubleToLongBits(0.0), Double.doubleToLongBits(segment.getEmission(Substance.NOX)));
      }
    }
  }

  @Test(expected = AeriusException.class)
  public void testInvalidGeometry() throws AeriusException {
    final SRM2EmissionSource ev = new SRM2EmissionSource();
    final WKTGeometry geometry = new WKTGeometry("POINT(135363  455540)");
    addVehicle(ev);

    try {
      final List<SRM2RoadSegment> segments = converter.convert(geometry, ev, NOX_KEY.hatch());
      fail("Unexpected exception shoud be AeriusException.Reason.ROAD_GEOMETRY_NOT_ALLOWED");

    } catch (final AeriusException e) {
      assertEquals("Unexpected reason.", AeriusException.Reason.ROAD_GEOMETRY_NOT_ALLOWED, e.getReason());
      throw e;
    }
  }

  @Test
  public void testToSRM2Input() throws AeriusException {
    final ArrayList<EmissionValueKey> keys = new ArrayList<>();

    keys.add(NOX_KEY);
    keys.add(PM10_KEY);

    final ArrayList<Substance> substances = new ArrayList<>();
    substances.add(Substance.NOX);
    substances.add(Substance.PM10);
    final EnumSet<EmissionResultKey> emissionResultKeys = EnumSet.noneOf(EmissionResultKey.class);
    emissionResultKeys.add(EmissionResultKey.NOX_DEPOSITION);
    emissionResultKeys.add(EmissionResultKey.PM10_CONCENTRATION);

    final ArrayList<EmissionSource> emissionSources = createSources();
    final List<AeriusPoint> receptors = new ArrayList<>();
    receptors.add(new AeriusPoint(100, AeriusPointType.POINT, 200, 1));
    receptors.add(new AeriusPoint(300, AeriusPointType.POINT, 400, 2));

    final SRM2InputData data = new SRM2InputData(Profile.PAS);
    data.setSubstances(substances);
    data.setEmissionResultKeys(emissionResultKeys);
    data.setEmissionSources(converter.convert(emissionSources, keys));
    data.setReceptors(receptors);

    assertEquals("#keys", 2, data.getEmissionSources().iterator().next().getEmissions().size());
    assertEquals("#segments", 2, data.getEmissionSources().size());
    assertEquals("#receptors", 2, data.getReceptors().size());
  }

  @Test
  public void testToSRM2RoadSegments() throws AeriusException {
    final List<EmissionValueKey> keys = new ArrayList<>();

    keys.add(NOX_KEY);
    keys.add(PM10_KEY);
    final List<EmissionSource> emissionSources = createSources();
    final List<SRM2RoadSegment> segments = converter.convert(emissionSources, keys);

    assertEquals("#keys", 2, segments.get(0).getEmissions().size());
    assertEquals("#segments", 2, segments.size());
  }

  @Test
  public void testSRM2DynamicSegments() throws AeriusException {
    final ArrayList<EmissionSource> emissionSources = new ArrayList<>();
    final List<EmissionValueKey> keys = new ArrayList<>();
    keys.add(NOX_KEY);

    final SRM2EmissionSource es = new SRM2EmissionSource();
    es.setGeometry(new WKTGeometry("LINESTRING(1000 1200, 1100 1100, 2000 2000)"));
    es.setFreeway(true);
    addVehicle(es);

    final ArrayList<SRM2EmissionSourceLinearReference> dynamicSegments = new ArrayList<>();

    final SRM2EmissionSourceLinearReference heightSegment = new SRM2EmissionSourceLinearReference(0, 0.25);
    heightSegment.setElevationHeight(50);
    dynamicSegments.add(heightSegment);

    final SRM2EmissionSourceLinearReference leftBarrierSegment = new SRM2EmissionSourceLinearReference(0.25, 0.75);
    final RoadSideBarrier leftBarrier = new RoadSideBarrier();
    leftBarrier.setBarrierType(RoadSideBarrierType.SCREEN);
    leftBarrier.setHeight(3);
    leftBarrier.setDistance(2);
    leftBarrierSegment.setBarrierLeft(leftBarrier);
    dynamicSegments.add(leftBarrierSegment);

    final SRM2EmissionSourceLinearReference rightBarrierSegment = new SRM2EmissionSourceLinearReference(0.5, 1);
    final RoadSideBarrier rightBarrier = new RoadSideBarrier();
    rightBarrier.setBarrierType(RoadSideBarrierType.WALL);
    rightBarrier.setHeight(4);
    rightBarrier.setDistance(3);
    rightBarrierSegment.setBarrierRight(rightBarrier);
    dynamicSegments.add(rightBarrierSegment);

    final SRM2EmissionSourceLinearReference nonFreewaySegment = new SRM2EmissionSourceLinearReference(0.75, 1);
    nonFreewaySegment.setFreeway(false);
    nonFreewaySegment.setMaxRoadSpeed(80);
    dynamicSegments.add(nonFreewaySegment);

    es.setDynamicSegments(dynamicSegments);
    emissionSources.add(es);

    final List<SRM2RoadSegment> segments = converter.convert(emissionSources, keys);

    assertEquals("Invalid number of segments.", 5, segments.size());

    assertEquals("Invalid X coordinate segment 0 start.", 1000, segments.get(0).getStartX(), 0.001);
    assertEquals("Invalid Y coordinate segment 0 start.", 1200, segments.get(0).getStartY(), 0.001);
    assertEquals("Invalid X coordinate segment 0 end.", 1100, segments.get(0).getEndX(), 0.001);
    assertEquals("Invalid Y coordinate segment 0 end.", 1100, segments.get(0).getEndY(), 0.001);
    assertEquals("Invalid elevation height segment 0.", 12, segments.get(0).getElevationHeight());
    assertEquals("Invalid sigma0 segment 0.", 3.0, segments.get(0).getSigma0(), 0.001);

    assertEquals("Invalid X coordinate segment 1 start.", 1100, segments.get(1).getStartX(), 0.001);
    assertEquals("Invalid Y coordinate segment 1 start.", 1100, segments.get(1).getStartY(), 0.001);
    assertEquals("Invalid X coordinate segment 1 end.", 1250, segments.get(1).getEndX(), 0.001);
    assertEquals("Invalid Y coordinate segment 1 end.", 1250, segments.get(1).getEndY(), 0.001);
    assertEquals("Invalid elevation height segment 1.", 12, segments.get(1).getElevationHeight());
    assertEquals("Invalid sigma0 segment 1.", 3.0, segments.get(1).getSigma0(), 0.001);

    assertEquals("Invalid X coordinate segment 2 start.", 1250, segments.get(2).getStartX(), 0.001);
    assertEquals("Invalid Y coordinate segment 2 start.", 1250, segments.get(2).getStartY(), 0.001);
    assertEquals("Invalid X coordinate segment 2 end.", 1500, segments.get(2).getEndX(), 0.001);
    assertEquals("Invalid Y coordinate segment 2 end.", 1500, segments.get(2).getEndY(), 0.001);
    assertEquals("Invalid elevation height segment 2.", 0, segments.get(2).getElevationHeight());
    assertEquals("Invalid sigma0 segment 2.", 4.5, segments.get(2).getSigma0(), 0.001);

    assertEquals("Invalid X coordinate segment 3 start.", 1500, segments.get(3).getStartX(), 0.001);
    assertEquals("Invalid Y coordinate segment 3 start.", 1500, segments.get(3).getStartY(), 0.001);
    assertEquals("Invalid X coordinate segment 3 end.", 1750, segments.get(3).getEndX(), 0.001);
    assertEquals("Invalid Y coordinate segment 3 end.", 1750, segments.get(3).getEndY(), 0.001);
    assertEquals("Invalid elevation height segment 3.", 0, segments.get(3).getElevationHeight());
    assertEquals("Invalid sigma0 segment 3.", 5.5, segments.get(3).getSigma0(), 0.001);

    assertEquals("Invalid X coordinate segment 4 start.", 1750, segments.get(4).getStartX(), 0.001);
    assertEquals("Invalid Y coordinate segment 4 start.", 1750, segments.get(4).getStartY(), 0.001);
    assertEquals("Invalid X coordinate segment 4 end.", 2000, segments.get(4).getEndX(), 0.001);
    assertEquals("Invalid Y coordinate segment 4 end.", 2000, segments.get(4).getEndY(), 0.001);
    assertEquals("Invalid elevation height segment 4.", 0, segments.get(4).getElevationHeight());
    assertEquals("Invalid sigma0 segment 4.", 3.5, segments.get(4).getSigma0(), 0.001);
  }

  private ArrayList<EmissionSource> createSources() {
    final ArrayList<EmissionSource> emissionSources = new ArrayList<>();
    final SRM2EmissionSource es = new SRM2EmissionSource();
    es.setGeometry(new WKTGeometry("LINESTRING(100 200, 300 400, 500 600)"));
    final ArrayList<VehicleEmissions> traffic = es.getEmissionSubSources();
    final VehicleStandardEmissions tr = new VehicleStandardEmissions();
    tr.setStagnationFraction(1.0);
    final RoadEmissionCategory ec = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 120, null);
    ec.setEmissionFactor(NOX_KEY, 10);
    ec.setStagnatedEmissionFactor(NOX_KEY, 0.3);
    ec.setEmissionFactor(PM10_KEY, 10);
    ec.setStagnatedEmissionFactor(PM10_KEY, 0.3);
    tr.setEmissionCategory(ec);
    tr.setVehicles(200, TimeUnit.DAY);
    traffic.add(tr);
    emissionSources.add(es);
    return emissionSources;
  }

  private void addVehicle(final SRM2EmissionSource ev) {
    final ArrayList<VehicleEmissions> traffic = ev.getEmissionSubSources();
    final VehicleStandardEmissions lt = new VehicleStandardEmissions();

    lt.setVehicles(200, TimeUnit.DAY);
    final RoadEmissionCategory rec = new RoadEmissionCategory(RoadType.FREEWAY, VehicleType.LIGHT_TRAFFIC, false, 100, null);
    rec.setEmissionFactor(NOX_KEY, 3.0);
    lt.setEmissionCategory(rec);
    traffic.add(lt);
  }
}
