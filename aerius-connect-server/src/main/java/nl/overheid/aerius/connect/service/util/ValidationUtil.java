/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import com.google.gwt.regexp.shared.RegExp;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.service.util.CalculationOptionsValidator.CalculationOptionsLimits;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.context.AERIUS2Context;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Validation util class to check user input values.
 */
public final class ValidationUtil {

  private static final RegExp EMAIL_REG_EXP = RegExp.compile(SharedConstants.VALID_EMAIL_ADDRESS_REGEX, "i");

  private ValidationUtil() {
    // util class
  }

  /**
   * Validates the email address.
   * @param emailAddress email address
   * @param locale the locale to be used in case of an error
   * @throws AeriusException throws exception in case of validation errors
   */
  public static void email(final String emailAddress, final Locale locale) throws AeriusException {
    if (emailAddress == null || emailAddress.length() == 0 || EMAIL_REG_EXP.exec(emailAddress.trim()) == null) {
      throw new AeriusException(Reason.CONNECT_NO_VALID_EMAIL_SUPPLIED, emailAddress);
    }
  }

  /**
   * Validate the user given input options.
   * @param options calculation options
   * @param user User bound to the submitted API-key.
   * @param locale for the produced feedback.
   * @param pmf database connection manager
   * @param locale locale to use in error messages
   *
   * @throws AeriusException throws exception in case of validation errors
   * @throws SQLException database error
   */
  public static void validateInput(final Connection con, final CalculationOptions options, final AERIUS2Context context,
      ScenarioUser user, Locale locale) throws AeriusException, SQLException {
    final CalculationOptionsValidator validator = new CalculationOptionsValidator(new CalculationOptionsLimits(context), con);
    validator.validate(options, user, locale);
  }

  /**
   * Check whether generating API keys is allowed.
   *
   * @param con Database connection.
   * @throws AeriusException throws exception if it's disabled.
   * @throws SQLException
   */
  public static void userRegistrationsAllowed(final Connection con) throws AeriusException, SQLException {
    if (!ConstantRepository.getBoolean(con, ConstantsEnum.CONNECT_GENERATING_APIKEY_ENABLED)) {
      throw new AeriusException(Reason.USER_API_KEY_GENERATION_DISABLED);
    }
  }

  public static void validatePriorityProjectUtilisationReferenceAllowed(final Connection con, final String apiKey, final String projectReference)
      throws SQLException, AeriusException {
    if (!ScenarioUserRepository.priorityProjectUtilisationExportAllowed(con, apiKey, projectReference)) {
      throw new AeriusException(Reason.USER_PRIORITY_PROJECT_UTILISATION_EXPORT_NOT_ALLOWED);
    }
  }

  /**
   * Validate if provided jobKey exists for user.
   * @param con database connection
   * @param user object
   * @param jobKey specified jobKey
   * @throws SQLException database error.
   * @throws AeriusException used to throw that jobKey does not exists
   */
  public static void validateJobKey(final Connection con, final ScenarioUser user, final String jobKey) throws SQLException, AeriusException {
    if (!JobRepository.isJobFromUser(con, user, jobKey)) {
      throw new AeriusException(Reason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, jobKey);
    }
  }
}
