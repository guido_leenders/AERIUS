/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.valueperhexagon;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.conversion.ScenarioInputReader;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.ScenarioInput;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.connect.service.util.DataObjectUtil;
import nl.overheid.aerius.connect.service.util.ValidationMessageUtil;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.LocaleUtils;

public class ValuePerHexagonHelper {

  private static final Logger LOG = LoggerFactory.getLogger(ValuePerHexagonHelper.class);

  private static final EmissionResultKey ERK = EmissionResultKey.NOXNH3_DEPOSITION;
  private static final int BYTES_PER_VALIDATION_THRESHOLD_VALUE = 1024;

  private final ValuePerHexagonImplementation implementation;

  public ValuePerHexagonHelper(final ValuePerHexagonImplementation implementation) {
    this.implementation = implementation;
  }

  public ConvertResponse valuePerHexagon(final ConnectServiceContext context, final List<DataObject> dataObjects)
      throws AeriusException, IOException, SQLException {
    final Map<Integer, AeriusResultPoint> resultMap = new LinkedHashMap<>();

    final ScenarioInput leadingImport = processDataObjects(context, dataObjects, resultMap);
    if (leadingImport == null) {
      throw new IllegalArgumentException("No files given to merge.");
    } else if (leadingImport.getSuccessful()) {
      return buildResult(context, DataObjectUtil.determineOutputDataType(dataObjects), resultMap.values(), leadingImport.getImportResult(),
          leadingImport.getWarnings());
      // If a file is bad, do let the user know
    } else {
      final ConvertResponse response = new ConvertResponse();
      response
          .errors(leadingImport.getErrors())
          .warnings(leadingImport.getWarnings())
          .successful(Boolean.FALSE);

      return response;
    }
  }

  private ScenarioInput processDataObjects(final ConnectServiceContext context, final List<DataObject> dataObjects,
      final Map<Integer, AeriusResultPoint> resultMap) throws AeriusException, SQLException, IOException {
    final int thresholdSize = BYTES_PER_VALIDATION_THRESHOLD_VALUE
        * ConstantRepository.getInteger(context.getPMF().getConnection(), ConstantsEnum.CONNECT_UTIL_VALIDATION_THRESHOLD_KB);
    final ScenarioInputReader scenarioInputReader = new ScenarioInputReader(context);
    ScenarioInput leadingImport = null;
    boolean validationSkipped = false;
    for (int i = 1; i <= dataObjects.size(); i++) {
      // Decrement to get proper index; for-loop index is human readable index
      final DataObject dataObject = dataObjects.get(i - 1);

      LOG.debug("Parsing file {} with dataType '{}' and contentType '{}'", i, dataObject.getDataType(), dataObject.getContentType());

      final int dataBytes = dataObject.getData().getBytes().length;

      final boolean validate = dataBytes < thresholdSize;
      if (!validate) {
        // keep track if for any file the validation is skipped
        validationSkipped = true;
      }
      LOG.debug("Validation for file {} will be {}. Filesize is {}, threshold value {}", i, validate ? "executed" : "skipped",
          dataBytes, thresholdSize);

      final ScenarioInput scenarioInput = scenarioInputReader.run(dataObject.getDataType(), dataObject.getContentType(), dataObject.getData(),
          false, true, false, false, null, validate);
      // If the GML is bad, return the bad entry so it can be used to report it to the user
      if (!scenarioInput.getSuccessful()) {
        return scenarioInput;
      }

      LOG.debug("Merging results of file {}", i);
      final ImportResult result = scenarioInput.getImportResult();
      if (!processResult(i, resultMap, result)) {
        throw new AeriusException(Reason.IMPORT_NO_RESULTS_PRESENT, result.getMetaData().getReference());
      }

      if (leadingImport == null) {
        leadingImport = scenarioInput;
      }
    }
    if (validationSkipped) {
      leadingImport.addWarningsItem(ValidationMessageUtil.create(Reason.CONNECT_VALIDATION_SKIPPED_WARNING, LocaleUtils.getDefaultLocale()));
    }
    return leadingImport;
  }

  private ConvertResponse buildResult(final ConnectServiceContext context, final DataType dataType,
      final Collection<AeriusResultPoint> resultList, final ImportResult leadingImport, final List<ValidationMessage> warnings)
      throws IOException, AeriusException, SQLException {
    final ArrayList<File> generatedFiles = new ArrayList<>();
    //ensure we get a CalculatedSingle
    final int sourceListId = leadingImport.getSourceLists().get(0).getId();
    final CalculatedScenario calculatedScenario = CalculatedScenarioUtil.toCalculatedScenario(leadingImport, sourceListId, -1);
    calculatedScenario.setYear(leadingImport.getImportedYear());
    // the fake calculationId is needed at this point as the GMLBuilder only includes results if the calculationId is not 0/null.
    final int fakeCalculationId = 1;
    final Calculation calculation = calculatedScenario.getCalculation(calculatedScenario.getCalculationId(sourceListId));
    calculation.setCalculationId(fakeCalculationId);

    // Set the default options enforced by the util
    calculation.getOptions().getEmissionResultKeys().add(ERK);
    calculation.getOptions().getSubstances().addAll(ERK.getSubstance().hatch());

    final ArrayList<AeriusResultPoint> resultPoints = calculatedScenario.getResultPoints(fakeCalculationId);
    resultPoints.clear();
    resultPoints.addAll(resultList);

    final File tempDir = Files.createTempDirectory("valueperhexagonservice").toFile();
    final GMLWriter builder = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(context.getPMF()));
    generatedFiles.addAll(builder.writeToFiles(tempDir, leadingImport.getDatabaseVersion(), calculatedScenario, null));

    final DataObject generatedDataObject = DataObjectUtil.create(generatedFiles, dataType, tempDir);

    final ConvertResponse returnValue = new ConvertResponse();
    returnValue.dataObject(new DataObject().contentType(generatedDataObject.getContentType())
        .dataType(generatedDataObject.getDataType())
        .data(generatedDataObject.getData()))
        .successful(Boolean.TRUE);
    // add any warnings
    returnValue.getWarnings().addAll(warnings);
    return returnValue;
  }

  /**
   * Merges the results with the already present results.
   * @param resultMap The new result list.
   * @param result The result to merge.
   * @return true if there are any potentially mergeable results (if file import contains any usable result)
   */
  private boolean processResult(final int fileIndex, final Map<Integer, AeriusResultPoint> resultMap, final ImportResult result) {
    boolean containsUsableResult = false;

    // Source lists are set - even if there are no sources - fortunately, so using it is fine
    for (int i = 0; i < result.getSourceLists().size(); ++i) {
      for (final AeriusResultPoint point : result.getResultPoints(i)) {
        containsUsableResult |= processResults(fileIndex, resultMap, point);
      }
    }

    return containsUsableResult;
  }

  private boolean processResults(final int fileIndex, final Map<Integer, AeriusResultPoint> resultMap, final AeriusResultPoint point) {
    boolean containsUsableResult = false;

    if (AeriusPointType.RECEPTOR == point.getPointType() && point.hasEmissionResult(ERK)) {
      containsUsableResult = true;

      AeriusResultPoint mergedResultPoint = resultMap.get(point.getId());
      // No copy() will be used on the point Objects, as we're the only ones processing them.
      if (mergedResultPoint == null) {
        mergedResultPoint = new AeriusResultPoint(point.getId(), point.getPointType(), point.getX(), point.getY());
        resultMap.put(mergedResultPoint.getId(), mergedResultPoint);
      }
      implementation.process(fileIndex, point, mergedResultPoint);
    }

    return containsUsableResult;
  }

}
