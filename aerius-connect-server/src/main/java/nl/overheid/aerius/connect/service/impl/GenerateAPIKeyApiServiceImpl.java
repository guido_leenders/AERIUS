/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.GenerateAPIKeyRequest;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.service.GenerateAPIKeyApiService;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.connect.service.util.ValidationUtil;
import nl.overheid.aerius.enums.MessagesEnum;
import nl.overheid.aerius.mail.MailMessageData;
import nl.overheid.aerius.mail.MailTo;
import nl.overheid.aerius.mail.MessageTaskClient;
import nl.overheid.aerius.mail.ReplacementToken;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Server side implementation of {@link GenerateAPIKeyApiService}.
 */
public class GenerateAPIKeyApiServiceImpl extends GenerateAPIKeyApiService {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateAPIKeyApiServiceImpl.class);

  private final ConnectServiceContext context;

  public GenerateAPIKeyApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  GenerateAPIKeyApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response postGenerateAPIKey(final GenerateAPIKeyRequest request, final SecurityContext securityContext) throws NotFoundException {
    try {
      return Response.ok(generateAPIKey(request.getEmail())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  ValidateResponse generateAPIKey(final String email) throws AeriusException {
    final ValidateResponse result = new ValidateResponse().successful(Boolean.FALSE);

    try {
      ValidationUtil.email(email, context.getLocale());

      try (final Connection con = context.getPMF().getConnection()) {
        ValidationUtil.userRegistrationsAllowed(con);

        sendMail(UserUtil.generateAPIKey(con, email));
        result.successful(Boolean.TRUE);
      }
      // catching exception is allowed here
    } catch (final Exception e) {
      LOG.error("Generating API key failed:", e);
      throw AeriusExceptionConversionUtil.convert(e, context.getLocale());
    }

    return result;
  }

  private void sendMail(final ScenarioUser user) throws IOException {
    final MailMessageData messageData = new MailMessageData(
        MessagesEnum.CONNECT_APIKEY_CONFIRM_SUBJECT, MessagesEnum.CONNECT_APIKEY_CONFIRM_BODY,
        context.getLocale(), new MailTo(user.getEmailAddress()));
    // Yeah, we could set this before generation, but I really don't care about the probable 1 second precision in this case.
    final Date creationDate = new Date();
    messageData.setReplacement(ReplacementToken.CONNECT_APIKEY, user.getApiKey());
    // I would recommend refactoring CALC_CREATION_* stuff to CREATION_* stuff as this is, well, less okay. Something to do on the Master branch.
    messageData.setReplacement(ReplacementToken.CALC_CREATION_DATE, MessageTaskClient.getDefaultDateFormatted(creationDate, context.getLocale()));
    messageData.setReplacement(ReplacementToken.CALC_CREATION_TIME, MessageTaskClient.getDefaultTimeFormatted(creationDate, context.getLocale()));
    MessageTaskClient.startMessageTask(context.getClientFactory(), messageData);
  }

}
