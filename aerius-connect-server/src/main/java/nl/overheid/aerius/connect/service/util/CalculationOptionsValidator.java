/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.db.common.UserCalculationPointSetsRepository;
import nl.overheid.aerius.server.util.AfterPASHandler;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Validates the options in the {@link CalculationOptions} to see if there are valid and/or within range.
 */
public class CalculationOptionsValidator {

  private final Connection con;
  private final CalculationOptionsLimits limits;

  public CalculationOptionsValidator(final CalculationOptionsLimits limits, final Connection con) {
    this.limits = limits;
    this.con = con;
  }

  public void validate(final CalculationOptions options, final ScenarioUser user, final Locale locale) throws AeriusException, SQLException {
    validateYear(options);
    validateCalculationType(options);
    validateReceptorSet(options, user);
    validateSubstances(options);
    validateMaximumRange(options);
    validateAfterPASOoptions(options, locale);
  }

  private void validateAfterPASOoptions(final CalculationOptions options, final Locale locale) throws AeriusException {
    ConnectAfterPASRestrictionsUtil.errorIfTemporaryProject(options, locale,
        AfterPASHandler.AFTERPAS_OPTION_TEMPORARY_PERIOD,
        AfterPASHandler.AFTERPAS_CALCULATION_WILLNOTRUN_REMOVEOPTION);
    ConnectAfterPASRestrictionsUtil.errorIfDistanceLimit(options, locale,
        AfterPASHandler.AFTERPAS_OPTION_DISTANCE_LIMIT,
        AfterPASHandler.AFTERPAS_CALCULATION_WILLNOTRUN_REMOVEOPTION);
  }

  private void validateYear(final CalculationOptions options) throws AeriusException {
    if (options.getYear() < limits.getMinYear() || options.getYear() > limits.getMaxYear()) {
      throw new AeriusException(Reason.CONNECT_INCORRECT_CALCULATIONYEAR,
          String.valueOf(options.getYear()), String.valueOf(limits.getMinYear()), String.valueOf(limits.getMaxYear()));
    }
  }

  private void validateCalculationType(final CalculationOptions options) throws AeriusException {
    if (options.getCalculationType() == null) {
      throw new AeriusException(Reason.CONNECT_NO_CALCULATIONTYPE_SUPPLIED);
    }
  }


  private void validateReceptorSet(final CalculationOptions options, final ScenarioUser user) throws AeriusException, SQLException {
    if ((options.getCalculationType() == CalculationTypeEnum.CUSTOM_POINTS) && !(StringUtils.isEmpty(options.getReceptorSetName()))) {
        if (UserCalculationPointSetsRepository.getUserCalculationPointSetByName(con, user.getId(), options.getReceptorSetName()) == null) {
          throw new AeriusException(Reason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST);
      }
    }
  }

  private void validateSubstances(final CalculationOptions options) throws AeriusException {
    if (options.getSubstances() == null || options.getSubstances().isEmpty()) {
      throw new AeriusException(Reason.CONNECT_NO_SUBSTANCE_SUPPLIED);
    }
    if (options.getSubstances().stream().anyMatch(Objects::isNull)) {
      throw new AeriusException(Reason.CONNECT_UNKNOWN_SUBSTANCE_SUPPLIED);
    }
  }

  private void validateMaximumRange(final CalculationOptions options) throws AeriusException {
    if ((options.getCalculationType() == CalculationTypeEnum.NATURE_AREA || options.getCalculationType() == CalculationTypeEnum.RADIUS)
        && (options.getRange() == null || options.getRange() <= 0 || options.getRange() > limits.getMaxRange())) {
      throw new AeriusException(Reason.CONNECT_INVALID_CALCULATION_RANGE,
          String.valueOf(options.getRange()), String.valueOf(limits.getMaxRange()));
    }
  }

  /**
   * Class containing all calculation option limit values.
   */
  public static class CalculationOptionsLimits {
    private final Context context;

    public CalculationOptionsLimits(final Context context) {
      this.context = context;
    }

    public int getMinYear() {
      return context.getSetting(SharedConstantsEnum.MIN_YEAR);
    }

    public int getMaxYear() {
      return context.getSetting(SharedConstantsEnum.MAX_YEAR);
    }

    public int getMaxRange() {
      return context.getSetting(SharedConstantsEnum.CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI);
    }

    public int getMinTempProjectYears() {
      return SharedConstants.MIN_TEMP_PROJECT_YEARS;
    }

    public int getMaxTempProjectYears() {
      return SharedConstants.MAX_TEMP_PROJECT_YEARS;
    }
  }
}
