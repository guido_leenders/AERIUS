/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.conversion.ScenarioInputReader;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.ReceptorSetAddRequest;
import nl.overheid.aerius.connect.domain.ScenarioInput;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.ReceptorSetApiService;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.db.common.UserCalculationPointSetsRepository;
import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.domain.user.UserCalculationPointSetMetadata;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 *Server side implementation of {@link ReceptorSetApiService}.
 */
public class ReceptorSetApiServiceImpl extends ReceptorSetApiService {

  private static final Logger LOG = LoggerFactory.getLogger(ReceptorSetApiServiceImpl.class);

  private final ConnectServiceContext context;

  public ReceptorSetApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  ReceptorSetApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response addReceptorSet(final ReceptorSetAddRequest request, final SecurityContext securityContext)
      throws NotFoundException {
    final ValidateResponse response;
    try {
      response = addReceptorSetResponse(request.getDataObject(), request.getName(), request.getDescription(), request.getApiKey());
    } catch (final AeriusException exception) {
      return SwaggerUtil.handleException(context, exception);
    }

    return Response.ok(response).build();
  }

  @Override
  public Response deleteReceptorSet(final String apiKey, final String name, final SecurityContext securityContext)
      throws NotFoundException {
    final ValidateResponse response;
    try {
      response = deleteReceptorSetByName(apiKey, name);
    } catch (final AeriusException exception) {
      return SwaggerUtil.handleException(context, exception);
    }
    return Response.ok(response).build();
  }

  /**
  * Adds receptorSet
  * @param dataObject
  * @param name
  * @param description
  * @param apiKey
  * @return
  * @throws AeriusException
  */
  ValidateResponse addReceptorSetResponse(final DataObject dataObject, final String name, final String description,
      final String apiKey) throws AeriusException {
    ValidateResponse response = null;
    try {
      try (final Connection con = context.getPMF().getConnection()) {
        final ScenarioUser user = UserUtil.getUserWithoutValidatingMaximumConcurrentJobs(con, apiKey);
        if (UserCalculationPointSetsRepository.getUserCalculationPointSetByName(con, user.getId(), name) != null) {
          throw new AeriusException(Reason.CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS, name);
        }
        response = addUserCalculationPointSet(dataObject, name, description, user, con);
      }
    } catch (final Exception e) {
      LOG.warn("Adding ReceptorSet failed:", e);
      throw AeriusExceptionConversionUtil.convert(e, context);
    }
    return response;
  }

  /**
   * @param dataObject
   * @param name
   * @param description
   * @param user
   * @param con
   * @return
   * @throws AeriusException
   * @throws SQLException
   * @throws IOException
   */
  private ValidateResponse addUserCalculationPointSet(final DataObject dataObject, final String name, final String description,
      final HasId user, final Connection con) throws AeriusException, SQLException, IOException {
    final ValidateResponse response = new ValidateResponse();
    final UserCalculationPointSetMetadata userCalculationPointSetMetadata = new UserCalculationPointSetMetadata();
    userCalculationPointSetMetadata.setUserId(user.getId());
    userCalculationPointSetMetadata.setName(name);
    userCalculationPointSetMetadata.setDescription(description);
    final List<ValidationMessage> errors = new ArrayList<>();
    final CalculationPointList customPointList = collectReceptorsetInputData(dataObject, errors);
    if (errors.isEmpty()) {
      UserCalculationPointSetsRepository.insertUserCalculationPointSet(con, userCalculationPointSetMetadata, customPointList);
    } else {
      response.errors(errors);
    }
    response.successful(errors.isEmpty());
    return response;
  }

  /**
   * Collects all the input data for Receptorset.
   * @param dataObjects
   * @param customPoints
   * @param readMetaData if true reads the meta data from the first data object
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   */
  private CalculationPointList collectReceptorsetInputData(final DataObject dataObject, final List<ValidationMessage> errors)
      throws AeriusException, SQLException, IOException {
    CalculationPointList list = null;
    final ScenarioInputReader inputReader = new ScenarioInputReader(context);
    final ScenarioInput input = inputReader.run(
        dataObject.getDataType(), dataObject.getContentType(), dataObject.getData(),
        false, false, false, false, null, true);
    if (Boolean.FALSE.equals(input.getSuccessful())) {
      errors.addAll(input.getErrors());
    } else {
      list = input.getImportResult().getCalculationPoints();
      if (list.isEmpty()) {
        throw new AeriusException(Reason.CONNECT_NO_RECEPTORS_IN_PARAMETERS);
      }
    }
    return list;
  }

  /**
  * Deletes ReceptorSet point using receptorSet point Name
   * @param apiKey
   * @param name
  * @return
  * @throws AeriusException
   * @throws SQLException
  */
  ValidateResponse deleteReceptorSetByName(final String apiKey, final String name) throws AeriusException {
    try (final Connection con = context.getPMF().getConnection()) {
      final ScenarioUser user = UserUtil.getUserWithoutValidatingMaximumConcurrentJobs(con, apiKey);
      final UserCalculationPointSetMetadata set = UserCalculationPointSetsRepository.getUserCalculationPointSetByName(con, user.getId(), name);
      if (set == null) {
        throw new AeriusException(Reason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST, name);
      }
      UserCalculationPointSetsRepository.deleteUserCalculationPointSet(con, set.getSetId());

      return new ValidateResponse().successful(Boolean.TRUE);
    } catch (final SQLException e) {
      throw AeriusExceptionConversionUtil.convert(e, context);
    }
  }

}
