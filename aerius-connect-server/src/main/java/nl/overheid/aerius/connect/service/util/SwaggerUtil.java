/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.util.Locale;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ErrorResponse;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.i18n.AeriusExceptionMessages;

public final class SwaggerUtil {

  private SwaggerUtil() {
    // util class.
  }

  /**
   * Builds a response based on the AeriusException. The status is set based on whether it's an internal (system) error or client error.
   * @param context The context to use.
   * @param e The exception that occurred.
   * @return
   */
  public static Response handleException(final ConnectServiceContext context, final AeriusException e) {
    final ErrorResponse errorResponse = convert(context.getLocale(), e);
    final ResponseBuilder response = e.isInternalError() ? Response.serverError() : Response.status(Status.BAD_REQUEST);

    return response.entity(errorResponse).build();
  }

  /**
   * Converts an {@link AeriusException} to a {@link ErrorResponse}.
   * @param ae {@link AeriusException}
   * @return {@link ErrorResponse}
   */
  private static ErrorResponse convert(final Locale locale, final AeriusException ae) {
    return new ErrorResponse().code(ae.getReason().getErrorCode()).message(new AeriusExceptionMessages(locale).getString(ae));
  }


}
