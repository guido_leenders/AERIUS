/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.conversion.Converter;
import nl.overheid.aerius.connect.conversion.ConverterSpecifics;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.domain.ValidateRequest;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.service.ValidateApiService;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.server.util.AfterPASErrorsHandler;
import nl.overheid.aerius.server.util.AfterPASHandler;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Server side implementation of {@link ValidationApiService}.
 */
public class ValidateApiServiceImpl extends ValidateApiService {

  private static final Logger LOG = LoggerFactory.getLogger(ValidateApiServiceImpl.class);

  private final ConnectServiceContext context;

  private final Converter<ValidateResponse> converter;

  public ValidateApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  ValidateApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
    final ConverterSpecifics<ValidateResponse> implementation = new ConverterSpecifics<ValidateResponse>() {

      private final AfterPASHandler afterPASHandler = new AfterPASErrorsHandler(context.getLocale(),
          AfterPASHandler.AFTERPAS_INVALID_INPUT_REMOVEWITHCONVERT);

      @Override
      public void onSuccess(final ConnectServiceContext context, final ValidateResponse vr, final ImportResult ir, final DataType dataType,
          final ContentType contentType) {
        // nothing to do on success.
      }

      @Override
      public ValidateResponse create() {
        return new ValidateResponse();
      }

      @Override
      public AfterPASHandler getAfterPASHandler() {
        return afterPASHandler;
      }
    };
    this.converter = new Converter<>(implementation, context);
  }

  @Override
  public Response postValidate(final ValidateRequest request, final SecurityContext securityContext) {
    try {
      final DataObject dataObject = request.getDataObject();
      return Response.ok(
          validate(dataObject.getDataType(), dataObject.getContentType(), dataObject.getData(), request.getStrict(),
              request.getValidateAsPriorityProject(), dataObject.getSubstance()))
          .build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  ValidateResponse validate(final DataType dataType, final ContentType contentType, final String data, final boolean strict,
      final boolean validateAsPP, final Substance substance) throws AeriusException {
    ValidateResponse result = null;

    try {
      validataDataType(dataType);
      result = converter.run(dataType, contentType, data, true, false, strict, validateAsPP, substance, true);
    } catch (final Exception e) {
      LOG.error("Validation failed:", e);
      throw AeriusExceptionConversionUtil.convert(e, context);
    }

    return result;
  }

  /**
   * Check for supported data types.
   * @param dataType data type to check
   * @throws AeriusException throw exception in case data type not supported
   */
  private void validataDataType(final DataType dataType) throws AeriusException {
    if (dataType != DataType.GML && dataType != DataType.ZIP) {
      throw new AeriusException(Reason.IMPORT_FILE_UNSUPPORTED);
    }
  }
}
