/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.PriorityProjectUtilisationRequest;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.connect.service.ExportApiService;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.connect.service.util.ValidationUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.export.ExportTaskClient;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportedData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData;
import nl.overheid.aerius.shared.domain.export.PriorityProjectExportData.PriorityProjectExportType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Server side implementation of {@link ExportApiService}.
 */
public class ExportApiServiceImpl extends ExportApiService {

  private static final Logger LOG = LoggerFactory.getLogger(ExportApiServiceImpl.class);

  private final ConnectServiceContext context;

  public ExportApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  ExportApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response postExportProject(final PriorityProjectUtilisationRequest request, final SecurityContext securityContext)
      throws NotFoundException {
    try {
      return Response.ok(exportProject(request.getApiKey(), request.getProjectKey())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  ValidateResponse exportProject(final String apiKey, final String projectKey) throws AeriusException {
    final ValidateResponse result = new ValidateResponse().successful(Boolean.FALSE);
    final List<ValidationMessage> errors = new ArrayList<>();

    try {
      final ScenarioUser user;
      final PMF pmf = context.getPMF();
      try (final Connection con = pmf.getConnection()) {
        user = UserUtil.getUserWhileValidatingMaximumConcurrentJobs(con, apiKey);
        ValidationUtil.validatePriorityProjectUtilisationReferenceAllowed(con, apiKey, projectKey);
      }

      startUtilisationExport(projectKey, user);
      if (errors.isEmpty()) {
        result.successful(Boolean.TRUE);
      } else {
        result.errors(errors);
      }
      // catching exception is allowed here
    } catch (final Exception e) {
      LOG.error("Exporting GML failed", e);
      throw AeriusExceptionConversionUtil.convert(e, context.getLocale());
    }

    return result;
  }

  private void startUtilisationExport(final String projectKey, final ScenarioUser user) throws IOException, SQLException {
    final PriorityProjectExportData priorityProjectExportData = new PriorityProjectExportData();
    priorityProjectExportData.setPriorityProjectReference(projectKey);
    final ArrayList<PriorityProjectExportType> exportTypes = new ArrayList<>();
    exportTypes.add(PriorityProjectExportType.UTILISATION_FILES);
    priorityProjectExportData.setExportTypes(exportTypes);
    priorityProjectExportData.setEmailAddress(user.getEmailAddress());
    priorityProjectExportData.getAdditionalOptions().remove(ExportAdditionalOptions.RETURN_FILE);
    priorityProjectExportData.getAdditionalOptions().add(ExportAdditionalOptions.EMAIL_USER);

    final String correlationId = ExportTaskClient.startPriorityProjectResultExport(context.getClientFactory(), WorkerType.REGISTER,
        QueueEnum.REGISTER_EXPORT, priorityProjectExportData, new ExportResultCallBack());

    try (final Connection con = context.getPMF().getConnection()) {
      JobRepository.createJob(con, user, JobType.PRIORITY_PROJECT_UTILISATION, correlationId, projectKey);
    }
  }

  protected ImportResult getImportResult(final String url, final PMF pmf, final Locale locale)
      throws AeriusException, IOException, SQLException {
    try (final InputStream inputStream = new BufferedInputStream(new URL(url).openStream())) {
      final Importer importer = new Importer(pmf, locale);
      importer.setUseValidMetaData(false);
      importer.setValidateAgainstSchema(false);
      importer.setIncludeResults(true);
      importer.setIncludeSources(false);
      final ImportResult result = importer.convertInputStream2ImportResult(url, inputStream);

      if (!result.getExceptions().isEmpty()) {
        for (final Exception exception : result.getExceptions()) {
          LOG.error("Error reading url: {}", url, exception);
        }
        return null;
      }
      return result;
    }
  }

  private class ExportResultCallBack implements TaskResultCallback {

    @Override
    public void onSuccess(final Object value, final String correlationId, final String messageId) {
      final ExportedData exportedData = (ExportedData) value;
      LOG.info("Utilisation export file - filename {} files count {} ", exportedData.getFileName(), exportedData.getFilesInZip());

      final String url = exportedData.getDownloadInfo().getUrl();
      try (final Connection con = context.getPMF().getConnection()) {
        final PMF pmf = context.getPMF();
        final Locale locale = context.getLocale();
        final ImportResult importResult = getImportResult(url, pmf, locale);
        if (importResult == null) {
          throw new AeriusException(Reason.IMPORT_NO_RESULTS_PRESENT);
        } else {
          JobRepository.increaseHexagonCounter(con, correlationId, importResult.getCalculationPoints().size());

          final int situationId2 = importResult.getSourceLists().size() == 2 ? 1 : -1;
          final CalculatedScenario calculatedScenario = CalculatedScenarioUtil.toCalculatedScenario(importResult, 0, situationId2);
          calculatedScenario.setYear(importResult.getImportedYear());
          CalculationRepository.insertCalculationWithResults(con, calculatedScenario);

          final List<Integer> calculations = new ArrayList<>();
          for (final Calculation calculation : calculatedScenario.getCalculations()) {
            calculations.add(calculation.getCalculationId());
          }
          JobRepository.attachCalculations(con, correlationId, calculations);
          JobRepository.setResultUrl(con, correlationId, url);
          JobRepository.setEndTimeToNow(con, correlationId);
          JobRepository.updateJobStatus(con, correlationId, JobState.COMPLETED);
        }

      } catch (final AeriusException | IOException | SQLException e) {
        // on failure simply removes the job and logs appropriately. We want that too.
        onFailure(e, correlationId, messageId);
      }
    }

    @Override
    public void onFailure(final Exception exception, final String correlationId, final String messageId) {
      LOG.error("Utilisation export with correlationId crashed: {}", correlationId, exception);

      try (final Connection con = context.getPMF().getConnection()) {
        JobRepository.removeJob(con, JobRepository.getJobId(con, correlationId));
      } catch (final SQLException e) {
        LOG.error("Could not remove job with correlationId after the utilisation export crashed: {}", correlationId, e);
      }
    }
  }
}
