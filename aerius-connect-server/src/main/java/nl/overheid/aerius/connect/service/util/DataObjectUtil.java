/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.gml.GMLWriter;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenarioUtil;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.ZipFileMaker;

/**
 * Util class to convert data to string and the other way around.
 */
public final class DataObjectUtil {

  private DataObjectUtil() {
    // util class.
  }

  /**
   * Gets an input stream from a data object.
   * @param data data object
   * @return input stream
   */
  public static InputStream toInputStream(final DataObject data) {
    return toInputStream(data.getContentType(), data.getData());
  }

  /**
   * Gets an input stream from a data and data properties.
   * @param contentType type of the content
   * @param data data
   * @return input stream
   */
  public static InputStream toInputStream(final ContentType contentType, final String data) {
    final byte[] is;
    if (contentType == ContentType.TEXT) {
      is = data.getBytes(StandardCharsets.UTF_8);
    } else if (contentType == ContentType.BASE64) {
      is = Base64.decodeBase64(data);
    } else {
      throw new IllegalArgumentException("Don't know how to convert ContentType " + contentType + " to inputStream.");
    }
    return new ByteArrayInputStream(is);
  }

  /**
   * Returns an encoded string from a file, encoding determined by content type.
   * @param contentType type of the content
   * @param file file to get content from.
   * @return data encoded as a String
   * @throws IOException in case file read errors
   */
  public static String bytesToString(final ContentType contentType, final File file) throws IOException {
    final byte[] encoded = Files.readAllBytes(Paths.get(file.toURI()));
    return bytesToString(contentType, encoded);
  }

  /**
   * Returns an encoded string from a byte array, encoding determined by content type.
   * @param contentType type of the content
   * @param data byte array representing the string
   * @return data encoded as a String
   */
  public static String bytesToString(final ContentType contentType, final byte[] data) {
    final byte[] bytes;
    if (contentType == ContentType.TEXT) {
      bytes = data;
    } else if (contentType == ContentType.BASE64) {
      bytes = Base64.encodeBase64(data);
    } else {
      throw new IllegalArgumentException("Don't know how to convert data of ContentType " + contentType + " to String.");
    }
    return new String(bytes, StandardCharsets.UTF_8);
  }

  /**
   * If one of the files is a ZIP file, the output should also be a ZIP file. Otherwise use a GML.
   * This is the default behaviour
   * @param dataObjects The data objects to loop through.
   * @return Output DataType.
   */
  public static DataType determineOutputDataType(final List<DataObject> dataObjects) {
    for (final DataObject dataObject : dataObjects) {
      if (DataType.ZIP == dataObject.getDataType()) {
        return DataType.ZIP;
      }
    }

    return DataType.GML;
  }

  /**
   * @param ir
   * @param pmf database factory
   * @return
   * @throws SQLException
   * @throws AeriusException
   * @throws IOException
   */
  public static DataObject build(final PMF pmf, final ImportResult ir, final DataType dataType) throws AeriusException, SQLException, IOException {
    final GMLWriter builder = new GMLWriter(ReceptorGridSettingsRepository.getReceptorGridSettings(pmf));
    final List<EmissionSourceList> sl = ir.getSourceLists();
    final CalculatedScenario cs = CalculatedScenarioUtil.toCalculatedScenario(ir, sl.get(0).getId(), sl.size() > 1 ? sl.get(1).getId() : -1);
    cs.setYear(ir.getImportedYear());
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setTemporaryProjectYears(ir.getImportedTemporaryPeriod());
    cs.setOptions(options);
    final File tempDir = Files.createTempDirectory("data_object_util").toFile();
    final List<File> files = builder.writeToFiles(tempDir, pmf.getDatabaseVersion(), cs, null);
    return create(files, dataType, tempDir);
  }

  /**
   * Generate a data object based on input.
   * Returns a newly created DataObject based on the input.
   * If dataType is a ZIP or multiple files are presented, the result will be a ZIP file in Base64 will be put in the DataObject.
   * Otherwise a GML file in TEXT will be put in the DataObject.
   *
   * @param files The file(s) to add to the object.
   * @param dataType The preferred datatype. Will be coerced to ZIP if multiple files.
   * @param baseDir The base directory of the files to add. Will not be included in the resulting zip file.
   * @return The dataobject containing the zip.
   * @throws IOException
   */
  public static DataObject create(final List<File> files, final DataType dataType, final File baseDir)
      throws IOException {
    DataObject returnObject;
    if (dataType == DataType.ZIP || files.size() > 1) {
      returnObject = createZip(files, baseDir);
    } else {
      returnObject = createGML(files.get(0));
    }
    return returnObject;
  }

  /**
   * Returns a newly created DataObject based on the GML file. Will always have the contentType TEXT.
   *
   * @param file The file to add to the object.
   * @return The DataObject containing the proper content
   * @throws IOException
   */
  private static DataObject createGML(final File file) throws IOException {
    final DataObject dataObject = new DataObject();

    dataObject
      .dataType(DataType.GML)
      .contentType(ContentType.TEXT)
      .data(DataObjectUtil.bytesToString(ContentType.TEXT, file));

    return dataObject;
  }

  /**
   * Returns a newly created DataObject based on the file(s)/baseDir.
   * Result will be a ZIP file in Base64 will be put in the DataObject. This effect can also be forced
   * for single files by providing the argument forceZip as true.
   *
   * @param files The file(s) to add to the object.
   * @param baseDir The base directory of the files to add. Will not be included in the resulting zip file.
   * @return The dataobject containing the zip.
   * @throws IOException
   */
  private static DataObject createZip(final List<File> files, final File baseDir) throws IOException {
    final DataObject dataObject = new DataObject()
      .dataType(DataType.ZIP)
      .contentType(ContentType.BASE64);
    try (final ByteArrayOutputStream bstream = new ByteArrayOutputStream()) {
      ZipFileMaker.files2ZipStream(bstream , files, baseDir, true);
      dataObject.setData(DataObjectUtil.bytesToString(ContentType.BASE64, bstream.toByteArray()));
    }

    return dataObject;
  }
}
