/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.conversion.ScenarioInputReader;
import nl.overheid.aerius.connect.domain.CalculateDataObject;
import nl.overheid.aerius.connect.domain.CalculateRequest;
import nl.overheid.aerius.connect.domain.CalculateResponse;
import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOutputOptions;
import nl.overheid.aerius.connect.domain.CalculationOutputOptions.OutputTypeEnum;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ScenarioInput;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.connect.service.CalculateApiService;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.OptionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.connect.service.util.ValidationMessageUtil;
import nl.overheid.aerius.connect.service.util.ValidationUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ContextRepository;
import nl.overheid.aerius.db.common.UserCalculationPointSetsRepository;
import nl.overheid.aerius.db.i18n.DBMessages.DBMessagesKey;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.export.ExportTaskClient;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.ResearchAreaCalculationScenario;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.user.UserCalculationPointSetMetadata;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.NoopCallback;
import nl.overheid.aerius.taskmanager.client.QueueEnum;
import nl.overheid.aerius.taskmanager.client.WorkerType;

/**
 * Server side implementation of {@link CalculateApiService}.
 */
public class CalculateApiServiceImpl extends CalculateApiService {

  private static final Logger LOG = LoggerFactory.getLogger(CalculateApiServiceImpl.class);

  private final ConnectServiceContext context;

  public CalculateApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  CalculateApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response postCalculate(final CalculateRequest request, final SecurityContext securityContext) throws NotFoundException {
    final CalculateResponse response;

    try {
      response = calculate(
          request.getOptions(), request.getCalculateDataObjects(), Boolean.TRUE.equals(request.getStrict()), request.getApiKey());
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }

    return Response.ok(response).build();
  }

  CalculateResponse calculate(final CalculationOptions options, final List<CalculateDataObject> dataObjects, final boolean strict,
      final String apiKey) throws AeriusException {
    final CalculateResponse response;

    try {
      final ScenarioUser user;
      final PMF pmf = context.getPMF();
      final Locale locale = context.getLocale();
      try (final Connection con = pmf.getConnection()) {
        user = UserUtil.getUserWhileValidatingMaximumConcurrentJobs(con, apiKey);
        ValidationUtil.validateInput(con, options, ContextRepository.getConnectContext(con, new DBMessagesKey(pmf.getProductType(), locale)),
            user, locale);
      }

      final CalculatedSingle scenario = new CalculatedSingle();
      scenario.setReceptorSetName(options.getReceptorSetName());
      OptionUtil.setCalculationOptions(scenario, options);
      final boolean isCustomPoints = options.getCalculationType() == CalculationTypeEnum.CUSTOM_POINTS;
      final List<ValidationMessage> errors = collectInputData(scenario, dataObjects, strict,
          isCustomPoints && Boolean.TRUE.equals(options.getValidate()),
          isCustomPoints ? options.getReceptorSetName() : null,
          user);
      validateResearchArea(scenario, errors);

      response = new CalculateResponse();
      response.successful(errors.isEmpty());
      if (errors.isEmpty()) {
        startGMLExport(response, user, scenario, options);
      } else {
        response.errors(errors);
      }
    } catch (final Exception e) {
      LOG.error("Calculation export failed:", e);
      throw AeriusExceptionConversionUtil.convert(e, context);
    }

    return response;
  }

  /**
   * Collects all the input data.
   * @param scenario Container where all the collected inputdata is stored.
   * @param dataObjects
   * @param strict
   * @param validateSchema if true the file will be validated against the XSD (only for IMAER)
   * @param receptorSetName Name when a previously uploaded receptorSet should exclusively be used. Null otherwise.
   * @throws AeriusException
   * @throws IOException
   * @throws SQLException
   * @throws ClassNotFoundException
   * @throws IllegalAccessException
   * @throws InstantiationException
   */
  private List<ValidationMessage> collectInputData(final CalculatedSingle scenario, final List<CalculateDataObject> dataObjects,
      final boolean strict, final boolean validateSchema, final String receptorSetName, final ScenarioUser user)
      throws AeriusException, SQLException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    if (dataObjects.isEmpty()) {
      throw new AeriusException(Reason.CONNECT_NO_SOURCES);
    }

    final List<ValidationMessage> errors = new ArrayList<>();
    final List<AeriusPoint> customPointList = new ArrayList<>();
    boolean first = true;

    for (final CalculateDataObject dataObject : dataObjects) {
      final ScenarioInputReader inputReader = new ScenarioInputReader(context);
      final ScenarioInput input = inputReader.run(
          dataObject.getDataType(), dataObject.getContentType(), dataObject.getData(),
          true, false, strict, false, dataObject.getSubstance(), validateSchema);

      if (Boolean.TRUE.equals(input.getSuccessful())) {
        if (first) {
          scenario.setMetaData(input.getImportResult().getMetaData());
          first = false;
        }

        setEmissionSourceList(scenario, dataObject, input.getImportResult().getSourceLists());

        handleCustomPoints(receptorSetName, user, customPointList, input);
      } else {
        errors.addAll(input.getErrors());
        break;
      }

    }

    if (errors.isEmpty()) {
      // get and set sources because setSources does some side-effects...
      scenario.setSources(scenario.getSources());
      // First all custom points are collected in a list so we can do setAll once. Because this will preserve Id's and setAll cleans internal list
      // so we can only use it once, without removing already added custom points.
      scenario.getCalculationPoints().setAll(customPointList);
    }

    return errors;
  }

  private void handleCustomPoints(final String receptorSetName, final ScenarioUser user, final List<AeriusPoint> customPointList,
      final ScenarioInput input) throws AeriusException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    if (StringUtils.isEmpty(receptorSetName)) {
      customPointList.addAll(input.getImportResult().getCalculationPoints());
    } else {
      if (customPointList.isEmpty()) { // The stored receptorset should only be added once.
        customPointList.addAll(getCalculationPoints(user, receptorSetName));
      }
    }
  }

  private List<AeriusPoint> getCalculationPoints(final ScenarioUser user, final String calculationPointSetName)
      throws AeriusException, SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
    List<AeriusPoint> result = null;
    try (final Connection con = context.getPMF().getConnection()) {
      final UserCalculationPointSetMetadata userCalculationPointSet = UserCalculationPointSetsRepository.getUserCalculationPointSetByName(con,
          user.getId(), calculationPointSetName);
      result = UserCalculationPointSetsRepository.getUserCalculationPointsFromSet(con, userCalculationPointSet.getSetId());
    }
    return result;
  }

  private void startGMLExport(final CalculateResponse response, final ScenarioUser user, final CalculatedScenario scenario,
      final CalculationOptions options) throws IOException, SQLException {
    final String correlationId = ExportTaskClient.startGMLExport(context.getClientFactory(), WorkerType.CONNECT, QueueEnum.CONNECT_GML_EXPORT,
        OptionUtil.getExportProperties(options, determineExportType(options), user.getEmailAddress(), scenario.getMetaData()), scenario,
        new NoopCallback());
    response.setKey(correlationId);
    try (final Connection con = context.getPMF().getConnection()) {
      JobRepository.createJob(con, user, JobType.CALCULATION, correlationId, options.getName());
    }
  }

  private ExportType determineExportType(final CalculationOptions options) {
    ExportType exportType = ExportType.GML_WITH_RESULTS;

    final CalculationOutputOptions outputOptions = options.getOutputOptions();
    if (outputOptions != null) {
      if (CalculationTypeEnum.NBWET != options.getCalculationType() && OutputTypeEnum.CSV == outputOptions.getOutputType()) {
        exportType = ExportType.CSV;
      } else if (Boolean.TRUE.equals(outputOptions.getSectorOutput())) {
        exportType = ExportType.GML_WITH_SECTORS_RESULTS;
      }
    }

    return exportType;
  }

  private void setEmissionSourceList(final CalculatedSingle scenario, final CalculateDataObject dataObject, final List<EmissionSourceList> list) {
    if (!list.isEmpty()) {
      setEmissionSourceList(scenario, dataObject, list.get(0));
    }
  }

  private void setEmissionSourceList(final CalculatedScenario scenario, final CalculateDataObject dataObject, final EmissionSourceList subList) {
    final EmissionSourceList esl;

    esl = ((CalculatedSingle) scenario).getSources();

    if (StringUtils.isEmpty(esl.getName())) {
      esl.setName(subList.getName());
    }
    esl.addAll(subList);
  }

  private void validateResearchArea(final CalculatedSingle scenario, final List<ValidationMessage> errorListToAddTo) {
    if (scenario instanceof ResearchAreaCalculationScenario) {
      final ResearchAreaCalculationScenario researchAreaScenario = (ResearchAreaCalculationScenario) scenario;

      if (researchAreaScenario.getResearchAreaSources() == null || researchAreaScenario.getResearchAreaSources().isEmpty()) {
        errorListToAddTo.add(ValidationMessageUtil.convert(new AeriusException(Reason.CALCULATION_NO_RESEARCH_AREA), context.getLocale()));
      }
      if (!researchAreaScenario.getOptions().isPermitCalculationRadiusImpact()) {
        errorListToAddTo.add(ValidationMessageUtil.convert(new AeriusException(Reason.RESEARCH_AREA_NO_RADIUS), context.getLocale()));
      }
    }
  }

}
