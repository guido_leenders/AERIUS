/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.CancelJobRequest;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.service.CancelJobApiService;
import nl.overheid.aerius.connect.service.NotFoundException;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.SwaggerUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.connect.service.util.ValidationUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Server side implementation of {@link CancelJobApiService}.
 */
public class CancelJobApiServiceImpl extends CancelJobApiService {

  private static final Logger LOG = LoggerFactory.getLogger(CancelJobApiServiceImpl.class);

  private final ConnectServiceContext context;

  public CancelJobApiServiceImpl() {
    this(new ConnectServiceContext());
  }

  CancelJobApiServiceImpl(final ConnectServiceContext context) {
    this.context = context;
  }

  @Override
  public Response postCancelJob(final CancelJobRequest request, final SecurityContext securityContext) throws NotFoundException {
    try {
      return Response.ok(cancelJob(request.getApiKey(), request.getJobKey())).build();
    } catch (final AeriusException e) {
      return SwaggerUtil.handleException(context, e);
    }
  }

  ValidateResponse cancelJob(final String apiKey, final String jobKey) throws AeriusException {
    try {
      final ScenarioUser user;
      final PMF pmf = context.getPMF();
      try (final Connection con = pmf.getConnection()) {
        user = UserUtil.getUserWithoutValidatingMaximumConcurrentJobs(con, apiKey);
        ValidationUtil.validateJobKey(con, user, jobKey);
      }
      changeJobStateToCancel(jobKey);
      // catching exception is allowed here
    } catch (final Exception e) {
      LOG.error("Canceling on JobId failed", e);
      throw AeriusExceptionConversionUtil.convert(e, context.getLocale());
    }
    return new ValidateResponse().successful(Boolean.TRUE);
  }

  private void changeJobStateToCancel(final String jobKey) throws IOException, SQLException, AeriusException {
    try (final Connection con = context.getPMF().getConnection()) {
      JobRepository.updateJobStatus(con, jobKey, JobState.CANCELLED);
    }
  }

}