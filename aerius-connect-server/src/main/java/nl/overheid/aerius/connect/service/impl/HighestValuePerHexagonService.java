/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.service.ValuePerHexagonApiService;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.valueperhexagon.ValuePerHexagonHelper;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Implementation of {@link ValuePerHexagonApiService}, specifically the highest part.
 */
public final class HighestValuePerHexagonService {
  private static final Logger LOG = LoggerFactory.getLogger(HighestValuePerHexagonService.class);

  private HighestValuePerHexagonService() {
    // not to be constructed
  }

  static ConvertResponse highestValuePerHexagon(final ConnectServiceContext context, final List<DataObject> dataObjects) throws AeriusException {
    LOG.debug("Received {} file(s) to determine highest values per hexagon for", dataObjects.size());

    try {
      final ValuePerHexagonHelper helper =
          new ValuePerHexagonHelper((fileIndex, point, mergedResultPoint) -> mergeResults(point, mergedResultPoint));

      return helper.valuePerHexagon(context, dataObjects);
    } catch (final Exception e) {
      LOG.error("Determining highest value failed unexpectedly:", e);
      throw AeriusExceptionConversionUtil.convert(e, LocaleUtils.getDefaultLocale());
    }
  }

  static void mergeResults(final AeriusResultPoint point, final AeriusResultPoint mergedResultPoint) {
    if (point.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION) > mergedResultPoint.getEmissionResult(EmissionResultKey.NOXNH3_DEPOSITION)) {
      mergedResultPoint.setEmissionResult(EmissionResultKey.NOX_DEPOSITION, point.getEmissionResult(EmissionResultKey.NOX_DEPOSITION));
      mergedResultPoint.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, point.getEmissionResult(EmissionResultKey.NH3_DEPOSITION));
    }
  }

}
