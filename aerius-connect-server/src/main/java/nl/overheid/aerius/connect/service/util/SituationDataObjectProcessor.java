/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.SituationDataObject;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * @author AERIUS
 *
 */
public class SituationDataObjectProcessor {

  private final Logger logger = LoggerFactory.getLogger(SituationDataObjectProcessor.class);

  private final List<DataObject> current = new ArrayList<>();

  private final List<DataObject> proposed = new ArrayList<>();

  public boolean isCurrentEmpty() {
    return current.isEmpty();
  }

  public boolean isProposedEmpty() {
    return proposed.isEmpty();
  }

  public DataObject[] getCurrent() {
    return current.toArray(new DataObject[current.size()]);
  }

  public DataObject[] getProposed() {
    return proposed.toArray(new DataObject[proposed.size()]);
  }

  public void process(final List<SituationDataObject> situationDataObjects) throws AeriusException {
    for (final SituationDataObject situationDataObject : situationDataObjects) {
      switch (situationDataObject.getSituationType()) {
      case CURRENT:
        current.add(situationDataObject);
        break;
      case PROPOSED:
        proposed.add(situationDataObject);
        break;
      default:
        logger.error("Unknown situationType encountered.");
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }
  }

}
