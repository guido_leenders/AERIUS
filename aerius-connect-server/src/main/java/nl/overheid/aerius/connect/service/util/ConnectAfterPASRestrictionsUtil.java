/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.PermitCalculationRadiusTypeEnum;
import nl.overheid.aerius.db.i18n.AeriusMessages;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Connect specific utility class for checking input for no longer supported options.
 */
public final class ConnectAfterPASRestrictionsUtil {

  // Not to be constructed.
  private ConnectAfterPASRestrictionsUtil() {
  }

  /**
   * @param options
   * @param locale
   * @param args
   * @throws AeriusException
   */
  public static void errorIfTemporaryProject(final CalculationOptions options, final Locale locale, final String... args) throws AeriusException {
    if (options.getTempProjectYears() != null && options.getTempProjectYears() != 0) {
      final AeriusMessages aeriusMessages = new AeriusMessages(locale);

      throw new AeriusException(Reason.CONNECT_UNSUPPORTED_PAS_OPTIONS,
          Stream.of(args).map(aeriusMessages::getString).collect(Collectors.toList()).toArray(new String[0]));
      }
  }

  public static void errorIfDistanceLimit(final CalculationOptions options, final Locale locale, final String... args) throws AeriusException {
    if (options.getPermitCalculationRadiusType() != null && options.getPermitCalculationRadiusType() != PermitCalculationRadiusTypeEnum.NONE) {
      final AeriusMessages aeriusMessages = new AeriusMessages(locale);

      throw new AeriusException(Reason.GENERAL_UNSUPPORTED_PAS_OPTIONS,
          Stream.of(args).map(aeriusMessages::getString).collect(Collectors.toList()).toArray(new String[0]));
      }
  }

}
