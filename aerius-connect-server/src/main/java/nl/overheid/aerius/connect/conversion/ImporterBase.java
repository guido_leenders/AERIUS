/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.conversion;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.service.util.DataObjectUtil;
import nl.overheid.aerius.connect.service.util.OptionUtil;
import nl.overheid.aerius.importer.Importer;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Base class to be used by other helpers that need to import things.
 */
class ImporterBase {

  private final ConnectServiceContext context;

  public ImporterBase() {
    this.context = new ConnectServiceContext();
  }

  public ImporterBase(final ConnectServiceContext context) {
    this.context = context;
  }

  public ImportResult importData(final DataType dataType, final ContentType contentType, final String data,
      final boolean includeSources, final boolean includeResults, final boolean strict, final Substance substance, final boolean validate)
          throws AeriusException, SQLException, IOException {
    final Importer importer = getImporter();
    importer.setIncludeResults(includeResults);
    importer.setIncludeSources(includeSources);
    importer.setValidateAgainstSchema(validate);
    importer.setValidateStrict(strict);
    importer.setUseImportedLanduses(true);

    try (final InputStream inputStream = DataObjectUtil.toInputStream(contentType, data)) {
      return importer.convertInputStream2ImportResult('.' + dataType.name(), inputStream, OptionUtil.toSubstance(substance));
    }
  }

  /**
   * Creates an Importer.
   * 
   * @return the new Importer instance.
   * @throws SQLException
   * @throws AeriusException
   */
  protected Importer getImporter() throws SQLException, AeriusException {
    return new Importer(context.getPMF());
  }

  protected final ConnectServiceContext getContext() {
    return context;
  }
}
