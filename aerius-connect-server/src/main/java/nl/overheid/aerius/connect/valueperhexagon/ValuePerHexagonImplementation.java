/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.valueperhexagon;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;

public interface ValuePerHexagonImplementation {
  /**
   * Process given result.
   * @param fileIndex The file index. 1 = first file, 2 = second file etc.
   * @param point The point containing the new value(s) process.
   * @param processedResultPoint The processed point. Mutations should be done on this point.
   */
  void process(int fileIndex, AeriusResultPoint point, AeriusResultPoint processedResultPoint);
}
