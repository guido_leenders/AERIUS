/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import java.util.List;
import java.util.Set;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOptions.PermitCalculationRadiusTypeEnum;
import nl.overheid.aerius.connect.domain.ResultType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.export.ExportData.ExportAdditionalOptions;
import nl.overheid.aerius.shared.domain.export.ExportProperties;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Util class to convert connect api enum options to AERIUS enum options.
 */
public final class OptionUtil {

  private OptionUtil() {
    // util class.
  }

  /**
   * Sets the CalculationSetOptions from the user input.
   * @param scenario the scenario to set the options.
   * @param options user specified options.
   * @throws AeriusException
   */
  public static void setCalculationOptions(final CalculatedScenario scenario, final CalculationOptions options) throws AeriusException {
    final CalculationSetOptions setOptions = new CalculationSetOptions();
    setOptions.setCalculateMaximumRange(options.getRange() == null ? 0 : options.getRange());
    setOptions.setCalculationType(getCalculationType(options.getCalculationType()));
    setOptions.setTemporaryProjectYears(options.getTempProjectYears());
    setOptions.setPermitCalculationRadiusType(null);
    final boolean customPoints = options.getCalculationType() == CalculationTypeEnum.CUSTOM_POINTS;
    setOptions.setRoadOPS(customPoints && Boolean.TRUE.equals(options.getRoadOPS()));
    setOptions.setForceAggregation(customPoints && Boolean.TRUE.equals(options.getAggregate()));
    setSubstances(setOptions.getSubstances(), options.getSubstances());
    //Setting setEmissionResult depends on setSubstances.
    setEmissionResultKeys(options.getCalculationType() == CalculationTypeEnum.NBWET, setOptions.getEmissionResultKeys(), setOptions.getSubstances(),
        options.getOutputOptions().getResultTypes());
    scenario.setOptions(setOptions);
    scenario.setYear(options.getYear());
  }

  /**
   * Returns the AERIUS calculation type given a connect calculation type or null if no specified.
   * @param ct connect calculation type
   * @return AERIUS calculation type
   * @throws AeriusException
   */
  public static CalculationType getCalculationType(final CalculationTypeEnum ct) throws AeriusException {
    final CalculationType calculationType;
    if (ct == null) {
      calculationType = null;
    } else {
      switch (ct) {
      case CUSTOM_POINTS:
        calculationType = CalculationType.CUSTOM_POINTS;
        break;
      case NBWET:
        calculationType = CalculationType.PAS;
        break;
      case NATURE_AREA:
        //calculationType = CalculationType.NATURE_AREA
        throw new AeriusException(Reason.CONNECT_CALCULATIONTYPE_SUPPLIED_NOT_SUPPORTED, ct.toString());
      case RADIUS:
        //calculationType = CalculationType.RADIUS
        throw new AeriusException(Reason.CONNECT_CALCULATIONTYPE_SUPPLIED_NOT_SUPPORTED, ct.toString());
      default:
        calculationType = null;
        break;
      }
    }
    return calculationType;
  }

  /**
   * Returns the AERIUS PermitCalculationRadius type given a connect PermitCalculationRadius or null or not specified.
   * @param ct connect permitCalculationRadius type
   * @return AERIUS PermitCalculationRadius type
   */
  public static PermitCalculationRadiusType getPermitCalculationRadiusType(final PermitCalculationRadiusTypeEnum ct,
      final List<PermitCalculationRadiusType> types) {
    for (final PermitCalculationRadiusType pcrt : types) {
      if (ct != null && pcrt.getCode().equals(ct.toString())) {
        return pcrt;
      }
    }
    return null;
  }

  /**
   * Sets the AERIUS substances from the connect substances list.
   * @param substances AERIUS substances list to add substances to
   * @param connectSubstances connect substances
   */
  public static void setSubstances(final List<Substance> substances, final List<nl.overheid.aerius.connect.domain.Substance> connectSubstances) {
    for (final nl.overheid.aerius.connect.domain.Substance cs : connectSubstances) {
      substances.add(toSubstance(cs));
    }
  }

  public static Substance toSubstance(final nl.overheid.aerius.connect.domain.Substance cs) {
    final Substance substance;
    if (cs == null) {
      substance = null;
    } else {
      switch (cs) {
      case NH3:
        substance = Substance.NH3;
        break;
      case NOX:
        substance = Substance.NOX;
        break;
      default:
        throw new IllegalArgumentException("Substance '" + cs + "' is not supported.");
      }
    }
    return substance;
  }

  public static EmissionResultType toEmissionResultType(final ResultType rt) {
    final EmissionResultType ert;
    if (rt == null) {
      ert = null;
    } else {
      switch (rt) {
      case CONCENTRATION:
        ert = EmissionResultType.CONCENTRATION;
        break;
      case DEPOSITION:
        ert = EmissionResultType.DEPOSITION;
        break;
      case DRY_DEPOSITION:
        ert = EmissionResultType.DRY_DEPOSITION;
        break;
      case WET_DEPOSITION:
        ert = EmissionResultType.WET_DEPOSITION;
        break;
      default:
        throw new IllegalArgumentException("Can't convert connect result type '" + rt + "' to a AERIUS result type");
      }
    }
    return ert;
  }

  /**
   * Sets the emission result types to calculate.
   *
   * @param isWetNbCalculation boolean if the calculation is a Wet NB calculation
   * @param erks list to set
   * @param substances substances to calculate
   * @param resultTypes result types to get results for
   */
  private static void setEmissionResultKeys(final boolean isWetNbCalculation, final Set<EmissionResultKey> erks, final List<Substance> substances,
      final List<ResultType> resultTypes) {
    if (isWetNbCalculation || resultTypes.isEmpty()) {
      for (final Substance substance : substances) {
        erks.add(EmissionResultKey.valueOf(substance, EmissionResultType.DEPOSITION));
      }
    } else {
      for (final ResultType resultType : resultTypes) {
        erks.addAll(EmissionResultKey.getEmissionResultKeys(substances, toEmissionResultType(resultType)));
      }
    }
  }

  /**
   * Get the export properties.
   * @param options options.
   * @param exportType export type.
   * @param emailAddress email address.
   * @param scenarioMetaData contact information.
   *
   * @return export properties
   */
  public static ExportProperties getExportProperties(final CalculationOptions options, final ExportType exportType, final String emailAddress,
      final ScenarioMetaData scenarioMetaData) {
    final ExportProperties exportProperties = new ExportProperties();
    exportProperties.setName(options.getName());
    exportProperties.setEmailAddress(emailAddress);
    exportProperties.setExportType(exportType);
    exportProperties.setYear(options.getYear());
    exportProperties.setScenarioMetaData(scenarioMetaData);
    // Always keep results for Connect.. initially. They will be cleaned when necessary by the JobCleanupWorker.
    exportProperties.getAdditionalOptions().add(ExportAdditionalOptions.TRACK_JOB_PROGRESS);
    if (options.getSendEmail() != null && !options.getSendEmail()) {
    	exportProperties.getAdditionalOptions().remove(ExportAdditionalOptions.EMAIL_USER);
    }
    return exportProperties;
  }
}

