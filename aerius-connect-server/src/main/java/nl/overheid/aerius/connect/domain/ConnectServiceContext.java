/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.domain;

import java.io.IOException;
import java.util.Locale;

import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.server.ServerPMF;
import nl.overheid.aerius.server.task.TaskClientFactory;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.util.LocaleUtils;

public class ConnectServiceContext {

  private final Locale locale;
  private PMF pmf;
  private TaskManagerClient clientFactory;

  public ConnectServiceContext()  {
    locale = LocaleUtils.getDefaultLocale();
  }

  public ConnectServiceContext(final PMF pmf, final TaskManagerClient clientFactory) {
    this();
    this.pmf = pmf;
    this.clientFactory = clientFactory;
  }

  public Locale getLocale() {
    return locale;
  }

  public PMF getPMF() {
    synchronized (this) {
      if (pmf == null) {
        pmf = ServerPMF.getInstance();
      }
      return pmf;
    }
  }

  public TaskManagerClient getClientFactory() throws IOException {
    synchronized (this) {
      if (clientFactory == null) {
        clientFactory = TaskClientFactory.getInstance();
      }
      return clientFactory;
    }
  }
}
