<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:imaer="http://imaer.aerius.nl/1.0" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/1.0 http://imaer.aerius.nl/1.0/IMAER.xsd">
    <imaer:aeriusCalculatorMetaData>
        <imaer:year>2015</imaer:year>
        <imaer:situationName>Situation 1</imaer:situationName>
        <imaer:reference>De referentie</imaer:reference>
        <imaer:projectName>Nieuwe club</imaer:projectName>
        <imaer:corporation>beach club</imaer:corporation>
        <imaer:facilityLocation>
          <imaer:streetAddress>Zeeweg 1</imaer:streetAddress>
          <imaer:postcode>2051 EC</imaer:postcode>
          <imaer:city>Bloemendaal</imaer:city>
        </imaer:facilityLocation>
        <imaer:description>De Nieuwe beach club</imaer:description>
    </imaer:aeriusCalculatorMetaData>
    <imaer:aeriusCalculatorMetaData>
        <imaer:year>2015</imaer:year>
        <imaer:situationName>Situation 1</imaer:situationName>
        <imaer:reference>De referentie</imaer:reference>
        <imaer:projectName>Nieuwe club</imaer:projectName>
        <imaer:corporation>beach club</imaer:corporation>
        <imaer:facilityLocation>
          <imaer:streetAddress>Zeeweg 1</imaer:streetAddress>
          <imaer:postcode>2051 EC</imaer:postcode>
          <imaer:city>Bloemendaal</imaer:city>
        </imaer:facilityLocation>
        <imaer:description>De Nieuwe beach club</imaer:description>
    </imaer:aeriusCalculatorMetaData>
    <imaer:featureMembers>
        <imaer:SRM2Road sectorId="3111" gml:id="ES.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>ES.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:label>Source 1</imaer:label>
            <imaer:emissionSourceCharacteristics>
                <imaer:EmissionSourceCharacteristics>
                    <imaer:heatContent>0.0</imaer:heatContent>
                    <imaer:emissionHeight>2.5</imaer:emissionHeight>
                    <imaer:spread>2.5</imaer:spread>
                </imaer:EmissionSourceCharacteristics>
            </imaer:emissionSourceCharacteristics>
            <imaer:geometry>
                <imaer:EmissionSourceGeometry>
                    <imaer:GM_Curve>
                        <gml:LineString srsName="urn:ogc:def:crs:EPSG::28992" gml:id="ES.1.CURVE">
                            <gml:posList>90328.06 474393.36 91268.86 476086.8 92397.82 477780.24 93580.54 479393.04 94306.3 481247.76 95058.94 483156.24 96026.62 484876.56 96806.14 486704.4 97236.22 487967.76</gml:posList>
                        </gml:LineString>
                    </imaer:GM_Curve>
                </imaer:EmissionSourceGeometry>
            </imaer:geometry>
            <imaer:emission>
                <imaer:Emission substance="NH3">
                    <imaer:value>199.9570667</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NOX">
                    <imaer:value>2660.18902515</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="PM10">
                    <imaer:value>151.33698619999998</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:emission>
                <imaer:Emission substance="NO2">
                    <imaer:value>812.4583107</imaer:value>
                </imaer:Emission>
            </imaer:emission>
            <imaer:vehicles>
                <imaer:StandardVehicle vehicleType="LIGHT_TRAFFIC">
                    <imaer:vehiclesPerDay>1000.0</imaer:vehiclesPerDay>
                    <imaer:stagnationFactor>0.0</imaer:stagnationFactor>
                </imaer:StandardVehicle>
            </imaer:vehicles>
            <imaer:isFreeway>true</imaer:isFreeway>
            <imaer:maximumSpeed>130</imaer:maximumSpeed>
            <imaer:strictEnforcement>false</imaer:strictEnforcement>
            <imaer:elevation>NORMAL</imaer:elevation>
        </imaer:SRM2Road>
        <imaer:CalculationPoint gml:id="CP.1">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.1</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.1.POINT">
                    <gml:pos>141626.0 458913.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:label>rekenpunt 1</imaer:label>
        </imaer:CalculationPoint>
    </imaer:featureMembers>
</imaer:FeatureCollectionCalculator>
