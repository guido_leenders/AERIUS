<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<imaer:FeatureCollectionCalculator xmlns:imaer="http://imaer.aerius.nl/1.1" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="NL.IMAER.Collection" xsi:schemaLocation="http://imaer.aerius.nl/1.1 http://imaer.aerius.nl/1.1/IMAER.xsd">
    <imaer:aeriusCalculatorMetaData>
        <imaer:year>2016</imaer:year>
        <imaer:version>1.0-SNAPSHOT_20160203_2b8e9c2706</imaer:version>
        <imaer:databaseVersion>1.0-SNAPSHOT_20160203_2b8e9c2706</imaer:databaseVersion>
        <imaer:situationName>Situatie 1</imaer:situationName>
        <imaer:reference>Rjx1hWrQ4xBE</imaer:reference>
    </imaer:aeriusCalculatorMetaData>
    <imaer:featureMembers>
        <imaer:ReceptorPoint receptorPointId="4369291" gml:id="CP.4369291">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4369291</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4369291.POINT">
                    <gml:pos>178092.0 450302.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.REPR.4369291">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>178123.0 450356.0 178154.0 450302.0 178123.0 450249.0 178061.0 450249.0 178030.0 450302.0 178061.0 450356.0 178123.0 450356.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.273</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0223</imaer:value>
                </imaer:Result>
            </imaer:result>
        </imaer:ReceptorPoint>
        <imaer:ReceptorPoint receptorPointId="4369290" gml:id="CP.4369290">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4369290</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4369290.POINT">
                    <gml:pos>177906.0 450302.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.REPR.4369290">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>177937.0 450356.0 177968.0 450302.0 177937.0 450249.0 177875.0 450249.0 177844.0 450302.0 177875.0 450356.0 177937.0 450356.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.999</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.0225</imaer:value>
                </imaer:Result>
            </imaer:result>
        </imaer:ReceptorPoint>
        <imaer:ReceptorPoint receptorPointId="4369289" gml:id="CP.4369289">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4369289</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4369289.POINT">
                    <gml:pos>177720.0 450302.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.REPR.4369289">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>177751.0 450356.0 177782.0 450302.0 177751.0 450249.0 177689.0 450249.0 177658.0 450302.0 177689.0 450356.0 177751.0 450356.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.27</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.02239</imaer:value>
                </imaer:Result>
            </imaer:result>
        </imaer:ReceptorPoint>
        <imaer:ReceptorPoint receptorPointId="4369288" gml:id="CP.4369288">
            <imaer:identifier>
                <imaer:NEN3610ID>
                    <imaer:namespace>NL.IMAER</imaer:namespace>
                    <imaer:localId>CP.4369288</imaer:localId>
                </imaer:NEN3610ID>
            </imaer:identifier>
            <imaer:GM_Point>
                <gml:Point srsName="urn:ogc:def:crs:EPSG::28992" gml:id="CP.4369288.POINT">
                    <gml:pos>177534.0 450302.0</gml:pos>
                </gml:Point>
            </imaer:GM_Point>
            <imaer:representation>
                <gml:Polygon srsName="urn:ogc:def:crs:EPSG::28992" gml:id="NL.IMAER.REPR.4369288">
                    <gml:exterior>
                        <gml:LinearRing>
                            <gml:posList>177565.0 450356.0 177596.0 450302.0 177565.0 450249.0 177503.0 450249.0 177472.0 450302.0 177503.0 450356.0 177565.0 450356.0</gml:posList>
                        </gml:LinearRing>
                    </gml:exterior>
                </gml:Polygon>
            </imaer:representation>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NH3">
                    <imaer:value>0.272</imaer:value>
                </imaer:Result>
            </imaer:result>
            <imaer:result>
                <imaer:Result resultType="DEPOSITION" substance="NOX">
                    <imaer:value>0.999</imaer:value>
                </imaer:Result>
            </imaer:result>
        </imaer:ReceptorPoint>
    </imaer:featureMembers>
</imaer:FeatureCollectionCalculator>
