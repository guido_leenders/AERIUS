/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.impl.util.Base64;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.ConvertResponse;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ConvertApiServiceImpl}.
 */
public class ConvertApiServiceImplTest extends BaseDBTest {
  private static final String CONTENT_TYPE_NOT_BASE64 = "ContentType not BASE64";
  private static final String CONTENT_TYPE_NOT_TEXT = "ContentType not TEXT";
  private static final String MISSING_FILE_DATA = "Missing file data.";
  private static final String CONVERSION_FAILED = "Conversion failed.";
  private static final String DATA_TYPE_NOT_GML = "DataType not GML";

  private ConvertApiServiceImpl converterService;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    converterService = new ConvertApiServiceImpl(new ConnectServiceContext(getCalcPMF(), null));
  }

  @Test
  public void testGmlConvertOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("beach_race_track.gml");
    final ConvertResponse result = converterService.convert(DataType.GML, ContentType.TEXT, content, null);

    assertTrue(CONVERSION_FAILED, result.getSuccessful());
    assertNotNull(MISSING_FILE_DATA, result.getDataObject().getData());
    assertSame(CONTENT_TYPE_NOT_TEXT, ContentType.TEXT, result.getDataObject().getContentType());
    assertSame(DATA_TYPE_NOT_GML, DataType.GML, result.getDataObject().getDataType());
  }

  @Test
  public void testGmlConvertOkBase64ForceTextResult() throws IOException, AeriusException {
    final String content = FileUtil.readFile("beach_race_track.gml");
    final String base64Content = new String(Base64.encode(content.getBytes(StandardCharsets.UTF_8)));
    final ConvertResponse result = converterService.convert(DataType.GML, ContentType.BASE64, base64Content, null);

    assertTrue(CONVERSION_FAILED, result.getSuccessful());
    assertNotNull(MISSING_FILE_DATA, result.getDataObject().getData());
    assertSame(CONTENT_TYPE_NOT_TEXT, ContentType.TEXT, result.getDataObject().getContentType());
    assertSame(DATA_TYPE_NOT_GML, DataType.GML, result.getDataObject().getDataType());
  }

  @Test
  public void testGmlConvertOkZip() throws IOException, AeriusException {
    final String content = new String(Base64.encode(IOUtils.toByteArray(FileUtil.class.getResourceAsStream("beach_race_track.zip"))));
    final ConvertResponse result = converterService.convert(DataType.ZIP, ContentType.BASE64, content, null);

    assertTrue(CONVERSION_FAILED, result.getSuccessful());
    assertNotNull(MISSING_FILE_DATA, result.getDataObject().getData());
    assertSame(CONTENT_TYPE_NOT_BASE64, ContentType.BASE64, result.getDataObject().getContentType());
    assertSame("DataType not ZIP", DataType.ZIP, result.getDataObject().getDataType());
  }

  @Test
  public void testGmlConvertNotOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("farm_case_1.gml");
    final ConvertResponse result = converterService.convert(DataType.GML, ContentType.TEXT, content, null);

    assertFalse("Conversion gml succesful, while expected otherwise.", result.getSuccessful());
  }

  @Test
  public void testCsvConvertOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("srm2_roads_example.csv");
    final ConvertResponse result = converterService.convert(DataType.CSV, ContentType.TEXT, content, null);

    assertTrue(CONVERSION_FAILED, result.getSuccessful());
    assertNotNull(MISSING_FILE_DATA, result.getDataObject().getData());
    assertSame(CONTENT_TYPE_NOT_TEXT, ContentType.TEXT, result.getDataObject().getContentType());
    assertSame(DATA_TYPE_NOT_GML, DataType.GML, result.getDataObject().getDataType());
  }

  @Test
  public void testCsvConvertNotOk() throws IOException, AeriusException {
    final String content = FileUtil.readFile("srm2_roads_invalid.csv");
    final ConvertResponse result = converterService.convert(DataType.CSV, ContentType.TEXT, content, null);

    assertFalse("Conversion csv succesful, while expected otherwise.", result.getSuccessful());
  }
}
