/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOutputOptions;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.SituationDataObject;
import nl.overheid.aerius.connect.domain.SituationType;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Test class for {@link CalculateDeltaApiServiceImpl}.
 */
public class CalculateDeltaApiServiceImplTest extends TestBaseService {

  private static final String TEST_EMAIL = "aerius@example.com";
  private CalculateDeltaApiServiceImpl service;
  private ScenarioUser testUser;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    service = new CalculateDeltaApiServiceImpl(new ConnectServiceContext(getCalcPMF(), new TaskManagerClient(factory)));
    // create temp user
    UserUtil.generateAPIKey(getCalcPMF().getConnection(), TEST_EMAIL);
    testUser = ScenarioUserRepository.getUserByEmailAddress(getCalcPMF().getConnection(), TEST_EMAIL);

  }

  @Test
  public void testCalculateDeltaAndEmailNBWet() throws AeriusException, SQLException, IOException {
    // tests that if no PermitReportType is specified that it still works.
    final ValidateResponse result = service.processCalculateDelta(getExampleOptions(), getExampleListDataObject(), testUser.getApiKey());
    assertTrue("Report didn't start successful", result.getSuccessful());
    assertJobCreated();
  }

  @Test
  public void testCalculateDeltaAndEmailCustomPoints() throws AeriusException, SQLException, IOException {
    final CalculationOptions options = getExampleOptions();
    options.setCalculationType(CalculationTypeEnum.CUSTOM_POINTS);
    final ValidateResponse result = service.processCalculateDelta(options, getExampleListDataObject(), testUser.getApiKey());
    assertTrue("Report didn't start successful", result.getSuccessful());
    assertJobCreated();
  }

  @Test
  public void testCalculateComparisonReportAndEmailNBWet() throws IOException, AeriusException, SQLException {
    final List<SituationDataObject> reportDataObjects = getExampleListDataObject();
    reportDataObjects.add(getDataObject().situationType(SituationType.CURRENT));
    final ValidateResponse result = service.processCalculateDelta(getExampleOptions(), reportDataObjects, testUser.getApiKey());
    assertTrue("Report didn't start successful", result.getSuccessful());
    assertJobCreated();
  }

  @Test
  public void testProcessReportOK() throws AeriusException, IOException, SQLException {
    final ValidateResponse result = service.processCalculateDelta(getExampleOptions(), getExampleListDataObject(), testUser.getApiKey());
    assertTrue("Report didn't finish successfully", result.getSuccessful());
    assertJobCreated();
  }

  @Test(expected = AeriusException.class)
  public void testProcessReportNoProposed() throws AeriusException, IOException, SQLException {
    final List<SituationDataObject> dataObjects = getExampleListDataObject();
    dataObjects.get(0).situationType(SituationType.CURRENT);

    service.processCalculateDelta(getExampleOptions(), dataObjects, testUser.getApiKey());
    assertJobNotCreated();
    fail("Generating report should have failed because proposed is missing");
  }

  private void assertJobCreated() throws SQLException {
    assertAmountOfJobs(1);
  }

  private void assertJobNotCreated() throws SQLException {
    assertAmountOfJobs(0);
  }

  private void assertAmountOfJobs(final int amount) throws SQLException {
    assertEquals("Amount of jobs different than expected", JobRepository.getProgressForUser(getCalcConnection(), testUser).size(), amount);
  }

  private List<SituationDataObject> getExampleListDataObject() throws IOException {
    final List<SituationDataObject> list = new ArrayList<>();
    list.add(getDataObject().situationType(SituationType.PROPOSED));

    return list;
  }

  private SituationDataObject getDataObject() throws IOException {
    final SituationDataObject example = (SituationDataObject) new SituationDataObject()
        .contentType(ContentType.TEXT)
        .dataType(DataType.GML)
        .data(FileUtil.readFile("beach_race_track.gml"));

    return example;
  }

  private CalculationOptions getExampleOptions() {
    final CalculationOptions options = new CalculationOptions();
    options.setCalculationType(CalculationTypeEnum.NBWET);
    options.setSubstances(new ArrayList<Substance>());
    options.getSubstances().add(Substance.NOX);
    options.setYear(2020);

    final CalculationOutputOptions outputOptions = new CalculationOutputOptions();
    options.setOutputOptions(outputOptions);

    return options;
  }

}
