/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.UuidUtil;

/**
 * Test class for {@link CancelJobApiServiceImpl}.
 */
public class CancelJobApiServiceImplTest extends TestBaseService {

  private static final String BAD_API_KEY = "badKey";
  private static final String BAD_JOB_KEY = "badJobKey";
  private static final String TEST_EMAIL = "aerius@aerius.nl";

  private CancelJobApiServiceImpl cancelService;
  private ScenarioUser user;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    cancelService = new CancelJobApiServiceImpl(new ConnectServiceContext(getCalcPMF(), null));
    user = createScenarioUser();
  }

  @Test(expected = AeriusException.class)
  public void testBadAPIKeyAndJobKey() throws IOException, AeriusException {
    try {
      cancelService.cancelJob(BAD_API_KEY, BAD_JOB_KEY);
    } catch (final AeriusException e) {
      assertEquals("Expected USER_INVALID_API_KEY as reason", Reason.USER_INVALID_API_KEY, e.getReason());
      throw e;
    }
  }


  @Test(expected = AeriusException.class)
  public void testBadJobKeyOnly() throws IOException, AeriusException, SQLException {
    try {
      cancelService.cancelJob(user.getApiKey(), BAD_JOB_KEY);
    } catch (final AeriusException e) {
      assertEquals("Expected CONNECT_USER_JOBKEY_DOES_NOT_EXIST as reason", Reason.CONNECT_USER_JOBKEY_DOES_NOT_EXIST, e.getReason());
      throw e;
    }
  }

  @Test
  public void testCancel() throws SQLException, AeriusException {
    final String jobKey = createJob(user);
    final ValidateResponse response = cancelService.cancelJob(user.getApiKey(), jobKey);
    assertTrue("Cancel should succeed", response.getSuccessful());
  }

  @Test
  public void testCancelWithMaxJobs() throws SQLException, AeriusException {
    String jobKey = null;
    for (int i = 0; i < user.getMaxConcurrentJobs(); i++) {
      jobKey = createJob(user);
    }

    // We should be able to cancel even if we're at max.
    final ValidateResponse response = cancelService.cancelJob(user.getApiKey(), jobKey);
    assertTrue("Cancel should succeed", response.getSuccessful());
  }

  private String createJob(final ScenarioUser user) throws SQLException {
    final String jobKey = UuidUtil.getStripped();

    JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, jobKey);

    return jobKey;
  }

  private ScenarioUser createScenarioUser() throws AeriusException, SQLException {
    return UserUtil.generateAPIKey(getCalcConnection(), TEST_EMAIL);
  }
}
