/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.CalculateDataObject;
import nl.overheid.aerius.connect.domain.CalculateResponse;
import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOutputOptions;
import nl.overheid.aerius.connect.domain.CalculationOutputOptions.OutputTypeEnum;
import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.domain.ValidationMessage;
import nl.overheid.aerius.connect.service.util.AeriusExceptionConversionUtil;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Test class for {@link CalculateApiServiceImpl}.
 */
public class CalculateApiServiceImplTest extends TestBaseService {

  private static final String TEST_EMAIL = "aerius@example.com";
  private ScenarioUser testUser;
  private CalculateApiServiceImpl service;
  private ReceptorSetApiServiceImpl receptorService;
  private static final String RECEPTOR_SET_NAME = "aReceptorSetName";
  private static final String RECEPTOR_SET_DESCRIPTION = "aReceptorSetDescription";

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    receptorService = new ReceptorSetApiServiceImpl(new ConnectServiceContext(getCalcPMF(), new TaskManagerClient(factory)));
    service = new CalculateApiServiceImpl(new ConnectServiceContext(getCalcPMF(), new TaskManagerClient(factory)));
    // create temp user
    UserUtil.generateAPIKey(getCalcPMF().getConnection(), TEST_EMAIL);
    testUser = ScenarioUserRepository.getUserByEmailAddress(getCalcPMF().getConnection(), TEST_EMAIL);
  }

  @Test
  public void testCalculateAndEmail() throws AeriusException, IOException, SQLException {
    final List<CalculateDataObject> dataCalculationObjects = new ArrayList<>();
    final CalculationOptions options = getExampleOptions();
    dataCalculationObjects.add(getExampleCalculateDataObject());
    final CalculateResponse result = service.calculate(options, dataCalculationObjects, false, testUser.getApiKey());
    assertTrue("Calculation didn't start successful", result.getSuccessful());
    assertJobCreated();
  }

  @Test
  public void testCalculate() throws AeriusException, IOException, InterruptedException, AeriusException, SQLException {
    final List<CalculateDataObject> dataCalculationObjects = new ArrayList<>();
    dataCalculationObjects.add(getExampleCalculateDataObject());
    assertCalculation(getExampleOptions(), dataCalculationObjects, true);
    assertJobCreated();
  }

  @Test
  public void testCalculateCsv() throws AeriusException, IOException, InterruptedException, AeriusException, SQLException {
    final List<CalculateDataObject> dataCalculationObjects = new ArrayList<>();
    dataCalculationObjects.add(getExampleCalculateDataObject());
    final CalculationOptions options = getExampleOptions();
    options.calculationType(CalculationTypeEnum.CUSTOM_POINTS);
    options.getOutputOptions().outputType(OutputTypeEnum.CSV);
    assertCalculation(options, dataCalculationObjects, true);
    assertJobCreated();
  }

  @Test
  public void testCalculateWithValidationDisabled() throws AeriusException, IOException, SQLException {
    final List<CalculateDataObject> dataCalculationObjects = new ArrayList<>();
    final CalculationOptions options = getExampleOptions().calculationType(CalculationTypeEnum.CUSTOM_POINTS);
    dataCalculationObjects.add(getExampleCalculateDataObject("beach_race_track_bad.gml"));
    CalculateResponse result = service.calculate(options, dataCalculationObjects, false, testUser.getApiKey());
    assertFalse("Calculation started while it should not pass the validation", result.getSuccessful());
    assertJobNotCreated();

    // Let's try a run without validation on - the file will not pass schema validation but apart from that is fine and should work
    options.validate(false);
    result = service.calculate(options, dataCalculationObjects, false, testUser.getApiKey());
    assertTrue("Calculation didn't start while it should work with the validation disabled", result.getSuccessful());
    assertJobCreated();
  }

  @Test
  public void testCalculateStoredCustomPoints() throws AeriusException, IOException, InterruptedException, SQLException {
    final DataObject receptorSetDataObject = getExampleDataObject();
    final ValidateResponse response =
        receptorService.addReceptorSetResponse(receptorSetDataObject, RECEPTOR_SET_NAME, RECEPTOR_SET_DESCRIPTION, testUser.getApiKey());
    assertTrue("ReceptorSet is added", response.getSuccessful());
    final List<CalculateDataObject> dataCalculationObjects = new ArrayList<>();
    dataCalculationObjects.add(getExampleCalculateDataObject());
    final CalculationOptions options = getExampleOptions();
    options.setReceptorSetName(RECEPTOR_SET_NAME);
    assertCalculation(options, dataCalculationObjects, true);
    assertJobCreated();
  }

  private List<AeriusException> assertCalculation(final CalculationOptions options, final List<CalculateDataObject> dataObjects,
      final boolean success) throws AeriusException, SQLException, IOException, InterruptedException {
    final List<AeriusException> calculationErrors = new ArrayList<>();
    final CalculateResponse result = service.calculate(options, dataObjects, Boolean.FALSE, testUser.getApiKey());
    final boolean isSuccessful = result.getSuccessful();
    assertEquals("Calculation started and not expected success", success, isSuccessful);
    if (!isSuccessful) {
      for (final ValidationMessage vm : result.getErrors()) {
        calculationErrors.add(AeriusExceptionConversionUtil.convert(vm));
      }
    }
    assertEquals("Should have no calculation errors" + ArrayUtils.toString(calculationErrors), success, calculationErrors.isEmpty());
    return calculationErrors;
  }

  private void assertJobCreated() throws SQLException {
    assertAmountOfJobs(1);
  }

  private void assertJobNotCreated() throws SQLException {
    assertAmountOfJobs(0);
  }

  private void assertAmountOfJobs(final int amount) throws SQLException {
    assertEquals("Amount of jobs different than expected", JobRepository.getProgressForUser(getCalcConnection(), testUser).size(), amount);
  }

  private CalculateDataObject getExampleCalculateDataObject() throws IOException {
    return getExampleCalculateDataObject("beach_race_track.gml");
  }

  private CalculateDataObject getExampleCalculateDataObject(final String filename) throws IOException {
    final CalculateDataObject example = (CalculateDataObject) new CalculateDataObject()
        .contentType(ContentType.TEXT)
        .dataType(DataType.GML)
        .data(FileUtil.readFile(filename));

    return example;
  }

  private CalculationOptions getExampleOptions() {
    final CalculationOptions options = new CalculationOptions()
        .calculationType(CalculationTypeEnum.NBWET)
        .year(2020)
        .substances(new ArrayList<Substance>());
    options.getSubstances().add(Substance.NOX);

    final CalculationOutputOptions outputOptions = new CalculationOutputOptions().sectorOutput(false);

    options.setOutputOptions(outputOptions);
    return options;
  }

  private DataObject getExampleDataObject() throws IOException {
    final DataObject example = new DataObject()
        .contentType(ContentType.BASE64)
        .dataType(DataType.ZIP)
        .data("UEsDBBQAAAAIAFahWk7f6mf3CQEAAH0CAAAPAAAAa2xlaW50amVfMTAucmNwZZE/SwRBDMX7+xRphBX0yN/JpLcRBCvrLSz0hLNQEPXTm5292TmVgc1beL9J8ub2Bu6P74"
            + "/Pb4eXj8PrE3zO0/HyCr6WAt/Yyt3DPF23QvN00QR3IV1oF9ZF6cK7qF3EInYINRzIUUoFjlIRgfYFHPDsELa6WxQbUCgFdzvuUYCaZ5zNTybAVJFs+N1At5v/+KOYZgerag6S"
            + "Y7knQQV4c/8jiucAlbniGYFWSRsVYxkZUI4EjIVQB5Q9o0Qu49TsvH79REkwRbbSNqNkC9OkJNSxNrNHb5VLD0ryZ814UEwlLNYBy0bpgFSjJz1a5Zq0pG2yBkIgDRmYuZ8CH1"
            + "hmgbyslY9Wfyf4A1BLAQI/ABQAAAAIAFahWk7f6mf3CQEAAH0CAAAPACQAAAAAAAAAIAAAAAAAAABrbGVpbnRqZV8xMC5yY3AKACAAAAAAAAEAGABbbp33Bs7UAVtunfcGztQB"
            + "/qOh2gbO1AFQSwUGAAAAAAEAAQBhAAAANgEAAAAA");
    return example;
  }
}
