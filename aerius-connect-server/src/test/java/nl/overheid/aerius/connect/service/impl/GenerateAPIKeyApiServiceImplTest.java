/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Test class for {@link GenerateAPIKeyApiServiceImpl}.
 */
public class GenerateAPIKeyApiServiceImplTest extends TestBaseService {

  private static final String TEST_EMAIL = "aerius@example.com";
  private static final String TEST_EMAIL_FAIL = "aerius@@example.com";
  private GenerateAPIKeyApiServiceImpl service;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    service = new GenerateAPIKeyApiServiceImpl(new ConnectServiceContext(getCalcPMF(), new TaskManagerClient(factory)));
  }

  @Test
  public void testGenerateAPIKey() throws SQLException, AeriusException {
    final ValidateResponse result = service.generateAPIKey(TEST_EMAIL);
    final ScenarioUser senarioUser = ScenarioUserRepository.getUserByEmailAddress(getCalcPMF().getConnection(), TEST_EMAIL);
    assertNotNull("Result is null", result);
    assertTrue("Result errors is not empty", result.getErrors().isEmpty());
    assertTrue("Result warnings is not empty", result.getWarnings().isEmpty());
    assertEquals("API key length invalid", senarioUser.getApiKey().length(), 32);
  }

  @Test
  public void testGenerateAPIKeyExistingUser() throws SQLException, AeriusException {
    final ValidateResponse result = service.generateAPIKey(TEST_EMAIL);
    final ScenarioUser senarioUser = ScenarioUserRepository.getUserByEmailAddress(getCalcPMF().getConnection(), TEST_EMAIL);
    final ValidateResponse updatedResult = service.generateAPIKey(TEST_EMAIL);
    final ScenarioUser senarioUpdatedUser = ScenarioUserRepository.getUserByEmailAddress(getCalcPMF().getConnection(), TEST_EMAIL);
    assertNotNull("Result is null", result);
    assertNotNull("UpdatedResult is null", updatedResult);
    assertNotEquals("API key before and after reset not different", senarioUser.getApiKey(), senarioUpdatedUser.getApiKey());
  }

  @Test(expected = AeriusException.class)
  public void testGenerateAPIKeyInvalidEmail() throws AeriusException {
    try {
      service.generateAPIKey(TEST_EMAIL_FAIL);
      fail("should throw error");
    } catch (final AeriusException e) {
      assertEquals("Should throw CONNECT_NO_VALID_EMAIL_SUPPLIED", e.getReason(), Reason.CONNECT_NO_VALID_EMAIL_SUPPLIED);
      throw e;
    }
  }

  @Test(expected = AeriusException.class)
  public void testGenerateAPIKeyWhenDisabled() throws SQLException, AeriusException {
    ConstantRepository.setString(getCalcPMF().getConnection(), ConstantsEnum.CONNECT_GENERATING_APIKEY_ENABLED, "false");

    try {
      service.generateAPIKey(TEST_EMAIL);
    } catch (final AeriusException e) {
      assertEquals("Should throw USER_API_KEY_GENERATION_DISABLED", e.getReason(), Reason.USER_API_KEY_GENERATION_DISABLED);
      throw e;
    }
  }
}
