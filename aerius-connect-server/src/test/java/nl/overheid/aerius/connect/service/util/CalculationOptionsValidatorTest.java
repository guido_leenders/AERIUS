/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.CalculationOptions;
import nl.overheid.aerius.connect.domain.CalculationOptions.CalculationTypeEnum;
import nl.overheid.aerius.connect.domain.CalculationOptions.PermitCalculationRadiusTypeEnum;
import nl.overheid.aerius.connect.domain.Substance;
import nl.overheid.aerius.connect.service.util.CalculationOptionsValidator.CalculationOptionsLimits;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.LocaleUtils;

/**
 * Test class for {@link CalculationOptionsValidator}
 */
public class CalculationOptionsValidatorTest extends BaseDBTest {

  private static final String TEST_EMAIL = "aerius@example.com";

  private CalculationOptionsValidator validator;

  @Override
  @Before
  public void setUp() throws SQLException, AeriusException {
    final Context context = new Context();
    context.setSetting(SharedConstantsEnum.MIN_YEAR, 1900);
    context.setSetting(SharedConstantsEnum.MAX_YEAR, 2100);
    context.setSetting(SharedConstantsEnum.CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI, 100);
    validator = new CalculationOptionsValidator(new CalculationOptionsLimits(context), getCalcConnection());
    UserUtil.generateAPIKey(getCalcConnection(), TEST_EMAIL);
  }

  @Test
  public void testValidateValidOptions() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    final ScenarioUser user = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);
    validator.validate(options, user, LocaleUtils.getDefaultLocale());
  }

  @Test
  public void testValidateInvalidYear() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    options.setYear(0);
    assertException(Reason.CONNECT_INCORRECT_CALCULATIONYEAR, options);
  }

  @Test
  public void testValidateInvalidCalculationType() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    options.setCalculationType(null);
    assertException(Reason.CONNECT_NO_CALCULATIONTYPE_SUPPLIED, options);
  }

  @Test
  public void testValidateInvalidReceptorSetName() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    options.setCalculationType(CalculationTypeEnum.CUSTOM_POINTS);
    options.setReceptorSetName("nonexistent");
    assertException(Reason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST, options);
  }

  @Test
  public void testValidateNoSubstances() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    options.setSubstances(new ArrayList<Substance>());
    assertException(Reason.CONNECT_NO_SUBSTANCE_SUPPLIED, options);
  }

  @Test
  public void testValidateInvalidSubstances() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    final ArrayList<Substance> substances = new ArrayList<Substance>();
    substances.add(Substance.NH3);
    substances.add(null);
    options.setSubstances(substances);
    assertException(Reason.CONNECT_UNKNOWN_SUBSTANCE_SUPPLIED, options);
  }

  @Test
  public void testValidateInvalidCalculationRange() throws AeriusException, SQLException {
    final CalculationOptions options = getValidOptions();
    options.setRange(10000);
    assertException(Reason.CONNECT_INVALID_CALCULATION_RANGE, options);
  }

  @Test
  public void testValidateNBWetWithInvalidTempProjectYears() throws AeriusException, SQLException {
    final CalculationOptions options = createCalculationOptions(CalculationTypeEnum.NBWET, 1);
    final ScenarioUser user = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);

    try {
      validator.validate(options, user, Locale.getDefault());
      fail("A positive value for temporary project duration should not result in a successfull validation.");
    } catch (final AeriusException e) {
      assertSame("A positive value for temporary project duration should result in an Exception with a relevant message.",
          Reason.CONNECT_UNSUPPORTED_PAS_OPTIONS, e.getReason());
    }
  }

  @Test
  public void testValidateNBWetWithInvalidDistanceLimit() throws AeriusException, SQLException {
    final CalculationOptions options = createCalculationOptions(CalculationTypeEnum.NBWET, 1);
    options.setTempProjectYears(null);
    options.setPermitCalculationRadiusType(PermitCalculationRadiusTypeEnum.PRIORITY_PROJECT_MAIN_ROADS);
    final ScenarioUser user = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);
    try {
      validator.validate(options, user, Locale.getDefault());
      fail("A not NONE value for the PermitCalculationRadiusType should not result in a successfull validation.");
    } catch (final AeriusException e) {
      assertSame("A not NONE value for the PermitCalculationRadiusType should result in an Exception with a relevant message.",
          Reason.GENERAL_UNSUPPORTED_PAS_OPTIONS, e.getReason());
    }
  }

  private CalculationOptions getValidOptions() {
    return createCalculationOptions(CalculationTypeEnum.RADIUS, null);
  }

  private CalculationOptions createCalculationOptions(final CalculationTypeEnum calcType, final Integer tempYears) {
    final CalculationOptions options = new CalculationOptions();
    options.setYear(2050);
    options.setSubstances(new ArrayList<Substance>());
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NH3);
    options.setCalculationType(calcType);
    options.setRange(10);
    options.setTempProjectYears(tempYears);
    return options;
  }

  private void assertException(final Reason expectedReason, final CalculationOptions options) throws AeriusException, SQLException {
    try {
      final ScenarioUser user = ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);
      validator.validate(options, user, Locale.getDefault());
    } catch (final AeriusException e) {
      assertEquals("Expected Reason " + expectedReason, expectedReason, e.getReason());
    }
  }
}
