/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.connect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.connect.domain.ConnectServiceContext;
import nl.overheid.aerius.connect.domain.ContentType;
import nl.overheid.aerius.connect.domain.DataObject;
import nl.overheid.aerius.connect.domain.DataType;
import nl.overheid.aerius.connect.domain.ReceptorSet;
import nl.overheid.aerius.connect.domain.ReceptorSetResponse;
import nl.overheid.aerius.connect.domain.ValidateResponse;
import nl.overheid.aerius.connect.service.util.UserUtil;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;

/**
 * Test class for {@link ReceptorSetApiServiceImpl}.
 */
public class ReceptorSetApiServiceImplTest extends TestBaseService {

  private static final String RECEPTOR_SET_NAME = "aReceptorSetName";
  private static final String RECEPTOR_SET_DESCRIPTION = "aReceptorSetDescription";

  private static final String ADDING_RECEPTOR_SET_FOR_TESTUSER_DID_NOT_SUCCEED = "Adding a receptorSet for testuser did not succeed.";
  private static final String TESTUSER_SHOULD_NOT_HAVE_ANY_RECEPTOR_SETS_TO_BEGIN_WITH = "Testuser should not have any receptorSets to begin with.";
  private static final String TEST_EMAIL = "aerius@example.com";

  private ReceptorSetApiServiceImpl service;
  private ReceptorSetsApiServiceImpl setsService;

  private ScenarioUser scenarioUser;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    ConnectServiceContext connectServiceContext = new ConnectServiceContext(getCalcPMF(), new TaskManagerClient(factory));
    service = new ReceptorSetApiServiceImpl(connectServiceContext);
    setsService = new ReceptorSetsApiServiceImpl(connectServiceContext);
    // create temp user
    scenarioUser = UserUtil.generateAPIKey(getCalcPMF().getConnection(), TEST_EMAIL);
  }

  @Test
  public void receptorSetListInitial() throws Exception {
    final List<ReceptorSet> startList = getReceptorSets4User(scenarioUser.getApiKey());
    assertTrue(TESTUSER_SHOULD_NOT_HAVE_ANY_RECEPTOR_SETS_TO_BEGIN_WITH, startList.isEmpty());
  }

  @Test
  public void receptorSetAdd() throws Exception {
    final String apiKey = scenarioUser.getApiKey();
    final List<ReceptorSet> startList = getReceptorSets4User(apiKey);
    assertTrue(ADDING_RECEPTOR_SET_FOR_TESTUSER_DID_NOT_SUCCEED, addAllowedreceptorSet4User(apiKey));
    final List<ReceptorSet> afterList = getReceptorSets4User(apiKey);
    assertEquals("After successfully adding legal receptorSet for testuser list should be one longer.", startList.size() + 1, afterList.size());
  }

  @Test
  public void receptorSetAddDuplicate() throws Exception {
    final String apiKey = scenarioUser.getApiKey();
    final List<ReceptorSet> startList = getReceptorSets4User(apiKey);
    addAllowedreceptorSet4User(apiKey);
    final int afterFirstSet = getReceptorSets4User(apiKey).size();
    try {
      addAllowedreceptorSet4User(apiKey);
      fail("Adding duplicate should throw an AeriusException.");
    } catch (final AeriusException e) {
      assertEquals("Adding duplicate receptorSet for testuser throws unexpected Exception", Reason.CONNECT_USER_CALCULATION_POINT_SET_ALREADY_EXISTS,
          e.getReason());
    }
    final List<ReceptorSet> afterList = getReceptorSets4User(apiKey);
    assertEquals("After adding the same receptorSet twice the list should be one longer.", startList.size() + 1, afterList.size());
  }

  @Test
  public void receptorSetDeleteNonExisting() throws Exception {
    final String apiKey = scenarioUser.getApiKey();
    final List<ReceptorSet> startList = getReceptorSets4User(apiKey);
    try {
      assertFalse("UserReceptorsetList should not contain example set to begin with.", listContainsSetWithName(startList, RECEPTOR_SET_NAME));
      deleteExampleReceptorSet4User(apiKey);
      fail("Deleting nonexistent receptorset should throw an AeriusException.");
    } catch (final AeriusException e) {
      assertEquals("Deleting nonexistent receptorSet for testuser throws unexpected Exception",
          Reason.CONNECT_USER_CALCULATION_POINT_SET_DOES_NOT_EXIST,
          e.getReason());
    }
  }

  @Test
  public void receptorSetAddAndDelete() throws Exception {
    final String apiKey = scenarioUser.getApiKey();
    final List<ReceptorSet> startList = getReceptorSets4User(apiKey);
    assertTrue("Adding allowed receptorset should succeed.", addAllowedreceptorSet4User(apiKey));
    assertTrue("Deleting existing receptorSet for testuser should succeed.", deleteExampleReceptorSet4User(apiKey));
    final List<ReceptorSet> afterList = getReceptorSets4User(apiKey);
    assertEquals("Adding and deleting receptorSet should result in the same length list.", startList.size(), afterList.size());
  }

  @Test
  public void receptorSetAddDisallowedFileType() throws Exception {
    final String apiKey = scenarioUser.getApiKey();
    final List<ReceptorSet> startList = getReceptorSets4User(apiKey);
    assertFalse("Adding receptors from disallowed fileformat should not succeed.", addDisallowedeceptorSet4User(apiKey));
    final List<ReceptorSet> afterList = getReceptorSets4User(apiKey);
    assertEquals("After failed addition of a receptorSet should result in the same length list.", startList.size(), afterList.size());
  }

  public List<ReceptorSet> getReceptorSets4User(final String apiKey) throws Exception {
    final ReceptorSetResponse response = setsService.getReceptorSetsForUser(apiKey);
    return response.getReceptorSets();
  }

  public boolean addAllowedreceptorSet4User(final String apiKey) throws Exception {
    final DataObject receptorSetDataObject = getExampleAllowedReceptorDataObject();
    final ValidateResponse response = service.addReceptorSetResponse(receptorSetDataObject, RECEPTOR_SET_NAME, RECEPTOR_SET_DESCRIPTION, apiKey);
    return response.getSuccessful();
  }

  public boolean addDisallowedeceptorSet4User(final String apiKey) throws Exception {
    final DataObject receptorSetDataObject = getExampleDisallowedReceptorDataObject();
    final ValidateResponse response = service.addReceptorSetResponse(receptorSetDataObject, RECEPTOR_SET_NAME, RECEPTOR_SET_DESCRIPTION, apiKey);
    return response.getSuccessful();
  }

  public boolean deleteExampleReceptorSet4User(final String apiKey) throws Exception {
    final ValidateResponse response = service.deleteReceptorSetByName(apiKey, RECEPTOR_SET_NAME);
    return response.getSuccessful();
  }

  public boolean listContainsSetWithName(final List<ReceptorSet> list, final String setName) throws Exception {
    return list.stream().filter(r -> StringUtils.equals(setName, r.getName())).findFirst().isPresent();
  }

  private DataObject getExampleAllowedReceptorDataObject() throws IOException {
    final DataObject example = new DataObject()
        .contentType(ContentType.BASE64)
        .dataType(DataType.ZIP)
        .data("UEsDBBQAAAAIAFahWk7f6mf3CQEAAH0CAAAPAAAAa2xlaW50amVfMTAucmNwZZE/SwRBDMX7+xRphBX0yN/JpLcRBCvrLSz0hLNQEPXTm5292TmVgc1beL9J8ub2Bu6P74"
            + "/Pb4eXj8PrE3zO0/HyCr6WAt/Yyt3DPF23QvN00QR3IV1oF9ZF6cK7qF3EInYINRzIUUoFjlIRgfYFHPDsELa6WxQbUCgFdzvuUYCaZ5zNTybAVJFs+N1At5v/+KOYZgerag6S"
            + "Y7knQQV4c/8jiucAlbniGYFWSRsVYxkZUI4EjIVQB5Q9o0Qu49TsvH79REkwRbbSNqNkC9OkJNSxNrNHb5VLD0ryZ814UEwlLNYBy0bpgFSjJz1a5Zq0pG2yBkIgDRmYuZ8CH1"
            + "hmgbyslY9Wfyf4A1BLAQI/ABQAAAAIAFahWk7f6mf3CQEAAH0CAAAPACQAAAAAAAAAIAAAAAAAAABrbGVpbnRqZV8xMC5yY3AKACAAAAAAAAEAGABbbp33Bs7UAVtunfcGztQB"
            + "/qOh2gbO1AFQSwUGAAAAAAEAAQBhAAAANgEAAAAA");
    return example;
  }

  private DataObject getExampleDisallowedReceptorDataObject() throws IOException {
    final DataObject example = new DataObject()
        .contentType(ContentType.BASE64)
        .dataType(DataType.CSV)
        .data("TmFhbTtQbGFhdHM7RGF0dW07T3JnYW5pc2F0b3INCkFudGlzaGl0IEZlc3Q7V29ybGQgU2thdGUgQ2VudGVyOzIwMTktMDUtMTEgdC9tIDIwMTktMDUtMTE7SC4gTGF0aG91d2"
            + "Vycw0KQmVhcmRzdGVyZGFtO1Bha2h1aXMgV2VzdDsyMDE5LTA1LTExIHQvbSAyMDE5LTA1LTExO1JhYnkgTWFsa2kNCkRlbmhhbSAtIHRhdHRvbyBldmVudCBpbiBzdG9yZTtC"
            + "YXRhdmlhc3RhZCBMZWx5c3RhZCAtIERlbmhhbTsyMDE5LTA1LTI1IHQvbSAyMDE5LTA1LTI2O0RlbmhhbSB0aGUgamVhbm1ha2VyDQpFaW5kaG92ZW4gVGF0dG9vIENvbnZlbn"
            + "Rpb247U3BvcnRoYWwgVGl2b2xpOzIwMTktMDUtMTggdC9tIDIwMTktMDUtMTk7RWluZGhvdmVuIHRhdHRvbyBDb252ZW50aW9uDQpIZWF2ZW56IEZlc3RpdmFsO1NjaGVsZmhv"
            + "cnN0cGFyazsyMDE5LTA1LTMwIHQvbSAyMDE5LTA1LTMwO0VucmljaG1lbnQgRW50ZXJ0YWlubWVudA0KSW5uZXJTb3VscyBFdmVudDtEZSBCZWxsZW1hbjsyMDE5LTA0LTE0IH"
            + "QvbSAyMDE5LTA0LTE0O0lubmVyIFRlYWNoZXIgSm9oYW4gTGFmb3JjZQ0KS2VsdGZlc3Q7R3JvZW5lIFdlZWxkZTsyMDE5LTA1LTI1IHQvbSAyMDE5LTA1LTI2O1ZhbmEgRXZl"
            + "bnRzDQpPbGQgU2tvb2wgVGF0dG9vIFdlZWtlbmQ7QUMgZGUgSG9sbTsyMDE5LTA0LTIwIHQvbSAyMDE5LTA0LTIxO1RhdHRvbyBCZW4NClBpdGZlc3Q7UmVzb3J0IFp1aWRkcm"
            + "VudGhlOzIwMTktMDUtMjQgdC9tIDIwMTktMDUtMjU7RHJlbnRoZXZlbnRzDQpQb3AtVXAgU3RvcmU7TW9kZWh1aXMgQmxvayAoQW1zdGVyZGFtKTsyMDE5LTA0LTE0IHQvbSAy"
            + "MDE5LTA0LTE0O01vZGVodWlzIEJsb2sNClByaW1hRG9ubmEgWCBNeWxhO0hvb2dUSUogQW1zdGVyZGFtOzIwMTktMDQtMTcgdC9tIDIwMTktMDQtMTc7QnVyZWF1IEpFWg0KU2"
            + "5lYWtlcm5lc3MgQW1zdGVyZGFtIDIwMTk7RGUgS3JvbWhvdXRoYWw7MjAxOS0wNi0wMSB0L20gMjAxOS0wNi0wMjtTcG9va2VyDQpUaGUgRnV0dXJlIHVuZGVyIG91ciBTa2lu"
            + "O01FU0ErIEluc3RpdHV0ZSBVbml2ZXJzaXR5IG9mIFR3ZW50ZTsyMDE5LTA0LTE4IHQvbSAyMDE5LTA0LTE4O0Rlc2lnbiBMYWIgVW5pdmVyc2l0ZWl0IFR3ZW50ZQ0KV29ybG"
            + "R3aWRlIEV5ZWJyb3cgRmVzdGl2YWwgJiBFeHBvO1ZhbiBOZWxsZWZhYnJpZWs7MjAxOS0wNS0xMCB0L20gMjAxOS0wNS0xMjtXb3JsZHdpZGUgRXllYnJvdyBGZXN0aXZhbCAm"
            + "IEV4cG8NCg==");
    return example;
  }
}
