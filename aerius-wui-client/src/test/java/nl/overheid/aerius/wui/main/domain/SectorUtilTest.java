/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwtmockito.GwtMockitoTestRunner;

import nl.overheid.aerius.shared.domain.sector.SectorIcon;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Test class for {@link SectorImageUtil}.
 */
@RunWith(GwtMockitoTestRunner.class)
public class SectorUtilTest {

  /**
   * Test if a sector image is available for each sector. Because the method returns by default the 'other' icon
   * a special case in the test is needed to check other itself.
   */
  @Test
  public void testGetSectorImageResource() {
    final String other = R.images().iconSectorOther().getSafeUri().asString();
    for (final SectorIcon sectorIcon: SectorIcon.values()) {
      final ImageResource ir = SectorImageUtil.getSectorImageResource(sectorIcon);
      if (sectorIcon == SectorIcon.OTHER) {
        assertEquals("Sector icon for " + sectorIcon, other, ir.getSafeUri().asString());
      } else {
        assertNotEquals("Sector icon for " + sectorIcon, other, ir.getSafeUri().asString());
      }
    }
  }
}
