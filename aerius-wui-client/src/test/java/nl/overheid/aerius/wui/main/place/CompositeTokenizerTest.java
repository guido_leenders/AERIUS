/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.place;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwt.place.shared.Place;
import com.google.gwtmockito.GwtMockitoTestRunner;

/**
 * Test class for {@link CompositeTokenizer}.
 */
@RunWith(GwtMockitoTestRunner.class)
public class CompositeTokenizerTest {


  private class TestPlace extends Place {
    private Map<String, String> tokens;

    public Map<String, String> getTokens() {
      return tokens;
    }

    public void setTokens(final Map<String, String> tokens) {
      this.tokens = tokens;
    }
  }

  private class TestCompositeTokenizer extends CompositeTokenizer<TestPlace> {
    @Override
    protected TestPlace createPlace() {
      return new TestPlace();
    }

    @Override
    protected void updatePlace(final Map<String, String> tokens, final TestPlace place) {
      place.setTokens(tokens);
    }

    @Override
    protected void setTokenMap(final TestPlace place, final Map<String, String> tokens) {
      tokens.putAll(place.getTokens());
    }
  }

  @Test
  public void testTokens2Place() {
    final TestCompositeTokenizer ct = new TestCompositeTokenizer();
    final TestPlace place = ct.getPlace("a=1&b=2");
    assertEquals("Tokens 2 place", 2, place.getTokens().size());
  }

  @Test
  public void testPlace2Token() {
    final TestCompositeTokenizer ct = new TestCompositeTokenizer();
    final Map<String, String> tokens = new HashMap<String, String>();
    tokens.put("a", "1");
    tokens.put("b", "2");
    final TestPlace place = new TestPlace();
    place.setTokens(tokens);
    final String st = ct.getToken(place);
    // Because HashMap (in tokenizer) doesn't guarantee order check for both cases.
    assertTrue("Place 2 tokens", "a=1&b=2".equals(st) || "b=2&a=1".equals(st));
  }
}
