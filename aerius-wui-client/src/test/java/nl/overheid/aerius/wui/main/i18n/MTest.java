/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.i18n;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.gwtmockito.GwtMockitoTestRunner;

/**
 * Test class for {@link M}.
 */
@RunWith(GwtMockitoTestRunner.class)
public class MTest {

  @Test
  public void testFormat() {
    assertEquals("No formating", "No formating", M.format("No formating", "A", "B"));
    assertEquals("One formating", "A formating", M.format("{0} formating", "A", "B"));
    assertEquals("Two formating", "A formating B", M.format("{0} formating {1}", "A", "B"));
    assertEquals("formating Two ", "B formating A", M.format("{1} formating {0}", "A", "B"));
  }
}
