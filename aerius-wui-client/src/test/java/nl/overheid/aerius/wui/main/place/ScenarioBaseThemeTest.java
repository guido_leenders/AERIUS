/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.place;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Test for {@link ScenarioBaseTheme} class.
 */
public class ScenarioBaseThemeTest {

  @Test
  public void testNewPlaceStructure() {
    ScenarioBaseTheme.values();
    assertEquals("count theme", 2, ScenarioBaseTheme.values().length);
    assertEquals("count theme NATURE_AREAS", 3, ScenarioBaseTheme.NATURE_AREAS.getSubstances().size());
    assertEquals("count theme AIR_QUALITY", 4, ScenarioBaseTheme.AIR_QUALITY.getSubstances().size());
  }

  @Test
  public void testGetThemeValidEmissionResultKey() {
    assertSame("Null input should return valid value", EmissionResultKey.NOXNH3_DEPOSITION,
        ScenarioBaseTheme.NATURE_AREAS.getThemeValidEmissionResultKey(null));
    assertSame("Valid key should return same", EmissionResultKey.NOX_DEPOSITION,
        ScenarioBaseTheme.NATURE_AREAS.getThemeValidEmissionResultKey(EmissionResultKey.NOX_DEPOSITION));
    assertSame("Valid substance should return deposition variant", EmissionResultKey.NOX_DEPOSITION,
        ScenarioBaseTheme.NATURE_AREAS.getThemeValidEmissionResultKey(EmissionResultKey.NOX_CONCENTRATION));
    assertSame("Unsupported key should return first entry", EmissionResultKey.NOXNH3_DEPOSITION,
        ScenarioBaseTheme.NATURE_AREAS.getThemeValidEmissionResultKey(EmissionResultKey.PM10_CONCENTRATION));
  }
}
