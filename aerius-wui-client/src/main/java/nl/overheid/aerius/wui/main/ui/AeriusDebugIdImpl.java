/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.UIObject.DebugIdImplEnabled;

public class AeriusDebugIdImpl extends DebugIdImplEnabled {

  @Override
  public void ensureDebugId(final UIObject uiObject, final String id) {
    super.ensureDebugId(uiObject, normalize(id));
  }

  @Override
  public void ensureDebugId(final Element elem, final String baseID, final String id) {
    super.ensureDebugId(elem, normalize(baseID), normalize(id));
  }

  private String normalize(final String id) {
    return id.replaceAll(" ", "_");
  }

}
