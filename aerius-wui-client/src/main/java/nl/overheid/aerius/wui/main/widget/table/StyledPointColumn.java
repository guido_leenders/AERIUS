/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class StyledPointColumn<T extends Point> extends WidgetFactory<T, Label> {
  private final String textColor;
  private final EventBus eventBus;

  public StyledPointColumn(final String textColor, final EventBus eventBus) {
    this.textColor = textColor;
    this.eventBus = eventBus;
  }

  @Override
  public Label createWidget(final T object) {
    final Label panel = new Label(getText(object));

    panel.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        eventBus.fireEvent(new LocationChangeEvent(new BBox(object.getX(), object.getY(), 0)));
      }
    });

    panel.getElement().getStyle().setBackgroundColor(getColor(object));
    panel.getElement().getStyle().setColor(textColor);
    panel.setStyleName(R.css().sourceRow(), true);
    return panel;
  }

  protected abstract String getText(final T object);

  protected abstract String getColor(final T object);

  @Override
  public Widget wrapWidget(final Widget widget) {
    return new SimplePanel(widget);
  }
}
