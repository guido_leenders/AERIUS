/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

public class LabelledWidget extends FlowPanel {
  private final Label label = new Label();

  private String contentStyle = R.css().sourceDetailCol2Var();

  public LabelledWidget() {
    label.setStyleName(R.css().sourceDetailCol1Var());
    addStyleName(R.css().flex());

    super.add(label);
  }

  public void setLabel(final String txt) {
    label.setText(txt);
  }

  public void setLabelStyleName(final String styleName) {
    label.setStyleName(styleName);
  }

  public void setContentStyleName(final String styleName) {
    contentStyle = styleName;
  }

  @Override
  public void add(final Widget w) {
    if (contentStyle != null) {
      w.addStyleName(contentStyle);
    }

    if (getWidgetCount() == 2) {
      remove(1);
    }

    super.add(w);
  }

  @Override
  public void add(final IsWidget w) {
    add(w.asWidget());
  }
}
