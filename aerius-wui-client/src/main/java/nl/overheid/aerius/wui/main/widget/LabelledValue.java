/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class LabelledValue extends Composite implements LeafValueEditor<String> {

  private static LabelledValueUiBinder uiBinder = GWT.create(LabelledValueUiBinder.class);

  interface LabelledValueUiBinder extends UiBinder<Widget, LabelledValue> {
  }

  @UiField SpanElement value;
  @UiField SpanElement label;

  @Override
  public String getValue() {
    return value.getInnerText();
  }

  @Override
  public void setValue(final String value) {
    this.value.setInnerText(value);
  }

  public void setLabel(final String label) {
    this.label.setInnerText(label);
  }

  public LabelledValue() {
    initWidget(uiBinder.createAndBindUi(this));
  }

}
