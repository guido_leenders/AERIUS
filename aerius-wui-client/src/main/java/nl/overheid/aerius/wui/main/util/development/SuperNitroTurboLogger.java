/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util.development;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.wui.main.event.ApplicationHelpChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionResultKeyChangeEvent;
import nl.overheid.aerius.wui.main.event.EmissionSourceListNameChangeEvent;
import nl.overheid.aerius.wui.main.event.HabitatClearEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeDisplayEvent;
import nl.overheid.aerius.wui.main.event.HabitatTypeHoverEvent;
import nl.overheid.aerius.wui.main.event.ImportEvent;
import nl.overheid.aerius.wui.main.event.InformationPointChangeEvent;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.event.NotificationEvent;
import nl.overheid.aerius.wui.main.event.YearChangeEvent;
import nl.overheid.aerius.wui.main.util.NotificationUtil;

/**
 * {@link SuperNitroTurboLogger} logs things.
 *
 * For development purposes. Primarily.
 *
 */
public class SuperNitroTurboLogger {
  interface SuperNitroTurboBinder extends EventBinder<SuperNitroTurboJuggernaut> {}

  /**
   * Playground where {@link SuperNitroTurboJuggernaut} will play.
   */
  private final SuperNitroTurboBinder playground;

  /**
   * {@link SuperNitroTurboJuggernaut} does all the work.
   */
  public static class SuperNitroTurboJuggernaut {
    protected SuperNitroTurboLogger logger;

    @EventHandler
    public void onInfoMarkerChange(final InformationPointChangeEvent e) {
      logger.log("Information marker changed");
    }

    @EventHandler
    public void onNotification(final NotificationEvent e) {
      logger.log("Notification: " + e.getValue().getMessage());
    }

    @EventHandler
    public void onLocationChanged(final LocationChangeEvent e) {
      logger.log("Location on map changed: " + e.getValue());
    }

    @EventHandler
    public void onHabitatTypeDisplayEvent(final HabitatTypeDisplayEvent e) {
      logger.log("Displaying habitat: " + e.toString());
    }

    @EventHandler
    public void onHabitatTypeDisplayEvent(final HabitatTypeHoverEvent e) {
      logger.log("Hovering habitat: " + e.toString());
    }

    @EventHandler
    public void onHabitatTypeDisplayEvent(final HabitatClearEvent e) {
      logger.log("Clear event");
    }

    @EventHandler
    public void onImport(final ImportEvent event) {
      logger.log("File imported.");
    }

    @EventHandler
    public void onHelpChanged(final ApplicationHelpChangeEvent e) {
      logger.log(e.getValue() ? "Help functionality enabled." : "Help functionality disabled.");
    }

    @EventHandler
    public void onSituationNameChange(final EmissionSourceListNameChangeEvent e) {
      logger.verbose("Situation (id:" + e.getId() + ") name changed: " + e.getName());
    }

    @EventHandler
    public void onEmissionResultKeyChange(final EmissionResultKeyChangeEvent e) {
      logger.log("Substance changed: " + e.getValue());
    }

    @EventHandler
    public void onYearChange(final YearChangeEvent e) {
      logger.log("Year changed: " + e.getValue());
    }

    protected <T> String parseSimpleClassName(final Class<T> c) {
      final String name = c.getName();
      return name.substring(name.lastIndexOf('.') + 1);
    }

    public SuperNitroTurboLogger getLogger() {
      return logger;
    }

    public void setLogger(final SuperNitroTurboLogger logger) {
      this.logger = logger;
    }
  }

  /**
   * Nothing.
   */
  public static final int NOTHING = 0;

  /**
   * Important stuff.
   */
  public static final int IMPORTANT = 1;
  /**
   * Anything relevant.
   */
  public static final int TURBO = 3;
  /**
   * Anything (ir)relevant.
   */
  public static final int SUPERTURBO = 7;
  /**
   * Anything.
   */
  public static final int SUPERNITROTURBO = 15;

  private EventBus eventBus;

  /**
   * Default constructor for a no-logger.
   */
  public SuperNitroTurboLogger() {
    //Initialize to null to avoid generation code in production.
    playground = null;
  }

  /**
   * Takes an {@link EventBus} and a {@link SuperNitroTurboJuggernaut} that will capture and log events.
   *
   * @param eventBus {@link EventBus} to listen to.
   * @param juggernaut {@link SuperNitroTurboJuggernaut} that's capturing events and logging.
   */
  public SuperNitroTurboLogger(final EventBus eventBus, final SuperNitroTurboJuggernaut juggernaut) {
    this.eventBus = eventBus;

    playground = GWT.create(SuperNitroTurboBinder.class);
    juggernaut.setLogger(this);
    log("SuperNitroTurboJuggernaut started.");
  }

  /**
   * Log normal message.
   *
   * @param txt message to log.
   */
  public void logImportant(final String txt) {
    if (mustDrink(IMPORTANT)) {
      spit(IMPORTANT, txt);
    }
  }

  /**
   * Log normal message.
   *
   * @param txt message to log.
   */
  public void log(final String txt) {
    if (mustDrink(TURBO)) {
      spit(TURBO, txt);
    }
  }

  /**
   * Log normal message.
   *
   * @param txt message to log.
   */
  public void logWeirdness(final String txt) {
    if (mustDrink(TURBO)) {
      spit(TURBO, txt, true);
    }
  }

  /**
   * Log verbose / irrelevant message.
   *
   * @param txt message to log.
   */
  public void verbose(final String txt) {
    if (mustDrink(SUPERTURBO)) {
      spit(SUPERTURBO, txt);
    }
  }

  /**
   * Log extremely verbose / utterly irrelevant message.
   *
   * @param txt message to log.
   */
  public void superVerbose(final String txt) {
    if (mustDrink(SUPERNITROTURBO)) {
      spit(SUPERNITROTURBO, txt);
    }
  }

  /**
   * Whether juggernaut must drink juice.
   */
  protected boolean mustDrink(final int req) {
    return (req & TURBO) >= req;
  }

  /**
   * Where juice goes to die.
   *
   * @param logLevel future use
   */
  protected void spit(final int logLevel, final String msg) {
    spit(logLevel, msg, false);
  }

  private void spit(final int logLevel, final String msg, final boolean weird) {
    // Log to server
    Logger.getLogger("SuperNitroTurboLogger").log(weird ? Level.WARNING : Level.FINE, msg);

    // Log to console
    GWT.log(msg);

    // If important or weird, log to application
    if ((logLevel & IMPORTANT) >= logLevel) {
      NotificationUtil.broadcastMessage(eventBus, "NITRO JUGGERNAUT BROADCAST: " + msg);
    } else if (weird) {
      NotificationUtil.broadcastError(eventBus, "NITRO JUGGERNAUT WEIRDNESS: " + msg);
    }
  }
}
