/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.validation;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;

/**
 * Popup positioned relative to the input field showing an error message. The
 * class also styles the input fields.
 *
 * TODO Keep errorInput style on elements that validated false. Remove on true validation.
 * TODO Don't hide+show the popup on elements it's already showing for
 */
class ErrorPopupPanel extends AttachedPopupPanel implements BlurHandler {

  private final HTML content = new HTML();
  private Widget relativeToWidgetToStyle;
  private HandlerRegistration blurHandler;

  public ErrorPopupPanel() {
    super();
    getArrow().setColor(R.css().colorErrorContent(), R.css().colorErrorBorder());
    setStyleName(R.css().errorPopupPanel());
    final FlowPanel fp = new FlowPanel();
    setWidget(fp);
    fp.add(new Image(R.images().errorExclamationMark()));
    fp.add(content);
  }

  /**
   * Displays the error popup relative to the widget with the message and styles
   * the widget as in error.
   *
   * @param widget Widget to show popup relative to
   * @param message message to display
   */
  public void show(final Widget widget, final String message) {
    show(widget, widget, message);
  }

  /**
   * Displays the error popup relative to the widget with the message and styles
   * the elementToStyle as in error.
   *
   * @param widget Widget to show popup relative to
   * @param elementToStyle
   * @param message message to display
   */
  public void show(final Widget widget, final Widget widgetToStyle, final String message) {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        showDelayed(widget, widgetToStyle, message);
      }
    });
  }

  public void showDelayed(final Widget widget, final Widget widgetToStyle, final String message) {
    if (relativeToWidgetToStyle != null) {
      relativeToWidgetToStyle.removeStyleName(R.css().errorInput());
    }
    relativeToWidgetToStyle = widgetToStyle == null ? widget : widgetToStyle;
    if (blurHandler != null) {
      blurHandler.removeHandler();
    }
    blurHandler = widget.addDomHandler(this, BlurEvent.getType());
    relativeToWidgetToStyle.addStyleName(R.css().errorInput());
    content.setHTML(message);
    content.ensureDebugId(TestID.DIV_VALIDATION_POPUP);
    show();
    // show/hide before calling setPopupPosition2
    setPopupPosition2(widget);
    attachToWidget(widget);
    if (widget instanceof FocusWidget) {
      ((FocusWidget) widget).setFocus(true);
    }
  }

  /**
   * Hide the popup if the popup is currently showing relative to the widget
   *
   * @param widget widget to check against
   */
  public void hide(final Widget widget) {
    hide(widget, widget);
  }

  /**
   * Hide the popup if the popup is currently showing relative to the widget and
   * remove the error style from the elementToStyle.
   *
   * @param widget Widget to check against
   * @param elementToStyle Element to remove style from
   */
  public void hide(final Widget widget, final Widget widgetToStyle) {
    hide();
    widget.removeStyleName(R.css().errorInput());
    if (widgetToStyle != null) {
      widgetToStyle.removeStyleName(R.css().errorInput());
    }
    if (relativeToWidgetToStyle != null) {
      relativeToWidgetToStyle.removeStyleName(R.css().errorInput());
    }
  }

  @Override
  public void onBlur(final BlurEvent event) {
    hide();
    relativeToWidgetToStyle.removeStyleName(R.css().errorInput());
  }
}
