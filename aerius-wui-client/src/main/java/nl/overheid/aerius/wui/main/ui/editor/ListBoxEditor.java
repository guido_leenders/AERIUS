/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.ListBox;

import nl.overheid.aerius.shared.domain.HasId;
import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.ui.validation.ValidatorCollection;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

/**
 * Data aware Editor for {@link ListBox} widget. This editor works on the related data object in combination with the  ListBox itself only
 * works on String types, so additional conversion is needed. To get other text on the ListBox override {@link #getLabel(Object)}.
 * @param <T> data type of this listbox editor
 */
public class ListBoxEditor<T> extends ListBox implements ValueAwareEditor<T>, LeafValueEditor<T> {

  /**
   * Use this list value for the option that can't be selected and most of the
   * time shows text to select an item. The implementation should take care of
   * checking against this value.
   */
  public static final String NO_OPTION_VALUE = "";

  private final ValidatorCollection<T> validators = new ValidatorCollection<>();
  private final DataClass<T> data = new DataClass<>();

  private EditorDelegate<T> delegate;

  public ListBoxEditor() {
    addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(final ChangeEvent event) {
        ErrorPopupController.clearError(ListBoxEditor.this);
        setTitle();
      }
    });
  }

  /**
   * Add a validator.
   *
   * @param validator validator
   */
  public void addValidator(final Validator<T> validator) {
    validators.addValidator(validator);
  }

  /**
   * Use this method to set a first option as a select option.
   */
  public void addFirstEmptyItem() {
    addItem(getLabel(null), NO_OPTION_VALUE);
  }

  public <I extends T> void addItems(final I[] list) {
    for (final I value : list) {
      addItem(value);
    }
  }

  public <I extends T> void addItems(final List<I> list) {
    if (list != null) {
      for (final I value : list) {
        addItem(value);
      }
    }
  }

  public void addItem(final T value) {
    addItem(getLabel(value), addOptionItem(value));
  }

  /**
   * Adds the item to the listbox select and returns the string used as value
   * @param value
   * @return
   */
  protected String addOptionItem(final T value) {
    return data.add(value, getKey(value));
  }

  @Override
  public void clear() {
    data.clear();
    super.clear();
  }

  @Override
  public T getValue() {
    final int i = getSelectedIndex();
    return i >= 0 ? data.getValue(getValue(i)) : null;
  }

  /**
   * Returns the label text to set on the  Override this method to show a custom text.
   * @param value The value to show on the label
   * @return The string representation of the value
   */
  protected String getLabel(final T value) {
    return value == null ? "" : value.toString();
  }

  /**
   * Returns the key in the data object.
   * @param value
   * @return
   */
  protected String getKey(final T value) {
    return value instanceof Enum ? ((Enum<?>) value).name()
        : (value instanceof String ? (String) value : String.valueOf(value instanceof HasId ? ((HasId) value).getId() : value));
  }

  @Override
  public void setDelegate(final EditorDelegate<T> delegate) {
    this.delegate = delegate;
  }

  @Override
  public void setValue(final T value) {

    if (value == null) {
      // if value is null, select the first entry.
      setSelectedIndex(0);
    } else {
      for (int i = 0; i < getItemCount(); i++) {
        final T listValue = data.getValue(getValue(i));
        if ((listValue != null) && listValue.equals(value)) {
          setSelectedIndex(i);
          break;
        }
      }
    }
  }

  @Override
  public void setItemSelected(final int index, final boolean selected) {
    super.setItemSelected(index, selected);
    setTitle();
  }

  @Override
  public void setSelectedIndex(final int index) {
    super.setSelectedIndex(index);
    setTitle();
  }

  private void setTitle() {
    setTitle(getSelectedItemText());
  }

  @Override
  public void flush() {
    if (delegate != null) {
      final String errorMessage = validators.validate(getValue());

      if (errorMessage != null) {
        delegate.recordError(errorMessage, null, ListBoxEditor.this);
      }
    }
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  private static class DataClass<D> {
    private final Map<String, D> data = new HashMap<>();

    public String add(final D value, final String key) {
      final String actualKey = key == null ? String.valueOf(data.size()) : key;
      data.put(actualKey, value);
      return actualKey;
    }

    public void clear() {
      data.clear();
    }

    public D getValue(final String value) {
      return (value == null) || value.isEmpty() ? null : data.get(value);
    }
  }
}
