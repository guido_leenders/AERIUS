/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.editor.client.LeafValueEditor;

public class DataProvider<C extends Collection<T>, T> implements LeafValueEditor<C> {
  private final Set<DivTable<T, ?>> displays = new HashSet<DivTable<T, ?>>();

  private C value;

  private Comparator<T> comparator;

  @Override
  public void setValue(final C value) {
    this.value = value;

    refresh();
  }

  @Override
  public C getValue() {
    return value;
  }

  public void setComparator(final Comparator<T> comparator) {
    this.comparator = comparator;
  }

  public boolean addDataDisplay(final IsDataTable<T> table) {
    return displays.add(table.asDataTable());
  }

  public boolean addDataDisplay(final DivTable<T, ?> table) {
    return displays.add(table);
  }

  public boolean removeDataDisplay(final IsDataTable<T> table) {
    return displays.remove(table.asDataTable());
  }

  public boolean removeDataDisplay(final DivTable<T, ?> table) {
    return displays.remove(table);
  }

  public void refresh() {
    if (comparator != null) {
      if (value instanceof List) {
        Collections.sort((List<T>) value, comparator);
      }
    }

    redraw();
  }

  public void redraw() {
    for (final DivTable<T, ?> table : displays) {
      table.setRowData(value, true);
    }
  }
}
