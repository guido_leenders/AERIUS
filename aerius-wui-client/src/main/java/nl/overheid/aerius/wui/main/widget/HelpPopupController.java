/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;
import nl.overheid.aerius.wui.main.event.ApplicationHelpChangeEvent;

/**
 * Class to manage all help tool tips. Therefore this class is a singleton and
 * should be passed via the injection framework.
 *
 * Example usage:
 * <pre>
 *   helpController.addWidget(myWidget, helpController.tt().ttMyWidgetHelpInfo());
 * </pre>
 */
@Singleton
public class HelpPopupController implements FocusHandler, MouseOverHandler, MouseOutHandler, AttachEvent.Handler {
  private static final int DELAY_HIDE_POPUP = 1500;

  private final HelpToolTipInfo infoMappings;

  private final HashMap<Widget, ArrayList<HandlerRegistration>> handlers = new HashMap<>();
  private final HashMap<Widget, HelpInfo> messages = new HashMap<>();
  private final HelpPopupPanel popup;
  private Widget lastFocusWidget;
  private boolean enabled;
  private final Timer timer = new Timer() {
    @Override
    public void run() {
      setMessageAndShow(null);
    }
  };
  private final EventBus eventBus;

  @Inject
  public HelpPopupController(final Context c, final EventBus eventBus, final HelpToolTipInfo infoMappings) {
    this.eventBus = eventBus;
    this.infoMappings = infoMappings;

    GWT.log(c.toString());

    final String s = c.getSetting(SharedConstantsEnum.HELP_URL_TEMPLATE);
    popup = new HelpPopupPanel(s);
    popup.addDomHandler(new MouseOverHandler() {
      @Override
      public void onMouseOver(final MouseOverEvent event) {
        timer.cancel();
      }
    }, MouseOverEvent.getType());
    popup.addDomHandler(new MouseOutHandler() {
      @Override
      public void onMouseOut(final MouseOutEvent event) {
        timer.schedule(DELAY_HIDE_POPUP);
      }
    }, MouseOutEvent.getType());
  }

  public void addWidget(final IsWidget widget, final HelpInfo helpInfo) {
    addWidget(widget.asWidget(), helpInfo);
  }

  /**
   * Register the widget to be able to show help tool tips. If the widget is
   * unloaded the caller should remove the handler created here itself.
   *
   * @param widget Widget to show tool tip for
   * @param info {@link HelpInfo} info to show
   * @return HandlerRegistration for unregistering in case the widget is unloaded
   */
  public HandlerRegistration addWidget(final Widget widget, final HelpInfo info) {
    if (info == null) {
      return null;
    }

    final boolean newWidget = messages.put(widget, info) == null;

    if (widget.isAttached()) {
      attachOrDetach(widget, true);
      return null;
    } else {
      return newWidget ? widget.addAttachHandler(this) : null;
    }
  }

  @Override
  public void onMouseOut(final MouseOutEvent event) {
    final Widget source = (Widget) event.getSource();

    if (lastFocusWidget != source) { //source is never null
      setMessageAndShow(lastFocusWidget);
    }

    // Putting setMessageAndShow(null) in the else clause will get you swift and seamless
    // popup behavior, but will also mean user interaction with the popup element is
    // impossible.
  }

  @Override
  public void onMouseOver(final MouseOverEvent event) {
    setMessageAndShow((Widget) event.getSource());
  }

  @Override
  public void onFocus(final FocusEvent event) {
    setMessageAndShow((Widget) event.getSource());
  }

  @Override
  public void onAttachOrDetach(final AttachEvent event) {
    final Widget source = (Widget) event.getSource();

    attachOrDetach(source, event.isAttached());
  }

  private void attachOrDetach(final Widget source, final boolean attached) {
    if (attached) {
      final ArrayList<HandlerRegistration> hrc = new ArrayList<>();

      hrc.add(source.addDomHandler(this, FocusEvent.getType()));
      hrc.add(source.addDomHandler(this, MouseOverEvent.getType()));
      hrc.add(source.addDomHandler(this, MouseOutEvent.getType()));
      handlers.put(source, hrc);
    } else {
      if (handlers.containsKey(source)) {
        handlers.get(source).clear();
      }
      // Hide popup in case visible, but detached.
      // Mem compare the objects to check exact match (no equals())
      if (lastFocusWidget == source) {
        popup.hide();
      }
    }
  }

  /**
   * Enables or disables the help showing tool tips. In case a tooltip is
   * visible it will be hid.
   *
   * @param enabled If true enables help else disables help
   */
  public void setEnabled(final Boolean enabled) {
    this.enabled = enabled;
    setMessageAndShow(lastFocusWidget);
    eventBus.fireEvent(new ApplicationHelpChangeEvent(enabled));
  }

  /**
   * Returns an convenience instance of the {@link HelpToolTipInfo}
   * interface to get easy access to the messages to show.
   *
   * @return instance to messages interface
   */
  public HelpToolTipInfo tt() {
    return infoMappings;
  }

  /**
   * Sets the message on the Popup and positions the the Popup relative to the
   * widget.
   *
   * @param widget widget to show message for
   */
  private void setMessageAndShow(final Widget widget) {
    if (enabled) {
      if (widget == null) {
        lastFocusWidget = null;
        popup.hide();
      } else {
        // We're hiding the tooltip so it gets reattached when show() is called. The reason for this is that while a tooltip is shown and another
        //  one is triggered it will simply reuse the tooltip. While very smart this causes issues if the tooltip is used for like a newly made
        //  ConfirmCancelDialog which will be lower in the DOM element list blocking clicks.
        // Disabling and enabling the animation in between makes sure the hide and show animations aren't executed so it keeps looking neat.
        popup.setAnimationEnabled(false);
        popup.hide();
        popup.setHelpInfo(messages.get(widget));
        popup.show();
        popup.setAnimationEnabled(true);
        popup.setSmartPopupPosition(widget);
        popup.attachToWidget(widget);
        lastFocusWidget = widget;
      }
    } else {
      popup.hide();
    }
  }
}
