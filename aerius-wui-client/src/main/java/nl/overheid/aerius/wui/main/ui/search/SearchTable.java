/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.search.SearchSuggestion;
import nl.overheid.aerius.shared.domain.search.SearchSuggestionType;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.DivTableRowDecorator;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTableRow;
import nl.overheid.aerius.wui.main.widget.table.TableColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public abstract class SearchTable<T extends SearchSuggestionType, E extends SearchSuggestion<T, E>> extends Composite implements IsDataTable<Entry<T, ArrayList<E>>> {
  @UiField SimpleDivTable<Entry<T, ArrayList<E>>> divTable;

  @UiField(provided = true) TextColumn<Entry<T, ArrayList<E>>> typeLabel;
  @UiField(provided = true) TableColumn<E, Entry<T, ArrayList<E>>, SearchSuggestionTable<T, E>> tableColumn;

  interface SearchTableUiBinder extends UiBinder<Widget, SearchTable<?, ?>> {}

  private static final SearchTableUiBinder UI_BINDER = GWT.create(SearchTableUiBinder.class);

  public SearchTable(final HelpPopupController hpC, final SearchAction<T, E> searchAction) {
    typeLabel = new TextColumn<Entry<T, ArrayList<E>>>() {
      @Override
      public String getValue(final Entry<T, ArrayList<E>> object) {
        return M.messages().searchSuggestionType(object.getKey().name());
      }
    };

    tableColumn = new TableColumn<E, Entry<T, ArrayList<E>>, SearchSuggestionTable<T, E>>() {
      @Override
      public Collection<E> getRowData(final Entry<T, ArrayList<E>> object) {
        return object.getValue();
      }

      @Override
      public SearchSuggestionTable<T, E> createDataTable(final Entry<T, ArrayList<E>> object) {
        final SearchSuggestionTable<T, E> table = createSuggestionTable(hpC, searchAction);
        table.asDataTable().ensureDebugId(TestID.DIV_SEARCHSUGGESTION + "-" + object.getKey().name());
        return table;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    divTable.addRowDecorator(new DivTableRowDecorator<Entry<T, ArrayList<E>>, SimpleDivTableRow>() {
      @Override
      public void applyRowOptions(final SimpleDivTableRow rowContainer, final Entry<T, ArrayList<E>> item) {
        rowContainer.ensureDebugId(TestID.DIV_SEARCHSUGGESTIONCATEGORY + "-" + item.getKey().name());
      }
    });
  }

  @Override
  protected void initWidget(final Widget widget) {
    super.initWidget(widget);

  }

  public void setSuggestions(final ArrayList<E> suggestions) {
    divTable.setRowData(toSuggestionMap(suggestions).entrySet(), true);
  }

  public void clear() {
    divTable.clear();
  }

  @Override
  public SimpleDivTable<Entry<T, ArrayList<E>>> asDataTable() {
    return divTable;
  }

  private HashMap<T, ArrayList<E>> toSuggestionMap(final ArrayList<E> suggestions) {
    final HashMap<T, ArrayList<E>> map = new HashMap<>();

    for (final E suggestion : suggestions) {
      if (map.containsKey(suggestion.getType())) {
        map.get(suggestion.getType()).add(suggestion);
      } else {
        final ArrayList<E> typedSuggestions = new ArrayList<>();
        typedSuggestions.add(suggestion);
        map.put(suggestion.getType(), typedSuggestions);
      }
    }

    return map;
  }

  protected abstract SearchSuggestionTable<T, E> createSuggestionTable(HelpPopupController hpC, SearchAction<T, E> searchAction);
}
