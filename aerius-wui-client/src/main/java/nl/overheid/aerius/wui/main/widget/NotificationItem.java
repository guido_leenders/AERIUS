/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;

class NotificationItem extends Composite {
  private static final DateTimeFormat FORMATTER = DateTimeFormat.getFormat(M.messages().notificationDateTimeFormat());

  private static NotificationItemUiBinder uiBinder = GWT.create(NotificationItemUiBinder.class);

  interface NotificationItemUiBinder extends UiBinder<Widget, NotificationItem> {
  }

  interface CustomStyle extends CssResource {
    String unread();

    String error();
  }

  @UiField CustomStyle style;

  @UiField FocusPanel item;
  @UiField DivElement title;
  @UiField DivElement content;
  @UiField FocusPanel closeButton;

  private boolean read;
  private boolean error;

  private final NotificationPopup nList;

  public NotificationItem(final NotificationPopup nList, final Notification notification) {
    this.nList = nList;
    initWidget(uiBinder.createAndBindUi(this));
    title.setInnerText(FORMATTER.format(notification.getDateTime()));
    if (notification.isError()) {
      final String msg = notification.getException() == null ? notification.getMessage() : M.getErrorMessage(notification.getException());
      content.setInnerHTML(SafeHtmlUtils.fromTrustedString(msg).asString()
          + "<br>" + SafeHtmlUtils.fromString(M.messages().notificationReference(notification.getReference())).asString());
      addStyleName(style.error());
      error = true;
    } else {
      final String url = notification.getUrl() == null ? "" : "<br>" + notification.getUrl();
      content.setInnerHTML(SafeHtmlUtils.fromString(notification.getMessage()).asString() + url);
      addStyleName(style.unread());
    }
    read = false;
    item.ensureDebugId(TestID.DIV_NOTIFICATIONITEM);
    closeButton.ensureDebugId(TestID.BUTTON_NOTIFICATION_CLOSE);
  }

  @UiHandler("item")
  public void onMouseOver(final MouseOverEvent event) {
    closeButton.setVisible(true);
    nList.setRead(this);
  }

  @UiHandler("item")
  public void onMouseOut(final MouseOutEvent event) {
    closeButton.setVisible(false);
  }

  public boolean isError() {
    return error;
  }

  public boolean isRead() {
    return read;
  }

  public void setRead() {
    read = true;
    item.removeStyleName(style.unread());
    item.removeStyleName(style.error());
  }

  @UiHandler("closeButton")
  void onRemove(final ClickEvent e) {
    nList.remove(this);
  }
}
