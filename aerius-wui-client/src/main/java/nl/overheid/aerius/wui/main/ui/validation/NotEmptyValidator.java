/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.validation;


/**
 * Validator checks for null or empty {@link String} objects.
 */
public abstract class NotEmptyValidator implements Validator<String> {

  private final String placeholder;

  public NotEmptyValidator() {
    this(null);
  }

  /**
   * Creates a validator which checks if the content is not equal to the placeholder
   * value.
   * @param placeholder value to check
   */
  public NotEmptyValidator(final String placeholder) {
    this.placeholder = placeholder;
  }

  @Override
  public String validate(final String value) {
    return value == null || value.isEmpty() || (placeholder != null && placeholder.equals(value)) ? getMessage(value) : null;
  }

  /**
   * Returns the localized error message for this object when null or empty.
   *
   * @param value current value
   * @return localized message
   */
  protected abstract String getMessage(String value);
}
