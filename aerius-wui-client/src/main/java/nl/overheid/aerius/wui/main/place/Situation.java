/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.place;

/**
 * Situation ENUM. A {@link Situation} represents a situation the way it is contained in a Scenario.
 * 
 * The UI may swap situations around as it sees fit, the order and method it is stored in a scenario
 * does not change however; its id remains the same.
 */
public enum Situation {

  /**
   * Starting situation or situation one.
   */
  SITUATION_ONE(0),

  /**
   * Situation two.
   */
  SITUATION_TWO(1),

  /**
   * Comparison of situation one and two.
   */
  COMPARISON(2);

  private final int id;

  private Situation(final int id) {
    this.id = id;
  }

  /**
   * Returns the situation from the id. This method should only be used to get the specific tab from the <code>situation</code>
   * param from the URL. DO NOT USE this method to get the situation given an emission source list ID!
   * 
   * The emission source list id does not explicitly correspond to a situation id.
   * 
   * @param id the situation ID to get the situation for.
   * @return The situation corresponding to the id or null if no match.
   */
  public static Situation situationFromId(final int id) {
    for (final Situation situation : values()) {
      if (situation.id == id) {
        return situation;
      }
    }
    return null;
  }

  public int getId() {
    return id;
  }

  /**
   * Safely returns a Situation. It is case independent and returns null in
   * case the input was null or the enum value could not be found.
   * 
   * @param value value to convert
   * @return Situation or null if no valid input
   */
  public static Situation safeValueOf(final String value) {
    if (value == null) {
      return null;
    }

    try {
      return valueOf(value.toUpperCase());
    } catch (final Exception e) {
      return null;
    }
  }
}
