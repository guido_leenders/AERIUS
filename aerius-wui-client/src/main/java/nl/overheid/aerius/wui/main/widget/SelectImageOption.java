/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public abstract class SelectImageOption<T> extends Composite implements SelectOption<T> {
  interface SelectImageOptionUiBinder extends UiBinder<Widget, SelectImageOption<?>> {}

  private static final SelectImageOptionUiBinder UI_BINDER = GWT.create(SelectImageOptionUiBinder.class);

  public interface CustomStyle extends CssResource {
    String selected();
  }

  @UiField FlowPanel container;
  @UiField Label label;
  @UiField Image image;

  @UiField CustomStyle style;

  private boolean selected;

  public SelectImageOption(final T value) {
    initWidget(UI_BINDER.createAndBindUi(this));

    label.setText(getItemText(value));
    image.setResource(getImageResource(value));
    container.addDomHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        setSelected(!selected, true);
      }
    }, ClickEvent.getType());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public void setSelected(final boolean select, final boolean fireEvents) {
    selected = select;

    container.setStyleName(style.selected(), selected);

    if (fireEvents) {
      ValueChangeEvent.fire(this, select);
    }
  }

  public abstract ImageResource getImageResource(final T item);
}
