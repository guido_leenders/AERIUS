/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.gen2.client.SliderBarHorizontal;

import nl.overheid.aerius.shared.MathUtil;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Displays a sliderbar as percentage, where the input value should be between
 * 0.0 ... 1.0
 */
public class PercentageSliderBar extends SliderBarHorizontal {
  private static final double STEP_SIZE = 0.01;

  private final DivElement label = Document.get().createDivElement();

  /**
   * Constructor.
   */
  public PercentageSliderBar() {
    super(0.0, 1.0, null, R.images());
    label.setClassName(R.css().sliderKnob());
    getKnob().insertFirst(label);
    setStepSize(STEP_SIZE);
  }

  @Override
  protected void onLoad() {
    redraw();
  }

  @Override
  public void setCurrentValue(final double curValue, final boolean fireEvent) {
    super.setCurrentValue(curValue, fireEvent);
    label.setInnerHTML(String.valueOf(MathUtil.round(getCurrentValue() * 100)));
  }
}
