/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.resources;

import com.google.gwt.gen2.client.SliderBarHorizontal.SliderBarImages;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.TextResource;

/**
 * Image Resource interface to all images.
 */
public interface ImageResources extends ClientBundle, SectorImageResources, SectorGroupResources, SliderBarImages {

  @Source("images/AERIUS-calculator.svg")
  @MimeType("image/svg+xml")
  DataResource aeriusCalculatorLogo();

  @Source("images/AERIUS-scenario.svg")
  @MimeType("image/svg+xml")
  DataResource aeriusScenarioLogo();

  @Source("images/AERIUS-register.svg")
  @MimeType("image/svg+xml")
  DataResource aeriusRegisterLogo();

  @Source("images/plus.svg")
  @MimeType("image/svg+xml")
  TextResource plus();

  @Source("images/natura2000-logo.png")
  ImageResource natura2000Logo();

  @Source("images/btn-user-active.png")
  ImageResource buttonProfileActive();

  @Source("images/btn-user-inactive.png")
  ImageResource buttonProfileInactive();

  @Source("images/btn-user-normal.png")
  ImageResource buttonProfile();

  @Source("images/btn-log-out-active.png")
  ImageResource buttonLogoutActive();

  @Source("images/btn-log-out-inactive.png")
  ImageResource buttonLogoutInactive();

  @Source("images/btn-log-out-normal.png")
  ImageResource buttonLogout();

  @Source("images/btn-delete-active.png")
  ImageResource buttonDeleteActive();

  @Source("images/btn-delete-inactive.png")
  ImageResource buttonDeleteInactive();

  @Source("images/btn-delete-normal.png")
  ImageResource buttonDelete();

  @Source("images/btn-delete-small-active.png")
  ImageResource buttonDeleteSmallActive();

  @Source("images/btn-delete-small-inactive.png")
  ImageResource buttonDeleteSmallInactive();

  @Source("images/btn-delete-small-normal.png")
  ImageResource buttonDeleteSmall();

  @Source("images/btn-delete-x-small-active.png")
  ImageResource buttonDeleteXSmallActive();

  @Source("images/btn-delete-x-small-inactive.png")
  ImageResource buttonDeleteXSmallInactive();

  @Source("images/btn-delete-x-small-normal.png")
  ImageResource buttonDeleteXSmall();

  @Source("images/btn-edit-x-small-active.png")
  ImageResource buttonEditXSmallActive();

  @Source("images/btn-edit-x-small-inactive.png")
  ImageResource buttonEditXSmallInactive();

  @Source("images/btn-edit-x-small-normal.png")
  ImageResource buttonEditXSmall();

  @Source("images/btn-edit-small-active.png")
  ImageResource buttonEditSmallActive();

  @Source("images/btn-edit-small-inactive.png")
  ImageResource buttonEditSmallInactive();

  @Source("images/btn-edit-small-normal.png")
  ImageResource buttonEditSmall();

  @Source("images/btn-edit-active.png")
  ImageResource buttonEditActive();

  @Source("images/btn-edit-inactive.png")
  ImageResource buttonEditInactive();

  @Source("images/btn-edit-normal.png")
  ImageResource buttonEdit();

  @Source("images/btn-view-x-small-active.png")
  ImageResource buttonViewXSmallActive();

  @Source("images/btn-view-x-small-inactive.png")
  ImageResource buttonViewXSmallInactive();

  @Source("images/btn-view-x-small-normal.png")
  ImageResource buttonViewXSmall();

  @Source("images/btn-view-active.png")
  ImageResource buttonViewActive();

  @Source("images/btn-view-inactive.png")
  ImageResource buttonViewInactive();

  @Source("images/btn-view-normal.png")
  ImageResource buttonView();

  @Source("images/btn-duplicate-active.png")
  ImageResource buttonCopyActive();

  @Source("images/btn-duplicate-inactive.png")
  ImageResource buttonCopyInactive();

  @Source("images/btn-duplicate-normal.png")
  ImageResource buttonCopy();

  @Source("images/btn-date-active.png")
  ImageResource buttonDateActive();

  @Source("images/btn-date-inactive.png")
  ImageResource buttonDateInactive();

  @Source("images/btn-date-normal.png")
  ImageResource buttonDate();

  @Source("images/btn-line-active.png")
  ImageResource buttonLineActive();

  @Source("images/btn-line-inactive.png")
  ImageResource buttonLineInactive();

  @Source("images/btn-line-normal.png")
  ImageResource buttonLine();

  @Source("images/btn-next-active.png")
  ImageResource buttonNextActive();

  @Source("images/btn-next-hover.png")
  ImageResource buttonNextHover();

  @Source("images/btn-next-inactive.png")
  ImageResource buttonNextInactive();

  @Source("images/btn-next-normal.png")
  ImageResource buttonNextNormal();

  @Source("images/btn-previous-active.png")
  ImageResource buttonPreviousActive();

  @Source("images/btn-previous-hover.png")
  ImageResource buttonPreviousHover();

  @Source("images/btn-previous-inactive.png")
  ImageResource buttonPreviousInactive();

  @Source("images/btn-previous-normal.png")
  ImageResource buttonPreviousNormal();

  @Source("images/btn-settings-active.png")
  ImageResource buttonSettingsActive();

  @Source("images/btn-settings-inactive.png")
  ImageResource buttonSettingsInactive();

  @Source("images/btn-settings-normal.png")
  ImageResource buttonSettingsNormal();

  @Source("images/btn-maplayers-active.png")
  ImageResource buttonLayersActive();

  @Source("images/btn-maplayers-inactive.png")
  ImageResource buttonLayersInactive();

  @Source("images/btn-maplayers-normal.png")
  ImageResource buttonLayers();

  @Source("images/btn-meta-active.png")
  ImageResource buttonMetaActive();

  @Source("images/btn-meta-inactive.png")
  ImageResource buttonMetaInactive();

  @Source("images/btn-meta-normal.png")
  ImageResource buttonMetaNormal();

  @Source("images/btn-min-active.png")
  ImageResource buttonMinActive();

  @Source("images/btn-min-mouse-down.png")
  ImageResource buttonMinMouseDown();

  @Source("images/btn-min-normal.png")
  ImageResource buttonMin();

  @Source("images/btn-legenda-active.png")
  ImageResource buttonLegendActive();

  @Source("images/btn-legenda-inactive.png")
  ImageResource buttonLegendInactive();

  @Source("images/btn-legenda-normal.png")
  ImageResource buttonLegend();

  @Source("images/btn-infolayer-active.png")
  ImageResource buttonInfoActive();

  @Source("images/btn-infolayer-inactive.png")
  ImageResource buttonInfoInactive();

  @Source("images/btn-infolayer-normal.png")
  ImageResource buttonInfo();

  @Source("images/legend-circle-overlay-32px.png")
  ImageResource legendCircleOverlay32px();

  @Source("images/legend-circle-overlay-16px.png")
  ImageResource legendCircleOverlay16px();

  @Source("images/legend-hexagon-overlay-32px.png")
  ImageResource legendHexagonOverlay32px();

  @Source("images/legend-hexagon-overlay-16px.png")
  ImageResource legendHexagonOverlay16px();

  @Source("images/btn-point-active.png")
  ImageResource buttonPointActive();

  @Source("images/btn-point-inactive.png")
  ImageResource buttonPointInactive();

  @Source("images/btn-point-normal.png")
  ImageResource buttonPoint();

  @Source("images/btn-search-animate.gif")
  ImageResource buttonSearchAnimate();

  @Source("images/btn-search-focus.gif")
  ImageResource buttonSearchFocus();

  @Source("images/btn-search-normal.gif")
  ImageResource buttonSearchNormal();

  @Source("images/btn-polygon-active.png")
  ImageResource buttonPolygonActive();

  @Source("images/btn-polygon-inactive.png")
  ImageResource buttonPolygonInactive();

  @Source("images/btn-polygon-normal.png")
  ImageResource buttonPolygon();

  @Source("images/checkbox-selected.png")
  ImageResource checkBoxSelected();

  @Source("images/checkbox.png")
  ImageResource checkBox();

  @Source("images/checkbox-disabled.png")
  ImageResource checkBoxDisabled();

  @Source("images/btn-calc-square-active.png")
  ImageResource calculatorActive();

  @Source("images/btn-calc-square-inactive.png")
  ImageResource calculatorInactive();

  @Source("images/btn-calc-square-normal.png")
  ImageResource calculatorNormal();

  @Source("images/radiobutton-selected.png")
  ImageResource radioButtonChecked();

  @Source("images/radiobutton-disabled.png")
  ImageResource radioButtonDisabled();

  @Source("images/radiobutton.png")
  ImageResource radioButton();

  @Source("images/btn-maplayers-active.png")
  ImageResource buttonMapLayersActive();

  @Source("images/btn-maplayers-inactive.png")
  ImageResource buttonMapLayersInactive();

  @Source("images/btn-maplayers-normal.png")
  ImageResource buttonMapLayers();

  @Source("images/waiting-animation.gif")
  ImageResource waitingAnimation();

  @Source("images/waiting.png")
  ImageResource waiting();

  @Source("images/error-exclamation-mark.png")
  ImageResource errorExclamationMark();

  @Source("images/btn-upload-active.png")
  ImageResource buttonUploadActive();

  @Source("images/btn-upload-normal.png")
  ImageResource buttonUploadNormal();

  @Source("images/btn-upload-inactive.png")
  ImageResource buttonUploadInactive();

  @Source("images/btn-replace-active.png")
  ImageResource buttonRefreshActive();

  @Source("images/btn-replace-inactive.png")
  ImageResource buttonRefreshInactive();

  @Source("images/btn-replace-normal.png")
  ImageResource buttonRefresh();

  /**
   * LayerPanel
   */
  @Source("images/layers-zoom-in.png")
  ImageResource layersZoomIn();

  @Source("images/toggle-layer-off.png")
  ImageResource toggleLayerOff();

  @Source("images/toggle-layer-on.png")
  ImageResource toggleLayerOn();

  @Override
  @Source("images/slider-active-block.png")
  ImageResource slider();

  @Override
  @Source("images/slider-inactive-block.png")
  ImageResource sliderDisabled();

  @Override
  @Source("images/slider-active-block.png")
  ImageResource sliderSliding();

  @Source("images/slider-inactive-background.png")
  ImageResource layerSliderBackgroundInactive();

  @Source("images/slider-active-background.png")
  ImageResource layerSliderBackgroundActive();

  @Source("images/layers-zoom-in.png")
  ImageResource layerZoomButton();

  @Source("images/layers-zoom-extent.png")
  ImageResource layerZoomExtent();

  /**
   * Markers.
   */
  @Source("images/info-marker.png")
  ImageResource informationMarker();

  @Source("images/info-marker-small.png")
  ImageResource informationMarkerSmall();

  @Source("images/m1.png")
  ImageResource highestCalculatedDepositionMarker();

  @Source("images/m2.png")
  ImageResource highestTotalDepositionMarker();

  @Source("images/m3.png")
  ImageResource highestExceedingPercentageMarker();

  @Source("images/m1+m2.png")
  ImageResource highestCalculatedAndTotalMarker();

  @Source("images/m2+m3.png")
  ImageResource highestTotalAndExceedingMarker();

  @Source("images/m1+m3.png")
  ImageResource highestCalculatedAndExceedingMarker();

  @Source("images/m1+m2+m3.png")
  ImageResource highestCalculatedAndTotalAndExceedingMarker();

  @Source("images/depositie-marker-projectbijdrage-small.png")
  ImageResource highestCalculatedDepositionMarkerSmall();

  @Source("images/depositie-marker-hoogste-depositie-small.png")
  ImageResource highestTotalDepositionMarkerSmall();

  @Source("images/depositie-marker-oranje-small.png")
  ImageResource highestExceedingPercentageMarkerSmall();

  /**
   * File icons
   */
  @Source("images/doc-icn-pdf.png")
  ImageResource fileiconPdf();

  @Source("images/doc-icn-zip.png")
  ImageResource fileiconZip();

  @Source("images/doc-icn-maps.png")
  ImageResource fileiconMaps();

  @Source("images/doc-icn-gml.png")
  ImageResource fileiconGml();

  /**
   * Notifications.
   */
  @Source("images/btn-message-empty-active.png")
  ImageResource notificationMarkerActive();

  @Source("images/btn-message-empty-hover.png")
  ImageResource notificationMarkerHover();

  @Source("images/btn-message-empty-normal.png")
  ImageResource notificationMarkerNormal();

  @Source("images/btn-messages-active.png")
  ImageResource notificationRectangularCloseActive();

  @Source("images/btn-messages-inactive.png")
  ImageResource notificationRectangularCloseInactive();

  @Source("images/btn-messages-normal.png")
  ImageResource notificationRectangularCloseNormal();

  @Source("images/btn-close-active.png")
  ImageResource notificationCloseActive();

  @Source("images/btn-close-hover.png")
  ImageResource notificationCloseHover();

  @Source("images/btn-close-normal.png")
  ImageResource notificationCloseNormal();

  /**
   * Switcheroo
   */
  @Source("images/btn-swop-active.png")
  ImageResource buttonSwitchActive();

  @Source("images/btn-swop-hover.png")
  ImageResource buttonSwitchHover();

  @Source("images/btn-swop-normal.png")
  ImageResource buttonSwitchNormal();

  /**
   * Habitat types
   */
  @Source("images/habitat-kdw-legend.png")
  ImageResource habitatKDWLegend();

  @Source("images/habitat-relevant-full.png")
  ImageResource habitatRelevantFull();

  @Source("images/habitat-relevant-partial.png")
  ImageResource habitatRelevantPartial();

  @Source("images/habitat-relevant-none.png")
  ImageResource habitatRelevantNone();

  /**
   * Farm RAV warning image
   */
  @Source("images/rav-alert.png")
  ImageResource ravAnimalWarning();
  
  /**
   * Heat content warning image
   */
  @Source("images/rav-alert.png")
  ImageResource heatContentWarning();

  /**
   * Permit Validation Rule images
   */
  @Source("images/permit-validation-rule-ok.png")
  ImageResource permitValidationRuleOk();

  @Source("images/permit-validation-rule-warning.png")
  ImageResource permitValidationRuleWarning();

  /**
   * Deposition slider
   */
  @Source("images/dep-slider-normal.png")
  ImageResource habitatSliderNormal();

  @Source("images/dep-slider-active.png")
  ImageResource habitatSliderActive();

  /**
   * Export button
   */
  @Source("images/btn-export-normal.png")
  ImageResource exportButtonNormal();

  @Source("images/btn-export-active.png")
  ImageResource exportButtonActive();

  @Source("images/btn-export-active.png")
  ImageResource exportButtonHover();

  @Source("images/btn-export-inactive.png")
  ImageResource exportButtonInactive();

  /**
   * Export types
   */
  @Source("images/exp-nationaal-active.png")
  ImageResource exportAreaTypeNationalNormal();

  @Source("images/exp-nationaal-inactive.png")
  ImageResource exportAreaTypeNationalInactive();

  @Source("images/exp-natuurgebied-active.png")
  ImageResource exportAreaTypeAssessmentNormal();

  @Source("images/exp-natuurgebied-inactive.png")
  ImageResource exportAreaTypeAssessmentInactive();

  @Source("images/exp-provincie-active.png")
  ImageResource exportAreaTypeProvincialNormal();

  @Source("images/exp-provincie-inactive.png")
  ImageResource exportAreaTypeProvincialInactive();

  /**
   * Deposition Space
   */

  @Source("images/deposition-space-almost-finished.png")
  ImageResource depositionSpaceAlmostFinished();

  @Source("images/deposition-space-almost-finished-inactive.png")
  ImageResource depositionSpaceAlmostFinishedInactive();

  @Source("images/deposition-space-available.png")
  ImageResource depositionSpaceAvailable();

  @Source("images/deposition-space-available-inactive.png")
  ImageResource depositionSpaceAvailableInactive();

  @Source("images/deposition-space-doubled.png")
  ImageResource depositionSpaceDoubled();

  @Source("images/deposition-space-doubled-inactive.png")
  ImageResource depositionSpaceDoubledInactive();

  @Source("images/deposition-space-shortage.png")
  ImageResource depositionSpaceShortage();

  @Source("images/deposition-space-shortage-inactive.png")
  ImageResource depositionSpaceShortageInactive();

  @Source("images/deposition-space-possible-shortage.png")
  ImageResource depositionSpacePossibleShortage();

  @Source("images/deposition-space-possible-shortage-inactive.png")
  ImageResource depositionSpacePossibleShortageInactive();

  /**
   * Register
   */
  @Source("images/registerstatus/register-status-unconfirmed.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateNoReservationPresent();

  @Source("images/registerstatus/register-status-waiting.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateQueued();

  @Source("images/registerstatus/register-status-waiting-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateQueuedInactive();

  @Source("images/registerstatus/register-status-pp-waiting.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePriorityProjectQueued();

  @Source("images/registerstatus/register-status-pp-waiting-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePriorityProjectQueuedInactive();

  @Source("images/registerstatus/register-status-pending-with-space.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePendingWithSpace();

  @Source("images/registerstatus/register-status-pending-with-space-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePendingWithSpaceInactive();

  @Source("images/registerstatus/register-status-pending-without-space.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePendingWithoutSpace();

  @Source("images/registerstatus/register-status-pending-without-space-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePendingWithoutSpaceInactive();

  @Source("images/registerstatus/register-status-admissable-no-space-potentially-rejectable.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstatePendingWithoutSpacePotentiallyRejectable();

  @Source("images/registerstatus/register-status-assigned.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateAssigned();

  @Source("images/registerstatus/register-status-assigned-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateAssignedInactive();

  @Source("images/registerstatus/register-status-rejected.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateRejectedWithoutSpace();

  @Source("images/registerstatus/register-status-rejected-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateRejectedWithoutSpaceInactive();

  @Source("images/registerstatus/register-status-assigned-final.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateAssignedFinal();

  @Source("images/registerstatus/register-status-assigned-final-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateAssignedFinalInactive();

  @Source("images/registerstatus/register-status-declined.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateRejectedFinal();

  @Source("images/registerstatus/register-status-declined-inactive.svg")
  @MimeType("image/svg+xml")
  DataResource applicationstateRejectedFinalInactive();

  @Source("images/register-progress-0-pct.svg")
  @MimeType("image/svg+xml")
  DataResource registerProgressZeroPct();

  @Source("images/register-progress-less-than-50-pct.svg")
  @MimeType("image/svg+xml")
  DataResource registerProgressLessThan50Pct();

  @Source("images/register-progress-more-than-50-pct.svg")
  @MimeType("image/svg+xml")
  DataResource registerProgressMoreThan50Pct();

  @Source("images/register-progress-100-pct.svg")
  @MimeType("image/svg+xml")
  DataResource registerProgressHundredPct();

  @Source("images/register-progress-done.svg")
  @MimeType("image/svg+xml")
  DataResource registerProgressDone();

  @Source("images/register-marked.svg")
  @MimeType("image/svg+xml")
  DataResource registerMarked();

  @Source("images/register-unmarked.svg")
  @MimeType("image/svg+xml")
  DataResource registerUnmarked();

  @Source("images/connect-job-queued.png")
  ImageResource jobStateInital();

  @Source("images/waiting-animation.gif")
  ImageResource jobStateRunning();

  @Source("images/connect-job-view.png")
  ImageResource jobStateCompleted();

  @Source("images/connect-job-cancelled.png")
  ImageResource jobStateCancelled();

  @Source("images/btn-marked-normal.png")
  ImageResource buttonMarked();

  @Source("images/btn-marked-inactive.png")
  ImageResource buttonMarkedInactive();

  @Source("images/btn-unmarked-normal.png")
  ImageResource buttonUnmarked();

  @Source("images/btn-unmarked-inactive.png")
  ImageResource buttonUnmarkedInactive();
}
