/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import nl.overheid.aerius.shared.domain.source.EmissionSource;

/**
 * Event when changes to on an EmissionSource are done. This event supports
 * multiple type of changes: ADDED, REMOVED, AND UPDATED.
 */
public class EmissionSourceChangeEvent extends SimpleGenericEvent<EmissionSource> {

  private final int listId;
  private final boolean selected;

  public EmissionSourceChangeEvent(final int listId, final EmissionSource obj, final CHANGE change) {
    this(listId, obj, change, false);
  }

  public EmissionSourceChangeEvent(final int listId, final EmissionSource obj, final CHANGE change, final boolean selected) {
    super(obj, change);
    this.listId = listId;
    this.selected = selected;
  }

  public int getListId() {
    return listId;
  }

  public boolean isSelected() {
    return selected;
  }
}
