/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.user.client.ui.Widget;

/**
 * Marker interface for the notification button.
 * 
 * Must have the following UiFields:
 * 
 * Area for notification text:
 * @UiField DivElement numNotificationText;
 * 
 * Clickable area:
 * @UiField FocusPanel notificationButton;
 */
public interface NotificationButton {
  Widget createAndBindUi(NotificationPanel handler);
}
