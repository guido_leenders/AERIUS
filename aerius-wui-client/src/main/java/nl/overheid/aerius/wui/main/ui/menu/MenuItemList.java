/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gwt.place.shared.Place;

/**
 * Base class of Application Menu items.
 * @param <P> Application base place class type
 * @param <T> Application switch type
 */
public abstract class MenuItemList<P extends Place, T> extends AbstractMenuItem {
  private final List<T> items;

  public MenuItemList(final ArrayList<T> items, final String testId) {
    super(testId);
    this.items = items;
  }

  public MenuItemList(final T[] items, final String testId) {
    super(testId);
    this.items = Arrays.asList(items);
  }

  public Place getDefaultPlace(final Place currentPlace) {
    return getPlaceFromSelection(currentPlace, getDefaultValue());
  }

  public String getDefaultName() {
    return getName(getDefaultValue());
  }

  /**
   * The default application switch type.
   * @return default type.
   */
  protected abstract T getDefaultValue();

  /**
   * Returns the application switch from the place.
   * @param currentPlace
   * @return
   */
  protected abstract T getValueFromPlace(final Place currentPlace);

  protected abstract P getPlaceFromSelection(final Place currentPlace, final T selection);

  protected abstract String getName(T value);

  public List<T> getItems() {
    return items;
  }
}
