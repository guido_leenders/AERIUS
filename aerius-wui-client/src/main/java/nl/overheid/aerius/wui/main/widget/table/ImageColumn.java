/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class ImageColumn<L> extends WidgetFactory<L, Image> {
  @Override
  public Image createWidget(final L object) {
    final ImageResource value = getValue(object);
    final Image image = value == null ? new Image() : new Image(value);

    if (value != null) {
      image.getElement().setAttribute("aerius-image", value.getName());
    }

    return image;
  }

  @Override
  public Widget wrapWidget(final Widget widget) {
    return new SimplePanel(widget.asWidget());
  }

  public abstract ImageResource getValue(final L object);
}
