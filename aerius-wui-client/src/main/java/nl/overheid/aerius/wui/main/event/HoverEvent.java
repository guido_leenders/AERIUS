/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Represents a hover event.
 *
 * @param <T> the type being hovered on/off
 */
public class HoverEvent<T> extends GwtEvent<HoverHandler<T>> {

  /**
   * Handler type.
   */
  private final static Type<HoverHandler<?>> TYPE = new Type<>();

  /**
   * Fires a hover event on all registered handlers in the handler
   * manager. If no such handlers exist, this method will do nothing.
   *
   * @param <T> the selected item type
   * @param source the source of the handlers
   * @param selectedItem the selected item
   * @param hoverGained whether the hover was gained or lost
   */
  public static <T> void fire(final HasHoverHandlers<T> source, final T selectedItem, final boolean hoverGained) {
    source.fireEvent(new HoverEvent<>(selectedItem, hoverGained));
  }

  /**
   * @return the type
   */
  public static Type<HoverHandler<?>> getType() {
    return TYPE;
  }

  public boolean isHoverGained() {
    return hoverGained;
  }

  private final T selectedItem;
  private final boolean hoverGained;

  /**
   * Creates a new selection event.
   *
   * @param selectedItem selected item
   */
  protected HoverEvent(final T selectedItem, final boolean hoverGained) {
    this.selectedItem = selectedItem;
    this.hoverGained = hoverGained;
  }

  // The instance knows its BeforeSelectionHandler is of type I, but the TYPE
  // field itself does not, so we have to do an unsafe cast here.
  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public final Type<HoverHandler<T>> getAssociatedType() {
    return (Type) TYPE;
  }

  /**
   * Gets the selected item.
   *
   * @return the selected item
   */
  public T getSelectedItem() {
    return selectedItem;
  }

  @Override
  protected void dispatch(final HoverHandler<T> handler) {
    if (hoverGained) {
      handler.onHoverGained(this);
    } else {
      handler.onHoverLost(this);
    }
  }
}
