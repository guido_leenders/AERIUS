/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.dialog;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SubmitButton;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.event.CancelEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

/**
 * Generic styled DialogBox with standard a cancel and confirm button at the
 * bottom.
 *
 * The DialogBox can be used with or without a {@link FormPanel}. Used without
 * the confirm will send a {@link ConfirmEvent} when confirm button is pressed.
 * With a form the dialog will send a form submit when confirm button is pressed.
 *
 * @param <T> Type of the data
 * @param <E> Type of the content panel editor
 */
public class ConfirmCancelDialog<T, E extends Editor<? super T>> extends AeriusDialogBox {
  private final FormPanel formPanel;
  private final FlowPanel buttonPanel;
  private Button confirmButton;
  final SimplePanel container = new SimplePanel();
  private final Image progressImage;
  private final String confirmButtonText;
  private SimpleBeanEditorDriver<T, E> driver;
  private final ClickHandler cancelHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      fireEvent(new CancelEvent());
      hide();
    }
  };
  private final ClickHandler confirmClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (driver == null) {
        fireEvent(new ConfirmEvent<T>(null));
      } else {
        final T data = driver.flush();

        if (driver.hasErrors()) {
          ErrorPopupController.addErrors(driver.getErrors());
        } else {
          fireEvent(new ConfirmEvent<T>(data));
        }
      }
    }
  };

  /**
   * Simple dialog without driver.
   * @param confirmText Text on confirm button
   * @param cancelText Text on cancel button
   */
  public ConfirmCancelDialog(final String confirmText, final String cancelText) {
    this(null, confirmText, cancelText);
  }

  /**
   * Constructs {@link ConfirmCancelDialog}.
   *
   * @param driver Driver for the content Editor object, used to pass content on confirm
   * @param confirmText Text on confirm button
   * @param cancelText Text on cancel button
   */
  public ConfirmCancelDialog(final SimpleBeanEditorDriver<T, E> driver, final String confirmText, final String cancelText) {
    this(driver, confirmText, cancelText, false);
  }

  /**
   * Constructs {@link ConfirmCancelDialog}.
   *
   * @param driver Driver for the content Editor object, used to pass content on confirm
   * @param confirmText Text on confirm button
   * @param cancelText Text on cancel button
   * @param form if true the dialog box will contain a {@link FormPanel}
   * @param glass
   */
  public ConfirmCancelDialog(final SimpleBeanEditorDriver<T, E> driver, final String confirmText, final String cancelText, final boolean form) {
    super();
    this.driver = driver;

    final Button cancelButton = new Button(cancelText);
    cancelButton.addClickHandler(cancelHandler);
    confirmButtonText = confirmText;
    buttonPanel = new FlowPanel();

    buttonPanel.setStyleName(R.css().confirmCancelDialogButtons());
    buttonPanel.add(cancelButton);
    setConfirmButtonForFormPanel(form);
    progressImage = new Image(R.images().waitingAnimation());
    showProgressImage(false);
    progressImage.addStyleName(R.css().confirmCancelDialogLoadingImage());
    panel.add(progressImage);
    panel.add(container);
    panel.add(buttonPanel);
    if (form) {
      formPanel = new FormPanel();
      formPanel.setWidget(panel);
      super.setWidget(formPanel);
    } else {
      formPanel = null;
      super.setWidget(panel);
    }

    cancelButton.ensureDebugId(TestID.BUTTON_DIALOG_CANCEL);
  }

  /**
   * Add cancelHandler. Call this method in the {@link #onLoad()}.
   *
   * @param handler handler to call
   * @return
   */
  public HandlerRegistration addCancelHandler(final CancelEvent.Handler handler) {
    return addHandler(handler, CancelEvent.TYPE);
  }

  /**
   * Add confirmHandler. Call this method in the {@link #onLoad()}.
   *
   * @param handler handler to call
   */
  public HandlerRegistration addConfirmHandler(final ConfirmHandler<T> handler) {
    return addHandler(handler, ConfirmEvent.getType());
  }

  /**
   * Show the progress image?
   * @param visible if true makes the image visible, otherwise hides it
   */
  public void showProgressImage(final boolean visible) {
    progressImage.setVisible(visible);
  }

  /**
   * Set the state of the confirm button.
   * @param enable if true enabled else disabled
   */
  public void enableConfirmButton(final boolean enable) {
    confirmButton.setEnabled(enable);
  }

  /**
   * ConfirmButton.
   * @return the confirmButton.
   */
  public Button getConfirmButton() {
    return confirmButton;
  }

  @Override
  public void show() {
    // Reset errors styles on widgets
    ErrorPopupController.clearWidgets();
    super.show();
  }

  /**
   * Change the function of the confirm button.
   * Any handlers previously added to the button will have to be added again.
   * @param useForFormPanel If true, the button will be a submitbutton, actually submitting the form.
   * If false, the button will be a normal button.
   */
  public void setConfirmButtonForFormPanel(final boolean useForFormPanel) {
    if (confirmButton != null) {
      buttonPanel.remove(confirmButton);
    }
    confirmButton = useForFormPanel ? new SubmitButton(confirmButtonText) : new Button(confirmButtonText);
    confirmButton.addClickHandler(confirmClickHandler);
    buttonPanel.add(confirmButton);
    confirmButton.addStyleName(R.css().primaryButton());

    confirmButton.ensureDebugId(TestID.BUTTON_DIALOG_CONFIRM);
  }

  public FormPanel getFormPanel() {
    return formPanel;
  }

  @Override
  public Widget getWidget() {
    return container.getWidget();
  }

  @Override
  public boolean remove(final Widget widget) {
    return container.remove(widget);
  }

  @Override
  public void setWidget(final Widget widget) {
    container.setWidget(widget);
  }
}
