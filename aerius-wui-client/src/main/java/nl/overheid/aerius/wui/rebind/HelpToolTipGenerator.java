/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.rebind;

import java.io.PrintWriter;

import com.google.gwt.core.ext.Generator;
import com.google.gwt.core.ext.GeneratorContext;
import com.google.gwt.core.ext.TreeLogger;
import com.google.gwt.core.ext.UnableToCompleteException;
import com.google.gwt.core.ext.typeinfo.JClassType;
import com.google.gwt.core.ext.typeinfo.JMethod;
import com.google.gwt.core.ext.typeinfo.JParameter;
import com.google.gwt.core.ext.typeinfo.TypeOracle;
import com.google.gwt.user.rebind.ClassSourceFileComposerFactory;
import com.google.gwt.user.rebind.SourceWriter;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.i18n.HelpManualUrl;
import nl.overheid.aerius.shared.i18n.HelpToolTipInfo;
import nl.overheid.aerius.shared.i18n.HelpToolTipMessages;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Generator for the {@link HelpToolTipInfo} helper interface.
 */
public class HelpToolTipGenerator extends Generator {

  private static final char SPACE = ' ';
  private static final String HELP_INFO_CLASSNAME = HelpInfo.class.getSimpleName();

  @Override
  public String generate(final TreeLogger logger, final GeneratorContext context, final String requestedClass) throws UnableToCompleteException {
      final TypeOracle typeOracle = context.getTypeOracle();
      final JClassType functionType = typeOracle.findType(requestedClass);
      final String implPackageName = functionType.getPackage().getName();
      final String implTypeName = functionType.getSimpleSourceName() + "Impl";
      final ClassSourceFileComposerFactory composerFactory = new ClassSourceFileComposerFactory(implPackageName, implTypeName);
      writeImports(composerFactory, functionType);

      final PrintWriter printWriter = context.tryCreate(logger, implPackageName, implTypeName);
      if (printWriter != null) {
        final SourceWriter sourceWriter = composerFactory.createSourceWriter(context, printWriter);

        sourceWriter.println("protected static final " + HelpToolTipMessages.class.getSimpleName() + "<String> tt = M.messages();");
        sourceWriter.println();
        // Generate the methods from the methods on the interface.
        for (final JMethod method : functionType.getInheritableMethods()) {
          sourceWriter.println("@Override");
          sourceWriter.print("public " + HELP_INFO_CLASSNAME + SPACE + method.getName() + "(");
          if (method.getParameters().length == 0) {
            sourceWriter.println(") {");
          } else {
            boolean first = true;
            for (final JParameter param : method.getParameters()) {
              if (first) {
                first = false;
              } else {
                sourceWriter.print(", ");
              }
              sourceWriter.print(param.getType().getSimpleSourceName() + SPACE + param.getName());
            }
            sourceWriter.println(") {");
          }
          sourceWriter.print("  return ");
          writeNewHelpInfo(sourceWriter, method);
          sourceWriter.println();
          sourceWriter.println("}");
        }
        sourceWriter.commit(logger);
      } // else class already generated.
      return implPackageName + "." + implTypeName;
  }

  /**
   * This method generates the code:
   * <code>new HelpInfo([message], [optional: id to manual page]);</code>
   */
  private void writeNewHelpInfo(final SourceWriter sourceWriter, final JMethod method) {
    sourceWriter.print("new " + HELP_INFO_CLASSNAME + "(tt." + method.getName() + "(");
    boolean first = true;
    for (final JParameter param : method.getParameters()) {
      if (first) {
        first = false;
      } else {
        sourceWriter.print(", ");
      }
      sourceWriter.print(param.getName());
    }
    // if HelpManualUrl annotation present generate the manual id in the method call
    final HelpManualUrl urlIdx = method.getAnnotation(HelpManualUrl.class);
    sourceWriter.print(")" + (urlIdx == null ? "" : ", " + urlIdx.value()) + ");");
  }

  /**
   * Generate the imports, including the imports parameters of methods on the interface.
   * @param composerFactory
   * @param functionType
   */
  private void writeImports(final ClassSourceFileComposerFactory composerFactory, final JClassType functionType) {
    composerFactory.addImport(M.class.getCanonicalName());
    composerFactory.addImport(HelpInfo.class.getCanonicalName());
    composerFactory.addImport(HelpToolTipMessages.class.getCanonicalName());
    composerFactory.addImport(HelpToolTipInfo.class.getCanonicalName());
    composerFactory.addImplementedInterface(functionType.getQualifiedSourceName());
    composerFactory.addImport(Singleton.class.getCanonicalName());
    composerFactory.addAnnotationDeclaration("@" + Singleton.class.getSimpleName());
    for (final JMethod method : functionType.getInheritableMethods()) {
      if (method.getParameters().length != 0) {
        for (final JParameter param : method.getParameters()) {
          composerFactory.addImport(param.getType().getQualifiedSourceName());
        }
      }
    }
  }
}
