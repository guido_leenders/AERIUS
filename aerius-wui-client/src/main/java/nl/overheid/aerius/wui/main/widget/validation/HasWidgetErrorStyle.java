/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.validation;

import com.google.gwt.user.client.ui.Widget;

/**
 * Widgets implementing this widget can a different widget to set the validation error style on.
 */
public interface HasWidgetErrorStyle {
  /**
   * The widget to set the validation error style on.
   * @param w widget
   */
  void setWidgetErrorStyle(final Widget w);

  /**
   * Returns the widget to set the error style on.
   * @return widget to set errror style on
   */
  Widget getWidgetErrorStyle();
}
