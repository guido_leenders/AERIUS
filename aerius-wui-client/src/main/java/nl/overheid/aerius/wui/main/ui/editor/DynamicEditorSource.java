/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.editor.client.adapters.EditorSource;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.ui.editor.DynamicRowEditor.RowHandler;

/**
 * {@link EditorSource}.
 */
public abstract class DynamicEditorSource<E, F extends DynamicRowEditor<E>> extends EditorSource<F> implements RowHandler {

  private final FlowPanel panel;
  private ScheduledCommand checkEmpty;
  private String debugIdPrefix = "";

  public DynamicEditorSource(final FlowPanel panel) {
    this.panel = panel;
  }

  @Override
  public final F create(final int index) {
    final F row = createRowWidget();

    panel.add(row);
    row.ensureDebugId(debugIdPrefix + String.valueOf(index));
    row.setRowHandler(this);
    row.addDeleteHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        onRemove(row.getShadowValue());
      }
    });
    return row;
  }

  public void setDebugIdPrefix(final String debugIdPrefix) {
    this.debugIdPrefix = debugIdPrefix;
  }

  /**
   * Implementations should remove the value from the list.
   * @param value value to remove
   */
  public abstract void onRemove(final E value);

  @Override
  public void dispose(final F subEditor) {
    panel.remove(subEditor);
    if (checkEmpty != null) {
      Scheduler.get().scheduleDeferred(checkEmpty);
    }
  }

  @Override
  public void setIndex(final F editor, final int index) {
    panel.insert(editor, index);
  }

  /**
   * Creates the row widget. Called by {@link #create(int)} and used to abstract
   * object creation from this class.
   *
   * @return new instance of object
   */
  protected abstract F createRowWidget();

  /**
   * Creates the data object.
   * @return
   */
  protected abstract E createObject();

  @Override
  public boolean isLast(final Widget editor) {
    return panel.getWidgetCount() == panel.getWidgetIndex(editor) + 1;
  }

  @Override
  public void handleKeyUp(final Widget row) {
    if (isLast(row) && checkEmpty != null) {
      Scheduler.get().scheduleDeferred(checkEmpty);
    }
  }

  /**
   * Callback called when a keyup event is triggered on the last row in the list.
   * @param checkEmpty scheduled command
   */
  public void setEmptyCheck(final ScheduledCommand checkEmpty) {
    this.checkEmpty = checkEmpty;
  }
}
