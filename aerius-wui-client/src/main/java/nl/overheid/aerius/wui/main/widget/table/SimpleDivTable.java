/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

public class SimpleDivTable<T> extends DivTable<T, SimpleDivTableRow> {
  /**
   * Create a simple default DivTable.
   */
  public SimpleDivTable() {
    super(false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleDivTable(final boolean loadingByDefault) {
    super(new TypedFlowPanel<SimpleDivTableRow>(), loadingByDefault);
  }

  /**
   * Create a DivTable with a custom row panel.
   *
   * @param rowPanel Custom row panel to use for rows.
   */
  public SimpleDivTable(final TypedFlowPanel<SimpleDivTableRow> rowPanel) {
    super(rowPanel, false);
  }

  /**
   * Creates a DivTable that's loading by default (or not).
   *
   * @param rowPanel Custom row panel to use for rows.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleDivTable(final TypedFlowPanel<SimpleDivTableRow> rowPanel, final boolean loadingByDefault) {
    super(rowPanel, loadingByDefault);
  }

  @Override
  protected SimpleDivTableRow createDivTableRow(final T object) {
    return new SimpleDivTableRow();
  }
}
