/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import nl.overheid.aerius.shared.domain.SortableAttribute;

public final class ColumnSortHeaderBinderUtil {
  private ColumnSortHeaderBinderUtil() {}

  /**
   * Connect the given ColumnSortHeaders - allowing for only a single one of them to be active at any time.
   * 
   * @param headers Headers to connect - be sure they are the same type.
   * 
   * @param <T> The set of attributes that are sortable.
   * 
   * @return A ColumnSortHeaderBinder you may use to disconnect or add connections.
   */
  @SafeVarargs
  public static <T extends SortableAttribute> ColumnSortHeaderBinder<T> connect(final ColumnSortHeader<T>... headers) {
    final ColumnSortHeaderBinder<T> binder = new ColumnSortHeaderBinder<T>();

    for (final ColumnSortHeader<T> header : headers) {
      binder.connect(header);
    }

    return binder;
  }
}
