/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.user.client.ui.Widget;

/**
 * Use this handler on widgets within the header of a AttachedPopupBase, so that their text can be selected,
 * and the parent's DragHandler (which makes the popup move) is skipped.
 */
public class PreventDragMouseHandler implements MouseDownHandler, MouseUpHandler, MouseMoveHandler, TouchMoveHandler, TouchStartHandler,
    TouchEndHandler {

  public void addToWidget(final Widget widget) {
    widget.addDomHandler(this, MouseDownEvent.getType());
    widget.addDomHandler(this, MouseUpEvent.getType());
    widget.addDomHandler(this, MouseMoveEvent.getType());
    widget.addDomHandler(this, TouchStartEvent.getType());
    widget.addDomHandler(this, TouchEndEvent.getType());
    widget.addDomHandler(this, TouchMoveEvent.getType());
  }

  @Override
  public void onMouseDown(final MouseDownEvent event) {
    haltEvent(event);
  }

  @Override
  public void onMouseMove(final MouseMoveEvent event) {
    haltEvent(event);
  }

  @Override
  public void onMouseUp(final MouseUpEvent event) {
    haltEvent(event);
  }

  @Override
  public void onTouchMove(final TouchMoveEvent event) {
    haltEvent(event);
  }

  @Override
  public void onTouchEnd(final TouchEndEvent event) {
    haltEvent(event);
  }

  @Override
  public void onTouchStart(final TouchStartEvent event) {
    haltEvent(event);
  }

  private void haltEvent(final DomEvent<?> event) {
    event.stopPropagation();
  }
}
