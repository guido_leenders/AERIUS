/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.menu;

import com.google.gwt.place.shared.Place;

/**
 * Data class for left side menu items.
 */
public abstract class SimpleMenuItem extends AbstractMenuItem {
  private final String title;

  /**
   * Construct with display title.
   * @param title display title
   */
  public SimpleMenuItem(final String title) {
    this(title, null);
  }

  /**
   * Construct with display title and test ID.
   * @param title display title
   * @param testId id for testing only
   */
  public SimpleMenuItem(final String title, final String testId) {
    super(testId);
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  /**
   * Returns the place to jump to. The argument is the current place, which can
   * be used by the implementation to change the link to go to.
   * @param currentPlace current place
   * @return place to go to
   */
  public abstract Place getPlace(Place currentPlace);

  /**
   * Returns if the given place matches the current menu item, meaning this menu
   * item should be shown as the active menu item. In the application only one
   * menu item should be active at any time. This must be enforced by the
   * implementation.
   *
   * @param place current place
   * @return returns true if the place matches the current menu item
   */
  @Override
  public abstract boolean isActivePlace(Place place);
}
