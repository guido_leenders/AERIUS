/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;

import nl.overheid.aerius.wui.main.util.development.DevPanel;
import nl.overheid.aerius.wui.main.util.development.DevPanelImpl;

public class AeriusClientModule extends AbstractGinModule {
  @Override
  protected void configure() {
    bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);

    bind(DevPanel.class).to(DevPanelImpl.class);
  }

  /**
   * Subclass to inject eventbus in constructor.
   */
  public static class AeriusPlaceController extends PlaceController {
    @Inject
    public AeriusPlaceController(final EventBus eventBus) {
      super(eventBus);
    }

    public AeriusPlaceController(final EventBus eventBus, final DefaultDelegate delegate) {
      super(eventBus, delegate);
    }
  }
}
