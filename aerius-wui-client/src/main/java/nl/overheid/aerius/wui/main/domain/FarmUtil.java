/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.domain;

import com.google.gwt.resources.client.ImageResource;

import nl.overheid.aerius.shared.domain.sector.category.AnimalType;
import nl.overheid.aerius.shared.domain.source.FarmEmissionSource;
import nl.overheid.aerius.shared.domain.source.FarmLodgingEmissions;
import nl.overheid.aerius.shared.domain.source.FarmLodgingStandardEmissions;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Util class for Farm related data.
 */
public final class FarmUtil {

  private FarmUtil() {
  }

  /**
   * Returns the image resource for an emission source containing {@link FarmEmissionValues}. If different animals are present the mixed icon is
   * returned, otherwise the animal specific icon.
   *
   * @param source emission source to get icon for
   * @return image resource representing the animals in the emission source
   */
  public static ImageResource getFarmImageResource(final FarmEmissionSource source) {
    ImageResource ir = null;

    for (final FarmLodgingEmissions fl: source.getEmissionSubSources()) {
      final ImageResource nir = !(fl instanceof FarmLodgingStandardEmissions) || ((FarmLodgingStandardEmissions) fl).getCategory() == null
          ? null : animalResource(((FarmLodgingStandardEmissions) fl).getCategory().getName());

      if (ir != null && ir != nir) {
        ir = null;
        break;
      }
      ir = nir;
    }
    return ir == null ? R.images().iconSectorAnimalMixed() : ir;
  }

  /**
   * Returns the image resource for an emission source containing {@link FarmEmissionValues}. If different animals are present the mixed icon is
   * returned, otherwise the animal specific icon.
   *
   * @param source emission source to get icon for
   * @return image resource representing the animals in the emission source
   */
  public static ImageResource getFarmImageResource(final FarmLodgingEmissions source) {
    ImageResource nir = null;
    if (source instanceof FarmLodgingStandardEmissions) {
      final FarmLodgingStandardEmissions standardEmissions = (FarmLodgingStandardEmissions) source;
      final String category = standardEmissions.getCategory() == null ? null : standardEmissions.getCategory().getName();

      if (!standardEmissions.isValid()) {
        nir = category == null ? R.images().iconSectorAnimalMixedWarning() : animalResourceWarning(category);
      } else if (standardEmissions.hasStacking()) {
        nir = category == null ? R.images().iconSectorAnimalMixedAdjusted() : animalResourceAdjusted(category);
      } else {
        nir = category == null ? R.images().iconSectorAnimalMixed() : animalResource(category);
      }
    }

    return nir == null ? R.images().iconSectorAnimalMixed() : nir;
  }

  /**
   * Returns a animal specific ImageResource base on the name. The name should be a RAV-code name.
   *
   * @param category RAV-code
   * @return Returns ImageResource
   */
  public static ImageResource animalResource(final String category) {
    final ImageResource ir;
    switch (AnimalType.getByCode(category)) {
    case COW:
      ir = R.images().iconSectorAnimalCow();
      break;
    case SHEEP:
      ir = R.images().iconSectorAnimalSheep();
      break;
    case GOAT:
      ir = R.images().iconSectorAnimalGoat();
      break;
    case PIG:
      ir = R.images().iconSectorAnimalPig();
      break;
    case CHICKEN:
      ir = R.images().iconSectorAnimalChicken();
      break;
    case TURKEY:
      ir = R.images().iconSectorAnimalTurkey();
      break;
    case HORSE:
      ir = R.images().iconSectorAnimalHorse();
      break;
    case DUCK:
      ir = R.images().iconSectorAnimalDuck();
      break;
    case MINK:
      ir = R.images().iconSectorAnimalMink();
      break;
    case RABBIT:
      ir = R.images().iconSectorAnimalRabbit();
      break;
    case GUINEA_FOWL:
      ir = R.images().iconSectorAnimalGuineaFowl();
      break;
    case OSTRICH:
      ir = R.images().iconSectorAnimalOstrich();
      break;
    default: // show Mixed icon for other categories...
      ir = R.images().iconSectorAnimalMixed();
      break;
    }
    return ir;
  }

  /**
   * Returns a animal specific ImageResource base on the name. The name should be a RAV-code name.
   * This is the image for additional or reducing measures
   *
   * @param category RAV-code
   * @return Returns ImageResource
   */
  public static ImageResource animalResourceAdjusted(final String category) {
    final ImageResource ir;
    switch (AnimalType.getByCode(category)) {
    case COW:
      ir = R.images().iconSectorAnimalCowAdjusted();
      break;
    case SHEEP:
      ir = R.images().iconSectorAnimalSheepAdjusted();
      break;
    case GOAT:
      ir = R.images().iconSectorAnimalGoatAdjusted();
      break;
    case PIG:
      ir = R.images().iconSectorAnimalPigAdjusted();
      break;
    case CHICKEN:
      ir = R.images().iconSectorAnimalChickenAdjusted();
      break;
    case TURKEY:
      ir = R.images().iconSectorAnimalTurkeyAdjusted();
      break;
    case HORSE:
      ir = R.images().iconSectorAnimalHorseAdjusted();
      break;
    case DUCK:
      ir = R.images().iconSectorAnimalDuckAdjusted();
      break;
    case MINK:
      ir = R.images().iconSectorAnimalMinkAdjusted();
      break;
    case RABBIT:
      ir = R.images().iconSectorAnimalRabbitAdjusted();
      break;
    case GUINEA_FOWL:
      ir = R.images().iconSectorAnimalGuineaFowlAdjusted();
      break;
    case OSTRICH:
      ir = R.images().iconSectorAnimalOstrichAdjusted();
      break;
    default: // show Mixed icon for other categories...
      ir = R.images().iconSectorAnimalMixedAdjusted();
      break;
    }
    return ir;
  }

  /**
   * Returns a animal specific ImageResource base on the name. The name should be a RAV-code name.
   * This is the image for additional or reducing measures that are not allowed
   *
   * @param category RAV-code
   * @return Returns ImageResource
   */
  public static ImageResource animalResourceWarning(final String category) {
    final ImageResource ir;
    switch (AnimalType.getByCode(category)) {
    case COW:
      ir = R.images().iconSectorAnimalCowWarning();
      break;
    case SHEEP:
      ir = R.images().iconSectorAnimalSheepWarning();
      break;
    case GOAT:
      ir = R.images().iconSectorAnimalGoatWarning();
      break;
    case PIG:
      ir = R.images().iconSectorAnimalPigWarning();
      break;
    case CHICKEN:
      ir = R.images().iconSectorAnimalChickenWarning();
      break;
    case TURKEY:
      ir = R.images().iconSectorAnimalTurkeyWarning();
      break;
    case HORSE:
      ir = R.images().iconSectorAnimalHorseWarning();
      break;
    case DUCK:
      ir = R.images().iconSectorAnimalDuckWarning();
      break;
    case MINK:
      ir = R.images().iconSectorAnimalMinkWarning();
      break;
    case RABBIT:
      ir = R.images().iconSectorAnimalRabbitWarning();
      break;
    case GUINEA_FOWL:
      ir = R.images().iconSectorAnimalGuineaFowlWarning();
      break;
    case OSTRICH:
      ir = R.images().iconSectorAnimalOstrichWarning();
      break;
    default: // show Mixed icon for other categories...
      ir = R.images().iconSectorAnimalMixedWarning();
      break;
    }
    return ir;
  }
}
