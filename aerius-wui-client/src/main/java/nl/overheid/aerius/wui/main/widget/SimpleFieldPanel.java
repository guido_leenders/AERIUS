/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

public abstract class SimpleFieldPanel extends FlowPanel {
  public interface CustomStyle extends CssResource {
    String sectionTitle();

    String panel();

    String fields();
  }

  @UiField CustomStyle style;

  private HasText title;
  private Widget titleContainer;

  private final FlowPanel fields = new FlowPanel();

  protected void initPanels(final HasText title, final Widget titleContainer) {
    this.title = title;
    this.titleContainer = titleContainer;
  }

  protected void init() {
    addStyleName(R.css().flex());
    addStyleName(R.css().columnsClean());
    addStyleName(style.panel());

    titleContainer.asWidget().addStyleName(style.sectionTitle());
    titleContainer.asWidget().addStyleName(R.css().noShrink());

    fields.addStyleName(R.css().flex());
    fields.addStyleName(R.css().columnsClean());
    fields.addStyleName(style.fields());

    super.add(titleContainer);
    super.add(fields);
  }

  public void setTitleText(final String titleText) {
    if (title != null) {
      title.setText(titleText);
    }
  }

  @Override
  public void add(final Widget w) {
    fields.add(w);
  }

  @Override
  public void insert(final Widget w, final int beforeIndex) {
    fields.insert(w, beforeIndex);
  }

  @Override
  public Widget getWidget(final int index) {
    return fields.getWidget(index);
  }

  @Override
  public int getWidgetCount() {
    return fields.getWidgetCount();
  }

  @Override
  public int getWidgetIndex(final Widget child) {
    return fields.getWidgetIndex(child);
  }

  @Override
  public boolean remove(final int index) {
    return fields.remove(index);
  }

  @Override
  public int getWidgetIndex(final IsWidget child) {
    return fields.getWidgetIndex(child);
  }

  @Override
  public void add(final IsWidget w) {
    fields.add(w);
  }

  @Override
  public void insert(final IsWidget w, final int beforeIndex) {
    fields.insert(w, beforeIndex);
  }
}
