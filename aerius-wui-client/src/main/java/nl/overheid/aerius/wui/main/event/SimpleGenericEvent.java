/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import com.google.web.bindery.event.shared.binder.GenericEvent;

/**
 * Simple event, encapsulating a generically typed object.
 * 
 * @param <E> The object type this event is wrapping
 */
public abstract class SimpleGenericEvent<E> extends GenericEvent {

  /**
   * Change done on the object.
   */
  public enum CHANGE {
    /**
     * Add data object event.
     */
    ADD,

    /**
     * Remove data object event.
     */
    REMOVE,

    /**
     * Update data object event.
     */
    UPDATE
  }

  private final CHANGE change;
  private final E object;

  /**
   * Create a {@link SimpleGenericEvent} with the given object.
   * 
   * @param obj object to encapsulate
   */
  public SimpleGenericEvent(final E obj) {
    this.object = obj;
    change = null;
  }

  /**
   * Create a {@link SimpleGenericEvent} with the given object and given change.
   * @param obj object to encapsulate
   * @param change change on the object
   */
  public SimpleGenericEvent(final E obj, final CHANGE change) {
    this.object = obj;
    this.change = change;
  }

  public CHANGE getChange() {
    return change;
  }

  public E getValue() {
    return object;
  }
}
