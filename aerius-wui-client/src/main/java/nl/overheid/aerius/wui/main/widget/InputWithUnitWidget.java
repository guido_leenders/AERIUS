/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.validation.HasWidgetErrorStyle;
import nl.overheid.aerius.wui.main.widget.validation.IsWidgetErrorStyle;

/**
 * Widget to display a input box with a unit displayed inside the borders of the
 * input and removes the default style of the inner input element.
 */
public class InputWithUnitWidget extends SimplePanel implements IsWidgetErrorStyle {
  private final SpanElement unitLabel = Document.get().createSpanElement();

  public InputWithUnitWidget() {
    // FIXME If a ui binder (or something else, which is a bit more unlikely) is calling setStyleName instead of addStyleNames, this style is overwritten.
    setStyleName(R.css().inputWithUnit());

    unitLabel.setClassName(R.css().inputWithUnitName());
  }

  @Override
  public Widget getFocusWidget() {
    return getWidget();
  }

  /**
   * Set the actual input widget. This should be an Widget that contains a
   * input element.
   *
   * @param w Input widget to wrap
   */
  @Override
  public void setWidget(final Widget w) {
    super.setWidget(w);
    if (w instanceof HasWidgetErrorStyle) {
      ((HasWidgetErrorStyle) w).setWidgetErrorStyle(this);
    }
    getElement().appendChild(unitLabel);
  }

  /**
   * Sets the unit on the input widget.
   *
   * @param unit unit to display
   */
  public void setUnit(final String unit) {
    unitLabel.setInnerHTML(unit);
  }
}
