/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class TabbedContentPanel extends Composite implements HasSelectionHandlers<Integer> {
  interface TabbedContentPanelUiBinder extends UiBinder<Widget, TabbedContentPanel> {}

  private static final TabbedContentPanelUiBinder UI_BINDER = GWT.create(TabbedContentPanelUiBinder.class);

  @UiField SimpleTabPanel tabPanel;
  @UiField SwitchPanel switchPanel;

  private int selectedTab;

  public TabbedContentPanel() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  @UiChild
  public void addTab(final Widget w) {
    tabPanel.add(w);
  }

  @UiHandler("tabPanel")
  void onTabSelect(final SelectionEvent<Integer> e) {
    selectedTab = e.getSelectedItem().intValue();
    switchPanel.showWidget(e.getSelectedItem());
  }

  @UiChild
  public void addContent(final Widget w) {
    switchPanel.add(w);

    // If this is the first widget we add, show it.
    if (switchPanel.getWidgetCount() == 1) {
      switchPanel.showWidget(0);
    }
  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<Integer> handler) {
    return tabPanel.addSelectionHandler(handler);
  }

  public int getSelectedTab() {
    return this.selectedTab;
  }

  /**
   * Set the style of the content panel.
   * 
   * @param style Style to set.
   */
  public void setContentStyleName(final String style) {
    switchPanel.setStyleName(style);
  }
}
