/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.ui.Panel;

public class GridScrollVisitor implements GridVisitor, ScrollHandler {
  /**
   * At what percentage of scrolling a new data load will be triggered.
   */
  private static final double TRIGGER_SENSITIVITY = 0.99;

  private Panel rowPanel;
  private final DataTableFetcher provider;

  private DivTable<?, ?> values;

  public GridScrollVisitor(final DataTableFetcher provider) {
    this.provider = provider;
  }

  @Override
  public final void onScroll(final ScrollEvent event) {
    final double scrollHeight = rowPanel.getElement().getScrollHeight();
    final double scrollTop = rowPanel.getElement().getScrollTop();
    final double clientHeight = rowPanel.getElement().getClientHeight();
    if ((scrollTop + clientHeight) / scrollHeight > TRIGGER_SENSITIVITY) {
      provider.loadData(values.size());
    }
  }

  @Override
  public void exposeRowPanel(final Panel rowPanel) {
    this.rowPanel = rowPanel;

    rowPanel.asWidget().addDomHandler(this, ScrollEvent.getType());
  }

  @Override
  public void exposeDataTable(final IsDataTable<?> values) {
    this.values = values.asDataTable();
  }
}
