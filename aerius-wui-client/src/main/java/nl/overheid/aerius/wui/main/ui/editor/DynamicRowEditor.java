/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.util.ArrayList;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorDelegate;
import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.i18n.HelpInfo;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;

/**
 * Row widget implemented as Editor for dynamic list widget. It also needs the value to determine no value is set, which is
 * used to dynamically add or remove a new row at the end. This class also provides a delete button.
 */
public abstract class DynamicRowEditor<E> extends Composite implements Editor<E>, KeyUpHandler, ValueAwareEditor<E> {

  /**
   * Interface to get a handle back to the list manager.
   */
  interface RowHandler {
    /**
    * Returns true if the given editor is the last editor in the list.
    * @param editor editor to check
    * @return true if the given editor is the last editor in the list.
    */
    boolean isLast(Widget rowEditor);

    /**
     * Called when the user does a keyup in any field. Subclasses should register their input fields
     * on the key handler in the {@link DynamicRowEditor} parent class.
     * @param row Widget of the row on which the key up event is triggered
     */
    void handleKeyUp(Widget row);
  }

  private final Button deleteButton = new Button();
  private final FlowPanel row = new FlowPanel();
  private final ArrayList<HandlerRegistration> handlers = new ArrayList<HandlerRegistration>();
  private RowHandler rowHandler;
  private E shadowValue;
  private EditorDelegate<E> delegate;

  public DynamicRowEditor(final HelpPopupController hpC, final HelpInfo ttDeleteHelp) {
    this(hpC, ttDeleteHelp, true);
  }

  public DynamicRowEditor(final HelpPopupController hpC, final HelpInfo ttDeleteHelp, final boolean deleteButtonRightAligned) {
    initWidget(row);
    row.setStyleName(R.css().calculatorRow());
    row.addStyleName(R.css().flex());
    row.addStyleName(R.css().categorizedEmissionRow());
    hpC.addWidget(deleteButton, ttDeleteHelp);
    deleteButton.addStyleName(R.css().buttonDeleteSmall());
    deleteButton.setStyleName(R.css().buttonDeleteLeftAligned(), !deleteButtonRightAligned);
    row.add(deleteButton);
  }

  protected FlowPanel getRow() {
    return row;
  }

  public void setRowHandler(final RowHandler rowHandler) {
    this.rowHandler = rowHandler;
  }

  protected abstract boolean isEmpty();

  protected void resetPlaceHolders() {
    // no-op
  }

  protected E getShadowValue() {
    return shadowValue;
  }

  /**
   * Called when the user makes a change. It checks if the change was on the last
   * item of the list and if true check deferred (because at key up the value is not
   * set yet) if the value is not empty. If it's not empty a new empty row will be
   * added. Call this method with null if it should be triggered not via a key-up event.
   * @param event key up event or null if not via a key up triggered.
   */
  @Override
  public void onKeyUp(final KeyUpEvent event) {
    rowHandler.handleKeyUp(this);
  }

  /**
   * Add handler that is called when the delete button is clicked.
   *
   * @param deleteHandler Handler
   */
  public void addDeleteHandler(final ClickHandler deleteHandler) {
    handlers.add(deleteButton.addClickHandler(deleteHandler));
  }

  @Override
  public void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);
    deleteButton.ensureDebugId(baseID + "-" + TestID.BUTTON_DELETE);
  }

  @Override
  public void flush() {
    // no-op
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  @Override
  public void setDelegate(final EditorDelegate<E> delegate) {
    this.delegate = delegate;
  }

  protected EditorDelegate<E> getDelegate() {
    return delegate;
  }

  @Override
  public void setValue(final E value) {
    this.shadowValue = value;
  }
}
