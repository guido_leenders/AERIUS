/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Tracker for a collection of {@link CollapsiblePanel}s ensuring only a single
 * panel is open at any one time.
 */
public class CollapsibleStackPanel extends FlowPanel implements HasValueChangeHandlers<Boolean>, CanCollapse {
  private final class EventHandlers implements ValueChangeHandler<Boolean> {
    @Override
    public void onValueChange(final ValueChangeEvent<Boolean> e) {
      // If the panel is opened (safe cast), select it (solely)
      if (e.getValue()) {
        selectItem((CanCollapse) e.getSource());
      } else {
        // If the panel is closed and is the same as the one that was already selected, deselect it and forget selection.
        if (e.getSource() == expandedPanel) {
          selectItem(null);
        }
      }

      ValueChangeEvent.fire(CollapsibleStackPanel.this, e.getValue());
    }
  }

  private final EventHandlers handlers = new EventHandlers();
  private CanCollapse expandedPanel;

  @Override
  public void insert(final Widget w, final int index) {
    track(w);

    super.insert(w, index);
  }

  @Override
  public void add(final Widget w) {
    super.add(w);

    track(w);
  }

  public void addUntracked(final Widget w) {
    super.add(w);
  }

  /**
   * Tracks and adds the given {@link Widget} if it is or contains a {@link CollapsiblePanel}.
   *
   * @param w the widget to track
   */
  private void track(final Widget w) {
    // If the Widget is of type CanCollapse, add a change listener
    if (w instanceof CanCollapse) {
      track((CanCollapse) w);
    } else if (w instanceof IsCollapsiblePanel) {
      track(((IsCollapsiblePanel) w).asCollapsible());
    }
  }

  /**
   * Tracks and adds the given {@link CollapsiblePanel}.
   *
   * @param panel the panel to add
   */
  private void track(final CanCollapse panel) {
    if (panel.getValue()) {
      selectItem(panel);
    }

    panel.addValueChangeHandler(handlers);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  private void selectItem(final CanCollapse newPanel) {
    if (expandedPanel != null && expandedPanel != newPanel) {
      expandedPanel.setValue(false, false);
    }

    expandedPanel = newPanel;
  }

  @Override
  public void setValue(final Boolean open) {
    if (!open) {
      selectItem(null);
    }

    // We wouldn't know which of our children to expand if collapse were false, so we're not doing anything
  }

  @Override
  public void setValue(final Boolean open, final boolean fireEvent) {
    setValue(open);
  }

  @Override
  @Deprecated
  public boolean isCollapsed() {
    return !getValue();
  }

  @Override
  public Boolean getValue() {
    return expandedPanel != null;
  }
}
