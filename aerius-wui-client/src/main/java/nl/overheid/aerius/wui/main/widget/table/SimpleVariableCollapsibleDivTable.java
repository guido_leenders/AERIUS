/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

/**
 * A {@link DivTable} that contains a {@link SimpleCollapsibleDivTableRow} with custom content on each
 * row.
 *
 * @param <L> Type of objects that rows will represent.
 */
public abstract class SimpleVariableCollapsibleDivTable<L> extends VariableCollapsibleDivTable<L, SimpleVariableCollapsibleDivTableRow> {
  /**
   * Create a default {@link CollapsibleDivTable} with a row panel that will accept and
   * track collapsible rows. Rows will only collapse/expand when clicking the explicit
   * 'open/close' button on the collapsible row.
   */
  public SimpleVariableCollapsibleDivTable() {
    super(false);
  }

  /**
   * Create a default {@link CollapsibleDivTable} with a row panel that will accept and track
   * collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * or just the toggle butotn.
   */
  public SimpleVariableCollapsibleDivTable(final boolean interactiveRows) {
    super(interactiveRows, false);
  }

  /**
   * Create a default {@link CollapsibleDivTable} with a row panel that will accept and track
   * collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * or just the toggle button.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleVariableCollapsibleDivTable(final boolean interactiveRows, final boolean loadingByDefault) {
    super(interactiveRows, loadingByDefault, new TypedFlowPanel<SimpleVariableCollapsibleDivTableRow>());
  }

  /**
   * Create a {@link CollapsibleDivTable}. For example to not track
   * the collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   * @param panel Custom panel to put rows in.
   */
  public SimpleVariableCollapsibleDivTable(final boolean interactiveRows, final boolean loadingByDefault,
      final TypedFlowPanel<SimpleVariableCollapsibleDivTableRow> panel) {
    super(interactiveRows, loadingByDefault, panel);
  }

  @Override
  protected SimpleVariableCollapsibleDivTableRow createDivTableRow(final boolean hasCollapsibleContent) {
    return new SimpleVariableCollapsibleDivTableRow(hasCollapsibleContent);
  }

  @Override
  protected DivTableRow createTitleRow(final L object) {
    return new SimpleDivTableRow();
  }
}
