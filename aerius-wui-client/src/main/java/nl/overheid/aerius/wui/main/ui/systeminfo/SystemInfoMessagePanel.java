/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.systeminfo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.SystemInfoMessageEvent;

@Singleton
public class SystemInfoMessagePanel extends Composite {

  interface SystemInfoPanelUiBinder extends UiBinder<Widget, SystemInfoMessagePanel> {}

  interface SystemInfoPanelViewImplEventBinder extends EventBinder<SystemInfoMessagePanel> {}

  private static final SystemInfoPanelUiBinder UI_BINDER = GWT.create(SystemInfoPanelUiBinder.class);

  private final SystemInfoPanelViewImplEventBinder eventBinder = GWT.create(SystemInfoPanelViewImplEventBinder.class);

  @UiField HTML infoLabel;

  @Inject
  public SystemInfoMessagePanel(final EventBus eventBus) {
    initWidget(UI_BINDER.createAndBindUi(this));
    eventBinder.bindEventHandlers(this, eventBus);
    infoLabel.ensureDebugId(TestID.SYSTEM_MESSAGE_BANNER);
  }

  @EventHandler
  void onSystemMessage(final SystemInfoMessageEvent event) {
    infoLabel.setHTML(event.getValue());
  }

}
