/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.filter;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.util.scaling.ScalingUtil;
import nl.overheid.aerius.wui.main.widget.DoubleVerticalSliderBar.ValueRange;

class VerticalLabels extends Composite {
  interface VerticalLabelsUiBinder extends UiBinder<Widget, VerticalLabels> {}

  private static VerticalLabelsUiBinder uiBinder = GWT.create(VerticalLabelsUiBinder.class);

  public interface CustomStyle extends CssResource {
    String label();

    String labelFocus();
  }

  @UiField CustomStyle style;
  @UiField FlowPanel labels;

  public VerticalLabels(final ScalingUtil scaler) {
    initWidget(uiBinder.createAndBindUi(this));
  }

  public void updateLabels(final ArrayList<Double> rangeValues) {
    labels.clear();
    final int numLabels = rangeValues.size();
    for (int i = 0; i < numLabels; i++) {
      final Label label = new Label(FormatUtil.formatDecimalCapped(rangeValues.get(i)));
      label.addStyleName(style.label());
      labels.insert(label, 0);
    }
  }

  public void setActiveRange(final ValueRange value) {
    for (int i = 0; i < labels.getWidgetCount(); i++) {
      labels.getWidget(i).setStyleName(style.labelFocus(), value.getLowerValue() <= i && i < value.getUpperValue());
    }
  }
}
