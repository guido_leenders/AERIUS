/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util.development;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.main.util.NotificationUtil;

@Singleton
public class DevPanelImpl extends Composite implements DevPanel {

  interface DevPanelUiBinder extends UiBinder<Widget, DevPanelImpl> {}

  private final static DevPanelUiBinder UI_BINDER = GWT.create(DevPanelUiBinder.class);

  private final EventBus eventBus;

  @UiField(provided = true) EmbeddedDevPanel addon;

  @Inject
  public DevPanelImpl(final EventBus eventBus, final EmbeddedDevPanel addon) {
    this.eventBus = eventBus;
    this.addon = addon;

    initWidget(UI_BINDER.createAndBindUi(this));

    GWT.log("DevPanel initialized.");
    if (addon != null) {
      GWT.log("DevPanel addon attached.");
    }
  }

  @UiHandler("refresh")
  void onRefresh(final ClickEvent e) {
    Window.Location.reload();
  }

  @UiHandler("sendNotification")
  void onSendNotificationClick(final ClickEvent e) {
    NotificationUtil.broadcastMessage(eventBus, "Flash! [thunder]");
  }
}
