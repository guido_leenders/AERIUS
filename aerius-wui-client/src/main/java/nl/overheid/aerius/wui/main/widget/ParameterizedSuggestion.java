/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

/**
 * Abstract suggestion initialized with a parameterized object.
 *
 * @param <E> The object type
 */
public abstract class ParameterizedSuggestion<E> implements Suggestion {
  protected E object;

  /**
   * Initialize a suggestion with the given object.
   *
   * @param object The object.
   */
  public ParameterizedSuggestion(final E object) {
    this.object = object;
  }

  @Override
  public abstract String getDisplayString();

  @Override
  public abstract String getReplacementString();

  public E getObject() {
    return object;
  }

  /**
   * Returns the style of the suggestion. This will be set for the suggestion in the suggestion box list.
   * @return name of the style or null if no style should be applied
   */
  public String getStyleName() {
    return null;
  }
  public void enableSort() {
  }
}
