/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SingleSelectionModel;

import nl.overheid.aerius.wui.main.resources.R;

public class SelectListBox<T> extends Composite implements HasValueChangeHandlers<T>, LeafValueEditor<T> {
  interface SelectListBoxUiBinder extends UiBinder<Widget, SelectListBox<?>> {}

  private static final SelectListBoxUiBinder UI_BINDER = GWT.create(SelectListBoxUiBinder.class);

  public interface CustomStyle extends CssResource {
    String popupStyle();
  }

  protected @UiField Label labelField;

  protected final AutoSizePopupPanel popup = new AutoSizePopupPanel(true);
  protected final FlowPanel popupWidget = new FlowPanel();

  protected final SingleSelectionModel<T> selectionModel;
  protected WidgetFactory<T, SelectOption<T>> widgetFactory;

  protected final HashMap<T, SelectOption<T>> values = new HashMap<>();

  @UiField FlowPanel labelContainer;
  @UiField CustomStyle style;

  private final ClickHandler fieldClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (popup.isShowing()) {
        labelContainer.getElement().getStyle().setProperty("borderBottomLeftRadius", "2px");
        labelContainer.getElement().getStyle().setProperty("borderBottomRightRadius", "2px");
        popup.hide();
      } else {
        popupWidget.getElement().getStyle().setProperty("minWidth", labelContainer.getOffsetWidth() - 3d, Unit.PX);
        labelContainer.getElement().getStyle().setProperty("borderBottomLeftRadius", "0px");
        labelContainer.getElement().getStyle().setProperty("borderBottomRightRadius", "0px");
        popup.setPopupPosition(labelContainer.getAbsoluteLeft() + 1, labelContainer.getAbsoluteTop() + labelContainer.getOffsetHeight() - 1);
        popup.show();
      }
    }
  };
  private String baseID;
  private SelectTextOption<String> defaultTextSelection;
  private String defaultText;

  private T defaultValue;
  private boolean nullable;

  public SelectListBox() {
    this(null, null);
  }

  public SelectListBox(final WidgetFactory<T, SelectOption<T>> widgetFactory) {
    this(null, widgetFactory);
  }

  public SelectListBox(final ProvidesKey<T> keyProvider) {
    this(keyProvider, null);
  }

  public SelectListBox(final ProvidesKey<T> keyProvider, final WidgetFactory<T, SelectOption<T>> widgetFactory) {
    this.selectionModel = new SingleSelectionModel<T>(keyProvider);
    this.widgetFactory = widgetFactory;

    popup.setWidget(popupWidget);

    init();

    labelContainer.addDomHandler(fieldClickHandler, ClickEvent.getType());
    popup.addAutoHidePartner(labelContainer.getElement());
    popup.setAnimationEnabled(true);
    popup.setAnimationType(AnimationType.ROLL_DOWN);
    popup.getElement().getStyle().setProperty("borderTop", "1px solid #bbb");
    popupWidget.setStyleName(style.popupStyle());
    popup.getElement().getStyle().setZIndex(R.css().zIndexGeneric());
  }

  protected void init() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public void setPopupStyle(final String styleName) {
    popupWidget.addStyleName(styleName);
  }

  public void setValues(final T[] values) {
    for (final T value : values) {
      addValue(value);
    }
  }

  public void setValues(final Collection<T> values) {
    for (final T value : values) {
      addValue(value);
    }
  }

  @Override
  public void setValue(final T value) {
    selectionModel.clear();

    setSelectedValue(value, true);
  }

  public void setSelectedValue(final T value, final boolean select) {
    setSelectedValue(value, select, true);
  }

  public void setSelectedValue(final T value, final boolean select, final boolean hard) {
    if (value == null) {
      deselect();
      return;
    }

    for (final SelectOption<T> option : values.values()) {
      option.setSelected(false, false);
    }

    if (values.containsKey(value)) {
      selectionModel.setSelected(value, select);
      values.get(value).setSelected(select, false);
    }

    if (hard) {
      updateDefaultTextSelection();
      updateHeaderText();
    }
  }

  public void addValue(final T value) {
    final SelectOption<T> widg = widgetFactory.createWidget(value);

    widg.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        deselect();

        selectionModel.setSelected(value, event.getValue());

        selectionChanged();
      }
    });

    if (baseID != null) {
      widg.asWidget().ensureDebugId(baseID + "-" + widg.getItemText(value));
    }

    values.put(value, widg);
    popupWidget.add(widg);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    this.baseID = baseID;

    labelContainer.ensureDebugId(baseID);
  }

  @UiChild
  public void addTemplate(final WidgetFactory<T, SelectOption<T>> widgetFactory) {
    this.widgetFactory = widgetFactory;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  private void selectionChanged() {
    updateDefaultTextSelection();
    updateHeaderText();

    ValueChangeEvent.fire(this, selectionModel.getSelectedObject());

    popup.hide();
  }

  private void updateDefaultTextSelection() {
    if (defaultTextSelection != null) {
      defaultTextSelection.setSelected(selectionModel.getSelectedObject() != null, false);
    }
  }

  protected void updateHeaderText() {
    final String headerText = getHeaderText(selectionModel.getSelectedObject());

    labelField.setText(headerText);
    labelField.setTitle(headerText);
  }

  public String getHeaderText(final T item) {
    return item == null ? defaultText : values.get(item).getItemText(item);
  }

  public void setDefaultText(final String defaultText) {
    if (defaultTextSelection == null) {
      defaultTextSelection = createDefaultTextWidget(defaultText);
    } else {
      defaultTextSelection.setValue(defaultText);
    }
    this.defaultText = defaultText;

    updateHeaderText();
  }

  private SelectTextOption<String> createDefaultTextWidget(final String value) {
    final SelectTextOption<String> wdg = new SelectTextOption<String>(value) {
      @Override
      public String getItemText(final String value) {
        return value;
      }
    };

    wdg.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        deselect();
      }
    });

    if (baseID != null) {
      wdg.asWidget().ensureDebugId(baseID + "-" + wdg.getItemText(value));
    }

    popupWidget.add(wdg);
    return wdg;
  }

  @Override
  public T getValue() {
    final T selectedObject = selectionModel.getSelectedObject();

    if (!isNullable() && selectedObject == null) {
      return defaultValue;
    }

    return selectedObject;
  }

  public HashSet<T> getValues() {
    return new HashSet<>(values.keySet());
  }

  public void deselect() {
    final SelectOption<T> selectOption = values.get(selectionModel.getSelectedObject());
    if (selectOption != null) {
      selectOption.setSelected(false, false);
    }

    selectionModel.clear();
    updateDefaultTextSelection();
    updateHeaderText();
  }

  public void clear() {
    values.clear();
    popupWidget.clear();
  }

  public boolean isNullable() {
    return nullable;
  }

  public void setNullable(final boolean nullable) {
    this.nullable = nullable;
  }

  public void setDefaultValue(final T defaultValue) {
    this.defaultValue = defaultValue;
  }
}
