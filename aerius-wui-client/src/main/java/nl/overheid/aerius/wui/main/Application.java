/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.context.Context;
import nl.overheid.aerius.shared.domain.context.UserContext;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.shared.service.ContextServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.SecurityUtil;

/**
 * Main application class with the applications {@link EntryPoint}.
 */
public class Application implements EntryPoint {
  protected static final String SPLASH_SCREEN_ID = "splash";
  protected static final String SPLASH_STYLE_ID = "splashStyle";
  protected static final String LOADING_IMG_ID = "loadingAnimation";

  private static final String SPLASH_ERROR = "error";
  private static final String STATUS_TEXT_ID = "message";

  @Inject private ContextServiceAsync contextServiceAsync;

  @Inject private AeriusContextProvider<?, ?> contextProvider;

  private ApplicationRoot appRoot;

  @Override
  public void onModuleLoad() {
    //As a first step register the UncaughtExcpetionHandler to log those
    //errors to the server.

    AeriusGinjector.INSTANCE.inject(this);
    R.init();
    startUpContext();
  }

  /**
   * Retrieves the Context object from the server. If that succeeds it continues
   * with the rest of the application startup process.
   */
  private void startUpContext() {
    try {
      contextServiceAsync.getContext(new AsyncCallback<Context>() {
        @Override
        public void onSuccess(final Context context) {
          try {
            contextProvider.setContext(context);
            SecurityUtil.init(contextServiceAsync, (String) context.getSetting(SharedConstantsEnum.LOGOUT_URL, true));
            startUpUserContext();
          } catch (final Exception e) {
            onFinishError("startUpContext#getContext", e);
          }
        }

        @Override
        public void onFailure(final Throwable e) {
          onFinishError("startUpContext#getContext", e);
        }
      });
    } catch (final Exception e) {
      onFinishError("startUpContext", e);
    }
  }

  /**
   * Retrieves the UserContext from the server. If that succeeds it continues
   * with the rest of the application startup process.
   */
  private void startUpUserContext() {
    try {
      contextServiceAsync.getUserContext(new AsyncCallback<UserContext>() {
        @Override
        public void onSuccess(final UserContext context) {
          contextProvider.setUserContext(context);

          onFinishedLoading();
        }

        @Override
        public void onFailure(final Throwable e) {
          onFinishError("startUpUserContext#getUserContext", e);
        }
      });
    } catch (final Exception e) {
      onFinishError("startUpUserContext", e);
    }
  }

  /**
   * Initializes the application activity managers, user interface, and
   * starts the application by handling the current history token.
   */
  private void onFinishedLoading() {
    // Only finish loading once
    if (appRoot != null) {
      return;
    }

    appRoot = new ApplicationRoot();
    try {
      appRoot.startUp();
      // Remove the splash screen elements
      DOM.getElementById(SPLASH_SCREEN_ID).removeFromParent();
      DOM.getElementById(SPLASH_STYLE_ID).removeFromParent();
      RootPanel.getBodyElement().removeAttribute("style");
      appRoot.showDisplay();
    } catch (final Exception e) {
      try {
        appRoot.hideDisplay();
      } finally {
        onFinishError("onFinishedLoading", e);
      }
    }
  }

  /**
   * Called when at any time during startup an fatal error occurs and the
   * application can't continue. The screen will be cleaned and only an
   * error message will be shown. The error, if possible, will be logged on the
   * server.
   *
   * @param method Method in which the error occurred
   * @param e Exception that was thrown
   */
  private void onFinishError(final String method, final Throwable e) {
    if (SecurityUtil.isAuthTimeoutException(e) || SecurityUtil.isAuthTimeoutException(e.getCause())) {
      Window.Location.reload();
    } else {
      try {
        Logger.getLogger("Application#" + method).log(Level.SEVERE, e.getMessage(), e);
      } finally {
        displayDefaultError(M.getErrorMessage(e));
        logAeriusException(e);
      }
    }
  }

  private void logAeriusException(final Throwable e) {
    if (e instanceof AeriusException) {
      final Reason reason = ((AeriusException) e).getReason();

      if (reason == Reason.AUTHORIZATION_ERROR) {
        Logger.getLogger("Application#onFinishError").log(Level.INFO, "Authorization exception. Session may be expired.");
        // FIXME Redirect to some login page + notify session expired session / invalid user
        // Fall-through for now.
      }
    }
  }

  private void displayDefaultError(final String errorMessage) {
    DOM.getElementById(LOADING_IMG_ID).removeFromParent();
    DOM.getElementById(SPLASH_SCREEN_ID).addClassName(SPLASH_ERROR);
    DOM.getElementById(STATUS_TEXT_ID).setInnerText(errorMessage);
  }
}
