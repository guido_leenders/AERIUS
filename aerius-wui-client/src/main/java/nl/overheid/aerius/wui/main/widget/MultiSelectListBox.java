/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel.AnimationType;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;

import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.TextUtil;
import nl.overheid.aerius.wui.main.util.TextUtil.TextFactory;

public class MultiSelectListBox<T> extends Composite implements HasValueChangeHandlers<HashSet<T>>, LeafValueEditor<HashSet<T>> {
  private static final String KOMMA_SEPERATOR = ", ";

  interface MultiSelectListBoxUiBinder extends UiBinder<Widget, MultiSelectListBox<?>> {}

  private static final MultiSelectListBoxUiBinder UI_BINDER = GWT.create(MultiSelectListBoxUiBinder.class);

  protected final AutoSizePopupPanel popup = new AutoSizePopupPanel(true);
  protected final FlowPanel popupWidget = new FlowPanel();

  protected final MultiSelectionModel<T> selectionModel;
  protected WidgetFactory<T, SelectOption<T>> widgetFactory;

  protected final HashMap<T, SelectOption<T>> values = new HashMap<>();

  @UiField SelectListBox.CustomStyle style;

  @UiField FlowPanel labelContainer;
  protected @UiField Label labelField;

  private final ClickHandler fieldClickHandler = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      if (!popup.isShowing()) {
        popupWidget.getElement().getStyle().setProperty("minWidth", labelContainer.getOffsetWidth() - 3, Unit.PX);
        labelContainer.getElement().getStyle().setProperty("borderBottomLeftRadius", "0px");
        labelContainer.getElement().getStyle().setProperty("borderBottomRightRadius", "0px");
        popup.setPopupPosition(labelContainer.getAbsoluteLeft() + 1, labelContainer.getAbsoluteTop() + labelContainer.getOffsetHeight() - 1);
        popup.show();
      } else {
        labelContainer.getElement().getStyle().setProperty("borderBottomLeftRadius", "2px");
        labelContainer.getElement().getStyle().setProperty("borderBottomRightRadius", "2px");
        popup.hide();
      }
    }
  };
  private String defaultText;
  private boolean singleSelectionMode;
  private String baseID;
  private SelectTextOption<String> defaultTextSelection;

  public MultiSelectListBox() {
    this(null, null);
  }

  public MultiSelectListBox(final WidgetFactory<T, SelectOption<T>> widgetFactory) {
    this(null, widgetFactory);
  }

  public MultiSelectListBox(final ProvidesKey<T> keyProvider) {
    this(keyProvider, null);
  }

  public MultiSelectListBox(final ProvidesKey<T> keyProvider, final WidgetFactory<T, SelectOption<T>> widgetFactory) {
    this.selectionModel = new MultiSelectionModel<T>(keyProvider) {
      @Override
      public void setSelected(final T item, final boolean selected) {
        if (isSingleSelectionMode()) {
          deselect();
        }

        super.setSelected(item, selected);
      }
    };
    this.widgetFactory = widgetFactory;

    popup.setWidget(popupWidget);

    init();

    labelContainer.addDomHandler(fieldClickHandler, ClickEvent.getType());
    popup.addAutoHidePartner(labelContainer.getElement());
    popup.setAnimationEnabled(true);
    popup.setAnimationType(AnimationType.ROLL_DOWN);
    popup.getElement().getStyle().setProperty("borderTop", "1px solid #bbb");
    popupWidget.setStyleName(style.popupStyle());
    popup.getElement().getStyle().setZIndex(R.css().zIndexGeneric());
  }

  protected void init() {
    initWidget(UI_BINDER.createAndBindUi(this));
  }

  public void setPopupStyle(final String styleName) {
    popupWidget.addStyleName(styleName);
  }

  public void setValues(final T[] values) {
    for (final T value : values) {
      addValue(value);
    }
  }

  public void setValues(final Collection<T> values) {
    for (final T value : values) {
      addValue(value);
    }
  }

  public void setSelectedValues(final T[] values, final boolean select) {
    for (final T value : values) {
      setSelectedValue(value, select);
    }

    updateDefaultTextSelection();
    updateHeaderText();
  }

  @Override
  public void setValue(final HashSet<T> value) {
    selectionModel.clear();

    for (final SelectOption<T> option : values.values()) {
      option.setSelected(false, false);
    }

    setSelectedValues(value, true);
  }

  public void setSelectedValues(final Collection<T> values, final boolean select) {
    deselect();
    for (final T value : values) {
      setSelectedValue(value, select, false);
    }

    updateDefaultTextSelection();
    updateHeaderText();
  }

  public void setSelectedValue(final T value, final boolean select) {
    setSelectedValue(value, select, true);
  }

  public void setSelectedValue(final T value, final boolean select, final boolean hard) {
    if (value == null) {
      deselect();
      return;
    }

    if (values.containsKey(value)) {
      selectionModel.setSelected(value, select);
      values.get(value).setSelected(select, false);
    }

    if (hard) {
      updateDefaultTextSelection();
      updateHeaderText();
    }
  }

  public void addValue(final T value) {
    final SelectOption<T> widg = widgetFactory.createWidget(value);

    widg.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        selectionModel.setSelected(value, event.getValue());

        selectionChanged();
      }
    });

    if (baseID != null) {
      widg.asWidget().ensureDebugId(baseID + "-" + widg.getItemText(value));
    }

    values.put(value, widg);
    popupWidget.add(widg);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    this.baseID = baseID;

    labelContainer.ensureDebugId(baseID);
  }

  @UiChild
  public void addTemplate(final WidgetFactory<T, SelectOption<T>> widgetFactory) {
    this.widgetFactory = widgetFactory;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<HashSet<T>> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  private void selectionChanged() {
    updateDefaultTextSelection();
    updateHeaderText();

    ValueChangeEvent.fire(this, (HashSet<T>) selectionModel.getSelectedSet());

    if (singleSelectionMode) {
      popup.hide();
    }
  }

  private void updateDefaultTextSelection() {
    if (defaultTextSelection != null) {
      defaultTextSelection.setSelected(selectionModel.getSelectedSet().isEmpty(), false);
    }
  }

  protected void updateHeaderText() {
    final String headerText = getHeaderText(selectionModel.getSelectedSet());

    labelField.setText(headerText);
    labelField.setTitle(headerText);
  }

  public String getHeaderText(final Set<T> selectedSet) {
    return selectedSet.isEmpty() ? defaultText : TextUtil.joinItems(selectedSet.iterator(), new TextFactory<T>() {
      @Override
      public String getText(final T item) {
        return values.get(item).getItemText(item);
      }
    }, KOMMA_SEPERATOR);
  }

  public void setDefaultText(final String defaultText) {
    if (defaultTextSelection == null) {
      defaultTextSelection = createDefaultTextWidget(defaultText);
    } else {
      defaultTextSelection.setValue(defaultText);
    }
    this.defaultText = defaultText;

    updateHeaderText();
  }

  private SelectTextOption<String> createDefaultTextWidget(final String value) {
    final SelectTextOption<String> wdg = new SelectTextOption<String>(value) {
      @Override
      public String getItemText(final String value) {
        return value;
      }
    };

    wdg.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        deselect();
      }
    });

    if (baseID != null) {
      wdg.asWidget().ensureDebugId(baseID + "-" + wdg.getItemText(value));
    }

    popupWidget.add(wdg);
    return wdg;
  }

  @Override
  public HashSet<T> getValue() {
    return getSelectedValues();
  }

  public HashSet<T> getSelectedValues() {
    return (HashSet<T>) selectionModel.getSelectedSet();
  }

  public HashSet<T> getValues() {
    return new HashSet<>(values.keySet());
  }

  public HashSet<T> getSelectedValues(final boolean allIfEmpty) {
    return selectionModel.getSelectedSet().isEmpty() ? getValues() : getSelectedValues();
  }

  public boolean isSingleSelectionMode() {
    return singleSelectionMode;
  }

  public void setSingleSelectionMode(final boolean singleSelectionMode) {
    this.singleSelectionMode = singleSelectionMode;
  }

  public void deselect() {
    for (final T thing : selectionModel.getSelectedSet()) {
      values.get(thing).setSelected(false, false);
    }

    selectionModel.clear();
    updateDefaultTextSelection();
    updateHeaderText();
  }

  public void clear() {
    values.clear();
    popupWidget.clear();
  }
}
