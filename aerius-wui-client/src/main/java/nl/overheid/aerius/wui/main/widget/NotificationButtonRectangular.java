/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;

public class NotificationButtonRectangular implements NotificationButton {
  interface NotificationButtonRectangularUiBinder extends UiBinder<Widget, NotificationPanel> {}
  private static NotificationButtonRectangularUiBinder uiBinder = GWT.create(NotificationButtonRectangularUiBinder.class);

  @Override
  public Widget createAndBindUi(final NotificationPanel ui) {
    return uiBinder.createAndBindUi(ui);
  }
}
