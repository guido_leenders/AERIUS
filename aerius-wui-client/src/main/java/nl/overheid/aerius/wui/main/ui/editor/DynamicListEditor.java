/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.util.List;

import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.editor.client.adapters.ListEditor;

/**
 * ListEditor with special functionality to have an empty row as the last entry which
 * is automatically created once the user adds data in the last row.
 */
public class DynamicListEditor<T, E extends DynamicRowEditor<T>> extends ListEditor<T, E> {

  private final DynamicEditorSource<T, E> source;
  private final ScheduledCommand checkEmpty = new ScheduledCommand() {
    @Override
    public void execute() {
      if (getEditors().isEmpty() || !getEditors().get(getEditors().size() - 1).isEmpty()) {
        addEmptyRow();
      }
    }
  };

  public DynamicListEditor(final DynamicEditorSource<T, E> source) {
    super(source);
    this.source = source;
    source.setEmptyCheck(checkEmpty);
  }

  @Override
  public void flush() {
    removeEmptyRows();
    super.flush();
    addEmptyRow();
  }

  /**
   * Adds an empty row to the list and returns the new editor.
   * @return returns the newly created editor for the new last added empty row
   */
  @Ignore // added ignore else gwt editor frameworks sees this method as part of the value.
  public E addEmptyRow() {
    getList().add(source.createObject());
    return getEditors().get(getEditors().size() - 1);
  }

  /**
   * Remove all rows that are empty.
   */
  private void removeEmptyRows() {
    for (int i = getEditors().size() - 1; i >= 0; i--) {
      if (getEditors().get(i).isEmpty()) {
        getList().remove(i);
      }
    }
  }

  @Override
  public void setValue(final List<T> value) {
    super.setValue(value);
    addEmptyRow();
  }

  /**
   * Reset content otherwise placeholder text not visible, but only the default values.
   */
  public void resetPlaceholders() {
    for (final E editor : getEditors()) {
      editor.resetPlaceHolders();
    }
  }
}
