/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.validation;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.editor.client.EditorError;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

/**
 * Controller for popup showing error message and styling of input fields with errors.
 */
public final class ErrorPopupController {
  private static final ErrorPopupPanel ERROR_POPUP_PANEL = new ErrorPopupPanel();

  private static final List<Widget> LAST_WIDGETS_WITH_ERRORS = new ArrayList<Widget>();

  private static final List<HandlerRegistration> ERROR_HANDLERS = new ArrayList<HandlerRegistration>();

  private static final ClickHandler REMOVE_ERROR_CLICK_HANDLER = new ClickHandler() {
    @Override
    public void onClick(final ClickEvent event) {
      final Widget widget = (Widget) event.getSource();
      addError(widget, null);
      // remove handlers
      for (final HandlerRegistration handle : ERROR_HANDLERS) {
        handle.removeHandler();
      }
    }
  };

  private ErrorPopupController() {}

  /**
   * Clears the error style on all the widgets on which the error state was set by {@link #addErrors(List)}.
   */
  public static void clearWidgets() {
    for (final Widget w : LAST_WIDGETS_WITH_ERRORS) {
      w.setStyleName(R.css().errorInput(), false);
      w.unsinkEvents(Event.ONCLICK);
    }
    LAST_WIDGETS_WITH_ERRORS.clear();
    ERROR_POPUP_PANEL.hide();
  }

  /**
   * Give the list of errors style the input fields in the errors as containing an error. When using {@link #addErrors(List)}, also make sure the
   * error style on the widgets is correctly removed by calling {@link #clearWidgets()}. Call the method prior to initializing the widgets on which
   * the errors possible can be set.
   *
   * @param errors List of errors to traverse
   */
  public static void addErrors(final List<EditorError> errors) {
    clearWidgets();

    boolean first = true;
    for (final EditorError editorError : errors) {
      if (editorError.getUserData() instanceof Widget) {
        final Widget w = (Widget) editorError.getUserData();

        w.addStyleName(R.css().errorInput());
        LAST_WIDGETS_WITH_ERRORS.add(w);
        if (first) {
          addError(w, editorError.getMessage());
          first = false;
        }
      }
    }
  }

  /**
   * Remove the error from the widget.
   *
   * @param widget widget to clear
   */
  public static void clearError(final Widget widget) {
    addError(widget, null);
  }

  /**
   * @see {@link #addError(Widget, Widget, String)}. Shows the error style on the widget given.
   *
   * @param widget Widget to show the error message relative to
   * @param errorMessage The error message or null to hide the popup
   */
  public static void addError(final Widget widget, final String errorMessage) {
    final Widget focusWidget = widget instanceof IsWidgetErrorStyle ? ((IsWidgetErrorStyle) widget).getFocusWidget() : widget;
    addError(focusWidget, widget, errorMessage);
  }

  /**
   * Shows the error message in a popup relative to the widget and adds a style to the errorStyledWidget. Mostly the widget and errorStyleWidget are
   * the same, but when the input widget is part of a complex widget the error style needs probably be set on a parent widget surrounding the input
   * widget.
   * <p>
   * If the errorMessage is null the error style is removed and the popup is hidden.
   *
   * @param widget Widget to show the error message relative to
   * @param errorStyledWidget Widget to show the error message on.
   * @param errorMessage The error message or null to hide the popup
   */
  public static void addError(final Widget widget, final Widget errorStyledWidget, final String errorMessage) {
    showError(widget, errorStyledWidget, errorMessage);
  }

  private static void showError(final Widget widget, final Widget errorStyledWidget, final String errorMessage) {
    if (errorMessage == null || errorMessage.isEmpty()) {
      // empty error is clear error, but only on widget that it applies to.
      if (LAST_WIDGETS_WITH_ERRORS.contains(widget)) {
        LAST_WIDGETS_WITH_ERRORS.remove(widget);
        widget.unsinkEvents(Event.ONCLICK);
        if (ERROR_POPUP_PANEL.isShowing()) {
          ERROR_POPUP_PANEL.hide(widget, errorStyledWidget);
        }
      }
    } else {
      ERROR_POPUP_PANEL.show(widget, errorStyledWidget, errorMessage);
      LAST_WIDGETS_WITH_ERRORS.add(widget);

      // add click handler to remove error
      widget.sinkEvents(Event.ONCLICK);
      ERROR_HANDLERS.add(widget.addHandler(REMOVE_ERROR_CLICK_HANDLER, ClickEvent.getType()));
    }
  }
}
