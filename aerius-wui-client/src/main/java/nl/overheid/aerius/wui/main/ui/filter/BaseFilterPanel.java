/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.filter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.domain.Filter;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.AnimatedLabel;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;

/**
 * Base panel for a collapsible panel containing a filter, a submit button, and nothing else.
 *
 * Extending classes have full UI control.
 *
 * Some fields are public because the package is likely not shared with extending components. (which is required for GWT binder generation)
 */
public abstract class BaseFilterPanel<F extends Filter> extends Composite implements HasValue<F> {
  public @UiField AnimatedLabel titleLabel;
  public @UiField CollapsiblePanel filterPanel;

  public @UiField Button applyButton;
  public @UiField Button resetButton;

  protected F value;

  @Override
  protected void initWidget(final Widget widget) {
    super.initWidget(widget);

    resetButton.ensureDebugId(TestID.BUTTON_FILTER_RESET);
    applyButton.ensureDebugId(TestID.BUTTON_FILTER_APPLY);
    filterPanel.ensureDebugId(TestID.DIV_FILTER_PANEL);
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<F> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public F getValue() {
    return value;
  }

  @Override
  public void setValue(final F value) {
    this.value = value;

    updateDisplayLabel();
  }

  @Override
  public void setValue(final F value, final boolean fireEvents) {
    setValue(value);

    if (fireEvents) {
      ValueChangeEvent.fire(this, value);
    }
  }

  protected void setTitleText(final String text) {
    titleLabel.setText(text, true);
  }

  protected void setTitleText(final String text, final boolean direction) {
    titleLabel.setText(text, direction);
  }

  @UiHandler("applyButton")
  public void onApplyButton(final ClickEvent event) {
    filterPanel.setValue(false, true);
  }

  @UiHandler("resetButton")
  public void onResetButton(final ClickEvent event) {
    value.fillDefault();
    setValue(value);
  }

  @UiHandler("filterPanel")
  public void onCollapseChanged(final ValueChangeEvent<Boolean> event) {
    if (event.getValue()) {
      setTitleText(M.messages().filterExpandTitle(), false);
    } else {
      submit();
    }
  }

  protected void submit() {
    updateFilter();
    ValueChangeEvent.fire(this, value);
    updateDisplayLabel();
  }

  protected void updateDisplayLabel() {
    if (!filterPanel.getValue()) {
      setTitleText(getTitleLabel());
    }
  }

  protected abstract String getTitleLabel();

  protected abstract void updateFilter();

}
