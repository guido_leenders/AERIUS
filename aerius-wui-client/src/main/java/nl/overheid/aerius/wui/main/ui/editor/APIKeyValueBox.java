/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.validation.NotEmptyValidator;
import nl.overheid.aerius.wui.main.ui.validation.APIKeyValidator;

public class APIKeyValueBox extends TextValueBox {
  public APIKeyValueBox() {
    super(M.messages().emailAddress());

    addValidator(new NotEmptyValidator() {
      @Override
      protected String getMessage(final String value) {
        return M.messages().ivRequiredInput(getLabel());
      }
    });
    addValidator(new APIKeyValidator());
  }
}
