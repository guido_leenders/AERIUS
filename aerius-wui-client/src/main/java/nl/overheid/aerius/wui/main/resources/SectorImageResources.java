/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Image resources for sectors.
 */
public interface SectorImageResources extends ClientBundle {

  @Source("images/sector/animal-chicken.png")
  ImageResource iconSectorAnimalChicken();

  @Source("images/sector/animal-chicken_adjusted.png")
  ImageResource iconSectorAnimalChickenAdjusted();

  @Source("images/sector/animal-chicken_warning.png")
  ImageResource iconSectorAnimalChickenWarning();

  @Source("images/sector/animal-cow.png")
  ImageResource iconSectorAnimalCow();

  @Source("images/sector/animal-cow_adjusted.png")
  ImageResource iconSectorAnimalCowAdjusted();

  @Source("images/sector/animal-cow_warning.png")
  ImageResource iconSectorAnimalCowWarning();

  @Source("images/sector/animal-duck.png")
  ImageResource iconSectorAnimalDuck();

  @Source("images/sector/animal-duck_adjusted.png")
  ImageResource iconSectorAnimalDuckAdjusted();

  @Source("images/sector/animal-duck_warning.png")
  ImageResource iconSectorAnimalDuckWarning();

  @Source("images/sector/animal-guinea-fowl.png")
  ImageResource iconSectorAnimalGuineaFowl();

  @Source("images/sector/animal-guinea-fowl_adjusted.png")
  ImageResource iconSectorAnimalGuineaFowlAdjusted();

  @Source("images/sector/animal-guinea-fowl_warning.png")
  ImageResource iconSectorAnimalGuineaFowlWarning();

  @Source("images/sector/animal-goat.png")
  ImageResource iconSectorAnimalGoat();

  @Source("images/sector/animal-goat_adjusted.png")
  ImageResource iconSectorAnimalGoatAdjusted();

  @Source("images/sector/animal-goat_warning.png")
  ImageResource iconSectorAnimalGoatWarning();

  @Source("images/sector/animal-horse.png")
  ImageResource iconSectorAnimalHorse();

  @Source("images/sector/animal-horse_adjusted.png")
  ImageResource iconSectorAnimalHorseAdjusted();

  @Source("images/sector/animal-horse_warning.png")
  ImageResource iconSectorAnimalHorseWarning();

  @Source("images/sector/animal-mink.png")
  ImageResource iconSectorAnimalMink();

  @Source("images/sector/animal-mink_adjusted.png")
  ImageResource iconSectorAnimalMinkAdjusted();

  @Source("images/sector/animal-mink_warning.png")
  ImageResource iconSectorAnimalMinkWarning();

  @Source("images/sector/animal-mixed.png")
  ImageResource iconSectorAnimalMixed();

  @Source("images/sector/animal-mixed_adjusted.png")
  ImageResource iconSectorAnimalMixedAdjusted();

  @Source("images/sector/animal-mixed_warning.png")
  ImageResource iconSectorAnimalMixedWarning();

  @Source("images/sector/animal-ostrich.png")
  ImageResource iconSectorAnimalOstrich();

  @Source("images/sector/animal-ostrich_adjusted.png")
  ImageResource iconSectorAnimalOstrichAdjusted();

  @Source("images/sector/animal-ostrich_warning.png")
  ImageResource iconSectorAnimalOstrichWarning();

  @Source("images/sector/animal-pig.png")
  ImageResource iconSectorAnimalPig();

  @Source("images/sector/animal-pig_adjusted.png")
  ImageResource iconSectorAnimalPigAdjusted();

  @Source("images/sector/animal-pig_warning.png")
  ImageResource iconSectorAnimalPigWarning();

  @Source("images/sector/animal-rabbit.png")
  ImageResource iconSectorAnimalRabbit();

  @Source("images/sector/animal-rabbit_adjusted.png")
  ImageResource iconSectorAnimalRabbitAdjusted();

  @Source("images/sector/animal-rabbit_warning.png")
  ImageResource iconSectorAnimalRabbitWarning();

  @Source("images/sector/animal-sheep.png")
  ImageResource iconSectorAnimalSheep();

  @Source("images/sector/animal-sheep_adjusted.png")
  ImageResource iconSectorAnimalSheepAdjusted();

  @Source("images/sector/animal-sheep_warning.png")
  ImageResource iconSectorAnimalSheepWarning();

  @Source("images/sector/animal-turkey.png")
  ImageResource iconSectorAnimalTurkey();

  @Source("images/sector/animal-turkey_adjusted.png")
  ImageResource iconSectorAnimalTurkeyAdjusted();

  @Source("images/sector/animal-turkey_warning.png")
  ImageResource iconSectorAnimalTurkeyWarning();

  @Source("images/sector/aviation.png")
  ImageResource iconSectorAviation();

  @Source("images/sector/aviation-aerodrome.png")
  ImageResource iconSectorAviationAerodome();

  @Source("images/sector/aviation-take-off.png")
  ImageResource iconSectorAviationTakeOff();

  @Source("images/sector/aviation-taxi.png")
  ImageResource iconSectorAviationTaxi();

  @Source("images/sector/aviation-touch-down.png")
  ImageResource iconSectorAviationTouchDown();

  @Source("images/sector/construction.png")
  ImageResource iconSectorConstruction();

  @Source("images/sector/consumers.png")
  ImageResource iconSectorConsumers();

  @Source("images/sector/energy.png")
  ImageResource iconSectorEnergy();

  @Source("images/sector/energy-power-plant.png")
  ImageResource iconSectorEnergyPowerPlant();

  @Source("images/sector/energy-production-distribution.png")
  ImageResource iconSectorEnergyProductionDistribution();

  @Source("images/sector/farm.png")
  ImageResource iconSectorFarm();

  @Source("images/sector/farm-fertilizer.png")
  ImageResource iconSectorFarmFertilizer();

  @Source("images/sector/farm-lodge.png")
  ImageResource iconSectorFarmLodge();

  @Source("images/sector/farm-manure-storage.png")
  ImageResource iconSectorFarmManureStorage();

  @Source("images/sector/farm-other.png")
  ImageResource iconSectorFarmOther();

  @Source("images/sector/farm-pasture.png")
  ImageResource iconSectorFarmPasture();

  @Source("images/sector/greenhouse.png")
  ImageResource iconSectorGreenhouse();

  @Source("images/sector/hdo.png")
  ImageResource iconSectorHdo();

  @Source("images/sector/industry.png")
  ImageResource iconSectorIndustry();

  @Source("images/sector/industry-base-material.png")
  ImageResource iconSectorIndustryBaseMaterial();

  @Source("images/sector/industry-building-materials.png")
  ImageResource iconSectorIndustryBuildingMaterials();

  @Source("images/sector/industry-chemical-industry.png")
  ImageResource iconSectorIndustryChemicalIndustry();

  @Source("images/sector/industry-food-industry.png")
  ImageResource iconSectorIndustryFoodIndustry();

  @Source("images/sector/industry-metal-industry.png")
  ImageResource iconSectorIndustryMetalIndustry();

  @Source("images/sector/mining.png")
  ImageResource iconSectorMining();

  @Source("images/sector/offices-shops.png")
  ImageResource iconSectorOfficesShops();

  @Source("images/sector/other.png")
  ImageResource iconSectorOther();

  @Source("images/sector/plan.png")
  ImageResource iconSectorPlan();

  @Source("images/sector/rail-emplacement.png")
  ImageResource iconSectorRailEmplacement();

  @Source("images/sector/rail-transport.png")
  ImageResource iconSectorRailTransport();

  @Source("images/sector/recreation.png")
  ImageResource iconSectorRecreation();

  @Source("images/sector/road-construction-industry.png")
  ImageResource iconSectorRoadConstructionIndustry();

  @Source("images/sector/road-farm.png")
  ImageResource iconSectorRoadFarm();

  @Source("images/sector/road-freeway.png")
  ImageResource iconSectorRoadFreeway();

  @Source("images/sector/road-other.png")
  ImageResource iconSectorRoadOther();
  
  @Source("images/sector/road-consumer.png")
  ImageResource iconSectorRoadConsumer();

  @Source("images/sector/road-non-urban.png")
  ImageResource iconSectorRoadNonUrban();

  @Source("images/sector/road-urban.png")
  ImageResource iconSectorRoadUrban();

  @Source("images/sector/road-underlying-network.png")
  ImageResource iconSectorRoadUnderlyingNetwork();

  @Source("images/sector/shipping-fishing.png")
  ImageResource iconSectorShippingFishing();

  @Source("images/sector/shipping-inland.png")
  ImageResource iconSectorShippingInland();

  @Source("images/sector/shipping-inland-dock.png")
  ImageResource iconSectorShippingInlandDocked();

  @Source("images/sector/shipping-maritime-dock.png")
  ImageResource iconSectorShippingDock();

  @Source("images/sector/shipping-maritime-mooring.png")
  ImageResource iconSectorShippingMaritimeMooring();

  @Source("images/sector/shipping-maritime-ncp.png")
  ImageResource iconSectorShippingMaritimeNCP();

  @Source("images/sector/shipping-recreational.png")
  ImageResource iconSectorShippingRecreational();

  @Source("images/sector/waste-management.png")
  ImageResource iconSectorWasteManagement();

  @Source("images/sector/farm.png")
  ImageResource iconSectorGroupAgriculture();

  @Source("images/sector/shipping-maritime-mooring.png")
  ImageResource iconSectorGroupShipping();

  @Source("images/sector/traffic-and-transport.png")
  ImageResource iconSectorGroupTrafficAndTransport();

  @Source("images/sector/abroad.png")
  ImageResource iconSectorGroupAbroad();

  @Source("images/sector/measurement-correction.png")
  ImageResource iconSectorGroupMeasurementCorrection();

  @Source("images/sector/background-deposition.png")
  ImageResource iconSectorGroupBackgroundDeposition();

  @Source("images/sector/returned-deposition.png")
  ImageResource iconSectorGroupReturnedDeposition();

  @Source("images/sector/returned-limburg.png")
  ImageResource iconSectorGroupReturnedLimburg();

  @Source("images/sector/raised.png")
  ImageResource iconSectorGroupRaised();
}
