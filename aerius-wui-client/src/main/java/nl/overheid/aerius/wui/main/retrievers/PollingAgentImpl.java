/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.retrievers;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Abstract polling agent. The agent polls the server until the using class calls {@link #stop()} or the server returns an error. The agent supports
 * one polling at a time. If a new polling is started the previous one is stopped.
 * <p>
 * The agent is guarded against incorrect calls. When a polling is started and stopped and a new polling is started it is possible the first polling
 * called the server before it was stopped. This would mean the server could return from that polling call, but the data should be ignored, because a
 * new polling was started.
 */
public abstract class PollingAgentImpl<M, C> implements PollingAgent<M, C> {
  private final int pollWaitTime;
  private PollingAsyncCallback<M, C> pollingCallback;

  private static final int DEFAULT_POLL_WAIT_TIME = 1000;

  public PollingAgentImpl() {
    this(DEFAULT_POLL_WAIT_TIME);
  }

  /**
   * @param pollWaitTime time between calls to the server
   */
  protected PollingAgentImpl(final int pollWaitTime) {
    this.pollWaitTime = pollWaitTime;
  }

  @Override
  public void start(final M key, final AsyncCallback<C> callback) {
    stop(); // before assigning a new value any existing must be stopped.
    pollingCallback = new PollingAsyncCallback<>(this, key, callback);

    pollingCallback.start();
  }

  public boolean isPolling() {
    return pollingCallback != null && !pollingCallback.isStopped();
  }

  @Override
  public final void stop() {
    if (pollingCallback != null) {
      pollingCallback.stop();
      pollingCallback = null;
    }
  }

  /**
   * Subclasses should implement this method with the actual call to the server.
   *
   * @param key key to identify recurring polls on the server
   * @param resultCallback callback that should be passed to the service. It should not be wrapped. Instead add functionality in the callback passed
   *          to the {@link #start(Object, AsyncCallback)} method
   */
  protected abstract void callService(M key, AsyncCallback<C> resultCallback);

  /**
   * Inner private class managing each polling call. A internal class is used that keeps it's own state because even while a new polling call is done
   * and the last one running is stopped a service call from the server can return after it is stopped. Therefore this class maintains the state if
   * the specific call was stopped and if so ignores it.
   */
  private static class PollingAsyncCallback<K, T> implements AsyncCallback<T> {

    private final PollingAgentImpl<K, T> agent;
    private final AsyncCallback<T> callback;
    private final Timer timer;
    private boolean stopped;

    public PollingAsyncCallback(final PollingAgentImpl<K, T> agent, final K key, final AsyncCallback<T> callback) {
      this.agent = agent;
      this.callback = callback;
      this.timer = new Timer() {
        @Override
        public void run() {
          agent.callService(key, PollingAsyncCallback.this);
        }
      };
    }

    public boolean isStopped() {
      return stopped;
    }

    public void start() {
      // By triggering the timer manually it will call the service immediately and process the result and start scheduling the next polls if needed.
      timer.run();
    }

    public void stop() {
      stopped = true;
      timer.cancel();
    }

    @Override
    public void onFailure(final Throwable caught) {
      if (stopped) {
        return;
      }

      callback.onFailure(caught);
      stopped = true;
    }

    @Override
    public void onSuccess(final T result) {
      if (stopped) {
        return;
      }

      if (result != null) {
        callback.onSuccess(result);
      }

      // Schedule next poll after processing result so there is no possibility of deadlock.
      Scheduler.get().scheduleDeferred(new ScheduledCommand() {
        @Override
        public void execute() {
          scheduleNextPoll();
        }
      });
    }

    private void scheduleNextPoll() {
      timer.schedule(agent.pollWaitTime);
    }
  }
}
