/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;

/**
 * A {@link DivTable} that contains a {@link CollapsiblePanel} with custom content on each
 * row.
 *
 * @param <L> Type of objects that rows will represent.
 */
public abstract class VariableCollapsibleDivTable<L, R extends VariableCollapsibleDivTableRow> extends InteractiveDivTable<L, R> {
  private WidgetFactory<L, ? extends Widget> content;
  private boolean interactiveRows;

  /**
   * Create a default {@link CollapsibleDivTable} with a row panel that will accept and
   * track collapsible rows. Rows will only collapse/expand when clicking the explicit
   * 'open/close' button on the collapsible row.
   */
  public VariableCollapsibleDivTable() {
    this(false);
  }

  /**
   * Create a default {@link CollapsibleDivTable} with a row panel that will accept and track
   * collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * or just the toggle butotn.
   */
  public VariableCollapsibleDivTable(final boolean interactiveRows) {
    this(interactiveRows, false);
  }

  /**
   * Create a default {@link CollapsibleDivTable} with a row panel that will accept and track
   * collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * or just the toggle button.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public VariableCollapsibleDivTable(final boolean interactiveRows, final boolean loadingByDefault) {
    this(interactiveRows, loadingByDefault, new TypedFlowPanel<R>());
  }

  /**
   * Create a {@link CollapsibleDivTable}. For example to not track
   * the collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   * @param panel Custom panel to put rows in.
   */
  public VariableCollapsibleDivTable(final boolean interactiveRows, final boolean loadingByDefault,
      final TypedFlowPanel<R> panel) {
    super(panel, loadingByDefault);

    this.interactiveRows = interactiveRows;

    setSelectionModel(new SingleSelectionModel<L>(keyProvider));
  }

  /**
   * @param content Content for each collapsible row to contain.
   */
  @UiChild(limit = 1)
  public void addContent(final WidgetFactory<L, ? extends Widget> content) {
    this.content = content;
  }

  /**
   * Create a row that's represented as a {@link CollapsiblePanel}.
   */
  @Override
  protected R createRow(final L object) {
    if (hasCollapsibleContent(object)) {
      final R row = createDivTableRow(object);

      final DivTableRow titleWidget = createTitleRow(object);

      createRow(titleWidget, object);
      titleWidget.asWidget().addStyleName(nl.overheid.aerius.wui.main.resources.R.css().flex());
      titleWidget.asWidget().addStyleName(nl.overheid.aerius.wui.main.resources.R.css().grow());
      titleWidget.asWidget().addStyleName(nl.overheid.aerius.wui.main.resources.R.css().alignCenter());

      row.addTitle(titleWidget.asWidget(), interactiveRows);

      final Widget contentWidget = content.createWidget(row.asWidget(), object);
      content.applyCellOptions(contentWidget, object);
      row.addContent(contentWidget);

      return row;
    } else {
      return super.createRow(object);
    }
  }

  @Override
  protected final R createDivTableRow(final L object) {
    return createDivTableRow(hasCollapsibleContent(object));
  }

  protected abstract DivTableRow createTitleRow(L object);

  protected abstract R createDivTableRow(boolean hasCollapsibleContent);

  protected abstract boolean hasCollapsibleContent(final L object);

  /**
   * Apply some style options to the row element.
   *
   * This implementation clears the existing style by default.
   *
   * @param row The row container
   * @param item The item to set the row for.
   */
  @Override
  public void applyRowOptions(final R row, final L item) {
    super.applyRowOptions(row, item);

    // Clear the style
    row.asWidget().setStyleName("");
  }

  public void setSelectionModel(final SingleSelectionModel<L> model) {
    super.setSelectionModel(model);
  }

  @Override
  public void setSelectionModel(final SelectionModel<L> model) {
    // No-op, we only support SingleSelectionModel
  }

  @Override
  public SingleSelectionModel<L> getSelectionModel() {
    return (SingleSelectionModel<L>) super.getSelectionModel();
  }
}
