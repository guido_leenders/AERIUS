/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.widget.CheckBoxSelectionItem.Messenger;

public class CheckBoxSelectionGroup<T> extends Composite implements LeafValueEditor<ArrayList<T>>, HasValue<ArrayList<T>> {
  interface CheckBoxSelectionGroupUiBinder extends UiBinder<Widget, CheckBoxSelectionGroup<?>> {}

  private static final CheckBoxSelectionGroupUiBinder UI_BINDER = GWT.create(CheckBoxSelectionGroupUiBinder.class);

  public interface CustomStyle extends CssResource {
    String selectionGroupTitle();
  }

  @UiField CustomStyle style;

  @UiField FlowPanel panel;

  private Label title;

  private final HashMap<T, CheckBoxSelectionItem<T>> groupOptions = new HashMap<T, CheckBoxSelectionItem<T>>();

  public CheckBoxSelectionGroup(final T[] options, final Messenger<T> messenger, final String debugID) {
    this(Arrays.asList(options), messenger, debugID);
  }

  public CheckBoxSelectionGroup(final List<T> options, final Messenger<T> messenger, final String debugID) {
    initWidget(UI_BINDER.createAndBindUi(this));

    for (final T option : options) {
      final CheckBoxSelectionItem<T> item = new CheckBoxSelectionItem<T>(option, messenger);
      item.setDebugID(debugID);
      groupOptions.put(option, item);
      panel.add(item);
    }
  }

  /**
   * Set the title text for this selection group (optional).
   *
   * @param titleText Text to set.
   */
  public void setTitleText(final String titleText) {
    if (title == null) {
      title = new Label();
    }

    title.setText(titleText);
    title.setStyleName(style.selectionGroupTitle());

    if (panel.getWidgetIndex(title) != 0) {
      panel.insert(title, 0);
    }
  }

  @Override
  public void setValue(final ArrayList<T> values) {
    // Reset all values first (meh).
    for (final CheckBoxSelectionItem<T> item : groupOptions.values()) {
      item.setValue(false);
    }

    for (final T value : values) {
      groupOptions.get(value).setValue(true);
    }
  }

  public void setEnabled(final T object, final boolean enable) {
    groupOptions.get(object).setEnabled(enable);
  }

  @Override
  public ArrayList<T> getValue() {
    final ArrayList<T> lst = new ArrayList<T>();
    for (final CheckBoxSelectionItem<T> item : groupOptions.values()) {
      if (item.getValue()) {
        lst.add(item.getObject());
      }
    }
    return lst;
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<ArrayList<T>> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @Override
  public void setValue(final ArrayList<T> value, final boolean fireEvents) {
    setValue(value);

    if (fireEvents) {
      ValueChangeEvent.fire(this, value);
    }
  }

  public void setCheckBoxStyle(final String checkBoxStyle) {
    // TODO; Make use of a WidgetFactory
    for (final CheckBoxSelectionItem<T> item : groupOptions.values()) {
      item.addStyleName(checkBoxStyle);
    }
  }

}
