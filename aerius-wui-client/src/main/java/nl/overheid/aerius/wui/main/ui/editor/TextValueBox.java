/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.text.ParseException;

import com.google.gwt.dom.client.Document;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.uibinder.client.UiConstructor;

import nl.overheid.aerius.wui.main.i18n.M;

/**
 * {@link ValidatedValueBox} for normal {@link String} values.
 * Easier to add validators to a Text box when using this (no need to have a separate editor).
 */
public class TextValueBox extends ValidatedValueBox<String> {

  private static final Renderer<String> RENDERER = new AbstractRenderer<String>() {
    @Override
    public String render(final String object) {
      return object == null ? "" : object;
    }
  };

  private static final Parser<String> PARSER = new Parser<String>() {
    @Override
    public String parse(final CharSequence object) throws ParseException {
      return object == null ? "" : object.toString();
    }
  };

  /**
   * UI constructor.
   * @param label The label that can be used in error messages.
   */
  @UiConstructor
  public TextValueBox(final String label) {
    super(Document.get().createTextInputElement(), RENDERER, PARSER, label, false);
  }

  @Override
  protected String noValidInput(final String value) {
    return M.messages().ivNoValidInput(getLabel(), value);
  }

}
