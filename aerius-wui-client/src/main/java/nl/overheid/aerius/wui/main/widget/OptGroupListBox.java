/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.OptGroupElement;
import com.google.gwt.dom.client.OptionElement;

/**
 * {@link OptGroupListBox} supports optgroups using addGroup() and addGroupedItem().
 */
public class OptGroupListBox extends CustomListBox {

  public static final String GROUP_SEPERATOR = ":";

  public static abstract class OptGroupData<T> {

    private final HashMap<String, ArrayList<T>> groups;

    @SuppressWarnings("serial")
    public OptGroupData() {
      groups = new LinkedHashMap<String, ArrayList<T>>() {
        @Override
        public ArrayList<T> get(final Object key) {
          if (!(key instanceof String)) {
            throw new IllegalArgumentException("Invalid key provided (wrong type).");
          }

          if (!containsKey(key)) {
            put((String) key, new ArrayList<T>());
          }

          return super.get(key);
        }
      };
    }

    public void setValue(final List<T> values) {
      for (final T value : values) {
        if (passedFilter(value)) {
          groups.get(asString(value).split(GROUP_SEPERATOR, 2)[0]).add(value);
        }
      }
    }

    /**
     * Name to display in the list.
     * @param name full name
     * @return filtered name
     */
    String getOptName(final T name) {
      final String[] split = asString(name).split(GROUP_SEPERATOR, 2);
      return split.length == 1 ? split[0] :  split[1];
    }

    /**
     * Returns the value that is the key of the data object.
     * @param value data object
     * @return key value as String
     */
    protected abstract String getKey(T value);

    /**
     * Returns the full string value of to display, this includes optgroup and subgroups in one string.
     * @param value data object
     * @return
     */
    protected abstract String asString(T value);


    protected boolean passedFilter(final T value) {
      return true;
    }
  }

  private OptGroupElement currentGroup;

  /**
   * The super method does not remove opt groups.
   *
   * We're doing a full sweep of all child elements here.
   *
   * Also, super.clear() crashes in IE8 when optgroups are present.
   */
  @Override
  public void clear() {
    while (getElement().getChildCount() > 0) {
      getElement().removeChild(getElement().getFirstChild());
    }
  }

  public <T> void addData(final OptGroupData<T> data) {
    for (final Map.Entry<String, ArrayList<T>> entry : data.groups.entrySet()) {
      if (entry.getValue().size() == 1) {
        final T v = entry.getValue().get(0);
        addItem(data.getOptName(v), data.getKey(v));
      } else {
        addGroup(entry.getKey());
        for (final T value : entry.getValue()) {
          addGroupedItem(data.getOptName(value), data.getKey(value));
        }
      }
    }
  }

  /**
   * Create a new opt group.
   *
   * @param name the name
   */
  public void addGroup(final String name) {
    final OptGroupElement elem = Document.get().createOptGroupElement();
    elem.setLabel(name);
    getElement().appendChild(elem);
    currentGroup = elem;
  }

  /**
   * Add an item to the last created group.
   *
   * If no group was created, returns and does nothing
   *
   * @param key item key
   * @param label item label
   */
  public void addGroupedItem(final String label, final String key) {
    if (currentGroup == null) {
      return;
    }

    final OptionElement option = Document.get().createOptionElement();
    setOptionText(option, label, null);
    option.setValue(key);

    // IE8 doesn't understand setText
    option.setInnerText(label);
    currentGroup.appendChild(option);
  }
}
