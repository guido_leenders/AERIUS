/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Touch;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseEvent;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchEvent;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.SimplePanel;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.util.TouchUtil;

/**
 * A widget that allows the user to select a value within a range of possible
 * values using a sliding bar that responds to mouse events.
 */
public class DoubleVerticalSliderBar extends Composite implements RequiresResize, HasValue<DoubleVerticalSliderBar.ValueRange>, HasValueChangeHandlers<DoubleVerticalSliderBar.ValueRange> {
  public interface DoubleVerticalSliderBarResources extends CssResource {
    String sliderBarVerticalKnob();

    String sliderBarVerticalShell();
  }

  public static class ValueRange {
    // The value with the highest index (confusingly, this will be the value of the bottom slider)
    private int upperValue;

    // The value with the lowest index (confusingly, this will be the value of the top slider)
    private int lowerValue;

    public ValueRange(final int lower, final int upper) {
      this.lowerValue = lower;
      this.upperValue = upper;
    }

    /**
     * @return Index at the bottom of the slider (highest value)
     */
    public int getUpperValue() {
      return upperValue;
    }

    /**
     * @return Index at the top of the slider (lowest value)
     */
    public int getLowerValue() {
      return lowerValue;
    }

    @Override
    public String toString() {
      return "DoubleValueRange [upperValue=" + upperValue + ", lowerValue=" + lowerValue + "]";
    }
  }

  private enum KnobResizeType {
    UPPER, LOWER, NONE
  }

  private KnobResizeType state = KnobResizeType.NONE;

  private final ValueRange value;

  /**
   * The knob that slides across the line.
   */
  private final SimplePanel upperKnob;

  /**
   * The knob that slides across the line.
   */
  private final SimplePanel lowerKnob;

  /**
   * The maximum slider value.
   */
  private int maxValue;

  /**
   * The minimum slider value.
   */
  private final int minValue;

  private class EventHandlers implements MouseDownHandler, MouseUpHandler, MouseMoveHandler, TouchStartHandler, TouchEndHandler, TouchMoveHandler {
    private final KnobResizeType knobType;

    public EventHandlers(final KnobResizeType knobType) {
      this.knobType = knobType;
    }

    @Override
    public void onMouseDown(final MouseDownEvent event) {
      haltEvent(event);
      setState(knobType);
    }

    @Override
    public void onTouchStart(final TouchStartEvent event) {
      haltEvent(event);
      setState(knobType);
    }

    @Override
    public void onMouseMove(final MouseMoveEvent event) {
      haltEvent(event);
      if (state != KnobResizeType.NONE) {
        slideKnob(event);
      }
    }

    @Override
    public void onTouchMove(final TouchMoveEvent event) {
      haltEvent(event);
      if (state != KnobResizeType.NONE) {
        slideKnob(event);
      }
    }

    @Override
    public void onMouseUp(final MouseUpEvent event) {
      haltEvent(event);
      if (state != KnobResizeType.NONE) {
        setState(KnobResizeType.NONE);
      }
    }

    @Override
    public void onTouchEnd(final TouchEndEvent event) {
      haltEvent(event);
      if (state != KnobResizeType.NONE) {
        setState(KnobResizeType.NONE);
      }
    }

    private void haltEvent(final DomEvent<?> event) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  /**
   * Create a slider bar.
   *
   * @param minValue the minimum value in the range
   * @param maxValue the maximum value in the range
   */
  public DoubleVerticalSliderBar(final int minValue, final int maxValue) {
    this.value = new ValueRange(minValue, maxValue);

    this.minValue = minValue;
    this.maxValue = maxValue;

    // Create the knobs
    upperKnob = new SimplePanel();
    lowerKnob = new SimplePanel();

    final EventHandlers upperKnobHandlers = new EventHandlers(KnobResizeType.UPPER);
    upperKnob.addDomHandler(upperKnobHandlers, MouseDownEvent.getType());
    upperKnob.addDomHandler(upperKnobHandlers, MouseUpEvent.getType());
    upperKnob.addDomHandler(upperKnobHandlers, MouseMoveEvent.getType());
    upperKnob.addDomHandler(upperKnobHandlers, TouchStartEvent.getType());
    upperKnob.addDomHandler(upperKnobHandlers, TouchEndEvent.getType());
    upperKnob.addDomHandler(upperKnobHandlers, TouchMoveEvent.getType());
    upperKnob.ensureDebugId(TestID.FILTER_UPPER_KNOB);

    final EventHandlers lowerKnobHandlers = new EventHandlers(KnobResizeType.LOWER);
    lowerKnob.addDomHandler(lowerKnobHandlers, MouseDownEvent.getType());
    lowerKnob.addDomHandler(lowerKnobHandlers, MouseUpEvent.getType());
    lowerKnob.addDomHandler(lowerKnobHandlers, MouseMoveEvent.getType());
    lowerKnob.addDomHandler(lowerKnobHandlers, TouchStartEvent.getType());
    lowerKnob.addDomHandler(lowerKnobHandlers, TouchEndEvent.getType());
    lowerKnob.addDomHandler(lowerKnobHandlers, TouchMoveEvent.getType());
    lowerKnob.ensureDebugId(TestID.FILTER_LOWER_KNOB);

    final FlowPanel panel = new FlowPanel();
    panel.add(upperKnob);
    panel.add(lowerKnob);

    initWidget(panel);
  }

  public void setResources(final DoubleVerticalSliderBarResources res) {
    getWidget().setStyleName(res.sliderBarVerticalShell(), true);
    upperKnob.setStyleName(res.sliderBarVerticalKnob());
    lowerKnob.setStyleName(res.sliderBarVerticalKnob());

    // Update heights.
    drawKnobs();
  }

  @Override
  public final HandlerRegistration addValueChangeHandler(final ValueChangeHandler<ValueRange> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  /**
   * Draw the knob where it is supposed to be relative to the line.
   */
  private void drawKnobs() {
    // Abort if not attached
    if (!isAttached()) {
      return;
    }
    switch (state) {
    case UPPER:
      drawUpperKnob();
      break;
    case LOWER:
      drawLowerKnob();
      break;
    case NONE:
    default:
      drawUpperKnob();
      drawLowerKnob();
      break;
    }
  }

  @Override
  protected void onLoad() {
    Scheduler.get().scheduleDeferred(new ScheduledCommand() {
      @Override
      public void execute() {
        drawKnobs();
      }
    });
  }

  private void drawUpperKnob() {
    final Element element = upperKnob.getElement();
    final double upperKnobTopOffset = upperKnobPercent() * getOffsetHeight() - element.getOffsetHeight() / 2.0;

    element.getStyle().setTop(upperKnobTopOffset, Unit.PX);
  }

  private void drawLowerKnob() {
    final Element element = lowerKnob.getElement();
    final double lowerKnobTopOffset = getLowerKnobPercent() * getOffsetHeight() - element.getOffsetHeight() / 2.0;

    element.getStyle().setTop(lowerKnobTopOffset, Unit.PX);
  }

  protected final double upperKnobPercent() {
    final double percent = ((double) value.getUpperValue() - minValue) / ((double) maxValue - (double) minValue);
    return Math.max(0.0, Math.min(1.0, percent));
  }

  protected final double getLowerKnobPercent() {
    final double percent = ((double) value.getLowerValue() - minValue) / ((double) maxValue - (double) minValue);
    return Math.max(0.0, Math.min(1.0, percent));
  }

  /**
   * Return the max value.
   * @return the max value
   */
  public final int getMaxValue() {
    return maxValue;
  }

  /**
   * Return the minimum value.
   * @return the minimum value
   */
  public final double getMinValue() {
    return minValue;
  }

  /**
   * Return the total range between the minimum and maximum values.
   * @return the total range
   */
  public final int getTotalRange() {
    return minValue > maxValue ? 0 : maxValue - minValue;
  }

  /**
   * Handle the resize event.
   */
  @Override
  public void onResize() {
    if (isAttached()) {
      drawKnobs();
    }
  }

  @Override
  public ValueRange getValue() {
    return value;
  }

  @Override
  public void setValue(final ValueRange value) {
    setValue(value, true);
  }

  /**
   * Set the current value and optionally fire the onValueChange event.
   * @param val the current value
   * @param fireEvent fire the onValue change event if true
   */
  @Override
  public final void setValue(final ValueRange val, final boolean fireEvent) {
    // Confine the value to the range
    value.upperValue = fixRange(val.getUpperValue());
    value.lowerValue = fixRange(val.getLowerValue());

    // Redraw the knob
    drawKnobs();

    if (fireEvent) {
      ValueChangeEvent.fire(this, value);
    }
  }

  public void reset() {
    setValue(new ValueRange(minValue, maxValue));
  }

  /**
   * Slide the knob to a new location.
   * @param event the mouse event
   * @param fireEvent whether or not to just slide the knob
   */
  private void slideKnob(final TouchEvent<?> event) {
    final Touch firstTouch = TouchUtil.getFirstTouch(event);

    if (firstTouch != null) {
      final int y = firstTouch.getRelativeY(getElement()) + upperKnob.getOffsetHeight() / 2;
      slideKnob(y);
    }
  }

  /**
   * Slide the knob to a new location.
   * @param event the mouse event
   * @param fireEvent whether or not to just slide the knob
   */
  private void slideKnob(final MouseEvent<?> event) {
    final int y = event.getRelativeY(getElement()) + upperKnob.getOffsetHeight() / 2;
    slideKnob(y);
  }

  /**
   * Slide the knob to a new location.
   * @param event the mouse event
   * @param fireEvent whether or not to just slide the knob
   */
  private void slideKnob(final int y) {
    if (y > 0) {
      final double percent = (double) y / getOffsetHeight();

      setCurrentlySlidingValue((int) (getTotalRange() * percent + minValue));
    }
  }

  private void setCurrentlySlidingValue(final int val) {
    boolean change = false;

    switch (state) {
    case UPPER:
      final int oldUpperVal = value.upperValue;
      value.upperValue = Math.max(value.lowerValue + 1, fixRange(val));
      change = oldUpperVal != value.upperValue;
      break;
    case LOWER:
      final int oldLowerVal = value.lowerValue;
      value.lowerValue = Math.min(value.upperValue - 1, fixRange(val));
      change = oldLowerVal != value.lowerValue;
      break;
    case NONE:
    default:
      return;
    }

    if (change) {
      // Redraw the knobs
      drawKnobs();
      ValueChangeEvent.fire(this, value);
    }
  }

  /**
   * Checks whether the slider was clicked or not.
   * @param event the mouse event
   * @return whether the slider was clicked
   */
  private void setState(final KnobResizeType type) {
    state = type;

    if (state == KnobResizeType.UPPER) {
      DOM.setCapture(upperKnob.getElement());
    } else if (state == KnobResizeType.LOWER) {
      DOM.setCapture(lowerKnob.getElement());
    } else {
      DOM.releaseCapture(lowerKnob.getElement());
      DOM.releaseCapture(upperKnob.getElement());
    }
  }

  private int fixRange(final int val) {
    return Math.max(minValue, Math.min(maxValue, val));
  }

  public SimplePanel getUpperKnob() {
    return upperKnob;
  }

  public SimplePanel getLowerKnob() {
    return lowerKnob;
  }

  public void setMaxHeight(final int maxValue) {
    this.maxValue = maxValue;
  }
}
