/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;

public class ButtonPlusMinus extends Composite implements HasValue<Boolean>, Focusable {
  private static final String CHECKED = "checked";

  interface ButtonPlusMinusUiBinder extends UiBinder<Widget, ButtonPlusMinus> {}

  private static final ButtonPlusMinusUiBinder UI_BINDER = GWT.create(ButtonPlusMinusUiBinder.class);

  private boolean checked;

  public ButtonPlusMinus() {
    initWidget(UI_BINDER.createAndBindUi(this));

    setTabIndex(0);

    addDomHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        toggle();
      }
    }, ClickEvent.getType());

    addDomHandler(new KeyPressHandler() {
      @Override
      public void onKeyPress(final KeyPressEvent event) {
        if (event.getCharCode() == KeyCodes.KEY_ENTER) {
          toggle();
        }
      }
    }, KeyPressEvent.getType());
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  /**
   * Returns true when the item is open/selected.
   */
  @Override
  public Boolean getValue() {
    return checked;
  }

  @Override
  public void setValue(final Boolean value) {
    setValue(value, false);
  }

  @Override
  public void setValue(final Boolean value, final boolean fireEvents) {
    this.checked = value;

    if (value) {
      getElement().setAttribute(CHECKED, "");
    } else {
      getElement().removeAttribute(CHECKED);
    }

    if (fireEvents) {
      ValueChangeEvent.fire(this, value);
    }
  }

  @Override
  public int getTabIndex() {
    return getElement().getTabIndex();
  }

  @Override
  public void setFocus(final boolean focused) {
    getElement().focus();
  }

  @Override
  public void setTabIndex(final int index) {
    getElement().setTabIndex(index);
  }

  @Override
  public void setAccessKey(final char key) {
    // IE only, don't care
  }

  public void toggle() {
    setValue(!getValue(), true);
  }
}
