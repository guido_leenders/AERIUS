/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class CheckBoxSelectionItem<T> extends Composite {
  interface CheckBoxSelectionItemUiBinder extends UiBinder<Widget, CheckBoxSelectionItem<?>> {}

  private static final CheckBoxSelectionItemUiBinder UI_BINDER = GWT.create(CheckBoxSelectionItemUiBinder.class);

  interface Style extends CssResource {
    String checkBoxLabelDisabled();
    String checkBoxLabelClickable();
  }

  public interface Messenger<T> {
    String getOptionText(T option);
  }

  private final T option;
  private String debugID = "";

  @UiField Style style;
  @UiField CheckBox checkBox;
  @UiField Label label;


  public CheckBoxSelectionItem(final T option, final Messenger<T> messenger) {
    initWidget(UI_BINDER.createAndBindUi(this));

    label.setText(messenger.getOptionText(option));

    this.option = option;

    label.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        if (checkBox.isEnabled()) {
          checkBox.setValue(!checkBox.getValue());
        }
      }
    });
  }

  public void setValue(final boolean b) {
    checkBox.setValue(b);
  }

  public boolean getValue() {
    return checkBox.getValue();
  }

  public T getObject() {
    return option;
  }

  public void setEnabled(final boolean enable) {
    checkBox.setEnabled(enable);
    label.setStyleName(style.checkBoxLabelDisabled(), !enable);
    label.setStyleName(style.checkBoxLabelClickable(), enable);
  }

  public String getDebugID() {
    return debugID;
  }

  public void setDebugID(final String debugID) {
    this.debugID = debugID;
    checkBox.ensureDebugId(getDebugID() + "_" + label.getText().replace(" ", "_"));
  }
}
