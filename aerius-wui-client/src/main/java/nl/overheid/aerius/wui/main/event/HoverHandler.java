/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Handler interface for {@link HoverEvent} events.
 *
 * @param <T> the type gaining/losing hover.
 */
public interface HoverHandler<T> extends EventHandler {

  /**
   * Called when {@link HoverEvent} is fired and the hover is gained.
   *
   * @param event the {@link HoverEvent} that was fired
   */
  void onHoverGained(HoverEvent<T> event);

  /**
   * Called when {@link HoverEvent} is fired and the hover is lost.
   *
   * @param event the {@link HoverEvent} that was fired
   */
  void onHoverLost(HoverEvent<T> event);

}