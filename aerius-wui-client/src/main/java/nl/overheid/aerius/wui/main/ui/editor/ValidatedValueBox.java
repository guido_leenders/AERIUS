/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import com.google.gwt.core.ext.typeinfo.ParseException;
import com.google.gwt.dom.client.Element;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.ValueBox;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.widget.validation.HasWidgetErrorStyle;

/**
 * {@link ValueBox} with built in support to add validation to the input.
 *
 * @param <T> Data type
 */
public abstract class ValidatedValueBox<T> extends ValueBox<T> implements HasWidgetErrorStyle {

  private String label;
  private ValidatedValueBoxEditor<T> editor;
  private Widget errorStyledWidget;
  private final boolean fireOnChange;

  /**
   * Constructor.
   *
   * @param element
   * @param renderer
   * @param parser
   * @param label User friendly name of this field
   */
  protected ValidatedValueBox(final Element element, final Renderer<T> renderer, final Parser<T> parser, final String label,
      final boolean fireOnChange) {
    super(element, renderer, parser);
    this.label = label;
    this.fireOnChange = fireOnChange;
  }

  @Override
  public ValidatedValueBoxEditor<T> asEditor() {
    if (editor == null) {
      editor = new ValidatedValueBoxEditor<T>(this, label, fireOnChange) {
        @Override
        protected String noValidInput(final String value) {
          return ValidatedValueBox.this.noValidInput(value);
        }

        @Override
        protected T getValueOnInvalidInput() {
          return ValidatedValueBox.this.getValueOnInvalidInput();
        }
        @Override
        protected Widget getErrorStyledWidget() {
          return errorStyledWidget == null ? super.getErrorStyledWidget() : errorStyledWidget;
        }
      };
    }
    return editor;
  }

  /**
   * Add validator.
   *
   * @param validator validator
   */
  public void addValidator(final Validator<T> validator) {
    asEditor().addValidator(validator);
  }

  /**
   * Remove validator.
   *
   * @param validator validator
   */
  public void removeValidator(final Validator<T> validator) {
    asEditor().removeValidator(validator);
  }

  public void setLabel(final String label) {
    this.label = label;
  }

  @Override
  public Widget getWidgetErrorStyle() {
    return errorStyledWidget;
  }

  @Override
  public void setWidgetErrorStyle(final Widget w) {
    this.errorStyledWidget = w;
  }

  /**
   * Localized message to display when the input is not valid and the {@link #getValueOrThrow()}
   * throws an error.
   *
   * @param value Current input value
   * @return Localized message to display
   */
  protected abstract String noValidInput(final String value);

  /**
   * Value to return when a {@link ParseException} occurred with the input. Override
   * this method if null is not a legal value for the editor.
   *
   * @return return a value that will not given an error on the editor.
   */
  protected T getValueOnInvalidInput() {
    return null;
  }

  protected String getLabel() {
    return label;
  }
}
