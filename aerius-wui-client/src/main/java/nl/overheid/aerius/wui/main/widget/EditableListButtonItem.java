/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor.Ignore;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Text Widget with edit and delete button that become visible on mouse over.
 */
public class EditableListButtonItem extends FocusPanel implements HasText {

  private static EditableListButtonItemUiBinder uiBinder = GWT.create(EditableListButtonItemUiBinder.class);

  @UiField @Ignore Label label;
  @UiField Button edit;
  @UiField Button delete;

  private ClickHandler deleteHandler;
  private ClickHandler editHandler;

  interface EditableListButtonItemUiBinder extends UiBinder<Widget, EditableListButtonItem> {
  }

  private final class EventHandlers implements MouseOutHandler, MouseOverHandler {
    @Override
    public void onMouseOver(final MouseOverEvent event) {
      final EditableListButtonItem source = (EditableListButtonItem) event.getSource();

      source.showButtons(true);
    }

    @Override
    public void onMouseOut(final MouseOutEvent event) {
      final EditableListButtonItem source = (EditableListButtonItem) event.getSource();

      source.showButtons(false);
    }
  }

  private final EventHandlers handlers = new EventHandlers();

  public EditableListButtonItem() {
    setWidget(uiBinder.createAndBindUi(this));

    addMouseOverHandler(handlers);
    addMouseOutHandler(handlers);
  }

  public void addDeleteHandler(final ClickHandler deleteHandler) {
    this.deleteHandler = deleteHandler;
  }

  public void addEditHandler(final ClickHandler editHandler) {
    this.editHandler = editHandler;
  }

  public void ensureDebugIdDeleteButton(final String id) {
    delete.ensureDebugId(id);
  }

  public void ensureDebugIdEditButton(final String id) {
    edit.ensureDebugId(id);
  }

  @Override
  public String getText() {
    return label.getText();
  }

  @Override
  public void setText(final String text) {
    label.setText(text);
  }

  protected void showButtons(final boolean visible) {
    delete.setVisible(visible);
    edit.setVisible(visible);
  }

  @UiHandler("delete")
  void onDeleteClick(final ClickEvent e) {
    showButtons(false);
    if (deleteHandler != null) {
      deleteHandler.onClick(e);
    }
    e.stopPropagation();
  }

  @UiHandler("edit")
  void onEditClick(final ClickEvent e) {
    showButtons(false);
    if (editHandler != null) {
      editHandler.onClick(e);
    }
    e.stopPropagation();
  }
}
