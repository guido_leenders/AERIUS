/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.service.ContextServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;

/**
 * Provides security related utility methods..
 * This class should be initialized with {@link #init(ContextServiceAsync, String)};
 */
public final class SecurityUtil {
  private static ContextServiceAsync service;
  private static String logoutURL;

  private SecurityUtil() {
  }

  /**
   * Initializes the logout parameters.
   * @param service service to close the session on the server
   * @param logoutURL optional logout url to external authentication server, if null not used
   */
  public static void init(final ContextServiceAsync service, final String logoutURL) {
    SecurityUtil.service = service;
    SecurityUtil.logoutURL = logoutURL;
  }

  /**
   * Tests if the given exception is thrown because of an authentication
   * timeout.
   *
   * @param t
   *          The throwable to test.
   * @return True if the throwable is an instance of {@link StatusCodeException}
   *         and the received status is
   *         {@link SharedConstants.AUTHENTICATION_EXPIRED}, false otherwise.
   */
  public static boolean isAuthTimeoutException(final Throwable t) {
    return t != null && t instanceof StatusCodeException
        && ((StatusCodeException) t).getStatusCode() == SharedConstants.AUTHENTICATION_EXPIRED;
  }

  /**
   * logs the user out the page if the give throwable or its cause represents an
   * authentication timeout.
   * @param t The throwable to test.
   * @param logoutURL
   */
  public static void reloadOnAuthenticationExpired(final Throwable t) {
    if (t != null && (isAuthTimeoutException(t) || isAuthTimeoutException(t.getCause()))) {
      logout();
    }
  }

  /**
   * Performs a logout on the server and additional calls a logout url to an external authentication service.
   */
  public static void logout() {
    assert service != null : "The #init method was not called!";
    service.closeSession(null, new AsyncCallback<Void>() {
      @Override
      public void onSuccess(final Void result) {
        reload();
      }

      @Override
      public void onFailure(final Throwable caught) {
        reload();
      }

      private void reload() {
        if (logoutURL != null) {
          try {
            new RequestBuilder(RequestBuilder.GET, logoutURL).sendRequest(null, new RequestCallback() {

              @Override
              public void onResponseReceived(final Request request, final Response response) {
                // ignore errors at this point.
              }

              @Override
              public void onError(final Request request, final Throwable exception) {
                // ignore errors at this point.
              }
            });
          } catch (final RequestException e) {
            // ignore errors at this point.
          }
        }
        // Ask the user to logout to avoid trigger a loop that keeps reloading the page in case something goes wrong.
        if (Window.confirm(M.messages().applicationReloadPageOnLogout())) {
          Window.Location.reload();
        }
      }
    });
  }
}
