/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.validation;

/**
 * Range validator for {@link Integer} objects.
 */
public class IntegerRangeValidator implements Validator<Integer> {

  private final Integer min;
  private final Integer max;
  private final String message;

  /**
   * Constructor.
   *
   * @param min Minimum range value
   * @param max Maximum range value
   */
  public IntegerRangeValidator(final Integer min, final Integer max) {
    this(min, max, null);
  }

  /**
   * Constructor for static messages. Use this constructor in case the message is static and doesn't contain a reference to the validated input.
   *
   * @param min Minimum range value
   * @param max Maximum range value
   * @param message static message to show the user
   */
  public IntegerRangeValidator(final Integer min, final Integer max, final String message) {
    this.min = min;
    this.max = max;
    this.message = message;
  }

  @Override
  public String validate(final Integer value) {
    return value == null || (value >= min && value <= max) ? null : getMessage(value);
  }

  /**
   * Returns the localized error message for this object when outside range.
   *
   * @param value current value
   * @return localized message
   */
  protected String getMessage(final Integer value) {
    return message;
  }
}
