/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.Notification;
import nl.overheid.aerius.wui.main.event.NotificationEvent;

/**
 * Sugar for broadcasting Notification objects.
 */
public final class NotificationUtil {
  private NotificationUtil() {
  }

  public static void broadcastMessage(final EventBus eventBus, final String txt) {
    broadcastMessage(eventBus, txt, (String) null);
  }

  /**
   * Broadcasts a message. Text. Not an error.
   * 
   * @param eventBus the bus to broadcast over
   * @param txt the message text to broadcast
   * @param url additional url
   */
  public static void broadcastMessage(final EventBus eventBus, final String txt, final String url) {
    broadcast(eventBus, new Notification(txt, url));
  }

  /**
   * Broadcasts an error. Text. Not a message.
   * 
   * @param eventBus the bus to broadcast over
   * @param txt the error text to broadcast
   */
  public static void broadcastError(final EventBus eventBus, final String txt) {
    broadcast(eventBus, new Notification(txt, true));
  }

  /**
   * Broadcasts a {@link Notification}.
   * 
   * @param eventBus the bus to broadcast over
   * @param notification the object to broadcast
   */
  public static void broadcast(final EventBus eventBus, final Notification notification) {
    broadcast(eventBus, new NotificationEvent(notification));
  }

  private static void broadcast(final EventBus eventBus, final NotificationEvent notification) {
    eventBus.fireEvent(notification);
  }
}
