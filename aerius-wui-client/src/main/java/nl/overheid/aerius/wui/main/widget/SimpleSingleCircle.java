/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Widget showing a Circle with additional border circles, with a number, and a label.
 */
public class SimpleSingleCircle extends Composite {

  interface SimpleSingleCircleUiBinder extends UiBinder<Widget, SimpleSingleCircle> {
  }

  interface CustomStyle extends CssResource {
    String additionalCircleStyle();
  }

  private static final SimpleSingleCircleUiBinder UI_BINDER = GWT.create(SimpleSingleCircleUiBinder.class);

  @UiField CustomStyle style;
  @UiField DivElement circleBlock;
  @UiField DivElement circle;
  @UiField DivElement circleValueLabel;
  @UiField DivElement label;
  private final ArrayList<DivElement> additionalCircles;

  public SimpleSingleCircle(final String circleColor, final int circleHeightInPx, final double circleValue, final String labelVal,
      final String[] additionalCircleColors, final Double[] additionalCircleValues) {
    if (additionalCircleColors != null && additionalCircleValues != null && additionalCircleColors.length != additionalCircleValues.length) {
      throw new IllegalArgumentException("Amount of additional circle colors and values should match: " + additionalCircleColors.length
          + " - " + additionalCircleValues.length);
    }
    additionalCircles = new ArrayList<>();

    initWidget(UI_BINDER.createAndBindUi(this));
    if (additionalCircleValues == null || additionalCircleColors == null) {
      return;
    }
    double maxValue = circleValue;
    for (final double val : additionalCircleValues) {
      if (val > maxValue) {
        maxValue = val;
      }
    }
    circleBlock.getStyle().setHeight(circleHeightInPx, Unit.PX);
    circleBlock.getStyle().setWidth(circleHeightInPx, Unit.PX);

    final Style circleStyle = circle.getStyle();
    setSizeOfCircle(circleStyle, circleHeightInPx, maxValue, circleValue);
    circleStyle.setBackgroundColor(ColorUtil.webColor(circleColor));
    circleStyle.setBorderColor(ColorUtil.webColor(circleColor));
    circleStyle.setBorderColor(ColorUtil.webColor(circleColor));

    circleValueLabel.setInnerText(FormatUtil.formatDeposition(circleValue, 0));
    setSizeOfCircle(circleValueLabel.getStyle(), circleHeightInPx, maxValue, circleValue);
    for (int i = 0; i < additionalCircleValues.length; ++i) {
      final double additionalCircleValue = additionalCircleValues[i];
      final String additionalCircleColor = additionalCircleColors[i];

      final DivElement additionalCircle = Document.get().createDivElement();
      additionalCircle.setClassName(style.additionalCircleStyle());
      final Style additionalCircleStyle = additionalCircle.getStyle();
      setSizeOfCircle(additionalCircleStyle, circleHeightInPx, maxValue, additionalCircleValue);
      additionalCircleStyle.setBorderColor(ColorUtil.webColor(additionalCircleColor));
      additionalCircles.add(additionalCircle);
    }

    for (final DivElement additionalCircle : additionalCircles) {
      circleBlock.appendChild(additionalCircle);
    }
    label.setInnerText(labelVal);
  }

  private void setSizeOfCircle(final Style circleStyle, final int circleHeightInPx, final double maxValue, final double value) {
    final double circleSizePercentage = value / maxValue;
    circleStyle.setHeight(circleSizePercentage * circleHeightInPx, Unit.PX);
    circleStyle.setLineHeight(circleSizePercentage * circleHeightInPx, Unit.PX);
    circleStyle.setWidth(circleSizePercentage * circleHeightInPx, Unit.PX);
    circleStyle.setMarginLeft(((1.0 - circleSizePercentage) / 2) * circleHeightInPx, Unit.PX);
    circleStyle.setMarginTop(((1.0 - circleSizePercentage) / 2) * circleHeightInPx, Unit.PX);
  }

  public void addCircleStyle(final String style) {
    circle.addClassName(style);
  }

  public void addCircleLabelStyle(final String style) {
    label.addClassName(style);
  }

  public void addCircleValueLabelStyle(final String style) {
    circleValueLabel.addClassName(style);
  }
}
