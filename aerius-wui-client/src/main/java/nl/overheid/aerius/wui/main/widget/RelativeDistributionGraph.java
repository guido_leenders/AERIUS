/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;

/**
 * Draws a graph with any amount of parts and their relative distribution based on 'some' weight value.
 *
 * @param <T> Type of object this bar represents
 */
public class RelativeDistributionGraph<T> extends Composite implements HasSelectionHandlers<T> {
  /**
   * The minimum percentage which will still display a value. If a bar in the graph is below this percentage, it will display as if
   * it were this value.
   */
  private static final double MIN_PERCENTAGE = 15;

  private static RelativeDistributionGraphUiBinder uiBinder = GWT.create(RelativeDistributionGraphUiBinder.class);

  interface RelativeDistributionGraphUiBinder extends UiBinder<Widget, RelativeDistributionGraph<?>> {}

  interface CustomStyle extends CssResource {
    String bar();

    String enabledBar();

    String column();

    String focus();
  }

  @UiField FlowPanel columnPanel;

  @UiField CustomStyle style;

  private Widget selectedColumn;

  private double totalValue;

  private boolean enabled = true;

  private String baseID;

  /**
   * Default constructor.
   */
  @Inject
  public RelativeDistributionGraph() {
    initWidget(uiBinder.createAndBindUi(this));
  }

  public Label addColumn(final double value, final T object, final String title, final boolean valueAsLabel, final boolean usePercentage) {
    final double percentage = value / totalValue * SharedConstants.PERCENTAGE_TO_FRACTION;

    final Label column = new Label();
    column.addStyleName(style.bar());
    column.addStyleName(R.css().number());
    column.addStyleName(R.css().flex());
    column.addStyleName(R.css().grow());
    if (enabled) {
      column.addStyleName(style.enabledBar());
    }

    if (valueAsLabel) {
      column.setText(usePercentage ? M.messages().unitPercentage(FormatUtil.toWhole(percentage)) : FormatUtil.toWhole(value));
    }
    column.setTitle(title);
    column.getElement().getStyle().setCursor(enabled ? Cursor.POINTER : Cursor.AUTO);
    column.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        if (enabled) {
          setSelectedColumn(column);
          SelectionEvent.fire(RelativeDistributionGraph.this, object);
        }
      }
    });

    final SimplePanel container = new SimplePanel();
    container.getElement().getStyle().setProperty("flexBasis", Math.max(MIN_PERCENTAGE, percentage), Unit.PCT);
    container.addStyleName(style.column());
    container.addStyleName(R.css().flex());
    container.addStyleName(R.css().grow());

    if (baseID != null) {
      UIObject.ensureDebugId(container.getElement(), baseID, object == null ? "" : object.toString());
    }

    container.setWidget(column);
    columnPanel.add(container);

    if (selectedColumn == null) {
      setSelectedColumn(column);
    }

    return column;
  }

  /**
   * @param value the value of the bar column.
   * @param object the object of the bar column.
   * @param title Title for the column
   * @param valueAsLabel if true the value will be shown as label.
   * @return The column its Label widget for optional styling or caption
   */
  public Label addColumn(final double value, final T object, final String title, final boolean valueAsLabel) {
    return addColumn(value, object, title, valueAsLabel, true);
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    this.baseID = baseID;
  }

  @Override
  public HandlerRegistration addSelectionHandler(final SelectionHandler<T> handler) {
    return addHandler(handler, SelectionEvent.getType());
  }

  /**
   * @param totalValue the total value to set, 100 for percentages.
   */
  public void setTotalValue(final double totalValue) {
    this.totalValue = totalValue;
  }

  public double getTotalValue() {
    return totalValue;
  }

  public Widget getSelectedColumn() {
    return selectedColumn;
  }

  /**
   * @param column the widget to select.
   */
  private void setSelectedColumn(final Widget column) {
    if (column == selectedColumn) {
      return;
    }

    if (selectedColumn != null) {
      selectedColumn.removeStyleName(style.focus());
    }

    if (column != null) {
      column.addStyleName(style.focus());
    }

    // Remove focus from the previously selected column
    selectedColumn = column;
  }

  public void setEnabled(final boolean enabled) {
    this.enabled = enabled;
  }

  public void clear() {
    columnPanel.clear();
    setSelectedColumn(null);
  }
}
