/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.widget.AnimatedLabel;

/**
 * Application Switch menu widget.
 *
 * @param <P> Application base place
 * @param <T> Application switch type
 */
public class MenuItemListWidget<P extends Place, T> extends Composite implements IsMenuItemWidget {

  interface ListMenuItemWidgetUiBinder extends UiBinder<Widget, MenuItemListWidget<?, ?>> {}

  private static final ListMenuItemWidgetUiBinder UI_BINDER = GWT.create(ListMenuItemWidgetUiBinder.class);

  private enum Direction {
    LEFT, RIGHT
  }

  @UiField InlineHTML buttonLeft;
  @UiField InlineHTML buttonRight;

  @UiField AnimatedLabel selectedItemLabel;

  private final int size;
  private int selectedItem;

  private final PlaceController placeController;
  private final MenuItemList<P, T> item;
  private Direction nextDirection;

  public MenuItemListWidget(final PlaceController placeController, final MenuItemList<P, T> item, final String panelStyle) {
    this.placeController = placeController;
    this.item = item;

    size = item.getItems().size();

    initWidget(UI_BINDER.createAndBindUi(this));

    addStyleName(panelStyle);
  }

  @Override
  public MenuItemList<P, T> getItem() {
    return item;
  }

  @Override
  public void update(final Place place) {
    setSelected(item.getValueFromPlace(place), Direction.RIGHT);
  }

  @UiHandler("buttonLeft")
  void clickLeft(final ClickEvent e) {
    gotoSelected(selectedItem - 1, Direction.LEFT);
  }

  @UiHandler({ "buttonRight", "selectedItemLabel" })
  void clickRight(final ClickEvent e) {
    gotoSelected(selectedItem + 1, Direction.RIGHT);
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent event) {
    final T selection = item.getValueFromPlace(event.getValue());

    setSelected(selection, nextDirection);
  }

  private void gotoSelected(final int newSelection, final Direction direction) {
    this.selectedItem = newSelection;
    this.nextDirection = direction;

    // Get the new selection object using positiveMod size on the new selection index
    goTo(item.getItems().get(positiveMod(selectedItem, size)));
  }

  private void setSelected(final T selection, final Direction dir) {
    selectedItemLabel.setText(item.getName(selection), dir == Direction.RIGHT);
  }

  private void goTo(final T selection) {
    placeController.goTo(item.getPlaceFromSelection(placeController.getWhere(), selection));
  }

  /**
   * Java uses the if-you-ask-me-incorrect-and-dumb-version of %, which yields a negative output for negative input.
   *
   * Ie. -3 % 2 yields -1, while it should yield 1. This is a correct implementation.
   *
   * @param a Input
   * @param b Mod
   * @return Positive output
   */
  private int positiveMod(final int a, final int b) {
    final int mod = a % b;
    return mod < 0 ? mod + b : mod;
  }
}
