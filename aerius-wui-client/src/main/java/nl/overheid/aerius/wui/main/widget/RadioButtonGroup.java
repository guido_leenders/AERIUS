/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

public class RadioButtonGroup<T> extends Composite implements HasValueChangeHandlers<T>, LeafValueEditor<T> {
  public interface RadioButtonContentResource<T> {
    String getRadioButtonText(T value);
  }

  interface RadioButtonGroupUiBinder extends UiBinder<Widget, RadioButtonGroup<?>> {}

  private static final RadioButtonGroupUiBinder UI_BINDER = GWT.create(RadioButtonGroupUiBinder.class);

  private final RadioButtonContentResource<T> resource;

  @UiField FlowPanel panel;

  private final String name;

  private String baseID;

  private final HashMap<T, RadioButton> buttonMap = new HashMap<>();

  /**
   * Initialise the group with the given resources.
   * @param resource Resource to use for naming.
   */
  public RadioButtonGroup(final RadioButtonContentResource<T> resource) {
    this.resource = resource;

    // Doesn't really matter so long as it's the same for this group.
    this.name = String.valueOf("rb"  + Math.random());

    initWidget(UI_BINDER.createAndBindUi(this));
  }

  /**
   * Clear the group.
   */
  public void clear() {
    panel.clear();
    buttonMap.clear();
  }

  @Override
  protected void onEnsureDebugId(final String baseID) {
    super.onEnsureDebugId(baseID);

    for (final Entry<T, RadioButton> entry : buttonMap.entrySet()) {
      UIObject.ensureDebugId(entry.getValue().getElement(), baseID, entry.getKey().toString());
    }
    this.baseID = baseID;
  }

  /**
   * Add a radio button to this group.
   *
   * @param object Object the radio button represents.
   */
  public void addButton(final T object) {
    final RadioButton button = new RadioButton(name, resource.getRadioButtonText(object));

    if (baseID != null) {
      UIObject.ensureDebugId(button.getElement(), baseID, object.toString());
    }

    button.addStyleName(R.css().radioGroupButton());
    button.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
      @Override
      public void onValueChange(final ValueChangeEvent<Boolean> event) {
        ValueChangeEvent.fire(RadioButtonGroup.this, object);
      }
    });

    buttonMap.put(object, button);

    panel.add(button);
  }

  /**
   * Add a list of radio buttons to this group.
   * @param objects Objects the buttons should represent
   */
  public void addButtons(final T[] objects) {
    for (final T obj : objects) {
      addButton(obj);
    }
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  public void setEnabled(final T value, final boolean enable) {
    final RadioButton radioButton = buttonMap.get(value);
    if (radioButton != null) {
      radioButton.setEnabled(enable);
    }
  }

  public void setVisible(final T value, final boolean visible) {
    final RadioButton radioButton = buttonMap.get(value);
    if (radioButton != null) {
      radioButton.getElement().getStyle().setProperty("display", visible ? "flex" : "none"); // Display.FLEX doesn't exist - so using this workaround
    }
  }

  @Override
  public void setValue(final T value) {
    setValue(value, false);
  }

  @Override
  public T getValue() {
    for (final Entry<T, RadioButton> entry : buttonMap.entrySet()) {
      if (entry.getValue().getValue()) {
        return entry.getKey();
      }
    }

    return null;
  }

  public void setValue(final T value, final boolean fireEvents) {
    resetAll();
    final RadioButton radioButton = buttonMap.get(value);
    if (radioButton != null) {
      radioButton.setValue(Boolean.TRUE, fireEvents);
    }
  }

  /**
   * When programmatically set a value make sure all other checkboxes are unset.
   */
  private void resetAll() {
    for (final Entry<T, RadioButton> entry : buttonMap.entrySet()) {
      entry.getValue().setValue(Boolean.FALSE);
    }
  }
}
