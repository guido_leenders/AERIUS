/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.HashMap;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.view.client.ProvidesKey;

public final class DivTableKeyProviders {
  private static final HashMap<Class<?>, ProvidesKey<?>> PROVIDERS = new HashMap<>();

  private DivTableKeyProviders() {}

  @SuppressWarnings("unchecked")
  public static <T> void autoConfigure(final Class<T> clazz, final DivTable<T, ?> table) {
    if (PROVIDERS.containsKey(clazz)) {
      table.setKeyProvider((ProvidesKey<T>) PROVIDERS.get(clazz));
    } else {
      // Print warning?
      GWT.log("Warning: Using unsafe key provider for type: " + clazz.getName() + " in table " + table.getClass().getName() + " while this may not have been intended.");
    }
  }

  public static <T> void register(final Class<T> clazz, final ProvidesKey<T> keyProvider) {
    PROVIDERS.put(clazz, keyProvider);
  }
}
