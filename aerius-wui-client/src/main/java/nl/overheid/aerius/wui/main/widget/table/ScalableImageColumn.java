/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public abstract class ScalableImageColumn<L> extends WidgetFactory<L, Image> {

  private final int width;
  private Integer height;

  public ScalableImageColumn(final int width) {
    this.width = width;
  }

  public ScalableImageColumn(final int width, final int height) {
    this(width);
    this.height = height;
  }

  @Override
  public Image createWidget(final L object) {
    final DataResource value = getValue(object);

    final Image image = value == null ? new Image() : new Image(value.getSafeUri());
    if (value != null) {
      image.getElement().setAttribute("aerius-image", value.getName());
    }
    image.getElement().getStyle().setWidth(width, Unit.PX);
    if (height != null) {
      image.getElement().getStyle().setHeight(height, Unit.PX);
    }

    return image;
  }

  @Override
  public Widget wrapWidget(final Widget widget) {
    return new SimplePanel(widget.asWidget());
  }

  public abstract DataResource getValue(L object);
}
