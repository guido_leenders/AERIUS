/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

/**
 * A {@link DivTable} that contains {@link CollapsibleDivTableRow} with custom content on each
 * row.
 *
 * @param <T> Type of objects that rows will represent.
 */
public class SimpleCollapsibleDivTable<T> extends VariableCollapsibleDivTable<T, SimpleCollapsibleDivTableRow> {

  /**
   * Create a default {@link SimpleCollapsibleDivTable} with a row panel that will accept and
   * track collapsible rows. Rows will only collapse/expand when clicking the explicit
   * 'open/close' button on the collapsible row.
   */
  public SimpleCollapsibleDivTable() {
    this(false);
  }

  /**
   * Create a default {@link SimpleCollapsibleDivTable} with a row panel that will accept and track
   * collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * or just the toggle butotn.
   */
  public SimpleCollapsibleDivTable(final boolean interactiveRows) {
    this(interactiveRows, false);
  }

  /**
   * Create a default {@link SimpleCollapsibleDivTable} with a row panel that will accept and track
   * collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * or just the toggle button.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   */
  public SimpleCollapsibleDivTable(final boolean interactiveRows, final boolean loadingByDefault) {
    this(interactiveRows, loadingByDefault, new TypedFlowPanel<SimpleCollapsibleDivTableRow>());
  }

  /**
   * Create a {@link SimpleCollapsibleDivTable}. For example to not track
   * the collapsible rows.
   *
   * @param interactiveRows True to only collapse/expand rows when clicking the toggle button.
   * @param loadingByDefault Whether this widget shows 'loading' content if empty.
   * @param panel Custom panel to put rows in.
   */
  public SimpleCollapsibleDivTable(final boolean interactiveRows, final boolean loadingByDefault,
      final TypedFlowPanel<SimpleCollapsibleDivTableRow> panel) {
    super(interactiveRows, loadingByDefault, panel);
  }

  @Override
  protected boolean hasCollapsibleContent(final T object) {
    return true;
  }

  @Override
  protected SimpleCollapsibleDivTableRow createDivTableRow(final boolean hasCollapsibleContent) {
    return new SimpleCollapsibleDivTableRow();
  }

  @Override
  protected DivTableRow createTitleRow(final T object) {
    return new SimpleDivTableRow();
  }
}
