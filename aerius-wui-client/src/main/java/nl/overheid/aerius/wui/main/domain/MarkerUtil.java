/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.domain;

import com.google.gwt.resources.client.ImageResource;

import nl.overheid.aerius.shared.domain.info.DepositionMarker.DepositionMarkerType;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Util class for Marker related helper methods.
 */
public final class MarkerUtil {

  private MarkerUtil() {
  }

  /**
   * Returns the {@link ImageResource} for the {@link markerType}. If no image
   * available IllegalArgumentException will be thrown.
   *
   * @param markerType The marker type to get the image for.
   * @return ImageResource The corresponding image resource.
   */
  public static ImageResource getMarkerImageResource(final DepositionMarkerType markerType) {
    final ImageResource marker;

    switch (markerType) {
    case HIGHEST_CALCULATED_DEPOSITION:
      marker = R.images().highestCalculatedDepositionMarker();
      break;
    case HIGHEST_TOTAL_DEPOSITION:
      marker = R.images().highestTotalDepositionMarker();
      break;
    case HIGHEST_CALCULATED_AND_TOTAL:
      marker = R.images().highestCalculatedAndTotalMarker();
      break;
    default:
      throw new IllegalArgumentException("[getMarkerImageResource] This marker has no image: " + markerType);
    }

    return marker;
  }

  /**
   * Returns the {@link ImageResource} for the {@link markerType}. If no image
   * available IllegalArgumentException will be thrown.
   *
   * @param markerType The marker type to get the image for.
   * @return ImageResource The corresponding image resource.
   */
  public static ImageResource getMarkerImageResourceSmall(final DepositionMarkerType markerType) {
    ImageResource marker = null;

    switch (markerType) {
    case HIGHEST_CALCULATED_DEPOSITION:
      marker = R.images().highestCalculatedDepositionMarkerSmall();
      break;
    case HIGHEST_TOTAL_DEPOSITION:
      marker = R.images().highestTotalDepositionMarkerSmall();
      break;
    default:
      throw new IllegalArgumentException("[getMarkerImageResourceSmall] This marker has no small image: " + markerType);
    }

    return marker;
  }
}
