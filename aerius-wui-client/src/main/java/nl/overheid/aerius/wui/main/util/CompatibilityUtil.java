/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util;

import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.main.i18n.M;

public final class CompatibilityUtil {
  private static final String FIREFOX = "Firefox";
  private static final int INCOMPATIBLE_FIREFOX_MAJOR_BEFORE = 38;

  public static void doVersionCompatibilityChecks(final EventBus eventBus) {
    final String userAgent = Window.Navigator.getUserAgent();

    if (userAgent.contains(FIREFOX)) {
      final String version = userAgent.substring(userAgent.indexOf(FIREFOX) + FIREFOX.length() + 1);
      if (Double.parseDouble(version) < INCOMPATIBLE_FIREFOX_MAJOR_BEFORE) {
        NotificationUtil.broadcastMessage(eventBus, M.messages().compatibilityOldFirefox());
      }
    }
  }
}
