/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.Iterator;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class SimpleVariableCollapsibleDivTableRow extends Composite implements VariableCollapsibleDivTableRow {
  private final InteractiveDivTableRow row;

  public SimpleVariableCollapsibleDivTableRow(final boolean hasCollapsibleContent) {
    row = hasCollapsibleContent ? new SimpleCollapsibleDivTableRow() : new SimpleInteractiveDivTableRow();

    initWidget(row.asWidget());
  }

  @Override
  public void addContent(final Widget content) {
    if (row instanceof SimpleCollapsibleDivTableRow) {
      ((SimpleCollapsibleDivTableRow) row).addContent(content);
    }
  }

  @Override
  public void addTitle(final Widget title, final boolean isInteractive) {
    if (row instanceof SimpleCollapsibleDivTableRow) {
      ((SimpleCollapsibleDivTableRow) row).addTitle(title, isInteractive);
    }
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
    return row.addValueChangeHandler(handler);
  }

  @Override
  public Boolean getValue() {
    return row.getValue();
  }

  @Override
  public void setValue(final Boolean value) {
    row.setValue(value);
  }

  @Override
  public void setValue(final Boolean value, final boolean fireEvents) {
    row.setValue(value, fireEvents);
  }

  @Override
  public void add(final Widget w) {
    row.add(w);
  }

  @Override
  public void clear() {
    row.clear();
  }

  @Override
  public Iterator<Widget> iterator() {
    return row.iterator();
  }

  @Override
  public boolean remove(final Widget w) {
    return row.remove(w);
  }
}
