/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.ToolBox;
import nl.overheid.aerius.wui.main.widget.ToolBox.ToolBoxHandler;
import nl.overheid.aerius.wui.main.widget.ToolBox.ToolBoxMessages;

/**
 * Modifiable DivTable, includes a toolbox that allows for modifications to the table
 * given a {@link ListManipulationHandler}.
 *
 * @param <T> the object type contained in the cell table
 */
public abstract class EditableDivTable<T> extends FlowPanel implements ToolBoxHandler<T>, IsInteractiveDataTable<T> {
  /**
   * Empty because it may be expanded later to explicitly localize things other than the toolbox.
   */
  public interface EditableDivTableMessages extends ToolBoxMessages {}

  private final class EventHandlers implements SelectionChangeEvent.Handler, ValueChangeHandler<Integer> {
    @Override
    public void onSelectionChange(final SelectionChangeEvent event) {
      updateToolboxEnabled();
    }

    @Override
    public void onValueChange(final ValueChangeEvent<Integer> event) {
      updateToolboxEnabled();
    }
  }

  private final EventHandlers handlers = new EventHandlers();

  private final InteractiveDivTable<T, ?> divTable;

  private final ToolBox<T> toolbox;
  private boolean disabled;

  /**
   * Create a new DivTable with the given manipulation handler.
   *
   * @param divTable the list of objects to display
   */
  public EditableDivTable(final InteractiveDivTable<T, ?> divTable, final HelpPopupController helpPopupController, final EditableDivTableMessages messages) {
    this.divTable = divTable;
    this.toolbox = new ToolBox<T>(this, helpPopupController, messages);

    // Define style
    toolbox.ensureDebugId(TestID.TOOLBOX_EDITABLE_DIVTABLE_PREFIX + '-' + TestID.TOOLBOX);

    // Add handlers
    divTable.setSingleSelectionModel();
    divTable.getSelectionModel().addSelectionChangeHandler(handlers);
    divTable.addValueChangeHandler(handlers);
    divTable.addStyleName(R.css().overflow());

    addStyleName(R.css().flex());
    addStyleName(R.css().grow());
    addStyleName(R.css().columnsClean());

    // Add the content
    add(toolbox);
    add(divTable.getParent());
  }

  public void setEventBus(final EventBus eventBus) {
    toolbox.setEventBus(eventBus);
  }

  /**
   * Automatically sets this table's controls to whatever is appropriate.
   */
  public void updateTableEnabled() {
    setToolboxEnabled(itemSelected());
    toolbox.setAdditionEnabled(true);
  }

  /**
   * Enables or disables the buttons for adding new entries to the table.
   *
   * @param enable true to enable, false to disable
   */
  public void setAddingEnabled(final boolean enable) {
    toolbox.setAdditionEnabled(enable);
  }

  /**
   * Enables or disables the entirety of this table's controls.
   *
   * @param enable true to enable, false to disable
   */
  public void setTableEnabled(final boolean enable) {
    disabled = !enable;
    toolbox.setAdditionEnabled(enable);
  }

  private void updateToolboxEnabled() {
    setToolboxEnabled(itemSelected());
  }

  private void setToolboxEnabled(final boolean enable) {
    toolbox.setButtonsEnabled(enable && !disabled);
  }

  private boolean itemSelected() {
    return getSelectedObject() != null;
  }

  @Override
  public InteractiveDivTable<T, ?> asDataTable() {
    return divTable;
  }

  @Override
  public T getSelectedObject() {
    return ((SingleSelectionModel<T>) divTable.getSelectionModel()).getSelectedObject();
  }

  @Override
  public abstract void handleCustomButton();

  @Override
  public abstract Class<T> getObjectClass();

  @Override
  protected void onEnsureDebugId(final String baseID) {
    divTable.ensureDebugId(baseID);

    /**
     * Unfortunately, we must emulate debug ID appending behaviour as setting the debug ID on a UIObject will
     * not propagate to a Widget's onEnsureDebugId
     */
    toolbox.ensureDebugId(baseID + "-" + TestID.TOOLBOX);
    divTable.ensureDebugId(baseID + "-" + TestID.TABLE);
  }

  public void setMaxHeight(final int height) {
    divTable.setPixelSize(divTable.getOffsetWidth(), Math.max(41, height - toolbox.getOffsetHeight()));
  }
}
