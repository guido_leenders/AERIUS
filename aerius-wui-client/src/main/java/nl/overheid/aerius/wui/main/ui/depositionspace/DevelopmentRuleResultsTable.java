/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.depositionspace;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.wui.main.domain.DevelopmentSpaceUtil;
import nl.overheid.aerius.wui.main.event.DevelopmentRulesEvent;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.table.ImageColumn;
import nl.overheid.aerius.wui.main.widget.table.IntegerColumn;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class DevelopmentRuleResultsTable extends Composite implements IsDataTable<DevelopmentRuleResult> {
  interface DevelopmentRuleResultsTableUiBinder extends UiBinder<Widget, DevelopmentRuleResultsTable> {}

  private static final DevelopmentRuleResultsTableUiBinder UI_BINDER = GWT.create(DevelopmentRuleResultsTableUiBinder.class);

  @UiField(provided = true) SimpleInteractiveClickDivTable<DevelopmentRuleResult> divTable;

  @UiField(provided = true) ImageColumn<DevelopmentRuleResult> iconColumn;
  @UiField(provided = true) TextColumn<DevelopmentRuleResult> labelColumn;
  @UiField(provided = true) IntegerColumn<DevelopmentRuleResult> valueColumn;

  /**
   * Grid for deposition space rules result.
   * @param eventBus @EventBus.
   */
  public DevelopmentRuleResultsTable(final EventBus eventBus) {
    iconColumn = new ImageColumn<DevelopmentRuleResult>() {
      @Override
      public ImageResource getValue(final DevelopmentRuleResult object) {
        return DevelopmentSpaceUtil.getDevelopmentRuleIcon(object.getRule(), object.isEmpty());
      }
    };
    labelColumn = new TextColumn<DevelopmentRuleResult>() {
      @Override
      public String getValue(final DevelopmentRuleResult object) {
        return M.messages().calculatorDevelopmentRuleType(object.getRule());
      }
    };
    valueColumn = new IntegerColumn<DevelopmentRuleResult>() {
      @Override
      public Integer getValue(final DevelopmentRuleResult object) {
        return object.size();
      }

      @Override
      public void applyCellOptions(final Widget cell, final DevelopmentRuleResult object) {
        super.applyCellOptions(cell, object);
        if (!object.isEmpty() && object.getRule() != DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK) {
          cell.setStyleName(R.css().negativeValue(), true);
        }
      }
    };

    divTable = new SimpleInteractiveClickDivTable<DevelopmentRuleResult>() {
      @Override
      protected boolean isItemSelectable(final DevelopmentRuleResult item) {
        return !item.isEmpty();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    divTable.setSelectionModel(new SingleSelectionModel<DevelopmentRuleResult>());
    divTable.addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final DevelopmentRuleResult selectedObject = ((SingleSelectionModel<DevelopmentRuleResult>) divTable.getSelectionModel()).getSelectedObject();
        eventBus.fireEvent(new DevelopmentRulesEvent(selectedObject != null, selectedObject));
      }
    });
  }

  @Override
  public SimpleInteractiveClickDivTable<DevelopmentRuleResult> asDataTable() {
    return divTable;
  }

}
