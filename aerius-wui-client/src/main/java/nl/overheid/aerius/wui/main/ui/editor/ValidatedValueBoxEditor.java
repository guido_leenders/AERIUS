/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.text.ParseException;

import com.google.gwt.editor.client.ValueAwareEditor;
import com.google.gwt.editor.ui.client.adapters.ValueBoxEditor;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.ui.validation.Validator;
import nl.overheid.aerius.wui.main.ui.validation.ValidatorCollection;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

/**
 * A {@link ValueBoxEditor} with validation support built in. It takes a {@link ValueBoxBase}
 * as input making it work with any class extending {@link ValueBoxBase}. The validation
 * is triggered when the {@link #onValueChange(ValueChangeEvent)} is fired. This
 * means the widget itself probably has lost focus.
 *
 * @param <T> data type this validated editor works on
 */
public abstract class ValidatedValueBoxEditor<T> extends ValueBoxEditor<T> implements ValueChangeHandler<T>, ValueAwareEditor<T> {

  private final ValidatorCollection<T> validators = new ValidatorCollection<T>();
  //Store valueBox in this class because the one passed to the super call is not accessible.
  private final ValueBoxBase<T> valueBox;
  private final String label;
  private String lastError;

  public ValidatedValueBoxEditor(final ValueBoxBase<T> valueBox, final String label) {
    this(valueBox, label, false);
  }

  /**
   * @param valueBox widget to be validated
   * @param label text to display as name to refer the to in error messages.
   */
  public ValidatedValueBoxEditor(final ValueBoxBase<T> valueBox, final String label, final boolean fireOnValueChange) {
    super(valueBox);
    this.valueBox = valueBox;
    this.label = label;
    if (fireOnValueChange) {
      valueBox.addValueChangeHandler(this);
    }
  }

  /**
   * Add validator.
   *
   * @param validator validator to remove
   */
  public void addValidator(final Validator<T> validator) {
    validators.addValidator(validator);
  }

  /**
   * Remove validator.
   *
   * @param validator validator to remove
   */
  public void removeValidator(final Validator<T> validator) {
    validators.removeValidator(validator);
  }

  @Override
  public void onValueChange(final ValueChangeEvent<T> event) {
    getValueAndValidate(false, true);
  }

  protected String getLabel() {
    return label;
  }

  @SuppressWarnings("unchecked")
  @Override
  public T getValue() {
    final T value = valueBox.getValue();
    //Check if content isn't equals to placeholder and if so return empty string.
    //This check is only relevant for IE because IE doesn't support placeholder
    //therefore the placeholder text is set in the actual input.
    return value instanceof String
        && valueBox.getElement().getAttribute("placeholder").equals(value) ? (T) "" : value;
  }

  @Override
  public void flush() {
    getValueAndValidate(true, false);
  }

  @Override
  public void onPropertyChange(final String... paths) {
    // no-op
  }

  /**
   * Triggers validation and returns if last error was not empty.
   * @return last error was not empty
   */
  public boolean validate() {
    getValueAndValidate(false, true);
    return lastError != null;
  }

  /**
   * Localized message to display when the input is not valid and the {@link #getValueOrThrow()}
   * throws an error.
   *
   * @param value Current input value
   * @return Localized message to display
   */
  protected abstract String noValidInput(final String value);

  /**
   * Value to return when a {@link ParseException} occurred with the input. Override
   * this method if null is not a legal value for the editor.
   *
   * @return return a value that will not given an error on the editor.
   */
  protected T getValueOnInvalidInput() {
    return null;
  }

  /**
   * Returns the last found error by the validators.
   *
   * @return error message or null if no errors
   */
  protected String getLastError() {
    return lastError;
  }

  /**
   * Returns the widget to set the error styling on.
   * @return widget to style error on
   */
  protected Widget getErrorStyledWidget() {
    return valueBox;
  }

  /**
   * Shows the error to the user.
   */
  protected void showError(final String errorMessage) {
    ErrorPopupController.addError(valueBox, getErrorStyledWidget(), errorMessage);
  }

  /**
   * Validates the value with the added validators and based on the parameters
   * acts differently. If record is true it will record the error with the delegate.
   * This should only be done when the method is used in a getValue() situation.
   * If show is true it also shows the error information to the user. This should
   * only be done when acting on user interaction, i.e. when the user has changed
   * the value.
   *
   * @param record if true records the error with the delegate
   * @param show if true shows error message popup
   * @return the value
   */
  private T getValueAndValidate(final boolean record, final boolean show) {
    T value = null;
    lastError = null;
    try {
      value = valueBox.getValueOrThrow();
      lastError = validators.validate(value);
    } catch (final ParseException e) {
      lastError = noValidInput(valueBox.getText());
      value = getValueOnInvalidInput();
    }
    if (record && lastError != null) {
      getDelegate().recordError(lastError, valueBox.getText(), getErrorStyledWidget());
    }
    if (show) {
      showError(lastError);
    }
    return value;
  }
}
