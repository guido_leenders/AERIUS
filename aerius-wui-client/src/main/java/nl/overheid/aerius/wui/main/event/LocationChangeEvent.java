/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import nl.overheid.aerius.geo.shared.BBox;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;

/**
 * Event which is fired when the location of the map should be changed.
 */
public class LocationChangeEvent extends SimpleGenericEvent<BBox> {

  private WKTGeometry geometry;

  private boolean soft;

  /**
  * Creates a {@link LocationChangeEvent}.
  */
  public LocationChangeEvent() {
    super(null);
  }

  /**
   * Creates a {@link LocationChangeEvent}.
   *
   * @param bbox The bounding box to 'move'/'zoom' to.
   */
  public LocationChangeEvent(final BBox bbox) {
    super(bbox);
  }

  /**
   * Creates a {@link LocationChangeEvent} including highlight geometry.
   *
   * @param bbox The bounding box to 'move'/'zoom' to.
   * @param geometry The geometry that is zoomed to, for highlighting purposes.
   */
  public LocationChangeEvent(final BBox bbox, final WKTGeometry geometry) {
    super(bbox);
    this.geometry = geometry;
  }

  /**
   * Creates a {@link LocationChangeEvent} including highlight geometry.
   *
   * @param bbox The bounding box to 'move'/'zoom' to.
   * @param soft Whether or not to zoom if the bbox is already in the extent
   */
  public LocationChangeEvent(final BBox bbox, final boolean soft) {
    super(bbox);
    this.setSoft(soft);
  }

  public LocationChangeEvent(final Point point, final int width) {
    this(point, width, false);
  }

  public LocationChangeEvent(final Point point, final int width, final boolean soft) {
    this(new BBox(point.getX(), point.getY(), width), soft);
  }

  /**
   * Get the optional geometry belonging to the boundingbox. For highlighting.
   * @return the geometry, or null if not specified
   */
  public WKTGeometry getGeometry() {
    return geometry;
  }

  /**
   * Set the optional geometry belonging to the boundingbox. For highlighting.
   * @param geometry the geometry to set
   */
  public void setGeometry(final WKTGeometry geometry) {
    this.geometry = geometry;
  }

  public boolean isSoft() {
    return soft;
  }

  public void setSoft(final boolean soft) {
    this.soft = soft;
  }
}
