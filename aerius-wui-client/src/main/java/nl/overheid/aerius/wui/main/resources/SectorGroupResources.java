/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Image resources for sectors.
 */
public interface SectorGroupResources extends ClientBundle {

  @Source("images/bucket/or-1-hoofdwegennet.png")
  ImageResource iconDepositionSpaceSectorPrioritaryRoadFreeway();

  @Source("images/bucket/or-2-hoofdwegennet.png")
  ImageResource iconDepositionSpaceSectorOtherRoadFreeway();

  @Source("images/bucket/or-3-hoofdwegennet.png")
  ImageResource iconDepositionSpaceSectorOverflowingRoadFreeway();

  @Source("images/bucket/or-1-industrie.png")
  ImageResource iconDepositionSpaceSectorPrioritaryIndustry();

  @Source("images/bucket/or-2-industrie.png")
  ImageResource iconDepositionSpaceSectorOtherIndustry();

  @Source("images/bucket/or-3-industrie.png")
  ImageResource iconDepositionSpaceSectorOverflowingIndustry();

  @Source("images/bucket/or-1-landbouw.png")
  ImageResource iconDepositionSpaceSectorPrioritaryAgriculture();

  @Source("images/bucket/or-2-landbouw.png")
  ImageResource iconDepositionSpaceSectorOtherAgriculture();

  @Source("images/bucket/or-3-landbouw.png")
  ImageResource iconDepositionSpaceSectorOverflowingAgriculture();

  @Source("images/bucket/or-1-overig.png")
  ImageResource iconDepositionSpaceSectorPrioritaryOther();

  @Source("images/bucket/or-2-overig.png")
  ImageResource iconDepositionSpaceSectorOtherOther();

  @Source("images/bucket/or-3-overig.png")
  ImageResource iconDepositionSpaceSectorOverflowingOther();

  @Source("images/bucket/or-1-scheepvaart.png")
  ImageResource iconDepositionSpaceSectorPrioritaryShipping();

  @Source("images/bucket/or-2-scheepvaart.png")
  ImageResource iconDepositionSpaceSectorOtherShipping();

  @Source("images/bucket/or-3-scheepvaart.png")
  ImageResource iconDepositionSpaceSectorOverflowingShipping();

  @Source("images/bucket/or-1-verkeer-vervoer.png")
  ImageResource iconDepositionSpaceSectorPrioritaryTrafficAndTransport();

  @Source("images/bucket/or-2-verkeer-vervoer.png")
  ImageResource iconDepositionSpaceSectorOtherTrafficAndTransport();

  @Source("images/bucket/or-3-verkeer-vervoer.png")
  ImageResource iconDepositionSpaceSectorOverflowingTrafficAndTransport();

  @Source("images/bucket/or-leg-hoofdwegennet.png")
  ImageResource iconDepositionSpaceSectorRoadFreeway();

  @Source("images/bucket/or-leg-industrie.png")
  ImageResource iconDepositionSpaceSectorIndustry();

  @Source("images/bucket/or-leg-landbouw.png")
  ImageResource iconDepositionSpaceSectorAgriculture();

  @Source("images/bucket/or-leg-overig.png")
  ImageResource iconDepositionSpaceSectorOther();

  @Source("images/bucket/or-leg-scheepvaart.png")
  ImageResource iconDepositionSpaceSectorShipping();

  @Source("images/bucket/or-leg-verkeer-vervoer.png")
  ImageResource iconDepositionSpaceSectorTrafficAndTransport();
}
