/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.DataGrid;

/**
 * Global resource class, access resources via R.css(). or R.images().
 */
public final class R {

  /**
   * Aerius CssResources.
   */
  public interface AeriusCssResource extends CssResource {
    String defaultPanel();

    String legendGraphicWidth();

    String legendGraphicHeight();

    String colorBorder();

    String link();

    String colorPopupTitleBackground();

    String colorSourceBackgroundDefault();

    String colorTextAlternative();

    String colorHelpBorder();

    String markerSite();

    String radioButton();

    String colorPopupEmptyBackground();

    String colorHelpContent();

    String colorNotificationItemErrorBackground();

    String colorNotificationItemUnreadBackground();

    String colorNotificationItemReadBackground();

    String colorErrorContent();

    String colorErrorBorder();

    String number();

    String unicode();

    String rowHeight();

    String errorPopupPanel();

    String errorInput();

    String subActivityTitle();

    String habitatKDWLegend();

    String overflow();

    String flexGroup();

    String flex();

    String wrap();

    String alignCenter();

    String baseline();

    String boxDelimiter();

    String justifyCenter();

    String spaceBetween();

    String grow();

    String autoSize();

    String shrink();

    String noShrink();

    String rows();

    String columns();

    String columnsClean();

    String resize();

    String flexEnd();

    String noBoxShadow();

    String textAlignRight();

    String breakWord();

    /**
     * Style for multiple buttons on a row. Handles hover, active and disabled
     * cases for the aligned buttons.
     *
     * @return
     */
    String buttonGroup();

    /**
     * Extra property to revert z-index to initial, as we don't want all button groups to float above the glass effect.
     * @return
     */
    String buttonGroupDefaultZIndex();

    String buttonCopy();

    String buttonDelete();

    String buttonDeleteSmall();

    String buttonDeleteXSmall();

    String buttonEditXSmall();

    String buttonEditSmall();

    String buttonEdit();

    String buttonViewXSmall();

    String buttonView();

    String buttonDate();

    String buttonLayers();

    String buttonProfile();

    String buttonProfileActive();

    String buttonLogout();

    String buttonRefresh();

    String buttonExport();

    String buttonNotificationsBlock();

    String buttonDownload();

    String buttonDownloadMaps();

    String buttonUpload();

    String listButton();

    String listButtonDisabled();

    String listButtonPopup();

    String listButtonItem();

    String substanceSelectionButtonLabel();

    String emissionValueTypeSelectionButtonLabel();

    String layerListButtonItemSelection();

    String primaryButton();

    String flexButton();

    String collapsiblePanel();

    String collapsiblePanelHeader();

    String collapsiblePanelHeaderLabel();

    String collapsibleContent();

    String collapsibleContentAnimated();

    String inputCollapsibleContent();

    String columnWidth();

    String fontColorAlternative();

    /**
     * Adds a padding below the element. Use this on all elements that need horizontal space between the elements conform the ui design.
     */
    String calculatorRow();

    String layoutRow();

    String calculatorLabel();

    String radioGroupButton();

    String toolButton();

    String toolPoint();

    String toolLine();

    String toolPolygon();

    String toolEdit();

    String inputWithUnit();

    String inputWithUnitName();

    String inputWithCalculator();

    String inputWithCalculatorButton();

    String sourcesListEmissionColumn();

    String sourceRow();

    String sourceDetailCol1();

    String sourceDetailCol1Var();

    String sourceDetailCol2();

    String sourceDetailCol2Var();

    String sourceDetailCol3Var();
    
    String sourceDetailCol4Var();

    String sourceMarker();

    String sourceMarkerComparable();

    int sourceMarkerWidth();

    String sourceMarkerArrow();

    String resultTypeListBox();

    String inputLikeSelect();

    String depositionMarkerItems();

    String depositionMarkerArrow();

    int sourceMarkerArrowWidth();

    String categorizedEmissionRow();

    String animalEmissionCategory();

    String shipRouteListItem();

    String categorizedEmissionAmount();

    String mobileEmissionRow();

    String animalReductiveEmissionDescription();

    String inputPlaceHolder();

    String calculatorFarmEmissionTableEmission();

    String calculatorFarmEmissionTable();

    String calculatorFarmEmissionSubstance();

    String searchPanel();

    String notificationPanel();

    String calculatorSelectLocation();

    String infoPopupPanel();

    String infoPanelItem();

    String layerWidgetDragPositioner();

    String buttonYearSelection();

    String buttonSubstanceSelection();

    String buttonOptionsPanel();

    String buttonOptionsPanelActive();

    String buttonLayerPanel();

    String buttonLayerPanelActive();

    String buttonMetaPanel();

    String buttonMetaPanelActive();

    String buttonLegendPanel();

    String buttonLegendPanelActive();

    String buttonInfoPanel();

    String buttonInfoPanelActive();

    String zoomColumn();

    String sliderKnob();

    String sliderKnobLabelDisabled();

    int zIndexInteractionLayer();

    int zIndexMarkersLayer();

    int zIndexNavigationWidget();

    int zIndexSonarVectorLayer();

    int zIndexSonarMarkerLayer();

    int zIndexGeneric();

    String layerLegendDialogContent();

    int arrowHeight();

    int arrowWidth();

    String sectorIconImage();

    String textOverflowEllipsis();

    String sourceMarkerContainer();

    String depositionMarkerContainer();

    String infoMarkerContainer();

    String shipRouteMarker();

    int shippingRouteMarkerSize();

    String colorCalculationPoint();

    String verticalText();

    String graphLabel();

    String confirmCancelDialog();

    String confirmCancelDialogButtons();

    String confirmCancelDialogLoadingImage();

    String closeButton();

    String notificationListPanel();

    String helpPopupPanel();

    String helpPopupPanelContent();

    String toggleButton();

    String toggleLabelButton();

    String mobileEmissionListBox();

    String tabPanel();

    // Tab Panel style for gwt TabPanel class.
    String tabPanel2();

    String tabPanelItem();

    String tabPanelItemFocus();

    String underlineSolid();

    String underlineSolidDark();

    String underlineDotted();

    String underlineDottedDark();

    String underlineDashed();

    String underlineDashedDark();

    String textLegend();

    String imagesLegendLabel();

    String colorRangesColorCircle();

    String colorRangesColorCircleSmall();

    String colorRangesColorHexagon();

    String colorRangesColorHexagonSmall();

    String comparisonListEmission();

    String comparisonListNumberSources();

    String comparisonListNegative();

    String negativeValue();

    String attachedPopupPanel();

    String whileDragging();

    String emissionSourceViewContainer();

    String emissionSourceViewTitle();

    String emissionSourceViewCol1();

    String emissionSourceViewCol2();

    String emissionSourceViewRow();

    String buttonAttachedPopupClose();

    String cursorPointer();

    String cursorDefault();

    String registerEditableIndicator();

    String buttonSwitch();

    String mobileEmissionListBoxContainer();

    String layerPanelList();

    String layerPanelListBox();

    String layerNotVisible();

    String registerPermitRule();

    String innerSonar();

    String outerSonar();

    String animationSonar();

    String sonar();

    String substanceToggleButton();

    String depositionMarker();

    String emptyPanelWithText();

    String tabPanelItemLocked();

    String mainMenuListSwitch();

    String mainMenuListPopup();

    String collapsibleDivTableRow();

    String markerSiteText();

    String markerIcon();

    String divTable();

    String divTableHeader();

    String divTableDataPanel();

    String divTableRowPanel();

    String divTableRow();

    String divTableFooter();

    String sectorDepositionGraphWidgetInnerTable();

    String adminTable();

    String sectorNonCollapsibleRow();

    String waitingPanel();

    String calculatorThemeSwitch();

    String colorDevelopmentSpace();

    String colorDisabled();

    String suggestionWarning();

    String suggestionWarningBox();

    String farmContextColumnValue();

    String rootLayoutPanel();

    String registerReviewHeaderIcon();

    String registerReviewHeaderIconAvailable();

    String registerReviewHeaderIconRequested();

    String registerReviewHeaderIconRemaining();

    String registerReviewHeaderIconReserved();

    String registerReviewHeaderIconAssigned();

    String buttonMarked();

    String buttonUnmarked();

    String buttonDeleteLeftAligned();

    String mapSearchPanel();

    String noContentStyle();

    String colorPicker();

    String aeriusCalculatorTitle();

    String aeriusScenarioTitle();

    String dynamicRows();
  }

  public interface AeriusResource extends ClientBundle, ImageResources, CellTable.Resources {
    @Source("strict.css")
    AeriusCssResource css();

    // Not used in code, but to separate notstrict css from css and inject
    // the notstrict css.
    @NotStrict
    @Source("notstrict.css")
    CssResource notstrictcss();

    @Override
    @Source("celltable.css")
    CellTable.Style cellTableStyle();
  }

  /**
   * Import with prefix to avoid conflicts with other CellTable styles.
   */
  @ImportedWithPrefix("ae-ctitem")
  interface CellTableItemStyle extends CellTable.Style {
  }

  public interface ItemCellTableResources extends CellTable.Resources {
    @Override
    @Source("itemcelltable.css")
    CellTableItemStyle cellTableStyle();
  }

  public interface DataGridResources extends DataGrid.Resources {
    @Override
    @Source("datagrid.css")
    DataGrid.Style dataGridStyle();
  }

  private static final AeriusResource RESOURCES = GWT.create(AeriusResource.class);
  private static final CellTable.Resources ITEM_RESOURCES = GWT.create(ItemCellTableResources.class);
  private static final DataGrid.Resources DATAGRID_RESOURCES = GWT.create(DataGridResources.class);

  // Don't instantiate directly, use the static fields.
  private R() {
  }

  /**
   * Ensures css is injected. Should be called as soon as possible on startup.
   */
  public static void init() {
    RESOURCES.notstrictcss().ensureInjected();
    RESOURCES.css().ensureInjected();
  }

  /**
   * Access to css resources.
   */
  public static AeriusCssResource css() {
    return RESOURCES.css();
  }

  /**
   * Access to image resources.
   */
  public static ImageResources images() {
    return RESOURCES;
  }

  /**
   * Access to the CellTable#Resource resources.
   * @return
   */
  public static CellTable.Resources cellTableResources() {
    return RESOURCES;
  }

  /**
   * Access to the CellTable#Resource resources.
   * @return
   */
  public static CellTable.Resources itemCellTableResources() {
    return ITEM_RESOURCES;
  }

  /**
   * Access to the DataGrid#Resource resources.
   * @return
   */
  public static DataGrid.Resources dataGridResources() {
    return DATAGRID_RESOURCES;
  }
}
