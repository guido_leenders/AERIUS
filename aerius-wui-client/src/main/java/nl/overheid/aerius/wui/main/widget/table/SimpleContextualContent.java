/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.Arrow;
import nl.overheid.aerius.wui.main.widget.Arrow.Orientation;

/**
 * TODO Figure out a way to unwrap the wrapper.
 */
public abstract class SimpleContextualContent<T> extends Composite implements ContextualContent<T> {
  private static final int ARROW_MARGIN_TOP = 10;
  private static final String BG_COLOR = "e9edf0";

  private static final SimpleContextualContentUiBinder UI_BINDER = GWT.create(SimpleContextualContentUiBinder.class);

  interface SimpleContextualContentUiBinder extends UiBinder<Widget, SimpleContextualContent<?>> {}

  @UiField Label title;
  @UiField SimplePanel content;

  public SimpleContextualContent() {
    initWidget(UI_BINDER.createAndBindUi(this));

    final Arrow arr = new Arrow(Orientation.LEFT, 1);
    arr.setHeight(R.css().arrowHeight());
    arr.setWidth(R.css().arrowWidth());
    arr.setColor(ColorUtil.webColor(BG_COLOR), R.css().colorBorder());
    arr.getElement().getStyle().setTop(ARROW_MARGIN_TOP, Unit.PX);

    // This hides the arrow element inside the container, not adopting it as a widget. GWT will never know.
    content.getElement().insertFirst(arr.getElement());
  }

  @Override
  public void setArrowTop(final int top) {
    //    arr.getElement().getStyle().setMarginTop(top - arr.getOffsetHeight() / 2, Unit.PX);
  }

  public void setTitleText(final String text) {
    title.setVisible(true);
    title.setText(text);
  }

  protected void initContent(final Widget wrapper) {
    content.setWidget(wrapper);
  }
}
