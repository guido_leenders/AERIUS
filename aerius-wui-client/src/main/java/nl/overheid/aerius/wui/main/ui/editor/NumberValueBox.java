/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.editor;

import java.util.Arrays;
import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.text.shared.Parser;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.Event;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;

/**
 * Generic ValueBox for numbers.
 */
abstract class NumberValueBox<T extends Number> extends ValidatedValueBox<T> {

  private static final List<Integer> VALID_KEY_CODES = Arrays.asList(KeyCodes.KEY_ALT, KeyCodes.KEY_BACKSPACE, KeyCodes.KEY_CTRL,
      KeyCodes.KEY_DELETE, KeyCodes.KEY_DOWN, KeyCodes.KEY_END, KeyCodes.KEY_ENTER, KeyCodes.KEY_ESCAPE, KeyCodes.KEY_HOME, KeyCodes.KEY_LEFT,
      KeyCodes.KEY_PAGEDOWN, KeyCodes.KEY_PAGEUP, KeyCodes.KEY_RIGHT, KeyCodes.KEY_SHIFT, KeyCodes.KEY_TAB, KeyCodes.KEY_UP);

  private boolean fraction;
  private boolean positive;
  public NumberValueBox(final Element element, final Renderer<T> renderer, final Parser<T> parser, final String label) {
    super(element, renderer, parser, label, true);
    element.addClassName(R.css().number());
    sinkEvents(Event.ONKEYPRESS);
  }

  @Override
  public void onBrowserEvent(final Event event) {
    boolean revertKeyPress = false;

    if (event.getTypeInt() == Event.ONKEYPRESS) {
      final String value = getText();
      String errorMessage = null;

      if (event.getCharCode() != 0) {
        switch (event.getCharCode()) {
        // The numbers 0-9.
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
          break;
          /* The minus sign, only allowed if it's at the start of the box and it doesn't already
           *  contain one. */
        case '-':
          if (positive || getCursorPos() != 0 || getSelectionLength() == 0 && value.contains("-")
          || getSelectionLength() > 0 && value.replace(getSelectedText(), "").contains("-")) {
            revertKeyPress = true;
            errorMessage = M.messages().ivNotAPositiveNumber(getLabel());
          }
          break;
          // In case , and . allow them.
        case '.':
        case ',':
          if (!fraction || value.contains(".") || value.contains(",")) {
            revertKeyPress = true;
          }
          break;
        default:
          revertKeyPress = !VALID_KEY_CODES.contains(event.getKeyCode());
          break;
        }
      }
      if ('-' != event.getCharCode() && revertKeyPress) {
        errorMessage = noValidInput(String.valueOf((char) event.getCharCode()));
      }

      asEditor().showError(errorMessage);
    }
    if (revertKeyPress) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      super.onBrowserEvent(event);
    }
  }

  @Override
  public String getText() {
    final String txt = super.getText();
    //Check if content isn't equals to placeholder and if so return empty string.
    //This check is only relevant for IE because IE doesn't support placeholder
    //therefore the placeholder text is set in the actual input.
    return getElement().getAttribute("placeholder").equals(txt) ? "" : txt;
  }

  /**
   * Set to true if the number accepts fractions, i.e. double or float input.
   *
   * @param fraction true if accepts fraction input
   */
  public void setFraction(final boolean fraction) {
    this.fraction = fraction;
  }

  /**
   * Set to true if only accept positive input values.
   *
   * @param positive true if only positive numbers allowed
   */
  public void setPositive(final boolean positive) {
    this.positive = positive;
  }

  /**
   * Sets the text content to null in order to make the placeholder visible.
   */
  public void resetPlaceHolder() {
    if (getValue().doubleValue() == 0) {
      setText(null);
    }
  }

  @Override
  protected String noValidInput(final String value) {
    return fraction
        ? M.messages().ivNotAnFloat(getLabel(), value)
            : M.messages().ivNotAnInteger(getLabel(), value);
  }
}
