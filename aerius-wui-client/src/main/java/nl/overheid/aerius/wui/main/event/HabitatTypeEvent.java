/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.event;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.web.bindery.event.shared.binder.GenericEvent;

import nl.overheid.aerius.shared.domain.HabitatHoverType;

/**
 * Event which is fired when a hover event occurs.
 */
public abstract class HabitatTypeEvent extends GenericEvent {
  public enum HabitatDisplayType {
    ADD, REMOVE, REPLACE;

    public static HabitatDisplayType fromVisibility(final boolean visible) {
      return visible ? ADD : REMOVE;
    }
  }

  private final HabitatHoverType hoverType;
  private final int hoverTypeId;
  private final Set<Integer> habitatTypeIds;
  private final HabitatDisplayType displayType;

  protected HabitatTypeEvent() {
    this(null, null, 0, null);
  }

  protected HabitatTypeEvent(final HabitatHoverType hoverType, final int hoverTypeId, final int habitatTypeId) {
    this(HabitatDisplayType.REPLACE, hoverType, hoverTypeId, habitatTypeId);
  }

  protected HabitatTypeEvent(final HabitatHoverType hoverType, final int hoverTypeId, final Set<Integer> habitatTypeIds) {
    this(HabitatDisplayType.REPLACE, hoverType, hoverTypeId, habitatTypeIds);
  }

  public HabitatTypeEvent(final HabitatDisplayType displayType, final HabitatHoverType hoverType, final int hoverTypeId, final int habitatTypeId) {
    this(displayType, hoverType, hoverTypeId, new HashSet<Integer>(Arrays.asList(new Integer[] { habitatTypeId })));
  }

  public HabitatTypeEvent(final HabitatDisplayType displayType, final HabitatHoverType hoverType, final int hoverTypeId, final Set<Integer> habitatTypeIds) {
    this.displayType = displayType;
    this.hoverType = hoverType;
    this.hoverTypeId = hoverTypeId;
    this.habitatTypeIds = habitatTypeIds;
  }

  public HabitatHoverType getHoverType() {
    return hoverType;
  }

  public int getHoverTypeId() {
    return hoverTypeId;
  }

  public Set<Integer> getHabitatTypeIds() {
    return habitatTypeIds;
  }

  public HabitatDisplayType getDisplayType() {
    return displayType;
  }

  @Override
  public String toString() {
    return "HabitatTypeEvent [hoverType=" + hoverType + ", hoverTypeId=" + hoverTypeId + ", habitatTypeIds=" + habitatTypeIds + ", displayType=" + displayType + "]";
  }
}
