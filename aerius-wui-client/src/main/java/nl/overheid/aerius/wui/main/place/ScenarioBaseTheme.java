/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.place;

import java.util.ArrayList;
import java.util.Arrays;

import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;

/**
 * Enum for theme switch of the calculator application.
 */
public enum ScenarioBaseTheme {
  /**
   * The Nature Area theme.
   */
  NATURE_AREAS("n",
      new ArrayList<>(Arrays.asList(new Substance[] { Substance.NOXNH3, Substance.NOX, Substance.NH3, })),
      new ArrayList<>(Arrays.asList(new Substance[] { Substance.NOX, Substance.NO2, Substance.NH3, })),
      new ArrayList<>(Arrays.asList(new EmissionResultKey[] { EmissionResultKey.NOXNH3_DEPOSITION,
          EmissionResultKey.NH3_DEPOSITION, EmissionResultKey.NOX_DEPOSITION }))),
  /**
   * The air quality theme.
   */
  AIR_QUALITY("a",
      new ArrayList<>(Arrays.asList(new Substance[] { Substance.NOX, Substance.NO2, Substance.PM25, Substance.PM10, })),
      new ArrayList<>(Arrays.asList(new Substance[] { Substance.NOX, Substance.NO2, Substance.PM10, })),
      new ArrayList<>(Arrays.asList(new EmissionResultKey[]
          { EmissionResultKey.NOX_CONCENTRATION, EmissionResultKey.NO2_CONCENTRATION, EmissionResultKey.PM25_CONCENTRATION,
              EmissionResultKey.PM10_CONCENTRATION, EmissionResultKey.PM10_NUM_EXCESS_DAY, })));

  private String type;
  private ArrayList<Substance> substance;
  private final ArrayList<Substance> calculationSubstances;
  private ArrayList<EmissionResultKey> substancesEmissionType;
  private boolean multipleResultTypes;

  private ScenarioBaseTheme(final String type, final ArrayList<Substance> substances, final ArrayList<Substance> calculationSubstances,
      final ArrayList<EmissionResultKey> substancesEmissionType) {
    this.type = type;
    this.substance = substances;
    this.calculationSubstances = calculationSubstances;
    this.substancesEmissionType = substancesEmissionType;
    final EmissionResultType first = substancesEmissionType.get(0).getEmissionResultType();
    for (final EmissionResultKey erk : substancesEmissionType) {
      if (first != erk.getEmissionResultType()) {
        multipleResultTypes = true;
        break;
      }
    }
  }

  /**
   * Return the safe value from a type string, or null if it could not be matched.
   *
   * @param type Type string to get the enum value of.
   *
   * @return Null if the type does not match, or the corresponding {@link ScenarioBaseTheme}.
   */
  public static ScenarioBaseTheme safeValueOf(final String type) {
    return NATURE_AREAS.type.equals(type) ? NATURE_AREAS : AIR_QUALITY.type.equals(type) ? AIR_QUALITY : null;
  }

  public String getType() {
    return type;
  }

  /**
   * @return The substances to be selectable for this theme.
   */
  public ArrayList<Substance> getSubstances() {
    return substance;
  }

  /**
   * @return The substances that should be calculated with this theme.
   */
  public ArrayList<Substance> getCalculationSubstances() {
    return calculationSubstances;
  }

  /**
   * Returns a theme valid {@link EmissionResultKey} that matches the given key or completely or on substance.
   * If nothing matches the first value of theme {@link EmissionResultKey}'s is returned.
   * @param matchKey match if available.
   * @return a theme valid {@link EmissionResultKey}
   */
  public EmissionResultKey getThemeValidEmissionResultKey(final EmissionResultKey matchKey) {
    for (final EmissionResultKey erk : substancesEmissionType) {
      if (matchKey == erk) {
        return erk;
      }
    }
    if (matchKey != null) {
      for (final EmissionResultKey erk : substancesEmissionType) {
        if (matchKey.getSubstance() == erk.getSubstance()) {
          return erk;
        }
      }
    }
    return substancesEmissionType.get(0);
  }

  public ArrayList<EmissionResultKey> getSubstancesEmissionType() {
    return substancesEmissionType;
  }

  public boolean hasMultipleResultTypes() {
    return multipleResultTypes;
  }
}
