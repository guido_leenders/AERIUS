/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.menu;

import java.util.List;

import com.google.gwt.place.shared.Place;

import nl.overheid.aerius.shared.i18n.HelpInfo;

/**
 * Interface for a menu item.
 */
public interface MenuItem {

  /**
   * Returns whether or not this menu item is enabled for this specific place.
   * @param place Place to check.
   * @return True if it is, false if it is not.
   */
  boolean isEnabled(Place place);

  /**
   * Returns help info for this menu item.
   *
   * @return {@link HelpInfo}
   */
  HelpInfo getHelpInfo();

  /**
   * Set the help info data on the menu item.
   * @param helpInfo help info data
   */
  void setHelpInfo(HelpInfo helpInfo);

  /**
   * Returns true if the place corresponds to the place of this menu item.
   * @param place place to check
   * @return true if place is for this menu item
   */
  boolean isActivePlace(Place place);

  /**
   * Menu's test id.
   * @return test id
   */
  String getTestId();

  /**
   * List of child menus items.
   * @return list of child menu items
   */
  List<MenuItem> getChildren();
}
