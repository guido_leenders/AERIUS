/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasOneWidget;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import nl.overheid.aerius.wui.main.resources.R;

/**
 * <p>Wraps an {@link InputCalculator} and a {@link HasValue} into a single widget.</p>
 *
 * <p>The {@link HasValue} widget can optionally be wrapped into one or multiple {@link HasOneWidget}s</p>
 *
 * <p>The {@link HasValue} object should also implement {@link IsWidget}, which it normally does, as described
 * by its implementation contract.</p>
 *
 * <p>The {@link HasValue} object must be generically typed to the same type as the {@link InputCalculator},
 * but unfortunately we can't guarantee that, using parameters, when this widget is implemented in a UiBinder.</p>
 *
 * <p>Does nothing if no {@link InputCalculator} is provided.</p>
 *
 * <p>The calculator button will be pushed over the element's bounds on the right. Unfortunate styling but
 * it's the simplest solution for now. This means the input element it's wrapping needs to shrink in size.
 * May be a hassle for now, but we don't want to make this widget responsible for modifying the input's
 * width property.</p>
 *
 * @param <E> The type of the input.
 */
public class InputWithCalculatorWidget<E> extends FlowPanel implements IsWidget {

  private InputCalculator<E> calculator;
  private HasValue<E> valueWidget;
  private final Button calculatorButton = new Button();

  public InputWithCalculatorWidget() {
    setStyleName(R.css().inputWithCalculator());
    configureCalculatorButton();
  }

  private void configureCalculatorButton() {
    calculatorButton.setStyleName(R.css().inputWithCalculatorButton());
    calculatorButton.addClickHandler(new ClickHandler() {
      @Override
      public void onClick(final ClickEvent event) {
        if (calculator != null) {
          calculator.display();
        }
      }
    });
  }

  @Override
  public void add(final Widget input) {
    super.add(input);

    // Find and bind the calculator with the input
    valueWidget = findValueWidget(input);
    bind();

    // Also add the calculator button here
    super.add(calculatorButton);
  }

  /**
   * Set the {@link InputCalculator} for this wrapper.
   */
  @UiChild(limit = 1, tagname = "calculator")
  public void setCalculator(final InputCalculator<E> calculator) {
    this.calculator = calculator;
    bind();
  }

  /**
   * <p>Recursively finds the value widget inside the given input widget.</p>
   *
   * <p>The {@link HasValue} and the {@link InputCalculator}
   * must be parameterized to the same type as described in the class contract.</p>
   *
   * @param input An {@link IsWidget} that is or contains a {@link HasValue}
   *
   * @return The {@link HasValue} object, or null if none can be found.
   */
  @SuppressWarnings("unchecked")
  private HasValue<E> findValueWidget(final IsWidget input) {
    if (input instanceof HasValue) {
      return (HasValue<E>) input;
    } else if (input instanceof HasOneWidget) {
      return findValueWidget(((HasOneWidget) input).getWidget());
    }

    return null;
  }

  /**
   * Bind the calculator to the widget, if binding is possible.
   */
  private void bind() {
    if (calculator != null && valueWidget != null) {
      calculator.setInput(valueWidget);
    }
  }

  public InputCalculator<E> getCalculator() {
    return calculator;
  }

  public Button getCalculatorButton() {
    return calculatorButton;
  }
}
