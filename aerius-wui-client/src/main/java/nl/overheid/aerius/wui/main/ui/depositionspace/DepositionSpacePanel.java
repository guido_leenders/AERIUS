/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.depositionspace;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResult;
import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.event.LocationChangeEvent;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.DataProvider;
import nl.overheid.aerius.wui.main.widget.table.SimpleCollapsibleDivTable;
import nl.overheid.aerius.wui.main.widget.table.TableColumn;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

/**
 * Stacked panel with result per nature area per habitat.
 */
public class DepositionSpacePanel extends Composite implements Editor<List<CalculationAreaSummaryResult>> {

  public interface DepositionSpaceDriver extends SimpleBeanEditorDriver<List<CalculationAreaSummaryResult>, DepositionSpacePanel> {}

  interface CalculationResultDespositionSpacePanelUiBinder extends UiBinder<Widget, DepositionSpacePanel> {}

  private static final CalculationResultDespositionSpacePanelUiBinder UI_BINDER = GWT.create(CalculationResultDespositionSpacePanelUiBinder.class);

  /**
   * Emtpy rule.
   */
  private static final DevelopmentRuleResult EMPTY_DEVELOPMENT_RULE = new DevelopmentRuleResult(DevelopmentRule.NOT_EXCEEDING_SPACE_CHECK);

  @Path("") final DataProvider<List<CalculationAreaSummaryResult>, CalculationAreaSummaryResult> dataProvider =
      new DataProvider<List<CalculationAreaSummaryResult>, CalculationAreaSummaryResult>();
  @UiField SimpleCollapsibleDivTable<CalculationAreaSummaryResult> divTable;

  @UiField(provided = true) TextColumn<CalculationAreaSummaryResult> labelColumn;
  @UiField(provided = true) WidgetFactory<CalculationAreaSummaryResult, DepositionSpaceHeaderWidget> widgetColumn;
  @UiField(provided = true) WidgetFactory<CalculationAreaSummaryResult, DevelopmentRuleResultsTable> tableContent;

  private final Comparator<CalculationAreaSummaryResult> areaCompator = new Comparator<CalculationAreaSummaryResult>() {

    @Override
    public int compare(final CalculationAreaSummaryResult casr1, final CalculationAreaSummaryResult casr2) {
      final DevelopmentRuleResult drr2 = getMax(casr2);
      final DevelopmentRuleResult drr1 = getMax(casr1);
      final int ruleCmp = drr2.getRule().compareTo(drr1.getRule());
      return ruleCmp == 0 ? Integer.compare(drr2.size(), drr1.size()) : ruleCmp;
    }

    private DevelopmentRuleResult getMax(final CalculationAreaSummaryResult casr) {
      DevelopmentRuleResult max = EMPTY_DEVELOPMENT_RULE;
      for (final DevelopmentRuleResult dr : casr.getDevelopmentRuleResults()) {
        if (!dr.isEmpty() && dr.getRule().compareTo(max.getRule()) >= 0) {
          max = dr;
        }
      }
      return max;
    }
  };

  @Inject
  public DepositionSpacePanel(final EventBus eventBus) {
    labelColumn = new TextColumn<CalculationAreaSummaryResult>() {
      @Override
      public String getValue(final CalculationAreaSummaryResult value) {
        return value.getArea().getName();
      }
    };
    labelColumn.ensureDebugId(TestID.LABEL);

    widgetColumn = new WidgetFactory<CalculationAreaSummaryResult, DepositionSpaceHeaderWidget>() {
      @Override
      public DepositionSpaceHeaderWidget createWidget(final CalculationAreaSummaryResult entry) {
        return new DepositionSpaceHeaderWidget(eventBus, entry.getArea(), entry.getDevelopmentRuleResults());
      }

      @Override
      public void applyRowOptions(final Widget rowContainer, final DepositionSpaceHeaderWidget widget) {
        rowContainer.addHandler(widget, ValueChangeEvent.getType());
      }

      @Override
      public void applyCellOptions(final Widget cell, final CalculationAreaSummaryResult object) {
        super.applyCellOptions(cell, object);

        cell.ensureDebugId(object.getArea().getName());
      }
    };
    labelColumn.ensureDebugId(TestID.LABEL);

    tableContent = new TableColumn<DevelopmentRuleResult, CalculationAreaSummaryResult, DevelopmentRuleResultsTable>() {
      @Override
      public DevelopmentRuleResultsTable createDataTable(final CalculationAreaSummaryResult origin) {
        final DevelopmentRuleResultsTable comparisonDepositionSpaceTable = new DevelopmentRuleResultsTable(eventBus);
        comparisonDepositionSpaceTable.ensureDebugId(TestID.INNER_TABLE);
        return comparisonDepositionSpaceTable;
      }

      @Override
      public Collection<DevelopmentRuleResult> getRowData(final CalculationAreaSummaryResult object) {
        return object.getDevelopmentRuleResults();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    dataProvider.addDataDisplay(divTable);
    dataProvider.setComparator(areaCompator);
    divTable.ensureDebugId(TestID.TABLE);

    // Automatically zoom to area when selected (if not in current extent)
    divTable.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final CalculationAreaSummaryResult selectedObject =
            ((SingleSelectionModel<CalculationAreaSummaryResult>) divTable.getSelectionModel()).getSelectedObject();
        if (selectedObject != null) {
          eventBus.fireEvent(new LocationChangeEvent(selectedObject.getArea().getBounds(), true));
        }
      }
    });
  }
}
