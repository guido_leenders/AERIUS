/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * A widget which displays content for a given widget.
 *
 * @param <T> The object to display context for.
 * 
 * TODO: Get setArrowTop out of here and replace this with WidgetFactory.
 */
public interface ContextualContent<T> extends IsWidget {
  /**
   * Show context for the given object.
   * 
   * @param obj Object to show context for.
   * 
   * @return Whether context is being shown.
   */
  boolean view(T obj);

  /**
   * TODO Get the arrow out of here. ContextDivTable should take care of this.
   */
  void setArrowTop(int top);
}
