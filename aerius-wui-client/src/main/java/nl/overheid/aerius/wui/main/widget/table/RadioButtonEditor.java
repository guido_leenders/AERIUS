/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.widget.table;

import java.util.List;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RadioButton;

import nl.overheid.aerius.wui.main.resources.R;

public abstract class RadioButtonEditor<T> extends FlowPanel implements HasValueChangeHandlers<T> {
  private final String groupId;

  public RadioButtonEditor(final List<T> values, final T selectedValue) {
    groupId = DOM.createUniqueId();

    for (final T value : values) {
      final RadioButton radioButton = new RadioButton(groupId, getText(value));
      radioButton.setStyleName(R.css().radioButton());
      radioButton.setValue(value.equals(selectedValue));
      radioButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
        @Override
        public void onValueChange(final ValueChangeEvent<Boolean> event) {
          if (event.getValue()) {
            ValueChangeEvent.fire(RadioButtonEditor.this, value);
          }
        }
      });

      add(radioButton);
    }
  }

  @Override
  public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<T> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  protected abstract String getText(final T value);
}
