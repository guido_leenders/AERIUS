/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.ui.validation;

/**
 * Checks if the value is between the value 0 and 100%. Only int values allowed.
 */
public class PercentageRangeValidator extends IntegerRangeValidator {

  private static final int DUH_MAX_PERCENTAGE = 100;

  public PercentageRangeValidator() {
    super(0, DUH_MAX_PERCENTAGE);
  }
}
