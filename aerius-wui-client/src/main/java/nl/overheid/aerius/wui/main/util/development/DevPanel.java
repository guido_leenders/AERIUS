/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.main.util.development;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * Interface for panel used in development mode debug. In script mode (compiled) the devpanel is compiled out of the application.
 */
public interface DevPanel extends IsWidget {

  /**
   * Height of the dev panel.
   */
  int HEIGHT = DevModeUtil.I.isDevMode() ? 75 : 0;
}
