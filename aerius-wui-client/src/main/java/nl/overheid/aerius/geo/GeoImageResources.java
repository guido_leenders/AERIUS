/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.geo;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * ClientBundle resource with all images used in the geo package. Mostly for the
 * navigation widget.
 */
public interface GeoImageResources extends ClientBundle {

  @Source("resources/mapnav-normal.png")
  ImageResource mapNavigationBackground();

  @Source("resources/mapnav-left-active.png")
  ImageResource mapNavigationLeft();

  @Source("resources/mapnav-left-hover.png")
  ImageResource mapNavigationLeftOver();

  @Source("resources/mapnav-right-active.png")
  ImageResource mapNavigationRight();

  @Source("resources/mapnav-right-hover.png")
  ImageResource mapNavigationRightOver();

  @Source("resources/mapnav-top-active.png")
  ImageResource mapNavigationUp();

  @Source("resources/mapnav-top-hover.png")
  ImageResource mapNavigationUpOver();

  @Source("resources/mapnav-bottom-active.png")
  ImageResource mapNavigationDown();

  @Source("resources/mapnav-bottom-hover.png")
  ImageResource mapNavigationDownOver();

  // Zoom
  @Source("resources/btn-mapzoom-plus-inactive.png")
  ImageResource zoomInInactive();

  @Source("resources/btn-mapzoom-plus-active.png")
  ImageResource zoomInOver();

  @Source("resources/btn-mapzoom-plus-normal.png")
  ImageResource zoomIn();

  @Source("resources/btn-mapzoom-min-inactive.png")
  ImageResource zoomOutInactive();

  @Source("resources/btn-mapzoom-min-active.png")
  ImageResource zoomOutOver();

  @Source("resources/btn-mapzoom-min-normal.png")
  ImageResource zoomOut();
}
