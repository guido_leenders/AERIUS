/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

/**
 * Messages interface for all tool tip messages for the Help Button.
 */
public interface HelpToolTipMessages<T> {
  // Tool tip Texts
  T ttNotificationCenter();

  // Location page
  T ttLocationButtonSource();
  T ttLocationButtonManual();
  T ttLocationButtonImport();
  T ttLocationButtonPoint();
  T ttLocationButtonLine();
  T ttLocationButtonPolygon();
  T ttLocationButtonEditShape();
  T ttLocationXCoordinate();
  T ttLocationYCoordinate();
  T ttLocationButtonNextStep();
  T ttLocationButtonCancel();

  // Import popup *
  T ttImportFileField();


  // Source Detail Page
  T ttSourceCharacteristics();
  T ttCalculateHeatContent();
  T ttSourceDetailButtonEditLocation();
  T ttSourceDetailLabel();
  T ttSourceDetailButtonNextStep();
  T ttSourceDetailSectorGroup();
  T ttGenericEmissions();

  T ttSourceDetailSector();
  T ttSourceDetailSectorAgriculture();
  T ttSourceDetailSectorLiveAndWork();
  T ttSourceDetailSectorIndustry();
  T ttSourceDetailSectorMobileEquipment();
  T ttSourceDetailSectorRailTransportation();
  T ttSourceDetailSectorAviation();
  T ttSourceDetailSectorRoadTransportation();
  T ttSourceDetailSectorShipping();
  T ttSourceDetailSectorShippingInland();
  T ttSourceDetailSectorShippingInlandDocked();
  T ttSourceDetailSectorShippingMaritimeInland();
  T ttSourceDetailSectorShippingMaritimeDocked();
  T ttSourceDetailSectorShippingMaritimeMaritime();
  T ttWaterwayEditor();


  // Calculation Point Page
  T ttCalculationPointLabel();
  T ttCalculationPointXCoordinate();
  T ttCalculationPointYCoordinate();
  T ttCalculationPointButtonNextStep();
  T ttCalculationPointDetermine();

  // Top buttons
  T ttInfoPanel();
  T ttLayerPanel();
  T ttLayerPanelSlider();
  T ttLayerPanelHabitatType();
  T ttOptionsPanel();
  T ttYearSelection();
  T ttSubstanceSelection();
  T ttResultTypeSelection();
  T ttInfoPanelHabitatType();

  // Search box
  T ttSearchBox();
  T ttSearchNatureArea();
  T ttNavigateControle();


  // Calculation Main menu
  T ttCalculationMainMenu();

  // Calculation Result
  T ttResultGraphicsTab();
  T ttResultTableTab();
  T ttResultFilterTab();
  T ttResultTypeDropDown();

  // Calculation Overview page
  T ttOverviewCalculationPointsButtonEdit();
  T ttOverviewCalculationPointsButtonDelete();
  T ttOverviewCalculationPointsButtonCopy();
  T ttCalculationPointOverview();


  // Export *
  T ttExportEmail();
  T ttExport();
  T ttExportCorporation();
  T ttExportProjectName();
  T ttExportLocation();
  T ttExportDescription();

  // Sources Overview page
  T ttOverviewButtonEdit();
  T ttOverviewButtonDelete();
  T ttOverviewButtonCopy();
  T ttOverviewButtonAddSource();
  T ttOverviewButtonAddCalculationPoint();
  T ttOverviewButtonImportCalculationPoint();
  T ttOverviewButtonExport();
  T ttOverviewButtonCalculate();
  T ttOverviewCalculationMethod();
  T ttOverviewCalculationMethodUserDefined();
  T ttOverviewCalculationMaxRange();
  T ttOverviewCalculationThreshold();
  T ttOverviewCalculationAssessmentBoundary();
  T ttOverviewEditSource();
  T ttOverviewCalculation();
  T ttOverviewCalculationPermitRadiusType();

  // Emission details
  T ttDeleteButtonHelp();
  T ttGenericEmissionNH3();
  T ttGenericEmissionNOX();
  T ttGenericEmissionPM10();

  // Road emissions
  T ttRoadVehicleAdd();
  T ttRoadVehicleEdit();
  T ttRoadVehicleDelete();
  T ttRoadVehicleCopy();
  T ttRoadEmissionsStagnation();
  T ttRoadEmissionsMaxSpeed();
  T ttRoadStandardVehicleType();
  T ttRoadVehiclesPerTimeUnit();
  T ttRoadCustomDescription();
  T ttRoadCustomEmission();
  T ttRoadCustomEmissionPM10();
  T ttRoadEmissionsEnforcement();
  T ttRoadType();

  // Farm emissions
  T ttFarmLodgingCategory();
  T ttFarmLodgingAdditionalCategory();
  T ttFarmLodgingReductiveCategory();
  T ttFarmLodgingFodderMeasure();
  T ttFarmLodgingSystemDefinition();
  T ttFarmLodgingAmount();
  T ttFarmLodgingConfirmRow();
  T ttFarmLodgingCustomLodgeDescription();
  T ttFarmLodgingCustomLodgeEmissionFactor();

  // Plan emissions
  T ttPlanCategory();
  T ttPlanEdit();
  T ttPlanCopy();
  T ttPlanDelete();
  T ttPlanAdd();
  T ttPlanName();
  T ttPlanAmount();
  T ttPlanCategoryList();
  T ttPlanSubmitButton();

  // OPS Characteristics
  T ttOPSHeight();
  T ttOPSBuildingInfluence();
  T ttOPSBuildingLength();
  T ttOPSBuildingWidth();
  T ttOPSBuildingHeight();
  T ttOPSBuildingOrientation();
  T ttOPSSpread();
  T ttOPSHeatContent();
  T ttOPSParticleSizeDistribution();

  // Progress page
  T ttProgressButtonStop();

  // Mobile sources *
  T ttMobileSourceEdit();
  T ttMobileSourceDelete();
  T ttMobileSourceCopy();
  T ttMobileSourceAdd();
  T ttMobileSourceName();
  T ttMobileSourceFuelage();
  T ttMobileSourceStageCategory();
  T ttMobileSourceCustomCategory();
  T ttMobileSourceCategory();
  T ttMobileSourceSave();
  T ttMobileShowCalculator();
  T ttMobileEmissionCalculator();
  T ttMobileOPSHeight();
  T ttMobileOPSSpread();
  T ttMobileOPSHeatContent();
  T ttMobileGenericEmissionNOX();

  // Shipping sources *
  T ttShipEdit();
  T ttShipDelete();
  T ttShipCopy();
  T ttShipAdd();
  T ttShipAmount();
  T ttShipName();
  T ttShipType();
  T ttShipResidenceTime();
  T ttShippingTimeUnit();
  T ttShipSave();
  T ttShipRoute();
  T ttInlandShipName();
  T ttInlandShipType();
  T ttInlandShipTimeUnitAtoB();
  T ttInlandShipTimeUnitBtoA();
  T ttInlandShipAmountAtoB();
  T ttInlandShipAmountBtoA();
  T ttInlandShipPercentageLadenAtoB();
  T ttInlandShipPercentageLadenBtoA();
  T ttInlandShipSave();
  T ttInlandMooringShipName();
  T ttInlandMooringShipType();
  T ttInlandMooringShipResidenceTime();
  T ttInlandMooringShipSave();

  T ttMaritimeRouteShipName();
  T ttMaritimeRouteShipType();
  T ttMaritimeRouteShipMovements();
  T ttMaritimeRouteShipSave();
  T ttMaritimeRouteShipTimeUnit();

  // Situations *
  T ttSituationOne();
  T ttSituationTwo();
  T ttSituationNew();
  T ttSituationComparison();
  T ttSituationComparisonDeposition();

  //Scenario tool tips
  T ttScenarioButtonImport();

  // Register tool tips
  T ttRegisterDashboardFilterProvince();
  T ttRegisterDashboardPermitHeading();
  T ttRegisterDashboardNoticeHeading();
  T ttRegisterRefresh();
  T ttRegisterExportNotices();
  T ttRegisterExportPermits();

  T ttRegisterLogoutButton();
  T ttHelpButton();

  T ttEcologicalQuality1a();
  T ttEcologicalQuality1b();
  T ttEcologicalQuality2();
  T ttEcologicalQualityNone();

  T ttHabitatsTableSurfaceTitleTotalMapped();
  T ttHabitatsTableSurfaceTitleTotalCartographic();
  T ttHabitatsTableSurfaceTitleRelevantMapped();
  T ttHabitatsTableSurfaceTitleRelevantCartographic();

}
