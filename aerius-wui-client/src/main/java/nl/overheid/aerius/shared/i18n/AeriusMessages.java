/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import java.util.Date;

import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.Messages;
import com.google.gwt.i18n.client.impl.plurals.DefaultRule_en;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.context.EmissionResultValueDisplaySettings.DepositionValueDisplayType;
import nl.overheid.aerius.shared.domain.deposition.ResultInfoType;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRule;
import nl.overheid.aerius.shared.domain.info.EcologyQualityType;
import nl.overheid.aerius.shared.domain.info.HabitatGoal;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadSideBarrier.RoadSideBarrierType;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;

/**
 * GWT interface to language specific static text.
 */
@DefaultLocale(SharedConstants.LOCALE_NL)
public interface AeriusMessages extends Messages, HelpToolTipMessages<String>, GeoMessages, SourceMessages, InputValidationMessages, RegisterMessages,
    MeldingMessages, AdminMessages, ScenarioMessages {

  @Description("A fatal error occurred, most likely a bug.")
  String errorInternalFatal();

  @Description("A layer {0} could not be loaded, most likely external server problems (out of our scope).")
  String errorLayerCouldNotBeLoaded(String text);

  @Description("User has an older version of application and should refresh")
  String errorInternalApplicationOutdated();

  @Description("Problem with internet connection.")
  String errorConnection();

  @Description("Error while communicating with API. {0} - {1}")
  String errorAPIError(int errorCode, String message);

  @Description("Service is not available. Probably in maintenance.")
  String serviceUnavailable();

  @Deprecated
  String errorCalculationResultNoConcentrationSummaryResults();

  String calculatorLeavePageQuestion();

  String calculator();

  @Override
  String scenario();

  String menuManual();

  String dateFormat();

  String dateTimeFormat();

  /**
   * Get a friendly language string for the given Language value.
   *
   * note: This is hardcoded and excluded from the .properties because we're returning the result in the native language requested;
   *
   * this method, while language oriented, is language agnostic.
   *
   * @param lang Language to get a friendly string for.
   *
   * @return The language in its native tongue.
   */
  @DefaultMessage("Unknown")
  @AlternateMessage({ "EN", "English", "NL", "Nederlands" })
  String applicationLanguage(@Select Language lang);

  String applicationReloadPageOnLogout();

  /**
   * Menus.
   */
  String calculatorMenuStart();

  String calculatorMenuEmissionSources();

  String calculatorMenuCalculationPoints();

  String calculatorMenuResults();

  String calculatorMenuMelding();

  String calculatorMenuScenarios();

  String calculatorMenuUtils();

  String calculatorMenuThemeSwitch(@Select ScenarioBaseTheme theme);

  // Titles. *
  String createEmissionSource();

  String configureEmissionSource();

  String createCalculationPoint();

  String calculatorLocationExplainDraw();

  String calculatorLocationExplainCoordinates();

  String calculatorLocationDetermineAutomaticallyText();

  String calculatorLocationDetermineAutomatically();

  String calculatorSelectSector();

  String calculatorSelectSectorGroup();

  String calculatorOptions();

  String calculatorExplainFarmStandard();

  String calculatorExplainFarmCustom();

  String calculatorWarningRAVStacking();

  String calculatorListExplain();

  String calculatorAddSource();

  String calculatorImportSource();

  String calculatorAddDeposition();

  String calculatorImportDeposition();

  String calculatorCalcDeposition();

  String calculatorToolDistance(String text);

  String calculatorToolSurface(String text);

  String calculatePermit();

  String calculateNatureAreas();

  String calculateRadius();

  String calculateUserDefined();

  String calculateMaxRange();

  String calculateTemporaryProjectSelection();

  String calculateTemporaryInfo();

  String calculateTemporaryStart();

  String calculateTemporaryMaxYears();

  String calculateTemporaryDefaultYearValue();

  String calculatePermitRadiusTypeTitleLabel();

  String calculateEndYear();

  String calculatorStop();

  String xCoordinate();

  String yCoordinate();

  String nextStepButton();

  String save();

  String cancelButton();

  String emailAddress();

  String projectKey();

  String emptyFirstCategory();

  String emissionResultType(@Select EmissionResultType emissionResultType);

  String emissionResultTypeChar(@Select EmissionResultType emissionResultType);

  String roadSideBarrierType(@Select RoadSideBarrierType roadSideBarrierType);

  String searchWidgetPlaceholder();

  /**
   * Extensions of SearchSuggestionType's names go in here.
   *
   * These are all enums but their parent type is an interface, the @Select annotation doesn't support interfaces. We therefore send the .name() as
   * String.
   *
   * @param type Type of SearchSuggestionType - an enum name.
   *
   * @return Localized named type.
   */
  String searchSuggestionType(@Select String type);

  String searchNoMatchesFound();

  String searchNoMatchesFoundAnnotation();

  String layerOpacityTitleNoSelection();

  // Legend. *
  String legendPanelTitle();

  String legendDepositionMarkerText();

  String legendDepositionHighestCalculatedMarkerText();

  String legendDepositionHighestTotalMarkerText();

  String legendDepositionHighestExceedingMarkerText();

  String okButton();

  String closeButton();

  String source();

  String markerLayerTitle();

  String developmentRuleLayerTitle();

  String calculatorBoundaryLayerTitle();

  // Config panel
  String settingsPanelTitle();

  // Layer Panel. *
  String layerPanelTitle();

  String layerConfigTitle();

  String layerConfigActiveLayerTitle();

  String layerConfigInactiveLayerTitle();

  String layerConfigUploadOwnLayer();

  String layerConfigUploadButton();

  String layerPanelSelect();

  // Info panel. *
  String infoPanelReceptor(int id);

  String infoPanelLocation(int x, int y);

  String infoPanelTitleDefault();

  String infoPanelTextDefault();

  String infoPanelTextLoad();

  String infoPanelAreaInfoLabelContractor();

  String infoPanelAreaInfoLabelAreaName();

  String infoPanelAreaInfoLabelAreaCode();

  String infoPanelAreaInfoLabelCategory();

  String infoPanelAreaInfoLabelCategoryValue(@Select EcologyQualityType ecologyQualityType);

  String infoPanelAreaInfoLabelEnvironment();

  String infoPanelAreaInfoLabelSurface();

  String infoPanelAreaInfoLabelSurfaceRelevantMapped();

  String infoPanelAreaInfoLabelSurfaceRelevantCartographic();

  String infoPanelAreaInfoLabelProtection();

  String infoPanelAreaInfoLabelStatus();

  String infoPanelAreaInfoDataSurface(String v);

  String infoPanelAreaText();

  String infoPanelAreaZoomTitle();

  String infoPanelHabitatTypesTitle();

  String infoPanelHabitatTypesText(int value);

  String infoPanelHabitatTypesLabelHabitatType();

  String infoPanelHabitatTypesLabelKDW();

  String infoPanelHabitatTypesLabelCoverage();

  String infoPanelHabitatTypesDataCode(String a, String b);

  String infoPanelEmissionResultLabelBackground(@Select EmissionResultType emissionResultType);

  String infoPanelEmissionResultLabelCalculatedSources(@Select EmissionResultType emissionResultType);

  String infoPanelEmissionResultUtilisationTitle();

  String infoPanelEmissionResultUtilisationLabel();

  String infoPanelEmissionResultLabelDifference(@Select EmissionResultType emissionResultType);

  String infoPanelMarkerTotalDeposition(String value);

  String infoPanelMarkerCalculatedDeposition(String value);

  String infoPanelMarkerExceedPercentage(String value);

  String infoPanelFeatureLabel();

  String depositionLocationExplain();

  String depositionLabel();

  String finish();

  // Import panel. *
  String importPanelTitle();

  String importPanelText();

  String importPanelDragDropText();

  String importPanelDragDropCancelText();

  String importPanelCounter(@PluralCount(DefaultRule01n.class) int amountOfSources, @PluralCount(DefaultRule01n.class) int amountOfPoints);

  String importPanelCounterConflict(@PluralCount(DefaultRule_en.class) int conflictsImported,
      @PluralCount(DefaultRule_en.class) int conlictsExisting);

  String importPanelSubstanceText();

  String importPanelConflict();

  String importPanelConflictOverwrite();

  String importPanelConflictAdd();

  String importPanelConflictRemoveExisting();

  String importPanelContinueOnError();

  String importPanelContinueOnErrorYes();

  String importPanelContinueOnErrorNo();

  String importPanelOk();

  String importPanelCheckBoxUtilsationText();

  String calculationPoint();

  String calculationListCalculationPointNameHeader();

  String calculationListCalculationPointPurgeAll();

  String calculationListEmissionSourceNameHeader();

  String calculationListEmissionSourcePurgeAll();

  String calculationListEmissionSourcePurgeAllConfirm();

  // Startup. *
  String startUpExplain();

  String startUpHelp();

  String inputSource();

  String importButton();

  String startUpTitle();

  // Progress. *
  String calculatorProgressMaxDistanceLabel();

  String resultPlaceOverview();

  String resultPlaceGraphics();

  String resultPlaceTable();

  String resultPlaceFilter();

  String calculatorDevelopmentRuleType(@Select DevelopmentRule rule);

  String calculationViewText(String pasPermitExplanationURL, String pasPpListURL);

  String calculationOverviewTextNoCalculation();

  String calculationOverviewTextMelding();

  String calculationOverviewTextPermitRequired(@PluralCount(DefaultRule01n.class) int assessmentAreas);

  String calculationOverviewTextPermitRequiredNoSpace();

  String calculationOverviewButtonMelding();

  String calculationOverviewButtonExport();

  String calculationOverviewMeldingTitle();

  String calculationOverviewMeldingText();

  String calculationOverviewMeldingForwardText();

  String calculationOverviewMeldingWarningText();

  String calculationOverviewMeldingForwardButton();

  String calculationOverviewMeldingPrepError();

  // Results.
  String calculationResultInfoType(@Select ResultInfoType val);

  String calculationResultHaCalculated(int numberOfHa);

  String calculatorResultDepositionSpaceLabel();

  String calculationTableNoData();

  String calculationTableNoDataYear(int year);

  String calculationTableNoDataSubstance(String substance);

  String calculationTableNoResultsInRange();

  String calculationFilterNoHabitatForArea(String naturaArea);

  String calculationFilterNoCustomPoints();

  String calculationTableCalculationRunning();

  // Determine calculation points.
  String determineCalcPointsTitle();

  String determineCalcPointsConfirm();

  String determineCalcPointsReallyConfirm();

  String determineCalcPointsDistanceFromSources();

  String determineCalcPointsDistanceFromSourcesUnit();

  String determineCalcPointsResult(@PluralCount(DefaultRule01n.class) int points, @PluralCount(DefaultRule01n.class) int areas);

  String determineCalcPointOverlapCentroid(String label);

  String determineCalcPointsWarningNoSource();

  // Habitat filter
  String habitatFilterBarTitleArea(String surface);

  String habitatFilterBarTitleHexagons(String numberOfReceptors);

  String habitatFilterLoadingText();

  String habitatFilterNumReceptors();

  String habitatFilterSurface();

  String habitatFilterAllRelevantHabitats();

  // Export
  String export(String situationName);

  String exportBothSituations();

  String exportGo();

  String exportOptionGisOnlyInput();

  String exportOptionGisAll();

  String exportOptionPAADevelopmentSpaces();

  String exportOptionPAADemand();

  String exportOptionPAAOwnUse();

  String exportOptionOPS();

  String exportExplainSingleSituation();

  String exportExplainEmail();

  String exportExplainAdditionalInfo();

  String exportCorporation();

  String exportProjectName();

  String exportFacilityLocation();

  String exportStreetAddress();

  String exportPostcode();

  String exportCity();

  String exportDescription();

  // Notifications. *
  String removeAllMessagesButton();

  String notificationCalculationStarted(@Select CalculationType type, @Optional String distance);

  String notificationCalculationStartNoValidSubstance();

  String notificationCalculationStartSubstanceCorrected(String newSubstance);

  String notificationCalculationStartNoValidCustomCalculationPoints();

  String notificationCalculationComplete();

  String notificationCalculationError();

  String notificationCalculationCancelled();

  String notificationImportComplete();

  String notificationImportCompleteSources(@PluralCount(DefaultRule01n.class) int addedSources,
      @PluralCount(DefaultRule01n.class) int overwrittenSources);

  String notificationImportCompletePoints(@PluralCount(DefaultRule01n.class) int addedPoints,
      @PluralCount(DefaultRule01n.class) int overwrittenPoints);

  String notificationImportCompleteNoItems();

  String notificationExportStarted();

  String notificationExportFinished();

  String notificationExportFinishedButNoEmail(@Optional String emailAddress);

  String notificationExportDownload();

  String notificationExportFailed();

  String notificationExportInvalid();

  String notificationDateTimeFormat();

  String notificationDateFormat();

  String notificationImportYearChanged();

  String notificationLineTooLong(int lengthLimit, int actualLength);

  String notificationSurfaceToLarge(int surfaceLimit, int actualSurface);

  String notificationLineIntersects();

  String notificationPolygonIntersects();

  String notificationReference(long reference);

  String notificationCalculationInvalidated();

  String notificationMaxSourcesReached(int max);

  String notificationSourceWarnDefaultOPS();

  // Help button.
  String helpPopupLinkText();

  String helpButton();

  String helpOn();

  String helpOff();

  // Label button.
  String toggleLabelButton();

  String labelOn();

  String labelOff();

  // There are more notifications than its container can hold. *
  String notificationsMoreThanMax();

  // Comparator. *
  String situationOne();

  String situationTwo();

  String situationUtilisationUtilised();

  String situationUtilisationReserved();

  String situationCreate();

  String situationNameLabel();

  String situationDefaultNameEmpty();

  String situationDefaultName(int id);

  String situationEditOkButton();

  String situationDepositionListExplain(String situationName);

  String situationDepositionListDepositionSpace();

  String situationDepositionListDepositionSpaceIndication(Date developmentSpaceSnapshotTime);

  String situationsCompare();

  String compareSourcesHeaderType();

  String compareSourcesHeaderNumberOfSources();

  String compareSourcesHeaderEmission();

  String compareSourcesFooterLabel();

  String compareEmissionListExplain(String originalName, String variantName);

  String compareDepositionListExplain(String originalName, String variantName);

  String compareDepositionUtilisationListExplain(String originalName, String variantName);

  // Heat content. *
  String heatContentCalculatorTitle();

  String heatContentCalculatorTemperature();

  String heatContentCalculatorHeatContent();

  String heatContentCalculatorSurface();

  String heatContentCalculatorOutflowVelocity();

  String heatContentCalculatorSurroundingTemperature();

  // Units and unit formatting. *
  String unitNoCriticalLevel();

  String unitDeposition(@Select DepositionValueDisplayType displayType, String str);

  String unitDepositionSingular(@Select DepositionValueDisplayType displayType);

  String unitDepositionSingularHa(@Select DepositionValueDisplayType displayType);

  String unitDepositionSingularY(@Select DepositionValueDisplayType displayType);

  String unitDepositionSingularHaY(@Select DepositionValueDisplayType displayType);

  // register:
  String unitSingularMolY();

  String unitC(String str);
  
  String unitKgY(String str);

  String unitMTY(String str);

  String unitMinimumMTY();

  String unitMinimumNegativeMTY();

  String unitMW(String str);

  String unitM(String str);

  String unitMSingular();

  String unitKgYSingular();

  String unitLiterPerYSingular();

  String unitGramPerKWHSingular();

  String unitPercentageSingular();

  String unitSingularCelsius();
  
  String unitSingularDegrees();

  String unitSingularkW();

  String unitSingularMW();

  String unitHoursPerYearSingular();

  String unitSingularSquareMetre();

  String unitSingularMetrePerSecond();

  String unitSurface(String str);

  String unitSurfaceSingular();

  String unitKmH(int ms);

  String unitKB(String kiloBytes);

  String unitMB(String megaBytes);

  String unitUgM3(String concentration);

  String unitUgM3Singular();

  String unitDays(@PluralCount(DefaultRule_en.class) int numDays);

  String unitDaysYearSingular();

  String unitKm(String km);

  String unitPercentage(String percentage);
  
  String unitBuildingDimensions(String lenght, String width, String height, String orientation);

  String decimalNumberCapped(double value, @Select int digits);

  String decimalNumberFixed(double value, @Select int digits);
  
  String emissionResultQuantityAndUnit(@Select EmissionResultType emissionResultType, @Select DepositionValueDisplayType depositionDisplayType);

  String emissionResultGraphicsFooter(@Select EmissionResultType emissionResultType, String emission);

  String genericCancel();

  String genericImport();

  String genericOK();

  String genericSave();

  String genericLogin();

  String genericSend();

  String layerHabitatFilter();

  String layerHabitatLegend();

  String calculatorSaveButton();

  String noLayerName();

  String nullDateDefault();

  String prioritySubProjectDevelopmentSpaceMinimum();

  String meldingMenuTitle();

  String compatibilityOldFirefox();

  String noContent();

  String filterExpandTitle();

  String habitatFilterDeposition();

  String habitatsTableCoveragePercent(String coverage);

  String habitatsTableHabitat();

  String habitatsTableRelevant();

  String habitatsTableExtentGoal();

  String habitatsTableSurface();

  String habitatsTableSurfaceMapped();

  String habitatsTableSurfaceCartographic();

  String habitatsTableSurfaceTotal();

  String habitatsTableSurfaceRelevant();

  String habitatsTableHabitatGoal(@Select HabitatGoal goal);

  String habitatsTableQualityGoal();

  String habitatsTableCDV();

  String featureSelectedSources();

  String featureSMR2RoadNetworkLabel();

  String calculationDelayedNotification();
}
