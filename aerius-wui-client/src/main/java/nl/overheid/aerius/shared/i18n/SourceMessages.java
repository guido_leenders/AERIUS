/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.Messages.Select;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics.HeatContentType;
import nl.overheid.aerius.shared.domain.ops.OutflowDirectionType;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.sector.category.LodgingStackType;
import nl.overheid.aerius.shared.domain.sector.category.PlanCategory.CategoryUnit;
import nl.overheid.aerius.shared.domain.sector.category.VehicleType;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.shared.domain.source.SRM2EmissionSource.RoadElevation;
import nl.overheid.aerius.shared.domain.source.TimeUnit;

/**
 * Messages interface for Emission Source related UI elements.
 */
public interface SourceMessages {

  String sectorGroup(@Select SectorGroup sectorGroup);

  String sourceGeometry();
  String sourceGeometry(@Select WKTGeometry.TYPE value);

  String sourceLabel();
  String sourceTemperature();
  String sourceOPSUsedValue(String value, String usedValue);
  String sourceOutflowDiameter();
  String sourceOutflowVelocity();
  String sourceOutflowDirection();
  String sourceOutflowDirectionType(@Select OutflowDirectionType type);
  String sourceHeatContent();
  String sourceValuesOutSideRangeWarning(String url);
  String sourceHeight();
  String sourceHeightWithBuildingInfluence();
  String sourceBuildingInfluence();
  String sourceBuildingLength();
  String sourceBuildingWidth();
  String sourceBuildingHeight();
  String sourceBuildingOrientation();
  String sourceBuildingDimensionLabel();
  String sourceDiameter();
  String sourceSpread();
  String sourceDiurnalVariation();
  String sourceParticleSizeDistribution();
  String sourceEmission();
  String sourceEmissions();
  String sourceHeatType(@Select HeatContentType type);

  String timeUnit(@Select TimeUnit value);

  // Farm sources
  String farmLodgingAddFarm();
  String farmLodgingRAVCode();
  String farmLodgingBWLCode();
  String farmLodgingAmount();
  String farmLodgingOverviewEntry(String code, int amount);
  String farmLodgingDescription();
  String farmLodgingEmissionFactor();
  String farmLodgingAddCustomLodge();
  String farmLodgingAddLodge();
  String farmLodgingRAVCodeFactor(String emissionFactor);
  String farmLodgingTotalAnimals();
  String farmLodgingStackType(@Select LodgingStackType lst);
  String farmLodgingSelectType();
  String farmNoValue();
  String farmSubSourceViewerAnimals();
  String farmSubSourceViewerReduction();
  String farmSubSourceViewerEmission();
  String farmSubSourceViewerFootNoteRef();
  String farmSubSourceViewerFootNote();

  // Road sources
  String roadSpeedTyping();
  String roadMaxSpeed();
  String roadSelectMaxSpeed();
  String roadStrictEnforcement(@Select boolean strict);
  String roadElevation(@Select RoadElevation elevation, int elevationHeight);
  String roadEmissionsTitle();
  String roadVehicleAdd();
  String roadStandardVehicle();
  String roadSpecificVehicle();
  String roadCustomVehicle();
  String roadUserSpecifiedVehicle();
  String roadStagnation();
  String roadCategory();
  String roadVehicleType(@Select VehicleType vehicleType);
  String roadNumberOfVehicles();
  String roadCustomDescription();
  String roadCustomEmissions();
  String roadCustomEmissionUnit();
  String roadCustomEmissionUnitPM10();

  // network emission
  String networkEmissionSource();
  String networkHeaderSourceType();
  String networkHeaderRoadparts();
  String networkHeaderLength();
  String networkHeaderEmission();
  String labelTraffic(@Select VehicleType vehicleType);
  String valuePerTimeUnit(@Select TimeUnit timeunit, String amount);
  String labelPercentStagnated();
  String labelTunnelFactor();
  String labelRoadSpeed();
  String labelStrictEnforcement();
  String labelElevation();
  String labelRoadSideBarrier();
  String labelRoadSideBarrierValue();
  String labelBarrierDistance();
  String labelBarrierHeight();
  String labelBarrierType();
  String labelTotalEmission();
  String labelNoSourceInArea();
  String valueShortStrictEnforcement();

  // Plan sources
  String planName();
  String planSourceAddPlan();
  String planSourceAmount();
  String planEmissionsTitle();
  String planUnit(@Select CategoryUnit unit);

  // Mobile sources *
  String mobileEmissionsTitle();
  String mobileEmissionsOffRoadTitle();
  String mobileSourceAddVehicle();
  String mobileSourceFuelageUnit();
  String mobileSourceName();
  String mobileSourceEmptyOption();
  String mobileSourceFuelage();
  String mobileSourceVehiclesPerDay();
  String mobileSourceClass();
  String mobileSourceEmission();
  String mobileSourceStageCategory();
  String mobileSourceCustomCategory();
  String mobileSourceRoadType();

  // Mobile source emission calculator. *
  String offRoadEmissionCalculatorTitle();
  String offRoadEmissionCalculatorMachineryType();
  String offRoadEmissionCalculatorPower();
  String offRoadEmissionCalculatorLoad();
  String offRoadEmissionCalculatorUsage();
  String offRoadEmissionCalculatorEmissionFactor();
  String offRoadEmissionCalculatorFuelType();
  String offRoadEmissionCalculatorFuelUsage();
  String offRoadEmissionCalculatorResult();
  String offRoadEmissionCalculatorType(@Select String value);
  String offRoadEmissionCalculatorRunningHours();
  String offRoadEmissionCalculatorComputingBase();
  String offRoadEmissionCalculatorEfficiency();

  // Shipping sources *
  String shipDock();
  String shipRouteArrive();
  String shipRouteDepart();
  String shipRoute();
  String shipPanelTitle();
  String shipAdd();
  String shipName();
  String shipMovements();
  String shipNumberOfShips();
  String shipNumberOfShipsShort();
  String shipResidenceTime();
  String shipResidenceTimeUnit();
  String shipRouteShort();
  String shipMaritimeRouteShort();
  String shipSubmit();
  String shipType();
  String shipNew();
  String shipInlandRouteSelect();
  String shipMaritimeRouteSelect();
  String shipRouteName(String id);
  String shipMaritimeRouteName(String id);
  String shipRouteEditConfirm();
  String shipWaterway();
  String shipWaterwayEmptyOption();
  String shipWaterwayDirectionRequired();
  String shipWaterwayNotDetermined();
  String shipWaterwayRequired();
  String shipWaterwayDirectionLabel();
  String shipWaterwayNotAllowedCombination(String ship, String route);
  String shipWaterwayDirection(@Select WaterwayDirection direction);
  String shipWaterwayDirectionEmptyOption();

  String inlandShipAtoB();
  String inlandShipBtoA();
  String shipRouteEditTitle();
  String inlandShipPanelTitle();
  String inlandShipNumberOfShips();
  String inlandShipNumberOfShipsUnit();
  String inlandShipPercentageLaden();
  String inlandShipPercentageLadenUnit();
  String inlandShipDirection(@Select NavigationDirection direction);
  String inlandShipDirectionShort(@Select NavigationDirection direction);
  String inlandShipWaterwayDirection();


  // EmissionSourceViewers *
  String emissionSourceViewerSectorTitle(String sectorGroup, String name);
  String emissionSourceViewerCharacteristicsTitle();
  String emissionSourceViewerRoadTitle();
  String emissionSourceViewerGenericTitle();
  String emissionSourceViewerMobileTitle();
  String emissionSourceViewerMobileEmission();
  String emissionSourceViewerFarmAnimals();
  String emissionSourceViewerFarmEmission();
  String emissionSourceViewerFarmAnimalsUnit();
  String emissionSourceViewerShippingTitle();
  String emissionSourceViewerShippingWaterWayTitle(String waterway);
  String emissionSourceViewerShippingUnit();
}
