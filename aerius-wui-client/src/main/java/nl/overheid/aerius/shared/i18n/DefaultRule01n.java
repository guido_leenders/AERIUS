/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

import com.google.gwt.i18n.client.impl.plurals.DefaultRule;
import com.google.gwt.i18n.client.impl.plurals.DefaultRule_0_1_n;

/**
 *  Common plural rule for to add support for using PluralCount with the values: none, one and other.
 */
public class DefaultRule01n extends DefaultRule {

  @Override
  public PluralForm[] pluralForms() {
    return DefaultRule_0_1_n.pluralForms();
  }

  @Override
  public int select(final int n) {
    return DefaultRule_0_1_n.select(n);
  }
}
