/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.shared.i18n;

/**
 * Messages related to input validation.
 */
public interface InputValidationMessages {

  // Generic validation messages
  String ivRequiredInput(String label);
  String ivNoValidInput(String label, String input);
  String ivNotAnInteger(String label, String input);
  String ivNotAnFloat(String label, String input);
  String ivNotAPositiveNumber(String label);
  String ivRangeBetween(String label, int min, int max);
  String ivDoubleRangeBetween(String label, double min, double max);
  String ivStringRangeBetween(String label, String min, String max);
  String ivInvalidUID(String uuid);
  String ivInvalidProjectKey(String uuid);

  // Emission source validation
  String ivEmissionUseMorePointForPolygon();
  String ivEmissionUseMorePointForLine();
  String ivEmissionUseDifferentPointForLine();

  // Farm validation
  String ivFarmNoValidRavCode(String value);
  String ivFarmNoValidMeasure(String value);
  String ivFarmNoValidBWLCode();
  String ivFarmNotEmptyAmount();
  String ivFarmCustomNoEmptyDescription();
  String ivFarmInvalidFodder(String measure);
  String ivFarmEmptyFodder();

  // Plan validation
  String ivPlanNoEmptyName();

  // Road validation
  String ivRoadVehicleTypeNotSelected();
  String ivRoadStagnation(String val);
  String ivRoadMaxSpeed();
  String ivRoadCategoryNotSelected();
  String ivRoadNumberOfVehiclesNotEmpty();
  String ivRoadCustomDescriptionNotEmpty();

  // Shipping validation
  String ivShippingNoEmptyName();
  String ivShippingNoRoute();
  String ivShipMaritimeNumberDontMatch();
  String ivShippingNoValidCategory(String value);
  String ivShipNumberOfShipsNotEmpty();
  String ivShipResidenceTimeNotEmpty();

  // Inland shipping validation
  String ivInlandShipDirectionNotEmpty();
  String ivInlandShippingPercentageLaden(Integer value);
  String ivInlandShipMovementsNotEmpty();
  String ivInlandShipMovementsFromAToBNotEmpty();
  String ivInlandShipMovementsFromBToANotEmpty();

  // Off road validation
  String ivOffRoadEmissionCalculatorLoadValidator();
  String ivOffRoadEmissionCalculatorUsageValidator();
  String ivMobileSourceFuelageNotEmpty();
  String ivVehicleNoEmptyName();
  String ivHeatContentCalculatorTemperatureValidator(String temp);

  // Export validation
  String ivExportNoEmptyValue(String fieldName);
  String ivExportBoundaryOutOfRange(String maxRange);
  String ivExportInvalidEmailAddress(String email);

  // Misc.
  String ivCalculationMaxRangeLargerZero();
  String ivCalculationMaxRangeExceedsMax(int maxRange);
  String ivCalculationChoiceDuration();

  // Import register validation
  String ivRegisterApplicationTabAppDossierNumberRequired();
  String ivRegisterPriorityProjectDossierIdRequired();
}
