/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.deposition.CalculationPointList;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * Class to initialize a calculation job and insert the calculation base in the database.
 */
final class InitCalculation {

  private static final Logger LOG = LoggerFactory.getLogger(InitCalculation.class);

  private InitCalculation() {
    // Util class.
  }

  /**
   * Initializes a CalculationJob, inserts the calculation in the database.
   *
   * TODO Refactor {@link CalculationJob} to not have both a list of {@link JobCalculation} as well as {@link EmissionSourceList}s in
   * getCalculatedScenario(), which shadows one another. Get rid of initJobCalculations();
   *
   * @param con database connection
   * @param profileFactory
   * @param scenario scenario to calculate
   * @param queueName queue to put calculation on
   * @param calculatorOptions calculator options (if null the default worker options will be used)
   * @param customPointsInDatabase store custom points in the database. Not needed when output is CSV file
   * @param correlationId
   * @return new CalculationJob object
   * @throws SQLException database errors
   * @throws AeriusException aerius errors
   */
  public static CalculationJob initCalculation(final Connection con, final CalculatorProfileFactory profileFactory, final CalculatedScenario scenario,
      final String queueName, final CalculatorOptions calculatorOptions, final boolean customPointsInDatabase, final String correlationId)
          throws SQLException, AeriusException {
    LOG.info("Init calculation:{}", scenario.getMetaData());

    sanityCheck(calculatorOptions.getMaxConcurrentChunks(), scenario);
    sanityCheckOptions(scenario.getOptions());
    sanityCheck2(scenario, calculatorOptions.getMaxEngineSources());

    final CalculationJob calculationJob = new CalculationJob(UUID.randomUUID(), scenario, queueName);
    initCustomPoints(calculationJob, correlationId);
    fixSubstances(calculationJob.getCalculationSetOptions());
    calculationJob.setCalculatorOptions(calculatorOptions);
    // start inserting in database after sanity checks are not triggered.
    CalculationRepository.insertCalculations(con, scenario, customPointsInDatabase);
    initJobCalculations(calculationJob, profileFactory);
    return calculationJob;
  }

  /**
   * Initializes the jobCalculation objects.
   *
   * @param calculationJob
   * @param profileFactory
   */
  private static void initJobCalculations(final CalculationJob calculationJob, final CalculatorProfileFactory profileFactory) {
    for (final Calculation calculation : calculationJob.getCalculatedScenario().getCalculations()) {
      final Set<Sector> sectors = new HashSet<>();
      setSectors(sectors, profileFactory::mapToSector, calculation.getSources());
      calculationJob.getCalculations().add(new JobCalculation(calculation.getCalculationId(), sectors));
    }
  }

  private static void setSectors(final Set<Sector> sectors, final Function<EmissionSource, Sector> function, final List<EmissionSource> sources) {
    for (final EmissionSource src : sources) {
      if (src instanceof SRM2NetworkEmissionSource) {
        setSectors(sectors, function, ((SRM2NetworkEmissionSource) src).getEmissionSources());
      } else {
        sectors.add(function.apply(src));
      }
    }
  }

  /**
   * @param options
   */
  private static void fixSubstances(final CalculationSetOptions options) {
    // ensure stuff like NOXNH3 is taken care of.
    final ArrayList<EmissionValueKey> keys = EmissionValueKey.getEmissionValueKeys(0, options.getSubstances());
    options.getSubstances().clear();
    for (final EmissionValueKey key : keys) {
      options.getSubstances().add(key.getSubstance());
    }
    final Set<EmissionResultKey> resultKeys = EnumSet.noneOf(EmissionResultKey.class);
    for (final EmissionResultKey key : options.getEmissionResultKeys()) {
      resultKeys.addAll(key.hatch());
    }
    options.getEmissionResultKeys().clear();
    options.getEmissionResultKeys().addAll(resultKeys);
  }

  private static void sanityCheck(final int maxChunks, final CalculatedScenario scenario) throws AeriusException {
    if (maxChunks == 0) {
      LOG.error("Maximum number of current chunks is 0 in configuration.");
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
    for (final Calculation calculation : scenario.getCalculations()) {
      if (calculation.getSources().isEmpty()) {
        final String name = scenario.getMetaData().getProjectName();

        throw new AeriusException(Reason.CALCULATION_NO_SOURCES, name == null ? "" : name);
      }
    }
  }

  private static void sanityCheckOptions(final CalculationSetOptions calculationSetOptions) throws AeriusException {
    // extra sanity check for substance/emission result keys
    if (calculationSetOptions.getSubstances().isEmpty() || calculationSetOptions.getEmissionResultKeys().isEmpty()) {
      LOG.error("Substances and emissionResultKeys in options should both be filled. Substances {}, ERKS {}.", calculationSetOptions.getSubstances(),
          calculationSetOptions.getEmissionResultKeys());
      throw new AeriusException(Reason.INTERNAL_ERROR);
    }
  }

  /**
   * After initialization check if everything is ok.
   *
   * @param calculationJob
   * @param maxEngineSources
   * @throws AeriusException
   */
  private static void sanityCheck2(final CalculatedScenario calculatedScenario, final int maxEngineSources) throws AeriusException {
    final String projectName = calculatedScenario.getMetaData().getProjectName();
    final String safeProjectName = projectName == null || projectName.isEmpty() ? "" : projectName;
    for (final EmissionSourceList sources : calculatedScenario.getScenario().getSourceLists()) {
      if (sources.isEmpty()) {
        throw new AeriusException(Reason.CALCULATION_NO_SOURCES, safeProjectName);
      } else if (maxEngineSources > 0 && sources.size() > maxEngineSources) {
        throw new AeriusException(Reason.CALCULATION_TO_COMPLEX, safeProjectName);
      }
    }
  }

  private static void initCustomPoints(final CalculationJob calculationJob, final String correlationId) throws AeriusException {
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();
    final CalculationPointList calculationPoints = calculationJob.getCalculatedScenario().getCalculationPoints();

    if (options.getCalculationType() == CalculationType.CUSTOM_POINTS || options.getCalculationType() == CalculationType.NSL) {
      if (calculationPoints.isEmpty()) {
        throw new AeriusException(Reason.IMPORT_NO_CALCULATION_POINTS_PRESENT);
      }
      LOG.info("[correlationId:{}] Calculating user defined calculation points.", correlationId);
    } else {
      calculationPoints.clear();
    }
  }
}
