/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;

import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;

/**
 * Factory class to initialize {@link CalculatorOptions} with default values.
 */
final class CalculatorOptionsFactory {

  private CalculatorOptionsFactory() {
    // util
  }

  /**
   * @param con The connection to use when retrieving options for UI.
   * @param calculationSetOptions
   * @return Options for UI (webserver) to use when calculating concentrations/depositions.
   * @throws SQLException In case of database errors.
   */
  public static CalculatorOptions initUICalculatorOptions(final Connection con) throws SQLException {
    final CalculatorOptions calculatorOptions = getDefaultCalculatorOptions(con);
    calculatorOptions.setMaxConcurrentChunks(ConstantRepository.getInteger(con, ConstantsEnum.MAX_CHUNKS_CONCURRENT_WEBSERVER));
    calculatorOptions.setMaxCalculationEngineUnits(ConstantRepository.getInteger(con, ConstantsEnum.MAX_OPS_UNITS_UI));
    calculatorOptions.setMinReceptors(ConstantRepository.getInteger(con, ConstantsEnum.MIN_RECEPTORS_UI));
    calculatorOptions.setMaxReceptors(ConstantRepository.getInteger(con, ConstantsEnum.MAX_RECEPTORS_UI));
    calculatorOptions.setMaxEngineSources(ConstantRepository.getInteger(con, ConstantsEnum.MAX_ENGINE_SOURCES_UI));
    return calculatorOptions;
  }

  public static void limitMaxRadius(final Connection con, final CalculationSetOptions calculationSetOptions) throws SQLException {
    final Integer maxRadius = ConstantRepository.getNumber(con, SharedConstantsEnum.CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI, Integer.class);

    // initialize new calculation
    calculationSetOptions.setCalculateMaximumRange(Math.min(calculationSetOptions.getCalculateMaximumRange(), maxRadius));
  }

  /**
   * @param con The connection to use when querying the database.
   * @return Options for a worker to use when calculating concentrations/depositions.
   * @throws SQLException In case of database errors.
   */
  public static CalculatorOptions initWorkerCalculationOptions(final Connection con) throws SQLException {
    return initWorkerCalculationOptions(con,
        ConstantRepository.getInteger(con, ConstantsEnum.MAX_CONCURRENT_CHUNKS_WORKER),
        ConstantRepository.getInteger(con, ConstantsEnum.MAX_CALCULATION_ENGINE_UNITS_WORKER));
  }

  /**
   *
   * @param con The connection to use..
   * @param maxConcurrentChunks The maximum concurrent chunks to use.
   * @param maxCalculationEngineUnits The maximum Calculation Engine units to use for one chunk.
   * @return The options for a worker.
   * @throws SQLException In case of database errors.
   */
  public static CalculatorOptions initWorkerCalculationOptions(final Connection con, final int maxConcurrentChunks,
      final int maxCalculationEngineUnits) throws SQLException {
    final CalculatorOptions calculatorOptions = getDefaultCalculatorOptions(con);
    calculatorOptions.setMaxConcurrentChunks(maxConcurrentChunks);
    calculatorOptions.setMaxCalculationEngineUnits(maxCalculationEngineUnits);
    calculatorOptions.setMaxReceptors(ConstantRepository.getInteger(con, ConstantsEnum.MAX_RECEPTORS_WORKER));
    calculatorOptions.setMinReceptors(ConstantRepository.getInteger(con, ConstantsEnum.MIN_RECEPTORS_WORKER));
    return calculatorOptions;
  }

  private static CalculatorOptions getDefaultCalculatorOptions(final Connection con) throws SQLException {
    final CalculatorOptions calculatorOptions = new CalculatorOptions();
    calculatorOptions.setMaxChunkDelay(ConstantRepository.getNumber(con, SharedConstantsEnum.CALCULATION_MAX_DELAY_CHUNKS, Integer.class));
    calculatorOptions.setMinReceptorsChunkDelay(
        ConstantRepository.getNumber(con, SharedConstantsEnum.CALCULATION_MIN_RECEPTORS_FOR_DELAY, Integer.class));
    return calculatorOptions;
  }

}
