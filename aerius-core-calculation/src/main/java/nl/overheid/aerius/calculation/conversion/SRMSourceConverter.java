/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.util.List;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.srm2.conversion.EmissionSourceSRM2Converter;

/**
 * Class to implement to convert {@link EmissionSource} to SRM2RoadSegment objects.
 */
public class SRMSourceConverter {

  private static final EmissionSourceSRM2Converter SRM2_CONVERTER = new EmissionSourceSRM2Converter();

  public boolean convert(final List<EngineSource> expanded, final EmissionSource originalSource, final List<EmissionValueKey> keys)
      throws AeriusException {
    return expanded.addAll(SRM2_CONVERTER.convert(originalSource, keys));
  }
}
