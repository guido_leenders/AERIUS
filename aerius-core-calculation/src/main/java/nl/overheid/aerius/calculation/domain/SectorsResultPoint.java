/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;


import java.util.HashMap;
import java.util.Map.Entry;

import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;

/**
 * Data class storing results for all sectors at a single point
 */
class SectorsResultPoint extends AeriusPoint {

  private static final long serialVersionUID = -4836895135339197161L;

  private final HashMap<Integer, EmissionResults> sectorResults = new HashMap<>();

  /**
   * Creates a new point for the given point location
   * @param ap point location
   */
  public SectorsResultPoint(final AeriusPoint ap) {
    super(ap.getId(), ap.getPointType(), ap.getX(), ap.getY());
  }

  public AeriusResultPoint getTotalsResultPoint() {
    final AeriusResultPoint arp = new AeriusResultPoint(getId(), getPointType(), getX(), getY());
    for (final Entry<Integer, EmissionResults> sectorResult : sectorResults.entrySet()) {
      for (final Entry<EmissionResultKey, Double> emission : sectorResult.getValue().entrySet()) {
        arp.setEmissionResult(emission.getKey(), arp.getEmissionResult(emission.getKey()) + emission.getValue());
      }
    }
    return arp;
  }

  /**
   * Adds the results for the given sector. The caller should guarantee the results are actually for the location of this point
   * @param sectorId sector Id the results are for
   * @param emissionResults emission results for sector
   */
  public void put(final int sectorId, final EmissionResults emissionResults) {
    sectorResults.put(sectorId, emissionResults);
  }

  /**
   * Returns the number of sectors results are put in this object.
   * @return number of sectors  results are put in this object.
   */
  public int size() {
    return sectorResults.size();
  }

  @Override
  public String toString() {
    return "SectorsResultPoint [sectorResults=" + sectorResults + "]";
  }
}
