/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import nl.overheid.aerius.geo.shared.Point;

/**
 * Utility class that describes the properties of the RIVM++ square grid on top of the Netherlands.
 *
 * FIXME GridUtil javadoc is out of date: other ranges are used. It starts in the top-left over at 0,625000 on the RDNew coordinate system, stretches
 * for 300 kilometers to the east and (for our purposes) indefinitely to the south.
 *
 * There's 300 square 1km^2 cells in a single row, or 60 5km^2 cells, or 12 25km^2 cells.
 *
 * The actual/original grid supposedly reaches 280 kilometers in width, which is weird, because the 25 square kilometer cells don't fit nicely in
 * there. We've superimposed that grid with the one described here.
 */
public class GridUtil {

  private final GridSettings gridSettings;
  private final int xMin;
  private final int yMax;
  private final int squareHor;
  private final double minRibLength;
  private final int scaleMultiplier;

  public GridUtil(final GridSettings gridSettings) {
    this.gridSettings = gridSettings;
    xMin = gridSettings.getXMin();
    yMax = gridSettings.getYMax();
    squareHor = gridSettings.getSquareHor();
    minRibLength = gridSettings.getZoomLevelRibLengthMin();
    scaleMultiplier = gridSettings.getScaleMultiplier();
  }

  public GridSettings getGridSettings() {
    return gridSettings;
  }

  public int getCellFromPosition(final Point p, final GridZoomLevel zl) {
    final int posX = (p.getRoundedX() - xMin) / zl.getRibLength();
    final int posY = (yMax - p.getRoundedY()) / zl.getRibLength();
    return (posX + posY * zl.getHorizontal() * zl.getScale()) * zl.getScale() + 1;
  }

  public Point getPositionFromCell(final int cell) {
    final int x = (cell - 1) % squareHor;
    final int y = (cell - 1) / squareHor;

    return new Point(x * minRibLength, yMax - y * minRibLength);
  }

  public Point getPositionFromCell(final int cell, final GridZoomLevel level) {
    final int x = (cell - 1) % squareHor;
    final int y = (cell - 1) / squareHor;

    return new Point(x * minRibLength + level.getRibLength() / 2, yMax - y * minRibLength - level.getRibLength() / 2);
  }

  /**
   * Somewhat inefficient method of getting a surrounding filled circle for the given radius.
   *
   * Would like to use the Midpoint Circle Algorithm, but that doesn't fill, and it uses x/y, plus the below method will be integer cached.
   */
  public Collection<Integer> getSurrounding(final int id, final int radius, final GridZoomLevel level) {
    // Sanity check
    if (!checkIsValidCell(id, getGridSettings().levelFromLevel(level.getLevel()))) {
      throw new IllegalArgumentException("Given cell " + id + " is not valid at the given zoom level " + level.getLevel());
    }

    final double diameterRadius = radius == 0 ? 0 : (level.getDiameter() - radius % level.getDiameter());
    final int actualRadius = (int) Math.ceil((radius + diameterRadius) / level.getDiameter());

    final int scale = level.getScale();
    final int hor = level.getHorizontal();
    final int totHor = scale * hor;
    final int radiusSquared = (int) (actualRadius * 1.5 * actualRadius * 1.5);

    final HashSet<Integer> surrounding = new HashSet<>();
    final int gridX = (id - 1) % totHor;
    final int gridY = (id - 1) / totHor;
    for (int y = -actualRadius; y <= actualRadius; y++) {
      for (int x = -actualRadius; x <= actualRadius; x++) {
        if (x * x + y * y > radiusSquared) {
          continue;
        }

        final int targetX = gridX + x * scale;
        if (targetX < 0 || targetX >= totHor || gridY + y * scale < 0) {
          continue;
        }

        surrounding.add(id + x * scale + y * totHor * scale);
      }
    }

    return surrounding;
  }

  /**
   * Return the cells of the given zoom level that are within the cell atop the given zoom level
   */
  public Collection<Integer> getInnerCells(final int id, final GridZoomLevel level) {
    final ArrayList<Integer> lst = new ArrayList<>();

    final int scale = level.getScale();
    final int totHor = scale * level.getHorizontal();

    for (int y = 0; y < scaleMultiplier; y++) {
      final int lineY = y * totHor * scale;
      for (int x = 0; x < scaleMultiplier; x++) {
        lst.add(id + lineY + x * scale);
      }
    }

    return lst;
  }

  public boolean checkIsValidCell(final int cell, final GridZoomLevel level) {
    final Point positionFromCell = getPositionFromCell(cell);
    final int snapped = getCellFromPosition(positionFromCell, level);

    return cell == snapped;
  }

  public boolean isFullyEncapsulated(final int target, final int center, final long distance, final GridZoomLevel targetLevel,
      final GridZoomLevel centerLevel) {
    return getMaxDistance(target, center, targetLevel, centerLevel) < distance;
  }

  public boolean isFullyOutside(final int target, final int center, final long distance, final GridZoomLevel targetLevel,
      final GridZoomLevel centerLevel) {
    return getMinDistance(target, center, targetLevel, centerLevel) >= distance;
  }

  public double getMaxDistance(final int a, final int b, final GridZoomLevel aLevel, final GridZoomLevel bLevel) {
    final int aMinX = (a - 1) % squareHor;
    final int aMinY = (a - 1) / squareHor;
    final int aMaxX = aMinX + aLevel.getScale();
    final int aMaxY = aMinY + aLevel.getScale();

    final int bMinX = (b - 1) % squareHor;
    final int bMinY = (b - 1) / squareHor;
    final int bMaxX = bMinX + bLevel.getScale();
    final int bMaxY = bMinY + bLevel.getScale();

    final long disX = aMaxX <= bMaxX && aMinX >= bMinX ? 0 : Math.max(Math.abs(aMinX - bMinX), Math.abs(aMaxX - bMaxX));
    final long disY = aMaxY <= bMaxY && aMinY >= bMinY ? 0 : Math.max(Math.abs(aMinY - bMinY), Math.abs(aMaxY - bMaxY));

    return Math.sqrt(disX * disX + disY * disY) * minRibLength;
  }

  public double getMinDistance(final int a, final int b, final GridZoomLevel aLevel, final GridZoomLevel bLevel) {
    final int aMinX = (a - 1) % squareHor;
    final int aMinY = (a - 1) / squareHor;
    final int aMaxX = aMinX + aLevel.getScale();
    final int aMaxY = aMinY + aLevel.getScale();

    final int bMinX = (b - 1) % squareHor;
    final int bMinY = (b - 1) / squareHor;
    final int bMaxX = bMinX + bLevel.getScale();
    final int bMaxY = bMinY + bLevel.getScale();

    final long disX = aMaxX <= bMaxX && aMinX >= bMinX ? 0 : Math.min(Math.abs(aMaxX - bMinX), Math.abs(bMaxX - aMinX));
    final long disY = aMaxY <= bMaxY && aMinY >= bMinY ? 0 : Math.min(Math.abs(aMaxY - bMinY), Math.abs(bMaxY - aMinY));

    return Math.sqrt(disX * disX + disY * disY) * minRibLength;
  }
}
