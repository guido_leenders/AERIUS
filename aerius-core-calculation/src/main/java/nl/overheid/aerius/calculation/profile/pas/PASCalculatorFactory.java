/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.UnitAccumulatorTaskHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.base.EngineSourceAggregator;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.base.WorkBySectorHandlerImpl;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.OPSCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.grid.AggregationDistanceProfilePicker;
import nl.overheid.aerius.calculation.grid.GridEmissionSourceDigestor;
import nl.overheid.aerius.calculation.grid.GridEngineSourceAggregatable;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.ResearchAreaCalculationScenario;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.geo.ReceptorGridSettings;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * {@link CalculatorProfileFactory} implementation for PAS Wet-NB calculations.
 */
public class PASCalculatorFactory implements CalculatorProfileFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(PASCalculatorFactory.class);

  private static final OPSCalculationEngineProvider OPS_CALCULATION_ENGINE_PROVIDER = new OPSCalculationEngineProvider();
  private static final DefaultCalculationEngineProvider DEFAULT_CALCULATION_ENGINE_PROVIDER = new DefaultCalculationEngineProvider();
  private static final IncludeResultsFilter NO_FILTER = resultPoint -> true;
  private static final WorkHandler<AeriusPoint> SKIP_OUTSIDE_RANGE_RECEPTORS = (j, c, cId, cpt) -> {};

  private final PMF pmf;
  private final EngineSourceAggregator<EngineSource, Substance> aggregator;
  private final GridUtil gridUtil;
  private final GridSettings gridSettings;
  private final Natura2kGridPointStore n2kPointStore;
  private final PASCalculationTaskFactory calculationTaskFactory;
  private final IncludeResultsFilter pasCalculationRadiusResultFilter;
  private final IncludeResultsFilter pasIncludeResultsFilter;
  /**
   * Recalculated the receptor id given the x-y coordinates in case a point is a receptor point.
   *
   * @param results list of result points.
   */
  private final ResultPostProcessHandler receptorPointResultsHandler;

  public PASCalculatorFactory(final PMF pmf) throws SQLException {
    this.pmf = pmf;
    final ReceptorGridSettings rgs = ReceptorGridSettingsRepository.getReceptorGridSettings(pmf);
    final ReceptorUtil receptorUtil = new ReceptorUtil(rgs);

    gridSettings = new GridSettings(rgs);
    gridUtil = new GridUtil(gridSettings);
    aggregator = new EngineSourceAggregator<>(new GridEngineSourceAggregatable(rgs.getEpsg().getSrid()));
    n2kPointStore = loadNatura2kGridPointStore(pmf, receptorUtil);
    calculationTaskFactory = new PASCalculationTaskFactory(rgs);
    pasIncludeResultsFilter = new RelevantResultsFilter(ConstantRepository.getDouble(pmf, ConstantsEnum.PRONOUNCEMENT_THRESHOLD_VALUE));
    // For PAS radius allow all positive results.
    pasCalculationRadiusResultFilter = new RelevantResultsFilter(0.0);
    receptorPointResultsHandler = results -> {
      for (final AeriusResultPoint arp : results) {
        if (arp.getPointType() == AeriusPointType.RECEPTOR) {
          // This looks stupid, but an x-y coordinate for exact center of hexagon has a resolution that is bigger then 1 meter
          // While i.e. OPS has resolution of 1 meter, it means the x-y coordinates back from ops have lost detail information.
          // Therefore first calculate id and then set the exact x-y coordinate for the id.
          receptorUtil.setAeriusPointFromId(receptorUtil.setReceptorIdFromPoint(arp));
        }
      }
    };
  }

  protected Natura2kGridPointStore loadNatura2kGridPointStore(final PMF pmf, final ReceptorUtil receptorUtil) throws SQLException {
    final ReceptorLoader webLoader = new ReceptorLoader(receptorUtil, gridSettings);
    try (Connection con = pmf.getConnection()) {
      return webLoader.fillPointStore(con);
    }
  }

  @Override
  public void prepareOptions(final CalculatedScenario scenario) {
    final CalculationSetOptions options = scenario.getOptions();

    if (options.getCalculationType() == CalculationType.PAS) {
      scenario.setOptions(PASUtil.getCalculationSetOptions(options.getTemporaryProjectYears(), options.getPermitCalculationRadiusType()));
      scenario.getCalculationPoints().clear();
    }
  }

  @Override
  public CalculationEngineProvider getProvider(final CalculationJob calculationJob) {
    return calculationJob.getCalculationSetOptions().isRoadOPS() ? OPS_CALCULATION_ENGINE_PROVIDER : DEFAULT_CALCULATION_ENGINE_PROVIDER;
  }

  @Override
  public SourceConverter getSourceConverter(final Connection con, final CalculationJob calculationJob) throws SQLException {
    return new PASGeometryExpander(con, calculationJob.getProvider());
  }

  @Override
  public Sector mapToSector(final EmissionSource source) {
    return source.getSector();
  }

  @Override
  public List<CalculationResultHandler> getCalculationResultHandler(final CalculationJob calculationJob) {
    final CalculationResultHandler handler = new CalculationResultHandler() {
      @Override
      public void onFinish(final CalculationState state) {
        PASUtil.deleteLowResults(pmf, calculationJob);
        PASUtil.insertCalculationDevelopmentSpaceDemands(pmf, calculationJob);
      }
    };
    return Collections.singletonList(handler);
  }

  @Override
  public void digest(final CalculationJob calculationJob, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final AggregationDistanceProfilePicker aggregationProfilePicker = new AggregationDistanceProfilePicker();
    final GridEmissionSourceDigestor sourceDigestor = new GridEmissionSourceDigestor(pmf, gridUtil, aggregator, aggregationProfilePicker);
    sourceDigestor.digest(calculationJob.getGeometryExpander(), calculationJob, keys);
  }

  @Override
  public WorkHandler<AeriusPoint> createWorkHandler(final CalculationJob calculationJob, final CalculationTaskHandler remoteWorkHandler,
      final ResultHandler resultHandler) throws AeriusException {
    final CalculationSetOptions calculationSetOptions = calculationJob.getCalculationSetOptions();
    final CalculationTaskHandler accumulatorHandler = calculationJob.isUnitsAccumulator() ? new UnitAccumulatorTaskHandler(calculationJob,
        remoteWorkHandler) : remoteWorkHandler;
    final WorkHandler<AeriusPoint> workHandlerImpl =
        new WorkBySectorHandlerImpl(calculationJob.getProvider(), calculationTaskFactory, wrapIfRunWithRoadOPS(calculationJob, accumulatorHandler));

    setIfTemporaryProject(calculationJob, resultHandler);

    if (calculationJob.getCalculatedScenario() instanceof ResearchAreaCalculationScenario) {
      final int radius = getRadius(calculationSetOptions);

      return new RadiusFilteredWorkHandler(workHandlerImpl, SKIP_OUTSIDE_RANGE_RECEPTORS,  calculationJob.getResearchAreaTree(), radius);
    } else if (calculationSetOptions.isPASPriorityProject()) {
      final int radius = getRadius(calculationSetOptions);

      return new RadiusFilteredWorkHandler(workHandlerImpl, new OutsideRadiusWorkHandler(resultHandler), calculationJob.getTree(), radius);
    } else {
      return workHandlerImpl;
    }
  }

  private int getRadius(final CalculationSetOptions calculationSetOptions) throws AeriusException {
    if (calculationSetOptions.getPermitCalculationRadiusType() == null) {
      throw new AeriusException(Reason.RESEARCH_AREA_NO_RADIUS);
    }
    return calculationSetOptions.getPermitCalculationRadiusType().getRadius();
  }

  /**
   * Results < melding threshold for permit calculations should be ignored.
   * @param calculationType filter based on calculation type
   * @param permitCalculationRadiusType filter based on radiusType if supplied
   * @return
   */
  @Override
  public IncludeResultsFilter getIncludeResultFilter(final CalculationSetOptions calculationSetOptions) {
    if (calculationSetOptions.getPermitCalculationRadiusType() == null) {
      return CalculationType.PAS == calculationSetOptions.getCalculationType() ? pasIncludeResultsFilter : NO_FILTER;
    } else {
      return pasCalculationRadiusResultFilter;
    }
  }

  /**
   * If this is temporary project calculation set the TemporaryProjectResultsHandler to adjust the result according to the temporary project
   * regulation.
   *
   * @param calculationJob calculation job to set
   * @param resultHandler
   */
  private void setIfTemporaryProject(final CalculationJob calculationJob, final ResultHandler resultHandler) {
    final CalculationSetOptions options = calculationJob.getCalculationSetOptions();

    resultHandler.addResultUpdateHandler(receptorPointResultsHandler);
    if (options.isTemporaryProjectImpact()) {
      resultHandler.addResultUpdateHandler(new TemporaryProjectResultsHandler(options.getTemporaryProjectYears()));
    }
  }

  /**
   * If the calculation should be done with roadOPS option the {@link OpsRoadCalculationTaskHandler} is wrapped around the passed handler.
   *
   * @param calculationJob
   * @param handlerToWrap
   * @return
   * @throws AeriusException
   */
  private CalculationTaskHandler wrapIfRunWithRoadOPS(final CalculationJob calculationJob, final CalculationTaskHandler handlerToWrap)
      throws AeriusException {
    final CalculationTaskHandler possibleRoad;

    if (calculationJob.getCalculationSetOptions().isRoadOPS()) {
      possibleRoad = new OpsRoadCalculationTaskHandler(calculationJob.getTree(), handlerToWrap);
    } else {
      possibleRoad = handlerToWrap;
    }
    return possibleRoad;
  }

  @Override
  public WorkDistributor<AeriusPoint> createWorkDistributor(final CalculationJob calculationJob) throws AeriusException, SQLException {
    final WorkDistributor<AeriusPoint> wd;
    final CalculationType ct = calculationJob.getCalculationSetOptions().getCalculationType();

    if (calculationJob.getCalculatedScenario() instanceof ResearchAreaCalculationScenario) {
      wd = new ResearchAreaWorkDistributor(n2kPointStore);
    } else {
      switch (ct) {
      case CUSTOM_POINTS:
        wd = new CustomPointsWorkDistributor(pmf, gridSettings, calculationJob.getCalculatedScenario().getCalculationPoints());
        break;
      case PAS:
        wd = new PASWorkDistributor(pmf, gridUtil, n2kPointStore);
        break;
      case NATURE_AREA:
      case RADIUS:
      default:
        LOGGER.error("Unsupported calculation type:'{}'", ct);
        throw new AeriusException(Reason.INTERNAL_ERROR);
      }
    }
    return wd;
  }
}
