/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.ProductType;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedComparison;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.calculation.NBWetCalculationUtil;
import nl.overheid.aerius.shared.domain.calculation.PermitCalculationRadiusType;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Util class for calculating to the PAS.
 */
public final class PASUtil {

  private static final Logger LOG = LoggerFactory.getLogger(PASUtil.class);

  private PASUtil() {
    // util
  }

  /**
   * Delete result below the threshold value if the calculation is a NBwet calculation.
   * @param pmf database manager
   * @param calculatorJob job to delete results for.
   */
  public static void deleteLowResults(final PMF pmf, final CalculationJob calculatorJob) {
    if (calculatorJob.getCalculationSetOptions().getCalculationType() == CalculationType.PAS) {
      try (final Connection con = pmf.getConnection()) {
        CalculationRepository.deleteLowCalculationResults(con,
            NBWetCalculationUtil.getProposedCalculationId(calculatorJob.getCalculatedScenario()),
            NBWetCalculationUtil.getCurrentCalculationId(calculatorJob.getCalculatedScenario()),
            !calculatorJob.getCalculationSetOptions().isPermitCalculationRadiusImpact());
      } catch (final SQLException e) {
        // suppress error
        LOG.error("deleteLowResults failed", e);
      }
    }
  }

  /**
   * Save results under threshold supply if we have a calculationRadiusType.
   * @param pmf database manager.
   * @param calculatorJob job to get calculation id and the radius type.
   */
  public static void insertCalculationDevelopmentSpaceDemands(final PMF pmf, final CalculationJob calculatorJob) {
    try (final Connection con = pmf.getConnection()) {
      CalculationDevelopmentSpaceRepository.insertCalculationDemands(con,
          NBWetCalculationUtil.getProposedCalculationId(calculatorJob.getCalculatedScenario()),
          NBWetCalculationUtil.getCurrentCalculationId(calculatorJob.getCalculatedScenario()),
          !calculatorJob.getCalculationSetOptions().isPermitCalculationRadiusImpact());
      // write extra result for ui option to switch senarios
      if (pmf.getProductType() == ProductType.CALCULATOR && calculatorJob.getCalculatedScenario() instanceof CalculatedComparison) {
        CalculationDevelopmentSpaceRepository.insertCalculationDemands(con,
            NBWetCalculationUtil.getCurrentCalculationId(calculatorJob.getCalculatedScenario()),
            NBWetCalculationUtil.getProposedCalculationId(calculatorJob.getCalculatedScenario()),
            !calculatorJob.getCalculationSetOptions().isPermitCalculationRadiusImpact());
      }
    } catch (final SQLException e) {
      // suppress error
      LOG.error("insert developmentspace demands failed", e);
    }
  }

  /**
   * Returns a {@link CalculationSetOptions} object configured to do a Wet natuur bescherming calculation.
   * @param temporaryProjectYears for temporary projects this should be the period, else null
   * @param permitCalculationRadiusType for priority projects with different calculation distance.
   * @return CalculationSetOptions object.
   */
  public static CalculationSetOptions getCalculationSetOptions(final Integer temporaryProjectYears,
      final PermitCalculationRadiusType permitCalculationRadiusType) {
    final CalculationSetOptions options = new CalculationSetOptions();
    options.getSubstances().add(Substance.NH3);
    options.getSubstances().add(Substance.NOX);
    options.getSubstances().add(Substance.NO2);
    options.getEmissionResultKeys().add(EmissionResultKey.NH3_DEPOSITION);
    options.getEmissionResultKeys().add(EmissionResultKey.NOX_DEPOSITION);
    options.setCalculationType(CalculationType.PAS);
    options.setTemporaryProjectYears(temporaryProjectYears);
    options.setPermitCalculationRadiusType(permitCalculationRadiusType);
    return options;
  }
}
