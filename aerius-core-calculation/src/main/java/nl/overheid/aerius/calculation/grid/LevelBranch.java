/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import java.util.ArrayList;

public class LevelBranch<L, E> extends ArrayList<LevelBranch<L, E>> {
  private static final long serialVersionUID = 4662487604287128913L;

  private final E element;
  private final L level;

  public LevelBranch(final L level) {
    this(level, null);
  }

  public LevelBranch(final L level, final E element) {
    this.level = level;
    this.element = element;
  }

  public boolean isLeaf() {
    return isEmpty();
  }

  public E getElement() {
    return element;
  }

  public L getLevel() {
    return level;
  }

  @Override
  public String toString() {
    return "LevelBranch [element=" + element + ", level=" + level + "]";
  }
}
