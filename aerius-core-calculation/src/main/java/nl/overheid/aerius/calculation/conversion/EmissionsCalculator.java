/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceCollection;
import nl.overheid.aerius.shared.domain.source.EmissionSubSource;
import nl.overheid.aerius.shared.domain.source.HasEmissionValues;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Class to determine emissions for the misc. objects.
 */
public final class EmissionsCalculator {

  private EmissionsCalculator() {
  }

  /**
   * @param con Database connection.
   * @param emissionSubSource The emissionvalues to get the total emissions for.
   * @param geometry The Geometry of the source.
   * @param keys emission keys
   * @param parentSource the emission source the subsource is part of
   * @return The total emissions for these emissionvalues.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public static EmissionValues getEmissions(final Connection con, final HasEmissionValues emissionSubSource,
      final WKTGeometry geometry, final List<EmissionValueKey> keys, final EmissionSource parentSource) throws SQLException, AeriusException {
    final EmissionValues emissions;
    if (emissionSubSource instanceof MooringMaritimeVesselGroup) {
      emissions = MaritimeShipEmissionCalculator.getEmissions(con, (MooringMaritimeVesselGroup) emissionSubSource, keys);
    } else if (emissionSubSource instanceof RouteMaritimeVesselGroup) {
      emissions = MaritimeShipEmissionCalculator.getEmissions(con, (RouteMaritimeVesselGroup) emissionSubSource, geometry, keys);
    } else if (emissionSubSource instanceof RouteInlandVesselGroup && parentSource instanceof InlandRouteEmissionSource) {
      emissions = InlandShipEmissionCalculator.getEmissions(con, (RouteInlandVesselGroup) emissionSubSource, geometry, keys,
          (InlandRouteEmissionSource) parentSource);
    } else if (emissionSubSource instanceof InlandMooringVesselGroup) {
      emissions = InlandShipEmissionCalculator.getEmissions(con, (InlandMooringVesselGroup) emissionSubSource, keys);
    } else {
      emissions = emissionSubSource.getEmissionValues().copy();
    }
    return emissions;
  }

  /**
   * Set the total emissions for all sources in supplied list.
   * Use this call server-side to ensure received objects have the right emissions set.
   *
   * @param con Connection to use for database operations.
   * @param emissionSources The sources to set total emissions for.
   * @param keys emission keys
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public static void setEmissions(final Connection con, final List<EmissionSource> emissionSources, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    for (final EmissionSource emissionSource : emissionSources) {
      setEmissions(con, emissionSource, emissionSource.getGeometry(), keys, null);
    }
  }

  private static void setEmissions(final Connection con, final Object object, final WKTGeometry geometry, final List<EmissionValueKey> keys,
      final EmissionSource parent) throws SQLException, AeriusException {
    if (object instanceof HasEmissionValues) {
      ((HasEmissionValues) object).setEmissionValues(getEmissions(con, (HasEmissionValues) object, geometry, keys, parent));
    }
    if (object instanceof EmissionSourceCollection<?>) {
      final EmissionSourceCollection<?> nextParent = (EmissionSourceCollection<?>) object;
      for (final EmissionSubSource emissionValuesChild : nextParent.getEmissionSubSources()) {
        if (emissionValuesChild instanceof HasEmissionValues) {
          setEmissions(con, emissionValuesChild, geometry, keys, nextParent);
        }
      }
    }
  }
}
