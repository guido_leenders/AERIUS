/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Implements the Nature Conservation Act rules for calculation deposition for temporary projects. The rule is:
 * <pre>
 *   project_deposition = (pas_period_duration * deposition) / number_of_years_project
 *   where:
 *     project_deposition = the deposition value that should be used.
 *     pas_period_duration = the duration of a single PAS period, which is 6 years.
 *     deposition = the deposition calculated.
 *     number_of_years_project = duration of the project in years.
 * </pre>
 * The maximum duration of a project should be less or equal to the PAS duration period. If it's longer it's unspecified.
 */
class TemporaryProjectResultsHandler implements ResultPostProcessHandler {

  private final double temporaryYearsCorrection;

  /**
   * Constructor with number of years.
   * @param temporaryYears number of years
   */
  public TemporaryProjectResultsHandler(final int temporaryYears) {
    throw new IllegalArgumentException("Temporary projects are not supported.");
  }

  /**
   * Implementation has been preserved in case of future revival.
   *
   * Updates the results to conform to the regulation related to temporary projects.
   * @param result results to update
   */
  @Override
  public void postProcessSectorResults(final List<AeriusResultPoint> points) {
    for (final AeriusResultPoint point : points) {
      for (final Entry<EmissionResultKey, Double> entry : point.getEmissionResults().entrySet()) {
        point.getEmissionResults().put(entry.getKey(), entry.getValue() * temporaryYearsCorrection);
      }
    }
  }
}
