/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.domain.CalculationInputData;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.profile.pas.PASCalculatorFactory;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.export.ExportType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.BrokerConnectionFactory;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.mq.RabbitMQQueueMonitor;

/**
 * Director class that constructs a complete calculator object. This class should be instantiated only once preferable.
 *
 * Director constructs a calculator object that is completely initialized. Then the calculator can be started which is a blocking process.
 */
public class CalculatorBuildDirector {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorBuildDirector.class);

  private final ExecutorService executor = Executors.newCachedThreadPool();
  private final PMF pmf;
  private final RabbitMQQueueMonitor rabbitMQQueueMonitor;
  private final TaskManagerClient taskManagerClient;
//  private final CalculatorProfileFactory nslCalculatorProfileFactory;
  private final CalculatorProfileFactory pasCalculatorProfileFactory;

  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory) throws SQLException {
    this(pmf, factory, false);
  }

  /**
   * Constructor to be used for testing only!!! It doesn't start the QueueMonitor if test is true.
   *
   * @param pmf
   * @param factory
   * @param test if true runs in test mode.
   * @throws SQLException
   */
  public CalculatorBuildDirector(final PMF pmf, final BrokerConnectionFactory factory, final boolean test)
      throws SQLException {
    this.pmf = pmf;
//    nslCalculatorProfileFactory = new NSLCalculatorFactory(pmf);
    pasCalculatorProfileFactory = new PASCalculatorFactory(pmf);
    taskManagerClient = new TaskManagerClient(factory);
    this.rabbitMQQueueMonitor = new RabbitMQQueueMonitor(factory.getConnectionConfiguration());
    if (!test) {
      executor.execute(rabbitMQQueueMonitor);
    }
  }

  public void shutdown() {
    try {
      rabbitMQQueueMonitor.shutdown();
    } finally {
      executor.shutdown();
    }
  }

  /**
   * Constructs a {@link Calculator} object.
   * @param inputData
   * @param correlationId
   * @param intermediateResultCallback
   * @return
   * @throws AeriusException
   * @throws SQLException
   */
  public Calculator construct(final CalculationInputData inputData, final String correlationId,
      final WorkerIntermediateResultSender intermediateResultCallback) throws AeriusException, SQLException {
    final CalculatorBuilder builder =
        init(inputData, correlationId)
        .setCalculationResultHandler(intermediateResultCallback != null, inputData.isSectorOutput(), inputData.isCsvOutput())
        .setIntermediateHandler(intermediateResultCallback);

    if (inputData.isTrackJobProcess()) {
      builder.setTrackJobHandler();
    }
    return builder.build(rabbitMQQueueMonitor, taskManagerClient);
  }

  /**
   * Initializes a CalculationJob, inserts the calculation in the database.
   */
  private CalculatorBuilder init(final CalculationInputData inputData, final String correlationId) throws AeriusException {
    LOGGER.info("[correlationId:{}] Init calculation.", correlationId);
    final CalculatorProfileFactory profileFactory = getCalculatorProfileFactory(inputData.getScenario().getOptions());
    final CalculatedScenario scenario = inputData.getScenario();

    profileFactory.prepareOptions(scenario);
    try (final Connection con = pmf.getConnection()) {
      final CalculatorOptions calculatorOptions = getCalculatorOptions(con, inputData.getExportType() == ExportType.CALCULATION_UI);
      final CalculationJob calculationJob = InitCalculation.initCalculation(con, profileFactory, scenario, inputData.getQueueName(),
          calculatorOptions, !inputData.isCsvOutput(), correlationId);

      calculationJob.setName(inputData.getName());
      calculationJob.setProvider(profileFactory.getProvider(calculationJob));
      calculationJob.setGeometryExpander(profileFactory.getSourceConverter(con, calculationJob));

      return new CalculatorBuilder(pmf, executor, profileFactory, calculationJob, correlationId);
    } catch (final SQLException e) {
      LOGGER.error("Error while trying to init calculation", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  protected CalculatorOptions getCalculatorOptions(final Connection con, final boolean calculatorOptionsUI) throws SQLException {
    return calculatorOptionsUI ? CalculatorOptionsFactory.initUICalculatorOptions(con) : CalculatorOptionsFactory.initWorkerCalculationOptions(con);
  }

  protected CalculatorProfileFactory getCalculatorProfileFactory(final CalculationSetOptions calculationSetOptions) {
    if (calculationSetOptions.getCalculationType() == CalculationType.NSL) {
      return null;//nslCalculatorProfileFactory;
    } else {
      return pasCalculatorProfileFactory;
    }
  }
}
