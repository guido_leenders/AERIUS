/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationInitResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;

/**
 * Calculator object to run the calculation. The Calculator is prepared with the {@link CalculatorBuilder} class via the
 * {@link CalculatorBuildDirector}. This class contains the running process that controls the calculation.
 */
public final class Calculator {

  private static final Logger LOGGER = LoggerFactory.getLogger(Calculator.class);

  private final PMF pmf;
  private final ExecutorService executor;

  private final CalculationJob calculationJob;
  private final String correlationId;
  private final MultipleCalculationResultHandler handler = new MultipleCalculationResultHandler();
  private final WorkTracker tracker = new WorkTrackerImpl();

  private IntermediateHandlerProcessor processor;
  private WorkerIntermediateResultSender intermediateResultCallback;
  private CalculationJobWorkHandler<AeriusPoint> workHandler;
  private WorkDistributor<AeriusPoint> workDistributor;


  Calculator(final PMF pmf, final ExecutorService executor, final CalculationJob calculationJob, final String correlationId) {
    this.pmf = pmf;
    this.executor = executor;
    this.calculationJob = calculationJob;
    this.correlationId = correlationId;
  }

  public <T extends CalculationResultHandler> T addCalculationResultHandler(final T resultHandler) {
    return handler.addHandler(resultHandler);
  }

  void addIntermediateResultSender(final WorkerIntermediateResultSender resultCallback) {
    this.intermediateResultCallback = resultCallback;
    processor = resultCallback == null ? null : new IntermediateHandlerProcessor(resultCallback);
  }

  void addIntermediateHandler(final IntermediateResultHandler handler) {
    if (processor == null) {
      throw new IllegalArgumentException("Trying to add handler '" +  handler.getClass() + "', but no processer initialized.");
    }
    processor.add(handler);
  }

  MultipleCalculationResultHandler getHandler() {
    return handler;
  }

  WorkTracker getTracker() {
    return tracker;
  }

  void setWorkHandler(final CalculationJobWorkHandler<AeriusPoint> workHandler) {
    this.workHandler = workHandler;
  }

  void setWorkDistributor(final WorkDistributor<AeriusPoint> workDistributor) {
    this.workDistributor = workDistributor;
  }

  private Future<?> runOptionalIntermediateHandler() throws IOException {
    Future<?> intermediateFuture = null;

    if (processor != null) {
      handler.addHandler(processor);
      intermediateFuture = executor.submit(processor);
      sendInit(intermediateResultCallback);
    }
    return intermediateFuture;
  }

  /**
   * Starts a calculation and blocks until it is finished.
   *
   * @param calculationJob the job to calculate
   * @param calculationPoints optional set of points, used when providing a custom set of points to calculate
   * @param handler handler that processes the returned results.
   * @throws Exception
   */
  public CalculationJob calculate() throws Exception {
    final Future<?> intermediateFuture = runOptionalIntermediateHandler();
    try {
      tracker.start();
      updateCalculationState(tracker.getState());
      workDistributor.distribute(calculationJob, workHandler);

      waitForCompletion();
    } catch (final TaskCancelledException e) {
      LOGGER.error("Calculation TaskCancelledException", e);
      throw (Exception) e.getCause();
    } catch (final Exception e) {
      LOGGER.error("Calculation cancelled:", e);
      throw e;
    } finally {
      LOGGER.info("[correlationId:{}] Finished calculation.", correlationId);
      updateCalculationState(tracker.getState());
      handler.onFinish(tracker.getState());
      if (intermediateFuture != null) {
        intermediateFuture.get();
      }
    }
    return calculationJob;
  }

  private void waitForCompletion() throws InterruptedException, Exception {
    final CalculationState state = tracker.waitForCompletion();

    if (state == CalculationState.CANCELLED) {
      throw tracker.getException();
    }
  }

  private void sendInit(final WorkerIntermediateResultSender sender) throws IOException {
    if (sender != null) {
      final CalculatedScenario calculatedScenario = calculationJob.getCalculatedScenario();

      sender.sendIntermediateResult(new CalculationInitResult(calculatedScenario.getOptions(), calculatedScenario.getCalculations()));
    }
  }

  private void updateCalculationState(final CalculationState state) {
    try (final Connection con = pmf.getConnection()) {
      for (final JobCalculation calculation : calculationJob.getCalculations()) {
        CalculationRepository.updateCalculationState(con, calculation.getCalculationId(), state);
      }
    } catch (final SQLException e) {
      LOGGER.error("Updating calculation state of job {} in database failed:", calculationJob.getWorkId(), e);
    }
  }
}
