/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Map.Entry;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.common.NBWetRepository;
import nl.overheid.aerius.db.common.NBWetRepository.AResult;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.NBWetCalculationUtil;

/**
 * Keeps the state of a Wet NatuurBeschreming calculation.
 * The calculation continues until the distance to the last complete calculated area and the first area that was completely below the threshold value
 * is at least {@link #MINIMUM_CALCULATION_DISTANCE}. If a nature area is found to be above threshold, and an earlier area was below the threshold
 * the distance counting is reset until a new area is found to be below the threshold value.
 */
class ChunkState {
  private static final Logger LOG = LoggerFactory.getLogger(ChunkState.class);

  private static final int MINIMUM_CALCULATION_DISTANCE = 10000;
  private static final int MINIMUM_AREAS_CALCULATED = 5;

  private final Queue<AreaData> areasSizes = new ArrayDeque<>();
  private final NBWetRepository rr;
  private final CalculationJob calculationJob;
  private final double meldingThresholdValue;
  /**
   * Distance to the area that was first found to have results below the threshold.
   */
  private int firstBelowThresholdDistance = -1;
  private int areasCalculatedBelowThreshold;

  private AssessmentAreaCompleteHandler completeHandler;

  public ChunkState(final CalculationJob calculationJob, final List<Entry<Integer, Integer>> listAreas, final NBWetRepository rr,
      final double meldingThresholdValue) {
    this.calculationJob = calculationJob;
    this.rr = rr;
    this.meldingThresholdValue = meldingThresholdValue;
  }

  public void addAreaCompleteHandler(final AssessmentAreaCompleteHandler completeHandler) {
    this.completeHandler = completeHandler;
  }

  /**
   * Add the given natura 2000 to the list of areas to calculate.
   * @param n2kId id of the natura 2000 area
   * @param size number of point to calculate for the given area
   * @param distance minimal distance to the sources.
   */
  public void addN2KArea(final Integer n2kId, final int size, final int distance) {
    areasSizes.add(new AreaData(n2kId, size, distance));
  }

  /**
   * Returns true as long as there are natura 2000 areas to calculate.
   * @param con database connection
   * @return
   * @throws SQLException
   */
  public boolean resultsPending(final Connection con) throws SQLException {
    final boolean pending;
    final AreaData as = areasSizes.peek();
    final String workId = calculationJob.getWorkId();
    if (as == null) {
      pending = false;
      LOG.trace("[workId:{}] Everything calculated.", workId);
    } else {
      final int assessmentId = as.getAssessmentId();
      final CalculatedScenario cs = calculationJob.getCalculatedScenario();
      final AResult resultCurrent = queryResultCurrent(con, NBWetCalculationUtil.getCurrentCalculationId(cs), assessmentId);
      final AResult resultProposed =
          rr.getCalculationResultsForAssessmentArea(con, assessmentId, NBWetCalculationUtil.getProposedCalculationId(cs));
      final int receptorCount = as.getReceptorCount();
      logResultPending(workId, assessmentId, receptorCount, resultCurrent, resultProposed);
      if (resultProposed.getCount() == receptorCount && (resultCurrent == null || resultCurrent.getCount() == receptorCount)) {
        callCompleteHandler(areasSizes.poll());
        LOG.trace("[workId:{}] Yes all receptors are calculated for assessment area:{}", workId, assessmentId);
        // only call resultsPending if current result is above threshold. If that is true the next area must be checked.
        final boolean aboveThreshold = calculateAboveThreshold(resultCurrent, resultProposed);
        final int distance = getDistance(as);
        final int deltaDistance = updateThresholdCounters(aboveThreshold, distance);
        final boolean belowMinimalDistance = deltaDistance < MINIMUM_CALCULATION_DISTANCE;
        final boolean notMinimalAreasCalculated = areasCalculatedBelowThreshold < MINIMUM_AREAS_CALCULATED;
        LOG.trace("[workId:{}] Below minimal distance:{}({}km), Distance to first low value area:{}km, Areas with minimal:{}(#{}), "
            + "aboveThreshold:{}, receptors:{}", workId, belowMinimalDistance, distance, deltaDistance, notMinimalAreasCalculated,
            areasCalculatedBelowThreshold, aboveThreshold, receptorCount);
        pending = (belowMinimalDistance || notMinimalAreasCalculated) && resultsPending(con);
      } else {
        LOG.trace("[workId:{}] Not all receptors calculated for n2k id:{}", workId, assessmentId);
        // not all result are stored in the database.
        pending = true;
      }
    }
    return pending;
  }

  private void callCompleteHandler(final AreaData areaData) {
    if (completeHandler != null && areaData != null) {
      completeHandler.onAssessmentAreaComplete(areaData.getAssessmentId());
    }
  }

  private int getDistance(final AreaData currentArea) {
    final AreaData next = areasSizes.peek();
    return (next == null ? currentArea : next).getDistance();
  }

  private boolean calculateAboveThreshold(final AResult resultCurrent, final AResult resultProposed) {
    return resultProposed.getMax() > meldingThresholdValue
        || (resultCurrent != null && resultCurrent.getMax() > meldingThresholdValue);
  }

  private AResult queryResultCurrent(final Connection con, final int currentCalculationId, final int assessmentId) throws SQLException {
    final AResult resultCurrent;
    if (currentCalculationId > 0) {
      resultCurrent = rr.getCalculationResultsForAssessmentArea(con, assessmentId, currentCalculationId);
    } else {
      resultCurrent = null;
    }
    return resultCurrent;
  }

  private void logResultPending(final String workId, final int assessmentId, final int receptorCount, final AResult resultCurrent,
      final AResult resultProposed) {
    LOG.trace("[workId:{}] Results for n2k id:{}, n2k:{} #proposed:{}, max:{} | #current:{} max:{}", workId, assessmentId, receptorCount,
        resultProposed.getCount(), resultProposed.getMax(), resultCurrent == null ? "none" : resultCurrent.getCount(),
        resultCurrent == null ? "none" : resultCurrent.getMax());
  }

  private int updateThresholdCounters(final boolean aboveThreshold, final int distance) {
    if (aboveThreshold) {
      firstBelowThresholdDistance = -1;
      areasCalculatedBelowThreshold = 0;
    } else {
      areasCalculatedBelowThreshold++;
      if (firstBelowThresholdDistance < 0) {
        firstBelowThresholdDistance = distance;
      }
    } // else keep the current value
    return firstBelowThresholdDistance < 0 ? -1 : (distance - firstBelowThresholdDistance);
  }
}
