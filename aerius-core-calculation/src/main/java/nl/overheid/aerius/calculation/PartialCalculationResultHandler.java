/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

// This class should use a separate Thread to send the results back.
public class PartialCalculationResultHandler implements CalculationResultHandler, IntermediateResultHandler {

  private static final Logger LOG = LoggerFactory.getLogger(PartialCalculationResultHandler.class);

  private final Map<Integer, Queue<AeriusResultPoint>> mappedResults = new ConcurrentHashMap<>();
  private final Queue<Integer> calculationIdCyclicQueue = new ArrayDeque<>();
  private final String correlationId;
  private final int maxResults;
  private final IncludeResultsFilter includeResultsFilter;

  public PartialCalculationResultHandler(final String correlationId, final int maxResults, final IncludeResultsFilter includeResultsFilter) {
    this.correlationId = correlationId;
    this.maxResults = maxResults;
    this.includeResultsFilter = includeResultsFilter;
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
    queueResults(calculationId, result);
  }

  @Override
  public Serializable get() {
    synchronized (this) {
      final PartialCalculationResult result = getResultChunk();
      if (result == null || result.isEmpty()) {
        return null;
      } else {
        if (LOG.isTraceEnabled()) {
          LOG.trace("[correlationId:{}, calculationId:{}] Sending results back to client #{}",
              correlationId, result.getCalculationId(), result.getResults().size());
        }
        return result;
      }
    }
  }

  private void queueResults(final int calculationId, final List<AeriusResultPoint> results) {
    synchronized (this) {
      if (!mappedResults.containsKey(calculationId)) {
        mappedResults.put(calculationId, new ArrayDeque<AeriusResultPoint>());
        calculationIdCyclicQueue.add(calculationId);
      }
      mappedResults.get(calculationId).addAll(results);
    }
  }

  /**
   * Returns available results for 1 calculation id (1 because {@link PartialCalculationResult} works for 1 calculation id). It uses a cyclic queue
   * to determine for which calculation id to get results. It will then collect filtered results and if those results are not empty then the results
   * are returned. If no results are present then null is returned. This method should be called until all results are drained from the
   * results queue.
   *
   * @return Partial calculation results for 1 calculation id or null if no results were available.
   */
  private PartialCalculationResult getResultChunk() {
    synchronized (this) {
      while (hasValues()) {
        final Integer next = pollAndReAdd();

        if (next == null) {
          return null;
        }
        final PartialCalculationResult pcr = collectPartialCalculationResult(next);

        if (pcr != null) {
          return pcr;
        }
      }
      return null;
    }
  }

  private Integer pollAndReAdd() {
    final Integer next = calculationIdCyclicQueue.poll();

    if (next != null) {
      calculationIdCyclicQueue.add(next);
    }
    return next;
  }

  private boolean hasValues() {
    return mappedResults.entrySet().stream().filter(e -> !e.getValue().isEmpty()).findAny().isPresent();
  }

  private PartialCalculationResult collectPartialCalculationResult(final Integer next) {
    final List<AeriusResultPoint> collectedResults = collectResults(mappedResults.get(next));

    if (collectedResults.isEmpty()) {
      return null;
    } else {
      final PartialCalculationResult result = new PartialCalculationResult();
      result.setCalculationId(next);
      result.getResults().addAll(collectedResults);
      return result;
    }
  }

  /**
   * Collects results from the queue. It return a maximum of filtered {@link #maxResults}.
   *
   * @param queue queue to get results from
   * @return list of filtered results
   */
  private List<AeriusResultPoint> collectResults(final Queue<AeriusResultPoint> queue) {
    final List<AeriusResultPoint> collectedResults = new ArrayList<>(maxResults);

    while (!queue.isEmpty() && collectedResults.size() < maxResults) {
      final AeriusResultPoint point = queue.poll();
      if (includeResultsFilter.includeResult(point)) {
        collectedResults.add(point);
      }
    }
    return collectedResults;
  }
}
