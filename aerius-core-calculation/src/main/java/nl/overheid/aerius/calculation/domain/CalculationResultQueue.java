/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;

import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;

/**
 * Queue to collected results from multiple calculations. When the multiple calculations are done for the same receptor set this queue keeps track if
 * the results for all sectors are available for a specific receptor. If that is the case the result is stored in the completed queue. The calculating
 * process can then pull these results from the queue.
 */
public final class CalculationResultQueue {

  private final Map<Integer, SectorResults> sectorResults = new HashMap<>();
  private final Map<Integer, Queue<SectorsResultPoint>> completeResults = new HashMap<>();

  /**
   * Initializes the queue with all calculations and sectors per calculation.
   */
  public void init(final List<JobCalculation> calculations) {
    for (final JobCalculation calculation : calculations) {
      final Set<Integer> sectorIds = new HashSet<>();
      for (final Sector s : calculation.getSectors()) {
        sectorIds.add(s.getSectorId());
      }
      addCalculation(calculation.getCalculationId(), sectorIds);
    }
  }

  /**
   * Adds a calculation for the set of sectors that will be calculated.
   *
   * @param calculationId id of the calculation
   * @param sectors set of the sectors that will be calculated
   */
  public void addCalculation(final int calculationId, final Set<Integer> sectors) {
    sectorResults.put(calculationId, new SectorResults(sectors));
    completeResults.put(calculationId, new ArrayDeque<SectorsResultPoint>());
  }

  /**
   * Clears all data. Only use when in case of canceling of job.
   */
  public void clear() {
    sectorResults.clear();
    completeResults.clear();
  }

  /**
   * Returns true if there results in the complete queue.
   *
   * @param calculationId the calculation id to check
   * @return true if results are present
   */
  public boolean hasResults(final int calculationId) {
    synchronized (this) {
      final Queue<SectorsResultPoint> queue = completeResults.get(calculationId);
      return queue != null && !queue.isEmpty();
    }
  }

  /**
   * Polls the completed queue for total results. If no results are present null is returned.
   *
   * @param calculationId the calculation to get the results for
   * @return null if no results else a result object
   */
  public ArrayList<AeriusResultPoint> pollTotalResults(final int calculationId) {
    final ArrayList<AeriusResultPoint> results = new ArrayList<>();
    synchronized (this) {
      final Queue<SectorsResultPoint> queue = completeResults.get(calculationId);
      while (!queue.isEmpty()) {
        results.add(queue.poll().getTotalsResultPoint());
      }
    }
    return results;
  }

  /**
   * Puts the calculation results by calculation id and sector id into the queue and calculates for each point if all results are present, and if so
   * places the result on the completed queue.
   *
   * @param results calculation result
   * @param calculationId id of the calculation the results are from
   * @param sectorId id of the sector the results are from
   */
  public void put(final List<AeriusResultPoint> results, final int calculationId, final int sectorId) {
    final SectorResults sr = sectorResults.get(calculationId);
    Objects.requireNonNull(sr, "CalculationResultQueue not initialized for calculationId:" + calculationId);

    for (final AeriusResultPoint result : results) {
      synchronized (this) {
        final SectorsResultPoint srp = sr.put(sectorId, result);

        if (srp != null) {
          completeResults.get(calculationId).add(srp);
        }
      }
    }
  }
}
