/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.ExpanderVisitor.SourceCollector;
import nl.overheid.aerius.calculation.domain.AutoFillingHashMap;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Expands {@link EmissionSource} objects to {@link EngineSource} objects.
 */
public final class EngineSourceExpander {

  private EngineSourceExpander() {
    // util class
  }

  /**
   * Converts all {@link EmissionSource} geometries to engine sources.
   *
   * @param con Connection to use for queries.
   * @param sources The emission sources to convert to engine sources.
   * @param keys emissions keys to convert
   * @param provider determines what {@link CalculationEngine} to use for the {@link EmissionSource}
   * @return The sources expanded to engine sources.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public static List<EngineSource> toEngineSources(final Connection con, final SourceConverter geometryExpander, final List<EmissionSource> sources,
      final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    return toEngineSourcesBySector(con, geometryExpander, sources, keys).values().stream().flatMap(List::stream).collect(Collectors.toList());
  }

  /**
   * Converts all {@link EmissionSource} geometries to engine sources and return them by sector.
   *
   * @param con Connection to use for queries.
   * @param sourceConverter converter class doing the actual conversion of the source
   * @param sources The emission sources to convert to engine sources.
   * @param keys emissions keys to convert
   * @return The sources expanded to engine sources by sector.
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case of other problems
   */
  public static Map<Sector, List<EngineSource>> toEngineSourcesBySector(final Connection con, final SourceConverter sourceConverter,
      final List<EmissionSource> sources, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    validateSources(sourceConverter, sources);

    final Map<Sector, Map<Integer, List<EngineSource>>> map = new AutoFillingHashMap<Sector, Map<Integer, List<EngineSource>>>() {
      private static final long serialVersionUID = -1114176048124553362L;

      @Override
      protected Map<Integer, List<EngineSource>> getEmptyObject() {
        return new HashMap<>();
      }
    };
    final SourceCollector collector = (engineSources, es) -> {
      final Map<Integer, List<EngineSource>> sectorMap = map.get(sourceConverter.mapToSector(es));

      engineSources.forEach(e -> putEngineSource(sectorMap, e));
    };
    final ExpanderVisitor visitor = new ExpanderVisitor(con, sourceConverter, keys, collector);

    for (final EmissionSource emissionSource : sources) {
      emissionSource.accept(visitor);
    }
    final Map<Sector, List<EngineSource>> result = new HashMap<>();

    map.forEach((s, m) -> result.put(s,
        m.values().stream().flatMap(List::stream).collect(Collectors.toList())));
    return result;
  }

  /**
   * Validates all sources and throws an exception if one of the sources doesn't validate.
   */
  private static void validateSources(final SourceConverter geometryExpander, final List<EmissionSource> sources) throws AeriusException {
    for (final EmissionSource emissionSource : sources) {
      geometryExpander.validate(emissionSource);
    }
  }

  /**
   * Add the source to the map. If the source already was in the map it was same source and thus the emission of that source needs to be added to
   * the newly inserted source as that source replaces the source in the map.
   *
   * @param map map with uniquely sources
   * @param src source to add to the map
   */
  private static void putEngineSource(final Map<Integer, List<EngineSource>> map, final EngineSource src) {
    final int hashCode = src.hashCode();
    List<EngineSource> list = map.get(hashCode);

    if (list == null) {
      list = new ArrayList<EngineSource>(1);
      list.add(src);
      map.put(hashCode, list);
    } else {
      final Optional<EngineSource> match = list.stream().filter(e -> e.equals(src)).findAny();

      if (match.isPresent()) {
        final EngineSource prev = match.get();

        src.getEmissions().forEach((s, e) -> prev.setEmission(s, prev.getEmission(s) + e));
      } else {
        list.add(src);
      }
    }
  }
}
