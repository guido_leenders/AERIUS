/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.TaskResultCallback;

/**
 * {@link CalculationTaskHandler} to send tasks to the queue.
 */
class RemoteCalculationTaskHandler implements CalculationTaskHandler, TaskResultCallback {

  private static final Logger LOGGER = LoggerFactory.getLogger(RemoteCalculationTaskHandler.class);

  private final TaskManagerClient taskManagerClient;
  private final ResultHandler resultHandler;
  private final String queueName;

  public RemoteCalculationTaskHandler(final TaskManagerClient taskManagerClient, final ResultHandler resultHandler, final String queueName) {
    this.taskManagerClient = taskManagerClient;
    this.resultHandler = resultHandler;
    this.queueName = queueName;
  }

  @Override
  public <E extends EngineSource, T extends CalculationTask<E, ?, ?>> void work(final WorkKey workKey, final T task,
      final Collection<AeriusPoint> points) throws InterruptedException, TaskCancelledException {
    final String taskId = resultHandler.registerTask(workKey);

    try {
      final EngineInputData<?, AeriusPoint> taskInput = (EngineInputData<?, AeriusPoint>) task.getTaskInput();
      taskInput.setReceptors(points);
      taskManagerClient.sendTask(taskInput, workKey.getWorkId(), taskId, this, task.getWorkerType(), queueName);
    } catch (final IOException e) {
      onFailure(e, workKey.getWorkId(), taskId);
    }
  }

  @Override
  public void onSuccess(final Object value, final String correlationId, final String messageId) {
    if (value instanceof CalculationResult) {
      resultHandler.onResult(messageId, ((CalculationResult) value).getResults());
    } else {
      LOGGER.error("[correlationId:{}] Unknown calculation result retreived: {}", correlationId, value);
      onFailure(new AeriusException(Reason.INTERNAL_ERROR), correlationId, messageId);
    }
  }

  @Override
  public void onFailure(final Exception exception, final String correlationId, final String messageId) {
    resultHandler.onFailure(messageId, exception);
  }
}
