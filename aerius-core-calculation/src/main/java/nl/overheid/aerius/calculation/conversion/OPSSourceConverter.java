/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.postgis.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.ConversionRepository;
import nl.overheid.aerius.db.common.ReceptorGridSettingsRepository;
import nl.overheid.aerius.db.common.WeightedPoint;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.conversion.EmissionSourceOPSConverter;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.util.GeometryUtil;

/**
 * Class to implement to convert {@link EmissionSource} to {@link OPSSource} objects.
 */
public class OPSSourceConverter implements SourceConverter {

  private static final Logger LOGGER = LoggerFactory.getLogger(OPSSourceConverter.class);

  private static final EmissionSourceOPSConverter OPS_CONVERTER = new EmissionSourceOPSConverter();

  private final double maxSegmentSize;
  private final Double maxPolygonToPointsGridSize;
  private final int srid;

  public OPSSourceConverter(final Connection con) throws SQLException {
    maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
    maxPolygonToPointsGridSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_POLYGON_TO_POINTS_GRID_SIZE, Double.class);
    srid = ReceptorGridSettingsRepository.getSrid(con);
  }

  @Override
  public boolean convert(Connection con, final List<EngineSource> expanded, final EmissionSource originalSource, final List<EmissionValueKey> keys)
      throws AeriusException {
    try {
      return lineToPoints(expanded, originalSource, keys) || polygonToPoints(con, expanded, originalSource, keys)
          || pointToPoints(expanded, originalSource, keys);
    } catch (final SQLException e) {
      LOGGER.error("SQLError in convertGenericGeometries", e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  private boolean pointToPoints(final List<EngineSource> expanded, final EmissionSource originalSource, final List<EmissionValueKey> keys) {
    expanded.addAll(OPS_CONVERTER.convert(originalSource, originalSource, originalSource, keys));
    return true;
  }

  /**
   * Converts a line EmissionSource to a list of EmissionSources with points. The emission of the total line is evenly divided over the points.
   * <p>
   * This method checks if the emission source contains a line, if not this method does nothing.
   *
   * @param expanded Add new emission sources to this list
   * @param emissionSource EmissionSource to convert
   * @param keys emission source keys
   * @return returns true if emission source matched condition and was converted to points
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException throws AeriusException in case converting the Geometry of the source to points failed.
   */
  boolean lineToPoints(final List<EngineSource> expanded, final EmissionSource emissionSource, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    final WKTGeometry geometry = emissionSource.getGeometry();
    final boolean converted = geometry != null && geometry.getType() == WKTGeometry.TYPE.LINE;
    if (converted) {
      final List<Point> points = GeometryUtil.convertToPoints(geometry, maxSegmentSize);
      final EmissionValues pointEmissionValues = getEmissionValuesPerPoint(emissionSource, points.size(), keys);

      for (final Point point : points) {
        expanded.addAll(OPS_CONVERTER.convert(emissionSource, point, pointEmissionValues, keys));
      }
    }
    return converted;
  }

  /**
   * Converts a polygon EmissionSource to a list of EmissionSources with points. The emission of the total polygon is evenly divided over the points.
   * <p>
   * This method checks if the emission source contains a polygon, if not this method does nothing.
   *
   * @param expanded Add new emission sources to this list
   * @param emissionSource EmissionSource to convert
   * @param keys emission source keys
   * @return returns true if emission source matched condition and was converted to points
   * @throws SQLException throws SQLException in case of database problems
   * @throws AeriusException
   */
  boolean polygonToPoints(final Connection con, final List<EngineSource> expanded, final EmissionSource emissionSource,
      final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final WKTGeometry geometry = emissionSource.getGeometry();
    final boolean converted = geometry != null && geometry.getType() == WKTGeometry.TYPE.POLYGON;
    if (converted) {
      final Polygon polygon = new Polygon(geometry.getWKT());
      polygon.setSrid(srid);
      final List<WeightedPoint> weightedPoints = ConversionRepository.convertToPoints(con, polygon);
      final double totalSurfaceRatio = calculateTotalSurfaceRatio(weightedPoints);

      for (final WeightedPoint point : weightedPoints) {
        final EmissionValues emissionValues = calculatePolygonEmission(emissionSource, keys, totalSurfaceRatio, point);
        setDiameter(emissionSource, point);
        final List<? extends EngineSource> es = OPS_CONVERTER.convert(emissionSource, point, emissionValues, keys);
        expanded.addAll(es);
      }
    }
    return converted;
  }

  private void setDiameter(final EmissionSource emissionSource, final WeightedPoint point) {
    emissionSource.getSourceCharacteristics().setDiameter(
        (int) Math.round(maxPolygonToPointsGridSize * Math.sqrt(point.getFactor())));
  }

  private EmissionValues calculatePolygonEmission(final EmissionSource emissionSource, final List<EmissionValueKey> keys,
      final double totalSurfaceRatio, final WeightedPoint point) {
    final EmissionValues emissionValues = new EmissionValues(emissionSource.isYearDependent());
    for (final EmissionValueKey key : keys) {
      final double totalWeightedEmission = emissionSource.getEmission(key) * point.getFactor();
      emissionValues.setEmission(key, totalWeightedEmission / totalSurfaceRatio);
    }
    return emissionValues;
  }

  private double calculateTotalSurfaceRatio(final List<WeightedPoint> weightedPoints) {
    double totalSurfaceRatio = 0.0;
    for (final WeightedPoint weightedPoint : weightedPoints) {
      totalSurfaceRatio += weightedPoint.getFactor();
    }
    return totalSurfaceRatio;
  }

  private static EmissionValues getEmissionValuesPerPoint(final EmissionSource originalEmissionSource, final int numberOfPoints,
      final List<EmissionValueKey> keys) {
    final EmissionValues ev = new EmissionValues(originalEmissionSource.isYearDependent());
    for (final EmissionValueKey key : keys) {
      // The result of getEmission of the emissionsource is a total, not per unit.
      ev.setEmission(key, originalEmissionSource.getEmission(key) / numberOfPoints);
    }
    return ev;
  }

}
