/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.sector.MaritimeShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.ShippingRepository;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EmissionValues;
import nl.overheid.aerius.shared.domain.sector.category.MaritimeShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.ShippingMovementType;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MaritimeRoute;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource.MooringMaritimeVesselGroup;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeRouteEmissionSource.RouteMaritimeVesselGroup;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Util class to expand {@link MaritimeMooringEmissionSource} or {@link MaritimeRouteEmissionSource} to single emission points.
 */
final class MaritimeShipEmissionCalculator {

  private MaritimeShipEmissionCalculator() {
    // util class.
  }

  /**
   * @param con Database connection.
   * @param vgev The VesselGroupEmissionValues to determine the total emissions for.
   * @param keys
   * @return The total emissions (per substance, year) for this particular vesselgroup.
   * @throws SQLException thrown in case of database problems
   * @throws AeriusException thrown when invalid geometry is used.
   */
  public static EmissionValues getEmissions(final Connection con, final MooringMaritimeVesselGroup vgev, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    final double maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
    //dock emission values are not per unit, but a total for the whole dock. No need to split.
    final EmissionValues emissionValues = getMaritimeMooringDockedEmissions(con, vgev, keys);
    emissionValues.addEmissions(getMaritimeMooringRoutesEmissions(con, vgev, maxSegmentSize, keys));
    return emissionValues;
  }

  /**
   * @param con Database connection.
   * @param vrev The VesselGroupEmissionValues to determine the total emissions for.
   * @param keys
   * @return The total emissions (per substance, year) for this particular vesselgroup.
   * @throws SQLException thrown in case of database problems
   * @throws AeriusException thrown when invalid geometry is used.
   */
  public static EmissionValues getEmissions(final Connection con, final RouteMaritimeVesselGroup vrev,
      final WKTGeometry geometry, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final double maxSegmentSize = ConstantRepository.getNumber(con, ConstantsEnum.CONVERT_LINE_TO_POINTS_SEGMENT_SIZE, Double.class);
    final List<MaritimeShippingRoutePoint> routePoints =
        ShippingRepository.getMaritimeShippingRoutePoints(con, geometry, maxSegmentSize);
    return getMaritimeRouteEmissions(con, routePoints, vrev.getCategory(), vrev.getShipMovementsPerYear(), vrev.getMovementType(), keys);
  }

  private static EmissionValues getMaritimeMooringRoutesEmissions(final Connection con, final MooringMaritimeVesselGroup vgev,
      final double maxSegmentSize, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final EmissionValues emissionValues = new EmissionValues();
    //determine the emission points on the inland part of the route.
    final List<MaritimeShippingRoutePoint> routePoints = ShippingRepository.getMaritimeMooringShippingInlandRoute(con, vgev, maxSegmentSize);
    emissionValues.addEmissions(getMaritimeRouteEmissions(
        con, routePoints, vgev.getCategory(), vgev.getNumberOfShipsPerYear() * 2, ShippingMovementType.INLAND, keys));
    //determine the emission points on the maritime part of the route.
    for (final MaritimeRoute mtr : vgev.getMaritimeRoutes()) {
      if (mtr.getRoute() != null && mtr.getShipMovementsPerTimeUnit() > 0) {
        //The route contains the geometry that should be used, use it to determine all route points
        final List<MaritimeShippingRoutePoint> maritimeRoutePoints =
            ShippingRepository.getMaritimeShippingRoutePoints(con, mtr.getRoute().getGeometry(), maxSegmentSize);
        emissionValues.addEmissions(getMaritimeRouteEmissions(
            con, maritimeRoutePoints, vgev.getCategory(), mtr.getShipMovementsPerYear(), ShippingMovementType.MARITIME, keys));
      }
    }
    return emissionValues;
  }

  private static EmissionValues getMaritimeMooringDockedEmissions(final Connection con, final MooringMaritimeVesselGroup vgev,
      final List<EmissionValueKey> keys) throws SQLException {
    final EmissionValues emissionValues = new EmissionValues();
    final HashMap<EmissionValueKey, Double> emissionFactors =
        ShippingCategoryRepository.findMaritimeCategoryEmissionFactors(con, vgev.getCategory(), ShippingMovementType.DOCK, keys.get(0).getYear());
    for (final EmissionValueKey key : keys) {
      if (emissionFactors.containsKey(key)) {
        emissionValues.setEmission(key, vgev.getNumberOfShipsPerYear() * vgev.getResidenceTime() * emissionFactors.get(key));
      }
    }
    return emissionValues;
  }

  private static EmissionValues getMaritimeRouteEmissions(final Connection con, final List<MaritimeShippingRoutePoint> routePoints,
      final MaritimeShippingCategory category, final int numberOfShips, final ShippingMovementType movementType, final List<EmissionValueKey> keys)
          throws SQLException {
    final EmissionValues emissionValues = new EmissionValues();
    //get emissionfactor (kg per meter) per ship from DB for each year, multiply times ships and set it.
    final HashMap<EmissionValueKey, Double> emissionFactors = ShippingCategoryRepository
        .findMaritimeCategoryEmissionFactors(con, category, movementType, keys.get(0).getYear());
    for (final MaritimeShippingRoutePoint routePoint : routePoints) {
      emissionValues.addEmissions(getMaritimeRoutePoints(emissionFactors, routePoint, numberOfShips, keys));
    }
    return emissionValues;
  }

  private static EmissionValues getMaritimeRoutePoints(final Map<EmissionValueKey, Double> emissionFactors,
      final MaritimeShippingRoutePoint routePoint, final int numberOfShips, final List<EmissionValueKey> keys) {
    return getEmissionsForRoute(emissionFactors, routePoint.getMeasure(), routePoint.getManeuverFactor(), numberOfShips, keys);
  }

  private static EmissionValues getEmissionsForRoute(final Map<EmissionValueKey, Double> emissionFactors, final double measure,
      final double maneuverFactor, final int numberOfMovements, final List<EmissionValueKey> keys) {
    final EmissionValues emissionValues = new EmissionValues();
    for (final EmissionValueKey key : keys) {
      if (emissionFactors.containsKey(key)) {
        emissionValues.setEmission(key, emissionFactors.get(key) * maneuverFactor * measure * numberOfMovements);
      }
    }
    return emissionValues;
  }
}
