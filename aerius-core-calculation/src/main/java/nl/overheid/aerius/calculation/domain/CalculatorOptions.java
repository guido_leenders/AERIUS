/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.io.Serializable;

/**
 * Data class for Calculator options that guide the calculation chunking process.
 */
public class CalculatorOptions implements Serializable {

  private static final long serialVersionUID = 5505052238521386215L;

  private int maxConcurrentChunks;
  private int maxCalculationEngineUnits;
  private int minReceptor;
  private int maxReceptors;
  private int maxChunkDelay;
  private int minReceptorsChunkDelay;
  private int maxEngineSources;

  public int getMaxConcurrentChunks() {
    return maxConcurrentChunks;
  }

  public void setMaxConcurrentChunks(final int maxConcurrentChunks) {
    this.maxConcurrentChunks = maxConcurrentChunks;
  }

  public int getMaxCalculationEngineUnits() {
    return maxCalculationEngineUnits;
  }

  public void setMaxCalculationEngineUnits(final int maxCalculationEngineUnits) {
    this.maxCalculationEngineUnits = maxCalculationEngineUnits;
  }

  public int getMinReceptor() {
    return minReceptor;
  }

  public void setMinReceptors(final int minReceptor) {
    this.minReceptor = minReceptor;
  }

  public int getMaxReceptors() {
    return maxReceptors;
  }

  public void setMaxReceptors(final int maxReceptors) {
    this.maxReceptors = maxReceptors;
  }

  public int getMaxChunkDelay() {
    return maxChunkDelay;
  }

  public void setMaxChunkDelay(final int maxChunkDelay) {
    this.maxChunkDelay = maxChunkDelay;
  }

  public int getMinReceptorsChunkDelay() {
    return minReceptorsChunkDelay;
  }

  public void setMinReceptorsChunkDelay(final int minReceptorsChunkDelay) {
    this.minReceptorsChunkDelay = minReceptorsChunkDelay;
  }

  public int getMaxEngineSources() {
    return maxEngineSources;
  }

  public void setMaxEngineSources(final int maxEngineSources) {
    this.maxEngineSources = maxEngineSources;
  }
}
