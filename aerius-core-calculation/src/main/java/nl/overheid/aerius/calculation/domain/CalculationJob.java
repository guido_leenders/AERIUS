/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.calculation.CalculatedScenario;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.ResearchAreaCalculationScenario;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Data object with a state and data information of a single calculation.
 */
public class CalculationJob {

  private final UUID workId;
  private String name;
  private final CalculatedScenario calculatedScenario;
  private final String queueName;
  private final List<JobCalculation> jobCalculations = new ArrayList<>();
  private CalculationEngineProvider provider;
  private CalculatorOptions calculatorOptions;
  private EmissionSourceListSTRTree tree;
  private UnitsPerSectorPerEngineAccumulator unitsAccumulator;
  private EmissionSourceListSTRTree researchAreaTree;
  private SourceConverter geometryExpander;

  public CalculationJob(final UUID workId, final CalculatedScenario calculatedScenario, final String queueName) {
    this.workId = workId;
    this.calculatedScenario = calculatedScenario;
    this.queueName = queueName;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  /**
   * It's to be removed when CalculationJob is no longer weird. (containing 2 lists with related data)
   */
  public JobCalculation findJobCalculation(final int calculationId) {
    for (final JobCalculation calc : getCalculations()) {
      if (calc.getCalculationId() == calculationId) {
        return calc;
      }
    }
    throw new IllegalStateException("Unreachable state reached. CalculationJob not properly initialized. workId:" + getWorkId());
  }

  public CalculatedScenario getCalculatedScenario() {
    return calculatedScenario;
  }

  public List<JobCalculation> getCalculations() {
    return jobCalculations;
  }

  public String getQueueName() {
    return queueName;
  }

  public String getWorkId() {
    return workId.toString();
  }

  public CalculationEngineProvider getProvider() {
    return provider;
  }

  public void setProvider(final CalculationEngineProvider provider) {
    this.provider = provider;
  }

  public SourceConverter getGeometryExpander() {
    return geometryExpander;
  }

  public void setGeometryExpander(final SourceConverter geometryExpander) {
    this.geometryExpander = geometryExpander;
  }

  public CalculationSetOptions getCalculationSetOptions() {
    return getCalculatedScenario().getOptions();
  }

  public CalculatorOptions getCalculatorOptions() {
    return calculatorOptions;
  }

  public void setCalculatorOptions(final CalculatorOptions calculatorOptions) {
    this.calculatorOptions = calculatorOptions;
  }

  public int getYear() {
    return calculatedScenario.getCalculations().get(0).getYear();
  }

  public ArrayList<EmissionValueKey> getEmissionValueKeys() {
    return EmissionValueKey.getEmissionValueKeys(getYear(), getCalculationSetOptions().getSubstances());
  }

  /**
   * Returns the tree of all sources. The tree is lazily created so it's only created when the tree is required.
   * @return tree of all sources.
   * @throws AeriusException
   */
  public synchronized EmissionSourceListSTRTree getResearchAreaTree() throws AeriusException {
    if (researchAreaTree == null && getCalculatedScenario() instanceof ResearchAreaCalculationScenario) {
      researchAreaTree = EmissionSourceListSTRTree.buildTree(
          Collections.singletonList(((ResearchAreaCalculationScenario) getCalculatedScenario()).getResearchAreaSources()));
    }
    return researchAreaTree;
  }

  /**
   * Returns the tree of all sources. The tree is lazily created so it's only created when the tree is required.
   * @return tree of all sources.
   * @throws AeriusException
   */
  public synchronized EmissionSourceListSTRTree getTree() throws AeriusException {
    if (tree == null) {
      tree = EmissionSourceListSTRTree.buildTree(getCalculatedScenario().getScenario().getSourceLists());
    }
    return tree;
  }

  public UnitsPerSectorPerEngineAccumulator getUnitsAccumulator() {
    return unitsAccumulator;
  }

  /**
   * @return true if has an unitsAccumulator
   */
  public boolean isUnitsAccumulator() {
    return unitsAccumulator != null;
  }

  /**
   * Call to enable units accumulator.
   */
  public void setUnitsAccumulator() {
    unitsAccumulator = new UnitsPerSectorPerEngineAccumulator();
  }
}
