/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculationResultSetType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * {@link CalculationResultHandler} to store both total and sector results into the database.
 */
public class SectorAndTotalResultDBHandler extends TotalResultDBHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(SectorAndTotalResultDBHandler.class);

  public SectorAndTotalResultDBHandler(final PMF pmf, final CalculationJob calculationJob) {
    super(pmf, calculationJob);
  }

  @Override
  public void onSectorResults(final List<AeriusResultPoint> results, final int calculationId, final int sectorId) throws AeriusException {
    try (final Connection con = getPMF().getConnection()) {
      CalculationRepository.insertCalculationResults(con, calculationId, CalculationResultSetType.SECTOR, sectorId, results);
    } catch (final SQLException e) {
      LOGGER.error("[calculationId:{}] sql error", calculationId, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }
}
