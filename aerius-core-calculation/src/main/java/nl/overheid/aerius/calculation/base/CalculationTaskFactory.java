/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;

import nl.overheid.aerius.calculation.EngineInputData;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface to create a calculation engine specific task from the calculation task.
 */
public interface CalculationTaskFactory {

  /**
   * Creates a task object for a specific worker type using the sector configured type.
   *
   * @param calculationEngine type of calculation
   * @return input object send to the calculation engine
   * @throws SQLException database error
   */
  <E extends EngineSource, R extends AeriusPoint, T extends EngineInputData<E, R>> CalculationTask<E, R, T>
  createTask(CalculationEngine calculationEngine) throws AeriusException;
}
