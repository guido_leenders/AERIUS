/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.CalculationRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * {@link CalculationResultHandler} to store the total results into the database.
 */
public class TotalResultDBHandler implements CalculationResultHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(TotalResultDBHandler.class);

  private final PMF pmf;
  private final CalculationJob calculationJob;


  public TotalResultDBHandler(final PMF pmf, final CalculationJob calculationJob) {
    this.pmf = pmf;
    this.calculationJob = calculationJob;
  }

  protected PMF getPMF() {
    return pmf;
  }

  /**
   * Insert results into the database.
   * @param result
   * @param calculationId
   * @throws AeriusException
   * @throws Exception
   */
  @Override
  public void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("[calculationId:{}] Received #{} results for calculation ", calculationId,  result.size());
    }
    try (final Connection con = pmf.getConnection()) {
      CalculationRepository.insertCalculationResults(con, calculationId, result);
    } catch (final SQLException e) {
      LOGGER.error("[calculationId:{}] sql error", calculationId, e);
      throw new AeriusException(Reason.SQL_ERROR);
    }
  }

  @Override
  public void onFinish(final CalculationState state) {
    try (final Connection con = pmf.getConnection()) {
      for (final JobCalculation calculation : calculationJob.getCalculations()) {
        CalculationRepository.updateCalculationState(con, calculation.getCalculationId(), state);
      }
    } catch (final SQLException sqlE) {
      LOGGER.error("Updating state in database failed:", sqlE);
    }
  }
}
