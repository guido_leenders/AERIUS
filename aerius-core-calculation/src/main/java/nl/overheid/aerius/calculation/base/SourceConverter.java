/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.Connection;
import java.util.List;

import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Interface class to implement engine source specific ways to convert {@link EmissionSource} to {@link EngineSource} objects.
 */
public interface SourceConverter {

  /**
   * Expand line or polygon sources to lists of single point sources. A point source is just passed as is.
   * @param con Database connection
   * @param expanded Add converted engine sources to this list
   * @param originalSource EmissionSource to convert
   *
   * @return true if source was converted
   * @throws AeriusException
   */
  boolean convert(Connection con, List<EngineSource> expanded, EmissionSource originalSource, List<EmissionValueKey> keys) throws AeriusException;

  /**
   * Maps a {@link EmissionSource} to the sector it should be calculated under.
   *
   * @param emissionSource emission source to get sector for
   * @return
   */
  default Sector mapToSector(final EmissionSource es) {
    return es.getSector();
  }

  /**
   * Perform additional validation checks on an {@link EmissionSource} that are related to expanding the source to an {@link EngineSource}.
   *
   * The default implementation does nothing
   * @param emissionSource emission source to check
   * @throws AeriusException
   */
  default void validate(final EmissionSource emissionSource) throws AeriusException {}
}
