/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.base.Aggregatable;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.ops.io.BrnConstants;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
/**
 * This Aggregatable aggregate sources that have the following properties:
 *
 * <pre>
 * - Is an OPSSource and
 * - Has an emission height + spread of < 25 and
 * - Has heat content of < 0.5
 * </pre>
 *
 * Note, it will aggregate sources with differing diurnal variations separately.
 *
 * Further, it cannot aggregate the comp, cat, area and particle size distribution properties. It will take the properties of the 'first' element in
 * the list of aggregable sources. In practice, it will be assumed that these properties are identical.
 */
public class GridEngineSourceAggregatable implements Aggregatable<EngineSource, Substance> {
  private static final int MAX_AGGREGABLE_HEIGHT = 25;
  private static final double MAX_AGGREGABLE_HEAT_CONTENT = 0.5;

  private final int srid;

  public GridEngineSourceAggregatable(final int srid) {
    this.srid = srid;
  }

  @Override
  public boolean isAggregatable(final EngineSource obj) {
    return obj instanceof OPSSource && isOPSSourceAggregatable((OPSSource) obj);
  }

  /**
   * Tests if source is aggregatable. Only main srid points are supported to be aggregated.
   * @param source
   * @return
   */
  private boolean isOPSSourceAggregatable(final OPSSource source) {
    return source.getPoint().isSystemSrid(srid)
        && source.getEmissionHeight() + source.getSpread() < MAX_AGGREGABLE_HEIGHT && source.getHeatContent() < MAX_AGGREGABLE_HEAT_CONTENT
        && Double.compare(BrnConstants.HEAT_CONTENT_NOT_APPLICABLE, source.getHeatContent()) != 0;
  }

  @Override
  public List<EngineSource> aggregateSources(final List<EngineSource> srcs, final List<Substance> substances) {
    final OPSSource original = (OPSSource) srcs.get(0);
    final List<EngineSource> aggregatedList = new ArrayList<>();

    for (final Substance substance : substances) {
      // Set sums to 0 / default values
      double cumX = 0D;
      double cumY = 0D;
      double summedHeightEmission = 0D;
      double summedHeatContent = 0D;
      double summedEmission = 0D;
      double sumDiameter = 0D;

      // Initial iteration to determine sums
      int count = 0;
      for (final EngineSource es : srcs) {
        final double emission = es.getEmission(substance);
        if (emission > 0) {
          count++;
          final OPSSource src = (OPSSource) es;
          cumX += src.getPoint().getX() * emission;
          cumY += src.getPoint().getY() * emission;
          summedEmission += emission;
          summedHeightEmission += emission * src.getEmissionHeight();
          summedHeatContent += emission * src.getHeatContent();
          sumDiameter += emission * src.getDiameter();
        }
      }
      if (count > 0) {
        final OPSSource aggregated = copyToNewOpsSource(original);
        aggregated.getPoint().setX(Math.round(cumX / summedEmission));
        aggregated.getPoint().setY(Math.round(cumY / summedEmission));
        aggregated.getPoint().setSrid(original.getPoint().getSrid());
        aggregated.setEmission(substance, summedEmission);
        aggregated.setEmissionHeight(summedHeightEmission / summedEmission);
        aggregated.setHeatContent(summedHeatContent / summedEmission);
        aggregated.setDiameter((int) Math.round(sumDiameter / summedEmission));
        aggregated.setSpread(calculateSpread(srcs, summedEmission, substance, count, summedHeightEmission / count));

        // Set non-aggregable properties to their (assumed-to-be-identical) original values
        aggregatedList.add(aggregated);
      }
    }
    return aggregatedList;
  }

  private double calculateSpread(final List<EngineSource> srcs, final double summedEmission, final Substance substance, final int length,
      final double averageHeight) {
    double summedSpreadDeviation = 0D;

    // Second iteration to determine standard deviation
    for (final EngineSource es : srcs) {
      final OPSSource src = (OPSSource) es;
      final double emissionHeight = src.getEmission(substance) * src.getEmissionHeight();

      if (emissionHeight > 0) {
        summedSpreadDeviation += Math.pow(emissionHeight - averageHeight, 2);
      }
    }

    final double spreadStandardDeviation = summedSpreadDeviation / length;
    return Math.sqrt(spreadStandardDeviation) / summedEmission;
  }

  private static OPSSource copyToNewOpsSource(final OPSSource original) {
    final OPSSource aggregated = new OPSSource(original.getId());
    aggregated.setDiurnalVariation(original.getDiurnalVariation());
    aggregated.setComp(original.getComp());
    aggregated.setParticleSizeDistribution(original.getParticleSizeDistribution());
    aggregated.setCat(original.getCat());
    aggregated.setArea(original.getArea());
    return aggregated;
  }

  @Override
  public AggregationKey aggregationKey(final EngineSource obj) {
    return obj instanceof OPSSource ? aggregationKeyOPSSource((OPSSource) obj) : null;
  }

  private AggregationKey aggregationKeyOPSSource(final OPSSource obj) {
    final int prime = 31;
    final int result = 1;
    final int hashCode = prime * result + obj.getDiurnalVariation();

    return new AggregationKey() {
      @Override
      public int hashCode() {
        return hashCode;
      }

      @Override
      public boolean equals(final Object obj) {
        return obj != null && getClass() == obj.getClass() && hashCode() == obj.hashCode();
      }
    };
  }

}
