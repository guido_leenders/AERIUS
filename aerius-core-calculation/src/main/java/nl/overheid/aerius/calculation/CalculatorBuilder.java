/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.SQLException;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.taskmanager.client.TaskManagerClient;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;
import nl.overheid.aerius.taskmanager.client.mq.RabbitMQQueueMonitor;

/**
 * Creates a Calculator object on which all handlers can be added. When done with the call to {@link #build(RabbitMQQueueMonitor, TaskManagerClient)}
 * the Calculator object is completed and ready to run.
 */
final class CalculatorBuilder {

  private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorBuilder.class);

  private final PMF pmf;
  private final CalculatorProfileFactory profileFactory;
  private final CalculationJob calculationJob;
  private final String correlationId;
  private final Calculator calculator;

  public CalculatorBuilder(final PMF pmf, final ExecutorService executor, final CalculatorProfileFactory profileFactory,
      final CalculationJob calculationJob, final String correlationId) {
    this.pmf = pmf;
    this.profileFactory = profileFactory;
    this.calculationJob = calculationJob;
    this.correlationId = correlationId;
    calculator = new Calculator(pmf, executor, calculationJob, correlationId);
  }

  /**
   * Sets the {@link CalculationResultHandler} on the calculator.
   * @param uiOutput if true the output is intended for displaying the results in a browser.
   * @param sectorOuput if true results by sector should be stored.
   * @param csvOutput if true the output should be stored in a file instead in the database.
   * @return this object
   */
  public CalculatorBuilder setCalculationResultHandler(final boolean uiOutput, final boolean sectorOuput, final boolean csvOutput) {
    final CalculationResultHandler handler;

    if (csvOutput) {
      logTrace("Add CsvOutputHandler");
      handler = new CsvOutputHandler(pmf, profileFactory, calculationJob, correlationId);
      calculationJob.setUnitsAccumulator();
    } else if (sectorOuput) {
      logTrace("Add SectorAndTotalResultDBHandler");
      handler = new SectorAndTotalResultDBHandler(pmf, calculationJob);
    } else if (uiOutput) {
      logTrace("Add TotalResultDBUnsafeHandler");
      handler = new TotalResultDBUnsafeHandler(pmf, calculationJob);
    } else {
      logTrace("Add TotalResultDBHandler");
      handler = new TotalResultDBHandler(pmf, calculationJob);
    }
    calculator.addCalculationResultHandler(handler);
    return this;
  }

  /**
   * Set the progress tracking of the calculation in the database. Used in combination with a database Job.
   * @return this object
   */
  public CalculatorBuilder setTrackJobHandler() throws AeriusException {
    logTrace("Add TrackJobProgressHandler");
    final TrackJobProgressHandler trackJobProgressHandler = new TrackJobProgressHandler(pmf, calculationJob, correlationId);
    calculator.addCalculationResultHandler(trackJobProgressHandler);
    trackJobProgressHandler.init();
    return this;
  }

  /**
   * Sets the intermediate result handler to send results when they arrive. Used when showing results in the browser.
   * @param resultCallback callback to send intermediate results to if not null
   * @return this object
   */
  public CalculatorBuilder setIntermediateHandler(final WorkerIntermediateResultSender resultCallback) throws AeriusException {
    if (resultCallback != null) {
      final IncludeResultsFilter resultFilter = profileFactory.getIncludeResultFilter(calculationJob.getCalculationSetOptions());
      final int maxReceptors = calculationJob.getCalculatorOptions().getMaxReceptors();
      calculator.addIntermediateResultSender(resultCallback);
      calculator.addIntermediateHandler(
          calculator.addCalculationResultHandler(new PartialCalculationResultHandler(correlationId, maxReceptors, resultFilter)));
      calculator.addIntermediateHandler(
          calculator.addCalculationResultHandler(new ResultHighValuesByDistanceHandler(calculationJob.getTree(), resultFilter)));
      logTrace("Add IntermediateHandlerProcessor");
    }
    return this;
  }

  /**
   * Builds the calculator object and stitches everything together.
   * @param rabbitMQQueueMonitor
   * @param taskManagerClient
   * @return new Calculator object
   */
  public Calculator build(final RabbitMQQueueMonitor rabbitMQQueueMonitor, final TaskManagerClient taskManagerClient)
      throws AeriusException, SQLException {
    logTrace("Build the Calculator object");
    final DynamicWorkSemaphore workLocker = new DynamicWorkSemaphore(rabbitMQQueueMonitor,
        calculationJob.getCalculatorOptions().getMaxConcurrentChunks());
    final WorkMonitorImpl workMonitor = new WorkMonitorImpl(calculator.getTracker(), workLocker);
    calculator.addCalculationResultHandler(workMonitor);
    profileFactory.getCalculationResultHandler(calculationJob).forEach(calculator::addCalculationResultHandler);
    final CalculationResultQueue queue = new CalculationResultQueue();
    queue.init(calculationJob.getCalculations());

    logTrace("Create Handlers");
    final ResultHandler resultHandler = new ResultHandlerImpl(workMonitor, queue, calculator.getHandler());
    final CalculationTaskHandler remoteWorkHandler =
        new RemoteCalculationTaskHandler(taskManagerClient, resultHandler, calculationJob.getQueueName());
    final CalculatorOptions calculatorOptions = calculationJob.getCalculatorOptions();
    final CalculationTaskHandler workSplitHandler = new WorkSplitterCalculationTaskHandler(remoteWorkHandler,
        calculatorOptions.getMaxCalculationEngineUnits(), calculatorOptions.getMinReceptor());

    logTrace("Create WorkHandler");
    calculator.setWorkHandler(
        new CalculationJobWorkHandler<AeriusPoint>(profileFactory.createWorkHandler(calculationJob, workSplitHandler, resultHandler)));

    logTrace("Digest sources");
    profileFactory.digest(calculationJob, calculationJob.getEmissionValueKeys());

    logTrace("Create WorkDistributor");
    calculator.setWorkDistributor(profileFactory.createWorkDistributor(calculationJob));
    return calculator;
  }

  private void logTrace(final String text) {
    LOGGER.trace("[correlationId:{}] %s", correlationId, text);
  }
}
