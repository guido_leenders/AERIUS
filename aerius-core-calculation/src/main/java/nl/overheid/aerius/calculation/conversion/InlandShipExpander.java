/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.db.common.sector.InlandCategoryEmissionFactorKey;
import nl.overheid.aerius.db.common.sector.InlandCategoryKey;
import nl.overheid.aerius.db.common.sector.InlandShippingRoutePoint;
import nl.overheid.aerius.db.common.sector.category.ShippingCategoryRepository;
import nl.overheid.aerius.ops.domain.OPSCopyFactory;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.ops.OPSSourceCharacteristics;
import nl.overheid.aerius.shared.domain.sector.category.InlandShippingCategory;
import nl.overheid.aerius.shared.domain.sector.category.InlandWaterwayCategory.WaterwayDirection;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringRoute;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.InlandMooringVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandMooringEmissionSource.NavigationDirection;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.RouteInlandVesselGroup;
import nl.overheid.aerius.shared.domain.source.InlandRouteEmissionSource.ShippingLaden;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

final class InlandShipExpander {

  private static final Logger LOGGER = LoggerFactory.getLogger(InlandShipExpander.class);

  private static final int MAX_PERCENTAGE = 100;

  private static final double INLAND_LOCK_HEAT_CONTENT_FACTOR = 0.15;

  private final Connection con;
  private final SourceConverter gse;

  public InlandShipExpander(final Connection con, final SourceConverter gse) {
    this.con = con;
    this.gse = gse;
  }

  /**
   * Converts a inland ship object (if presented as a line) to individual point objects with the emission values per object.
   *
   * @param expanded expand emission source to separate sources
   * @param emissionSource source to expand
   * @param keys
   * @return returns true if any expansion has been done, if no processing has been done false is returned.
   * @throws AeriusException
   */
  public void inlandMooringToPoints(final List<EngineSource> expanded, final InlandMooringEmissionSource sev, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    // Calculate the emission points on the geometry. For docked ships this
    // is the emission of stationary vessels over the length of the dock.
    for (final InlandMooringVesselGroup vgev : sev.getEmissionSubSources()) {
      // For docked ships the emission at the dock and the emission on the route(s) have to be calculated separately.
      // first the dock
      expanded.addAll(getInlandMooringDockedEmissionSources(sev, vgev, keys));
      // next all the routes
      for (final InlandMooringRoute route : vgev.getRoutes()) {
        expanded.addAll(getInlandMooringRoutesEmissionPoints(vgev, route, keys));
      }
    }
  }

  /**
   * Converts a inland ship object (if presented as a line) to individual point objects with the emission values per object.
   *
   * @param expanded expand emission source to separate sources
   * @param emissionSource source to expand
   * @param keys
   * @return returns true if any expansion has been done, if no processing has been done false is returned.
   * @throws AeriusException
   */
  public void inlandRouteToPoints(final List<EngineSource> expanded, final InlandRouteEmissionSource sev, final List<EmissionValueKey> keys)
      throws SQLException, AeriusException {
    final List<InlandShippingRoutePoint> routePoints =
        InlandRouteEmissionsCalculator.getRoutePoints(con, sev.getGeometry(), sev.getInlandWaterwayType());
    for (final RouteInlandVesselGroup ivg : sev.getEmissionSubSources()) {
      expanded.addAll(getInlandRouteEmissionSources(con, routePoints, ivg, keys));
    }
  }

  private List<EngineSource> getInlandMooringDockedEmissionSources(final EmissionSource es, final InlandMooringVesselGroup vgev,
      final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final List<EngineSource> converted = new ArrayList<>();
    // get the characteristics (different by laden / unladen!)
    final Map<ShippingLaden, OPSSourceCharacteristics> characteristicsMap = ShippingCategoryRepository.getInlandCategoryMooringCharacteristics(con,
        vgev.getCategory());

    // convert the split between arriving and departing to a split between laden and unladen.
    setPointSources(converted, characteristicsMap.get(ShippingLaden.LADEN), dockToPoints(es, keys, vgev, vgev.getShipsResidenceTimeLadenPerYear()));
    setPointSources(converted, characteristicsMap.get(ShippingLaden.UNLADEN),
        dockToPoints(es, keys, vgev, vgev.getShipsResidenceTimeUnLadenPerYear()));
    return converted;
  }

  private List<EngineSource> dockToPoints(final EmissionSource emissionSource, final List<EmissionValueKey> keys, final InlandMooringVesselGroup vgev,
      final double residenceTime) throws SQLException, AeriusException {
    final List<EngineSource> pointSources = new ArrayList<>();
    final GenericEmissionSource copy = emissionSource.copyTo(new GenericEmissionSource());
    setInlandMooringDockedEmissionValues(con, copy, vgev, residenceTime, keys);
    gse.convert(con, pointSources, copy, keys);
    return pointSources;
  }

  private static void setPointSources(final List<EngineSource> converted, final OPSSourceCharacteristics characteristicsAtDock,
      final List<EngineSource> pointSources) {
    for (final EngineSource pointES : pointSources) {
      if (pointES instanceof OPSSource) {
        // query DB for right characteristics: heatcontent/height for movement type = DOCK.
        OPSCopyFactory.toOpsSource(characteristicsAtDock, (OPSSource) pointES);
      }
    }
    converted.addAll(pointSources);
  }

  private static void setInlandMooringDockedEmissionValues(final Connection con, final GenericEmissionSource emissionSource,
      final InlandMooringVesselGroup vgev, final double residenceTime, final List<EmissionValueKey> keys) throws SQLException {
    emissionSource.setEmissionPerUnit(false);
    emissionSource.setYearDependent(true);
    final Map<EmissionValueKey, Double> emissionFactors = ShippingCategoryRepository.findInlandCategoryMooringEmissionFactors(con,
        vgev.getCategory(), keys.get(0).getYear());
    for (final EmissionValueKey key : keys) {
      if (emissionFactors.containsKey(key)) {
        emissionSource.setEmission(key, residenceTime * emissionFactors.get(key));
      }
    }
  }

  private List<EngineSource> getInlandMooringRoutesEmissionPoints(final InlandMooringVesselGroup imveg, final InlandMooringRoute route,
      final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final List<EngineSource> pointSources;
    if (route == null || route.getRoute() == null || route.getShipMovementsPerTimeUnit() == 0) {
      pointSources = new ArrayList<>();
    } else {
      // determine the emission points.
      // small performance gain by doing this for each ShippingRoute instead of InlandMooringRoute (they might be shared).
      // would mean grouping by shippingroute however.
      final List<InlandShippingRoutePoint> routePoints = InlandRouteEmissionsCalculator.getRoutePoints(con, route.getRoute().getGeometry(),
          route.getRoute().getInlandWaterwayType());
      // convert them to emission points.
      pointSources = getInlandMooringRouteEmissionSources(routePoints, imveg, route, keys);
    }
    return pointSources;
  }

  private List<EngineSource> getInlandMooringRouteEmissionSources(final List<InlandShippingRoutePoint> routePoints,
      final InlandMooringVesselGroup imvg, final InlandMooringRoute route, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    // use the normal RouteInlandVesselGroup way at this point.
    final RouteInlandVesselGroup ivg = new RouteInlandVesselGroup();
    ivg.setCategory(imvg.getCategory());

    // the route is coded from dock, so arrive is from B to A, departure is from A to B.
    if (route.getDirection() == NavigationDirection.ARRIVE) {
      ivg.setNumberOfShipsBtoAperTimeUnit(route.getShipMovementsPerTimeUnit());
      ivg.setTimeUnitShipsBtoA(route.getTimeUnit());
      ivg.setPercentageLadenBtoA(route.getPercentageLaden());
    } else {
      ivg.setNumberOfShipsAtoBperTimeUnit(route.getShipMovementsPerTimeUnit());
      ivg.setTimeUnitShipsAtoB(route.getTimeUnit());
      ivg.setPercentageLadenAtoB(route.getPercentageLaden());
    }
    return getInlandRouteEmissionSources(con, routePoints, ivg, keys);
  }

  private static List<EngineSource> getInlandRouteEmissionSources(final Connection con, final List<InlandShippingRoutePoint> routePoints,
      final RouteInlandVesselGroup ivg, final List<EmissionValueKey> keys) throws SQLException, AeriusException {
    final List<EngineSource> pointSources = new ArrayList<>();

    final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors = ShippingCategoryRepository.findInlandCategoryRouteEmissionFactors(con,
        ivg.getCategory(), keys.get(0).getYear());
    final Map<InlandCategoryKey, OPSSourceCharacteristics> characteristics = ShippingCategoryRepository.getInlandCategoryRouteCharacteristics(con,
        ivg.getCategory());

    for (final InlandShippingRoutePoint routePoint : routePoints) {
      // add laden A to B emissionsources
      if (ivg.getPercentageLadenAtoB() > 0) {
        addInlandRouteOpsSSource(pointSources, emissionFactors, characteristics, keys, routePoint, ivg, false, ShippingLaden.LADEN);
      }
      // add laden B to A emissionsources
      if (ivg.getPercentageLadenBtoA() > 0) {
        addInlandRouteOpsSSource(pointSources, emissionFactors, characteristics, keys, routePoint, ivg, true, ShippingLaden.LADEN);
      }
      // add unladen A to B emissionsources
      if (ivg.getPercentageLadenAtoB() < MAX_PERCENTAGE) {
        addInlandRouteOpsSSource(pointSources, emissionFactors, characteristics, keys, routePoint, ivg, false, ShippingLaden.UNLADEN);
      }
      // add unladen B to A emissionsources
      if (ivg.getPercentageLadenBtoA() < MAX_PERCENTAGE) {
        addInlandRouteOpsSSource(pointSources, emissionFactors, characteristics, keys, routePoint, ivg, true, ShippingLaden.UNLADEN);
      }
    }
    return pointSources;
  }

  private static void addInlandRouteOpsSSource(final List<EngineSource> pointSources,
      final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors,
      final Map<InlandCategoryKey, OPSSourceCharacteristics> emissionCharacteristics, final List<EmissionValueKey> keys,
      final InlandShippingRoutePoint routePoint, final RouteInlandVesselGroup ivg, final boolean reverse, final ShippingLaden laden)
          throws AeriusException {
    // determine the right direction and # ships per year based on reverse & laden
    final WaterwayDirection direction = getDirection(routePoint, reverse);
    final double shipsPerYear = reverse ? ivg.getShipsBtoAPerYear(laden) : ivg.getShipsAtoBPerYear(laden);

    if (shipsPerYear > 0) {
      // emission values and characteristics can be different based on direction and laden/unladen.
      // this causes quite some sources, but these can be (and are) aggregated with the EmissionSourceAggregator.
      final OPSSource source = getInlandRouteEmissionValues(emissionFactors, keys, direction, laden, routePoint, shipsPerYear, ivg.getCategory());
      source.getPoint().setX(routePoint.getX());
      source.getPoint().setY(routePoint.getY());
      OPSCopyFactory.toOpsSource(emissionCharacteristics.get(new InlandCategoryKey(direction, laden, routePoint.getWaterwayCategoryId())), source);
      if (routePoint.getLockFactor() > 1.0) {
        // for ships in a lock, the heatcontent is 15% of normal.
        source.setHeatContent(source.getHeatContent() * INLAND_LOCK_HEAT_CONTENT_FACTOR);
      }
      pointSources.add(source);
    }
  }

  private static WaterwayDirection getDirection(final InlandShippingRoutePoint routePoint, final boolean reverse) {
    final WaterwayDirection d = routePoint.getDirection();
    return d != null && reverse ? d.getOpposite() : d;
  }

  private static OPSSource getInlandRouteEmissionValues(final Map<InlandCategoryEmissionFactorKey, Double> emissionFactors,
      final List<EmissionValueKey> keys, final WaterwayDirection direction, final ShippingLaden laden, final InlandShippingRoutePoint routePoint,
      final double shipsPerYear, final InlandShippingCategory inlandShippingCategory) throws AeriusException {
    final OPSSource emissionSource = new OPSSource();
    for (final EmissionValueKey key : keys) {
      emissionSource.setEmission(key.getSubstance(), InlandRouteEmissionsCalculator.getInlandRouteEmission(emissionFactors, routePoint,
          new InlandCategoryEmissionFactorKey(key, direction, laden, routePoint.getWaterwayCategoryId()), shipsPerYear));
    }
    // if the supplied key list is empty, there are never any emissions (like when clustering sources). In that case we're not expecting them.
    if (!keys.isEmpty() && !emissionSource.hasEmission()) {
      final String directionString = direction == null ? "?" : direction.toString();
      final String waterwayCategoryCode = routePoint.getWaterwayCategoryCode();
      LOGGER.trace("No emissionfactor found for routePoint:{}, direction:{}", routePoint, direction);
      if (direction == null && routePoint.getDirection() != WaterwayDirection.IRRELEVANT) {
        throw new AeriusException(Reason.INLAND_SHIPPING_WATERWAY_NO_DIRECTION, waterwayCategoryCode, directionString);
      } else {
        throw new AeriusException(Reason.INLAND_SHIPPING_SHIP_TYPE_NOT_ALLOWED, inlandShippingCategory.getName(), waterwayCategoryCode);
      }
    }
    return emissionSource;
  }
}
