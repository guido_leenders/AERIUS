/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.scenario.JobProgress;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

public class TrackJobProgressHandler implements CalculationResultHandler {

  private static final Logger LOG = LoggerFactory.getLogger(TrackJobProgressHandler.class);

  private final PMF pmf;
  private final CalculationJob calculationJob;
  private final String correlationId;
  private boolean jobFound;

  /**
   * Default constructor.
   * @param pmf Persistence manager factory for database operations.
   * @param calculationJob calculatorJob
   * @param correlationId The correlation id of the job.
   */
  public TrackJobProgressHandler(final PMF pmf, final CalculationJob calculationJob, final String correlationId) {
    this.pmf = pmf;
    this.calculationJob = calculationJob;
    this.correlationId = correlationId;

    try (final Connection con = pmf.getConnection()) {
      jobFound = JobRepository.getJobId(con, correlationId) > 0;

      if (!jobFound) {
        LOG.warn("I am created to track job '{}' that doesn't exist. I won't track anything. "
            + "If this keeps happening it's likely parts of the application are configured to use different databases.", correlationId);
        // This could also mean that a new database deploy is done (or the job manually removed from the database)
        // while a calculation is still present on the queue, which results in the job being lost.
        // In such a case as they say in one of the most annoying song from a movie from 2013.. "Let it go!"
      }
    } catch (final SQLException e) {
      jobFound = false;
      // Keep executing the job so the user might get his job result, instead of crashing here. Do log it though.
      LOG.error("Error occurred while trying to check if job {} is present. "
          + "If there is a job present no progress will be recorded.", correlationId, e);
    }
  }

  public void init() throws AeriusException {
    try (final Connection con = pmf.getConnection()) {
      failIfJobCancelledByUser(con);
      final List<JobCalculation> jobCalculations = calculationJob.getCalculations();
      if (jobFound && !jobCalculations.isEmpty()) {
        final List<Integer> calculations = new ArrayList<>();
        for (final JobCalculation jobCalculation : jobCalculations) {
          calculations.add(jobCalculation.getCalculationId());
        }
        JobRepository.attachCalculations(con, correlationId, calculations);
      }
    } catch (final SQLException e) {
      LOG.error("Error occurred while trying to attach calculations for job {}", correlationId, e);
    }
  }

  private void failIfJobCancelledByUser(final Connection con) throws AeriusException, SQLException {
    final JobProgress jobProgress = JobRepository.getProgress(con, correlationId);
    if (jobProgress != null && jobProgress.getState() == JobState.CANCELLED) {
      throw new AeriusException(Reason.CONNECT_JOB_CANCELLED);
    }
  }

  boolean isJobFound() {
    return jobFound;
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> results, final int calculationId) throws AeriusException {
    if (jobFound) {
      try (final Connection con = pmf.getConnection()) {
        failIfJobCancelledByUser(con);
        JobRepository.increaseHexagonCounter(con, correlationId, results.size());
      } catch (final SQLException e) {
        LOG.error("Error occurred while trying to increase hexagon counter for job {}", correlationId, e);
      }
    }
  }
}
