/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IncludeResultsFilter;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.calculation.ResultHighValuesByDistances;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResults;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Given total results, splits those results by distance (km) and keep the highest result by result type.
 */
class ResultHighValuesByDistanceHandler implements CalculationResultHandler, IntermediateResultHandler {

  private final EmissionSourceListSTRTree sourcesTree;
  private final IncludeResultsFilter includeResultsFilter;

  private final ResultHighValuesByDistances results = new ResultHighValuesByDistances();
  private boolean changed;

  public ResultHighValuesByDistanceHandler(final EmissionSourceListSTRTree sourcesTree, final IncludeResultsFilter includeResultsFilter) {
    this.sourcesTree = sourcesTree;
    this.includeResultsFilter = includeResultsFilter;
  }

  @Override
  public void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
    synchronized (this) {
      groupResultsByDistance(result, calculationId);
      changed = true;
    }
  }

  @Override
  public Serializable get() {
    synchronized (this) {
      try {
        return changed ? results : null;
      } finally {
        changed = false;
      }
    }
  }

  /**
   * Group the results by distance in km from the supplied sources.
   * @param calculationId
   * @param resultsToGroup The results to group by distance.
   * Can be for multiple calculations in which case the results are grouped by calculation ID as well.
   * @param sources The sources to determine the distance to.
   * @return The results grouped by (minimum) distance to the source clusters.
   * @throws AeriusException
   */
  private void groupResultsByDistance(final List<AeriusResultPoint> points, final int calculationId) throws AeriusException {
    results.addNumberOfResults(points.size());
    for (final AeriusResultPoint point : points) {
      if (includeResultsFilter.includeResult(point)) {
        putResultsForCalculationId(calculationId, minDistanceInKm(point), point.getEmissionResults());
      }
    }
  }

  public void putResultsForCalculationId(final int calculationId, final Integer distance, final EmissionResults emissionResults) {
    final HashMap<Integer, EmissionResults> calculationIdMap = results.getCalculationIdMap(calculationId);
    results.updateHighestValue(calculationId, distance);
    if (calculationIdMap.containsKey(distance)) {
      final EmissionResults presentResults = calculationIdMap.get(distance);
      for (final Entry<EmissionResultKey, Double> entry : emissionResults.entrySet()) {
        if (presentResults.hasResult(entry.getKey())) {
          presentResults.put(entry.getKey(), Math.max(entry.getValue(), presentResults.get(entry.getKey())));
        } else {
          presentResults.put(entry.getKey(), entry.getValue());
        }
      }
    } else {
      // copy the result to new object
      final EmissionResults distanceResults = new EmissionResults();
      for (final Entry<EmissionResultKey, Double> result : emissionResults.entrySet()) {
        distanceResults.put(result.getKey(), result.getValue());
      }
      calculationIdMap.put(distance, distanceResults);
    }
  }

  /**
   * Find the closest distance in km of a point to the sources.
   *
   * @param point point to determine distance of
   * @return distance to the closest point in km
   * @throws AeriusException
   */
  private int minDistanceInKm(final Point point) throws AeriusException {
    return (int) Math.ceil(sourcesTree.findShortestDistance(point) / SharedConstants.M_TO_KM);
  }
}
