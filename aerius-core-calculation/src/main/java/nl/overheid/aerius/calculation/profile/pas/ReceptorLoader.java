/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.db.common.ReceptorRepository;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.ReceptorUtil;

/**
 * Class to pre load all receptors for all assessment areas receptors relevant for the PAS.
 */
class ReceptorLoader {
  private static final Logger LOGGER = LoggerFactory.getLogger(ReceptorLoader.class);

  private final ReceptorUtil receptorUtil;
  private final GridSettings gridSettings;

  /**
   * Constructor
   * @param receptorUtil receptorUtil
   * @param gridSettings grid settings
   */
  public ReceptorLoader(final ReceptorUtil receptorUtil, final GridSettings gridSettings) {
    this.receptorUtil = receptorUtil;
    this.gridSettings = gridSettings;
  }

  /**
   * Fills a {@link Natura2kGridPointStore} from the database.
   * @param con database connection
   * @return storage with all points.
   * @throws SQLException
   */
  public Natura2kGridPointStore fillPointStore(final Connection con)
      throws SQLException {
    final Map<Integer, List<AeriusPoint>> pointsMap = ReceptorRepository.getPermitRequiredPoints(con, receptorUtil);
    final Natura2kGridPointStore pointStore = new Natura2kGridPointStore(receptorUtil, gridSettings);
    fillPointMap(pointsMap, pointStore);
    pointStore.sortPointStore();
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Total number gridcells in Natura2kGridPointStore:{}", pointStore.size());
    }
    return pointStore;
  }

  static void fillPointMap(final Map<Integer, List<AeriusPoint>> pointsMap, final Natura2kGridPointStore pointStore) {
    for (final Entry<Integer, List<AeriusPoint>> entry : pointsMap.entrySet()) {
      final Integer n2kId = entry.getKey();
      final List<AeriusPoint> aeriusPoints = entry.getValue();

      if (aeriusPoints.isEmpty()) {
        LOGGER.trace("Ignoring n2k id:{} because size is 0.", n2kId);
      } else {
        pointStore.addAll(n2kId, aeriusPoints);
        if (LOGGER.isTraceEnabled()) {
          LOGGER.trace("Initializing n2kid:{} #grid cells:{}, #receptors:{}, gridids:{}", n2kId, pointStore.getGridIds(n2kId).size(),
              aeriusPoints.size(), ArrayUtils.toString(pointStore.getGridIds(n2kId)));
        }
      }
    }
  }
}
