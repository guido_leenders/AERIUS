/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import nl.overheid.aerius.db.common.CalculationDevelopmentSpaceRepository;
import nl.overheid.aerius.db.common.CalculationSummaryRepository;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.GeneralRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResult;
import nl.overheid.aerius.shared.domain.calculation.CalculationAreaSummaryResult.HabitatSummaryResults;
import nl.overheid.aerius.shared.domain.calculation.CalculationSummary;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.developmentspace.DevelopmentRuleResultList;
import nl.overheid.aerius.shared.domain.info.AssessmentArea;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Factory class to get all summary results of a calculation.
 */
public final class CalculationSummaryFactory {

  private CalculationSummaryFactory() {
    // util class
  }

  /**
   * Returns the summary results of a calculation. If calculationId1 and calculationId1 are both > 0 then a comparison summary is returned
   * of calculationId2 - calculationId1. Else the summary results of the calculation id that is larger then 0 is returned.
   * @param con connection
   * @param calculationId1 calculation id of calculation 1
   * @param calculationId2 calculation id of calculation 2
   * @param substances substances to get summary results for
   * @return calculation summary object.
   * @throws SQLException database error
   */
  public static CalculationSummary getSummary(final Connection con, final CalculationType calculationType, final int calculationId1,
      final int calculationId2, final ArrayList<Substance> substances) throws SQLException {
    final CalculationSummary calculationSummary;
    final Map<Integer, AssessmentArea> areas = GeneralRepository.getAssessmentAreas(con);
    if (calculationId1 <= 0 || calculationId2 <= 0) {
      final int id = calculationId1 > 0 ? calculationId1 : calculationId2;
      calculationSummary = getSingleResults(con, areas, calculationType, id, substances);
    } else {
      calculationSummary = getComparisonResults(con, areas, calculationType, calculationId1, calculationId2, substances);
    }
    final long syncTimestamp = ConstantRepository.getNumber(con, ConstantsEnum.DEVELOPMENT_SPACE_SYNC_TIMESTAMP, Long.class);
    calculationSummary.setDevelopmentSpaceSnapshotTime(new Date(syncTimestamp));
    return calculationSummary;
  }

  private static CalculationSummary getSingleResults(final Connection con, final Map<Integer, AssessmentArea> areas,
      final CalculationType calculationType, final int id, final ArrayList<Substance> substances) throws SQLException {
    final CalculationSummary calculationSummary = new CalculationSummary();
    CalculationSummaryRepository.determineHabitatResults(con, areas, calculationSummary, id, substances);
    if (calculationType == CalculationType.PAS) {
      // skip the developmentResult test
      // setDevelopmentResults(con, calculationSummary, -1, id);
    }
    return calculationSummary;
  }

  private static CalculationSummary getComparisonResults(final Connection con, final Map<Integer, AssessmentArea> areas,
      final CalculationType calculationType, final int calculationId1, final int calculationId2, final ArrayList<Substance> substances)
          throws SQLException {
    final CalculationSummary calculationSummary = new CalculationSummary();
    CalculationSummaryRepository.determineHabitatResults(con, areas, calculationSummary, calculationId1, substances);
    CalculationSummaryRepository.determineHabitatResults(con, areas, calculationSummary, calculationId2, substances);
    CalculationSummaryRepository.determineHabitatResultsComparison(con, calculationSummary, calculationId1, calculationId2, substances);
    updateComparisonPercentageKDW(calculationSummary, calculationId1, calculationId2);
    if (calculationType == CalculationType.PAS) {
      // skip the developmentResult test
      // setDevelopmentResults(con, calculationSummary, calculationId2, calculationId1);
    }
    return calculationSummary;
  }

  /**
   * @param con
   * @param calculationSummary
   * @param calculationId2
   * @param calculationId1
   * @throws SQLException
   */
  private static void setDevelopmentResults(final Connection con, final CalculationSummary calculationSummary, final int calculationId2,
      final int calculationId1) throws SQLException {
    HashSet<DevelopmentRuleResultList> dRulesList;
    if (calculationId2 >= 0) {
      dRulesList = CalculationDevelopmentSpaceRepository.determineResultsByArea(con, calculationId2, calculationId1);
    } else {
      dRulesList = CalculationDevelopmentSpaceRepository.determineResultsByArea(con, calculationId1);
    }
    convert2CalculationSummary(calculationSummary, dRulesList);
  }

  private static void convert2CalculationSummary(final CalculationSummary calculationSummary, final HashSet<DevelopmentRuleResultList> dRulesList) {
    for (final Entry<Integer, CalculationAreaSummaryResult> casr : calculationSummary.getSummaryResult().entrySet()) {
      for (final DevelopmentRuleResultList dRules : dRulesList) {
        if (dRules.getAssessmentArea().getId() == casr.getKey()) {
          casr.getValue().getDevelopmentRuleResults().addAll(dRules.getDevelopmentRuleResults());
          break;
        }
      }
    }
  }

  private static void updateComparisonPercentageKDW(final CalculationSummary calculationSummary, final int calculation1Id,
      final int calculation2Id) {
    // This code assumes that all 4 sets of results have the same keys (which should be the case by design).
    // (If not, not all numbers will be updated.)
    // Take calculation 1 as leading (for keys) and find percentages.
    for (final Entry<Integer, CalculationAreaSummaryResult> entry : calculationSummary.getSummaryResult().entrySet()) {
      final CalculationAreaSummaryResult casr = entry.getValue();
      for (final Entry<Integer, HabitatSummaryResults> habitatResult : casr.getHabitats().entrySet()) {
        for (final Entry<EmissionResultKey, Double> eKDW1
            : habitatResult.getValue().getHabitatSummaryResults(calculation1Id).getPercentageKDW().entrySet()) {

          // Find matching percentage in calculation 2 (same keys).
          final HabitatSummaryResult hsr2 = habitatResult.getValue().getHabitatSummaryResults(calculation2Id);
          final double pKDW2 = hsr2 == null ? 0 : hsr2.getPercentageKDW().get(eKDW1.getKey());

          // Calculate percentage 2 - 1 and put in comparisonresult 1 under the same keys.
          setPercentageKDWDiff(habitatResult, calculation1Id, calculation2Id, eKDW1.getKey(), eKDW1.getValue(), pKDW2);
        }
      }
    }
  }

  /**
   * @param habitatResult
   * @param calculationIdOne
   * @param calculationIdTwo
   * @param key
   * @param value
   * @param pKDW2
   */
  private static void setPercentageKDWDiff(final Entry<Integer, HabitatSummaryResults> habitatResult, final int calculation1Id,
      final int calculation2Id, final EmissionResultKey key, final double pkdw1, final double pkdw2) {
    habitatResult.getValue().getComparisonSummary(calculation1Id, calculation2Id).getPercentageKDW().put(key, pkdw2 - pkdw1);
  }
}
