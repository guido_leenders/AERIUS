/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.grid.GridPointStore;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ReceptorRepository;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Work distributor to calculate a custom list of points.
 */
class CustomPointsWorkDistributor implements WorkDistributor<AeriusPoint> {
  private final PMF pmf;
  private final GridSettings gridSettings;
  private final List<AeriusPoint> calculationPoints;

  public CustomPointsWorkDistributor(final PMF pmf, final GridSettings gridSettings, final List<AeriusPoint> calculationPoints) {
    this.pmf = pmf;
    this.gridSettings = gridSettings;
    this.calculationPoints = calculationPoints;
  }

  @Override
  public void distribute(final CalculationJob calculationJob, final CalculationJobWorkHandler<AeriusPoint> workHandler)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final GridPointStore pointStore = new GridPointStore(gridSettings);
    final boolean hasOps = hasOps(calculationJob);

    fillPoints(pointStore, calculationPoints, hasOps);
    setOptionalTerrainData(pointStore, hasOps);
    for (final Entry<Integer, Collection<AeriusPoint>> entry : pointStore.entrySet()) {
      workHandler.work(calculationJob, entry.getKey(), entry.getValue());
    }
  }

  private void setOptionalTerrainData(final GridPointStore pointStore, final boolean hasOps) throws SQLException {
    if (hasOps) {
      for (final Entry<Integer, Collection<AeriusPoint>> entry : pointStore.entrySet()) {
        ReceptorRepository.setTerrainData(pmf, entry.getValue());
      }
    }
  }

  private boolean hasOps(final CalculationJob calculationJob) {
    final CalculationEngineProvider provider = calculationJob.getProvider();

    for (final JobCalculation job : calculationJob.getCalculations()) {
      for (final Sector sector : job.getSectors()) {
        if (provider.getCalculationEngine(sector) == CalculationEngine.OPS) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Put calculationPoints in store receptors.
   * @param pointStore
   * @param calculationPoints
   * @param hasOps
   */
  private void fillPoints(final GridPointStore pointStore, final List<AeriusPoint> calculationPoints, final boolean hasOps) {
    for (final AeriusPoint point : calculationPoints) {
      final AeriusPoint pointToAdd;
      if (hasOps && !(point instanceof OPSReceptor)) {
        pointToAdd = new OPSReceptor(point);
      } else {
        pointToAdd = point;
      }
      pointStore.add(pointToAdd);
    }
  }
}
