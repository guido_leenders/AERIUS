/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.base;

import java.sql.SQLException;
import java.util.List;

import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Handler to process results. Implementations can store results in the database.
 */
public interface CalculationResultHandler {

  /**
   * Called with the results for a specific sector.
   * @param results
   * @param calculationId
   * @param sectorId
   * @throws AeriusException
   * @throws SQLException
   */
  default void onSectorResults(final List<AeriusResultPoint> results, final int calculationId, final int sectorId) throws AeriusException {
  }

  /**
   * Called with results where results for all sectors processes are available and summarized.
   * @param result
   * @param calculationId
   * @throws AeriusException
   * @throws Exception
   */
  default void onTotalResults(final List<AeriusResultPoint> result, final int calculationId) throws AeriusException {
  }

  /**
   * Called when calculation job is finished.
   * @param state state with which the calculation was finished.
   */
  default void onFinish(final CalculationState state) {
  }
}
