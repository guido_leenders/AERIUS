/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonIOException;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.result.EmissionResultType;
import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;

/**
 * {@link CalculationResultHandler} to write results to csv files.
 */
public class CsvOutputHandler implements CalculationResultHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(CsvOutputHandler.class);
  private static final String FOLDER_NAME = "results_calculation";

  // <user specified name>_resulttype-<resulttype>.csv
  private static final String FILENAME_FORMAT = "%s_resulttype-%s%s";
  private static final String CSV_EXT = ".csv";
  private static final String FILE_HEADER = "year;sector_id;result_type;substance_id;receptor_id;result";
  private static final String DELIMITER = ";";

  private final String name;
  private final int year;
  private final String directory;
  private final HashMap<Integer, ReentrantLock> locks = new HashMap<>();
  private final SummaryCollector collector;
  private final PMF pmf;
  private final CalculationJob calculationJob;

  /**
   * Constructor.
   *
   * @param pmf Database connection factory, only needed to get the database version for metadata
   * @param profileFactory
   * @param calculationJob The calculation job.
   * @param correlationId The correlation id of the job.
   * @throws SQLException
   * @throws IOException
   */
  public CsvOutputHandler(final PMF pmf, final CalculatorProfileFactory profileFactory, final CalculationJob calculationJob,
      final String correlationId) {
    this.pmf = pmf;
    this.calculationJob = calculationJob;
    this.year = calculationJob.getYear();
    name = calculationJob.getName() == null ? correlationId : calculationJob.getName();
    final File tempDir = getFolderForJob(correlationId);
    if (tempDir.exists()) {
      // job was already started, but aborted without cleanup of
    } else if (!tempDir.mkdirs()) {
      throw new IllegalArgumentException("Could not create temp directories needed to write CSV files to: " + tempDir);
    }
    this.directory = tempDir.getAbsolutePath();
    collector = new SummaryCollector(profileFactory, calculationJob);
  }

  public static File getFolderForJob(final String correlationId) {
    return Paths.get(System.getProperty("java.io.tmpdir"), FOLDER_NAME, correlationId).toFile();
  }

  @Override
  public void onSectorResults(final List<AeriusResultPoint> results, final int calculationId, final int sectorId) throws AeriusException {
    // files are opened per onSectorResults as we don't want to have files open unnecessarily
    final EnumMap<EmissionResultType, PrintWriter> openedFiles = new EnumMap<>(EmissionResultType.class);

    final ReentrantLock lock = getLock(calculationId);
    try {
      lock.lock();
      for (final AeriusResultPoint result : results) {
        for (final Entry<EmissionResultKey, Double> entry : result.getEmissionResults().entrySet()) {
          final EmissionResultKey key = entry.getKey();
          final EmissionResultType ert = key.getEmissionResultType();

          try {
            final PrintWriter writer = getWriter(openedFiles, ert, calculationId);

            writeRecord(writer, year, sectorId, ert, key.getSubstance(), result.getId(), entry.getValue());
          } catch (final IOException e) {
            LOGGER.error("Error writing sector results", e);
            throw new AeriusException(Reason.INTERNAL_ERROR);
          }
          collector.addResult(calculationId, sectorId, key, entry.getValue());
        }
      }
    } finally {
      for (final PrintWriter writer : openedFiles.values()) {
        writer.close();
      }
      lock.unlock();
    }
  }

  private synchronized ReentrantLock getLock(final int calculationId) {
    if (!locks.containsKey(calculationId)) {
      locks.put(calculationId, new ReentrantLock());
    }

    return locks.get(calculationId);
  }

  private PrintWriter getWriter(final EnumMap<EmissionResultType, PrintWriter> openedFiles, final EmissionResultType ert, final int calculationId)
      throws IOException {
    if (!openedFiles.containsKey(ert)) {
      final String ertString = ert.toDatabaseString();
      final String filename = String.format(FILENAME_FORMAT, name, ertString, CSV_EXT);
      final Path openFile = Paths.get(directory, filename);
      final boolean newFile = !openFile.toFile().exists();
      final PrintWriter writer = new PrintWriter(
          Files.newBufferedWriter(openFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND));
      openedFiles.put(ert, writer);

      if (newFile) {
        collector.addSummary(calculationId, ert, filename);
        writer.println(FILE_HEADER);
      }
    }

    return openedFiles.get(ert);
  }

  @Override
  public void onFinish(final CalculationState state) {
    try {
      collector.writeSummary(this.directory, name);
      MetaDataUtil.writeMetadata(pmf, calculationJob, directory, name, collector.getDuration());
    } catch (final IOException | JsonIOException | SQLException e) {
      LOGGER.error("Error writing summary file for job: {}", name, e);
    }
  }

  private void writeRecord(final PrintWriter writer, final int year, final int sectorId, final EmissionResultType ert, final Substance substance,
      final int receptorId, final double value) {
    writer.print(year);
    writer.print(DELIMITER);
    writer.print(sectorId);
    writer.print(DELIMITER);
    writer.print(ert.toDatabaseString());
    writer.print(DELIMITER);
    writer.print(substance.getId());
    writer.print(DELIMITER);
    writer.print(receptorId);
    writer.print(DELIMITER);
    writer.print(value);
    writer.println();
  }
}
