/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.WorkDistributor;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.grid.GridUtil;
import nl.overheid.aerius.calculation.grid.GridZoomLevel;
import nl.overheid.aerius.db.PMF;
import nl.overheid.aerius.db.common.ConstantRepository;
import nl.overheid.aerius.db.common.NBWetRepository;
import nl.overheid.aerius.enums.ConstantsEnum;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * {@link WorkDistributor} implementation for the PAS Wet-NB calculation.
 * Given the sources it collects the receptors in Natura2000 areas needed to be calculated.
 * This process runs until based on the results it is determined all relevant receptors have been calculated.
 */
public class PASWorkDistributor implements WorkDistributor<AeriusPoint> {

  private static final int SOURCES_ZOOM_LEVEL = 3;

  private final PMF pmf;
  private final GridUtil gridUtil;
  private final GridZoomLevel zoomLevelSources;
  private final double meldingThresholdValue;
  private final NBWetRepository rr;
  private final Natura2kGridPointStore pointStore;

  public PASWorkDistributor(final PMF pmf, final GridUtil gridUtil, final Natura2kGridPointStore pointStore) throws SQLException {
    this.pmf = pmf;
    this.gridUtil = gridUtil;
    this.zoomLevelSources = gridUtil.getGridSettings().levelFromLevel(SOURCES_ZOOM_LEVEL);
    this.pointStore = pointStore;
    meldingThresholdValue = ConstantRepository.getDouble(pmf, ConstantsEnum.PRONOUNCEMENT_THRESHOLD_VALUE);
    rr = getNBWetRepository();
  }

  @Override
  public void distribute(final CalculationJob calculationJob, final CalculationJobWorkHandler<AeriusPoint> workHandler)
      throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    final List<Entry<Integer, Integer>> listn2k = sortedAssessmentAreaByDistance(calculationJob);
    final ChunkState chunkState = new ChunkState(calculationJob, listn2k, rr, meldingThresholdValue);

    initializeChunkStateWithDistances(listn2k, chunkState);

    final Set<Integer> calculatedGridIds = new HashSet<>();
    for (final Entry<Integer, Integer> entry : listn2k) {
      if (pending(chunkState)) {
        distributeWorkPerGrid(calculationJob, workHandler, calculatedGridIds, entry.getKey());
      } else {
        break;
      }
    }
  }

  private void initializeChunkStateWithDistances(final List<Entry<Integer, Integer>> listn2k, final ChunkState chunkState) {
    for (final Entry<Integer, Integer> entry : listn2k) {
      final Integer n2kId = entry.getKey();
      final int size = pointStore.getSize(n2kId);

      if (size > 0) {
        chunkState.addN2KArea(n2kId, size, entry.getValue());
      }
    }
  }

  private void distributeWorkPerGrid(final CalculationJob calculationJob, final CalculationJobWorkHandler<AeriusPoint> workHandler,
      final Set<Integer> gridIds, final Integer n2kId) throws SQLException, AeriusException, InterruptedException, TaskCancelledException {
    final Collection<Integer> n2kGridIds = pointStore.getGridIds(n2kId);
    if (n2kGridIds != null) {
      handleWork(calculationJob, workHandler, gridIds, n2kGridIds);
    }
  }

  private void handleWork(final CalculationJob calculationJob, final CalculationJobWorkHandler<AeriusPoint> workHandler, final Set<Integer> gridIds,
      final Collection<Integer> n2kGridIds) throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
    for (final Integer gridId : n2kGridIds) {
      if (gridIds.add(gridId)) {
        workHandler.work(calculationJob, gridId, pointStore.getGridCell(gridId));
      }
    }
  }

  private boolean pending(final ChunkState chunkState) throws SQLException {
    try (Connection con = pmf.getConnection()) {
      return chunkState.resultsPending(con);
    }
  }

  /**
   * Returns the wrapper around the repository objects. Override this method in tests to stub the database.
   *
   * @return wrapper object
   */
  protected NBWetRepository getNBWetRepository() {
    return new NBWetRepository();
  }

  private List<Entry<Integer, Integer>> sortedAssessmentAreaByDistance(final CalculationJob calculationJob) throws SQLException {
    final Collection<Point> points = new HashSet<>();
    for (final JobCalculation job : calculationJob.getCalculations()) {
      final Iterable<Integer> sourceGridIds = job.getSourceGridIds(zoomLevelSources);

      for (final Integer gridId : sourceGridIds) {
        points.add(gridUtil.getPositionFromCell(gridId, zoomLevelSources));
      }
    }
    try (Connection con = pmf.getConnection()) {
      return sortAssessmentAreaByDistance(con, points);
    }
  }

  /**
   * Sorts the assessment areas by distance to the sources.
   *
   * @param con database connection
   * @param points
   * @return Entry tuple of Id's and distances in sorted order by distance of assessment areas. Sorted order by increasing distance.
   * @throws SQLException
   */
  private <P extends Point> List<Entry<Integer, Integer>> sortAssessmentAreaByDistance(final Connection con, final Collection<P> points)
      throws SQLException {
    final Map<Integer, Integer> map = new HashMap<>();
    for (final Point point : points) {
      for (final Entry<Integer, Integer> pointEntry : rr.getDistanceSetForPoint(con, point).entrySet()) {
        final Integer currentDistance = map.get(pointEntry.getKey());
        if (currentDistance == null || currentDistance > pointEntry.getValue()) {
          map.put(pointEntry.getKey(), pointEntry.getValue());
        }
      }
    }
    final List<Entry<Integer, Integer>> list = new ArrayList<>();
    for (final Entry<Integer, Integer> entry : map.entrySet()) {
      list.add(entry);
    }
    Collections.sort(list, new Comparator<Entry<Integer, Integer>>() {
      @Override
      public int compare(final Entry<Integer, Integer> o1, final Entry<Integer, Integer> o2) {
        return Integer.compare(o1.getValue(), o2.getValue());
      }
    });
    return list;
  }
}
