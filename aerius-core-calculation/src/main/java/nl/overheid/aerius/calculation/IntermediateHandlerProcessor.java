/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;

/**
 * Processes all added {@link IntermediateResultHandler}s in the {@link #run()} method. The {@link #run()} method runs until the status running is
 * false. This status is set when {@link #onFinish(CalculationState)} is calculated. In that case it keeps running until all handlers report they
 * have send all results.
 */
class IntermediateHandlerProcessor implements CalculationResultHandler, Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(IntermediateHandlerProcessor.class);
  /**
   * Time to wait before sending new results back in milliseconds.
   */
  private static final long INTERMEDIATE_SLEEP_TIME = 2000;

  private final List<IntermediateResultHandler> intermediateHandlers = new ArrayList<>();
  private final WorkerIntermediateResultSender sender;
  private final long sleepTime;
  private boolean running = true;
  private CalculationState state;

  public IntermediateHandlerProcessor(final WorkerIntermediateResultSender sender) {
    this(sender, INTERMEDIATE_SLEEP_TIME);
  }

  public IntermediateHandlerProcessor(final WorkerIntermediateResultSender sender, final long sleepTime) {
    this.sender = sender;
    this.sleepTime = sleepTime;
  }

  public void add(final IntermediateResultHandler handler) {
    intermediateHandlers.add(handler);
  }

  @Override
  public void run() {
    try {
      while (running) {
        sendResultsOnHandlers();
        Thread.sleep(sleepTime);
      }
      while (state == CalculationState.COMPLETED && sendResultsOnHandlers()) { }
    } catch (final InterruptedException | IOException e) {
      running = false;
      LOG.error("IntermediateHandlerProcessor exception", e);
    }
  }

  private boolean sendResultsOnHandlers() throws IOException {
    boolean hasSendResults = false;
    for (final IntermediateResultHandler handler : intermediateHandlers) {
      final Serializable value = handler.get();
      if (value != null) {
        sender.sendIntermediateResult(value);
        hasSendResults = true;
      }
    }
    return hasSendResults;
  }

  boolean isRunning() {
    return running;
  }

  @Override
  public void onFinish(final CalculationState state) {
    this.state = state;
    running = false;
  }
}
