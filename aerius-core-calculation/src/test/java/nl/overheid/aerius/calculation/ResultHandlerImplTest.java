/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.calculation.base.CalculationResultHandler;
import nl.overheid.aerius.calculation.base.ResultHandler;
import nl.overheid.aerius.calculation.base.ResultPostProcessHandler;
import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.domain.CalculationResultQueue;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link ResultHandlerImpl}.
 */
public class ResultHandlerImplTest extends BaseDBTest {

  private WorkMonitor monitor;
  private CalculationResultQueue queue;
  private CalculationResultHandler handler;
  private AtomicBoolean handlerCalled;
  private ResultHandler resultHandler;
  private Exception exception;

  @SuppressWarnings("unchecked")
  @Before
  public void before() throws AeriusException {
    exception = null;
    handlerCalled = new AtomicBoolean();
    monitor = Mockito.mock(WorkMonitor.class);
    final WorkKey workKey = new WorkKey("", "1", 1, 1);
    Mockito.when(monitor.getWorkKey(anyString())).then(new Answer<WorkKey>() {
      @Override
      public WorkKey answer(final InvocationOnMock invocation) throws Throwable {
        return workKey;
      }
    });
    queue = new CalculationResultQueue();
    final List<JobCalculation> calculations = new ArrayList<>();
    final List<Sector> sectors = new ArrayList<>();
    sectors.add(new Sector(1, null, "", null));
    final JobCalculation job = new JobCalculation(1, sectors);
    calculations.add(job);
    queue.init(calculations);
    handler = Mockito.mock(CalculationResultHandler.class);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        handlerCalled.set(true);
        return null;
      }
    }).when(handler).onSectorResults(anyList(), anyInt(), anyInt());
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        handlerCalled.set(true);
        return null;
      }
    }).when(handler).onSectorResults(anyList(), anyInt(), anyInt());
    resultHandler = new ResultHandlerImpl(monitor, queue, handler);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        exception = invocation.getArgumentAt(1, Exception.class);
        return null;
      }
    }).when(monitor).taskFailed(anyString(), any());
  }

  @Test
  public void testSetResultUpdateHandler() {
    final AtomicBoolean resultHandlerCalled = new AtomicBoolean();
    resultHandler.addResultUpdateHandler(new ResultPostProcessHandler() {
      @Override
      public void postProcessSectorResults(final List<AeriusResultPoint> results) {
        resultHandlerCalled.set(true);
      }
    });
    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(new AeriusResultPoint());
    resultHandler.onResult("1", results);
    assertTrue("UpdateHandler should have been called", resultHandlerCalled.get());
    assertNull("No exception should be triggered", exception);
  }

  @Test
  public void testOnResult() throws AeriusException {
    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(new AeriusResultPoint());
    resultHandler.onResult("1", results);
    assertTrue("CalculationResultHandler should have been called", handlerCalled.get());
    assertNull("No exception should be triggered", exception);
  }

  @Test
  public void testException() throws AeriusException {
    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(new AeriusResultPoint());
    Mockito.doThrow(new RuntimeException("Test Exception")).when(handler).onSectorResults(anyList(), anyInt(), anyInt());
    resultHandler.onResult("1", results);
    assertNotNull("Exception should be triggered", exception);
  }

}
