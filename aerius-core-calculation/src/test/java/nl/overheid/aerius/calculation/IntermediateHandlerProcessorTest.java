/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.taskmanager.client.WorkerIntermediateResultSender;

/**
 * Test class for {@link IntermediateHandlerProcessor}.
 */
public class IntermediateHandlerProcessorTest {

  private WorkerIntermediateResultSender sender;
  private Serializable callbackResult;
  private IntermediateHandlerProcessor ihp;
  private Thread thread;

  @Before
  public void before() {
    sender = new WorkerIntermediateResultSender() {
      @Override
      public void sendIntermediateResult(final Serializable result) throws IOException {
        callbackResult = result;
      }
    };
    ihp = new IntermediateHandlerProcessor(sender, 1);
    thread = new Thread(ihp);
  }

  @After
  public void after() throws InterruptedException {
    if (thread.isAlive()) {
      thread.interrupt();
    }
  }

  @Test(timeout = 1000)
  public void testSendResults() throws InterruptedException {
    final String resultString = "AERIUS";
    final Semaphore semaphore = new Semaphore(0);
    final AtomicBoolean send = new AtomicBoolean(false);
    ihp.add(new IntermediateResultHandler() {
      @Override
      public Serializable get() {
        if (send.get()) { // in second call release semaphore
          semaphore.release();
          return null;
        } else {
          send.set(true); // in first call mark as result returned.
          return resultString;
        }
      }
    });
    thread.start();
    // wait for release. This happens in second call to get, first get sets callback.
    // this is in second call so we know for sure results has been processed in first call.
    semaphore.acquire();
    ihp.onFinish(CalculationState.CANCELLED);
    thread.join();
    assertEquals("Intermediate result should have been send", resultString, callbackResult);
  }

  @Test(timeout = 1000)
  public void testSendResultsOnFinish() throws InterruptedException {
    final String resultString1 = "AERIUS_1";
    final String resultString2 = "AERIUS_2";
    final AtomicInteger send = new AtomicInteger(0);
    final Semaphore semaphore = new Semaphore(0);
    ihp.add(new IntermediateResultHandler() {
      @Override
      public Serializable get() {
        if (ihp.isRunning()) { // run this while running
          return resultString1;
        } else if (send.get() == 1) { // when running finished wait for #onFinish to be processed
          semaphore.release();
          send.set(2); // set to new value to value through null in next run.
          return resultString2;
        } else if (send.get() == 2) { // this means it was run after running completed.
          return null;
        } else {
          return resultString1;
        }
      }
    });
    thread.start();
    ihp.onFinish(CalculationState.COMPLETED);
    send.set(1);
    semaphore.acquire();
    thread.join();
    assertEquals("Intermediate result should have been send in onFinish", resultString2, callbackResult);
  }

  @Test(timeout = 1000)
  public void testFinish() throws InterruptedException {
    thread.start();
    assertTrue("Thread should be alive", thread.isAlive());
    ihp.onFinish(CalculationState.COMPLETED);
    thread.join();
    assertFalse("Thread should be dead", thread.isAlive());
  }
}
