/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link InlandShipExpander}.
 */
public class InlandShipExpanderTest extends BaseDBTest {

  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);

  private static SourceConverter geometryExpander;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    geometryExpander = new OPSSourceConverter(getCalcPMF().getConnection());
  }

  @Test
  public void testInlandMooringShipsToPoints() throws SQLException, AeriusException {
    final GenericEmissionSource emissionSource = new GenericEmissionSource();
    emissionSource.setId(1);
    final List<EmissionSource> sources = new ArrayList<>();
    sources.add(emissionSource);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, sources, KEY_NOX.hatch());
    // TODO add usefull test cases.
  }
}
