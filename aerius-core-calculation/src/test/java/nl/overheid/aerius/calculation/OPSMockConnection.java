/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;

import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.shared.domain.ops.OPSReceptor;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.CalculationResult;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.taskmanager.client.MockChannel;
import nl.overheid.aerius.taskmanager.client.MockConnection;
import nl.overheid.aerius.taskmanager.client.util.QueueHelper;

/**
 * Mocking connection for OPS related workers.
 * Any OPSInputData send through will result in random results for each receptor up to a maximum value.
 */
public class OPSMockConnection extends MockConnection {

  private static final double MINIMAL_DEPOSITION = 0.0001;
  private double resultFactor;
  private final double resultDecrementFactor;

  public OPSMockConnection(final double resultFactor) {
    this(resultFactor, 0.7);
  }

  public OPSMockConnection(final double resultFactor, final double resultDecrementFactor) {
    this.resultFactor = resultFactor;
    this.resultDecrementFactor = resultDecrementFactor;
  }

  @Override
  public Channel createChannel() throws IOException {
    return new OPSReceptorResultsMockChannel();
  }

  /**
   * Mocks a Channel to return results that with each run decrease in value. This can be used to mock deposition results in unit tests. Given the
   * points are with each run at a large distance to the sources.
   */
  private class OPSReceptorResultsMockChannel extends MockChannel {

    @Override
    protected byte[] mockResults(final BasicProperties properties, final byte[] body) {
      synchronized (OPSReceptorResultsMockChannel.class) {
        try {
          final OPSInputData input = (OPSInputData) QueueHelper.bytesToObject(body);
          final CalculationResult cr = new CalculationResult();
          final ArrayList<AeriusResultPoint> results = new ArrayList<>();
          for (final OPSReceptor receptor : input.getReceptors()) {
            results.add(toResultPoint(receptor, resultFactor, input.getEmissionResultKeys()));
          }
          cr.setResults(results);
          resultFactor = Math.max(MINIMAL_DEPOSITION, resultFactor * resultDecrementFactor);

          return QueueHelper.objectToBytes(cr);
        } catch (final IOException | ClassNotFoundException e) {
          return null;
        }
      }
    }

    private AeriusResultPoint toResultPoint(final OPSReceptor receptor, final double resultFactor, final Set<EmissionResultKey> keys) {
      final AeriusResultPoint resultPoint = new AeriusResultPoint(receptor.getId(), receptor.getPointType(), receptor.getX(), receptor.getY());
      for (final EmissionResultKey resultKey : keys) {
        resultPoint.setEmissionResult(resultKey, resultFactor);
      }
      return resultPoint;
    }
  }
}
