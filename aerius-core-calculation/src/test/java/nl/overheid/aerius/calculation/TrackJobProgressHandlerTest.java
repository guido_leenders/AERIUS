/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import nl.overheid.aerius.db.scenario.JobRepository;
import nl.overheid.aerius.db.scenario.ScenarioUserRepository;
import nl.overheid.aerius.shared.domain.calculation.CalculationState;
import nl.overheid.aerius.shared.domain.scenario.JobProgress;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.ScenarioUser;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link TrackJobProgressHandler}.
 */
public class TrackJobProgressHandlerTest extends CalculationTestBase {

  private static final String TEST_EMAIL = "aerius@example.com";
  private static final String CORRELATION_ID = "1234";
  private static final String BAD_CORRELATION_ID = "4321";
  private static final Date BEFORE_ALL_TIME = new Date();

  private int jobId;
  private TrackJobProgressHandler handler;
  private int calculationId;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    final ScenarioUser user = createUser();
    jobId = JobRepository.createJob(getCalcConnection(), user, JobType.CALCULATION, CORRELATION_ID);
    handler = initHandler(CORRELATION_ID);
  }

  @Test
  public void testInit() throws SQLException {
    assertTrue("Job should be found", handler.isJobFound());
    assertTrue("JobId should be set:" + jobId, jobId > 0);
    final JobProgress progress = JobRepository.getProgress(getCalcConnection(), CORRELATION_ID);
    assertTrue("CreationDateTime should be later than the start time of this test", progress.getCreationDateTime().after(BEFORE_ALL_TIME));
  }

  @Test
  public void testOnTotalResults() throws AeriusException, SQLException {
    handler.onTotalResults(IntermediateResultHandlerUtil.createResults(), calculationId);
    final JobProgress progress = JobRepository.getProgress(getCalcConnection(), CORRELATION_ID);
    assertEquals("Hexagon count should be matching created results", 3, progress.getHexagonCount());
  }

  @Test
  public void testOnFinishWrongId() throws SQLException, AeriusException {
    handler = initHandler(BAD_CORRELATION_ID);
    // the handler should not crash!
    handler.onFinish(CalculationState.COMPLETED);
  }

  private TrackJobProgressHandler initHandler(final String correlationId) throws AeriusException, SQLException {
    final TrackJobProgressHandler handler = new TrackJobProgressHandler(getCalcPMF(), getExampleCalculationJob(), correlationId);
    handler.init();

    return handler;
  }

  private ScenarioUser createUser() throws SQLException, AeriusException {
    final ScenarioUser createUser = new ScenarioUser();
    createUser.setApiKey(TEST_EMAIL);
    createUser.setEmailAddress(TEST_EMAIL);
    createUser.setEnabled(true);

    ScenarioUserRepository.createUser(getCalcConnection(), createUser);
    return ScenarioUserRepository.getUserByEmailAddress(getCalcConnection(), TEST_EMAIL);
  }
}
