/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import nl.overheid.aerius.shared.domain.calculation.PartialCalculationResult;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link PartialCalculationResultHandler}.
 */
public class PartialCalculationResultHandlerTest {

  private static final int CALCULATION_ID = 33;
  private static final String CORRElATION_ID = "1";
  private static final int MAX_RECEPTORS = 2;

  @Test
  public void testWithNoFilter() throws AeriusException {
    final PartialCalculationResultHandler handler = new PartialCalculationResultHandler(CORRElATION_ID, MAX_RECEPTORS, new IncludeAllResultsFilter());
    final PartialCalculationResult result1 = addAndGetResults(handler);
    assertEquals("CalculationId should be set with no filter", CALCULATION_ID, result1.getCalculationId());
    assertEquals("Size of results with no filter", 2, result1.getResults().size());
    assertEquals("Size of results with second call with no filter", 1, ((PartialCalculationResult) handler.get()).getResults().size());
  }

  @Test
  public void testWithFilter() throws AeriusException {
    final PartialCalculationResultHandler handler = new PartialCalculationResultHandler(CORRElATION_ID, MAX_RECEPTORS,
        r -> r.getEmissionResult(IntermediateResultHandlerUtil.KEY) > 1.0);
    final PartialCalculationResult result = addAndGetResults(handler);
    assertEquals("CalculationId should be set with filter", CALCULATION_ID, result.getCalculationId());
    assertEquals("Size of results with filter", 2, result.getResults().size());
    assertNull("No results with second call with filter", handler.get());
  }

  /**
   * Test if all results are returned. The first set of results are filtered out, so it should not stop.
   */
  @Test
  public void testGetAll() throws AeriusException {
    final PartialCalculationResultHandler handler = new PartialCalculationResultHandler(CORRElATION_ID, MAX_RECEPTORS,
        r -> r.getEmissionResult(IntermediateResultHandlerUtil.KEY) > 1.0);
    final List<AeriusResultPoint> values = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      IntermediateResultHandlerUtil.addResult(values, i, i, 0.01);
    }
    for (int i = 0; i < 10; i++) {
      IntermediateResultHandlerUtil.addResult(values, i, i, 10);
    }
    handler.onTotalResults(values, CALCULATION_ID);
    int cnt = 0;
    boolean found = true;
    while (found) {
      final PartialCalculationResult result = (PartialCalculationResult) handler.get();
      if (result == null) {
        found = false;
      } else {
        cnt += result.getResults().size();
      }
    }
    assertEquals("All results inserted should be retrieved", 10, cnt);
  }

  private PartialCalculationResult addAndGetResults(final PartialCalculationResultHandler handler) throws AeriusException {
    handler.onTotalResults(IntermediateResultHandlerUtil.createResults(), CALCULATION_ID);
    final PartialCalculationResult result = (PartialCalculationResult) handler.get();
    return result;
  }
}
