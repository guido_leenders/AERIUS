/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.grid;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;

/**
 * Test class for {@link GridUtil}.
 */
public class GridUtilTest extends BaseDBTest {

  private static final GridSettings ZOOM_LEVELS = new GridSettings(RECEPTOR_GRID_SETTINGS);
  private static final GridZoomLevel ZOOM_LEVEL_1 = ZOOM_LEVELS.levelFromLevel(1);
  private static final GridZoomLevel ZOOM_LEVEL_2 = ZOOM_LEVELS.levelFromLevel(2);
  private static final GridZoomLevel ZOOM_LEVEL_3 = ZOOM_LEVELS.levelFromLevel(3);
  private static final GridZoomLevel ZOOM_LEVEL_4 = ZOOM_LEVELS.levelFromLevel(4);
  private static final GridZoomLevel ZOOM_LEVEL_5 = ZOOM_LEVELS.levelFromLevel(5);
  private static final GridZoomLevel ZOOM_LEVEL_MAX = ZOOM_LEVELS.getZoomLevelMax();
  private static final GridUtil GRID_UTIL = new GridUtil(ZOOM_LEVELS);

  @Test
  public void test2AndFromCell() {
    final Point[] points = { new Point(0, 96000), new Point(96000, 0), new Point(96000, 96000) };
    for (final Point p : points) {
      assertEquals("Should be same point", p, GRID_UTIL.getPositionFromCell(GRID_UTIL.getCellFromPosition(p, ZOOM_LEVEL_MAX)));
    }
  }

  @Test
  public void testSurrounding1() {
    final Collection<Integer> surrounding1 = GRID_UTIL.getSurrounding(1, 0, ZOOM_LEVEL_2);
    assertSize(surrounding1, 1);
    assertContains(surrounding1, 1);
  }

  @Test
  public void testSurrounding2() {
    final Collection<Integer> surrounding2 = GRID_UTIL.getSurrounding(1, 500, ZOOM_LEVEL_2);
    assertSize(surrounding2, 4);
    assertContains(surrounding2, 1, 3, 1281, 1283);
  }

  @Test
  public void testSurrounding3() {
    final Collection<Integer> surrounding3 = GRID_UTIL.getSurrounding(1281, 250, ZOOM_LEVEL_2);
    assertSize(surrounding3, 6);
    assertContains(surrounding3, 1, 3, 1283, 1281, 2561, 2563);
  }

  @Test
  public void testSurrounding4() {
    final Collection<Integer> surrounding4 = GRID_UTIL.getSurrounding(640, 500, ZOOM_LEVEL_1);
    assertSize(surrounding4, 4);
    assertContains(surrounding4, 639, 640, 1279, 1280);
  }

  @Test
  public void testSurrounding5() {
    final Collection<Integer> surrounding5 = GRID_UTIL.getSurrounding(1, 1750, ZOOM_LEVEL_1);
    assertSize(surrounding5, 16);
    assertContains(surrounding5, 1, 2, 3, 4);
    assertContains(surrounding5, 641, 642, 643, 644);
    assertContains(surrounding5, 1281, 1282, 1283, 1284);
    assertContains(surrounding5, 1921, 1922, 1923, 1924);
  }

  @Test
  public void testSurrounding6() {
    final Collection<Integer> surrounding6 = GRID_UTIL.getSurrounding(1, 10000, ZOOM_LEVEL_3);
    assertSize(surrounding6, 25);
    assertContains(surrounding6, 1, 5, 9, 13, 17);
    assertContains(surrounding6, 2561, 2565, 2569, 2573, 2577);
    assertContains(surrounding6, 5121, 5125, 5129, 5133, 5137);
    assertContains(surrounding6, 7681, 7685, 7689, 7693, 7697);
    assertContains(surrounding6, 10241, 10245, 10249, 10253, 10257);
  }

  @Test
  public void testSurrounding7() {
    final Collection<Integer> surrounding1 = GRID_UTIL.getSurrounding(1, 250, ZOOM_LEVEL_1);
    assertSize(surrounding1, 4);
    assertContains(surrounding1, 1, 2, 641, 642);
  }

  @Test
  public void testSurrounding8() {
    final Collection<Integer> surrounding4 = GRID_UTIL.getSurrounding(639, 500, ZOOM_LEVEL_2);
    assertSize(surrounding4, 4);
    assertContains(surrounding4, 637, 639, 1917, 1919);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSurrounding9() {
    final Collection<Integer> surrounding9 = GRID_UTIL.getSurrounding(2, 0, ZOOM_LEVEL_2);
  }

  @Test
  public void testInnerLevel1Var1() {
    final Collection<Integer> inner = GRID_UTIL.getInnerCells(1, ZOOM_LEVEL_1);

    assertSize(inner, 4);
    assertContains(inner, 1, 2, 641, 642);
  }

  @Test
  public void testInnerIsValidCellHorizontal() {
    Assert.assertTrue("Cell 1 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 1 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_2)); // 1
    Assert.assertTrue("Cell 1 is expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_3)); // 1
    Assert.assertTrue("Cell 1 is expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_4)); // 1
    Assert.assertTrue("Cell 1 is expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(1, ZOOM_LEVEL_5)); // 1

    Assert.assertTrue("Cell 2 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 2 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 2 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 2 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 2 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(2, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 3 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 3 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_2)); // 1
    Assert.assertFalse("Cell 3 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 3 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 3 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(3, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 4 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 4 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 4 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 4 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 4 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(4, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 5 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 5 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_2)); // 1
    Assert.assertTrue("Cell 5 is expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_3)); // 1
    Assert.assertFalse("Cell 5 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 5 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(5, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 6 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 6 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 6 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 6 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 6 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(6, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 7 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 7 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_2)); // 1
    Assert.assertFalse("Cell 7 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 7 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 7 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(7, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 8 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 8 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 8 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 8 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 8 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(8, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 9 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 9 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_2)); // 1
    Assert.assertTrue("Cell 9 is expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_3)); // 1
    Assert.assertTrue("Cell 9 is expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_4)); // 1
    Assert.assertFalse("Cell 9 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(9, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 10 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 10 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 10 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 10 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 10 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(10, ZOOM_LEVEL_5)); // 0
  }

  @Test
  public void testInnerIsValidCellVertical() {
    Assert.assertTrue("Cell 641 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 641 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 641 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 641 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 641 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(641, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 1281 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 1281 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_2)); // 1
    Assert.assertFalse("Cell 1281 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 1281 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 1281 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(1281, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 1921 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_1)); // 1
    Assert.assertFalse("Cell 1921 is NOT expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_2)); // 0
    Assert.assertFalse("Cell 1921 is NOT expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_3)); // 0
    Assert.assertFalse("Cell 1921 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 1921 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(1921, ZOOM_LEVEL_5)); // 0

    Assert.assertTrue("Cell 2561 is expected to exist on zoom level 1.", GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_1)); // 1
    Assert.assertTrue("Cell 2561 is expected to exist on zoom level 2.", GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_2)); // 1
    Assert.assertTrue("Cell 2561 is expected to exist on zoom level 3.", GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_3)); // 1
    Assert.assertFalse("Cell 2561 is NOT expected to exist on zoom level 4.", GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_4)); // 0
    Assert.assertFalse("Cell 2561 is NOT expected to exist on zoom level 5.", GRID_UTIL.checkIsValidCell(2561, ZOOM_LEVEL_5)); // 0
  }

  @Test
  public void testInnerLevel2() {
    final Collection<Integer> inner = GRID_UTIL.getInnerCells(1, ZOOM_LEVEL_2);

    assertSize(inner, 4);
    assertContains(inner, 1, 3, 1281, 1283);
  }

  private void assertSize(final Collection<Integer> surrounding, final int expectedSize) {
    Assert.assertEquals("Surrounding size different", expectedSize, surrounding.size());
  }

  private void assertContains(final Collection<Integer> surrounding, final int... contains) {
    for (final int value : contains) {
      Assert.assertTrue("Surrounding cell should contain cell " + value + " from(" + Arrays.toString(surrounding.toArray()) + ")",
          surrounding.contains(value));
    }
  }

  @Test
  public void testIdDerivation() {
    Assert.assertEquals("ID Should be 1", 1, GRID_UTIL.getCellFromPosition(new Point(0, 624000), ZOOM_LEVEL_1));
    Assert.assertEquals("ID Should be 1", 1, GRID_UTIL.getCellFromPosition(new Point(0, 624000), ZOOM_LEVEL_2));
    Assert.assertEquals("ID Should be 1", 1, GRID_UTIL.getCellFromPosition(new Point(0, 624000), ZOOM_LEVEL_3));

    Assert.assertEquals("ID Should be 1", 2, GRID_UTIL.getCellFromPosition(new Point(500, 624000), ZOOM_LEVEL_1));
    Assert.assertEquals("ID Should be 11", 11, GRID_UTIL.getCellFromPosition(new Point(5000, 624000), ZOOM_LEVEL_2));
    Assert.assertEquals("ID Should be 49", 49, GRID_UTIL.getCellFromPosition(new Point(25000, 624000), ZOOM_LEVEL_3));
    Assert.assertEquals("ID Should be 33", 33, GRID_UTIL.getCellFromPosition(new Point(25000, 624000), ZOOM_LEVEL_MAX));
    Assert.assertEquals("ID Should be 33", 33, GRID_UTIL.getCellFromPosition(new Point(20000, 624000), ZOOM_LEVEL_MAX));

    Assert.assertEquals("ID should be 1", 1, GRID_UTIL.getCellFromPosition(new Point(0, 624000), ZOOM_LEVEL_1));
    Assert.assertEquals("ID should be 5121", 5121, GRID_UTIL.getCellFromPosition(new Point(0, 620000), ZOOM_LEVEL_2));
    Assert.assertEquals("ID should be 30721", 30721, GRID_UTIL.getCellFromPosition(new Point(0, 599000), ZOOM_LEVEL_3));
  }

  @Test
  public void testMinDistance() {
    Assert.assertEquals("Min distance invalid", 500D, GRID_UTIL.getMinDistance(1, 3, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0D);
    Assert.assertEquals("Min distance invalid", 149000D, GRID_UTIL.getMinDistance(1, 300, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0D);

    Assert.assertEquals("Min distance invalid", 1500D, GRID_UTIL.getMinDistance(6, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D);
    Assert.assertEquals("Min distance invalid", 4000D, GRID_UTIL.getMinDistance(11, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D);
    Assert.assertEquals("Min distance invalid", 154000D, GRID_UTIL.getMinDistance(311, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D);
    Assert.assertEquals("Min distance invalid", 114000D, GRID_UTIL.getMinDistance(1511, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D);
  }

  @Test
  public void testMaxDistance() {
    Assert.assertEquals("Max distance should be 1", 500D, GRID_UTIL.getMaxDistance(1, 2, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0D);
    Assert.assertEquals("Max distance should be 1", 149500D, GRID_UTIL.getMaxDistance(1, 300, ZOOM_LEVEL_1, ZOOM_LEVEL_1), 0.1D);

    Assert.assertEquals("Max distance should be 1", 2500D, GRID_UTIL.getMaxDistance(6, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D);
    Assert.assertEquals("Max distance should be 1", 2500D, GRID_UTIL.getMaxDistance(6, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_2), 0D);
    Assert.assertEquals("Max distance should be 1", 12500D, GRID_UTIL.getMaxDistance(26, 1, ZOOM_LEVEL_1, ZOOM_LEVEL_3), 0D);
  }

  @Test
  public void testIsOutside() {
    Assert.assertTrue("Cell 1 should be outside 6 on zl 1", GRID_UTIL.isFullyOutside(1, 6, 0, ZOOM_LEVEL_2, ZOOM_LEVEL_1));
    Assert.assertTrue("Cell 1 should be outside 6 on zl 2", GRID_UTIL.isFullyOutside(1, 6, 0, ZOOM_LEVEL_2, ZOOM_LEVEL_2));
    Assert.assertTrue("Cell 1 should be outside 6 on zl 3", GRID_UTIL.isFullyOutside(1, 6, 0, ZOOM_LEVEL_2, ZOOM_LEVEL_3));

    Assert.assertFalse("Cell 7 should NOT be fully outside 7 on zl 1", GRID_UTIL.isFullyOutside(7, 6, 1000, ZOOM_LEVEL_1, ZOOM_LEVEL_1));
    Assert.assertFalse("Cell 7 should NOT be fully outside 7 on zl 1", GRID_UTIL.isFullyOutside(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_1));
    Assert.assertFalse("Cell 7 should NOT be fully outside 7 on zl 2", GRID_UTIL.isFullyOutside(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_2));
    Assert.assertFalse("Cell 7 should NOT be fully outside 7 on zl 3", GRID_UTIL.isFullyOutside(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_3));
  }

  @Test
  public void testIsFullyEncapsulated() {
    Assert.assertTrue("Cell 7 should NOT be fully outside 7 on zl 1", GRID_UTIL.isFullyEncapsulated(7, 6, 1000, ZOOM_LEVEL_1, ZOOM_LEVEL_1));
    Assert.assertTrue("Cell 7 should NOT be fully outside 7 on zl 1", GRID_UTIL.isFullyEncapsulated(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_1));
    Assert.assertTrue("Cell 7 should NOT be fully outside 7 on zl 2", GRID_UTIL.isFullyEncapsulated(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_2));
    Assert.assertTrue("Cell 7 should NOT be fully outside 7 on zl 3", GRID_UTIL.isFullyEncapsulated(7, 6, 10000, ZOOM_LEVEL_1, ZOOM_LEVEL_3));
  }
}
