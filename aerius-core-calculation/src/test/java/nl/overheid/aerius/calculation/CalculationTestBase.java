/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import nl.overheid.aerius.calculation.base.CalculatorProfileFactory;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.CalculatorOptions;
import nl.overheid.aerius.calculation.profile.pas.PASCalculatorFactory;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.calculation.CalculatedSingle;
import nl.overheid.aerius.shared.domain.calculation.CalculationSetOptions;
import nl.overheid.aerius.shared.domain.calculation.CalculationType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.SectorGroup;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Base class for testing classes in the calculation package.
 */
public class CalculationTestBase extends BaseDBTest {

  protected static final int NUMBER_OF_RECEPTORS = 100;

  protected CalculatorProfileFactory profileFactory;

  @Override
  @Before
  public void setUp()throws Exception {
    super.setUp();
    profileFactory = new PASCalculatorFactory(getCalcPMF());
  }

  protected CalculationJob getExampleCalculationJob() throws SQLException, AeriusException {
    //add sources
    final EmissionSourceList emissionSources = createSources();

    final ArrayList<AeriusResultPoint> calculationPoints = new ArrayList<>();
    //add receptors
    for (int i = 1; i <= NUMBER_OF_RECEPTORS; i++) {
      final AeriusResultPoint calculationPoint = new AeriusResultPoint();
      calculationPoint.setId(i);
      RECEPTOR_UTIL.setAeriusPointFromId(calculationPoint);
      calculationPoints.add(calculationPoint);
    }
    final CalculationJob result = createCalculationJob(emissionSources, calculationPoints);
    return result;
  }

  protected EmissionSourceList createSources() {
    final EmissionSourceList emissionSources = new EmissionSourceList();

    addSources(emissionSources, 1800, 50);
    final SRM2NetworkEmissionSource network = new SRM2NetworkEmissionSource();
    addSources(network.getEmissionSources(), 3111, 25);
    addSources(network.getEmissionSources(), 3112, 25);
    emissionSources.add(network);
    return emissionSources;
  }

  private void addSources(final ArrayList<EmissionSource> arrayList, final int sectorId, final int nrOfSources) {
    for (int i = 1; i <= nrOfSources; i++) {
      final GenericEmissionSource emissionSource = new GenericEmissionSource(1, new Point(1000000 + 1000 * i, 2000000 + 1000 * i));
      emissionSource.setEmission(Substance.NH3, 1000);
      emissionSource.setEmission(Substance.NOX, 1000);
      emissionSource.setEmission(Substance.PM10, 1000);
      emissionSource.setSector(new Sector(sectorId, SectorGroup.OTHER, "", null));
      arrayList.add(emissionSource);
    }
  }

  protected CalculationJob createCalculationJob(final EmissionSourceList emissionSources, final List<AeriusResultPoint> calculationPoints)
      throws SQLException, AeriusException {
    final CalculatedSingle scenario = new CalculatedSingle();
    scenario.setSources(emissionSources);
    scenario.getCalculationPoints().addAll(calculationPoints);
    final CalculationSetOptions options = new CalculationSetOptions();
    options.setCalculationType(CalculationType.CUSTOM_POINTS);
    options.setCalculateMaximumRange(1000);
    scenario.setOptions(options);

    scenario.getCalculation().setYear(2020);
    scenario.getOptions().getSubstances().add(Substance.NOXNH3);
    scenario.getOptions().getSubstances().add(Substance.PM10);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.NOXNH3_DEPOSITION);
    scenario.getOptions().getEmissionResultKeys().add(EmissionResultKey.PM10_CONCENTRATION);
    final CalculatorOptions co = CalculatorOptionsFactory.initWorkerCalculationOptions(getCalcConnection());
    co.setMaxConcurrentChunks(1);
    return InitCalculation.initCalculation(getCalcConnection(), profileFactory, scenario, "calculator", co, true, "1");
  }
}
