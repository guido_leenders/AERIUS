/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import nl.overheid.aerius.TestDomain;
import nl.overheid.aerius.calculation.base.SourceConverter;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.profile.pas.PASGeometryExpander;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.EmissionValueKey;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.Substance;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.sector.category.RoadType;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.domain.source.MaritimeMooringEmissionSource;
import nl.overheid.aerius.shared.domain.source.OffRoadMobileEmissionSource;
import nl.overheid.aerius.shared.domain.source.PlanEmissionSource;
import nl.overheid.aerius.shared.domain.source.SRM2NetworkEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Unit test for {@link EngineSourceExpander}
 */
public class EngineSourceExpanderTest extends BaseDBTest {

  private static final EmissionValueKey KEY_NH3 = new EmissionValueKey(Substance.NH3);
  private static final EmissionValueKey KEY_NOX = new EmissionValueKey(Substance.NOX);
  private static final EmissionValueKey KEY_NOX_NH3 = new EmissionValueKey(Substance.NOXNH3);

  private static SourceConverter geometryExpander;
  private static TestDomain testDomain;

  @BeforeClass
  public static void setUpBeforeClass() throws IOException, SQLException {
    BaseDBTest.setUpBeforeClass();
    testDomain = new TestDomain(getCalcPMF());
    geometryExpander = new PASGeometryExpander(getCalcPMF().getConnection(), new DefaultCalculationEngineProvider());
  }

  @Test
  public void testMobileSourceToPoints() throws SQLException, AeriusException {
    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    emissionSource.setId(1);
    emissionSource.setX(4);
    emissionSource.setY(5);
    final EmissionValueKey evk = TestDomain.EVK_WITH_YEAR_NOX;

    emissionSource.setGeometry(new WKTGeometry(emissionSource.toWKT()));
    testDomain.getOffRoadMobileEmissionSource(emissionSource);

    final List<EmissionSource> sources = new ArrayList<>();
    sources.add(emissionSource);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, sources, evk.hatch());
    assertEquals("Number of point sources", 2, pointSources.size());
    boolean foundCustom = false;
    for (final EngineSource ps : pointSources) {
      final OPSSource es = (OPSSource) ps;
      if (es.getEmission(evk.getSubstance()) == 101010) {
        assertFalse("Should only find it once", foundCustom);
        foundCustom = true;
        assertEquals("Emission height custom source", 10, es.getEmissionHeight(), 0.001);
        assertEquals("Spread custom source", 5, es.getSpread(), 0.001);
        assertEquals("Heat content custom source", 20, es.getHeatContent(), 0.001);
      } else {
        assertEquals("Emission height non-custom source", 0, es.getEmissionHeight(), 0.001);
        assertEquals("Spread custom non-source", 0, es.getSpread(), 0.001);
        assertEquals("Heat content non-custom source", 0, es.getHeatContent(), 0.001);
      }
    }
    assertTrue("Should have found the custom source", foundCustom);
  }

  @Test
  public void testMobileSourceOPSDefaults() throws SQLException, AeriusException {
    final Sector sector = testDomain.getCategories().getSectorById(3220);
    final OffRoadMobileEmissionSource emissionSource = new OffRoadMobileEmissionSource();
    emissionSource.setId(1);
    emissionSource.setX(4);
    emissionSource.setY(5);
    emissionSource.setSector(sector);

    final EmissionValueKey evk = TestDomain.EVK_WITH_YEAR_NOX;

    emissionSource.setGeometry(new WKTGeometry(emissionSource.toWKT()));
    testDomain.getOffRoadMobileToolsEmissionSource(emissionSource);

    final List<EmissionSource> sourceList = new ArrayList<>();
    sourceList.add(emissionSource);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, sourceList, evk.hatch());
    assertEquals("Number of point sources", 2, pointSources.size());
    boolean foundCustom = false;
    for (final EngineSource ps : pointSources) {
      final OPSSource es = (OPSSource) ps;
      if (es.getEmission(evk.getSubstance()) == 101010) {
        assertFalse("Should only find it once", foundCustom);
        foundCustom = true;
        assertEquals("Emission height custom source", 10, es.getEmissionHeight(), 0.001);
        assertEquals("Spread custom source", 5, es.getSpread(), 0.001);
        assertEquals("Heat content custom source", 20, es.getHeatContent(), 0.001);
        assertEquals("Particle size distribution", sector.getDefaultCharacteristics().getParticleSizeDistribution(), es.getParticleSizeDistribution(),
            0.001);
      } else {
        assertEquals("Emission height non-custom source", sector.getDefaultCharacteristics().getEmissionHeight(), es.getEmissionHeight(), 0.001);
        assertEquals("Spread custom non-source", sector.getDefaultCharacteristics().getSpread(), es.getSpread(), 0.001);
        assertEquals("Heat content non-custom source", sector.getDefaultCharacteristics().getHeatContent(), es.getHeatContent(), 0.001);
        assertEquals("Particle size distribution", sector.getDefaultCharacteristics().getParticleSizeDistribution(), es.getParticleSizeDistribution(),
            0.001);
      }
    }
    assertTrue("Should have found the custom source", foundCustom);
  }

  @Test
  public void testPlanToPoints() throws SQLException, AeriusException {
    final PlanEmissionSource emissionSource = testDomain.getPlanEmissionSource();
    emissionSource.setId(1);
    emissionSource.setX(4);
    emissionSource.setY(5);
    emissionSource.setGeometry(new WKTGeometry(emissionSource.toWKT()));
    assertEquals("Should be 2 plan emission values", 2, emissionSource.getEmissionSubSources().size());

    final List<EmissionSource> sources = new ArrayList<>();
    sources.add(emissionSource);
    final List<EngineSource> pointSources = EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, sources, KEY_NOX_NH3.hatch());
    assertEquals("Number of point sources", 1, pointSources.size());
    double emissionNH3 = 0.0;
    double emissionNOx = 0.0;
    for (final EngineSource es : pointSources) {
      emissionNH3 += es.getEmission(KEY_NH3.getSubstance());
      emissionNOx += es.getEmission(KEY_NOX.getSubstance());
    }
    assertEquals("Emission NH3", emissionSource.getEmission(KEY_NH3), emissionNH3, 0.001);
    assertEquals("Emission NOx", emissionSource.getEmission(KEY_NOX), emissionNOx, 0.001);
  }

  @Test
  public void testSRM2NetworkEmissionSource() throws SQLException, AeriusException {
    final SRM2NetworkEmissionSource network = new SRM2NetworkEmissionSource();
    // Add random sources. We have a SRM2 network, but for the test it doesn't matter we add non road sources.
    network.getEmissionSources().add(testDomain.getFarmEmissionSource());
    network.getEmissionSources().add(testDomain.getPlanEmissionSource());
    final List<EmissionSource> list = new ArrayList<>();
    list.add(network);
    final Map<Sector, List<EngineSource>> sources =
        EngineSourceExpander.toEngineSourcesBySector(getCalcConnection(), geometryExpander, list, KEY_NOX_NH3.hatch());
    assertEquals("There should be 2 sectors in the map", 2, sources.size());
  }

  @Test(expected = AeriusException.class)
  public void testGuardAgainstInvalidSector() throws SQLException, AeriusException {
    final MaritimeMooringEmissionSource emissionSource = new MaritimeMooringEmissionSource();
    emissionSource.setId(1);

    emissionSource.setGeometry(GenericSourceExpanderTest.getExampleLineWKTGeometry());
    final MaritimeMooringEmissionSource source = testDomain.getMooringMaritimeShipEmissionSource(emissionSource);
    // Set the sector to have a none ops calculation engine
    source.setSector(testDomain.getSectorById(RoadType.FREEWAY.getSectorId()));
    final List<EmissionSource> list = new ArrayList<>();
    list.add(source);

    try {
      EngineSourceExpander.toEngineSources(getCalcConnection(), geometryExpander, list, KEY_NOX_NH3.hatch());
    } catch (final AeriusException e) {
      assertEquals("Should throw expected exception", 1016, e.getReason().getErrorCode());
      throw e;
    }
  }

}
