/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.calculation.base.CalculationJobWorkHandler;
import nl.overheid.aerius.calculation.base.WorkHandler;
import nl.overheid.aerius.calculation.conversion.CalculationEngineProvider.DefaultCalculationEngineProvider;
import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.calculation.domain.JobCalculation;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link CustomPointsWorkDistributor}.
 */
public class CustomPointsWorkDistributorTest extends CalculationTestBase {

  @Test
  public void testCalculationPointsChunker() throws SQLException, IOException, InterruptedException, AeriusException, TaskCancelledException {
    final CalculationJob calculationJob = getExampleCalculationJob();
    calculationJob.setProvider(new DefaultCalculationEngineProvider());

    final List<AeriusPoint> points = calculationJob.getCalculatedScenario().getCalculationPoints();
    final AtomicInteger counter = new AtomicInteger();
    final CustomPointsWorkDistributor distributor =
        new CustomPointsWorkDistributor(getCalcPMF(), new GridSettings(RECEPTOR_GRID_SETTINGS), points);
    distributor.distribute(calculationJob, new CalculationJobWorkHandler<>(new WorkHandler<AeriusPoint>() {

      @Override
      public void work(final JobCalculation job, final CalculationJob calculation, final int cellId,
          final Collection<AeriusPoint> calculationPointStore) throws AeriusException, SQLException, InterruptedException, TaskCancelledException {
        counter.addAndGet(calculationPointStore.size());
      }
    }));
    assertEquals("Number of points in should be same as passed to handler", points.size(), counter.get());
  }
}
