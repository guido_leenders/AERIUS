/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import java.util.ArrayList;
import java.util.List;

import nl.overheid.aerius.calculation.base.IntermediateResultHandler;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * Util class to generate data to test {@link IntermediateResultHandler} implementations.
 */
final class IntermediateResultHandlerUtil {

  public static final EmissionResultKey KEY = EmissionResultKey.NOX_DEPOSITION;

  private IntermediateResultHandlerUtil() {
    // util class
  }

  public static List<AeriusResultPoint> createResults() {
    final List<AeriusResultPoint> result = new ArrayList<>();
    addResult(result, 11500, 10000, 2.0);
    addResult(result, 12000, 10000, 3.0);
    addResult(result, 13000, 10000, 0.5);
    return result;
  }

  public static void addResult(final List<AeriusResultPoint> result, final int x, final int y, final double emission) {
    final AeriusResultPoint point = new AeriusResultPoint(x, y);
    point.getEmissionResults().put(KEY, emission);
    result.add(point);
  }

}
