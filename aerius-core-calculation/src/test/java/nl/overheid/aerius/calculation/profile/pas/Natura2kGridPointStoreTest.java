/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Test;

import nl.overheid.aerius.calculation.grid.GridPointStore;
import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.profile.pas.Natura2kGridPointStore;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;

/**
 * Test class for {@link Natura2kGridPointStore}.
 */
public class Natura2kGridPointStoreTest extends BaseDBTest {

  @Test
  public void testSort() {
    final Natura2kGridPointStore store = new Natura2kGridPointStore(RECEPTOR_UTIL, new GridSettings(RECEPTOR_GRID_SETTINGS));
    addPoint(store, 4760704, 1);
    final AeriusPoint level5a = addPoint(store, 4893761, 5);
    assertTrue("", RECEPTOR_UTIL.isReceptorAtZoomLevel(level5a, RECEPTOR_GRID_SETTINGS.getHexagonZoomLevels().get(4)));
    store.add(level5a);
    final AeriusPoint level5b = addPoint(store, 4893769, 5);
    store.sortPointStore();
    final Iterator<Entry<Integer, Collection<AeriusPoint>>> iterator = store.entrySet().iterator();
    final List<AeriusPoint> list = (List<AeriusPoint>) iterator.next().getValue();
    assertEquals("5a should be first", level5a, list.get(0));
    assertEquals("5b should be second", level5b, list.get(1));
  }

  private AeriusPoint addPoint(final GridPointStore store, final int id, final int level) {
    final AeriusPoint point = RECEPTOR_UTIL.setAeriusPointFromId(new AeriusPoint(id));
    store.add(point);
    assertEquals("Receptor on zoom level", level, RECEPTOR_UTIL.getZoomLevelForReceptor(point));
    return point;
  }
}
