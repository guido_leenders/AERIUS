/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.Thread.State;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.calculation.base.WorkMonitor;
import nl.overheid.aerius.calculation.base.WorkSemaphore;
import nl.overheid.aerius.calculation.base.WorkTracker;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;

/**
 * Test class for {@link WorkMonitorImpl}.
 */
public class WorkMonitorImplTest {

  private static final WorkKey WORK_KEY_1 = new WorkKey("", "1", 1, 1);
  private static final WorkKey WORK_KEY_2 = new WorkKey("", "1", 2, 2);
  private static final WorkKey WORK_KEY_3 = new WorkKey("", "1", 3, 3);
  private AtomicBoolean cancelled;
  private WorkTracker tracker;
  private WorkMonitor monitor;

  @Before
  public void before() {
    tracker = Mockito.mock(WorkTracker.class);
    monitor = new WorkMonitorImpl(tracker, new WorkSemaphore() {
      private final Semaphore semaphore = new Semaphore(2);

      @Override
      public void release(final String queueName) {
        semaphore.release();
      }

      @Override
      public void drain() {
        while (semaphore.hasQueuedThreads()) {
          semaphore.release();
        }
      }

      @Override
      public void acquire(final String queueName) throws InterruptedException {
        semaphore.acquire();
      }
    });
    cancelled = new AtomicBoolean();
    Mockito.doAnswer(new Answer<Boolean>() {
      @Override
      public Boolean answer(final InvocationOnMock invocation) throws Throwable {
        return cancelled.get();
      }

    }).when(tracker).isCanceled();
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        cancelled.set(true);
        return null;
      }}).when(tracker).cancel(Matchers.any());
  }

  @Test(timeout = 1000)
  public void testDrain() throws InterruptedException, TaskCancelledException {
    final String taskId1 = monitor.registerTask(WORK_KEY_1);
    monitor.registerTask(WORK_KEY_2);
    final Semaphore stop = new Semaphore(0);
    final Thread thread = new Thread(() -> {
        try {
          monitor.registerTask(WORK_KEY_3);
          stop.release();
        } catch (final TaskCancelledException | InterruptedException e) {
          // ignore this exception
        }
    });
    thread.start();
    while (thread.getState() != State.WAITING) {}
    monitor.drain();
    stop.acquire();
    assertNull("Key should be gone after drain", monitor.getWorkKey(taskId1));
  }

  @Test(timeout = 1000)
  public void testIsCancelled() {
    monitor.taskFailed("", new IllegalArgumentException(""));
    assertTrue("Should report cancelled", monitor.isCancelled());
  }

  @Test(timeout = 1000)
  public void testGetWorkKey() throws InterruptedException, TaskCancelledException {
    assertSame("Key should be same", WORK_KEY_1, monitor.getWorkKey(monitor.registerTask(WORK_KEY_1)));
  }

  @Test(timeout = 1000)
  public void testRegisterTask() throws InterruptedException, TaskCancelledException {
    assertNotNull("Should return a uuid", monitor.registerTask(WORK_KEY_1));
  }

  @Test(expected = TaskCancelledException.class, timeout = 1000)
  public void testRegisterTaskOnCancelled() throws InterruptedException, TaskCancelledException {
    monitor.taskFailed("", new IllegalArgumentException(""));
    monitor.registerTask(WORK_KEY_1);
  }

  @Test(timeout = 1000)
  public void testUnRegisterTask() throws InterruptedException, TaskCancelledException {
    final AtomicInteger counter = new AtomicInteger();
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        counter.getAndIncrement();
        return null;
      }
    }).when(tracker).decrement();
    final String taskId1 = monitor.registerTask(WORK_KEY_1);
    final String taskId2 = monitor.registerTask(WORK_KEY_2);
    final Semaphore stop = new Semaphore(0);
    final AtomicReference<String> taskId3 = new AtomicReference<>();
    final Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          taskId3.set(monitor.registerTask(WORK_KEY_3));
          stop.release();
        } catch (final TaskCancelledException | InterruptedException e) {
          // ignore this exception
        }
      }
    });
    thread.start();
    while (thread.getState() != State.WAITING) {}
    monitor.unRegisterTask(taskId1);
    stop.acquire();
    assertEquals("Decrement should be called once", 1, counter.get());
    monitor.unRegisterTask(taskId2);
    monitor.unRegisterTask(taskId3.get());
    assertEquals("Decrement should be called 3 times", 3, counter.get());
  }
}
