/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;

import org.junit.Test;

import nl.overheid.aerius.calculation.domain.CalculationJob;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.sector.Sector;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link InitCalculation} class.
 */
public class InitCalculationTest extends CalculationTestBase {

  /**
   * Test of calculation of all sectors in sources is correct for SRM2Network sources.
   */
  @Test
  public void testSRM2NetworkSectorInit() throws SQLException, AeriusException {
    final EmissionSourceList networkSources = createSources();
    final CalculationJob cj = createCalculationJob(networkSources, Collections.singletonList(new AeriusResultPoint(1, 1)));

    assertEquals("Sectors inside SRM2 network should be calculated", 3, ((HashSet<Sector>) cj.getCalculations().get(0).getSectors()).size());
  }

}
