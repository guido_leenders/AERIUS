/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import nl.overheid.aerius.taskmanager.client.mq.QueueUpdateHandler;
import nl.overheid.aerius.taskmanager.client.mq.RabbitMQQueueMonitor;

/**
 * Test class for {@link DynamicWorkSemaphore}.
 */
public class DynamicWorkSemaphoreTest {

  private static final int INIT_TASKS_COUNT = 5;
  private static final String QUEUE_NAME = "test";

  @Mock
  private RabbitMQQueueMonitor monitor;

  private DynamicWorkSemaphore semaphore;
  private QueueUpdateHandler handler;

  @Before
  public void before() {
    MockitoAnnotations.initMocks(this);
    semaphore = new DynamicWorkSemaphore(monitor, INIT_TASKS_COUNT);
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        handler = invocation.getArgumentAt(1, QueueUpdateHandler.class);
        return null;
      }
    }).when(monitor).addQueueUpdateHandler(eq(QUEUE_NAME), Matchers.any());
  }

  @Test(timeout = 1000)
  public void testAcquireAndRelease() throws InterruptedException {
    acquireNTimes(INIT_TASKS_COUNT);
    assertEquals("The semaphore should be exhausted", 0, semaphore.availablePermits(QUEUE_NAME));
    semaphore.release(QUEUE_NAME);
    assertEquals("There should be 1 slot available", 1, semaphore.availablePermits(QUEUE_NAME));
    semaphore.acquire(QUEUE_NAME);
    assertEquals("The semaphore should be exhausted again", 0, semaphore.availablePermits(QUEUE_NAME));
    assertNotNull("After release we should get new aquire, and have a handler.", handler);
  }

  @Test(timeout = 1000)
  public void testAddingNewSlot() throws InterruptedException {
    // claim all slots
    acquireNTimes(INIT_TASKS_COUNT);
    assertEquals("The semaphore should be exhausted", 0, semaphore.availablePermits(QUEUE_NAME));
    // This should release a new slot
    onQueueUpdateNTimesMessages(INIT_TASKS_COUNT + 1, 0);
    assertEquals("There should be a new slot available", 4, semaphore.availablePermits(QUEUE_NAME));
    for (int i = 0; i < 4; i++) {
      semaphore.acquire(QUEUE_NAME);
    }
    assertEquals("The semaphore should be exhausted again", 0, semaphore.availablePermits(QUEUE_NAME));
  }

  @Test(timeout = 1000)
  public void testRemovingSlotsNotAcquired() throws InterruptedException {
    semaphore.availablePermits(QUEUE_NAME); // this inits the mock handler
    onQueueUpdateNTimesMessages(INIT_TASKS_COUNT * INIT_TASKS_COUNT, 1);
    assertEquals("There should be 1 slot left", 1, semaphore.availablePermits(QUEUE_NAME));
  }


  @Test(timeout = 1000)
  public void testRemovingSlots() throws InterruptedException {
    // claim some slots
    acquireNTimes(3);
    // This should drain all available permits
    assertEquals("There should be a 2 slot available", 2, semaphore.availablePermits(QUEUE_NAME));
    onQueueUpdateNTimesMessages(INIT_TASKS_COUNT * INIT_TASKS_COUNT, 1);
    assertEquals("The semaphore should be exhausted", 0, semaphore.availablePermits(QUEUE_NAME));
    releaseNTimes(3);
    assertEquals("There should only be 1 slot available", 1, semaphore.availablePermits(QUEUE_NAME));
  }

  @Test(timeout = 1000)
  public void testRemovingSlotsWhenNoFreeSlots() throws InterruptedException {
    // claim all slots
    acquireNTimes(INIT_TASKS_COUNT);
    // This should drain all available permits
    assertEquals("There should be no slots available", 0, semaphore.availablePermits(QUEUE_NAME));
    onQueueUpdateNTimesMessages(INIT_TASKS_COUNT * INIT_TASKS_COUNT, 1);
    releaseNTimes(INIT_TASKS_COUNT);
    assertEquals("There should only be 1 slot available", 1, semaphore.availablePermits(QUEUE_NAME));
  }

  private void onQueueUpdateNTimesMessages(final int times, final int numberOfMessagesReady) {
    for (int i = 0; i < times; i++) {
      handler.onQueueUpdate(QUEUE_NAME, 0, 0, numberOfMessagesReady);
    }
  }

  private void acquireNTimes(final int times) throws InterruptedException {
    for (int i = 0; i < times; i++) {
      semaphore.acquire(QUEUE_NAME);
    }
  }

  private void releaseNTimes(final int times) {
    for (int i = 0; i < times; i++) {
      semaphore.release(QUEUE_NAME);
    }
  }
}
