/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import java.sql.SQLException;
import java.util.Collection;

import org.junit.Test;

import nl.overheid.aerius.calculation.grid.GridSettings;
import nl.overheid.aerius.calculation.profile.pas.Natura2kGridPointStore;
import nl.overheid.aerius.calculation.profile.pas.ReceptorLoader;
import nl.overheid.aerius.db.common.BaseDBTest;

/**
 * Test class for {@link ReceptorLoader}.
 */
public class ReceptorLoaderTest extends BaseDBTest {

  @Test
  public void testFillPointStore() throws SQLException {
    final ReceptorLoader loader = new ReceptorLoader(RECEPTOR_UTIL, new GridSettings(RECEPTOR_GRID_SETTINGS));
    final Natura2kGridPointStore store = loader.fillPointStore(getCalcConnection());
    final int n2kId = 62;
    final Collection<Integer> gridIds = store.getGridIds(n2kId);
    assertNotEquals("Grid id's for natura area should not be empty", 0, gridIds.size());
    assertFalse("Grid id for first grid store for grid in area should not be empty",
        store.getGridCell(gridIds.iterator().next()).isEmpty());
    assertNotEquals("Number of receptors in nature area should not be 0", 0, store.getSize(n2kId));
  }

}
