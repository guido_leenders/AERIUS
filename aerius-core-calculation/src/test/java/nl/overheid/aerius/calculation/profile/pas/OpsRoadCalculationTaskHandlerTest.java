/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.profile.pas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import nl.overheid.aerius.calculation.base.CalculationTaskHandler;
import nl.overheid.aerius.calculation.domain.CalculationTask;
import nl.overheid.aerius.calculation.domain.EmissionSourceListSTRTree;
import nl.overheid.aerius.calculation.domain.TaskCancelledException;
import nl.overheid.aerius.calculation.domain.WorkKey;
import nl.overheid.aerius.db.common.BaseDBTest;
import nl.overheid.aerius.geo.shared.Point;
import nl.overheid.aerius.geo.shared.RDNew;
import nl.overheid.aerius.geo.shared.WKTGeometry;
import nl.overheid.aerius.ops.domain.OPSInputData;
import nl.overheid.aerius.ops.domain.OPSSource;
import nl.overheid.aerius.shared.domain.CalculationEngine;
import nl.overheid.aerius.shared.domain.EngineSource;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.shared.domain.source.GenericEmissionSource;
import nl.overheid.aerius.shared.exception.AeriusException;

/**
 * Test class for {@link OpsRoadCalculationTaskHandler}.
 */
public class OpsRoadCalculationTaskHandlerTest extends BaseDBTest {

  private static final int SRC_CNT = 100;

  @Test
  public void testSplitting() throws AeriusException, InterruptedException, TaskCancelledException, SQLException {
    final EmissionSourceListSTRTree sourcesTree = new EmissionSourceListSTRTree();
    final EmissionSourceList esl = createPoints();
    sourcesTree.add(esl);
    sourcesTree.build();
    final CalculationTaskHandler wrappedHandler = new CalculationTaskHandler() {

      @Override
      public <X extends EngineSource, T extends CalculationTask<X, ?, ?>> void work(final WorkKey workKey, final T task,
          final Collection<AeriusPoint> points) throws AeriusException, InterruptedException, TaskCancelledException {
        assertEquals("", SRC_CNT * SRC_CNT, task.getTaskInput().getEmissionSources().size()
            + ((OPSInputData) task.getTaskInput()).getNearbySources().size());
        assertNotEquals("", SRC_CNT * SRC_CNT, task.getTaskInput().getEmissionSources().size());
        assertNotEquals("", SRC_CNT * SRC_CNT, ((OPSInputData) task.getTaskInput()).getNearbySources().size());
      }
    };
    final OpsRoadCalculationTaskHandler handler = new OpsRoadCalculationTaskHandler(sourcesTree, wrappedHandler);
    final PASCalculationTaskFactory factory = new PASCalculationTaskFactory(RECEPTOR_GRID_SETTINGS);
    final CalculationTask<OPSSource,?,?> task = factory.createTask(CalculationEngine.OPS);
    task.getTaskInput().setEmissionSources(sourcesToEngineSources(esl));
    handler.work(null, task, createAeriusPoints());
  }

  private Collection<OPSSource> sourcesToEngineSources(final EmissionSourceList esl) {
    final Collection<OPSSource> opss = new ArrayList<>();

    esl.stream().forEach(e -> opss.add(new OPSSource(e.getId(), RDNew.SRID, e.getX(), e.getY())));
    return opss;
  }

  private EmissionSourceList createPoints() {
    final EmissionSourceList sources = new EmissionSourceList();

    for (int i = 0; i < SRC_CNT; i++) {
      for (int j = 0; j < SRC_CNT; j++) {
        final GenericEmissionSource source = new GenericEmissionSource(i * SRC_CNT + j, new Point(i * 100 + 1000, j * 100 + 1000));
        source.setGeometry(new WKTGeometry(source.toWKT()));
        sources.add(source);
      }
    }
    return sources;
  }

  private Collection<AeriusPoint> createAeriusPoints() {
    final Collection<AeriusPoint> points = new ArrayList<>();

    for (int i = 0; i < 50; i++) {
      for (int j = 0; j < 50; j++) {
        points.add(new AeriusPoint(i * 100 + j, AeriusPointType.POINT, i * 10 + 1000, j * 10 + 1000));
      }
    }
    return points;
  }
}
