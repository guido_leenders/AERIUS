/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.calculation.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Test;

import nl.overheid.aerius.calculation.CalculationTestBase;
import nl.overheid.aerius.shared.domain.geo.AeriusPoint.AeriusPointType;
import nl.overheid.aerius.shared.domain.result.AeriusResultPoint;
import nl.overheid.aerius.shared.domain.result.EmissionResultKey;

/**
 * The class <code>CalculationResultQueueTest</code> contains tests for the
 * class {@link <code>CalculationResultQueue</code>}.
 */
public class CalculationResultQueueTest extends CalculationTestBase {

  private static final int POINT_ID_1 = 4361388;
  private static final int POINT_X_1 = 130000;
  private static final int POINT_Y_1 = 450000;
  private static final int POINT_ID_2 = 90998;
  private static final int POINT_X_2 = 150000;
  private static final int POINT_Y_2 = 300000;
  private static final int CALCULATION_ID = 5;
  private static final int SECTOR_ID_1 = 30;
  private static final int SECTOR_ID_2 = 40;

  /**
   * Run the boolean hasResults(int) method test
   */
  @Test
  public void testHasResultsEmpty() {
    final CalculationResultQueue queue = new CalculationResultQueue();
    assertFalse("No results on empty", queue.hasResults(CALCULATION_ID));
    queue.addCalculation(CALCULATION_ID, new HashSet<Integer>());
    assertFalse("Still no results on empty", queue.hasResults(CALCULATION_ID));
  }

  @Test(expected = NullPointerException.class)
  public void testPutNotInitialized() {
    new CalculationResultQueue().put(new ArrayList<AeriusResultPoint>(), CALCULATION_ID, SECTOR_ID_1);
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  public void testPut() {
    final CalculationResultQueue queue = init();
    final List<AeriusResultPoint> value = createResults();
    queue.put(value, CALCULATION_ID, SECTOR_ID_1);
    assertFalse("Should not have results", queue.hasResults(CALCULATION_ID));
    queue.put(value, CALCULATION_ID, SECTOR_ID_2);
    assertTrue("Should have results", queue.hasResults(CALCULATION_ID));
  }

  /**
   * Run the void put(CalculationResult, int, int) method test
   */
  @Test
  public void testPollCompleteResults() {
    final CalculationResultQueue queue = init();
    final List<AeriusResultPoint> value = createResults();
    queue.put(value, CALCULATION_ID, SECTOR_ID_1);
    assertTrue("Should return empty", queue.pollTotalResults(CALCULATION_ID).isEmpty());
    queue.put(value, CALCULATION_ID, SECTOR_ID_2);
    assertEquals("Should return results", 2, queue.pollTotalResults(CALCULATION_ID).size());
  }

  private CalculationResultQueue init() {
    final CalculationResultQueue queue = new CalculationResultQueue();
    final Set<Integer> sectors = new HashSet<>();
    sectors.add(SECTOR_ID_1);
    sectors.add(SECTOR_ID_2);
    queue.addCalculation(CALCULATION_ID, sectors);
    return queue;
  }

  private List<AeriusResultPoint> createResults() {
    final List<AeriusResultPoint> results = new ArrayList<>();
    results.add(new AeriusResultPoint(POINT_ID_1, AeriusPointType.RECEPTOR, POINT_X_1, POINT_Y_1));
    results.add(new AeriusResultPoint(POINT_ID_2, AeriusPointType.RECEPTOR, POINT_X_2, POINT_Y_2));
    for (final AeriusResultPoint rp : results) {
      rp.setEmissionResult(EmissionResultKey.NH3_DEPOSITION, new Random().nextDouble());
    }
    return results;
  }
}
