/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.auth.AdminPermission;
import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.context.AdminUserContext;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.wui.admin.context.AdminAppContext;
import nl.overheid.aerius.wui.admin.ui.UserManagementViewImpl.UserManagementEditDriver;
import nl.overheid.aerius.wui.admin.widget.UserManagementEditor;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceCall;
import nl.overheid.aerius.wui.main.util.ConcurrentAsyncServiceUtil.ConcurrentAsyncServiceHandle;
import nl.overheid.aerius.wui.main.widget.validation.ErrorPopupController;

public class UserManagementActivity extends AdminAbstractActivity<AdminAppContext<AdminContext, AdminUserContext>>
    implements UserManagementView.Presenter {

  private static final String ADMIN_RETRIEVE_USERS = "RETRIEVE_USERS";

  private final HashMap<String, ConcurrentAsyncServiceHandle<?>> concurrencyMap = new HashMap<>();

  private final SimpleBeanEditorDriver<AdminUserProfile, UserManagementEditor> userEditDriver = GWT.create(UserManagementEditDriver.class);

  private final UserManagementView view;

  private AdminUserProfile currentUserProfile;

  private final AsyncCallback<Void> userUpdateCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      onRefresh();

      // TODO Specify which user was updated.
      broadcastNotificationMessage(M.messages().adminNotificationUserUpdateComplete());
    }

    @Override
    public void onFailure(final Throwable caught) {
      onRefresh();

      super.onFailure(caught);
    }
  };

  private final AsyncCallback<AdminUserProfile> userCreateCallback = new AppAsyncCallback<AdminUserProfile>() {
    @Override
    public void onSuccess(final AdminUserProfile result) {
      // Add the result to the view and select it
      view.asDataTable().addRowData(result);

      broadcastNotificationMessage(M.messages().adminNotificationUserCreationComplete(result.getName()));

      view.userCreateComplete(true);
    }

    @Override
    public void onFailure(final Throwable caught) {
      view.userCreateComplete(false);

      super.onFailure(caught);
    }
  };

  private final AsyncCallback<Void> userRemovalCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      broadcastNotificationMessage(M.messages().adminNotificationUserRemovalComplete());

      onRefresh();
    }
  };

  private final AsyncCallback<Void> passwordResetCallback = new AppAsyncCallback<Void>() {
    @Override
    public void onSuccess(final Void result) {
      // Squat
    }
  };

  @Inject
  public UserManagementActivity(final UserManagementView view, final AdminAppContext<AdminContext, AdminUserContext> appContext) {
    super(appContext, AdminPermission.ADMINISTER_USERS);

    this.view = view;
  }

  @Override
  public void start(final AcceptsOneWidget panel) {
    panel.setWidget(view);

    userEditDriver.initialize(view.getModificationEditor());

    view.setPresenter(this);

    onRefresh();
  }

  public void onRefresh() {
    onAdaptFilter();
  }

  @Override
  public void onAdaptFilter() {
    final UserManagementFilter filter = appContext.getUserContext().getFilter(UserManagementFilter.class);

    view.asDataTable().clear();

    ConcurrentAsyncServiceUtil.register(ADMIN_RETRIEVE_USERS, concurrencyMap, new AppAsyncCallback<ArrayList<AdminUserProfile>>() {
      @Override
      public void onSuccess(final ArrayList<AdminUserProfile> result) {
        view.asDataTable().setRowData(result);
      }
    },
        new ConcurrentAsyncServiceCall<ArrayList<AdminUserProfile>>() {
      @Override
      public void execute(final AsyncCallback<ArrayList<AdminUserProfile>> callback) {
        appContext.getAdminService().getUsers(filter, callback);
      }
    });
  }

  @Override
  public void submitUserEdit() {
    final AdminUserProfile profile = userEditDriver.flush();

    if (userEditDriver.hasErrors()) {
      ErrorPopupController.addErrors(userEditDriver.getErrors());
      return;
    } else {
      appContext.getAdminService().updateUser(profile, userUpdateCallback);
    }
  }

  @Override
  public void editUser(final AdminUserProfile user) {
    this.currentUserProfile = user;

    userEditDriver.edit(user);
  }

  /**
   * Figure out what to do with an unsaved user. Request the user be explicitly saved first? Use old one? Save automatically?
   */
  @Override
  public void resetPassword() {
    if (Window.confirm(M.messages().adminUsersResetPasswordConfirmation())) {
      appContext.getAdminService().resetPassword(currentUserProfile, passwordResetCallback);
    }
  }

  @Override
  public void removeUser(final AdminUserProfile profile) {
    appContext.getAdminService().removeUser(profile, userRemovalCallback);
  }

  @Override
  public void createUser(final AdminUserProfile profile) {
    appContext.getAdminService().createUser(profile, userCreateCallback);
  }

  @Override
  public AdminUserProfile getDefaultUserProfile() {
    return new AdminUserProfile();
  }
}
