/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui.filter;

import java.util.HashSet;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.domain.user.Authority;
import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.filter.BaseFilterPanel;
import nl.overheid.aerius.wui.main.widget.MultiSelectListBox;
import nl.overheid.aerius.wui.main.widget.SelectOption;
import nl.overheid.aerius.wui.main.widget.SelectTextOptionFactory;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;

public class UserManagementFilterPanel extends BaseFilterPanel<UserManagementFilter> {
  interface UserManagementFilterUiBinder extends UiBinder<Widget, UserManagementFilterPanel> {}

  private static final UserManagementFilterUiBinder UI_BINDER = GWT.create(UserManagementFilterUiBinder.class);

  @UiField TextBox userName;

  @UiField MultiSelectListBox<Boolean> enabledListBox;
  @UiField(provided = true) WidgetFactory<Boolean, SelectOption<Boolean>> enabledOptionTemplate=new SelectTextOptionFactory<Boolean>(){
    @Override
    public String getItemText(final Boolean value) {
      return M.messages().adminUsersEnabledText(value);
    }
  };

  @UiField MultiSelectListBox<Authority> organisationListBox;
  @UiField(provided = true) WidgetFactory<Authority, SelectOption<Authority>> organisationOptionTemplate = new SelectTextOptionFactory<Authority>() {
    @Override
    public String getItemText(final Authority value) {
      return value.getDescription();
    }
  };

  @UiField MultiSelectListBox<AdminUserRole> roleListBox;
  @UiField(provided = true) WidgetFactory<AdminUserRole, SelectOption<AdminUserRole>> roleOptionTemplate = new SelectTextOptionFactory<AdminUserRole>() {
    @Override
    public String getItemText(final AdminUserRole value) {
      return value.getName();
    }
  };

  @Inject
  public UserManagementFilterPanel(final AdminContext adminContext) {
    initWidget(UI_BINDER.createAndBindUi(this));

    userName.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_FILTER_USERNAME);
    enabledListBox.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_FILTER_ENABLED);
    organisationListBox.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_FILTER_ORGANISATION);
    roleListBox.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_FILTER_ROLES);

    organisationListBox.setValues(adminContext.getAuthorities());
    roleListBox.setValues(adminContext.getRoles());
    enabledListBox.setSingleSelectionMode(true);
    enabledListBox.setValues(new Boolean[] { true, false });
  }

  @Override
  public void setValue(final UserManagementFilter value) {
    if (value != null) {
      userName.setValue(value.getUsername());
      organisationListBox.setSelectedValues(value.getOrganisations(), true);
      roleListBox.setSelectedValues(value.getRoles(), true);
      enabledListBox.deselect();
      enabledListBox.setSelectedValue(value.getEnabled(), true);
    }

    super.setValue(value);
  }

  @Override
  protected String getTitleLabel() {
    final HashSet<Boolean> selectedValues = enabledListBox.getSelectedValues();
    final String enabled = enabledListBox.getHeaderText(selectedValues);

    final String organisations = organisationListBox.getHeaderText(organisationListBox.getSelectedValues());
    final String roles = roleListBox.getHeaderText(roleListBox.getSelectedValues());
    final String value = userName.getValue();
    final String label;
    if ((value == null) || value.isEmpty()) {
      label = M.messages().adminUsersFilterTextNoName(enabled, organisations, roles);
    } else {
      label = M.messages().adminUsersFilterText(enabled, value, organisations, roles);
    }
    return label;
  }

  @Override
  protected void updateFilter() {
    value.setUsername(userName.getValue());
    value.setOrganisations(organisationListBox.getSelectedValues());
    value.setRoles(roleListBox.getSelectedValues());

    final HashSet<Boolean> selectedValues = enabledListBox.getSelectedValues(true);
    if (selectedValues.size() == 1) {
      value.setEnabled(selectedValues.iterator().next());
    } else {
      value.setEnabled(null);
    }
  }
}
