/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.context;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.context.AdminUserContext;
import nl.overheid.aerius.shared.service.AdminServiceAsync;
import nl.overheid.aerius.wui.main.context.AppContext;

/**
 * Application Context object for Admin.
 */
public class AdminAppContext<C extends AdminContext, U extends AdminUserContext> extends AppContext<C, U> {

  private final AdminServiceAsync adminService;

  @Inject
  public AdminAppContext(final EventBus eventBus, final PlaceController placeController, final C context, final U userContext,
      final AdminServiceAsync adminService) {
    super(eventBus, placeController, context, userContext);

    this.adminService = adminService;
  }

  public AdminServiceAsync getAdminService() {
    return adminService;
  }

}
