/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleCollapsibleDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class RoleManagementLegendTable extends Composite implements IsInteractiveDataTable<AdminUserRole> {
  interface RoleManagementLegendTableUiBinder extends UiBinder<Widget, RoleManagementLegendTable> {}

  private static final RoleManagementLegendTableUiBinder UI_BINDER = GWT.create(RoleManagementLegendTableUiBinder.class);

  @UiField SimpleCollapsibleDivTable<AdminUserRole> table;

  @UiField(provided = true) TextColumn<AdminUserRole> idColumn;

  @UiField(provided = true) SimpleWidgetFactory<AdminUserRole> content;

  public interface RoleChangeHandler {
    void submit(AdminUserRole role);
  }

  private RoleChangeHandler presenter;

  private final AdminContext context;

  @Inject
  public RoleManagementLegendTable(final AdminContext context) {
    this.context = context;
    idColumn = new TextColumn<AdminUserRole>() {
      @Override
      public String getValue(final AdminUserRole object) {
        return String.valueOf(object.getId());
      }

      @Override
      public void applyCellOptions(final Widget cell, final AdminUserRole object) {
        super.applyCellOptions(cell, object);

        cell.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(object.getColor()));
      }
    };

    content = new SimpleWidgetFactory<AdminUserRole>() {
      @Override
      public Widget createWidget(final AdminUserRole object) {
        return new RoleManagementLegendContent(object) {
          @Override
          void onCancel() {
            table.deselectAll();
          }

          @Override
          void onSubmit(final AdminUserRole value) {
            presenter.submit(value);
          }

        };
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    refresh();
  }

  public void refresh() {
    table.clear();
    table.setRowData(context.getRoles());
  }

  @Override
  public SimpleCollapsibleDivTable<AdminUserRole> asDataTable() {
    return table;
  }

  public void setPresenter(final RoleChangeHandler presenter) {
    this.presenter = presenter;
  }
}
