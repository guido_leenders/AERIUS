/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import nl.overheid.aerius.shared.domain.admin.UserManagementFilter;
import nl.overheid.aerius.shared.domain.context.AdminUserContext;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.wui.admin.ui.filter.UserManagementFilterPanel;
import nl.overheid.aerius.wui.admin.widget.UserManagementEditor;
import nl.overheid.aerius.wui.admin.widget.UserManagementTable;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;

/**
 * View for User management
 */
@Singleton
public class UserManagementViewImpl extends Composite implements UserManagementView {
  interface UserManagementViewImplUiBinder extends UiBinder<Widget, UserManagementViewImpl> {}

  private static final UserManagementViewImplUiBinder UI_BINDER = GWT.create(UserManagementViewImplUiBinder.class);

  private static final int COLUMN_THRESHOLD = 800;

  public interface UserManagementEditDriver extends SimpleBeanEditorDriver<AdminUserProfile, UserManagementEditor> {}

  public interface CustomStyle extends CssResource {
    String userCreationPopup();
  }

  @UiField FlowPanel container;

  @UiField CustomStyle style;

  @UiField(provided = true) UserManagementFilterPanel filterPanel;
  @UiField(provided = true) UserManagementTable userTable;

  @UiField SimplePanel userEditorContainer;
  @UiField(provided = true) UserManagementEditor userEditor;

  @UiField Button newUserButton;
  @UiField Button buttonDelete;
  @UiField Button buttonCancel;
  @UiField Button buttonSubmit;

  private final Handler selectionChangeHandler = new SelectionChangeEvent.Handler() {
    @Override
    public void onSelectionChange(final SelectionChangeEvent event) {
      setUserEditor(((SingleSelectionModel<AdminUserProfile>) userTable.asDataTable().getSelectionModel()).getSelectedObject());
    }
  };

  private Presenter presenter;
  private final ValueChangeHandler<UserManagementFilter> filterChangeHandler = new ValueChangeHandler<UserManagementFilter>() {
    @Override
    public void onValueChange(final ValueChangeEvent<UserManagementFilter> event) {
      presenter.onAdaptFilter();
    }
  };

  @UiField Button passwordResetButton;

  private final HandlerRegistration handle;
  private boolean layoutScheduled;
  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  private final ConfirmCancelDialog<AdminUserProfile, UserManagementEditor> userCreationPopup;
  private final UserManagementEditor userCreationEditor;
  private final SimpleBeanEditorDriver<AdminUserProfile, UserManagementEditor> userCreationDriver = GWT.create(UserManagementEditDriver.class);

  private AdminUserProfile currentEditProfile;

  private final ConfirmHandler<AdminUserProfile> userCreationConfirmHandler = new ConfirmHandler<AdminUserProfile>() {
    @Override
    public void onConfirm(final ConfirmEvent<AdminUserProfile> event) {
      userCreationPopup.enableConfirmButton(false);
      presenter.createUser(event.getValue());
    }
  };

  @Inject
  public UserManagementViewImpl(final AdminUserContext context, final UserManagementFilterPanel filterPanel, final UserManagementTable userTable, final UserManagementEditor userEditor, final UserManagementEditor userCreationEditor) {
    this.filterPanel = filterPanel;
    this.userEditor = userEditor;
    this.userCreationEditor = userCreationEditor;
    this.userTable = userTable;

    userCreationEditor.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_ADD_EDITOR);
    userEditor.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_EDIT_EDITOR);

    initWidget(UI_BINDER.createAndBindUi(this));
    newUserButton.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_NEW_USER);
    buttonDelete.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_EDIT_DELETE);
    buttonCancel.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_EDIT_CANCEL);
    buttonSubmit.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_EDIT_SUBMIT);

    userTable.asDataTable().addSelectionChangeHandler(selectionChangeHandler);

    filterPanel.setValue(context.getFilter(UserManagementFilter.class));
    filterPanel.addValueChangeHandler(filterChangeHandler);

    userCreationPopup = new ConfirmCancelDialog<>(userCreationDriver, M.messages().genericSave(), M.messages().cancelButton());
    userCreationPopup.setText(M.messages().adminUsersNewUserTitle());
    userCreationPopup.setWidget(userCreationEditor);
    userCreationPopup.addConfirmHandler(userCreationConfirmHandler);
    userCreationDriver.initialize(userCreationEditor);
    userCreationEditor.addStyleName(style.userCreationPopup());
    userCreationEditor.setUsernameEnabled(true);

    handle = Window.addResizeHandler(new ResizeHandler() {
      @Override
      public void onResize(final ResizeEvent event) {
        scheduleLayout();
      }
    });
  }

  @Override
  protected void onUnload() {
    handle.removeHandler();
  }

  @Override
  protected void onLoad() {
    scheduleLayout();
  }

  @Override
  public void userCreateComplete(final boolean success) {
    userCreationPopup.enableConfirmButton(true);

    if (success) {
      userCreationPopup.hide();
    }
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @UiHandler("passwordResetButton")
  public void onSubmitClick(final ClickEvent e) {
    if (presenter != null) {
      presenter.resetPassword();
    }
  }

  @Override
  public SimpleInteractiveClickDivTable<AdminUserProfile> asDataTable() {
    return userTable.asDataTable();
  }

  private void scheduleLayout() {
    if (isAttached() && !layoutScheduled) {
      layoutScheduled = true;
      Scheduler.get().scheduleDeferred(layoutCmd);
    }
  }

  public void forceLayout() {
    container.setStyleName(R.css().columnsClean(), getOffsetWidth() < COLUMN_THRESHOLD);
  }

  private void setUserEditor(final AdminUserProfile profile) {
    if (profile == null) {
      userEditorContainer.setVisible(false);
      return;
    }

    currentEditProfile = profile;

    userEditorContainer.setVisible(true);

    presenter.editUser(profile);
  }

  @UiHandler("buttonCancel")
  public void onCancelEditClick(final ClickEvent e) {
    userTable.asDataTable().getSelectionModel().setSelected(currentEditProfile, false);
  }

  @UiHandler("buttonDelete")
  public void onDeleteClick(final ClickEvent e) {
    final ConfirmCancelDialog<Object, ?> dialog = new ConfirmCancelDialog<>(M.messages().genericOK(), M.messages().genericCancel());

    final Label label = new Label(M.messages().adminUsersRemoveConfirm());
    label.getElement().getStyle().setPadding(10, Unit.PX);

    dialog.setWidget(label);

    dialog.addConfirmHandler(new ConfirmHandler<Object>() {
      @Override
      public void onConfirm(final ConfirmEvent<Object> event) {
        presenter.removeUser(currentEditProfile);
        dialog.hide();
      }
    });
    dialog.center();
  }

  @UiHandler("buttonSubmit")
  public void onSubmitEditClick(final ClickEvent e) {
    presenter.submitUserEdit();
  }

  @UiHandler("newUserButton")
  public void onNewUserClick(final ClickEvent e) {
    createUser(presenter.getDefaultUserProfile());
  }

  @Override
  public void createUser(final AdminUserProfile profile) {
    userCreationPopup.center();

    userCreationDriver.edit(profile);
  }

  @Override
  public UserManagementEditor getModificationEditor() {
    return userEditor;
  }

  @Override
  public UserManagementEditor getCreationEditor() {
    return userCreationEditor;
  }

  @Override
  public String getTitleText() {
    return M.messages().adminMenuUserManagementTitle();
  }
}
