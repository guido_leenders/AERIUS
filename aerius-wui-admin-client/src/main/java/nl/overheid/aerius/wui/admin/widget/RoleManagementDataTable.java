/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.user.AdminUserRole;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.WidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.InteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class RoleManagementDataTable extends Composite implements IsInteractiveDataTable<AdminUserRole> {
  interface RoleManagementTableUiBinder extends UiBinder<Widget, RoleManagementDataTable> {}

  private static final RoleManagementTableUiBinder UI_BINDER = GWT.create(RoleManagementTableUiBinder.class);

  public interface CustomStyle extends CssResource {
    String block();

    String last();
  }

  @UiField CustomStyle style;

  @UiField InteractiveClickDivTable<AdminUserRole, RoleManagementAdminUserRoleRow> divTable;

  @UiField FlowPanel nameContainer;
  @UiField(provided = true) TextColumn<AdminUserRole> roleHeader;

  private final AdminContext context;

  @Inject
  public RoleManagementDataTable(final AdminContext context) {
    this.context = context;

    roleHeader = new TextColumn<AdminUserRole>(true) {
      @Override
      public String getValue(final AdminUserRole object) {
        return String.valueOf(object.getId());
      }

      @Override
      public void applyCellOptions(final Widget cell, final AdminUserRole object) {
        super.applyCellOptions(cell, object);

        cell.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(object.getColor()));
      }

      @Override
      public Widget wrapWidget(final Widget widget) {
        return new SimplePanel(widget);
      }

      @Override
      public String getTitleText(final AdminUserRole object) {
        return object.getName();
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    divTable.setSelectionModel(new SingleSelectionModel<AdminUserRole>());

    refresh();
  }

  public void refresh() {
    divTable.clear();
    divTable.clearColumns();
    divTable.addColumn(roleHeader);
    nameContainer.clear();
    setPermissions(context.getPermissions());
  }

  public void setPermissions(final Permission[] permissions) {
    final int size = context.getRoles().size();

    for (final Permission permission : permissions) {
      final Label permissionLabel = new Label(M.messages().adminPermission(((Enum<?>) permission).name()));
      nameContainer.add(permissionLabel);
      permissionLabel.setStyleName(R.css().noShrink());

      divTable.addColumn(new WidgetFactory<AdminUserRole, IsWidget>() {
        int counter = 0;

        @Override
        public IsWidget createWidget(final AdminUserRole object) {
          final SimplePanel panel = new SimplePanel();

          panel.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(object.getColor()));
          panel.setTitle(permission.getName());
          panel.getElement().getStyle().setVisibility(object.getPermissions().contains(permission.getName()) ? Visibility.VISIBLE : Visibility.HIDDEN);

          return panel;
        }

        @Override
        public Widget wrapWidget(final Widget widget) {
          return new SimplePanel(widget);
        }

        @Override
        public void applyCellOptions(final Widget cell, final AdminUserRole object) {
          super.applyCellOptions(cell, object);

          if (++counter == size) {
            cell.addStyleName(style.last());
          }
        }
      }, style.block(), R.css().flex(), R.css().grow(), R.css().noShrink());
    }

    divTable.setRowData(context.getRoles());
  }

  @Override
  public InteractiveClickDivTable<AdminUserRole, RoleManagementAdminUserRoleRow> asDataTable() {
    return divTable;
  }
}
