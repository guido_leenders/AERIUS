/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.util;

import java.util.List;

import com.google.gwt.place.shared.Place;

import nl.overheid.aerius.shared.domain.auth.Permission;
import nl.overheid.aerius.shared.domain.user.UserProfile;
import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.wui.admin.place.RoleManagementPlace;
import nl.overheid.aerius.wui.admin.place.UserManagementPlace;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.ui.menu.MenuItem;
import nl.overheid.aerius.wui.main.ui.menu.SimpleMenuItem;

public class AdminMenuHandler {
  public static void compose(final List<MenuItem> menus, final UserProfile userProfile, final Permission administratorPermission) {
    if (userProfile.hasPermission(administratorPermission)) {
      menus.add(new SimpleMenuItem(M.messages().adminMenuUserManagement(), TestIDAdmin.MENU_USER_MANAGEMENT) {
        @Override
        public boolean isActivePlace(final Place place) {
          return place instanceof UserManagementPlace;
        }

        @Override
        public Place getPlace(final Place currentPlace) {
          return new UserManagementPlace();
        }
      });
      menus.add(new SimpleMenuItem(M.messages().adminMenuRoleManagement(), TestIDAdmin.MENU_ROLE_MANAGEMENT) {
        @Override
        public boolean isActivePlace(final Place place) {
          return place instanceof RoleManagementPlace;
        }

        @Override
        public Place getPlace(final Place currentPlace) {
          return new RoleManagementPlace();
        }
      });
    }
  }
}
