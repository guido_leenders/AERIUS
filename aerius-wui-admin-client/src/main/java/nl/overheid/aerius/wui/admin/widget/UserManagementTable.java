/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.admin.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.AdminContext;
import nl.overheid.aerius.shared.domain.user.AdminUserProfile;
import nl.overheid.aerius.shared.domain.user.UserRole;
import nl.overheid.aerius.shared.test.TestIDAdmin;
import nl.overheid.aerius.shared.util.ColorUtil;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.widget.table.DivTableKeyProviders;
import nl.overheid.aerius.wui.main.widget.table.IsInteractiveDataTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class UserManagementTable extends Composite implements IsInteractiveDataTable<AdminUserProfile> {
  private static final int ROLE_COLUMN_IDX_START = 1;

  interface UserManagementTableUiBinder extends UiBinder<Widget, UserManagementTable> {}

  private static final UserManagementTableUiBinder UI_BINDER = GWT.create(UserManagementTableUiBinder.class);

  public interface CustomStyle extends CssResource {
    String roleCell();
  }

  @UiField CustomStyle style;

  @UiField SimpleInteractiveClickDivTable<AdminUserProfile> divTable;

  @UiField(provided = true) SimpleWidgetFactory<AdminUserProfile> userColumn;

  @Inject
  public UserManagementTable(final AdminContext userContext) {
    userColumn = new SimpleWidgetFactory<AdminUserProfile>() {
      @Override
      public Widget createWidget(final AdminUserProfile object) {
        return new UserManagementUsernameCell(object);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    int counter = 0;
    for (final UserRole role : userContext.getRoles()) {
      final TextColumn<AdminUserProfile> textColumn = new TextColumn<AdminUserProfile>(true) {
        @Override
        public String getValue(final AdminUserProfile object) {
          return String.valueOf(role.getId());
        }

        @Override
        public String getTitleText(final AdminUserProfile object) {
          return role.getName();
        }

        @Override
        public void applyCellOptions(final Widget cell, final AdminUserProfile object) {
          super.applyCellOptions(cell, object);

          cell.getElement().getStyle().setBackgroundColor(ColorUtil.webColor(role.getColor()));
          cell.getElement().getStyle().setVisibility(object.hasRole(role) ? Visibility.VISIBLE : Visibility.HIDDEN);
        }
      };

      textColumn.ensureDebugId(TestIDAdmin.USER_MANAGEMENT_OVERVIEW_ROLES);

      divTable.insertColumn(ROLE_COLUMN_IDX_START + counter++, textColumn, style.roleCell(), R.css().noShrink(), R.css().alignCenter(), R.css().justifyCenter(), R.css().flex(), R.css().number());
    }

    DivTableKeyProviders.autoConfigure(AdminUserProfile.class, divTable);
    divTable.setSingleSelectionModel();
  }

  @Override
  public SimpleInteractiveClickDivTable<AdminUserProfile> asDataTable() {
    return divTable;
  }
}
