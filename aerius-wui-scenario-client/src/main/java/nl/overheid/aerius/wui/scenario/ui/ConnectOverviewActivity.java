/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.context.ScenarioUserContext;
import nl.overheid.aerius.shared.domain.scenario.JobProgress;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.domain.source.EmissionSourceList;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.main.widget.dialog.ConfirmCancelDialog;
import nl.overheid.aerius.wui.main.widget.event.ConfirmEvent;
import nl.overheid.aerius.wui.main.widget.event.ConfirmHandler;
import nl.overheid.aerius.wui.scenario.base.place.ResultTablePlace;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.place.ConnectConfigurePlace;
import nl.overheid.aerius.wui.scenario.place.ConnectOverviewPlace;
import nl.overheid.aerius.wui.scenario.processor.ScenarioApiInitPollingAgent;
import nl.overheid.aerius.wui.scenario.service.APIServiceAsync;

public class ConnectOverviewActivity extends AbstractActivity implements ConnectOverviewView.Presenter {

//  private final ProjectExportDriver projectKeyExportDriver = GWT.create(ProjectExportDriver.class);

  private final ConnectOverviewView view;
  private final ScenarioUserContext userContext;
  private final ScenarioApiInitPollingAgent pollAgent;
  private final APIServiceAsync service;
  private final ScenarioAppContext appContext;
  private EventBus eventBus;
  private boolean filterActive;

  @Inject
  public ConnectOverviewActivity(final ScenarioAppContext appContext, @Assisted final ConnectOverviewPlace place, final ConnectOverviewView view,
      final ScenarioApiInitPollingAgent pollAgent, final APIServiceAsync service) {
    this.appContext = appContext;
    this.view = view;
    this.userContext = appContext.getUserContext();
    this.pollAgent = pollAgent;
    this.service = service;
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;
    view.setPresenter(this);

//    projectKeyExportDriver.initialize(view.getExportEditor());
//    projectKeyExportDriver.edit("");

    panel.setWidget(view);
    startPollAgent();

  }

  private void startPollAgent() {
    pollAgent.start(userContext.getAPIKey(), processCallBack());
  }

  @Override
  public void onStop() {
    pollAgent.stop();
  }

  @Override
  public void newCalculation() {
    appContext.getPlaceController().goTo(new ConnectConfigurePlace(appContext.getPlaceController().getWhere()));
  }

//  @Override
//  public void newExport(final String projectKey) {
//    projectKeyExportDriver.flush();
//
//    if (projectKeyExportDriver.hasErrors()) {
//      ErrorPopupController.addErrors(projectKeyExportDriver.getErrors());
//    } else {
//      service.requestExport(userContext.getAPIKey(), projectKey, new AppAsyncCallback<JSONValue>() {
//        @Override
//        public void onSuccess(final JSONValue result) {
//          NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioConnectExportJobStarted(projectKey));
//        }
//      });
//      projectKeyExportDriver.edit("");
//    }
//  }

  private AppAsyncCallback<JSONValue> processCallBack() {
    return new AppAsyncCallback<JSONValue>() {
      @Override
      public void onSuccess(final JSONValue result) {
        final JSONArray rows = result.isObject().get("entries").isArray();
        final ArrayList<JobProgress> jobs = new ArrayList<>();
        for (int i = 0; i < rows.size(); i++) {
          final JobProgress job = fillJobProgress(new JobProgress(), rows.get(i).isObject());
          final boolean filterMatches = job.getState() == JobState.RUNNING || job.getState() == JobState.INITIALIZED;
          if (!filterActive || (filterActive && filterMatches)) {
            jobs.add(job);
          }
        }
        Collections.sort(jobs, new Comparator<JobProgress>() {
          @Override
          public int compare(final JobProgress o1, final JobProgress o2) {
            if (o1.getCreationDateTime() == null || o2.getCreationDateTime() == null) {
              return -1;
            }
            return o2.getCreationDateTime().compareTo(o1.getCreationDateTime());
          }
        });
        view.asDataTable().setRowData(jobs);
      }
    };
  }

  private JobProgress fillJobProgress(final JobProgress jobProgress, final JSONObject jsonObject) {
    jobProgress.setState(determineJobState(jsonObject.get("state").isString().stringValue()));
    jobProgress.setType(JobType.safeValueOf(jsonObject.get("type").isString().stringValue()));
    jobProgress.setHexagonCount((long) jsonObject.get("hectareCalculated").isNumber().doubleValue());
    final JSONValue startDateTime = jsonObject.get("startDateTime");
    if (startDateTime != null) {
      jobProgress.setCreationDateTime(convertDateTime(startDateTime.isString().stringValue()));
    }
    final JSONValue name = jsonObject.get("name");
    if (name != null) {
      jobProgress.setName(name.isString().stringValue());
    }
    jobProgress.setKey(jsonObject.get("key").isString().stringValue());
    return jobProgress;
  }

  private Date convertDateTime(final String dateInString) {
    if (dateInString == null || dateInString.length() == 0) {
      return new Date();
    } else {
      final String format = "yyyy-MM-ddTHH:mm:ss.SSSZZ";
      final DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat(format);
      return dateTimeFormat.parse(dateInString);
    }
  }

  private JobState determineJobState(final String jobState) {
    JobState state = JobState.UNDEFINED;
    switch (jobState) {
    case "RUNNING":
      state = JobState.RUNNING;
      break;
    case "COMPLETED":
      state = JobState.COMPLETED;
      break;
    case "QUEUED":
      state = JobState.INITIALIZED;
      break;
    case "CANCELLED":
      state = JobState.CANCELLED;
      break;
    default:
      break;
    }
    return state;
  }

  @Override
  public void openJob(final String jobKey, final JobType jobType) {
    final Scenario scenario = appContext.getUserContext().getScenario();

    final ArrayList<EmissionSourceList> sourceLists = scenario.getSourceLists();

    if (sourceLists.isEmpty()) {
      followJob(jobKey, jobType);
    } else {
      final ConfirmCancelDialog<Object, ?> dialog = new ConfirmCancelDialog<>(M.messages().genericOK(), M.messages().genericCancel());

      final Label label = new Label(M.messages().scenarioConnectPurgeExistingScenario());
      label.getElement().getStyle().setPadding(10, Unit.PX);
      dialog.setWidget(label);
      dialog.addConfirmHandler(new ConfirmHandler<Object>() {
        @Override
        public void onConfirm(final ConfirmEvent<Object> event) {
          final Scenario scenario = appContext.getUserContext().getScenario();

          // Purge
          for (final EmissionSourceList lst : sourceLists) {
            if (lst != null) {
              scenario.removeSources(lst.getId());
            }
          }

          dialog.hide();

          followJob(jobKey, jobType);
        }
      });
      dialog.center();
    }
  }

  @Override
  public void cancelJob(final String jobKey) {
    service.cancelJob(userContext.getAPIKey(), jobKey, new AppAsyncCallback<JSONValue>() {

      @Override
      public void onSuccess(final JSONValue result) {
        NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioConnectJobCancelSuccessful());
      }
    });
  }

  private void followJob(final String jobKey, final JobType jobType) {
    final ResultTablePlace place = new ResultTablePlace(null);
    place.setJobKey(jobKey);
    place.setJobType(jobType);

    appContext.getUserContext().setFollowingJob(false);
    appContext.getPlaceController().goTo(place);
  }

  @Override
  public void updateFilterStatus(final Boolean value) {
    filterActive = value;
    startPollAgent();
  }

}
