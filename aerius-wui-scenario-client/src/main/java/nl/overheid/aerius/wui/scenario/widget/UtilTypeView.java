/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;

public class UtilTypeView extends Composite implements Editor<UtilType> {
  private static final UtilTypeViewImplUiBinder UI_BINDER = GWT.create(UtilTypeViewImplUiBinder.class);

  interface UtilTypeViewImplUiBinder extends UiBinder<Widget, UtilTypeView> {
  }

  public interface UtilTypeHandler {
    void updateUtilType(UtilType utilType);
  }

  @UiField FlowPanel utilFunction;

  private UtilTypeHandler handler;
  private UtilType selectedUtilType = UtilType.VALIDATE;

  LeafValueEditor<UtilType> utilTypeEditor = new LeafValueEditor<UtilType>() {

    @Override
    public UtilType getValue() {
      handler.updateUtilType(selectedUtilType);
      return selectedUtilType;
    }

    @Override
    public void setValue(final UtilType value) {
      selectedUtilType = value;
      handler.updateUtilType(value);
    }
  };

  @Inject
  public UtilTypeView() {

    initWidget(UI_BINDER.createAndBindUi(this));

    for (final UtilType utilType : UtilType.values()) {
      utilFunction.add(createCheckBoxEntry(utilType));
    }
  }

  private FlowPanel createCheckBoxEntry(final UtilType utilType) {
    final FlowPanel fp = new FlowPanel();
    fp.setStyleName(R.css().flex());
    final RadioButton rb = new RadioButton("utilFunction", M.messages().scenarioConnectSelectedUtilTitle(utilType));
    rb.addClickHandler(new ClickHandler() {

      @Override
      public void onClick(final ClickEvent event) {
        utilTypeEditor.setValue(utilType);
      }
    });
    rb.setValue(utilType == selectedUtilType);
    rb.addStyleName(R.css().flex());
    rb.addStyleName(R.css().sourceDetailCol1Var());
    rb.ensureDebugId(TestIDScenario.CONNECT_UTIL_SELECT + utilType.name());
    final Label label = new Label(M.messages().scenarioConnectSelectedUtilDescription(utilType));
    label.addStyleName(R.css().fontColorAlternative());
    label.addStyleName(R.css().sourceDetailCol2Var());
    fp.add(rb);
    fp.add(label);
    return fp;
  }

  public LeafValueEditor<UtilType> getUtilTypeEditor() {
    return utilTypeEditor;
  }

  public void setContentHandler(final UtilTypeHandler handler) {
    this.handler = handler;
  }

}