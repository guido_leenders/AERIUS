/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.geo;

import java.util.ArrayList;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.LayerFactory;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.domain.WMSLayerType;
import nl.overheid.aerius.shared.domain.importer.ImportResult;
import nl.overheid.aerius.shared.domain.importer.UserGeoLayer;
import nl.overheid.aerius.shared.domain.info.InformationReceptorPoint;
import nl.overheid.aerius.shared.domain.source.EmissionSource;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.shared.service.InfoServiceAsync;
import nl.overheid.aerius.wui.geo.HabitatTypeLayerWrapper;
import nl.overheid.aerius.wui.geo.search.MapSearchPanel;
import nl.overheid.aerius.wui.main.event.ImportEvent;
import nl.overheid.aerius.wui.main.event.InformationPointChangeEvent;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.NotificationPanel;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.ImportedImaerFeaturesChangeEvent;
import nl.overheid.aerius.wui.scenario.base.geo.CalculationMarkerLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.geo.CalculationResultLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.geo.CalculatorDevelopmentRuleLayerWrapper;
import nl.overheid.aerius.wui.scenario.base.geo.ScenarioBaseMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.base.place.NetworkDetailPlace;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Special MapLayoutPanel for showing special utilisation results.
 */
@Singleton
public class ScenarioMapLayoutPanel extends ScenarioBaseMapLayoutPanel {

  private final WmsUtilisationRelativeResultLayerWrapper utilisationResultLayerWrapper;
  private final WmsUtilisationAbsoluteResultLayerWrapper utilisationAbsoluteLayerWrapper;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final FetchGeometryServiceAsync service;
  private final InfoServiceAsync infoService;
  private ArrayList<WmsUserRoadBaseLayerWrapper> featureLayerList = new ArrayList<>();
  private Integer importedImaerFileId;
  private boolean hasUserGeoLayers;

  @Inject
  public ScenarioMapLayoutPanel(final LayerFactory lf, final EventBus eventBus, final FetchGeometryServiceAsync service,
      final InfoServiceAsync infoService,
      final MapSearchPanel searchPanel, final ScenarioBaseAppContext<?, ?> appContext, final NotificationPanel notificationPanel,
      final CalculationResultLayerWrapper activeResultLayerWrapper, final CalculatorDevelopmentRuleLayerWrapper developmentRuleLayerWrapper,
      final HabitatTypeLayerWrapper activeHabitatTypeLayerWrapper, final CalculationMarkerLayerWrapper activeMarkerLayerWrapper,
      final HelpPopupController hpC, final WmsUtilisationRelativeResultLayerWrapper utilisationResultLayerWrapper,
      final WmsUtilisationAbsoluteResultLayerWrapper utilisationAbsoluteLayerWrapper) {

    super(lf, eventBus, service, searchPanel, appContext, notificationPanel,
        activeResultLayerWrapper, developmentRuleLayerWrapper,
        activeHabitatTypeLayerWrapper, activeMarkerLayerWrapper, hpC);

    this.appContext = appContext;
    this.service = service;
    this.infoService = infoService;
    this.utilisationAbsoluteLayerWrapper = utilisationAbsoluteLayerWrapper;
    this.utilisationResultLayerWrapper = utilisationResultLayerWrapper;
    utilisationAbsoluteLayerWrapper.setMap(this);
    utilisationResultLayerWrapper.setMap(this);
  }

  @Override
  @EventHandler
  protected void onImportEvent(final ImportEvent e) {
    final ImportResult value = e.getValue();

    if (featureLayerList != null && !featureLayerList.isEmpty()) {
      for (final WmsUserRoadBaseLayerWrapper oldfeatureLayer : featureLayerList) {
        oldfeatureLayer.setKey(null);
        oldfeatureLayer.detach();
      }
      featureLayerList = new ArrayList<>();
    }

    for (final UserGeoLayer layer : value.getUserGeoLayers()) {
      final WMSLayerType wmsLayerType = WMSLayerType.valueOf(WMSLayerType.WMS_USER_PREFIX + layer.getType());
      final WmsUserRoadBaseLayerWrapper featureLayer =
          new WmsUserRoadBaseLayerWrapper(appContext, service, (LayerWMSProps) appContext.getContext().getLayer(wmsLayerType));

      featureLayer.setKey(layer.getKey());
      featureLayer.setMap(this);
      featureLayer.setVisible(true);
      featureLayerList.add(featureLayer);
    }
    this.importedImaerFileId = value.getImportedImaerFileId();
    this.hasUserGeoLayers = !value.getUserGeoLayers().isEmpty();
    super.onImportEvent(e);
  }

  @Override
  @EventHandler
  protected void onInformationPointChange(final InformationPointChangeEvent event) {
    setInfoMarker(event.getValue());
  }

  private void setInfoMarker(final InformationReceptorPoint rp) {
    if (importedImaerFileId != null && hasUserGeoLayers) {
      infoService.getImportedImaerFeatures(importedImaerFileId, rp, new AppAsyncCallback<ArrayList<EmissionSource>>() {

        @Override
        public void onSuccess(final ArrayList<EmissionSource> result) {
          // result can be null
          final PlaceController place = appContext.getPlaceController();
          final ScenarioBasePlace from = (ScenarioBasePlace) place.getWhere();
          if (place.getWhere() instanceof NetworkDetailPlace) {
            eventBus.fireEvent(new ImportedImaerFeaturesChangeEvent(result));
          } else {
            place.goTo(new NetworkDetailPlace(from.getSid1(), result, from));
          }
        }
      });
    }
  }

  /**
   * Hides all substance layers.
   *
   * @param visible set visibility
   */
  @Override
  public void setVisibleSubstanceLayers(final boolean visible) {
    super.setVisibleSubstanceLayers(visible);
    utilisationAbsoluteLayerWrapper.setVisible(true);
    utilisationResultLayerWrapper.setVisible(true);
  }
}
