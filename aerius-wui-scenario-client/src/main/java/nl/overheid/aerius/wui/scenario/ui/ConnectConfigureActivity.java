/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.Iterator;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.SharedConstants;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.CalculationType;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.service.ScenarioConnectServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.main.util.NotificationUtil;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.place.ConnectOverviewPlace;
import nl.overheid.aerius.wui.scenario.ui.ConnectConfigureViewImpl.ConnectConfigureViewDriver;

public class ConnectConfigureActivity extends AbstractActivity implements ConnectConfigureView.Presenter {
  private final ConnectConfigureView view;
  private final ScenarioAppContext appContext;
  private final ScenarioConnectServiceAsync service;

  private final SimpleBeanEditorDriver<ConnectCalculationInformation, ConnectConfigureViewImpl> connectConfigurationDriver = GWT
      .create(ConnectConfigureViewDriver.class);
  private EventBus eventBus;
  private boolean clearPassage;

  @Inject
  public ConnectConfigureActivity(final ConnectConfigureView view, final ScenarioAppContext appContext, final ScenarioConnectServiceAsync service) {
    this.view = view;
    this.appContext = appContext;
    this.service = service;

    connectConfigurationDriver.initialize((ConnectConfigureViewImpl) view);
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    this.eventBus = eventBus;

    view.setPresenter(this);

    final ConnectCalculationInformation info = new ConnectCalculationInformation();
    info.setCalculationType(CalculationType.NBWET);
    info.setExportType(ExportType.GML);
    info.setYear(SharedConstants.getCurrentYear());
    info.setPermitCalculationRadiusType(null);

    panel.setWidget(view);
    connectConfigurationDriver.edit(info);
    updateView();
  }

  @Override
  public String mayStop() {
    return clearPassage || getLatestInfo().getFiles().isEmpty() ? super.mayStop() : M.messages().scenarioConnectCalculationLeaveConfirm();
  }

  @Override
  public void onStop() {
    for (final ConnectCalculationFile file : getLatestInfo().getFiles()) {
      removeFile(file.getUuid());
    }

    super.onStop();
  }

  @Override
  public void calculate() {
    final ConnectCalculationInformation latestInfo = getLatestInfo();
    latestInfo.setApiKey(appContext.getUserContext().getAPIKey());

    service.submitCalculation(latestInfo, new ComplexAPIAsyncCallback() {
      @Override
      public void reportSuccess() {
        NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioConnectCalculationConfirmStarted());
      }

      @Override
      public void reportWarningMessages(final String warnings) {
        NotificationUtil.broadcastMessage(eventBus, M.messages().scenarioConnectCalculationWarnings(warnings));
      }

      @Override
      public void reportErrorMessages(final String errors) {
        NotificationUtil.broadcastError(eventBus, M.messages().scenarioConnectCalculationErrors(errors));
      }

      @Override
      public void onSuccess() {
        gotoOverview(true);
      }

      @Override
      public void onFailure(final Throwable caught) {
        view.calculateFailed(caught);

        super.onFailure(caught);
      }

      @Override
      String formatValidationMessage(final int code, final String message) {
        return M.messages().scenarioConnectCalculationValidationMessage(code, message);
      }
    });

  }

  @Override
  public void cancel() {
    gotoOverview(false);
  }

  private void gotoOverview(final boolean cleanly) {
    clearPassage = cleanly;
    appContext.getPlaceController().goTo(new ConnectOverviewPlace(appContext.getPlaceController().getWhere()));
  }

  @Override
  public void removeFile(final String uid) {
    service.deleteUploadedFile(uid, new AppAsyncCallback<Void>() {
      @Override
      public void onSuccess(final Void result) {
        final Iterator<ConnectCalculationFile> iterator = getLatestInfo().getFiles().iterator();
        while (iterator.hasNext()) {
          if (iterator.next().getUuid().equals(uid)) {
            iterator.remove();
            break;
          }
        }
        updateView();
      }
    });
  }

  @Override
  public void addFile(final String filename, final String uid) {
    final ConnectCalculationFile file = new ConnectCalculationFile();
    file.setUuid(uid);
    file.setFilename(filename);

    getLatestInfo().getFiles().add(file);
    updateView();
  }

  @Override
  public void setExportType(final ExportType exportType) {
    getLatestInfo().setExportType(exportType);
    updateView();
  }

  private void updateView() {
    final ConnectCalculationInformation latestInfo = getLatestInfo();
    view.setData(latestInfo.getFiles(), latestInfo.getExportType());
  }

  private ConnectCalculationInformation getLatestInfo() {
    return connectConfigurationDriver.flush();
  }

}
