/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.resources.client.DataResource;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.shared.test.TestID;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.ui.menu.MenuItem;
import nl.overheid.aerius.wui.main.ui.menu.SimpleMenuItem;
import nl.overheid.aerius.wui.main.widget.NotificationPanel;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlace;
import nl.overheid.aerius.wui.scenario.base.place.ResultPlaceFactory;
import nl.overheid.aerius.wui.scenario.base.place.SourcePlace;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.place.StartUpPlace;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseTaskBar;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseViewImpl;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.geo.ScenarioMapLayoutPanel;
import nl.overheid.aerius.wui.scenario.importer.ScenarioImportController;
import nl.overheid.aerius.wui.scenario.place.ConnectLoginPlace;
import nl.overheid.aerius.wui.scenario.place.ConnectOverviewPlace;
import nl.overheid.aerius.wui.scenario.place.ConnectUtilsPlace;

/**
 * Main View for the Scenario application. Contains left panel + top buttons and map.
 */
@Singleton
public class ScenarioViewImpl extends ScenarioBaseViewImpl {

  @Inject
  public ScenarioViewImpl(final ScenarioAppContext appContext, final EventBus eventBus, final ScenarioBaseTaskBar taskBar,
      final ScenarioMapLayoutPanel map, final ScenarioImportController importController, final NotificationPanel notificationPanel) {
    super(appContext, eventBus, taskBar, map, importController, notificationPanel);
  }

  @Override
  public String getTitleText() {
    return M.messages().scenario();
  }

  @Override
  public String getTitleColor() {
    return R.css().aeriusScenarioTitle();
  }

  @Override
  public List<MenuItem> getMenuItems() {
    final ArrayList<MenuItem> items = new ArrayList<>();

    final SimpleMenuItem homeMenuItem = new SimpleMenuItem(M.messages().calculatorMenuStart(), TestID.MENU_ITEM_START) {
      @Override
      public Place getPlace(final Place place) {
        return new StartUpPlace(place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof StartUpPlace;
      }
    };
    items.add(homeMenuItem);

    final SimpleMenuItem sourcesMenuItem = new SimpleMenuItem(M.messages().calculatorMenuEmissionSources(),
        TestID.MENU_ITEM_EMISSIONSOURCES_OVERVIEW) {
      @Override
      public Place getPlace(final Place place) {
        return new SourcesPlace(place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof SourcesPlace || place instanceof SourcePlace;
      }
    };
    items.add(sourcesMenuItem);

    final SimpleMenuItem resultMenuItem = new SimpleMenuItem(M.messages().calculatorMenuResults(), TestID.MENU_ITEM_RESULTS) {
      @Override
      public Place getPlace(final Place place) {
        return ResultPlaceFactory.resultState2Place(appContext.getResultPlaceState(), place);
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof ResultPlace;
      }
    };
    items.add(resultMenuItem);

    final SimpleMenuItem utilsMenuItem = new SimpleMenuItem(M.messages().calculatorMenuUtils(), TestID.MENU_ITEM_UTILS) {
      @Override
      public Place getPlace(final Place place) {
        return new ConnectUtilsPlace(appContext.getPlaceController().getWhere());
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof ConnectUtilsPlace;
      }
    };
    items.add(utilsMenuItem);

    final SimpleMenuItem scenariosMenuItem = new SimpleMenuItem(M.messages().calculatorMenuScenarios(), TestID.MENU_ITEM_SCENARIOS) {
      @Override
      public Place getPlace(final Place place) {
        return new ConnectOverviewPlace(appContext.getPlaceController().getWhere());
      }

      @Override
      public boolean isActivePlace(final Place place) {
        return place instanceof ConnectOverviewPlace || place instanceof ConnectLoginPlace;
      }
    };
    items.add(scenariosMenuItem);


    return items;
  }

  @Override
  public DataResource getLogo() {
    return R.images().aeriusScenarioLogo();
  }
}
