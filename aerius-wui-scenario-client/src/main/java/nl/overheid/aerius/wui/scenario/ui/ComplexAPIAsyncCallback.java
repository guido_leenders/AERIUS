/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;

import nl.overheid.aerius.shared.exception.AeriusException;
import nl.overheid.aerius.shared.exception.AeriusException.Reason;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;

public abstract class ComplexAPIAsyncCallback extends AppAsyncCallback<String> {
  @Override
  public void onSuccess(final String result) {
    final JSONObject jsonResult = JSONParser.parseStrict(result).isObject();
    if (jsonResult == null) {
      failWithInternalError();
    } else {
      final boolean successful = ((JSONBoolean) jsonResult.get("successful")).booleanValue();
      final JSONArray warnings = jsonResult.containsKey("warnings") ? jsonResult.get("warnings").isArray() : null;
      final JSONArray errors = jsonResult.containsKey("errors") ? jsonResult.get("errors").isArray() : null;

      if (successful && (warnings == null || warnings.size() == 0)) {
        reportSuccess();
      } else if (successful) {
        reportWarningMessages(toValidationMessages(warnings));
      } else if (errors != null && errors.size() > 0) {
        reportErrorMessages(toValidationMessages(errors));
      } else {
        failWithInternalError();
      }
    }

    onSuccess();
  }

  abstract void onSuccess();

  abstract void reportErrorMessages(String validationMessages);

  abstract void reportWarningMessages(String validationMessages);

  abstract void reportSuccess();

  abstract String formatValidationMessage(final int code, final String message);

  private String toValidationMessages(final JSONArray array) {
    final StringBuilder messages = new StringBuilder();

    // TODO; For now we'll only support one entry. Later one this should be done in a nice DialogPanel.
    final JSONObject entry = array.get(0).isObject();
    messages.append(formatValidationMessage((int) ((JSONNumber) entry.get("code")).doubleValue(), ((JSONString) entry.get("message")).stringValue()));

    return messages.toString();
  }

  private void failWithInternalError() {
    onFailure(new AeriusException(Reason.INTERNAL_ERROR));
  }

}
