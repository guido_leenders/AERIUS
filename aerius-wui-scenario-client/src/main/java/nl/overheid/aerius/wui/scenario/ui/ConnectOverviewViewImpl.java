/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.scenario.JobProgress;
import nl.overheid.aerius.shared.domain.scenario.JobType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.resources.R;
import nl.overheid.aerius.wui.main.util.FormatUtil;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.main.widget.table.SimpleInteractiveClickDivTable;
import nl.overheid.aerius.wui.main.widget.table.SimpleWidgetFactory;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;
import nl.overheid.aerius.wui.scenario.util.JobStateImages;

public class ConnectOverviewViewImpl extends Composite implements ConnectOverviewView {
  interface ProjectExportDriver extends SimpleBeanEditorDriver<String, Editor<String>> {
  }

  private static final ConnectOverviewViewImplUiBinder UI_BINDER = GWT.create(ConnectOverviewViewImplUiBinder.class);

  interface ConnectOverviewViewImplUiBinder extends UiBinder<Widget, ConnectOverviewViewImpl> {}

  @UiField Button newJobButton;
//  @UiField ProjectKeyValueBox projectKeyValueBoxEditor;
//  @UiField Button newExportButton;
  @UiField CheckBox toggleButton;
  private final SpanElement toggleText = Document.get().createSpanElement();

  @UiField SimpleInteractiveClickDivTable<JobProgress> table;

  @UiField(provided = true) TextColumn<JobProgress> jobIdColumn;
  @UiField(provided = true) TextColumn<JobProgress> infoColumn;
  @UiField(provided = true) SimpleWidgetFactory<JobProgress> stateIconDownloadColumn;
  @UiField(provided = true) SimpleWidgetFactory<JobProgress> cancelButtonColumn;

  private Presenter presenter;

  @Inject
  public ConnectOverviewViewImpl(final HelpPopupController hpC, final ScenarioContext c) {
    jobIdColumn = new TextColumn<JobProgress>() {
      @Override
      public String getValue(final JobProgress object) {
        String result = "";

        if (object.getCreationDateTime() != null) {
          result = addText(result, FormatUtil.getDefaultCompactDateTimeFormatter().format(object.getCreationDateTime()));
        }
        return addText(result, object.getName() == null ? object.getKey() : object.getName());
      }

      private String addText(final String input, final String add) {
        return input + (input.isEmpty() ? "" : " - ") + add;
      }
    };

    stateIconDownloadColumn = new SimpleWidgetFactory<JobProgress>() {
      @Override
      public Widget createWidget(final JobProgress jobProgress) {
        final FlowPanel panel = new FlowPanel();
        panel.addStyleName(R.css().flex());
        final Image image = new Image(JobStateImages.getJobStateIcon(jobProgress.getState()));
        panel.add(image);
        return panel;
      }
    };

    cancelButtonColumn = new SimpleWidgetFactory<JobProgress>() {
      @Override
      public Widget createWidget(final JobProgress jobProgress) {
        final FlowPanel panel = new FlowPanel();
        panel.addStyleName(R.css().flex());
        final Button button = new Button(M.messages().scenarioConnectJobCancel());
        button.setWidth("200px");
        button.setVisible(jobProgress.getType() != JobType.PRIORITY_PROJECT_UTILISATION
            && (jobProgress.getState() == JobState.RUNNING || jobProgress.getState() == JobState.INITIALIZED));
        button.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_TABLE_CANCEL_JOB);
        button.addClickHandler(new ClickHandler() {

          @Override
          public void onClick(final ClickEvent event) {
            cancelJob(jobProgress.getKey());
            button.setEnabled(false);
          }
        });
        panel.add(button);
        return panel;
      }
    };

    infoColumn = new TextColumn<JobProgress>() {
      @Override
      public String getValue(final JobProgress object) {
        String label;
        switch (object.getState()) {
        case RUNNING:
          label = M.messages().scenarioConnectJobInfoRunning(object.getHexagonCount());
          break;
        case COMPLETED:
          label = M.messages().scenarioConnectLoadJobButton(object.getType());
          break;
        default:
          label = M.messages().scenarioConnectJobInfo(object.getState());
          break;
        }

        return label;
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));
//    StyleUtil.I.setPlaceHolder(projectKeyValueBoxEditor, M.messages().scenarioConnectProjectKeyPlaceholder());
    toggleButton.getElement().appendChild(toggleText);
    toggleText.setInnerHTML(M.messages().labelOff());

    table.setSelectionModel(new SingleSelectionModel<JobProgress>());
    table.setLoadingByDefault(true);
    table.addSelectionChangeHandler(new Handler() {
      @Override
      public void onSelectionChange(final SelectionChangeEvent event) {
        final JobProgress jobProgress = ((SingleSelectionModel<JobProgress>) table.asDataTable().getSelectionModel()).getSelectedObject();
        if (jobProgress != null && jobProgress.getState() == JobState.COMPLETED) {
          openJob(jobProgress.getKey(), jobProgress.getType());
        }
      }
    });

    newJobButton.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_NEXT);
//    projectKeyValueBoxEditor.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_PROJECT_KEY);
//    newExportButton.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_REQUEST_EXPORT);
    toggleButton.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_TOGGLE_STATUS);
    table.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_TABLE);
    jobIdColumn.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_TABLE_JOB);
    stateIconDownloadColumn.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_TABLE_STATE);
    infoColumn.ensureDebugId(TestIDScenario.CONNECT_OVERVIEW_TABLE_INFO);
  }

  private void openJob(final String key, final JobType type) {
    presenter.openJob(key, type);
  }

  private void cancelJob(final String jobKey) {
    presenter.cancelJob(jobKey);
  }

  @UiHandler("toggleButton")
  void onLabelButtonClick(final ClickEvent e) {
    toggleText.setInnerHTML(toggleButton.getValue() ? M.messages().labelOn() : M.messages().labelOff());
    presenter.updateFilterStatus(toggleButton.getValue());
  }

  @UiHandler("newJobButton")
  public void newCalculationClick(final ClickEvent e) {
    presenter.newCalculation();
  }

//  @UiHandler("newExportButton")
//  public void newExportClick(final ClickEvent e) {
//    presenter.newExport(projectKeyValueBoxEditor.getValue());
//  }

//  @Override
//  public Editor<String> getExportEditor() {
//    return projectKeyValueBoxEditor.asEditor();
//  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
  }

  @Override
  public SimpleInteractiveClickDivTable<JobProgress> asDataTable() {
    return table;
  }

}