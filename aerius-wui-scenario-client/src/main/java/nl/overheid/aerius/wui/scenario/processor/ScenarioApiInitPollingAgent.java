/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.processor;

import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.constants.SharedConstantsEnum;
import nl.overheid.aerius.wui.main.retrievers.PollingAgentImpl;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.service.APIServiceAsync;

/**
 * Scenario Polling Agent.
 */
public class ScenarioApiInitPollingAgent extends PollingAgentImpl<String, JSONValue> {
  private static final int POLL_WAIT_TIME = 10000;

  private final APIServiceAsync service;

  /**
   * Constructor where the {@link APIServiceAsync} is being injected.
   * @param service the service being injected
   * @param appContext Application context
   */
  @Inject
  public ScenarioApiInitPollingAgent(final APIServiceAsync service, final ScenarioBaseAppContext<?, ?> appContext) {
    super(Math.max((int) appContext.getContext().getSetting(SharedConstantsEnum.CALCULATOR_POLLING_TIME), POLL_WAIT_TIME));
    this.service = service;
  }

  @Override
  protected void callService(final String key, final AsyncCallback<JSONValue> resultCallback) {
    service.requestStatus(key, resultCallback);
  }
}
