/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.importer;

import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.scenario.base.importer.ConnectImportDialogController;
import nl.overheid.aerius.wui.scenario.base.importer.ConnectImportDialogPanel;
import nl.overheid.aerius.wui.scenario.ui.ImportContentController;

/**
 * Handles import dialog and updates data from the import in the ui.
 */
public class ConnectImportController {
  protected ConnectImportDialogController idController;
  private ImportContentController controller;

  /**
   * Shows the import dialog.
   */
  public void showImportDialog() {
    showImportDialog(false);
  }

  public final void showImportDialog(final boolean dragMode) {
    final ConnectImportDialogPanel connectImportDialogPanel = new ConnectImportDialogPanel(M.messages().importPanelText(), dragMode);

    idController = new ConnectImportDialogController(connectImportDialogPanel) {
      @Override
      public void getImport(final String uid) {
        final String fileName = connectImportDialogPanel.getFileUploadFilename();
        if (fileName == null || fileName.isEmpty()) {
          connectImportDialogPanel.showError(M.messages().scenarioConnectImportNoFileSelected());
          setInProgress(false);
        } else {
          // The filename will contain a path (most likely), so remove everything before the last \ (which is escaped in both Java aswell as regex,
          // so 4 \'s)
          controller.addFile(fileName.replaceAll(".*\\\\", ""), uid);
          hide();
        }
      }
    };
    idController.show();
  }

  public void setContentController(final ImportContentController controller) {
    this.controller = controller;
  }
}
