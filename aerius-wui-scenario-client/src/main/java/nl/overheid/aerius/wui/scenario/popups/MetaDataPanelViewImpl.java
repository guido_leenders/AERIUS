/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.popups;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.shared.domain.context.ScenarioUserContext;
import nl.overheid.aerius.shared.domain.scenario.ScenarioMetaData;
import nl.overheid.aerius.wui.main.event.ImportEvent;
import nl.overheid.aerius.wui.main.widget.AttachedPopupBase;
import nl.overheid.aerius.wui.main.widget.AttachedPopupPanel;

public class MetaDataPanelViewImpl extends Composite implements MetaDataPanelView {

  interface MetaDataPanelViewImplUiBinder extends UiBinder<Widget, MetaDataPanelViewImpl> {}

  private static final MetaDataPanelViewImplUiBinder UI_BINDER = GWT.create(MetaDataPanelViewImplUiBinder.class);

  interface MetaDataPanelViewImplEventBinder extends EventBinder<MetaDataPanelViewImpl> {}

  private final MetaDataPanelViewImplEventBinder eventBinder = GWT.create(MetaDataPanelViewImplEventBinder.class);


  @UiField AttachedPopupBase wrapper;

  @UiField FlowPanel panel;

  @UiField Label reference;
  @UiField Label projectName;
  @UiField Label description;
  @UiField Label corporation;
  @UiField Label city;
  @UiField Label postcode;
  @UiField Label streetAddress;

  private HandlerRegistration resizeRegistration;
  private boolean layoutScheduled;

  private final ScheduledCommand layoutCmd = new ScheduledCommand() {
    @Override
    public void execute() {
      layoutScheduled = false;
      forceLayout();
    }
  };

  private final ResizeHandler resizeHandler = new ResizeHandler() {
    @Override
    public void onResize(final ResizeEvent event) {
      forceLayout();
    }
  };


  private final ScenarioUserContext userContext;

  @Inject
  public MetaDataPanelViewImpl(final ScenarioUserContext userContext, final EventBus eventBus) {
    this.userContext = userContext;

    initWidget(UI_BINDER.createAndBindUi(this));

    eventBinder.bindEventHandlers(this, eventBus);
  }

  @EventHandler
  public void onImportCOmpleteEvent(final ImportEvent e) {
    final ScenarioMetaData metaData = userContext.getScenario().getMetaData();

    reference.setText(metaData.getReference());
    projectName.setText(metaData.getProjectName());
    description.setText(metaData.getDescription());
    corporation.setText(metaData.getCorporation());
    city.setText(metaData.getCity());
    postcode.setText(metaData.getPostcode());
    streetAddress.setText(metaData.getStreetAddress());
  }

  @Override
  public void setPopup(final AttachedPopupPanel popup) {
    wrapper.setPopup(popup);
  }

  @Override
  protected void onAttach() {
    super.onAttach();

    resizeRegistration = Window.addResizeHandler(resizeHandler);

    scheduledLayout();
  }

  @Override
  protected void onDetach() {
    super.onDetach();

    resizeRegistration.removeHandler();
  }

  @Override
  public void onResize() {
    scheduledLayout();
  }

  private void scheduledLayout() {
    if (layoutScheduled) {
      return;
    }

    layoutScheduled = true;
    Scheduler.get().scheduleDeferred(layoutCmd);
  }

  private void forceLayout() {
    panel.getElement().getStyle().setProperty("maxHeight", wrapper.calculateMaxHeightBasedOnScreen(), Unit.PX);
  }
}
