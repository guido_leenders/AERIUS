/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.widget;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile.Situation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.table.HoverButtonColumn;
import nl.overheid.aerius.wui.main.widget.table.IsDataTable;
import nl.overheid.aerius.wui.main.widget.table.RadioButtonColumn;
import nl.overheid.aerius.wui.main.widget.table.SimpleDivTable;
import nl.overheid.aerius.wui.main.widget.table.TextColumn;

public class CalculationDetailTable extends Composite implements IsDataTable<ConnectCalculationFile>, LeafValueEditor<List<ConnectCalculationFile>> {

  interface CalculationDetailTableUiBinder extends UiBinder<Widget, CalculationDetailTable> {}

  private static final CalculationDetailTableUiBinder UI_BINDER = GWT.create(CalculationDetailTableUiBinder.class);

  public interface FileHandler {
    void removeFile(String uid);
  }

  @UiField SimpleDivTable<ConnectCalculationFile> divTable;
  @UiField Label labelColumnSituation;

  @UiField(provided = true) TextColumn<ConnectCalculationFile> fileColumn;
  @UiField(provided = true) RadioButtonColumn<ConnectCalculationFile, Situation> situationColumn;
  @UiField(provided = true) HoverButtonColumn<ConnectCalculationFile> deleteButtonColumn;

  private List<ConnectCalculationFile> value;
  private FileHandler handler;
  private boolean showSituationColumn;

  @Inject
  public CalculationDetailTable(final ScenarioContext userContext) {
    fileColumn = new TextColumn<ConnectCalculationFile>() {
      @Override
      public String getValue(final ConnectCalculationFile file) {
        return file.getFilename();
      }
    };
    situationColumn = new RadioButtonColumn<ConnectCalculationFile, Situation>() {
      @Override
      protected String getText(final Situation value) {
        return M.messages().scenarioConnectCalculationDocumentTableColumnSituationText(value);
      }

      @Override
      protected Situation getObject(final ConnectCalculationFile file) {
        return file.getSituation();
      }

      @Override
      protected void setObject(final ConnectCalculationFile file, final Situation situation) {
        file.setSituation(situation);
      }

      @Override
      public void applyCellOptions(final Widget cell, final ConnectCalculationFile object) {
        super.applyCellOptions(cell, object);
        // set visibility for the current cell only, the visibility of the header column is done in determineColumnVisibility().
        cell.setVisible(showSituationColumn);
      }

    };

    deleteButtonColumn = new HoverButtonColumn<ConnectCalculationFile>() {
      @Override
      protected void onClick(final Button button, final ConnectCalculationFile object) {
        handler.removeFile(object.getUuid());

        divTable.setRowData(value);
      }
    };

    initWidget(UI_BINDER.createAndBindUi(this));

    situationColumn.setValues(Situation.values());
    fileColumn.ensureDebugId(TestIDScenario.CALCULATION_DETAIL_TABLE_FILENAME);
    situationColumn.ensureDebugId(TestIDScenario.CALCULATION_DETAIL_TABLE_SITUATION);
    divTable.ensureDebugId(TestIDScenario.CALCULATION_DETAIL_TABLE);
  }

  @Override
  public SimpleDivTable<ConnectCalculationFile> asDataTable() {
    return divTable;
  }

  @Override
  public void setValue(final List<ConnectCalculationFile> value) {
    this.value = value;

    divTable.setRowData(value);
  }

  @Override
  public List<ConnectCalculationFile> getValue() {
    return value;
  }

  public void setFileHandler(final FileHandler handler) {
    this.handler = handler;
  }

  public void determineColumnVisibility(final ExportType exportType, final int amountFiles, final boolean showSituation) {
    showSituationColumn = showSituation || ((exportType == ExportType.PDF_CALCULATION || exportType == ExportType.GML_DELTA) && amountFiles > 1);

    // only header columns are changed here, the data columns are done when the row data is set again triggering a re-populating of the rows.
    labelColumnSituation.setVisible(showSituationColumn);
  }

}
