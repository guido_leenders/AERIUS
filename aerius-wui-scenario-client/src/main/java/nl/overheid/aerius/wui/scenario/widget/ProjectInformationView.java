/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.wui.main.widget.SwitchPanel;

public class ProjectInformationView extends Composite {
  private static final ProjectInformationViewImplUiBinder UI_BINDER = GWT.create(ProjectInformationViewImplUiBinder.class);

  interface ProjectInformationViewImplUiBinder extends UiBinder<Widget, ProjectInformationView> {}

  @UiField SwitchPanel projectInformationSwitchPanel;
  @UiField Label projectContactName;
  @UiField Label projectName;
  @UiField Label projectLocationAddress;
  @UiField Label projectLocationPostalCode;
  @UiField Label projectDescription;

  @Inject
  public ProjectInformationView() {
    initWidget(UI_BINDER.createAndBindUi(this));

    projectInformationSwitchPanel.showWidget(0);
  }
}
