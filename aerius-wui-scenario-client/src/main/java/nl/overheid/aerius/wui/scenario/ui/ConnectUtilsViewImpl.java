/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.widget.HeadingWidget;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsActivity.ViewState;
import nl.overheid.aerius.wui.scenario.widget.UtilBreadCrumbView;
import nl.overheid.aerius.wui.scenario.widget.UtilProgressView;
import nl.overheid.aerius.wui.scenario.widget.UtilTypeView;
import nl.overheid.aerius.wui.scenario.widget.UtilTypeView.UtilTypeHandler;
import nl.overheid.aerius.wui.scenario.widget.UtilUploadHandler;
import nl.overheid.aerius.wui.scenario.widget.UtilUploadView;

public class ConnectUtilsViewImpl extends Composite implements ConnectUtilsView, UtilTypeHandler, UtilUploadHandler {
  private static final ConnectUtilsViewImplUiBinder UI_BINDER = GWT.create(ConnectUtilsViewImplUiBinder.class);

  interface ConnectUtilsViewImplUiBinder extends UiBinder<Widget, ConnectUtilsViewImpl> {
  }

  public interface ConnectUtilsViewDriver extends SimpleBeanEditorDriver<ConnectUtilInformation, ConnectUtilsViewImpl> {
  }

  @Ignore @UiField UtilBreadCrumbView utilBreadCrumbView;
  @Path("utilType") @UiField(provided = true) UtilTypeView utilTypeView;
  @Ignore @UiField FlowPanel selectedTitleView;
  @Path("") @UiField(provided = true) UtilUploadView utilUploadView;
  @Ignore @UiField UtilProgressView progressView;
  @Ignore @UiField FlowPanel resultView;
  @Ignore @UiField FlowPanel actionView;
  @Ignore @UiField HeadingWidget documentsTitle;
  @Ignore @UiField Label successLabel;
  @Ignore @UiField Label warningLabel;
  @Ignore @UiField Label errorLabel;
  @UiField Button cancelButton;
  @UiField Button nextButton;

  private Presenter presenter;
  private ViewState currentViewState;

  @Inject
  public ConnectUtilsViewImpl(final HelpPopupController hpC, final ScenarioContext c, final UtilTypeView utilTypeView,
      final UtilUploadView utilUploadView) {
    this.utilUploadView = utilUploadView;
    this.utilTypeView = utilTypeView;

    initWidget(UI_BINDER.createAndBindUi(this));
    updateViewState(ViewState.SELECT);
    successLabel.ensureDebugId(TestIDScenario.CONNECT_UTIL_SUCCESS_LABEL);
    warningLabel.ensureDebugId(TestIDScenario.CONNECT_UTIL_WARNING_LABEL);
    errorLabel.ensureDebugId(TestIDScenario.CONNECT_UTIL_ERROR_LABEL);
    cancelButton.ensureDebugId(TestIDScenario.CONNECT_UTIL_CANCEL);
    nextButton.ensureDebugId(TestIDScenario.CONNECT_UTIL_NEXT);
  }

  protected void updateSelectedUtilState(final UtilType ut) {
    utilUploadView.updateSelectedUtilState(ut);
    documentsTitle.setText(M.messages().scenarioConnectSelectedUtilTitle(ut));
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;
    utilUploadView.setContentHandler(this);
    utilTypeView.setContentHandler(this);
  }

  @Override
  public void updateViewState(final ViewState newState) {
    resetViewState();
    utilBreadCrumbView.setCrumbSelected(newState);
    switch (newState) {
    case SELECT:
      switchSelectViewState();
      break;
    case UPLOAD:
      switchUploadViewState();
      break;
    case RESULT:
      switchResultViewState();
      break;
    default:
      break;
    }
    currentViewState = newState;
    utilUploadView.updateViewState(newState);
  }

  @Override
  public void updateUtilType(final UtilType utilType) {
    presenter.setUtilType(utilType);
  }

  @Override
  public void removeFile(final String uid) {
    presenter.removeFile(uid);
  }

  @Override
  public void submit() {
    presenter.submit();
  }

  @Override
  public void setData(final ArrayList<ConnectCalculationFile> files, final UtilType utilType) {
    utilUploadView.setData(files, utilType);
    if (currentViewState == ViewState.UPLOAD) {
      nextButton.setEnabled(!files.isEmpty());
    }
  }

  @UiHandler("cancelButton")
  public void onCancelClick(final ClickEvent e) {
    updateViewState(ViewState.SELECT);
  }

  @UiHandler("nextButton")
  public void onNextClick(final ClickEvent e) {
    if (currentViewState == ViewState.SELECT) {
      updateSelectedUtilState(utilTypeView.getUtilTypeEditor().getValue());
      updateViewState(ViewState.UPLOAD);
    } else {
      presenter.submit();
    }
  }

  @Override
  public void addFile(final String filename, final String uid) {
    presenter.addFile(filename, uid);
  }

  @Override
  public void setWarningMessage(final String message) {
    warningLabel.setText(message);
  }

  @Override
  public void setErrorMessage(final String message) {
    errorLabel.setText(message);
  }

  @Override
  public void setResultTitle(final String message) {
    successLabel.setText(message);
  }

  @Override
  public void setFilesWarningText(final String message) {
    utilUploadView.setFilesWarningText(message);
  }

  @Override
  public void setProgressDone() {
    progressView.setVisible(false);
  }

  private void resetViewState() {
    utilTypeView.setVisible(false);
    selectedTitleView.setVisible(false);
    utilUploadView.setVisible(false);
    resultView.setVisible(false);
    actionView.setVisible(false);
    cancelButton.setEnabled(false);
    nextButton.setEnabled(false);
    clearlabels();
  }

  private void switchSelectViewState() {
    utilTypeView.setVisible(true);
    actionView.setVisible(true);
    nextButton.setEnabled(true);
  }

  private void switchUploadViewState() {
    selectedTitleView.setVisible(true);
    utilUploadView.setVisible(true);
    actionView.setVisible(true);
    cancelButton.setEnabled(true);
    nextButton.setEnabled(!presenter.isFilesEmpy());
  }

  private void switchResultViewState() {
    selectedTitleView.setVisible(true);
    resultView.setVisible(true);
    progressView.setVisible(true);
  }

  private void clearlabels() {
    utilUploadView.clearLabel();
    successLabel.setText("");
    warningLabel.setText("");
    errorLabel.setText("");
  }

}
