/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.place.shared.PlaceController;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.scenario.base.place.SourcesPlace;
import nl.overheid.aerius.wui.scenario.base.ui.CalculationController;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.EmissionSourcesView;
import nl.overheid.aerius.wui.scenario.base.ui.SituationView;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;

/**
 * Presenter for emission sources list.
 */
public class ScenarioEmissionSourcesPresenter extends EmissionSourcesPresenter {
  @Inject
  public ScenarioEmissionSourcesPresenter(final PlaceController placeController, final CalculationController calculatorController,
      final SituationView parentView, final EmissionSourcesView view, @Assisted final SourcesPlace place, final ScenarioAppContext appContext) {
    super(placeController, calculatorController, parentView, view, place, appContext);
  }

  @Override
  public void onDelete(final Situation situation) {
    // Do nothing
  }

  @Override
  public void onAdd(final Situation situation) {
    // Do nothing
  }

  @Override
  public void switchSituations() {
    // Do nothing
  }

  @Override
  public boolean isEditableSituations() {
    return false;
  }
}
