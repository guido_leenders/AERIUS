/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.util;

import com.google.gwt.resources.client.ImageResource;

import nl.overheid.aerius.shared.domain.calculation.JobState;
import nl.overheid.aerius.wui.main.resources.R;

public final class JobStateImages {

  private JobStateImages() {
  }
  
  public static ImageResource getJobStateIcon(final JobState state) {
    final ImageResource resource;
    switch (state) {
    case RUNNING:
      resource = R.images().jobStateRunning();
      break;
    case COMPLETED:
      resource = R.images().jobStateCompleted();
      break;
    case INITIALIZED:
      resource = R.images().jobStateInital();
      break;
    case CANCELLED:
      resource = R.images().jobStateCancelled();
      break;
    default:
      resource = null;
      break;
    }
    return resource;
  }
}
