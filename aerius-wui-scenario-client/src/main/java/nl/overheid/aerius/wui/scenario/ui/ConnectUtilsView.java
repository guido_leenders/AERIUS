/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.ArrayList;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.wui.scenario.base.ui.FullPageView;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsActivity.ViewState;

public interface ConnectUtilsView extends IsWidget, FullPageView, Editor<ConnectUtilInformation> {


  interface Presenter extends ImportContentController {

    void removeFile(String uid);

    void submit();

    void setUtilType(UtilType utilType);

    boolean isFilesEmpy();

  }

  /**
   * Set the presenter for the view.
   *
   * @param presenter Presenter to use
   */
  void setPresenter(Presenter presenter);

  void setData(ArrayList<ConnectCalculationFile> files, UtilType exportType);

  void setWarningMessage(String string);

  void setErrorMessage(String string);

  void setResultTitle(String string);

  void setFilesWarningText(String string);

  void setProgressDone();

  void updateViewState(ViewState newState);

  void updateUtilType(UtilType utilType);

  void submit();

}
