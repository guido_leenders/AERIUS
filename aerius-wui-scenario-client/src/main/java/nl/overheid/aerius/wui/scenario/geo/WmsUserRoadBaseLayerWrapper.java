/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.geo;

import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import nl.overheid.aerius.geo.LayerPreparationUtil;
import nl.overheid.aerius.geo.MapLayer;
import nl.overheid.aerius.geo.MapLayoutPanel;
import nl.overheid.aerius.geo.WMSMapLayer;
import nl.overheid.aerius.geo.shared.LayerProps;
import nl.overheid.aerius.geo.shared.LayerWMSProps;
import nl.overheid.aerius.shared.service.FetchGeometryServiceAsync;
import nl.overheid.aerius.wui.main.event.PlaceChangeEvent;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.context.ScenarioBaseAppContext;
import nl.overheid.aerius.wui.scenario.base.events.CalculationInitEvent;
import nl.overheid.aerius.wui.scenario.base.events.CalculationResultsRetrievedEvent;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;

/**
 * Layer to highlight specific IMAER feature.
 */
public class WmsUserRoadBaseLayerWrapper {

  private static final String KEY_PARAM = "key";

  interface WmsFeatureLayerWrapperEventBinder extends EventBinder<WmsUserRoadBaseLayerWrapper> {
  }

  private final WmsFeatureLayerWrapperEventBinder eventBinder = GWT.create(WmsFeatureLayerWrapperEventBinder.class);

  private final FetchGeometryServiceAsync service;
  private final ScenarioBaseAppContext<?, ?> appContext;
  private final LayerWMSProps layerProps;
  private MapLayoutPanel map;
  private WMSMapLayer layer;
  private String key;

  @Inject
  public WmsUserRoadBaseLayerWrapper(final ScenarioBaseAppContext<?, ?> appContext, final FetchGeometryServiceAsync service,
      final LayerWMSProps layerWMSProps) {
    this.service = service;
    this.appContext = appContext;
    this.layerProps = layerWMSProps;
    LayerPreparationUtil.prepareLayer(service, getLayerProps());
    eventBinder.bindEventHandlers(this, appContext.getEventBus());
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent event) {
    if (event.getValue() instanceof ScenarioBasePlace) {
      updateLayer();
    }
  }

  @EventHandler
  void onCalculationInit(final CalculationInitEvent event) {
    updateLayer();
  }

  @EventHandler
  void onCalculationResultsRetrieved(final CalculationResultsRetrievedEvent event) {
    updateLayer();
  }

  /**
   * Updates the layer
   */
  private void updateLayer() {
    final String wmsFeatureLayerKey = layerProps == null ? null : getKey();
    if (wmsFeatureLayerKey != null && !wmsFeatureLayerKey.isEmpty()) {
      final LayerWMSProps layerProps = selectLayer(wmsFeatureLayerKey);
      if (layer == null) {
        if (layerProps != null) {
          LayerPreparationUtil.prepareLayer(service, layerProps, new AppAsyncCallback<LayerProps>() {
            @Override
            public void onSuccess(final LayerProps result) {
              detach();
              layerProps.setEnabled(true);
              layer = (WMSMapLayer) map.addLayer(layerProps);
            }
          });
        }
      } else if (layerProps == null) {
        detach();
      } else {
        layer.redraw();
      }
    }
  }

  private LayerWMSProps selectLayer(final String wmsFeatureLayerKey) {
    final Place where = appContext.getPlaceController().getWhere();
    if (!(where instanceof ScenarioBasePlace)) {
      return null;
    }

    LayerWMSProps selected = null;
    final boolean hasScenario = appContext.getUserContext().getCalculatedScenario() != null;
    if (hasScenario) {
      updateKey(wmsFeatureLayerKey);
      selected = getLayerProps();
    }
    return selected;
  }

  private void updateKey(final String key) {
    layerProps.setParamFilter(KEY_PARAM, key);
  }

  public void detach() {
    if (layer != null) {
      map.removeLayer((MapLayer) layer);
      layer = null;
    }
  }

  public void setMap(final MapLayoutPanel map) {
    this.map = map;
  }

  public void setVisible(final boolean visible) {
    if (layer == null) {
      return;
    }
    map.setVisible(layer, visible);
  }

  public LayerWMSProps getLayerProps() {
    return layerProps;
  }

  private String getKey() {
    return key;
  }

  public void setKey(final String key) {
    this.key = key;
  }
}
