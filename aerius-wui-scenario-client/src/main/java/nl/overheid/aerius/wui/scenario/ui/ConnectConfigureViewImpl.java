/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DragEnterEvent;
import com.google.gwt.event.dom.client.DragEnterHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.context.ScenarioContext;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationInformation.ExportType;
import nl.overheid.aerius.shared.test.TestIDScenario;
import nl.overheid.aerius.wui.main.widget.CollapsiblePanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.importer.ConnectImportController;
import nl.overheid.aerius.wui.scenario.widget.CalculationConfigurationView;
import nl.overheid.aerius.wui.scenario.widget.CalculationConfigurationView.ConfigurationContentHandler;
import nl.overheid.aerius.wui.scenario.widget.CalculationDetailTable;
import nl.overheid.aerius.wui.scenario.widget.CalculationDetailTable.FileHandler;
import nl.overheid.aerius.wui.scenario.widget.ProjectInformationView;

public class ConnectConfigureViewImpl extends Composite implements ConnectConfigureView, DragEnterHandler, FileHandler,
    ConfigurationContentHandler {

  private static final ConnectConfigureViewImplUiBinder UI_BINDER = GWT.create(ConnectConfigureViewImplUiBinder.class);

  interface ConnectConfigureViewImplUiBinder extends UiBinder<Widget, ConnectConfigureViewImpl> {}

  public interface ConnectConfigureViewDriver extends SimpleBeanEditorDriver<ConnectCalculationInformation, ConnectConfigureViewImpl> {}

  private Presenter presenter;

  @UiField ProjectInformationView projectInformationView;
  @Path("") @UiField(provided = true) CalculationConfigurationView calculationConfigurationView;
  @Path("files") @UiField(provided = true) CalculationDetailTable calculationDetailTable;

  @UiField CollapsiblePanel collapsibleConfiguration;

  @UiField Button importButton;
  @UiField Button calculate;
  @UiField Button cancel;

  private final ConnectImportController importController;

  @Inject
  public ConnectConfigureViewImpl(final HelpPopupController hpC, final ScenarioContext c, final CalculationConfigurationView configView,
      final CalculationDetailTable detailTable, final ConnectImportController importController) {
    this.calculationConfigurationView = configView;
    this.calculationDetailTable = detailTable;
    this.importController = importController;

    initWidget(UI_BINDER.createAndBindUi(this));

    addDomHandler(this, DragEnterEvent.getType());

    collapsibleConfiguration.ensureDebugId(TestIDScenario.CONNECT_CONFIGURE_OPTIONS_COLLAPSIBLE);
    importButton.ensureDebugId(TestIDScenario.CONNECT_CONFIGURE_IMPORT);
    calculate.ensureDebugId(TestIDScenario.CONNECT_CONFIGURE_CALCULATE);
    cancel.ensureDebugId(TestIDScenario.CONNECT_CONFIGURE_CANCEL);
  }

  @Override
  public void onDragEnter(final DragEnterEvent event) {
    event.preventDefault();
    event.stopPropagation();
    importController.showImportDialog(true);
  }

  @UiHandler("importButton")
  public void onImportClick(final ClickEvent e) {
    importController.showImportDialog();
  }

  @UiHandler("calculate")
  public void onCalculateClick(final ClickEvent e) {
    presenter.calculate();

    calculate.setEnabled(false);
  }

  @Override
  public void calculateFailed(final Throwable e) {
    calculate.setEnabled(true);
  }

  @UiHandler("cancel")
  public void onCancelClick(final ClickEvent e) {
    presenter.cancel();
  }

  @Override
  public void setPresenter(final Presenter presenter) {
    this.presenter = presenter;

    importController.setContentController(presenter);
    calculationDetailTable.setFileHandler(this);
    calculationConfigurationView.setContentHandler(this);
  }

  @Override
  public void setData(final ArrayList<ConnectCalculationFile> files, final ExportType exportType) {
    calculationDetailTable.determineColumnVisibility(exportType, files.size(), false);
    calculationDetailTable.asDataTable().setRowData(files);
    calculate.setEnabled(!files.isEmpty());
  }

  @Override
  public void removeFile(final String uid) {
    presenter.removeFile(uid);
  }

  @Override
  public void updateExportType(final ExportType exportType) {
    presenter.setExportType(exportType);
  }

}
