/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;

import nl.overheid.aerius.wui.geo.LayerPanel;
import nl.overheid.aerius.wui.main.widget.HelpPopupController;
import nl.overheid.aerius.wui.scenario.base.popups.InfoPanelPresenter;
import nl.overheid.aerius.wui.scenario.base.ui.ScenarioBaseTaskBarImpl;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.popups.MetaDataPanelView;

/**
 * Implementation of the Scenario taskbar.
 */
@Singleton
public class ScenarioTaskBarImpl extends ScenarioBaseTaskBarImpl {

  interface ScenarioTaskBarImplUiBinder extends UiBinder<Widget, ScenarioTaskBarImpl> {}

  private static final ScenarioTaskBarImplUiBinder UI_BINDER = GWT.create(ScenarioTaskBarImplUiBinder.class);

  @UiField(provided = true) MetaDataPanelView metaDataPanel;

  @Inject
  public ScenarioTaskBarImpl(final ScenarioAppContext appContext, final EventBus eventBus, final HelpPopupController hpC,
      final InfoPanelPresenter infoPanelPresenter, final LayerPanel layerPanel, final MetaDataPanelView metaDataPanel) {
    super(appContext, eventBus, hpC, infoPanelPresenter, layerPanel);
    this.metaDataPanel = metaDataPanel;

    initWidget(UI_BINDER.createAndBindUi(this));
  }
}
