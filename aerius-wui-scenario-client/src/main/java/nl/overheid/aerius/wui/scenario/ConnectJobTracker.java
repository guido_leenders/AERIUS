/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario;

import java.util.ArrayList;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.inject.Inject;

import nl.overheid.aerius.shared.domain.calculation.Calculation;
import nl.overheid.aerius.shared.domain.scenario.Scenario;
import nl.overheid.aerius.shared.service.ScenarioConnectServiceAsync;
import nl.overheid.aerius.wui.main.place.ScenarioBaseTheme;
import nl.overheid.aerius.wui.main.place.Situation;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.base.place.ScenarioBasePlace;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.processor.ScenarioResultProcessor;

public class ConnectJobTracker {
  private final class ConnectTrackerCallback extends AppAsyncCallback<ArrayList<Calculation>> {
    private final ScenarioBasePlace place;

    private ConnectTrackerCallback(final ScenarioBasePlace place) {
      this.place = place;
    }

    @Override
    public void onSuccess(final ArrayList<Calculation> result) {
      resultProcessor.init(result);

      final Scenario scenario = appContext.getUserContext().getScenario();
      place.setsId1(scenario.getSourceLists().get(0).getId());

      if (scenario.getSourceLists().size() == 2) {
        place.setsId2(scenario.getSourceLists().get(1).getId());
      }

      place.setSituation(Situation.SITUATION_ONE);
      place.setTheme(ScenarioBaseTheme.NATURE_AREAS);
      if (!result.isEmpty()) {
        appContext.getUserContext().getEmissionValueKey().setYear(result.get(0).getYear());
      }

      appContext.getPlaceController().goTo(place);

      Scheduler.get().scheduleDeferred(new ScheduledCommand() {
        @Override
        public void execute() {
          resultProcessor.start(result);
        }
      });
    }

    @Override
    public void onFailure(final Throwable caught) {
      appContext.getUserContext().setFollowingJob(false);

      super.onFailure(caught);
    }
  }

  private final ScenarioConnectServiceAsync connectService;
  private final ScenarioResultProcessor resultProcessor;
  private final ScenarioAppContext appContext;

  @Inject
  public ConnectJobTracker(final ScenarioConnectServiceAsync connectService, final ScenarioResultProcessor resultProcessor,
      final ScenarioAppContext appContext) {
    this.connectService = connectService;
    this.resultProcessor = resultProcessor;
    this.appContext = appContext;
  }

  public void trackJobProgress(final ScenarioBasePlace place) {
    appContext.getUserContext().setFollowingJob(true);
    connectService.fetchJobCalculations(place.getJobKey(), new ConnectTrackerCallback(place));
  }
}
