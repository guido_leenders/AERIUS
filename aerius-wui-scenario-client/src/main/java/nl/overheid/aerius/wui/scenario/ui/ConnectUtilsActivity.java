/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.overheid.aerius.wui.scenario.ui;

import java.util.Iterator;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectCalculationFile.Situation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation;
import nl.overheid.aerius.shared.domain.scenario.connect.ConnectUtilInformation.UtilType;
import nl.overheid.aerius.shared.service.ScenarioConnectServiceAsync;
import nl.overheid.aerius.wui.main.i18n.M;
import nl.overheid.aerius.wui.main.util.AppAsyncCallback;
import nl.overheid.aerius.wui.scenario.context.ScenarioAppContext;
import nl.overheid.aerius.wui.scenario.place.ConnectUtilsPlace;
import nl.overheid.aerius.wui.scenario.ui.ConnectUtilsViewImpl.ConnectUtilsViewDriver;

public class ConnectUtilsActivity extends AbstractActivity implements ConnectUtilsView.Presenter {

  private static final int MAX_REQUIRED_FILES = 1;
  private static final int MIN_REQUIRED_FILES = 2;
  private static final int DELTA_REQUIRED_FILES = 2;

  public enum ViewState {
    SELECT, UPLOAD, RESULT;
  }

  private final ConnectUtilsView view;
  private final ScenarioConnectServiceAsync service;

  private final SimpleBeanEditorDriver<ConnectUtilInformation, ConnectUtilsViewImpl> connectUtilDriver = GWT.create(ConnectUtilsViewDriver.class);

  @Inject
  public ConnectUtilsActivity(final ScenarioAppContext appContext, @Assisted final ConnectUtilsPlace place, final ConnectUtilsView view,
      final ScenarioConnectServiceAsync service) {
    this.view = view;
    this.service = service;
    connectUtilDriver.initialize((ConnectUtilsViewImpl) view);
  }

  @Override
  public void start(final AcceptsOneWidget panel, final EventBus eventBus) {
    view.setPresenter(this);
    final ConnectUtilInformation info = new ConnectUtilInformation();
    info.setUtilType(UtilType.VALIDATE);
    panel.setWidget(view);
    connectUtilDriver.edit(info);
    updateView();
  }

  @Override
  public void setUtilType(final UtilType utilType) {
    getLatestInfo().setUtilType(utilType);
    updateView();
  }

  @Override
  public void removeFile(final String uid) {
    view.setFilesWarningText("");
    service.deleteUploadedFile(uid, new AppAsyncCallback<Void>() {
      @Override
      public void onSuccess(final Void result) {
        final Iterator<ConnectCalculationFile> iterator = getLatestInfo().getFiles().iterator();
        while (iterator.hasNext()) {
          if (iterator.next().getUuid().equals(uid)) {
            iterator.remove();
            break;
          }
        }
        updateView();
      }
    });
  }

  @Override
  public void addFile(final String filename, final String uid) {
    final ConnectCalculationFile file = new ConnectCalculationFile();
    file.setUuid(uid);
    file.setFilename(filename);
    getLatestInfo().getFiles().add(file);
    updateView();
  }

  @Override
  public boolean isFilesEmpy() {
    return getLatestInfo().getFiles().isEmpty();
  }

  private void updateView() {
    final ConnectUtilInformation latestInfo = getLatestInfo();
    view.setData(latestInfo.getFiles(), latestInfo.getUtilType());
  }

  private ConnectUtilInformation getLatestInfo() {
    return connectUtilDriver.flush();
  }

  private boolean validateFiles(final ConnectUtilInformation latestInfo) {
    boolean valid = true;
    switch (latestInfo.getUtilType()) {
    case VALIDATE:
    case CONVERT:
      valid = validateMaxRequiredFiles(latestInfo, MAX_REQUIRED_FILES);
      break;
    case DELTA_VALUE:
      valid = validateDeltaValue(latestInfo);
      break;
    case HIGHEST_VALUE:
    case TOTAL_VALUE:
      valid = validateMinRequiredFiles(latestInfo, MIN_REQUIRED_FILES);
      break;
    default:
      break;
    }
    return valid;
  }

  private boolean validateMaxRequiredFiles(final ConnectUtilInformation latestInfo, final int max) {
    final boolean valid = latestInfo.getFiles().size() <= max;
    if (!valid) {
      view.setFilesWarningText(M.messages().scenarioConnectUtilMaxFileValidation(max));
    }
    return valid;
  }

  private boolean validateMinRequiredFiles(final ConnectUtilInformation latestInfo, final int min) {
    final boolean valid = latestInfo.getFiles().size() >= min;
    if (!valid) {
      view.setFilesWarningText(M.messages().scenarioConnectUtilMinFileValidation(min));
    }
    return valid;
  }

  private boolean validateDeltaValue(final ConnectUtilInformation latestInfo) {
    return validateMaxRequiredFiles(latestInfo, DELTA_REQUIRED_FILES)
        && validateMinRequiredFiles(latestInfo, DELTA_REQUIRED_FILES)
        && validateCurrentAndProposedSituationExist(latestInfo);
  }

  private boolean validateCurrentAndProposedSituationExist(final ConnectUtilInformation latestInfo) {
    int currentCount = 0;
    int proposedCount = 0;
    for (final ConnectCalculationFile file : latestInfo.getFiles()) {
      if (file.getSituation() == Situation.CURRENT) {
        currentCount++;
      } else if (file.getSituation() == Situation.PROPOSED) {
        proposedCount++;
      }
    }

    final boolean valid = currentCount == 1 && proposedCount == 1;
    if (!valid) {
      view.setFilesWarningText(M.messages().scenarioConnectUtilCorrectSituationValidation());
    }

    return valid;
  }

  @Override
  public void submit() {
    final ConnectUtilInformation latestInfo = getLatestInfo();
    if (validateFiles(latestInfo)) {
      view.updateViewState(ViewState.RESULT);
      service.submitUtil(latestInfo, new UtilAPIAsyncCallback() {

        @Override
        public void reportSuccess() {
          view.setResultTitle(M.messages().scenarioConnectUtilSuccessfull());
        }

        @Override
        public void reportWarningMessages(final String message) {
          view.setResultTitle(M.messages().scenarioConnectUtilWarningsTitle());
          view.setWarningMessage(M.messages().scenarioConnectUtilWarnings(message));
        }

        @Override
        public void reportErrorMessages(final String message) {
          view.setResultTitle(M.messages().scenarioConnectUtilErrorsTitle());
          view.setErrorMessage(M.messages().scenarioConnectUtilErrors(message));
        }

        @Override
        public void onFailure(final Throwable caught) {
          view.setProgressDone();
          view.setResultTitle(M.messages().scenarioConnectUtilErrorsTitle());
          view.setErrorMessage(M.messages().scenarioConnectUtilErrors(caught.getLocalizedMessage()));
        }

        @Override
        String formatValidationMessage(final int code, final String message) {
          return M.messages().scenarioConnectCalculationValidationMessage(code, message);
        }

        @Override
        void reportSuccessWithResult(final String url, final String warning) {
          view.setResultTitle(warning.isEmpty() ? M.messages().scenarioConnectUtilExcecuted()
              : M.messages().scenarioConnectUtilExcecutedWithWarning());
          view.setWarningMessage(warning.isEmpty() ? "" : M.messages().scenarioConnectUtilWarnings(warning));

          // trigger download 
          Window.open(url, "_blank", "");
        }

        @Override
        public void onSuccess() {
          view.setProgressDone();
        }
      });
    }
  }

}
