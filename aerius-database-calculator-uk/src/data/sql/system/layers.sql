{import_common 'UK/system/layers.sql'}

--update the layer capabilities URL to be calculator specific.
UPDATE system.layer_capabilities SET url = 'https://test.aerius.nl/calculator/aerius-geo-wms?' WHERE layer_capabilities_id = 1;

--wms layers from 11+
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (11, 11, 'wms', 'calculator:wms_nature_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (12, 12, 'wms', 'calculator:wms_depositions_jurisdiction_policies_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (13, 13, 'wms', 'calculator:wms_habitat_areas_sensitivity_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (14, 14, 'wms', 'calculator:wms_habitat_types');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (17, 10, 'wms', 'calculator:wms_habitats_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (18, 10, 'wms', 'calculator:wms_relevant_habitat_info_for_receptor_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (19, 25, 'wms', 'calculator:wms_calculation_substance_deposition_results_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (20, 27, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
	VALUES (21, 28, 'wms', 'calculator:wms_province_areas_view');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (23, 30, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (24, 31, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (25, 36, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_label');
INSERT INTO system.layers (layer_id, layer_properties_id, layer_type, name)
  VALUES (26, 37, 'wms', 'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage_label');

-- default layers
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer)
	VALUES (5, 0, true);
--INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer)
--	VALUES (21, 1, true);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer)
	VALUES (11, 4, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer)
	VALUES (12, 5, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer)
	VALUES (13, 6, false);
INSERT INTO system.default_layers (layer_id, sort_order, part_of_base_layer)
	VALUES (14, 7, false);
