{import_common 'UK/system/sld.sql'}

--Product specific SLD's,
--Calculator, ranged between 1001-1999
--INSERT INTO system.sld (sld_id, description) VALUES (1001,'Naam');
--REMEMBER: update when new SLDs are added: next sld_id = 1001

--Product specific WMS layers. Can refer to sld_id 1-999 (common) and 1001-1999 (calculator).
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (1,1,'calculator:wms_depositions_jurisdiction_policies_view','Totale depositie');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (2,4,'calculator:wms_habitat_areas_sensitivity_view','Stikstofgevoelige habitattypen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (3,2,'calculator:wms_nature_areas_view','Natuurgebieden (UI, gelabeled)');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (4,2,'calculator:wms_nature_areas_no_names_view','Natuurgebieden (PDF, zonder labels)','calculator:wms_nature_areas_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title, target_layer_name)
	VALUES (5,2,'calculator:wms_nature_areas_names_only_view','Natuurgebieden (PDF, alleen labels)','calculator:wms_nature_areas_view');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (6,4,'calculator:wms_relevant_habitat_info_for_receptor_view','Habitattype bij receptor');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (6,4,'calculator:wms_habitats_view','Habitattype voor natuurgebied');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (9,5,'calculator:wms_habitat_types','Habitattypen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (11,6,'calculator_report:wms_exceeding_calculation_deposition_results_view','Berekeningsresultaten (alleen op plekken van overschrijding)');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (12,6,'calculator_report:wms_calculations_deposition_results_difference_view','Verschil projectbijdrage');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (13,2,'calculator:wms_shipping_maritime_network_view','Scheepvaart netwerk');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (11,1,'calculator:wms_calculation_substance_deposition_results_view','Berekeningsresultaten');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (12,1,'calculator:wms_calculations_substance_deposition_results_difference_view','Verschil berekeningsresultaten');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (14,5,'calculator:wms_province_areas_view', 'Provincegrenzen');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (15,1,'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation','Resterende ruimte');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (16,1,'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage','Percentage resterende ruimte');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (17,8,'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_label','Resterende ruimte waarde');
INSERT INTO system.sld_wms_layers (sld_id, wms_zoom_level_id, name, title) VALUES (18,8,'calculator:wms_calculations_substance_deposition_results_difference_view_utilisation_percentage_label','Percentage resterende ruimte waarde');
