/**
 * Huidige opstelling:
 * De queues voor normale calculator hebben een hogere prioriteit dan die van PAA export.
 */

/*
 * Inserts system task_schedulers.
 */
INSERT INTO system.task_schedulers (name, description)
	VALUES ('OPS', 'Scheduler voor OPS');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('ASRM2', 'Scheduler voor AERIUS SRM2');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('CALCULATOR', 'Scheduler voor Calculator database');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('CONNECT', 'Scheduler voor Connect database');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('MELDING', 'Scheduler voor Melding (runs on Register database)');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('MONITOR', 'Scheduler voor Monitor database');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('REGISTER', 'Scheduler voor Register database');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('IMPORT_REGISTER', 'Scheduler voor import met Register database');

INSERT INTO system.task_schedulers (name, description)
	VALUES ('MESSAGE', 'Scheduler voor versturen van berichten (bijv. mails) naar gebruikers');

/*
 * Inserts system task_scheduler_queues.
 */
-- Calculator queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'calculation_ui', 'calculator berekeningen calculator (berekening-op-worker)', 3, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'CALCULATOR';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'calculation_ui', 'OPS berekeningen calculator(OPS)', 2, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'calculation_ui', 'SRM2 berekeningen calculator(ASRM2)', 2, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

-- Calculator PAA queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'paa_export', 'PAA berekeningen calculator(OPS)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'paa_export', 'PAA berekeningen calculator(ASRM2)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'paa_export', 'PAA berekeningen calculator(CALCULATOR)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'CALCULATOR';

-- Connect PAA queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'connect_paa_export', 'PAA berekeningen connect(OPS)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'connect_paa_export', 'PAA berekeningen connect(ASRM2)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'connect_paa_export', 'PAA berekeningen connect(CONNECT)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'CONNECT';

-- Calculator GML queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'gml_export', 'GML export calculator(OPS)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'gml_export', 'GML export calculator(ASRM2)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'gml_export', 'GML export calculator(CALCULATOR)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'CALCULATOR';

-- Connect GML queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'connect_gml_export', 'GML export connect(OPS)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'connect_gml_export', 'GML export connect(ASRM2)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'connect_gml_export', 'GML export connect(CONNECT)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'CONNECT';

-- OPS export queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'ops_export', 'OPS export aanvragen calculator(database)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'CALCULATOR';

-- Custom job queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'custom_jobs', 'Handmatige berekeningen(OPS)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'custom_jobs', 'Handmatige berekeningen(ASRM2)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

-- Register queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'permit', 'Register berekeningen aanvragen(OPS)', 1, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'permit', 'Register berekeningen aanvragen(ASRM2)', 1, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'permit', 'Register berekeningen vergunning aanvragen(database)', 1, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'REGISTER';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'register_export', 'Register export aanvragen(database)', 1, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'REGISTER';

-- Melding queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'melding', 'Melding berekeningen register (database)', 2, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'MELDING';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'melding', 'OPS berekeningen melding(OPS)', 3, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'OPS';

INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'melding', 'SRM2 berekeningen melding(ASRM2)', 3, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'ASRM2';

-- Monitor queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'monitor_export', 'Monitor export aanvragen(database)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'MONITOR';

-- Import queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'import', 'Register import (database)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'IMPORT_REGISTER';

-- Message queues
INSERT INTO system.task_scheduler_queues (task_scheduler_id, name, description, priority, max_capacity_use)
	SELECT task_scheduler_id, 'message', 'Berichten (default)', 0, 0.8
	FROM system.task_schedulers scheduler
	WHERE scheduler.name = 'MESSAGE';

