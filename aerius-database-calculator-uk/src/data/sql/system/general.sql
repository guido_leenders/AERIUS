{import_common 'UK/system/general.sql'}

{import_common 'UK/system/sectors.sql'}
{import_common 'UK/system/sectors_sectorgroup.sql'}

{import_common 'UK/system/plans.sql'}

{import_common 'UK/system/gml_conversions.sql'}

INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_POLLING_TIME', 300);
INSERT INTO system.constants (key, value) VALUES ('MELDING_ENABLED', 'false');
INSERT INTO system.constants (key, value) VALUES ('MELDING_URL', 'https://test.aerius.nl/melding/melden');

-- Geoserver URLs
INSERT INTO system.constants (key, value) VALUES ('SHARED_WMS_URL', 'https://test.aerius.nl/geoserver-calculator/wms?');
INSERT INTO system.constants (key, value) VALUES ('GEOSERVER_CAPIBILITIES_ORIGINAL_URL', 'https://test.aerius.nl:80/geoserver-calculator/wms?');
INSERT INTO system.constants (key, value) VALUES ('INTERNAL_WMS_PROXY_URL', 'https://test.aerius.nl/calculator/aerius-geo-wms?');
INSERT INTO system.constants (key, value) VALUES ('SHARED_WMS_PROXY_URL', 'https://test.aerius.nl/calculator/aerius-geo-wms?');

-- URLs where Geoserver can access the servlet for SLD
INSERT INTO system.constants (key, value) VALUES ('SHARED_SLD_URL', 'http://localhost:8080/calculator/aerius/');
INSERT INTO system.constants (key, value) VALUES ('SHARED_SLD_EXTERNAL_URL', 'https://test.aerius.nl/calculator/aerius/');

-- Calculation config for the UI
INSERT INTO system.constants (key, value) VALUES ('MAX_OPS_UNITS_UI', '20000');
INSERT INTO system.constants (key, value) VALUES ('MIN_RECEPTORS_UI', '5');
INSERT INTO system.constants (key, value) VALUES ('MAX_RECEPTORS_UI', '2000');
INSERT INTO system.constants (key, value) VALUES ('MAX_CHUNKS_CONCURRENT_WEBSERVER', '8');
INSERT INTO system.constants (key, value) VALUES ('MAX_ENGINE_SOURCES_UI', '1000000');
INSERT INTO system.constants (key, value) VALUES ('NUMBER_OF_TASKCLIENT_THREADS', '40');

-- Help URLs
INSERT INTO system.constants (key, value) VALUES ('MANUAL_URL', 'http://www.aerius.nl/nl/manuals/calculator');
INSERT INTO system.constants (key, value) VALUES ('PAS_PERMIT_EXPLANATION_URL', 'http://pas.bij12.nl/content/vergunning-aanvragen-vergunningvrij');

-- Email for PAA (PDF Export)
INSERT INTO system.constants (key, value) VALUES ('PAA_MAIL_DOWNLOAD_LINK', 'https://test.aerius.nl/downloads/');

-- PAA constants
INSERT INTO system.constants (key, value) VALUES ('PAA_CONCEPT_WATERMARK', 'true');

-- Calculation constants
INSERT INTO system.constants (key, value) VALUES ('CALCULATE_EMISSIONS_MAX_RADIUS_DISTANCE_UI', '150000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATE_EMISSIONS_DEFAULT_RADIUS_DISTANCE_UI', '10000');
INSERT INTO system.constants (key, value) VALUES ('CONVERT_LINE_TO_POINTS_SEGMENT_SIZE', '25');
INSERT INTO system.constants (key, value) VALUES ('CONVERT_POLYGON_TO_POINTS_GRID_SIZE', '100');
INSERT INTO system.constants (key, value) VALUES ('SHIPPING_INLAND_LOCK_HEAT_CONTENT_FACTOR', '0.15');

-- Calculator limits
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_SOURCES', '1000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_LINE_LENGTH', '25000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_MAX_POLYGON_SURFACE', '1000');
INSERT INTO system.constants (key, value) VALUES ('CALCULATOR_LIMITS_RECEPTORS_RECEPTOR_TILE', '9');

-- Layer references
INSERT INTO system.constants (key, value) VALUES ('LAYER_SHIP_NETWORK', 'calculator:wms_shipping_maritime_network_view');
INSERT INTO system.constants (key, value) VALUES ('LAYER_MARITIME_SHIP_NETWORK', 'nzvss_beg,nzvss_sym,nzvss_sep');

-- Calculator min/max years (used for selection-range for user).
INSERT INTO system.constants (key, value) VALUES ('MIN_YEAR', '2014');
INSERT INTO system.constants (key, value) VALUES ('MAX_YEAR', '2030');

-- SyncWorker constants
INSERT INTO system.constants (key, value) VALUES ('DEVELOPMENT_SPACE_SYNC_TIMESTAMP', '0');

-- Source related
INSERT INTO system.constants (key, value) VALUES ('MARITIME_MOORING_INLAND_ROUTE_SHIPPING_NODE_SEARCH_DISTANCE', '100');

-- Connect constants
INSERT INTO system.constants (key, value) VALUES ('CONNECT_ROOT_REDIRECT_URL', 'https://connect.aerius.nl/api/doc/');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_GENERATING_APIKEY_ENABLED', 'true');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_MAX_CONCURRENT_JOBS_FOR_NEW_USER', '3');
INSERT INTO system.constants (key, value) VALUES ('CONNECT_UTIL_VALIDATION_THRESHOLD_KB', '10000');
