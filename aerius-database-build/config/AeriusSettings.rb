#
# Application specific settings. Define override values in AeriusSettings.User.rb (same location).
#

$runscripts_path = File.expand_path(File.dirname(__FILE__) + '/../scripts/').fix_pathname


$git_bin_path = '' # Git bin folder should be in PATH


$pg_username = 'aerius'
$pg_password = '...' # Override in AeriusSettings.User.rb


$dbdata_dir = 'db-data/aeriusII/'
$dbdata_path = File.expand_path(File.dirname(__FILE__) + '/../../../' + $dbdata_dir).fix_pathname


$database_name_prefix = 'AERIUSII'

$db_function_prefix = 'ae'


$sftp_data_path = 'sftp://aerius-sftp.rivm.nl/'

$sftp_data_readonly_username = 'aeriusro'
$sftp_data_readonly_password = '...' # Override in AeriusSettings.User.rb

$sftp_data_username = '' # Please Leave this blank and do not use two way sync
$sftp_data_password = '' # Please Leave this blank and do not use two way sync