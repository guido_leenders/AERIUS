#!/bin/bash

# Don't store command history for this script
unset HISTFILE

################################################################################
# Important variables
################################################################################

HOME_FOLDER='/home/vagrant'
PROJECT_FOLDER="${HOME_FOLDER}/project_data"
SCRIPTS_FOLDER="${HOME_FOLDER}/scripts"

REPOS_HOSTNAME='github.com'
REPOS_CLONE_URL="https://${REPOS_HOSTNAME}/aerius/AERIUS-II.git"

DATABASE_USERNAME='aerius'
DATABASE_PASSWORD='aerius'


################################################################################
# The real work is done below
################################################################################

# Use NL Ubuntu archive mirror
sed -i 's/http:\/\/archive.ubuntu.com\/ubuntu\//http:\/\/nl.archive.ubuntu.com\/ubuntu\//' /etc/apt/sources.list

# Update the system - do this noninteractively so we don't get blocked by package upgrades requiring user input
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade


################################################################################
# Install the mandatory tools
################################################################################

# Configure the locale
export LANGUAGE='en_US.UTF-8'
export LANG='en_US.UTF-8'
export LC_ALL='en_US.UTF-8'
locale-gen en_US.UTF-8
dpkg-reconfigure --frontend=noninteractive locales

# Install utilities
apt-get -y install git
apt-get -y install zip p7zip-full
apt-get -y install language-pack-en



################################################################################
# Install the graphical environment
################################################################################

# Force encoding
echo 'LANG=en_US.UTF-8' >> /etc/environment
echo 'LANGUAGE=en_US.UTF-8' >> /etc/environment
echo 'LC_ALL=en_US.UTF-8' >> /etc/environment
echo 'LC_CTYPE=en_US.UTF-8' >> /etc/environment

# Install Ubuntu desktop and VirtualBox guest tools
apt-get -y install lubuntu-desktop
apt-get -y install linux-generic linux-headers-generic linux-headers-"$(uname -r)" build-essential module-assistant
apt-get -y install virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11



################################################################################
# Install the development tools
################################################################################

# Install PostgreSQL and PostGIS
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' | tee -a /etc/apt/sources.list.d/pgdg.list
apt-get update 
apt-get -y install postgresql-9.4-postgis-2.2 pgadmin3

sudo -u postgres bash -c 'psql -c "CREATE USER '"${DATABASE_USERNAME}"' WITH PASSWORD '"'${DATABASE_PASSWORD}'"'; ALTER ROLE '"${DATABASE_USERNAME}"' SUPERUSER;"'

cat << EOF > /etc/postgresql/9.4/main/pg_hba.conf
# Allow any user on the local system to connect to any database with
# any database user name using Unix-domain sockets (the default for local
# connections).
#
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             all                                     trust

# The same using local loopback TCP/IP connections.
#
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    all             all             127.0.0.1/32            trust

# The same over IPv6.
#
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    all             all             ::1/128                 trust

# The same using a host name (would typically cover both IPv4 and IPv6).
#
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    all             all             localhost               trust
EOF


# Install Ruby
apt-get -y install ruby2.3
gem install net-ssh
gem install net-sftp
gem install clbustos-rtf

# Install Maven
apt-get install maven


################################################################################
# Set up the project folder
################################################################################

mkdir -p "${PROJECT_FOLDER}"
cd "${PROJECT_FOLDER}"

# Configure git
git config --global user.name "${GIT_USERNAME}"
git config --global user.email "${GIT_EMAIL}"

# Clone repo - need to find a better way to do this without storing the password
cat << EOF > ~/.netrc
machine ${REPOS_HOSTNAME}
login ${GIT_USERNAME}
password ${GIT_PASSWORD}
EOF

git clone "${REPOS_CLONE_URL}"

rm -v ~/.netrc

# Write user specific configuration for the database build script
cat << EOF > "${PROJECT_FOLDER}/AERIUS-II/aerius-database-build/config/AeriusSettings.User.rb"
\$pg_hostname = 'localhost'
\$pg_password = 'aerius'
\$sftp_data_readonly_password = '${FTP_RO_PASSWORD}'
EOF

mkdir -p "${PROJECT_FOLDER}/db-data/aeriusII"
mkdir -p "${SCRIPTS_FOLDER}"

chown -R vagrant:vagrant "${HOME_FOLDER}"



################################################################################
# Cleanup box
################################################################################
apt-get -y autoclean
apt-get -y clean
apt-get -y autoremove

################################################################################
# (Re)start the needed services
################################################################################
service lightdm start
service postgresql restart
